package com.wali.gamecenter.report;

import android.content.Context;

public class ReportDBObserver {
    Context mContext;

    public ReportDBObserver(Context context) {
        this.mContext = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0094  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onChange(boolean r10) {
        /*
            r9 = this;
            r4 = 0
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()
            boolean r0 = r0.isCTAluseable
            if (r0 == 0) goto L_0x001a
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()
            boolean r0 = r0.isDebugging
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = "ReportManager"
            java.lang.String r1 = "CTA mode"
            android.util.Log.e(r0, r1)
        L_0x0019:
            return
        L_0x001a:
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()
            boolean r0 = r0.isDebugging
            if (r0 == 0) goto L_0x003a
            java.lang.String r0 = "ReportManager"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "***** Report database is changed ***** selfChange="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
        L_0x003a:
            com.wali.gamecenter.report.db.ReportDataDao r0 = com.wali.gamecenter.report.db.DBManager.getReportDao()
            if (r0 == 0) goto L_0x0019
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()
            r0.stopCheck()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r1 = 0
            java.lang.Object r2 = com.wali.gamecenter.report.ReportManager.mDBSyncObj     // Catch:{ Throwable -> 0x008e }
            monitor-enter(r2)     // Catch:{ Throwable -> 0x008e }
            com.wali.gamecenter.report.db.ReportDataDao r0 = com.wali.gamecenter.report.db.DBManager.getReportDao()     // Catch:{ all -> 0x008b }
            com.xiaomi.greendao.query.QueryBuilder r1 = r0.queryBuilder()     // Catch:{ all -> 0x008b }
            monitor-exit(r2)     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x016b
            long r2 = r1.count()     // Catch:{ Throwable -> 0x008e }
        L_0x005f:
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()     // Catch:{ Throwable -> 0x0168 }
            boolean r0 = r0.isDebugging     // Catch:{ Throwable -> 0x0168 }
            if (r0 == 0) goto L_0x007f
            java.lang.String r0 = "ReportManager"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0168 }
            r7.<init>()     // Catch:{ Throwable -> 0x0168 }
            java.lang.String r8 = "cursor count = "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x0168 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ Throwable -> 0x0168 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0168 }
            android.util.Log.e(r0, r7)     // Catch:{ Throwable -> 0x0168 }
        L_0x007f:
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x0094
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()
            r0.checkTime()
            goto L_0x0019
        L_0x008b:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x008b }
            throw r0     // Catch:{ Throwable -> 0x008e }
        L_0x008e:
            r0 = move-exception
            r2 = r4
        L_0x0090:
            r0.printStackTrace()
            goto L_0x007f
        L_0x0094:
            r4 = 10
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x009c
            if (r10 != 0) goto L_0x015f
        L_0x009c:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00fc }
            r2.<init>()     // Catch:{ Throwable -> 0x00fc }
            java.util.List r0 = r1.list()     // Catch:{ Throwable -> 0x00fc }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Throwable -> 0x00fc }
        L_0x00a9:
            boolean r0 = r1.hasNext()     // Catch:{ Throwable -> 0x00fc }
            if (r0 == 0) goto L_0x0139
            java.lang.Object r0 = r1.next()     // Catch:{ Throwable -> 0x00fc }
            com.wali.gamecenter.report.db.ReportData r0 = (com.wali.gamecenter.report.db.ReportData) r0     // Catch:{ Throwable -> 0x00fc }
            java.lang.Long r3 = r0.getId()     // Catch:{ Throwable -> 0x00fc }
            long r4 = r3.longValue()     // Catch:{ Throwable -> 0x00fc }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x00fc }
            r2.add(r3)     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r3 = r0.getMethod()     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r0 = r0.getParam()     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r0 = com.wali.gamecenter.report.utils.ReportUtils.decryptUrl(r0)     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r4 = "post"
            boolean r3 = r4.equals(r3)     // Catch:{ Throwable -> 0x00fc }
            if (r3 == 0) goto L_0x010a
            com.wali.gamecenter.report.ReportManager r3 = com.wali.gamecenter.report.ReportManager.getInstance()     // Catch:{ Throwable -> 0x00fc }
            boolean r3 = r3.isDebugging     // Catch:{ Throwable -> 0x00fc }
            if (r3 == 0) goto L_0x00f8
            java.lang.String r3 = "ReportManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00fc }
            r4.<init>()     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r5 = "POST cursor value = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x00fc }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x00fc }
            android.util.Log.e(r3, r4)     // Catch:{ Throwable -> 0x00fc }
        L_0x00f8:
            r6.add(r0)     // Catch:{ Throwable -> 0x00fc }
            goto L_0x00a9
        L_0x00fc:
            r0 = move-exception
            com.wali.gamecenter.report.ReportManager r1 = com.wali.gamecenter.report.ReportManager.getInstance()
            boolean r1 = r1.isDebugging
            if (r1 == 0) goto L_0x0019
            r0.printStackTrace()
            goto L_0x0019
        L_0x010a:
            com.wali.gamecenter.report.ReportManager r3 = com.wali.gamecenter.report.ReportManager.getInstance()     // Catch:{ Throwable -> 0x00fc }
            boolean r3 = r3.isDebugging     // Catch:{ Throwable -> 0x00fc }
            if (r3 == 0) goto L_0x012a
            java.lang.String r3 = "ReportManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00fc }
            r4.<init>()     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r5 = "GET cursor value = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x00fc }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Throwable -> 0x00fc }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x00fc }
            android.util.Log.e(r3, r4)     // Catch:{ Throwable -> 0x00fc }
        L_0x012a:
            java.lang.String r3 = "_rr_"
            com.wali.gamecenter.report.InsertRecord r4 = new com.wali.gamecenter.report.InsertRecord     // Catch:{ Throwable -> 0x00fc }
            r5 = 0
            r4.<init>(r0, r5)     // Catch:{ Throwable -> 0x00fc }
            r0 = 10
            com.wali.gamecenter.report.utils.AutoThreadFactory.AppendTask(r3, r4, r0)     // Catch:{ Throwable -> 0x00fc }
            goto L_0x00a9
        L_0x0139:
            int r0 = r2.size()     // Catch:{ Throwable -> 0x00fc }
            r1 = 1
            if (r0 <= r1) goto L_0x014b
            java.lang.Object r1 = com.wali.gamecenter.report.ReportManager.mDBSyncObj     // Catch:{ Throwable -> 0x00fc }
            monitor-enter(r1)     // Catch:{ Throwable -> 0x00fc }
            com.wali.gamecenter.report.db.ReportDataDao r0 = com.wali.gamecenter.report.db.DBManager.getReportDao()     // Catch:{ all -> 0x015c }
            r0.deleteByKeyInTx(r2)     // Catch:{ all -> 0x015c }
            monitor-exit(r1)     // Catch:{ all -> 0x015c }
        L_0x014b:
            if (r6 == 0) goto L_0x0019
            int r0 = r6.size()     // Catch:{ Throwable -> 0x00fc }
            if (r0 <= 0) goto L_0x0019
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()     // Catch:{ Throwable -> 0x00fc }
            r0.postReport(r6)     // Catch:{ Throwable -> 0x00fc }
            goto L_0x0019
        L_0x015c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x015c }
            throw r0     // Catch:{ Throwable -> 0x00fc }
        L_0x015f:
            com.wali.gamecenter.report.ReportManager r0 = com.wali.gamecenter.report.ReportManager.getInstance()
            r0.checkTime()
            goto L_0x0019
        L_0x0168:
            r0 = move-exception
            goto L_0x0090
        L_0x016b:
            r2 = r4
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wali.gamecenter.report.ReportDBObserver.onChange(boolean):void");
    }
}
