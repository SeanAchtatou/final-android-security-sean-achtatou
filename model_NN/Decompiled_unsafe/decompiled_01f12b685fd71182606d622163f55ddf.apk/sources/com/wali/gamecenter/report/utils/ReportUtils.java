package com.wali.gamecenter.report.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import java.util.Random;

public class ReportUtils {
    public static final String MANUALLY_OPEN_STATUS = "manually_open_status";
    private static final byte[] URL_KEY = "_&L^W%&*20120724#$U%I)M%I^@".getBytes();

    public static boolean canUseNetwork(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(MANUALLY_OPEN_STATUS, false);
    }

    public static String decryptUrl(String str) {
        return new String(encryptUrl(Base64.decode(str)));
    }

    public static byte[] encrypt(byte[] bArr, byte[] bArr2) {
        if (bArr == null) {
            return null;
        }
        if (bArr2 == null) {
            return bArr;
        }
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] bArr3 = new byte[length];
        int i = 0;
        while (i < length) {
            int i2 = 0;
            while (i2 < length2 && i + i2 < length) {
                bArr3[i + i2] = (byte) (bArr[i + i2] ^ bArr2[i2]);
                i2++;
            }
            i += length2;
        }
        return bArr3;
    }

    public static String encryptUrl(String str) {
        return (str == null || str.length() < 1) ? "" : Base64.encode(encryptUrl(str.getBytes()));
    }

    static byte[] encryptUrl(byte[] bArr) {
        return encrypt(bArr, URL_KEY);
    }

    public static String[] getSecurityParameters(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            sb.append("_");
        }
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2);
            sb.append("_");
        }
        if (!TextUtils.isEmpty(str3)) {
            sb.append(str3);
            sb.append("_");
        }
        if (sb.length() <= 0) {
            return null;
        }
        sb.deleteCharAt(sb.length() - 1);
        String sb2 = sb.toString();
        if (sb2.length() == 0) {
            return null;
        }
        try {
            byte[] bytes = sb2.getBytes("UTF-8");
            if (bytes == null || bytes.length <= 0) {
                return null;
            }
            String md5 = MD5.getMD5(bytes);
            if (TextUtils.isEmpty(md5)) {
                return null;
            }
            int length = md5.length();
            Random random = new Random(System.currentTimeMillis());
            int nextInt = random.nextInt(length - 3);
            int nextInt2 = random.nextInt((length - nextInt) - 1) + nextInt + 1;
            return new String[]{md5.substring(nextInt, nextInt2), nextInt + "." + (nextInt2 - nextInt)};
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isWifiNetwork(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo.getType() == 1 || activeNetworkInfo.getType() == 6;
    }
}
