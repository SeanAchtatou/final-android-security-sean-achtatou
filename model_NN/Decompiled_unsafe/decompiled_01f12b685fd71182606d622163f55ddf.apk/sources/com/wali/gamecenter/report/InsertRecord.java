package com.wali.gamecenter.report;

import android.text.TextUtils;
import android.util.Log;
import com.wali.gamecenter.report.io.HttpConnectionManager;
import com.wali.gamecenter.report.utils.Base64;
import java.util.Iterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class InsertRecord implements Runnable {
    private int method;
    private String param;

    public InsertRecord(String str, int i) {
        this.param = str;
        this.method = i;
    }

    public void run() {
        if (!ReportManager.getInstance().isDebug() || !TextUtils.isEmpty(this.param)) {
            DefaultHttpClient httpClient = HttpConnectionManager.getHttpClient();
            if (this.method == 1) {
                if (ReportManager.getInstance().isDebug()) {
                    ReportManager.getInstance().reportLog().debug("ReportManager", "data=" + this.param);
                }
                HttpPost httpPost = new HttpPost("https://data.game.xiaomi.com/p.do");
                try {
                    if (ReportManager.getInstance().isDebug()) {
                        Log.e("ReportManager", "send post data=" + this.param);
                    }
                    httpPost.setEntity(new StringEntity("data=" + Base64.encode(this.param.getBytes())));
                    HttpResponse execute = httpClient.execute(httpPost);
                    if (ReportManager.getInstance().isDebug()) {
                        Log.e("ReportManager", execute.getStatusLine().toString());
                    }
                    int statusCode = execute.getStatusLine().getStatusCode();
                    if (execute.getEntity() != null) {
                        execute.getEntity().consumeContent();
                    }
                    if (statusCode != 200) {
                        if (ReportManager.getInstance().isDebug()) {
                            Log.e("ReportManager", "send post data fail");
                        }
                        Iterator<String> it = ReportManager.spiltReportToJson(this.param).iterator();
                        while (it.hasNext()) {
                            ReportManager.getInstance().saveReportToDB("post", it.next(), false);
                        }
                    } else if (ReportManager.getInstance().isDebug()) {
                        Log.e("ReportManager", "send post data success");
                    }
                } catch (Exception e) {
                    if (ReportManager.getInstance().isDebug()) {
                        e.printStackTrace();
                    }
                    ReportManager.getInstance().reportLog().error("ReportManager", "report post error", e);
                    Iterator<String> it2 = ReportManager.spiltReportToJson(this.param).iterator();
                    while (it2.hasNext()) {
                        ReportManager.getInstance().saveReportToDB("post", it2.next(), false);
                    }
                    try {
                        httpPost.abort();
                    } catch (Exception e2) {
                        if (ReportManager.getInstance().isDebug()) {
                            e2.printStackTrace();
                        }
                    }
                }
            } else if (this.method == 0) {
                ReportManager.getInstance().reportLog().debug("ReportManager", "https://data.game.xiaomi.com/1px.gif?" + this.param);
                if (ReportManager.getInstance().isDebug()) {
                    Log.e("ReportManager", "send get data=" + this.param);
                }
                HttpGet httpGet = new HttpGet("https://data.game.xiaomi.com/1px.gif?" + this.param);
                try {
                    HttpResponse execute2 = httpClient.execute(httpGet);
                    if (ReportManager.getInstance().isDebug()) {
                        Log.e("ReportManager", execute2.getStatusLine().toString());
                    }
                    int statusCode2 = execute2.getStatusLine().getStatusCode();
                    if (execute2.getEntity() != null) {
                        execute2.getEntity().consumeContent();
                    }
                    if (statusCode2 != 200) {
                        ReportManager.getInstance().saveReportToDB("get", this.param, false);
                    } else if (ReportManager.getInstance().isDebug()) {
                        Log.e("ReportManager", "send get data success");
                    }
                } catch (Exception e3) {
                    if (ReportManager.getInstance().isDebug()) {
                        e3.printStackTrace();
                    }
                    ReportManager.getInstance().reportLog().error("ReportManager", "report get error", e3);
                    ReportManager.getInstance().saveReportToDB("get", this.param, false);
                    try {
                        httpGet.abort();
                    } catch (Exception e4) {
                        if (ReportManager.getInstance().isDebug()) {
                            e3.printStackTrace();
                        }
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("param is null");
        }
    }
}
