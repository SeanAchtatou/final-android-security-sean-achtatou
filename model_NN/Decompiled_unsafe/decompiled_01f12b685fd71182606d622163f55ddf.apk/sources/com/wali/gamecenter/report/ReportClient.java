package com.wali.gamecenter.report;

public interface ReportClient {
    public static final String BEGIN_DOWNLOAD = "begindownload";
    public static final String BEGIN_INSTALL = "begininstall";
    public static final String DOWNLOAD_FAIL = "downloadFail";
    public static final String DOWNLOAD_FINISH = "downloadfinish";
    public static final String DOWNLOAD_PAUSE = "downloadPause";
    public static final String INSTALL_SUCCESS = "installsuccess";
    public static final String OPEN_GAMECENTER = "op_newmsgcnt_";
    public static final String TRACK = "track";
}
