package com.wali.gamecenter.report.utils;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Vector;

public final class AutoThreadFactory {
    private static final int DEFAULT_MAX_LIFE = 60000;
    private static HashMap<String, TaskQueue> _group = new HashMap<>(2);

    public class TaskQueue {
        private static final int DEF_MAX_WAIT_TIME = 60000;
        /* access modifiers changed from: private */
        public WorkThreads contoller;
        private String name;
        private int priority = 3;
        /* access modifiers changed from: private */
        public long wait_time = 60000;

        public class WorkThreads extends Thread {
            private Object _lock_ = new Object();
            /* access modifiers changed from: private */
            public volatile boolean running = true;
            private Runnable working;
            /* access modifiers changed from: private */
            public Vector<Runnable> works = new Vector<>(5);

            public WorkThreads() {
            }

            public void append(Runnable runnable) {
                synchronized (this._lock_) {
                    this.works.add(runnable);
                    this._lock_.notifyAll();
                }
            }

            public void append(Runnable[] runnableArr) {
                synchronized (this._lock_) {
                    for (Runnable add : runnableArr) {
                        this.works.add(add);
                    }
                    this._lock_.notifyAll();
                }
            }

            public void cancel() {
                synchronized (this._lock_) {
                    this.running = false;
                    this._lock_.notifyAll();
                }
            }

            public void cancel(Runnable runnable) {
                synchronized (this._lock_) {
                    if (this.working != runnable) {
                        this.works.remove(runnable);
                    }
                }
            }

            /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
                r7.running = false;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    r6 = 0
                L_0x0001:
                    boolean r0 = r7.running
                    if (r0 == 0) goto L_0x005d
                    java.util.Vector<java.lang.Runnable> r0 = r7.works
                    int r0 = r0.size()
                    if (r0 <= 0) goto L_0x001d
                    java.util.Vector<java.lang.Runnable> r0 = r7.works
                    java.lang.Object r0 = r0.firstElement()
                    java.lang.Runnable r0 = (java.lang.Runnable) r0
                    r0.run()     // Catch:{ Exception -> 0x0044 }
                L_0x0018:
                    java.util.Vector<java.lang.Runnable> r0 = r7.works
                    r0.remove(r6)
                L_0x001d:
                    java.util.Vector<java.lang.Runnable> r0 = r7.works
                    int r0 = r0.size()
                    if (r0 != 0) goto L_0x0001
                    java.lang.Object r1 = r7._lock_
                    monitor-enter(r1)
                    long r2 = java.lang.System.currentTimeMillis()     // Catch:{ InterruptedException -> 0x0069 }
                    java.lang.Object r0 = r7._lock_     // Catch:{ InterruptedException -> 0x0069 }
                    com.wali.gamecenter.report.utils.AutoThreadFactory$TaskQueue r4 = com.wali.gamecenter.report.utils.AutoThreadFactory.TaskQueue.this     // Catch:{ InterruptedException -> 0x0069 }
                    long r4 = r4.wait_time     // Catch:{ InterruptedException -> 0x0069 }
                    r0.wait(r4)     // Catch:{ InterruptedException -> 0x0069 }
                    java.util.Vector<java.lang.Runnable> r0 = r7.works     // Catch:{ InterruptedException -> 0x0069 }
                    int r0 = r0.size()     // Catch:{ InterruptedException -> 0x0069 }
                    if (r0 <= 0) goto L_0x0049
                    monitor-exit(r1)     // Catch:{ all -> 0x0041 }
                    goto L_0x0001
                L_0x0041:
                    r0 = move-exception
                    monitor-exit(r1)     // Catch:{ all -> 0x0041 }
                    throw r0
                L_0x0044:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0018
                L_0x0049:
                    long r4 = java.lang.System.currentTimeMillis()     // Catch:{ InterruptedException -> 0x0069 }
                    long r2 = r4 - r2
                    com.wali.gamecenter.report.utils.AutoThreadFactory$TaskQueue r0 = com.wali.gamecenter.report.utils.AutoThreadFactory.TaskQueue.this     // Catch:{ InterruptedException -> 0x0069 }
                    long r4 = r0.wait_time     // Catch:{ InterruptedException -> 0x0069 }
                    int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                    if (r0 < 0) goto L_0x006a
                    r0 = 0
                    r7.running = r0     // Catch:{ InterruptedException -> 0x0069 }
                    monitor-exit(r1)     // Catch:{ all -> 0x0041 }
                L_0x005d:
                    java.util.Vector<java.lang.Runnable> r0 = r7.works
                    r0.clear()
                    com.wali.gamecenter.report.utils.AutoThreadFactory$TaskQueue r0 = com.wali.gamecenter.report.utils.AutoThreadFactory.TaskQueue.this
                    r1 = 0
                    com.wali.gamecenter.report.utils.AutoThreadFactory.TaskQueue.WorkThreads unused = r0.contoller = r1
                    return
                L_0x0069:
                    r0 = move-exception
                L_0x006a:
                    monitor-exit(r1)     // Catch:{ all -> 0x0041 }
                    goto L_0x0001
                */
                throw new UnsupportedOperationException("Method not decompiled: com.wali.gamecenter.report.utils.AutoThreadFactory.TaskQueue.WorkThreads.run():void");
            }
        }

        TaskQueue() {
        }

        TaskQueue(long j, int i, String str) {
            if (i < 1 || i > 10) {
                this.priority = 3;
            } else {
                this.priority = i;
            }
            if (j < 60000) {
                this.wait_time = 60000;
            } else {
                this.wait_time = j;
            }
            this.name = str;
        }

        private void checkThreadStatus() {
            if (this.contoller == null) {
                this.contoller = new WorkThreads();
                if (!TextUtils.isEmpty(this.name)) {
                    this.contoller.setName(this.name);
                }
                this.contoller.setPriority(this.priority);
                this.contoller.start();
            } else if (!this.contoller.running) {
                this.contoller = null;
                this.contoller = new WorkThreads();
                if (!TextUtils.isEmpty(this.name)) {
                    this.contoller.setName(this.name);
                }
                this.contoller.setPriority(this.priority);
                this.contoller.start();
            }
        }

        public void appendTask(Runnable runnable) {
            checkThreadStatus();
            if (this.contoller != null) {
                this.contoller.append(runnable);
            }
        }

        public void appendTask(Runnable[] runnableArr) {
            checkThreadStatus();
            this.contoller.append(runnableArr);
        }

        public int getTaskCount() {
            if (this.contoller != null) {
                return this.contoller.works.size();
            }
            return 0;
        }

        public void terminat() {
            if (this.contoller != null) {
                this.contoller.cancel();
            }
        }
    }

    public static void AppendTask(String str, Runnable runnable, int i) {
        synchronized (_group) {
            TaskQueue taskQueue = _group.get(str);
            if (taskQueue == null) {
                TaskQueue taskQueue2 = new TaskQueue(60000, i, str);
                _group.put(str, taskQueue2);
                taskQueue2.appendTask(runnable);
            } else {
                taskQueue.appendTask(runnable);
            }
        }
    }

    public static void ClearTask(String str) {
        synchronized (_group) {
            TaskQueue taskQueue = _group.get(str);
            if (taskQueue != null) {
                taskQueue.terminat();
            }
        }
    }

    public static int GetTaskCount(String str) {
        TaskQueue taskQueue = _group.get(str);
        if (taskQueue != null) {
            return taskQueue.getTaskCount();
        }
        return 0;
    }

    public static void clearTasks() {
        synchronized (_group) {
            for (TaskQueue terminat : _group.values()) {
                terminat.terminat();
            }
            _group.clear();
        }
    }
}
