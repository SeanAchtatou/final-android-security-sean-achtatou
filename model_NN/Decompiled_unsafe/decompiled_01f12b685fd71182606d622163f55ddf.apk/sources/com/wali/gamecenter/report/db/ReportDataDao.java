package com.wali.gamecenter.report.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.tendcloud.tenddata.dc;
import com.xiaomi.greendao.AbstractDao;
import com.xiaomi.greendao.Property;
import com.xiaomi.greendao.internal.DaoConfig;

public class ReportDataDao extends AbstractDao<ReportData, Long> {
    public static final String TABLENAME = "REPORT_DATA";

    public class Properties {
        public static final Property Id = new Property(0, Long.class, dc.V, true, "_id");
        public static final Property Method = new Property(1, String.class, "method", false, "METHOD");
        public static final Property Param = new Property(2, String.class, "param", false, "PARAM");
    }

    public ReportDataDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public ReportDataDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(SQLiteDatabase sQLiteDatabase, boolean z) {
        sQLiteDatabase.execSQL("CREATE TABLE " + (z ? "IF NOT EXISTS " : "") + "'REPORT_DATA' (" + "'_id' INTEGER PRIMARY KEY ," + "'METHOD' TEXT NOT NULL ," + "'PARAM' TEXT);");
    }

    public static void dropTable(SQLiteDatabase sQLiteDatabase, boolean z) {
        sQLiteDatabase.execSQL("DROP TABLE " + (z ? "IF EXISTS " : "") + "'REPORT_DATA'");
    }

    /* access modifiers changed from: protected */
    public void bindValues(SQLiteStatement sQLiteStatement, ReportData reportData) {
        sQLiteStatement.clearBindings();
        Long id = reportData.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        sQLiteStatement.bindString(2, reportData.getMethod());
        String param = reportData.getParam();
        if (param != null) {
            sQLiteStatement.bindString(3, param);
        }
    }

    public Long getKey(ReportData reportData) {
        if (reportData != null) {
            return reportData.getId();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean isEntityUpdateable() {
        return true;
    }

    public ReportData readEntity(Cursor cursor, int i) {
        String str = null;
        Long valueOf = cursor.isNull(i + 0) ? null : Long.valueOf(cursor.getLong(i + 0));
        String string = cursor.getString(i + 1);
        if (!cursor.isNull(i + 2)) {
            str = cursor.getString(i + 2);
        }
        return new ReportData(valueOf, string, str);
    }

    public void readEntity(Cursor cursor, ReportData reportData, int i) {
        String str = null;
        reportData.setId(cursor.isNull(i + 0) ? null : Long.valueOf(cursor.getLong(i + 0)));
        reportData.setMethod(cursor.getString(i + 1));
        if (!cursor.isNull(i + 2)) {
            str = cursor.getString(i + 2);
        }
        reportData.setParam(str);
    }

    public Long readKey(Cursor cursor, int i) {
        if (cursor.isNull(i + 0)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i + 0));
    }

    /* access modifiers changed from: protected */
    public Long updateKeyAfterInsert(ReportData reportData, long j) {
        reportData.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }
}
