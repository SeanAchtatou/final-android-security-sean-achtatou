package com.wali.gamecenter.report.db;

public class ReportData {
    private Long id;
    private String method;
    private String param;

    public ReportData() {
    }

    public ReportData(Long l) {
        this.id = l;
    }

    public ReportData(Long l, String str, String str2) {
        this.id = l;
        this.method = str;
        this.param = str2;
    }

    public Long getId() {
        return this.id;
    }

    public String getMethod() {
        return this.method;
    }

    public String getParam() {
        return this.param;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setMethod(String str) {
        this.method = str;
    }

    public void setParam(String str) {
        this.param = str;
    }
}
