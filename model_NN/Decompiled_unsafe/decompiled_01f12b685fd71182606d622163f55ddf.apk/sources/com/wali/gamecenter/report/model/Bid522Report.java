package com.wali.gamecenter.report.model;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.gson.Gson;
import com.xiaomi.gson.annotations.SerializedName;
import com.xiaomi.gson.reflect.TypeToken;
import java.net.URLEncoder;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Bid522Report extends BaseReport {
    private String appid;
    @SerializedName("imei")
    private String bidImei = this.imei;
    private String channelId;
    private String cpChannel;
    private String md5imei;
    private String mid;

    public Bid522Report(Context context) {
        super(context);
        setAc("bid522");
        this.imei = null;
        this.index = null;
        setType(null);
        this.ext = new EXT();
        this.ext.packageName = context.getPackageName();
    }

    public String getAppid() {
        return this.appid;
    }

    public String getBidImei() {
        return this.bidImei;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public String getCpChannel() {
        return this.cpChannel;
    }

    public String getMd5imei() {
        return this.md5imei;
    }

    public String getMid() {
        return this.mid;
    }

    public void setAppid(String str) {
        this.appid = str;
    }

    public void setBidImei(String str) {
        this.bidImei = str;
    }

    public void setChannelId(String str) {
        this.channelId = str;
    }

    public void setCpChannel(String str) {
        this.cpChannel = str;
    }

    public void setMd5imei(String str) {
        this.md5imei = str;
    }

    public void setMid(String str) {
        this.mid = str;
    }

    public String toString() {
        String json = toJson();
        try {
            JSONObject jSONObject = new JSONObject(json);
            jSONObject.put("ext", jSONObject.optJSONObject("ext").toString());
            json = jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : ((Map) new Gson().fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType())).entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            if (!TextUtils.isEmpty((CharSequence) entry.getValue())) {
                sb.append((String) entry.getKey());
                sb.append("=");
                try {
                    sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                } catch (Exception e2) {
                    sb.append("");
                    e2.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
