package com.wali.gamecenter.report;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.wali.gamecenter.report.db.DBManager;
import com.wali.gamecenter.report.db.ReportData;
import com.wali.gamecenter.report.log.ReportLog;
import com.wali.gamecenter.report.model.BaseReport;
import com.wali.gamecenter.report.utils.AutoThreadFactory;
import com.wali.gamecenter.report.utils.ReportUtils;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportManager {
    private static long DELAY_LOOP_TIME = 60000;
    static final String GET_URL = "https://data.game.xiaomi.com/1px.gif?";
    public static final int MAX_REPORT_COUNT = 10;
    public static final int METHOD_GET = 0;
    public static final int METHOD_POST = 1;
    /* access modifiers changed from: private */
    public static int MSG_FORCE_SEND_REPORT = 48;
    static final String POST_URL = "https://data.game.xiaomi.com/p.do";
    private static String[] SYNCUPLOADAC = {"bid522", "xmsdk", ReportAction.XM_CLIENT};
    static final String TAG = "ReportManager";
    static Object mDBSyncObj = new Object();
    private static ReportManager mInstance;
    private Context ctx;
    boolean isCTAluseable = false;
    boolean isDebugging = false;
    private ReportDBObserver mDBObserver;
    private Handler mHandler;
    private ReportLog mReportLog;
    private HandlerThread mReportThread;

    ReportManager(Context context) {
        this.ctx = context;
        try {
            this.mReportThread = new HandlerThread("gamecenter_report");
            if (this.mReportThread != null) {
                this.mReportThread.start();
                this.mHandler = new Handler(this.mReportThread.getLooper()) {
                    public void handleMessage(Message message) {
                        super.handleMessage(message);
                        if (ReportManager.MSG_FORCE_SEND_REPORT == message.what) {
                            ReportManager.getInstance().forceSendReport();
                        }
                    }
                };
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        DBManager.init(context);
        this.mDBObserver = new ReportDBObserver(context);
        this.mReportLog = new ReportLog(context, "report.log");
    }

    public static void Init(Context context) {
        if (mInstance == null) {
            mInstance = new ReportManager(context);
        }
    }

    public static void Init(Context context, boolean z) {
        if (mInstance == null) {
            mInstance = new ReportManager(context);
            if (z) {
                mInstance.forceSendReport();
            }
        }
    }

    public static ReportManager getInstance() {
        return mInstance;
    }

    private String report_string_post(ArrayList<String> arrayList) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("uid", ReportBaseParams.getInstance().uid);
            jSONObject.put("os", ReportBaseParams.getInstance().os);
            jSONObject.put("app_version", ReportBaseParams.getInstance().version_name);
            JSONArray jSONArray = new JSONArray();
            jSONObject.put("content", jSONArray);
            for (int i = 0; i < arrayList.size(); i++) {
                if (!TextUtils.isEmpty(arrayList.get(i))) {
                    jSONArray.put(i, new JSONObject(arrayList.get(i)));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (this.isDebugging) {
            Log.e("report_string_post", jSONObject.toString());
        }
        return jSONObject.toString();
    }

    public static ArrayList<String> spiltReportToJson(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("content")) {
                JSONArray optJSONArray = jSONObject.optJSONArray("content");
                for (int i = 0; i < optJSONArray.length(); i++) {
                    arrayList.add(optJSONArray.getJSONObject(i).toString());
                }
                return arrayList;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void checkTime() {
        if (this.mHandler != null) {
            if (this.mHandler.hasMessages(MSG_FORCE_SEND_REPORT)) {
                this.mHandler.removeMessages(MSG_FORCE_SEND_REPORT);
            }
            this.mHandler.sendEmptyMessageDelayed(MSG_FORCE_SEND_REPORT, DELAY_LOOP_TIME);
        }
    }

    public void forceSendReport() {
        if (this.mDBObserver != null) {
            this.mDBObserver.onChange(false);
        }
    }

    public boolean isDebug() {
        return this.isDebugging;
    }

    /* access modifiers changed from: protected */
    public boolean isSyncUpload(String str) {
        for (String equals : SYNCUPLOADAC) {
            if (str.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void postReport(ArrayList<String> arrayList) {
        AutoThreadFactory.AppendTask("_rr_", new InsertRecord(report_string_post(arrayList), 1), 1);
    }

    public void release() {
        if (this.mDBObserver != null) {
            this.mDBObserver = null;
        }
    }

    public ReportLog reportLog() {
        return this.mReportLog;
    }

    public void saveReport(Report report) {
        if (report != null) {
            saveReportToDB("post", report.getParamsToJSON(this.ctx), false);
        }
    }

    public void saveReportToDB(String str, String str2, boolean z) {
        try {
            ReportData reportData = new ReportData(null, str, ReportUtils.encryptUrl(str2));
            synchronized (mDBSyncObj) {
                DBManager.getReportDao().insert(reportData);
            }
            if (this.isDebugging) {
                Log.e(TAG, "save data:" + str + "<<>>" + str2);
            }
            if (this.mDBObserver != null && z) {
                this.mDBObserver.onChange(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendReport(Report report) {
        if (report != null) {
            ReportType type = report.getType();
            if (this.isDebugging) {
                Log.e(TAG, "type=" + type);
            }
            String client = report.getClient();
            if (type == ReportType.VIEW && !TextUtils.equals(client, ReportClient.TRACK)) {
                saveReportToDB("post", report.getParamsToJSON(this.ctx), true);
            } else if (this.isCTAluseable) {
                saveReportToDB("get", report.getParamsToString(this.ctx), false);
            } else {
                AutoThreadFactory.AppendTask("_rr_", new InsertRecord(report.getParamsToString(this.ctx), 0), 1);
            }
        }
    }

    public void sendReport(BaseReport baseReport) {
        if (baseReport != null) {
            ReportType type = baseReport.getType();
            if (this.isDebugging) {
                Log.e(TAG, "type=" + type);
            }
            if (!isSyncUpload(baseReport.getAc())) {
                saveReportToDB("post", baseReport.toJson(), true);
            } else if (this.isCTAluseable) {
                saveReportToDB("get", baseReport.toString(), false);
            } else if (type == ReportType.JARCRASH) {
                saveReportToDB("post", baseReport.toJson(), true);
                forceSendReport();
            } else {
                AutoThreadFactory.AppendTask("_rr_", new InsertRecord(baseReport.toString(), 0), 1);
            }
        }
    }

    public void setCTAUseable(boolean z) {
        this.isCTAluseable = z;
    }

    public void setDebug(boolean z) {
        this.isDebugging = z;
    }

    /* access modifiers changed from: package-private */
    public void stopCheck() {
        if (this.mHandler != null && this.mHandler.hasMessages(MSG_FORCE_SEND_REPORT)) {
            this.mHandler.removeMessages(MSG_FORCE_SEND_REPORT);
        }
    }

    public void syncReport() {
        if (this.mDBObserver != null) {
            this.mDBObserver.onChange(true);
        }
    }
}
