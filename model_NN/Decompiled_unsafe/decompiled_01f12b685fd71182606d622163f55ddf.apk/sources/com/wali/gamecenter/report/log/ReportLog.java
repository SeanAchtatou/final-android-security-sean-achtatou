package com.wali.gamecenter.report.log;

import android.content.Context;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Locale;

public class ReportLog implements ILogAppender {
    public static final int MAX_FILE_SIZE = 52428800;
    private FileWriter logfile;
    private File mFile = null;
    private BufferedWriter writer;

    public enum LOG_LEVEL {
        NONE,
        ERROR,
        DEBUG
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public ReportLog(Context context, String str) {
        try {
            this.mFile = new File(context.getFilesDir(), str);
            this.logfile = new FileWriter(this.mFile.getAbsolutePath(), true);
            this.writer = new BufferedWriter(this.logfile, 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void appendLog(LOG_LEVEL log_level, String str, String str2, Throwable th) {
        if (this.logfile != null) {
            String str3 = str2 == null ? "" : str2;
            if (th != null) {
                str3 = str3 + "\n" + Log.getStackTraceString(th);
            }
            try {
                this.writer.append((CharSequence) String.format(Locale.US, "%1$tY/%1$tm/%1$td %1$tH:%1$tM [%2$-5s]-[%3$s] %4$s\r\n", new Date(), log_level.toString(), str, str3));
                if (log_level == LOG_LEVEL.ERROR || log_level == LOG_LEVEL.DEBUG) {
                    this.writer.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public void close() {
        this.mFile = null;
        if (this.logfile != null) {
            try {
                this.writer.close();
            } catch (Exception e) {
            }
            try {
                this.logfile.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.logfile = null;
            this.writer = null;
        }
    }

    public void debug(String str, String str2) {
        appendLog(LOG_LEVEL.DEBUG, str, str2, null);
    }

    public void error(String str, String str2, Throwable th) {
        appendLog(LOG_LEVEL.ERROR, str, str2, th);
    }
}
