package com.wali.gamecenter.report.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ZSIMInfo {
    public static String SHA1(String str) {
        try {
            return Base64.encodeToString(MessageDigest.getInstance("SHA1").digest(str.getBytes()), 8).substring(0, 16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00ec, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00ed, code lost:
        r8 = r2;
        r2 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00c8 A[SYNTHETIC, Splitter:B:50:0x00c8] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d4 A[SYNTHETIC, Splitter:B:58:0x00d4] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00dd A[SYNTHETIC, Splitter:B:63:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00ec A[ExcHandler: all (r2v12 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:25:0x0089] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String get3gMacAddress(android.content.Context r9) {
        /*
            r3 = 0
            r4 = -1
            r1 = 0
            java.lang.String r0 = "wifi"
            java.lang.Object r0 = r9.getSystemService(r0)
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0
            if (r0 == 0) goto L_0x0016
            boolean r0 = r0.isWifiEnabled()
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = ""
        L_0x0015:
            return r1
        L_0x0016:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00cc }
            java.lang.String r2 = "/sys/class/net/"
            r0.<init>(r2)     // Catch:{ Exception -> 0x00cc }
            java.io.File[] r5 = r0.listFiles()     // Catch:{ Exception -> 0x00cc }
            r2 = r1
        L_0x0022:
            int r0 = r5.length     // Catch:{ Exception -> 0x00cc }
            if (r3 >= r0) goto L_0x0103
            r0 = r5[r3]     // Catch:{ Exception -> 0x00cc }
            boolean r0 = r0.isDirectory()     // Catch:{ Exception -> 0x00cc }
            if (r0 == 0) goto L_0x0101
            r0 = r5[r3]     // Catch:{ Exception -> 0x00cc }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00cc }
            java.lang.String r6 = "lo"
            boolean r0 = r0.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x00cc }
            if (r0 != 0) goto L_0x0101
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            r6.<init>()     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            r7 = r5[r3]     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            java.lang.String r7 = r7.getPath()     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            java.lang.String r7 = "/carrier"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x00b8, all -> 0x00c5 }
            r0.read()     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            if (r0 == 0) goto L_0x0062
            r0.close()     // Catch:{ IOException -> 0x00e1 }
        L_0x0061:
            r0 = r1
        L_0x0062:
            r2 = r0
        L_0x0063:
            if (r4 == r3) goto L_0x00ff
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            r6.<init>()     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            java.lang.String r7 = "/sys/class/net/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            r3 = r5[r3]     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            java.lang.String r3 = r3.getName()     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            java.lang.String r5 = "/address"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            r0.<init>(r3)     // Catch:{ Exception -> 0x00cf, all -> 0x00da }
            int r2 = r0.available()     // Catch:{ Exception -> 0x00f1, all -> 0x00ec }
            if (r2 <= 0) goto L_0x00fd
            byte[] r3 = new byte[r2]     // Catch:{ Exception -> 0x00f1, all -> 0x00ec }
            r0.read(r3)     // Catch:{ Exception -> 0x00f1, all -> 0x00ec }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x00f1, all -> 0x00ec }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00f1, all -> 0x00ec }
            java.lang.String r3 = "\n"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x00f4, all -> 0x00ec }
            if (r3 == r4) goto L_0x00ad
            r4 = 0
            java.lang.String r2 = r2.substring(r4, r3)     // Catch:{ Exception -> 0x00f4, all -> 0x00ec }
            int r3 = r2.length()     // Catch:{ Exception -> 0x00f4, all -> 0x00ec }
            if (r3 != 0) goto L_0x00ad
            r2 = r1
        L_0x00ad:
            if (r0 == 0) goto L_0x00b2
            r0.close()     // Catch:{ IOException -> 0x00e8 }
        L_0x00b2:
            java.lang.String r1 = upperCase(r2)     // Catch:{ Exception -> 0x00cc }
            goto L_0x0015
        L_0x00b8:
            r0 = move-exception
            r0 = r2
        L_0x00ba:
            if (r0 == 0) goto L_0x00c0
            r0.close()     // Catch:{ IOException -> 0x00e4 }
        L_0x00bf:
            r0 = r1
        L_0x00c0:
            int r3 = r3 + 1
            r2 = r0
            goto L_0x0022
        L_0x00c5:
            r0 = move-exception
        L_0x00c6:
            if (r2 == 0) goto L_0x00cb
            r2.close()     // Catch:{ IOException -> 0x00e6 }
        L_0x00cb:
            throw r0     // Catch:{ Exception -> 0x00cc }
        L_0x00cc:
            r0 = move-exception
            goto L_0x0015
        L_0x00cf:
            r0 = move-exception
            r0 = r2
            r2 = r1
        L_0x00d2:
            if (r0 == 0) goto L_0x00b2
            r0.close()     // Catch:{ IOException -> 0x00d8 }
            goto L_0x00b2
        L_0x00d8:
            r0 = move-exception
            goto L_0x00b2
        L_0x00da:
            r0 = move-exception
        L_0x00db:
            if (r2 == 0) goto L_0x00e0
            r2.close()     // Catch:{ IOException -> 0x00ea }
        L_0x00e0:
            throw r0     // Catch:{ Exception -> 0x00cc }
        L_0x00e1:
            r0 = move-exception
            goto L_0x0061
        L_0x00e4:
            r0 = move-exception
            goto L_0x00bf
        L_0x00e6:
            r2 = move-exception
            goto L_0x00cb
        L_0x00e8:
            r0 = move-exception
            goto L_0x00b2
        L_0x00ea:
            r2 = move-exception
            goto L_0x00e0
        L_0x00ec:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x00db
        L_0x00f1:
            r2 = move-exception
            r2 = r1
            goto L_0x00d2
        L_0x00f4:
            r3 = move-exception
            goto L_0x00d2
        L_0x00f6:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x00c6
        L_0x00fb:
            r2 = move-exception
            goto L_0x00ba
        L_0x00fd:
            r2 = r1
            goto L_0x00ad
        L_0x00ff:
            r2 = r1
            goto L_0x00b2
        L_0x0101:
            r0 = r2
            goto L_0x00c0
        L_0x0103:
            r3 = r4
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wali.gamecenter.report.utils.ZSIMInfo.get3gMacAddress(android.content.Context):java.lang.String");
    }

    public static String getDeviceID(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String getIMSI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static String getMacAddress(Context context) {
        WifiInfo connectionInfo;
        String macAddress;
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null || (macAddress = connectionInfo.getMacAddress()) == null) {
            return null;
        }
        return upperCase(macAddress);
    }

    public static String getMacAddressNew(Context context) {
        WifiInfo connectionInfo;
        String macAddress;
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null || (macAddress = connectionInfo.getMacAddress()) == null) {
            return null;
        }
        return macAddress;
    }

    public static String getPhoneType(Context context) {
        int phoneType = ((TelephonyManager) context.getSystemService("phone")).getPhoneType();
        return phoneType == 1 ? "GSM" : phoneType == 2 ? "CDMA" : "未知";
    }

    public static String getSIMCountryIso(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimCountryIso();
    }

    public static String getSIMNumber(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
    }

    public static String getSIMOperator(Context context) {
        String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
        return ("46000".equals(simOperator) || "46002".equals(simOperator) || "45412".equals(simOperator) || "46007".equals(simOperator)) ? "中国移动" : "46001".equals(simOperator) ? "中国联通" : "46003".equals(simOperator) ? "中国电信" : "未知";
    }

    public static String getSIMOperatorEnName(Context context) {
        String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
        return ("46000".equals(simOperator) || "46002".equals(simOperator) || "45412".equals(simOperator) || "46007".equals(simOperator)) ? "CMCC" : ("46001".equals(simOperator) || "46006".equals(simOperator)) ? "UNICOM" : "46003".equals(simOperator) ? "TELECOM" : "UNKNOW";
    }

    public static String getSIMOperatorName(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimOperatorName();
    }

    public static int getSIMState(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimState();
    }

    public static String getSha1DeviceID(Context context) {
        return SHA1(getDeviceID(context));
    }

    private static String upperCase(String str) {
        return str.replace(":", "").toUpperCase();
    }
}
