package com.wali.gamecenter.report.io;

import com.badlogic.gdx.net.HttpRequestHeader;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpVersion;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.objectweb.asm.Opcodes;

public final class HttpConnectionManager {
    public static final int GPRS_WAIT_TIMEOUT = 30000;
    private static final int KEEP_ALIVE_DURATION_SECS = 15;
    private static final int KEEP_ALIVE_INTERVAL_SECS = 30;
    public static final int MAX_ROUTE_CONNECTIONS = 256;
    public static final int MAX_TOTAL_CONNECTIONS = 1024;
    public static int WAIT_TIMEOUT = GPRS_WAIT_TIMEOUT;
    public static final int WIFI_WAIT_TIMEOUT = 15000;
    public static Object _lock_ = new Object();
    private static DefaultHttpClient client;
    private static HttpParams httpParams;

    class ClientConnectionManager extends ThreadSafeClientConnManager {
        public ClientConnectionManager(HttpParams httpParams, SchemeRegistry schemeRegistry) {
            super(httpParams, schemeRegistry);
        }

        public ClientConnectionRequest requestConnection(HttpRoute httpRoute, Object obj) {
            IdleConnectionMonitorThread.ensureRunning(this, 15, 30);
            getConnectionsInPool();
            return HttpConnectionManager.super.requestConnection(httpRoute, obj);
        }
    }

    public class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(HttpEntity httpEntity) {
            super(httpEntity);
        }

        public InputStream getContent() {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }

    class IdleConnectionMonitorThread extends Thread {
        private static IdleConnectionMonitorThread thread = null;
        private final int checkIntervalSeconds;
        private final int idleTimeoutSeconds;
        private final ClientConnectionManager manager;

        public IdleConnectionMonitorThread(ClientConnectionManager clientConnectionManager, int i, int i2) {
            this.manager = clientConnectionManager;
            this.idleTimeoutSeconds = i;
            this.checkIntervalSeconds = i2;
        }

        public static synchronized void ensureRunning(ClientConnectionManager clientConnectionManager, int i, int i2) {
            synchronized (IdleConnectionMonitorThread.class) {
                if (thread == null) {
                    thread = new IdleConnectionMonitorThread(clientConnectionManager, i, i2);
                    thread.start();
                }
            }
        }

        public void run() {
            while (true) {
                try {
                    synchronized (this) {
                        wait((long) (this.checkIntervalSeconds * 1000));
                    }
                    this.manager.closeExpiredConnections();
                    this.manager.closeIdleConnections((long) this.idleTimeoutSeconds, TimeUnit.SECONDS);
                    synchronized (IdleConnectionMonitorThread.class) {
                        if (this.manager.getConnectionsInPool() == 0) {
                            thread = null;
                            return;
                        }
                    }
                } catch (InterruptedException e) {
                    thread = null;
                    return;
                }
            }
            while (true) {
            }
        }
    }

    class WaliSSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public WaliSSLSocketFactory(KeyStore keyStore) {
            super(keyStore);
            AnonymousClass1 r0 = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
                }

                public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
                    x509CertificateArr[0].checkValidity();
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            this.sslContext.init(null, new TrustManager[]{r0}, null);
        }

        public Socket createSocket() {
            return this.sslContext.getSocketFactory().createSocket();
        }

        public Socket createSocket(Socket socket, String str, int i, boolean z) {
            return this.sslContext.getSocketFactory().createSocket(socket, str, i, z);
        }
    }

    static ClientConnectionManager Init() {
        WAIT_TIMEOUT = GPRS_WAIT_TIMEOUT;
        httpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(httpParams, "UTF-8");
        ConnManagerParams.setMaxTotalConnections(httpParams, 1024);
        ConnManagerParams.setTimeout(httpParams, (long) WAIT_TIMEOUT);
        ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRouteBean(256));
        HttpConnectionParams.setConnectionTimeout(httpParams, WAIT_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, WAIT_TIMEOUT);
        HttpClientParams.setRedirecting(httpParams, true);
        HttpConnectionParams.setSocketBufferSize(httpParams, Opcodes.ACC_ANNOTATION);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        try {
            KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
            instance.load(null, null);
            WaliSSLSocketFactory waliSSLSocketFactory = new WaliSSLSocketFactory(instance);
            waliSSLSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            schemeRegistry.register(new Scheme("https", waliSSLSocketFactory, 443));
        } catch (Exception e) {
        }
        return new ClientConnectionManager(httpParams, schemeRegistry);
    }

    public static DefaultHttpClient getHttpClient() {
        DefaultHttpClient defaultHttpClient;
        synchronized (_lock_) {
            if (client == null) {
                client = new DefaultHttpClient(Init(), httpParams);
                client.setHttpRequestRetryHandler(new HttpRequestRetryHandler() {
                    public boolean retryRequest(IOException iOException, int i, HttpContext httpContext) {
                        if (i > 2) {
                            return false;
                        }
                        if (iOException instanceof NoHttpResponseException) {
                            return true;
                        }
                        return !(((HttpRequest) httpContext.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest);
                    }
                });
                client.addRequestInterceptor(new HttpRequestInterceptor() {
                    public void process(HttpRequest httpRequest, HttpContext httpContext) {
                        if (!httpRequest.containsHeader(HttpRequestHeader.AcceptEncoding)) {
                            httpRequest.addHeader(HttpRequestHeader.AcceptEncoding, "gzip");
                        }
                    }
                });
                client.addResponseInterceptor(new HttpResponseInterceptor() {
                    public void process(HttpResponse httpResponse, HttpContext httpContext) {
                        Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                        if (contentEncoding != null) {
                            HeaderElement[] elements = contentEncoding.getElements();
                            for (HeaderElement name : elements) {
                                if (name.getName().equalsIgnoreCase("gzip")) {
                                    httpResponse.setEntity(new GzipDecompressingEntity(httpResponse.getEntity()));
                                    return;
                                }
                            }
                        }
                    }
                });
            }
            defaultHttpClient = client;
        }
        return defaultHttpClient;
    }

    public static void reset() {
        synchronized (_lock_) {
            client = null;
        }
    }
}
