package com.wali.gamecenter.report;

import android.text.TextUtils;
import cn.egame.terminal.paysdk.EgamePay;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportBaseParams {
    static ReportBaseParams mInstance;
    String carrier;
    String cuid;
    String fuid;
    String mnc;
    String os;
    String ua;
    public String uid;
    String version;
    String version_name;

    private ReportBaseParams() {
    }

    public static ReportBaseParams getInstance() {
        if (mInstance == null) {
            mInstance = new ReportBaseParams();
        }
        return mInstance;
    }

    public String getBaseParamsString(boolean z) {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(this.uid)) {
            sb.append("uid=").append(this.uid).append("&");
        }
        if (!TextUtils.isEmpty(this.cuid)) {
            sb.append("userId=").append(this.cuid).append("&");
        }
        if (!z && !TextUtils.isEmpty(this.fuid)) {
            sb.append("fuid=").append(this.fuid).append("&");
        }
        if (!TextUtils.isEmpty(this.carrier)) {
            sb.append("carrier=").append(this.carrier).append("&");
        }
        if (!TextUtils.isEmpty(this.mnc)) {
            sb.append("mnc=").append(this.mnc).append("&");
        }
        if (!TextUtils.isEmpty(this.os)) {
            sb.append("os=").append(this.os).append("&");
        }
        if (!TextUtils.isEmpty(this.ua)) {
            sb.append("ua=").append(this.ua).append("&");
        }
        if (!TextUtils.isEmpty(this.version)) {
            sb.append("version=").append(this.version).append("&");
        }
        return sb.toString();
    }

    public void setBaseParamsToJSON(JSONObject jSONObject) {
        if (jSONObject != null) {
            try {
                if (!TextUtils.isEmpty(this.uid)) {
                    jSONObject.put("uid", URLEncoder.encode(this.uid, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.cuid)) {
                    jSONObject.put(EgamePay.PAY_PARAMS_KEY_USERID, URLEncoder.encode(this.cuid, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.fuid) && !jSONObject.has("fuid")) {
                    jSONObject.put("fuid", URLEncoder.encode(this.fuid, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.carrier)) {
                    jSONObject.put("carrier", URLEncoder.encode(this.carrier, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.mnc)) {
                    jSONObject.put("mnc", URLEncoder.encode(this.mnc, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.os)) {
                    jSONObject.put("os", URLEncoder.encode(this.os, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.ua)) {
                    jSONObject.put("ua", URLEncoder.encode(this.ua, "UTF-8"));
                }
                if (!TextUtils.isEmpty(this.version)) {
                    jSONObject.put("version", URLEncoder.encode(this.version, "UTF-8"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void setReportBaseParams(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        this.uid = str;
        this.cuid = str2;
        this.fuid = str3;
        this.carrier = str4;
        this.mnc = str5;
        this.os = str6;
        this.ua = str7;
        this.version = str8;
        this.version_name = str9;
    }
}
