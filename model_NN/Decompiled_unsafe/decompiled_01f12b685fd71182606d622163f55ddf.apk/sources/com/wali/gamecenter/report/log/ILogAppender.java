package com.wali.gamecenter.report.log;

public interface ILogAppender {
    void close();

    void debug(String str, String str2);

    void error(String str, String str2, Throwable th);
}
