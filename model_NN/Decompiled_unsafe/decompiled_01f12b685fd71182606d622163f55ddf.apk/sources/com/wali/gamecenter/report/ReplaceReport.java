package com.wali.gamecenter.report;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.wali.gamecenter.report.utils.ReportUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import org.json.JSONObject;

public class ReplaceReport extends Report {
    public static final Parcelable.Creator<ReplaceReport> CREATOR = new Parcelable.Creator<ReplaceReport>() {
        public ReplaceReport createFromParcel(Parcel parcel) {
            return new ReplaceReport(parcel);
        }

        public ReplaceReport[] newArray(int i) {
            return new ReplaceReport[i];
        }
    };
    String action;
    String client;
    String curPageid;
    String fromApp;
    String fromPkgName;
    String installtype;
    String iuid;
    String loginstatus;
    String mipackagename;
    String otherplatform;
    String packagename;
    String path;
    String pvcode;
    String type;

    public class Builder {
        ReplaceReportParams P = new ReplaceReportParams();

        public ReplaceReport create() {
            ReplaceReport replaceReport = new ReplaceReport();
            replaceReport.apply(this.P);
            return replaceReport;
        }

        public Builder setAction(String str) {
            this.P.action = str;
            return this;
        }

        public Builder setClient(String str) {
            this.P.client = str;
            return this;
        }

        public Builder setCurPageId(String str) {
            this.P.curPageid = str;
            return this;
        }

        public Builder setFromApp(String str) {
            this.P.fromApp = str;
            return this;
        }

        public Builder setFromPkgName(String str) {
            this.P.fromPkgName = str;
            return this;
        }

        public Builder setImei(String str) {
            this.P.iuid = str;
            return this;
        }

        public Builder setInstallType(String str) {
            this.P.installtype = str;
            return this;
        }

        public Builder setLoginStatus(String str) {
            this.P.loginstatus = str;
            return this;
        }

        public Builder setMiPkgName(String str) {
            this.P.mipackagename = str;
            return this;
        }

        public Builder setOtherPlatform(String str) {
            this.P.otherplatform = str;
            return this;
        }

        public Builder setPath(String str) {
            this.P.path = str;
            return this;
        }

        public Builder setPkgName(String str) {
            this.P.packagename = str;
            return this;
        }

        public Builder setPvcode(String str) {
            this.P.pvcode = str;
            return this;
        }

        public Builder setType(String str) {
            this.P.type = str;
            return this;
        }
    }

    ReplaceReport() {
    }

    ReplaceReport(Parcel parcel) {
        this.action = parcel.readString();
        this.client = parcel.readString();
        this.type = parcel.readString();
        this.curPageid = parcel.readString();
        this.fromPkgName = parcel.readString();
        this.fromApp = parcel.readString();
        this.pvcode = parcel.readString();
        this.path = parcel.readString();
        this.otherplatform = parcel.readString();
        this.packagename = parcel.readString();
        this.mipackagename = parcel.readString();
        this.loginstatus = parcel.readString();
        this.installtype = parcel.readString();
        this.iuid = parcel.readString();
    }

    public void apply(ReplaceReportParams replaceReportParams) {
        this.action = replaceReportParams.action;
        this.type = replaceReportParams.type;
        this.client = replaceReportParams.client;
        this.curPageid = replaceReportParams.curPageid;
        this.fromPkgName = replaceReportParams.fromPkgName;
        this.fromApp = replaceReportParams.fromApp;
        this.pvcode = replaceReportParams.pvcode;
        this.path = replaceReportParams.path;
        this.otherplatform = replaceReportParams.otherplatform;
        this.packagename = replaceReportParams.packagename;
        this.mipackagename = replaceReportParams.mipackagename;
        this.loginstatus = replaceReportParams.loginstatus;
        this.installtype = replaceReportParams.installtype;
        this.mHasCallCreate = true;
    }

    /* access modifiers changed from: package-private */
    public String getParamsToJSON(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("ac", this.action);
            if (!TextUtils.isEmpty(this.type)) {
                jSONObject.put("type", URLEncoder.encode(this.type, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.client)) {
                jSONObject.put("client", this.client);
            }
            if (!TextUtils.isEmpty(this.curPageid)) {
                jSONObject.put("curPageid", URLEncoder.encode(this.curPageid, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.fromApp)) {
                jSONObject.put("cid", URLEncoder.encode(this.fromApp, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.pvcode)) {
                jSONObject.put("pvcode", URLEncoder.encode(this.pvcode, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.path)) {
                jSONObject.put("path", URLEncoder.encode(this.path, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.otherplatform)) {
                jSONObject.put("otherplatform", URLEncoder.encode(this.otherplatform, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.packagename)) {
                jSONObject.put("packagename", URLEncoder.encode(this.packagename, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.mipackagename)) {
                jSONObject.put("mipackagename", URLEncoder.encode(this.mipackagename, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.loginstatus)) {
                jSONObject.put("loginstatus", URLEncoder.encode(this.loginstatus, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.installtype)) {
                jSONObject.put("installtype", URLEncoder.encode(this.installtype, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.iuid)) {
                jSONObject.put("iuid", URLEncoder.encode(this.iuid, "UTF-8"));
            }
            ReportBaseParams.getInstance().setBaseParamsToJSON(jSONObject);
            if (!TextUtils.isEmpty(this.fromPkgName)) {
                jSONObject.put("fuid", URLEncoder.encode(this.fromPkgName, "UTF-8"));
            }
            String[] securityParameters = ReportUtils.getSecurityParameters(ReportBaseParams.getInstance().uid, this.curPageid, this.type);
            if (securityParameters != null && securityParameters.length > 0) {
                jSONObject.put("pp1", securityParameters[0]);
                jSONObject.put("pp2", securityParameters[1]);
            }
            jSONObject.put("tm", Calendar.getInstance().getTimeInMillis());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: package-private */
    public String getParamsToString(Context context) {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("ac=").append(this.action).append("&");
            if (!TextUtils.isEmpty(this.type)) {
                sb.append("type=").append(URLEncoder.encode(this.type, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.client)) {
                sb.append("client=").append(this.client).append("&");
            }
            if (!TextUtils.isEmpty(this.curPageid)) {
                sb.append("curPageid=").append(URLEncoder.encode(this.curPageid, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.fromApp)) {
                sb.append("cid=").append(URLEncoder.encode(this.fromApp, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.fromPkgName)) {
                sb.append("fuid=").append(URLEncoder.encode(this.fromPkgName, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.pvcode)) {
                sb.append("pvcode=").append(URLEncoder.encode(this.pvcode, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.path)) {
                sb.append("path=").append(URLEncoder.encode(this.path, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.otherplatform)) {
                sb.append("otherplatform=").append(URLEncoder.encode(this.otherplatform, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.packagename)) {
                sb.append("packagename=").append(URLEncoder.encode(this.packagename, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.mipackagename)) {
                sb.append("mipackagename=").append(URLEncoder.encode(this.mipackagename, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.loginstatus)) {
                sb.append("loginstatus=").append(URLEncoder.encode(this.loginstatus, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.installtype)) {
                sb.append("installtype=").append(URLEncoder.encode(this.installtype, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.iuid)) {
                sb.append("iuid=").append(this.iuid);
            }
            sb.append(ReportBaseParams.getInstance().getBaseParamsString(true));
            String[] securityParameters = ReportUtils.getSecurityParameters(ReportBaseParams.getInstance().uid, this.curPageid, this.type);
            if (securityParameters != null && securityParameters.length > 0) {
                sb.append("pp1=").append(securityParameters[0]).append("&");
                sb.append("pp2=").append(securityParameters[1]).append("&");
            }
            sb.append("tm=").append(Calendar.getInstance().getTimeInMillis());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.action);
        parcel.writeString(this.client);
        parcel.writeString(this.type);
        parcel.writeString(this.curPageid);
        parcel.writeString(this.fromPkgName);
        parcel.writeString(this.fromApp);
        parcel.writeString(this.pvcode);
        parcel.writeString(this.path);
        parcel.writeString(this.otherplatform);
        parcel.writeString(this.packagename);
        parcel.writeString(this.mipackagename);
        parcel.writeString(this.loginstatus);
        parcel.writeString(this.installtype);
        parcel.writeString(this.iuid);
    }
}
