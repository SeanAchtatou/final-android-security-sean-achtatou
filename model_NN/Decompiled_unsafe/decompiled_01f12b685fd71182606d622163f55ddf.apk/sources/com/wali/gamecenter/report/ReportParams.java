package com.wali.gamecenter.report;

public final class ReportParams {
    String action;
    String adsCv;
    String adsId;
    String adsType;
    String cid;
    String client;
    String curPage;
    String curPageId;
    String curPagePkgName;
    String from;
    String fromId;
    String fromLabel;
    String moduleId;
    String originManual;
    String position;
    String trace;
    String tt;
    ReportType type;
}
