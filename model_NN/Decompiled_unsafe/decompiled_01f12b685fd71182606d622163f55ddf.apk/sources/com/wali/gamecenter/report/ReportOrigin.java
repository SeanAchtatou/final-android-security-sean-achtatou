package com.wali.gamecenter.report;

import android.content.Context;
import android.content.SharedPreferences;

public class ReportOrigin {
    public static final String ORIGIN_CATEGORY = "category";
    public static final String ORIGIN_DISCOVERY = "discovery";
    public static final String ORIGIN_NEWGAME = "new_game";
    public static final String ORIGIN_NEWS = "news";
    public static final String ORIGIN_NEW_RECOMMEND = "new_home_recommend";
    public static final String ORIGIN_OTHER = "other";
    public static final String ORIGIN_PERSONAL = "personal";
    public static final String ORIGIN_RANK = "rank";
    public static final String ORIGIN_SEARCH = "search";

    public static void clear(Context context) {
        context.getSharedPreferences("origin", 4).edit().clear().commit();
    }

    public static String getOrigin(Context context) {
        return context.getSharedPreferences("origin", 4).getString("origin", ORIGIN_OTHER);
    }

    public static void init(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("origin", 4).edit();
        edit.putString("origin", str);
        edit.commit();
    }
}
