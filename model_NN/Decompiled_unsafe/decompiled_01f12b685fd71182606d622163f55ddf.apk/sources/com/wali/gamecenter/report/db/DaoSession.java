package com.wali.gamecenter.report.db;

import android.database.sqlite.SQLiteDatabase;
import com.xiaomi.greendao.AbstractDao;
import com.xiaomi.greendao.AbstractDaoSession;
import com.xiaomi.greendao.identityscope.IdentityScopeType;
import com.xiaomi.greendao.internal.DaoConfig;
import java.util.Map;

public class DaoSession extends AbstractDaoSession {
    private final ReportDataDao reportDataDao = new ReportDataDao(this.reportDataDaoConfig, this);
    private final DaoConfig reportDataDaoConfig;

    public DaoSession(SQLiteDatabase sQLiteDatabase, IdentityScopeType identityScopeType, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig> map) {
        super(sQLiteDatabase);
        this.reportDataDaoConfig = map.get(ReportDataDao.class).clone();
        this.reportDataDaoConfig.initIdentityScope(identityScopeType);
        registerDao(ReportData.class, this.reportDataDao);
    }

    public void clear() {
        this.reportDataDaoConfig.getIdentityScope().clear();
    }

    public ReportDataDao getReportDataDao() {
        return this.reportDataDao;
    }
}
