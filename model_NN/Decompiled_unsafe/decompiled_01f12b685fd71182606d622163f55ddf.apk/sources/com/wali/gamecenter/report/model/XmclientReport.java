package com.wali.gamecenter.report.model;

import android.content.Context;
import com.wali.gamecenter.report.ReportBaseParams;
import com.wali.gamecenter.report.utils.ReportUtils;
import com.xiaomi.gson.annotations.SerializedName;
import java.util.Calendar;

public class XmclientReport extends BaseReport {
    @SerializedName("cv")
    private String adsCv;
    @SerializedName("adId")
    private String adsId;
    private String adsType;
    private String cid;
    private String curPage;
    private String curPageId;
    private String from;
    private String fromId;
    private String fromLabel;
    private String moduleId;
    private String origin;
    private String position;
    private String pp1;
    private String pp2;
    private String tm;
    private String tt;

    public XmclientReport(Context context) {
        super(context);
        setAc("game_center");
        String[] securityParameters = ReportUtils.getSecurityParameters(ReportBaseParams.getInstance().uid, this.curPageId, getType().toString());
        setPp1(securityParameters[0]);
        setPp2(securityParameters[1]);
        setTm(Calendar.getInstance().getTimeInMillis() + "");
    }

    public String getAdsCv() {
        return this.adsCv;
    }

    public String getAdsId() {
        return this.adsId;
    }

    public String getAdsType() {
        return this.adsType;
    }

    public String getCid() {
        return this.cid;
    }

    public String getCurPage() {
        return this.curPage;
    }

    public String getCurPageId() {
        return this.curPageId;
    }

    public String getFrom() {
        return this.from;
    }

    public String getFromId() {
        return this.fromId;
    }

    public String getFromLabel() {
        return this.fromLabel;
    }

    public String getModuleId() {
        return this.moduleId;
    }

    public String getOrigin() {
        return this.origin;
    }

    public String getPosition() {
        return this.position;
    }

    public String getPp1() {
        return this.pp1;
    }

    public String getPp2() {
        return this.pp2;
    }

    public String getTm() {
        return this.tm;
    }

    public String getTt() {
        return this.tt;
    }

    public void setAdsCv(String str) {
        this.adsCv = str;
    }

    public void setAdsId(String str) {
        this.adsId = str;
    }

    public void setAdsType(String str) {
        this.adsType = str;
    }

    public void setCid(String str) {
        this.cid = str;
    }

    public void setCurPage(String str) {
        this.curPage = str;
    }

    public void setCurPageId(String str) {
        this.curPageId = str;
    }

    public void setFrom(String str) {
        this.from = str;
    }

    public void setFromId(String str) {
        this.fromId = str;
    }

    public void setFromLabel(String str) {
        this.fromLabel = str;
    }

    public void setModuleId(String str) {
        this.moduleId = str;
    }

    public void setOrigin(String str) {
        this.origin = str;
    }

    public void setPosition(String str) {
        this.position = str;
    }

    public void setPp1(String str) {
        this.pp1 = str;
    }

    public void setPp2(String str) {
        this.pp2 = str;
    }

    public void setTm(String str) {
        this.tm = str;
    }

    public void setTt(String str) {
        this.tt = str;
    }
}
