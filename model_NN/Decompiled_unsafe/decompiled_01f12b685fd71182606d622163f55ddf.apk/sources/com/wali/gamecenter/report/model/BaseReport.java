package com.wali.gamecenter.report.model;

import android.content.Context;
import android.text.TextUtils;
import com.wali.gamecenter.report.ReportManager;
import com.wali.gamecenter.report.ReportType;
import com.wali.gamecenter.report.utils.TelUtils;
import com.xiaomi.gson.Gson;
import com.xiaomi.gson.annotations.SerializedName;
import com.xiaomi.gson.reflect.TypeToken;
import java.net.URLEncoder;
import java.util.Map;

public class BaseReport {
    private String ac;
    public String carrier;
    private String client;
    protected EXT ext;
    @SerializedName("3gmac")
    public String gmac;
    @SerializedName("IMEI")
    public String imei;
    public String index;
    public String jarver;
    private ReportType type = ReportType.STATISTICS;
    public String ua;
    public String ver;

    public BaseReport(Context context) {
        this.ver = TelUtils.getVersionCode(context) + "";
        this.imei = TelUtils.getDeviceId(context);
        this.index = TelUtils.getRandomIndex();
        this.carrier = TelUtils.getSIMOperatorName(context);
        this.gmac = TelUtils.get3gMacAddress(context);
        this.jarver = TelUtils.versionCode + "";
        this.ua = TelUtils.get_device_agent_(context);
    }

    public String getAc() {
        return this.ac;
    }

    public String getClient() {
        return this.client;
    }

    public EXT getExt() {
        return this.ext;
    }

    public ReportType getType() {
        return this.type;
    }

    public void send() {
        ReportManager.getInstance().sendReport(this);
    }

    public void setAc(String str) {
        this.ac = str;
    }

    public void setClient(String str) {
        this.client = str;
    }

    public void setExt(EXT ext2) {
        this.ext = ext2;
    }

    public void setType(ReportType reportType) {
        this.type = reportType;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public String toString() {
        String json = toJson();
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : ((Map) new Gson().fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType())).entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            if (!TextUtils.isEmpty((CharSequence) entry.getValue())) {
                sb.append((String) entry.getKey());
                sb.append("=");
                try {
                    sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                } catch (Exception e) {
                    sb.append("");
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
