package com.wali.gamecenter.report.model;

import android.content.Context;
import android.text.TextUtils;
import com.wali.gamecenter.report.utils.MD5;
import com.wali.gamecenter.report.utils.TelUtils;
import com.xiaomi.gson.Gson;
import com.xiaomi.gson.annotations.SerializedName;
import com.xiaomi.gson.reflect.TypeToken;
import java.net.URLEncoder;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class XmsdkReport extends BaseReport {
    private String CID;
    private String appid;
    private String cpChannel;
    private String md5imei;
    private String num;
    private String p1;
    private String p2;
    private String step;
    private String uid;
    @SerializedName("time")
    private String wasteTime;

    public XmsdkReport(Context context) {
        super(context);
        setAc("xmsdk");
        String md5 = MD5.getMD5((this.imei + "_" + this.appid).getBytes());
        int random = TelUtils.getRandom(20);
        int random2 = TelUtils.getRandom(md5.length() - random);
        String substring = md5.substring(random, random + random2);
        setP1(substring);
        setP2(random + "." + random2);
        this.ext = new EXT();
        this.ext.localtime = System.currentTimeMillis() + "";
        this.ext.packageName = context.getPackageName();
    }

    public String getAppid() {
        return this.appid;
    }

    public String getCID() {
        return this.CID;
    }

    public String getCpChannel() {
        return this.cpChannel;
    }

    public String getMd5imei() {
        return this.md5imei;
    }

    public String getNum() {
        return this.num;
    }

    public String getP1() {
        return this.p1;
    }

    public String getP2() {
        return this.p2;
    }

    public String getStep() {
        return this.step;
    }

    public String getUid() {
        return this.uid;
    }

    public String getWasteTime() {
        return this.wasteTime;
    }

    public void setAppid(String str) {
        this.appid = str;
    }

    public void setCID(String str) {
        this.CID = str;
    }

    public void setCpChannel(String str) {
        this.cpChannel = str;
    }

    public void setMd5imei(String str) {
        this.md5imei = str;
    }

    public void setNum(String str) {
        this.num = str;
    }

    public void setP1(String str) {
        this.p1 = str;
    }

    public void setP2(String str) {
        this.p2 = str;
    }

    public void setStep(String str) {
        this.step = str;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public void setWasteTime(String str) {
        this.wasteTime = str;
    }

    public String toString() {
        String json = toJson();
        try {
            JSONObject jSONObject = new JSONObject(json);
            jSONObject.put("ext", jSONObject.optJSONObject("ext").toString());
            json = jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : ((Map) new Gson().fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType())).entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            if (!TextUtils.isEmpty((CharSequence) entry.getValue())) {
                sb.append((String) entry.getKey());
                sb.append("=");
                try {
                    sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                } catch (Exception e2) {
                    sb.append("");
                    e2.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
