package com.wali.gamecenter.report.model;

import android.content.Context;

public class GmchannelReport extends BaseReport {
    private String j;

    public GmchannelReport(Context context) {
        super(context);
        setAc("gm_channel");
    }

    public String getJ() {
        return this.j;
    }

    public void setJ(String str) {
        this.j = str;
    }
}
