package com.sg.Screen;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorNum;
import com.kbz.Actors.LoadFont;
import com.kbz.Actors.LoadingGroup;
import com.kbz.Sound.GameSound;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.PolygonHit;
import com.kbz.tools.Tools;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.UI.MyCover;
import com.sg.util.GameScreen;
import com.sg.util.GameStage;

public class GameStartLoadScreen extends GameScreen implements GameConstant {
    ActorImage bgImage;
    int index;
    boolean isOver;
    ActorClipImage loadClipImage;
    private LoadingGroup loadingGroup;
    Group mygGroup;
    ActorNum num;

    public void init() {
        this.mygGroup = new Group();
        this.loadClipImage = new ActorClipImage(PAK_ASSETS.IMG_LOAD2, 86, PAK_ASSETS.IMG_2X1, 12, this.mygGroup);
        this.loadClipImage.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.mygGroup, 3);
        this.isOver = false;
        PolygonHit.setShowRect(false);
        initLoadingGroup();
    }

    public void testNum() {
        this.num = new ActorNum(0, this.index, 300, 300, this.mygGroup, (byte) 1);
    }

    public void run(float delta) {
    }

    private void initLoadingGroup() {
        this.loadingGroup = new LoadingGroup();
        this.loadingGroup.addLoadingListener(new LoadingGroup.LoadingListener() {
            public void begin() {
                for (int i = 0; i < PAK_ASSETS.packNameStr.length; i++) {
                    Tools.loadTextureRegion(i);
                }
                GameSound.loadAllMusic(PAK_ASSETS.MUSIC_NAME);
                GameSound.loadAllSound(PAK_ASSETS.SOUND_NAME);
                LoadFont.initFontCu();
                RankData.initGameData();
            }

            public void end() {
                GameStartLoadScreen.this.loadClipImage.setFullClip();
                GameMain.toScreen(new MyCover());
            }

            public void update(float progress) {
                GameStartLoadScreen.this.loadClipImage.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (GameStartLoadScreen.this.loadClipImage.getWidth() * progress) / 100.0f, GameStartLoadScreen.this.loadClipImage.getHeight());
            }
        });
        GameStage.addActor(this.loadingGroup, 2);
    }

    public void dispose() {
        GameStage.removeActor(this.loadingGroup);
        this.mygGroup.clear();
    }
}
