package com.sg.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Particle.GParticleSystem;
import com.kbz.Particle.GameParticle;
import com.sg.GameSprites.GameSprite;
import com.sg.pak.GameConstant;
import com.sg.util.GameScreen;

public class GameMainMenuScreen extends GameScreen implements GameConstant {
    GParticleSystem bbb;
    ActorImage bgActorImage;
    ActorImage bgImage;
    Group mainGroup;
    GameSprite sprite;
    ActorText strText;
    GameParticle test;

    public void init() {
    }

    public void run(float delta) {
        if (Gdx.input.justTouched()) {
        }
    }

    public boolean gKeyDown(int keycode) {
        return true;
    }

    public boolean gKeyUp(int keycode) {
        return true;
    }

    public void dispose() {
    }

    /* access modifiers changed from: package-private */
    public void initEnemeys() {
    }

    /* access modifiers changed from: package-private */
    public void createEnemys() {
    }
}
