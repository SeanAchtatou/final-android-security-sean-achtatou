package com.sg.GameRecord;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

public class GameRecord {
    private static final String RECORD_NAME = "sg_game_td_";
    protected DataInputStream dis;
    protected DataOutputStream dos;

    /* access modifiers changed from: protected */
    public void readRecord(int recordID) {
        String name = RECORD_NAME + recordID;
        FileHandle file = Gdx.files.local(name);
        if (!file.exists()) {
            new FileHandle(new File(Gdx.files.getLocalStoragePath(), name));
            System.err.println("存没有存档！");
            return;
        }
        this.dis = new DataInputStream(file.read());
        try {
            read();
            this.dis.close();
        } catch (IOException e) {
            System.out.println("read record: " + recordID + " error !");
            e.printStackTrace();
        }
    }

    public void writeRecord(int recordID) {
        this.dos = new DataOutputStream(Gdx.files.local(RECORD_NAME + recordID).write(false));
        try {
            save();
            this.dos.close();
        } catch (IOException e) {
            System.out.println("write record: " + recordID + " error !");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void save() throws IOException {
    }

    /* access modifiers changed from: protected */
    public void writeInt(int v) throws IOException {
        this.dos.writeInt(v);
    }

    /* access modifiers changed from: protected */
    public void writeLong(long v) throws IOException {
        this.dos.writeLong(v);
    }

    /* access modifiers changed from: protected */
    public int readInt() throws IOException {
        return this.dis.readInt();
    }

    /* access modifiers changed from: protected */
    public long readLong() throws IOException {
        return this.dis.readLong();
    }

    /* access modifiers changed from: protected */
    public void writeIntArray(int[] array) throws IOException {
        for (int writeInt : array) {
            this.dos.writeInt(writeInt);
        }
    }

    /* access modifiers changed from: protected */
    public int[] readIntArray(int[] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            array[i] = this.dis.readInt();
        }
        return array;
    }

    /* access modifiers changed from: protected */
    public void writeIntArray2(int[][] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            for (int writeInt : array[i]) {
                this.dos.writeInt(writeInt);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int[][] readIntArray2(int[][] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = this.dis.readInt();
            }
        }
        return array;
    }

    /* access modifiers changed from: protected */
    public void writeBoolean(Boolean v) throws IOException {
        this.dos.writeBoolean(v.booleanValue());
    }

    /* access modifiers changed from: protected */
    public void writeBooleanArray(boolean[] array) throws IOException {
        for (boolean writeBoolean : array) {
            this.dos.writeBoolean(writeBoolean);
        }
    }

    /* access modifiers changed from: protected */
    public boolean[] readBooleanArray(boolean[] array) throws IOException {
        for (int i = 0; i < array.length; i++) {
            array[i] = this.dis.readBoolean();
        }
        return array;
    }

    /* access modifiers changed from: protected */
    public void writeString(String s) throws IOException {
        this.dos.writeBytes(s);
    }

    /* access modifiers changed from: protected */
    public boolean readBoolean() throws IOException {
        return this.dis.readBoolean();
    }

    /* access modifiers changed from: protected */
    public void read() throws IOException {
    }
}
