package com.sg.GameEntry;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.td.UI.MyCover;
import com.sg.td.UI.MyCoverSDK;
import com.sg.td.message.MySwitch;
import com.sg.util.GameStage;

public class GameMain extends Game implements GameConstant {
    public static SDKInterface dialog;
    public static Screen lastScreen;
    public static GameMain me;
    private static Screen nextScreen;
    public static float sceenHeight;
    public static float sceenWidth;
    public static SDKInterface sdkInterface;
    public static int sleepTime = 10;
    public static float zoomX;
    public static float zoomY;
    Long beginTime;
    Screen startloadScreen;
    Long timeDiff;

    public static void toScreen(Screen screen) {
        if (screen == null) {
            System.out.println("screen==null");
        } else {
            nextScreen = screen;
        }
    }

    public static void toScreen(Screen screen, Screen last) {
        if (screen == null) {
            System.out.println("screen==null");
        } else {
            lastScreen = last;
        }
    }

    public void create() {
        me = this;
        Tools.initTools(false);
        sceenWidth = (float) Gdx.graphics.getWidth();
        sceenHeight = (float) Gdx.graphics.getHeight();
        zoomX = sceenWidth / 640.0f;
        zoomY = sceenHeight / 1136.0f;
        System.out.println("zoomX==" + zoomX);
        System.out.println("zoomy==" + zoomY);
        GameStage.init(640, GameConstant.SCREEN_HEIGHT);
        System.out.println("**********isSDkCover::" + MySwitch.isSDkCover);
        if (MySwitch.isZhonggao) {
            setScreen(new MyCoverSDK());
        } else {
            setScreen(new MyCover());
        }
    }

    public void render() {
        super.render();
        if (nextScreen != null) {
            setScreen(nextScreen);
            nextScreen = null;
        }
    }
}
