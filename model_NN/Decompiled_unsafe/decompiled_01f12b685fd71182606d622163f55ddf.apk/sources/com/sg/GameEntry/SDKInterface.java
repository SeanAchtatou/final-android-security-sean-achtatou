package com.sg.GameEntry;

public interface SDKInterface {
    void changeSwitch();

    void exit();

    int getSimsId();

    void initSDK();

    String[] initSGManager();

    void loaginGUI();

    void sendMessage(int i);

    void setName();

    void showAD(int i);
}
