package com.sg.BeizierCurve;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.utils.Array;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.esotericsoftware.spine.Animation;

public class MyTools {
    private static final float MyPoint_ACCURACY = 0.01f;
    public static final int TYPE_BC = 3;
    public static final int TYPE_BL = 7;
    public static final int TYPE_BR = 5;
    public static final int TYPE_CENTER = 0;
    public static final int TYPE_LC = 2;
    public static final int TYPE_RC = 4;
    public static final int TYPE_TC = 8;
    public static final int TYPE_TL = 1;
    public static final int TYPE_TR = 6;
    private static ActorShapeSprite actorShapeSprite1 = new ActorShapeSprite();
    private static ActorShapeSprite actorTriangle = new ActorShapeSprite();
    private static boolean isAdd;
    public static final boolean isTest = false;
    private static Polygon polygon0 = new Polygon();
    private static Polygon polygon1 = new Polygon();
    private static float[] polygonFloat0 = new float[8];
    private static float[] polygonFloat1 = new float[8];
    private static float[] tianglFloat = new float[6];

    public static boolean isHitPolygons(Polygon polygon02, float x, float y, float w, float h) {
        polygonFloat1[0] = 0.0f;
        polygonFloat1[1] = 0.0f;
        polygonFloat1[2] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat1[3] = 0.0f;
        polygonFloat1[4] = 0.0f;
        polygonFloat1[5] = Animation.CurveTimeline.LINEAR + h;
        polygonFloat1[6] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat1[7] = Animation.CurveTimeline.LINEAR + h;
        polygon1.setVertices(polygonFloat1);
        polygon1.setPosition(x, y);
        if (Intersector.overlapConvexPolygons(polygon02, polygon1)) {
            actorShapeSprite1.setColor(Color.RED);
            return true;
        }
        actorShapeSprite1.setColor(Color.WHITE);
        return false;
    }

    public static boolean isHitPolygons(float x, float y, float w, float h, int OriginType, int angle, float x2, float y2, float w2, float h2) {
        polygonFloat0[0] = 0.0f;
        polygonFloat0[1] = 0.0f;
        polygonFloat0[2] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat0[3] = 0.0f;
        polygonFloat0[4] = 0.0f;
        polygonFloat0[5] = Animation.CurveTimeline.LINEAR + h;
        polygonFloat0[6] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat0[7] = Animation.CurveTimeline.LINEAR + h;
        polygon0.setVertices(polygonFloat0);
        polygon0.setPosition(x, y);
        switch (OriginType) {
            case 0:
                polygon0.setOrigin(w / 2.0f, h / 2.0f);
                break;
            case 1:
                polygon0.setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                break;
            case 2:
                polygon0.setOrigin(Animation.CurveTimeline.LINEAR, h / 2.0f);
                break;
            case 3:
                polygon0.setOrigin(w / 2.0f, h);
                break;
            case 4:
                polygon0.setOrigin(w, h / 2.0f);
                break;
            case 5:
                polygon0.setOrigin(w, h);
                break;
            case 6:
                polygon0.setOrigin(w, Animation.CurveTimeline.LINEAR);
                break;
            case 7:
                polygon0.setOrigin(Animation.CurveTimeline.LINEAR, h);
                break;
            case 8:
                polygon0.setOrigin(w / 2.0f, Animation.CurveTimeline.LINEAR);
                break;
        }
        polygon0.setRotation((float) angle);
        polygonFloat1[0] = 0.0f;
        polygonFloat1[1] = 0.0f;
        polygonFloat1[2] = Animation.CurveTimeline.LINEAR + w2;
        polygonFloat1[3] = 0.0f;
        polygonFloat1[4] = 0.0f;
        polygonFloat1[5] = Animation.CurveTimeline.LINEAR + h2;
        polygonFloat1[6] = Animation.CurveTimeline.LINEAR + w2;
        polygonFloat1[7] = Animation.CurveTimeline.LINEAR + h2;
        polygon1.setVertices(polygonFloat1);
        polygon1.setPosition(x2, y2);
        if (Intersector.overlapConvexPolygons(polygon0, polygon1)) {
            actorShapeSprite1.setColor(Color.RED);
            return true;
        }
        actorShapeSprite1.setColor(Color.WHITE);
        return false;
    }

    public static boolean isHitTriangle(float x, float y, float r, boolean isLeft, float x1, float y1, float w1, float h1) {
        if (!isLeft) {
            tianglFloat[0] = 0.0f;
            tianglFloat[1] = 0.0f;
            tianglFloat[2] = Animation.CurveTimeline.LINEAR + r;
            tianglFloat[3] = Animation.CurveTimeline.LINEAR - (r * 0.75f);
            tianglFloat[4] = Animation.CurveTimeline.LINEAR + r;
            tianglFloat[5] = (r * 0.75f) + Animation.CurveTimeline.LINEAR;
        } else {
            tianglFloat[0] = 0.0f;
            tianglFloat[1] = 0.0f;
            tianglFloat[2] = Animation.CurveTimeline.LINEAR - r;
            tianglFloat[3] = Animation.CurveTimeline.LINEAR - (r * 0.75f);
            tianglFloat[4] = Animation.CurveTimeline.LINEAR - r;
            tianglFloat[5] = (r * 0.75f) + Animation.CurveTimeline.LINEAR;
        }
        polygon0.setVertices(tianglFloat);
        polygon0.setPosition(x, y);
        return isHitPolygons(polygon0, x1, y1, w1, h1);
    }

    public static Array<MyPoint> calculatePoints(MyPoint start, MyPoint con, MyPoint end) {
        Array<MyPoint> MyPoints = new Array<>();
        for (float i = Animation.CurveTimeline.LINEAR; i <= 1.0f; i += MyPoint_ACCURACY) {
            MyPoints.add(new MyPoint((int) (((1.0f - i) * (1.0f - i) * ((float) start.x)) + (2.0f * i * (1.0f - i) * ((float) con.x)) + (i * i * ((float) end.x))), (int) (((1.0f - i) * (1.0f - i) * ((float) start.y)) + (2.0f * i * (1.0f - i) * ((float) con.y)) + (i * i * ((float) end.y)))));
        }
        return MyPoints;
    }

    public static Array<MyPoint> calculatePoints(MyPoint start, MyPoint con1, MyPoint con2, MyPoint end) {
        Array<MyPoint> MyPoints = new Array<>();
        for (float i = Animation.CurveTimeline.LINEAR; i <= 1.0f; i += MyPoint_ACCURACY) {
            MyPoints.add(new MyPoint((int) (((1.0f - i) * (1.0f - i) * (1.0f - i) * ((float) start.x)) + (3.0f * i * (1.0f - i) * (1.0f - i) * ((float) con1.x)) + (3.0f * i * i * (1.0f - i) * ((float) con2.x)) + (i * i * i * ((float) end.x))), (int) (((1.0f - i) * (1.0f - i) * (1.0f - i) * ((float) start.y)) + (3.0f * i * (1.0f - i) * (1.0f - i) * ((float) con1.y)) + (3.0f * i * i * (1.0f - i) * ((float) con2.y)) + (i * i * i * ((float) end.y)))));
        }
        return MyPoints;
    }
}
