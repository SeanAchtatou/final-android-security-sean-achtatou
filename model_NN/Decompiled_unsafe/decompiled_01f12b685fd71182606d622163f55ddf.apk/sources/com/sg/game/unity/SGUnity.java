package com.sg.game.unity;

import android.app.Activity;
import com.sg.game.pay.SGPay;
import com.sg.game.pay.Unity;
import com.sg.game.pay.UnityExitCallback;
import com.sg.game.pay.UnityInitCallback;
import com.sg.game.pay.UnityPayCallback;
import com.xiaomi.gamecenter.wxwap.HyWxWapPay;
import com.xiaomi.gamecenter.wxwap.PayResultCallback;
import com.xiaomi.gamecenter.wxwap.config.a;
import com.xiaomi.gamecenter.wxwap.purchase.UnrepeatPurchase;

public class SGUnity extends Unity {
    private String[] point = getBuildConfig(BuildConfig.APPLICATION_ID, "point").split(",");
    private SGPay sgPay;

    public SGUnity(Activity activity, UnityInitCallback callback) {
        super(activity, callback);
        this.sgPay = new SGPay(this, callback);
    }

    public String getName() {
        return a.d;
    }

    public String getConfig(String name) {
        return this.sgPay.getConfig(name);
    }

    public void init() {
        this.sgPay.init();
    }

    public void pay(int payMode, final int index, final UnityPayCallback callback) {
        if (payMode == 99) {
            String modeStr = this.sgPay.getConfig("value");
            if (modeStr != null) {
                payMode = Integer.parseInt(modeStr);
            } else {
                payMode = 1;
            }
        }
        if (getSimID() == -1) {
            payMode = 2;
        }
        switch (payMode) {
            case 2:
                switch (getPayInfos(index).reBuy) {
                    case 0:
                        UnrepeatPurchase purchase = new UnrepeatPurchase();
                        purchase.setCpOrderId(System.currentTimeMillis() + "");
                        purchase.setChargeCode(this.point[index]);
                        HyWxWapPay.getInstance().pay(this.activity, purchase, new PayResultCallback() {
                            public void onSuccess(String cpOrderId) {
                                callback.paySuccess(index);
                            }

                            public void onError(int code, String message) {
                                callback.payFail(index, "failed:" + code + "  " + message);
                            }
                        });
                        return;
                    default:
                        UnrepeatPurchase purchase1 = new UnrepeatPurchase();
                        purchase1.setCpOrderId(System.currentTimeMillis() + "");
                        purchase1.setChargeCode(this.point[index]);
                        HyWxWapPay.getInstance().pay(this.activity, purchase1, new PayResultCallback() {
                            public void onSuccess(String cpOrderId) {
                                callback.paySuccess(index);
                            }

                            public void onError(int code, String message) {
                                callback.payFail(index, "failed:" + code + "  " + message);
                            }
                        });
                        return;
                }
            default:
                defaultPay(this.sgPay, index, callback);
                return;
        }
    }

    public void moreGame() {
        this.sgPay.moreGame();
    }

    public void exitGame(UnityExitCallback callback) {
        defaultExit(this.sgPay, callback);
    }

    public boolean exitGameShow() {
        return true;
    }

    public int getSimID() {
        return this.sgPay.getSimId();
    }

    public void onPause() {
        this.sgPay.onPause();
    }

    public void onResume() {
        this.sgPay.onResume();
    }

    public void resetPay() {
        this.sgPay.resetPay();
    }

    public void prePay(int id, int[] exceptIds) {
        this.sgPay.prePay(id, exceptIds);
    }
}
