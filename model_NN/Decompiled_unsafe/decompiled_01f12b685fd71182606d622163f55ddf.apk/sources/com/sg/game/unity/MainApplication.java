package com.sg.game.unity;

import com.sg.game.pay.PayApplication;
import com.xiaomi.gamecenter.wxwap.HyWxWapPay;

public class MainApplication extends PayApplication {
    public void onCreate() {
        super.onCreate();
        HyWxWapPay.init(this, SGUnity.getBuildConfig(BuildConfig.APPLICATION_ID, "AppID"), SGUnity.getBuildConfig(BuildConfig.APPLICATION_ID, "AppKey"));
    }
}
