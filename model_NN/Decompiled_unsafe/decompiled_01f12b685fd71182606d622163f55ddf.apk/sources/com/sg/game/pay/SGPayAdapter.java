package com.sg.game.pay;

import android.app.Activity;

public abstract class SGPayAdapter {
    protected Activity activity;
    protected Unity unity;

    public abstract void exit(ExitCallback exitCallback);

    public abstract String getConfig(String str);

    public abstract void init();

    public abstract void moreGame();

    public abstract void pay(int i, PayCallback payCallback);

    public SGPayAdapter(Unity unity2) {
        this.unity = unity2;
        this.activity = unity2.activity;
    }

    public String getPayType() {
        return null;
    }

    public int getSimId() {
        return -1;
    }

    public void onPause() {
    }

    public void onResume() {
    }

    @Deprecated
    public void resetPay() {
    }

    @Deprecated
    public void prePay(int id, int[] exceptIds) {
    }
}
