package com.sg.game.pay;

public interface UnityExitCallback {
    void cancel();

    void exit();
}
