package com.sg.game.pay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import cn.cmgame.am;
import com.unicom.dcLoader.Utils;
import java.util.HashMap;

public class Pay_CUCC implements PayInterface {
    private Activity activity;
    /* access modifiers changed from: private */
    public PayCallback callback;
    private String channelid;
    private HashMap<String, String> month = new HashMap<>(2);
    /* access modifiers changed from: private */
    public String orderId;
    private String[] payPoint;
    private final String urlsgcb = "http://203.195.166.79:8080/sgcb";

    public Pay_CUCC(Unity unity) {
        this.activity = unity.activity;
        try {
            this.payPoint = ((String) Class.forName("com.sg.game.pay.cucc.BuildConfig").getField("cucc").get(null)).split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.channelid = (String) Class.forName("com.sg.game.pay.cucc.BuildConfig").getField("channelid").get(null);
        } catch (Exception e2) {
        }
        try {
            String cuccmonth = (String) Class.forName("com.sg.game.pay.cucc.BuildConfig").getField("cuccmonth").get(null);
            if (cuccmonth != null) {
                String[] monthPoint = cuccmonth.split(",");
                if (monthPoint.length % 2 != 0) {
                    Log.e("SGManager", "Pay_CUCC cuccmonth 必须成对出现");
                    return;
                }
                int i = 0;
                while (i < monthPoint.length) {
                    HashMap<String, String> hashMap = this.month;
                    String str = monthPoint[i];
                    int i2 = i + 1;
                    hashMap.put(str, monthPoint[i2]);
                    i = i2 + 1;
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            Log.e("SGManager", "Pay_CUCC catch:" + e3.getMessage());
        }
    }

    public void init() {
        Utils.getInstances().initPayContext(this.activity, new Utils.UnipayPayResultListener() {
            public void PayResult(String arg0, int arg1, int arg2, String arg3) {
            }
        });
    }

    public void pay(int index, PayCallback callback2) {
        String monthly = this.month.get(this.payPoint[index]);
        if (monthly != null) {
            payOnline(index, monthly, callback2);
        } else {
            payOffline(index, callback2);
        }
    }

    private void payOffline(final int index, final PayCallback callback2) {
        Utils instances = Utils.getInstances();
        Activity activity2 = this.activity;
        String str = this.payPoint[index];
        AnonymousClass2 r3 = new Utils.UnipayPayResultListener() {
            public void PayResult(String paycode, int flag, int flag2, String error) {
                switch (flag) {
                    case 1:
                        callback2.paySuccess(index);
                        return;
                    case 2:
                        callback2.payFail(index, error);
                        return;
                    case 3:
                        callback2.payCancel(index);
                        return;
                    default:
                        callback2.payFail(index, "unknow");
                        return;
                }
            }
        };
        am.bb();
        instances.pay(activity2, str, r3);
    }

    private void payOnline(int index, String monthly, PayCallback callback2) {
        this.callback = callback2;
        new GetOrderIdAsyncTask(this.activity, index, this.channelid, monthly).execute(new String[0]);
    }

    /* access modifiers changed from: private */
    public void online(final int index, String orderId2) {
        Utils.getInstances().payOnline(this.activity, this.payPoint[index], "1", orderId2, new Utils.UnipayPayResultListener() {
            public void PayResult(String paycode, int flag, int flag2, String error) {
                Log.e("SGManager", "online PayResult:" + flag + ":" + flag2 + ":" + error);
                switch (flag) {
                    case 1:
                        Pay_CUCC.this.callback.paySuccess(index);
                        return;
                    case 2:
                        Pay_CUCC.this.callback.payFail(index, error);
                        return;
                    case 3:
                        Pay_CUCC.this.callback.payCancel(index);
                        return;
                    default:
                        Pay_CUCC.this.callback.payFail(index, "unknow");
                        return;
                }
            }
        });
    }

    public void exitGame(ExitCallback exitcallback) {
        exitcallback.exit();
    }

    public void moreGame() {
    }

    /* access modifiers changed from: private */
    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "0";
        }
    }

    class GetOrderIdAsyncTask extends AsyncTask<String, String, String> {
        private String channelid;
        private String consumeCode;
        private Context context;
        private ProgressDialog dialog;
        private int index;

        public GetOrderIdAsyncTask(Context context2, int index2, String channelid2, String consumeCode2) {
            this.context = context2;
            this.index = index2;
            this.channelid = channelid2;
            this.consumeCode = consumeCode2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.dialog = ProgressDialog.show(this.context, "提示", "正在申请订单...");
            this.dialog.setCancelable(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... args) {
            String url = "http://203.195.166.79:8080/sgcb/unipay?appversion=" + Pay_CUCC.getVersionName(this.context) + "&channelid=" + this.channelid + "&consumeCode=" + this.consumeCode;
            Log.e("SGManager", "请求链接：" + url);
            return HttpService.httpQueryGet(url);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String message) {
            this.dialog.dismiss();
            if (!TextUtils.isEmpty(message)) {
                Log.e("SGManager", "订单号" + message);
                String unused = Pay_CUCC.this.orderId = message;
                Pay_CUCC.this.online(this.index, message);
                return;
            }
            Log.e("SGManager", "订单号获取失败");
            Pay_CUCC.this.callback.payFail(this.index, "订单号获取失败");
        }
    }

    class OrderResultAsyncTask extends AsyncTask<String, String, String> {
        private Context context;
        private ProgressDialog dialog;
        private int index;

        public OrderResultAsyncTask(Context context2, int index2) {
            this.context = context2;
            this.index = index2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.dialog = ProgressDialog.show(this.context, "提示", "正在查询支付结果...");
            this.dialog.setCancelable(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... args) {
            return HttpService.httpQueryGet("http://203.195.166.79:8080/sgcb/unipayquery?orderid=" + Pay_CUCC.this.orderId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String message) {
            this.dialog.dismiss();
            Log.e("SGManager", "获取支付结果" + message);
            if ("1".equals(message)) {
                Pay_CUCC.this.callback.paySuccess(this.index);
            } else if (!"0".equals(message)) {
                Pay_CUCC.this.callback.payFail(this.index, "支付失败");
            }
        }
    }
}
