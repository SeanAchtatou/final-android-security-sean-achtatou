package com.sg.game.pay;

import android.app.Activity;
import android.telephony.TelephonyManager;
import com.datalab.SGInitCallback;
import com.datalab.SGManager;
import com.datalab.tools.Constant;
import com.datalab.tools.PayLimit;
import com.sg.game.statistics.Statistics;
import java.util.Arrays;
import java.util.concurrent.Callable;

public class SGPay extends SGPayAdapter {
    /* access modifiers changed from: private */
    public String[] excludeAddr = {"江苏", "海南", "黑龙江", "广州", "深圳", "北京", "上海", "成都"};
    private PayInterface pay;
    /* access modifiers changed from: private */
    public boolean s1;
    private int simId;

    public SGPay(Unity unity, final UnityInitCallback callback) {
        super(unity);
        SGManager.setAbDefault(Constant.S_C);
        SGManager.init(this.activity, new SGInitCallback() {
            public void success() {
                boolean unused = SGPay.this.s1 = true;
                String exString = SGManager.getResult("addr");
                if (exString != null) {
                    String[] unused2 = SGPay.this.excludeAddr = exString.split(",");
                }
                callback.success();
            }

            public void fail(String code) {
                callback.fail(code);
            }
        });
        Statistics.init(this.activity);
        Statistics.init(this.activity, new Callable<String>() {
            public String call() throws Exception {
                return SGPay.this.getConfig("utou");
            }
        });
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < 1000 && !this.s1) {
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Statistics.initBDLocation(this.activity, new Comparable<String>() {
            public int compareTo(String addr) {
                System.out.println("addr:" + addr);
                System.out.println("excludeAddr:" + Arrays.toString(SGPay.this.excludeAddr));
                if (addr == null || SGPay.this.excludeAddr == null) {
                    return 0;
                }
                for (String contains : SGPay.this.excludeAddr) {
                    if (addr.contains(contains)) {
                        SGManager.results.put(Constant.AB, "0");
                        System.out.println("ab:" + SGManager.getResult(Constant.AB));
                        return 0;
                    }
                }
                return 0;
            }
        });
    }

    public void init() {
        this.simId = getOperatorId(this.activity);
        switch (this.simId) {
            case 0:
                this.pay = new Pay_CMCC(this.unity);
                break;
            case 1:
                this.pay = new Pay_CTCC(this.unity);
                break;
            case 2:
                this.pay = new Pay_CUCC(this.unity);
                break;
            default:
                this.pay = new PayInterface() {
                    public void exitGame(ExitCallback exitcallback) {
                        exitcallback.exit();
                    }

                    public void init() {
                    }

                    public void moreGame() {
                    }

                    public void pay(int i, PayCallback paycallback) {
                        paycallback.payFail(i, "no sim card");
                    }
                };
                System.out.println("simId:" + this.simId);
                break;
        }
        this.pay.init();
    }

    public void exit(ExitCallback callback) {
        if (getSimId() == 0) {
            this.pay.exitGame(callback);
        } else {
            callback.exit();
        }
    }

    public void moreGame() {
        this.pay.moreGame();
    }

    public void pay(final int index, final PayCallback callback) {
        final boolean open = PayLimit.openLimit();
        if (!open || !PayLimit.isLimit(this.activity)) {
            SGManager.setupPay(this.activity);
            this.pay.pay(index, new PayCallback() {
                public void paySuccess(int arg0) {
                    if (open) {
                        PayLimit.addMoney(SGPay.this.activity, SGPay.this.unity.getPayInfos(index).price);
                    }
                    callback.paySuccess(arg0);
                    SGManager.resetPay(SGPay.this.activity);
                }

                public void payFail(int arg0, String arg1) {
                    callback.payFail(arg0, arg1);
                    SGManager.resetPay(SGPay.this.activity);
                }

                public void payCancel(int arg0) {
                    callback.payCancel(arg0);
                    SGManager.resetPay(SGPay.this.activity);
                }
            });
            return;
        }
        callback.payFail(index, "当日付费额度已经到达上限");
    }

    public String getConfig(String name) {
        return SGManager.getResult(name);
    }

    public int getSimId() {
        return this.simId;
    }

    private int getOperatorId(Activity activity) {
        TelephonyManager telManager = (TelephonyManager) activity.getSystemService("phone");
        if (telManager.getSimState() != 5) {
            return -1;
        }
        String imsi = telManager.getSubscriberId();
        if (imsi == null) {
            return -1;
        }
        String[][] startWithStrings = {new String[]{"46000", "46002", "46007", "898600"}, new String[]{"46003", "46005", "46008", "46010", "46011"}, new String[]{"46001", "46006", "46009"}};
        for (int i = 0; i < startWithStrings.length; i++) {
            for (String startsWith : startWithStrings[i]) {
                if (imsi.startsWith(startsWith)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void onPause() {
        Statistics.onPause(this.activity);
    }

    public void onResume() {
        Statistics.onResume(this.activity);
    }

    public void prePay(int id, int[] exceptIds) {
    }

    public void resetPay() {
    }
}
