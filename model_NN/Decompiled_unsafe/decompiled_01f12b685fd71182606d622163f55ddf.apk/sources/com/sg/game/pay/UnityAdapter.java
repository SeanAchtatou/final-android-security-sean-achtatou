package com.sg.game.pay;

import com.datalab.tools.Constant;

public class UnityAdapter implements UnityInterface {
    public static final int RMB_001 = 1;
    public static final int RMB_01 = 0;

    public String getName() {
        return "";
    }

    public void init() {
    }

    public void pay(int payMode, int index, UnityPayCallback callback) {
    }

    public void moreGame() {
    }

    public void exitGame(UnityExitCallback callback) {
    }

    public void showBanner() {
    }

    public void destroyBanner() {
    }

    public void showInterstitialAd() {
    }

    public void destroyInterstitialAd() {
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void onDestroy() {
    }

    public void pause() {
    }

    public void login(UnityLoginCallback callback) {
    }

    public String getConfig(String name) {
        return null;
    }

    public void onCreate() {
    }

    public void onStart() {
    }

    public void onRestart() {
    }

    public void onStop() {
    }

    public void onNewIntent() {
    }

    public boolean exitGameShow() {
        return false;
    }

    public boolean moreGameShow() {
        return false;
    }

    public int getSimID() {
        return -1;
    }

    public void resetPay() {
    }

    public void prePay(int id, int[] exceptIds) {
    }

    public int getSmall() {
        String name = getName();
        if ("cmcc".equals(name)) {
            return 1;
        }
        if ("ctcc".equals(name) || "cucc".equals(name)) {
            return 0;
        }
        if (Constant.S_C.equals(getConfig("value"))) {
            return 0;
        }
        if (getSimID() != 0) {
            return 0;
        }
        return 1;
    }
}
