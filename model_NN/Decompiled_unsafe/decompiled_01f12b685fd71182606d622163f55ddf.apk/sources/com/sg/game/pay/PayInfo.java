package com.sg.game.pay;

public class PayInfo {
    public String discription;
    public int index;
    public String name;
    public int price;
    public int reBuy;

    public PayInfo(int index2, String name2, int price2, String discription2, int reBuy2) {
        this.index = index2;
        this.name = name2;
        this.discription = discription2;
        this.price = price2;
        this.reBuy = reBuy2;
    }
}
