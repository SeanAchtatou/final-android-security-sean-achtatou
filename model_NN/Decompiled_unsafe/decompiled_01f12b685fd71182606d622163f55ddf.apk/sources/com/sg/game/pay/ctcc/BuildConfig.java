package com.sg.game.pay.ctcc;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.sg.game.pay.ctcc";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "xiaomi";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
    public static final String ctcc = "TOOL1,TOOL2,TOOL3,TOOL4,TOOL5,TOOL6,TOOL7,TOOL8,TOOL9,TOOL10,TOOL11,TOOL12";
}
