package com.sg.game.pay;

public final class R {

    public static final class color {
        public static final int gc_black = 2131034112;
        public static final int gc_gray = 2131034113;
        public static final int gc_green = 2131034114;
        public static final int gc_light_green = 2131034115;
        public static final int gc_white = 2131034116;
    }

    public static final class drawable {
        public static final int game_arrow_big = 2130837504;
        public static final int game_arrow_little = 2130837505;
        public static final int game_arrow_text = 2130837506;
        public static final int game_check_success = 2130837507;
        public static final int game_checkbox_mark = 2130837508;
        public static final int game_contacts = 2130837509;
        public static final int game_failure = 2130837510;
        public static final int game_grey_logo = 2130837511;
        public static final int game_loading = 2130837512;
        public static final int game_logo = 2130837513;
        public static final int game_network = 2130837514;
        public static final int game_piccode_refresh_touched = 2130837515;
        public static final int game_save = 2130837516;
        public static final int game_show_pwd = 2130837517;
        public static final int game_start_logo = 2130837518;
        public static final int game_success = 2130837519;
        public static final int gray = 2130837520;
        public static final int icon_about = 2130837525;
        public static final int icon_back = 2130837526;
        public static final int icon_bind_email = 2130837527;
        public static final int icon_bind_tel = 2130837528;
        public static final int icon_center_about = 2130837529;
        public static final int icon_center_arrow = 2130837530;
        public static final int icon_center_look = 2130837531;
        public static final int icon_center_save = 2130837532;
        public static final int icon_check_failure = 2130837533;
        public static final int icon_checkbox = 2130837534;
        public static final int icon_close = 2130837535;
        public static final int icon_common_problem = 2130837536;
        public static final int icon_compact_close = 2130837537;
        public static final int icon_discount_icon = 2130837538;
        public static final int icon_edit_del = 2130837539;
        public static final int icon_email_icon = 2130837540;
        public static final int icon_extend = 2130837541;
        public static final int icon_firends_circle = 2130837542;
        public static final int icon_full_arrow_down = 2130837543;
        public static final int icon_full_arrow_up = 2130837544;
        public static final int icon_grey_contacts = 2130837545;
        public static final int icon_head = 2130837546;
        public static final int icon_hide_pwd = 2130837547;
        public static final int icon_magnet_draghide = 2130837548;
        public static final int icon_magnet_gameshare = 2130837549;
        public static final int icon_magnet_help = 2130837550;
        public static final int icon_magnet_onlineservice = 2130837551;
        public static final int icon_magnet_startlogin = 2130837552;
        public static final int icon_magnet_welfare = 2130837553;
        public static final int icon_notification = 2130837554;
        public static final int icon_online_service = 2130837555;
        public static final int icon_personal_bg = 2130837556;
        public static final int icon_personal_bg_l = 2130837557;
        public static final int icon_piccode = 2130837558;
        public static final int icon_piccode_refresh = 2130837559;
        public static final int icon_qq = 2130837560;
        public static final int icon_recommend_flow_one = 2130837561;
        public static final int icon_recommend_flow_third = 2130837562;
        public static final int icon_recommend_flow_two = 2130837563;
        public static final int icon_recommend_hall = 2130837564;
        public static final int icon_rightextend = 2130837565;
        public static final int icon_security_setting = 2130837566;
        public static final int icon_service_tel = 2130837567;
        public static final int icon_share_game = 2130837568;
        public static final int icon_shrink = 2130837569;
        public static final int icon_sina = 2130837570;
        public static final int icon_sms = 2130837571;
        public static final int icon_tel = 2130837572;
        public static final int icon_transaction_detail = 2130837573;
        public static final int icon_upgrade_pass = 2130837574;
        public static final int icon_wechat = 2130837575;
        public static final int icon_window = 2130837576;
        public static final int pay_icon_0 = 2130837577;
        public static final int pay_icon_1 = 2130837578;
        public static final int pay_icon_2 = 2130837579;
        public static final int pay_icon_3 = 2130837580;
        public static final int pay_icon_4 = 2130837581;
        public static final int pay_icon_5 = 2130837582;
        public static final int pay_icon_payment = 2130837583;
        public static final int pay_icon_phonenumber = 2130837584;
        public static final int pay_icon_telpoint = 2130837585;
        public static final int plus_check_success = 2130837586;
        public static final int plus_checkbox_mark = 2130837587;
        public static final int plus_contacts = 2130837588;
        public static final int plus_failure = 2130837589;
        public static final int plus_grey_logo = 2130837590;
        public static final int plus_loading = 2130837591;
        public static final int plus_logo = 2130837592;
        public static final int plus_network = 2130837593;
        public static final int plus_piccode_refesh_touched = 2130837594;
        public static final int plus_save = 2130837595;
        public static final int plus_show_pwd = 2130837596;
        public static final int plus_start_logo = 2130837597;
        public static final int plus_success = 2130837598;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131296273;
        public static final int action_settings = 2131296284;
        public static final int btnBilling2 = 2131296261;
        public static final int btnBilling3 = 2131296265;
        public static final int btnBilling5 = 2131296268;
        public static final int btnGetPic = 2131296263;
        public static final int btnGetSession = 2131296260;
        public static final int btnGetSmsCode = 2131296267;
        public static final int button = 2131296270;
        public static final int custom_icon = 2131296278;
        public static final int deleteMessage = 2131296276;
        public static final int edtAccount = 2131296259;
        public static final int edtPic = 2131296262;
        public static final int edtSms = 2131296266;
        public static final int getMessages = 2131296274;
        public static final int imgCode = 2131296264;
        public static final int messageListView = 2131296271;
        public static final int messageText = 2131296272;
        public static final int notification_content = 2131296283;
        public static final int notification_img = 2131296282;
        public static final int relayout_custom_icon = 2131296277;
        public static final int sendMessage = 2131296275;
        public static final int textView = 2131296269;
        public static final int tv_billing_index = 2131296256;
        public static final int tv_charge_type = 2131296257;
        public static final int tv_custom_content = 2131296281;
        public static final int tv_custom_time = 2131296280;
        public static final int tv_custom_title = 2131296279;
        public static final int tv_tip = 2131296258;
    }

    public static final class layout {
        public static final int adapter_demo_layout = 2130903040;
        public static final int layout_main = 2130903041;
        public static final int main = 2130903042;
        public static final int messageapp = 2130903043;
        public static final int notification_message_icon = 2130903044;
        public static final int notification_message_pic = 2130903045;
    }

    public static final class menu {
        public static final int main = 2131230720;
    }

    public static final class raw {
        public static final int opening_sound = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int g_class_name = 2131099649;
        public static final int gc_gamepad_auto_connected = 2131099650;
        public static final int gc_gamepad_bind_tip = 2131099651;
        public static final int gc_gamepad_confirm_cancel = 2131099652;
        public static final int gc_gamepad_confirm_pay = 2131099653;
        public static final int gc_gamepad_confirm_pay_with_session = 2131099654;
        public static final int gc_gamepad_dialog_bind = 2131099655;
        public static final int gc_gamepad_dialog_connect_fail = 2131099656;
        public static final int gc_gamepad_dialog_connect_fail_gh = 2131099657;
        public static final int gc_gamepad_dialog_connect_lost = 2131099658;
        public static final int gc_gamepad_dialog_connect_none = 2131099659;
        public static final int gc_gamepad_dialog_connect_ok = 2131099660;
        public static final int gc_gamepad_dialog_connect_tip = 2131099661;
        public static final int gc_gamepad_dialog_connect_tip_2 = 2131099662;
        public static final int gc_gamepad_dialog_connecting = 2131099663;
        public static final int gc_gamepad_dialog_find_multi = 2131099664;
        public static final int gc_gamepad_dialog_find_nothing = 2131099665;
        public static final int gc_gamepad_dialog_init_btn_connect = 2131099666;
        public static final int gc_gamepad_dialog_init_btn_game = 2131099667;
        public static final int gc_gamepad_dialog_init_btn_open = 2131099668;
        public static final int gc_gamepad_dialog_low_battery = 2131099669;
        public static final int gc_gamepad_dialog_scaned = 2131099670;
        public static final int gc_gamepad_dialog_state_2 = 2131099671;
        public static final int gc_gamepad_dialog_state_3 = 2131099672;
        public static final int gc_gamepad_dialog_state_4 = 2131099673;
        public static final int gc_gamepad_errcode_201220 = 2131099674;
        public static final int gc_gamepad_errcode_201221 = 2131099675;
        public static final int gc_gamepad_errcode_201222 = 2131099676;
        public static final int gc_gamepad_errcode_999999 = 2131099677;
        public static final int gc_gamepad_input_pwd_tip = 2131099678;
        public static final int gc_gamepad_input_reset_tip = 2131099679;
        public static final int gc_gamepad_prompt_back = 2131099680;
        public static final int gc_gamepad_prompt_no = 2131099681;
        public static final int gc_gamepad_prompt_sure = 2131099682;
        public static final int gc_gamepad_prompt_yes = 2131099683;
        public static final int gc_gamepad_supported = 2131099684;
        public static final int gc_gamepad_unavailable_pwd_tip = 2131099685;
        public static final int gc_leaderboard_add_friend_from_contacts = 2131099686;
        public static final int gc_leaderboard_add_friend_success = 2131099687;
        public static final int gc_leaderboard_add_friend_title = 2131099688;
        public static final int gc_leaderboard_apply_accept = 2131099689;
        public static final int gc_leaderboard_apply_empty_content = 2131099690;
        public static final int gc_leaderboard_apply_ignore = 2131099691;
        public static final int gc_leaderboard_btn_add_friend = 2131099692;
        public static final int gc_leaderboard_btn_friend = 2131099693;
        public static final int gc_leaderboard_check_contacts = 2131099694;
        public static final int gc_leaderboard_commit_score_failed = 2131099695;
        public static final int gc_leaderboard_commit_score_ok = 2131099696;
        public static final int gc_leaderboard_contacts_empty_content = 2131099697;
        public static final int gc_leaderboard_contacts_next_page = 2131099698;
        public static final int gc_leaderboard_contacts_search = 2131099699;
        public static final int gc_leaderboard_contacts_search_hint = 2131099700;
        public static final int gc_leaderboard_create_record = 2131099701;
        public static final int gc_leaderboard_current_high_score = 2131099702;
        public static final int gc_leaderboard_current_rank = 2131099703;
        public static final int gc_leaderboard_default_nick_name = 2131099704;
        public static final int gc_leaderboard_empty_friend_scores = 2131099705;
        public static final int gc_leaderboard_empty_score = 2131099706;
        public static final int gc_leaderboard_error_loading = 2131099707;
        public static final int gc_leaderboard_friend = 2131099708;
        public static final int gc_leaderboard_friend_accept_failed = 2131099709;
        public static final int gc_leaderboard_friend_accept_ok = 2131099710;
        public static final int gc_leaderboard_friend_reject_failed = 2131099711;
        public static final int gc_leaderboard_friend_reject_ok = 2131099712;
        public static final int gc_leaderboard_friend_request = 2131099713;
        public static final int gc_leaderboard_invite = 2131099714;
        public static final int gc_leaderboard_invite_friend = 2131099715;
        public static final int gc_leaderboard_invited = 2131099716;
        public static final int gc_leaderboard_item_data_is_null = 2131099717;
        public static final int gc_leaderboard_land_friend = 2131099718;
        public static final int gc_leaderboard_land_player = 2131099719;
        public static final int gc_leaderboard_list_item_add = 2131099720;
        public static final int gc_leaderboard_loading = 2131099721;
        public static final int gc_leaderboard_login_need = 2131099722;
        public static final int gc_leaderboard_not_adressbook = 2131099723;
        public static final int gc_leaderboard_notify_apply_title = 2131099724;
        public static final int gc_leaderboard_notify_invite_title = 2131099725;
        public static final int gc_leaderboard_player = 2131099726;
        public static final int gc_leaderboard_processing = 2131099727;
        public static final int gc_leaderboard_search_edittext_null = 2131099728;
        public static final int gc_leaderboard_search_empty_content = 2131099729;
        public static final int gc_leaderboard_search_friend = 2131099730;
        public static final int gc_leaderboard_search_hint = 2131099731;
        public static final int gc_leaderboard_search_result = 2131099732;
        public static final int gc_leaderboard_search_result_title = 2131099733;
        public static final int gc_leaderboard_start_game = 2131099734;
        public static final int gc_leaderboard_waiting_check = 2131099735;
    }
}
