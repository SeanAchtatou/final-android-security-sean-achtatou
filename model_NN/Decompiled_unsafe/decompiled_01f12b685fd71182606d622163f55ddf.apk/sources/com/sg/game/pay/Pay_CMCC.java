package com.sg.game.pay;

import android.app.Activity;
import cn.cmgame.am;
import cn.cmgame.billing.api.GameInterface;
import com.datalab.tools.Constant;
import com.wali.gamecenter.report.io.HttpConnectionManager;
import java.util.Random;

public class Pay_CMCC implements PayInterface {
    private Activity activity;
    private String cpparam;
    /* access modifiers changed from: private */
    public long lastPayTime;
    private String[] payPoint;
    private Random random = new Random();
    private Unity unity;

    public Pay_CMCC(Unity unity2) {
        this.unity = unity2;
        this.activity = unity2.activity;
        try {
            this.payPoint = ((String) Class.forName("com.sg.game.pay.cmcc.BuildConfig").getField("cmcc").get(null)).split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        Activity activity2 = this.activity;
        am.bm(activity2);
        GameInterface.initializeApp(activity2);
    }

    public void pay(final int index, final PayCallback callback) {
        if ("1".equals(this.unity.getConfig("pause"))) {
            callback.payFail(index, "pause==1");
            return;
        }
        String ab = this.unity.getConfig(Constant.AB);
        if (ab != null && !"0".equals(ab)) {
            int time = HttpConnectionManager.GPRS_WAIT_TIMEOUT;
            try {
                time = Integer.parseInt(this.unity.getConfig("timeSpan"));
            } catch (Exception e) {
            }
            int rndTime = HttpConnectionManager.GPRS_WAIT_TIMEOUT;
            try {
                rndTime = Integer.parseInt(this.unity.getConfig("rndTime"));
            } catch (Exception e2) {
            }
            int time2 = time + this.random.nextInt(rndTime);
            long payTime = System.currentTimeMillis();
            if (payTime - this.lastPayTime < ((long) time2)) {
                callback.payFail(index, "time is too short");
                return;
            }
            this.lastPayTime = payTime;
        }
        GameInterface.IPayCallback billingCallback = new GameInterface.IPayCallback() {
            public void onResult(int resultCode, String billingIndex, Object arg) {
                am.dd(resultCode, billingIndex);
                switch (resultCode) {
                    case 1:
                        callback.paySuccess(index);
                        return;
                    case 2:
                        callback.payFail(index, arg.toString());
                        long unused = Pay_CMCC.this.lastPayTime = 0;
                        return;
                    case 3:
                        callback.payCancel(index);
                        long unused2 = Pay_CMCC.this.lastPayTime = 0;
                        return;
                    default:
                        return;
                }
            }
        };
        Activity activity2 = this.activity;
        String str = this.payPoint[index];
        String str2 = this.cpparam;
        am.bb();
        GameInterface.doBilling(activity2, true, true, str, str2, billingCallback);
    }

    public void exitGame(final ExitCallback callback) {
        GameInterface.exit(this.activity, new GameInterface.GameExitCallback() {
            public void onCancelExit() {
                callback.cancel();
            }

            public void onConfirmExit() {
                callback.exit();
            }
        });
    }

    public void exitGameWithUI(final ExitCallback callback) {
        GameInterface.exit(this.activity, new GameInterface.GameExitCallback() {
            public void onCancelExit() {
                callback.cancel();
            }

            public void onConfirmExit() {
                callback.exit();
            }
        });
    }

    public void moreGame() {
        GameInterface.viewMoreGames(this.activity);
    }

    public void setCpparam(String cpparam2) {
        this.cpparam = cpparam2;
    }
}
