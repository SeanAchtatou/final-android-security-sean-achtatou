package com.sg.game.pay;

public interface PayInterface {
    void exitGame(ExitCallback exitCallback);

    void init();

    void moreGame();

    void pay(int i, PayCallback payCallback);
}
