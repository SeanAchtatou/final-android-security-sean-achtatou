package com.sg.game.pay;

public interface UnityInterface {
    public static final int PAYMODE_AUTO = 99;
    public static final int PAYMODE_EXT_ALIPAY = 4;
    public static final int PAYMODE_EXT_BANKCARD = 5;
    public static final int PAYMODE_EXT_WECHAT = 3;
    public static final int PAYMODE_JD = 1;
    public static final int PAYMODE_MM = 0;
    public static final int PAYMODE_THIRD = 2;

    void destroyBanner();

    void destroyInterstitialAd();

    void exitGame(UnityExitCallback unityExitCallback);

    boolean exitGameShow();

    String getConfig(String str);

    String getName();

    int getSimID();

    void init();

    void login(UnityLoginCallback unityLoginCallback);

    void moreGame();

    boolean moreGameShow();

    void onCreate();

    void onDestroy();

    void onNewIntent();

    void onPause();

    void onRestart();

    void onResume();

    void onStart();

    void onStop();

    void pause();

    void pay(int i, int i2, UnityPayCallback unityPayCallback);

    void prePay(int i, int[] iArr);

    void resetPay();

    void showBanner();

    void showInterstitialAd();
}
