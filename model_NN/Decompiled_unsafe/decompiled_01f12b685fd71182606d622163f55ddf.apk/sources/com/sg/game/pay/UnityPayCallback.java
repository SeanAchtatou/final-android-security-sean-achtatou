package com.sg.game.pay;

public interface UnityPayCallback {
    void payCancel(int i);

    void payFail(int i, String str);

    void paySuccess(int i);
}
