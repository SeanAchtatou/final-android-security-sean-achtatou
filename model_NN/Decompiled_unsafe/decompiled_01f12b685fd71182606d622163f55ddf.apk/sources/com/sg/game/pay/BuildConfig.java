package com.sg.game.pay;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.sg.game.pay";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "xiaomi";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
}
