package com.sg.game.pay;

public class HttpService {
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String httpQueryGet(java.lang.String r11) {
        /*
            r10 = 10000(0x2710, float:1.4013E-41)
            java.lang.String r8 = "SGManager"
            java.lang.String r9 = "star query "
            android.util.Log.e(r8, r9)
            r3 = 0
            java.lang.String r7 = ""
            org.apache.http.params.BasicHttpParams r5 = new org.apache.http.params.BasicHttpParams
            r5.<init>()
            r8 = 10000(0x2710, double:4.9407E-320)
            org.apache.http.conn.params.ConnManagerParams.setTimeout(r5, r8)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r5, r10)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r5, r10)
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
            r1.<init>(r5)
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0076 }
            r4.<init>(r11)     // Catch:{ Exception -> 0x0076 }
            org.apache.http.HttpResponse r6 = r1.execute(r4)     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            org.apache.http.StatusLine r8 = r6.getStatusLine()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            int r8 = r8.getStatusCode()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            r9 = 200(0xc8, float:2.8E-43)
            if (r8 == r9) goto L_0x006d
            java.lang.String r8 = "SGManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            r9.<init>()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            java.lang.String r10 = "Yangzc http status code "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            org.apache.http.StatusLine r10 = r6.getStatusLine()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            int r10 = r10.getStatusCode()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            android.util.Log.e(r8, r9)     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            java.lang.String r7 = ""
        L_0x0058:
            if (r4 == 0) goto L_0x005d
            r4.abort()
        L_0x005d:
            org.apache.http.conn.ClientConnectionManager r8 = r1.getConnectionManager()
            r8.shutdown()
            java.lang.String r8 = "SGManager"
            java.lang.String r9 = "finally .................end"
            android.util.Log.e(r8, r9)
            r3 = r4
        L_0x006c:
            return r7
        L_0x006d:
            org.apache.http.HttpEntity r2 = r6.getEntity()     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            java.lang.String r7 = org.apache.http.util.EntityUtils.toString(r2)     // Catch:{ Exception -> 0x00c2, all -> 0x00bf }
            goto L_0x0058
        L_0x0076:
            r0 = move-exception
        L_0x0077:
            r0.printStackTrace()     // Catch:{ all -> 0x00aa }
            java.lang.String r8 = "SGManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x00aa }
            r9.<init>()     // Catch:{ all -> 0x00aa }
            java.lang.String r10 = "Yangzc http Exception "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00aa }
            java.lang.String r10 = r0.getMessage()     // Catch:{ all -> 0x00aa }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00aa }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00aa }
            android.util.Log.e(r8, r9)     // Catch:{ all -> 0x00aa }
            if (r3 == 0) goto L_0x009b
            r3.abort()
        L_0x009b:
            org.apache.http.conn.ClientConnectionManager r8 = r1.getConnectionManager()
            r8.shutdown()
            java.lang.String r8 = "SGManager"
            java.lang.String r9 = "finally .................end"
            android.util.Log.e(r8, r9)
            goto L_0x006c
        L_0x00aa:
            r8 = move-exception
        L_0x00ab:
            if (r3 == 0) goto L_0x00b0
            r3.abort()
        L_0x00b0:
            org.apache.http.conn.ClientConnectionManager r9 = r1.getConnectionManager()
            r9.shutdown()
            java.lang.String r9 = "SGManager"
            java.lang.String r10 = "finally .................end"
            android.util.Log.e(r9, r10)
            throw r8
        L_0x00bf:
            r8 = move-exception
            r3 = r4
            goto L_0x00ab
        L_0x00c2:
            r0 = move-exception
            r3 = r4
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sg.game.pay.HttpService.httpQueryGet(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f7  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x00c2=Splitter:B:16:0x00c2, B:22:0x00d3=Splitter:B:22:0x00d3, B:28:0x00e4=Splitter:B:28:0x00e4} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String httpQueryPost(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = 10000(0x2710, float:1.4013E-41)
            r4 = 0
            java.lang.String r7 = ""
            org.apache.http.params.BasicHttpParams r3 = new org.apache.http.params.BasicHttpParams
            r3.<init>()
            r8 = 10000(0x2710, double:4.9407E-320)
            org.apache.http.conn.params.ConnManagerParams.setTimeout(r3, r8)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r10)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r10)
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
            r1.<init>(r3)
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ ClientProtocolException -> 0x00c1, IOException -> 0x00d2, Exception -> 0x00e3 }
            r5.<init>(r11)     // Catch:{ ClientProtocolException -> 0x00c1, IOException -> 0x00d2, Exception -> 0x00e3 }
            java.lang.String r8 = "UnipayDemo"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            r9.<init>()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r10 = "Yangzc Post "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r9 = r9.toString()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            android.util.Log.d(r8, r9)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r8 = "UnipayDemo"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            r9.<init>()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r10 = "Yangzc Post "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r9 = r9.toString()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            android.util.Log.d(r8, r9)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r8 = "Content-Type"
            java.lang.String r9 = "text/plain; charset=UTF-8"
            r5.setHeader(r8, r9)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            org.apache.http.entity.StringEntity r8 = new org.apache.http.entity.StringEntity     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r9 = "utf-8"
            r8.<init>(r12, r9)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            r5.setEntity(r8)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            org.apache.http.HttpResponse r6 = r1.execute(r5)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            org.apache.http.StatusLine r8 = r6.getStatusLine()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            int r8 = r8.getStatusCode()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r8 == r9) goto L_0x00b8
            java.lang.String r8 = "UnipayDemo"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            r9.<init>()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r10 = "Yangzc http status code "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            org.apache.http.StatusLine r10 = r6.getStatusLine()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            int r10 = r10.getStatusCode()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r9 = r9.toString()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            android.util.Log.e(r8, r9)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r7 = ""
        L_0x0092:
            if (r5 == 0) goto L_0x0097
            r5.abort()
        L_0x0097:
            org.apache.http.conn.ClientConnectionManager r8 = r1.getConnectionManager()
            r8.shutdown()
            r4 = r5
        L_0x009f:
            java.lang.String r8 = "UnipayDemo"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "response:"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r9 = r9.toString()
            android.util.Log.d(r8, r9)
            return r7
        L_0x00b8:
            org.apache.http.HttpEntity r2 = r6.getEntity()     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            java.lang.String r7 = org.apache.http.util.EntityUtils.toString(r2)     // Catch:{ ClientProtocolException -> 0x010b, IOException -> 0x0108, Exception -> 0x0105, all -> 0x0102 }
            goto L_0x0092
        L_0x00c1:
            r0 = move-exception
        L_0x00c2:
            r0.printStackTrace()     // Catch:{ all -> 0x00f4 }
            if (r4 == 0) goto L_0x00ca
            r4.abort()
        L_0x00ca:
            org.apache.http.conn.ClientConnectionManager r8 = r1.getConnectionManager()
            r8.shutdown()
            goto L_0x009f
        L_0x00d2:
            r0 = move-exception
        L_0x00d3:
            r0.printStackTrace()     // Catch:{ all -> 0x00f4 }
            if (r4 == 0) goto L_0x00db
            r4.abort()
        L_0x00db:
            org.apache.http.conn.ClientConnectionManager r8 = r1.getConnectionManager()
            r8.shutdown()
            goto L_0x009f
        L_0x00e3:
            r0 = move-exception
        L_0x00e4:
            r0.printStackTrace()     // Catch:{ all -> 0x00f4 }
            if (r4 == 0) goto L_0x00ec
            r4.abort()
        L_0x00ec:
            org.apache.http.conn.ClientConnectionManager r8 = r1.getConnectionManager()
            r8.shutdown()
            goto L_0x009f
        L_0x00f4:
            r8 = move-exception
        L_0x00f5:
            if (r4 == 0) goto L_0x00fa
            r4.abort()
        L_0x00fa:
            org.apache.http.conn.ClientConnectionManager r9 = r1.getConnectionManager()
            r9.shutdown()
            throw r8
        L_0x0102:
            r8 = move-exception
            r4 = r5
            goto L_0x00f5
        L_0x0105:
            r0 = move-exception
            r4 = r5
            goto L_0x00e4
        L_0x0108:
            r0 = move-exception
            r4 = r5
            goto L_0x00d3
        L_0x010b:
            r0 = move-exception
            r4 = r5
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sg.game.pay.HttpService.httpQueryPost(java.lang.String, java.lang.String):java.lang.String");
    }
}
