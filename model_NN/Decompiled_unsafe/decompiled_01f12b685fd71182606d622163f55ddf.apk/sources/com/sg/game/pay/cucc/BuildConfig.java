package com.sg.game.pay.cucc;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.sg.game.pay.cucc";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "xiaomi";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
    public static final String channelid = "00020620";
    public static final String cucc = "001,002,003,004,005,006,007,008,009,010,011,012";
}
