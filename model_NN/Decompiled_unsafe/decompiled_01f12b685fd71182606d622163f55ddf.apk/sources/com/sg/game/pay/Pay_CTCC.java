package com.sg.game.pay;

import android.app.Activity;
import android.widget.Toast;
import cn.cmgame.am;
import cn.egame.terminal.paysdk.EgameExitListener;
import cn.egame.terminal.paysdk.EgamePay;
import cn.egame.terminal.paysdk.EgamePayListener;
import java.util.HashMap;
import java.util.Map;

public class Pay_CTCC implements PayInterface {
    /* access modifiers changed from: private */
    public Activity activity;
    private String[] payPoint;

    public Pay_CTCC(Unity unity) {
        this.activity = unity.activity;
        try {
            this.payPoint = ((String) Class.forName("com.sg.game.pay.ctcc.BuildConfig").getField("ctcc").get(null)).split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        Activity activity2 = this.activity;
        EgamePay.init(activity2);
        am.bm(activity2);
    }

    public void pay(final int index, final PayCallback callback) {
        Map<String, String> payParams = new HashMap<>();
        payParams.put(EgamePay.PAY_PARAMS_KEY_TOOLS_ALIAS, this.payPoint[index]);
        Activity activity2 = this.activity;
        AnonymousClass1 r2 = new EgamePayListener() {
            public void payCancel(Map<String, String> map) {
                callback.payCancel(index);
            }

            public void payFailed(Map<String, String> map, int errorInt) {
                Toast.makeText(Pay_CTCC.this.activity, "支付失败=" + errorInt, 1).show();
                callback.payFail(index, "" + errorInt);
            }

            public void paySuccess(Map<String, String> map) {
                callback.paySuccess(index);
            }
        };
        am.bb();
        EgamePay.pay(activity2, payParams, r2);
    }

    public void moreGame() {
        EgamePay.moreGame(this.activity);
    }

    public void exitGame(final ExitCallback callback) {
        EgamePay.exit(this.activity, new EgameExitListener() {
            public void exit() {
                callback.exit();
            }

            public void cancel() {
                callback.cancel();
            }
        });
    }
}
