package com.sg.game.pay;

import android.app.Activity;
import android.content.Intent;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.objectweb.asm.Opcodes;

public class Unity extends UnityAdapter {
    protected Activity activity;
    protected Map<Integer, PayInfo> payInfos;

    public Unity(Activity activity2, UnityInitCallback callback) {
        this.activity = activity2;
        loadPayInfo();
    }

    private void loadPayInfo() {
        InputStream is = null;
        DataOutputStream dos = null;
        try {
            is = this.activity.getAssets().open("payPoint.txt");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos2 = new DataOutputStream(baos);
            try {
                byte[] buffer = new byte[Opcodes.ACC_STRICT];
                while (true) {
                    int len = is.read(buffer);
                    if (len <= 0) {
                        break;
                    }
                    dos2.write(buffer, 0, len);
                }
                String[] data = new String(baos.toByteArray(), "UTF-8").replace("\r\n", "\n").split("\n");
                int len2 = data.length - 1;
                this.payInfos = new HashMap();
                for (int i = 0; i < len2; i++) {
                    System.out.println(data[i + 1]);
                    String[] temp = data[i + 1].split("#");
                    int index = Integer.parseInt(temp[0]);
                    this.payInfos.put(Integer.valueOf(index), new PayInfo(index, temp[1], Integer.parseInt(temp[2]), temp[3], Integer.parseInt(temp[4])));
                }
                try {
                    is.close();
                } catch (Exception e) {
                }
                try {
                    dos2.close();
                } catch (Exception e2) {
                }
            } catch (IOException e3) {
                e1 = e3;
                dos = dos2;
                try {
                    e1.printStackTrace();
                    try {
                        is.close();
                    } catch (Exception e4) {
                    }
                    try {
                        dos.close();
                    } catch (Exception e5) {
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        is.close();
                    } catch (Exception e6) {
                    }
                    try {
                        dos.close();
                    } catch (Exception e7) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                dos = dos2;
                is.close();
                dos.close();
                throw th;
            }
        } catch (IOException e8) {
            e1 = e8;
            e1.printStackTrace();
            is.close();
            dos.close();
        }
    }

    public PayInfo getPayInfos(int index) {
        return this.payInfos.get(Integer.valueOf(index));
    }

    /* access modifiers changed from: protected */
    public void defaultPay(SGPayAdapter sgPay, int index, final UnityPayCallback callback) {
        sgPay.pay(index, new PayCallback() {
            public void paySuccess(int index) {
                callback.paySuccess(index);
            }

            public void payFail(int index, String errorCode) {
                callback.payFail(index, errorCode);
            }

            public void payCancel(int index) {
                callback.payCancel(index);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void defaultExit(SGPayAdapter sgPay, final UnityExitCallback callback) {
        if (sgPay.getSimId() == 0) {
            sgPay.exit(new ExitCallback() {
                public void exit() {
                    callback.exit();
                }

                public void cancel() {
                    callback.cancel();
                }
            });
        } else {
            callback.exit();
        }
    }

    public static String getBuildConfig(String buildConfigPrefix, String key) {
        try {
            return (String) Class.forName(buildConfigPrefix + ".BuildConfig").getField(key).get(null);
        } catch (Exception e) {
            return null;
        }
    }

    public void onNewIntent(Intent intent) {
        onNewIntent();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}
