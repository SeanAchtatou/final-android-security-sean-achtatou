package com.sg.game.pay;

import com.datalab.SGManager;
import com.sg.game.statistics.Statistics;
import com.unicom.shield.UnicomApplicationWrapper;

public class PayApplication extends UnicomApplicationWrapper {
    public void onCreate() {
        super.onCreate();
        SGManager.initService(this);
        Statistics.applicationOnCreate(this);
        try {
            System.loadLibrary("megjb");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
