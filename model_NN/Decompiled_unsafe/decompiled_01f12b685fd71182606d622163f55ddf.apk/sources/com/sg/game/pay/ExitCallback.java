package com.sg.game.pay;

public interface ExitCallback {
    void cancel();

    void exit();
}
