package com.sg.game.util;

public class Unityutil {
    public static String getBuildConfig(String buildConfigPrefix, String key) {
        try {
            return (String) Class.forName(buildConfigPrefix + ".BuildConfig").getField(key).get(null);
        } catch (Exception e) {
            return null;
        }
    }
}
