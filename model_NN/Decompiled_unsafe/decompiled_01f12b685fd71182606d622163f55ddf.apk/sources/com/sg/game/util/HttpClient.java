package com.sg.game.util;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestHeader;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpClient {
    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    private interface HttpResponseListener {
        void failed(Throwable th);

        void handleHttpResponse(HttpClientResponse httpClientResponse);
    }

    public static void sendRequest(final Request request, String url, int timeout) {
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setTimeOut(timeout);
        httpRequest.setUrl(url);
        httpRequest.setContent(request.getContent());
        httpRequest.setHeader(HttpRequestHeader.UserAgent, "SG");
        sendHttpRequest(httpRequest, new HttpResponseListener() {
            public void handleHttpResponse(HttpClientResponse httpResponse) {
                int code = httpResponse.getCode();
                if (code == 200) {
                    try {
                        request.success(httpResponse.getResultAsString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        request.fail(code);
                    }
                } else {
                    request.fail(code);
                }
            }

            public void failed(Throwable t) {
                request.fail(-1);
                t.printStackTrace();
            }
        });
    }

    private HttpClient() {
    }

    public static void close() {
        executorService.shutdown();
    }

    private static void sendHttpRequest(HttpRequest httpRequest, final HttpResponseListener httpResponseListener) {
        if (httpRequest.getUrl() == null) {
            httpResponseListener.failed(new RuntimeException("can't process a HTTP request without URL set"));
            return;
        }
        String queryString = "";
        try {
            String value = httpRequest.getContent();
            if (value != null && !"".equals(value)) {
                queryString = "?" + value;
            }
            URL url = new URL(httpRequest.getUrl() + queryString);
            System.out.println(url);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(Net.HttpMethods.GET);
            for (Map.Entry<String, String> header : httpRequest.getHeaders().entrySet()) {
                connection.addRequestProperty((String) header.getKey(), (String) header.getValue());
            }
            connection.setConnectTimeout(httpRequest.getTimeOut());
            connection.setReadTimeout(httpRequest.getTimeOut());
            executorService.submit(new Runnable() {
                public void run() {
                    try {
                        connection.connect();
                        httpResponseListener.handleHttpResponse(new HttpClientResponse(connection));
                    } catch (Exception e) {
                        httpResponseListener.failed(e);
                    } finally {
                        connection.disconnect();
                    }
                }
            });
        } catch (Exception e) {
            httpResponseListener.failed(e);
        }
    }

    private static class HttpClientResponse {
        private int code;
        private HttpURLConnection connection;
        private InputStream inputStream;

        public HttpClientResponse(HttpURLConnection connection2) throws IOException {
            this.connection = connection2;
            try {
                this.inputStream = connection2.getInputStream();
            } catch (IOException e) {
                this.inputStream = connection2.getErrorStream();
            }
            try {
                this.code = connection2.getResponseCode();
            } catch (IOException e2) {
                this.code = 500;
            }
        }

        public String getResultAsString() {
            StringBuilder b;
            BufferedReader reader = null;
            try {
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(this.inputStream, "UTF-8"));
                try {
                    int approxStringLength = this.connection.getContentLength();
                    if (approxStringLength > 0) {
                        b = new StringBuilder(approxStringLength);
                    } else {
                        b = new StringBuilder();
                    }
                    while (true) {
                        String line = reader2.readLine();
                        if (line != null) {
                            b.append(line);
                        } else {
                            String sb = b.toString();
                            HttpClient.closeQuietly(reader2);
                            return sb;
                        }
                    }
                } catch (IOException e) {
                    reader = reader2;
                    HttpClient.closeQuietly(reader);
                    return "";
                } catch (Throwable th) {
                    HttpClient.closeQuietly(reader2);
                    throw th;
                }
            } catch (IOException e2) {
                HttpClient.closeQuietly(reader);
                return "";
            }
        }

        public int getCode() {
            return this.code;
        }
    }

    private static class HttpRequest {
        private String content;
        private Map<String, String> headers = new HashMap();
        private int timeOut = 0;
        private String url;

        public void setUrl(String url2) {
            this.url = url2;
        }

        public void setHeader(String name, String value) {
            this.headers.put(name, value);
        }

        public void setContent(String content2) {
            this.content = content2;
        }

        public void setTimeOut(int timeOut2) {
            this.timeOut = timeOut2;
        }

        public int getTimeOut() {
            return this.timeOut;
        }

        public String getUrl() {
            return this.url;
        }

        public String getContent() {
            return this.content;
        }

        public Map<String, String> getHeaders() {
            return this.headers;
        }
    }

    public static void closeQuietly(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (IOException e) {
            }
        }
    }

    public static abstract class Request {
        private String content;

        public abstract void fail(int i);

        public abstract void success(String str) throws Exception;

        public Request(String content2) {
            this.content = content2;
        }

        public String getContent() {
            return this.content;
        }
    }
}
