package com.sg.game.statistics;

import android.app.Activity;
import android.content.Context;
import java.util.concurrent.Callable;

public class Statistics {
    private static Class<?> lotuseedCls;
    private static Class<?> umengCls;

    public static void init(Activity activity) {
        try {
            lotuseedCls = Class.forName("com.sg.game.statistics.LotuseedStatistics");
            lotuseedCls.getMethod("init", Activity.class).invoke(null, activity);
            System.out.println("Lotuseed is loaded");
        } catch (Exception e) {
        }
        try {
            umengCls = Class.forName("com.sg.game.statistics.UmengStatistics");
            umengCls.getMethod("init", Activity.class).invoke(null, activity);
            System.out.println("Umeng is loaded");
        } catch (Exception e2) {
        }
        try {
            Class.forName("com.sg.game.statistics.CPAStatistics").getMethod("init", Activity.class).invoke(null, activity);
            System.out.println("CPA is loaded");
        } catch (Exception e3) {
        }
        try {
            Class.forName("com.sg.game.statistics.SSLStatistics").getMethod("init", Context.class).invoke(null, activity);
            System.out.println("RTSDK is loaded");
        } catch (Exception e4) {
        }
    }

    public static void onPause(Activity activity) {
        if (lotuseedCls != null) {
            try {
                lotuseedCls.getMethod("onPause", Activity.class).invoke(null, activity);
            } catch (Exception e) {
            }
        }
        if (umengCls != null) {
            try {
                umengCls.getMethod("onPause", Activity.class).invoke(null, activity);
            } catch (Exception e2) {
            }
        }
    }

    public static void onResume(Activity activity) {
        if (lotuseedCls != null) {
            try {
                lotuseedCls.getMethod("onResume", Activity.class).invoke(null, activity);
            } catch (Exception e) {
            }
        }
        if (umengCls != null) {
            try {
                umengCls.getMethod("onResume", Activity.class).invoke(null, activity);
            } catch (Exception e2) {
            }
        }
    }

    public static void applicationOnCreate(Context context) {
        try {
            Class.forName("com.sg.game.statistics.TalkingDataStatistics").getMethod("onCreat", Context.class).invoke(null, context);
            System.out.println("TalkingData is loaded");
        } catch (Exception e) {
        }
    }

    public static void init(Activity activity, Callable<String> callable) {
        try {
            Class.forName("com.sg.game.statistics.UtouStatistics").getMethod("init", Activity.class, Callable.class).invoke(null, activity, callable);
            System.out.println("utou is loaded");
        } catch (Exception e) {
        }
    }

    public static void initBDLocation(Context context, Comparable<String> callable) {
        try {
            Class.forName("com.sg.game.statistics.BaiduLocation").getMethod("init", Context.class, Comparable.class).invoke(null, context, callable);
            System.out.println("BaiduLocation is loaded");
        } catch (Exception e) {
        }
    }
}
