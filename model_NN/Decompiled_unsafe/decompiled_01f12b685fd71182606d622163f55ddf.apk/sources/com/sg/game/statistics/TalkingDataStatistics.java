package com.sg.game.statistics;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.tendcloud.tenddata.TCAgent;

public class TalkingDataStatistics {
    public static void onCreat(Context context) throws Exception {
        TCAgent.LOG_ON = true;
        ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
        String TD_APP_ID = appInfo.metaData.getString("TD_APP_ID");
        String TD_CHANNEL_ID = appInfo.metaData.getString("TD_CHANNEL_ID");
        TCAgent.init(context, TD_APP_ID, TD_CHANNEL_ID);
        TCAgent.setReportUncaughtExceptions(true);
        StringBuffer buffer = new StringBuffer();
        buffer.append("TD_APP_ID=").append(TD_APP_ID);
        buffer.append("\nTD_CHANNEL_ID=").append(TD_CHANNEL_ID);
        System.setProperty("sgdebug", System.getProperty("sgdebug", "") + buffer.toString() + "\n");
    }
}
