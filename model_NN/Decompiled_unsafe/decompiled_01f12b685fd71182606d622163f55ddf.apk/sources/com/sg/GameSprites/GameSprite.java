package com.sg.GameSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.spine.MySpine;
import com.sg.map.GameMap;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;

public class GameSprite extends MySpine implements GameConstant {
    int a;
    private int attackDelay = 0;
    int col;
    private int colorTime;
    private boolean isAttack = false;
    public boolean isCanMove = true;
    public boolean isInjured = false;
    boolean isJumpMove;
    public boolean isPlaySoundOne = false;
    public boolean isSlow = false;
    int line;
    private GameMap map;
    int mapIndex;
    float mx;
    float my;
    int n;
    int nextDir;
    int random;
    float startBone;
    int startJumpSpeed;
    int tx;
    int ty;

    public GameSprite(int type, int x, int y) {
        this.type = type;
        init(SPINE_NAME);
        createSpineRole(21, 1.0f);
        setMix(0.3f);
        initMotion(motion_leiguman);
        setStatus(1);
        setPosition((float) x, (float) y);
        setId();
        setAniSpeed(1.0f);
        GameStage.addActor(this, y, 2);
    }

    public void initProp() {
    }

    public void runTime() {
        setCdTime(getCdTime() + GameStage.getDelta());
    }

    public void run(float delta) {
        updata();
        this.index++;
    }

    public void act(float delta) {
        super.act(delta);
        run(delta);
    }

    public void moveRole() {
        float f = Animation.CurveTimeline.LINEAR;
        if (this.colorTime > 0) {
            this.colorTime--;
            if (this.colorTime == 0) {
                setColor(Color.WHITE);
            }
        }
        switch (this.curStatus) {
            case 1:
                roleMove(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                break;
            case 2:
                roleMove(this.isLeft ? -5.0f : 5.0f, Animation.CurveTimeline.LINEAR);
                break;
            case 8:
                if (this.index == 1) {
                    this.startJumpSpeed = -20;
                    this.a = 1;
                }
                this.startJumpSpeed += this.a;
                if (this.isJumpMove) {
                    f = (float) (this.isLeft ? -5 : 5);
                }
                roleMove(f, (float) this.startJumpSpeed);
                break;
            case 11:
                this.isAttack = true;
                setStatus(4);
                break;
            case 42:
                if (this.attackDelay > 0) {
                    this.attackDelay--;
                    if (this.attackDelay == 1) {
                        this.attackDelay = 40;
                        setStatus(4);
                        break;
                    }
                }
                break;
        }
        setFilpX(this.isLeft);
        if (isEnd()) {
            statusToAnimation();
        }
        ctrl();
    }

    public void roleMove(float mx2, float my2) {
        int x = (int) getX();
        int y = (int) getY();
        if (mx2 > Animation.CurveTimeline.LINEAR) {
        }
        setPosition((float) ((int) (((float) x) + mx2)), (float) ((int) (((float) y) + my2)));
    }

    public void gDown(int keycode) {
        if (keycode == 29) {
            if (this.curStatus == 1) {
                setStatus(2);
                this.isLeft = true;
                this.isJumpMove = false;
            }
            if (this.curStatus == 8) {
                this.isLeft = true;
                this.isJumpMove = true;
                System.err.println("STATUS_JUMP_UP left");
            }
        }
        if (keycode == 32) {
            if (this.curStatus == 1) {
                setStatus(2);
                this.isLeft = false;
                this.isJumpMove = false;
            }
            if (this.curStatus == 8) {
                this.isLeft = false;
                this.isJumpMove = true;
                System.err.println("STATUS_JUMP_UP right");
            }
        }
        if (keycode == 38) {
            if (this.curStatus == 1 || this.curStatus == 2) {
                setStatus(4);
                this.isJumpMove = true;
            }
            if (this.curStatus == 1) {
                setStatus(4);
                this.isJumpMove = false;
            }
        }
    }

    public void gUp(int keycode) {
        setStatus(1);
        this.isJumpMove = false;
    }

    public void ctrl() {
        if (Gdx.input.isTouched()) {
            int tx2 = Gdx.input.getX();
            int y = Gdx.input.getY();
            if (tx2 < 160) {
                setStatus(2);
                this.isLeft = true;
            } else if (tx2 < 320) {
                setStatus(2);
                this.isLeft = false;
            }
        }
    }

    private void toStopStatus() {
        if (this.curStatus == 8) {
            setStatus(1);
            this.isJumpMove = false;
        }
    }

    public void injured(int attack, int rota, boolean _isBuff) {
    }
}
