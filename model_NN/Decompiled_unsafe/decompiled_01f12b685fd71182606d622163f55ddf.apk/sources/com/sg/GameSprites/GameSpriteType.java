package com.sg.GameSprites;

public interface GameSpriteType {
    public static final byte DIR_ATTACK = 5;
    public static final byte DIR_DOWN = 3;
    public static final byte DIR_LEFT = 0;
    public static final byte DIR_LEFT_DOWN = 2;
    public static final byte DIR_LEFT_UP = 0;
    public static final byte DIR_RIGHT = 1;
    public static final byte DIR_RIGHT_DOWN = 3;
    public static final byte DIR_RIGHT_UP = 1;
    public static final byte DIR_STOP = 4;
    public static final byte DIR_UP = 2;
    public static final byte STATUS_ANGRY = 101;
    public static final byte STATUS_APPEAR = 36;
    public static final byte STATUS_ATTACK = 4;
    public static final byte STATUS_ATTACK1 = 102;
    public static final byte STATUS_ATTACK_END = 11;
    public static final byte STATUS_BACK = 40;
    public static final byte STATUS_COOK = 55;
    public static final byte STATUS_CUSHION = 39;
    public static final byte STATUS_DAODI = 45;
    public static final byte STATUS_DAODI_END = 46;
    public static final byte STATUS_DEAD = 10;
    public static final byte STATUS_DECLINE = 15;
    public static final byte STATUS_DEFEND = 38;
    public static final byte STATUS_DEFEND_END = 37;
    public static final byte STATUS_DIE = 101;
    public static final byte STATUS_DISAPPEAR = 35;
    public static final byte STATUS_FAIL = 6;
    public static final byte STATUS_FLY = 53;
    public static final byte STATUS_FREEZE = 56;
    public static final byte STATUS_FUKONG = 43;
    public static final byte STATUS_FUKONG_END = 44;
    public static final byte STATUS_HAPPY = 3;
    public static final byte STATUS_HURT = 7;
    public static final byte STATUS_ICE = 58;
    public static final byte STATUS_ICE_END = 59;
    public static final byte STATUS_ICE_FOR_BUTTLE = 61;
    public static final byte STATUS_INJURE = 17;
    public static final byte STATUS_JUMP_ATTACK = 25;
    public static final byte STATUS_JUMP_DOWN = 9;
    public static final byte STATUS_JUMP_DOWN2 = 29;
    public static final byte STATUS_JUMP_UP = 8;
    public static final byte STATUS_JUMP_UP2 = 28;
    public static final byte STATUS_LEVEL_UP = 18;
    public static final byte STATUS_MENU_SHOW = 59;
    public static final byte STATUS_MOVE = 2;
    public static final byte STATUS_MOVESCREEN = 23;
    public static final byte STATUS_MOVE_ATTACK = 60;
    public static final byte STATUS_NULL = 0;
    public static final byte STATUS_PATROL = 100;
    public static final byte STATUS_READY_ATTACK = 24;
    public static final byte STATUS_RUN = 31;
    public static final byte STATUS_RUNTOPOINT = 30;
    public static final byte STATUS_RUN_ATTACK = 32;
    public static final byte STATUS_SKILL = 5;
    public static final byte STATUS_SKILL1 = 12;
    public static final byte STATUS_SKILL2 = 13;
    public static final byte STATUS_SKILL4 = 20;
    public static final byte STATUS_SKILL5 = 14;
    public static final byte STATUS_STOP = 1;
    public static final byte STATUS_TUI = 54;
    public static final byte STATUS_UP = 22;
    public static final byte STATUS_WAIT = 42;
    public static final byte STATUS_WIN = 51;
    public static final byte STATUS_WUDI = 45;
    public static final byte STATUS_YUN = 57;

    /* renamed from: TYPE_BOSS_坦克  reason: contains not printable characters */
    public static final byte f0TYPE_BOSS_ = 49;

    /* renamed from: TYPE_BOSS_蝎子  reason: contains not printable characters */
    public static final byte f1TYPE_BOSS_ = 48;

    /* renamed from: TYPE_BOSS_飞碟1  reason: contains not printable characters */
    public static final byte f2TYPE_BOSS_1 = 47;

    /* renamed from: TYPE_BOSS_飞碟2  reason: contains not printable characters */
    public static final byte f3TYPE_BOSS_2 = 50;

    /* renamed from: TYPE_ENEMY_乌鸦  reason: contains not printable characters */
    public static final byte f4TYPE_ENEMY_ = 64;

    /* renamed from: TYPE_ENEMY_僵尸小丑精英  reason: contains not printable characters */
    public static final byte f5TYPE_ENEMY_ = 56;

    /* renamed from: TYPE_ENEMY_僵尸狗精英  reason: contains not printable characters */
    public static final byte f6TYPE_ENEMY_ = 53;

    /* renamed from: TYPE_ENEMY_士兵僵尸  reason: contains not printable characters */
    public static final byte f7TYPE_ENEMY_ = 24;

    /* renamed from: TYPE_ENEMY_士兵僵尸1  reason: contains not printable characters */
    public static final byte f8TYPE_ENEMY_1 = 25;

    /* renamed from: TYPE_ENEMY_士兵僵尸2  reason: contains not printable characters */
    public static final byte f9TYPE_ENEMY_2 = 26;

    /* renamed from: TYPE_ENEMY_士兵僵尸精英  reason: contains not printable characters */
    public static final byte f10TYPE_ENEMY_ = 58;

    /* renamed from: TYPE_ENEMY_小丑  reason: contains not printable characters */
    public static final byte f11TYPE_ENEMY_ = 15;

    /* renamed from: TYPE_ENEMY_小丑1  reason: contains not printable characters */
    public static final byte f12TYPE_ENEMY_1 = 16;

    /* renamed from: TYPE_ENEMY_小丑2  reason: contains not printable characters */
    public static final byte f13TYPE_ENEMY_2 = 17;

    /* renamed from: TYPE_ENEMY_护士  reason: contains not printable characters */
    public static final byte f14TYPE_ENEMY_ = 12;

    /* renamed from: TYPE_ENEMY_护士1  reason: contains not printable characters */
    public static final byte f15TYPE_ENEMY_1 = 13;

    /* renamed from: TYPE_ENEMY_护士2  reason: contains not printable characters */
    public static final byte f16TYPE_ENEMY_2 = 14;

    /* renamed from: TYPE_ENEMY_护士僵尸精英  reason: contains not printable characters */
    public static final byte f17TYPE_ENEMY_ = 55;

    /* renamed from: TYPE_ENEMY_沼泽气泡  reason: contains not printable characters */
    public static final byte f18TYPE_ENEMY_ = 42;

    /* renamed from: TYPE_ENEMY_狗  reason: contains not printable characters */
    public static final byte f19TYPE_ENEMY_ = 9;

    /* renamed from: TYPE_ENEMY_狗1  reason: contains not printable characters */
    public static final byte f20TYPE_ENEMY_1 = 10;

    /* renamed from: TYPE_ENEMY_狗2  reason: contains not printable characters */
    public static final byte f21TYPE_ENEMY_2 = 11;

    /* renamed from: TYPE_ENEMY_白发僵尸  reason: contains not printable characters */
    public static final byte f22TYPE_ENEMY_ = 27;

    /* renamed from: TYPE_ENEMY_白发僵尸1  reason: contains not printable characters */
    public static final byte f23TYPE_ENEMY_1 = 28;

    /* renamed from: TYPE_ENEMY_白发僵尸2  reason: contains not printable characters */
    public static final byte f24TYPE_ENEMY_2 = 29;

    /* renamed from: TYPE_ENEMY_白发僵尸精英  reason: contains not printable characters */
    public static final byte f25TYPE_ENEMY_ = 59;

    /* renamed from: TYPE_ENEMY_白盔僵尸  reason: contains not printable characters */
    public static final byte f26TYPE_ENEMY_ = 39;

    /* renamed from: TYPE_ENEMY_白盔僵尸1  reason: contains not printable characters */
    public static final byte f27TYPE_ENEMY_1 = 40;

    /* renamed from: TYPE_ENEMY_白盔僵尸2  reason: contains not printable characters */
    public static final byte f28TYPE_ENEMY_2 = 41;

    /* renamed from: TYPE_ENEMY_白盔僵尸精英  reason: contains not printable characters */
    public static final byte f29TYPE_ENEMY_ = 63;

    /* renamed from: TYPE_ENEMY_瞬移僵尸  reason: contains not printable characters */
    public static final byte f30TYPE_ENEMY_ = 18;

    /* renamed from: TYPE_ENEMY_瞬移僵尸1  reason: contains not printable characters */
    public static final byte f31TYPE_ENEMY_1 = 19;

    /* renamed from: TYPE_ENEMY_瞬移僵尸2  reason: contains not printable characters */
    public static final byte f32TYPE_ENEMY_2 = 20;

    /* renamed from: TYPE_ENEMY_礼帽僵尸  reason: contains not printable characters */
    public static final byte f33TYPE_ENEMY_ = 21;

    /* renamed from: TYPE_ENEMY_礼帽僵尸1  reason: contains not printable characters */
    public static final byte f34TYPE_ENEMY_1 = 22;

    /* renamed from: TYPE_ENEMY_礼帽僵尸2  reason: contains not printable characters */
    public static final byte f35TYPE_ENEMY_2 = 23;

    /* renamed from: TYPE_ENEMY_礼帽僵尸精英  reason: contains not printable characters */
    public static final byte f36TYPE_ENEMY_ = 57;

    /* renamed from: TYPE_ENEMY_绿僵尸  reason: contains not printable characters */
    public static final byte f37TYPE_ENEMY_ = 0;

    /* renamed from: TYPE_ENEMY_绿僵尸_1  reason: contains not printable characters */
    public static final byte f38TYPE_ENEMY__1 = 1;

    /* renamed from: TYPE_ENEMY_绿僵尸_2  reason: contains not printable characters */
    public static final byte f39TYPE_ENEMY__2 = 2;

    /* renamed from: TYPE_ENEMY_绿僵尸精英  reason: contains not printable characters */
    public static final byte f40TYPE_ENEMY_ = 51;

    /* renamed from: TYPE_ENEMY_绿帽僵尸  reason: contains not printable characters */
    public static final byte f41TYPE_ENEMY_ = 33;

    /* renamed from: TYPE_ENEMY_绿帽僵尸1  reason: contains not printable characters */
    public static final byte f42TYPE_ENEMY_1 = 34;

    /* renamed from: TYPE_ENEMY_绿帽僵尸2  reason: contains not printable characters */
    public static final byte f43TYPE_ENEMY_2 = 35;

    /* renamed from: TYPE_ENEMY_绿帽僵尸精英  reason: contains not printable characters */
    public static final byte f44TYPE_ENEMY_ = 61;

    /* renamed from: TYPE_ENEMY_绿盔僵尸  reason: contains not printable characters */
    public static final byte f45TYPE_ENEMY_ = 36;

    /* renamed from: TYPE_ENEMY_绿盔僵尸1  reason: contains not printable characters */
    public static final byte f46TYPE_ENEMY_1 = 37;

    /* renamed from: TYPE_ENEMY_绿盔僵尸2  reason: contains not printable characters */
    public static final byte f47TYPE_ENEMY_2 = 38;

    /* renamed from: TYPE_ENEMY_绿盔僵尸精英  reason: contains not printable characters */
    public static final byte f48TYPE_ENEMY_ = 62;

    /* renamed from: TYPE_ENEMY_胖僵尸精英  reason: contains not printable characters */
    public static final byte f49TYPE_ENEMY_ = 54;

    /* renamed from: TYPE_ENEMY_胖子  reason: contains not printable characters */
    public static final byte f50TYPE_ENEMY_ = 6;

    /* renamed from: TYPE_ENEMY_胖子1  reason: contains not printable characters */
    public static final byte f51TYPE_ENEMY_1 = 7;

    /* renamed from: TYPE_ENEMY_胖子2  reason: contains not printable characters */
    public static final byte f52TYPE_ENEMY_2 = 8;

    /* renamed from: TYPE_ENEMY_蓝僵尸  reason: contains not printable characters */
    public static final byte f53TYPE_ENEMY_ = 3;

    /* renamed from: TYPE_ENEMY_蓝僵尸1  reason: contains not printable characters */
    public static final byte f54TYPE_ENEMY_1 = 4;

    /* renamed from: TYPE_ENEMY_蓝僵尸2  reason: contains not printable characters */
    public static final byte f55TYPE_ENEMY_2 = 5;

    /* renamed from: TYPE_ENEMY_蓝僵尸精英  reason: contains not printable characters */
    public static final byte f56TYPE_ENEMY_ = 52;

    /* renamed from: TYPE_ENEMY_蓝盔僵尸  reason: contains not printable characters */
    public static final byte f57TYPE_ENEMY_ = 30;

    /* renamed from: TYPE_ENEMY_蓝盔僵尸1  reason: contains not printable characters */
    public static final byte f58TYPE_ENEMY_1 = 31;

    /* renamed from: TYPE_ENEMY_蓝盔僵尸2  reason: contains not printable characters */
    public static final byte f59TYPE_ENEMY_2 = 32;

    /* renamed from: TYPE_ENEMY_蓝盔僵尸精英  reason: contains not printable characters */
    public static final byte f60TYPE_ENEMY_ = 60;

    /* renamed from: TYPE_ENEMY_飞船  reason: contains not printable characters */
    public static final byte f61TYPE_ENEMY_ = 43;

    /* renamed from: TYPE_ENEMY_飞行宝箱  reason: contains not printable characters */
    public static final byte f62TYPE_ENEMY_ = 44;

    /* renamed from: TYPE_ENEMY_飞行金币  reason: contains not printable characters */
    public static final byte f63TYPE_ENEMY_ = 45;

    /* renamed from: TYPE_ENEMY_飞行钻石  reason: contains not printable characters */
    public static final byte f64TYPE_ENEMY_ = 46;
    public static final byte TYPE_PAOTAI_0 = 0;
    public static final byte TYPE_PAOTAI_1 = 1;
    public static final byte TYPE_PAOTAI_10 = 10;
    public static final byte TYPE_PAOTAI_11 = 11;
    public static final byte TYPE_PAOTAI_12 = 12;
    public static final byte TYPE_PAOTAI_13 = 13;
    public static final byte TYPE_PAOTAI_14 = 14;
    public static final byte TYPE_PAOTAI_15 = 15;
    public static final byte TYPE_PAOTAI_16 = 16;
    public static final byte TYPE_PAOTAI_17 = 17;
    public static final byte TYPE_PAOTAI_18 = 18;
    public static final byte TYPE_PAOTAI_19 = 19;
    public static final byte TYPE_PAOTAI_2 = 2;
    public static final byte TYPE_PAOTAI_20 = 20;
    public static final byte TYPE_PAOTAI_21 = 21;
    public static final byte TYPE_PAOTAI_22 = 22;
    public static final byte TYPE_PAOTAI_23 = 23;
    public static final byte TYPE_PAOTAI_3 = 3;
    public static final byte TYPE_PAOTAI_4 = 4;
    public static final byte TYPE_PAOTAI_5 = 5;
    public static final byte TYPE_PAOTAI_6 = 6;
    public static final byte TYPE_PAOTAI_7 = 7;
    public static final byte TYPE_PAOTAI_8 = 8;
    public static final byte TYPE_PAOTAI_9 = 9;
    public static final byte TYPE_ROLE = 1;
}
