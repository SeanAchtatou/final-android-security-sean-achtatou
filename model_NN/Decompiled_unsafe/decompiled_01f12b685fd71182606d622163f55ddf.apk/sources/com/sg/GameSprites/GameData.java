package com.sg.GameSprites;

import com.datalab.tools.Constant;

public interface GameData extends GameSpriteType {
    public static final String[][] motion_Cloud = {new String[]{"1", "1", "lock"}, new String[]{Constant.S_C, "1", "unlock"}};
    public static final String[][] motion_Cover = {new String[]{"1", "1", "animation2"}, new String[]{Constant.S_C, "1", "animation1"}};
    public static final String[][] motion_RWkaweili = {new String[]{"1", "1", "animation"}};
    public static final String[][] motion_Unlock = {new String[]{"1", "1", "jiesuo"}};
    public static final String[][] motion_bisha = {new String[]{Constant.S_E, Constant.S_E, "animation"}};
    public static final String[][] motion_boss = {new String[]{"1", "1", "daiji"}, new String[]{Constant.S_E, Constant.S_E, "fangguai"}, new String[]{Constant.S_D, Constant.S_D, "gaoxing"}, new String[]{"101", "101", "shengqi"}};
    public static final String[][] motion_leiguman = {new String[]{"1", "1", "ZD_daiji"}, new String[]{Constant.S_C, Constant.S_C, "ZD_gongji"}, new String[]{Constant.S_E, "1", "ZD_gongji"}, new String[]{Constant.S_F, "1", "ZD_skill"}, new String[]{"6", "6", "UI_shibai"}};
    public static final String[][] motion_maplayer1 = {new String[]{"1", "1", "animation"}};
    public static final String[][] motion_maplayer2 = {new String[]{"1", "1", "animation"}};
    public static final String[][] motion_role = {new String[]{"1", "1", "daiji"}, new String[]{Constant.S_C, Constant.S_C, "paobu"}, new String[]{Constant.S_E, "1", "attack1"}};
}
