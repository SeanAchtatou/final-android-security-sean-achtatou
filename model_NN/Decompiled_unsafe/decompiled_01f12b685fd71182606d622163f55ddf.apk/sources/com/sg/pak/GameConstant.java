package com.sg.pak;

import com.sg.GameSprites.GameData;
import com.sg.GameSprites.GameSpriteType;

public interface GameConstant extends GameData, GameSpriteType, GameState, PAK_ASSETS {
    public static final String ANIMA_ATTACK = "_attack";
    public static final String ANIMA_ATTACK1 = "_attack1";
    public static final String ANIMA_STOP = "_stand";
    public static final String ANIMA_STOP1 = "_stand1";
    public static final int ANTHOR_BOTTOM_LEFT = 10;
    public static final int ANTHOR_BOTTOM_RIGHT = 18;
    public static final int ANTHOR_HCENTER_BOTTOM = 3;
    public static final int ANTHOR_HCENTER_TOP = 5;
    public static final int ANTHOR_HCENTER_VCENTER = 1;
    public static final int ANTHOR_TOP_LEFT = 12;
    public static final int ANTHOR_TOP_RIGHT = 20;
    public static final int ANTHOR_VCENTER_LEFT = 9;
    public static final int ANTHOR_VCENTER_RIGHT = 17;
    public static final byte AT_BEAR2 = 11;
    public static final byte AT_BEAR3 = 12;
    public static final byte AT_BEAR3_BISHA = 15;
    public static final byte AT_BEAR4 = 16;
    public static final byte AT_BEAR4_BISHA = 17;
    public static final byte AT_BOSS = 2;
    public static final byte AT_DECK = 1;
    public static final byte AT_ENEMY = 0;
    public static final byte AT_HUIXUANBIAO = 8;
    public static final byte AT_HUOCHAI = 1;
    public static final byte AT_HUOGUO = 3;
    public static final byte AT_LENGDONG = 10;
    public static final byte AT_LENGGUANG = 4;
    public static final byte AT_MUCHUI = 9;
    public static final byte AT_PENSHEQI = 13;
    public static final byte AT_PINGGUOZHADAN = 2;
    public static final byte AT_YANHUA = 5;
    public static final byte AT_YUSAN = 3;
    public static final byte AT_ZUQIU = 14;
    public static final byte BEAR_FULL_LEVEL = 10;
    public static final byte BUFFDATA_INTERVAL = 2;
    public static final byte BUFFDATA_TIME = 1;
    public static final byte BUFFDATA_TYPE = 3;
    public static final byte BUFFDATA_VALUE = 0;
    public static final byte BUFF_FIRE = 3;
    public static final byte BUFF_HOLD = 4;
    public static final byte BUFF_KWL = 5;
    public static final byte BUFF_SLOW = 1;
    public static final byte BUFF_STOP = 2;
    public static final byte BUFF_SUOLI = 6;
    public static final byte COMPLETE = 1;
    public static final byte COUNT_EVERYDAY_GET = 7;
    public static final byte DIR_DOWN = 2;
    public static final byte DIR_LEFT = 3;
    public static final int DIR_LEFT_DOWN = 5;
    public static final int DIR_LEFT_UP = 7;
    public static final byte DIR_RIGHT = 1;
    public static final int DIR_RIGHT_DOWN = 4;
    public static final int DIR_RIGHT_UP = 6;
    public static final byte DIR_UP = 0;
    public static final String EVERYDAYGET1 = "斗龙手环  +1";
    public static final byte FM_CHANNELING = 3;
    public static final byte FM_DEGREES = 1;
    public static final byte FM_DEGREESCHANNELING = 4;
    public static final byte FM_FLASH = 0;
    public static final byte FM_TRANS = 2;
    public static final byte FT_DECK = 2;
    public static final byte FT_ENEMY = 1;
    public static final byte FT_NULL = 0;
    public static final byte JIDI_FULL_LEVEL = 11;
    public static final int LAYER0_BOTTOM = 0;
    public static final int LAYER1_MAP = 1;
    public static final int LAYER2_BULLET = 6;
    public static final int LAYER2_SPRITE = 2;
    public static final int LAYER3_UI = 3;
    public static final int LAYER4_TOP = 4;
    public static final int LAYER5_MAX = 5;
    public static final int LAYER7_ANIM = 7;
    public static final String LEVEL = "Level";
    public static final int LEVEL_MAX = 3;
    public static final byte LIFE_AUTO_MAX = 5;
    public static final int LIFE_AUTO_TIME = 300000;
    public static final byte MODE_EASY = 0;
    public static final byte MODE_HARD = 1;
    public static final byte NOT_COMPLETE = 0;
    public static final byte NUMBER_CENTER = 1;
    public static final byte NUMBER_LEFT = 0;
    public static final byte NUMBER_RIGHT = 2;
    public static final byte NUMBER_STYLE_0 = 0;
    public static final byte NUMBER_STYLE_1 = 1;
    public static final byte NUMBER_STYLE_10_rank = 10;
    public static final byte NUMBER_STYLE_11_rolek = 11;
    public static final byte NUMBER_STYLE_12_role = 12;
    public static final byte NUMBER_STYLE_13_ctask = 13;
    public static final byte NUMBER_STYLE_14_cgold = 14;
    public static final byte NUMBER_STYLE_15_GiftNum = 15;
    public static final byte NUMBER_STYLE_2_task = 2;
    public static final byte NUMBER_STYLE_3_achie = 3;
    public static final byte NUMBER_STYLE_4_achi = 4;
    public static final byte NUMBER_STYLE_5_batt = 5;
    public static final byte NUMBER_STYLE_6_endle = 6;
    public static final byte NUMBER_STYLE_7_endl = 7;
    public static final byte NUMBER_STYLE_8_main = 8;
    public static final byte NUMBER_STYLE_9_ranki = 9;
    public static final byte NUM_MINUTE = 1;
    public static final byte NUM_PERCENT = 3;
    public static final byte NUM_RANKS = 54;
    public static final byte NUM_SECOND = 2;
    public static final byte NUM_STAR = 0;
    public static final byte NUM_TOWERS = 12;
    public static final float PER_FRAME = 60.0f;
    public static final int POS_X = 0;
    public static final int POS_Y = 1;
    public static final int PROP_BLANK = 2;
    public static final int PROP_GLASSES = 1;
    public static final int PROP_SHOSE = 3;
    public static final int PROP_WATCH = 0;
    public static final int ROLE_MOVE_MONEY = 50;
    public static final String[] ROLE_NAME = {"Bear1", "Bear2", "Bear3", "Bear4"};
    public static final int SCREEN_HEIGHT = 1136;
    public static final int SCREEN_WIDTH = 640;
    public static final String SKILL1_EFFECT = "game_hit_penhuo.px";
    public static final String SKILL2_EFFECT = "game_bisha_KJkaweili02.px";
    public static final int SKILL_DAMAGE = 1500;
    public static final byte SKILL_ENEMY = 2;
    public static final byte SKILL_TOWER = 1;
    public static final int ST_ATTACK = 3;
    public static final byte ST_CHALLENGING = 5;
    public static final int ST_DEAD = 1;
    public static final int ST_FLY = 4;
    public static final int ST_LIVE = 6;
    public static final int ST_MOVE = 0;
    public static final int ST_SKILL = 7;
    public static final int ST_STOP = 2;
    public static final String SUFFIX = "_shuoming.png";
    public static final byte SYS_BUY_HONEY = 15;
    public static final byte SYS_BUY_REPLAY = 16;
    public static final byte SYS_FAIL = 5;
    public static final byte SYS_HERO_KILL = 10;
    public static final byte SYS_HOME_UP = 17;
    public static final byte SYS_MIDMENU = 9;
    public static final byte SYS_NULL = 0;
    public static final byte SYS_READY = 2;
    public static final byte SYS_RELIVE = 4;
    public static final byte SYS_RETRY = 8;
    public static final byte SYS_ROLE_UP = 12;
    public static final byte SYS_SHOW = 7;
    public static final byte SYS_SHOW_STAR_FAIL = 18;
    public static final byte SYS_SHOW_STAR_WIN = 11;
    public static final byte SYS_TALK = 14;
    public static final byte SYS_TEACH = 13;
    public static final byte SYS_WAVE = 3;
    public static final byte SYS_WIN = 6;
    public static final byte TAKEN = 2;
    public static final byte TOWER_FULL_LEVEL = 18;
    public static final byte TYPE_ENDLESS = 3;
    public static final byte TYPE_NORMAL = 1;
    public static final int fps = 100;
    public static final String[] homeState = {"home_1", "home_2", "home_3", "home_4"};
    public static final boolean isShowFps = false;
    public static final boolean isShowRect = false;
    public static final boolean isShowRender = false;

    /* renamed from: is_抗锯齿  reason: contains not printable characters */
    public static final boolean f86is_ = true;
    public static final int[][] numImage = {new int[]{16, 22, -4, PAK_ASSETS.IMG_JISHI}, new int[]{23, 31, -6, PAK_ASSETS.IMG_JISHI}, new int[]{12, 17, 0, PAK_ASSETS.IMG_JISHI}, new int[]{12, 16, 0, PAK_ASSETS.IMG_JISHI}, new int[]{14, 19, 0, PAK_ASSETS.IMG_JISHI}, new int[]{13, 20, -2, PAK_ASSETS.IMG_JISHI}, new int[]{22, 36, 0, PAK_ASSETS.IMG_JISHI}, new int[]{16, 24, 0, PAK_ASSETS.IMG_JISHI}, new int[]{12, 19, -2, PAK_ASSETS.IMG_JISHI}, new int[]{19, 24, 0, PAK_ASSETS.IMG_JISHI}, new int[]{29, 29, 0, PAK_ASSETS.IMG_JISHI}, new int[]{11, 18, 0, PAK_ASSETS.IMG_JISHI}, new int[]{22, 24, 0, PAK_ASSETS.IMG_JISHI}};
}
