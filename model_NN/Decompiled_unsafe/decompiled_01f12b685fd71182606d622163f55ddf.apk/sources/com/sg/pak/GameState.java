package com.sg.pak;

public interface GameState {
    public static final int ST_GAMENEWMAP = 13;
    public static final int ST_KAIJI = 16;
    public static final int ST_LOAD = 0;
    public static final int ST_LOGO = -1;
    public static final int ST_MAP = 6;
    public static final int ST_MAPCHOOSE = 7;
    public static final int ST_MAPCHOOSERANK = 8;
    public static final int ST_MENU = 5;
    public static final int ST_OPEN = 1;
    public static final int ST_PERSONAL = 14;
    public static final int ST_PLAY = 2;
    public static final int ST_RANKCHOOSE1 = 9;
    public static final int ST_RANKCHOOSE2 = 10;
    public static final int ST_RANKCHOOSE3 = 11;
    public static final int ST_RANKCHOOSE4 = 12;
    public static final int ST_SHOP = 4;
    public static final int ST_SM = -3;
    public static final int ST_SP = -2;
    public static final int ST_STARTLOAD = 3;
    public static final int ST_SUPERGAME = 15;
}
