package com.sg.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.sg.td.MainActivity;

public class NotifiManager {
    private Context context;

    public NotifiManager(Context context2, String strShow, String strTitle, String strContent, Long time) {
        this.context = context2;
        System.out.println(context2);
        NotificationManager nm = (NotificationManager) context2.getSystemService("notification");
        System.out.println(nm);
        Notification nofi = new Notification(17301659, strShow, time.longValue());
        nofi.flags = 16;
        Intent intent = new Intent(context2, MainActivity.class);
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setFlags(270532608);
        nofi.setLatestEventInfo(context2, strTitle, strContent, PendingIntent.getActivity(context2, 0, intent, 0));
        nm.notify(0, nofi);
    }
}
