package com.sg.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoBootBroadcast extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.i("zl", "开机自启");
        context.startService(new Intent(context, BgService.class));
    }
}
