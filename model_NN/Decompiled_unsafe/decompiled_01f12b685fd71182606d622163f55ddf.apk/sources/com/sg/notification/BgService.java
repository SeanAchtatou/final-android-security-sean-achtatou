package com.sg.notification;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.sg.notification.TimerManager;
import java.util.Calendar;

public class BgService extends Service {
    private static TimerManager tm;
    private Thread thread;
    private TimerManager tm2;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Log.i("zl", "启动bgservice");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (tm == null) {
            Calendar instance = Calendar.getInstance();
            tm = new TimerManager(18, 0, 0);
            tm.setListener(new TimerManager.TimerListener() {
                public void toDO() {
                    new NotifiManager(BgService.this, "斗龙战士3之天降小怪兽", "斗龙战士3之天降小怪兽", "葫芦兄弟送您金刚神力，助你斩妖除魔", Long.valueOf(System.currentTimeMillis()));
                }
            });
            tm.begin();
        }
        if (this.tm2 == null) {
            Calendar instance2 = Calendar.getInstance();
            this.tm2 = new TimerManager(12, 0, 0);
            this.tm2.setListener(new TimerManager.TimerListener() {
                public void toDO() {
                    new NotifiManager(BgService.this, "斗龙战士3之天降小怪兽", "斗龙战士3之天降小怪兽", "葫芦兄弟送您金刚神力，助你斩妖除魔", Long.valueOf(System.currentTimeMillis()));
                }
            });
            this.tm2.begin();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void onLowMemory() {
        super.onLowMemory();
        System.out.println("onLowMemory");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("zl", "serv destory");
        Intent localIntent = new Intent();
        localIntent.setClass(this, BgService.class);
        startService(localIntent);
    }
}
