package com.sg.notification;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerManager {
    private static final int PERIOD_DAY = 86400000;
    /* access modifiers changed from: private */
    public static TimerListener tl;
    /* access modifiers changed from: private */
    public Calendar calendarNow = Calendar.getInstance();
    private Calendar calendarOrder = ((Calendar) this.calendarNow.clone());
    private Date date;
    private TimerTask task = new TimerTask() {
        public void run() {
            System.out.println("calendarNow:" + TimerManager.this.calendarNow.getTime());
            TimerManager.tl.toDO();
        }
    };
    private Timer timer = new Timer();

    public interface TimerListener {
        void toDO();
    }

    public TimerManager(int hour, int minute, int second) {
        this.calendarOrder.set(this.calendarNow.get(1), this.calendarNow.get(2), this.calendarNow.get(5), hour, minute, second);
        if (this.calendarNow.after(this.calendarOrder)) {
            this.calendarOrder.add(5, 1);
        }
        this.date = this.calendarOrder.getTime();
        System.out.println("calendarNow:" + this.calendarNow.getTime());
        System.out.println("calendarOrder:" + this.calendarOrder.getTime());
        System.out.println("time:" + this.date);
    }

    public void begin() {
        System.out.println("begin schedule");
        this.timer.schedule(this.task, this.date, 86400000);
    }

    public void setListener(TimerListener tl2) {
        tl = tl2;
    }
}
