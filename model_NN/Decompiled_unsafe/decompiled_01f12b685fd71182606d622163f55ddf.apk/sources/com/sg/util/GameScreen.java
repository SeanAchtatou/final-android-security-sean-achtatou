package com.sg.util;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.tools.GameTime;
import com.sg.tools.OnlineTime;

public abstract class GameScreen implements Screen {
    public static boolean canInput = true;
    public static boolean isDebug = true;
    public Group curStateGroup = new Group();

    public abstract void dispose();

    public abstract void init();

    public abstract void run(float f);

    public void render(float delta) {
        GameTime.delta = delta;
        if (!Rank.isPause()) {
            GameTime.reMainTimeRun();
            GameTime.Run();
        }
        run(delta);
        OnlineTime.Run(delta);
        RankData.runOnline();
        RankData.runPower();
        GameStage.render(delta);
    }

    public void show() {
        GameStage.clearListeners();
        initGestureProcessor();
        initInputProcessor();
        init();
    }

    public void hide() {
        dispose();
    }

    public void pause() {
        System.out.println("GameScreen pause()...");
        RankData.quiteSave();
    }

    public void resume() {
        System.out.println("GameScreen resume()...");
    }

    public void resize(int width, int height) {
    }

    /* access modifiers changed from: protected */
    public void initScreenTouch() {
        initGestureProcessor();
        initInputProcessor();
    }

    /* access modifiers changed from: protected */
    public void initGestureProcessor() {
        GameStage.getStage().addCaptureListener(new ActorGestureListener() {
            public void fling(InputEvent event, float velocityX, float velocityY, int button) {
                GameScreen.this.gFling(velocityX, velocityY, button);
            }

            public boolean longPress(Actor actor, float x, float y) {
                return GameScreen.this.gLongPress(x, y);
            }

            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                GameScreen.this.gPan(x, y, deltaX, deltaY);
            }

            public void pinch(InputEvent event, Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                GameScreen.this.gPinch(initialPointer1, initialPointer2, pointer1, pointer2);
            }

            public void tap(InputEvent event, float x, float y, int count, int button) {
                GameScreen.this.gTap(x, y, count, button);
            }

            public void zoom(InputEvent event, float initialDistance, float distance) {
                GameScreen.this.gZoom(initialDistance, distance);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initInputProcessor() {
        GameStage.getStage().addListener(new InputListener() {
            public boolean keyDown(InputEvent event, int keycode) {
                if (!GameScreen.canInput) {
                    return false;
                }
                return GameScreen.this.gKeyDown(keycode);
            }

            public boolean keyTyped(InputEvent event, char character) {
                if (!GameScreen.canInput) {
                    return false;
                }
                return GameScreen.this.gKeyTyped(character);
            }

            public boolean keyUp(InputEvent event, int keycode) {
                if (!GameScreen.canInput) {
                    return false;
                }
                return GameScreen.this.gKeyUp(keycode);
            }

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return GameScreen.this.gTouchDown((int) x, (int) y, pointer);
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                GameScreen.this.gTouchDragged((int) x, (int) y, pointer);
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                GameScreen.this.gTouchUp((int) x, (int) y, pointer, button);
            }
        });
    }

    public boolean gKeyDown(int keycode) {
        return false;
    }

    public boolean gKeyUp(int keycode) {
        return false;
    }

    public boolean gKeyTyped(char character) {
        return false;
    }

    public boolean gTouchDown(int screenX, int screenY, int pointer) {
        return true;
    }

    public boolean gTouchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    public boolean gTouchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    public boolean gFling(float velocityX, float velocityY, int button) {
        return false;
    }

    public boolean gLongPress(float x, float y) {
        return false;
    }

    public boolean gPan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    public boolean gPinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    public boolean gTap(float x, float y, int count, int button) {
        return false;
    }

    public boolean gZoom(float initialDistance, float distance) {
        return false;
    }

    public static void debugSystem(String s) {
        if (isDebug) {
            System.out.println(s);
        }
    }
}
