package com.sg.util;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorText;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;

public class GameHirt implements GameConstant {
    /* access modifiers changed from: private */
    public static float alpha;

    public static void hint(String hintContent, int y) {
        alpha = 1.0f;
        final Group hitGroup = new Group();
        new ActorText(hintContent, ((int) GameMain.sceenWidth) / 2, y, 1, hitGroup);
        GameStage.addActor(hitGroup, GameLayer.max);
        hitGroup.addAction(Actions.sequence(Actions.delay(3.0f), Actions.repeat(11, Actions.sequence(Actions.run(new Runnable() {
            public void run() {
                if (GameHirt.alpha >= 1.0f) {
                    float unused = GameHirt.alpha = 1.0f;
                }
                hitGroup.setColor(1.0f, 1.0f, 1.0f, GameHirt.alpha);
                float unused2 = GameHirt.alpha = (float) (((double) GameHirt.alpha) - 0.1d);
            }
        }), Actions.delay(0.04f))), Actions.run(new Runnable() {
            public void run() {
                float unused = GameHirt.alpha = 1.0f;
                hitGroup.remove();
                hitGroup.clear();
            }
        })));
        hitGroup.setTouchable(Touchable.disabled);
    }
}
