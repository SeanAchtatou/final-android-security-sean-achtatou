package com.sg.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.sg.pak.GameConstant;
import com.sg.tools.GameLayerGroup;
import java.util.Comparator;

public enum GameLayer implements GameConstant {
    bottom("bottom", Touchable.enabled, false),
    map("map", Touchable.enabled, false),
    sprite("sprite", Touchable.enabled, false),
    effect("effect", Touchable.enabled, false),
    anim("anim", Touchable.enabled, false),
    ui("ui", Touchable.enabled, false),
    top("top", Touchable.enabled, false),
    max("max", Touchable.enabled, false);
    
    private Comparator comparator;
    private GameLayerGroup group;
    private boolean isSort;
    private String name;
    private Touchable touchable;

    public static GameLayer getLay(int index) {
        switch (index) {
            case 0:
                return bottom;
            case 1:
                return map;
            case 2:
                return sprite;
            case 3:
                return ui;
            case 4:
                return top;
            case 5:
                return max;
            case 6:
                return effect;
            case 7:
                return anim;
            default:
                System.out.println("GameLayer getLay() 错误");
                return null;
        }
    }

    public static void setLayerIsSort(GameLayer lay, boolean is) {
        System.out.println("设置" + lay.getName() + "图层排序" + is);
        lay.setIsSort(is);
        lay.comparator = new Comparator<Actor>() {
            public int compare(Actor o1, Actor o2) {
                if (o1.getLayer() < o2.getLayer()) {
                    return -1;
                }
                if (o1.getLayer() > o2.getLayer()) {
                    return 1;
                }
                return 0;
            }
        };
    }

    private void createComparator() {
        Comparator comparator2 = new Comparator<Actor>() {
            public int compare(Actor o1, Actor o2) {
                if (o1.getLayer() < o2.getLayer()) {
                    return -1;
                }
                if (o1.getLayer() > o2.getLayer()) {
                    return 1;
                }
                return 0;
            }
        };
        switch (this) {
            case bottom:
            case map:
            case top:
            case ui:
            case sprite:
                if (getIsSort()) {
                    this.comparator = comparator2;
                    return;
                }
                return;
            case max:
                return;
            default:
                return;
        }
    }

    private void setIsSort(boolean is) {
        this.isSort = is;
    }

    private boolean getIsSort() {
        return this.isSort;
    }

    private GameLayer(String name2, Touchable touchable2, boolean isSrot) {
        this.name = name2;
        this.touchable = touchable2;
        this.isSort = isSrot;
    }

    public void init(GameLayerGroup group2) {
        this.group = group2;
        group2.setName(this.name);
        group2.setTouchable(this.touchable);
        group2.setTransform(false);
        createComparator();
    }

    public Touchable getTouchable() {
        return this.touchable;
    }

    public String getName() {
        return this.name;
    }

    public GameLayerGroup getGroup() {
        return this.group;
    }

    public Comparator getComparator() {
        return this.comparator;
    }
}
