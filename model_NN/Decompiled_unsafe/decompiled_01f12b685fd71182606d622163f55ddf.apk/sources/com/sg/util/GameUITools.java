package com.sg.util;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.esotericsoftware.spine.Animation;

public class GameUITools {
    public static boolean isDragged;
    /* access modifiers changed from: private */
    public static float mapChoiceX;
    /* access modifiers changed from: private */
    public static float mapOX;
    /* access modifiers changed from: private */
    public static float mapX;

    public static InputListener getMoveListener(Group group, float width, float offWidth, float power, boolean isX) {
        final float f = width;
        final float f2 = offWidth;
        final boolean z = isX;
        final Group group2 = group;
        final float f3 = power;
        return new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (f < f2) {
                    return false;
                }
                if (!z) {
                    x = y;
                }
                float unused = GameUITools.mapX = x;
                float unused2 = GameUITools.mapChoiceX = z ? group2.getX() : group2.getY();
                return true;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
                if ((r3 ? r4.getX() : r4.getY() - com.sg.util.GameUITools.access$100()) < -15.0f) goto L_0x0021;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent r5, float r6, float r7, int r8) {
                /*
                    r4 = this;
                    r3 = 0
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x004e
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x000b:
                    r1 = 1097859072(0x41700000, float:15.0)
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 > 0) goto L_0x0021
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x005a
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x001b:
                    r1 = -1049624576(0xffffffffc1700000, float:-15.0)
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 >= 0) goto L_0x0024
                L_0x0021:
                    r0 = 1
                    com.sg.util.GameUITools.isDragged = r0
                L_0x0024:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x0066
                L_0x0028:
                    float r0 = com.sg.util.GameUITools.mapX
                    float r0 = r6 - r0
                    float unused = com.sg.util.GameUITools.mapOX = r0
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x0068
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x003b:
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 != 0) goto L_0x006f
                    float r0 = com.sg.util.GameUITools.mapOX
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 >= 0) goto L_0x006f
                L_0x004d:
                    return
                L_0x004e:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    float r1 = com.sg.util.GameUITools.mapChoiceX
                    float r0 = r0 - r1
                    goto L_0x000b
                L_0x005a:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    float r1 = com.sg.util.GameUITools.mapChoiceX
                    float r0 = r0 - r1
                    goto L_0x001b
                L_0x0066:
                    r6 = r7
                    goto L_0x0028
                L_0x0068:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x003b
                L_0x006f:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00ae
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x0079:
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 != 0) goto L_0x0085
                    float r0 = com.sg.util.GameUITools.mapOX
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 > 0) goto L_0x004d
                L_0x0085:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00b5
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x008f:
                    float r1 = com.sg.util.GameUITools.mapOX
                    float r0 = r0 + r1
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 >= 0) goto L_0x00c8
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00bc
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    r0.setPosition(r1, r3)
                    goto L_0x004d
                L_0x00ae:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x0079
                L_0x00b5:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x008f
                L_0x00bc:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    r0.setPosition(r3, r1)
                    goto L_0x004d
                L_0x00c8:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00e2
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x00d2:
                    float r1 = com.sg.util.GameUITools.mapOX
                    float r0 = r0 + r1
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 <= 0) goto L_0x00e9
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    r0.setPosition(r3, r3)
                    goto L_0x004d
                L_0x00e2:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x00d2
                L_0x00e9:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00ff
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                    float r2 = com.sg.util.GameUITools.mapOX
                    float r1 = r1 + r2
                    r0.setPosition(r1, r3)
                    goto L_0x004d
                L_0x00ff:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    float r2 = com.sg.util.GameUITools.mapOX
                    float r1 = r1 + r2
                    r0.setPosition(r3, r1)
                    goto L_0x004d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.sg.util.GameUITools.AnonymousClass1.touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent, float, float, int):void");
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Action move1;
                GameUITools.isDragged = false;
                if ((z ? group2.getX() : group2.getY()) != (-(f - f2)) || GameUITools.mapOX >= Animation.CurveTimeline.LINEAR) {
                    if ((z ? group2.getX() : group2.getY()) != Animation.CurveTimeline.LINEAR || GameUITools.mapOX <= Animation.CurveTimeline.LINEAR) {
                        if ((z ? group2.getX() : group2.getY()) >= (-(f - f2))) {
                            if ((z ? group2.getX() : group2.getY()) > Animation.CurveTimeline.LINEAR) {
                                group2.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                            } else {
                                if (z) {
                                    group2.setPosition(group2.getX() + GameUITools.mapOX, Animation.CurveTimeline.LINEAR);
                                } else {
                                    group2.setPosition(Animation.CurveTimeline.LINEAR, group2.getY() + GameUITools.mapOX);
                                }
                                if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) < Animation.CurveTimeline.LINEAR) {
                                    if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) > (-(f - f2))) {
                                        if (z) {
                                            move1 = Actions.moveBy(GameUITools.mapOX * f3, Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                        } else {
                                            move1 = Actions.moveBy(Animation.CurveTimeline.LINEAR, GameUITools.mapOX * f3, 0.5f, Interpolation.pow5Out);
                                        }
                                        group2.addAction(move1);
                                    }
                                }
                                if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) >= Animation.CurveTimeline.LINEAR) {
                                    move1 = Actions.moveTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                } else if (z) {
                                    move1 = Actions.moveTo(-(f - f2), Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                } else {
                                    move1 = Actions.moveTo(Animation.CurveTimeline.LINEAR, -(f - f2), 0.5f, Interpolation.pow5Out);
                                }
                                group2.addAction(move1);
                            }
                        } else if (z) {
                            group2.setPosition(-(f - f2), Animation.CurveTimeline.LINEAR);
                        } else {
                            group2.setPosition(Animation.CurveTimeline.LINEAR, -(f - f2));
                        }
                        float unused = GameUITools.mapX = Animation.CurveTimeline.LINEAR;
                        float unused2 = GameUITools.mapOX = Animation.CurveTimeline.LINEAR;
                    }
                }
            }
        };
    }
}
