package com.sg.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.Sort;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kbz.Actors.ActorNum;
import com.kbz.Actors.ActorText;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.tools.GameLayerGroup;
import java.util.Comparator;
import java.util.Vector;

public final class GameStage implements GameConstant {
    private static Color clearColor = Color.BLACK;
    static ActorText debugStr;
    private static float deltaTime;
    private static BitmapFont font;
    private static int fps;
    static ActorNum fpsActorNum;
    private static double gameTime;
    static boolean isFirst = true;
    private static long javaHeap;
    private static long lastIndex = 0;
    private static int maxSpritesInBatch;
    private static long nativeHeap;
    private static boolean pause = false;
    private static int renderCalls;
    private static float sleepTime = 0.05f;
    private static Stage stage;
    private static int stageActorcount;
    private static int totalRenderCalls;
    private static ObjectMap<String, GUpdateService> updateServices = new ObjectMap<>();

    public interface GStageBorder {
        void drawHorizontalBorder(SpriteBatch spriteBatch, float f, float f2);

        void drawVerticalBorder(SpriteBatch spriteBatch, float f, float f2);
    }

    public interface GUpdateService {
        boolean update(float f);
    }

    public static void addActor(Actor actor, int layindex, GameLayer lay) {
        addActorToMyStage(lay, actor);
    }

    public static void addActor(Actor actor, int layindex, int bigLayerIndex) {
        addActorToMyStage(GameLayer.getLay(bigLayerIndex), actor);
    }

    public static void addActor(Actor actor, GameLayer lay) {
        addActorToMyStage(lay, actor);
    }

    public static void addActor(Actor actor, int bigLayerIndex) {
        addActorToMyStage(GameLayer.getLay(bigLayerIndex), actor);
    }

    public static Group getRoot() {
        return stage.getRoot();
    }

    public static void addActorToMyStage(GameLayer layer, Actor actor) {
        getLayer(layer).addActor(actor);
    }

    public static void removeActor(Actor actor) {
        GameLayer[] layers = GameLayer.values();
        for (GameLayer layer : layers) {
            getLayer(layer).removeActor(actor);
        }
    }

    public static void removeMoreActor(Actor... actor) {
        for (Actor temp : actor) {
            removeActor(temp);
        }
    }

    public static void removeActorForArray(Actor[]... actor) {
        for (Actor[] temp : actor) {
            for (int i = 0; i < temp.length; i++) {
                if (temp[i] != null) {
                    removeActor(temp[i]);
                }
            }
        }
    }

    public static GameLayerGroup getLayer(GameLayer layer) {
        return layer.getGroup();
    }

    public static Actor getActorAtLayer(GameLayer layer, String actorName) {
        return getLayer(layer).findActor(actorName);
    }

    private static void createLayer(GameLayer layer) {
        GameLayerGroup group = new GameLayerGroup();
        layer.init(group);
        stage.addActor(group);
    }

    private static void initAllLayers() {
        GameLayer[] layers = GameLayer.values();
        for (GameLayer createLayer : layers) {
            createLayer(createLayer);
        }
    }

    private static void initStage(int width, int height) {
        SpriteBatch batch = new SpriteBatch(5000);
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(true, (float) width, (float) height);
        stage = new Stage(new StretchViewport((float) width, (float) height, camera), batch);
        Gdx.input.setInputProcessor(stage);
    }

    public static void init(int width, int height) {
        initStage(width, height);
        initAllLayers();
    }

    public static OrthographicCamera getCamera() {
        return (OrthographicCamera) stage.getCamera();
    }

    public static void setSleepTime(float sleepTime2) {
        sleepTime = sleepTime2;
    }

    public static float getSleepTime() {
        return sleepTime / ((float) Rank.getGameSpeed());
    }

    public static int getIndex() {
        return (int) (gameTime / ((double) sleepTime));
    }

    public static boolean isUpdate() {
        boolean isUpdate = ((long) getIndex()) - lastIndex >= 1;
        lastIndex = (long) getIndex();
        return isUpdate;
    }

    public static void setClearColor(Color color) {
        clearColor = color;
    }

    public static float getDelta() {
        return deltaTime;
    }

    public static int getFPS() {
        return fps;
    }

    public static int getRenderCalls() {
        return renderCalls;
    }

    public static void initDebug() {
        ActorText.initFont();
        debugStr = new ActorText("show me the money", 10, 10, 12, 5);
        debugStr.setWidth(200);
    }

    public static void runDebug() {
    }

    private static Vector<Actor> getStageActor(Group group) {
        Vector<Actor> actors = new Vector<>();
        SnapshotArray<Actor> children = group.getChildren();
        children.begin();
        for (int i = 0; i < children.size; i++) {
            Actor actor = children.get(i);
            if (actor instanceof Group) {
                actors.addAll(getStageActor((Group) actor));
            } else if (actor.getName() == null || !actor.getName().equals("debugActor")) {
                actors.add(actor);
            }
        }
        children.end();
        return actors;
    }

    public static void render(float delta) {
        updateServices(delta);
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(16384);
        deltaTime = Math.min(Gdx.graphics.getDeltaTime(), sleepTime);
        sortLayers();
        renderCalls = getBatch().renderCalls;
        stage.act(deltaTime);
        stage.draw();
        gameTime += (double) deltaTime;
    }

    public static void showFps() {
    }

    private static void sortLayers() {
        GameLayer[] layers = GameLayer.values();
        for (int i = 0; i < layers.length; i++) {
            if (layers[i].getComparator() != null && Actor.getisSort()) {
                sort(layers[i]);
            }
        }
        Actor.setisSort(false);
    }

    public static SpriteBatch getBatch() {
        return (SpriteBatch) stage.getBatch();
    }

    public static float getStageWidth() {
        return stage.getWidth();
    }

    public static float getStageHeight() {
        return stage.getHeight();
    }

    public static Stage getStage() {
        return stage;
    }

    public static void clearLayer(GameLayer layer) {
        getLayer(layer).clearChildren();
    }

    public static void clearAllLayers() {
        GameLayer[] layers = GameLayer.values();
        for (GameLayer clearLayer : layers) {
            clearLayer(clearLayer);
        }
    }

    public static void clearListeners() {
        getStageLayer().clearListeners();
    }

    public static void clearStage() {
        stage.clear();
    }

    public static Group getStageLayer() {
        return stage.getRoot();
    }

    public static void sort(GameLayer layer) {
        sort(getLayer(layer), layer.getComparator());
    }

    public static void sort(Group group, Comparator comparator) {
        Sort.instance().sort(group.getChildren(), comparator);
    }

    public TextureRegion getScreenSnapshot() {
        return ScreenUtils.getFrameBufferTexture();
    }

    public void dispose() {
        stage.dispose();
    }

    public static void removeTopLayerActor(GameLayer layer, Actor actor) {
        getLayer(GameLayer.top).removeActor(actor);
    }

    public static void setPause(boolean pause2) {
        pause = pause2;
    }

    public static boolean isPause() {
        return pause;
    }

    public static void registerUpdateService(String serviceName, GUpdateService service) {
        if (service != null) {
            updateServices.put(serviceName, service);
        }
    }

    public static void removeUpdateService(String serviceName) {
        System.out.println("--==" + serviceName);
        if (serviceName != null) {
            updateServices.remove(serviceName);
        }
    }

    public static void removeUpdateService(GUpdateService service) {
        System.out.println("--==--" + service);
        removeUpdateService(updateServices.findKey(service, true));
    }

    public static void updateServices(float delta) {
        if (!pause) {
            ObjectMap.Values<GUpdateService> it = updateServices.values().iterator();
            while (it.hasNext()) {
                GUpdateService service = (GUpdateService) it.next();
                if (service.update(delta)) {
                    removeUpdateService(service);
                }
            }
        }
    }
}
