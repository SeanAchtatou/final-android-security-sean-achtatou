package com.sg.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class GRecord {
    private static final String RECORD_NAME = "sg_td_record";

    public interface RecordReader {
        void read(DataInputStream dataInputStream) throws IOException;
    }

    public interface RecordWriter {
        void write(DataOutputStream dataOutputStream) throws IOException;
    }

    public static void readRecord(int recordID, RecordReader reader) {
        FileHandle file = Gdx.files.local(RECORD_NAME + recordID);
        if (file.exists()) {
            DataInputStream dis = new DataInputStream(file.read());
            try {
                reader.read(dis);
                dis.close();
            } catch (IOException e) {
                System.out.println("read record: " + recordID + " error !");
                e.printStackTrace();
            }
        }
    }

    public static void writeRecord(int recordID, RecordWriter writer) {
        DataOutputStream dos = new DataOutputStream(Gdx.files.local(RECORD_NAME + recordID).write(false));
        try {
            writer.write(dos);
            dos.close();
        } catch (IOException e) {
            System.out.println("write record: " + recordID + " error !");
            e.printStackTrace();
        }
    }
}
