package com.sg.map;

import com.kbz.Actors.ActorImage;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;

public class GameMapObj implements GameConstant {
    public boolean isCanHit;
    ActorImage objImage;
    int type;

    public GameMapObj(int x, int y, int type2, int bigLayer) {
        this.type = type2;
        this.objImage = new ActorImage(type2, x, y, bigLayer, 10);
        int width = (int) this.objImage.getWidth();
        int height = (int) this.objImage.getHeight();
    }

    public float getX() {
        return this.objImage.getX();
    }

    public void initProp(int type2) {
    }

    public void clean() {
        GameStage.removeActor(this.objImage);
    }
}
