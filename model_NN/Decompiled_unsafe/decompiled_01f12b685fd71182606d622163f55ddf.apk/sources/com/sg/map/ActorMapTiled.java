package com.sg.map;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kbz.ActorsExtra.ActorInterface;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class ActorMapTiled extends ActorInterface implements GameConstant {
    private int cliph;
    private int clipw;
    private int clipx;
    private int clipy;
    TextureRegion img;
    boolean isvisible = true;

    public ActorMapTiled(int imgID, int x, int y, int clipx2, int clipy2, int clipw2, int cliph2, int layer, GameLayer biglay) {
        init(imgID, x, y, layer, biglay);
        setClip((float) clipx2, (float) clipy2, (float) clipw2, (float) cliph2);
    }

    public void init(int imgID, int x, int y, int layer, GameLayer biglay) {
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.img.getTexture().getWidth(), (float) this.img.getTexture().getHeight());
        setPosition((float) (x - this.clipw), (float) (y - this.clipy));
        GameStage.addActor(this, layer, biglay);
    }

    public void setClip(float x, float y, float w, float h) {
        this.clipx = (int) x;
        this.clipy = (int) y;
        this.clipw = (int) w;
        this.cliph = (int) h;
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.isvisible) {
            draw2(batch, parentAlpha, 5);
        }
    }

    public void draw2(Batch batch, float parentAlpha, int transFlag) {
    }
}
