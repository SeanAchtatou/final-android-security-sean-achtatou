package com.sg.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Polygon;
import com.kbz.MapData.tmxMap;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.Tools;
import com.sg.util.GameStage;

public class GameMap extends tmxMap {
    public static int CAMERA_SPEED = 200;
    public static final int SCREEN_MOVE = 40;
    public static final int SCREEN_POS = 320;

    /* renamed from: TILE_0_从上到下  reason: contains not printable characters */
    public static final byte f65TILE_0_ = 0;

    /* renamed from: TILE_10_出兵口_从右到左  reason: contains not printable characters */
    public static final byte f66TILE_10__ = 10;

    /* renamed from: TILE_11_出兵口  reason: contains not printable characters */
    public static final byte f67TILE_11_ = 11;

    /* renamed from: TILE_14_随机口  reason: contains not printable characters */
    public static final byte f68TILE_14_ = 14;

    /* renamed from: TILE_15_随机口_从左到右  reason: contains not printable characters */
    public static final byte f69TILE_15__ = 15;

    /* renamed from: TILE_16_随机口_从右到左  reason: contains not printable characters */
    public static final byte f70TILE_16__ = 16;

    /* renamed from: TILE_17_随机口_从上到下  reason: contains not printable characters */
    public static final byte f71TILE_17__ = 17;

    /* renamed from: TILE_1_从左到右  reason: contains not printable characters */
    public static final byte f72TILE_1_ = 1;

    /* renamed from: TILE_2_从右到左  reason: contains not printable characters */
    public static final byte f73TILE_2_ = 2;

    /* renamed from: TILE_3_从左到右_从上到下  reason: contains not printable characters */
    public static final byte f74TILE_3__ = 3;

    /* renamed from: TILE_4_从左到右_从右到左  reason: contains not printable characters */
    public static final byte f75TILE_4__ = 4;

    /* renamed from: TILE_5__从右到左_从上到下  reason: contains not printable characters */
    public static final byte f76TILE_5___ = 5;
    public static final byte TILE_6 = 6;

    /* renamed from: TILE_7_直线移动  reason: contains not printable characters */
    public static final byte f77TILE_7_ = 7;

    /* renamed from: TILE_8_出兵口_从上到下  reason: contains not printable characters */
    public static final byte f78TILE_8__ = 8;

    /* renamed from: TILE_9_出兵口_从左到右  reason: contains not printable characters */
    public static final byte f79TILE_9__ = 9;

    /* renamed from: TILE_全部通过  reason: contains not printable characters */
    public static final byte f80TILE_ = -1;

    /* renamed from: TILE_召唤门  reason: contains not printable characters */
    public static final byte f81TILE_ = 19;

    /* renamed from: TILE_攻击位置  reason: contains not printable characters */
    public static final byte f82TILE_ = 12;

    /* renamed from: TILE_沼泽气泡  reason: contains not printable characters */
    public static final byte f83TILE_ = 18;

    /* renamed from: TILE_生化试管  reason: contains not printable characters */
    public static final byte f84TILE_ = 20;

    /* renamed from: TILE_障碍  reason: contains not printable characters */
    public static final byte f85TILE_ = 13;
    public float cameraSpeed;
    public int collisionsIndex2;
    private int tempX;
    private int tempY;

    public GameMap(String mapIndex) {
        initTmxMap(mapIndex);
    }

    public void AdjustSrceen(float x, float y, boolean isLeft) {
        int posX;
        if (isLeft) {
            posX = 320;
        } else {
            posX = 320;
        }
        this.tempX = (short) ((int) (x - ((float) posX)));
        this.tempX = (short) Math.min(this.tempX, getMapWidth() - 640);
        this.tempX = (short) Math.max(this.tempX, 0);
        float speed = ((float) CAMERA_SPEED) * Gdx.graphics.getDeltaTime();
        if (isLeft) {
            if (Tools.setOffX > ((float) this.tempX)) {
                if (Tools.setOffX - speed < ((float) this.tempX)) {
                    this.cameraSpeed = ((float) this.tempX) - Tools.setOffX;
                } else {
                    this.cameraSpeed = -speed;
                }
                Tools.setOffX += this.cameraSpeed;
                GameStage.getCamera().translate(this.cameraSpeed, Animation.CurveTimeline.LINEAR);
            }
        } else if (Tools.setOffX < ((float) this.tempX)) {
            if (Tools.setOffX + speed > ((float) this.tempX)) {
                this.cameraSpeed = ((float) this.tempX) - Tools.setOffX;
            } else {
                this.cameraSpeed = speed;
            }
            Tools.setOffX += this.cameraSpeed;
            GameStage.getCamera().translate(this.cameraSpeed, Animation.CurveTimeline.LINEAR);
        }
        this.tempY = (short) ((int) (y - 568.0f));
        this.tempY = (short) Math.min(this.tempY, getMapHight() - 1136);
        this.tempY = (short) Math.max(this.tempY, 0);
        if (Tools.setOffY < ((float) this.tempY)) {
            if (Tools.setOffY - speed < ((float) this.tempY)) {
                this.cameraSpeed = ((float) this.tempY) - Tools.setOffY;
            } else {
                this.cameraSpeed = -speed;
            }
            Tools.setOffY += this.cameraSpeed;
            GameStage.getCamera().translate(Animation.CurveTimeline.LINEAR, this.cameraSpeed);
        }
        if (Tools.setOffY > ((float) this.tempY)) {
            if (Tools.setOffY + speed > ((float) this.tempY)) {
                this.cameraSpeed = ((float) this.tempY) - Tools.setOffY;
            } else {
                this.cameraSpeed = speed;
            }
            Tools.setOffY += this.cameraSpeed;
            GameStage.getCamera().translate(Animation.CurveTimeline.LINEAR, this.cameraSpeed);
        }
    }

    public boolean canFireRun(Polygon polygon) {
        return true;
    }

    public GameMapCollision canShellRun(Polygon polygon) {
        return null;
    }

    public int getMapIndex(float x, float y) {
        int tx = (int) (x / ((float) getTileWidth()));
        int ty = (int) (y / ((float) getTileHight()));
        if (tx < 0 || tx >= getWidthNum() || ty < 0 || ty >= getHightNum()) {
            return -1;
        }
        return (getWidthNum() * ty) + tx;
    }
}
