package com.sg.map;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Polygon;
import com.kbz.Actors.ActorShapeSprite;
import com.sg.pak.GameConstant;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;
import com.tendcloud.tenddata.dc;

public class GameMapCollision implements GameConstant {
    public static final int COLLISION_BAOXIANG1 = 40;
    public static final int COLLISION_BAOXIANG2 = 41;
    public static final int COLLISION_BOSS1 = 120;
    public static final int COLLISION_CENTER_SLOPE = 4;
    public static final int COLLISION_ENEMY1 = 100;
    public static final int COLLISION_ENEMY2 = 101;
    public static final int COLLISION_ENEMY3 = 102;
    public static final int COLLISION_FUTI = 5;
    public static final int COLLISION_GAOTAIMUXIANG = 30;
    public static final int COLLISION_LEFT_SLOPE = 2;
    public static final int COLLISION_MUXIANG1 = 20;
    public static final int COLLISION_MUXIANG2 = 21;
    public static final int COLLISION_RIGHT_SLOPE = 3;
    public static final int COLLISION_ROAD = 1;
    public static final int COLLISION_ROLE = 1000;
    public static final int COLLISION_YOUTONG = 11;
    public static final int COLLISION_ZHADAN = 10;
    public int AI;
    private ActorShapeSprite actorShapeSprite;
    public int appearType;
    public int attackD;
    public int goodsmax;
    public int goodsmin;
    private int height;
    int[] hitArray = new int[4];
    private int id;
    private boolean isRoleInStanding = false;
    boolean isTest = false;
    private boolean isVisible = false;
    public int leftRight;
    public int lv;
    public int patrolD;
    public Polygon polygon = new Polygon();
    private float[] polygonFloat0 = new float[8];
    public int refresh;
    private int type;
    public int upDown;
    public int visibleDistance;
    private int width;
    private int x;
    private int y;

    public int getId() {
        return this.id;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x2) {
        this.x = x2;
        this.hitArray[0] = this.x;
    }

    public void setY(int y2) {
        this.y = y2;
        this.hitArray[1] = this.y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public int getType() {
        return this.type;
    }

    public int getUpDown() {
        return this.upDown;
    }

    public boolean isThisType(int type2) {
        return this.type == type2;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public void setVisible(boolean isVisible2) {
        this.isVisible = isVisible2;
    }

    public boolean isRoleInStanding() {
        return this.isRoleInStanding;
    }

    public void setRoleInStanding(boolean isRoleInStanding2) {
        this.isRoleInStanding = isRoleInStanding2;
    }

    public float tan() {
        return (1.0f * ((float) this.height)) / ((float) this.width);
    }

    /* access modifiers changed from: protected */
    public void initDefault(MapProperties object) {
        this.id = getCollsionInter(object, dc.V);
        this.x = (int) getCollsionFloat(object, "x");
        this.y = (int) getCollsionFloat(object, "y");
        this.width = (int) getCollsionFloat(object, "width");
        this.height = (int) getCollsionFloat(object, "height");
        this.type = getCollsionStr(object, "type");
        this.hitArray[0] = this.x;
        this.hitArray[1] = this.y;
        this.hitArray[2] = this.width;
        this.hitArray[3] = this.height;
        if (this.isTest) {
            initHitPolygon(this.x, this.y, this.width, this.height);
        }
    }

    /* access modifiers changed from: protected */
    public void initExtro(MapProperties object) {
        this.upDown = getCollsionStr(object, "upDown");
        this.leftRight = getCollsionStr(object, "leftRight");
        this.lv = getCollsionStr(object, "lv");
        this.attackD = getCollsionStr(object, "attackD");
        this.patrolD = getCollsionStr(object, "patrolD");
        this.AI = getCollsionStr(object, "AI");
        this.goodsmin = getCollsionStr(object, "goodsmin");
        this.goodsmax = getCollsionStr(object, "goodsmax");
        this.visibleDistance = getCollsionStr(object, "visibleDistance");
        if (this.visibleDistance == -1) {
            this.visibleDistance = 0;
        }
        this.appearType = getCollsionStr(object, "appearType");
        this.refresh = getCollsionStr(object, "refresh");
    }

    public GameMapCollision(MapProperties object) {
        initDefault(object);
        initExtro(object);
    }

    /* access modifiers changed from: package-private */
    public float getCollsionFloat(MapProperties object, String properties) {
        Float kbz = (Float) object.get(properties);
        if (kbz == null) {
            return -1.0f;
        }
        return kbz.floatValue();
    }

    private int getCollsionInter(MapProperties object, String properties) {
        Integer kbz = (Integer) object.get(properties);
        if (kbz == null) {
            return -1;
        }
        return kbz.intValue();
    }

    private int getCollsionStr(MapProperties object, String properties) {
        String string = (String) object.get(properties);
        if (string == null) {
            return -1;
        }
        return Integer.parseInt(string);
    }

    private String getCollsionStr_2(MapProperties object, String properties) {
        String string = (String) object.get(properties);
        System.err.println("getCollsionStr_2:" + string);
        if (string == null) {
            return "name null";
        }
        return string;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void initHitPolygon(int tx, int ty, int tw, int th) {
        this.polygonFloat0[0] = 0.0f;
        this.polygonFloat0[1] = 0.0f;
        this.polygonFloat0[2] = (float) tw;
        this.polygonFloat0[3] = 0.0f;
        this.polygonFloat0[4] = (float) (tw + 0);
        this.polygonFloat0[5] = (float) (th + 0);
        this.polygonFloat0[6] = 0.0f;
        this.polygonFloat0[7] = (float) th;
        this.polygon.setVertices(this.polygonFloat0);
        this.polygon.setPosition((float) this.x, (float) this.y);
        if (this.isTest) {
            this.actorShapeSprite = new ActorShapeSprite();
            this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            GameStage.addActorToMyStage(GameLayer.max, this.actorShapeSprite);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void updataHitPolygon() {
        if (this.polygon != null) {
            this.polygon.setPosition((float) getX(), (float) getY());
            if (this.isTest) {
                this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            }
        }
    }
}
