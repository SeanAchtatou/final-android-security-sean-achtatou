package com.sg.tools;

import com.badlogic.gdx.scenes.scene2d.Group;

public class GameLayerGroup extends Group {
    private boolean pause;

    public void act(float delta) {
        if (!this.pause) {
            super.act(delta);
        }
    }

    public void setPause(boolean pause2) {
        this.pause = pause2;
    }

    public boolean isPause() {
        return this.pause;
    }
}
