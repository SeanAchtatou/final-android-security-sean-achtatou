package com.sg.tools;

import com.sg.GameEntry.GameMain;

public final class GameFunction {
    public static final int getPoint(int[][] pointPos, int x, int y) {
        float kx = GameMain.zoomX;
        float ky = GameMain.zoomY;
        for (int i = 0; i < pointPos.length; i++) {
            if (((float) x) >= ((float) pointPos[i][0]) * kx && ((float) x) < (((float) pointPos[i][0]) * kx) + (((float) pointPos[i][2]) * kx) && ((float) y) >= ((float) pointPos[i][1]) * ky && ((float) y) < (((float) pointPos[i][1]) * ky) + (((float) pointPos[i][3]) * ky)) {
                return i;
            }
        }
        return -1;
    }
}
