package com.sg.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.AssetManger.GRes;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.Tools;
import java.util.HashMap;

public class GNumSprite extends Actor {
    public static final byte HCENTER_BOTTOM = 5;
    public static final byte HCENTER_TOP = 3;
    public static final byte HCENTER_VCENTER = 4;
    public static final byte LEFT_BOTTOM = 2;
    public static final byte LEFT_TOP = 0;
    public static final byte LEFT_VCENTER = 1;
    public static final byte RIGHT_BOTTOM = 8;
    public static final byte RIGHT_TOP = 6;
    public static final byte RIGHT_VCENTER = 7;
    private int anchor;
    private HashMap<Character, TextureRegion> charMap;
    private String extraSymbol;
    private int num;
    private int numH;
    private String numStr;
    private int numW;
    private int spacing;
    private int type;

    public GNumSprite(int atlasRegion, String num2, String extraSymbol2, int spacing2, int anchor2) {
        this(Tools.getImage(atlasRegion), num2, extraSymbol2, spacing2, anchor2);
    }

    public GNumSprite(TextureRegion atlasRegion, String numStr2, String extraSymbol2, int spacing2, int anchor2) {
        this.numStr = "0";
        this.charMap = new HashMap<>();
        this.type = 1;
        setAtlasRegion(atlasRegion, extraSymbol2);
        this.spacing = spacing2;
        this.anchor = anchor2;
        setNum(numStr2);
    }

    public void setNum(int num2) {
        if (this.num != num2 || this.type != 0) {
            this.type = 0;
            this.num = num2;
            this.numStr = Integer.toString(num2);
        }
    }

    public void setNum(String numStr2) {
        this.type = 1;
        this.numStr = numStr2;
    }

    public int getNum() {
        return this.num;
    }

    public String getNumStr() {
        return this.numStr;
    }

    public void setAnchor(int anchor2) {
        this.anchor = anchor2;
    }

    public void setSpacing(int spacing2) {
        this.spacing = spacing2;
    }

    public void setAtlasRegion(TextureAtlas.AtlasRegion atlasRegion) {
        setAtlasRegion(atlasRegion, this.extraSymbol);
    }

    public float getNumImageWidth() {
        int digitCounter = this.numStr.length();
        return (float) ((this.numW * digitCounter) + (this.spacing * digitCounter));
    }

    public void setAtlasRegion(TextureRegion atlasRegion, String extraSymbol2) {
        int col;
        if (extraSymbol2 == null) {
            col = 10;
        } else {
            col = extraSymbol2.length() + 10;
        }
        this.numW = atlasRegion.getRegionWidth() / col;
        this.numH = atlasRegion.getRegionHeight();
        setHeight((float) this.numH);
        this.charMap.clear();
        char c = '0';
        for (int i = 0; i < col; i++) {
            TextureRegion region = GRes.createRegionFromTextureRegion(atlasRegion, this.numW * i, 0, this.numW, this.numH);
            if (i < 10) {
                this.charMap.put(Character.valueOf(c), region);
                c = (char) (c + 1);
            } else {
                this.charMap.put(Character.valueOf(extraSymbol2.charAt(i - 10)), region);
            }
        }
    }

    public float getWidth() {
        return (float) ((this.numStr.length() * (this.numW + this.spacing)) - this.spacing);
    }

    public float getHeight() {
        return (float) this.numH;
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.numStr != null) {
            int width = (this.numStr.length() * (this.numW + this.spacing)) - this.spacing;
            float scaleX = getScaleX();
            float scaleY = getScaleY();
            float rotation = getRotation();
            float sin = MathUtils.sinDeg(rotation);
            float cos = MathUtils.cosDeg(rotation);
            float startX = (((float) (((-this.anchor) / 3) * width)) * scaleX) / 2.0f;
            float startY = (float) ((((-this.anchor) % 3) * this.numH) / 2);
            for (int i = 0; i < this.numStr.length(); i++) {
                TextureRegion region = this.charMap.get(Character.valueOf(this.numStr.charAt(i)));
                if (region != null) {
                    float r = startX + (((float) ((this.numW + this.spacing) * i)) * scaleX);
                    float x = getX() + (r * cos);
                    float y = getY() + (r * sin);
                    Color color = getColor();
                    batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
                    Batch batch2 = batch;
                    batch2.draw(region, x, y + startY, Animation.CurveTimeline.LINEAR, -startY, (float) this.numW, (float) this.numH, scaleX, scaleY, rotation);
                }
            }
        }
    }
}
