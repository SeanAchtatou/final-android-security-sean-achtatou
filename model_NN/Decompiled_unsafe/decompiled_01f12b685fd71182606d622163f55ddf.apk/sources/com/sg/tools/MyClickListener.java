package com.sg.tools;

import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorImage;

public class MyClickListener extends ClickListener {
    private ActorImage imgtime;
    private boolean isTouchDown;
    private GameTimer timer = new GameTimer();

    public MyClickListener(float time) {
        this.timer.setFrequency(time);
    }

    public boolean MytouchDown() {
        if (!this.timer.istrue2()) {
            return false;
        }
        this.isTouchDown = true;
        return true;
    }

    public boolean MytouchUp() {
        if (!this.isTouchDown) {
            return false;
        }
        this.isTouchDown = false;
        return true;
    }
}
