package com.sg.tools;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.td.Rank;

public class GameTime {
    private static float brushCountdown;
    public static float delta;
    private static float gameRemainTime;
    private static float gameRemainTime_limit;
    private static float gameRemainTime_min;
    private static float gameRemainTime_sec;
    private static float gameTime;
    private static int gameTime_hou;
    private static int gameTime_min;
    private static int gameTime_sec;

    public static void initGameTime() {
        gameTime = Animation.CurveTimeline.LINEAR;
        gameTime_sec = 0;
        gameTime_min = 0;
        gameTime_hou = 0;
    }

    public static int getGameTime_sec() {
        return gameTime_sec;
    }

    public static void setGameTime_sec(int gameTime_sec2) {
        gameTime_sec = gameTime_sec2;
    }

    public static int getGameTime_min() {
        return gameTime_min;
    }

    public static void setGameTime_min(int gameTime_min2) {
        gameTime_min = gameTime_min2;
    }

    public static int getGameTime_hou() {
        return gameTime_hou;
    }

    public static void setGameTime_hou(int gameTime_hou2) {
        gameTime_hou = gameTime_hou2;
    }

    public static void initGameReMainTimeLimit(float time) {
        gameRemainTime_limit = time;
    }

    public static void initGameReMainTime(int time, int sec) {
        gameRemainTime_min = (float) time;
        gameRemainTime_sec = (float) sec;
        gameRemainTime = Animation.CurveTimeline.LINEAR;
    }

    public static float getGameReMainTimeSec() {
        return gameRemainTime_sec;
    }

    public static float getGameReMainTimeLimit() {
        return gameRemainTime_limit;
    }

    public static float getGameReMainTimeMin() {
        return gameRemainTime_min;
    }

    public static void setBrushCountdown(int brushCountdown2) {
        brushCountdown = (float) brushCountdown2;
    }

    public static float getBrushCountdown() {
        return brushCountdown;
    }

    public static void reMainTimeRun() {
        gameRemainTime += getDelta();
        if (gameRemainTime > gameRemainTime_limit) {
            gameRemainTime_limit = Animation.CurveTimeline.LINEAR;
            gameRemainTime = Animation.CurveTimeline.LINEAR;
        }
    }

    static float getDelta() {
        return delta * ((float) Rank.getGameSpeed());
    }

    public static void Run() {
        gameTime += getDelta();
        if (gameTime >= 1.0f) {
            gameTime_sec++;
            gameTime = Animation.CurveTimeline.LINEAR;
        }
        if (gameTime_sec >= 60) {
            gameTime_min++;
            gameTime_sec = 0;
        }
        if (gameTime_min >= 60) {
            gameTime_hou++;
            gameTime_min = 0;
        }
    }

    public static int getGameTimeSeconds() {
        return (gameTime_min * 60) + gameTime_sec;
    }

    public static void reduceTime(int seconds) {
        if (gameTime_sec >= seconds) {
            gameTime_sec -= seconds;
        } else if (gameTime_min >= 1) {
            gameTime_min--;
            gameTime_sec = (gameTime_sec + 60) - seconds;
        } else {
            gameTime_sec = 0;
        }
    }

    public static void resetTime(int min, int sec) {
        gameTime_min = min;
        gameTime_sec = sec;
    }
}
