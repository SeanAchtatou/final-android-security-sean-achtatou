package com.sg.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kbz.Animation.GShapeTools;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.Tools;

public class MyImage extends Actor {
    private int anchor;
    private float[] checkArea;
    private float[] clipArea;
    private int flag;
    boolean isDebug;
    private boolean isDraw;
    private boolean isSuperposition;
    private TextureRegion region;

    public MyImage() {
        this.isDebug = true;
    }

    public MyImage(int imgName) {
        this(imgName, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0, 0);
    }

    public MyImage(int imgName, float x, float y, int anchor2, int type) {
        this(imgName, x, y, anchor2, new float[]{Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR}, type);
    }

    public MyImage(int imgName, float x, float y, int anchor2) {
        this(imgName, x, y, anchor2, new float[]{Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR}, 0);
    }

    public MyImage(String imgPath, float x, float y, int anchor2) {
        this(imgPath, x, y, anchor2, new float[]{Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR}, 0);
    }

    public MyImage(TextureAtlas.AtlasRegion region2, float x, float y, int anchor2) {
        this(region2, x, y, anchor2, new float[]{Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR}, 0);
    }

    public MyImage(TextureRegion region2, float x, float y, int anchor2, float[] checkArea2, int type) {
        this.isDebug = true;
        setTextureRegion(region2);
        init(x, y, anchor2, checkArea2, type);
    }

    public MyImage(TextureAtlas.AtlasRegion region2, float x, float y, int anchor2, float[] checkArea2, int type) {
        this.isDebug = true;
        setTextureRegion(region2);
        init(x, y, anchor2, checkArea2, type);
    }

    public MyImage(int textureName, float x, float y, int anchor2, float[] checkArea2, int type) {
        this.isDebug = true;
        this.region = Tools.getImage(textureName);
        init(x, y, anchor2, checkArea2, type);
    }

    public MyImage(String imgPath, float x, float y, int anchor2, float[] checkArea2, int type) {
        this.isDebug = true;
        this.region = Tools.getImage(Tools.getImageId(imgPath));
        init(x, y, anchor2, checkArea2, type);
    }

    private void init(float x, float y, int anchor2, float[] checkArea2, int type) {
        setPosition(x, y);
        setAnchor(anchor2);
        if (checkArea2 != null) {
            setCheckArea(checkArea2);
        }
        setWidth((float) this.region.getRegionWidth());
        setHeight((float) this.region.getRegionHeight());
        setTouchable(Touchable.disabled);
        setType(type);
        this.isSuperposition = false;
    }

    public void setIsSuperposition(boolean b) {
        this.isSuperposition = b;
    }

    public boolean getIsSuperposition() {
        return this.isSuperposition;
    }

    public TextureRegion getTextureRegion() {
        return this.region;
    }

    public void setTextureRegion(TextureRegion region2) {
        this.region = region2;
    }

    public void setTextureRegion(int imgName) {
        this.region = Tools.getImage(imgName);
    }

    public void setTextureRegion(String imgName) {
        setTextureRegion(Tools.getImage(Tools.getImageId(imgName)));
    }

    public void setTextureRegion(TextureAtlas.AtlasRegion region2) {
        this.region = region2;
    }

    public void setClipArea(float x, float y, float w, float h) {
        this.clipArea = new float[]{x, y, w, h};
    }

    public void setCheckArea(float[] area) {
        this.checkArea = area;
    }

    public void setCheckArea(float offsetX, float offsetY, float width, float height) {
        this.checkArea = new float[]{offsetX, offsetY, width, height};
    }

    public void setType(int type) {
        this.flag = type;
    }

    public int getType() {
        return this.flag;
    }

    public float[] getArea() {
        return this.checkArea;
    }

    public float getArea(int index) {
        return this.checkArea[index];
    }

    public void setAnchor(int anchor2) {
        this.anchor = anchor2;
    }

    public int getAnchor() {
        return this.anchor;
    }

    /* access modifiers changed from: protected */
    public void setOriginType(int anchor2) {
        switch (anchor2) {
            case 0:
                setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                return;
            case 1:
                setOrigin(Animation.CurveTimeline.LINEAR, getHeight());
                return;
            case 2:
                setOrigin(getWidth(), Animation.CurveTimeline.LINEAR);
                return;
            case 3:
                setOrigin(getWidth(), getHeight());
                return;
            case 4:
                setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
                return;
            case 5:
                setOrigin(getWidth() / 2.0f, getHeight());
                return;
            case 6:
                setOrigin(getWidth() / 2.0f, Animation.CurveTimeline.LINEAR);
                return;
            default:
                return;
        }
    }

    private void checkIsDraw() {
        float dx = getX();
        float dy = getY();
        for (Actor parent = getParent(); parent != null; parent = parent.getParent()) {
            dx += parent.getX();
            dy += parent.getY();
        }
        if (!GTools.isDraw(dx, dy, getWidth(), getHeight(), this.anchor)) {
            this.isDraw = false;
        } else {
            this.isDraw = true;
        }
    }

    public boolean getIsDraw() {
        return this.isDraw;
    }

    public void draw(Batch batch, float parentAlpha) {
        float w = getWidth();
        float h = getHeight();
        float ox = getOriginX();
        float oy = getOriginY();
        float scaleX = getScaleX();
        float scaleY = getScaleY();
        float rotation = getRotation();
        float x = GTools.getX(getX(), w, this.anchor);
        float y = GTools.getY(getY(), h, this.anchor);
        checkIsDraw();
        if (getIsDraw()) {
            Color color = getColor();
            if (this.isSuperposition) {
                batch.setBlendFunction(770, 1);
            }
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            float x2 = x + getArea(0);
            float y2 = y + getArea(1);
            if (this.clipArea != null) {
                batch.draw(this.region.getTexture(), x2, y2, (int) this.clipArea[0], (int) this.clipArea[1], (int) this.clipArea[2], (int) this.clipArea[3]);
            } else {
                batch.draw(this.region, x2, y2, ox, oy, w, h, scaleX, scaleY, rotation);
            }
            if (this.isDebug) {
                GShapeTools.drawRectangle(batch, Color.RED, getX(), getY(), getArea(2), getArea(3), false);
            }
            if (this.isSuperposition) {
                batch.setBlendFunction(770, 771);
            }
        }
    }
}
