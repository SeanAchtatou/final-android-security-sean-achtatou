package com.sg.tools;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.sg.pak.GameConstant;

public abstract class MyGroup extends Group implements GameConstant {
    private boolean pause;

    public abstract void exit();

    public abstract void init();

    public void setPause(boolean pause2) {
        this.pause = pause2;
    }

    public void act(float delta) {
        if (!this.pause) {
            super.act(delta);
            run(delta);
        }
    }

    public void run(float delta) {
    }

    public MyGroup() {
        setTransform(false);
        init();
    }

    public void free() {
        exit();
        remove();
        clear();
    }
}
