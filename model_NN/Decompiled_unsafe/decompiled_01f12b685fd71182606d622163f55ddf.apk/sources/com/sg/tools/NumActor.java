package com.sg.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.Tools;

public class NumActor extends Actor {
    private int anchor = 0;
    private int digitCounter = 0;
    private int[] digits = new int[10];
    int num;
    private int numH;
    private int numW;
    TextureRegion region;
    private int space;
    private int srcH;
    private int srcW;
    private int srcX;
    private int srcY;
    private Texture texture;

    public void setTextureRegion(int imgName) {
        this.region = Tools.getImage(imgName);
    }

    public void setTextureRegion(String imgName) {
        this.region = Tools.getImage(Tools.getImageId(imgName));
    }

    public void set(int imgName, int num2, int space2) {
        setTextureRegion(imgName);
        this.num = num2;
        this.space = space2;
        this.texture = this.region.getTexture();
        this.srcX = this.region.getRegionX();
        this.srcY = this.region.getRegionY();
        this.srcW = this.region.getRegionWidth();
        this.srcH = this.region.getRegionHeight();
        this.srcY -= this.srcH;
        this.numW = this.srcW / 10;
        do {
            this.digits[this.digitCounter] = num2 % 10;
            num2 /= 10;
            this.digitCounter++;
        } while (num2 > 0);
    }

    public void set(int imgName, int num2, int space2, int anchor2) {
        setTextureRegion(imgName);
        this.num = num2;
        this.space = space2;
        this.anchor = anchor2;
        this.texture = this.region.getTexture();
        this.srcX = this.region.getRegionX();
        this.srcY = this.region.getRegionY();
        this.srcW = this.region.getRegionWidth();
        this.srcH = this.region.getRegionHeight();
        this.srcY -= this.srcH;
        this.numW = this.srcW / 10;
        this.numH = this.srcH;
        do {
            this.digits[this.digitCounter] = num2 % 10;
            num2 /= 10;
            this.digitCounter++;
        } while (num2 > 0);
    }

    public void set2(int imgName, int num2, int space2, int anchor2) {
        setTextureRegion(imgName);
        this.num = num2;
        this.space = space2;
        this.anchor = anchor2;
        this.texture = this.region.getTexture();
        this.srcX = this.region.getRegionX();
        this.srcY = this.region.getRegionY();
        this.srcW = this.region.getRegionWidth();
        this.srcH = this.region.getRegionHeight();
        this.srcY -= this.srcH;
        this.numW = this.srcW / 10;
        this.numH = this.srcH;
        do {
            this.digits[this.digitCounter] = num2 % 10;
            num2 /= 10;
            this.digitCounter++;
        } while (num2 > 0);
        if (this.num < 10) {
            this.digits[this.digitCounter] = 0;
            this.digitCounter++;
        }
    }

    public void changeNum(int num2) {
        this.digitCounter = 0;
        do {
            this.digits[this.digitCounter] = num2 % 10;
            num2 /= 10;
            this.digitCounter++;
        } while (num2 > 0);
    }

    public void changeNum2(int num2) {
        int initNum = num2;
        this.digitCounter = 0;
        do {
            this.digits[this.digitCounter] = num2 % 10;
            num2 /= 10;
            this.digitCounter++;
        } while (num2 > 0);
        if (initNum < 10) {
            this.digits[this.digitCounter] = 0;
            this.digitCounter++;
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        switch (getAnchor()) {
            case 0:
                for (int i = 0; i < this.digitCounter; i++) {
                    Batch batch2 = batch;
                    batch2.draw(this.texture, ((float) (this.space * i)) + getX() + ((float) (this.numW * i)), getY(), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.numW, (float) this.srcH, getScaleX(), getScaleY(), getRotation(), (this.digits[(this.digitCounter - i) - 1] * this.numW) + this.srcX, this.srcY, this.numW, this.srcH, false, true);
                }
                return;
            case 1:
            case 3:
            case 5:
            default:
                return;
            case 2:
                for (int i2 = 0; i2 < this.digitCounter; i2++) {
                    Batch batch3 = batch;
                    batch3.draw(this.texture, ((float) (this.space * i2)) + (getX() - ((float) (this.digitCounter * (this.numW + this.space)))) + ((float) (this.numW * i2)), getY(), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.numW, (float) this.srcH, getScaleX(), getScaleY(), getRotation(), (this.digits[(this.digitCounter - i2) - 1] * this.numW) + this.srcX, this.srcY, this.numW, this.srcH, false, true);
                }
                return;
            case 4:
                for (int i3 = 0; i3 < this.digitCounter; i3++) {
                    Batch batch4 = batch;
                    batch4.draw(this.texture, ((float) (this.space * i3)) + (getX() - ((float) ((this.digitCounter * (this.numW + this.space)) / 2))) + ((float) (this.numW * i3)), getY() - ((float) (this.numH / 2)), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.numW, (float) this.srcH, getScaleX(), getScaleY(), getRotation(), (this.digits[(this.digitCounter - i3) - 1] * this.numW) + this.srcX, this.srcY, this.numW, this.srcH, false, true);
                }
                return;
            case 6:
                for (int i4 = 0; i4 < this.digitCounter; i4++) {
                    Batch batch5 = batch;
                    batch5.draw(this.texture, ((float) (this.space * i4)) + (getX() - ((float) ((this.digitCounter * (this.numW + this.space)) / 2))) + ((float) (this.numW * i4)), getY(), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.numW, (float) this.srcH, getScaleX(), getScaleY(), getRotation(), (this.digits[(this.digitCounter - i4) - 1] * this.numW) + this.srcX, this.srcY, this.numW, this.srcH, false, true);
                }
                return;
        }
    }

    private int getAnchor() {
        return this.anchor;
    }
}
