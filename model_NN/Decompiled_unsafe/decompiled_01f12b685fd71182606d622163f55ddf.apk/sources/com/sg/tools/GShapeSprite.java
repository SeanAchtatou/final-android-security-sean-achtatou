package com.sg.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.Animation.GShapeTools;
import com.kbz.esotericsoftware.spine.Animation;

public class GShapeSprite extends Group {
    private GShape shape = new GShape();

    public GShapeSprite() {
        addActor(this.shape);
    }

    public void createTriangle(boolean isFill, float x1, float y1, float x2, float y2, float x3, float y3) {
        this.shape.createTriangle(isFill, x1, y1, x2, y2, x3, y3);
    }

    public void createCurve(float x1, float y1, float cx1, float cy1, float cx2, float cy2, float x2, float y2) {
        this.shape.createCurve(x1, y1, cx1, cy1, cx2, cy2, x2, y2);
    }

    public void createRectangle(boolean isFill, float x, float y, float w, float h) {
        this.shape.createRectangle(isFill, x, y, w, h);
    }

    public void createLine(float x1, float y1, float x2, float y2) {
        this.shape.createLine(x1, y1, x2, y2);
    }

    public void createPoint(float x, float y) {
        this.shape.createPoint(x, y);
    }

    public void createCircle(boolean isFill, float x, float y, float radius) {
        this.shape.createCircle(isFill, x, y, radius);
    }

    public void createPolygon(boolean isClosed, float[] vertices) {
        this.shape.createPolygon(isClosed, vertices);
    }

    public void createX(float x, float y, float radius) {
        this.shape.createX(x, y, radius);
    }

    public void createEllipse(boolean isFill, float x, float y, float w, float h) {
        this.shape.createEllipse(isFill, x, y, w, h);
    }

    public void createARC(boolean isFill, float x, float y, float radius, float start, float angle) {
        this.shape.createARC(isFill, x, y, radius, start, angle);
    }

    public void setColor(Color color) {
        this.shape.setColor(color);
    }

    public void setColor(float r, float g, float b, float a) {
        this.shape.setColor(r, g, b, a);
    }

    private class GShape extends Actor {
        private static final int TYPE_ARC = 9;
        private static final int TYPE_BOX = 10;
        private static final int TYPE_CIRCLE = 4;
        private static final int TYPE_CONE = 12;
        private static final int TYPE_CURVE = 5;
        private static final int TYPE_ELLIPSE = 8;
        private static final int TYPE_LINE = 1;
        private static final int TYPE_POINT = 0;
        private static final int TYPE_POLYGON = 6;
        private static final int TYPE_POLYGON_LINE = 7;
        private static final int TYPE_RECTANGLE = 3;
        private static final int TYPE_TRIANGLE = 2;
        private static final int TYPE_X = 11;
        float cx1;
        float cx2;
        float cy1;
        float cy2;
        float radius;
        private ShapeRenderer.ShapeType shapeType;
        private int type;
        float[] vertices;
        float x2;
        float x3;
        float y2;
        float y3;

        private GShape() {
            this.shapeType = null;
            this.type = -1;
        }

        /* access modifiers changed from: private */
        public void createPolygon(boolean isClosed, float[] vertices2) {
            this.vertices = vertices2;
            this.shapeType = ShapeRenderer.ShapeType.Line;
            if (isClosed) {
                this.type = 6;
            } else {
                this.type = 7;
            }
        }

        /* access modifiers changed from: private */
        public void createARC(boolean isFill, float x, float y, float radius2, float start, float angle) {
            this.shapeType = isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line;
            this.type = 9;
            setX(x);
            setY(y);
            this.radius = radius2;
            this.cx1 = start;
            this.cy1 = angle;
        }

        /* access modifiers changed from: private */
        public void createTriangle(boolean isFill, float x1, float y1, float x22, float y22, float x32, float y32) {
            this.shapeType = isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line;
            this.type = 2;
            setX(x1);
            setY(y1);
            this.x2 = x22;
            this.y2 = y22;
            this.x3 = x32;
            this.y3 = y32;
        }

        /* access modifiers changed from: private */
        public void createCurve(float x1, float y1, float cx12, float cy12, float cx22, float cy22, float x22, float y22) {
            this.shapeType = ShapeRenderer.ShapeType.Line;
            this.type = 5;
            setX(x1);
            setY(y1);
            this.cx1 = cx12;
            this.cy1 = cy12;
            this.cx2 = cx22;
            this.cy2 = cy22;
            this.x2 = x22;
            this.y2 = y22;
        }

        /* access modifiers changed from: private */
        public void createRectangle(boolean isFill, float x, float y, float w, float h) {
            this.shapeType = isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line;
            this.type = 3;
            setBounds(x, y, w, h);
        }

        /* access modifiers changed from: private */
        public void createEllipse(boolean isFill, float x, float y, float w, float h) {
            this.shapeType = isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line;
            this.type = 8;
            setBounds(x, y, w, h);
        }

        /* access modifiers changed from: private */
        public void createLine(float x1, float y1, float x22, float y22) {
            this.shapeType = ShapeRenderer.ShapeType.Line;
            this.type = 1;
            setX(x1);
            setY(y1);
            this.x2 = x22;
            this.y2 = y22;
        }

        /* access modifiers changed from: private */
        public void createPoint(float x, float y) {
            this.shapeType = ShapeRenderer.ShapeType.Point;
            this.type = 0;
            setX(x);
            setY(y);
        }

        /* access modifiers changed from: private */
        public void createX(float x, float y, float radius2) {
            this.shapeType = ShapeRenderer.ShapeType.Line;
            this.type = 11;
            setX(x);
            setY(y);
            this.radius = radius2;
        }

        /* access modifiers changed from: private */
        public void createCircle(boolean isFill, float x, float y, float radius2) {
            this.shapeType = isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line;
            this.type = 4;
            setX(x);
            setY(y);
            this.radius = radius2;
        }

        public void draw(Batch batch, float parentAlpha) {
            if (this.type < 0) {
                System.out.println("no set shape !!!!!!!!  " + this.type);
                return;
            }
            batch.end();
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(770, 771);
            ShapeRenderer sRender = GShapeTools.getShapeRenderer();
            sRender.setProjectionMatrix(batch.getProjectionMatrix());
            sRender.setTransformMatrix(batch.getTransformMatrix());
            Color color = getColor();
            sRender.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            sRender.begin(this.shapeType);
            float x = getX();
            float y = getY();
            float w = getWidth();
            float h = getHeight();
            switch (this.type) {
                case 0:
                    sRender.point(x, y, Animation.CurveTimeline.LINEAR);
                    break;
                case 1:
                    sRender.line(x, y, this.x2, this.y2);
                    break;
                case 2:
                    sRender.triangle(x, y, this.x2, this.y2, this.x3, this.y3);
                    break;
                case 3:
                    sRender.rect(x, y, w, h);
                    break;
                case 4:
                    sRender.circle(x, y, this.radius);
                    break;
                case 5:
                    sRender.curve(x, y, this.cx1, this.cy1, this.cx2, this.cy2, this.x2, this.y2, 1);
                    break;
                case 6:
                    sRender.polygon(this.vertices);
                    break;
                case 7:
                    sRender.polyline(this.vertices);
                    break;
                case 8:
                    sRender.ellipse(x, y, w, h);
                    break;
                case 9:
                    sRender.arc(x, y, this.radius, this.cx1, this.cy1);
                    break;
                case 10:
                    sRender.box(x, y, Animation.CurveTimeline.LINEAR, w, h, Animation.CurveTimeline.LINEAR);
                    break;
                case 11:
                    sRender.x(x, y, this.radius);
                    break;
                case 12:
                    sRender.cone(x, y, Animation.CurveTimeline.LINEAR, this.radius, h);
                    break;
            }
            sRender.end();
            batch.begin();
        }
    }
}
