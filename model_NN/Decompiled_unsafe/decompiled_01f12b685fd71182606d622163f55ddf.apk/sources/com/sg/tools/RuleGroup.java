package com.sg.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.util.GameStage;

public class RuleGroup extends Group {
    private BitmapFont bitmapFont = new BitmapFont(true);
    /* access modifiers changed from: private */
    public boolean cancle;
    private InputListener listener = new InputListener() {
        public boolean keyDown(InputEvent event, int keycode) {
            if (RuleGroup.this.target == null) {
                return false;
            }
            float tx = Animation.CurveTimeline.LINEAR;
            float ty = Animation.CurveTimeline.LINEAR;
            switch (keycode) {
                case 19:
                    ty = Animation.CurveTimeline.LINEAR - 1.0f;
                    break;
                case 20:
                    ty = Animation.CurveTimeline.LINEAR + 1.0f;
                    break;
                case 21:
                    tx = Animation.CurveTimeline.LINEAR - 1.0f;
                    break;
                case 22:
                    tx = Animation.CurveTimeline.LINEAR + 1.0f;
                    break;
            }
            RuleGroup.this.target.moveBy(tx, ty);
            return true;
        }

        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            boolean unused = RuleGroup.this.cancle = button == 1;
            return true;
        }

        public boolean scrolled(InputEvent event, float x, float y, int amount) {
            return super.scrolled(event, x, y, amount);
        }

        public boolean mouseMoved(InputEvent event, float x, float y) {
            if (RuleGroup.this.target == null || RuleGroup.this.cancle) {
                return super.mouseMoved(event, x, y);
            }
            RuleGroup.this.target.setPosition(x - RuleGroup.this.getX(), y - RuleGroup.this.getY());
            return super.mouseMoved(event, x, y);
        }
    };
    /* access modifiers changed from: private */
    public Actor target;

    public RuleGroup() {
        setSize(GameMain.sceenWidth, GameMain.sceenHeight);
        GameStage.getStage().addListener(this.listener);
        debug();
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (this.target != null) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            int x = (int) this.target.getX();
            this.bitmapFont.draw(batch, "x:" + x + " y:" + ((int) this.target.getY()), (getX() + getWidth()) - 80.0f, getY() + 10.0f);
        }
    }

    public void setTarget(Actor target2) {
        this.target = target2;
        this.target.setTouchable(Touchable.enabled);
        setVisible(true);
        addActor(target2);
    }
}
