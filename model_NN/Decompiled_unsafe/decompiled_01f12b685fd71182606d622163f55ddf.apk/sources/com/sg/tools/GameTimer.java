package com.sg.tools;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.util.GameStage;

public class GameTimer {
    private float cdTime;
    private float frequency;

    public float getCdTime() {
        return this.cdTime;
    }

    public void setCdTime(float obj) {
        this.cdTime = obj;
    }

    public float getFrequency() {
        return this.frequency;
    }

    public void setFrequency(float obj) {
        this.frequency = obj;
    }

    public void run() {
        setCdTime(getCdTime() + (GameStage.getDelta() * 3.0f));
    }

    public boolean istrue() {
        run();
        if (getCdTime() != Animation.CurveTimeline.LINEAR && getCdTime() / getFrequency() < 1.0f) {
            return false;
        }
        setCdTime(Animation.CurveTimeline.LINEAR);
        return true;
    }

    public boolean istrue2() {
        if (getCdTime() != Animation.CurveTimeline.LINEAR && getCdTime() / getFrequency() < 1.0f) {
            return false;
        }
        setCdTime(Animation.CurveTimeline.LINEAR);
        return true;
    }
}
