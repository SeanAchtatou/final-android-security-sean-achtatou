package com.sg.tools;

public class MyPoint {
    public int dir;
    public float x;
    public float y;

    public MyPoint(float x2, float y2, int dir2) {
        this.x = x2;
        this.y = y2;
        this.dir = dir2;
    }

    public void setPoint(float x2, float y2, int dir2) {
        this.x = x2;
        this.y = y2;
        this.dir = dir2;
    }

    public void setPoint(int[] points) {
        this.x = (float) points[0];
        this.y = (float) points[1];
        this.dir = points[2];
    }

    public int[] getPoint() {
        return new int[]{(int) this.x, (int) this.y, this.dir};
    }
}
