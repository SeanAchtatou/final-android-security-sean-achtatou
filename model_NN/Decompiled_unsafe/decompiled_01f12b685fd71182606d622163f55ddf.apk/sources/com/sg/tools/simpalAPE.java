package com.sg.tools;

public class simpalAPE {
    public static final int R = 16;
    public static final double angleV = 100.0d;

    public static boolean isHitWithCircle(double x1, double y1, int r1, double x2, double y2, int r2) {
        if (Math.sqrt(Math.pow(x1 - x2, 2.0d) + Math.pow(y1 - y2, 2.0d)) <= ((double) (r1 + r2))) {
            return true;
        }
        return false;
    }

    public static boolean hit(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {
        int y12 = y1 - h1;
        int y22 = y2 - h2;
        return x1 < x2 + w2 && x2 < x1 + w1 && y12 < y22 + h2 && y22 < y12 + h1;
    }

    public static boolean hit(int x1, int y1, int x2, int y2, int w2, int h2) {
        int y22 = y2 - h2;
        return x1 >= x2 && x1 < x2 + w2 && y1 >= y22 && y1 < y22 + h2;
    }

    public static boolean Hit(double x1, double y1, int r1, double x2, double y2) {
        if (x2 <= x1 - ((double) r1) || x2 > ((double) r1) + x1 || y2 <= y1 - ((double) r1) || y2 > ((double) r1) + y1) {
            return false;
        }
        return true;
    }

    public static double getCorrectionAngle(double angle) {
        return (double) (((((int) angle) / 60) * 60) + 30);
    }

    public static float myround(double f) {
        return (float) (((double) Math.round(f * 100.0d)) / 100.0d);
    }

    public static double myround2(double f) {
        return (double) Math.round(f);
    }

    public static double[] getCorrectionPosXY(double x, double y, double angle, double objx, double objy) {
        double CorAngle = getCorrectionAngle(getAngle_XY_X(x, y, objx, objy));
        return new double[]{x + (Math.cos(getRadian((90.0d - angle) + CorAngle)) * 32.0d), y + (Math.sin(getRadian((90.0d - angle) + CorAngle)) * 32.0d), (90.0d - angle) + CorAngle};
    }

    public static float[] getRotationPosXY2(float x1, float y1, float x2, float y2, float angle) {
        float angle2 = myround((double) angle);
        float x = x2 - x1;
        float y = y2 - y1;
        return new float[]{((float) ((((double) x) * Math.cos((((double) angle2) * 3.141592653589793d) / 180.0d)) - (((double) y) * Math.sin(((double) angle2) * 0.017453292519943295d)))) + x1, ((float) ((((double) x) * Math.sin((((double) angle2) * 3.141592653589793d) / 180.0d)) + (((double) y) * Math.cos(((double) angle2) * 0.017453292519943295d)))) + y1};
    }

    public static double[] getRotationPosXY(double L, double angle1, double angle2) {
        return new double[]{L * Math.cos(getRadian(angle1 + angle2)), L * Math.sin(getRadian(angle1 + angle2))};
    }

    public static double getRange(double x1, double y1, double x2, double y2) {
        double x = x2 - x1;
        double y = y2 - y1;
        return Math.sqrt((x * x) + (y * y));
    }

    public static double getAngle_XY_X(double x, double y, double mx, double my) {
        double angle = (Math.atan2(my - y, mx - x) * 180.0d) / 3.141592653589793d;
        if (angle < 0.0d) {
            return angle + 360.0d;
        }
        return angle;
    }

    public static double getAngle_xl(double x1, double y1, double x2, double y2) {
        double n = (x1 * x2) + (y1 * y2);
        double m = Math.sqrt((x1 * x1) + (y1 * y1)) * Math.sqrt((x2 * x2) + (y2 * y2));
        if (m != 0.0d) {
            return Math.acos(n / m) * 57.29577951308232d;
        }
        System.out.println("求两个向量夹角参数错误");
        return 0.0d;
    }

    public static float getRadian_XY_X(float x, float y, float mx, float my) {
        return (float) Math.atan2((double) (my - y), (double) (mx - x));
    }

    private static void conforMainCilcle(int x1, int y1, int r1, int angular_velocity, int r2) {
    }

    private static double getX(int x, int y, int l, int angle) {
        return ((double) x) + (((double) l) * Math.cos(getRadian((double) angle)));
    }

    private static double getY(int x, int y, int l, int angle) {
        return ((double) y) + (((double) l) * Math.sin(getRadian((double) angle)));
    }

    public static double getRadian(double angle) {
        return (3.141592653589793d * angle) / 180.0d;
    }

    public static double getAngle(double radian) {
        return (180.0d * radian) / 3.141592653589793d;
    }

    public static double getVX(int srcx, int srcy, int x, int y) {
        return ((double) x) + (Math.cos(getAngle_XY_X((double) srcx, (double) srcy, (double) x, (double) y)) * getRange((double) srcx, (double) srcy, (double) x, (double) y));
    }

    public static double getVY(int srcx, int srcy, int x, int y) {
        return ((double) x) + (Math.sin(getAngle_XY_X((double) srcx, (double) srcy, (double) x, (double) y)) * getRange((double) srcx, (double) srcy, (double) x, (double) y));
    }

    public static double getAngle_Torque_X(double x1, double y1, double x2, double y2, double vx, double vy) {
        return (90.0d + getAngle_XY_X(0.0d, 0.0d, (double) ((int) vx), (double) ((int) vy))) - getAngle_XY_X(x1, y1, x2, y2);
    }

    public static double getTorque(double angle, double vx, double vy) {
        return Math.sqrt((vx * vx) + (vy * vy)) * Math.cos(getRadian(angle));
    }

    public static double getAngleV(double x1, double y1, double x2, double y2, double vx, double vy, double L) {
        return (100.0d * getTorque(getAngle_Torque_X(x1, y1, x2, y2, vx, vy), vx, vy)) / L;
    }

    public static double getAngle(double[] dPoint) {
        double s1 = Math.sqrt(Math.pow(dPoint[0] - dPoint[2], 2.0d) + Math.pow(dPoint[1] - dPoint[3], 2.0d));
        double s2 = Math.sqrt(Math.pow(dPoint[4] - dPoint[2], 2.0d) + Math.pow(dPoint[5] - dPoint[3], 2.0d));
        double s3 = Math.sqrt(Math.pow(dPoint[0] - dPoint[4], 2.0d) + Math.pow(dPoint[1] - dPoint[5], 2.0d));
        double p = ((s1 + s2) + s3) / 2.0d;
        return (double) myround((180.0d * Math.asin((2.0d * Math.sqrt((((p - s1) * p) * (p - s2)) * (p - s3))) / (s1 * s2))) / 3.14d);
    }
}
