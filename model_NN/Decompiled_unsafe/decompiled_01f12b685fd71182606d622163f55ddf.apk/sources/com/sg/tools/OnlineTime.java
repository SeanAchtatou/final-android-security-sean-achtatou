package com.sg.tools;

import com.kbz.esotericsoftware.spine.Animation;

public class OnlineTime {
    private static int gameIndex;
    private static float gameTime;
    private static int gameTime_hou;
    private static int gameTime_min;
    private static int gameTime_sec;

    public static int getOnLineMinus() {
        return gameTime_min;
    }

    public static int getOnLineSecond() {
        return gameTime_sec;
    }

    public static int getOnLineIndex() {
        return gameIndex;
    }

    public static void initOnlineTime() {
        gameTime = Animation.CurveTimeline.LINEAR;
        gameTime_sec = 0;
        gameTime_min = 0;
        gameTime_hou = 0;
    }

    public static void Run(float delta) {
        gameTime += delta;
        if (gameTime > 1.0f) {
            gameTime_sec++;
            gameTime = Animation.CurveTimeline.LINEAR;
            gameIndex++;
        }
        if (gameTime_sec > 60) {
            gameTime_min++;
            gameTime_sec = 0;
        }
        if (gameTime_min > 60) {
            gameTime_hou++;
            gameTime_min = 0;
        }
    }
}
