package com.sg.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class MyMath {
    static Random rnd = new Random();

    public static int getMaxLen(int[][] _ware) {
        int index = 0;
        for (int i = 0; i < _ware.length; i++) {
            if (index < _ware[i].length) {
                index = _ware[i].length;
            }
        }
        return index;
    }

    public static int[] getRandomDataArray(int[] tmpArray, int startNum) {
        if (tmpArray == null) {
            return null;
        }
        List<Integer> list = new ArrayList<>();
        for (int i = startNum; i < tmpArray.length; i++) {
            list.add(Integer.valueOf(tmpArray[i]));
        }
        Random r = new Random();
        for (int i2 = startNum; i2 < tmpArray.length; i2++) {
            int j = r.nextInt(list.size());
            tmpArray[i2] = Integer.valueOf(((Integer) list.get(j)).toString()).intValue();
            list.remove(j);
        }
        return tmpArray;
    }

    public static int[] resultNoSame(int[] data, int num) {
        int[] array = new int[num];
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            int irdm = r.nextInt(data.length - i);
            array[i] = data[irdm];
            for (int j = irdm; j < (data.length - i) - 1; j++) {
                data[j] = data[j + 1];
            }
        }
        return array;
    }

    public static int[] result(int[] data, int num) {
        int[] array = new int[num];
        Random r = new Random();
        for (int i = 0; i < num; i++) {
            array[i] = data[r.nextInt(data.length - i)];
        }
        return array;
    }

    public static int[] int_To_Array(int num) {
        int[] tmpArray = new int[num];
        for (int i = 0; i < num; i++) {
            tmpArray[i] = i;
        }
        return tmpArray;
    }

    public static int getArrayIndex(int[][] _array, int num) {
        for (int i = 0; i < _array.length; i++) {
            for (int j = 0; j < _array[i].length; j++) {
                if (j == num) {
                    return i;
                }
            }
        }
        return 0;
    }

    private static int[] createEnemyGruop(int[] enemyType, int[] enemyNum) {
        int len = 0;
        for (int i : enemyNum) {
            len += i;
        }
        int[] temp = new int[len];
        int len2 = 0;
        int len1 = 0;
        for (int i2 = 0; i2 < temp.length; i2++) {
            temp[i2] = enemyType[len1];
            len2++;
            if (len2 > enemyNum[len1] - 1) {
                len2 = 0;
                len1++;
            }
        }
        return temp;
    }

    public static int[] createEnemyGruop(int[] enemyType) {
        if (enemyType == null) {
            return null;
        }
        int len = 0;
        for (int i = 0; i < enemyType.length / 2; i++) {
            len++;
        }
        int[] temp1 = new int[len];
        int[] temp2 = new int[len];
        for (int i2 = 0; i2 < enemyType.length / 2; i2++) {
            temp1[i2] = enemyType[i2 * 2];
            temp2[i2] = enemyType[(i2 * 2) + 1];
        }
        return createEnemyGruop(temp1, temp2);
    }

    public static int[][] getRestartSortArray(int[][] array, int betweenNum, int len, int changeArrayDataNum) {
        int id;
        int[][] new_array = new int[len][];
        for (int i = 1; i <= len; i++) {
            if (i % changeArrayDataNum == 0) {
                id = result(betweenNum, array.length);
                new_array[i - 1] = (int[]) createEnemyGruop(array[id]).clone();
            } else {
                id = result(0, betweenNum);
                new_array[i - 1] = (int[]) createEnemyGruop(array[id]).clone();
            }
            System.out.println("    i:" + (i - 1) + " :" + id);
        }
        return new_array;
    }

    public static int getArraySumOfElements(int[][] arraySrc, int[][] arrayDir, int getRowNum, int getElementType) {
        int sum = 0;
        for (int i = 0; i < getRowNum; i++) {
            for (int i2 : arraySrc[i]) {
                sum += arrayDir[i2][getElementType];
            }
        }
        return sum;
    }

    public static String[] splitString(String src, String key) {
        Vector<String> vt = new Vector<>();
        int w = 0;
        boolean end = false;
        while (!end) {
            int pos = src.indexOf(key, w);
            if (pos == -1) {
                pos = src.length();
                end = true;
            }
            int endIndex = pos;
            if (pos > 0 && src.charAt(pos - 1) == 13) {
                endIndex = pos - 1;
            }
            String s = src.substring(w, endIndex).trim();
            if (!s.equals("")) {
                vt.addElement(s);
            }
            w = pos + key.length();
        }
        String[] cs = new String[vt.size()];
        vt.copyInto(cs);
        return cs;
    }

    public static float GetDistance(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public static int[] getArrayToSortAppointNum(int[] array, int getNum) {
        int index = 0;
        int[] tmpArray = new int[array.length];
        for (int i : array) {
            if (i == getNum) {
                tmpArray[index] = getNum;
                index++;
            }
        }
        int[] tmpArray2 = new int[(array.length - index)];
        int index2 = 0;
        for (int i2 = 0; i2 < array.length; i2++) {
            if (array[i2] != getNum) {
                tmpArray2[index2] = array[i2];
                index2++;
            }
        }
        int[] tmpArray3 = new int[array.length];
        for (int i3 = 0; i3 < tmpArray3.length; i3++) {
            if (i3 < index) {
                tmpArray3[i3] = tmpArray[i3];
            } else {
                tmpArray3[i3] = tmpArray2[i3 - index];
            }
        }
        return tmpArray3;
    }

    public static int getArraySameNumIndex(int[] array, int num) {
        int index = 0;
        for (int i : array) {
            if (i == num) {
                index++;
            }
        }
        return index;
    }

    public static final int[] result(int[] array) {
        int len = array.length;
        int[] r = new int[len];
        for (int i = 0; i < len; i++) {
            int index = result(i, array.length);
            r[i] = array[i];
            array[i] = array[index];
            array[index] = r[i];
            r[i] = array[i];
        }
        return r;
    }

    public static final int result(int r) {
        return (rnd.nextInt() >>> 1) % r;
    }

    public static final int result(int min, int max) {
        if (min == max) {
            return min;
        }
        if (min > max) {
            int temp = max;
            max = min;
            min = temp;
        }
        return min + ((rnd.nextInt() >>> 1) % (max - min));
    }
}
