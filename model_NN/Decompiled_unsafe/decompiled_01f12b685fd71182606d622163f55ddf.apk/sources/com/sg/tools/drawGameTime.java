package com.sg.tools;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorNum;
import com.sg.pak.GameConstant;

public class drawGameTime implements GameConstant {
    ActorNum f;
    ActorNum m;
    ActorNum s;

    public void drawTime(int imgID, int x, int y, Group group, boolean ishaveS) {
        int w = numImage[imgID][0] + numImage[imgID][2];
        if (ishaveS) {
            this.s = new ActorNum(imgID, 0, 2, x, y, group);
            new ActorImage(numImage[imgID][3] + 10, (w * 2) + x, y, group);
        }
        this.f = new ActorNum(imgID, 0, 2, x + (w * 3), y, group);
        new ActorImage(numImage[imgID][3] + 10, (w * 5) + x, y, group);
        this.m = new ActorNum(imgID, 0, 2, x + (w * 6), y, group);
    }

    public void run(int time) {
        if (this.s != null) {
            this.s.setNum(time / 3600);
        }
        this.f.setNum((time / 60) % 60);
        this.m.setNum(time % 60);
    }
}
