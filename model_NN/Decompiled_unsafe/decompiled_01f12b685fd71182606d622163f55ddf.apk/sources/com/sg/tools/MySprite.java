package com.sg.tools;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kbz.Animation.GAnimationManager;
import com.kbz.Animation.GSimpleAnimation;
import com.kbz.Animation.GTools;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class MySprite extends GSimpleAnimation {
    /* access modifiers changed from: private */
    public boolean animationComplete;
    private float[] area;
    GAnimationManager.GAnimationCompleteListener c2;
    GAnimationManager.GAnimationCompleteListener cl;
    private int flag;
    private boolean isDraw;
    GameLayer layer;
    GAnimationManager.GAnimationCompleteListener removeListener;
    String spriteName;

    public String getSpriteName() {
        return this.spriteName;
    }

    public void setSpriteName(String spriteName2) {
        this.spriteName = spriteName2;
    }

    public MySprite() {
        this.c2 = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                MySprite.this.setAnimationComplete(true);
            }
        };
        this.removeListener = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                MySprite.this.removeStage();
            }
        };
        this.cl = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                boolean unused = MySprite.this.animationComplete = true;
            }
        };
    }

    public MySprite(String animationPack, String animationName) {
        super(animationPack, animationName);
        this.c2 = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                MySprite.this.setAnimationComplete(true);
            }
        };
        this.removeListener = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                MySprite.this.removeStage();
            }
        };
        this.cl = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                boolean unused = MySprite.this.animationComplete = true;
            }
        };
    }

    public MySprite(String animationPack, String animationName, int type) {
        this(animationPack, animationName, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, null, type);
    }

    public MySprite(String animationPack, String animationName, float x, float y, float[] area2, int type) {
        this.c2 = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                MySprite.this.setAnimationComplete(true);
            }
        };
        this.removeListener = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                MySprite.this.removeStage();
            }
        };
        this.cl = new GAnimationManager.GAnimationCompleteListener() {
            public void complete(Actor animation) {
                boolean unused = MySprite.this.animationComplete = true;
            }
        };
        GAnimationManager.load(animationPack);
        super.changeAnimation(animationPack, animationName);
        super.setTouchable(Touchable.disabled);
        setPosition(x, y);
        setType(type);
        if (this.area == null) {
            setArea(area2);
        }
    }

    public void removeStage() {
        GameStage.removeActor(this);
    }

    public void setRemoveListener() {
        setCompleteListener(this.removeListener);
    }

    public void setType(int type) {
        this.flag = type;
    }

    public int getType() {
        return this.flag;
    }

    public void setAnimationComplete(boolean b) {
        this.animationComplete = b;
    }

    public boolean getAnimationComplete() {
        return this.animationComplete;
    }

    public void setArea(float[] area2) {
        if (area2 == null) {
            setArea(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, getWidth(), getHeight());
        } else {
            setArea(area2[0], area2[1], area2[2], area2[3]);
        }
    }

    public void setArea(float offsetX, float offsetY, float width, float height) {
        this.area = new float[]{offsetX, offsetY, width, height};
    }

    public float[] getArea() {
        return this.area;
    }

    public float getArea(int index) {
        return this.area[index];
    }

    public void changeAnimation(String animationPack, String animationName, byte playMode, boolean isFastMode) {
        super.changeAnimation(animationPack, animationName, playMode, false);
        setCompleteListener(this.cl);
        this.animationComplete = false;
    }

    private void checkIsDraw() {
        if (!GTools.isDraw(getX() - getOriginX(), getY() - getOriginY(), getWidth(), getHeight(), 0)) {
            this.isDraw = false;
        } else {
            this.isDraw = true;
        }
    }

    public boolean isDraw() {
        return this.isDraw;
    }

    public void act(float delta) {
        if (isDraw()) {
            super.act(delta);
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        checkIsDraw();
        if (isDraw()) {
            super.draw(batch, parentAlpha);
        }
    }
}
