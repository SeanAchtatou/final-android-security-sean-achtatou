package com.sg.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kbz.esotericsoftware.spine.Animation;

public class MyProgressCircle extends Image {
    private static PolygonSpriteBatch polyBatch;
    static TextureRegion texture;
    Vector2 center = new Vector2(getWidth() / 2.0f, getHeight() / 2.0f);
    Vector2 centerTop = new Vector2(getWidth() / 2.0f, getHeight());
    float[] fv;
    IntersectAt intersectAt;
    Vector2 leftBottom = new Vector2(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    Vector2 leftTop = new Vector2(Animation.CurveTimeline.LINEAR, getHeight());
    Vector2 progressPoint = new Vector2(getWidth() / 2.0f, getHeight() / 2.0f);
    Vector2 rightBottom = new Vector2(getWidth(), Animation.CurveTimeline.LINEAR);
    Vector2 rightTop = new Vector2(getWidth(), getHeight());

    public enum IntersectAt {
        NONE,
        TOP,
        BOTTOM,
        LEFT,
        RIGHT
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MyProgressCircle(int r6) {
        /*
            r5 = this;
            r4 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            com.badlogic.gdx.graphics.g2d.TextureRegion r0 = com.kbz.tools.Tools.getImage(r6)
            com.sg.tools.MyProgressCircle.texture = r0
            r5.<init>(r0)
            com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch r0 = new com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
            r0.<init>()
            com.sg.tools.MyProgressCircle.polyBatch = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            float r1 = r5.getWidth()
            float r1 = r1 / r4
            float r2 = r5.getHeight()
            float r2 = r2 / r4
            r0.<init>(r1, r2)
            r5.center = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            float r1 = r5.getWidth()
            float r1 = r1 / r4
            float r2 = r5.getHeight()
            r0.<init>(r1, r2)
            r5.centerTop = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            float r1 = r5.getHeight()
            r0.<init>(r3, r1)
            r5.leftTop = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            r0.<init>(r3, r3)
            r5.leftBottom = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            float r1 = r5.getWidth()
            r0.<init>(r1, r3)
            r5.rightBottom = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            float r1 = r5.getWidth()
            float r2 = r5.getHeight()
            r0.<init>(r1, r2)
            r5.rightTop = r0
            com.badlogic.gdx.math.Vector2 r0 = new com.badlogic.gdx.math.Vector2
            float r1 = r5.getWidth()
            float r1 = r1 / r4
            float r2 = r5.getHeight()
            float r2 = r2 / r4
            r0.<init>(r1, r2)
            r5.progressPoint = r0
            r5.setPercentage(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sg.tools.MyProgressCircle.<init>(int):void");
    }

    private Vector2 IntersectPoint(Vector2 line) {
        Vector2 v = new Vector2();
        if (Intersector.intersectSegments(this.leftTop, this.rightTop, this.center, line, v)) {
            this.intersectAt = IntersectAt.TOP;
            return v;
        } else if (Intersector.intersectSegments(this.leftBottom, this.rightBottom, this.center, line, v)) {
            this.intersectAt = IntersectAt.BOTTOM;
            return v;
        } else if (Intersector.intersectSegments(this.leftTop, this.leftBottom, this.center, line, v)) {
            this.intersectAt = IntersectAt.LEFT;
            return v;
        } else if (Intersector.intersectSegments(this.rightTop, this.rightBottom, this.center, line, v)) {
            this.intersectAt = IntersectAt.RIGHT;
            return v;
        } else {
            this.intersectAt = IntersectAt.NONE;
            return null;
        }
    }

    public void setPercentage(float percent) {
        float len;
        float angle = convertToRadians(90.0f) + convertToRadians((360.0f * percent) / 100.0f);
        if (getWidth() > getHeight()) {
            len = getWidth();
        } else {
            len = getHeight();
        }
        float dy = (float) (Math.sin((double) angle) * ((double) len));
        Vector2 v = IntersectPoint(new Vector2(this.center.x + ((float) (Math.cos((double) angle) * ((double) len))), this.center.y + dy));
        if (this.intersectAt == IntersectAt.TOP) {
            if (v.x >= getWidth() / 2.0f) {
                this.fv = new float[]{this.center.x, this.center.y, this.centerTop.x, this.centerTop.y, this.leftTop.x, this.leftTop.y, this.leftBottom.x, this.leftBottom.y, this.rightBottom.x, this.rightBottom.y, this.rightTop.x, this.rightTop.y, v.x, v.y};
                return;
            }
            this.fv = new float[]{this.center.x, this.center.y, this.centerTop.x, this.centerTop.y, v.x, v.y};
        } else if (this.intersectAt == IntersectAt.BOTTOM) {
            this.fv = new float[]{this.center.x, this.center.y, this.centerTop.x, this.centerTop.y, this.leftTop.x, this.leftTop.y, this.leftBottom.x, this.leftBottom.y, v.x, v.y};
        } else if (this.intersectAt == IntersectAt.LEFT) {
            this.fv = new float[]{this.center.x, this.center.y, this.centerTop.x, this.centerTop.y, this.leftTop.x, this.leftTop.y, v.x, v.y};
        } else if (this.intersectAt == IntersectAt.RIGHT) {
            this.fv = new float[]{this.center.x, this.center.y, this.centerTop.x, this.centerTop.y, this.leftTop.x, this.leftTop.y, this.leftBottom.x, this.leftBottom.y, this.rightBottom.x, this.rightBottom.y, v.x, v.y};
        } else {
            this.fv = null;
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.fv != null) {
            batch.end();
            drawMe();
            batch.begin();
        }
    }

    private void drawMe() {
        PolygonSprite poly = new PolygonSprite(new PolygonRegion(texture, this.fv, new EarClippingTriangulator().computeTriangles(this.fv).toArray()));
        poly.setOrigin(getOriginX(), getOriginY());
        poly.setPosition(getX(), getY());
        poly.setRotation(getRotation());
        poly.setColor(getColor());
        poly.setScale(((float) Gdx.graphics.getWidth()) / 800.0f, ((float) Gdx.graphics.getHeight()) / 480.0f);
        polyBatch.begin();
        poly.draw(polyBatch);
        polyBatch.end();
    }

    public void setPosition(float x, float y) {
        setX((((float) Gdx.graphics.getWidth()) / 800.0f) * x);
        setY((((float) Gdx.graphics.getHeight()) - (getHeight() * (((float) Gdx.graphics.getHeight()) / 480.0f))) - ((((float) Gdx.graphics.getHeight()) / 480.0f) * y));
    }

    private float convertToRadians(float angleInDegrees) {
        return angleInDegrees * 0.017453292f;
    }
}
