package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GSimpleAction;
import com.kbz.Actors.SimpleButton;
import com.kbz.Particle.GameParticle;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.record.Bear;
import com.sg.td.record.LoadBear;
import com.sg.td.spine.RWkaweili;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyUpgradeNoOPen extends MyGroup {
    static Actor effect1;
    static int[] role = {PAK_ASSETS.IMG_ZHU025, PAK_ASSETS.IMG_ZHU027, PAK_ASSETS.IMG_ZHU028, PAK_ASSETS.IMG_ZHU026};
    static int roleId = 0;
    static int[] roleplay = {13, 12, 15, 14};
    static Group upOngroup;
    ActorImage attack;
    ActorClipImage bar;
    float curretatk = Animation.CurveTimeline.LINEAR;
    int levelNum;
    GameParticle libaop;
    int money;
    String rolename = "";
    float speed = 0.1f;
    private float timeparLength;
    private float timeparLength1;

    public void init() {
        if (upOngroup != null) {
            free();
        }
        upOngroup = new Group();
        upOngroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(upOngroup, 4);
        upOngroup.addActor(new Mask());
    }

    public MyUpgradeNoOPen(int id, boolean isFull, int jiemian, int level) {
        switch (id) {
            case 0:
                this.rolename = "Bear1";
                break;
            case 1:
                this.rolename = "Bear2";
                break;
            case 2:
                this.rolename = "Bear3";
                break;
            case 3:
                this.rolename = "Bear4";
                break;
        }
        Bear bear = LoadBear.bearData.get(this.rolename);
        roleId = id;
        initbj(id);
        initSelectRolegruop(bear);
        initData(bear, roleId, isFull);
        initbutton(isFull, level);
        initlistener(jiemian, level);
    }

    /* access modifiers changed from: package-private */
    public void initbj(int id) {
        GameUITools.GetbackgroundImage(320, 168, 4, upOngroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 138, 1, upOngroup);
        new ActorImage(role[id], 320, 108, 1, upOngroup);
    }

    /* access modifiers changed from: package-private */
    public void initSelectRolegruop(Bear bear) {
        float Scale = 0.4f;
        if (roleId == 0) {
            Scale = 0.5f;
        }
        upOngroup.addActor(new RWkaweili(140, PAK_ASSETS.IMG_DAJI10, roleplay[roleId], Scale));
        new ActorImage((int) PAK_ASSETS.IMG_UP003, (int) PAK_ASSETS.IMG_ICE70, (int) PAK_ASSETS.IMG_KAWEILI05, 1, upOngroup);
        if (!Rank.isRanking()) {
            ActorButton left = new ActorButton((int) PAK_ASSETS.IMG_ZHU008, "1", -5, 300, 12, upOngroup);
            ActorButton right = new ActorButton((int) PAK_ASSETS.IMG_ZHU009, Constant.S_C, (int) PAK_ASSETS.IMG_E01, 300, 12, upOngroup);
            left.setScale(0.6f, 0.6f);
            right.setScale(0.6f, 0.6f);
        }
    }

    /* access modifiers changed from: package-private */
    public void initData(Bear bear, int id, boolean isFull) {
        if (isFull) {
            this.levelNum = RankData.roleLevel[id];
        } else {
            this.levelNum = RankData.roleLevel[id] + 1;
        }
        int gongji = bear.getCurAttack(this.levelNum);
        int bulletNum = bear.getBullet();
        String rangeNum = bear.getRange();
        String abilityNum = bear.getBuff();
        String stuntNum = bear.getSkill();
        int atkNumfull = bear.getFullAttack();
        this.money = bear.getBuyDiamond();
        System.out.println("meney::" + this.money);
        new ActorImage((int) PAK_ASSETS.IMG_UP023, (int) PAK_ASSETS.IMG_SHENGJITIPS06, 193, 12, upOngroup);
        GNumSprite level = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + this.levelNum, "", -2, 4);
        level.setPosition(400.0f, 208.0f);
        upOngroup.addActor(level);
        ActorImage attack2 = new ActorImage((int) PAK_ASSETS.IMG_UP005, (int) PAK_ASSETS.IMG_PPSG04, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, upOngroup);
        this.bar = new ActorClipImage(PAK_ASSETS.IMG_UP006, PAK_ASSETS.IMG_PPSG04, PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, upOngroup);
        this.bar.setVisible(false);
        this.curretatk = ((float) gongji) / ((float) atkNumfull);
        this.timeparLength1 = attack2.getWidth();
        timeparAction();
        this.timeparLength = Animation.CurveTimeline.LINEAR;
        new ActorText(gongji + "/" + atkNumfull, 412, (int) PAK_ASSETS.IMG_SHUOMINGZI03, 12, upOngroup);
        new ActorText(bulletNum + "", (int) PAK_ASSETS.IMG_SHENGJITIPS06, (int) PAK_ASSETS.IMG_DAJI05, 12, upOngroup);
        new ActorText(rangeNum, (int) PAK_ASSETS.IMG_SHENGJITIPS06, (int) PAK_ASSETS.IMG_REAPDAOJU01A, 12, upOngroup);
        new ActorText(abilityNum, (int) PAK_ASSETS.IMG_UI_TILI02, (int) PAK_ASSETS.IMG_LIGHTD02, 12, upOngroup);
        new ActorText(stuntNum, (int) PAK_ASSETS.IMG_SHENGJITIPS06, 415, 12, upOngroup);
        if (!isFull) {
            if (id != 3) {
                new ActorImage((int) PAK_ASSETS.IMG_SHOP043, 40, (int) PAK_ASSETS.IMG_MAP4, 12, upOngroup);
                GNumSprite boxNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC012, this.money + "", "X", -2, 4);
                boxNum.setPosition((float) 147, (float) PAK_ASSETS.IMG_GONGGAO010);
                upOngroup.addActor(boxNum);
            }
            if (id == 3) {
                new ActorText("￥", 105, (int) PAK_ASSETS.IMG_PAO005, 4, upOngroup).setFontScaleXY(1.5f);
                GNumSprite rmb = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC012, "20", "X", -2, 4);
                rmb.setPosition((float) 140, (float) PAK_ASSETS.IMG_ONLINE001);
                upOngroup.addActor(rmb);
            }
        }
    }

    private void initbutton(boolean isFull, int level) {
        SimpleButton call;
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 120, 12, upOngroup);
        int[] tt = {PAK_ASSETS.IMG_UP019, PAK_ASSETS.IMG_UP020};
        int[] tt1 = {PAK_ASSETS.IMG_UP017, PAK_ASSETS.IMG_UP018};
        int[] tt2 = {PAK_ASSETS.IMG_UP021, PAK_ASSETS.IMG_UP022};
        if (MyGameMap.isInMap) {
            if (level > 0) {
                call = new SimpleButton(250, PAK_ASSETS.IMG_CHUANGGUAN001, 1, tt2);
            } else {
                call = new SimpleButton(250, PAK_ASSETS.IMG_CHUANGGUAN001, 1, tt);
            }
        } else if (isFull) {
            call = new SimpleButton(250, PAK_ASSETS.IMG_CHUANGGUAN001, 1, tt1);
        } else {
            call = new SimpleButton(250, PAK_ASSETS.IMG_CHUANGGUAN001, 1, tt);
        }
        call.setName(Constant.S_D);
        upOngroup.addActor(call);
    }

    /* access modifiers changed from: package-private */
    public void initlistener(final int jiemian, final int level) {
        upOngroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    System.out.println("buttonName:" + buttonName);
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                        System.out.println("codeName:" + codeName);
                    } else {
                        codeName = 9;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            Rank.clearRoleUpEvent();
                            if (jiemian == 0) {
                                System.out.println("+++1");
                                MyUpgradeNoOPen.this.free();
                                if (Mymainmenu2.isZhuToUp) {
                                    Mymainmenu2.isZhuToUp = false;
                                    Mymainmenu2.selectIndex = MyUpgradeNoOPen.roleId;
                                    GameMain.toScreen(new Mymainmenu2());
                                    System.out.println("+++2");
                                    return;
                                }
                                return;
                            }
                            System.out.println("+++3");
                            MyUpgradeNoOPen.this.free();
                            new MyEquipment();
                            return;
                        case 1:
                            MyUpgradeNoOPen.this.free();
                            Sound.playSound(17);
                            if (MyGameMap.isInMap) {
                                MyUpgradeNoOPen.roleId = MyUpgradeInMap.getSelectIndexInMap(true);
                            } else {
                                MyUpgradeNoOPen.roleId = Mymainmenu2.getSelectIndex(true);
                            }
                            if (!MyGameMap.isInMap) {
                                new MyUpgrade(MyUpgradeNoOPen.roleId);
                                return;
                            } else if (jiemian == 0) {
                                new MyUpgradeInMap(MyUpgradeNoOPen.roleId, 0);
                                return;
                            } else {
                                new MyUpgradeInMap(MyUpgradeNoOPen.roleId, 1);
                                return;
                            }
                        case 2:
                            MyUpgradeNoOPen.this.free();
                            Sound.playSound(17);
                            if (MyGameMap.isInMap) {
                                MyUpgradeNoOPen.roleId = MyUpgradeInMap.getSelectIndexInMap(false);
                            } else {
                                MyUpgradeNoOPen.roleId = Mymainmenu2.getSelectIndex(false);
                            }
                            if (!MyGameMap.isInMap) {
                                new MyUpgrade(MyUpgradeNoOPen.roleId);
                                return;
                            } else if (jiemian == 0) {
                                new MyUpgradeInMap(MyUpgradeNoOPen.roleId, 0);
                                return;
                            } else {
                                new MyUpgradeInMap(MyUpgradeNoOPen.roleId, 1);
                                return;
                            }
                        case 3:
                            if (MyGameMap.isInMap) {
                                if (jiemian == 0) {
                                    if (level > 0) {
                                        System.out.println("确认选择：" + MyUpgradeNoOPen.roleId);
                                        Mymainmenu2.userChoseIndex = MyUpgradeNoOPen.roleId;
                                        System.out.println("Mymainmenu2.selectIndex：" + Mymainmenu2.selectIndex);
                                        GameStage.clearAllLayers();
                                        GameMain.toScreen(new MyGameMap());
                                        return;
                                    }
                                    MyUpgradeNoOPen.this.zhaohuantiaojian(MyUpgradeNoOPen.roleId);
                                    return;
                                } else if (level > 0) {
                                    MyUpgradeNoOPen.this.free();
                                    Mymainmenu2.userChoseIndex = MyUpgradeNoOPen.roleId;
                                    new MyEquipment();
                                    return;
                                } else {
                                    MyUpgradeNoOPen.this.zhaohuantiaojian(MyUpgradeNoOPen.roleId);
                                    return;
                                }
                            } else if (level > 0) {
                                MyUpgradeNoOPen.this.free();
                                if (Mymainmenu2.isZhuToUp) {
                                    Mymainmenu2.isZhuToUp = false;
                                    Mymainmenu2.selectIndex = MyUpgradeNoOPen.roleId;
                                    GameMain.toScreen(new Mymainmenu2());
                                    System.out.println("+++3");
                                }
                                Rank.clearRoleUpEvent();
                                return;
                            } else {
                                MyUpgradeNoOPen.this.zhaohuantiaojian(MyUpgradeNoOPen.roleId);
                                return;
                            }
                        default:
                            return;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void zhaohuantiaojian(int id) {
        switch (id) {
            case 1:
            case 2:
                if (RankData.spendDiamond(this.money)) {
                    zhaohuan();
                    return;
                } else if (MySwitch.isCaseA == 0) {
                    MyTip.Notenought(false);
                    return;
                } else {
                    new MyGift(7);
                    return;
                }
            case 3:
                new MyGift(11);
                return;
            default:
                return;
        }
    }

    public static void zhaohuan() {
        Sound.playSound(71);
        RankData.bearOpen(roleId);
        RankData.setLastSelectRoleIndex(roleId);
        effect1 = new Effect().getEffect_Diejia(90, 140, PAK_ASSETS.IMG_UI_ZHUANPAN006);
        if (upOngroup == null) {
            upOngroup = new Group();
        }
        upOngroup.addActor(effect1);
        upOngroup.addAction(getCountAction(2, upOngroup));
    }

    public static Action getCountAction(final int time, final Group group) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    System.out.println("clean1");
                    group.remove();
                    group.clear();
                    GameStage.removeActor(group);
                    if (Rank.isRanking()) {
                        new MyUpgrade(MyUpgradeNoOPen.roleId);
                    } else if (MyGameMap.isInMap) {
                        new MyUpgradeInMap(MyUpgradeNoOPen.roleId, 0);
                    } else {
                        new MyUpgrade(MyUpgradeNoOPen.roleId);
                    }
                }
                return true;
            }
        });
    }

    private void timeparAction() {
        this.timeparLength = this.curretatk * this.timeparLength1;
        System.out.println("dd:" + this.timeparLength);
        this.bar.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.timeparLength, this.bar.getHeight());
        this.bar.setVisible(true);
    }

    public void exit() {
        upOngroup.remove();
        upOngroup.clear();
    }
}
