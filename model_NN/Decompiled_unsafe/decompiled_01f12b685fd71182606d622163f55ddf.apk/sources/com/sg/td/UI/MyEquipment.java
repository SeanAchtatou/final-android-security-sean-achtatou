package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.Particle.GameParticle;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIGetFont;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.td.spine.RoleSpineInEquip;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.NumActor;
import com.sg.util.GameStage;

public class MyEquipment extends MyGroup {
    static Group equipgruop;
    static int state;
    static int state1;
    static int state2;
    static int state3;
    static int state4;
    ActorImage HideTowerUP_image;
    GNumSprite Num1;
    GNumSprite Num2;
    GNumSprite Num3;
    GNumSprite Num4;
    int Num_1;
    int Num_2;
    int Num_3;
    int Num_4;
    GNumSprite bearcion;
    ActorButton buchong;
    ActorButton close;
    String[] condition_1 = {"123", ""};
    ActorSprite dImage1;
    ActorSprite dImage2;
    ActorSprite dImage3;
    ActorSprite dImage4;
    Effect daoju;
    Group datagroup;
    Effect effectGet;
    ActorImage frame_1;
    ActorImage frame_2;
    ActorImage frame_3;
    ActorImage frame_4;
    ActorImage frame_5;
    ActorShapeSprite gShapeSprite;
    SimpleButton go;
    boolean isFlag;
    boolean isTouch1;
    GameParticle libaop;
    RoleSpineInEquip mapLayerRole;
    ActorImage percentage;
    Group role;
    ActorButton shuoming_1;
    float speed = 0.01f;
    ActorImage star_1;
    ActorImage star_2;
    ActorImage star_3;
    ActorClipImage timebar;
    private float timeparLength;
    private float timeparLengthall;
    ActorImage title_1;
    ActorImage title_2;
    int upNum;
    Group yuan;

    public void init() {
        RankData.teach.giveWatchProp();
        new Mask();
        if (equipgruop != null) {
            equipgruop.remove();
            equipgruop.clear();
        }
        equipgruop = new Group() {
            public void act(float delta) {
                super.act(delta);
                int bearcionNum = RankData.getCakeNum();
                if (bearcionNum > 99999) {
                    bearcionNum = 99999;
                }
                MyEquipment.this.bearcion.setNum(bearcionNum);
            }
        };
        RankData.initProp(Rank.rank);
        GameStage.addActor(equipgruop, 4);
        equipgruop.addActor(new Mask());
        RankData.initRankData(Rank.rank);
        initbj();
        initframe1();
        initframeData();
        initbutton();
        getlistener();
        grouplistener();
        RankData.teach.addEquipTeach();
    }

    public Group getgroup(boolean isflag) {
        System.out.println("isflag:" + isflag);
        if (isflag) {
            initframeData();
            this.isFlag = false;
        } else {
            initframeShuoming();
            this.isFlag = true;
        }
        return this.datagroup;
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 148, 12, equipgruop, false, false);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 118, 1, equipgruop);
        this.title_2 = new ActorImage(86, 320, 88, 1, equipgruop);
    }

    /* access modifiers changed from: package-private */
    public void initframe1() {
        this.frame_1 = new ActorImage(95, 320, (int) PAK_ASSETS.IMG_BOOM01BD, 1, equipgruop);
        this.daoju = new Effect();
        this.daoju.addEffect_Diejia(69, 320, (int) PAK_ASSETS.IMG_E03, 4);
        this.shuoming_1 = new ActorButton(89, "1", (int) PAK_ASSETS.IMG_PUBLIC011, 170, 12, equipgruop);
        Group groupwhy = new Group();
        groupwhy.setPosition(542.0f, 160.0f);
        groupwhy.setSize(80.0f, 69.0f);
        equipgruop.addActor(groupwhy);
        groupwhy.setTouchable(Touchable.enabled);
        groupwhy.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Sound.playButtonPressed();
                MyEquipment.this.getgroup(MyEquipment.this.isFlag);
            }
        });
        this.buchong = new ActorButton(100, Constant.S_C, 195, 170, 12, equipgruop);
        initbeardata();
        dawNum();
    }

    /* access modifiers changed from: package-private */
    public void dawNum() {
        if (this.yuan != null) {
            this.yuan.remove();
            this.yuan.clear();
        }
        int isHave1 = RankData.getPropsNum(0);
        int isHave2 = RankData.getPropsNum(1);
        int isHave3 = RankData.getPropsNum(2);
        int isHave4 = RankData.getPropsNum(3);
        System.out.println("-----1是否选中：" + RankData.isSelectUseProp(0));
        if (RankData.isSelectUseProp(0)) {
            isHave1--;
        }
        if (RankData.isSelectUseProp(1)) {
            isHave2--;
        }
        if (RankData.isSelectUseProp(2)) {
            isHave3--;
        }
        if (RankData.isSelectUseProp(3)) {
            isHave4--;
        }
        this.yuan = new Group();
        int Num_12 = isHave1;
        this.frame_2 = new ActorImage(96, 48, 272, 1, this.yuan);
        this.Num1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + Num_12, "X", -2, 4);
        this.Num1.setPosition((float) 48, (float) PAK_ASSETS.IMG_BISHATIPSPUR);
        if (Num_12 == 0) {
            this.Num1.setVisible(false);
            this.frame_2.setVisible(false);
        }
        this.yuan.addActor(this.Num1);
        this.dImage1 = new ActorSprite(40, (int) PAK_ASSETS.IMG_UI_A29, 4, 97, 98, 99);
        this.yuan.addActor(this.dImage1);
        state1 = judgeImageState(isHave1, this.dImage1, 0);
        int Num_22 = isHave2;
        this.frame_3 = new ActorImage(96, 190, 272, 1, this.yuan);
        this.Num2 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + Num_22, "X", -2, 4);
        this.Num2.setPosition((float) 190, (float) PAK_ASSETS.IMG_BISHATIPSPUR);
        if (Num_22 == 0) {
            this.Num2.setVisible(false);
            this.frame_3.setVisible(false);
        }
        this.yuan.addActor(this.Num2);
        this.dImage2 = new ActorSprite(185, (int) PAK_ASSETS.IMG_UI_A29, 4, 97, 98, 99);
        this.yuan.addActor(this.dImage2);
        state2 = judgeImageState(isHave2, this.dImage2, 1);
        int Num_32 = isHave3;
        this.frame_4 = new ActorImage(96, (int) PAK_ASSETS.IMG_READYGO01, 272, 1, this.yuan);
        this.Num3 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + Num_32, "X", -2, 4);
        this.Num3.setPosition((float) PAK_ASSETS.IMG_READYGO01, (float) PAK_ASSETS.IMG_BISHATIPSPUR);
        if (Num_32 == 0) {
            this.Num3.setVisible(false);
            this.frame_4.setVisible(false);
        }
        this.yuan.addActor(this.Num3);
        this.dImage3 = new ActorSprite((int) PAK_ASSETS.IMG_PPSG04, (int) PAK_ASSETS.IMG_UI_A29, 4, 97, 98, 99);
        this.yuan.addActor(this.dImage3);
        state3 = judgeImageState(isHave3, this.dImage3, 2);
        int Num_42 = isHave4;
        this.frame_5 = new ActorImage(96, (int) PAK_ASSETS.IMG_ZHUANPAN003, 272, 1, this.yuan);
        this.Num4 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + Num_42, "X", -2, 4);
        this.Num4.setPosition((float) PAK_ASSETS.IMG_ZHUANPAN003, (float) PAK_ASSETS.IMG_BISHATIPSPUR);
        if (Num_42 == 0) {
            this.Num4.setVisible(false);
            this.frame_5.setVisible(false);
        }
        this.yuan.addActor(this.Num4);
        this.dImage4 = new ActorSprite((int) PAK_ASSETS.IMG_ZHUANPAN006, (int) PAK_ASSETS.IMG_UI_A29, 4, 97, 98, 99);
        this.yuan.addActor(this.dImage4);
        state4 = judgeImageState(isHave4, this.dImage4, 3);
        System.out.println("state1:" + state1);
        System.out.println("state2:" + state2);
        System.out.println("state3:" + state3);
        System.out.println("state4:" + state4);
        equipgruop.addActor(this.yuan);
    }

    /* access modifiers changed from: package-private */
    public void dawNumxin(int id) {
        int propsNum = RankData.getPropsNum(0);
        int propsNum2 = RankData.getPropsNum(1);
        int propsNum3 = RankData.getPropsNum(2);
        int propsNum4 = RankData.getPropsNum(3);
        this.yuan = new Group();
        switch (id) {
            case 1:
                this.frame_2.setVisible(true);
                this.Num1.setNum(1);
                this.Num1.setVisible(true);
                break;
            case 2:
                this.frame_3.setVisible(true);
                this.Num2.setNum(1);
                this.Num2.setVisible(true);
                break;
            case 3:
                this.frame_4.setVisible(true);
                this.Num3.setNum(1);
                this.Num3.setVisible(true);
                break;
            case 4:
                this.frame_5.setVisible(true);
                this.Num4.setNum(1);
                this.Num4.setVisible(true);
                break;
        }
        equipgruop.addActor(this.yuan);
    }

    /* access modifiers changed from: package-private */
    public int judgeImageState(int num, ActorSprite dImage, int id) {
        System.out.println("-----是否选中：" + id + RankData.isSelectUseProp(id));
        if (!RankData.isSelectUseProp(id)) {
            if (num == 0) {
                state = 0;
            } else {
                state = 1;
            }
            switch (state) {
                case -1:
                    dImage.setTexture(2);
                    state = -1;
                    break;
                case 0:
                    dImage.setTexture(0);
                    break;
                case 1:
                    dImage.setTexture(1);
                    break;
            }
        } else {
            dImage.setTexture(2);
            state = -1;
        }
        return state;
    }

    /* access modifiers changed from: package-private */
    public int judgeImageStateH(int num, ActorSprite dImage, int id) {
        switch (id) {
            case 1:
                state = state1;
                break;
            case 2:
                state = state2;
                break;
            case 3:
                state = state3;
                break;
            case 4:
                state = state4;
                break;
        }
        return state;
    }

    /* access modifiers changed from: package-private */
    public void dawNumTouchBian(int id, int num, int state5) {
        int num1 = 0;
        if (state5 == 1) {
            num1 = num;
        } else if (state5 == -1) {
            num1 = num - 1;
        }
        System.out.println("num::" + num);
        System.out.println("num1::" + num1);
        switch (id) {
            case 1:
                if (num1 > 0) {
                    this.frame_2.setVisible(true);
                    this.Num1.setNum(num1 + "");
                    this.Num1.setVisible(true);
                    return;
                }
                this.frame_2.setVisible(false);
                this.Num1.setVisible(false);
                return;
            case 2:
                if (num1 > 0) {
                    this.frame_3.setVisible(true);
                    this.Num2.setNum(num1);
                    this.Num2.setVisible(true);
                    return;
                }
                this.frame_3.setVisible(false);
                this.Num2.setVisible(false);
                return;
            case 3:
                if (num1 > 0) {
                    this.frame_4.setVisible(true);
                    this.Num3.setNum(num1);
                    this.Num3.setVisible(true);
                    return;
                }
                this.frame_4.setVisible(false);
                this.Num3.setVisible(false);
                return;
            case 4:
                if (num1 > 0) {
                    this.frame_5.setVisible(true);
                    this.Num4.setNum(num1);
                    this.Num4.setVisible(true);
                    return;
                }
                this.frame_5.setVisible(false);
                this.Num4.setVisible(false);
                return;
            default:
                return;
        }
    }

    public void isTouch(int num, ActorSprite dImage, boolean istouch, int stateimge, int index) {
        Get get = LoadGet.getData.get("Shop");
        int num2 = RankData.getPropsNum(index - 1);
        if (istouch) {
            if (stateimge == 1) {
                stateimge = -1;
            } else if (stateimge == -1) {
                stateimge = 1;
            }
        }
        System.out.println("stateimge1:" + stateimge);
        switch (stateimge) {
            case -1:
                System.out.println("已经选择");
                Sound.playButtonPressed();
                RankData.selectUseProp(index - 1);
                dImage.setTexture(2);
                break;
            case 0:
                System.out.println("点击购买");
                if (!RankData.buyProp(index - 1, 400)) {
                    Sound.playButtonPressed();
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(10);
                        break;
                    } else {
                        MyTip.Notenought(true);
                        break;
                    }
                } else {
                    Sound.playSound(16);
                    dImage.setTexture(2);
                    stateimge = -1;
                    RankData.selectUseProp(index - 1);
                    num2 = RankData.getPropsNum(index - 1);
                    break;
                }
            case 1:
                System.out.println("点击选择");
                RankData.selectUseProp(index - 1);
                Sound.playButtonPressed();
                dImage.setTexture(1);
                break;
        }
        System.out.println("stateimge:" + stateimge);
        if (stateimge == 1 || stateimge == -1) {
            dawNumTouchBian(index, num2, stateimge);
        }
        switch (index) {
            case 1:
                state1 = stateimge;
                return;
            case 2:
                state2 = stateimge;
                return;
            case 3:
                state3 = stateimge;
                return;
            case 4:
                state4 = stateimge;
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void getlistener() {
        final int isHave1 = RankData.getPropsNum(0);
        final int isHave2 = RankData.getPropsNum(1);
        final int isHave3 = RankData.getPropsNum(2);
        final int isHave4 = RankData.getPropsNum(3);
        Group group1 = new Group();
        group1.setPosition(55.0f, 258.0f);
        group1.setSize(126.0f, 168.0f);
        equipgruop.addActor(group1);
        group1.setTouchable(Touchable.enabled);
        group1.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("zu1");
                MyEquipment.this.isTouch1 = true;
                System.out.println("touch:" + isHave1);
                MyEquipment.this.isTouch(isHave1, MyEquipment.this.dImage1, MyEquipment.this.isTouch1, MyEquipment.this.judgeImageStateH(isHave1, MyEquipment.this.dImage1, 1), 1);
            }
        });
        Group group2 = new Group();
        group2.setPosition(193.0f, 258.0f);
        group2.setSize(126.0f, 168.0f);
        equipgruop.addActor(group2);
        group2.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyEquipment.this.isTouch1 = true;
                System.out.println("touch:" + isHave2);
                MyEquipment.this.isTouch(isHave2, MyEquipment.this.dImage2, MyEquipment.this.isTouch1, MyEquipment.this.judgeImageStateH(isHave2, MyEquipment.this.dImage2, 2), 2);
            }
        });
        Group group3 = new Group();
        group3.setPosition(333.0f, 258.0f);
        group3.setSize(126.0f, 168.0f);
        equipgruop.addActor(group3);
        group3.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyEquipment.this.isTouch1 = true;
                MyEquipment.this.isTouch(isHave3, MyEquipment.this.dImage3, MyEquipment.this.isTouch1, MyEquipment.this.judgeImageStateH(isHave3, MyEquipment.this.dImage3, 3), 3);
            }
        });
        Group group4 = new Group();
        group4.setPosition(475.0f, 258.0f);
        group4.setSize(126.0f, 168.0f);
        equipgruop.addActor(group4);
        group4.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyEquipment.this.isTouch1 = true;
                MyEquipment.this.isTouch(isHave4, MyEquipment.this.dImage4, MyEquipment.this.isTouch1, MyEquipment.this.judgeImageStateH(isHave4, MyEquipment.this.dImage4, 4), 4);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void initframeData() {
        if (this.datagroup != null) {
            this.datagroup.remove();
            this.datagroup.clear();
        }
        this.datagroup = new Group() {
            public void act(float delta) {
                super.act(delta);
            }
        };
        String tiaojian = RankData.questIntro;
        String[] power = RankData.rankTowerIcons;
        String[] powerhide = RankData.hideTowerIcons;
        String[] powerName = RankData.towerNames;
        int time2 = RankData.time2;
        int time3 = RankData.time3;
        equipgruop.addActor(this.datagroup);
        new ActorImage(101, 320, (int) PAK_ASSETS.IMG_SHOP011, 1, this.datagroup);
        if (RankData.drawRankInfo(Rank.rank)) {
            new ActorImage(108, (int) PAK_ASSETS.IMG_ICE70, (int) PAK_ASSETS.IMG_TIP007, 1, this.datagroup);
        }
        new ActorImage(102, (int) PAK_ASSETS.IMG_PPSG04, (int) PAK_ASSETS.IMG_CHUANGGUAN001, 1, this.datagroup);
        new ActorImage(102, (int) PAK_ASSETS.IMG_MAP14, (int) PAK_ASSETS.IMG_CHUANGGUAN001, 1, this.datagroup);
        NumActor min1 = new NumActor();
        min1.setPosition((float) PAK_ASSETS.IMG_DAJI07A, (float) PAK_ASSETS.IMG_CHUANGGUAN001);
        min1.set(103, time2, -3, 4);
        this.datagroup.addActor(min1);
        NumActor min2 = new NumActor();
        min2.setPosition((float) PAK_ASSETS.IMG_LOAD2, (float) PAK_ASSETS.IMG_CHUANGGUAN001);
        min2.set(103, time3, -3, 4);
        this.datagroup.addActor(min2);
        this.timebar = new ActorClipImage(90, 106, PAK_ASSETS.IMG_PUBLIC014, 12, this.datagroup);
        this.timebar.setVisible(false);
        this.timeparLength = Animation.CurveTimeline.LINEAR;
        this.timeparLengthall = this.timebar.getWidth();
        ActorText guanka = new ActorText("第" + Rank.rank + "关:", -74, (int) PAK_ASSETS.IMG_JIESUO011, 1, this.datagroup);
        guanka.setWidth(400);
        guanka.setFontScaleXY(1.3f);
        guanka.setColor(MyUIGetFont.maintopdataColor);
        ActorText guoguantiaojian = new ActorText(tiaojian, 320, (int) PAK_ASSETS.IMG_SHOP011, 1, this.datagroup);
        guoguantiaojian.setWidth(PAK_ASSETS.IMG_G02);
        guoguantiaojian.setFontScaleXY(1.3f);
        guoguantiaojian.setPosition((float) 320, (float) PAK_ASSETS.IMG_SHOP008);
        int all = power.length + powerhide.length;
        for (int i = 0; i < power.length; i++) {
            int xx = 0;
            switch (all) {
                case 1:
                    xx = 140;
                    break;
                case 2:
                    xx = 110;
                    break;
                case 3:
                    xx = 72;
                    break;
                case 4:
                    xx = 33;
                    break;
            }
            System.out.println(power[i]);
            ActorImage powers = new ActorImage(power[i], xx + PAK_ASSETS.IMG_ZUQIU_SHUOMING + (i * 70), (int) PAK_ASSETS.IMG_ZHU001, 1, this.datagroup);
            powers.setScale(0.7f, 0.7f);
            powers.setTouchable(Touchable.enabled);
            powers.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    if (MyEquipment.this.upNum > 0) {
                        MyEquipment.this.effectGet.remove_Diejia();
                        MyEquipment.this.daoju.remove_Diejia();
                        new MyForceHitOut();
                    }
                }
            });
            String towerName = powerName[i];
            ActorImage UP_image = null;
            if (RankData.towerCanStrength(towerName)) {
                UP_image = new ActorImage((int) PAK_ASSETS.IMG_PAO003, xx + PAK_ASSETS.IMG_BUFF_JINBI + (i * 70), (int) PAK_ASSETS.IMG_UP016, 1, this.datagroup);
                UP_image.setScale(0.8f, 0.8f);
                this.upNum = this.upNum + 1;
                if (i > 4) {
                    UP_image.setPosition((float) (((i - 5) * 65) + 260), (float) PAK_ASSETS.IMG_CHONGZHI007);
                }
            }
            if (all > 5) {
                if (i >= 0 && i <= 4) {
                    powers.setPosition((float) (xx + 206 + (i * 70)), (float) PAK_ASSETS.IMG_SHOP028);
                    if (RankData.towerCanStrength(towerName)) {
                        UP_image.setPosition((float) (xx + 260 + (i * 70)), (float) PAK_ASSETS.IMG_SHOP036);
                    }
                }
                if (i > 4) {
                    powers.setPosition((float) (xx + 206 + ((i - 5) * 70)), (float) PAK_ASSETS.IMG_UP022);
                    if (RankData.towerCanStrength(towerName)) {
                        UP_image.setPosition((float) (((i - 5) * 65) + 260), (float) PAK_ASSETS.IMG_CHONGZHI007);
                    }
                }
            }
        }
        for (int i2 = 0; i2 < powerhide.length; i2++) {
            int xx2 = 0;
            switch (power.length + powerhide.length) {
                case 1:
                    xx2 = 140;
                    break;
                case 2:
                    xx2 = 110;
                    break;
                case 3:
                    xx2 = 72;
                    break;
                case 4:
                    xx2 = 33;
                    break;
            }
            ActorImage hidepowers = new ActorImage(powerhide[i2], ((power.length + i2) * 70) + xx2 + PAK_ASSETS.IMG_ZUQIU_SHUOMING, (int) PAK_ASSETS.IMG_ZHU001, 1, this.datagroup);
            hidepowers.setScale(0.7f, 0.7f);
            if (RankData.towerCanStrength(powerName[i2])) {
                this.HideTowerUP_image = new ActorImage((int) PAK_ASSETS.IMG_PAO003, ((power.length + i2) * 70) + xx2 + PAK_ASSETS.IMG_BUFF_JINBI, (int) PAK_ASSETS.IMG_UP016, 1, this.datagroup);
                this.HideTowerUP_image.setScale(0.8f, 0.8f);
                this.upNum = this.upNum + 1;
            }
            ActorImage clock_image = new ActorImage(107, ((power.length + i2) * 70) + xx2 + PAK_ASSETS.IMG_016BDJ, (int) PAK_ASSETS.IMG_ZHU001, 1, this.datagroup);
            clock_image.setScale(0.8f, 0.8f);
            for (int j = 0; j < all; j++) {
                if (j > 4) {
                    hidepowers.setPosition((float) (((j - 5) * 70) + 207), (float) PAK_ASSETS.IMG_UP022);
                    clock_image.setPosition((float) (((j - 5) * 70) + PAK_ASSETS.IMG_PENSHEQI_SHUOMING), (float) PAK_ASSETS.IMG_ZHU001);
                    if (this.HideTowerUP_image != null) {
                        this.HideTowerUP_image.setPosition((float) (((j - 5) * 65) + PAK_ASSETS.IMG_UI_A29B), (float) PAK_ASSETS.IMG_CHONGZHI005);
                    }
                }
            }
        }
        getRole();
        drawStar();
        initData();
        initbuttonData();
    }

    /* access modifiers changed from: package-private */
    public void getRole() {
        if (this.role != null) {
            this.role.remove();
            this.role.clear();
        }
        this.role = new Group();
        int roleMapId = Mymainmenu2.userChoseIndex;
        RankData.setLastSelectRoleIndex(roleMapId);
        if (roleMapId == 3) {
            this.mapLayerRole = new RoleSpineInEquip(130, PAK_ASSETS.IMG_CHONGZHI005, MyGameMap.roleNumid[roleMapId], 0.7f);
        } else {
            this.mapLayerRole = new RoleSpineInEquip(130, PAK_ASSETS.IMG_CHONGZHI005, MyGameMap.roleNumid[roleMapId], 1.0f);
        }
        this.role.addActor(this.mapLayerRole);
        this.datagroup.addActor(this.role);
    }

    /* access modifiers changed from: package-private */
    public void initframeShuoming() {
        if (this.datagroup != null) {
            this.datagroup.remove();
            this.datagroup.clear();
        }
        this.datagroup = new Group();
        equipgruop.addActor(this.datagroup);
        new ActorImage(104, 320, (int) PAK_ASSETS.IMG_SHOP011, 1, this.datagroup);
        initbuttonClose();
    }

    /* access modifiers changed from: package-private */
    public void drawStar() {
        this.star_1 = new ActorImage(93, 105, (int) PAK_ASSETS.IMG_CHUANGGUAN004, 12, this.datagroup);
        this.star_2 = new ActorImage(93, (int) PAK_ASSETS.IMG_BOOM02, (int) PAK_ASSETS.IMG_CHUANGGUAN004, 12, this.datagroup);
        this.star_3 = new ActorImage(93, (int) PAK_ASSETS.IMG_JIESUAN011, (int) PAK_ASSETS.IMG_CHUANGGUAN004, 12, this.datagroup);
        this.star_1.setVisible(false);
        this.star_2.setVisible(false);
        this.star_3.setVisible(false);
        int f = getFlag();
        switch (f) {
            case 1:
                this.star_1.setVisible(true);
                break;
            case 2:
                this.star_1.setVisible(true);
                this.star_2.setVisible(true);
                break;
            case 3:
                this.star_1.setVisible(true);
                this.star_2.setVisible(true);
                this.star_3.setVisible(true);
                break;
        }
        timeparActionlength(f);
    }

    public int getFlag() {
        int starNum = RankData.getRankStar(Rank.rank);
        if (!RankData.drawRankInfo(Rank.rank)) {
            return 0;
        }
        return starNum;
    }

    private void timeparActionlength(int num) {
        switch (num) {
            case 1:
                this.timeparLength = 125.0f;
                break;
            case 2:
                this.timeparLength = 285.0f;
                break;
            case 3:
                this.timeparLength = this.timeparLengthall;
                break;
        }
        this.timebar.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.timeparLength, this.timebar.getHeight());
        this.timebar.setVisible(true);
    }

    /* access modifiers changed from: package-private */
    public void initbeardata() {
        this.bearcion = new GNumSprite((int) PAK_ASSETS.IMG_ZHU024, "" + RankData.getCakeNum(), "", -2, 4);
        this.bearcion.setPosition((float) 151, (float) 191);
        equipgruop.addActor(this.bearcion);
    }

    /* access modifiers changed from: package-private */
    public void initData() {
        int minuteNum = RankData.historyMinute;
        int secondNum = RankData.historySecond;
        NumActor minute = new NumActor();
        minute.set(PAK_ASSETS.IMG_ONLINE005, minuteNum, -3);
        minute.setPosition(296.0f, 595.0f);
        this.datagroup.addActor(minute);
        NumActor second = new NumActor();
        second.set(PAK_ASSETS.IMG_ONLINE005, secondNum, -3);
        second.setPosition(397.0f, 595.0f);
        this.datagroup.addActor(second);
        int percentageNun = RankData.historyPercent;
        this.percentage = new ActorImage(94, (int) PAK_ASSETS.IMG_UI_A29, (int) PAK_ASSETS.IMG_TIP005, 12, this.datagroup);
        NumActor percentageN = new NumActor();
        percentageN.set(PAK_ASSETS.IMG_ONLINE005, percentageNun, -3);
        percentageN.setPosition((float) PAK_ASSETS.IMG_SHENGJITIPS01, (float) PAK_ASSETS.IMG_YINGZI);
        this.datagroup.addActor(percentageN);
        if (!RankData.drawRankInfo(Rank.rank)) {
            minute.setVisible(false);
            second.setVisible(false);
            this.percentage.setVisible(false);
            percentageN.setVisible(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        this.close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_C01, 110, 12, equipgruop);
    }

    /* access modifiers changed from: package-private */
    public void initbuttonData() {
        SimpleButton up = new SimpleButton(60, PAK_ASSETS.IMG_ZHU033, 1, new int[]{91, 92, 91});
        up.setName(Constant.S_F);
        this.go = new SimpleButton(178, 925, 1, new int[]{87, 88});
        this.go.setName(Constant.S_D);
        this.datagroup.addActor(up);
        this.datagroup.addActor(this.go);
        this.effectGet = new Effect();
        this.effectGet.addEffect_Diejia(68, (int) PAK_ASSETS.IMG_KAWEILI06, 983, 4);
    }

    /* access modifiers changed from: package-private */
    public void initbuttonClose() {
        this.effectGet.remove_Diejia();
        SimpleButton close2 = new SimpleButton(320, PAK_ASSETS.IMG_2X1, 1, new int[]{105, 106, 105});
        close2.setName(Constant.S_E);
        close2.setPosition((float) PAK_ASSETS.IMG_LENGGUANG_SHUOMING, (float) 943);
        this.datagroup.addActor(close2);
    }

    /* access modifiers changed from: package-private */
    public void grouplistener() {
        equipgruop.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                        System.out.println("codeName:" + codeName);
                    } else {
                        codeName = 9;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            if (MyGameMap.roleMapId == Mymainmenu2.userChoseIndex) {
                                MyEquipment.this.effectGet.remove_Diejia();
                                MyEquipment.this.daoju.remove_Diejia();
                                MyEquipment.this.free();
                                break;
                            } else {
                                MyEquipment.this.free();
                                GameStage.clearAllLayers();
                                GameMain.toScreen(new MyGameMap());
                                break;
                            }
                        case 2:
                            Sound.playButtonPressed();
                            new MyShop(2);
                            break;
                        case 3:
                            MyEquipment.this.go();
                            break;
                        case 4:
                            Sound.playButtonPressed();
                            MyEquipment.this.getgroup(MyEquipment.this.isFlag);
                            break;
                        case 5:
                            Sound.playButtonPressed();
                            MyEquipment.this.free();
                            new MyUpgradeInMap(Mymainmenu2.userChoseIndex, 1);
                            break;
                    }
                    super.touchUp(event, x, y, pointer, button);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void go() {
        if (this.upNum > 0) {
            Sound.playButtonPressed();
            this.effectGet.remove_Diejia();
            this.daoju.remove_Diejia();
            new MyForceHitOut();
            return;
        }
        Sound.playSound(10);
        free();
        GameStage.clearAllLayers();
        System.out.println("----Mymainmenu2.userChoseIndex::" + Mymainmenu2.userChoseIndex);
        RankData.selectRoleIndex = Mymainmenu2.userChoseIndex;
        MyGameMap.isInMap = false;
        GameMain.toScreen(new Rank());
    }

    public void exit() {
        System.out.println("free");
        this.effectGet.remove_Diejia();
        this.daoju.remove_Diejia();
        equipgruop.remove();
        equipgruop.clear();
    }
}
