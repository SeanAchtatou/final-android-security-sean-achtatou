package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.SimpleButton;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyMore extends MyGroup {
    Group moregroup;

    public void init() {
        this.moregroup = new Group();
        GameStage.addActor(this.moregroup, 4);
        this.moregroup.addActor(new Mask());
        initbj();
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 200, 3, this.moregroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, 170, 1, this.moregroup);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 160, 12, this.moregroup);
        SimpleButton qiandao = new SimpleButton(40, 250, 1, new int[]{514, 515, 514});
        qiandao.setName("1");
        this.moregroup.addActor(qiandao);
        SimpleButton tongji = new SimpleButton(PAK_ASSETS.IMG_SHENGJITIPS06, 250, 1, new int[]{512, 513, 512});
        tongji.setName(Constant.S_C);
        this.moregroup.addActor(tongji);
        SimpleButton about = new SimpleButton(40, PAK_ASSETS.IMG_210P, 1, new int[]{PAK_ASSETS.IMG_MORE003, PAK_ASSETS.IMG_MORE004, PAK_ASSETS.IMG_MORE003});
        about.setName(Constant.S_D);
        this.moregroup.addActor(about);
        SimpleButton set = new SimpleButton(PAK_ASSETS.IMG_SHENGJITIPS06, PAK_ASSETS.IMG_210P, 1, new int[]{PAK_ASSETS.IMG_MORE001, PAK_ASSETS.IMG_MORE002, PAK_ASSETS.IMG_MORE001});
        set.setName(Constant.S_E);
        this.moregroup.addActor(set);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.moregroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MyMore.this.free();
                            return;
                        case 1:
                            Sound.playButtonPressed();
                            new EveryDayGet();
                            MyMore.this.free();
                            return;
                        case 2:
                            Sound.playButtonPressed();
                            new ResourceCount();
                            MyMore.this.free();
                            return;
                        case 3:
                            Sound.playButtonPressed();
                            MyAbout.guanyu(false);
                            MyMore.this.free();
                            return;
                        case 4:
                            Sound.playButtonPressed();
                            new MySetting();
                            MyMore.this.free();
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public void exit() {
        this.moregroup.remove();
        this.moregroup.clear();
    }
}
