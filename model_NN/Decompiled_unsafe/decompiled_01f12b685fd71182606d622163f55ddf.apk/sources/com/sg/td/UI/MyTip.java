package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GSimpleAction;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MyUIGetFont;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyTip extends MyGroup {
    static Group tipgroud;

    public void init() {
    }

    public static void tip(final int index) {
        tipgroud = new Group();
        tipgroud.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(tipgroud, 4);
        tipgroud.addActor(new Mask());
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_074, 2, tipgroud, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_SHENGJITIPS04, 1, tipgroud);
        new ActorImage((int) PAK_ASSETS.IMG_TIP010, 320, (int) PAK_ASSETS.IMG_GUANQIANDQ, 1, tipgroud);
        ActorButton close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_C01, (int) PAK_ASSETS.IMG_PPSG02, 12, tipgroud);
        SimpleButton sure = new SimpleButton(50, PAK_ASSETS.IMG_PAO002, 1, new int[]{PAK_ASSETS.IMG_TIP011, PAK_ASSETS.IMG_TIP013});
        SimpleButton quxiao = new SimpleButton(PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_PAO002, 1, new int[]{PAK_ASSETS.IMG_TIP012, PAK_ASSETS.IMG_TIP014});
        tipgroud.addActor(sure);
        tipgroud.addActor(quxiao);
        if (index > 19) {
            new ActorText("是否花费" + GMessage.BUY_PRICE_X1[index] + "，购买" + GMessage.BUY_NAME_X1[index] + "?", 320, (int) PAK_ASSETS.IMG_QIANDAO011, 1, tipgroud);
        } else {
            new ActorText("是否花费" + GMessage.BUY_PRICE[index] + "元，购买" + GMessage.BUY_NAME_X1[index] + "?", 320, (int) PAK_ASSETS.IMG_QIANDAO011, 1, tipgroud);
        }
        sure.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                GMessage.send(index);
                MyTip.tipgroud.clear();
                MyTip.tipgroud.remove();
            }
        });
        quxiao.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                MyTip.tipgroud.clear();
                MyTip.tipgroud.remove();
            }
        });
        close.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                MyTip.tipgroud.clear();
                MyTip.tipgroud.remove();
            }
        });
    }

    public static void FristBuyedFail() {
        SimpleButton sure;
        final Group failgrouop = new Group();
        failgrouop.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(failgrouop, 4);
        failgrouop.addActor(new Mask());
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_074, 1, failgrouop, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 175, (int) PAK_ASSETS.IMG_SHENGJITIPS04, 1, failgrouop);
        new ActorText("礼包超值优惠，只此一次呦！", 320, (int) PAK_ASSETS.IMG_BOMB1DJ, 1, failgrouop);
        if (GameSpecial.isRightlingqu) {
            sure = new SimpleButton(PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_QIANDAO011, 1, new int[]{154, 155});
        } else {
            sure = new SimpleButton(PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_QIANDAO011, 1, new int[]{145, 146});
        }
        SimpleButton quxiao = new SimpleButton(50, PAK_ASSETS.IMG_QIANDAO011, 1, new int[]{PAK_ASSETS.IMG_TIP017, PAK_ASSETS.IMG_TIP018});
        failgrouop.addActor(quxiao);
        failgrouop.addActor(sure);
        quxiao.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                failgrouop.remove();
                failgrouop.clear();
            }
        });
        sure.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                failgrouop.remove();
                failgrouop.clear();
                GMessage.send(GMessage.PP_TEMPgift);
            }
        });
    }

    public static void Notenought(boolean isKonglongbi) {
        Group buzu = new Group();
        ActorImage onOPen = new ActorImage(isKonglongbi ? PAK_ASSETS.IMG_TIP015 : PAK_ASSETS.IMG_TIP016, 320, (int) PAK_ASSETS.IMG_2X1, 1, buzu);
        GameStage.addActor(buzu, 4);
        buzu.addAction(MyGet.getCountActor(2, buzu, onOPen));
    }

    public static void NotBuy() {
        Group buzu = new Group();
        ActorImage onOPen = new ActorImage((int) PAK_ASSETS.IMG_TIP019, 320, (int) PAK_ASSETS.IMG_2X1, 1, buzu);
        GameStage.addActor(buzu, 4);
        buzu.addAction(MyGet.getCountActor(2, buzu, onOPen));
    }

    public static void tishi(String inf) {
        Group tishi = new Group();
        ActorText interformat = new ActorText(inf, 79, (int) PAK_ASSETS.IMG_2X1, 1, tishi);
        interformat.setWidth(PAK_ASSETS.IMG_G02);
        interformat.setFontScaleXY(1.3f);
        interformat.setColor(MyUIGetFont.jihuomaColor);
        GameStage.addActor(tishi, 4);
        tishi.addAction(getCountActor(3, tishi, interformat));
    }

    public static Action getCountActor(final int time, final Group group, ActorText text) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    GameStage.removeActor(group);
                    actor.remove();
                    actor.clear();
                    group.remove();
                    group.clear();
                }
                return true;
            }
        });
    }

    public void exit() {
    }
}
