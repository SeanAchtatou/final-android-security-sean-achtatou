package com.sg.td.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GClipGroup;
import com.kbz.Actors.SimpleButton;
import com.kbz.Sound.GameSound;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIData;
import com.sg.td.record.Bear;
import com.sg.td.record.LoadBear;
import com.sg.td.spine.RWkaweili;
import com.sg.tools.GNumSprite;
import com.sg.util.GameScreen;
import com.sg.util.GameStage;
import com.tendcloud.tenddata.y;

public class Mymainmenu2 extends GameScreen implements GameConstant {
    public static int NUM = 1;
    static final int Role_NUM = 7;
    static Group beginGroup;
    public static int continuousDay = 0;
    public static RWkaweili czhb;
    public static boolean isAward;
    public static boolean isZhuToUp;
    static int[] roleNumid = {13, 12, 15, 14};
    static int selectIndex;
    static int[] upNum = {1100, 1000};
    static int userChoseIndex;
    public static RWkaweili xshb;
    ActorImage achievement;
    Group allGroup;
    Group anniu;
    ActorImage bj;
    SimpleButton chakan;
    /* access modifiers changed from: private */
    public GClipGroup clipGroup;
    float flayX1 = Animation.CurveTimeline.LINEAR;
    Group groupbshb;
    Group groupxshb;
    ActorImage hasAward;
    Group infgroup;
    boolean isPan = false;
    GNumSprite levelNum;
    RWkaweili mapLayer1;
    ActorImage mission;
    ActorImage more;
    ActorSprite role;
    int roleID;
    String rolename = "Bear1";
    Group selectGroup;
    SimpleButton shengji;
    ActorImage shop;
    ActorImage title;
    MyMainTop top;
    SimpleButton zhaohuan;

    public void init() {
        GameSound.playMusic(1);
        if (beginGroup != null) {
            beginGroup.remove();
            beginGroup.clear();
        }
        if (this.allGroup != null) {
            this.allGroup.remove();
            this.allGroup.clear();
        }
        beginGroup = new Group();
        initbg();
        if (MyCover.isCover || RankData.isRankReturnMain()) {
            MyCover.isCover = false;
            selectIndex = RankData.getReturnRoleIndex();
        }
        System.out.println("selectIndex::" + selectIndex);
        initRole();
        judge(selectIndex);
        initbutton();
        grouplistener();
        GameStage.addActor(beginGroup, 3);
        this.top = new MyMainTop(false);
        beginGroup.addActor(this.top);
        if (MySwitch.isHaveNotice && MySwitch.isGuoqingHD && Rank.getRank() == 1 && NUM == 1) {
            NUM++;
            new MyNotice();
        }
        if (!RankData.isSameDay()) {
            new EveryDayGet();
            RankData.addSignTeach();
        }
        if (MySwitch.isGuoqingHD && MySwitch.isCaseA != 0 && !MyUIData.isGethdlb()) {
            MyGift.guoqinglb();
        }
        if (GameSpecial.atuoPop && !MyCover.isCover_PoP && !RankData.rankThreeOver()) {
            new MyGift(GMessage.PP_TEMPgift);
        }
        MyCover.isCover_PoP = false;
    }

    public void judge(int roleID2) {
        switch (roleID2) {
            case 0:
                this.rolename = "Bear1";
                break;
            case 1:
                this.rolename = "Bear2";
                break;
            case 2:
                this.rolename = "Bear3";
                break;
            case 3:
                this.rolename = "Bear4";
                break;
            default:
                this.rolename = "Bear1";
                break;
        }
        disposeinf();
        initInformation(LoadBear.bearData.get(this.rolename));
    }

    /* access modifiers changed from: package-private */
    public void initbg() {
        this.bj = new ActorImage((int) PAK_ASSETS.IMG_ZHU029, 320, (int) PAK_ASSETS.IMG_2X1, 1, beginGroup);
    }

    /* access modifiers changed from: package-private */
    public void initRoleBg() {
        this.allGroup = new Group();
        if (this.clipGroup != null) {
            this.clipGroup.remove();
        }
        this.clipGroup = new GClipGroup();
        this.clipGroup.setClipArea(0, 180, 640, PAK_ASSETS.IMG_MORE003);
        this.selectGroup = new Group();
        this.selectGroup.setPosition(105.0f, 260.0f);
        this.selectGroup.setSize(435.0f, 445.0f);
        getRole(selectIndex, this.clipGroup);
        this.clipGroup.addActor(this.selectGroup);
        this.clipGroup.setTouchable(Touchable.enabled);
        this.clipGroup.addListener(new ActorGestureListener() {
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                if (x > 30.0f && x < 430.0f) {
                    Mymainmenu2.this.selectGroup.setX(Mymainmenu2.this.selectGroup.getX() + deltaX);
                }
                Mymainmenu2.this.flayX1 = deltaX;
                Mymainmenu2.this.isPan = true;
            }

            public void fling(InputEvent event, float velocityX, float velocityY, int button) {
                if (velocityX > Animation.CurveTimeline.LINEAR) {
                    Mymainmenu2.getSelectIndex(true);
                } else {
                    Mymainmenu2.getSelectIndex(false);
                }
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Mymainmenu2.this.isPan = false;
                if (Mymainmenu2.this.flayX1 != Animation.CurveTimeline.LINEAR) {
                    Mymainmenu2.this.getRole(Mymainmenu2.selectIndex, Mymainmenu2.this.clipGroup);
                    Mymainmenu2.this.clipGroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    Mymainmenu2.this.judge(Mymainmenu2.selectIndex);
                }
                Mymainmenu2.this.flayX1 = Animation.CurveTimeline.LINEAR;
            }
        });
        this.allGroup.addActor(this.clipGroup);
    }

    public void getRole(int id, Group group) {
        if (this.mapLayer1 != null) {
            this.mapLayer1.remove();
            this.mapLayer1.clear();
        }
        this.mapLayer1 = new RWkaweili(0, 70, roleNumid[id], 1.0f);
        this.mapLayer1.setPosition(320.0f, 518.0f);
        this.selectGroup.setPosition(105.0f, 260.0f);
        group.addActor(this.mapLayer1);
    }

    public static int getSelectIndex(boolean isLeft) {
        if (isLeft) {
            selectIndex--;
            if (selectIndex == -1) {
                selectIndex = 3;
            }
        } else {
            selectIndex++;
            if (selectIndex == 4) {
                selectIndex = 0;
            }
        }
        switch (selectIndex) {
            case 0:
            case 3:
                Sound.playSound(50);
                break;
            case 1:
                Sound.playSound(34);
                break;
            case 2:
                Sound.playSound(42);
                break;
        }
        return selectIndex;
    }

    private void initRole() {
        initRoleBg();
        GameStage.addActor(this.allGroup, 4);
        ActorImage left = new ActorImage((int) PAK_ASSETS.IMG_ZHU008, 3, (int) PAK_ASSETS.IMG_PUBLIC004, 12, beginGroup);
        ActorImage right = new ActorImage((int) PAK_ASSETS.IMG_ZHU009, (int) PAK_ASSETS.IMG_ZD_LIUXINGCHUI1, (int) PAK_ASSETS.IMG_PUBLIC004, 12, beginGroup);
        left.setTouchable(Touchable.enabled);
        right.setTouchable(Touchable.enabled);
        left.setName(Constant.S_C);
        right.setName(Constant.S_D);
    }

    private void initInformation(Bear bear) {
        this.infgroup = new Group() {
            public void act(float delta) {
                super.act(delta);
                Mymainmenu2.this.levelNum.setNum(RankData.roleLevel[Mymainmenu2.selectIndex]);
            }
        };
        String inf = bear.getIntro();
        String name = bear.getName();
        int level = RankData.roleLevel[selectIndex];
        int star = bear.getStar();
        String Effectsinf = bear.getSkill();
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_SHOP006, 1, this.infgroup, true, false);
        new ActorImage((int) PAK_ASSETS.IMG_ZHU021, 320, (int) PAK_ASSETS.IMG_SHOP018, 1, this.infgroup);
        new ActorImage(name, 36, (int) PAK_ASSETS.IMG_ZHANDOU066, 12, this.infgroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP023, (int) PAK_ASSETS.IMG_READYGO02, (int) PAK_ASSETS.IMG_SHOP018, 1, this.infgroup);
        this.levelNum = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + level, "", -2, 4);
        this.levelNum.setPosition(372.0f, 732.0f);
        this.infgroup.addActor(this.levelNum);
        for (int i = 0; i < star; i++) {
            new ActorImage((int) PAK_ASSETS.IMG_ZHU014, (i * 40) + 443, (int) PAK_ASSETS.IMG_SHOP021, 1, this.infgroup);
        }
        new ActorImage((int) PAK_ASSETS.IMG_ZHU020, 80, (int) PAK_ASSETS.IMG_ZHU006, 1, this.infgroup);
        StringBuffer stringBuffer = new StringBuffer(inf);
        stringBuffer.insert(24, "\n");
        if (stringBuffer.length() > 48) {
            stringBuffer.insert(48, "\n");
        }
        if (stringBuffer.length() > 72) {
            stringBuffer.insert(72, "\n");
        }
        String stringBuffer2 = stringBuffer.toString();
        ActorText interformat = new ActorText(inf, 138, 772, 12, this.infgroup);
        interformat.setWidth(PAK_ASSETS.IMG_JIESUO002);
        interformat.setFontScaleXY(1.1f);
        ActorText Effects = new ActorText(Effectsinf, 138, (int) PAK_ASSETS.IMG_ZHU037, 12, this.infgroup);
        Effects.setColor(Color.YELLOW);
        Effects.setWidth(PAK_ASSETS.IMG_JIESUO002);
        Effects.setFontScaleXY(1.2f);
        int[] iArr = {PAK_ASSETS.IMG_ZHU022, PAK_ASSETS.IMG_ZHU023, PAK_ASSETS.IMG_ZHU022};
        int level11 = RankData.roleLevel[selectIndex];
        if (level11 == 0) {
            this.zhaohuan = new SimpleButton(PAK_ASSETS.IMG_LENGGUANG_SHUOMING, 885, 1, new int[]{PAK_ASSETS.IMG_ZHU034, PAK_ASSETS.IMG_ZHU035});
            this.zhaohuan.setName("1");
            this.infgroup.addActor(this.zhaohuan);
        } else if (level11 == 10) {
            this.chakan = new SimpleButton(PAK_ASSETS.IMG_LENGGUANG_SHUOMING, 885, 1, new int[]{PAK_ASSETS.IMG_ZHU037, PAK_ASSETS.IMG_ZHU038});
            this.chakan.setName("1");
            this.infgroup.addActor(this.chakan);
        } else {
            this.shengji = new SimpleButton(PAK_ASSETS.IMG_LENGGUANG_SHUOMING, 885, 1, new int[]{PAK_ASSETS.IMG_ZHU015, PAK_ASSETS.IMG_ZHU016});
            this.shengji.setName("1");
            this.infgroup.addActor(this.shengji);
        }
        getRoleUpNum(bear, selectIndex);
        beginGroup.addActor(this.infgroup);
    }

    /* access modifiers changed from: package-private */
    public void getRoleUpNum(Bear bear, int id) {
        if (RankData.getRoleUpdateTimes(id) != 0) {
            new ActorImage(96, 405, 888, 1, this.infgroup);
            GNumSprite Num1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + RankData.getRoleUpdateTimes(id), "X", -2, 4);
            Num1.setPosition((float) 405, (float) 888);
            this.infgroup.addActor(Num1);
        }
    }

    /* access modifiers changed from: package-private */
    public void disposeinf() {
        if (this.infgroup != null) {
            this.infgroup.remove();
            this.infgroup.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        this.anniu = new Group();
        SimpleButton beginbutton = new SimpleButton(320, 1060, 1, new int[]{PAK_ASSETS.IMG_ZHU022, PAK_ASSETS.IMG_ZHU023, PAK_ASSETS.IMG_ZHU022});
        beginbutton.setName("0");
        beginbutton.setPosition((float) 143, (float) 990);
        beginGroup.addActor(beginbutton);
        czhb = new RWkaweili(65, 1050, 6, 1.0f);
        xshb = new RWkaweili(65, 1050, 17, 1.0f);
        RWkaweili bshb = new RWkaweili(PAK_ASSETS.IMG_PUBLIC009, 1050, 4, 1.0f);
        if (GMessage.getIsBuyed(1)) {
            xshb.setVisible(false);
        } else {
            czhb.setVisible(false);
        }
        this.groupxshb = new Group();
        this.groupxshb.setPosition((float) 10, (float) y.e);
        this.groupxshb.setSize(130.0f, 110.0f);
        this.groupbshb = new Group();
        this.groupbshb.setPosition(500.0f, (float) y.e);
        this.groupbshb.setSize(130.0f, 110.0f);
        this.groupxshb.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                if (GMessage.getIsBuyed(1)) {
                    new MyGift(2);
                } else {
                    new MyGift(1);
                }
                GMessage.setMfContonl(new GMessage.MianGiftContonl() {
                    public void Yes() {
                        if (GMessage.getIsBuyed(1)) {
                            Mymainmenu2.xshb.setVisible(false);
                            Mymainmenu2.czhb.setVisible(true);
                            return;
                        }
                        Mymainmenu2.czhb.setVisible(false);
                    }

                    public void No() {
                    }
                });
            }
        });
        this.groupbshb.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                Sound.playButtonPressed();
                new MyGift(4);
            }
        });
        GameStage.addActor(this.groupxshb, 4);
        GameStage.addActor(this.groupbshb, 4);
        beginGroup.addActor(czhb);
        beginGroup.addActor(xshb);
        beginGroup.addActor(bshb);
        new ActorButton((int) PAK_ASSETS.IMG_ZHU031, Constant.S_E, 0, (int) PAK_ASSETS.IMG_HUOCHAI_SHUOMING, 12, this.anniu);
        new ActorButton((int) PAK_ASSETS.IMG_ZHU033, Constant.S_F, 0, (int) PAK_ASSETS.IMG_PPSG02, 12, this.anniu);
        new ActorButton((int) PAK_ASSETS.IMG_ZHU030, "7", (int) PAK_ASSETS.IMG_PAO004, (int) PAK_ASSETS.IMG_PPSG02, 12, this.anniu);
        if (RankData.showTowerStrengButton()) {
            new ActorButton((int) PAK_ASSETS.IMG_ZHU032, "6", (int) PAK_ASSETS.IMG_PAO004, (int) PAK_ASSETS.IMG_HUOCHAI_SHUOMING, 12, this.anniu);
            new Effect().addEffect(67, (int) PAK_ASSETS.IMG_F02, 248, this.anniu);
        }
        this.hasAward = new ActorImage((int) PAK_ASSETS.IMG_JIESUAN015, (int) PAK_ASSETS.IMG_G02, (int) PAK_ASSETS.IMG_PPSG02, 12, this.anniu);
        this.hasAward.setScale(0.8f, 0.8f);
        this.hasAward.addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.5f))));
        if (MySwitch.isGuoqingHD && MySwitch.isCaseA != 0) {
            new ActorButton((int) PAK_ASSETS.IMG_ZHU042, "8", 0, (int) PAK_ASSETS.IMG_SHIBAI, 12, this.anniu);
        }
        new ActorButton((int) PAK_ASSETS.IMG_ZHU043, "9", (int) PAK_ASSETS.IMG_PAO004, (int) PAK_ASSETS.IMG_SHIBAI, 12, this.anniu).addAction(Actions.repeat(-1, Actions.sequence(Actions.scaleTo(1.2f, 1.2f, 0.5f), Actions.scaleTo(1.0f, 1.0f, 1.5f))));
        beginGroup.addActor(this.anniu);
    }

    /* access modifiers changed from: package-private */
    public void grouplistener() {
        beginGroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (event.getPointer() == 0) {
                    switch (Integer.parseInt(event.getTarget().getName())) {
                        case 0:
                            Sound.playButtonPressed();
                            System.out.println("kaishi");
                            if (Mymainmenu2.this.isOpenRole()) {
                                MyGameMap.isInMap = true;
                                Mymainmenu2.userChoseIndex = Mymainmenu2.selectIndex;
                                RankData.setLastSelectRoleIndex(Mymainmenu2.userChoseIndex);
                                GameMain.toScreen(new MyGameMap());
                                Mymainmenu2.this.dispose();
                                return;
                            }
                            Mymainmenu2.isZhuToUp = true;
                            new MyUpgrade(Mymainmenu2.selectIndex);
                            return;
                        case 1:
                            Sound.playButtonPressed();
                            Mymainmenu2.isZhuToUp = true;
                            new MyUpgrade(Mymainmenu2.selectIndex);
                            return;
                        case 2:
                            Sound.playSound(17);
                            Mymainmenu2.this.getRole(Mymainmenu2.getSelectIndex(true), Mymainmenu2.this.clipGroup);
                            Mymainmenu2.this.judge(Mymainmenu2.selectIndex);
                            System.out.println("left");
                            return;
                        case 3:
                            Sound.playSound(17);
                            Mymainmenu2.this.getRole(Mymainmenu2.getSelectIndex(false), Mymainmenu2.this.clipGroup);
                            Mymainmenu2.this.judge(Mymainmenu2.selectIndex);
                            return;
                        case 4:
                            Sound.playButtonPressed();
                            new MyLuckyRoller();
                            return;
                        case 5:
                            Sound.playButtonPressed();
                            new MyJiDi();
                            return;
                        case 6:
                            Sound.playButtonPressed();
                            new MyTowerUP();
                            return;
                        case 7:
                            Sound.playButtonPressed();
                            new MyOnlineAward();
                            return;
                        case 8:
                            Sound.playButtonPressed();
                            new MyRecharge();
                            return;
                        case 9:
                            Sound.playButtonPressed();
                            MyAbout.guanyu(true);
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public boolean isOpenRole() {
        if (RankData.roleLevel[selectIndex] == 0) {
            return false;
        }
        return true;
    }

    public void run(float delta) {
        RankData.teach.addRoleUpTeach();
        Rank.runTip(delta);
        if (RankData.showOnlineTip()) {
            if (this.hasAward != null) {
                this.hasAward.setVisible(true);
            }
        } else if (this.hasAward != null) {
            this.hasAward.setVisible(false);
        }
    }

    public void dispose() {
        this.top.free();
        this.anniu.remove();
        this.anniu.clear();
        this.clipGroup.remove();
        this.clipGroup.clear();
        beginGroup.remove();
        beginGroup.clear();
        this.groupxshb.remove();
        this.groupxshb.clear();
        this.groupbshb.remove();
        this.groupbshb.clear();
    }

    public boolean gTouchDown(int screenX, int screenY, int pointer) {
        RankData.teach.touchDown_UI(screenX, screenY);
        return true;
    }

    public boolean gTouchUp(int screenX, int screenY, int pointer, int button) {
        RankData.teach.touchUp_UI(screenX, screenY);
        return true;
    }
}
