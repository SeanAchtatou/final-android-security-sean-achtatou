package com.sg.td.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.td.record.LoadTargetData;
import com.sg.td.record.TargetData;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class EveryDayMission extends MyGroup {
    static int[] iconimage = {126, 127, 128, 129, 130};
    static Group missgroup;
    static Group missiongroup;
    ActorText completeness;
    ActorImage explain;
    ActorText explain_1;
    ActorImage icon_1;
    boolean isCanlingqu;
    ActorImage kuang;
    ActorImage title_1;
    ActorImage title_2;

    public void init() {
        missgroup = new Group();
        missiongroup = new Group();
        missgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(missgroup, 4);
        missgroup.addActor(new Mask());
        LoadTargetData.loadTargetDate();
        initbj();
        initframe();
        initbutton();
    }

    /* access modifiers changed from: package-private */
    public void initdata() {
        System.out.println("完成所有任务获得奖励钻石个数：" + RankData.targets.getDiamonds());
        for (int i : RankData.everyDayTargertIndex) {
        }
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 198, 9, missgroup, false, true);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 168, 1, missgroup);
        this.title_2 = new ActorImage(109, 320, 138, 1, missgroup);
        this.explain = new ActorImage(110, 320, 243, 1, missgroup);
    }

    /* access modifiers changed from: package-private */
    public void initframe() {
        for (int i = 0; i < RankData.everyDayTargertIndex.length; i++) {
            loadMissongruop(320, (i * 170) + 300, i, RankData.everyDayTargertIndex[i]);
            System.out.println("RankData.everyDayTargertIndex[i]:" + i + "  " + RankData.everyDayTargertIndex[i]);
        }
    }

    /* access modifiers changed from: package-private */
    public void loadMissongruop(int x, int y, int id, int index) {
        missiongroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        System.out.println(id);
        TargetData.Target target = RankData.targets.getTargets()[index];
        String missionExplain = target.getDesp();
        String tubiao = target.getIcon();
        System.out.println("tubiao:" + tubiao);
        String complete = target.getDegreeDescp();
        final int price = target.getMoney();
        boolean unfinish = target.unfinish();
        boolean isReceive = target.isReceive();
        boolean isComplete = target.isComplete();
        final ActorSprite anniu = new ActorSprite(x + 120, y + 29, 4, 113, 11, 115, PAK_ASSETS.IMG_PUBLIC009);
        anniu.setName("1");
        this.kuang = new ActorImage(116, x, y + 50, 1, missiongroup);
        this.kuang.setTouchable(Touchable.enabled);
        this.icon_1 = new ActorImage(tubiao, x - 190, y + 50, 1, missiongroup);
        this.icon_1.setScale(0.7f, 0.7f);
        this.explain_1 = new ActorText(missionExplain, x - 90, y, 12, missiongroup);
        this.explain_1.setColor(Color.BLUE);
        this.completeness = new ActorText(complete, x - 90, y + 40, 12, missiongroup);
        this.completeness.setColor(Color.PURPLE);
        GNumSprite bearcoin = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "X" + price, "X", -2, 4);
        bearcoin.setPosition((float) (x + 35), (float) (y + 89));
        missiongroup.addActor(bearcoin);
        if (unfinish) {
            anniu.setTexture(0);
        } else if (isReceive) {
            anniu.setPosition((float) (x + 98), (float) (y + 20));
            anniu.setTexture(1);
        } else {
            anniu.setPosition((float) (x + 139), (float) (y + 20));
            anniu.setTexture(3);
        }
        final boolean z = isReceive;
        final int i = index;
        final TargetData.Target target2 = target;
        final int i2 = x;
        final int i3 = y;
        anniu.addListener(new ClickListener() {
            public void clicked(InputEvent event, float xx, float yy) {
                super.clicked(event, xx, yy);
                System.out.println("dianji");
                System.out.println("isCanlingqu:" + EveryDayMission.this.isCanlingqu);
                Get get = LoadGet.getData.get("Mission");
                if (z) {
                    Sound.playSound(9);
                    new MyGet(get, i, 2);
                    target2.setReceive();
                    RankData.addCakeNum(price);
                    anniu.setPosition((float) (i2 + 139), (float) (i3 + 20));
                    anniu.setTexture(3);
                    EveryDayMission.this.isCanlingqu = false;
                }
            }
        });
        missiongroup.addActor(anniu);
        missgroup.addActor(missiongroup);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_C01, 160, 12, missgroup).addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                EveryDayMission.this.free();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }
        });
        TargetData.Target[] targets = RankData.targets.getTargets();
        if (RankData.canTakeTargetAward()) {
            final SimpleButton lingqu = new SimpleButton(PAK_ASSETS.IMG_MAP1, PAK_ASSETS.IMG_JINGANGDAODAN2, 1, new int[]{111, 112, 111});
            lingqu.setName("1");
            missgroup.addActor(lingqu);
            lingqu.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (RankData.haveTakeTargetAward()) {
                        return true;
                    }
                    RankData.takeTargetAward(50);
                    Sound.playSound(7);
                    new MyGet(LoadGet.getData.get("Mission"), 5, 2);
                    lingqu.setVisible(false);
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                }
            });
        }
    }

    public void exit() {
        missgroup.remove();
        missgroup.clear();
    }
}
