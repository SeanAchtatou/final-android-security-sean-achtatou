package com.sg.td.UI;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.GSimpleAction;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyLuckyRoller extends MyGroup {
    static Group luckygroup;
    double RAD;
    private float acc = 600.0f;
    Effect back;
    /* access modifiers changed from: private */
    public boolean canRestart = true;
    ActorButton close;
    int daynum = 0;
    private float degree;
    Effect efzhuanpan;
    Effect efzhuanpanrun;
    int getid;
    ActorSprite image_free;
    ActorImage image_pointer;
    ActorImage image_pointer_black;
    ActorImage image_pointer_blackpan;
    private int[] index = {1, 2, 3, 4, 5, 6, 7, 8};
    private boolean isStart;
    GNumSprite money;
    int moneyNum;
    int num = 0;
    int o;
    int p;
    private float speed = 40.0f;
    ActorButton start;
    ActorImage title_1;
    ActorImage title_2;
    int x;
    int y;

    public void init() {
        new Mask();
        luckygroup = new Group() {
            public void act(float delta) {
                super.act(delta);
                MyLuckyRoller.this.moneyNum = RankData.getCakeNum();
                if (MyLuckyRoller.this.moneyNum > 99999) {
                    MyLuckyRoller.this.moneyNum = 99999;
                }
                MyLuckyRoller.this.money.setNum(MyLuckyRoller.this.moneyNum);
            }
        };
        addActor(luckygroup);
        luckygroup.addActor(new Mask());
        GameStage.addActor(this, 4);
        initbj();
        initButton();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 180, 9, luckygroup, false, true);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 150, 1, luckygroup);
        this.title_2 = new ActorImage((int) PAK_ASSETS.IMG_ZHUANPAN001, 320, 120, 1, luckygroup);
        this.image_pointer_blackpan = new ActorImage((int) PAK_ASSETS.IMG_ZHUANPAN002, 320, 501, 1, luckygroup);
        this.efzhuanpan = new Effect();
        this.efzhuanpan.addEffect(70, 320, 501, 4);
        this.image_pointer_black = new ActorImage((int) PAK_ASSETS.IMG_ZHUANPAN007, 320, (int) PAK_ASSETS.IMG_QIANDAO009, 1, luckygroup);
        this.image_pointer = new ActorImage((int) PAK_ASSETS.IMG_ZHUANPAN008, 320, (int) PAK_ASSETS.IMG_JIESUAN002, 1, luckygroup);
        this.image_pointer.setOrigin(this.image_pointer.getWidth() / 2.0f, this.image_pointer.getHeight() - (this.image_pointer.getWidth() / 2.0f));
        this.image_pointer.setScale(0.7f);
        this.image_free = new ActorSprite((int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA, (int) PAK_ASSETS.IMG_SHOP034, 4, PAK_ASSETS.IMG_ZHUANPAN003, PAK_ASSETS.IMG_ZHUANPAN004);
        if (RankData.luckDrawTimes == 0) {
            this.image_free.setTexture(0);
            this.image_free.setPosition(271.0f, 748.0f);
        } else {
            this.image_free.setTexture(1);
            this.image_free.setPosition(222.0f, 748.0f);
        }
        luckygroup.addActor(this.image_free);
        initData();
    }

    /* access modifiers changed from: package-private */
    public void initData() {
        this.moneyNum = RankData.getCakeNum();
        this.money = new GNumSprite((int) PAK_ASSETS.IMG_ZHU036, "" + this.moneyNum, "X", -2, 4);
        this.money.setPosition(146.0f, 215.0f);
        luckygroup.addActor(this.money);
    }

    /* access modifiers changed from: package-private */
    public void initButton() {
        this.start = new ActorButton((int) PAK_ASSETS.IMG_ZHUANPAN009, "go", 186, (int) PAK_ASSETS.IMG_UP009, 12, luckygroup);
        this.back = new Effect();
        this.back.addEffect_Diejia(72, (int) PAK_ASSETS.IMG_PENHUO01, (int) PAK_ASSETS.IMG_ZHU038, 4);
        this.close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_C01, 140, 12, luckygroup);
        this.start.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("canRestart:" + MyLuckyRoller.this.canRestart);
                if (!MyLuckyRoller.this.canRestart) {
                    System.out.println("转的时候点击开始");
                } else if (RankData.isLuckFree()) {
                    RankData.addLuckDrawTimes();
                    MyLuckyRoller.this.start();
                } else if (RankData.spendCake(300)) {
                    RankData.addLuckDrawTimes();
                    MyLuckyRoller.this.start();
                } else if (MySwitch.isCaseA == 0) {
                    MyTip.Notenought(true);
                } else {
                    new MyGift(10);
                }
                super.touchUp(event, x, y, pointer, button);
            }
        });
        this.close.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (MyLuckyRoller.this.canRestart) {
                    Sound.playButtonClosed();
                    MyLuckyRoller.this.free();
                }
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void initDot() {
        ActorImage image_dot;
        this.o = 320;
        this.p = PAK_ASSETS.IMG_QIANDAO004;
        for (int i = 0; i < 12; i++) {
            this.RAD = 0.5235987755982988d * ((double) i);
            this.x = (int) (((double) this.o) + (Math.cos(this.RAD) * 230.0d));
            this.y = (int) (((double) this.p) - (Math.sin(this.RAD) * 230.0d));
            if (i % 2 == 0) {
                image_dot = new ActorImage((int) PAK_ASSETS.IMG_ZHUANPAN005, this.x, this.y, 1, luckygroup);
            } else {
                image_dot = new ActorImage((int) PAK_ASSETS.IMG_ZHUANPAN006, this.x, this.y, 1, luckygroup);
            }
            luckygroup.addActor(image_dot);
        }
    }

    /* access modifiers changed from: private */
    public void start() {
        System.out.println("开始");
        this.canRestart = false;
        this.isStart = true;
        this.speed = 30.0f;
        this.getid = this.index[getRandom()];
        this.degree = (float) ((this.getid * 45) - 15);
    }

    private int getRandom() {
        int num2 = MathUtils.random(0, 100);
        int num_1 = 0;
        System.out.println("num::" + num2);
        if (num2 <= 1) {
            num_1 = 7;
        }
        if (num2 <= 10 && num2 > 1) {
            num_1 = MathUtils.random(1, 4);
        }
        if (num2 <= 20 && num2 > 10) {
            num_1 = MathUtils.random(1, 4);
        }
        if (num2 > 20) {
            return 0;
        }
        return num_1;
    }

    public void run(float delta) {
        runCicle(delta);
        super.run(delta);
    }

    private void runCicle(float delta) {
        if (this.isStart) {
            if (this.efzhuanpan != null) {
                this.efzhuanpan.remove();
            }
            this.image_free.setTexture(1);
            this.image_free.setPosition(222.0f, 748.0f);
            this.daynum = 1;
            if (this.speed > 1200.0f) {
                this.num = 0;
                if (this.efzhuanpanrun != null) {
                    this.efzhuanpanrun.remove();
                }
                showResult(delta);
                return;
            }
            if (this.num == 0) {
                this.efzhuanpanrun = new Effect();
                this.efzhuanpanrun.addEffect(71, 320, 501, 4);
                Sound.playSound(84);
            }
            this.num++;
            this.speed += this.acc * delta;
            this.image_pointer.rotateBy(this.speed * delta);
        }
    }

    private void showResult(float delta) {
        RankData.addLuckDrawTimes();
        if (this.efzhuanpanrun != null) {
            this.efzhuanpanrun.remove();
        }
        this.efzhuanpan = new Effect();
        this.efzhuanpan.addEffect(70, 320, 501, 4);
        System.out.println("image_pointer.getRotation():" + this.image_pointer.getRotation());
        System.out.println("degree:" + this.degree);
        this.image_pointer.setRotation(this.image_pointer.getRotation() % 360.0f);
        SequenceAction action = Actions.sequence();
        action.addAction(Actions.rotateTo(this.degree + 720.0f, 3.0f, Interpolation.exp5Out));
        this.isStart = false;
        action.addAction(GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            public boolean run(float delta, Actor actor) {
                boolean unused = MyLuckyRoller.this.canRestart = true;
                Get get = LoadGet.getData.get("zhuanpan");
                System.out.println("(get==null)" + (get == null));
                Sound.playSound(16);
                new MyGet(get, MyLuckyRoller.this.getid - 1, 2);
                MyLuckyRoller.this.addget(MyLuckyRoller.this.getid - 1);
                return true;
            }
        }));
        this.image_pointer.addAction(action);
    }

    /* access modifiers changed from: package-private */
    public void addget(int id) {
        System.out.println("id:" + id);
        switch (id) {
            case 0:
                RankData.addCakeNum(100);
                return;
            case 1:
                RankData.addHoneyNum(5);
                return;
            case 2:
                RankData.addDiamondNum(20);
                return;
            case 3:
                RankData.addHeartNum(2);
                return;
            case 4:
                RankData.addCakeNum(1000);
                return;
            case 5:
                RankData.addHeartNum(1);
                return;
            case 6:
                RankData.addHoneyNum(2);
                return;
            case 7:
                RankData.addDiamondNum(999);
                return;
            default:
                return;
        }
    }

    public void exit() {
        if (this.efzhuanpan != null) {
            System.out.println("--------*******");
            this.efzhuanpan.remove();
        }
        luckygroup.remove();
        luckygroup.clear();
        this.back.remove_Diejia();
    }
}
