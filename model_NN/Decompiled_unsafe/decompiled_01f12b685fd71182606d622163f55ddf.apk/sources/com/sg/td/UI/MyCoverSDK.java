package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.message.MySwitch;
import com.sg.util.GameScreen;
import com.sg.util.GameStage;

public class MyCoverSDK extends GameScreen implements GameConstant {
    public void init() {
        if (MySwitch.isSDkCover) {
            sdkCover();
        } else {
            zhaonggaoCover();
        }
    }

    /* access modifiers changed from: package-private */
    public void sdkCover() {
        final Group zgggroup = new Group();
        ActorImage coverCenter = new ActorImage(82, 320, (int) PAK_ASSETS.IMG_2X1, 1, zgggroup);
        GameStage.addActor(zgggroup, 3);
        coverCenter.addAction(Actions.sequence(Actions.delay(2.0f), Actions.run(new Runnable() {
            public void run() {
                zgggroup.remove();
                zgggroup.clear();
                if (MySwitch.isZhonggao) {
                    MyCoverSDK.this.zhaonggaoCover();
                } else {
                    GameMain.toScreen(new MyCover());
                }
            }
        })));
    }

    /* access modifiers changed from: package-private */
    public void zhaonggaoCover() {
        final Group SDKgroup = new Group();
        ActorImage coverCenter = new ActorImage(84, 320, (int) PAK_ASSETS.IMG_2X1, 1, SDKgroup);
        GameStage.addActor(SDKgroup, 3);
        coverCenter.addAction(Actions.sequence(Actions.delay(2.0f), Actions.run(new Runnable() {
            public void run() {
                SDKgroup.remove();
                SDKgroup.clear();
                GameMain.toScreen(new MyCover());
            }
        })));
    }

    public void run(float delta) {
    }

    public void dispose() {
    }
}
