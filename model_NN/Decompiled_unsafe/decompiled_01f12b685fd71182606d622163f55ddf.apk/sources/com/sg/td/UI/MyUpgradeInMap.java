package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.Particle.GameParticle;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.record.Bear;
import com.sg.td.record.LoadBear;
import com.sg.td.spine.RWkaweili;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyUpgradeInMap extends MyGroup {
    static int atkNum;
    static int atkNumNext;
    static int[] role = {PAK_ASSETS.IMG_ZHU025, PAK_ASSETS.IMG_ZHU027, PAK_ASSETS.IMG_ZHU028, PAK_ASSETS.IMG_ZHU026};
    static int roleId = 0;
    static int[] roleplay = {13, 12, 15, 14};
    static int skillNum;
    static int skillNumNext;
    Effect anniu;
    ActorImage attack;
    ActorClipImage bar;
    ActorSprite consume1;
    int consume1Num;
    int consume3Num;
    float curretatk = Animation.CurveTimeline.LINEAR;
    Group datagroup;
    Actor effect1;
    Actor effect2;
    Actor effect3;
    Actor effect4;
    Actor effect5;
    Actor effect6;
    Actor effect7;
    Group effectgroup;
    boolean isfull;
    int levelNum;
    GameParticle libaop;
    int moneyall;
    int moneyone;
    String rolename = "";
    float speed = 0.1f;
    int state;
    private float timeparLength = 120.0f;
    private float timeparLength1;
    Group upGroup;

    public void run(float delta) {
        super.run(delta);
    }

    public void init() {
        if (this.upGroup != null) {
            free();
        }
        this.upGroup = new Group() {
        };
        this.upGroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.upGroup, 4);
        this.upGroup.addActor(new Mask());
    }

    public Bear getbear(int id) {
        switch (id) {
            case 0:
                this.rolename = "Bear1";
                break;
            case 1:
                this.rolename = "Bear2";
                break;
            case 2:
                this.rolename = "Bear3";
                break;
            case 3:
                this.rolename = "Bear4";
                break;
        }
        return LoadBear.bearData.get(this.rolename);
    }

    public MyUpgradeInMap(int id, int jiemian) {
        Mymainmenu2.isZhuToUp = false;
        int level = RankData.roleLevel[id];
        if (level == 0) {
            free();
            new MyUpgradeNoOPen(id, false, jiemian, level);
            return;
        }
        Bear bearid = getbear(id);
        boolean isjudgefull = bearid.isFullLevel(RankData.roleLevel[id]);
        System.out.println("isjudgefull是否满级:" + isjudgefull);
        if (isjudgefull) {
            free();
            new MyUpgradeNoOPen(id, true, jiemian, level);
            return;
        }
        roleId = id;
        initbj(bearid);
        initSelectRolegruop();
        initData(bearid, roleId);
        initbutton();
        initlistener(jiemian);
    }

    /* access modifiers changed from: package-private */
    public void initbj(Bear bear) {
        String name = bear.getName();
        System.out.println("name:" + name);
        GameUITools.GetbackgroundImage(320, 168, 10, this.upGroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 138, 1, this.upGroup);
        new ActorImage(name, 320, 108, 1, this.upGroup);
    }

    /* access modifiers changed from: package-private */
    public void initSelectRolegruop() {
        float Scale = 0.4f;
        if (roleId == 0) {
            Scale = 0.5f;
        }
        this.upGroup.addActor(new RWkaweili(140, PAK_ASSETS.IMG_DAJI10, roleplay[roleId], Scale));
        new ActorImage((int) PAK_ASSETS.IMG_UP003, (int) PAK_ASSETS.IMG_ICE70, (int) PAK_ASSETS.IMG_KAWEILI05, 1, this.upGroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP004, 180, (int) PAK_ASSETS.IMG_ZHANDOU054, 1, this.upGroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP008, 120, (int) PAK_ASSETS.IMG_SHUOMING004, 1, this.upGroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP004, (int) PAK_ASSETS.IMG_LOAD2, (int) PAK_ASSETS.IMG_ZHANDOU054, 1, this.upGroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP008, 400, (int) PAK_ASSETS.IMG_SHUOMING004, 1, this.upGroup);
        if (!Rank.isRanking()) {
            ActorButton left = new ActorButton((int) PAK_ASSETS.IMG_ZHU008, "1", -5, 300, 12, this.upGroup);
            ActorButton right = new ActorButton((int) PAK_ASSETS.IMG_ZHU009, Constant.S_C, (int) PAK_ASSETS.IMG_E01, 300, 12, this.upGroup);
            left.setScale(0.6f, 0.6f);
            right.setScale(0.6f, 0.6f);
        }
    }

    /* access modifiers changed from: package-private */
    public void initData(Bear bear, int id) {
        this.datagroup = new Group();
        this.consume1 = new ActorSprite(70, (int) PAK_ASSETS.IMG_SHOP013, 4, PAK_ASSETS.IMG_UP011, PAK_ASSETS.IMG_UP010);
        this.datagroup.addActor(this.consume1);
        this.levelNum = RankData.roleLevel[id];
        int bulletNum = bear.getBullet();
        String rangeNum = bear.getRange();
        String abilityNum = bear.getBuff();
        String stuntNum = bear.getSkill();
        atkNum = bear.getCurAttack(this.levelNum);
        skillNum = bear.getCurDamage(this.levelNum);
        atkNumNext = bear.getNextAttack(this.levelNum);
        skillNumNext = bear.getNextDamage(this.levelNum);
        this.consume1Num = bear.getMoney(this.levelNum);
        this.consume3Num = bear.getMoney(this.levelNum);
        if (this.consume1Num == 0) {
            this.consume1Num = bear.getDiamond(this.levelNum);
            this.consume1.setTexture(1);
        }
        int atkNumfull = bear.getFullAttack();
        int skillNumfull = bear.getFullDamage();
        int consume2Num = bear.getDiamond();
        this.moneyone = this.consume1Num;
        this.moneyall = consume2Num;
        this.isfull = bear.isFullLevel(this.levelNum);
        new ActorImage((int) PAK_ASSETS.IMG_UP023, (int) PAK_ASSETS.IMG_SHENGJITIPS06, 193, 12, this.datagroup);
        GNumSprite level = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + this.levelNum, "", -2, 4);
        level.setPosition(400.0f, 208.0f);
        this.datagroup.addActor(level);
        ActorImage attack2 = new ActorImage((int) PAK_ASSETS.IMG_UP005, (int) PAK_ASSETS.IMG_PPSG04, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.datagroup);
        this.bar = new ActorClipImage(PAK_ASSETS.IMG_UP006, PAK_ASSETS.IMG_PPSG04, PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.datagroup);
        this.bar.setVisible(false);
        this.curretatk = ((float) atkNum) / ((float) atkNumfull);
        this.timeparLength1 = attack2.getWidth();
        timeparAction();
        this.timeparLength = Animation.CurveTimeline.LINEAR;
        new ActorText(atkNum + "/" + atkNumfull, 400, (int) PAK_ASSETS.IMG_SHUOMINGZI03, 12, this.datagroup);
        new ActorText(bulletNum + "", (int) PAK_ASSETS.IMG_SHENGJITIPS06, (int) PAK_ASSETS.IMG_DAJI05, 12, this.datagroup);
        new ActorText(rangeNum, (int) PAK_ASSETS.IMG_SHENGJITIPS06, (int) PAK_ASSETS.IMG_REAPDAOJU01A, 12, this.datagroup);
        new ActorText(abilityNum, (int) PAK_ASSETS.IMG_UI_TILI02, (int) PAK_ASSETS.IMG_LIGHTD02, 12, this.datagroup);
        new ActorText(stuntNum, (int) PAK_ASSETS.IMG_SHENGJITIPS06, 415, 12, this.datagroup);
        new ActorText(atkNum + "", 115, (int) PAK_ASSETS.IMG_A03, 1, this.datagroup);
        new ActorText(skillNum + "%", 115, (int) PAK_ASSETS.IMG_ZHANDOU025, 1, this.datagroup);
        new ActorText(atkNum + "", (int) PAK_ASSETS.IMG_UI_TILI04, (int) PAK_ASSETS.IMG_A03, 1, this.datagroup);
        new ActorText(skillNum + "%", (int) PAK_ASSETS.IMG_UI_TILI04, (int) PAK_ASSETS.IMG_ZHANDOU025, 1, this.datagroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP009, 160, (int) PAK_ASSETS.IMG_TIP001, 12, this.datagroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP009, 160, (int) PAK_ASSETS.IMG_ZHANDOU013, 12, this.datagroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP009, (int) PAK_ASSETS.IMG_JIESUAN008, (int) PAK_ASSETS.IMG_TIP001, 12, this.datagroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP009, (int) PAK_ASSETS.IMG_JIESUAN008, (int) PAK_ASSETS.IMG_ZHANDOU013, 12, this.datagroup);
        new ActorText(atkNumNext + "", 247, (int) PAK_ASSETS.IMG_A03, 1, this.datagroup);
        new ActorText(skillNumNext + "%", 247, (int) PAK_ASSETS.IMG_ZHANDOU025, 1, this.datagroup);
        new ActorText(atkNumfull + "", (int) PAK_ASSETS.IMG_GONGGAO009, (int) PAK_ASSETS.IMG_A03, 1, this.datagroup);
        new ActorText(skillNumfull + "%", (int) PAK_ASSETS.IMG_GONGGAO009, (int) PAK_ASSETS.IMG_ZHANDOU025, 1, this.datagroup);
        new ActorImage((int) PAK_ASSETS.IMG_UP010, (int) PAK_ASSETS.IMG_TX_JIANSU03, (int) PAK_ASSETS.IMG_SHOP013, 12, this.datagroup);
        GNumSprite nextmoney = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + this.consume1Num, "X", -2, 0);
        nextmoney.setPosition(150.0f, 747.0f);
        this.datagroup.addActor(nextmoney);
        GNumSprite fullmoney = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + consume2Num, "X", -2, 0);
        fullmoney.setPosition(435.0f, 747.0f);
        this.datagroup.addActor(fullmoney);
        int starNum = bear.getStar();
        for (int i = 1; i <= starNum; i++) {
            new ActorImage((int) PAK_ASSETS.IMG_ZHU014, (i * 40) + 40, (int) PAK_ASSETS.IMG_QIANDAO011, 1, this.datagroup);
        }
        this.upGroup.addActor(this.datagroup);
    }

    private void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 120, 12, this.upGroup);
        int[] tt = {PAK_ASSETS.IMG_UP014, PAK_ASSETS.IMG_UP015, PAK_ASSETS.IMG_UP014};
        int[] tt1 = {PAK_ASSETS.IMG_UP012, PAK_ASSETS.IMG_UP013, PAK_ASSETS.IMG_UP012};
        SimpleButton upone = new SimpleButton(75, PAK_ASSETS.IMG_TIP018, 1, tt);
        SimpleButton upall = new SimpleButton(PAK_ASSETS.IMG_TX_JIANSU02, PAK_ASSETS.IMG_TIP018, 1, tt1);
        this.effect6 = new Effect().getEffect_Diejia(65, 176, PAK_ASSETS.IMG_ZHU018);
        this.effect7 = new Effect().getEffect_Diejia(65, PAK_ASSETS.IMG_JIESUO008, PAK_ASSETS.IMG_ZHU018);
        upone.setName(Constant.S_D);
        upall.setName(Constant.S_E);
        SimpleButton sure = new SimpleButton(206, 875, 1, new int[]{PAK_ASSETS.IMG_UP021, PAK_ASSETS.IMG_UP022});
        sure.setName(Constant.S_F);
        this.upGroup.addActor(upone);
        this.upGroup.addActor(upall);
        this.upGroup.addActor(sure);
        this.upGroup.addActor(this.effect6);
        this.upGroup.addActor(this.effect7);
    }

    /* access modifiers changed from: package-private */
    public void UpOne(int id) {
        int stateone = getStateone(0);
        this.effectgroup = new Group();
        this.effectgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.effectgroup.setSize(Animation.CurveTimeline.LINEAR, 400.0f);
        if (stateone == 3) {
            Sound.playSound(71);
            switch (id) {
                case 0:
                case 3:
                    Sound.playSound(47);
                    break;
                case 1:
                    Sound.playSound(31);
                    break;
                case 2:
                    Sound.playSound(39);
                    break;
            }
            RankData.bearUpdate(id);
            if (this.datagroup != null) {
                this.datagroup.remove();
                this.datagroup.clear();
            }
            initData(getbear(id), id);
            this.effect1 = new Effect().getEffect_Diejia(64, 140, PAK_ASSETS.IMG_UI_ZHUANPAN006);
            this.effect2 = new Effect().getEffect_Diejia(82, PAK_ASSETS.IMG_ZHUANPAN001, 253);
            this.effect3 = new Effect().getEffect_Diejia(83, 155, PAK_ASSETS.IMG_E01A);
            this.effect4 = new Effect().getEffect_Diejia(83, 155, PAK_ASSETS.IMG_ZHANDOU022);
            new Effect().addEffect(84, (int) PAK_ASSETS.IMG_ICESG05, (int) PAK_ASSETS.IMG_QIANDAO011, this.effectgroup);
            this.effectgroup.addActor(this.effect1);
            this.effectgroup.addActor(this.effect2);
            this.effectgroup.addActor(this.effect3);
            this.effectgroup.addActor(this.effect4);
            this.upGroup.addActor(this.effectgroup);
            return;
        }
        if (stateone == 1) {
        }
    }

    /* access modifiers changed from: package-private */
    public void UpAll(final int id) {
        int stateAll = getStateone(1);
        this.effectgroup = new Group();
        this.effectgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.effectgroup.setSize(Animation.CurveTimeline.LINEAR, 400.0f);
        if (stateAll == 3) {
            switch (id) {
                case 0:
                case 3:
                    Sound.playSound(47);
                    break;
                case 1:
                    Sound.playSound(31);
                    break;
                case 2:
                    Sound.playSound(39);
                    break;
            }
            RankData.oneKeyFullLevel(id);
            if (this.datagroup != null) {
                this.datagroup.remove();
                this.datagroup.clear();
            }
            initData(getbear(id), id);
            this.effect1 = new Effect().getEffect_Diejia(66, 160, PAK_ASSETS.IMG_UI_ZHUANPAN006);
            this.effect2 = new Effect().getEffect_Diejia(82, PAK_ASSETS.IMG_ZHUANPAN001, 253);
            this.effect3 = new Effect().getEffect_Diejia(83, PAK_ASSETS.IMG_JIESUAN014, PAK_ASSETS.IMG_E01A);
            this.effect4 = new Effect().getEffect_Diejia(83, PAK_ASSETS.IMG_JIESUAN014, PAK_ASSETS.IMG_ZHANDOU022);
            new Effect().addEffect(85, (int) PAK_ASSETS.IMG_ICESG05, (int) PAK_ASSETS.IMG_QIANDAO011, this.effectgroup);
            this.effectgroup.addActor(this.effect1);
            this.effectgroup.addActor(this.effect2);
            this.effectgroup.addActor(this.effect3);
            this.effectgroup.addActor(this.effect4);
            this.upGroup.addActor(this.effectgroup);
            this.effectgroup.addAction(Actions.sequence(Actions.delay(0.5f), Actions.run(new Runnable() {
                public void run() {
                    MyUpgradeInMap.this.free();
                    new MyUpgradeNoOPen(id, true, 0, RankData.roleLevel[id]);
                }
            })));
            return;
        }
        if (stateAll == 1) {
        }
    }

    /* access modifiers changed from: package-private */
    public int getStateone(int i) {
        if (this.isfull) {
            this.state = 2;
        } else if (i == 0) {
            if (this.consume3Num == 0) {
                if (RankData.spendDiamond(this.moneyone)) {
                    this.state = 3;
                } else {
                    this.state = 1;
                    if (MySwitch.isCaseA == 0) {
                        MyTip.Notenought(false);
                    } else {
                        new MyGift(7);
                    }
                }
            } else if (RankData.spendCake(this.moneyone)) {
                this.state = 3;
            } else {
                this.state = 1;
                if (MySwitch.isCaseA == 0) {
                    MyTip.Notenought(true);
                } else {
                    new MyGift(10);
                }
            }
        } else if (RankData.spendDiamond(this.moneyall)) {
            this.state = 3;
        } else {
            this.state = 1;
            if (MySwitch.isCaseA == 0) {
                MyTip.Notenought(false);
            } else {
                new MyGift(7);
            }
        }
        return this.state;
    }

    /* access modifiers changed from: package-private */
    public void initlistener(final int jiemian) {
        this.upGroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    System.out.println("buttonName:" + buttonName);
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                        System.out.println("codeName:" + codeName);
                    } else {
                        codeName = 9;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            if (jiemian == 0) {
                                MyUpgradeInMap.this.free();
                                return;
                            }
                            MyUpgradeInMap.this.free();
                            new MyEquipment();
                            return;
                        case 1:
                            MyUpgradeInMap.this.free();
                            Sound.playSound(17);
                            MyUpgradeInMap.roleId = MyUpgradeInMap.getSelectIndexInMap(true);
                            new MyUpgradeInMap(MyUpgradeInMap.roleId, jiemian);
                            return;
                        case 2:
                            MyUpgradeInMap.this.free();
                            Sound.playSound(17);
                            MyUpgradeInMap.roleId = MyUpgradeInMap.getSelectIndexInMap(false);
                            new MyUpgradeInMap(MyUpgradeInMap.roleId, jiemian);
                            return;
                        case 3:
                            MyUpgradeInMap.this.UpOne(MyUpgradeInMap.roleId);
                            return;
                        case 4:
                            MyUpgradeInMap.this.UpAll(MyUpgradeInMap.roleId);
                            return;
                        case 5:
                            if (jiemian == 0) {
                                Mymainmenu2.userChoseIndex = MyUpgradeInMap.roleId;
                                GameStage.clearAllLayers();
                                GameMain.toScreen(new MyGameMap());
                                return;
                            }
                            MyUpgradeInMap.this.free();
                            Mymainmenu2.userChoseIndex = MyUpgradeInMap.roleId;
                            new MyEquipment();
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public static int getSelectIndexInMap(boolean isLeft) {
        System.out.println("00--selectIndex:" + roleId);
        if (isLeft) {
            roleId--;
            if (roleId == -1) {
                roleId = 3;
            }
        } else {
            roleId++;
            if (roleId == 4) {
                roleId = 0;
            }
        }
        switch (roleId) {
            case 0:
            case 3:
                Sound.playSound(50);
                break;
            case 1:
                Sound.playSound(34);
                break;
            case 2:
                Sound.playSound(42);
                break;
        }
        System.out.println("11--selectIndex:" + roleId);
        return roleId;
    }

    private void timeparAction() {
        this.timeparLength = this.curretatk * this.timeparLength1;
        System.out.println("dd:" + this.timeparLength);
        this.bar.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.timeparLength, this.bar.getHeight());
        this.bar.setVisible(true);
    }

    public void exit() {
        Mymainmenu2.isZhuToUp = false;
        this.upGroup.remove();
        this.upGroup.clear();
    }
}
