package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.message.GameUITools;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyPause extends MyGroup {
    ActorShapeSprite gShapeSprite;
    Group pausegroup;

    public void init() {
        Rank.setPause(false);
        this.pausegroup = new Group();
        this.pausegroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.pausegroup, 4);
        this.gShapeSprite = new ActorShapeSprite();
        this.gShapeSprite.createRectangle(true, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 640.0f, 1136.0f);
        this.gShapeSprite.setColor(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.8f);
        this.pausegroup.addActor(this.gShapeSprite);
        initbj();
        initData();
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 4, this.pausegroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, (int) PAK_ASSETS.IMG_UI_ZHUANPAN006, 1, this.pausegroup);
        new ActorImage((int) PAK_ASSETS.IMG_ZANTING001, 180, (int) PAK_ASSETS.IMG_SHUOMING004, 1, this.pausegroup);
    }

    /* access modifiers changed from: package-private */
    public void initData() {
        new ActorText(10 + "", 160, (int) PAK_ASSETS.IMG_LGM02, 12, this.pausegroup);
        new ActorText("剩余:" + 10 + "天", (int) PAK_ASSETS.IMG_LOAD2, (int) PAK_ASSETS.IMG_LGM02, 12, this.pausegroup);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.pausegroup);
        SimpleButton jixu = new SimpleButton(PAK_ASSETS.IMG_JUXINGNENGLIANGTA, PAK_ASSETS.IMG_DAJI03, 1, new int[]{PAK_ASSETS.IMG_ZANTING002, PAK_ASSETS.IMG_ZANTING003, PAK_ASSETS.IMG_ZANTING002});
        jixu.setName("1");
        jixu.setScale(0.8f, 0.8f);
        this.pausegroup.addActor(jixu);
        SimpleButton chongxinkaishi = new SimpleButton(PAK_ASSETS.IMG_JUXINGNENGLIANGTA, PAK_ASSETS.IMG_UI_QIANDAO_BUTTON01, 1, new int[]{PAK_ASSETS.IMG_ZANTING004, PAK_ASSETS.IMG_ZANTING005, PAK_ASSETS.IMG_ZANTING004});
        chongxinkaishi.setName(Constant.S_C);
        chongxinkaishi.setScale(0.8f, 0.8f);
        this.pausegroup.addActor(chongxinkaishi);
        SimpleButton zhucaidan = new SimpleButton(PAK_ASSETS.IMG_JUXINGNENGLIANGTA, PAK_ASSETS.IMG_MAP, 1, new int[]{PAK_ASSETS.IMG_ZANTING006, PAK_ASSETS.IMG_CHONGZHI007, PAK_ASSETS.IMG_ZANTING006});
        zhucaidan.setName(Constant.S_D);
        zhucaidan.setScale(0.8f, 0.8f);
        this.pausegroup.addActor(zhucaidan);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.pausegroup.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            MyPause.this.free();
                            Rank.setPause(true);
                            break;
                        case 1:
                            MyPause.this.free();
                            Rank.setPause(true);
                            break;
                        case 2:
                            MyPause.this.free();
                            GameStage.clearAllLayers();
                            GameMain.toScreen(new Rank());
                            break;
                    }
                    super.clicked(event, x, y);
                }
            }
        });
    }

    public void exit() {
        this.pausegroup.remove();
        this.pausegroup.clear();
    }
}
