package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.message.GameUITools;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyTurretExplain extends MyGroup {
    Group explaingruop;
    ActorShapeSprite gShapeSprite;

    public void init() {
        this.explaingruop = new Group();
        this.explaingruop.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.explaingruop, 4);
        this.gShapeSprite = new ActorShapeSprite();
        this.gShapeSprite.createRectangle(true, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 640.0f, 1136.0f);
        this.gShapeSprite.setColor(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.8f);
        this.explaingruop.addActor(this.gShapeSprite);
    }

    public MyTurretExplain(int id) {
        initbj();
        initframe(id);
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_JUEHUIXUANFU, 4, this.explaingruop, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 180, (int) PAK_ASSETS.IMG_PENSHEQI_SHUOMING, 1, this.explaingruop);
    }

    /* access modifiers changed from: package-private */
    public void initframe(int id) {
        int[] iArr = new int[0];
        new ActorImage(203, 62, (int) PAK_ASSETS.IMG_SHUOMINGZI03, 12, this.explaingruop);
        new ActorImage(203, 100, 260, 12, this.explaingruop);
        new ActorImage(201, 120, (int) PAK_ASSETS.IMG_TIPS04, 12, this.explaingruop).setScale(1.4f, 1.4f);
        new ActorText(new String[]{"范围攻击,爆炸两次", ""}[id], 300, 260, 12, this.explaingruop);
        new ActorImage((int) PAK_ASSETS.IMG_SHUOMING003, 300, 300, 12, this.explaingruop);
        new ActorText(new String[]{"高", ""}[id], (int) PAK_ASSETS.IMG_JIESUO002, (int) PAK_ASSETS.IMG_ICE70, 12, this.explaingruop);
        new ActorText(new String[]{"无", ""}[id], (int) PAK_ASSETS.IMG_JIESUO002, (int) PAK_ASSETS.IMG_UI_A29, 12, this.explaingruop);
        SimpleButton know = new SimpleButton(PAK_ASSETS.IMG_JIDONGSHEXIAN, PAK_ASSETS.IMG_ONLINE003, 1, new int[]{PAK_ASSETS.IMG_SHUOMING004, PAK_ASSETS.IMG_SHUOMING005, PAK_ASSETS.IMG_SHUOMING004});
        know.setName("1");
        this.explaingruop.addActor(know);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 178, 12, this.explaingruop);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.explaingruop.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 9;
                    }
                    switch (codeName) {
                        case 0:
                            MyTurretExplain.this.free();
                            break;
                        case 1:
                            MyTurretExplain.this.free();
                            break;
                    }
                    super.clicked(event, x, y);
                }
            }
        });
    }

    public void exit() {
        this.explaingruop.remove();
        this.explaingruop.clear();
    }
}
