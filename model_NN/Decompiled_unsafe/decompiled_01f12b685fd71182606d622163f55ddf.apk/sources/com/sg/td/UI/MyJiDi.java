package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.record.JiDi;
import com.sg.td.record.LoadJiDi;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class MyJiDi extends MyGroup {
    static int money;
    static int[] role = {PAK_ASSETS.IMG_ZHU025, PAK_ASSETS.IMG_ZHU026, PAK_ASSETS.IMG_ZHU027, PAK_ASSETS.IMG_ZHU028};
    static int[] roleplay = {12, 12, 12, 12};
    ActorImage attack;
    MyImage bar;
    Actor effect3;
    Actor effect4;
    ActorShapeSprite gShapeSprite = null;
    Group jidiDategroup;
    Group jidiGroup;
    ActorImage title_7;
    ActorImage title_8;
    ActorImage title_9;

    public void init() {
        this.jidiGroup = new Group();
        this.jidiGroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.jidiGroup, 4);
        this.jidiGroup.addActor(new Mask());
        String nowlevel = String.valueOf(RankData.jidiLevel);
        initbj();
        initData(LoadJiDi.jidiData.get(nowlevel));
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 168, 4, this.jidiGroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 138, 1, this.jidiGroup);
        new ActorImage((int) PAK_ASSETS.IMG_JIDI008, 320, 108, 1, this.jidiGroup);
        new ActorImage((int) PAK_ASSETS.IMG_JIDI002, (int) PAK_ASSETS.IMG_SHENGJITIPS06, (int) PAK_ASSETS.IMG_DAJI03, 1, this.jidiGroup);
        new ActorImage((int) PAK_ASSETS.IMG_JIDI007, 140, (int) PAK_ASSETS.IMG_KAWEILI05, 1, this.jidiGroup);
        new ActorImage((int) PAK_ASSETS.IMG_JIDI001, 140, (int) PAK_ASSETS.IMG_QIANDAO011, 1, this.jidiGroup);
        this.title_7 = new ActorImage((int) PAK_ASSETS.IMG_UP009, (int) PAK_ASSETS.IMG_ZHUANPAN001, (int) PAK_ASSETS.IMG_PENSHEQI_SHUOMING, 1, this.jidiGroup);
        this.title_8 = new ActorImage((int) PAK_ASSETS.IMG_UP009, (int) PAK_ASSETS.IMG_ZHUANPAN001, (int) PAK_ASSETS.IMG_DAJI06A, 1, this.jidiGroup);
        this.title_9 = new ActorImage((int) PAK_ASSETS.IMG_SHOP044, (int) PAK_ASSETS.IMG_MAP1, (int) PAK_ASSETS.IMG_TIPS04, 1, this.jidiGroup);
        this.title_9.setScale(0.8f, 0.8f);
    }

    /* access modifiers changed from: package-private */
    public void initData(JiDi jd) {
        this.jidiDategroup = new Group();
        int level = jd.getLevel();
        int nextlevel = jd.getNextLevel();
        int life = jd.getLife();
        int nextlife = jd.getNextLife();
        money = jd.getMoney();
        String descp = jd.getDescp();
        boolean isFullLevel = jd.isFullLevel();
        new ActorImage((int) PAK_ASSETS.IMG_UP023, (int) PAK_ASSETS.IMG_UI_TILI04, (int) PAK_ASSETS.IMG_LENGGUANG_SHUOMING, 1, this.jidiDategroup);
        GNumSprite level1 = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + level, "", -2, 4);
        level1.setPosition((float) 424, (float) PAK_ASSETS.IMG_LENGGUANG_SHUOMING);
        this.jidiDategroup.addActor(level1);
        GNumSprite life1 = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + life, "", -2, 4);
        life1.setPosition((float) 400, (float) PAK_ASSETS.IMG_DAJI06);
        this.jidiDategroup.addActor(life1);
        GNumSprite money1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + money, "X", -2, 4);
        money1.setPosition((float) 505, (float) PAK_ASSETS.IMG_TIPS04);
        this.jidiDategroup.addActor(money1);
        GNumSprite nowlife = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + life, "X", -2, 4);
        nowlife.setPosition((float) 160, (float) PAK_ASSETS.IMG_QIANDAO010);
        this.jidiDategroup.addActor(nowlife);
        int[] t = {PAK_ASSETS.IMG_JIDI003, PAK_ASSETS.IMG_JIDI004, PAK_ASSETS.IMG_JIDI003};
        int[] tt = {PAK_ASSETS.IMG_JIDI005, PAK_ASSETS.IMG_JIDI006, PAK_ASSETS.IMG_JIDI005};
        if (isFullLevel) {
            if (this.title_7 != null) {
                this.title_7.setVisible(false);
            }
            if (this.title_8 != null) {
                this.title_8.setVisible(false);
            }
            if (this.title_9 != null) {
                this.title_9.setVisible(false);
                money1.setVisible(false);
            }
        } else {
            new ActorImage((int) PAK_ASSETS.IMG_UP023, (int) PAK_ASSETS.IMG_ONLINE003, (int) PAK_ASSETS.IMG_LENGGUANG_SHUOMING, 1, this.jidiDategroup);
            GNumSprite nextlevel1 = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + nextlevel, "", -2, 4);
            nextlevel1.setPosition((float) PAK_ASSETS.IMG_PUBLIC016, (float) PAK_ASSETS.IMG_LENGGUANG_SHUOMING);
            this.jidiDategroup.addActor(nextlevel1);
            GNumSprite nextlife1 = new GNumSprite((int) PAK_ASSETS.IMG_UP024, "" + nextlife, "", -2, 4);
            nextlife1.setPosition((float) PAK_ASSETS.IMG_ONLINE006, (float) PAK_ASSETS.IMG_DAJI06);
            this.jidiDategroup.addActor(nextlife1);
        }
        if (!isFullLevel) {
            tt = t;
        }
        SimpleButton simpleButton = new SimpleButton(PAK_ASSETS.IMG_JUXINGNENGLIANGTA, PAK_ASSETS.IMG_MAP, 1, tt);
        simpleButton.setName(isFullLevel ? "1" : Constant.S_C);
        this.jidiDategroup.addActor(simpleButton);
        StringBuffer stringBuffer = new StringBuffer(jd.getDescp());
        stringBuffer.insert(17, "\n");
        if (stringBuffer.length() > 34) {
            stringBuffer.insert(35, "\n");
        }
        if (stringBuffer.length() > 68) {
            stringBuffer.insert(68, "\n");
        }
        new ActorText(stringBuffer.toString(), (int) PAK_ASSETS.IMG_SHUOMINGZI03, (int) PAK_ASSETS.IMG_UI_TILI02, 12, this.jidiDategroup);
        this.jidiGroup.addActor(this.jidiDategroup);
    }

    private void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 120, 12, this.jidiGroup);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.jidiGroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    System.out.println("buttonName:" + buttonName);
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                        System.out.println("codeName:" + codeName);
                    } else {
                        codeName = 9;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MyJiDi.this.free();
                            return;
                        case 1:
                            System.out.println("已满级");
                            return;
                        case 2:
                            MyJiDi.this.UPone();
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public void UPone() {
        if (RankData.spendCake(money)) {
            Sound.playSound(71);
            this.effect3 = new Effect().getEffect_Diejia(83, PAK_ASSETS.IMG_QIANDAO007, PAK_ASSETS.IMG_LENGDONG_SHUOMING);
            this.effect4 = new Effect().getEffect_Diejia(83, PAK_ASSETS.IMG_QIANDAO007, PAK_ASSETS.IMG_DAJI01);
            new Effect().addEffect(84, (int) PAK_ASSETS.IMG_ICESG05, 181, this.jidiGroup);
            this.jidiGroup.addActor(this.effect3);
            this.jidiGroup.addActor(this.effect4);
            RankData.updateJiDi();
            if (this.jidiDategroup != null) {
                this.jidiDategroup.remove();
                this.jidiDategroup.clear();
            }
            initData(LoadJiDi.jidiData.get(String.valueOf(RankData.jidiLevel)));
            return;
        }
        Sound.playButtonPressed();
        if (MySwitch.isCaseA == 0) {
            MyTip.Notenought(true);
        } else {
            new MyGift(10);
        }
    }

    public void exit() {
        Rank.clearHomeUpEvent();
        this.jidiGroup.remove();
        this.jidiGroup.clear();
    }
}
