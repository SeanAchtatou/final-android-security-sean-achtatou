package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIData;
import com.sg.td.message.MyUIGetFont;
import com.sg.td.record.LoadGet;
import com.sg.tools.MyGroup;
import com.sg.tools.MySprite;
import com.sg.util.GameStage;

public class MyGift extends MyGroup implements GameConstant {
    static int[] frame = {165, 162, 505, 0, 143, 0, 0, 179, 0, 0, 148, 151, 507, 504, 516};
    static Group giftgroup;
    public static String[] infor_name = {"幸运红包", "新手红包", "成长礼包", "战斗宝盒", "博士秘藏宝箱", "补充钻石（小）", "补充钻石（中）", "补充钻石（大）", "补充恐龙币（小）", "补充恐龙币（中）", "补充恐龙币（大）", "铠甲雷古曼"};
    static Group jinliback;
    static Group jinlipay;
    static int[] price_A_yifen = {167, 163, 163, 0, 149, 0, 0, 180, 0, 0, 149, 152, 147, 144, 178};
    static int[] price_A_yimao = {166, 163, 163, 0, 149, 0, 0, 180, 0, 0, 149, 152, 147, 144, 178};
    static int[] price_B_yifen = {169, 164, 164, 0, 150, 0, 0, 181, 0, 0, 150, 153};
    static int[] price_B_yimao = {168, 164, 164, 0, 150, 0, 0, 181, 0, 0, 150, 153};

    public void init() {
    }

    public MyGift(int payIndex) {
        if (payIndex == GMessage.PP_TEMPgift) {
            GMessage.isCalled7++;
            RankData.saveRecord();
        }
        giftgroup = new Group();
        giftgroup.addActor(new Mask());
        GameStage.addActor(giftgroup, 4);
        initframe(payIndex);
    }

    /* access modifiers changed from: package-private */
    public void initframe(int id) {
        float[] price;
        String lqOrgm;
        ActorImage priceImg;
        SimpleButton goumai;
        SimpleButton quxiao;
        ActorImage priceImg2;
        new ActorImage(frame[id], 320, (int) PAK_ASSETS.IMG_MAP, 1, giftgroup);
        if (GameSpecial.price == 0) {
            if (MySwitch.Yifen) {
                priceImg2 = new ActorImage(price_A_yifen[id], 92, 416, 1, giftgroup);
            } else {
                priceImg2 = new ActorImage(price_A_yimao[id], 92, 416, 1, giftgroup);
            }
            if (id == 11) {
                priceImg2.setPosition((float) 9, (float) PAK_ASSETS.IMG_LEIGUMAN08A);
            }
        } else if (GameSpecial.price == 1) {
            if (MySwitch.Yifen) {
                priceImg = new ActorImage(price_B_yifen[id], 95, (int) PAK_ASSETS.IMG_ZHU004, 1, giftgroup);
            } else {
                priceImg = new ActorImage(price_B_yimao[id], 95, (int) PAK_ASSETS.IMG_ZHU004, 1, giftgroup);
            }
            if (id == 11) {
                priceImg.setPosition(34.0f, 719.0f);
            }
        } else {
            if (MySwitch.Yifen) {
                price = GMessage.BUY_PRICE_f_jd;
            } else {
                price = GMessage.BUY_PRICE_f;
            }
            if (GameSpecial.button == 1) {
                lqOrgm = "领取";
            } else {
                lqOrgm = "购买";
            }
            new ActorText("点击" + lqOrgm + "立即支付" + price[id] + "元，客服电话：4008289368", 320, 1108, 1, giftgroup).setColor(MyUIGetFont.priceColor);
        }
        System.out.println("GameSpecial.button:" + GameSpecial.button);
        if (GameSpecial.button == 0) {
            goumai = new SimpleButton(74, PAK_ASSETS.IMG_SHOP042, 1, new int[]{145, 146});
            quxiao = new SimpleButton(PAK_ASSETS.IMG_LIGHT01, PAK_ASSETS.IMG_SHOP042, 1, new int[]{158, 159});
            if (id == 11) {
                goumai.setPosition((float) 74, (float) PAK_ASSETS.IMG_ZHANDOU026);
                quxiao.setPosition((float) PAK_ASSETS.IMG_LIGHT01, (float) PAK_ASSETS.IMG_ZHANDOU026);
            }
        } else if (GameSpecial.button == 2) {
            goumai = new SimpleButton(192, PAK_ASSETS.IMG_SHOP034, 1, new int[]{145, 146});
            quxiao = new SimpleButton(PAK_ASSETS.IMG_E02, PAK_ASSETS.IMG_UI_ZUANSHI04, 1, new int[]{562});
            if (id == 11) {
                goumai.setPosition((float) 192, (float) PAK_ASSETS.IMG_ZHANDOU026);
                quxiao.setPosition((float) PAK_ASSETS.IMG_E02, (float) PAK_ASSETS.IMG_DAJI10);
            }
        } else {
            goumai = new SimpleButton(192, PAK_ASSETS.IMG_SHOP034, 1, new int[]{154, 155});
            quxiao = new SimpleButton(PAK_ASSETS.IMG_C01, PAK_ASSETS.IMG_YUSHAN01, 1, new int[]{562});
            if (id == 11) {
                goumai.setPosition((float) 192, (float) PAK_ASSETS.IMG_ZHANDOU026);
                quxiao.setPosition((float) PAK_ASSETS.IMG_F02, (float) PAK_ASSETS.IMG_UI_ZHUANPAN006);
            }
        }
        if (GameSpecial.XLeftTop) {
            quxiao.setPosition((float) 25, (float) PAK_ASSETS.IMG_UI_LIGHT01);
            if (id == 11) {
                quxiao.setPosition((float) 25, (float) PAK_ASSETS.IMG_DAJI07);
            }
            if (GameSpecial.getLeftTop) {
                quxiao.setPosition((float) PAK_ASSETS.IMG_C01, (float) PAK_ASSETS.IMG_UI_LIGHT01);
                if (id == 11) {
                    quxiao.setPosition((float) PAK_ASSETS.IMG_C01, (float) PAK_ASSETS.IMG_DAJI07);
                }
            }
        }
        if (GameSpecial.getRightCorner) {
            goumai.setPosition((float) PAK_ASSETS.IMG_TX_FLASH0BDJ, (float) PAK_ASSETS.IMG_SHOP034);
            if (id == 11) {
                goumai.setPosition((float) PAK_ASSETS.IMG_TIPS01, (float) PAK_ASSETS.IMG_ZHANDOU011);
            }
        }
        if (GameSpecial.hitAnyWhere) {
            touchAnyWhere(id);
        }
        giftgroup.addActor(goumai);
        giftgroup.addActor(quxiao);
        final int i = id;
        goumai.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyGift.this.free();
                Sound.playButtonPressed();
                GMessage.send(i);
            }
        });
        final int i2 = id;
        quxiao.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                MyGift.this.free();
                System.out.println("gift isCalled7:" + GMessage.isCalled7);
                if (GMessage.isCalled7 == 1 && i2 == GMessage.PP_TEMPgift && MySwitch.isCaseA != 0) {
                    MyTip.FristBuyedFail();
                }
            }
        });
        if (GameSpecial.addgetRightTop) {
            SimpleButton getRightTop = new SimpleButton(PAK_ASSETS.IMG_C01, PAK_ASSETS.IMG_UI_LIGHT01, 1, new int[]{PAK_ASSETS.IMG_PUBLIC017, PAK_ASSETS.IMG_PUBLIC017});
            if (GameSpecial.getLeftTop) {
                getRightTop.setPosition((float) 25, (float) PAK_ASSETS.IMG_UI_LIGHT01);
            }
            final int i3 = id;
            getRightTop.addListener(new ClickListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    MyGift.this.free();
                    Sound.playButtonPressed();
                    GMessage.send(i3);
                }
            });
            if (id == 11) {
                getRightTop.setPosition((float) PAK_ASSETS.IMG_C01, (float) PAK_ASSETS.IMG_DAJI07);
                if (GameSpecial.getLeftTop) {
                    getRightTop.setPosition((float) 25, (float) PAK_ASSETS.IMG_DAJI07);
                }
            }
            giftgroup.addActor(getRightTop);
        }
        if (GameSpecial.extendedX) {
            addCloseSise(quxiao, quxiao.getX(), quxiao.getY());
        }
        if (GameSpecial.delay && quxiao != null) {
            addAphleAction(quxiao);
        }
        if (!GameSpecial.hand) {
            return;
        }
        if (id == 11) {
            giftgroup.addActor(handGroup(320, PAK_ASSETS.IMG_SHOP014));
        } else {
            giftgroup.addActor(handGroup(320, PAK_ASSETS.IMG_UP019));
        }
    }

    public static Group handGroup(int f, int g) {
        Group hand = new Group();
        MySprite sprite = new MySprite(ANIMATION_NAME[0] + ".json", "dianji");
        sprite.setPosition((float) f, (float) g);
        sprite.setTouchable(Touchable.disabled);
        hand.addActor(sprite);
        return hand;
    }

    public static void addAphleAction(Actor actor) {
        actor.addAction(Actions.sequence(Actions.alpha(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR), Actions.delay(0.8f), Actions.alpha(1.0f, 0.3f)));
    }

    private static void touchAnyWhere(final int id) {
        Actor actor = new Actor();
        actor.setBounds(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 640.0f, 1136.0f);
        giftgroup.addActor(actor);
        actor.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                GMessage.send(id);
            }
        });
    }

    public void addCloseSise(SimpleButton quxiao, float f, float h) {
        if (quxiao != null) {
            Group group = new Group();
            group.setPosition(f, h);
            group.setName("close");
            group.setSize(quxiao.getWidth() * 2.0f, quxiao.getHeight() * 2.0f);
            quxiao.setPosition(quxiao.getWidth() / 2.0f, quxiao.getHeight() / 2.0f);
            group.addActor(quxiao);
            group.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    Sound.playButtonClosed();
                    MyGift.this.free();
                }
            });
            giftgroup.addActor(group);
        }
    }

    public void exit() {
        giftgroup.remove();
        giftgroup.clear();
    }

    public static void choseJinLiPay(final int id) {
        if (jinlipay != null) {
            jinlipay.remove();
            jinlipay.clear();
        }
        jinlipay = new Group();
        jinlipay.addActor(new Mask());
        GameStage.addActor(jinlipay, 4);
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_DAJI10, 3, jinlipay, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, (int) PAK_ASSETS.IMG_UI_ZHUANPAN006, 1, jinlipay);
        new ActorImage(172, 320, (int) PAK_ASSETS.IMG_010POINT_PURPLE, 1, jinlipay);
        new ActorImage(173, 320, (int) PAK_ASSETS.IMG_2X1, 1, jinlipay);
        SimpleButton weixin = new SimpleButton(155, 420, 1, new int[]{170, 170});
        jinlipay.addActor(weixin);
        SimpleButton zhifubao = new SimpleButton(PAK_ASSETS.IMG_JIDI003, 420, 1, new int[]{171, 171});
        jinlipay.addActor(zhifubao);
        ActorButton close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 260, 12, jinlipay);
        weixin.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MySwitch.isWeiXin = true;
                GameMain.dialog.sendMessage(id);
            }
        });
        zhifubao.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MySwitch.isWeiXin = false;
                GameMain.dialog.sendMessage(id);
            }
        });
        close.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyGift.jinlipay.remove();
                MyGift.jinlipay.clear();
                if (MySwitch.isJinLi) {
                    MyGift.jinliBack(id);
                }
            }
        });
    }

    public static void jinliBack(final int id) {
        if (jinliback != null) {
            jinliback.remove();
            jinliback.clear();
        }
        jinliback = new Group();
        jinliback.addActor(new Mask());
        GameStage.addActor(jinliback, 4);
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_DAJI10, 3, jinliback, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, (int) PAK_ASSETS.IMG_UI_ZHUANPAN006, 1, jinliback);
        SimpleButton jixu = new SimpleButton(40, 420, 1, new int[]{174, 175});
        jinliback.addActor(jixu);
        SimpleButton fanhui = new SimpleButton(PAK_ASSETS.IMG_SHENGJITIPS01, 420, 1, new int[]{176, 177});
        jinliback.addActor(fanhui);
        jixu.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyGift.jinliback.remove();
                MyGift.jinliback.clear();
                MyGift.choseJinLiPay(id);
            }
        });
        fanhui.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MyGift.jinliback.remove();
                MyGift.jinliback.clear();
            }
        });
    }

    public static void guoqinglb() {
        final Group gq = new Group();
        gq.addActor(new Mask());
        GameStage.addActor(gq, 4);
        new ActorImage((int) PAK_ASSETS.IMG_GUOQING001, 320, (int) PAK_ASSETS.IMG_MAP, 1, gq);
        SimpleButton goumaiqg = new SimpleButton(192, PAK_ASSETS.IMG_SHOP034, 1, new int[]{154, 155});
        gq.addActor(goumaiqg);
        goumaiqg.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                LoadGet.loadGetDate();
                new MyGet(LoadGet.getData.get("Shop"), 0, 4);
                MyUIData.setGethdlb(true);
                RankData.saveRecord();
                RankData.addHoneyNum(2);
                RankData.addDiamondNum(100);
                RankData.addCakeNum(1000);
                gq.remove();
                gq.clear();
            }
        });
    }
}
