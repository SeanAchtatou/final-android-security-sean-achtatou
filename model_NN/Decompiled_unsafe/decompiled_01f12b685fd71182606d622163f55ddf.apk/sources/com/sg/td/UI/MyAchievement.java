package com.sg.td.UI;

import cn.egame.terminal.paysdk.FailedCode;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GClipGroup;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MyUIGetFont;
import com.sg.td.record.AchieveData;
import com.sg.td.record.LoadAchieve;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;
import com.tendcloud.tenddata.y;

public class MyAchievement extends MyGroup {
    static Group achgroup = new Group();
    int[] Completedbattle;
    int[] Completedcollection;
    int[] Completedfood;
    int[] Completedrank;
    ActorButton close;
    GNumSprite completed;
    Group group2;
    Group initbjGroup;
    boolean isCanlingqu;
    ActorImage tishiImage;
    ActorImage tishibackImage;
    ActorSprite titleImage1;
    ActorSprite titleImage2;
    ActorSprite titleImage3;
    ActorSprite titleImage4;
    ActorImage title_1;
    ActorImage title_2;

    public void init() {
        this.initbjGroup = new Group();
        addActor(new Mask());
        initbj();
        LoadAchieve.loadAchieveData();
        initRunk(LoadAchieve.achieveData.get("Rank").achieves, 0);
        initbutton();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        this.initbjGroup = this;
        GameUITools.GetbackgroundImage(320, 168, 11, this, false, false);
        new ActorImage(36, 320, 303, 1, this);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 138, 1, this);
        this.title_2 = new ActorImage(9, 320, 108, 1, this);
        this.tishiImage = new ActorImage(0, 250, (int) PAK_ASSETS.IMG_DAJI03, 1, this);
        this.Completedrank = RankData.getAchieveNum(0);
        this.Completedfood = RankData.getAchieveNum(1);
        this.Completedbattle = RankData.getAchieveNum(2);
        this.Completedcollection = RankData.getAchieveNum(3);
        this.titleImage1 = new ActorSprite(30, 203, 4, 5, 1);
        this.titleImage1.setTexture(1);
        this.titleImage2 = new ActorSprite(175, 203, 4, 6, 2);
        this.titleImage3 = new ActorSprite(320, 203, 4, 7, 3);
        this.titleImage4 = new ActorSprite((int) PAK_ASSETS.IMG_QIANDAO007, 203, 4, 8, 4);
        getcomp(LoadAchieve.achieveData.get("Rank").achieves, this.Completedrank);
        this.titleImage1.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Sound.playButtonPressed();
                MyAchievement.this.titleImage1.setTexture(1);
                MyAchievement.this.titleImage2.setTexture(0);
                MyAchievement.this.titleImage3.setTexture(0);
                MyAchievement.this.titleImage4.setTexture(0);
                AchieveData.Achieve[] achieves = LoadAchieve.achieveData.get("Rank").achieves;
                MyAchievement.this.initRunk(achieves, 0);
                MyAchievement.this.getcomp(achieves, MyAchievement.this.Completedrank);
                MyAchievement.this.reset();
            }
        });
        this.titleImage2.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Sound.playButtonPressed();
                MyAchievement.this.titleImage1.setTexture(0);
                MyAchievement.this.titleImage2.setTexture(1);
                MyAchievement.this.titleImage3.setTexture(0);
                MyAchievement.this.titleImage4.setTexture(0);
                AchieveData.Achieve[] achieves = LoadAchieve.achieveData.get("Food").achieves;
                MyAchievement.this.initRunk(achieves, 1);
                MyAchievement.this.getcomp(achieves, MyAchievement.this.Completedfood);
                MyAchievement.this.reset();
            }
        });
        this.titleImage3.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Sound.playButtonPressed();
                MyAchievement.this.titleImage1.setTexture(0);
                MyAchievement.this.titleImage2.setTexture(0);
                MyAchievement.this.titleImage3.setTexture(1);
                MyAchievement.this.titleImage4.setTexture(0);
                AchieveData.Achieve[] achieves = LoadAchieve.achieveData.get("Battle").achieves;
                MyAchievement.this.initRunk(achieves, 2);
                MyAchievement.this.getcomp(achieves, MyAchievement.this.Completedbattle);
                MyAchievement.this.reset();
            }
        });
        this.titleImage4.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Sound.playButtonPressed();
                MyAchievement.this.titleImage1.setTexture(0);
                MyAchievement.this.titleImage2.setTexture(0);
                MyAchievement.this.titleImage3.setTexture(0);
                MyAchievement.this.titleImage4.setTexture(1);
                AchieveData.Achieve[] achieves = LoadAchieve.achieveData.get("Collection").achieves;
                MyAchievement.this.initRunk(achieves, 3);
                MyAchievement.this.getcomp(achieves, MyAchievement.this.Completedcollection);
                MyAchievement.this.reset();
            }
        });
        this.initbjGroup.addActor(this.titleImage1);
        this.initbjGroup.addActor(this.titleImage2);
        this.initbjGroup.addActor(this.titleImage3);
        this.initbjGroup.addActor(this.titleImage4);
        this.initbjGroup.addActor(this.group2);
        GameStage.addActor(this, 4);
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        achgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    /* access modifiers changed from: package-private */
    public void getcomp(AchieveData.Achieve[] achieves, int[] num) {
        if (this.group2 != null) {
            this.group2.remove();
            this.group2.clear();
        }
        this.group2 = new Group();
        new ActorText(num[1] + "/" + achieves.length, 410, (int) PAK_ASSETS.IMG_DAJI03, 1, this.group2);
        this.completed = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + num[1], "X", -2, 4);
        if (this.Completedrank[0] > 0) {
            new ActorImage(96, 163, (int) PAK_ASSETS.IMG_LAOLONGTUBIAO_SHUOMING, 1, this.group2);
            new ActorText("" + this.Completedrank[0], 163, (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA2, 1, this.group2);
        }
        if (this.Completedfood[0] > 0) {
            new ActorImage(96, (int) PAK_ASSETS.IMG_GUANQIANDQ, (int) PAK_ASSETS.IMG_LAOLONGTUBIAO_SHUOMING, 1, this.group2);
            new ActorText("" + this.Completedfood[0], (int) PAK_ASSETS.IMG_GUANQIANDQ, (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA2, 1, this.group2);
        }
        if (this.Completedbattle[0] > 0) {
            new ActorImage(96, (int) PAK_ASSETS.IMG_JIESUO005, (int) PAK_ASSETS.IMG_LAOLONGTUBIAO_SHUOMING, 1, this.group2);
            new ActorText("" + this.Completedbattle[0], (int) PAK_ASSETS.IMG_JIESUO005, (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA2, 1, this.group2);
        }
        if (this.Completedcollection[0] > 0) {
            new ActorImage(96, (int) PAK_ASSETS.IMG_FUHUO004, (int) PAK_ASSETS.IMG_LAOLONGTUBIAO_SHUOMING, 1, this.group2);
            new ActorText("" + this.Completedcollection[0], (int) PAK_ASSETS.IMG_FUHUO004, (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA2, 1, this.group2);
        }
        this.initbjGroup.addActor(this.group2);
    }

    /* access modifiers changed from: package-private */
    public void initRunk(AchieveData.Achieve[] achieves, int id) {
        for (int i = 0; i < achieves.length; i++) {
            AchieveData.Achieve achieve = achieves[i];
            achieve(320, (i * 120) + PAK_ASSETS.IMG_UI_TILI02, achieve.getIcon(), achieve.getDescp(), achieve.getDegreeDescp(), achieve.getAward(), achieve.unfinished(), achieve, achieves, id);
        }
        addmove();
    }

    /* access modifiers changed from: package-private */
    public void initGetButton() {
    }

    /* access modifiers changed from: package-private */
    public void achieve(int x, int y, String iconName, String descp, String complete, int award, boolean isComplete, AchieveData.Achieve achieve, AchieveData.Achieve[] achieves, int id) {
        boolean isLingqu = achieve.isRecieve();
        new ActorImage(10, x, y, 1, achgroup).setTouchable(Touchable.enabled);
        String str = iconName;
        achgroup.addActor(new MyImage(str, (float) (x + FailedCode.REASON_CODE_INIT_FAILED), (float) y, 4));
        new ActorText(descp, x - 135, y - 35, 12, achgroup).setColor(MyUIGetFont.achieveDescp1Color);
        new ActorText(complete, x - 125, y - 10, 12, achgroup).setColor(MyUIGetFont.achievecomplete1Color);
        GNumSprite timeSprite = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "X" + award, "X", -2, 4);
        timeSprite.setPosition((float) (x - 30), (float) (y + 30));
        achgroup.addActor(timeSprite);
        final ActorSprite complete2 = new ActorSprite(x + 124, y - 12, 1, 113, 11, PAK_ASSETS.IMG_PUBLIC009);
        if (isComplete) {
            complete2.setTexture(0);
        } else if (isLingqu) {
            complete2.setTexture(1);
            complete2.setPosition((float) (x + 100), (float) (y - 12));
        } else {
            complete2.setPosition((float) (x + 139), (float) (y - 30));
            complete2.setTexture(2);
        }
        final AchieveData.Achieve achieve2 = achieve;
        final int i = award;
        final int i2 = x;
        final int i3 = y;
        final int i4 = id;
        final AchieveData.Achieve[] achieveArr = achieves;
        complete2.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float xx, float yy, int pointer, int button) {
                if (achieve2.isRecieve()) {
                    new MyGetAchievementEffect(i);
                    complete2.setTexture(2);
                    complete2.setPosition((float) (i2 + 139), (float) (i3 - 30));
                    achieve2.setRecieve();
                    RankData.addDiamondNum(i);
                    switch (i4) {
                        case 0:
                            MyAchievement.this.Completedrank = RankData.getAchieveNum(0);
                            MyAchievement.this.getcomp(achieveArr, MyAchievement.this.Completedrank);
                            break;
                        case 1:
                            MyAchievement.this.Completedfood = RankData.getAchieveNum(1);
                            MyAchievement.this.getcomp(achieveArr, MyAchievement.this.Completedfood);
                            break;
                        case 2:
                            MyAchievement.this.Completedbattle = RankData.getAchieveNum(2);
                            MyAchievement.this.getcomp(achieveArr, MyAchievement.this.Completedbattle);
                            break;
                        case 3:
                            MyAchievement.this.Completedcollection = RankData.getAchieveNum(3);
                            MyAchievement.this.getcomp(achieveArr, MyAchievement.this.Completedcollection);
                            break;
                    }
                }
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }
        });
        achgroup.addActor(complete2);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        this.close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "close", (int) PAK_ASSETS.IMG_C01, 130, 12, this);
        this.close.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("退出");
                Sound.playButtonClosed();
                MyAchievement.this.free();
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void addmove() {
        Group group = new Group();
        GClipGroup achClipgroup = new GClipGroup();
        achClipgroup.setClipArea(3, PAK_ASSETS.IMG_ICE70, PAK_ASSETS.IMG_SHUOMINGKUANG, PAK_ASSETS.IMG_SHEZHI004);
        achClipgroup.addActor(achgroup);
        achgroup.addListener(GameUITools.getMoveListener(achgroup, 1850.0f, 120.0f, 5.0f, false));
        group.addActor(achClipgroup);
        addActor(group);
        new ActorImage(35, 320, (int) y.b, 1, this);
    }

    public void exit() {
        remove();
        clear();
    }
}
