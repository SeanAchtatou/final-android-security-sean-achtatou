package com.sg.td.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Message;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GSecretUtil;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIGetFont;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyMainTop extends MyGroup implements GameConstant {
    ActorButton achievement;
    GNumSprite bearCion;
    GNumSprite diamond;
    Get get;
    ActorImage hasAchievement;
    ActorImage hasAward;
    ActorImage hasMission;
    /* access modifiers changed from: private */
    public int index = -1;
    Mask mask;
    ActorButton mission;
    ActorButton more;
    ActorButton moregame;
    ActorButton notice;
    ActorButton shop;
    GNumSprite star;
    GNumSprite tiliNum1;
    ActorImage title;
    Group top;

    public void init() {
        this.top = new Group() {
            public void act(float delta) {
                super.act(delta);
                int diamondNum = RankData.getDiamondNum();
                int starNum = RankData.getStarNum();
                int bearCionNum = RankData.getCakeNum();
                if (bearCionNum > 99999) {
                    bearCionNum = 99999;
                }
                if (diamondNum > 99999) {
                    diamondNum = 99999;
                }
                if (RankData.getEndlessPowerDays() <= 0 && MyGameMap.isInMap && MyMainTop.this.tiliNum1 != null) {
                    MyMainTop.this.tiliNum1.setNum(RankData.getPowerNum());
                }
                MyMainTop.this.bearCion.setNum(bearCionNum);
                MyMainTop.this.diamond.setNum(diamondNum);
                if (MyMainTop.this.index != -1) {
                    MyMainTop.this.getGift();
                }
                if (RankData.showTaskTip()) {
                    if (MyMainTop.this.hasMission != null) {
                        MyMainTop.this.hasMission.setVisible(true);
                    }
                } else if (MyMainTop.this.hasMission != null) {
                    MyMainTop.this.hasMission.setVisible(false);
                }
                if (RankData.showAchieveTip()) {
                    if (MyMainTop.this.hasAchievement != null) {
                        MyMainTop.this.hasAchievement.setVisible(true);
                    }
                } else if (MyMainTop.this.hasAchievement != null) {
                    MyMainTop.this.hasAchievement.setVisible(false);
                }
                if (RankData.showOnlineTip()) {
                    if (MyMainTop.this.hasAward != null) {
                        MyMainTop.this.hasAward.setVisible(true);
                    }
                } else if (MyMainTop.this.hasAward != null) {
                    MyMainTop.this.hasAward.setVisible(false);
                }
            }
        };
        this.top.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.top, 3);
        initgruoplistener();
    }

    public MyMainTop(boolean isMap) {
        initframe();
        if (isMap) {
            initMap();
        }
    }

    /* access modifiers changed from: package-private */
    public void initframe() {
        this.title = new ActorImage((int) PAK_ASSETS.IMG_ZHU001, -13, -10, 12, this.top);
        this.shop = new ActorButton((int) PAK_ASSETS.IMG_ZHU002, "0", 30, 80, 12, this.top);
        this.mission = new ActorButton((int) PAK_ASSETS.IMG_ZHU003, "1", 130, 80, 12, this.top);
        this.achievement = new ActorButton((int) PAK_ASSETS.IMG_ZHU004, Constant.S_C, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 80, 12, this.top);
        this.more = new ActorButton((int) PAK_ASSETS.IMG_ZHU005, Constant.S_D, (int) PAK_ASSETS.IMG_PPSG04, 80, 12, this.top);
        if (MySwitch.isMoreGame) {
            this.moregame = new ActorButton((int) PAK_ASSETS.IMG_ZHU040, "14", (int) PAK_ASSETS.IMG_JIDI006, 80, 12, this.top);
        } else if (MySwitch.isHaveNotice) {
            this.notice = new ActorButton((int) PAK_ASSETS.IMG_ZHU041, "15", (int) PAK_ASSETS.IMG_JIDI006, 80, 12, this.top);
        }
        this.hasMission = new ActorImage((int) PAK_ASSETS.IMG_JIESUAN015, 184, 65, 12, this.top);
        this.hasMission.setScale(0.8f, 0.8f);
        this.hasMission.addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.5f))));
        this.hasAchievement = new ActorImage((int) PAK_ASSETS.IMG_JIESUAN015, (int) PAK_ASSETS.IMG_BUFFCZ01, 65, 12, this.top);
        this.hasAchievement.setScale(0.8f, 0.8f);
        this.hasAchievement.addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.5f))));
        this.top.addActor(new Effect().getEffect_Diejia(75, PAK_ASSETS.IMG_KAWEILI04, 95));
        initData();
    }

    /* access modifiers changed from: package-private */
    public void initMap() {
        new ActorImage(500, 0, 188, 12, this.top);
        if (RankData.getEndlessPowerDays() > 0) {
            new ActorImage((int) PAK_ASSETS.IMG_CHUANGGUAN008, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, (int) PAK_ASSETS.IMG_JUEHUIXUANFU2, 12, this.top);
            GNumSprite dayNum = new GNumSprite((int) PAK_ASSETS.IMG_CHUANGGUAN009, "" + RankData.getEndlessPowerDays(), "", -2, 4);
            dayNum.setPosition(375.0f, (float) PAK_ASSETS.IMG_SHENSHENYILONGGONG);
            this.top.addActor(dayNum);
        }
        new ActorButton((int) PAK_ASSETS.IMG_ZHU031, Constant.S_E, 0, (int) PAK_ASSETS.IMG_BUFF_GONGJI, 12, this.top);
        new ActorButton((int) PAK_ASSETS.IMG_ZHU033, Constant.S_F, 0, (int) PAK_ASSETS.IMG_UI002, 12, this.top);
        new ActorButton((int) PAK_ASSETS.IMG_ZHU030, "7", (int) PAK_ASSETS.IMG_PAO004, (int) PAK_ASSETS.IMG_PUBLIC012, 12, this.top);
        this.hasAward = new ActorImage((int) PAK_ASSETS.IMG_JIESUAN015, (int) PAK_ASSETS.IMG_G02, (int) PAK_ASSETS.IMG_PUBLIC002, 12, this.top);
        this.hasAward.setScale(0.8f, 0.8f);
        this.hasAward.addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.5f))));
        new ActorButton((int) PAK_ASSETS.IMG_ZHU039, "13", 0, (int) PAK_ASSETS.IMG_PUBLIC012, 12, this.top);
        new ActorButton(501, "8", (int) PAK_ASSETS.IMG_PAO004, (int) PAK_ASSETS.IMG_UI002, 12, this.top);
        new ActorButton(100, "9", 180, (int) PAK_ASSETS.IMG_JINGANGDAODAN2, 12, this.top);
        if (RankData.showTowerStrengButton()) {
            new ActorButton((int) PAK_ASSETS.IMG_ZHU032, "6", (int) PAK_ASSETS.IMG_PAO004, (int) PAK_ASSETS.IMG_BUFF_GONGJI, 12, this.top);
            new Effect().addEffect(67, (int) PAK_ASSETS.IMG_E02, (int) PAK_ASSETS.IMG_GUANQIANDQ, this.top);
        }
        new Effect().addEffect(67, (int) PAK_ASSETS.IMG_E02, (int) PAK_ASSETS.IMG_SHIBAI, this.top);
        SimpleButton simpleButton = new SimpleButton(0, 1048, 1, new int[]{PAK_ASSETS.IMG_CHUANGGUAN001, PAK_ASSETS.IMG_CHUANGGUAN002, PAK_ASSETS.IMG_CHUANGGUAN001});
        simpleButton.setName("10");
        this.top.addActor(simpleButton);
        if (RankData.openHardMode()) {
            SimpleButton simpleButton2 = new SimpleButton(120, 1038, 1, new int[]{PAK_ASSETS.IMG_MAP11, PAK_ASSETS.IMG_MAP12});
            simpleButton2.setName("21");
            this.top.addActor(simpleButton2);
            SimpleButton simpleButton3 = new SimpleButton(PAK_ASSETS.IMG_UI_A29, 1038, 1, new int[]{PAK_ASSETS.IMG_MAP14, PAK_ASSETS.IMG_MAP13});
            simpleButton3.setName("20");
            this.top.addActor(simpleButton3);
            if (RankData.getMode() == 0) {
                simpleButton3.setTexture(1);
            } else {
                simpleButton2.setTexture(1);
            }
            simpleButton2.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    Sound.playButtonPressed();
                    RankData.setMode((byte) 0);
                    GameMain.toScreen(new MyGameMap());
                }
            });
            simpleButton3.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    Sound.playButtonPressed();
                    RankData.setMode((byte) 1);
                    GameMain.toScreen(new MyGameMap());
                }
            });
        }
        if (MySwitch.isSimId == 0 && MySwitch.isCaseA == 0 && MySwitch.caseJiOrMM == 1) {
            new ActorButton((int) PAK_ASSETS.IMG_I1, "16", 0, (int) PAK_ASSETS.IMG_ZHANDOU064, 12, this.top);
            new ActorButton((int) PAK_ASSETS.IMG_I2, "17", 0, (int) PAK_ASSETS.IMG_ZHU018, 12, this.top);
            new ActorButton((int) PAK_ASSETS.IMG_I3, "18", (int) PAK_ASSETS.IMG_GONGGAO004, (int) PAK_ASSETS.IMG_ZHANDOU064, 12, this.top);
        }
        if (MySwitch.isCaseA != 0 && MySwitch.isSimId == 0 && MySwitch.caseJiOrMM == 1) {
            switch (Integer.parseInt(MySwitch.giftType)) {
                case 0:
                    new ActorButton((int) PAK_ASSETS.IMG_I1, "16", 0, (int) PAK_ASSETS.IMG_ZHANDOU064, 12, this.top);
                    break;
                case 1:
                    new ActorButton((int) PAK_ASSETS.IMG_I2, "17", 0, (int) PAK_ASSETS.IMG_ZHU018, 12, this.top);
                    break;
                case 2:
                    new ActorButton((int) PAK_ASSETS.IMG_I3, "18", (int) PAK_ASSETS.IMG_GONGGAO004, (int) PAK_ASSETS.IMG_ZHANDOU064, 12, this.top);
                    break;
            }
        }
        initdatamap();
    }

    /* access modifiers changed from: package-private */
    public void initdatamap() {
        if (RankData.getEndlessPowerDays() > 0) {
            int tiliNum = RankData.getEndlessPowerDays();
            new ActorImage((int) PAK_ASSETS.IMG_CHUANGGUAN007, 125, (int) PAK_ASSETS.IMG_JUCAIMANAOFU, 4, this.top);
            return;
        }
        this.tiliNum1 = new GNumSprite((int) PAK_ASSETS.IMG_SHOP047, "" + RankData.getPowerNum(), "X", -2, 4);
        this.tiliNum1.setPosition((float) 126, (float) PAK_ASSETS.IMG_SHENSHENYILONGGONG);
        this.top.addActor(this.tiliNum1);
    }

    /* access modifiers changed from: package-private */
    public void initData() {
        int bearCionNum = RankData.getCakeNum();
        System.out.println("bearCionNum:" + bearCionNum);
        this.bearCion = new GNumSprite((int) PAK_ASSETS.IMG_ZHU036, "" + bearCionNum, "/", -2, 4);
        this.bearCion.setPosition(110.0f, 35.0f);
        this.top.addActor(this.bearCion);
        this.bearCion.setColor(MyUIGetFont.maintopdataColor);
        new ActorButton(100, "11", 167, 15, 12, this.top);
        this.diamond = new GNumSprite((int) PAK_ASSETS.IMG_ZHU036, "" + RankData.getDiamondNum(), "/", -2, 4);
        this.diamond.setPosition(325.0f, 35.0f);
        this.top.addActor(this.diamond);
        this.diamond.setColor(MyUIGetFont.maintopdataColor);
        new ActorButton(100, "12", (int) PAK_ASSETS.IMG_UI_SHOP_SCANNING, 15, 12, this.top);
        this.star = new GNumSprite((int) PAK_ASSETS.IMG_ZHU036, RankData.getStarNum() + "/" + RankData.getTotalStarNum(), "/", -2, 4);
        this.star.setPosition(552.0f, 35.0f);
        this.top.addActor(this.star);
        this.star.setColor(MyUIGetFont.maintopdataColor);
    }

    /* access modifiers changed from: package-private */
    public void initgruoplistener() {
        this.top.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                String buttonName = event.getTarget().getName();
                System.out.println("buttonName:" + buttonName);
                if (buttonName != null) {
                    codeName = Integer.parseInt(buttonName);
                    System.out.println("codeName:" + codeName);
                } else {
                    codeName = 9;
                }
                switch (codeName) {
                    case 0:
                        Sound.playButtonPressed();
                        new MyShop(0);
                        return true;
                    case 1:
                        Sound.playButtonPressed();
                        new EveryDayMission();
                        return true;
                    case 2:
                        Sound.playButtonPressed();
                        new MyAchievement();
                        return true;
                    case 3:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        new MyMore();
                        return true;
                    case 4:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        new MyLuckyRoller();
                        return true;
                    case 5:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        new MyJiDi();
                        return true;
                    case 6:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        new MyTowerUP();
                        return true;
                    case 7:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        new MyOnlineAward();
                        return true;
                    case 8:
                        Sound.playButtonPressed();
                        System.out.println("up");
                        new MyUpgradeInMap(Mymainmenu2.userChoseIndex, 0);
                        return true;
                    case 9:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        new MySupply(2);
                        return true;
                    case 10:
                        Sound.playButtonPressed();
                        System.out.println("more");
                        Mymainmenu2.selectIndex = Mymainmenu2.userChoseIndex;
                        MyGameMap.isInMap = false;
                        GameStage.clearAllLayers();
                        GameMain.toScreen(new Mymainmenu2());
                        return true;
                    case 11:
                        Sound.playButtonPressed();
                        new MyShop(2);
                        return true;
                    case 12:
                        Sound.playButtonPressed();
                        new MyShop(3);
                        return true;
                    case 13:
                        Sound.playButtonPressed();
                        MyMainTop.this.getActiveSecret();
                        return true;
                    case 14:
                        Sound.playButtonPressed();
                        Message.moreGame();
                        return true;
                    case 15:
                        Sound.playButtonPressed();
                        new MyNotice();
                        return true;
                    case 16:
                        Sound.playButtonPressed();
                        new MyGift(12);
                        return true;
                    case 17:
                        Sound.playButtonPressed();
                        new MyGift(13);
                        return true;
                    case 18:
                        Sound.playButtonPressed();
                        new MyGift(14);
                        return true;
                    default:
                        return true;
                }
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    /* access modifiers changed from: private */
    public void getActiveSecret() {
        final Group jihuo = new Group();
        this.mask = new Mask();
        jihuo.addActor(this.mask);
        GameStage.addActor(jihuo, 4);
        Gdx.input.getTextInput(new Input.TextInputListener() {
            public void input(String text) {
                jihuo.remove();
                jihuo.clear();
                int group = GSecretUtil.checkNum(text);
                if (group < 0) {
                    System.out.println("激活码不正确");
                    int unused = MyMainTop.this.index = -2;
                    jihuo.remove();
                    jihuo.clear();
                } else if (GSecretUtil.isGiftGet(group)) {
                    System.out.println("激活码已兑换");
                    int unused2 = MyMainTop.this.index = -3;
                    jihuo.remove();
                    jihuo.clear();
                } else {
                    MyMainTop.this.get = LoadGet.getData.get("Jihuoma");
                    int unused3 = MyMainTop.this.index = group;
                    jihuo.remove();
                    jihuo.clear();
                    GSecretUtil.setGiftGet(group);
                    RankData.saveRecord();
                }
            }

            public void canceled() {
                jihuo.remove();
                jihuo.clear();
            }
        }, "请输入激活码", null, null);
    }

    /* access modifiers changed from: private */
    public void getGift() {
        switch (this.index) {
            case -3:
                MyTip.tishi("激活码已兑换");
                break;
            case -2:
                MyTip.tishi("激活码不正确");
                break;
            case 0:
                getJihuoGifts0();
                break;
            case 1:
                getJihuoGifts1();
                break;
            case 2:
                getJihuoGifts2();
                break;
            case 3:
                getJihuoGifts3();
                break;
            case 4:
                getJihuoGifts4();
                break;
            case 5:
                getJihuoGifts5();
                break;
            case 6:
                getJihuoGifts6();
                break;
            case 7:
                getJihuoGifts7();
                break;
            case 8:
                getJihuoGifts8();
                break;
            case 9:
                getJihuoGifts9();
                break;
            case 10:
                getJihuoGifts10();
                break;
            case 11:
                getJihuoGifts11();
                break;
            case 12:
                getJihuoGifts12();
                break;
            case 13:
                getJihuoGifts13();
                break;
            case 14:
                getJihuoGifts14();
                break;
            case 15:
                getJihuoGifts15();
                break;
            case 16:
                getJihuoGifts16();
                break;
            case 17:
                getJihuoGifts17();
                break;
            case 18:
                getJihuoGifts18();
                break;
            case 19:
                getJihuoGifts19();
                break;
            case 20:
                getJihuoGifts20();
                break;
            case 21:
                getJihuoGifts21();
                break;
            case 22:
                getJihuoGifts22();
                break;
            case 23:
                getJihuoGifts23();
                break;
            case 24:
                getJihuoGifts24();
                break;
        }
        this.index = -1;
    }

    private void getJihuoGifts0() {
        new MyGet(this.get, 0, 3);
        RankData.addDiamondNum(PAK_ASSETS.IMG_UP011);
    }

    private void getJihuoGifts1() {
        new MyGet(this.get, 1, 3);
        RankData.addDiamondNum(1800);
    }

    private void getJihuoGifts2() {
        new MyGet(this.get, 2, 3);
        RankData.addDiamondNum(4500);
    }

    private void getJihuoGifts3() {
        new MyGet(this.get, 3, 3);
        RankData.addCakeNum(8000);
    }

    private void getJihuoGifts4() {
        new MyGet(this.get, 4, 3);
        RankData.addCakeNum(20000);
    }

    private void getJihuoGifts5() {
        new MyGet(this.get, 5, 3);
        RankData.addCakeNum(40000);
    }

    private void getJihuoGifts6() {
        new MyGet(this.get, 6, 3);
        RankData.addHoneyNum(1);
        RankData.addHeartNum(1);
        RankData.addCakeNum(200);
    }

    private void getJihuoGifts7() {
        new MyGet(this.get, 7, 3);
        RankData.addHoneyNum(6);
        RankData.addHeartNum(6);
        RankData.addCakeNum(3000);
    }

    private void getJihuoGifts8() {
        new MyGet(this.get, 8, 3);
        RankData.addHoneyNum(3);
        RankData.addHeartNum(2);
        RankData.addCakeNum(1000);
    }

    private void getJihuoGifts9() {
        new MyGet(this.get, 9, 3);
        RankData.addHoneyNum(6);
        RankData.addHeartNum(4);
        RankData.addCakeNum(5000);
    }

    private void getJihuoGifts10() {
        new MyGet(this.get, 10, 3);
        RankData.addHoneyNum(18);
        RankData.addHeartNum(11);
        RankData.addCakeNum(Constant.DEFAULT_LIMIT);
    }

    private void getJihuoGifts11() {
        new MyGet(LoadGet.getData.get("Shop"), 2, 1);
        RankData.addProps(0, 5);
        RankData.addProps(1, 5);
        RankData.addProps(2, 5);
        RankData.addProps(3, 5);
        RankData.addCakeNum(2000);
    }

    private void getJihuoGifts12() {
        new MyGet(this.get, 12, 3);
        RankData.addProps(0, 5);
    }

    private void getJihuoGifts13() {
        new MyGet(this.get, 13, 3);
        RankData.addProps(1, 5);
    }

    private void getJihuoGifts14() {
        new MyGet(this.get, 14, 3);
        RankData.addProps(2, 5);
    }

    private void getJihuoGifts15() {
        new MyGet(this.get, 15, 3);
        RankData.addProps(3, 5);
    }

    private void getJihuoGifts16() {
        new MyGet(this.get, 16, 3);
        RankData.buyPower(5);
    }

    private void getJihuoGifts17() {
        RankData.bearOpen(3);
        MySwitch.isKJLGM = true;
        new MyGet(LoadGet.getData.get("Shop"), 21, 1);
    }

    private void getJihuoGifts18() {
        new MyGet(this.get, 18, 3);
        RankData.buyEndlessPower(5);
    }

    private void getJihuoGifts19() {
        new MyGet(this.get, 19, 3);
        RankData.buyEndlessPower(5);
    }

    private void getJihuoGifts20() {
        new MyGet(this.get, 20, 3);
        RankData.buyEndlessReplay(5);
    }

    private void getJihuoGifts21() {
        new MyGet(this.get, 21, 3);
        RankData.buyEndlessReplay(5);
    }

    private void getJihuoGifts22() {
        new MyGet(this.get, 22, 3);
        RankData.addHeartNum(5);
    }

    private void getJihuoGifts23() {
        new MyGet(this.get, 23, 3);
        RankData.addHoneyNum(5);
    }

    private void getJihuoGifts24() {
        new MyGet(this.get, 24, 3);
        RankData.addProps(0, 5);
        RankData.addProps(1, 5);
        RankData.addProps(2, 5);
        RankData.addProps(3, 5);
    }

    public void exit() {
        this.top.remove();
        this.top.clear();
    }
}
