package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GClipGroup;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.record.Strength;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;
import java.util.ArrayList;

public class MyForceHitOut extends MyGroup {
    Group hitoutgroup;
    Group uptowergroup;

    public void init() {
        Rank.setPause(false);
        if (this.hitoutgroup != null) {
            this.hitoutgroup.remove();
            this.hitoutgroup.clear();
        }
        if (this.uptowergroup != null) {
            this.uptowergroup.remove();
            this.uptowergroup.clear();
        }
        if (this != null) {
            remove();
            clear();
        }
        this.hitoutgroup = new Group();
        this.hitoutgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.hitoutgroup, 4);
        this.hitoutgroup.addActor(new Mask());
    }

    public MyForceHitOut() {
        if (RankData.getNeedStrengthTowers().size() == 0) {
            if (this.hitoutgroup != null) {
                this.hitoutgroup.remove();
                this.hitoutgroup.clear();
            }
            if (this.uptowergroup != null) {
                this.uptowergroup.remove();
                this.uptowergroup.clear();
            }
            new MyEquipment();
            return;
        }
        initbj();
        initframe();
        initbutton();
        initlistener();
        addmove();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 4, this.hitoutgroup, false, false);
    }

    /* access modifiers changed from: package-private */
    public void initframe() {
        if (this.uptowergroup != null) {
            this.uptowergroup.remove();
            this.uptowergroup.clear();
        }
        this.uptowergroup = new Group();
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC011, 320, (int) PAK_ASSETS.IMG_DAJI10, 1, this.hitoutgroup);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, (int) PAK_ASSETS.IMG_UI_ZHUANPAN006, 1, this.hitoutgroup);
        new ActorText("推荐等级:" + RankData.getRecommendLevel(), 320, (int) PAK_ASSETS.IMG_DAJI10, 1, this.hitoutgroup);
        ArrayList<Strength> tower = RankData.getNeedStrengthTowers();
        for (int i = 0; i < tower.size(); i++) {
            Strength s = tower.get(i);
            String towerName = s.getIcon();
            int Upmoney = s.getMoney();
            ActorImage title_3 = new ActorImage(75, (i * 143) + 35, (int) PAK_ASSETS.IMG_SHENGJITIPS04, 12, this.uptowergroup);
            new ActorText("Lv:" + s.getLevel(), (i * 143) + 107, (int) PAK_ASSETS.IMG_TX_ZUOSHAO, 1, this.uptowergroup);
            SimpleButton simpleButton = new SimpleButton((i * 143) + 35, PAK_ASSETS.IMG_PAO002, 1, new int[]{76, 77, 76});
            simpleButton.setName((i + 2) + "");
            title_3.setTouchable(Touchable.enabled);
            this.uptowergroup.addActor(simpleButton);
            final Strength strength = s;
            final int i2 = Upmoney;
            simpleButton.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    System.out.println("qinghua:");
                    MyForceHitOut.this.up(strength, i2);
                }
            });
            new ActorImage((int) PAK_ASSETS.IMG_UP011, (i * 143) + 18, (int) PAK_ASSETS.IMG_QIANDAO011, 12, this.uptowergroup).setScale(0.8f, 0.8f);
            new ActorImage(towerName, (i * 143) + 60, (int) PAK_ASSETS.IMG_UI_A014, 12, this.uptowergroup);
            new ActorText("" + Upmoney, (i * 143) + 122, (int) PAK_ASSETS.IMG_MAP13, 1, this.uptowergroup);
        }
    }

    /* access modifiers changed from: package-private */
    public void up(Strength s, int money) {
        if (RankData.spendCake(money)) {
            Sound.playSound(80);
            this.uptowergroup.addActor(new Effect().getEffect_Diejia(83, PAK_ASSETS.IMG_MAP1, PAK_ASSETS.IMG_UP010));
            s.strength();
            free();
            new MyForceHitOut();
            new MyGetTowerUpInMapEffect(s);
        } else if (MySwitch.isCaseA == 0) {
            MyTip.Notenought(true);
        } else {
            new MyGift(10);
        }
    }

    /* access modifiers changed from: package-private */
    public void addmove() {
        GClipGroup hitClipgroup = new GClipGroup();
        hitClipgroup.setClipArea(32, PAK_ASSETS.IMG_PPSG04, PAK_ASSETS.IMG_ZD_LIUXINGCHUI2, PAK_ASSETS.IMG_UI_A29B);
        hitClipgroup.addActor(this.uptowergroup);
        GameStage.addActor(hitClipgroup, 4);
        this.uptowergroup.addListener(GameUITools.getMoveListener(this.uptowergroup, (float) (RankData.getNeedStrengthTowers().size() * 143), 575.0f, 5.0f, true));
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.hitoutgroup);
        SimpleButton go = new SimpleButton(PAK_ASSETS.IMG_SHENLONG2, PAK_ASSETS.IMG_FUHUO004, 1, new int[]{78, 79, 78});
        go.setName("1");
        go.setScale(0.8f, 0.8f);
        this.hitoutgroup.addActor(go);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.hitoutgroup.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                int codeName;
                if (!GameUITools.isDragged && event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MyForceHitOut.this.free();
                            new MyEquipment();
                            break;
                        case 1:
                            Sound.playSound(10);
                            MyForceHitOut.this.free();
                            Rank.setPause(true);
                            GameStage.clearAllLayers();
                            MyGameMap.isInMap = false;
                            RankData.selectRoleIndex = Mymainmenu2.userChoseIndex;
                            GameMain.toScreen(new Rank());
                            break;
                    }
                    super.clicked(event, x, y);
                }
            }
        });
    }

    public void exit() {
        this.uptowergroup.remove();
        this.uptowergroup.clear();
        this.hitoutgroup.remove();
        this.hitoutgroup.clear();
    }
}
