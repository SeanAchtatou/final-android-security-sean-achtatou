package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIData;
import com.sg.td.record.LoadGet;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyRecharge extends MyGroup {
    static float curretatk = Animation.CurveTimeline.LINEAR;
    static float[] money = {30.0f, 50.0f, 80.0f, 120.0f};
    SimpleButton duihuan;
    ActorShapeSprite gShapeSprite;
    Group rechargegroup;
    private float timeparLength = 120.0f;
    private float timeparLength1;

    public void init() {
        this.rechargegroup = new Group();
        this.rechargegroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.rechargegroup, 4);
        this.rechargegroup.addActor(new Mask());
        initbj();
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 9, this.rechargegroup, false, true);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_SHUOMINGZI01, 1, this.rechargegroup);
        new ActorImage((int) PAK_ASSETS.IMG_GONGGAO007, 320, (int) PAK_ASSETS.IMG_HUOCHAI_SHUOMING, 1, this.rechargegroup);
        new ActorImage((int) PAK_ASSETS.IMG_GONGGAO011, 320, (int) PAK_ASSETS.IMG_2X1, 1, this.rechargegroup);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, Constant.S_F, (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.rechargegroup);
        int[] t = {PAK_ASSETS.IMG_GONGGAO008, PAK_ASSETS.IMG_GONGGAO009, PAK_ASSETS.IMG_GONGGAO008};
        for (int i = 0; i < 4; i++) {
            this.duihuan = new SimpleButton(PAK_ASSETS.IMG_JIESUAN011, (i * 125) + PAK_ASSETS.IMG_WSPARTICLE_RING09, 1, t);
            this.duihuan.setName(i + "");
            if (MyUIData.isDuihuan[i]) {
                this.duihuan.setTexture(1);
            }
            this.rechargegroup.addActor(this.duihuan);
            ActorClipImage bar1 = new ActorClipImage(PAK_ASSETS.IMG_GONGGAO010, 60, (i * 126) + PAK_ASSETS.IMG_UI_LIGHT_YELLOW01, 12, this.rechargegroup);
            bar1.setVisible(false);
            if (i == 3) {
                bar1.setPosition((float) 60, (float) (((i * 126) + PAK_ASSETS.IMG_UI_LIGHT_YELLOW01) - 2));
            }
            System.out.println("MyUIData.getRechargeAmount()::" + MyUIData.getRechargeAmount());
            if (MyUIData.getRechargeAmount() == 0) {
                curretatk = Animation.CurveTimeline.LINEAR;
            } else {
                curretatk = ((float) MyUIData.getRechargeAmount()) / money[i];
            }
            this.timeparLength1 = bar1.getWidth();
            System.out.println("timeparLength1::" + this.timeparLength1);
            timeparAction(bar1, curretatk, this.timeparLength1);
        }
    }

    private void timeparAction(ActorClipImage bar1, float curretatk2, float timeparLength12) {
        this.timeparLength = curretatk2 * timeparLength12;
        System.out.println("dd:" + this.timeparLength);
        bar1.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.timeparLength, bar1.getHeight());
        bar1.setVisible(true);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.rechargegroup.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            MyRecharge.this.judge(0);
                            break;
                        case 1:
                            MyRecharge.this.judge(1);
                            break;
                        case 2:
                            MyRecharge.this.judge(2);
                            break;
                        case 3:
                            MyRecharge.this.judge(3);
                            break;
                        case 5:
                            MyRecharge.this.free();
                            break;
                    }
                    super.clicked(event, x, y);
                }
            }
        });
    }

    public void judge(int id) {
        if (MyUIData.isDuihuan[id]) {
            MyTip.tishi("已兑换");
            return;
        }
        switch (id) {
            case 0:
                if (MyUIData.getRechargeAmount() >= 30) {
                    MyUIData.isDuihuan[id] = true;
                    RankData.saveRecord();
                    MyTip.tishi("兑换成功");
                    RankData.addCakeNum(5000);
                    return;
                }
                MyTip.tishi("充值金额不满足");
                return;
            case 1:
                if (MyUIData.getRechargeAmount() >= 50) {
                    MyUIData.isDuihuan[id] = true;
                    RankData.saveRecord();
                    MyTip.tishi("兑换成功");
                    RankData.addDiamondNum(1000);
                    RankData.addCakeNum(5000);
                    return;
                }
                MyTip.tishi("充值金额不满足");
                return;
            case 2:
                if (MyUIData.getRechargeAmount() >= 80) {
                    MyUIData.isDuihuan[id] = true;
                    RankData.saveRecord();
                    MyTip.tishi("兑换成功");
                    RankData.addHoneyNum(2);
                    RankData.addDiamondNum(1000);
                    RankData.addCakeNum(8000);
                    RankData.addProps(0, 10);
                    RankData.addHoneyNum(10);
                    RankData.addHeartNum(10);
                    return;
                }
                MyTip.tishi("充值金额不满足");
                return;
            case 3:
                if (MyUIData.getRechargeAmount() >= 120) {
                    MyUIData.isDuihuan[id] = true;
                    RankData.saveRecord();
                    MyTip.tishi("兑换成功");
                    if (RankData.roleLevel[3] > 1) {
                        RankData.addDiamondNum(4000);
                        return;
                    }
                    RankData.bearOpen(3);
                    MySwitch.isKJLGM = true;
                    new MyGet(LoadGet.getData.get("Shop"), 21, 1);
                    return;
                }
                MyTip.tishi("充值金额不满足");
                return;
            default:
                return;
        }
    }

    public void exit() {
        this.rechargegroup.remove();
        this.rechargegroup.clear();
    }
}
