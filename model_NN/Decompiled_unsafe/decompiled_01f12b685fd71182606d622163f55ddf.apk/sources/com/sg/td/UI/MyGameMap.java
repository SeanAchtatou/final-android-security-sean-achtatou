package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.GClipGroup;
import com.kbz.Sound.GameSound;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.data.MyBigMapPlace;
import com.sg.td.message.GameUITools;
import com.sg.td.spine.CloudSpine;
import com.sg.td.spine.MapLayer1;
import com.sg.td.spine.MapLayer2;
import com.sg.td.spine.RoleSpineMap;
import com.sg.td.spine.UnlockSpine;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyImage;
import com.sg.util.GameScreen;
import com.sg.util.GameStage;

public class MyGameMap extends GameScreen implements GameConstant {
    static boolean isInMap;
    static Group map1group;
    public static Group mapgroup;
    private static float[] rankPlace = {-5312.0f, -5312.0f, -5312.0f, -5312.0f, -5312.0f, -5312.0f, -5312.0f, -5152.0f, -4992.0f, -4855.0f, -4691.0f, -4660.0f, -4526.0f, -4556.0f, -4461.0f, -4416.0f, -4256.0f, -4256.0f, -4073.0f, -4014.0f, -3851.0f, -3851.0f, -3724.0f, -3654.0f, -3355.0f, -3272.0f, -3190.0f, -3054.0f, -3054.0f, -2933.0f, -2933.0f, -2847.0f, -2808.0f, -2742.0f, -2647.0f, -2468.0f, -2401.0f, -2246.0f, -2246.0f, -2124.0f, -2028.0f, -1929.0f, -1766.0f, -1647.0f, -1584.0f, -1438.0f, -1438.0f, -1303.0f, -1303.0f, -1172.0f, -1172.0f, -1024.0f, -1024.0f, -1024.0f, -1024.0f};
    static int roleMapId;
    static int[] roleNumid = {21, 20, 22, 19};
    static int[] roleNumid1 = {13, 12, 15, 14};
    ActorImage clockImage;
    CloudSpine cloud13;
    CloudSpine cloud19;
    CloudSpine cloud25;
    CloudSpine cloud31;
    CloudSpine cloud37;
    CloudSpine cloud43;
    CloudSpine cloud49;
    CloudSpine cloud7;
    GClipGroup cloudClipgroup;
    Effect dipan;
    boolean isRoleMoved;
    boolean isclicklock;
    GClipGroup mapClipgroup;
    RoleSpineMap mapLayerRole;
    UnlockSpine mapLayerUnlock;
    Effect point;
    GNumSprite rankNumImage;
    int runkNum = 0;
    MyMainTop top;

    public void init() {
        GameSound.playMusic(1);
        initbj();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        if (this.mapClipgroup != null) {
            this.mapClipgroup.remove();
            this.mapClipgroup.clear();
        }
        if (mapgroup != null) {
            mapgroup.remove();
            mapgroup.clear();
        }
        mapgroup = new Group();
        map1group = new Group();
        MyBigMapPlace.laodData();
        roleMapId = Mymainmenu2.userChoseIndex;
        Group mapOne = new Group();
        new ActorImage((int) PAK_ASSETS.IMG_MAP, 0, 0, 12, mapOne);
        new ActorImage((int) PAK_ASSETS.IMG_MAP, 0, 1614, 12, mapOne);
        new ActorImage((int) PAK_ASSETS.IMG_MAP, 0, 3228, 12, mapOne);
        new ActorImage((int) PAK_ASSETS.IMG_MAP, 0, 4842, 12, mapOne);
        mapgroup.addActor(mapOne);
        Group mapTwo = new Group();
        mapTwo.addActor(new MapLayer1(320, GameConstant.SCREEN_HEIGHT, 7));
        mapTwo.moveBy(Animation.CurveTimeline.LINEAR, (float) 5317);
        mapgroup.addActor(mapTwo);
        for (int i = 0; i < MyBigMapPlace.bigMapPlaceData.size; i++) {
            final int xx = MyBigMapPlace.bigMapPlaceData.get(Integer.valueOf(i)).getX() + 8;
            final int yy = MyBigMapPlace.bigMapPlaceData.get(Integer.valueOf(i)).getY() + 178;
            final MyImage runk = new MyImage(MyBigMapPlace.bigMapPlaceData.get(Integer.valueOf(i)).getName(), (float) xx, (float) yy, 12);
            runk.setTouchable(Touchable.enabled);
            runk.setName((MyBigMapPlace.bigMapPlaceData.size - i) + "");
            this.runkNum = Integer.parseInt(runk.getName());
            runk.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    MyGameMap.this.runkNum = Integer.parseInt(event.getTarget().getName());
                    Sound.playButtonPressed();
                    if (MyGameMap.this.runkNum <= RankData.getRoleRankIndex()) {
                        Rank.rank = MyGameMap.this.runkNum;
                        MyGameMap.this.judgeIsEnoughPower();
                    } else if (RankData.getMode() == 1) {
                        MyGameMap.this.getOnOPenTip(xx, yy);
                    } else if (RankData.reachOpenStar(MyGameMap.this.runkNum)) {
                        if (MyGameMap.this.runkNum % 6 != 1 || MyGameMap.this.runkNum == 1) {
                            MyGameMap.this.getOnOPenTip(xx, yy);
                        } else if (!RankData.nextRankOpen(MyGameMap.this.runkNum)) {
                            new MyStarUnlock(MyGameMap.this.runkNum, false);
                        } else {
                            MyGameMap.this.isRoleMoved = true;
                            if (MyGameMap.this.clockImage != null) {
                                MyGameMap.this.clockImage.setVisible(false);
                            }
                            if (!MyGameMap.this.isclicklock) {
                                MyGameMap.this.roleMoveActions(runk);
                            }
                            RankData.unlockRank();
                            MyGameMap.this.getCloudUnlock(MyGameMap.this.runkNum);
                        }
                    } else if (MyGameMap.this.runkNum % 6 != 1 || MyGameMap.this.runkNum == 1) {
                        MyGameMap.this.getOnOPenTip(xx, yy);
                    } else if (RankData.getMode() == 1) {
                        MyGameMap.this.getOnOPenTip(xx, yy);
                    } else {
                        new MyStarUnlock(MyGameMap.this.runkNum, true);
                    }
                    return super.touchDown(event, x, y, pointer, button);
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                }
            });
            mapgroup.addActor(runk);
            addStar(runk, mapgroup, this.runkNum, xx, yy);
            this.rankNumImage = new GNumSprite((int) PAK_ASSETS.IMG_MAP10, this.runkNum + "", "", -2, 4);
            this.rankNumImage.setPosition((float) (xx + 50), (float) (yy + 70));
            this.rankNumImage.setScale(0.8f, 0.8f);
            mapgroup.addActor(this.rankNumImage);
            if (RankData.drawLock(this.runkNum)) {
                this.clockImage = new ActorImage(503, xx + 50, yy, 1, mapgroup);
                if (RankData.getRoleRankIndex() == this.runkNum) {
                    this.clockImage.setVisible(false);
                }
            }
        }
        getLockCloud();
        new Group();
        MapLayer2 mapLayer2 = new MapLayer2(320, GameConstant.SCREEN_HEIGHT, 8);
        MapLayer2 mapLayer22 = new MapLayer2(320, GameConstant.SCREEN_HEIGHT, 8);
        MapLayer2 mapLayer23 = new MapLayer2(320, GameConstant.SCREEN_HEIGHT, 8);
        MapLayer2 mapLayer24 = new MapLayer2(320, GameConstant.SCREEN_HEIGHT, 8);
        mapLayer2.setPosition(320.0f, Animation.CurveTimeline.LINEAR);
        mapLayer22.setPosition(320.0f, 2000.0f);
        mapLayer23.setPosition(320.0f, 4000.0f);
        mapLayer24.setPosition(320.0f, 6000.0f);
        map1group.addActor(mapLayer2);
        map1group.addActor(mapLayer22);
        map1group.addActor(mapLayer23);
        map1group.addActor(mapLayer24);
        this.top = new MyMainTop(true);
        move();
    }

    /* access modifiers changed from: package-private */
    public void judgeIsEnoughPower() {
        if (!RankData.powerEnough()) {
            new MySupply(2);
        } else if (!this.isRoleMoved) {
            new MyEquipment();
        } else if (this.isclicklock) {
            new MyEquipment();
        }
    }

    /* access modifiers changed from: package-private */
    public void roleMoveActions(final MyImage runk) {
        Sound.playSound(25);
        this.mapLayerUnlock = new UnlockSpine(((int) runk.getX()) + 50, (int) runk.getY(), 16);
        this.mapLayerUnlock.addAction(Actions.sequence(Actions.delay(1.3f), Actions.run(new Runnable() {
            public void run() {
                MyGameMap.this.mapLayerUnlock.remove();
                MyGameMap.this.mapLayerUnlock.clear();
                Action action = Actions.moveTo(runk.getX() + 50.0f, runk.getY() + 20.0f, 1.0f);
                Action action1 = Actions.sequence(Actions.moveTo(runk.getX() + 50.0f, runk.getY() + 20.0f, 1.0f), Actions.run(new Runnable() {
                    public void run() {
                        MyGameMap.this.isclicklock = true;
                    }
                }));
                MyGameMap.this.mapLayerRole.addAction(action);
                MyGameMap.this.mapLayerRole.addAction(action1);
            }
        })));
        mapgroup.addActor(this.mapLayerUnlock);
    }

    /* access modifiers changed from: package-private */
    public void getOnOPenTip(int xx, int yy) {
        Group wenzi = new Group();
        ActorImage onOPen = new ActorImage(502, xx + 50, yy, 1, wenzi);
        mapgroup.addActor(wenzi);
        wenzi.addAction(MyGet.getCountActor(2, wenzi, onOPen));
    }

    /* access modifiers changed from: package-private */
    public void getLockCloud() {
        this.cloud7 = new CloudSpine(0, 1619, 10);
        this.cloud7.setStatus(1);
        this.cloud7.setPosition(320.0f, (float) 5402);
        mapgroup.addActor(this.cloud7);
        if (RankData.getRoleRankIndex() >= 7 && this.cloud7 != null) {
            this.cloud7.remove();
            this.cloud7.clear();
        }
        this.cloud13 = new CloudSpine(0, 1619, 10);
        this.cloud13.setStatus(1);
        this.cloud13.setPosition(320.0f, (float) 4842);
        mapgroup.addActor(this.cloud13);
        if (RankData.getRoleRankIndex() >= 13 && this.cloud13 != null) {
            this.cloud13.remove();
            this.cloud13.clear();
        }
        this.cloud19 = new CloudSpine(0, 1619, 10);
        this.cloud19.setStatus(1);
        this.cloud19.setPosition(320.0f, (float) 4328);
        mapgroup.addActor(this.cloud19);
        if (RankData.getRoleRankIndex() >= 19 && this.cloud19 != null) {
            this.cloud19.remove();
            this.cloud19.clear();
        }
        this.cloud25 = new CloudSpine(0, 1619, 10);
        this.cloud25.setStatus(1);
        this.cloud25.setPosition(320.0f, (float) 3528);
        mapgroup.addActor(this.cloud25);
        if (RankData.getRoleRankIndex() >= 25 && this.cloud25 != null) {
            this.cloud25.remove();
            this.cloud25.clear();
        }
        this.cloud31 = new CloudSpine(0, 1619, 10);
        this.cloud31.setStatus(1);
        this.cloud31.setPosition(320.0f, (float) 3128);
        mapgroup.addActor(this.cloud31);
        if (RankData.getRoleRankIndex() >= 31 && this.cloud31 != null) {
            this.cloud31.remove();
            this.cloud31.clear();
        }
        this.cloud37 = new CloudSpine(0, 1619, 10);
        this.cloud37.setStatus(1);
        this.cloud37.setPosition(320.0f, (float) 2528);
        mapgroup.addActor(this.cloud37);
        if (RankData.getRoleRankIndex() >= 37 && this.cloud37 != null) {
            this.cloud37.remove();
            this.cloud37.clear();
        }
        this.cloud43 = new CloudSpine(0, 1619, 10);
        this.cloud43.setStatus(1);
        this.cloud43.setPosition(320.0f, (float) 1928);
        mapgroup.addActor(this.cloud43);
        if (RankData.getRoleRankIndex() >= 43 && this.cloud43 != null) {
            this.cloud43.remove();
            this.cloud43.clear();
        }
        this.cloud49 = new CloudSpine(0, 1619, 10);
        this.cloud49.setStatus(1);
        this.cloud49.setPosition(320.0f, (float) 1428);
        mapgroup.addActor(this.cloud49);
        if (RankData.getRoleRankIndex() >= 49 && this.cloud49 != null) {
            this.cloud49.remove();
            this.cloud49.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public void getCloudUnlock(int rankNum) {
        if (rankNum == 7) {
            this.cloud7.setStatus(2);
            this.cloud7.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud7 != null) {
                        MyGameMap.this.cloud7.remove();
                        MyGameMap.this.cloud7.clear();
                    }
                }
            })));
        } else if (rankNum == 13) {
            this.cloud13.setStatus(2);
            this.cloud13.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud13 != null) {
                        MyGameMap.this.cloud13.remove();
                        MyGameMap.this.cloud13.clear();
                    }
                }
            })));
        } else if (rankNum == 19) {
            this.cloud19.setStatus(2);
            this.cloud19.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud19 != null) {
                        MyGameMap.this.cloud19.remove();
                        MyGameMap.this.cloud19.clear();
                    }
                }
            })));
        } else if (rankNum == 25) {
            this.cloud25.setStatus(2);
            this.cloud25.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud25 != null) {
                        MyGameMap.this.cloud25.remove();
                        MyGameMap.this.cloud25.clear();
                    }
                }
            })));
        } else if (rankNum == 31) {
            this.cloud31.setStatus(2);
            this.cloud31.setStatus(2);
            this.cloud31.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud31 != null) {
                        MyGameMap.this.cloud31.remove();
                        MyGameMap.this.cloud31.clear();
                    }
                }
            })));
        } else if (rankNum == 37) {
            this.cloud37.setStatus(2);
            this.cloud37.setStatus(2);
            this.cloud37.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud37 != null) {
                        MyGameMap.this.cloud37.remove();
                        MyGameMap.this.cloud37.clear();
                    }
                }
            })));
        } else if (rankNum == 43) {
            this.cloud43.setStatus(2);
            this.cloud43.setStatus(2);
            this.cloud43.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud43 != null) {
                        MyGameMap.this.cloud43.remove();
                        MyGameMap.this.cloud43.clear();
                    }
                }
            })));
        } else if (rankNum == 49) {
            this.cloud49.setStatus(2);
            this.cloud49.setStatus(2);
            this.cloud49.addAction(Actions.sequence(Actions.delay(1.0f), Actions.run(new Runnable() {
                public void run() {
                    if (MyGameMap.this.cloud49 != null) {
                        MyGameMap.this.cloud49.remove();
                        MyGameMap.this.cloud49.clear();
                    }
                }
            })));
        }
    }

    /* access modifiers changed from: package-private */
    public void getRole(int x, int y, Group group) {
        System.out.println("mapMymainmenu2.userChoseIndex:" + Mymainmenu2.userChoseIndex);
        roleMapId = Mymainmenu2.userChoseIndex;
        System.out.println("roleMapId:" + roleMapId);
        switch (roleMapId) {
            case 0:
                this.mapLayerRole = new RoleSpineMap(x + 50, y, roleNumid[roleMapId], 1.0f);
            case 1:
                this.mapLayerRole = new RoleSpineMap(x + 50, y, roleNumid[roleMapId], 1.0f);
                this.mapLayerRole.setPosition((float) (x + 50), (float) ((y - 20) + 8));
                break;
            case 2:
                this.mapLayerRole = new RoleSpineMap(x + 50, y, roleNumid[roleMapId], 1.0f);
                this.mapLayerRole.setPosition((float) (x + 50), (float) ((y - 20) + 18));
                break;
            case 3:
                this.mapLayerRole = new RoleSpineMap(x + 50, y, roleNumid[roleMapId], 0.8f);
                this.mapLayerRole.setPosition((float) (x + 50), (float) ((y - 20) + 20));
                break;
        }
        group.addActor(this.mapLayerRole);
    }

    /* access modifiers changed from: package-private */
    public void getRoleEffect(int x, int y, Group group) {
        new ActorImage((int) PAK_ASSETS.IMG_MAO8, x - 15, y + 20, 12, group);
        new ActorImage((int) PAK_ASSETS.IMG_MAO8, x + 30, y + 20, 12, group);
        new ActorImage((int) PAK_ASSETS.IMG_MAO8, x + 75, y + 20, 12, group);
        new Effect().addEffect(78, x + 51, y + 30, mapgroup);
    }

    private void addStar(MyImage runkImage, Group group, int rankNum, int x, int y) {
        int rolerank = RankData.getRoleRankIndex();
        if (rankNum == RankData.getCursorRankIndex()) {
            getRoleEffect(x, y, group);
        }
        boolean isOpen = RankData.rankOpen(rankNum);
        int starNum = RankData.getRankStar(rankNum);
        boolean isdeck = RankData.cleanDeck(rankNum);
        boolean isfullBlook = RankData.fullBloodThrough(rankNum);
        if (RankData.cleanFood(rankNum)) {
            new ActorImage((int) PAK_ASSETS.IMG_MAP2, (x - 15) + 10, (y - 53) + 25, 12, group);
        }
        if (isOpen) {
            new ActorImage((int) PAK_ASSETS.IMG_MAP1, x + 15, y - 53, 12, group);
        }
        switch (starNum) {
            case 1:
                group.addActor(new ActorImage((int) PAK_ASSETS.IMG_MAP5, x + 30, y + 15, 12));
                break;
            case 2:
                ActorImage star1 = new ActorImage((int) PAK_ASSETS.IMG_MAP5, x + 5, y + 15, 12);
                ActorImage actorImage = new ActorImage((int) PAK_ASSETS.IMG_MAP5, x + 50, y + 15, 12);
                group.addActor(star1);
                group.addActor(actorImage);
                break;
            case 3:
                ActorImage star12 = new ActorImage((int) PAK_ASSETS.IMG_MAP5, x - 15, y + 15, 12);
                ActorImage actorImage2 = new ActorImage((int) PAK_ASSETS.IMG_MAP5, x + 30, y + 15, 12);
                ActorImage actorImage3 = new ActorImage((int) PAK_ASSETS.IMG_MAP5, x + 75, y + 15, 12);
                group.addActor(star12);
                group.addActor(actorImage2);
                group.addActor(actorImage3);
                break;
        }
        if (isdeck) {
            new ActorImage((int) PAK_ASSETS.IMG_MAP4, x + 25, y - 78, 12, group);
        }
        if (isfullBlook) {
            new ActorImage((int) PAK_ASSETS.IMG_MAP3, x + 16, y - 23, 12, group);
        }
        if (rolerank == rankNum) {
            getRole(x, y, group);
        }
    }

    /* access modifiers changed from: package-private */
    public void move() {
        this.mapClipgroup = new GClipGroup();
        this.mapClipgroup.setClipArea(0, 0, 640, 9000);
        mapgroup.setPosition(Animation.CurveTimeline.LINEAR, getroleY(RankData.getRoleRankIndex()));
        mapgroup.setWidth(640.0f);
        mapgroup.setHeight(9000.0f);
        this.mapClipgroup.addActor(mapgroup);
        GameStage.addActor(mapgroup, 1);
        GameStage.addActor(map1group, 2);
        GameStage.addActor(this.mapClipgroup, 1);
        mapgroup.addListener(GameUITools.getMapMoveListener(mapgroup, 6926.0f, 1614.0f, 5.0f, false));
    }

    public float getroleY(int rolerank) {
        if (rolerank < 6) {
            return -5312.0f;
        }
        float y1 = rankPlace[RankData.getRoleRankIndex()];
        if (getStopY() < y1) {
            return getStopY();
        }
        return y1;
    }

    public static float getStopY() {
        int rolerank = RankData.getRoleRankIndex();
        if (rolerank > 0 && rolerank < 7) {
            return -5067.0f;
        }
        if (6 < rolerank && rolerank < 13) {
            return -4516.0f;
        }
        if (12 < rolerank && rolerank < 19) {
            return -4054.0f;
        }
        if (18 < rolerank && rolerank < 25) {
            return -3281.0f;
        }
        if (24 < rolerank && rolerank < 31) {
            return -2818.0f;
        }
        if (30 < rolerank && rolerank < 37) {
            return -2235.0f;
        }
        if (36 < rolerank && rolerank < 43) {
            return -1639.0f;
        }
        if (42 >= rolerank || rolerank >= 49) {
            return Animation.CurveTimeline.LINEAR;
        }
        return -1164.0f;
    }

    public void run(float delta) {
        map1group.setPosition(Animation.CurveTimeline.LINEAR, mapgroup.getY() * 0.5f);
        RankData.teach.addGameBeginTeach();
        Rank.runTip(delta);
    }

    public void dispose() {
        this.top.free();
        mapgroup.remove();
        mapgroup.clear();
        map1group.remove();
        map1group.clear();
        this.mapClipgroup.remove();
        this.mapClipgroup.clear();
    }

    public boolean gTouchDown(int screenX, int screenY, int pointer) {
        RankData.teach.touchDown_UI(screenX, screenY);
        return true;
    }

    public boolean gTouchUp(int screenX, int screenY, int pointer, int button) {
        RankData.teach.touchUp_UI(screenX, screenY);
        return true;
    }
}
