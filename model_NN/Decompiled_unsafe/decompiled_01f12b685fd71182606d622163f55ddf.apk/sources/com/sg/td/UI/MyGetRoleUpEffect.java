package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GSimpleAction;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.spine.RWkaweili;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyGetRoleUpEffect extends MyGroup {
    static int[] roleNumid = {13, 12, 15, 14};
    Group anygroup;
    Effect back;
    Group getupgroup;

    public void init() {
        initbj();
        initRoleUp();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        Sound.playSound(71);
        this.getupgroup = new Group();
        this.getupgroup.addActor(new Mask());
        GameStage.addActor(this.getupgroup, 4);
    }

    /* access modifiers changed from: package-private */
    public void initRoleUp() {
        int id = Mymainmenu2.selectIndex;
        this.back = new Effect();
        this.back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_QIANDAO011, this.getupgroup);
        this.getupgroup.addActor(new RWkaweili(320, PAK_ASSETS.IMG_QIANDAO011, roleNumid[id], 0.3f));
        new ActorImage((int) PAK_ASSETS.IMG_TIP002, 320, (int) PAK_ASSETS.IMG_ONLINE006, 1, this.getupgroup);
        new ActorImage((int) PAK_ASSETS.IMG_TIP008, 320, (int) PAK_ASSETS.IMG_ZANTING008, 1, this.getupgroup);
        new ActorText(MyUpgrade.atkNum + "", 320, (int) PAK_ASSETS.IMG_K02A, 12, this.getupgroup);
        new ActorText(MyUpgrade.skillNum + "%", 320, (int) PAK_ASSETS.IMG_ZHANDOU011, 12, this.getupgroup);
        new ActorText(MyUpgrade.atkNumNext + "", (int) PAK_ASSETS.IMG_JIESUAN004, (int) PAK_ASSETS.IMG_K02A, 12, this.getupgroup);
        new ActorText(MyUpgrade.skillNumNext + "%", (int) PAK_ASSETS.IMG_JIESUAN004, (int) PAK_ASSETS.IMG_ZHANDOU011, 12, this.getupgroup);
        this.getupgroup.addAction(getCountAction(3, this.getupgroup, this.back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    public void touchAnywhere() {
        this.anygroup = new Group();
        this.anygroup.addActor(this.getupgroup);
        this.anygroup.setPosition(5.0f, 5.0f);
        this.anygroup.setSize(640.0f, 1136.0f);
        GameStage.addActor(this.anygroup, 4);
        this.anygroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                GameStage.removeActor(MyGetRoleUpEffect.this.getupgroup);
                MyGetRoleUpEffect.this.getupgroup.remove();
                MyGetRoleUpEffect.this.getupgroup.clear();
                MyGetRoleUpEffect.this.back.remove_Diejia();
                MyGetRoleUpEffect.this.anygroup.remove();
                MyGetRoleUpEffect.this.anygroup.clear();
                System.out.println("clear");
            }
        });
    }

    public static Action getCountAction(final int time, final Group group, final Effect back2) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    GameStage.removeActor(group);
                    group.remove();
                    group.clear();
                    back2.remove_Diejia();
                    group.clear();
                }
                return true;
            }
        });
    }

    public void exit() {
    }
}
