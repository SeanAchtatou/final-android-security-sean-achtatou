package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.Animation.GAnimationManager;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIGetFont;
import com.sg.td.record.LoadStrength;
import com.sg.td.record.Strength;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.tools.MySprite;
import com.sg.util.GameStage;

public class MyTowerUP extends MyGroup {
    static int Money;
    static int NextPower;
    static int NowPower;
    static int TowerID;
    static String icon;
    static boolean isluck;
    static int level;
    static Strength selectStrength;
    ActorImage attack;
    MyImage bar;
    Actor effect1;
    Actor effect6;
    int roleId = 0;
    Group tower;
    Group towergroup;
    Group truuetupGroup;

    public void init() {
        this.truuetupGroup = new Group();
        this.truuetupGroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.truuetupGroup, 4);
        this.truuetupGroup.addActor(new Mask());
        loadAnimation();
        initbj();
        initframe(0);
        tower(LoadStrength.strengthData.get("HuoChai"), 0);
        initData();
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void loadAnimation() {
        for (String load : ANIMATION_NAME) {
            GAnimationManager.load(load);
        }
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_JUEHUIXUANFU, 10, this.truuetupGroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 188, 1, this.truuetupGroup);
        new ActorImage((int) PAK_ASSETS.IMG_PAO001, 320, 128, 1, this.truuetupGroup);
        new ActorImage((int) PAK_ASSETS.IMG_PAO002, 320, 158, 1, this.truuetupGroup);
        new ActorImage((int) PAK_ASSETS.IMG_PAO004, 320, (int) PAK_ASSETS.IMG_JIDI004, 1, this.truuetupGroup);
        new ActorImage((int) PAK_ASSETS.IMG_PAO005, 320, (int) PAK_ASSETS.IMG_UP009, 1, this.truuetupGroup);
    }

    /* access modifiers changed from: package-private */
    public void initframe(int id) {
        ActorImage title_3;
        ActorImage title_32 = null;
        int i = -1;
        int j = -1;
        int k = -1;
        if (this.towergroup != null) {
            this.towergroup.remove();
            this.towergroup.clear();
        }
        this.towergroup = new Group();
        ActorImage gaoliang = new ActorImage((int) PAK_ASSETS.IMG_PAO009, (id * 105) + 110, (int) PAK_ASSETS.IMG_LIGHT02, 0, this.towergroup);
        if (4 < id && id < 10) {
            gaoliang.setPosition((float) (((id - 5) * 105) + 45), (float) PAK_ASSETS.IMG_014);
        } else if (id > 9) {
            gaoliang.setPosition((float) (((id - 10) * 105) + 45), (float) PAK_ASSETS.IMG_QIANDAO012);
        }
        if (!getStrength(id).notLock()) {
            gaoliang.setVisible(false);
        }
        for (Strength s : LoadStrength.strengthData.values()) {
            String icon2 = s.getIcon();
            boolean isStreng = s.isStreng();
            i++;
            boolean isluck2 = s.notLock();
            ActorImage icon1 = new ActorImage(icon2, (i * 105) + 110, (int) PAK_ASSETS.IMG_LIGHT02, 0, this.towergroup);
            if (!isluck2 || !isStreng) {
                title_3 = title_32;
            } else {
                title_3 = new ActorImage((int) PAK_ASSETS.IMG_PAO003, (i * 105) + 60, 273, 12, this.towergroup);
            }
            ActorImage lock = null;
            if (this.towergroup != null) {
            }
            if (!isluck2) {
                lock = new ActorImage(107, (i * 105) + 110, (int) PAK_ASSETS.IMG_LIGHT02, 0, this.towergroup);
            }
            icon1.setTouchable(Touchable.enabled);
            icon1.setName(Constant.S_C + i);
            if (i > 4 && i < 10) {
                j++;
                icon1.setPosition((float) ((j * 105) + 60), (float) PAK_ASSETS.IMG_UI_A014);
                if (isluck2 && isStreng) {
                    title_3.setPosition((float) ((j * 105) + 60), (float) PAK_ASSETS.IMG_UI_A014);
                }
                if (lock != null) {
                    lock.setPosition((float) ((j * 105) + 80), (float) 403);
                }
            } else if (i > 9) {
                k++;
                icon1.setPosition((float) ((k * 105) + 60), (float) PAK_ASSETS.IMG_CHUANGGUAN004);
                if (isluck2 && isStreng) {
                    title_3.setPosition((float) ((k * 105) + 60), (float) PAK_ASSETS.IMG_CHUANGGUAN004);
                }
                if (lock != null) {
                    lock.setPosition((float) ((k * 105) + 80), (float) PAK_ASSETS.IMG_MORE001);
                }
            }
            this.truuetupGroup.addActor(this.towergroup);
            title_32 = title_3;
        }
        if (id == 11 && title_32 != null) {
            title_32.setVisible(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void tower(Strength s, int id) {
        TowerID = id;
        selectStrength = s;
        isluck = s.notLock();
        if (isluck) {
            if (this.tower != null) {
                this.tower.remove();
                this.tower.clear();
            }
            this.tower = new Group();
            level = s.getLevel();
            String name = s.getNamePath();
            icon = s.getIcon();
            String descp = s.getDescp();
            String buff = s.getBuff();
            NowPower = s.getNowPower();
            NextPower = s.getNextPower();
            Money = s.getMoney();
            MySprite sprite = s.getSprite();
            new ActorText("Lv:" + level, 135, (int) PAK_ASSETS.IMG_ZHANDOU054, 12, this.tower);
            new ActorImage(name, 160, (int) PAK_ASSETS.IMG_ZHANDOU011, 1, this.tower);
            StringBuffer stringBuffer = new StringBuffer(descp);
            stringBuffer.insert(8, "\n");
            if (stringBuffer.length() > 17) {
                stringBuffer.insert(17, "\n");
            }
            new ActorText(stringBuffer.toString(), 413, (int) PAK_ASSETS.IMG_TIP007, 12, this.tower);
            new ActorText(buff, 413, (int) PAK_ASSETS.IMG_SHOP009, 12, this.tower);
            if (id == 11) {
                new ActorText("无", 413, (int) PAK_ASSETS.IMG_TIP017, 12, this.tower);
                new ActorText("不可强化", 413, (int) PAK_ASSETS.IMG_ZHU029, 12, this.tower);
            } else {
                new ActorImage((int) PAK_ASSETS.IMG_SHOP044, (int) PAK_ASSETS.IMG_ZHUANPAN006, (int) PAK_ASSETS.IMG_ZHU038, 1, this.tower).setScale(0.8f, 0.8f);
                new ActorImage((int) PAK_ASSETS.IMG_UP009, (int) PAK_ASSETS.IMG_CHUANGGUAN003, (int) PAK_ASSETS.IMG_UP009, 1, this.tower);
                GNumSprite NowPower1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + NowPower, "X", -2, 0);
                NowPower1.setPosition((float) 410, (float) PAK_ASSETS.IMG_TIP016);
                this.tower.addActor(NowPower1);
                GNumSprite NextPower1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + NextPower, "X", -2, 0);
                NextPower1.setPosition((float) PAK_ASSETS.IMG_GONGGAO009, (float) PAK_ASSETS.IMG_TIP016);
                NextPower1.setColor(MyUIGetFont.paoNextPowerColor);
                this.tower.addActor(NextPower1);
                GNumSprite money1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + Money, "X", -2, 4);
                money1.setPosition((float) PAK_ASSETS.IMG_MAP1, (float) PAK_ASSETS.IMG_ZHU037);
                this.tower.addActor(money1);
            }
            sprite.setScale(2.0f, 2.0f);
            sprite.setPosition((float) 150, (float) PAK_ASSETS.IMG_CHONGZHI005);
            sprite.setTouchable(Touchable.disabled);
            this.tower.addActor(sprite);
            this.truuetupGroup.addActor(this.tower);
        }
    }

    /* access modifiers changed from: package-private */
    public void initData() {
    }

    private void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 180, 12, this.truuetupGroup);
        SimpleButton call = new SimpleButton(PAK_ASSETS.IMG_PENHUO01, 890, 1, new int[]{PAK_ASSETS.IMG_PAO006, PAK_ASSETS.IMG_PAO007, PAK_ASSETS.IMG_PAO006});
        call.setName("1");
        this.effect6 = new Effect().getEffect_Diejia(65, PAK_ASSETS.IMG_JIESUAN009, 933);
        this.truuetupGroup.addActor(call);
        this.truuetupGroup.addActor(this.effect6);
    }

    public Strength getStrength(int id) {
        return LoadStrength.strengthData.get(LoadStrength.towerNameList.get(id));
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.truuetupGroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 1000;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MyTowerUP.this.free();
                            return;
                        case 1:
                            if (MyTowerUP.isluck) {
                                MyTowerUP.this.UPOne();
                                return;
                            }
                            return;
                        case 20:
                            if (MyTowerUP.this.getIsLock(0)) {
                                MyTowerUP.this.initframe(0);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(0), 0);
                                return;
                            }
                            return;
                        case 21:
                            if (MyTowerUP.this.getIsLock(1)) {
                                MyTowerUP.this.initframe(1);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(1), 1);
                                return;
                            }
                            return;
                        case 22:
                            if (MyTowerUP.this.getIsLock(2)) {
                                MyTowerUP.this.initframe(2);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(2), 2);
                                return;
                            }
                            return;
                        case 23:
                            if (MyTowerUP.this.getIsLock(3)) {
                                MyTowerUP.this.initframe(3);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(3), 3);
                                return;
                            }
                            return;
                        case 24:
                            if (MyTowerUP.this.getIsLock(4)) {
                                MyTowerUP.this.initframe(4);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(4), 4);
                                return;
                            }
                            return;
                        case 25:
                            if (MyTowerUP.this.getIsLock(5)) {
                                MyTowerUP.this.initframe(5);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(5), 5);
                                return;
                            }
                            return;
                        case 26:
                            if (MyTowerUP.this.getIsLock(6)) {
                                MyTowerUP.this.initframe(6);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(6), 6);
                                return;
                            }
                            return;
                        case 27:
                            if (MyTowerUP.this.getIsLock(7)) {
                                MyTowerUP.this.initframe(7);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(7), 7);
                                return;
                            }
                            return;
                        case 28:
                            if (MyTowerUP.this.getIsLock(8)) {
                                MyTowerUP.this.initframe(8);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(8), 8);
                                return;
                            }
                            return;
                        case 29:
                            if (MyTowerUP.this.getIsLock(9)) {
                                MyTowerUP.this.initframe(9);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(9), 9);
                                return;
                            }
                            return;
                        case PAK_ASSETS.IMG_JIDONGSHEXIAN:
                            if (MyTowerUP.this.getIsLock(10)) {
                                MyTowerUP.this.initframe(10);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(10), 10);
                                return;
                            }
                            return;
                        case PAK_ASSETS.IMG_JIDONGSHEXIAN2:
                            if (MyTowerUP.this.getIsLock(11)) {
                                MyTowerUP.this.initframe(11);
                                MyTowerUP.this.tower(MyTowerUP.this.getStrength(11), 11);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public boolean getIsLock(int id) {
        return getStrength(id).notLock();
    }

    /* access modifiers changed from: package-private */
    public void UPOne() {
        if (selectStrength.getIndex() != 11) {
            if (!selectStrength.canStrength()) {
                Sound.playButtonPressed();
                new MyResetRunk(2, RankData.getNextRank(TowerID));
            } else if (RankData.spendCake(Money)) {
                Sound.playSound(80);
                this.effect1 = new Effect().getEffect_Diejia(83, PAK_ASSETS.IMG_MAP1, PAK_ASSETS.IMG_UP010);
                this.truuetupGroup.addActor(this.effect1);
                if (this.tower != null) {
                    this.tower.remove();
                    this.tower.clear();
                }
                new MyGetTowerUpEffect();
                selectStrength.strength();
                initframe(TowerID);
                tower(selectStrength, TowerID);
            } else {
                Sound.playButtonPressed();
                if (MySwitch.isCaseA == 0) {
                    MyTip.Notenought(true);
                } else {
                    new MyGift(10);
                }
            }
        }
    }

    public void exit() {
        this.truuetupGroup.remove();
        this.truuetupGroup.clear();
    }
}
