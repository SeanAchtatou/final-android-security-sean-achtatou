package com.sg.td.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GSimpleAction;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.record.Strength;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyGetTowerUpInMapEffect extends MyGroup {
    static Group anygroup;
    Effect back;
    Group getupgroup;

    public void init() {
        initbj();
    }

    public MyGetTowerUpInMapEffect(Strength s) {
        initRoleUp(s);
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        Sound.playSound(71);
        this.getupgroup = new Group();
        this.getupgroup.addActor(new Mask());
        GameStage.addActor(this.getupgroup, 4);
    }

    /* access modifiers changed from: package-private */
    public void initRoleUp(Strength s) {
        System.out.println("sname--------:" + s.getIcon());
        int i = Mymainmenu2.selectIndex;
        this.back = new Effect();
        this.back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_QIANDAO011, this.getupgroup);
        new ActorImage(s.getIcon(), 320, (int) PAK_ASSETS.IMG_QIANDAO011, 1, this.getupgroup);
        new ActorImage((int) PAK_ASSETS.IMG_TIP002, 320, (int) PAK_ASSETS.IMG_ONLINE006, 1, this.getupgroup);
        new ActorImage((int) PAK_ASSETS.IMG_TIP009, 320, (int) PAK_ASSETS.IMG_ZANTING008, 1, this.getupgroup);
        new ActorText((s.getLevel() - 1) + "", (int) PAK_ASSETS.IMG_SHUAGUAI01, (int) PAK_ASSETS.IMG_MEN, 1, this.getupgroup);
        new ActorText(s.getNowPower() + "", (int) PAK_ASSETS.IMG_SMOKE1, (int) PAK_ASSETS.IMG_ZHANDOU023, 1, this.getupgroup);
        ActorText levelnext = new ActorText(s.getLevel() + "", (int) PAK_ASSETS.IMG_JIESUO007, (int) PAK_ASSETS.IMG_MEN, 1, this.getupgroup);
        ActorText skillto1 = new ActorText(s.getNextPower() + "", (int) PAK_ASSETS.IMG_LOAD2, (int) PAK_ASSETS.IMG_ZHANDOU023, 1, this.getupgroup);
        levelnext.setColor(Color.GREEN);
        skillto1.setColor(Color.GREEN);
        this.getupgroup.addAction(getCountAction(3, this.getupgroup, this.back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    public void touchAnywhere() {
        anygroup = new Group();
        anygroup.addActor(this.getupgroup);
        anygroup.setPosition(5.0f, 5.0f);
        anygroup.setSize(640.0f, 1136.0f);
        GameStage.addActor(anygroup, 4);
        anygroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                GameStage.removeActor(MyGetTowerUpInMapEffect.this.getupgroup);
                MyGetTowerUpInMapEffect.this.getupgroup.remove();
                MyGetTowerUpInMapEffect.this.getupgroup.clear();
                MyGetTowerUpInMapEffect.this.back.remove_Diejia();
                MyGetTowerUpInMapEffect.anygroup.remove();
                MyGetTowerUpInMapEffect.anygroup.clear();
                System.out.println("clear");
            }
        });
    }

    public static Action getCountAction(final int time, final Group group, final Effect back2) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    GameStage.removeActor(group);
                    if (MyGetTowerUpInMapEffect.anygroup != null) {
                        MyGetTowerUpInMapEffect.anygroup.remove();
                        MyGetTowerUpInMapEffect.anygroup.clear();
                    }
                    group.remove();
                    group.clear();
                    back2.remove_Diejia();
                    group.clear();
                }
                return true;
            }
        });
    }

    public void exit() {
    }
}
