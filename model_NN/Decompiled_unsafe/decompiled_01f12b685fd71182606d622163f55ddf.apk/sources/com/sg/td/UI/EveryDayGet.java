package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.SimpleButton;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class EveryDayGet extends MyGroup {
    static int[][] chosePoint = {new int[]{105, PAK_ASSETS.IMG_JIESUO007}, new int[]{249, PAK_ASSETS.IMG_JIESUO007}, new int[]{PAK_ASSETS.IMG_UI_ZUANSHI01, PAK_ASSETS.IMG_JIESUO007}, new int[]{PAK_ASSETS.IMG_PAO001, PAK_ASSETS.IMG_JIESUO007}, new int[]{177, PAK_ASSETS.IMG_ZHANDOU047}, new int[]{PAK_ASSETS.IMG_LEIGUMAN08A, PAK_ASSETS.IMG_ZHANDOU047}, new int[]{PAK_ASSETS.IMG_QIANDAO007, PAK_ASSETS.IMG_ZHANDOU047}};
    static Group daygetgroup;
    static int getindex;
    static boolean isLingqu;
    SimpleButton Noget;
    ActorImage bracelet;
    ActorImage checkImage;
    ActorImage choseImage;
    ActorButton close;
    ActorImage content;
    Effect effectChose;
    Effect effectGet;
    SimpleButton get;
    ActorImage giftImage;
    ActorImage reviveheart;
    ActorImage title_1;
    ActorImage title_2;
    ActorImage todayImage;
    ActorImage todayframe;

    public void init() {
        daygetgroup = new Group();
        GameStage.addActor(daygetgroup, 4);
        daygetgroup.addActor(new Mask());
        getindex = RankData.getGetIndex();
        initbj(getindex);
        initbutton();
        groupListener();
    }

    /* access modifiers changed from: package-private */
    public void initbj(int i) {
        GameUITools.GetbackgroundImage(320, 250, 9, daygetgroup, false, true);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA, 1, daygetgroup);
        this.title_2 = new ActorImage((int) PAK_ASSETS.IMG_QIANDAO001, 320, 190, 1, daygetgroup);
        if (RankData.isSameDay()) {
            initChose(1, RankData.getTodayGetIndex());
        } else {
            initChose(0, i);
        }
    }

    /* access modifiers changed from: package-private */
    public void initChose(int i, int iday) {
        System.out.println("i::" + i);
        System.out.println("iday:" + iday);
        System.out.println("getindex:" + getindex);
        if (i < 0 || i > 6) {
        }
        if (iday < 0 || iday > 6) {
            iday = 0;
            if (this.checkImage != null) {
                this.checkImage.remove();
                this.checkImage.clean();
            }
        }
        if (this.choseImage != null) {
            this.choseImage.remove();
            this.choseImage.clean();
        }
        for (int j = 0; j < 7; j++) {
            if (j != iday) {
                this.giftImage = new ActorImage((int) PAK_ASSETS.IMG_QIANDAO009, chosePoint[j][0], chosePoint[j][1], 1, daygetgroup);
            }
        }
        this.todayImage = new ActorImage((int) PAK_ASSETS.IMG_QIANDAO010, chosePoint[iday][0], chosePoint[iday][1], 1, daygetgroup);
        this.choseImage = new ActorImage((int) PAK_ASSETS.IMG_QIANDAO003, chosePoint[iday][0], chosePoint[iday][1], 1, daygetgroup);
        this.content = new ActorImage((int) PAK_ASSETS.IMG_QIANDAO004, 320, (int) PAK_ASSETS.IMG_2X1, 1, daygetgroup);
        if (RankData.isSameDay()) {
            this.checkImage = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC009, chosePoint[iday][0] + 30, chosePoint[iday][1] + 55, 1, daygetgroup);
        }
        for (int j2 = 0; j2 < iday && getindex != 7; j2++) {
            this.checkImage = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC009, chosePoint[j2][0] + 30, chosePoint[j2][1] + 55, 1, daygetgroup);
        }
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        int[] t = {PAK_ASSETS.IMG_QIANDAO006, PAK_ASSETS.IMG_QIANDAO007};
        int[] tt = {PAK_ASSETS.IMG_QIANDAO011, PAK_ASSETS.IMG_QIANDAO012};
        if (this.get != null) {
            this.get.remove();
            this.get.clear();
        }
        this.get = new SimpleButton(180, PAK_ASSETS.IMG_ZHU040, 1, t);
        this.Noget = new SimpleButton(180, PAK_ASSETS.IMG_ZHU040, 1, tt);
        if (RankData.isSameDay()) {
            this.get.setVisible(false);
            if (this.get != null) {
                this.get.remove();
                this.get.clear();
            }
            if (this.effectGet != null) {
                this.effectGet.remove_Diejia();
            }
        } else {
            this.Noget.setVisible(false);
            if (this.Noget != null) {
                System.out.println("CCCCCCCCC");
                this.Noget.remove();
                this.Noget.clear();
            }
            this.effectGet = new Effect();
            this.effectGet.addEffect_Diejia(80, 320, 915, 4);
        }
        this.get.setName("1");
        this.Noget.setName(Constant.S_C);
        daygetgroup.addActor(this.get);
        daygetgroup.addActor(this.Noget);
    }

    /* access modifiers changed from: package-private */
    public void groupListener() {
        daygetgroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            EveryDayGet.this.free();
                            return;
                        case 1:
                            Sound.playSound(54);
                            EveryDayGet.this.move();
                            EveryDayGet.this.initGet();
                            EveryDayGet.isLingqu = true;
                            RankData.getEveryDayTarget();
                            EveryDayGet.this.initChose(1, EveryDayGet.getindex);
                            RankData.eveyDayGet();
                            EveryDayGet.this.initbutton();
                            return;
                        case 2:
                            Sound.playButtonClosed();
                            EveryDayGet.this.free();
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void initGet() {
        Get get2 = LoadGet.getData.get("EveryDayGet");
        if (getindex == 7) {
            getindex = 0;
        }
        new MyGet(get2, getindex, 0);
        Mymainmenu2.isAward = true;
        System.out.println("isAward11:" + Mymainmenu2.isAward);
        switch (getindex) {
            case 0:
                RankData.addProps(0, 1);
                return;
            case 1:
                RankData.addHoneyNum(1);
                return;
            case 2:
                RankData.addHeartNum(1);
                return;
            case 3:
                RankData.addCakeNum(1000);
                return;
            case 4:
                RankData.addProps(3, 4);
                return;
            case 5:
                RankData.addHoneyNum(2);
                return;
            case 6:
                RankData.addDiamondNum(100);
                return;
            default:
                return;
        }
    }

    public void exit() {
        if (daygetgroup != null) {
            daygetgroup.remove();
            daygetgroup.clear();
        }
    }

    public void move() {
        this.effectGet.remove_Diejia();
        if (this.effectChose != null) {
            this.effectChose.remove_Diejia();
        }
    }

    public void chong() {
        free();
        new EveryDayGet();
    }
}
