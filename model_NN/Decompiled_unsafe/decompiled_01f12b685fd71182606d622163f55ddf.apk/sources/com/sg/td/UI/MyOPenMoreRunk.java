package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyOPenMoreRunk extends MyGroup {
    Group openrunkgroup;

    public void init() {
        this.openrunkgroup = new Group();
        this.openrunkgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.openrunkgroup, 4);
        this.openrunkgroup.addActor(new Mask());
        initbj();
        initbutton();
        initData();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 4, this.openrunkgroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, (int) PAK_ASSETS.IMG_UI_ZHUANPAN006, 1, this.openrunkgroup);
        new ActorImage((int) PAK_ASSETS.IMG_CHONGZHI006, 320, (int) PAK_ASSETS.IMG_UI002, 1, this.openrunkgroup);
    }

    /* access modifiers changed from: package-private */
    public void initData() {
        new ActorText(10 + "", 400, (int) PAK_ASSETS.IMG_SHIBAI, 12, this.openrunkgroup);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.openrunkgroup);
        SimpleButton get = new SimpleButton(200, PAK_ASSETS.IMG_PAO002, 1, new int[]{PAK_ASSETS.IMG_CHONGZHI007, PAK_ASSETS.IMG_CHONGZHI008, PAK_ASSETS.IMG_CHONGZHI007});
        get.setName("1");
        this.openrunkgroup.addActor(get);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.openrunkgroup.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            MyOPenMoreRunk.this.free();
                            break;
                        case 1:
                            MyOPenMoreRunk.this.free();
                            break;
                    }
                    super.clicked(event, x, y);
                }
            }
        });
    }

    public void exit() {
        this.openrunkgroup.remove();
        this.openrunkgroup.clear();
    }
}
