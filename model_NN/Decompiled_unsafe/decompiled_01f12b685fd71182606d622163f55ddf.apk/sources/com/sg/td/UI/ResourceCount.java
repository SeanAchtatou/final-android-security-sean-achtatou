package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class ResourceCount extends MyGroup {
    static Group countgroup;
    GNumSprite barrier;
    GNumSprite blood;
    ActorButton close;
    GNumSprite food;
    ActorImage frame;
    GNumSprite partner;
    GNumSprite power;
    GNumSprite revive;
    GNumSprite shoe;
    GNumSprite starNum;
    ActorImage title_1;
    ActorImage title_2;

    public void init() {
        countgroup = new Group();
        countgroup.addActor(new Mask());
        GameStage.addActor(countgroup, 4);
        initbj();
        initframe();
        initdata();
        initbutton();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 180, 10, countgroup, false, true);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 150, 1, countgroup);
        this.title_2 = new ActorImage((int) PAK_ASSETS.IMG_TONGJI002, 320, 120, 1, countgroup);
    }

    /* access modifiers changed from: package-private */
    public void initframe() {
        this.frame = new ActorImage((int) PAK_ASSETS.IMG_TONGJI001, 25, 250, 12, countgroup);
    }

    /* access modifiers changed from: package-private */
    public void initdata() {
        int star1 = RankData.getStarNum();
        int blood1 = RankData.getFullBloodThroughs();
        int barrier1 = RankData.getCleanDeckThroughs();
        int food1 = RankData.getcleanFoodThroughs();
        int partner1 = RankData.getRoleNum();
        int power1 = RankData.getHoneyNum();
        int revive1 = RankData.getHeartNum();
        int shoe1 = RankData.getPropNum(3) + RankData.getPropNum(0) + RankData.getPropNum(1) + RankData.getPropNum(2);
        this.starNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC014, star1 + "/" + RankData.getTotalStarNum(), "/", -2, 6);
        this.starNum.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) 320);
        countgroup.addActor(this.starNum);
        this.blood = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + blood1, "X", -2, 6);
        this.blood.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_LIGHTINGMAGICP);
        countgroup.addActor(this.blood);
        this.barrier = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + barrier1, "X", -2, 6);
        this.barrier.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_JIDI003);
        countgroup.addActor(this.barrier);
        this.food = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + food1, "X", -2, 6);
        this.food.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_ZHUANPAN009);
        countgroup.addActor(this.food);
        this.partner = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC014, partner1 + "/4", "/", -2, 6);
        this.partner.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_E03A);
        countgroup.addActor(this.partner);
        this.power = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + power1, "X", -2, 6);
        this.power.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_ZHANDOU010);
        countgroup.addActor(this.power);
        this.revive = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + revive1, "X", -2, 6);
        this.revive.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_ZHANDOU063);
        countgroup.addActor(this.revive);
        this.shoe = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + shoe1, "X", -2, 6);
        this.shoe.setPosition((float) PAK_ASSETS.IMG_PUBLIC017, (float) PAK_ASSETS.IMG_SHOP034);
        countgroup.addActor(this.shoe);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        this.close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "close", (int) PAK_ASSETS.IMG_C01, 135, 12, countgroup);
        this.close.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                ResourceCount.this.free();
            }
        });
    }

    public void exit() {
        countgroup.remove();
        countgroup.clear();
    }
}
