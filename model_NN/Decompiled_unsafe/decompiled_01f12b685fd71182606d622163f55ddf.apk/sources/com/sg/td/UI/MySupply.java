package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MySupply extends MyGroup {
    static int[] frame_2_2 = {45, 46, 47, 48, 49, 53};
    static int[] title_2_2 = {37, 38, 39, 40, 41, 52};
    Effect effectGet;
    Group supplygruop;

    public void init() {
        this.supplygruop = new Group();
        this.supplygruop.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.supplygruop, 4);
        this.supplygruop.addActor(new Mask());
    }

    public MySupply(int id) {
        initbj(id);
        initframe(id);
        initbutton(id);
        initlistener(id);
    }

    /* access modifiers changed from: package-private */
    public void initbj(int id) {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_DAJI03, 5, this.supplygruop, false, true);
        new ActorImage(42, 320, (int) PAK_ASSETS.IMG_PENSHEQI_SHUOMING, 1, this.supplygruop);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_016BDJ, 1, this.supplygruop);
        new ActorImage(title_2_2[id], 320, (int) PAK_ASSETS.IMG_PENSHEQI_SHUOMING, 1, this.supplygruop);
    }

    /* access modifiers changed from: package-private */
    public void initframe(int id) {
        if (id != 4) {
            new ActorImage(43, 320, (int) PAK_ASSETS.IMG_JIESUO010, 1, this.supplygruop);
        } else {
            new ActorImage(44, 320, (int) PAK_ASSETS.IMG_ZHUANPAN009, 1, this.supplygruop);
        }
        this.effectGet = new Effect();
        this.effectGet.addEffect_Diejia(59, 320, (int) PAK_ASSETS.IMG_JIESUO010, 4);
        new ActorImage(frame_2_2[id], 320, 423, 1, this.supplygruop);
    }

    /* access modifiers changed from: package-private */
    public void initbutton1(int id) {
        int[] price = {100, 500, 300, 2000, 0, 200};
        int[] diamondprice = {PAK_ASSETS.IMG_G02, 2000, 2000, 500, 0, 1000};
        int[] tt = {50, 51, 50};
        switch (id) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
                SimpleButton button_1 = new SimpleButton(50, PAK_ASSETS.IMG_ZHANDOU023, 1, tt);
                button_1.setName(id + "1");
                this.supplygruop.addActor(button_1);
                SimpleButton button_2 = new SimpleButton(PAK_ASSETS.IMG_210P, PAK_ASSETS.IMG_ZHANDOU023, 1, tt);
                button_2.setName(id + Constant.S_C);
                this.supplygruop.addActor(button_2);
                new ActorImage(141, 110, (int) PAK_ASSETS.IMG_ZHANDOU066, 1, this.supplygruop);
                GNumSprite diamond = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC012, diamondprice[id] + "", "X", -2, 4);
                diamond.setPosition(185.0f, 700.0f);
                this.supplygruop.addActor(diamond);
                new ActorImage(141, (int) PAK_ASSETS.IMG_JIDI001, (int) PAK_ASSETS.IMG_ZHANDOU066, 1, this.supplygruop);
                GNumSprite priceNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC012, "" + price[id], "X", -2, 0);
                priceNum.setPosition(465.0f, 685.0f);
                this.supplygruop.addActor(priceNum);
                return;
            case 4:
                SimpleButton button_3 = new SimpleButton(200, PAK_ASSETS.IMG_ZHANDOU023, 1, tt);
                button_3.setName(id + Constant.S_D);
                this.supplygruop.addActor(button_3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void initbutton(int id) {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, 250, 12, this.supplygruop);
        initbutton1(id);
    }

    /* access modifiers changed from: package-private */
    public void initlistener(final int id) {
        this.supplygruop.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    Get get = LoadGet.getData.get("Supply");
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            if (id == 0) {
                                if (Rank.isRanking()) {
                                    Rank.putSkill(true);
                                }
                            } else if (id == 1) {
                                if (Rank.isRanking()) {
                                    Rank.repalyWave();
                                }
                            } else if (id == 2 && RankData.getEndlessPowerDays() > 0 && MyGameMap.isInMap) {
                                GameStage.clearAllLayers();
                                GameMain.toScreen(new MyGameMap());
                            }
                            MySupply.this.free();
                            return;
                        case 1:
                            if (RankData.spendDiamond(PAK_ASSETS.IMG_G02)) {
                                Sound.playSound(8);
                                new MyGet(get, 0, 2);
                                RankData.addHoneyNum(6);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 2:
                            if (RankData.spendDiamond(100)) {
                                Sound.playSound(8);
                                new MyGet(get, 1, 2);
                                RankData.addHoneyNum(1);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 11:
                            if (RankData.spendDiamond(2000)) {
                                Sound.playSound(8);
                                new MyGet(get, 2, 2);
                                RankData.buyEndlessReplay(30);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 12:
                            if (RankData.spendDiamond(500)) {
                                Sound.playSound(8);
                                new MyGet(get, 3, 2);
                                RankData.buyEndlessReplay(5);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 13:
                        case 31:
                        case 32:
                        case 43:
                        default:
                            return;
                        case 21:
                            if (RankData.spendDiamond(2000)) {
                                Sound.playSound(8);
                                new MyGet(get, 4, 2);
                                RankData.buyEndlessPower(30);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 22:
                            if (RankData.spendDiamond(300)) {
                                Sound.playSound(8);
                                new MyGet(get, 5, 2);
                                RankData.buyPower(5);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 51:
                            if (RankData.spendDiamond(1000)) {
                                Sound.playSound(8);
                                new MyGet(get, 6, 2);
                                RankData.addHeartNum(5);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                        case 52:
                            if (RankData.spendDiamond(200)) {
                                Sound.playSound(8);
                                new MyGet(get, 7, 2);
                                RankData.addHeartNum(1);
                                return;
                            } else if (MySwitch.isCaseA == 0) {
                                MyTip.Notenought(false);
                                return;
                            } else {
                                new MyGift(7);
                                return;
                            }
                    }
                }
            }
        });
    }

    public void exit() {
        this.effectGet.remove_Diejia();
        this.supplygruop.remove();
        this.supplygruop.clear();
    }
}
