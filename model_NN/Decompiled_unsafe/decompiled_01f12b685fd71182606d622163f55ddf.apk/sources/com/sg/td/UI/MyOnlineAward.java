package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Particle.GameParticle;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class MyOnlineAward extends MyGroup implements GameConstant {
    static Group onlinegroup;
    MyImage bar;
    Group button;
    ActorButton close;
    ActorImage frame_1;
    ActorSprite[] get;
    GameParticle libaop;
    float speed = 0.01f;
    GNumSprite time;
    ActorClipImage timebar;
    ActorImage timebar_1;
    private float timeparLength;
    private float timeparLength1;
    ActorImage title_1;
    ActorImage title_2;
    ActorImage title_3;

    public void init() {
        this.get = new ActorSprite[4];
        onlinegroup = new Group() {
            public void act(float delta) {
                super.act(delta);
                MyOnlineAward.this.run();
            }
        };
        GameStage.addActor(onlinegroup, 4);
        onlinegroup.addActor(new Mask());
        initbj();
        initframe();
        initbutton();
        initbuttonlistener();
    }

    /* access modifiers changed from: package-private */
    public void run() {
        timeparActionlength();
        changeButton();
        if (this.time != null) {
            this.time.setNum(initGettime() + "");
        }
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_JUEHUIXUANFU, 5, onlinegroup, false, false);
        this.title_3 = new ActorImage((int) PAK_ASSETS.IMG_ONLINE002, 320, 158, 1, onlinegroup);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 188, 1, onlinegroup);
        this.title_2 = new ActorImage((int) PAK_ASSETS.IMG_ONLINE001, 320, 158, 1, onlinegroup);
    }

    /* access modifiers changed from: package-private */
    public void initframe() {
        this.frame_1 = new ActorImage((int) PAK_ASSETS.IMG_ONLINE003, 320, (int) PAK_ASSETS.IMG_UI002, 1, onlinegroup);
        this.time = new GNumSprite((int) PAK_ASSETS.IMG_ONLINE005, initGettime() + "", "", -2, 4);
        this.time.setPosition((float) 193, (float) PAK_ASSETS.IMG_BISHATIPSPUR);
        addActor(this.time);
        this.timebar = new ActorClipImage(PAK_ASSETS.IMG_ONLINE004, 40, PAK_ASSETS.IMG_DAJI10, 12, onlinegroup);
        onlinegroup.addActor(this.time);
        this.timebar.setVisible(false);
        timeparActionlength();
        this.timeparLength1 = this.timebar.getWidth();
    }

    private void timeparActionlength() {
        this.timeparLength = (float) RankData.getClipTimeLength();
        if (this.timeparLength != Animation.CurveTimeline.LINEAR) {
            this.timebar.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.timeparLength, this.timebar.getHeight());
            this.timebar.setVisible(true);
        }
    }

    public int initGettime() {
        return RankData.getWaitTime();
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        if (this.button != null) {
            this.button.remove();
            this.button.clear();
        }
        this.button = new Group();
        this.close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, Constant.S_E, (int) PAK_ASSETS.IMG_E02, 180, 12, this.button);
        for (int i = 0; i < 4; i++) {
            loadGet((i * 143) + 30, PAK_ASSETS.IMG_K04A, i);
        }
        onlinegroup.addActor(this.button);
    }

    /* access modifiers changed from: package-private */
    public void loadGet(int x, int y, int id) {
        boolean isCanGet = RankData.canReceive(id);
        System.out.println("id::" + isCanGet);
        if (this.get[id] == null) {
            this.get[id] = new ActorSprite(x, y, 4, PAK_ASSETS.IMG_ONLINE008, PAK_ASSETS.IMG_ONLINE006, PAK_ASSETS.IMG_ONLINE008, PAK_ASSETS.IMG_ONLINE009);
        }
        this.get[id].setName(id + "");
        if (isCanGet) {
            this.get[id].setTexture(1);
        } else {
            this.get[id].setTexture(0);
        }
        if (RankData.haveTaken(id)) {
            this.get[id].setTexture(3);
        }
        this.button.addActor(this.get[id]);
        this.button.addActor(new Effect().getEffect_Diejia(79, x + 55, y + 20));
    }

    /* access modifiers changed from: package-private */
    public void changeButton() {
        for (int i = 0; i < this.get.length; i++) {
            if (this.get[i] != null) {
                if (RankData.canReceive(i)) {
                    this.get[i].setTexture(1);
                } else {
                    this.get[i].setTexture(0);
                }
                if (RankData.haveTaken(i)) {
                    this.get[i].setTexture(3);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void initbuttonlistener() {
        onlinegroup.addListener(new InputListener() {
            private int codeName;

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    System.out.println("buttonName:" + buttonName);
                    if (buttonName != null) {
                        this.codeName = Integer.parseInt(buttonName);
                        System.out.println("codeName:" + this.codeName);
                    } else {
                        this.codeName = 9;
                    }
                    switch (this.codeName) {
                        case 0:
                            MyOnlineAward.this.getAward(0);
                            break;
                        case 1:
                            MyOnlineAward.this.getAward(1);
                            break;
                        case 2:
                            MyOnlineAward.this.getAward(2);
                            break;
                        case 3:
                            MyOnlineAward.this.getAward(3);
                            break;
                        case 4:
                            Sound.playButtonClosed();
                            MyOnlineAward.this.free();
                            break;
                    }
                    super.touchUp(event, x, y, pointer, button);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void getAward(int index) {
        Get get2 = LoadGet.getData.get("Online");
        if (RankData.canReceive(index)) {
            RankData.receiveAward(index);
            get(index);
            initbutton();
            Sound.playSound(8);
            new MyGet(get2, index, 2);
        }
    }

    /* access modifiers changed from: package-private */
    public void get(int index) {
        switch (index) {
            case 0:
                RankData.addHoneyNum(2);
                return;
            case 1:
                RankData.addHeartNum(2);
                return;
            case 2:
                RankData.addCakeNum(3000);
                return;
            case 3:
                RankData.addDiamondNum(500);
                return;
            default:
                return;
        }
    }

    public void exit() {
        onlinegroup.remove();
        onlinegroup.clear();
    }
}
