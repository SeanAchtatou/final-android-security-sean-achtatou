package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.LoadFont;
import com.kbz.Actors.LoadingGroup;
import com.kbz.Sound.GameSound;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;
import com.sg.td.message.MySwitch;
import com.sg.td.record.LoadGet;
import com.sg.td.spine.CoverSpine;
import com.sg.util.GameScreen;
import com.sg.util.GameStage;

public class MyCover extends GameScreen implements GameConstant {
    public static boolean isCover;
    public static boolean isCover_PoP;
    boolean addPause;
    CoverSpine cover;
    ActorImage coverCenter;
    Group group;
    private LoadingGroup loadingGroup;
    CoverSpine logo;
    float time;

    public void init() {
        LoadFont.initFontCu();
        GameSound.playMusic(2);
        this.group = new Group();
        this.group.setSize(640.0f, 1136.0f);
        this.cover = new CoverSpine(320, PAK_ASSETS.IMG_2X1, 5);
        this.logo = new CoverSpine(320, 128, 11);
        this.group.addActor(this.cover);
        this.group.addActor(this.logo);
        new ActorText("批准文号:新广出审[2016]2284号\n出版物号:ISBN 978-7-7979-0991-4", 10, 5, 12, this.group);
        new ActorText("抵制不良游戏，拒绝盗版游戏。注意自我保护，谨防受骗上当。\n适度游戏益脑，沉迷游戏伤身。合理安排时间，享受健康生活。", 320, 1098, 1, this.group);
        this.group.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!MyCover.this.addPause) {
                    return true;
                }
                Sound.playButtonPressed();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (!MyCover.this.addPause) {
                    return;
                }
                if (RankData.isFirstPlay()) {
                    GameMain.toScreen(new MyGameMap());
                    return;
                }
                MyCover.isCover = true;
                MyCover.isCover_PoP = true;
                GameMain.toScreen(new Mymainmenu2());
            }
        });
        GameStage.addActor(this.group, 4);
        System.out.println("添加cover...");
        initLoadingGroup();
    }

    public void run(float delta) {
    }

    public void dispose() {
        this.group.remove();
        this.group.clear();
    }

    private void initLoadingGroup() {
        this.loadingGroup = new LoadingGroup();
        this.loadingGroup.addLoadingListener(new LoadingGroup.LoadingListener() {
            public void begin() {
                MyCover.this.loading();
                RankData.initGameData();
                LoadGet.loadGetDate();
                System.out.println("+++++++++MySwitch.isParent" + MySwitch.isParent);
                if (MySwitch.isParent) {
                    MySwitch.isCaseA = 0;
                    GameSpecial.initCaseA();
                    MyCover.this.changegift();
                    return;
                }
                System.out.println("+++++++++切包");
                GameSpecial.initCloudNew();
            }

            public void end() {
                if (MySwitch.isCaseA == 5) {
                    MyCover.this.coverCenter.addAction(Actions.sequence(Actions.delay(15.0f), Actions.run(new Runnable() {
                        public void run() {
                            if (MyCover.this.coverCenter != null) {
                                MyCover.this.coverCenter.remove();
                                MyCover.this.coverCenter.clean();
                            }
                            MyCover.this.loadEnd();
                        }
                    })));
                    return;
                }
                if (MyCover.this.coverCenter != null) {
                    MyCover.this.coverCenter.remove();
                    MyCover.this.coverCenter.clean();
                }
                MyCover.this.loadEnd();
            }

            public void update(float progress) {
            }
        });
        GameStage.addActor(this.loadingGroup, 2);
    }

    /* access modifiers changed from: package-private */
    public void loadEnd() {
        new ActorImage(83, 320, 868, 1, this.group).addAction(Actions.repeat(-1, Actions.sequence(Actions.alpha(0.3f, 0.5f), Actions.alpha(1.0f, 0.5f))));
        this.addPause = true;
    }

    /* access modifiers changed from: package-private */
    public void loading() {
        this.coverCenter = new ActorImage(85, 320, 868, 1, this.group);
    }

    /* access modifiers changed from: private */
    public void changegift() {
        switch (Integer.parseInt(MySwitch.giftType)) {
            case 0:
                GMessage.PP_TEMPgift = 12;
                break;
            case 1:
                GMessage.PP_TEMPgift = 13;
                break;
            case 2:
                GMessage.PP_TEMPgift = 14;
                break;
        }
        if (MySwitch.caseJiOrMM == 2) {
            GMessage.PP_TEMPgift = 7;
        }
        if (MySwitch.isSimId != 0) {
            GMessage.PP_TEMPgift = 7;
        }
    }
}
