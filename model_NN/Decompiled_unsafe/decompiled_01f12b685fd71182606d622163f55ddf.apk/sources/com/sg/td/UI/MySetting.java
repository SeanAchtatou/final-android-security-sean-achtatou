package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Sound.GameSound;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class MySetting extends MyGroup {
    static Group setgroup;
    int bgX = 320;
    int bgY = 170;
    ActorButton close;
    ActorButton music;
    MyImage openORclose_music;
    MyImage openORclose_sound;
    ActorButton reset;
    ActorButton sound;
    ActorImage soundimage;
    ActorImage title_1;
    ActorImage title_2;

    public void init() {
        setgroup = new Group();
        GameStage.addActor(setgroup, 4);
        setgroup.addActor(new Mask());
        initbj();
        setListener();
        initbutton();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, 200, 3, setgroup, false, false);
        this.title_1 = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 170, 1, setgroup);
        this.title_2 = new ActorImage((int) PAK_ASSETS.IMG_SHEZHI001, 320, 140, 1, setgroup);
        this.soundimage = new ActorImage((int) PAK_ASSETS.IMG_SHEZHI006, (int) PAK_ASSETS.IMG_ICE70, 320, 1, setgroup);
        this.reset = new ActorButton((int) PAK_ASSETS.IMG_SHEZHI007, "1", 320, (int) PAK_ASSETS.IMG_ZHUANPAN001, 1, setgroup);
        if (GameSound.isMusicOpen) {
            this.music = new ActorButton((int) PAK_ASSETS.IMG_SHEZHI003, Constant.S_C, 320, (int) PAK_ASSETS.IMG_BUFF_JINBI, 1, setgroup);
            this.openORclose_music = new MyImage((int) PAK_ASSETS.IMG_SHEZHI005, (float) PAK_ASSETS.IMG_SMOKE1, (float) PAK_ASSETS.IMG_DAJI07, 1);
        } else {
            this.music = new ActorButton((int) PAK_ASSETS.IMG_SHEZHI003, Constant.S_C, (int) PAK_ASSETS.IMG_JIESUAN009, (int) PAK_ASSETS.IMG_BUFF_JINBI, 1, setgroup);
            this.openORclose_music = new MyImage((int) PAK_ASSETS.IMG_SHEZHI004, (float) PAK_ASSETS.IMG_SMOKE1, (float) PAK_ASSETS.IMG_DAJI07, 1);
        }
        if (GameSound.isSoundOpen) {
            this.sound = new ActorButton((int) PAK_ASSETS.IMG_SHEZHI002, Constant.S_D, 320, (int) PAK_ASSETS.IMG_005, 1, setgroup);
            this.openORclose_sound = new MyImage((int) PAK_ASSETS.IMG_SHEZHI005, (float) PAK_ASSETS.IMG_SMOKE1, (float) PAK_ASSETS.IMG_KJKWL03, 1);
        } else {
            this.sound = new ActorButton((int) PAK_ASSETS.IMG_SHEZHI002, Constant.S_D, (int) PAK_ASSETS.IMG_JIESUAN009, (int) PAK_ASSETS.IMG_005, 1, setgroup);
            this.openORclose_sound = new MyImage((int) PAK_ASSETS.IMG_SHEZHI004, (float) PAK_ASSETS.IMG_SMOKE1, (float) PAK_ASSETS.IMG_KJKWL03, 1);
        }
        this.reset.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                new MyResetRunk(1, 1);
            }
        });
        setgroup.addActor(this.openORclose_music);
        setgroup.addActor(this.openORclose_sound);
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        this.close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_C01, 160, 12, setgroup);
    }

    /* access modifiers changed from: package-private */
    public void setListener() {
        setgroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                boolean z = false;
                boolean z2 = true;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 1000;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MySetting.this.free();
                            break;
                        case 2:
                            if (!GameSound.isMusicOpen) {
                                MySetting.this.openORclose_music.setTextureRegion((int) PAK_ASSETS.IMG_SHEZHI005);
                                MySetting.this.openORclose_music.setPosition((float) (MySetting.this.bgX + 25), (float) (MySetting.this.bgY + 124));
                                MySetting.this.music.setPosition(300.0f, (float) ((MySetting.this.bgY + 110) - 22));
                                if (!GameSound.isMusicOpen) {
                                    z = true;
                                }
                                GameSound.isMusicOpen = z;
                                GameSound.playMusic(1);
                                break;
                            } else {
                                MySetting.this.openORclose_music.setTextureRegion((int) PAK_ASSETS.IMG_SHEZHI004);
                                MySetting.this.openORclose_music.setPosition((float) (MySetting.this.bgX + 25), (float) (MySetting.this.bgY + 124));
                                MySetting.this.music.setPosition(420.0f, (float) ((MySetting.this.bgY + 110) - 22));
                                GameSound.stopAllMusic();
                                if (GameSound.isMusicOpen) {
                                    z2 = false;
                                }
                                GameSound.isMusicOpen = z2;
                                break;
                            }
                        case 3:
                            if (GameSound.isSoundOpen) {
                                MySetting.this.openORclose_sound.setTextureRegion((int) PAK_ASSETS.IMG_SHEZHI004);
                                MySetting.this.openORclose_sound.setPosition((float) (MySetting.this.bgX + 25), (float) (MySetting.this.bgY + 203));
                                MySetting.this.sound.setPosition(420.0f, (float) ((MySetting.this.bgY + 192) - 22));
                            } else {
                                MySetting.this.openORclose_sound.setTextureRegion((int) PAK_ASSETS.IMG_SHEZHI005);
                                MySetting.this.openORclose_sound.setPosition((float) (MySetting.this.bgX + 25), (float) (MySetting.this.bgY + 203));
                                MySetting.this.sound.setPosition(300.0f, (float) ((MySetting.this.bgY + 192) - 22));
                            }
                            if (GameSound.isSoundOpen) {
                                z2 = false;
                            }
                            GameSound.isSoundOpen = z2;
                            break;
                    }
                    super.touchUp(event, x, y, pointer, button);
                }
            }
        });
    }

    public void exit() {
        setgroup.remove();
        setgroup.clear();
    }
}
