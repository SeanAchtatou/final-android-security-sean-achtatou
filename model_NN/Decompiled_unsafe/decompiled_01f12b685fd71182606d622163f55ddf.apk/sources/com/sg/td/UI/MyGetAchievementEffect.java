package com.sg.td.UI;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GSimpleAction;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyGetAchievementEffect extends MyGroup {
    static Group anygroup;
    Effect back;
    Group getupgroup;

    public void init() {
        initbj();
    }

    public MyGetAchievementEffect(int award) {
        initRoleUp(award);
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        Sound.playSound(7);
        this.getupgroup = new Group();
        this.getupgroup.addActor(new Mask());
        GameStage.addActor(this.getupgroup, 4);
    }

    /* access modifiers changed from: package-private */
    public void initRoleUp(int award) {
        int i = Mymainmenu2.selectIndex;
        this.back = new Effect();
        this.back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_2X1, this.getupgroup);
        maimeng(new ActorImage(134, 320, (int) PAK_ASSETS.IMG_2X1, 1, this.getupgroup));
        ActorText atk1 = new ActorText("钻石X" + award, 320, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getupgroup);
        atk1.setWidth(PAK_ASSETS.IMG_G02);
        atk1.setFontScaleXY(1.3f);
        atk1.setPosition((float) 320, (float) PAK_ASSETS.IMG_ZHANDOU031);
        this.getupgroup.addAction(getCountAction(5, this.getupgroup, this.back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    public void maimeng(Actor bj) {
        Action scale = Actions.scaleTo(1.5f, 1.5f, 0.5f, Interpolation.pow4Out);
        Action scale1 = Actions.scaleTo(1.2f, 1.2f, 1.5f, Interpolation.pow5Out);
        Action scale2 = Actions.moveTo(bj.getX(), bj.getY() + 10.0f, 0.7f);
        Action scale3 = Actions.moveTo(bj.getX(), bj.getY(), 0.7f);
        DelayAction delay = Actions.delay(0.5f);
        bj.addAction(Actions.sequence(Actions.sequence(scale, scale1), Actions.repeat(-1, Actions.sequence(scale2, scale3))));
    }

    /* access modifiers changed from: package-private */
    public void touchAnywhere() {
        anygroup = new Group();
        anygroup.addActor(this.getupgroup);
        anygroup.setPosition(5.0f, 5.0f);
        anygroup.setSize(640.0f, 1136.0f);
        GameStage.addActor(anygroup, 4);
        anygroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                GameStage.removeActor(MyGetAchievementEffect.this.getupgroup);
                MyGetAchievementEffect.this.getupgroup.remove();
                MyGetAchievementEffect.this.getupgroup.clear();
                MyGetAchievementEffect.this.back.remove_Diejia();
                MyGetAchievementEffect.anygroup.remove();
                MyGetAchievementEffect.anygroup.clear();
                System.out.println("clear");
            }
        });
    }

    public static Action getCountAction(final int time, final Group group, final Effect back2) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    GameStage.removeActor(group);
                    if (MyGetAchievementEffect.anygroup != null) {
                        MyGetAchievementEffect.anygroup.remove();
                        MyGetAchievementEffect.anygroup.clear();
                    }
                    group.remove();
                    group.clear();
                    back2.remove_Diejia();
                    group.clear();
                }
                return true;
            }
        });
    }

    public void exit() {
    }
}
