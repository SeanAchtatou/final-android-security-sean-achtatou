package com.sg.td.UI;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GSimpleAction;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.MySwitch;
import com.sg.td.record.Get;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyGet extends MyGroup {
    static Group anygroup;
    static Effect back;
    static Effect effectChose;
    static int[] icon_guoqing = {126, 134, 135};
    static int[] icon_jihuoma_0 = {126, 127, 135};
    static int[] icon_jihuoma_1 = {132, 133, 130, 131};
    static int[] icon_jihuoma_2 = {127, 135};
    static int[] icon_shop_0 = {126, 127, 135};
    static int[] icon_shop_1 = {132, 133, 130, 131, 135};
    static int[] icon_shop_2 = {127, 135};
    static int[] icon_shop_newsan = {134, 127, 126, 135};
    static String[] infor0 = {"元素之力X1", "复活心X1", "恐龙币X200"};
    static String[] infor1 = {"元素之力X3", "复活心X2", "恐龙币X1000"};
    static String[] infor2 = {"斗龙眼镜X5", "斗龙手环X5", "零钱罐X5", "弹簧鞋X5", "恐龙币X2000"};
    static String[] infor3 = {"元素之力X6", "复活心X4", "恐龙币X5000"};
    static String[] infor4 = {"元素之力X18", "复活心X11", "恐龙币X10000"};
    static String[] infor5 = {"元素之力X6", "复活心X6", "恐龙币X3000"};
    static String[] infor_newsan0 = {"钻石X1800", "复活心X1", "元素之力X1", "恐龙币X200"};
    static String[] infor_newsan1 = {"钻石X2500", "复活心X3", "元素之力X3", "恐龙币X1000"};
    static String[] infor_newsan2 = {"钻石X4000", "复活心X5", "元素之力X10", "恐龙币X5000"};
    static String[] inforguoqing = {"元素之力X2", "钻石X100", "恐龙币X1000"};
    static String[] jihuoma_infor0 = {"元素之力X1", "复活X1", "恐龙币X200"};
    static String[] jihuoma_infor1 = {"元素之力X6", "复活X6", "恐龙币X3000"};
    static String[] jihuoma_infor2 = {"元素之力X3", "复活X2", "恐龙币X1000"};
    static String[] jihuoma_infor24 = {"斗龙眼镜X5", "斗龙手环X5", "零钱罐X5", "弹簧鞋X5"};
    static String[] jihuoma_infor3 = {"元素之力X6", "复活X4", "恐龙币X5000"};
    static String[] jihuoma_infor4 = {"元素之力X18", "复活X11", "恐龙币X1000"};
    Effect getEffect;
    Group getShopgroup;
    Group getgroup;
    int[] icon_shop_temp;

    public void init() {
    }

    public MyGet(Get get, int day, int id) {
        switch (id) {
            case 0:
                this.getgroup = new Group();
                initbj(id);
                initframe(get, day);
                return;
            case 1:
                this.getShopgroup = new Group();
                initbj(id);
                initframeShop(get, day);
                return;
            case 2:
                this.getgroup = new Group();
                initbj(id);
                initframePuTong(get, day);
                return;
            case 3:
                this.getgroup = new Group();
                initbj(id);
                initframeJihuoma(get, day);
                return;
            case 4:
                this.getgroup = new Group();
                initbj(id);
                initframeguoqing(get, day);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void initbj(int id) {
        Mask mask = new Mask();
        if (id == 0 || id == 2 || id == 3 || id == 4) {
            this.getgroup.addActor(mask);
            return;
        }
        if (this.getShopgroup == null) {
            this.getShopgroup = new Group();
        }
        this.getShopgroup.addActor(mask);
    }

    /* access modifiers changed from: package-private */
    public void initframeguoqing(Get get, int day) {
        this.getEffect = new Effect();
        back = new Effect();
        back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_2X1, this.getgroup);
        for (int i = 0; i < icon_guoqing.length; i++) {
            new ActorImage(icon_guoqing[i], (i * 100) + PAK_ASSETS.IMG_JUXINGNENGLIANGTA + 12, (int) PAK_ASSETS.IMG_2X1, 1, this.getgroup).addAction(Actions.sequence(Actions.scaleTo(0.8f, 0.8f, 0.1f, Interpolation.pow4Out)));
            new ActorText(inforguoqing[i], (i * 105) + PAK_ASSETS.IMG_JUXINGNENGLIANGTA + 12, (int) PAK_ASSETS.IMG_TIP007, 1, this.getgroup).setFontScaleXY(1.0f);
        }
        this.getgroup.addAction(getCountAction(5, this.getgroup, back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    public void initframe(Get get, int day) {
        Sound.playSound(16);
        this.getEffect = new Effect();
        back = new Effect();
        back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_2X1, this.getgroup);
        effectChose = new Effect();
        effectChose.addEffect(81, 320, (int) PAK_ASSETS.IMG_2X1, this.getgroup);
        String ImgName = get.getCurrentName(day);
        String inf = get.getCurrentIfn(day);
        maimeng(new ActorImage(ImgName, 320, (int) PAK_ASSETS.IMG_2X1, 1, this.getgroup));
        ActorText information = new ActorText(inf, 320, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
        information.setWidth(PAK_ASSETS.IMG_G02);
        information.setFontScaleXY(1.3f);
        information.setPosition((float) 320, (float) PAK_ASSETS.IMG_ZHANDOU031);
        this.getgroup.addAction(getCountAction(5, this.getgroup, back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    public void maimeng(Actor bj) {
        Action scale = Actions.scaleTo(1.5f, 1.5f, 0.5f, Interpolation.pow4Out);
        Action scale1 = Actions.scaleTo(1.2f, 1.2f, 1.5f, Interpolation.pow5Out);
        Action scale2 = Actions.moveTo(bj.getX(), bj.getY() + 10.0f, 0.7f);
        Action scale3 = Actions.moveTo(bj.getX(), bj.getY(), 0.7f);
        DelayAction delay = Actions.delay(0.5f);
        bj.addAction(Actions.sequence(Actions.sequence(scale, scale1), Actions.repeat(-1, Actions.sequence(scale2, scale3))));
    }

    /* access modifiers changed from: package-private */
    public void initframePuTong(Get get, int day) {
        this.getEffect = new Effect();
        back = new Effect();
        back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_2X1, this.getgroup);
        System.out.println("day:" + day);
        String ImgName = get.getCurrentName(day);
        System.out.println("ImgName:" + ImgName);
        String inf = get.getCurrentIfn(day);
        ActorImage bj = new ActorImage(ImgName, 320, (int) PAK_ASSETS.IMG_2X1, 1, this.getgroup);
        bj.addAction(Actions.sequence(Actions.scaleTo(1.5f, 1.5f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
        maimeng(bj);
        ActorText information = new ActorText(inf, 320, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
        information.setWidth(PAK_ASSETS.IMG_G02);
        information.setFontScaleXY(1.3f);
        information.setPosition((float) 320, (float) PAK_ASSETS.IMG_ZHANDOU031);
        this.getgroup.addAction(getCountAction(5, this.getgroup, back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x02e7  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x033a  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x038d  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x03e0  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0433  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0486  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initframeShop(com.sg.td.record.Get r27, int r28) {
        /*
            r26 = this;
            java.io.PrintStream r8 = java.lang.System.out
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "day::"
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r28
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r9 = r9.toString()
            r8.println(r9)
            r8 = 18
            r0 = r28
            if (r0 == r8) goto L_0x002c
            r8 = 19
            r0 = r28
            if (r0 == r8) goto L_0x002c
            r8 = 20
            r0 = r28
            if (r0 != r8) goto L_0x0160
        L_0x002c:
            java.io.PrintStream r8 = java.lang.System.out
            java.lang.String r9 = "播放钻石"
            r8.println(r9)
            r8 = 7
            com.sg.td.Sound.playSound(r8)
        L_0x0037:
            r4 = 320(0x140, float:4.48E-43)
            r25 = 568(0x238, float:7.96E-43)
            com.sg.td.actor.Effect r8 = new com.sg.td.actor.Effect
            r8.<init>()
            r0 = r26
            r0.getEffect = r8
            com.sg.td.actor.Effect r8 = new com.sg.td.actor.Effect
            r8.<init>()
            com.sg.td.UI.MyGet.back = r8
            r16 = 0
            r3 = 0
            r19 = 0
            r8 = 22
            r0 = r28
            if (r0 >= r8) goto L_0x005e
            java.lang.String r3 = r27.getCurrentName(r28)
            java.lang.String r19 = r27.getCurrentIfn(r28)
        L_0x005e:
            r22 = 220(0xdc, float:3.08E-43)
            r24 = 200(0xc8, float:2.8E-43)
            r23 = 15
            r8 = 6
            r0 = r28
            if (r0 < r8) goto L_0x0187
            r8 = 21
            r0 = r28
            if (r0 == r8) goto L_0x0187
            r8 = 22
            r0 = r28
            if (r0 >= r8) goto L_0x0187
            com.sg.td.actor.Effect r8 = com.sg.td.UI.MyGet.back
            r9 = 58
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            r0 = r25
            r8.addEffect_Diejia(r9, r4, r0, r10)
            com.kbz.Actors.ActorImage r2 = new com.kbz.Actors.ActorImage
            r5 = 558(0x22e, float:7.82E-43)
            r6 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r7 = r0.getShopgroup
            r2.<init>(r3, r4, r5, r6, r7)
            r8 = 1065353216(0x3f800000, float:1.0)
            r9 = 1065353216(0x3f800000, float:1.0)
            r10 = 1056964608(0x3f000000, float:0.5)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r2.addAction(r8)
            r0 = r26
            r0.maimeng(r2)
            com.kbz.Actors.ActorText r5 = new com.kbz.Actors.ActorText
            r8 = 658(0x292, float:9.22E-43)
            r9 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            r6 = r19
            r7 = r4
            r5.<init>(r6, r7, r8, r9, r10)
            r8 = 600(0x258, float:8.41E-43)
            r5.setWidth(r8)
            r8 = 1067869798(0x3fa66666, float:1.3)
            r5.setFontScaleXY(r8)
            float r8 = (float) r4
            r9 = 658(0x292, float:9.22E-43)
            float r9 = (float) r9
            r5.setPosition(r8, r9)
        L_0x00c7:
            r8 = 21
            r0 = r28
            if (r0 != r8) goto L_0x012b
            com.sg.td.actor.Effect r8 = com.sg.td.UI.MyGet.back
            r9 = 58
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            r0 = r25
            r8.addEffect_Diejia(r9, r4, r0, r10)
            com.sg.td.spine.RoleSpine r20 = new com.sg.td.spine.RoleSpine
            r8 = 19
            r0 = r20
            r1 = r25
            r0.<init>(r4, r1, r8)
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r8 = r0.getShopgroup
            r0 = r20
            r8.addActor(r0)
            r8 = 1065353216(0x3f800000, float:1.0)
            r9 = 1065353216(0x3f800000, float:1.0)
            r10 = 1056964608(0x3f000000, float:0.5)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r0 = r20
            r0.addAction(r8)
            r0 = r26
            r1 = r20
            r0.maimeng(r1)
            com.kbz.Actors.ActorText r5 = new com.kbz.Actors.ActorText
            java.lang.String r6 = "获得铠甲雷古曼"
            r8 = 658(0x292, float:9.22E-43)
            r9 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            r7 = r4
            r5.<init>(r6, r7, r8, r9, r10)
            r8 = 600(0x258, float:8.41E-43)
            r5.setWidth(r8)
            r8 = 1067869798(0x3fa66666, float:1.3)
            r5.setFontScaleXY(r8)
            float r8 = (float) r4
            r9 = 658(0x292, float:9.22E-43)
            float r9 = (float) r9
            r5.setPosition(r8, r9)
        L_0x012b:
            r8 = 21
            r0 = r28
            if (r0 <= r8) goto L_0x0138
            r15 = 30
            r14 = 12
            switch(r28) {
                case 22: goto L_0x01ea;
                case 23: goto L_0x023d;
                case 24: goto L_0x0292;
                default: goto L_0x0138;
            }
        L_0x0138:
            r6 = r16
        L_0x013a:
            r18 = 0
            r15 = 30
            r14 = 12
            if (r28 < 0) goto L_0x014a
            r8 = 6
            r0 = r28
            if (r0 >= r8) goto L_0x014a
            switch(r28) {
                case 0: goto L_0x02e7;
                case 1: goto L_0x033a;
                case 2: goto L_0x0486;
                case 3: goto L_0x038d;
                case 4: goto L_0x03e0;
                case 5: goto L_0x0433;
                default: goto L_0x014a;
            }
        L_0x014a:
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r8 = r0.getShopgroup
            r9 = 5
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            com.sg.td.actor.Effect r11 = com.sg.td.UI.MyGet.back
            com.badlogic.gdx.scenes.scene2d.Action r9 = getCountAction(r9, r10, r11)
            r8.addAction(r9)
            r26.touchAnywhere1()
            return
        L_0x0160:
            r8 = 15
            r0 = r28
            if (r0 == r8) goto L_0x0172
            r8 = 16
            r0 = r28
            if (r0 == r8) goto L_0x0172
            r8 = 17
            r0 = r28
            if (r0 != r8) goto L_0x0180
        L_0x0172:
            java.io.PrintStream r8 = java.lang.System.out
            java.lang.String r9 = "播放钱"
            r8.println(r9)
            r8 = 9
            com.sg.td.Sound.playSound(r8)
            goto L_0x0037
        L_0x0180:
            r8 = 8
            com.sg.td.Sound.playSound(r8)
            goto L_0x0037
        L_0x0187:
            r8 = 21
            r0 = r28
            if (r0 == r8) goto L_0x00c7
            r8 = 22
            r0 = r28
            if (r0 >= r8) goto L_0x00c7
            com.sg.td.actor.Effect r8 = com.sg.td.UI.MyGet.back
            r9 = 58
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            r0 = r25
            r8.addEffect_Diejia(r9, r4, r0, r10)
            com.kbz.Actors.ActorImage r2 = new com.kbz.Actors.ActorImage
            r9 = 558(0x22e, float:7.82E-43)
            r10 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r11 = r0.getShopgroup
            r6 = r2
            r7 = r3
            r8 = r4
            r6.<init>(r7, r8, r9, r10, r11)
            r8 = 1065353216(0x3f800000, float:1.0)
            r9 = 1065353216(0x3f800000, float:1.0)
            r10 = 1056964608(0x3f000000, float:0.5)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r2.addAction(r8)
            r0 = r26
            r0.maimeng(r2)
            com.kbz.Actors.ActorText r5 = new com.kbz.Actors.ActorText
            r8 = 628(0x274, float:8.8E-43)
            r9 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r10 = r0.getShopgroup
            r6 = r19
            r7 = r4
            r5.<init>(r6, r7, r8, r9, r10)
            r8 = 600(0x258, float:8.41E-43)
            r5.setWidth(r8)
            r8 = 1067869798(0x3fa66666, float:1.3)
            r5.setFontScaleXY(r8)
            float r8 = (float) r4
            r9 = 658(0x292, float:9.22E-43)
            float r9 = (float) r9
            r5.setPosition(r8, r9)
            goto L_0x00c7
        L_0x01ea:
            r17 = 0
            r6 = r16
        L_0x01ee:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_newsan
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x013a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_newsan
            r7 = r8[r17]
            int r8 = r17 * 110
            int r8 = r8 + 120
            int r8 = r8 + r14
            r9 = 678(0x2a6, float:9.5E-43)
            r10 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r11 = r0.getShopgroup
            r6.<init>(r7, r8, r9, r10, r11)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor_newsan0
            r8 = r8[r17]
            int r9 = r17 * 115
            int r9 = r9 + 120
            int r9 = r9 + r14
            r10 = 723(0x2d3, float:1.013E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x01ee
        L_0x023d:
            r17 = 0
            r6 = r16
        L_0x0241:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_newsan
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x013a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_newsan
            r9 = r8[r17]
            int r8 = r17 * 110
            int r8 = r8 + 120
            int r10 = r8 + r14
            r11 = 678(0x2a6, float:9.5E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor_newsan1
            r8 = r8[r17]
            int r9 = r17 * 115
            int r9 = r9 + 120
            int r9 = r9 + r14
            r10 = 723(0x2d3, float:1.013E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x0241
        L_0x0292:
            r17 = 0
            r6 = r16
        L_0x0296:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_newsan
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x013a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_newsan
            r9 = r8[r17]
            int r8 = r17 * 110
            int r8 = r8 + 120
            int r10 = r8 + r14
            r11 = 678(0x2a6, float:9.5E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor_newsan2
            r8 = r8[r17]
            int r9 = r17 * 120
            int r9 = r9 + 120
            int r9 = r9 + r14
            r10 = 723(0x2d3, float:1.013E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x0296
        L_0x02e7:
            r17 = 0
        L_0x02e9:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x014a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            r9 = r8[r17]
            int r8 = r17 * 100
            int r8 = r8 + r22
            int r10 = r8 + r14
            r11 = 733(0x2dd, float:1.027E-42)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor0
            r8 = r8[r17]
            int r9 = r17 * 105
            int r9 = r9 + r22
            int r9 = r9 + r14
            r10 = 778(0x30a, float:1.09E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x02e9
        L_0x033a:
            r17 = 0
        L_0x033c:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x014a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            r9 = r8[r17]
            int r8 = r17 * 100
            int r8 = r8 + r22
            int r10 = r8 + r14
            r11 = 713(0x2c9, float:9.99E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor1
            r8 = r8[r17]
            int r9 = r17 * 105
            int r9 = r9 + r22
            int r9 = r9 + r14
            r10 = 778(0x30a, float:1.09E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x033c
        L_0x038d:
            r17 = 0
        L_0x038f:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x014a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            r9 = r8[r17]
            int r8 = r17 * 100
            int r8 = r8 + r22
            int r10 = r8 + r14
            r11 = 713(0x2c9, float:9.99E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor3
            r8 = r8[r17]
            int r9 = r17 * 105
            int r9 = r9 + r22
            int r9 = r9 + r14
            r10 = 778(0x30a, float:1.09E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x038f
        L_0x03e0:
            r17 = 0
        L_0x03e2:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x014a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            r9 = r8[r17]
            int r8 = r17 * 100
            int r8 = r8 + r22
            int r10 = r8 + r14
            r11 = 713(0x2c9, float:9.99E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor4
            r8 = r8[r17]
            int r9 = r17 * 120
            int r9 = r9 + r22
            int r9 = r9 + r14
            r10 = 778(0x30a, float:1.09E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x03e2
        L_0x0433:
            r17 = 0
        L_0x0435:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x014a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_0
            r9 = r8[r17]
            int r8 = r17 * 100
            int r8 = r8 + r22
            int r10 = r8 + r14
            r11 = 713(0x2c9, float:9.99E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor5
            r8 = r8[r17]
            int r9 = r17 * 105
            int r9 = r9 + r22
            int r9 = r9 + r14
            r10 = 778(0x30a, float:1.09E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            int r17 = r17 + 1
            goto L_0x0435
        L_0x0486:
            r17 = 0
        L_0x0488:
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_1
            int r8 = r8.length
            r0 = r17
            if (r0 >= r8) goto L_0x014a
            com.kbz.Actors.ActorImage r6 = new com.kbz.Actors.ActorImage
            int[] r8 = com.sg.td.UI.MyGet.icon_shop_1
            r9 = r8[r17]
            int r8 = r17 * 100
            int r8 = r8 + r24
            int r10 = r8 + r14
            r11 = 713(0x2c9, float:9.99E-43)
            r12 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r13 = r0.getShopgroup
            r8 = r6
            r8.<init>(r9, r10, r11, r12, r13)
            r8 = 1061997773(0x3f4ccccd, float:0.8)
            r9 = 1061997773(0x3f4ccccd, float:0.8)
            r10 = 1036831949(0x3dcccccd, float:0.1)
            com.badlogic.gdx.math.Interpolation$PowOut r11 = com.badlogic.gdx.math.Interpolation.pow4Out
            com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction r21 = com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo(r8, r9, r10, r11)
            com.badlogic.gdx.scenes.scene2d.actions.SequenceAction r8 = com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence(r21)
            r6.addAction(r8)
            com.kbz.Actors.ActorText r7 = new com.kbz.Actors.ActorText
            java.lang.String[] r8 = com.sg.td.UI.MyGet.infor2
            r8 = r8[r17]
            int r9 = r17 * 100
            int r9 = r9 + r24
            int r9 = r9 + r14
            r10 = 768(0x300, float:1.076E-42)
            r11 = 1
            r0 = r26
            com.badlogic.gdx.scenes.scene2d.Group r12 = r0.getShopgroup
            r7.<init>(r8, r9, r10, r11, r12)
            r8 = 1065353216(0x3f800000, float:1.0)
            r7.setFontScaleXY(r8)
            r8 = 3
            r0 = r17
            if (r0 != r8) goto L_0x04ed
            r0 = r24
            float r8 = (float) r0
            r9 = 778(0x30a, float:1.09E-42)
            float r9 = (float) r9
            r6.setPosition(r8, r9)
            r8 = 240(0xf0, float:3.36E-43)
            float r8 = (float) r8
            r9 = 895(0x37f, float:1.254E-42)
            float r9 = (float) r9
            r7.setPosition(r8, r9)
        L_0x04ed:
            r8 = 4
            r0 = r17
            if (r0 != r8) goto L_0x0504
            r8 = 300(0x12c, float:4.2E-43)
            float r8 = (float) r8
            r9 = 795(0x31b, float:1.114E-42)
            float r9 = (float) r9
            r6.setPosition(r8, r9)
            r8 = 350(0x15e, float:4.9E-43)
            float r8 = (float) r8
            r9 = 895(0x37f, float:1.254E-42)
            float r9 = (float) r9
            r7.setPosition(r8, r9)
        L_0x0504:
            int r17 = r17 + 1
            goto L_0x0488
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sg.td.UI.MyGet.initframeShop(com.sg.td.record.Get, int):void");
    }

    /* access modifiers changed from: package-private */
    public void initframeJihuoma(Get get, int day) {
        this.getEffect = new Effect();
        back = new Effect();
        back.addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_2X1, this.getgroup);
        System.out.println("day:" + day);
        String ImgName = get.getCurrentName(day);
        System.out.println("ImgName:" + ImgName);
        String inf = get.getCurrentIfn(day);
        if ((day >= 6 && day <= 11) || day == 24) {
            switch (day) {
                case 6:
                    for (int i = 0; i < icon_jihuoma_0.length; i++) {
                        ActorImage bj = new ActorImage(icon_jihuoma_0[i], (i * 140) + 167 + 12, (int) PAK_ASSETS.IMG_PUBLIC002, 1, this.getgroup);
                        bj.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
                        maimeng(bj);
                        ActorText information = new ActorText(jihuoma_infor0[i], (i * 150) - 73, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
                        if (i == 1) {
                            information.setPosition((float) ((i * 150) - 95), (float) PAK_ASSETS.IMG_ZHANDOU031);
                        }
                        information.setWidth(PAK_ASSETS.IMG_G02);
                        information.setFontScaleXY(1.3f);
                    }
                    break;
                case 7:
                    for (int i2 = 0; i2 < icon_jihuoma_0.length; i2++) {
                        ActorImage actorImage = new ActorImage(icon_jihuoma_0[i2], (i2 * 140) + 167 + 12, (int) PAK_ASSETS.IMG_PUBLIC002, 1, this.getgroup);
                        actorImage.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
                        maimeng(actorImage);
                        ActorText information2 = new ActorText(jihuoma_infor1[i2], (i2 * 150) - 73, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
                        if (i2 == 1) {
                            information2.setPosition((float) ((i2 * 150) - 95), (float) PAK_ASSETS.IMG_ZHANDOU031);
                        }
                        information2.setWidth(PAK_ASSETS.IMG_G02);
                        information2.setFontScaleXY(1.3f);
                    }
                    break;
                case 8:
                    for (int i3 = 0; i3 < icon_jihuoma_0.length; i3++) {
                        ActorImage actorImage2 = new ActorImage(icon_jihuoma_0[i3], (i3 * 140) + 167 + 12, (int) PAK_ASSETS.IMG_PUBLIC002, 1, this.getgroup);
                        actorImage2.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
                        maimeng(actorImage2);
                        ActorText information3 = new ActorText(jihuoma_infor2[i3], (i3 * 150) - 73, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
                        if (i3 == 1) {
                            information3.setPosition((float) ((i3 * 150) - 95), (float) PAK_ASSETS.IMG_ZHANDOU031);
                        }
                        information3.setWidth(PAK_ASSETS.IMG_G02);
                        information3.setFontScaleXY(1.3f);
                    }
                    break;
                case 9:
                    for (int i4 = 0; i4 < icon_jihuoma_0.length; i4++) {
                        ActorImage actorImage3 = new ActorImage(icon_jihuoma_0[i4], (i4 * 140) + 167 + 12, (int) PAK_ASSETS.IMG_PUBLIC002, 1, this.getgroup);
                        actorImage3.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
                        maimeng(actorImage3);
                        ActorText information4 = new ActorText(jihuoma_infor3[i4], (i4 * 150) - 73, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
                        if (i4 == 1) {
                            information4.setPosition((float) ((i4 * 150) - 95), (float) PAK_ASSETS.IMG_ZHANDOU031);
                        }
                        information4.setWidth(PAK_ASSETS.IMG_G02);
                        information4.setFontScaleXY(1.3f);
                    }
                    break;
                case 10:
                    for (int i5 = 0; i5 < icon_jihuoma_0.length; i5++) {
                        ActorImage actorImage4 = new ActorImage(icon_jihuoma_0[i5], (i5 * 140) + 167 + 12, (int) PAK_ASSETS.IMG_PUBLIC002, 1, this.getgroup);
                        actorImage4.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
                        maimeng(actorImage4);
                        ActorText information5 = new ActorText(jihuoma_infor4[i5], (i5 * 150) - 73, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
                        if (i5 == 1) {
                            information5.setPosition((float) ((i5 * 150) - 95), (float) PAK_ASSETS.IMG_ZHANDOU031);
                        }
                        information5.setWidth(PAK_ASSETS.IMG_G02);
                        information5.setFontScaleXY(1.3f);
                    }
                    break;
                case 24:
                    for (int i6 = 0; i6 < icon_jihuoma_1.length; i6++) {
                        ActorImage actorImage5 = new ActorImage(icon_jihuoma_1[i6], (i6 * 140) + 99 + 12, (int) PAK_ASSETS.IMG_PUBLIC002, 1, this.getgroup);
                        actorImage5.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
                        maimeng(actorImage5);
                        ActorText information6 = new ActorText(jihuoma_infor24[i6], (i6 * 150) - 141, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
                        information6.setWidth(PAK_ASSETS.IMG_G02);
                        information6.setFontScaleXY(1.3f);
                    }
                    break;
            }
        } else {
            ActorImage actorImage6 = new ActorImage(ImgName, 320, (int) PAK_ASSETS.IMG_2X1, 1, this.getgroup);
            actorImage6.addAction(Actions.sequence(Actions.scaleTo(1.5f, 1.5f, 0.5f, Interpolation.pow4Out), Actions.rotateBy(0.2f, 0.5f, Interpolation.pow4Out)));
            maimeng(actorImage6);
            ActorText actorText = new ActorText(inf, 320, (int) PAK_ASSETS.IMG_ZHANDOU031, 1, this.getgroup);
            actorText.setWidth(900);
            actorText.setFontScaleXY(1.3f);
            actorText.setPosition((float) 320, (float) PAK_ASSETS.IMG_ZHANDOU031);
        }
        this.getgroup.addAction(getCountAction(5, this.getgroup, back));
        touchAnywhere();
    }

    /* access modifiers changed from: package-private */
    public void touchAnywhere() {
        anygroup = new Group();
        anygroup.addActor(this.getgroup);
        anygroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        anygroup.setSize(640.0f, 1136.0f);
        GameStage.addActor(anygroup, 4);
        anygroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                GameStage.removeActor(MyGet.this.getgroup);
                MyGet.this.getgroup.remove();
                MyGet.this.getgroup.clear();
                MyGet.back.remove_Diejia();
                MyGet.anygroup.remove();
                MyGet.anygroup.clear();
                System.out.println("clear");
                if (MyGet.effectChose != null) {
                    MyGet.effectChose.remove_Diejia();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void touchAnywhere1() {
        anygroup = new Group();
        anygroup.addActor(this.getShopgroup);
        anygroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        anygroup.setSize(640.0f, 1136.0f);
        GameStage.addActor(anygroup, 4);
        anygroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (MySwitch.isKJLGM) {
                    MySwitch.isKJLGM = false;
                    MyUpgradeNoOPen.zhaohuan();
                }
                MyGet.this.getShopgroup.remove();
                MyGet.this.getShopgroup.clear();
                MyGet.back.remove_Diejia();
                MyGet.anygroup.remove();
                System.out.println("clearshop");
                if (MyGet.effectChose != null) {
                    MyGet.effectChose.remove_Diejia();
                }
                MyGet.anygroup.clear();
            }
        });
    }

    public static Action getCountAction(final int time, final Group group, final Effect back2) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    GameStage.removeActor(group);
                    group.remove();
                    group.clear();
                    back2.remove_Diejia();
                    group.clear();
                    if (MyGet.anygroup != null) {
                        MyGet.anygroup.remove();
                        MyGet.anygroup.clear();
                    }
                    if (MySwitch.isKJLGM) {
                        MySwitch.isKJLGM = false;
                        MyUpgradeNoOPen.zhaohuan();
                    }
                }
                if (MyGet.effectChose != null) {
                    MyGet.effectChose.remove_Diejia();
                }
                return true;
            }
        });
    }

    public static Action getCountActor(final int time, final Group group, Actor actor) {
        System.out.println("clean1");
        return GSimpleAction.simpleAction(new GSimpleAction.GActInterface() {
            int repeat = (time * 100);

            public boolean run(float delta, Actor actor) {
                this.repeat -= 2;
                if (this.repeat / 100 != 0) {
                    return false;
                }
                if (group != null) {
                    GameStage.removeActor(group);
                    actor.remove();
                    actor.clear();
                    group.remove();
                    group.clear();
                }
                return true;
            }
        });
    }

    public void exit() {
        this.getgroup.remove();
        this.getgroup.clear();
        this.getShopgroup.remove();
        this.getShopgroup.clear();
    }
}
