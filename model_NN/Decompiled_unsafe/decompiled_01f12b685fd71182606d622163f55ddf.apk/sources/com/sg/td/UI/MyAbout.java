package com.sg.td.UI;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.td.message.MyUIGetFont;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyAbout extends MyGroup {
    static Group aboutgroud;

    public void init() {
    }

    public static void guanyu(boolean iskefu) {
        aboutgroud = new Group();
        aboutgroud.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(aboutgroud, 4);
        aboutgroud.addActor(new Mask());
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 5, aboutgroud, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_SHUOMINGZI01, 1, aboutgroud);
        new ActorImage(iskefu ? 183 : 182, 320, (int) PAK_ASSETS.IMG_HUOCHAI_SHUOMING, 1, aboutgroud);
        ActorButton close = new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_C01, (int) PAK_ASSETS.IMG_PENSHEQI_SHUOMING, 12, aboutgroud);
        Group group = new Group();
        group.setPosition(Animation.CurveTimeline.LINEAR, 60.0f);
        SimpleButton simpleButton = new SimpleButton(50, PAK_ASSETS.IMG_K02A, 1, new int[]{184, 186});
        SimpleButton simpleButton2 = new SimpleButton(PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_K02A, 1, new int[]{185, 187});
        aboutgroud.addActor(simpleButton);
        aboutgroud.addActor(simpleButton2);
        ActorText gameName = new ActorText(MySwitch.gameName, 60, (int) PAK_ASSETS.IMG_BUFF_SUDU, 12, group);
        ActorText ServiceTitle = new ActorText(MySwitch.customerServiceTitle, 60, (int) PAK_ASSETS.IMG_SHENGJITIPS02, 12, group);
        ActorText ServiceTelephone = new ActorText(MySwitch.customerServiceTelephone, 60, 410, 12, group);
        ActorText ServiceEmail = new ActorText(MySwitch.customerServiceEmail, 60, (int) PAK_ASSETS.IMG_JIESUAN007, 12, group);
        ActorText ServiceSms = new ActorText(MySwitch.customerServiceSms, 60, (int) PAK_ASSETS.IMG_QIANDAO009, 12, group);
        gameName.setWidth(PAK_ASSETS.IMG_G02);
        ServiceTitle.setWidth(PAK_ASSETS.IMG_ONLINE003);
        ServiceTelephone.setWidth(640);
        ServiceEmail.setWidth(640);
        ServiceSms.setWidth(640);
        gameName.setFontScaleXY(1.3f);
        gameName.setColor(MyUIGetFont.maintopdataColor);
        ServiceTelephone.setColor(MyUIGetFont.maintopdataColor);
        if (MySwitch.isDx) {
            group.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            new ActorText(MySwitch.egameAbout, 60, (int) PAK_ASSETS.IMG_QIANDAO011, 12, group).setWidth(PAK_ASSETS.IMG_ONLINE003);
        }
        simpleButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                GameMain.dialog.showAD(0);
            }
        });
        final boolean z = iskefu;
        simpleButton2.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                if (z) {
                    Action move = Actions.moveBy(540.0f, 448.0f, 0.5f, Interpolation.pow3Out);
                    Action scale = Actions.scaleTo(0.1f, 0.1f, 0.3f, Interpolation.pow3Out);
                    Action cleanAction = Actions.run(new Runnable() {
                        public void run() {
                            MyAbout.aboutgroud.remove();
                            MyAbout.aboutgroud.clear();
                            MyAbout.aboutgroud = null;
                        }
                    });
                    MyAbout.aboutgroud.setOrigin(GameMain.sceenWidth / 2.0f, GameMain.sceenWidth / 2.0f);
                    MyAbout.aboutgroud.addAction(Actions.sequence(scale, move, cleanAction));
                    return;
                }
                MyAbout.aboutgroud.clear();
                MyAbout.aboutgroud.remove();
            }
        });
        final boolean z2 = iskefu;
        close.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonClosed();
                if (z2) {
                    Action move = Actions.moveBy(540.0f, 448.0f, 0.5f, Interpolation.pow3Out);
                    Action scale = Actions.scaleTo(0.1f, 0.1f, 0.3f, Interpolation.pow3Out);
                    Action cleanAction = Actions.run(new Runnable() {
                        public void run() {
                            MyAbout.aboutgroud.remove();
                            MyAbout.aboutgroud.clear();
                            MyAbout.aboutgroud = null;
                        }
                    });
                    MyAbout.aboutgroud.setOrigin(GameMain.sceenWidth / 2.0f, GameMain.sceenWidth / 2.0f);
                    MyAbout.aboutgroud.addAction(Actions.sequence(scale, move, cleanAction));
                    return;
                }
                MyAbout.aboutgroud.clear();
                MyAbout.aboutgroud.remove();
            }
        });
        aboutgroud.addActor(group);
    }

    public void exit() {
    }
}
