package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyNotice extends MyGroup {
    ActorShapeSprite gShapeSprite;
    int noticeId;
    Group noticegroup;
    ActorSprite point_1;
    ActorSprite point_2;
    ActorSprite point_3;
    ActorImage title_3;

    public void init() {
        this.noticegroup = new Group();
        this.noticegroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.noticegroup, 4);
        this.noticegroup.addActor(new Mask());
        initbj();
        initpoint();
        initbutton();
        if (MySwitch.NoticeNum > 0) {
            noticelistener();
        }
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 8, this.noticegroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_SHUOMINGZI01, 1, this.noticegroup);
        new ActorImage(517, 320, (int) PAK_ASSETS.IMG_HUOCHAI_SHUOMING, 1, this.noticegroup);
        this.title_3 = new ActorImage(518, 320, (int) PAK_ASSETS.IMG_PAO002, 1, this.noticegroup);
        this.title_3.setTouchable(Touchable.enabled);
    }

    /* access modifiers changed from: package-private */
    public void initpoint() {
        switch (MySwitch.NoticeNum) {
            case 0:
                this.point_1 = new ActorSprite(320, (int) PAK_ASSETS.IMG_TIP016, 4, PAK_ASSETS.IMG_GONGGAO005, PAK_ASSETS.IMG_GONGGAO006);
                this.noticegroup.addActor(this.point_1);
                return;
            case 1:
                this.point_1 = new ActorSprite((int) PAK_ASSETS.IMG_ICE70, (int) PAK_ASSETS.IMG_TIP016, 4, PAK_ASSETS.IMG_GONGGAO005, PAK_ASSETS.IMG_GONGGAO006);
                this.point_2 = new ActorSprite((int) PAK_ASSETS.IMG_PPSG04, (int) PAK_ASSETS.IMG_TIP016, 4, PAK_ASSETS.IMG_GONGGAO005, PAK_ASSETS.IMG_GONGGAO006);
                this.point_2.setTexture(1);
                this.noticegroup.addActor(this.point_1);
                this.noticegroup.addActor(this.point_2);
                return;
            case 2:
                this.point_1 = new ActorSprite(253, (int) PAK_ASSETS.IMG_TIP016, 4, PAK_ASSETS.IMG_GONGGAO005, PAK_ASSETS.IMG_GONGGAO006);
                this.point_2 = new ActorSprite(303, (int) PAK_ASSETS.IMG_TIP016, 4, PAK_ASSETS.IMG_GONGGAO005, PAK_ASSETS.IMG_GONGGAO006);
                this.point_3 = new ActorSprite((int) PAK_ASSETS.IMG_TX_JIANSU01, (int) PAK_ASSETS.IMG_TIP016, 4, PAK_ASSETS.IMG_GONGGAO005, PAK_ASSETS.IMG_GONGGAO006);
                this.point_2.setTexture(1);
                this.point_3.setTexture(1);
                this.noticegroup.addActor(this.point_1);
                this.noticegroup.addActor(this.point_2);
                this.noticegroup.addActor(this.point_3);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void noticelistener() {
        this.title_3.addListener(new ActorGestureListener() {
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                super.pan(event, x, y, deltaX, deltaY);
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }

            public void fling(InputEvent event, float velocityX, float velocityY, int button) {
                if (velocityX > Animation.CurveTimeline.LINEAR) {
                    MyNotice.this.getNoteceID(true);
                } else {
                    MyNotice.this.getNoteceID(false);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void getNoteceID(boolean isLeft) {
        switch (MySwitch.NoticeNum) {
            case 1:
                if (isLeft) {
                    this.noticeId--;
                    return;
                } else {
                    this.noticeId++;
                    return;
                }
            case 2:
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.noticegroup);
        SimpleButton know = new SimpleButton(185, PAK_ASSETS.IMG_CHONGZHI006, 1, new int[]{519, PAK_ASSETS.IMG_GONGGAO004, 519});
        know.setName("1");
        this.noticegroup.addActor(know);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.noticegroup.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MyNotice.this.free();
                            break;
                        case 1:
                            Sound.playButtonPressed();
                            MyNotice.this.free();
                            break;
                    }
                    super.clicked(event, x, y);
                }
            }
        });
    }

    public void exit() {
        this.noticegroup.remove();
        this.noticegroup.clear();
    }
}
