package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyStarUnlock extends MyGroup {
    Group unlockgroup;

    public void init() {
        this.unlockgroup = new Group();
        this.unlockgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.unlockgroup, 4);
        this.unlockgroup.addActor(new Mask());
        initbj();
    }

    public MyStarUnlock(int rankNum, boolean needStar) {
        if (needStar) {
            initframe(rankNum);
            initData();
            initbutton();
            initlistener();
            return;
        }
        initframe();
        initbutton();
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initframe() {
        SimpleButton tishi = new SimpleButton(179, 408, 1, new int[]{PAK_ASSETS.IMG_JIESUO010, PAK_ASSETS.IMG_JIESUO011});
        tishi.setName(Constant.S_C);
        this.unlockgroup.addActor(tishi);
    }

    /* access modifiers changed from: package-private */
    public void initbj() {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 3, this.unlockgroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, (int) PAK_ASSETS.IMG_SHUOMINGZI01, 1, this.unlockgroup);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUO001, 320, (int) PAK_ASSETS.IMG_HUOCHAI_SHUOMING, 1, this.unlockgroup);
    }

    /* access modifiers changed from: package-private */
    public void initframe(int rankNum) {
        ActorImage starbarhui = new ActorImage((int) PAK_ASSETS.IMG_JIESUO003, 320, (int) PAK_ASSETS.IMG_UI_QIANDAO_BUTTON01, 1, this.unlockgroup);
        ActorClipImage starbaryellow = new ActorClipImage(PAK_ASSETS.IMG_JIESUO004, 120, PAK_ASSETS.IMG_LIGHTD01, 12, this.unlockgroup);
        starbaryellow.setVisible(false);
        starbaryellow.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, starbarhui.getWidth() * (((float) RankData.getStarNum()) / ((float) RankData.getUnlockStarNum(rankNum))), starbaryellow.getHeight());
        if (RankData.getStarNum() != 0) {
            starbaryellow.setVisible(true);
        } else {
            starbaryellow.setVisible(false);
        }
        GNumSprite star = new GNumSprite((int) PAK_ASSETS.IMG_JIESUO006, RankData.getStarNum() + "/" + RankData.getUnlockStarNum(rankNum), "/", -2, 4);
        star.setPosition((float) 320, (float) PAK_ASSETS.IMG_WSPARTICLE_RING09);
        this.unlockgroup.addActor(star);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUO002, 133, (int) PAK_ASSETS.IMG_UI_JINBI01, 1, this.unlockgroup);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUO005, 320, (int) PAK_ASSETS.IMG_JIDI004, 1, this.unlockgroup);
        GNumSprite needstarNum = new GNumSprite((int) PAK_ASSETS.IMG_JIESUO007, RankData.getNeedStarNum(rankNum) + "", "", -2, 4);
        needstarNum.setPosition((float) 260, (float) PAK_ASSETS.IMG_JIDI004);
        this.unlockgroup.addActor(needstarNum);
        SimpleButton simpleButton = new SimpleButton(179, PAK_ASSETS.IMG_MORE001, 1, new int[]{PAK_ASSETS.IMG_JIESUO008, PAK_ASSETS.IMG_JIESUO009});
        simpleButton.setName("1");
        this.unlockgroup.addActor(simpleButton);
    }

    /* access modifiers changed from: package-private */
    public void initData() {
    }

    private void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA, 12, this.unlockgroup);
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.unlockgroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    System.out.println("buttonName:" + buttonName);
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                        System.out.println("codeName:" + codeName);
                    } else {
                        codeName = 9;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MyStarUnlock.this.free();
                            return;
                        case 1:
                            Sound.playButtonClosed();
                            MyStarUnlock.this.free();
                            return;
                        case 2:
                            Sound.playButtonClosed();
                            MyStarUnlock.this.free();
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public void exit() {
        this.unlockgroup.remove();
        this.unlockgroup.clear();
    }
}
