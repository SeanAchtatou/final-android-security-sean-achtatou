package com.sg.td.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.Actors.SimpleButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MyUIGetFont;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyResetRunk extends MyGroup {
    ActorShapeSprite gShapeSprite;
    Group resetgroup;

    public void init() {
        this.resetgroup = new Group();
        this.resetgroup.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        GameStage.addActor(this.resetgroup, 4);
        this.resetgroup.addActor(new Mask());
    }

    public MyResetRunk(int id, int needrnk) {
        if (id == 1) {
            initbj(id);
            initbutton();
            initlistener();
            return;
        }
        initbj(id);
        initbutton1(needrnk);
        initlistener();
    }

    /* access modifiers changed from: package-private */
    public void initbj(int id) {
        GameUITools.GetbackgroundImage(320, PAK_ASSETS.IMG_UI_ZHUANPAN006, 4, this.resetgroup, false, false);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC010, 190, (int) PAK_ASSETS.IMG_UI_ZHUANPAN006, 1, this.resetgroup);
        if (id == 1) {
            new ActorImage((int) PAK_ASSETS.IMG_CHONGZHI001, 320, (int) PAK_ASSETS.IMG_UI002, 1, this.resetgroup);
        } else {
            new ActorImage((int) PAK_ASSETS.IMG_CHONGZHI006, 320, (int) PAK_ASSETS.IMG_UI002, 1, this.resetgroup);
        }
    }

    /* access modifiers changed from: package-private */
    public void initbutton() {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.resetgroup);
        SimpleButton get = new SimpleButton(50, PAK_ASSETS.IMG_PAO002, 1, new int[]{PAK_ASSETS.IMG_CHONGZHI002, PAK_ASSETS.IMG_CHONGZHI003, PAK_ASSETS.IMG_CHONGZHI002});
        get.setName("1");
        this.resetgroup.addActor(get);
        SimpleButton quxiao = new SimpleButton(PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_PAO002, 1, new int[]{PAK_ASSETS.IMG_CHONGZHI004, PAK_ASSETS.IMG_CHONGZHI005, PAK_ASSETS.IMG_CHONGZHI004});
        quxiao.setName(Constant.S_C);
        this.resetgroup.addActor(quxiao);
    }

    /* access modifiers changed from: package-private */
    public void initbutton1(int needrak) {
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_F02, (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 12, this.resetgroup);
        SimpleButton know = new SimpleButton(PAK_ASSETS.IMG_JUXINGNENGLIANGTA, PAK_ASSETS.IMG_PAO002, 1, new int[]{PAK_ASSETS.IMG_CHONGZHI007, PAK_ASSETS.IMG_CHONGZHI008, PAK_ASSETS.IMG_CHONGZHI007});
        know.setName(Constant.S_D);
        GNumSprite bearcoin = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + needrak, "X", -2, 0);
        bearcoin.setPosition(405.0f, 450.0f);
        bearcoin.setScale(1.3f, 1.3f);
        this.resetgroup.addActor(bearcoin);
        bearcoin.setColor(MyUIGetFont.paoColor);
        this.resetgroup.addActor(know);
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        RankData.resetRankData();
        if (!MyGameMap.isInMap) {
            if (RankData.roleLevel[Mymainmenu2.selectIndex] > 0) {
                Mymainmenu2.userChoseIndex = Mymainmenu2.selectIndex;
            } else {
                Mymainmenu2.userChoseIndex = RankData.lastSelectRoleIndex;
            }
        }
        GameStage.clearAllLayers();
        GameMain.toScreen(new MyGameMap());
    }

    /* access modifiers changed from: package-private */
    public void initlistener() {
        this.resetgroup.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                if (event.getPointer() == 0) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            System.out.println("00");
                            MyResetRunk.this.free();
                            return;
                        case 1:
                            Sound.playButtonPressed();
                            MyResetRunk.this.free();
                            MyResetRunk.this.reset();
                            return;
                        case 2:
                            Sound.playButtonClosed();
                            MyResetRunk.this.free();
                            return;
                        case 3:
                            Sound.playButtonClosed();
                            System.out.println("33");
                            MyResetRunk.this.free();
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    public void exit() {
        this.resetgroup.remove();
        this.resetgroup.clear();
    }
}
