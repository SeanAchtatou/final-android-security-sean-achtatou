package com.sg.td.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.datalab.tools.Constant;
import com.kbz.Actors.ActorButton;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorSprite;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.GClipGroup;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;
import com.sg.td.message.GameUITools;
import com.sg.td.message.MySwitch;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class MyShop extends MyGroup {
    static String[] bearcoin1_1 = {"", "", "X2000"};
    static String[] bearcoin_1 = {"X10000", "X5000", "X5", "X1000", "X3000", "X200"};
    static int[] coin1 = {PAK_ASSETS.IMG_SHOP015, PAK_ASSETS.IMG_SHOP016, PAK_ASSETS.IMG_SHOP017};
    static int[] coin2 = {PAK_ASSETS.IMG_SHOP026, PAK_ASSETS.IMG_SHOP025, PAK_ASSETS.IMG_SHOP024};
    static String[] costPrice_1 = {"50", "19", "1000钻", "8", "21", Constant.S_F};
    static int[] decorate1 = {PAK_ASSETS.IMG_SHOP018, PAK_ASSETS.IMG_SHOP019, PAK_ASSETS.IMG_SHOP020};
    static int[] decorate2 = {PAK_ASSETS.IMG_SHOP023, PAK_ASSETS.IMG_SHOP022, PAK_ASSETS.IMG_SHOP021};
    static int[] gift_1 = {137, 138, 139, 136, 140, 137};
    static int[] gift_baioti = {PAK_ASSETS.IMG_SHOP035, PAK_ASSETS.IMG_SHOP034, PAK_ASSETS.IMG_SHOP033, PAK_ASSETS.IMG_SHOP032, PAK_ASSETS.IMG_SHOP031, PAK_ASSETS.IMG_SHOP037};
    static String[] gift_price_001 = {"25", "15", "800钻", "6", "6", "0.01"};
    static String[] gift_price_01 = {"25", "15", "800钻", "6", "6", "0.1"};
    static String[] honey_1 = {"X18", "X6", "X5", "X3", "X6", "X1"};
    static String[] inf_prop = {"元素之力X1", "复活心X1", "体力X1", "无限重玩:30天", "无限体力:30天", "斗龙手环X1", "斗龙眼镜X1", "零钱罐X1", "弹簧鞋X1"};
    static int[] num = new int[0];
    static int[] numbgImage_prop = {PAK_ASSETS.IMG_SHOP011, PAK_ASSETS.IMG_SHOP011, PAK_ASSETS.IMG_SHOP010, PAK_ASSETS.IMG_SHOP011, PAK_ASSETS.IMG_SHOP011, PAK_ASSETS.IMG_SHOP010, PAK_ASSETS.IMG_SHOP010, PAK_ASSETS.IMG_SHOP010, PAK_ASSETS.IMG_SHOP010};
    static int[] price_prop = {100, 200, 1000, 2000, 2000, 400, 400, 400, 400};
    static int[] propicon_1 = {126, 127, 128, 129, 128, 133, 132, 130, 131};
    static String[] revive_1 = {"X11", "X4", "X5", "X2", "X6", "X1"};
    static String[] shoes_1 = {"", "", "X5"};
    static Group zuanshigrop = new Group();
    ActorImage Cloud;
    float MoveY0;
    float MoveY1;
    Effect back;
    ActorImage backgroundImageDown;
    ActorImage backgroundImageMid;
    ActorImage backgroundImageUp;
    GNumSprite bearCionNum;
    GNumSprite diamondNum;
    ActorImage giftkuang;
    int moveX = 0;
    ActorImage priceback;
    ActorImage shopImage;
    ActorImage tishiImage;
    ActorImage tishibackImage;
    ActorImage titleImage;
    ActorSprite titleImage1;
    ActorSprite titleImage2;
    ActorSprite titleImage3;
    ActorSprite titleImage4;

    public void act(float delta) {
        super.act(delta);
        int diamondNum11 = RankData.getDiamondNum();
        int bearCionNum11 = RankData.getCakeNum();
        if (bearCionNum11 > 99999) {
            bearCionNum11 = 99999;
        }
        if (diamondNum11 > 99999) {
            diamondNum11 = 99999;
        }
        this.bearCionNum.setNum(bearCionNum11);
        this.diamondNum.setNum(diamondNum11);
    }

    public void init() {
        addActor(new Mask());
        initBackGround();
        addgift();
        grouplistener();
    }

    public MyShop(int id) {
        MySwitch.isInShop = true;
        initShopTitle(id);
        reset();
        if (GameSpecial.shopPop) {
            new MyGift(GMessage.PP_TEMPgift);
        }
    }

    /* access modifiers changed from: package-private */
    public void initBackGround() {
        GameUITools.GetbackgroundImage(320, 150, 12, this, false, false);
        GameStage.addActor(this, 4);
    }

    /* access modifiers changed from: package-private */
    public void initShopTitle(int id) {
        Group topgroup = new Group();
        new ActorButton((int) PAK_ASSETS.IMG_PUBLIC005, "0", (int) PAK_ASSETS.IMG_K04A, 130, 1, this);
        this.titleImage = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC008, 320, 115, 1, this);
        this.shopImage = new ActorImage((int) PAK_ASSETS.IMG_SHOP001, 320, 85, 1, this);
        Group group = new Group();
        GClipGroup gClipGroup = new GClipGroup();
        gClipGroup.setClipArea(30, PAK_ASSETS.IMG_SHENLONG2, PAK_ASSETS.IMG_C01, 40);
        group.addActor(gClipGroup);
        this.tishibackImage = new ActorImage(36, 320, (int) PAK_ASSETS.IMG_BOOM02, 1, this);
        this.tishiImage = new ActorImage((int) PAK_ASSETS.IMG_SHOP013, 320, 255, 1, gClipGroup);
        this.tishiImage.addAction(Actions.repeat(-1, Actions.moveBy(-2.0f, Animation.CurveTimeline.LINEAR)));
        gClipGroup.addActor(new Actor() {
            public void act(float delta) {
                if (MyShop.this.tishiImage.getX() <= -410.0f) {
                    MyShop.this.tishiImage.setVisible(false);
                    MyShop.this.tishiImage.setPosition(648.0f, MyShop.this.tishiImage.getY());
                    return;
                }
                MyShop.this.tishiImage.setVisible(true);
            }
        });
        addActor(group);
        this.titleImage1 = new ActorSprite(30, 170, 4, PAK_ASSETS.IMG_SHOP006, PAK_ASSETS.IMG_SHOP002);
        this.titleImage2 = new ActorSprite(175, 170, 4, PAK_ASSETS.IMG_SHOP007, PAK_ASSETS.IMG_SHOP003);
        this.titleImage3 = new ActorSprite(320, 170, 4, PAK_ASSETS.IMG_SHOP008, PAK_ASSETS.IMG_SHOP004);
        this.titleImage4 = new ActorSprite((int) PAK_ASSETS.IMG_QIANDAO007, 170, 4, PAK_ASSETS.IMG_SHOP009, PAK_ASSETS.IMG_SHOP005);
        switch (id) {
            case 0:
                this.titleImage1.setTexture(1);
                this.moveX = getMoveX1();
                reset();
                addgift();
                break;
            case 1:
                this.titleImage2.setTexture(1);
                this.moveX = PAK_ASSETS.IMG_SHENGJITIPS06;
                reset();
                addprop();
                break;
            case 2:
                this.titleImage3.setTexture(1);
                this.moveX = PAK_ASSETS.IMG_JUXINGNENGLIANGTA;
                reset();
                addcoin();
                break;
            case 3:
                this.titleImage4.setTexture(1);
                this.moveX = PAK_ASSETS.IMG_JUXINGNENGLIANGTA;
                reset();
                addzuanshi();
                break;
        }
        this.titleImage1.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                MyShop.this.titleImage1.setTexture(1);
                MyShop.this.titleImage2.setTexture(0);
                MyShop.this.titleImage3.setTexture(0);
                MyShop.this.titleImage4.setTexture(0);
                MyShop.this.moveX = MyShop.this.getMoveX1();
                MyShop.this.addgift();
                MyShop.this.reset();
            }
        });
        this.titleImage2.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                MyShop.this.titleImage2.setTexture(1);
                MyShop.this.titleImage1.setTexture(0);
                MyShop.this.titleImage3.setTexture(0);
                MyShop.this.titleImage4.setTexture(0);
                MyShop.this.moveX = PAK_ASSETS.IMG_SHENGJITIPS06;
                MyShop.this.addprop();
                MyShop.this.reset();
            }
        });
        this.titleImage3.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                MyShop.this.titleImage3.setTexture(1);
                MyShop.this.titleImage1.setTexture(0);
                MyShop.this.titleImage2.setTexture(0);
                MyShop.this.titleImage4.setTexture(0);
                MyShop.this.moveX = PAK_ASSETS.IMG_JUXINGNENGLIANGTA;
                MyShop.this.addcoin();
                MyShop.this.reset();
            }
        });
        this.titleImage4.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                MyShop.this.titleImage4.setTexture(1);
                MyShop.this.titleImage1.setTexture(0);
                MyShop.this.titleImage2.setTexture(0);
                MyShop.this.titleImage3.setTexture(0);
                MyShop.this.moveX = PAK_ASSETS.IMG_JUXINGNENGLIANGTA;
                MyShop.this.addzuanshi();
                MyShop.this.reset();
            }
        });
        topgroup.addActor(this.titleImage1);
        topgroup.addActor(this.titleImage2);
        topgroup.addActor(this.titleImage3);
        topgroup.addActor(this.titleImage4);
        addActor(topgroup);
        GameStage.addActor(this, 4);
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        zuanshigrop.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public int getMoveX1() {
        if (GMessage.getIsBuyed(1) && GMessage.getIsBuyed(0)) {
            System.out.println("买过两个");
            return PAK_ASSETS.IMG_UI_A29;
        } else if (GMessage.getIsBuyed(1) || GMessage.getIsBuyed(0)) {
            System.out.println("买过其中一个");
            return PAK_ASSETS.IMG_C01;
        } else {
            System.out.println("一个也没买过");
            return PAK_ASSETS.IMG_TIP008;
        }
    }

    /* access modifiers changed from: package-private */
    public void addzuanshi() {
        zuanshigrop.remove();
        zuanshigrop.clear();
        zuanshi(320, PAK_ASSETS.IMG_ZHANDOU041, 0, "X600", 6);
        zuanshi(320, 875, 1, "X1800", 15);
        zuanshi(320, 1075, 2, "X4350", 29);
        buylistener(1);
    }

    /* access modifiers changed from: package-private */
    public void addprop() {
        zuanshigrop.remove();
        zuanshigrop.clear();
        int id = -1;
        for (int i = 0; i <= 2; i++) {
            int y = (i * 250) + PAK_ASSETS.IMG_TONGJI001;
            for (int j = 0; j <= 2; j++) {
                id++;
                prop((190 * j) + 129, y, id, "1000");
            }
        }
        addmove();
    }

    public void run(float delta) {
        super.run(delta);
    }

    /* access modifiers changed from: package-private */
    public void addgift() {
        zuanshigrop.remove();
        zuanshigrop.clear();
        gift(320, PAK_ASSETS.IMG_ZHANDOU041, 0, 0);
        gift(320, 875, 1, 0);
        gift(320, 1075, 2, 1);
        gift(320, 1275, 3, 0);
        if (!GMessage.getIsBuyed(1) && !GMessage.getIsBuyed(0)) {
            gift(320, 1475, 4, 0);
            gift(320, 1675, 5, 0);
        } else if (!GMessage.getIsBuyed(1)) {
            gift(320, 1475, 4, 0);
        } else if (!GMessage.getIsBuyed(0)) {
            gift(320, 1475, 5, 0);
        }
        buylistener(1);
        addmove();
    }

    /* access modifiers changed from: package-private */
    public void addcoin() {
        zuanshigrop.remove();
        zuanshigrop.clear();
        coin(320, PAK_ASSETS.IMG_ZHANDOU041, 0, "X6000", 6);
        coin(320, 875, 1, "X18000", 15);
        coin(320, 1075, 2, "X37500", 25);
    }

    /* access modifiers changed from: package-private */
    public void zuanshi(int x, int y, int id, String diamondsNumber, int price) {
        int bgX = x;
        int bgY = y - 280;
        this.giftkuang = new ActorImage((int) PAK_ASSETS.IMG_SHOP014, bgX, bgY, 1, zuanshigrop);
        this.giftkuang.setTouchable(Touchable.enabled);
        new ActorImage(decorate1[id], bgX - 180, bgY - 2, 1, zuanshigrop);
        new ActorImage(decorate2[id], bgX, bgY - 50, 1, zuanshigrop);
        new ActorImage((int) PAK_ASSETS.IMG_SHOP043, bgX, bgY + 10, 1, zuanshigrop);
        GNumSprite diamondsNum = new GNumSprite((int) PAK_ASSETS.IMG_SHOP047, diamondsNumber, "X", 0, 4);
        diamondsNum.setPosition((float) (bgX + 16), (float) (bgY + 9));
        zuanshigrop.addActor(diamondsNum);
        this.priceback = new ActorImage((int) PAK_ASSETS.IMG_SHOP041, bgX + 190, bgY, 1, zuanshigrop);
        this.priceback.setName(Constant.S_E + id);
        this.priceback.setTouchable(Touchable.enabled);
        ActorImage pricesign = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC007, bgX + 165, bgY - 5, 1, zuanshigrop);
        GNumSprite priceNum = new GNumSprite((int) PAK_ASSETS.IMG_SHOP039, "" + price, ".", -2, 0);
        priceNum.setPosition((float) (bgX + 180), (float) (bgY - 20));
        if (price < 10) {
            pricesign.setPosition((float) (bgX + 158), (float) (bgY - 20));
            priceNum.setPosition((float) (bgX + 193), (float) (bgY - 20));
        }
        zuanshigrop.addActor(priceNum);
        zuanshigrop.addActor(new Effect().getEffect_Diejia(88, bgX - 180, (bgY - 2) + 50));
        addActor(zuanshigrop);
    }

    /* access modifiers changed from: package-private */
    public void coin(int x, int y, int id, String diamondsNumber, int price) {
        int bgX = x;
        int bgY = y - 280;
        this.giftkuang = new ActorImage((int) PAK_ASSETS.IMG_SHOP014, bgX, bgY, 1, zuanshigrop);
        this.giftkuang.setTouchable(Touchable.enabled);
        new ActorImage(coin1[id], bgX - 180, bgY - 2, 1, zuanshigrop);
        new ActorImage(coin2[id], bgX, bgY - 50, 1, zuanshigrop);
        new ActorImage((int) PAK_ASSETS.IMG_SHOP044, bgX, bgY + 10, 1, zuanshigrop);
        GNumSprite diamondsNum = new GNumSprite((int) PAK_ASSETS.IMG_SHOP047, diamondsNumber, "X", 0, 4);
        diamondsNum.setPosition((float) (bgX + 18), (float) (bgY + 9));
        zuanshigrop.addActor(diamondsNum);
        this.priceback = new ActorImage((int) PAK_ASSETS.IMG_SHOP041, bgX + 190, bgY, 1, zuanshigrop);
        this.priceback.setName(Constant.S_D + id);
        this.priceback.setTouchable(Touchable.enabled);
        ActorImage pricesign = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC007, bgX + 165, bgY - 5, 1, zuanshigrop);
        GNumSprite priceNum = new GNumSprite((int) PAK_ASSETS.IMG_SHOP039, "" + price, ".", -2, 0);
        priceNum.setPosition((float) (bgX + 180), (float) (bgY - 20));
        if (price < 10) {
            pricesign.setPosition((float) (bgX + 158), (float) (bgY - 20));
            priceNum.setPosition((float) (bgX + 193), (float) (bgY - 20));
        }
        zuanshigrop.addActor(priceNum);
        zuanshigrop.addActor(new Effect().getEffect_Diejia(88, bgX - 180, (bgY - 2) + 50));
        addActor(zuanshigrop);
    }

    /* access modifiers changed from: package-private */
    public void prop(int x, int y, int id, String price) {
        int bgX = x;
        int bgY = y - 280;
        ActorImage propbj = new ActorImage((int) PAK_ASSETS.IMG_SHOP012, bgX, bgY, 1, zuanshigrop);
        propbj.setTouchable(Touchable.enabled);
        propbj.setName(Constant.S_C + id);
        new ActorImage(propicon_1[id], bgX, bgY - 45, 1, zuanshigrop);
        ActorText reviveStar = new ActorText(inf_prop[id], bgX, bgY + 20, 1, zuanshigrop);
        reviveStar.setColor(Color.BLUE);
        if (id == 3 || id == 4) {
            new ActorText("剩余：" + getPropNum(id) + "天", bgX, bgY + 45, 1, zuanshigrop).setColor(Color.GREEN);
        } else if (id != 2) {
            new ActorText("数量：" + getPropNum(id), bgX, bgY + 45, 1, zuanshigrop).setColor(Color.GREEN);
        } else if (RankData.getEndlessPowerDays() > 0) {
            reviveStar.setPosition((float) bgX, (float) (bgY + 30));
        } else {
            new ActorText("数量：" + getPropNum(id), bgX, bgY + 45, 1, zuanshigrop).setColor(Color.GREEN);
        }
        new ActorImage(numbgImage_prop[id], bgX, bgY + 80, 1, zuanshigrop);
        GNumSprite price1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "X" + price_prop[id], "X", -2, 4);
        price1.setPosition((float) (bgX + 10), (float) (bgY + 80));
        zuanshigrop.addActor(price1);
        zuanshigrop.addActor(new Effect().getEffect_Diejia(88, bgX, bgY));
        addActor(zuanshigrop);
    }

    public int getPropNum(int id) {
        switch (id) {
            case 0:
                return RankData.getHoneyNum();
            case 1:
                return RankData.getHeartNum();
            case 2:
                return RankData.getPowerNum();
            case 3:
                return RankData.getEndlessReplayDays();
            case 4:
                return RankData.getEndlessPowerDays();
            case 5:
                return RankData.getPropNum(0);
            case 6:
                return RankData.getPropNum(1);
            case 7:
                return RankData.getPropNum(2);
            case 8:
                return RankData.getPropNum(3);
            default:
                return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void gift(int x, int y, int id, int index) {
        GNumSprite priceNum;
        GNumSprite priceNum2;
        int bgX = x;
        int bgY = y - 280;
        this.giftkuang = new ActorImage((int) PAK_ASSETS.IMG_SHOP014, bgX, bgY, 1, zuanshigrop);
        this.giftkuang.setTouchable(Touchable.enabled);
        new ActorImage(gift_1[id], bgX - 190, bgY, 1, zuanshigrop);
        if (index == 1) {
            ActorImage glasses = new ActorImage(132, (int) PAK_ASSETS.IMG_SHUOMINGZI03, bgY - 45, 1, zuanshigrop);
            ActorImage shouhuan = new ActorImage(133, 320, bgY - 45, 1, zuanshigrop);
            ActorImage box = new ActorImage(130, 400, bgY - 45, 1, zuanshigrop);
            ActorImage shoes = new ActorImage(131, (int) PAK_ASSETS.IMG_SHUOMINGZI03, bgY + 30, 1, zuanshigrop);
            ActorImage bearcoin = new ActorImage(135, 320, bgY + 30, 1, zuanshigrop);
            glasses.setScale(0.5f);
            shouhuan.setScale(0.5f);
            box.setScale(0.5f);
            shoes.setScale(0.5f);
            bearcoin.setScale(0.5f);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, (int) PAK_ASSETS.IMG_SHUOMINGZI03, bgY - 20, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, 320, bgY - 20, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, 400, bgY - 20, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, (int) PAK_ASSETS.IMG_SHUOMINGZI03, bgY + 55, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP045, (int) PAK_ASSETS.IMG_LS007A, bgY + 55, 1, zuanshigrop);
            GNumSprite glassesNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, honey_1[id], "X", -2, 4);
            glassesNum.setPosition((float) PAK_ASSETS.IMG_SHUOMINGZI03, (float) (bgY - 20));
            zuanshigrop.addActor(glassesNum);
            GNumSprite shouhuanNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, revive_1[id], "X", -2, 4);
            shouhuanNum.setPosition((float) 320, (float) (bgY - 20));
            zuanshigrop.addActor(shouhuanNum);
            GNumSprite boxNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, bearcoin_1[id], "X", -2, 4);
            boxNum.setPosition((float) 400, (float) (bgY - 20));
            zuanshigrop.addActor(boxNum);
            GNumSprite shoesNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, shoes_1[id], "X", -2, 4);
            shoesNum.setPosition((float) PAK_ASSETS.IMG_SHUOMINGZI03, (float) (bgY + 55));
            zuanshigrop.addActor(shoesNum);
            GNumSprite bearcoinNum1 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, bearcoin1_1[id], "X", -2, 4);
            bearcoinNum1.setPosition((float) 320, (float) (bgY + 55));
            zuanshigrop.addActor(bearcoinNum1);
            new ActorText("原价￥" + costPrice_1[id], bgX + 120, bgY + 30, 12, zuanshigrop).setColor(Color.BLUE);
        } else if (index == 0) {
            ActorImage revive = new ActorImage(126, (int) PAK_ASSETS.IMG_SHUOMINGZI03, bgY - 30, 1, zuanshigrop);
            ActorImage honey = new ActorImage(127, 320, bgY - 30, 1, zuanshigrop);
            ActorImage actorImage = new ActorImage(135, 400, bgY - 30, 1, zuanshigrop);
            revive.setScale(0.5f);
            honey.setScale(0.5f);
            actorImage.setScale(0.5f);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, (int) PAK_ASSETS.IMG_SHUOMINGZI03, bgY + 20, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, 320, bgY + 20, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP045, 405, bgY + 20, 1, zuanshigrop);
            GNumSprite honeyNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, honey_1[id], "X", -2, 4);
            honeyNum.setPosition((float) PAK_ASSETS.IMG_SHUOMINGZI03, (float) (bgY + 20));
            zuanshigrop.addActor(honeyNum);
            GNumSprite reviveNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, revive_1[id], "X", -2, 4);
            reviveNum.setPosition((float) 320, (float) (bgY + 20));
            zuanshigrop.addActor(reviveNum);
            GNumSprite bearcoinNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, bearcoin_1[id], "X", -2, 4);
            bearcoinNum.setPosition((float) 400, (float) (bgY + 20));
            zuanshigrop.addActor(bearcoinNum);
            new ActorText("原价￥" + costPrice_1[id], bgX + 150, bgY + 30, 12, zuanshigrop).setColor(Color.BLUE);
        } else {
            ActorImage actorImage2 = new ActorImage(126, 260, bgY - 30, 1, zuanshigrop);
            ActorImage actorImage3 = new ActorImage(135, (int) PAK_ASSETS.IMG_UI_BUTTON_BEGIN01, bgY - 30, 1, zuanshigrop);
            actorImage2.setScale(0.5f);
            actorImage3.setScale(0.5f);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP027, 260, bgY + 20, 1, zuanshigrop);
            new ActorImage((int) PAK_ASSETS.IMG_SHOP045, (int) PAK_ASSETS.IMG_UI_BUTTON_BEGIN01, bgY + 20, 1, zuanshigrop);
            GNumSprite gNumSprite = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, revive_1[id], "X", -2, 4);
            gNumSprite.setPosition((float) 260, (float) (bgY + 20));
            zuanshigrop.addActor(gNumSprite);
            GNumSprite gNumSprite2 = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, bearcoin_1[id], "X", -2, 4);
            gNumSprite2.setPosition((float) PAK_ASSETS.IMG_UI_BUTTON_BEGIN01, (float) (bgY + 20));
            zuanshigrop.addActor(gNumSprite2);
            new ActorText("原价￥" + costPrice_1[id], bgX + 150, bgY + 30, 12, zuanshigrop).setColor(Color.BLUE);
        }
        if (id == 4 || id == 5) {
            new ActorImage((int) PAK_ASSETS.IMG_SHOP029, bgX + 190, bgY - 50, 1, zuanshigrop);
        }
        this.priceback = new ActorImage((int) PAK_ASSETS.IMG_SHOP041, bgX + 190, bgY, 1, zuanshigrop);
        this.priceback.setName("1" + id);
        this.priceback.setTouchable(Touchable.enabled);
        if (id != 2) {
            ActorImage pricesign = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC007, bgX + 165, bgY - 5, 1, zuanshigrop);
            if (MySwitch.Yifen) {
                priceNum2 = new GNumSprite((int) PAK_ASSETS.IMG_SHOP039, gift_price_001[id], ".", -2, 0);
            } else {
                priceNum2 = new GNumSprite((int) PAK_ASSETS.IMG_SHOP039, gift_price_01[id], ".", -2, 0);
            }
            priceNum2.setPosition((float) (bgX + 180), (float) (bgY - 20));
            if (id == 3 || id == 4) {
                priceNum2.setPosition((float) (bgX + 185), (float) (bgY - 20));
                pricesign.setPosition((float) (bgX + 155), (float) (bgY - 20));
            }
            zuanshigrop.addActor(priceNum2);
            if (id == 5) {
                if (MySwitch.Yifen) {
                    pricesign.setPosition((float) (bgX + 130), (float) (bgY - 20));
                    priceNum2.setPosition((float) (bgX + 160), (float) (bgY - 20));
                } else {
                    pricesign.setPosition((float) (bgX + 140), (float) (bgY - 20));
                    priceNum2.setPosition((float) (bgX + 170), (float) (bgY - 20));
                }
            }
        } else {
            new ActorImage(134, bgX + 150, bgY - 5, 1, zuanshigrop).setScale(0.4f, 0.4f);
            if (MySwitch.Yifen) {
                priceNum = new GNumSprite((int) PAK_ASSETS.IMG_SHOP039, gift_price_001[id], ".", -2, 0);
            } else {
                priceNum = new GNumSprite((int) PAK_ASSETS.IMG_SHOP039, gift_price_01[id], ".", -2, 0);
            }
            priceNum.setPosition((float) (bgX + 170), (float) (bgY - 20));
            zuanshigrop.addActor(priceNum);
        }
        new ActorImage((int) PAK_ASSETS.IMG_SHOP030, bgX + 185, bgY + 40, 1, zuanshigrop);
        new ActorImage(gift_baioti[id], (bgX - 190) + 10, (bgY + 9) - 40, 1, zuanshigrop);
        zuanshigrop.addActor(new Effect().getEffect_Diejia(88, bgX - 190, bgY + 51));
        addActor(zuanshigrop);
    }

    /* access modifiers changed from: package-private */
    public void buylistener(int id) {
        this.priceback.addListener(new InputListener() {
            public boolean keyDown(InputEvent event, int keycode) {
                System.out.println("购买 ");
                return super.keyDown(event, keycode);
            }

            public boolean keyUp(InputEvent event, int keycode) {
                return super.keyUp(event, keycode);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void addmove() {
        Group group = new Group();
        GClipGroup shopClipgroup = new GClipGroup();
        shopClipgroup.setClipArea(3, PAK_ASSETS.IMG_BUFF_SUDU, PAK_ASSETS.IMG_SHUOMINGKUANG, PAK_ASSETS.IMG_SHOP016);
        shopClipgroup.addActor(zuanshigrop);
        System.out.println("moveX:" + this.moveX);
        zuanshigrop.addListener(GameUITools.getMoveListener(zuanshigrop, (float) this.moveX, 180.0f, 5.0f, false));
        group.addActor(shopClipgroup);
        addActor(group);
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC006, 320, 967, 1, this);
        new ActorImage((int) PAK_ASSETS.IMG_SHOP044, 160, 1025, 1, this);
        new ActorImage((int) PAK_ASSETS.IMG_SHOP043, (int) PAK_ASSETS.IMG_CHUANGGUAN001, 1025, 1, this);
        new ActorButton(100, "50", (int) PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 1025, 1, this);
        new ActorButton(100, "51", (int) PAK_ASSETS.IMG_PUBLIC004, 1025, 1, this);
        int bearCionNum1 = RankData.getCakeNum();
        int diamondNum1 = RankData.getDiamondNum();
        this.bearCionNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + bearCionNum1, "X", -2, 4);
        this.bearCionNum.setPosition(170.0f, 1025.0f);
        addActor(this.bearCionNum);
        this.diamondNum = new GNumSprite((int) PAK_ASSETS.IMG_PUBLIC013, "" + diamondNum1, "X", -2, 4);
        this.diamondNum.setPosition(490.0f, 1025.0f);
        addActor(this.diamondNum);
    }

    /* access modifiers changed from: package-private */
    public void grouplistener() {
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                MyShop.this.MoveY0 = MyShop.zuanshigrop.getY();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                int codeName;
                MyShop.this.MoveY1 = MyShop.zuanshigrop.getY();
                if (Math.abs(MyShop.this.MoveY0 - MyShop.this.MoveY1) <= 5.0f) {
                    String buttonName = event.getTarget().getName();
                    if (buttonName != null) {
                        codeName = Integer.parseInt(buttonName);
                    } else {
                        codeName = 100;
                    }
                    switch (codeName) {
                        case 0:
                            Sound.playButtonClosed();
                            MySwitch.isInShop = false;
                            MyShop.this.free();
                            if (RankData.getEndlessPowerDays() > 0 && MyGameMap.isInMap) {
                                GameStage.clearAllLayers();
                                GameMain.toScreen(new MyGameMap());
                                break;
                            }
                        case 10:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(4);
                                break;
                            } else {
                                new MyGift(4);
                                break;
                            }
                        case 11:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(3);
                                break;
                            } else {
                                MyTip.tip(3);
                                break;
                            }
                        case 12:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(20);
                                break;
                            } else {
                                MyTip.tip(20);
                                break;
                            }
                        case 13:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(2);
                                break;
                            } else {
                                new MyGift(2);
                                break;
                            }
                        case 14:
                            Sound.playButtonPressed();
                            MyShop.this.getXinGift();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(1);
                                break;
                            } else {
                                new MyGift(1);
                                break;
                            }
                        case 15:
                            Sound.playButtonPressed();
                            MyShop.this.getXinGift();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(0);
                                break;
                            } else {
                                new MyGift(0);
                                break;
                            }
                        case 20:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(21);
                                break;
                            } else {
                                MyTip.tip(21);
                                break;
                            }
                        case 21:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(22);
                                break;
                            } else {
                                MyTip.tip(22);
                                break;
                            }
                        case 22:
                            Sound.playButtonPressed();
                            if (RankData.getEndlessPowerDays() <= 0) {
                                MyShop.this.getXinprop();
                                if (MySwitch.isCaseA != 0) {
                                    GMessage.send(23);
                                    break;
                                } else {
                                    MyTip.tip(23);
                                    break;
                                }
                            } else {
                                MyTip.NotBuy();
                                break;
                            }
                        case 23:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(24);
                                break;
                            } else {
                                MyTip.tip(24);
                                break;
                            }
                        case 24:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(25);
                                break;
                            } else {
                                MyTip.tip(25);
                                break;
                            }
                        case 25:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(26);
                                break;
                            } else {
                                MyTip.tip(26);
                                break;
                            }
                        case 26:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(27);
                                break;
                            } else {
                                MyTip.tip(27);
                                break;
                            }
                        case 27:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(28);
                                break;
                            } else {
                                MyTip.tip(28);
                                break;
                            }
                        case 28:
                            Sound.playButtonPressed();
                            MyShop.this.getXinprop();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(29);
                                break;
                            } else {
                                MyTip.tip(29);
                                break;
                            }
                        case 30:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(8);
                                break;
                            } else {
                                MyTip.tip(8);
                                break;
                            }
                        case 31:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(9);
                                break;
                            } else {
                                MyTip.tip(9);
                                break;
                            }
                        case 32:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(10);
                                break;
                            } else {
                                new MyGift(10);
                                break;
                            }
                        case 40:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(5);
                                break;
                            } else {
                                MyTip.tip(5);
                                break;
                            }
                        case 41:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(6);
                                break;
                            } else {
                                MyTip.tip(6);
                                break;
                            }
                        case 42:
                            Sound.playButtonPressed();
                            if (MySwitch.isCaseA != 0) {
                                GMessage.send(7);
                                break;
                            } else {
                                new MyGift(7);
                                break;
                            }
                        case 50:
                            Sound.playButtonPressed();
                            MyShop.this.free();
                            new MyShop(2);
                            break;
                        case 51:
                            Sound.playButtonPressed();
                            MyShop.this.free();
                            new MyShop(3);
                            break;
                    }
                    super.touchUp(event, x, y, pointer, button);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void getXinprop() {
        GMessage.setMfContonl(new GMessage.MianGiftContonl() {
            public void Yes() {
                MyShop.this.addprop();
            }

            public void No() {
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void getXinGift() {
        GMessage.setMfContonl(new GMessage.MianGiftContonl() {
            public void Yes() {
                System.out.println("---更新");
                if (GMessage.getIsBuyed(1)) {
                    System.out.println("---已购买新手礼包");
                    if (Mymainmenu2.xshb != null) {
                        Mymainmenu2.xshb.setVisible(false);
                    }
                    if (Mymainmenu2.czhb != null) {
                        Mymainmenu2.czhb.setVisible(true);
                    }
                } else {
                    System.out.println("---没有购买新手礼包");
                    if (Mymainmenu2.czhb != null) {
                        Mymainmenu2.czhb.setVisible(false);
                    }
                }
                System.out.println("更新礼包组----");
                MyShop.this.moveX = MyShop.this.getMoveX1();
                MyShop.this.reset();
                MyShop.this.addgift();
            }

            public void No() {
            }
        });
    }

    public void exit() {
        remove();
        clear();
        zuanshigrop.remove();
        zuanshigrop.clear();
    }
}
