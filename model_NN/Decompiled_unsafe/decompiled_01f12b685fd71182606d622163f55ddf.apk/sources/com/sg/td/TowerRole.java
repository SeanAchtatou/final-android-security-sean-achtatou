package com.sg.td;

import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.AnimationState;
import com.kbz.esotericsoftware.spine.Event;
import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.td.record.Bear;
import com.sg.td.record.LoadBear;
import com.sg.util.GameStage;

public class TowerRole extends Tower {
    static int[] sound_hit = {45, 27, 79, 45};
    static int[] sound_show = {50, 34, 42, 50};
    int aimX;
    int aimY;
    Bear bearData;
    boolean complete;
    int damage;
    public int damageAdd;
    int level;
    boolean move;
    RoleSpine role;
    public int roleIndex;
    float scale = 1.0f;
    int spindId;
    int step = 80;
    float time;

    public TowerRole(int x, int y, int index) {
        this.roleIndex = index;
        this.spindId = RankData.getRoleSpineId(this.roleIndex);
        if (this.spindId == 19) {
            this.scale = 0.8f;
        }
        this.role = new RoleSpine(x, y, this.spindId, this.scale);
        this.isRole = true;
    }

    public int getSpineId() {
        return this.spindId;
    }

    public void setBearData() {
        this.bearData = LoadBear.bearData.get(this.name);
    }

    public int getDamage(int value) {
        this.level = Math.max(1, RankData.roleLevel[this.roleIndex]);
        this.damageAdd = this.bearData.getCurDamage(this.level);
        this.damage = ((Rank.role.damageAdd * value) / 100) + value;
        return this.damage;
    }

    public int getPower() {
        this.level = Math.max(1, RankData.roleLevel[this.roleIndex]);
        this.power = this.bearData.getCurAttack(this.level);
        return calcuPower(this.power);
    }

    /* access modifiers changed from: package-private */
    public void setRoleIndex(int index) {
        this.roleIndex = index;
    }

    public int getRoleIndex() {
        return this.roleIndex;
    }

    public void run(float delta) {
        super.run(delta);
        setFaceDir();
        this.time += delta;
        runUpIcon();
    }

    /* access modifiers changed from: package-private */
    public float getAttackTime() {
        return 1.0f / ((float) Rank.getGameSpeed());
    }

    /* access modifiers changed from: package-private */
    public void runTower(float delta) {
        this.attackFrame = false;
        if (isVisible()) {
            switch (this.curStatus) {
                case 2:
                case 4:
                case 5:
                case 6:
                default:
                    return;
                case 3:
                    if (this.frameTime == 0 && this.role.curStatus == 1) {
                        this.role.setStatus(4);
                        this.complete = false;
                        this.time = Animation.CurveTimeline.LINEAR;
                        this.frameTime--;
                    }
                    if (getAnimationComplete()) {
                        finishAttack();
                        playSound_hit();
                        return;
                    }
                    return;
                case 7:
                    this.role.setStatus(5);
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void finishAttack() {
        this.attackFrame = true;
        setStatus(2);
    }

    /* access modifiers changed from: package-private */
    public void setFaceDir() {
        switch (this.faceDir) {
            case 1:
            case 4:
            case 6:
                this.role.setFilpX(true);
                return;
            case 2:
            case 3:
            case 5:
            case 7:
                this.role.setFilpX(false);
                return;
            default:
                return;
        }
    }

    public boolean getAnimationComplete() {
        return this.complete;
    }

    /* access modifiers changed from: package-private */
    public void setMove(int ax, int ay) {
        this.aimX = ax;
        this.aimY = ay;
        this.move = true;
    }

    /* access modifiers changed from: protected */
    public void move_line_follow() {
        if (this.move) {
            int a = this.aimX - this.x;
            int b = this.aimY - this.y;
            int moveX = 0;
            int moveY = 0;
            float r = (float) Math.sqrt((double) ((a * a) + (b * b)));
            if (a != 0) {
                moveX = (int) (((float) (this.step * a)) / r);
            }
            if (b != 0) {
                moveY = (int) (((float) (this.step * b)) / r);
            }
            if (Math.abs(a) > Math.abs(moveX) || Math.abs(b) > Math.abs(moveY)) {
                this.x += moveX;
                this.y += moveY;
            } else {
                this.x = this.aimX;
                this.y = this.aimY;
                this.move = false;
                Rank.setGride();
            }
            setPosition(this.x, this.y);
        }
    }

    /* access modifiers changed from: package-private */
    public void setRoleStop() {
        this.role.setStatus(1);
    }

    /* access modifiers changed from: package-private */
    public void roleRemoveStage() {
        GameStage.removeActor(this.role);
        removeStage();
    }

    /* access modifiers changed from: package-private */
    public void runUpIcon() {
        if (!canUp()) {
            this.upIcon.removeStage();
        } else {
            this.upIcon.canSee();
        }
    }

    public void setPosition(int x, int y) {
        this.role.setPosition((float) x, (float) y);
        this.upIcon.setPosition(x, y - 70);
        moveBuff(x, y - 15);
    }

    public boolean isFull() {
        return RankData.roleFullLevel(this.roleIndex);
    }

    public boolean canUp() {
        if (isFull() || !RankData.isRoleOpen(this.roleIndex)) {
            return false;
        }
        return true;
    }

    public void setRoleAniSpeed() {
        this.role.setAniSpeed((float) Rank.getGameSpeed());
    }

    /* access modifiers changed from: package-private */
    public void playSound_hit() {
        Sound.playSound(sound_hit[this.roleIndex]);
    }

    public static void playSound_show(int roleIndex2) {
        Sound.playSound(sound_show[roleIndex2]);
    }

    class RoleSpine extends MySpine implements GameConstant {
        private AnimationState.AnimationStateAdapter listener = new AnimationState.AnimationStateAdapter() {
            public void event(int trackIndex, Event event) {
                String name = event.getData().getName();
                if (RoleSpine.this.curStatus == 4 && "gongji".equals(name)) {
                    TowerRole.this.complete = true;
                }
            }
        };
        MySpine roleSpine;

        public RoleSpine(int x, int y, int spineID, float sacle) {
            init(SPINE_NAME);
            createSpineRole(spineID, sacle);
            initMotion(motion_leiguman);
            setStatus(1);
            setPosition((float) x, (float) y);
            setId();
            setAniSpeed(1.0f);
            GameStage.addActor(this, 2);
            addListener(this.listener);
        }

        public void setPosition(float x, float y) {
            super.setPosition(x, y + ((float) ((Map.tileHight / 2) - 13)));
        }

        public void run(float delta) {
            updata();
            this.index++;
        }

        public void act(float delta) {
            super.act(delta);
            run(delta);
            if (!isEnd()) {
                return;
            }
            if (this.curStatus == 4 || this.curStatus == 5) {
                setStatus(1);
            }
        }
    }
}
