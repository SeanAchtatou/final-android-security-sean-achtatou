package com.sg.td;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import cn.cmgame.am;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.sg.GameEntry.GameMain;
import com.sg.notification.BgService;
import com.sg.td.message.MySwitch;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MainActivity extends AndroidApplication {
    static boolean finished = false;
    public static MainActivity me;
    public static SDKMessage sdkInterface;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        if (finished) {
            finished = false;
            System.exit(0);
            startActivity(getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName()));
            return;
        }
        super.onCreate(savedInstanceState);
        am.bm(this);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useAccelerometer = false;
        config.useCompass = false;
        initialize(new GameMain(), config);
        me = this;
        sdkInterface = new SDKMessage(me);
        GameMain.dialog = sdkInterface;
        pushMessage();
    }

    private void pushMessage() {
        startService(new Intent(me, BgService.class));
    }

    private String getSonggeChannel() {
        InputStream file = Gdx.files.internal("songgeChannel.txt").read();
        Properties prop = new Properties();
        try {
            prop.load(file);
            return prop.getProperty("songgeChannel").substring(prop.getProperty("songgeChannel").lastIndexOf("_") + 1);
        } catch (IOException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SDKMessage.unity.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        SDKMessage.unity.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finished = true;
        SDKMessage.unity.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        SDKMessage.unity.onStop();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        SDKMessage.unity.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        SDKMessage.unity.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        SDKMessage.unity.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SDKMessage.unity.onActivityResult(requestCode, resultCode, data);
    }

    public void onBackPressed() {
        if (Rank.me != null) {
            Rank.me.pause();
        }
        AppUtil.checkInfo(this);
        if (MySwitch.isExit) {
            sdkInterface.exit();
        } else if (!Rank.ranking) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setMessage("确定要退出吗?");
            builder.setTitle("提示");
            if (Rank.me != null) {
                Rank.me.pause();
            }
            builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    MainActivity.sdkInterface.exit();
                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (Rank.me != null) {
                        Rank.me.resume();
                    }
                }
            });
            builder.create().show();
        }
    }
}
