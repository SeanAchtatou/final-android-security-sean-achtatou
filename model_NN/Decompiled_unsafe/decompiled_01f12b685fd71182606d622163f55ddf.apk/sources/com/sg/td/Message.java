package com.sg.td;

import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.td.UI.MyGift;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;

public class Message implements GameConstant {
    static boolean messageShow;

    public static boolean showQuit() {
        if (Rank.isRanking()) {
            if (Rank.isMidSystem() || Rank.isSystemEventPause() || Rank.isFail() || Rank.isWin()) {
                return false;
            }
            if (!Rank.isSystemEvent()) {
                Rank.setSystemEvent((byte) 9);
                return false;
            }
        }
        return true;
    }

    static void initMessageData() {
        messageShow = false;
    }

    public static void showGiftMessage(int index) {
        if (index == Rank.award.size() - 1 && GameSpecial.atuoPop && !messageShow) {
            new MyGift(GMessage.PP_TEMPgift);
            messageShow = true;
        }
    }

    public static void showAD() {
        if (GameMain.dialog != null) {
            GameMain.dialog.showAD(1);
        }
    }

    public static void moreGame() {
        if (GameMain.dialog != null) {
            GameMain.dialog.showAD(5);
        }
    }
}
