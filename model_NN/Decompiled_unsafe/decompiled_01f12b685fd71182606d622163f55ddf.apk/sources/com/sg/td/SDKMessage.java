package com.sg.td;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.widget.EditText;
import android.widget.Toast;
import com.sg.GameEntry.GetName;
import com.sg.GameEntry.SDKInterface;
import com.sg.game.pay.UnityExitCallback;
import com.sg.game.pay.UnityInitCallback;
import com.sg.game.pay.UnityLoginCallback;
import com.sg.game.pay.UnityPayCallback;
import com.sg.game.unity.SGUnity;
import com.sg.td.message.GMessage;
import com.sg.td.message.MySwitch;
import java.lang.reflect.Field;

public class SDKMessage implements SDKInterface {
    static SGUnity unity;
    public Context context;
    Handler nameHandler = new Handler();
    int payWay = 99;
    Handler send = new Handler();
    Handler showAD = new Handler();

    public SDKMessage(Context context2) {
        this.context = context2;
        initSDK();
        changeSwitch();
    }

    public void changeSwitch() {
        MySwitch.isSimId = unity.getSimID();
        MySwitch.isMoreGame = unity.moreGameShow();
        MySwitch.isExit = !unity.exitGameShow();
        MySwitch.isQIhu = unity.getName().equals("qihu");
        if (MySwitch.isSimId == 0) {
            MySwitch.Yifen = true;
        }
        MySwitch.giftType = unity.getConfig("gift");
        if (MySwitch.giftType == null) {
            MySwitch.giftType = "1";
        }
        System.out.println("1111MySwitch.giftType" + MySwitch.giftType);
        if (unity.getName().equals("cucc") || unity.getName().equals("ctcc") || unity.getName().equals("cmcc") || unity.getName().equals("mm")) {
            MySwitch.isParent = true;
        }
    }

    public void initSDK() {
        unity = new SGUnity(MainActivity.me, new UnityInitCallback() {
            public void success() {
                System.out.println("success");
            }

            public void fail(String arg0) {
                System.out.println("fail");
            }
        });
        unity.onCreate();
        unity.init();
        unity.login(new UnityLoginCallback() {
            public void loginFinish() {
            }
        });
    }

    public void sendMessage(final int id) {
        this.send.post(new Runnable() {
            public void run() {
                SDKMessage.unity.pay(99, id, new UnityPayCallback() {
                    public void paySuccess(int arg0) {
                        GMessage.SendSuccess();
                    }

                    public void payFail(int arg0, String arg1) {
                        GMessage.sendFail();
                        Toast.makeText(MainActivity.me, "购买失败，服务器处理中，请稍后重试", 0).show();
                    }

                    public void payCancel(int arg0) {
                        GMessage.sendFail();
                        Toast.makeText(MainActivity.me, "购买失败，服务器处理中，请稍后重试", 0).show();
                    }
                });
            }
        });
    }

    public int getOperatorId() {
        TelephonyManager telManager = (TelephonyManager) this.context.getSystemService("phone");
        if (telManager.getSimState() != 5) {
            return 0;
        }
        String imsi = telManager.getSubscriberId();
        if (imsi == null) {
            return -1;
        }
        String[][] startWithStrings = {new String[]{"46000", "46002", "46007", "898600"}, new String[]{"46003", "46005"}, new String[]{"46001", "46006", "46009"}};
        for (int i = 0; i < startWithStrings.length; i++) {
            for (String startsWith : startWithStrings[i]) {
                if (imsi.startsWith(startsWith)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void setName() {
        this.nameHandler.post(new Runnable() {
            public void run() {
                SDKMessage.this.setNickname();
            }
        });
    }

    /* access modifiers changed from: private */
    public void setNickname() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.me);
        builder.setTitle("请输入您的游戏昵称：");
        final EditText editText = new EditText(MainActivity.me);
        builder.setCancelable(false);
        builder.setView(editText);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Field field = null;
                try {
                    field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
                    field.setAccessible(true);
                    field.set(dialog, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String name = editText.getText().toString().trim();
                if (name.equals("") || name == null) {
                    Toast.makeText(MainActivity.me, "请输入名称", 0).show();
                    return;
                }
                for (int i = 0; i < name.length(); i++) {
                    char c = name.charAt(i);
                    if ((c < '0' || c > '9') && ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z'))) {
                        Toast.makeText(MainActivity.me, "名称不合法", 0).show();
                        return;
                    }
                }
                if (name.length() > 11) {
                    Toast.makeText(MainActivity.me, "名称过长。", 0).show();
                    return;
                }
                field.setAccessible(true);
                try {
                    field.set(dialog, true);
                } catch (IllegalArgumentException e2) {
                    e2.printStackTrace();
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("随机取名", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                try {
                    Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
                    field.setAccessible(true);
                    field.set(dialog, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                editText.setText(GetName.getName());
            }
        });
        builder.create();
        builder.show();
    }

    public void loaginGUI() {
    }

    public void exit() {
        unity.exitGame(new UnityExitCallback() {
            public void exit() {
                AppUtil.exitGameProcess(MainActivity.me);
            }

            public void cancel() {
                System.out.println("********cancel");
                if (Rank.me != null) {
                    Rank.me.resume();
                }
            }
        });
    }

    public void showAD(int id) {
        switch (id) {
            case 0:
                MainActivity.me.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:4008289368")));
                return;
            case 1:
                this.showAD.post(new Runnable() {
                    public void run() {
                        SDKMessage.unity.pause();
                    }
                });
                return;
            case 2:
            case 3:
            case 4:
            default:
                return;
            case 5:
                unity.moreGame();
                return;
        }
    }

    public int getSimsId() {
        return getOperatorId();
    }

    public String[] initSGManager() {
        return XiaXing.initSGManager();
    }
}
