package com.sg.td;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Sort;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.EatBuff;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Up;
import com.sg.td.data.Mydata;
import com.sg.td.data.TowerData;
import com.sg.td.record.LoadStrength;
import com.sg.tools.MySprite;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;
import java.util.Comparator;

public class Tower extends MySprite implements GameConstant {
    static Sort sort = Sort.instance();
    float CD;
    int aimIndex = -1;
    byte aimType;
    String animationName;
    String animationPack;
    public boolean attackFrame;
    int attackFrequency;
    int attackSpeed = 1;
    int attackType;
    TowerBase base;
    boolean block;
    float buffLife;
    float buffRate;
    int buffType;
    float buffValue;
    int bulletAspeed;
    int bulletCount;
    float bulletLife;
    int bulletSpeed;
    int curStatus;
    boolean dead;
    int[] degrees;
    public int delPrice;
    EatBuff eatBuff;
    int eatCDTime;
    int eatPowerTime;
    int eatRangeTime;
    byte enemyType;
    int explodeRange;
    int faceDir = 3;
    byte flashMode;
    int frameTime = 0;
    float frequencyTime;
    public int h;
    int hitBackSoundId = -1;
    int hitComeSoundId = -1;
    int hitSoundId = -1;
    int hitSoundTowId = -1;
    String hurtEffect;
    int id;
    public boolean isBox;
    public boolean isRandom;
    public boolean isRole;
    int jianshePower;
    int jiansheRange;
    int level;
    int modeOffsetY;
    public String name;
    int nextDegrees;
    int power;
    int range;
    float rotateSpeed;
    float runTime;
    int space = 20;
    TowerData tData;
    Up upIcon;
    public int upPrice;
    public int w;
    public int x;
    public int y;

    public Tower() {
    }

    public Tower(String animationPack2, String animationName2) {
        super(animationPack2, animationName2);
        this.animationPack = animationPack2;
        this.animationName = animationName2;
        this.isRole = false;
    }

    public int getRange() {
        int nowRange = this.range;
        for (int i = 0; i < this.eatRangeTime; i++) {
            nowRange += this.tData.getFruitadd().getRange();
        }
        if (RankData.useGLasses()) {
            return (int) (((float) nowRange) + (((float) nowRange) * 0.1f));
        }
        return nowRange;
    }

    /* access modifiers changed from: package-private */
    public int getJiansheRange() {
        return this.jiansheRange;
    }

    /* access modifiers changed from: package-private */
    public int getExplodeRange() {
        return this.explodeRange;
    }

    /* access modifiers changed from: package-private */
    public void setJiansheRange(int jiansheRange2) {
        this.jiansheRange = jiansheRange2;
    }

    /* access modifiers changed from: package-private */
    public int getJianshePower() {
        return this.jianshePower;
    }

    /* access modifiers changed from: package-private */
    public void setJianshePower(int jianshePower2) {
        this.jianshePower = jianshePower2;
    }

    /* access modifiers changed from: package-private */
    public int getBuffType() {
        return this.buffType;
    }

    /* access modifiers changed from: package-private */
    public float getBuffRate() {
        return this.buffRate;
    }

    /* access modifiers changed from: package-private */
    public float getBuffValue() {
        return this.buffValue;
    }

    /* access modifiers changed from: package-private */
    public float getBuffLife() {
        return this.buffLife;
    }

    /* access modifiers changed from: package-private */
    public int getAttackType() {
        return this.attackType;
    }

    public void setAttackType(int attackType2) {
        this.attackType = attackType2;
    }

    /* access modifiers changed from: package-private */
    public int getFlyRange() {
        return this.tData.getFlyRange(this.level - 1);
    }

    /* access modifiers changed from: package-private */
    public void setRange(int range2) {
        this.range = range2;
    }

    /* access modifiers changed from: package-private */
    public float getCD() {
        float nowCD = this.CD;
        for (int i = 0; i < this.eatCDTime; i++) {
            nowCD -= (this.tData.getFruitadd().getSpeed() * nowCD) / 10.0f;
        }
        return nowCD;
    }

    /* access modifiers changed from: package-private */
    public void setCD(float CD2) {
        this.CD = CD2;
    }

    public int getPower() {
        return calcuPower(this.power);
    }

    public int calcuPower(int basePower) {
        int nowPower = basePower;
        for (int i = 0; i < this.eatPowerTime; i++) {
            nowPower = (int) (((float) nowPower) + ((((float) nowPower) * this.tData.getFruitadd().getAttack()) / 10.0f));
        }
        if (RankData.useWatch()) {
            return (int) (((double) nowPower) + (((double) nowPower) * 0.2d));
        }
        return nowPower;
    }

    /* access modifiers changed from: package-private */
    public void setPower(int power2) {
        this.power = power2;
    }

    /* access modifiers changed from: package-private */
    public int getModeOffsetY() {
        return this.modeOffsetY;
    }

    /* access modifiers changed from: package-private */
    public float getBulletLife() {
        return this.bulletLife;
    }

    /* access modifiers changed from: package-private */
    public int getBulletSpeed() {
        return this.bulletSpeed;
    }

    /* access modifiers changed from: package-private */
    public int getBulletAspeed() {
        return this.bulletAspeed;
    }

    /* access modifiers changed from: package-private */
    public void setBulletLife(float bulletLife2) {
        this.bulletLife = bulletLife2;
    }

    /* access modifiers changed from: package-private */
    public void setBulletCount(int bulletCount2) {
        this.bulletCount = bulletCount2;
    }

    /* access modifiers changed from: package-private */
    public float getRunTime() {
        return this.runTime;
    }

    /* access modifiers changed from: package-private */
    public void setModeOffsetY(int modeOffsetY2) {
        this.modeOffsetY = modeOffsetY2;
    }

    public int getUpdatePrice() {
        return this.upPrice;
    }

    public int getDeletPrice() {
        return this.delPrice;
    }

    public void setDeletPrice(int delPrice2) {
        this.delPrice = delPrice2;
    }

    public float getRotateSpeed(float delta) {
        return ((180.0f * this.rotateSpeed) / 3.14f) / (1.0f / delta);
    }

    public void init(int x2, int y2, int id2, String name2) {
        this.x = x2;
        this.y = y2;
        this.level = 1;
        this.tData = Mydata.towerData.get(name2);
        this.id = id2;
        this.name = name2;
        this.w = Map.tileWidth;
        this.h = Map.tileHight;
        initLevleData();
        this.attackFrequency = 0;
        this.curStatus = 2;
        this.hurtEffect = this.tData.getHurteffect();
        this.hitSoundId = this.tData.getHitSoundId();
        this.hitSoundTowId = this.tData.getHitSoundTowId();
        this.hitComeSoundId = this.tData.getHitSoundComeId();
        this.hitBackSoundId = this.tData.getHitSoundComeId();
        addUpIcon();
        setXY();
        setFlashMode();
    }

    /* access modifiers changed from: package-private */
    public void addUpIcon() {
        if (this instanceof TowerRole) {
            this.upIcon = new Up(this.x, this.y - this.h);
        } else {
            this.upIcon = new Up(this.x, this.y - (this.h / 2));
        }
    }

    /* access modifiers changed from: package-private */
    public void setXY() {
        this.degrees = new int[]{0, this.x, this.y};
        setPosition((float) this.x, (float) (this.y - getModeOffsetY()));
    }

    /* access modifiers changed from: package-private */
    public void addStage() {
        this.base = new TowerBase();
        this.base.addDiZuo(this.name, this.x, this.y);
        GameStage.addActor(this, 2);
    }

    /* access modifiers changed from: package-private */
    public void addBuildEffect() {
        new Effect().addEffect(42, this.x, this.y, 1);
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public int getLevel() {
        return this.level;
    }

    /* access modifiers changed from: package-private */
    public void setFlashMode() {
        switch (this.attackType) {
            case 1:
            case 4:
            case 5:
            case 10:
            case 11:
            case 12:
            case 13:
            case 16:
                this.flashMode = 1;
                return;
            case 2:
            case 3:
            case 6:
            case 7:
            case 8:
            case 9:
            case 14:
            case 15:
            default:
                this.flashMode = 0;
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void initLevleData() {
        this.range = this.tData.getRange(this.level - 1);
        this.CD = this.tData.getCD(this.level - 1);
        if (!(this instanceof TowerRole)) {
            setTowerPower();
        }
        this.rotateSpeed = this.tData.getRotateSpeed(this.level - 1);
        this.buffType = this.tData.getBuff(this.level - 1).getType();
        this.buffRate = this.tData.getBuff(this.level - 1).getRate();
        this.buffValue = this.tData.getBuff(this.level - 1).getValue();
        this.buffLife = this.tData.getBuff(this.level - 1).getLife();
        this.attackType = this.tData.getAttackType(this.level - 1);
        this.modeOffsetY = this.tData.getModeOffsetY(this.level - 1);
        this.jiansheRange = this.tData.getJianSheRange(this.level - 1);
        this.explodeRange = this.tData.getExplodeRange(this.level - 1);
        this.jianshePower = this.tData.getJianShePower(this.level - 1);
        this.bulletCount = this.tData.getBulletCount(this.level - 1);
        if (this.tData.getBullet() != null) {
            if (this instanceof TowerRole) {
                this.bulletSpeed = this.tData.getBullet2().getSpeed();
            } else {
                this.bulletSpeed = this.tData.getBullet()[this.level - 1].getSpeed();
                this.bulletAspeed = this.tData.getBullet()[this.level - 1].getAspeed();
                this.bulletLife = this.tData.getBullet()[this.level - 1].getLife();
                this.space = this.tData.getBullet()[this.level - 1].getSpace();
            }
        }
        if (this.level < 3) {
            this.upPrice = this.tData.getMoney(this.level);
        } else {
            this.upPrice = -1;
        }
        this.delPrice = getSellPrice();
    }

    /* access modifiers changed from: package-private */
    public int getSellPrice() {
        int delPrice2 = 0;
        for (int i = 0; i < this.level; i++) {
            delPrice2 += this.tData.getMoney(i);
        }
        return (int) (((double) delPrice2) * 0.8d);
    }

    /* access modifiers changed from: package-private */
    public void setTowerPower() {
        if (LoadStrength.strengthData.get(this.name) != null) {
            int nowPower = LoadStrength.strengthData.get(this.name).getNowPower();
            if (this.level == 1) {
                this.power = nowPower;
            } else {
                this.power = nowPower + (this.tData.getPower(this.level - 1) - this.tData.getPower(this.level - 2));
            }
        }
    }

    public void setStatus(int status) {
        this.curStatus = status;
    }

    public void run(float delta) {
        clearDeadTarget();
        getAim();
        setFace(delta);
        runAttackFrequency(delta);
        if (this.attackFrame) {
            attackEnemy();
        }
        runTower(delta);
        runUpIcon();
    }

    /* access modifiers changed from: package-private */
    public void sort() {
        sort.sort(GameStage.getLayer(GameLayer.sprite).getChildren(), new Comparator<Actor>() {
            public int compare(Actor o1, Actor o2) {
                return o1.getY() < o2.getY() ? -1 : 1;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void clearDeadTarget() {
        if (!isChallenging() && aimIsEnemy() && this.aimIndex < Rank.enemy.size() && Rank.enemy.get(this.aimIndex).isDead()) {
            clearTarget();
        }
    }

    public void clearTarget() {
        setStatus(2);
        this.aimIndex = -1;
        this.enemyType = -1;
    }

    /* access modifiers changed from: package-private */
    public boolean aimIsEnemy() {
        return this.aimType == 0 && this.aimIndex != -1;
    }

    /* access modifiers changed from: package-private */
    public boolean aimIsDeck() {
        return this.aimType == 1 && this.aimIndex != -1 && this.aimIndex == Rank.focus;
    }

    /* access modifiers changed from: package-private */
    public boolean aimIsBoss() {
        return this.enemyType == 2 && this.aimIndex != -1;
    }

    /* access modifiers changed from: package-private */
    public void getAim() {
        if (!canTargetFoucs() && this.curStatus == 2 && !setNormalAim()) {
            this.aimIndex = -1;
            setVisible(true);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean canTargetFoucs() {
        return targetEnemyFoucs() || targetDeckFocus();
    }

    /* access modifiers changed from: package-private */
    public boolean targetEnemyFoucs() {
        if (isChallenging()) {
            return false;
        }
        if (!Rank.isEnemyFocus()) {
            return false;
        }
        if (!Rank.enemy.get(Rank.focus).canAttack()) {
            Rank.clearFocus();
            return false;
        } else if (!Rank.enemy.get(Rank.focus).inAttackArea(this.x, this.y, getRange())) {
            return false;
        } else {
            targetFocus((byte) 0);
            return true;
        }
    }

    private boolean targetDeckFocus() {
        if (isChallenging()) {
            return false;
        }
        if (!Rank.isDeckFocus()) {
            return false;
        }
        if (!Rank.deck.get(Rank.focus).canAttack()) {
            Rank.clearFocus();
            return false;
        } else if (!Rank.deck.get(Rank.focus).inAttackArea(this.x, this.y, getRange())) {
            return false;
        } else {
            targetFocus((byte) 1);
            return true;
        }
    }

    private void targetFocus(byte type) {
        if (this.aimIndex != Rank.focus || this.aimType != type) {
            setAimIndex(type, Rank.focus);
            if (isAttack()) {
                setStatus(2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean setNormalAim() {
        if (Rank.level.enemySort == null) {
            return false;
        }
        for (int index : Rank.level.enemySort) {
            if (!Rank.enemy.get(index).isFocus()) {
                if (Rank.enemy.get(index).boss) {
                    if (((Boss) Rank.enemy.get(index)).isStop()) {
                        continue;
                    } else if (Rank.isEnemyPause()) {
                        continue;
                    }
                } else if (!Rank.enemy.get(index).canAttack()) {
                    continue;
                }
                if (Rank.enemy.get(index).inAttackArea(this.x, this.y, getRange())) {
                    setAimIndex((byte) 0, index);
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setAimIndex(byte type, int aIndex) {
        this.aimType = type;
        this.aimIndex = aIndex;
        if (this.aimType != 0 || !Rank.enemy.get(this.aimIndex).boss) {
            this.enemyType = this.aimType;
        } else {
            this.enemyType = 2;
        }
    }

    /* access modifiers changed from: package-private */
    public void setFace(float delta) {
        if (this.aimIndex != -1) {
            if (((this.attackType != 13 && this.attackType != 10) || this.curStatus == 2) && this.attackType != 15) {
                if (aimIsDeck()) {
                    setAttackFace(Rank.deck.get(Rank.focus).hitX, Rank.deck.get(Rank.focus).hitY, delta);
                }
                if (aimIsEnemy() && this.aimIndex < Rank.enemy.size()) {
                    setAttackFace(Rank.enemy.get(this.aimIndex).hitX, Rank.enemy.get(this.aimIndex).hitY, delta);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setAttackFace(int ex, int ey, float delta) {
        switch (this.flashMode) {
            case 1:
            case 4:
                setDegrees(new int[]{Tools.getDegrees(this.x, this.y - getModeOffsetY(), ex, ey), this.x, this.y - getModeOffsetY()}, delta);
                break;
        }
        this.faceDir = getFaceDir(this.x, this.y - getModeOffsetY(), ex, ey);
    }

    /* access modifiers changed from: package-private */
    public int getFaceDir(int x2, int y2, int aimX, int aimY) {
        return Math.abs(aimX - x2) <= Math.abs(aimY - y2) ? aimY < y2 ? aimX < x2 ? 7 : 6 : aimX < x2 ? 5 : 4 : aimX < x2 ? 3 : 1;
    }

    private void setDegrees(int[] nextDegrees2, float delta) {
        this.nextDegrees = nextDegrees2[0];
        this.degrees[1] = nextDegrees2[1];
        this.degrees[2] = nextDegrees2[2];
        int off = nextDegrees2[0] - this.degrees[0];
        int degreesSpeed = (int) getRotateSpeed(delta);
        if (Math.abs(off) <= degreesSpeed || Math.abs(off) >= 360 - degreesSpeed) {
            this.degrees[0] = nextDegrees2[0];
        } else if (off < 0) {
            if (off <= -180) {
                int[] iArr = this.degrees;
                iArr[0] = iArr[0] + degreesSpeed;
            } else {
                int[] iArr2 = this.degrees;
                iArr2[0] = iArr2[0] - degreesSpeed;
            }
        } else if (off > 0) {
            if (off > 180) {
                int[] iArr3 = this.degrees;
                iArr3[0] = iArr3[0] - degreesSpeed;
            } else {
                int[] iArr4 = this.degrees;
                iArr4[0] = iArr4[0] + degreesSpeed;
            }
        }
        this.degrees[0] = (this.degrees[0] + PAK_ASSETS.IMG_XINJILU) % PAK_ASSETS.IMG_XINJILU;
        addAction(Actions.rotateTo((float) this.degrees[0]));
    }

    /* access modifiers changed from: package-private */
    public void runAttackFrequency(float delta) {
        this.frequencyTime -= delta;
        if (this.frequencyTime <= Animation.CurveTimeline.LINEAR && this.aimIndex != -1 && degreesToAim() && !isAttack()) {
            setStatus(3);
            this.frameTime = 0;
            this.frequencyTime = getFrequencyTime();
        }
    }

    /* access modifiers changed from: package-private */
    public float getFrequencyTime() {
        return getCD();
    }

    /* access modifiers changed from: package-private */
    public boolean isAttack() {
        return this.curStatus == 3 || this.curStatus == 5 || this.curStatus == 7;
    }

    /* access modifiers changed from: package-private */
    public boolean isChallenging() {
        return this.curStatus == 5 || this.curStatus == 7;
    }

    /* access modifiers changed from: package-private */
    public boolean degreesToAim() {
        switch (this.flashMode) {
            case 1:
            case 4:
                if (this.degrees[0] != this.nextDegrees) {
                    return false;
                }
                return true;
            case 2:
            case 3:
            default:
                return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void runTower(float delta) {
        this.attackFrame = false;
        if (isVisible()) {
            switch (this.curStatus) {
                case 2:
                    changeAnimation(getAnimationPack(), getAnimaName(), (byte) 2, false);
                    return;
                case 3:
                    if (this.frameTime == 0) {
                        changeAnimation(getAnimationPack(), getAnimaName(), (byte) 0, false);
                    }
                    this.frameTime--;
                    if (getAnimationComplete()) {
                        finishAttack();
                        return;
                    }
                    return;
                case 4:
                default:
                    return;
                case 5:
                    runTower_CHALLENGING(delta);
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void finishAttack() {
        this.attackFrame = true;
        switch (this.attackType) {
            case 9:
                setStatus(2);
                setVisible(false);
                return;
            case 10:
                setStatus(5);
                this.runTime = Animation.CurveTimeline.LINEAR;
                return;
            case 11:
            case 12:
            default:
                setStatus(2);
                return;
            case 13:
                changeAnimation(getAnimationPack(), getAnimaName(), (byte) 2, false);
                this.runTime = Animation.CurveTimeline.LINEAR;
                setStatus(5);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void runTower_CHALLENGING(float delta) {
        this.runTime += delta;
        if (this.runTime >= getBulletLife()) {
            setStatus(2);
        }
    }

    public boolean challengOver() {
        return this.runTime >= getBulletLife();
    }

    /* access modifiers changed from: package-private */
    public void attackEnemy() {
        if ("Bear2".equals(this.name)) {
            if (aimIsBoss()) {
                Bullet.addBullet(this);
            } else {
                Bullet.addBullet_num_range(this, 2, getAimRange());
            }
        } else if ("Bear4".equals(this.name)) {
            if (aimIsBoss()) {
                Bullet.addBullet(this);
            } else {
                Bullet.addBullet_num_range(this, 3, getAimRange());
            }
        } else if (this.aimIndex == -1) {
        } else {
            if (!"LengGuang".equals(this.name)) {
                Bullet.addBullet(this);
            } else if (this.bulletCount > 0) {
                Bullet.addBullet(this, this.bulletCount, 30 - (this.level * 5));
            }
        }
    }

    public void attackEnemy_Bear3(int aimIndex2, int power2) {
        this.aimType = 0;
        Bullet.addBullet_aim(this, aimIndex2, power2);
    }

    /* access modifiers changed from: package-private */
    public int[] getAimRange() {
        if (Rank.level.enemySort == null) {
            return null;
        }
        int[] aim = new int[Rank.level.enemySort.length];
        for (int i = 0; i < Rank.level.enemySort.length; i++) {
            int aimIndex2 = Rank.level.enemySort[i];
            if (!Rank.enemy.get(aimIndex2).canAttack()) {
                aim[i] = -1;
            } else if (Rank.enemy.get(aimIndex2).boss) {
                aim[i] = -1;
            } else if (Rank.enemy.get(aimIndex2).inAttackArea(this.x, this.y, getRange())) {
                aim[i] = aimIndex2;
            } else {
                aim[i] = -1;
            }
        }
        return aim;
    }

    public void addBuff(String buffName) {
        if (buffName.indexOf("Orange") != -1 || buffName.indexOf("Pear") != -1) {
            addCD();
        } else if (buffName.indexOf("Meat") != -1) {
            addPower();
        } else if (buffName.indexOf("IceCream") != -1) {
            addRange();
        }
        addBuff();
        RankData.addTowerEatNum(this.name);
    }

    public void addCD() {
        this.eatCDTime++;
        new Effect().addEffect(15, this.x, this.y, 3);
    }

    public void addPower() {
        this.eatPowerTime++;
        new Effect().addEffect(14, this.x, this.y, 3);
    }

    public void addRange() {
        this.eatRangeTime++;
        new Effect().addEffect(13, this.x, this.y, 3);
    }

    /* access modifiers changed from: package-private */
    public void addBuff() {
        if (this.eatBuff == null) {
            this.eatBuff = new EatBuff(this.x, this.y - this.modeOffsetY);
        }
    }

    /* access modifiers changed from: package-private */
    public void moveBuff(int x2, int y2) {
        if (this.eatBuff != null) {
            this.eatBuff.setPosition(x2, y2);
        }
    }

    /* access modifiers changed from: package-private */
    public void die() {
        System.out.println("tower die...");
        removeStage();
        this.dead = true;
        new Effect().addEffect(43, this.x, this.y, 3);
    }

    public void removeStage() {
        GameStage.removeActor(this);
        if (this.base != null) {
            this.base.remove();
        }
        if (this.upIcon != null) {
            this.upIcon.removeStage();
        }
        if (this.eatBuff != null) {
            this.eatBuff.remove();
        }
    }

    public boolean isDead() {
        return this.dead;
    }

    public boolean isBlock() {
        return this.block;
    }

    public void setBlock(boolean b) {
        this.block = b;
    }

    public void relieveBlock() {
        this.block = false;
    }

    public boolean update() {
        if (!canUp()) {
            return false;
        }
        int i = this.level + 1;
        this.level = i;
        setLevel(i);
        initLevleData();
        changeAnimation(getAnimationPack(), getAnimaName(), (byte) 2, false);
        setXY();
        this.base.setDizuoScale(this.level);
        moveBuff(this.x, this.y - this.modeOffsetY);
        return true;
    }

    public boolean canUp() {
        if (!this.isRandom && !isDead() && this.level < 3) {
            return true;
        }
        return false;
    }

    public String getAnimaName() {
        String n;
        if (this.curStatus == 3) {
            n = this.name + GameConstant.ANIMA_ATTACK + this.level;
        } else {
            n = this.name + GameConstant.ANIMA_STOP + this.level;
        }
        this.animationName = n;
        return n;
    }

    public String getAnimaName_stop() {
        return this.name + GameConstant.ANIMA_STOP + this.level;
    }

    /* access modifiers changed from: package-private */
    public void runUpIcon() {
        if (isBlock()) {
            this.upIcon.canNotSee();
        } else if (this.isRandom) {
            this.upIcon.canNotSee();
        } else if (this.level == 3) {
            this.upIcon.canNotSee();
        } else if (getUpdatePrice() <= Rank.money) {
            this.upIcon.canSee();
        } else {
            this.upIcon.canNotSee();
        }
    }

    public int getEatCDTime() {
        return this.eatCDTime;
    }

    public void setEatCDTime(int eatCDTime2) {
        this.eatCDTime = eatCDTime2;
    }

    public int getEatPowerTime() {
        return this.eatPowerTime;
    }

    public void setEatPowerTime(int eatPowerTime2) {
        this.eatPowerTime = eatPowerTime2;
    }

    public int getEatRangeTime() {
        return this.eatRangeTime;
    }

    public void setEatRangeTime(int eatRangeTime2) {
        this.eatRangeTime = eatRangeTime2;
    }
}
