package com.sg.td;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Buff;
import com.sg.td.actor.Effect;
import com.sg.td.data.Mydata;
import com.sg.td.kill.Kill;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;
import java.util.ArrayList;

public class Bullet implements GameConstant {
    int ImageID;
    int JianSheRange = 100;
    ArrayList<Integer> aimArray;
    int aimIndex;
    int aimType;
    int aimX;
    int aimY;
    String aniName;
    float aspeedTime;
    int atkType;
    boolean back = false;
    float backTime;
    int buffType;
    Effect bulletEffect;
    MyImage bulletImage;
    boolean canRemove = true;
    boolean circle = false;
    float circleDegree = Animation.CurveTimeline.LINEAR;
    int curIndex;
    ArrayList<Integer> deckArray;
    int[] degrees;
    String explodeAniName;
    int explodeSpeed;
    float flyRange;
    int frameLen;
    float g = 1.8f;
    int h = 10;
    String hitAniName;
    byte hitEffId = 0;
    int hitSoundId;
    String hurtEffect;
    MyImage hurtImage;
    short hurtIndex;
    short hurtInterval;
    int initAimType;
    int initAimX;
    int initAimY;
    int initX;
    int initY;
    int lineDegree;
    MyImage lineImage;
    int moveTime;
    int moveTimeMax = 20;
    float moveX;
    float moveY;
    float parabolaIndex;
    float pauseTime;
    float penHuoTime;
    int power;
    int r;
    float scaleX = 1.0f;
    float scaleY = 1.0f;
    int setAimCount = 0;
    int speed;
    int srcX;
    int srcY;
    float t;
    int totoalSetAimCount = 2;
    Tower tower;
    int towerIndex;
    int tx;
    int ty;
    int type;
    float vx;
    float vy;
    float vz;
    int w = 10;
    int x;
    int y;

    public Bullet() {
    }

    public Bullet(Tower tower2) {
        this.tower = tower2;
    }

    /* access modifiers changed from: package-private */
    public void init(Tower tower2, int aimIndex2, int power2) {
        this.tower = tower2;
        this.towerIndex = tower2.id;
        this.speed = tower2.getBulletSpeed();
        this.flyRange = ((float) tower2.getRange()) + (0.5f * ((float) tower2.getRange()));
        if (power2 == 0) {
            this.power = tower2.getPower();
        } else {
            this.power = power2;
        }
        this.aimIndex = aimIndex2;
        byte b = tower2.aimType;
        this.aimType = b;
        this.initAimType = b;
        this.x = tower2.x;
        this.y = tower2.y - tower2.getModeOffsetY();
        this.atkType = tower2.getAttackType();
        this.buffType = tower2.getBuffType();
        this.hurtEffect = tower2.hurtEffect;
        this.hitSoundId = tower2.hitSoundId;
        Sound.playSound(tower2.hitComeSoundId);
        initAtkType();
        initBullet();
        initXY(this.x, this.y);
        initMoveData();
        initAimArray();
        init_parabola_follow();
        overridesXY();
        Rank.setConsumePower();
    }

    public int getJianSheRange() {
        return this.tower.getJiansheRange();
    }

    public int getExplodeRange() {
        return this.tower.getExplodeRange();
    }

    public int getRange() {
        return this.tower.getRange();
    }

    public float getSpeed() {
        return ((float) (Rank.getGameSpeed() * this.speed)) / 60.0f;
    }

    /* access modifiers changed from: package-private */
    public void initAtkType() {
        this.frameLen = 3;
        if (this.atkType == 10 || this.atkType == 13) {
            this.frameLen = 6;
        }
    }

    /* access modifiers changed from: package-private */
    public void initBullet() {
        this.aniName = Mydata.towerData.get(this.tower.name).getAnimName();
        if (this.aniName.equals("null")) {
            this.aniName = Mydata.towerData.get(this.tower.name).getAnimName(this.tower.level - 1);
            this.hitAniName = Mydata.towerData.get(this.tower.name).getHitAnimName(this.tower.level - 1);
        }
        if (Mydata.towerData.get(this.tower.name).getExplodeBullet() != null) {
            this.explodeAniName = Mydata.towerData.get(this.tower.name).getExplodeAnimName(this.tower.level - 1);
            this.explodeSpeed = Mydata.towerData.get(this.tower.name).getExplodeSpeed(this.tower.level - 1);
        }
        if (this.aniName.indexOf(".px") != -1) {
            this.bulletEffect = new Effect();
            int layer = 6;
            if (this.atkType == 10) {
                layer = 1;
            }
            if (this.atkType == 3) {
                this.bulletEffect.addEffect(this.aniName, this.tower.x, this.tower.y, layer);
                this.bulletEffect.setScale(((float) this.tower.getRange()) / 200.0f);
                return;
            }
            this.bulletEffect.addEffect(this.aniName, this.x, this.y, layer);
            return;
        }
        if (this.atkType == 9) {
            this.lineImage = new MyImage((int) PAK_ASSETS.IMG_ZD_LIUXINGCHUI1, (float) this.x, (float) (this.y - 8), 6);
            this.lineImage.setOrigin(this.lineImage.getWidth() / 2.0f, Animation.CurveTimeline.LINEAR);
            GameStage.addActor(this.lineImage, 3);
            this.w = (int) this.lineImage.getWidth();
            this.h = (int) this.lineImage.getHeight();
        }
        this.bulletImage = new MyImage(this.aniName, (float) this.x, (float) this.y, 4);
        this.bulletImage.setOrigin(this.bulletImage.getWidth() / 2.0f, this.bulletImage.getHeight() / 2.0f);
        if (this.atkType == 8 || this.atkType == 2) {
            this.bulletImage.addAction(Actions.repeat(-1, Actions.rotateBy(360.0f, 0.3f)));
        }
        GameStage.addActor(this.bulletImage, 3);
    }

    private void initXY(int towerX, int towerY) {
        this.x = towerX;
        this.tx = towerX;
        this.y = towerY;
        this.ty = towerY;
    }

    private void initMoveData() {
        int aimX2 = getAimX();
        this.initAimX = aimX2;
        this.aimX = aimX2;
        int aimY2 = getAimY();
        this.initAimY = aimY2;
        this.aimY = aimY2;
        int a = this.aimX - this.x;
        int b = this.aimY - this.y;
        float r2 = (float) Math.sqrt((double) ((a * a) + (b * b)));
        switch (this.atkType) {
            case 10:
            case 13:
                this.degrees = new int[]{Tools.getDegrees(this.x, this.y, this.aimX, this.aimY, r2), this.x, this.y};
                move_line_through_degrees();
                return;
            case 11:
            case 12:
            default:
                this.degrees = new int[]{Tools.getDegrees(this.x, this.y, this.aimX, this.aimY, r2), this.x, this.y};
                if (a != 0) {
                    this.moveX = (float) ((int) ((((float) a) * getSpeed()) / r2));
                }
                if (b != 0) {
                    this.moveY = (float) ((int) ((((float) b) * getSpeed()) / r2));
                    return;
                }
                return;
        }
    }

    public int getAimX() {
        switch (this.aimType) {
            case 0:
                if (this.aimIndex >= Rank.enemy.size()) {
                    return this.aimX;
                }
                return Rank.enemy.get(this.aimIndex).hitX;
            case 1:
                return Rank.deck.get(this.aimIndex).hitX;
            default:
                return 0;
        }
    }

    public int getAimY() {
        switch (this.aimType) {
            case 0:
                if (this.aimIndex >= Rank.enemy.size()) {
                    return this.aimY;
                }
                return Rank.enemy.get(this.aimIndex).hitY;
            case 1:
                return Rank.deck.get(this.aimIndex).hitY;
            default:
                return 0;
        }
    }

    private void setDegree() {
        int degrees2 = 0;
        if (this.towerIndex >= Rank.tower.size() || Rank.tower.get(this.towerIndex) == null) {
            if (this.degrees != null) {
                degrees2 = this.degrees[0];
            }
        } else if (Rank.tower.get(this.towerIndex).degrees != null) {
            degrees2 = this.tower.degrees[0];
        }
        if (this.bulletEffect != null) {
            this.bulletEffect.setOrigin_center_bottom();
            this.bulletEffect.setRotation((float) degrees2);
            this.bulletEffect.setPosition(this.x, this.y);
        }
    }

    private void overridesXY() {
        if (this.tower.space == 0) {
            setDegree();
            return;
        }
        int degrees2 = 0;
        if (this.towerIndex >= Rank.tower.size() || Rank.tower.get(this.towerIndex) == null) {
            if (this.degrees != null) {
                degrees2 = this.degrees[0];
            }
        } else if (Rank.tower.get(this.towerIndex).degrees != null) {
            degrees2 = this.tower.degrees[0];
        }
        int[] offXY = Tools.getDegreesXYOfSpace(degrees2, this.tower.space);
        this.x = this.tx + offXY[0];
        this.y = this.ty + offXY[1];
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
        if (this.bulletEffect != null) {
            int a = this.aimX - this.x;
            int b = this.aimY - this.y;
            int degrees3 = Tools.getDegrees(this.x, this.y, this.aimX, this.aimY, (float) Math.sqrt((double) ((a * a) + (b * b))));
            this.bulletEffect.setOrigin_center_bottom();
            this.bulletEffect.setRotation((float) degrees3);
            this.bulletEffect.setPosition(this.x, this.y);
        }
    }

    private void initAimArray() {
        this.aimArray = new ArrayList<>();
        this.deckArray = new ArrayList<>();
        if (Rank.enemy != null) {
            for (int i = 0; i < Rank.enemy.size(); i++) {
                if (!Rank.enemy.get(i).isDead() && Rank.enemy.get(i).getVisible()) {
                    this.aimArray.add(Integer.valueOf(i));
                }
            }
        }
        if (Rank.deck != null) {
            for (int i2 = 0; i2 < Rank.deck.size(); i2++) {
                if (Rank.deck.get(i2) != null && !Rank.deck.get(i2).isDead()) {
                    this.deckArray.add(Integer.valueOf(i2));
                }
            }
        }
    }

    public void move(float delta) {
        switch (this.atkType) {
            case 1:
                run_line();
                move_line_follow();
                break;
            case 2:
                if (this.circle) {
                    createNewBullt();
                    moveCircle();
                    break;
                } else {
                    run_line_range();
                    move_line_follow();
                    break;
                }
            case 3:
                run_self();
                break;
            case 4:
                run_line_more();
                move_line_more();
                break;
            case 5:
                run_eject();
                move_line_follow();
                break;
            case 8:
                run_through();
                if (!this.back) {
                    move_line_speedUp();
                    move_comeback(delta);
                }
                if (this.back) {
                    move_huixuanbiao_line_back_speedUp();
                    break;
                }
                break;
            case 9:
                run_muchui();
                if (!this.back) {
                    move_muchui_line_follow();
                } else {
                    move_muchui_line_back();
                }
                run_muchui_degree();
                break;
            case 10:
                if (this.curIndex == 2) {
                    run_line_through_degrees();
                }
                if (this.curIndex >= this.frameLen - 1) {
                    removeStage();
                    break;
                }
                break;
            case 11:
                run_line();
                move_line_follow();
                break;
            case 12:
                run_line_range2();
                move_line_follow();
                break;
            case 13:
                this.penHuoTime += delta;
                if (this.penHuoTime > 0.3f) {
                    initAimArray();
                    this.penHuoTime = Animation.CurveTimeline.LINEAR;
                }
                run_line_through_degrees();
                if (this.tower.challengOver()) {
                    removeStage();
                    break;
                }
                break;
            case 14:
                run_rebound();
                move_line_follow();
                break;
            case 15:
                run_line_range2();
                move_parabola_follow();
                break;
            case 16:
                run_line();
                move_line_follow();
                break;
        }
        runCurIndex();
        if (!(this.bulletImage == null || this.degrees == null)) {
            this.bulletImage.setPosition((float) this.x, (float) this.y);
            switch (this.tower.flashMode) {
                case 1:
                case 4:
                    this.bulletImage.setRotation((float) this.degrees[0]);
                    break;
            }
            if (this.atkType == 14) {
                this.bulletImage.setRotation((float) this.degrees[0]);
            }
        }
        if (this.atkType == 11) {
            this.bulletEffect.setPosition(this.x, this.y);
        }
        if (this.atkType == 12 || this.atkType == 15) {
            this.bulletEffect.setPosition(this.x, this.y);
            this.bulletEffect.setRotation((float) this.degrees[0]);
        }
        if (this.atkType == 8 && this.moveX == Animation.CurveTimeline.LINEAR && this.moveY == Animation.CurveTimeline.LINEAR && this.back) {
            removeStage();
        }
        if (this.lineImage != null) {
            this.lineImage.setRotation((float) this.lineDegree);
            this.lineImage.setScale(this.scaleX, this.scaleY);
        }
    }

    private void move_line_more() {
        this.x = (int) (((float) this.x) + this.moveX);
        this.y = (int) (((float) this.y) + this.moveY);
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
    }

    private void run_line_more() {
        touch();
        if (!Tools.isDraw(this.x, this.y, this.w, this.h, (byte) 2)) {
            removeStage();
        }
    }

    private void touch() {
        touchEnemy();
        touchDeck();
        if (!Tools.isDraw(this.x, this.y, this.w, this.h, (byte) 2)) {
            removeStage();
        }
    }

    private void touchEnemy() {
        if (Rank.enemy != null) {
            for (int i = 0; i < Rank.enemy.size(); i++) {
                if (!Rank.enemy.get(i).isDead() && hit(0, i, Rank.enemy.get(i).hitX, Rank.enemy.get(i).hitY)) {
                    hurtEnemy(0, i, this.hitEffId);
                    removeStage();
                    return;
                }
            }
        }
    }

    private void touchDeck() {
        if (Rank.deck != null) {
            int i = 0;
            while (i < Rank.deck.size()) {
                if (Rank.deck.get(i) == null || Rank.deck.get(i).isDead() || !hit(1, i, Rank.deck.get(i).hitX, Rank.deck.get(i).hitY)) {
                    i++;
                } else {
                    hurtEnemy(1, i, this.hitEffId);
                    removeStage();
                    return;
                }
            }
        }
    }

    private void init_parabola_follow() {
        if (this.atkType == 15) {
            this.srcX = this.x;
            this.srcY = this.y;
            this.aimX = getAimX();
            this.aimY = getAimY();
            int deltaX = this.x - this.aimX;
            int deltaY = this.y - this.aimY;
            if (Math.abs(deltaX) > Math.abs(deltaY)) {
                this.vx = deltaX > 0 ? -getSpeed() : getSpeed();
                this.t = ((float) Math.abs(deltaX)) / Math.abs(this.vx);
                this.vy = ((float) (-deltaY)) / this.t;
            } else {
                this.vy = deltaY > 0 ? -getSpeed() : getSpeed();
                this.t = ((float) Math.abs(deltaY)) / Math.abs(this.vy);
                this.vx = ((float) (-deltaX)) / this.t;
            }
            this.vz = ((-this.g) * this.t) / 2.0f;
            this.parabolaIndex = Animation.CurveTimeline.LINEAR;
            this.canRemove = false;
        }
    }

    private void move_parabola_follow() {
        float deltaZ = (this.vz * this.parabolaIndex) + (((this.g * this.parabolaIndex) * this.parabolaIndex) / 2.0f);
        this.x = (int) (((float) this.srcX) + (this.vx * this.parabolaIndex));
        this.y = (int) (((float) this.srcY) + (this.vy * this.parabolaIndex) + deltaZ);
        float f = this.parabolaIndex + 1.0f;
        this.parabolaIndex = f;
        if (f >= this.t) {
            this.x = this.aimX;
            this.y = this.aimY;
            this.canRemove = true;
        }
    }

    public boolean canRemove() {
        return this.canRemove;
    }

    private void run_line_range2() {
        if (followHit(this.aimX, this.aimY)) {
            hurtEnemy(this.aimType, this.aimIndex, this.hitEffId);
            hurtRange(-1);
            removeStage();
            if (this.atkType == 15) {
                Kill.shakeStage();
            }
        }
    }

    private void run_line_range() {
        if (followHit(this.aimX, this.aimY)) {
            hurtEnemy(this.aimType, this.aimIndex, this.hitEffId);
            hurtRange_aim(-1, this.aimType);
            if (this.circleDegree == Animation.CurveTimeline.LINEAR) {
                this.circle = true;
                this.hitSoundId = this.tower.hitSoundTowId;
                return;
            }
            removeStage();
        }
    }

    /* access modifiers changed from: package-private */
    public void createNewBullt() {
        float radian = 0.017444445f * this.circleDegree;
        this.x = (int) (((double) this.aimX) + (Math.cos((double) radian) * ((double) this.r)));
        this.y = (int) (((double) this.aimY) + (Math.sin((double) radian) * ((double) this.r)));
        this.r = Math.min(80, this.r + 8);
        this.circleDegree += (float) (Rank.getGameSpeed() * 8);
        this.bulletImage.setTextureRegion(this.explodeAniName);
    }

    /* access modifiers changed from: package-private */
    public void moveCircle() {
        if (this.circleDegree >= 300.0f) {
            boolean aim = setNormalAim(-1, getExplodeRange());
            targetDeckFocus();
            if (aim || this.aimType == 1) {
                this.circle = false;
                this.speed = this.explodeSpeed;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean setNormalAim(int aimIndex2, int range) {
        if (Rank.isEnemyFocus()) {
            return true;
        }
        for (int index : Rank.level.enemySort) {
            if (index != aimIndex2 && !Rank.enemy.get(index).isFocus() && Rank.enemy.get(index).canAttack() && Rank.enemy.get(index).inAttackArea(this.x, this.y, range)) {
                setAimIndex((byte) 0, index);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean setDeckAim(int aimIndex2) {
        for (int i = 0; i < this.deckArray.size(); i++) {
            int deckIndex = this.deckArray.get(i).intValue();
            if (Rank.deck.get(deckIndex).isDead() || deckIndex == aimIndex2) {
                this.deckArray.remove(i);
            }
        }
        if (this.deckArray.size() == 0) {
            removeStage();
            return false;
        }
        setAimIndex((byte) 1, this.deckArray.get(Tools.nextInt(this.deckArray.size())).intValue());
        return true;
    }

    private boolean targetDeckFocus() {
        if (!Rank.isDeckFocus()) {
            return false;
        }
        if (!Rank.deck.get(Rank.focus).canAttack()) {
            Rank.clearFocus();
            return false;
        } else if (!Rank.deck.get(Rank.focus).inAttackArea(this.x, this.y, getRange())) {
            return false;
        } else {
            setAimIndex((byte) 1, Rank.focus);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void setAimIndex(byte type2, int aIndex) {
        this.aimType = type2;
        this.aimIndex = aIndex;
    }

    /* access modifiers changed from: package-private */
    public void hurtRange_aim(int hurtRangeEffect, int aimType2) {
        hurtRange_aim(this.x, this.y, hurtRangeEffect, aimType2);
    }

    /* access modifiers changed from: package-private */
    public void hurtRange_aim(int aimX2, int aimY2, int hurtRangeEffect, int aimType2) {
        if (aimType2 == 0) {
            hurtRangeEnemy(aimX2, aimY2, hurtRangeEffect);
        }
        if (aimType2 == 1) {
            hurtRangeDeck(aimX2, aimY2, hurtRangeEffect);
        }
    }

    /* access modifiers changed from: package-private */
    public void hurtRange(int hurtRangeEffect) {
        hurtRange(this.x, this.y, hurtRangeEffect);
    }

    /* access modifiers changed from: package-private */
    public void hurtRange(int aimX2, int aimY2, int hurtRangeEffect) {
        hurtRangeEnemy(aimX2, aimY2, hurtRangeEffect);
        hurtRangeDeck(aimX2, aimY2, hurtRangeEffect);
    }

    private void hurtRangeEnemy(int aimX2, int aimY2, int hurtRangeEffect) {
        if (Rank.enemy != null) {
            for (int i = 0; i < Rank.enemy.size(); i++) {
                if (!Rank.enemy.get(i).isDead() && !Rank.enemy.get(i).boss && Rank.enemy.get(i).inAttackArea(aimX2, aimY2, getJianSheRange())) {
                    hurtEnemy_Jianshe(0, i, hurtRangeEffect);
                }
            }
        }
    }

    private void hurtRangeDeck(int aimX2, int aimY2, int hurtRangeEffect) {
        if (Rank.deck != null) {
            for (int i = 0; i < Rank.deck.size(); i++) {
                if (Rank.deck.get(i) != null && !Rank.deck.get(i).isDead() && Rank.deck.get(i).inAttackArea(aimX2, aimY2, getJianSheRange())) {
                    hurtEnemy_Jianshe(1, i, hurtRangeEffect);
                }
            }
        }
    }

    private void move_line_speedUp() {
        this.aspeedTime += 1.0f;
        this.x = (int) (((float) this.x) + ((this.moveX * this.aspeedTime) / 10.0f));
        this.y = (int) (((float) this.y) + ((this.moveY * this.aspeedTime) / 10.0f));
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
        if (this.initX == 0 && this.initY == 0) {
            this.initX = this.x;
            this.initY = this.y;
        }
    }

    private void move_comeback(float delta) {
        int a = this.initX - this.x;
        int b = this.initY - this.y;
        if (((float) Math.sqrt((double) ((a * a) + (b * b)))) > this.flyRange && !this.back) {
            this.moveX = Animation.CurveTimeline.LINEAR;
            this.moveY = Animation.CurveTimeline.LINEAR;
            this.pauseTime += delta;
            if (this.pauseTime > 0.15f) {
                this.back = true;
                initAimArray();
            }
        }
    }

    private void run_through() {
        int i = 0;
        while (i < this.aimArray.size()) {
            int aimIndex2 = this.aimArray.get(i).intValue();
            if (Rank.enemy.get(aimIndex2).isDead()) {
                this.aimArray.remove(i);
            } else if (hit(0, aimIndex2, Rank.enemy.get(aimIndex2).hitX, Rank.enemy.get(aimIndex2).hitY)) {
                hurtEnemy(0, aimIndex2, this.hitEffId);
                this.aimArray.remove(i);
                i--;
            }
            i++;
        }
        int i2 = 0;
        while (i2 < this.deckArray.size()) {
            int aimIndex3 = this.deckArray.get(i2).intValue();
            if (Rank.deck.get(aimIndex3).isDead()) {
                this.deckArray.remove(i2);
            } else if (hit(1, aimIndex3, Rank.deck.get(aimIndex3).hitX, Rank.deck.get(aimIndex3).hitY)) {
                hurtEnemy(1, aimIndex3, this.hitEffId);
                this.deckArray.remove(i2);
                i2--;
            }
            i2++;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hit(int aimType2, int index, int aimX2, int aimY2) {
        switch (aimType2) {
            case 0:
                return Rank.enemy.get(index).inAttackArea(this.x, this.y, 1);
            case 1:
                return Rank.deck.get(index).inBulletArea_hit(this.x, this.y);
            default:
                return false;
        }
    }

    private void run_self() {
        int i = 0;
        while (i < this.aimArray.size()) {
            int aimIndex2 = this.aimArray.get(i).intValue();
            if (Rank.enemy.get(aimIndex2).isDead()) {
                this.aimArray.remove(i);
            } else if (Rank.enemy.get(aimIndex2).inAttackArea(this.tower.x, this.tower.y, (getRange() * (this.curIndex + 1)) / this.frameLen)) {
                hurtEnemy(0, aimIndex2, this.hitEffId);
                this.aimArray.remove(i);
                i--;
            }
            i++;
        }
        int i2 = 0;
        while (i2 < this.deckArray.size()) {
            int aimIndex3 = this.deckArray.get(i2).intValue();
            if (Rank.deck.get(aimIndex3).isDead()) {
                this.deckArray.remove(i2);
            } else if (Rank.deck.get(aimIndex3).inAttackArea(this.tower.x, this.tower.y, (getRange() * (this.curIndex + 1)) / this.frameLen)) {
                hurtEnemy(1, aimIndex3, this.hitEffId);
                this.deckArray.remove(i2);
                i2--;
            }
            i2++;
        }
        if (this.curIndex >= this.frameLen - 1) {
            removeStage();
        }
    }

    private void move_line_through_degrees() {
        float radian = ((float) (this.degrees[0] % 90)) * 0.017f;
        int moveX2 = 0;
        int moveY2 = 0;
        switch (Tools.getExactQD(this.degrees[0])) {
            case 1:
                moveX2 = (int) (((double) 640) * Math.cos((double) radian));
                moveY2 = (int) (((double) 640) * Math.sin((double) radian));
                break;
            case 2:
                moveX2 = (int) (((double) (-640)) * Math.sin((double) radian));
                moveY2 = (int) (((double) 640) * Math.cos((double) radian));
                break;
            case 3:
                moveX2 = (int) (((double) (-640)) * Math.cos((double) radian));
                moveY2 = (int) (((double) (-640)) * Math.sin((double) radian));
                break;
            case 4:
                moveX2 = (int) (((double) 640) * Math.sin((double) radian));
                moveY2 = (int) (((double) (-640)) * Math.cos((double) radian));
                break;
            case 5:
                moveX2 = 0;
                moveY2 = -640;
                break;
            case 6:
                moveX2 = 640;
                moveY2 = 0;
                break;
            case 7:
                moveX2 = 0;
                moveY2 = 640;
                break;
            case 8:
                moveX2 = -640;
                moveY2 = 0;
                break;
        }
        this.aimX = this.x + moveX2;
        this.aimY = this.y + moveY2;
    }

    private void run_line_through_degrees() {
        int i = 0;
        while (i < this.aimArray.size()) {
            int aimIndex2 = this.aimArray.get(i).intValue();
            if (Rank.enemy.get(aimIndex2).isDead()) {
                this.aimArray.remove(i);
            } else if (Tools.pointToSegment(this.x, this.y, this.aimX, this.aimY, Rank.enemy.get(aimIndex2).hitX, Rank.enemy.get(aimIndex2).hitY) < ((float) Rank.enemy.get(aimIndex2).getOffRange()) && Rank.enemy.get(aimIndex2).canAttack()) {
                hurtEnemy(0, aimIndex2, 0);
                this.aimArray.remove(i);
                i--;
            }
            i++;
        }
        int i2 = 0;
        while (i2 < this.deckArray.size()) {
            int aimIndex3 = this.deckArray.get(i2).intValue();
            if (Rank.deck.get(aimIndex3).isDead()) {
                this.deckArray.remove(i2);
            } else if (Tools.pointToSegment(this.x, this.y, this.aimX, this.aimY, Rank.deck.get(aimIndex3).hitX, Rank.deck.get(aimIndex3).hitY) < ((float) Rank.deck.get(aimIndex3).offRange)) {
                hurtEnemy(1, aimIndex3, 0);
                this.deckArray.remove(i2);
                i2--;
            }
            i2++;
        }
    }

    /* access modifiers changed from: package-private */
    public void runCurIndex() {
        int maxIndex = this.frameLen;
        int i = this.curIndex + 1;
        this.curIndex = i;
        this.curIndex = i % maxIndex;
    }

    private void run_muchui() {
        if (followHit(this.aimX, this.aimY)) {
            hurtEnemy(this.aimType, this.aimIndex, 0);
            this.back = true;
            Sound.playSound(this.tower.hitBackSoundId);
        }
    }

    private void run_line() {
        if (followHit(this.aimX, this.aimY)) {
            hurtEnemy(this.aimType, this.aimIndex, 0);
            removeStage();
        }
    }

    private void run_eject() {
        if (followHit(this.aimX, this.aimY)) {
            hurtEnemy(this.aimType, this.aimIndex, 0);
            if (this.aimType == 0) {
                hurtRange_aim(-1, this.aimType);
            }
            if (this.initAimType == 1 || this.setAimCount >= this.tower.getLevel()) {
                removeStage();
                return;
            }
            setDeckAim(this.aimIndex);
            this.setAimCount++;
            initMoveData();
            this.speed = this.explodeSpeed;
        }
    }

    private void run_rebound() {
        if (followHit(this.aimX, this.aimY)) {
            hurtEnemy(this.aimType, this.aimIndex, 0);
            this.bulletImage.setTextureRegion(this.hitAniName);
            if (this.initAimType == 1 || this.setAimCount >= this.tower.getLevel()) {
                removeStage();
                return;
            }
            setNormalAim(this.aimIndex, getExplodeRange());
            this.setAimCount++;
            initMoveData();
            return;
        }
        this.bulletImage.setTextureRegion(this.aniName);
    }

    /* access modifiers changed from: package-private */
    public void addHurtImage(int x2, int y2) {
        this.hurtImage = new MyImage(this.hitAniName, (float) x2, (float) y2, 4);
        this.hurtImage.addAction(Actions.sequence(Actions.delay(0.3f), Actions.removeActor()));
        GameStage.addActor(this.hurtImage, 2);
    }

    private void move_line_follow() {
        this.aimX = getAimX();
        this.aimY = getAimY();
        int a = this.aimX - this.x;
        int b = this.aimY - this.y;
        float r2 = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (float) ((int) ((((float) a) * getSpeed()) / r2));
        }
        if (b != 0) {
            this.moveY = (float) ((int) ((((float) b) * getSpeed()) / r2));
        }
        if (((float) Math.abs(a)) > Math.abs(this.moveX) || ((float) Math.abs(b)) > Math.abs(this.moveY)) {
            this.x = (int) (((float) this.x) + this.moveX);
            this.y = (int) (((float) this.y) + this.moveY);
        } else {
            this.x = this.aimX;
            this.y = this.aimY;
        }
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
    }

    private void move_muchui_line_back() {
        int a = this.tx - this.x;
        int b = this.ty - this.y;
        float r2 = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (float) ((int) ((((float) a) * getSpeed()) / r2));
        }
        if (b != 0) {
            this.moveY = (float) ((int) ((((float) b) * getSpeed()) / r2));
        }
        if (((float) Math.abs(a)) > Math.abs(this.moveX) || ((float) Math.abs(b)) > Math.abs(this.moveY)) {
            this.x = (int) (((float) this.x) + this.moveX);
            this.y = (int) (((float) this.y) + this.moveY);
        } else {
            this.x = this.tx;
            this.y = this.ty;
        }
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
        if (this.tx == this.x && this.ty == this.y) {
            this.moveX = Animation.CurveTimeline.LINEAR;
            this.moveY = Animation.CurveTimeline.LINEAR;
            removeStage();
            this.tower.setVisible(true);
        }
    }

    private void move_huixuanbiao_line_back_speedUp() {
        int a = this.tx - this.x;
        int b = this.ty - this.y;
        float r2 = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (float) ((int) ((((float) a) * getSpeed()) / r2));
        }
        if (b != 0) {
            this.moveY = (float) ((int) ((((float) b) * getSpeed()) / r2));
        }
        this.backTime += 1.0f;
        this.moveX = (this.moveX * this.backTime) / 10.0f;
        this.moveY = (this.moveY * this.backTime) / 10.0f;
        if (((float) Math.abs(a)) > Math.abs(this.moveX) || ((float) Math.abs(b)) > Math.abs(this.moveY)) {
            this.x = (int) (((float) this.x) + this.moveX);
            this.y = (int) (((float) this.y) + this.moveY);
        } else {
            this.x = this.tx;
            this.y = this.ty;
        }
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
        if (this.tx == this.x && this.ty == this.y) {
            this.moveX = Animation.CurveTimeline.LINEAR;
            this.moveY = Animation.CurveTimeline.LINEAR;
        }
    }

    private void move_muchui_line_follow() {
        this.aimX = getAimX();
        this.aimY = getAimY();
        int a = this.aimX - this.x;
        int b = this.aimY - this.y;
        float r2 = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (float) ((int) ((((float) a) * getSpeed()) / r2));
        }
        if (b != 0) {
            this.moveY = (float) ((int) ((((float) b) * getSpeed()) / r2));
        }
        if (((float) Math.abs(a)) > Math.abs(this.moveX) || ((float) Math.abs(b)) > Math.abs(this.moveY)) {
            this.x = (int) (((float) this.x) + this.moveX);
            this.y = (int) (((float) this.y) + this.moveY);
        } else {
            this.x = this.aimX;
            this.y = this.aimY;
        }
        if (this.degrees != null) {
            this.degrees[1] = this.x;
            this.degrees[2] = this.y;
        }
    }

    /* access modifiers changed from: package-private */
    public void run_muchui_degree() {
        float space = Tools.getSpace(this.tx, this.ty, this.x, this.y);
        this.lineDegree = Tools.getDegrees(this.x, this.y, this.tx, this.ty, space);
        this.scaleY = space / ((float) this.h);
    }

    public void removeStage() {
        GameStage.removeActor(this.bulletImage);
        Rank.bullet.remove(this);
        if (this.lineImage != null) {
            GameStage.removeActor(this.lineImage);
        }
        if ((this.atkType == 11 || this.atkType == 12 || this.atkType == 15) && this.bulletEffect != null) {
            this.bulletEffect.remove();
        }
        if (this.atkType == 13 && this.bulletEffect != null) {
            this.bulletEffect.remove();
        }
    }

    public void removeEffect() {
        if (this.bulletEffect != null) {
            this.bulletEffect.remove();
        }
    }

    /* access modifiers changed from: package-private */
    public void hurtEnemy_Jianshe(int aimType2, int enemyIndex, int eftIndex) {
        switch (aimType2) {
            case 0:
                if (enemyIndex < Rank.enemy.size()) {
                    Rank.enemy.get(enemyIndex).hurt(this.tower.getJianshePower(), this.tower.hurtEffect, this.hitSoundId, this.tower.name);
                    break;
                } else {
                    return;
                }
            case 1:
                if (enemyIndex < Rank.deck.size()) {
                    Rank.deck.get(enemyIndex).hurt(this.tower.getJianshePower(), this.tower.hurtEffect, this.hitSoundId);
                    break;
                } else {
                    return;
                }
        }
        addBuff(aimType2, enemyIndex);
    }

    /* access modifiers changed from: package-private */
    public void hurtEnemy(int aimType2, int enemyIndex, int eftIndex) {
        switch (aimType2) {
            case 0:
                if (enemyIndex < Rank.enemy.size()) {
                    Rank.enemy.get(enemyIndex).hurt(this.power, this.tower.hurtEffect, this.hitSoundId, this.tower.name);
                    break;
                } else {
                    return;
                }
            case 1:
                if (enemyIndex < Rank.deck.size()) {
                    Rank.deck.get(enemyIndex).hurt(this.power, this.tower.hurtEffect, this.hitSoundId);
                    break;
                } else {
                    return;
                }
        }
        addBuff(aimType2, enemyIndex);
    }

    /* access modifiers changed from: package-private */
    public void addBuff(int aimType2, int enemyIndex) {
        if (this.tower.getBuffType() > 0) {
            if (aimType2 == 0 && (Rank.enemy.get(enemyIndex).isDead() || !Rank.enemy.get(enemyIndex).canAttack() || Rank.enemy.get(enemyIndex).boss)) {
                return;
            }
            if (aimType2 != 1 || !Rank.deck.get(enemyIndex).isDead()) {
                new Buff().init(aimType2, enemyIndex, this.tower.getBuffType(), this.tower.getBuffRate(), this.tower.getBuffValue(), this.tower.getBuffLife(), this.tower.name);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean followHit(int aimX2, int aimY2) {
        return Tools.hit(this.x - 5, this.y - 5, 10, 10, aimX2 - 5, aimY2 - 10, 10, 10);
    }

    public boolean outScreen() {
        return !Tools.isDraw(this.x + -70, this.y + -70, 140, 140, (byte) 1);
    }

    static int addBullet(Tower tower2) {
        Bullet b = new Bullet();
        b.init(tower2, tower2.aimIndex, 0);
        Rank.bullet.add(b);
        return Rank.bullet.size() - 1;
    }

    static void addBullet(Tower tower2, int num, int offDegrees) {
        int start = ((-offDegrees) * (num - 1)) / 2;
        for (int i = 0; i < num; i++) {
            Bullet b = new Bullet();
            b.init(tower2, tower2.aimIndex, 0);
            b.setDegrees((offDegrees * i) + start + b.getDegrees());
            int[] offXY = Tools.resetMove(b.getDegrees(), b.getSpeed());
            b.moveX = (float) offXY[0];
            b.moveY = (float) offXY[1];
            Rank.bullet.add(b);
        }
    }

    public static void addBullet_num_range(Tower tower2, int num, int[] aimRange) {
        if (tower2.aimIsDeck()) {
            addBullet(tower2);
            return;
        }
        int j = 0;
        for (int i = 0; i < aimRange.length; i++) {
            if (aimRange[i] != -1 && j < num) {
                Bullet b = new Bullet();
                b.init(tower2, aimRange[i], 0);
                Rank.bullet.add(b);
                j++;
            }
        }
    }

    public static void addBullet_num(Tower tower2, int num) {
        if (tower2.aimIsDeck()) {
            addBullet(tower2);
            return;
        }
        int[] aim = getAllAim();
        int j = 0;
        for (int i = 0; i < aim.length; i++) {
            if (aim[i] != -1 && j < num) {
                Bullet b = new Bullet();
                b.init(tower2, aim[i], 0);
                Rank.bullet.add(b);
                j++;
            }
        }
    }

    static void addBullet_aim(Tower tower2, int aimIndex2, int power2) {
        Bullet b = new Bullet();
        b.init(tower2, aimIndex2, power2);
        Rank.bullet.add(b);
    }

    static int[] getAllAim() {
        if (Rank.level.enemySort == null) {
            return null;
        }
        int[] aim = new int[Rank.level.enemySort.length];
        for (int i = 0; i < Rank.level.enemySort.length; i++) {
            int aimIndex2 = Rank.level.enemySort[i];
            if (!Rank.enemy.get(aimIndex2).canAttack()) {
                aim[i] = -1;
            } else {
                aim[i] = aimIndex2;
            }
        }
        return aim;
    }

    public int getDegrees() {
        if (this.degrees == null) {
            return 0;
        }
        return this.degrees[0];
    }

    public void setDegrees(int degrees2) {
        int degrees3 = (degrees2 + PAK_ASSETS.IMG_XINJILU) % PAK_ASSETS.IMG_XINJILU;
        if (this.degrees == null) {
            this.degrees = new int[]{degrees3, this.x, this.y};
            return;
        }
        this.degrees[0] = degrees3;
    }
}
