package com.sg.td;

import com.kbz.Sound.GameSound;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;

public class Sound implements GameConstant {
    public static int getSoundId(String soundName) {
        for (int i = 0; i < PAK_ASSETS.SOUND_NAME.length; i++) {
            if (PAK_ASSETS.SOUND_NAME[i].equals(soundName)) {
                return i;
            }
        }
        return -1;
    }

    public static void playSound(int soundId) {
        if (soundId != -1) {
            GameSound.playSound(soundId);
        }
    }

    public static void playButtonPressed() {
        GameSound.playSound(5);
    }

    public static void playButtonClosed() {
        GameSound.playSound(6);
    }
}
