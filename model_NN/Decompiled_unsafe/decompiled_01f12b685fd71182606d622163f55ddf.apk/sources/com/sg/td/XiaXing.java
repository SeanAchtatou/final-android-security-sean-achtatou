package com.sg.td;

import com.datalab.tools.Constant;

public class XiaXing {
    public static String isFail;
    public static String isQianMing;
    public static SDKMessage sdkInterface;

    public static String[] initSGManager() {
        String isABCDEF = SDKMessage.unity.getConfig(Constant.AB);
        String isMMorJD = SDKMessage.unity.getConfig("value");
        isQianMing = SDKMessage.unity.getConfig(Constant.SIGN);
        isFail = SDKMessage.unity.getConfig("pause");
        return new String[]{isABCDEF, isMMorJD, isQianMing, isFail};
    }

    public static void reset() {
        SDKMessage.unity.resetPay();
    }

    public static void send(int id) {
        if (isQianMing == null || Integer.parseInt(isQianMing) != -1) {
            SDKMessage.unity.prePay(id, new int[]{0});
            return;
        }
        SDKMessage.unity.resetPay();
    }
}
