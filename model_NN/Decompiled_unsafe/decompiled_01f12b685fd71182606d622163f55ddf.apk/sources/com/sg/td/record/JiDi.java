package com.sg.td.record;

import com.sg.pak.GameConstant;
import com.sg.td.RankData;

public class JiDi implements GameConstant {
    static String descp = "阿莱达基地是斗龙宝贝的出生地，存放着各时代的恐龙资料，千帆想偷取龙印改造邪恶恐龙去改变人类世界。";
    boolean fullLevel;
    int level;
    int life;
    int money;
    int nextLife;

    public int getLevel() {
        return this.level;
    }

    public int getNextLevel() {
        return getLevel() + 1;
    }

    public int getLife() {
        return this.life;
    }

    public int getNextLife() {
        return this.nextLife;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public void setLife(int life2) {
        this.life = life2;
    }

    public void setNextLife(int nextLife2) {
        this.nextLife = nextLife2;
    }

    public int getMoney() {
        return this.money;
    }

    public void setMoney(int money2) {
        this.money = money2;
    }

    public String getDescp() {
        return descp;
    }

    public boolean isFullLevel() {
        return RankData.jidiLevel >= 11;
    }

    public String toString() {
        return "JiDi [level=" + this.level + ", life=" + this.life + ", nextLife=" + this.nextLife + ", money=" + this.money + ", descp=" + descp + "]";
    }
}
