package com.sg.td.record;

import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GRes;
import com.sg.pak.GameConstant;
import com.sg.td.record.Bear;

public class LoadBear extends LoadJsonData {
    public static ObjectMap<String, Bear> bearData = new ObjectMap<>();

    public static void loadBearDate() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Bear.json");
        if (fileString == null) {
            System.out.println("data/Bear.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String bearName = value.name;
            Bear bear = new Bear();
            JsonValue v = value.get("Levels");
            bear.levels = new Bear.Levels[v.size];
            for (int j = 0; j < v.size; j++) {
                JsonValue value2 = v.get(j);
                bear.levels[j] = new Bear.Levels();
                bear.levels[j].setLevel(getValueInt(value2, GameConstant.LEVEL));
                bear.levels[j].setAttack(getValueInt(value2, "Attack"));
                bear.levels[j].setNextAttack(getValueInt(value2, "NextAttack"));
                bear.levels[j].setDamage(getValueInt(value2, "Damage"));
                bear.levels[j].setNextDamage(getValueInt(value2, "NextDamage"));
                bear.levels[j].setMoney(getValueInt(value2, "Money"));
                bear.levels[j].setDiamond(getValueInt(value2, "Diamond"));
            }
            bear.setName(getValueString(value, "Name"));
            bear.setIntro(getValueString(value, "Intro"));
            bear.setSkill(getValueString(value, "Skill"));
            bear.setStar(getValueInt(value, "Star"));
            bear.setFullDamage(getValueInt(value, "FullDamage"));
            bear.setFullAttack(getValueInt(value, "FullAttack"));
            bear.setBullet(getValueInt(value, "Bullet"));
            bear.setRange(getValueString(value, HttpRequestHeader.Range));
            bear.setBuff(getValueString(value, "Buff"));
            bear.setDiamond(getValueInt(value, "Diamond"));
            bear.setBuyDiamond(getValueInt(value, "BuyDiamond"));
            bear.setBuyRMB(getValueInt(value, "BuyRMB"));
            bearData.put(bearName, bear);
        }
    }
}
