package com.sg.td.record;

import com.sg.pak.GameConstant;
import java.util.Arrays;

public class Get implements GameConstant {
    Days[] days;

    static class Days {
        int day;
        String inf;
        String name;

        Days() {
        }

        public int getDay() {
            return this.day;
        }

        public void setDay(int day2) {
            this.day = day2;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getInf() {
            return this.inf;
        }

        public void setInf(String inf2) {
            this.inf = inf2;
        }

        public String toString() {
            return "Days [day=" + this.day + ", name=" + this.name + ", inf=" + this.inf + "]";
        }
    }

    public Days[] getDays() {
        return this.days;
    }

    public void setDays(Days[] days2) {
        this.days = days2;
    }

    public String getCurrentName(int day) {
        return this.days[day].getName();
    }

    public String getCurrentIfn(int day) {
        return this.days[day].getInf();
    }

    public String toString() {
        return " [Days=" + Arrays.toString(this.days) + "]";
    }
}
