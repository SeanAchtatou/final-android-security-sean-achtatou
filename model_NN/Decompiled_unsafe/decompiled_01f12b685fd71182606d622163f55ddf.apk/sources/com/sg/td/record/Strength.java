package com.sg.td.record;

import com.sg.pak.GameConstant;
import com.sg.td.RankData;
import com.sg.td.data.Mydata;
import com.sg.tools.MySprite;

public class Strength implements GameConstant {
    String animName;
    String buff;
    boolean clock;
    String descp;
    int index;
    int level = 1;
    int lvUpCostAdd;
    int money;
    int nextPower;
    int nowPower;
    int power0;
    MySprite sprite;
    boolean streng;
    int strengTimes;
    int strengthLvUpCost;
    float strengthValue;
    String towerName;

    public String getIcon() {
        if (notLock()) {
            return Mydata.towerData.get(this.towerName).getTubiaoName();
        }
        return Mydata.towerData.get(this.towerName).getHuitubiaoName();
    }

    public boolean canStrength() {
        return RankData.canStrength(this.index);
    }

    public boolean notLock() {
        return RankData.towerClock[this.index];
    }

    public MySprite getSprite() {
        MySprite mySprite = new MySprite(ANIMATION_NAME[3] + ".json", this.towerName + GameConstant.ANIMA_STOP1);
        this.sprite = mySprite;
        return mySprite;
    }

    public int getLevel() {
        return RankData.towerLevel[this.index];
    }

    public String getDescp() {
        return this.descp;
    }

    public String getBuff() {
        return this.buff;
    }

    public int getNowPower() {
        float power = (float) getPower0();
        for (int i = 1; i < getLevel(); i++) {
            power += getStrengthValue() * power;
        }
        return (int) power;
    }

    public int getNextPower() {
        float power = (float) getPower0();
        for (int i = 1; i <= getLevel(); i++) {
            power += getStrengthValue() * power;
        }
        return (int) power;
    }

    public int getMoney() {
        return getStrengthLvUpCost() + ((getLevel() - 1) * getLvUpCostAdd());
    }

    public int getStrengTimes() {
        return this.strengTimes;
    }

    public String getNamePath() {
        return getTowerName().toLowerCase() + GameConstant.SUFFIX;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index2) {
        this.index = index2;
    }

    public void setDescp(String descp2) {
        this.descp = descp2;
    }

    public void setBuff(String buff2) {
        this.buff = buff2;
    }

    public void setNowPower(int nowPower2) {
        this.nowPower = nowPower2;
    }

    public void setNextPower(int nextPower2) {
        this.nextPower = nextPower2;
    }

    public void setMoney(int money2) {
        this.money = money2;
    }

    public void setStrengthValue(float strengthValue2) {
        this.strengthValue = strengthValue2;
    }

    public void setStrengthLvUpCost(int strengthLvUpCost2) {
        this.strengthLvUpCost = strengthLvUpCost2;
    }

    public void setLvUpCostAdd(int lvUpCostAdd2) {
        this.lvUpCostAdd = lvUpCostAdd2;
    }

    public int getStrengthLvUpCost() {
        return this.strengthLvUpCost;
    }

    public float getStrengthValue() {
        return this.strengthValue;
    }

    public int getLvUpCostAdd() {
        return this.lvUpCostAdd;
    }

    public int getPower0() {
        return this.power0;
    }

    public void setPower0(int power02) {
        this.power0 = power02;
    }

    public String getTowerName() {
        return this.towerName;
    }

    public void setTowerName(String towerName2) {
        this.towerName = towerName2;
    }

    public void setClock(boolean clock2) {
        this.clock = clock2;
    }

    public boolean isStreng() {
        if (this.towerName.equals("MoneyBox")) {
            return false;
        }
        return RankData.canStrength(this.index);
    }

    public void setStreng(boolean streng2) {
        this.streng = streng2;
    }

    public void setStrengTimes(int times) {
        this.strengTimes = times;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public String getAnimName() {
        return this.animName;
    }

    public void setAnimName(String animName2) {
        this.animName = animName2;
    }

    public void strength() {
        RankData.updateTower(this.index);
    }

    public String toString() {
        return "Strength [ clock=" + this.clock + ", streng=" + this.streng + ", strengTimes=" + this.strengTimes + ", level=" + this.level + ", animName=" + this.animName + ", descp=" + this.descp + ", buff=" + this.buff + ", power0=" + this.power0 + ", nowPower=" + this.nowPower + ", nextPower=" + this.nextPower + ", money=" + this.money + ", strengthValue=" + this.strengthValue + ", strengthLvUpCost=" + this.strengthLvUpCost + ", lvUpCostAdd=" + this.lvUpCostAdd + "]";
    }
}
