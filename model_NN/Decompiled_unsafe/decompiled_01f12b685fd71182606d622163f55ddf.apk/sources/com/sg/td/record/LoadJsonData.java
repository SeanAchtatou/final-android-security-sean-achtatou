package com.sg.td.record;

import com.badlogic.gdx.utils.JsonValue;
import com.kbz.esotericsoftware.spine.Animation;

public class LoadJsonData {
    static String getValueString(JsonValue value, String name) {
        try {
            return value.getString(name);
        } catch (Exception e) {
            return "null";
        }
    }

    static int getValueInt(JsonValue value, String name) {
        try {
            return value.getInt(name);
        } catch (Exception e) {
            return 0;
        }
    }

    static float getValueFloat(JsonValue value, String name) {
        try {
            return value.getFloat(name);
        } catch (Exception e) {
            return Animation.CurveTimeline.LINEAR;
        }
    }
}
