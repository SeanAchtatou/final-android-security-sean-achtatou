package com.sg.td.record;

import com.sg.pak.GameConstant;
import com.sg.td.RankData;
import java.util.Arrays;

public class TargetData implements GameConstant {
    int diamonds;
    Target[] targets;

    public static class Target {
        boolean complete;
        String desp;
        String icon;
        int id;
        int money;
        int num;
        boolean select;
        int type;

        public int getId() {
            return this.id;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public boolean unfinish() {
            return RankData.targetComplete[this.id] == 0;
        }

        public boolean isReceive() {
            return RankData.targetComplete[this.id] == 1;
        }

        public void setReceive() {
            RankData.targetComplete[this.id] = 2;
        }

        public boolean isSelect() {
            return this.select;
        }

        public void setSelect(boolean select2) {
            this.select = select2;
        }

        public boolean isComplete() {
            return this.complete;
        }

        public void setComplete(boolean complete2) {
            this.complete = complete2;
        }

        public String getDesp() {
            return this.desp;
        }

        public void setDesp(String desp2) {
            this.desp = desp2;
        }

        public int getType() {
            return this.type;
        }

        public void setType(int type2) {
            this.type = type2;
        }

        public String getIcon() {
            return this.icon;
        }

        public void setIcon(String icon2) {
            this.icon = icon2;
        }

        public int getNum() {
            return this.num;
        }

        public void setNum(int num2) {
            this.num = num2;
        }

        public int getMoney() {
            return this.money;
        }

        public void setMoney(int money2) {
            this.money = money2;
        }

        public int getCompleteDegree() {
            int Num = 0;
            switch (this.type) {
                case 0:
                    Num = RankData.fullBloodThroughTime_everday;
                    break;
                case 1:
                    Num = RankData.sellTowerTime_everday;
                    break;
                case 2:
                    Num = RankData.pickFoodTime_everday;
                    break;
                case 3:
                    Num = RankData.eatAllThroughTime_everday;
                    break;
                case 4:
                    Num = RankData.useSkillTime_everday;
                    break;
            }
            this.complete = Num >= this.num;
            if (this.complete) {
                return this.num;
            }
            return Num;
        }

        public String getDegreeDescp() {
            return "(完成度" + getCompleteDegree() + "/" + getNum() + ")";
        }

        public String toString() {
            return "Target [desp=" + this.desp + ", type=" + this.type + ", icon=" + this.icon + ", num=" + this.num + ", money=" + this.money + "]";
        }
    }

    public int getDiamonds() {
        return this.diamonds;
    }

    public void setDiamonds(int diamonds2) {
        this.diamonds = diamonds2;
    }

    public Target[] getTargets() {
        return this.targets;
    }

    public void setTargets(Target[] targets2) {
        this.targets = targets2;
    }

    public String toString() {
        return "TargetData [diamonds=" + this.diamonds + ", targets=" + Arrays.toString(this.targets) + "]";
    }
}
