package com.sg.td.record;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.kbz.AssetManger.GRes;
import com.sg.td.record.TargetData;

public class LoadTargetData extends LoadJsonData {
    public static TargetData targetData = new TargetData();

    public static void loadTargetDate() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/target.json");
        if (fileString == null) {
            System.out.println("data/target.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        targetData.setDiamonds(getValueInt(arrayJsonValue, "Diamonds"));
        JsonValue v = arrayJsonValue.get("Targets");
        TargetData.Target[] targets = new TargetData.Target[v.size];
        for (int i = 0; i < v.size; i++) {
            JsonValue value = v.get(i);
            targets[i] = new TargetData.Target();
            targets[i].setId(i);
            targets[i].setDesp(getValueString(value, "Desp"));
            targets[i].setType(getValueInt(value, "Type"));
            targets[i].setIcon(getValueString(value, "Icon"));
            targets[i].setNum(getValueInt(value, "Num"));
            targets[i].setMoney(getValueInt(value, "Money"));
        }
        targetData.setTargets(targets);
    }
}
