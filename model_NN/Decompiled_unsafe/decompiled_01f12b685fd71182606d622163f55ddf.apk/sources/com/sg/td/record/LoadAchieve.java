package com.sg.td.record;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GRes;
import com.sg.td.record.AchieveData;

public class LoadAchieve extends LoadJsonData {
    public static ObjectMap<String, AchieveData> achieveData = new ObjectMap<>();

    public static void loadAchieveData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Achieve.json");
        if (fileString == null) {
            System.out.println("data/Achieve.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        int index = 0;
        for (int i = 0; i < arrayJsonValue.size; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String name = value.name;
            AchieveData data = new AchieveData();
            JsonValue v = value.get("Achieves");
            data.achieves = new AchieveData.Achieve[v.size];
            for (int j = 0; j < v.size; j++) {
                JsonValue achieve = v.get(j);
                data.achieves[j] = new AchieveData.Achieve();
                data.achieves[j].setId(index);
                data.achieves[j].setName(getValueString(achieve, "Name"));
                data.achieves[j].setType(getValueInt(achieve, "Type"));
                data.achieves[j].setIcon(getValueString(achieve, "Icon"));
                data.achieves[j].setDescp(getValueString(achieve, "Descp"));
                data.achieves[j].setTowerName(getValueString(achieve, "Tower"));
                data.achieves[j].setRank(getValueInt(achieve, "Rank"));
                data.achieves[j].setDifficulty(getValueInt(achieve, "Difficulty"));
                data.achieves[j].setDegree(getValueInt(achieve, "Degree"));
                data.achieves[j].setAward(getValueInt(achieve, "Award"));
                data.achieves[j].setFood(getValueInt(achieve, "Food"));
                data.achieves[j].setTimes(getValueInt(achieve, "Times"));
                data.achieves[j].setNums(getValueInt(achieve, "Num"));
                index++;
            }
            achieveData.put(name, data);
        }
    }
}
