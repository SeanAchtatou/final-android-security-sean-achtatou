package com.sg.td.record;

import com.sg.pak.GameConstant;
import java.util.Arrays;

public class Bear implements GameConstant {
    String buff;
    int bullet;
    int buyDiamond;
    int buyRMB;
    int diamond;
    int fullAttack;
    int fullDamage;
    boolean fullLevel;
    String intro;
    Levels[] levels;
    String name;
    String range;
    String skill;
    int star;

    static class Levels {
        int attack;
        int damage;
        int diamond;
        int level;
        int money;
        int nextAttack;
        int nextDamage;

        Levels() {
        }

        public int getLevel() {
            return this.level;
        }

        public void setLevel(int level2) {
            this.level = level2;
        }

        public int getAttack() {
            return this.attack;
        }

        public void setAttack(int attack2) {
            this.attack = attack2;
        }

        public int getNextAttack() {
            return this.nextAttack;
        }

        public void setNextAttack(int nextAttack2) {
            this.nextAttack = nextAttack2;
        }

        public int getDamage() {
            return this.damage;
        }

        public void setDamage(int damage2) {
            this.damage = damage2;
        }

        public int getNextDamage() {
            return this.nextDamage;
        }

        public void setNextDamage(int nextDamage2) {
            this.nextDamage = nextDamage2;
        }

        public int getMoney() {
            return this.money;
        }

        public void setMoney(int money2) {
            this.money = money2;
        }

        public int getDiamond() {
            return this.diamond;
        }

        public void setDiamond(int diamond2) {
            this.diamond = diamond2;
        }

        public String toString() {
            return "Levels [level=" + this.level + ", attack=" + this.attack + ", nextAttack=" + this.nextAttack + ", damage=" + this.damage + ", nextDamage=" + this.nextDamage + ", money=" + this.money + ", diamond=" + this.diamond + "]";
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIntro() {
        return this.intro;
    }

    public void setIntro(String intro2) {
        this.intro = intro2;
    }

    public String getSkill() {
        return this.skill;
    }

    public void setSkill(String skill2) {
        this.skill = skill2;
    }

    public int getStar() {
        return this.star;
    }

    public void setStar(int star2) {
        this.star = star2;
    }

    public int getFullAttack() {
        return this.fullAttack;
    }

    public void setFullAttack(int fullAttack2) {
        this.fullAttack = fullAttack2;
    }

    public int getFullDamage() {
        return this.fullDamage;
    }

    public void setFullDamage(int fullDamage2) {
        this.fullDamage = fullDamage2;
    }

    public int getBullet() {
        return this.bullet;
    }

    public void setBullet(int bullet2) {
        this.bullet = bullet2;
    }

    public String getRange() {
        return this.range;
    }

    public void setRange(String range2) {
        this.range = range2;
    }

    public String getBuff() {
        return this.buff;
    }

    public void setBuff(String buff2) {
        this.buff = buff2;
    }

    public int getDiamond() {
        return this.diamond;
    }

    public void setDiamond(int diamond2) {
        this.diamond = diamond2;
    }

    public boolean isFullLevel(int level) {
        return level == 10;
    }

    public int getCurAttack(int level) {
        return this.levels[level - 1].getAttack();
    }

    public int getNextAttack(int level) {
        return this.levels[level - 1].getNextAttack();
    }

    public int getCurDamage(int level) {
        return this.levels[level - 1].getDamage();
    }

    public int getNextDamage(int level) {
        return this.levels[level - 1].getNextDamage();
    }

    public int getMoney(int level) {
        return this.levels[level - 1].getMoney();
    }

    public int getDiamond(int level) {
        return this.levels[level - 1].getDiamond();
    }

    public Levels[] getLevels() {
        return this.levels;
    }

    public void setLevels(Levels[] levels2) {
        this.levels = levels2;
    }

    public int getBuyDiamond() {
        return this.buyDiamond;
    }

    public void setBuyDiamond(int buyDiamond2) {
        this.buyDiamond = buyDiamond2;
    }

    public int getBuyRMB() {
        return this.buyRMB;
    }

    public void setBuyRMB(int buyRMB2) {
        this.buyRMB = buyRMB2;
    }

    public String toString() {
        return "Bear [name=" + this.name + ", intro=" + this.intro + ", skill=" + this.skill + ", star=" + this.star + ", fullAttack=" + this.fullAttack + ", fullDamage=" + this.fullDamage + ", bullet=" + this.bullet + ", range=" + this.range + ", buff=" + this.buff + ", diamond=" + this.diamond + ", levels=" + Arrays.toString(this.levels) + ", fullLevel=" + this.fullLevel + ", buyDiamond=" + this.buyDiamond + ", buyRMB=" + this.buyRMB + "]";
    }
}
