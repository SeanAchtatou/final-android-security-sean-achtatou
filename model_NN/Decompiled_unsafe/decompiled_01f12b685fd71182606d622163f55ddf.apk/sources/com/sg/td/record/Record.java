package com.sg.td.record;

import com.sg.GameRecord.GameRecord;
import com.sg.td.RankData;
import com.sg.td.group.Teach;
import com.sg.td.message.GMessage;
import com.sg.td.message.GSecretUtil;
import com.sg.td.message.MyUIData;
import java.io.IOException;

public class Record extends GameRecord {
    public void readRecord(int recordId) {
        System.out.println("读档......");
        super.readRecord(recordId);
    }

    public void writeRecord(int recordId) {
        System.err.println("存档!");
        super.writeRecord(recordId);
    }

    /* access modifiers changed from: protected */
    public void save() throws IOException {
        writeInt(RankData.eatNum);
        writeInt(RankData.pickNum);
        writeInt(RankData.buildNum);
        writeInt(RankData.skillNum_use);
        writeInt(RankData.deckNum);
        writeInt(RankData.cakeNum);
        writeInt(RankData.diamondNum);
        writeInt(RankData.honeyNum);
        writeInt(RankData.propNum_all);
        writeInt(RankData.eatAllThroughTime);
        writeInt(RankData.oneBloodThroughTime);
        writeInt(RankData.jidiLevel);
        int[] towerEatNum = new int[RankData.towerEatNum.size()];
        int[] towerHurtNum = new int[RankData.towerHurtNum.size()];
        int a = 0;
        for (Integer integer : RankData.towerEatNum.values()) {
            towerEatNum[a] = integer.intValue();
            a++;
        }
        int b = 0;
        for (Integer integer2 : RankData.towerHurtNum.values()) {
            towerHurtNum[b] = integer2.intValue();
            b++;
        }
        writeIntArray(towerEatNum);
        writeIntArray(towerHurtNum);
        writeIntArray(RankData.achieveComplete);
        writeIntArray(RankData.roleLevel);
        writeInt(RankData.fullBloodThroughTime_everday);
        writeInt(RankData.sellTowerTime_everday);
        writeInt(RankData.pickFoodTime_everday);
        writeInt(RankData.eatAllThroughTime_everday);
        writeInt(RankData.useSkillTime_everday);
        writeIntArray(RankData.targetComplete);
        writeIntArray(RankData.everyDayTargertIndex);
        writeInt(RankData.lastYear);
        writeInt(RankData.lastMonth);
        writeInt(RankData.lastDay);
        writeInt(RankData.eveyDayGetIndex);
        writeInt(RankData.todayGetIndex);
        writeInt(RankData.lastOnlineTime);
        writeBooleanArray(RankData.towerClock);
        writeIntArray(RankData.towerLevel);
        writeIntArray(RankData.props_num);
        writeIntArray(RankData.receiveAward);
        writeInt(RankData.luckDrawTimes);
        writeInt(RankData.takeTargetAward);
        writeInt(RankData.endlessPalyDays);
        writeInt(RankData.endlessPowerDays);
        writeInt(RankData.powerCard);
        writeInt(RankData.buyEndlessPaly_Year);
        writeInt(RankData.buyEndlessPaly_Month);
        writeInt(RankData.buyEndlessPaly_Day);
        writeInt(RankData.buyEndlessPower_Year);
        writeInt(RankData.buyEndlessPower_Month);
        writeInt(RankData.buyEndlessPower_Day);
        writeInt(RankData.roleRankIndex);
        writeInt(RankData.roleRankIndex_hard);
        writeIntArray2(RankData.rankStar_easy);
        writeIntArray2(RankData.rankStar_hard);
        writeBooleanArray(RankData.fullBlood);
        writeBooleanArray(RankData.cleanDeck);
        writeBooleanArray(RankData.cleanFood);
        writeBooleanArray(RankData.fullBlood_hard);
        writeBooleanArray(RankData.cleanDeck_hard);
        writeBooleanArray(RankData.cleanFood_hard);
        writeBooleanArray(Teach.hasTeach);
        writeBoolean(Boolean.valueOf(RankData.giveMoney));
        writeInt(RankData.lastSelectRoleIndex);
        writeBoolean(Boolean.valueOf(RankData.firstPaly));
        writeBooleanArray(RankData.rankDrop);
        writeBooleanArray(GMessage.isBuyed);
        writeLong(RankData.timeStart);
        writeInt(RankData.honeyNum_all);
        writeInt(RankData.heartNum_all);
        writeInt(RankData.cakeNum_all);
        writeInt(RankData.heartNum);
        writeBooleanArray(GSecretUtil.giftGet);
        writeInt(GMessage.isCalled7);
        writeInt(RankData.completeNum);
        writeBoolean(Boolean.valueOf(MyUIData.updataJCHD));
        writeInt(MyUIData.rechargeAmount);
        writeBooleanArray(MyUIData.isDuihuan);
        writeBoolean(Boolean.valueOf(MyUIData.isGethdlb));
    }

    /* access modifiers changed from: protected */
    public void read() throws IOException {
        RankData.eatNum = readInt();
        RankData.pickNum = readInt();
        RankData.buildNum = readInt();
        RankData.skillNum_use = readInt();
        RankData.deckNum = readInt();
        RankData.cakeNum = readInt();
        RankData.diamondNum = readInt();
        RankData.honeyNum = readInt();
        RankData.propNum_all = readInt();
        RankData.eatAllThroughTime = readInt();
        RankData.oneBloodThroughTime = readInt();
        RankData.jidiLevel = readInt();
        int[] towerEatNum = new int[RankData.towerEatNum.size()];
        int[] towerHurtNum = new int[RankData.towerHurtNum.size()];
        readIntArray(towerEatNum);
        readIntArray(towerHurtNum);
        RankData.readTowerData(towerEatNum, towerHurtNum);
        readIntArray(RankData.achieveComplete);
        readIntArray(RankData.roleLevel);
        RankData.fullBloodThroughTime_everday = readInt();
        RankData.sellTowerTime_everday = readInt();
        RankData.pickFoodTime_everday = readInt();
        RankData.eatAllThroughTime_everday = readInt();
        RankData.useSkillTime_everday = readInt();
        readIntArray(RankData.targetComplete);
        readIntArray(RankData.everyDayTargertIndex);
        RankData.lastYear = readInt();
        RankData.lastMonth = readInt();
        RankData.lastDay = readInt();
        RankData.eveyDayGetIndex = readInt();
        RankData.todayGetIndex = readInt();
        RankData.lastOnlineTime = readInt();
        readBooleanArray(RankData.towerClock);
        readIntArray(RankData.towerLevel);
        readIntArray(RankData.props_num);
        readIntArray(RankData.receiveAward);
        RankData.luckDrawTimes = readInt();
        RankData.takeTargetAward = readInt();
        RankData.endlessPalyDays = readInt();
        RankData.endlessPowerDays = readInt();
        RankData.powerCard = readInt();
        RankData.buyEndlessPaly_Year = readInt();
        RankData.buyEndlessPaly_Month = readInt();
        RankData.buyEndlessPaly_Day = readInt();
        RankData.buyEndlessPower_Year = readInt();
        RankData.buyEndlessPower_Month = readInt();
        RankData.buyEndlessPower_Day = readInt();
        RankData.roleRankIndex = readInt();
        RankData.roleRankIndex_hard = readInt();
        readIntArray2(RankData.rankStar_easy);
        readIntArray2(RankData.rankStar_hard);
        readBooleanArray(RankData.fullBlood);
        readBooleanArray(RankData.cleanDeck);
        readBooleanArray(RankData.cleanFood);
        readBooleanArray(RankData.fullBlood_hard);
        readBooleanArray(RankData.cleanDeck_hard);
        readBooleanArray(RankData.cleanFood_hard);
        readBooleanArray(Teach.hasTeach);
        RankData.giveMoney = readBoolean();
        RankData.lastSelectRoleIndex = readInt();
        RankData.firstPaly = readBoolean();
        readBooleanArray(RankData.rankDrop);
        readBooleanArray(GMessage.isBuyed);
        RankData.timeStart = readLong();
        RankData.honeyNum_all = readInt();
        RankData.heartNum_all = readInt();
        RankData.cakeNum_all = readInt();
        RankData.heartNum = readInt();
        readBooleanArray(GSecretUtil.giftGet);
        GMessage.isCalled7 = readInt();
        RankData.completeNum = readInt();
        MyUIData.updataJCHD = readBoolean();
        MyUIData.rechargeAmount = readInt();
        readBooleanArray(MyUIData.isDuihuan);
        MyUIData.isGethdlb = readBoolean();
    }

    public void resetRankData() {
        RankData.roleRankIndex = 1;
        RankData.roleRankIndex_hard = 1;
        for (int i = 0; i < RankData.rankStar_easy.length; i++) {
            for (int j = 0; j < RankData.rankStar_easy[i].length; j++) {
                RankData.rankStar_easy[i][j] = 0;
            }
        }
        for (int i2 = 0; i2 < RankData.rankStar_hard.length; i2++) {
            for (int j2 = 0; j2 < RankData.rankStar_hard[i2].length; j2++) {
                RankData.rankStar_hard[i2][j2] = 0;
            }
        }
        for (int i3 = 0; i3 < RankData.fullBlood.length; i3++) {
            RankData.fullBlood[i3] = false;
        }
        for (int i4 = 0; i4 < RankData.cleanDeck.length; i4++) {
            RankData.cleanDeck[i4] = false;
        }
        for (int i5 = 0; i5 < RankData.cleanFood.length; i5++) {
            RankData.cleanFood[i5] = false;
        }
        for (int i6 = 0; i6 < RankData.fullBlood_hard.length; i6++) {
            RankData.fullBlood_hard[i6] = false;
        }
        for (int i7 = 0; i7 < RankData.cleanDeck_hard.length; i7++) {
            RankData.cleanDeck_hard[i7] = false;
        }
        for (int i8 = 0; i8 < RankData.cleanFood_hard.length; i8++) {
            RankData.cleanFood_hard[i8] = false;
        }
        writeRecord(1);
        System.err.println("重置关卡数据");
    }
}
