package com.sg.td.record;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.kbz.AssetManger.GRes;
import com.sg.pak.GameConstant;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class LoadStrength extends LoadJsonData implements GameConstant {
    public static LinkedHashMap<String, Strength> strengthData = new LinkedHashMap<>();
    public static LinkedList<String> towerNameList = new LinkedList<>();

    public static void loadStrengthData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Tower.json");
        if (fileString == null) {
            System.out.println("data/Tower.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < 12; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String towerName = value.name;
            Strength strength = new Strength();
            JsonValue leves = value.get("Levels").get(0);
            strength.setIndex(i);
            strength.setTowerName(towerName);
            strength.setPower0(getValueInt(leves, "Power"));
            strength.setAnimName(getValueString(leves, "AnimName"));
            strength.setDescp(getValueString(value, "Descp"));
            strength.setBuff(getValueString(value, "Descp3"));
            strength.setStrengthValue(getValueFloat(value, "StrengthValue"));
            strength.setStrengthLvUpCost(getValueInt(value, "StrengthLvlUPCost"));
            strength.setLvUpCostAdd(getValueInt(value, "LvlUPCostAdd"));
            strengthData.put(towerName, strength);
            towerNameList.add(towerName);
        }
    }
}
