package com.sg.td.record;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GRes;
import com.sg.td.record.Get;

public class LoadGet extends LoadJsonData {
    public static ObjectMap<String, Get> getData = new ObjectMap<>();

    public static void loadGetDate() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Get.json");
        if (fileString == null) {
            System.out.println("data/Get.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String getName = value.name;
            Get get = new Get();
            JsonValue v = value.get("Days");
            get.days = new Get.Days[v.size];
            for (int j = 0; j < v.size; j++) {
                JsonValue value2 = v.get(j);
                get.days[j] = new Get.Days();
                get.days[j].setDay(getValueInt(value2, "Day"));
                get.days[j].setName(getValueString(value2, "Name"));
                get.days[j].setInf(getValueString(value2, "Inf"));
            }
            getData.put(getName, get);
        }
    }
}
