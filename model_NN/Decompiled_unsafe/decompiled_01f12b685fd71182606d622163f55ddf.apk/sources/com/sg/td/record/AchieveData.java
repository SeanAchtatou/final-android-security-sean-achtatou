package com.sg.td.record;

import com.sg.pak.GameConstant;
import com.sg.td.RankData;

public class AchieveData implements GameConstant {
    public Achieve[] achieves;

    public static class Achieve {
        int award;
        boolean complete;
        int degree;
        String descp;
        int difficulty;
        int food;
        String icon;
        public int id;
        String name;
        int nums;
        int rank;
        int times;
        String towerName;
        int type;

        public boolean unfinished() {
            return RankData.achieveComplete[getId()] == 0;
        }

        public boolean isRecieve() {
            return RankData.achieveComplete[getId()] == 1;
        }

        public void setRecieve() {
            RankData.achieveComplete[getId()] = 2;
            RankData.record.writeRecord(1);
        }

        public boolean isTaken() {
            return RankData.achieveComplete[getId()] == 2;
        }

        public int getId() {
            return this.id;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public String getTowerName() {
            return this.towerName;
        }

        public void setTowerName(String towerName2) {
            this.towerName = towerName2;
        }

        public int getNums() {
            return this.nums;
        }

        public void setNums(int nums2) {
            this.nums = nums2;
        }

        public int getTimes() {
            return this.times;
        }

        public void setTimes(int times2) {
            this.times = times2;
        }

        public int getFood() {
            return this.food;
        }

        public void setFood(int food2) {
            this.food = food2;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public int getType() {
            return this.type;
        }

        public void setType(int type2) {
            this.type = type2;
        }

        public String getIcon() {
            return this.icon;
        }

        public void setIcon(String icon2) {
            this.icon = icon2;
        }

        public String getDescp() {
            return this.descp;
        }

        public void setDescp(String descp2) {
            this.descp = descp2;
        }

        public int getRank() {
            return this.rank;
        }

        public void setRank(int rank2) {
            this.rank = rank2;
        }

        public int getDifficulty() {
            return this.difficulty;
        }

        public void setDifficulty(int difficulty2) {
            this.difficulty = difficulty2;
        }

        public int getDegree() {
            return this.degree;
        }

        public void setDegree(int degree2) {
            this.degree = degree2;
        }

        public int getAward() {
            return this.award;
        }

        public void setAward(int award2) {
            this.award = award2;
        }

        public String toString() {
            return "Achieve [name=" + this.name + ", type=" + this.type + ", icon=" + this.icon + ", descp=" + this.descp + ", rank=" + this.rank + ", difficulty=" + this.difficulty + ", degree=" + this.degree + ", award=" + this.award + "]";
        }

        public boolean isComplete() {
            return this.complete;
        }

        public int getCompleteDegree() {
            int Num;
            boolean z = true;
            int Num2 = 0;
            if (isRecieve() || isTaken()) {
                this.complete = true;
                return getDegree();
            }
            switch (getType()) {
                case 1:
                    this.complete = RankData.rankThrough(this.rank, this.difficulty);
                    break;
                case 2:
                    this.complete = RankData.rankThroughWithStar(this.rank, this.difficulty);
                    break;
                case 3:
                    Num2 = RankData.eatNum;
                    this.complete = Num2 >= this.food;
                    break;
                case 4:
                    Num2 = RankData.eatAllThroughTime;
                    if (Num2 < this.times) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 5:
                    Num2 = RankData.pickNum;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 6:
                    if (this.towerName.equals("Bear")) {
                        Num2 = RankData.getBearEatNum();
                    } else {
                        Num2 = RankData.towerEatNum.get(this.towerName).intValue();
                    }
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 7:
                    this.complete = RankData.someTowerEatFoods(this.nums);
                    break;
                case 8:
                    Num2 = RankData.buildNum;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 9:
                    this.complete = RankData.someRankPutSkill(this.nums);
                    break;
                case 10:
                    Num2 = RankData.deckNum;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 11:
                    Num2 = RankData.oneBloodThroughTime;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 12:
                case 13:
                    if (this.towerName.equals("Bear")) {
                        Num = RankData.getBearHurtNum();
                    } else {
                        Num = RankData.towerHurtNum.get(this.towerName).intValue();
                    }
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 14:
                    Num2 = RankData.cakeNum_all;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 15:
                    Num2 = RankData.honeyNum_all;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 16:
                    Num2 = RankData.heartNum_all;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 17:
                    Num2 = RankData.propNum_all;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 18:
                    Num2 = RankData.getNewRoleNum();
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 19:
                    Num2 = RankData.getFullRoleNum();
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 20:
                    Num2 = RankData.jidiLevel;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 21:
                    Num2 = RankData.completeNum;
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
                case 22:
                    Num2 = RankData.getRoleNum();
                    if (Num2 < this.nums) {
                        z = false;
                    }
                    this.complete = z;
                    break;
            }
            if (this.complete) {
                return getDegree();
            }
            return Num2;
        }

        public String getDegreeDescp() {
            return "(完成度" + getCompleteDegree() + "/" + getDegree() + ")";
        }
    }
}
