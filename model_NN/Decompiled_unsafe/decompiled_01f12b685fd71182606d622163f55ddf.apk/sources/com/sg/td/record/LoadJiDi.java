package com.sg.td.record;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GRes;
import com.sg.pak.GameConstant;

public class LoadJiDi extends LoadJsonData {
    public static ObjectMap<String, JiDi> jidiData = new ObjectMap<>();

    public static void loadJidiDate() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/jidi.json");
        if (fileString == null) {
            System.out.println("data/jidi.json" + "  = null");
            return;
        }
        JsonValue v = reader.parse(fileString).get("Levels");
        for (int i = 0; i < v.size; i++) {
            JsonValue value = v.get(i);
            JiDi jidi = new JiDi();
            int level = getValueInt(value, GameConstant.LEVEL);
            jidi.setLevel(level);
            jidi.setLife(getValueInt(value, "Life"));
            jidi.setNextLife(getValueInt(value, "NextLife"));
            jidi.setMoney(getValueInt(value, "Money"));
            jidiData.put(level + "", jidi);
        }
    }
}
