package com.sg.td;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import java.util.Random;

public class Tools implements GameConstant {
    static final /* synthetic */ boolean $assertionsDisabled;
    public static final byte LEFT_BOTTOM = 2;
    public static final byte LEFT_TOP = 1;
    public static final float PI = 3.14f;
    static Random rnd = new Random();

    static {
        boolean z;
        if (!Tools.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public static boolean inAttackRange(int ex, int ey, int tx, int ty, int trange, int offSetRange) {
        return pointAndCircle(ex, ey, tx, ty, trange + offSetRange);
    }

    private static boolean pointAndCircle(int px, int py, int cx, int cy, int r) {
        return ((cx - px) * (cx - px)) + ((cy - py) * (cy - py)) <= r * r;
    }

    public static boolean pointAndRect(int px, int py, int x, int y, int w, int h) {
        if (px <= x || px >= x + w || py <= y || py >= y + h) {
            return false;
        }
        return true;
    }

    static int getDegrees(int x1, int y1, int x2, int y2) {
        int a = Math.abs(x2 - x1);
        int b = Math.abs(y2 - y1);
        double c = Math.sqrt((double) ((a * a) + (b * b)));
        byte quadrant = getExactQD(x1, y1, x2, y2);
        int degrees = (int) ((180.0d * Math.asin(((double) a) / c)) / 3.140000104904175d);
        switch (quadrant) {
            case 1:
                return degrees;
            case 2:
                return 360 - degrees;
            case 3:
                return degrees + 180;
            case 4:
                return 180 - degrees;
            case 5:
            case 6:
            case 7:
            case 8:
                return (quadrant - 5) * 90;
            default:
                return 0;
        }
    }

    static int getDegrees(int x1, int y1, int x2, int y2, float space) {
        int a = Math.abs(x2 - x1);
        byte quadrant = getExactQD(x1, y1, x2, y2);
        int degrees = (int) ((180.0d * Math.asin((double) (((float) a) / space))) / 3.140000104904175d);
        switch (quadrant) {
            case 1:
                return degrees;
            case 2:
                return 360 - degrees;
            case 3:
                return degrees + 180;
            case 4:
                return 180 - degrees;
            case 5:
            case 6:
            case 7:
            case 8:
                return (quadrant - 5) * 90;
            default:
                return 0;
        }
    }

    static byte getExactQD(int x1, int y1, int x2, int y2) {
        int i;
        if (x1 == x2 || y1 == y2) {
            if (x1 == x2) {
                if (y2 > y1) {
                    return 7;
                }
                if (y2 < y1) {
                    return 5;
                }
            }
            if (y2 == y1) {
                if (x2 > x1) {
                    return 6;
                }
                if (x2 < x1) {
                    return 8;
                }
            }
            return 0;
        } else if (x2 > x1) {
            if (y2 > y1) {
                i = 4;
            } else {
                i = 1;
            }
            return (byte) i;
        } else {
            return (byte) (y2 > y1 ? 3 : 2);
        }
    }

    static int[] resetMove(int degrees, float speed) {
        int moveX = 0;
        int moveY = 0;
        float radian = ((float) (degrees % 90)) * 0.017f;
        switch (getExactQD(degrees)) {
            case 1:
                moveX = (int) (((double) speed) * Math.cos((double) radian));
                moveY = (int) (((double) speed) * Math.sin((double) radian));
                break;
            case 2:
                moveX = (int) (((double) (-speed)) * Math.sin((double) radian));
                moveY = (int) (((double) speed) * Math.cos((double) radian));
                break;
            case 3:
                moveX = (int) (((double) (-speed)) * Math.cos((double) radian));
                moveY = (int) (((double) (-speed)) * Math.sin((double) radian));
                break;
            case 4:
                moveX = (int) (((double) speed) * Math.sin((double) radian));
                moveY = (int) (((double) (-speed)) * Math.cos((double) radian));
                break;
            case 5:
                moveX = 0;
                moveY = (int) (-speed);
                break;
            case 6:
                moveX = (int) speed;
                moveY = 0;
                break;
            case 7:
                moveX = 0;
                moveY = (int) speed;
                break;
            case 8:
                moveX = (int) (-speed);
                moveY = 0;
                break;
        }
        return new int[]{moveX, moveY};
    }

    static int[] getDegreesXYOfSpace(int degrees, int space) {
        int moveX;
        int moveY;
        if (space == 0) {
            return new int[]{0, 0};
        }
        float radian = ((float) (degrees % 90)) * 0.017f;
        switch (getExactQD(degrees)) {
            case 1:
                moveX = (int) (((double) space) * Math.cos((double) radian));
                moveY = (int) (((double) space) * Math.sin((double) radian));
                break;
            case 2:
                moveX = (int) (((double) (-space)) * Math.sin((double) radian));
                moveY = (int) (((double) space) * Math.cos((double) radian));
                break;
            case 3:
                moveX = (int) (((double) (-space)) * Math.cos((double) radian));
                moveY = (int) (((double) (-space)) * Math.sin((double) radian));
                break;
            case 4:
                moveX = (int) (((double) space) * Math.sin((double) radian));
                moveY = (int) (((double) (-space)) * Math.cos((double) radian));
                break;
            case 5:
                moveX = 0;
                moveY = -space;
                break;
            case 6:
                moveX = space;
                moveY = 0;
                break;
            case 7:
                moveX = 0;
                moveY = space;
                break;
            case 8:
                moveX = -space;
                moveY = 0;
                break;
            default:
                moveY = 0;
                moveX = 0;
                break;
        }
        return new int[]{moveX, moveY};
    }

    static byte getExactQD(int degrees) {
        switch (degrees) {
            case 0:
                return 5;
            case 90:
                return 6;
            case 180:
                return 7;
            case PAK_ASSETS.IMG_BISHATIPSBLUE:
                return 8;
            default:
                if (degrees > 0 && degrees < 90) {
                    return 4;
                }
                if (degrees > 90 && degrees < 180) {
                    return 1;
                }
                if (degrees > 180 && degrees < 270) {
                    return 2;
                }
                if (degrees <= 270 || degrees >= 360) {
                    return 0;
                }
                return 3;
        }
    }

    public static boolean hit(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {
        return x1 < x2 + w2 && x2 < x1 + w1 && y1 < y2 + h2 && y2 < y1 + h1;
    }

    static float getSpace(int x1, int y1, int x2, int y2) {
        return (float) Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public static float pointToSegment(int x1, int y1, int x2, int y2, int x0, int y0) {
        float a = getSpace(x1, y1, x2, y2);
        float b = getSpace(x1, y1, x0, y0);
        float c = getSpace(x2, y2, x0, y0);
        if (((double) c) <= 1.0E-6d || ((double) b) <= 1.0E-6d) {
            return Animation.CurveTimeline.LINEAR;
        }
        if (((double) a) <= 1.0E-6d || c * c >= (a * a) + (b * b)) {
            return b;
        }
        if (b * b >= (a * a) + (c * c)) {
            return c;
        }
        float p = ((a + b) + c) / 2.0f;
        return (2.0f * ((float) Math.sqrt((double) ((((p - a) * p) * (p - b)) * (p - c))))) / a;
    }

    public static double PointToSegDist(double x, double y, double x1, double y1, double x2, double y2) {
        double cross = ((x2 - x1) * (x - x1)) + ((y2 - y1) * (y - y1));
        if (cross <= 0.0d) {
            return Math.sqrt(((x - x1) * (x - x1)) + ((y - y1) * (y - y1)));
        }
        double d2 = ((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1));
        if (cross >= d2) {
            return Math.sqrt(((x - x2) * (x - x2)) + ((y - y2) * (y - y2)));
        }
        double r = cross / d2;
        double px = x1 + ((x2 - x1) * r);
        double py = y1 + ((y2 - y1) * r);
        return Math.sqrt(((x - px) * (x - px)) + ((py - y1) * (py - y1)));
    }

    public static boolean circleAndCircle(int px, int py, int pr, int cx, int cy, int cr) {
        return ((cx - px) * (cx - px)) + ((cy - py) * (cy - py)) <= (pr + cr) * (pr + cr);
    }

    public static boolean circleAndRect(int cx, int cy, int cr, int rx, int ry, int rw, int rh) {
        return isCircleIntersectRectangle((float) cx, (float) cy, (float) cr, (float) ((rw / 2) + rx), (float) ((rh / 2) + ry), (float) ((rw / 2) + rx), (float) ry, (float) (rx + rw), (float) ((rh / 2) + ry));
    }

    static float DistanceBetweenTwoPoints(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt((double) (((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1))));
    }

    static float DistanceFromPointToLine(float x, float y, float x1, float y1, float x2, float y2) {
        float a = y2 - y1;
        float b = x1 - x2;
        float c = (x2 * y1) - (x1 * y2);
        if ($assertionsDisabled || Math.abs(a) > 1.0E-5f || Math.abs(b) > 1.0E-5f) {
            return (float) (((double) Math.abs(((a * x) + (b * y)) + c)) / Math.sqrt((double) ((a * a) + (b * b))));
        }
        throw new AssertionError();
    }

    static boolean isCircleIntersectRectangle(float x, float y, float r, float x0, float y0, float x1, float y1, float x2, float y2) {
        float w1 = DistanceBetweenTwoPoints(x0, y0, x2, y2);
        float h1 = DistanceBetweenTwoPoints(x0, y0, x1, y1);
        float w2 = DistanceFromPointToLine(x, y, x0, y0, x1, y1);
        float h2 = DistanceFromPointToLine(x, y, x0, y0, x2, y2);
        if (w2 <= w1 + r && h2 <= h1 + r) {
            return w2 <= w1 || h2 <= h1 || ((w2 - w1) * (w2 - w1)) + ((h2 - h1) * (h2 - h1)) <= r * r;
        }
        return false;
    }

    public static final boolean isDraw(int dx, int dy, int dw, int dh, byte anchor) {
        if (dx < (-dw) - 200 || dx > 640 + dw + 200 || dy < (-dh) - 200 || dy > GameConstant.SCREEN_HEIGHT + dh + 200) {
            return false;
        }
        return true;
    }

    public static int nextInt(int n) {
        if (n <= 0) {
            return 0;
        }
        return Math.abs(rnd.nextInt()) % n;
    }

    public static int nextInt(int n, int m) {
        if (n == m) {
            return n;
        }
        if (n > m) {
            int temp = n;
            n = m;
            m = temp;
        }
        return n + (Math.abs(rnd.nextInt()) % ((m - n) + 1));
    }

    static boolean betweenNum(int x, int num1, int num2) {
        if ((x < num1 || x >= num2) && (x <= num2 || x > num1)) {
            return false;
        }
        return true;
    }

    public static boolean inArea(int[] area, int[] ponit) {
        return ponit[0] > area[0] && ponit[0] < area[0] + area[2] && ponit[1] > area[1] && ponit[1] < area[1] + area[3];
    }
}
