package com.sg.td;

import com.sg.pak.GameConstant;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class TowerBase implements GameConstant {
    MyImage dizuo;

    /* access modifiers changed from: package-private */
    public void addDiZuo(String tName, int tx, int ty) {
        if (tName.indexOf("Random") == -1) {
            this.dizuo = new MyImage("DZ_" + tName.toLowerCase() + ".png", (float) tx, (float) (((Map.tileHight / 2) + ty) - 5), 5);
            this.dizuo.setOrigin(this.dizuo.getWidth() / 2.0f, this.dizuo.getHeight());
            GameStage.addActor(this.dizuo, 1);
            setDizuoScale(1);
        }
    }

    /* access modifiers changed from: package-private */
    public void setDizuoScale(int level) {
        if (this.dizuo != null) {
            if (level == 1) {
                this.dizuo.setScale(0.8f);
            } else if (level == 2) {
                this.dizuo.setScale(0.9f);
            } else if (level == 3) {
                this.dizuo.setScale(1.0f);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void remove() {
        if (this.dizuo != null) {
            GameStage.removeActor(this.dizuo);
        }
    }
}
