package com.sg.td;

import com.kbz.MapData.MapTileLayer;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.actor.FlyFood;
import com.sg.td.data.LevelData;
import com.sg.td.data.Mydata;
import java.util.ArrayList;
import java.util.Vector;

public class Level implements GameConstant {
    public int[] enemySort;
    float gameTime;
    LevelData.Monsters m;
    String mapName;
    ArrayList<Integer> newSort = new ArrayList<>();
    public int showWave;
    public int wave;
    public int waveMax;

    public Level(String mapName2) {
        this.mapName = mapName2;
        this.wave = 0;
        this.showWave = this.wave + 1;
        this.enemySort = null;
        this.waveMax = Rank.data.monsters.size();
    }

    public boolean isWaveOver() {
        for (int i = 0; i < Rank.enemy.size(); i++) {
            if (Rank.enemy.get(i).boss) {
                if (((Boss) Rank.enemy.get(i)).isFly()) {
                    return false;
                }
            } else if (!Rank.enemy.get(i).isDead()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void cheackWaveOver(float delta) {
        if (isWaveOver()) {
            this.gameTime += delta;
            if (this.gameTime > 2.0f) {
                this.gameTime = Animation.CurveTimeline.LINEAR;
            }
            if (this.wave < this.waveMax - 1) {
                if (this.gameTime == Animation.CurveTimeline.LINEAR) {
                    this.wave++;
                    this.showWave++;
                    initEnemy();
                    Rank.setSystemEvent((byte) 3);
                    resetWaveBoolean();
                }
            } else if (Rank.ENDLESS_MODE) {
                this.wave = 0;
            } else {
                System.out.println("刷怪结束！");
                Rank.setSystemEvent((byte) 11);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void resetWaveBoolean() {
        Rank.rankUI.resetSoundTip();
    }

    /* access modifiers changed from: package-private */
    public void initEnemy() {
        Enemy ene;
        Rank.enemy.clear();
        int counts = 0;
        if (Rank.ENDLESS_MODE) {
            Rank.enemy.add(Rank.boss);
            counts = 1;
        }
        Vector<LevelData.Monsters[]> monsters = Rank.data.monsters;
        int i = 0;
        while (true) {
            if (i < monsters.get(this.wave).length) {
                this.m = monsters.get(this.wave)[i];
                if (this.m != null) {
                    createFlyBuff(this.m);
                    int count = this.m.getCount();
                    String monsterName = this.m.getType();
                    if (this.m.getType().indexOf("Random") != -1) {
                        Vector<String> randoms = Mydata.monsterData.get(this.m.getType()).getRandoms();
                        monsterName = randoms.get(Tools.nextInt(randoms.size()));
                    }
                    for (int j = 0; j < count; j++) {
                        if ("FlyGuangTouQiang".equals(this.m.getType())) {
                            int[] skillType = new int[this.m.getSkills().length];
                            for (int k = 0; k < this.m.getSkills().length; k++) {
                                skillType[k] = this.m.getSkills()[k].getSkillType();
                            }
                            Rank.boss.init(skillType, this.m.getHp(), this.m.getSpeed(), Mydata.monsterData.get(this.m.getType()));
                        } else {
                            if (!this.m.getHoneyMonster().equals("null") && !RankData.rankDrop(Rank.getRank()) && j == count - 1) {
                                monsterName = this.m.getHoneyMonster().split(",")[0];
                            }
                            String aninName = Mydata.monsterData.get(monsterName).getAnimName();
                            if ("GuangTouQiang".equals(monsterName)) {
                                ene = new Enemy(true);
                            } else {
                                ene = new Enemy(ANIMATION_NAME[2] + ".json", aninName);
                            }
                            ene.setMonsterData(Mydata.monsterData.get(monsterName));
                            ene.init(monsterName, counts + j, this.m.getHp(), this.m.getSpeed(), this.m.getTime(), this.m.getNextTime(), this.m.getDrop(), this.m.getWave());
                            Rank.enemy.add(ene);
                        }
                    }
                    counts += count;
                }
                i++;
            } else {
                initEnemySort();
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void createFlyBuff(LevelData.Monsters m2) {
        String fly = m2.getFlyFruit();
        if (!fly.equals("null")) {
            String[] fs = fly.split("#");
            for (String split : fs) {
                String[] buff = split.split(",");
                FlyFood buff2 = new FlyFood();
                buff2.init(buff[0], Integer.parseInt(buff[1]), Integer.parseInt(buff[2]), Integer.parseInt(buff[3]), Integer.parseInt(buff[4]));
                Rank.flyFood.add(buff2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void initEnemySort() {
        if (Rank.enemy != null) {
            this.enemySort = new int[Rank.enemy.size()];
            for (int i = 0; i < Rank.enemy.size(); i++) {
                this.enemySort[i] = Rank.enemy.get(i).index;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void runEnemySort_maopao() {
        if (Rank.enemy != null && this.enemySort != null) {
            for (int i = 0; i < this.enemySort.length - 1; i++) {
                for (int j = i + 1; j < this.enemySort.length; j++) {
                    if (Rank.enemy.get(this.enemySort[i]).curPoint <= Rank.enemy.get(this.enemySort[j]).curPoint && (Rank.enemy.get(this.enemySort[i]).curPoint != Rank.enemy.get(this.enemySort[j]).curPoint || Rank.enemy.get(this.enemySort[i]).movePos < Rank.enemy.get(this.enemySort[j]).movePos)) {
                        int temp = this.enemySort[i];
                        this.enemySort[i] = this.enemySort[j];
                        this.enemySort[j] = temp;
                    }
                }
            }
        }
    }

    public void runEnemySort() {
        if (Rank.enemy != null && this.enemySort != null) {
            initEnemySort_cansee();
            int n = this.enemySort.length;
            for (int k = 0; k < n - 1; k++) {
                int max = k;
                for (int i = k + 1; i < n; i++) {
                    if (Rank.enemy.get(this.enemySort[i]).curPoint > Rank.enemy.get(this.enemySort[max]).curPoint || (Rank.enemy.get(this.enemySort[i]).curPoint == Rank.enemy.get(this.enemySort[max]).curPoint && Rank.enemy.get(this.enemySort[i]).movePos > Rank.enemy.get(this.enemySort[max]).movePos)) {
                        max = i;
                    }
                }
                if (k != max) {
                    int temp = this.enemySort[k];
                    this.enemySort[k] = this.enemySort[max];
                    this.enemySort[max] = temp;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void initEnemySort_cansee() {
        if (Rank.enemy != null) {
            this.newSort.clear();
            for (int i = 0; i < Rank.enemy.size(); i++) {
                if (Rank.enemy.get(i).canAttack()) {
                    this.newSort.add(Integer.valueOf(Rank.enemy.get(i).index));
                }
            }
            this.enemySort = new int[this.newSort.size()];
            for (int i2 = 0; i2 < this.newSort.size(); i2++) {
                this.enemySort[i2] = this.newSort.get(i2).intValue();
            }
        }
    }

    public void createEnemy(int x, int y, int skillIndex) {
        LevelData.Skill skill = this.m.getSkills()[skillIndex];
        int count = skill.getMonsterCount();
        String type = skill.getMonsterType();
        int hp = skill.getHp();
        int speed = skill.getSpeed();
        float time = skill.getMonsterTime();
        for (int i = 0; i < count; i++) {
            String monsterName = null;
            if (type.indexOf("Random") != -1) {
                Vector<String> randoms = Mydata.monsterData.get(type).getRandoms();
                monsterName = randoms.get(Tools.nextInt(randoms.size()));
            } else if (Mydata.monsterData.get(type) != null) {
                monsterName = type;
            }
            if (monsterName != null) {
                String aninName = Mydata.monsterData.get(monsterName).getAnimName();
                Enemy ene = new Enemy(ANIMATION_NAME[2] + ".json", aninName);
                ene.setMonsterData(Mydata.monsterData.get(monsterName));
                ene.init(aninName, i + 1, hp, speed, time, this.m.getNextTime(), this.m.getDrop(), this.m.getWave());
                ene.resetXY(x, y, getCurPoint(x, y));
                Rank.enemy.add(ene);
            }
        }
        initEnemySort();
    }

    /* access modifiers changed from: package-private */
    public int getCurPoint(int x, int y) {
        for (int i = 0; i < Map.path.length - 1; i++) {
            int ox = Map.path[i][0];
            int oy = Map.path[i][1];
            int ox1 = Map.path[i + 1][0];
            int oy1 = Map.path[i + 1][1];
            if (x == ox && x == ox1 && Tools.betweenNum(y, oy, oy1)) {
                return i;
            }
            if (y == oy && y == oy1 && Tools.betweenNum(x, ox, ox1)) {
                return i;
            }
            if (Tools.pointAndRect(x, y, ox, oy, MapTileLayer.tileWidth, MapTileLayer.tileHight)) {
                return i;
            }
        }
        System.err.println("getCurPoint:0");
        return 0;
    }

    public boolean isAllShow() {
        for (int i = 0; i < Rank.enemy.size(); i++) {
            if (!Rank.enemy.get(i).boss && !Rank.enemy.get(i).getVisible()) {
                return false;
            }
        }
        return true;
    }

    public boolean isPassPath_2() {
        for (int i = 0; i < Rank.enemy.size(); i++) {
            if (!Rank.enemy.get(i).isDead() && Rank.enemy.get(i).getMoveDis() > (Enemy.totalDis * 2) / 3) {
                return true;
            }
        }
        return false;
    }

    public boolean isPassPath_1() {
        for (int i = 0; i < Rank.enemy.size(); i++) {
            if (!Rank.enemy.get(i).isDead() && Rank.enemy.get(i).getMoveDis() > (Enemy.totalDis * 1) / 3) {
                return true;
            }
        }
        return false;
    }

    public boolean selectEnemy(int x, int y) {
        if (Rank.enemy == null) {
            return false;
        }
        for (short i = 0; i < Rank.enemy.size(); i = (short) (i + 1)) {
            if (Rank.enemy.get(i).canAttack()) {
                if (Tools.hit(x, y, 1, 1, Rank.enemy.get(i).x - (Rank.enemy.get(i).w / 2), (Rank.enemy.get(i).y - Rank.enemy.get(i).h) + Rank.enemy.get(i).ADD_Y, Rank.enemy.get(i).w, Rank.enemy.get(i).h)) {
                    if (Rank.enemy.get(i).isFocus()) {
                        Rank.clearFocus();
                        return true;
                    }
                    Rank.setFocus((byte) 1, i);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isEnemyArea(int x, int y) {
        if (Rank.enemy == null) {
            return false;
        }
        for (short i = 0; i < Rank.enemy.size(); i = (short) (i + 1)) {
            if (Rank.enemy.get(i).canAttack()) {
                if (Tools.hit(x, y, 1, 1, Rank.enemy.get(i).x - (Rank.enemy.get(i).w / 2), Rank.enemy.get(i).y - Rank.enemy.get(i).h, Rank.enemy.get(i).w, Rank.enemy.get(i).h)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void killAllEnemy() {
        if (Rank.enemy != null) {
            for (int i = 0; i < Rank.enemy.size(); i++) {
                if (Rank.enemy.get(i).visible && !Rank.enemy.get(i).isDead() && !Rank.enemy.get(i).boss) {
                    Rank.enemy.get(i).hurt(10000000, null, -1, null);
                }
            }
            Rank.home.resetBlood();
        }
    }

    public int getAttackEnemyNum() {
        int num = 0;
        for (int i = 0; i < Rank.enemy.size(); i++) {
            if (Rank.enemy.get(i).canAttack()) {
                num++;
            }
        }
        return num;
    }
}
