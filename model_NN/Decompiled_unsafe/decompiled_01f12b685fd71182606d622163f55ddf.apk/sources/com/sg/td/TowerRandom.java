package com.sg.td;

public class TowerRandom extends Tower {
    public TowerRandom(String animationPack, String animationName) {
        changeAnimation(animationPack, animationName, (byte) 0, false);
        this.isRandom = true;
    }

    public void init(int x, int y, int id, String name) {
        super.init(x, y, id, name);
        Sound.playSound(this.hitComeSoundId);
    }

    public void run(float delta) {
        if (getAnimationComplete()) {
            Rank.building.buildRandomTower(this.x, this.y);
            Rank.building.removeTower(this.id);
        }
    }
}
