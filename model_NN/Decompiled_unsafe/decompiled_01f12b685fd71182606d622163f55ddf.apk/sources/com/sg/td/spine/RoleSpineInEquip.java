package com.sg.td.spine;

import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.td.Map;
import com.sg.util.GameStage;

public class RoleSpineInEquip extends MySpine implements GameConstant {
    MySpine roleSpine;

    public RoleSpineInEquip(int x, int y, int spineID, float ScaleXY) {
        init(SPINE_NAME);
        createSpineRole(spineID, ScaleXY);
        setMix(0.5f);
        initMotion(motion_leiguman);
        setStatus(1);
        setPosition((float) x, (float) y);
        setId();
        setAniSpeed(1.2f);
        GameStage.addActor(this, 2);
    }

    public void setPosition(float x, float y) {
        super.setPosition(x, y + ((float) ((Map.tileHight / 2) - 10)));
    }

    public void run(float delta) {
        updata();
        this.index++;
    }

    public void act(float delta) {
        super.act(delta);
        run(delta);
    }
}
