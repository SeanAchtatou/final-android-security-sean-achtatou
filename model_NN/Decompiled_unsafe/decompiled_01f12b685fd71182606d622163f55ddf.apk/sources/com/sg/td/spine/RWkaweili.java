package com.sg.td.spine;

import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.td.Map;

public class RWkaweili extends MySpine implements GameConstant {
    public RWkaweili(int x, int y, int spineID, float ScaleXY) {
        int y2 = y + ((Map.tileHight / 2) - 10);
        init(SPINE_NAME);
        createSpineRole(spineID, ScaleXY);
        setMix(0.3f);
        initMotion(motion_RWkaweili);
        setStatus(1);
        setPosition((float) x, (float) y2);
        setId();
        setAniSpeed(1.0f);
        System.out.println("new role x:" + x + "  y:" + y2);
    }

    public void run(float delta) {
        updata();
        this.index++;
    }

    public void act(float delta) {
        super.act(delta);
        run(delta);
    }
}
