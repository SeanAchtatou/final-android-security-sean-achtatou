package com.sg.td.spine;

import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.td.Map;

public class MapLayer1 extends MySpine implements GameConstant {
    public MapLayer1(int x, int y, int spineID) {
        int y2 = y + ((Map.tileHight / 2) - 10);
        init(SPINE_NAME);
        createSpineRole(spineID, 1.0f);
        setMix(0.3f);
        initMotion(motion_maplayer1);
        setStatus(1);
        setPosition((float) x, (float) y2);
        setId();
        setAniSpeed(1.0f);
    }

    public void run(float delta) {
        updata();
        this.index++;
    }

    public void act(float delta) {
        super.act(delta);
        run(delta);
    }
}
