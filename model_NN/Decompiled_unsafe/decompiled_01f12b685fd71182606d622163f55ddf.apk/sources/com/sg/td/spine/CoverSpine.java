package com.sg.td.spine;

import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.td.Map;

public class CoverSpine extends MySpine implements GameConstant {
    public boolean isEnd;
    MySpine roleSpine;

    public CoverSpine(int x, int y, int spineID) {
        init(SPINE_NAME);
        createSpineRole(spineID, 1.0f);
        setMix(0.5f);
        initMotion(motion_Cover);
        setStatus(2);
        setPosition((float) x, (float) y);
        setId();
        setAniSpeed(1.0f);
    }

    public void setPosition(float x, float y) {
        super.setPosition(x, y + ((float) ((Map.tileHight / 2) - 10)));
    }

    public void run(float delta) {
        updata();
        this.index++;
    }

    public void act(float delta) {
        super.act(delta);
        run(delta);
        if (isEnd() && this.curStatus == 2) {
            setStatus(1);
            this.isEnd = true;
        }
    }
}
