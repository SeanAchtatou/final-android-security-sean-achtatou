package com.sg.td;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Money;
import com.sg.td.actor.TowerFrag;

public class TowerBox extends Tower {
    int[] levelAddBaseMoney = {0, 10, 20};
    int[] levelBaseMoney = {10, 20, 40};
    int levelMoney;
    int money = this.levelBaseMoney[0];

    public TowerBox(String animationPack, String animationName) {
        super(animationPack, animationName);
        this.isBox = true;
    }

    public void run(float delta) {
        this.frequencyTime += delta;
        if (this.frequencyTime >= getFrequencyTime()) {
            new Money().init(this.x, this.y - Map.tileHight, getPower(), true);
            this.frequencyTime = Animation.CurveTimeline.LINEAR;
        }
    }

    /* access modifiers changed from: package-private */
    public void addMoney() {
        this.levelMoney = this.levelBaseMoney[getLevel() - 1];
        this.money = (int) (((float) this.money) + (((float) this.levelMoney) * this.tData.getFruitadd().getAttack()));
    }

    /* access modifiers changed from: package-private */
    public void updateMoney() {
        this.money += this.levelAddBaseMoney[getLevel() - 1];
    }

    public boolean update() {
        if (!super.update()) {
            return false;
        }
        updateMoney();
        return true;
    }

    public void addCD() {
        setCD(getCD() - ((getCD() * this.tData.getFruitadd().getSpeed()) / 10.0f));
        new Effect().addEffect(17, this.x, this.y, 3);
    }

    public void addPower() {
        addMoney();
        new Effect().addEffect(16, this.x, this.y, 3);
    }

    public int getPower() {
        return this.money;
    }

    /* access modifiers changed from: package-private */
    public void setPower(int power) {
        this.money = power;
    }

    public void addRange() {
        setDeletPrice(getDeletPrice() + this.tData.getFruitadd().getRange());
        new Effect().addEffect(10, this.x, this.y, 3);
    }

    /* access modifiers changed from: package-private */
    public void setRange(int delPrice) {
        setDeletPrice(delPrice);
    }

    public int getRange() {
        return getDeletPrice();
    }

    /* access modifiers changed from: package-private */
    public void die() {
        super.die();
        createFrag();
    }

    /* access modifiers changed from: package-private */
    public void createFrag() {
        TowerFrag frag = new TowerFrag(this.x, this.y);
        frag.addToStage();
        Rank.frag.add(frag);
    }
}
