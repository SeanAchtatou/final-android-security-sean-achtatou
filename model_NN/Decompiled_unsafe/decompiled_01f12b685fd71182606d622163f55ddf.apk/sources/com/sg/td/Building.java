package com.sg.td;

import com.kbz.MapData.MapTileLayer;
import com.sg.pak.GameConstant;
import com.sg.td.actor.Effect;
import java.util.Map;

public class Building implements GameConstant {
    /* access modifiers changed from: package-private */
    public void initDeck() {
        int deckIndex = 0;
        for (int i = 0; i < Map.buildingData.size(); i++) {
            MapTileLayer.BuildingData c = (MapTileLayer.BuildingData) Map.buildingData.get(i);
            String type = c.getType();
            int x = c.getX() + 40;
            int y = c.getY() + 183;
            if (!"Hero".equals(type) && !"Tower".equals(type) && !"Fort".equals(type) && !"1_Mid_12".equals(type)) {
                Deck deck = new Deck();
                deck.init(x, y, c.getType(), deckIndex, -1, c.isFlip(), c.getTower(), c.getBuildCreate());
                deck.addDeck();
                Rank.deck.add(deck);
                deckIndex++;
                RankData.addDeckNum_rank(1);
            }
        }
    }

    public void addDeck(int x, int y, String type, int blockIndex, boolean flip, Map<String, String> dropTower, String dropString) {
        int deckIndex = Rank.deck.size();
        Deck deck = new Deck();
        deck.init(x, y, type, deckIndex, blockIndex, flip, dropTower, dropString);
        deck.addDeck();
        Rank.deck.add(deck);
    }

    public void initDefaultTower() {
        Tower t;
        for (int i = 0; i < Map.buildingData.size(); i++) {
            MapTileLayer.BuildingData c = (MapTileLayer.BuildingData) Map.buildingData.get(i);
            String type = c.getType();
            int x = c.getX() + (Map.tileWidth / 2) + 40;
            int y = c.getY() + (Map.tileHight / 2) + 183;
            if ("Tower".equals(type)) {
                if ("MoneyBox".equals(c.getName())) {
                    t = new TowerBox(ANIMATION_NAME[3] + ".json", c.getName() + GameConstant.ANIMA_STOP1);
                    System.err.println("创建的是金猪");
                } else {
                    t = new Tower(ANIMATION_NAME[3] + ".json", c.getName() + GameConstant.ANIMA_STOP1);
                }
                t.init(x, y, Rank.tower.size(), c.getName());
                t.addStage();
                t.addBuildEffect();
                if (c.getLevel() != -1) {
                    t.setLevel(c.getLevel() - 1);
                    t.update();
                }
                Rank.tower.add(t);
                System.out.println("添加炮塔");
            }
            if ("Hero".equals(type)) {
                buildRole(x, y, RankData.getSelectRoleIndex());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void buildRole(int x, int y, int roleIndex) {
        Rank.role = new TowerRole(x, y, roleIndex);
        Rank.role.init(x, y, Rank.tower.size(), ROLE_NAME[roleIndex]);
        Rank.role.addBuildEffect();
        Rank.role.setBearData();
        Rank.tower.add(Rank.role);
        Rank.setGride();
        System.out.println("添加主角...");
    }

    public void changeRole(int roleIndex) {
        Rank.role.roleRemoveStage();
        Rank.killFree();
        Rank.tower.remove(Rank.role);
        buildRole(Rank.role.x, Rank.role.y, roleIndex);
    }

    public void buildTower(int x, int y, String name, boolean count) {
        Tower t;
        if (name.indexOf("Bear") != -1) {
            buildRole(x, y, RankData.getSelectRoleIndex());
            return;
        }
        if ("MoneyBox".equals(name)) {
            t = new TowerBox(ANIMATION_NAME[3] + ".json", name + GameConstant.ANIMA_STOP1);
            System.out.println("创建的是金猪");
        } else if ("Random".equals(name)) {
            t = new TowerRandom(ANIMATION_NAME[3] + ".json", name + GameConstant.ANIMA_STOP);
        } else {
            t = new Tower(ANIMATION_NAME[3] + ".json", name + GameConstant.ANIMA_STOP1);
            System.out.println("创建的是炮塔");
        }
        t.init(x, y, Rank.tower.size(), name);
        t.addStage();
        t.addBuildEffect();
        Rank.tower.add(t);
        Rank.setGride();
        Sound.playSound(58);
        if (count) {
            RankData.addBuildNum();
        }
    }

    public void buildRandomTower(int x, int y) {
        int size = Rank.data.randomTowers.size();
        if (size > 0) {
            buildTower(x, y, Rank.data.randomTowers.get(Tools.nextInt(size)), false);
        }
    }

    /* access modifiers changed from: package-private */
    public void runTower(float delta) {
        for (int i = 0; i < Rank.tower.size(); i++) {
            if (!Rank.tower.get(i).isDead() && !Rank.tower.get(i).isBlock()) {
                Rank.tower.get(i).run(delta);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void runTowerRadom(float delta) {
        for (int i = 0; i < Rank.tower.size(); i++) {
            if (!Rank.tower.get(i).isDead() && Rank.tower.get(i).isRandom) {
                Rank.tower.get(i).run(delta);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void runTowerUp() {
        for (int i = 0; i < Rank.tower.size(); i++) {
            if (!Rank.tower.get(i).isDead()) {
                Rank.tower.get(i).runUpIcon();
            }
        }
    }

    public void moveSlectRole() {
        Rank.roleMove = true;
        Rank.setFlashGride();
    }

    public void removeSlectTower() {
        if (Rank.selectTowerIndex != -1) {
            Rank.money = Rank.tower.get(Rank.selectTowerIndex).getDeletPrice() + Rank.money;
            Rank.tower.get(Rank.selectTowerIndex).die();
            Rank.selectTowerIndex = -1;
            Rank.setGride();
            Rank.setConsumePower();
            RankData.addSellTower();
            Sound.playSound(70);
        }
    }

    public void removeTower(int index) {
        if (index < Rank.tower.size()) {
            Rank.tower.get(index).die();
            Rank.setGride();
        }
    }

    public void updateRole() {
        Rank.setRoleUpEvent();
    }

    public void updateTower() {
        int needMoney;
        if (Rank.tower.get(Rank.selectTowerIndex) instanceof TowerRole) {
            if (Rank.role.canUp()) {
                updateRole();
            }
        } else if (Rank.tower.get(Rank.selectTowerIndex).canUp() && Rank.money >= (needMoney = Rank.tower.get(Rank.selectTowerIndex).getUpdatePrice())) {
            Rank.money -= needMoney;
            Rank.tower.get(Rank.selectTowerIndex).update();
            new Effect().addEffect_Diejia(48, Rank.tower.get(Rank.selectTowerIndex).x, Rank.tower.get(Rank.selectTowerIndex).y, 3);
            Sound.playSound(80);
            Rank.setConsumePower();
        }
    }

    public int getRandomTowerIndex() {
        if (!existTower()) {
            return -1;
        }
        int index = Tools.nextInt(Rank.tower.size());
        while (true) {
            if (!Rank.tower.get(index).isDead() && !Rank.tower.get(index).isRole && !Rank.tower.get(index).isBlock()) {
                return index;
            }
            index = Tools.nextInt(Rank.tower.size());
        }
    }

    public boolean existTower() {
        for (int i = 0; i < Rank.tower.size(); i++) {
            if (!Rank.tower.get(i).isDead() && !Rank.tower.get(i).isRole && !Rank.tower.get(i).isBlock()) {
                return true;
            }
        }
        return false;
    }
}
