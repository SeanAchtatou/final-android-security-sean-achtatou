package com.sg.td.data;

import com.kbz.esotericsoftware.spine.Animation;
import java.util.Arrays;
import java.util.Vector;

public class LevelData {
    int fortHP;
    float hardAddHp = Animation.CurveTimeline.LINEAR;
    public Vector<String> hideTowers = new Vector<>();
    String introObj;
    int money;
    public Vector<Monsters[]> monsters = new Vector<>();
    float normalAddHp = Animation.CurveTimeline.LINEAR;
    public Vector<String> randomTowers = new Vector<>();
    public int[][] starTime;
    public Vector<String> towers = new Vector<>();
    int type;

    public float getNormalAddHp() {
        return this.normalAddHp;
    }

    public void setNormalAddHp(float normalAddHp2) {
        this.normalAddHp = normalAddHp2;
    }

    public String getIntroObj() {
        return this.introObj;
    }

    public float getHardAddHp() {
        return this.hardAddHp;
    }

    public void setHardAddHp(float hardAddHp2) {
        this.hardAddHp = hardAddHp2;
    }

    public void setIntroObj(String introObj2) {
        this.introObj = introObj2;
    }

    public int getFortHP() {
        return this.fortHP;
    }

    public void setFortHP(int fortHP2) {
        this.fortHP = fortHP2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public int getMoney() {
        return this.money;
    }

    public void setMoney(int money2) {
        this.money = money2;
    }

    public static class Skill {
        int hp;
        int monsterCount;
        float monsterTime;
        String monsterType;
        int skillType;
        int speed;

        public int getSkillType() {
            return this.skillType;
        }

        public void setSkillType(int skillType2) {
            this.skillType = skillType2;
        }

        public String getMonsterType() {
            return this.monsterType;
        }

        public void setMonsterType(String monsterType2) {
            this.monsterType = monsterType2;
        }

        public int getHp() {
            return this.hp;
        }

        public void setHp(int hp2) {
            this.hp = hp2;
        }

        public int getSpeed() {
            return this.speed;
        }

        public void setSpeed(int speed2) {
            this.speed = speed2;
        }

        public float getMonsterTime() {
            return this.monsterTime;
        }

        public void setMonsterTime(float monsterTime2) {
            this.monsterTime = monsterTime2;
        }

        public int getMonsterCount() {
            return this.monsterCount;
        }

        public void setMonsterCount(int monsterCount2) {
            this.monsterCount = monsterCount2;
        }

        public String toString() {
            return "Skill [skillType=" + this.skillType + ", monsterType=" + this.monsterType + ", hp=" + this.hp + ", speed=" + this.speed + ", monsterTime=" + this.monsterTime + ", monsterCount=" + this.monsterCount + "]";
        }
    }

    public static class Monsters {
        int count;
        String drop;
        String dropOnceKey;
        String flyFruit;
        String honeyMonster;
        int hp;
        float nextTime;
        Skill[] skills;
        int speed;
        float time;
        String type;
        int wave;

        public String getDrop() {
            return this.drop;
        }

        public void setDrop(String drop2) {
            this.drop = drop2;
        }

        public String getHoneyMonster() {
            return this.honeyMonster;
        }

        public void setHoneyMonster(String honeyMonster2) {
            this.honeyMonster = honeyMonster2;
        }

        public String getDropOnceKey() {
            return this.dropOnceKey;
        }

        public void setDropOnceKey(String dropOnceKey2) {
            this.dropOnceKey = dropOnceKey2;
        }

        public Skill[] getSkills() {
            return this.skills;
        }

        public void setSkills(Skill[] skills2) {
            this.skills = skills2;
        }

        public String getFlyFruit() {
            return this.flyFruit;
        }

        public void setFlyFruit(String flyFruit2) {
            this.flyFruit = flyFruit2;
        }

        public String getType() {
            return this.type;
        }

        public void setType(String type2) {
            this.type = type2;
        }

        public int getHp() {
            return this.hp;
        }

        public void setHp(int hp2) {
            this.hp = hp2;
        }

        public int getSpeed() {
            return this.speed;
        }

        public void setSpeed(int speed2) {
            this.speed = speed2;
        }

        public int getCount() {
            return this.count;
        }

        public void setCount(int count2) {
            this.count = count2;
        }

        public float getTime() {
            return this.time;
        }

        public void setTime(float time2) {
            this.time = time2;
        }

        public float getNextTime() {
            return this.nextTime;
        }

        public void setNextTime(float nextTime2) {
            this.nextTime = nextTime2;
        }

        public int getWave() {
            return this.wave;
        }

        public void setWave(int wave2) {
            this.wave = wave2;
        }

        public String toString() {
            return "Monsters [type=" + this.type + ", hp=" + this.hp + ", speed=" + this.speed + ", count=" + this.count + ", time=" + this.time + ", nextTime=" + this.nextTime + ", wave=" + this.wave + ", flyFruit=" + this.flyFruit + ", skills=" + Arrays.toString(this.skills) + "]";
        }
    }
}
