package com.sg.td.data;

import java.util.Vector;

public class MonsterData {
    String anim;
    String animName;
    int backOffsetY;
    int collideHeight;
    int collideWidth;
    int damage;
    int effectOffsetY;
    int hp;
    int hpBarY;
    int isRandom = 0;
    int money;
    int offsetY;
    Vector<String> randoms = new Vector<>();
    float scale;
    int shadowOffsetY;
    int speed;

    public Vector<String> getRandoms() {
        return this.randoms;
    }

    public void setRandoms(Vector<String> randoms2) {
        this.randoms = randoms2;
    }

    public int getIsRandom() {
        return this.isRandom;
    }

    public int getMoney() {
        return this.money;
    }

    public void setMoney(int money2) {
        this.money = money2;
    }

    public String getAnim() {
        return this.anim;
    }

    public void setAnim(String anim2) {
        this.anim = anim2;
    }

    public String getAnimName() {
        return this.animName;
    }

    public void setAnimName(String animName2) {
        this.animName = animName2;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp2) {
        this.hp = hp2;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed2) {
        this.speed = speed2;
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int damage2) {
        this.damage = damage2;
    }

    public int getCollideWidth() {
        return this.collideWidth;
    }

    public void setCollideWidth(int collideWidth2) {
        this.collideWidth = collideWidth2;
    }

    public float getScale() {
        return this.scale;
    }

    public void setScale(float scale2) {
        this.scale = scale2;
    }

    public int getCollideHeight() {
        return this.collideHeight;
    }

    public void setCollideHeight(int collideHeight2) {
        this.collideHeight = collideHeight2;
    }

    public int getOffsetY() {
        return this.offsetY;
    }

    public void setOffsetY(int offsetY2) {
        this.offsetY = offsetY2;
    }

    public int getShadowOffsetY() {
        return this.shadowOffsetY;
    }

    public void setShadowOffsetY(int shadowOffsetY2) {
        this.shadowOffsetY = shadowOffsetY2;
    }

    public int getEffectOffsetY() {
        return this.effectOffsetY;
    }

    public void setEffectOffsetY(int effectOffsetY2) {
        this.effectOffsetY = effectOffsetY2;
    }

    public int getHpBarY() {
        return this.hpBarY;
    }

    public void setHpBarY(int hpBarY2) {
        this.hpBarY = hpBarY2;
    }

    public int getBackOffsetY() {
        return this.backOffsetY;
    }

    public void setBackOffsetY(int backOffsetY2) {
        this.backOffsetY = backOffsetY2;
    }

    public boolean isRandom() {
        return this.isRandom == 1;
    }

    public void setIsRandom(int isRandom2) {
        this.isRandom = isRandom2;
    }

    public String toString() {
        return "MonsterData [anim=" + this.anim + ", animName=" + this.animName + ", hp=" + this.hp + ", money=" + this.money + ", speed=" + this.speed + ", damage=" + this.damage + ", collideWidth=" + this.collideWidth + ", scale=" + this.scale + ", collideHeight=" + this.collideHeight + ", offsetY=" + this.offsetY + ", shadowOffsetY=" + this.shadowOffsetY + ", effectOffsetY=" + this.effectOffsetY + ", hpBarY=" + this.hpBarY + ", backOffsetY=" + this.backOffsetY + ", isRandom=" + this.isRandom + "]";
    }
}
