package com.sg.td.data;

public class TowerData {
    String Bg;
    Bullet[] bullet;
    Bullet bullet2;
    String descp;
    String descp1;
    String descp2;
    String descp3;
    Bullet[] explodeBullet;
    FruitAdd fruitadd;
    int hashitanim;
    int hitSoundBackId;
    int hitSoundComeId;
    int hitSoundId;
    int hitSoundTowId;
    String huitubiaoName;
    String hurtEffect;
    String hurteffect;
    String icon;
    int index;
    Levels[] leves;
    int lvlupcostadd;
    String name;
    String shuomingName;
    String sound;
    String soundBack;
    String soundCome;
    String soundTow;
    int strengthlvlupcost;
    float strengthvalue;
    Talk[] talks;
    String tubiaoName;

    static class Levels {
        int AOERange;
        float CD;
        String anim;
        String animname;
        int attacktype;
        String bgscale;
        Buff buff;
        int bulletCount;
        int bulletOffsetY;
        int bulletwidth;
        int explodeRange;
        int flyRange;
        int jianshePower;
        int jiansheRange;
        int modeOffsetY;
        int money;
        int power;
        int range;
        float rotatespeed;

        Levels() {
        }

        public static class Buff {
            int confictId;
            String efct;
            float life;
            int priority;
            float rate;
            int type;
            float value;

            public float getRate() {
                return this.rate;
            }

            public void setRate(float rate2) {
                this.rate = rate2;
            }

            public int getType() {
                return this.type;
            }

            public void setType(int type2) {
                this.type = type2;
            }

            public float getValue() {
                return this.value;
            }

            public void setValue(float value2) {
                this.value = value2;
            }

            public float getLife() {
                return this.life;
            }

            public void setLife(float life2) {
                this.life = life2;
            }

            public int getConfictId() {
                return this.confictId;
            }

            public void setConfictId(int confictId2) {
                this.confictId = confictId2;
            }

            public int getPriority() {
                return this.priority;
            }

            public void setPriority(int priority2) {
                this.priority = priority2;
            }

            public String getEfct() {
                return this.efct;
            }

            public void setEfct(String efct2) {
                this.efct = efct2;
            }
        }

        public int getExplodeRange() {
            return this.explodeRange;
        }

        public void setExplodeRange(int explodeRange2) {
            this.explodeRange = explodeRange2;
        }

        public int getBulletCount() {
            return this.bulletCount;
        }

        public void setBulletCount(int bulletCount2) {
            this.bulletCount = bulletCount2;
        }

        public int getJiansheRange() {
            return this.jiansheRange;
        }

        public void setJiansheRange(int jiansheRange2) {
            this.jiansheRange = jiansheRange2;
        }

        public int getJianshePower() {
            return this.jianshePower;
        }

        public void setJianshePower(int jianshePower2) {
            this.jianshePower = jianshePower2;
        }

        public int getAOERange() {
            return this.AOERange;
        }

        public void setAOERange(int aOERange) {
            this.AOERange = aOERange;
        }

        public Buff getBuff() {
            return this.buff;
        }

        public void setBuff(Buff buff2) {
            this.buff = buff2;
        }

        public int getFlyRange() {
            return this.flyRange;
        }

        public void setFlyRange(int flyRange2) {
            this.flyRange = flyRange2;
        }

        public int getMoney() {
            return this.money;
        }

        public void setMoney(int money2) {
            this.money = money2;
        }

        public int getRange() {
            return this.range;
        }

        public void setRange(int range2) {
            this.range = range2;
        }

        public float getCD() {
            return this.CD;
        }

        public void setCD(float cD) {
            this.CD = cD;
        }

        public float getRotatespeed() {
            return this.rotatespeed;
        }

        public void setRotatespeed(float rotatespeed2) {
            this.rotatespeed = rotatespeed2;
        }

        public int getAttacktype() {
            return this.attacktype;
        }

        public void setAttacktype(int attacktype2) {
            this.attacktype = attacktype2;
        }

        public int getBulletwidth() {
            return this.bulletwidth;
        }

        public void setBulletwidth(int bulletwidth2) {
            this.bulletwidth = bulletwidth2;
        }

        public int getPower() {
            return this.power;
        }

        public void setPower(int power2) {
            this.power = power2;
        }

        public int getModeOffsetY() {
            return this.modeOffsetY;
        }

        public void setModeOffsetY(int modeOffsetY2) {
            this.modeOffsetY = modeOffsetY2;
        }

        public int getBulletOffsetY() {
            return this.bulletOffsetY;
        }

        public void setBulletOffsetY(int bulletOffsetY2) {
            this.bulletOffsetY = bulletOffsetY2;
        }

        public String getBgscale() {
            return this.bgscale;
        }

        public void setBgscale(String bgscale2) {
            this.bgscale = bgscale2;
        }

        public String getAnim() {
            return this.anim;
        }

        public void setAnim(String anim2) {
            this.anim = anim2;
        }

        public String getAnimname() {
            return this.animname;
        }

        public void setAnimname(String animname2) {
            this.animname = animname2;
        }
    }

    public static class Bullet {
        String anim;
        String animname;
        int animrepeat;
        int aspeed;
        String hitAnimname;
        float life;
        String scale;
        int space;
        int speed;

        public String getHitAnimname() {
            return this.hitAnimname;
        }

        public void setHitAnimname(String hitAnimname2) {
            this.hitAnimname = hitAnimname2;
        }

        public float getLife() {
            return this.life;
        }

        public void setLife(float life2) {
            this.life = life2;
        }

        public String getAnim() {
            return this.anim;
        }

        public void setAnim(String anim2) {
            this.anim = anim2;
        }

        public String getAnimname() {
            return this.animname;
        }

        public void setAnimname(String animname2) {
            this.animname = animname2;
        }

        public String getScale() {
            return this.scale;
        }

        public void setScale(String scale2) {
            this.scale = scale2;
        }

        public int getAnimrepeat() {
            return this.animrepeat;
        }

        public void setAnimrepeat(int animrepeat2) {
            this.animrepeat = animrepeat2;
        }

        public int getSpeed() {
            return this.speed;
        }

        public int getSpace() {
            return this.space;
        }

        public void setSpace(int space2) {
            this.space = space2;
        }

        public int getAspeed() {
            return this.aspeed;
        }

        public void setAspeed(int aspeed2) {
            this.aspeed = aspeed2;
        }

        public void setSpeed(int speed2) {
            this.speed = speed2;
        }
    }

    public static class FruitAdd {
        float attack;
        int range;
        float speed;

        public float getAttack() {
            return this.attack;
        }

        public void setAttack(float attack2) {
            this.attack = attack2;
        }

        public float getSpeed() {
            return this.speed;
        }

        public void setSpeed(float speed2) {
            this.speed = speed2;
        }

        public int getRange() {
            return this.range;
        }

        public void setRange(int range2) {
            this.range = range2;
        }
    }

    public Bullet[] getExplodeBullet() {
        return this.explodeBullet;
    }

    public void setExplodeBullet(Bullet[] explodeBullet2) {
        this.explodeBullet = explodeBullet2;
    }

    public Bullet getBullet2() {
        return this.bullet2;
    }

    public void setBullet2(Bullet bullet22) {
        this.bullet2 = bullet22;
    }

    public String getShuomingName() {
        return this.shuomingName;
    }

    public void setShuomingName(String shuomingName2) {
        this.shuomingName = shuomingName2;
    }

    public String getHuitubiaoName() {
        return this.huitubiaoName;
    }

    public void setHuitubiaoName(String huitubiaoName2) {
        this.huitubiaoName = huitubiaoName2;
    }

    public String getTubiaoName() {
        return this.tubiaoName;
    }

    public void setTubiaoName(String tubiaoName2) {
        this.tubiaoName = tubiaoName2;
    }

    public Talk[] getTalks() {
        return this.talks;
    }

    public void setTalks(Talk[] talks2) {
        this.talks = talks2;
    }

    public String getSoundTow() {
        return this.soundTow;
    }

    public void setSoundTow(String soundTow2) {
        this.soundTow = soundTow2;
    }

    public int getHitSoundTowId() {
        return this.hitSoundTowId;
    }

    public void setHitSoundTowId(int hitSoundTowId2) {
        this.hitSoundTowId = hitSoundTowId2;
    }

    public String getSoundCome() {
        return this.soundCome;
    }

    public void setSoundCome(String soundCome2) {
        this.soundCome = soundCome2;
    }

    public int getHitSoundComeId() {
        return this.hitSoundComeId;
    }

    public void setHitSoundComeId(int hitSoundComeId2) {
        this.hitSoundComeId = hitSoundComeId2;
    }

    public String getSoundBack() {
        return this.soundBack;
    }

    public void setSoundBack(String soundBack2) {
        this.soundBack = soundBack2;
    }

    public int getHitSoundBackId() {
        return this.hitSoundBackId;
    }

    public void setHitSoundBackId(int hitSoundBackId2) {
        this.hitSoundBackId = hitSoundBackId2;
    }

    public int getHitSoundId() {
        return this.hitSoundId;
    }

    public void setHitSoundId(int hitSoundId2) {
        this.hitSoundId = hitSoundId2;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index2) {
        this.index = index2;
    }

    public String getHurtEffect() {
        return this.hurtEffect;
    }

    public void setHurtEffect(String hurtEffect2) {
        this.hurtEffect = hurtEffect2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getDescp() {
        return this.descp;
    }

    public void setDescp(String descp4) {
        this.descp = descp4;
    }

    public String getDescp1() {
        return this.descp1;
    }

    public void setDescp1(String descp12) {
        this.descp1 = descp12;
    }

    public String getDescp2() {
        return this.descp2;
    }

    public void setDescp2(String descp22) {
        this.descp2 = descp22;
    }

    public String getDescp3() {
        return this.descp3;
    }

    public void setDescp3(String descp32) {
        this.descp3 = descp32;
    }

    public String getBg() {
        return this.Bg;
    }

    public void setBg(String bg) {
        this.Bg = bg;
    }

    public int getHashitanim() {
        return this.hashitanim;
    }

    public void setHashitanim(int hashitanim2) {
        this.hashitanim = hashitanim2;
    }

    public float getStrengthvalue() {
        return this.strengthvalue;
    }

    public void setStrengthvalue(float strengthvalue2) {
        this.strengthvalue = strengthvalue2;
    }

    public int getStrengthlvlupcost() {
        return this.strengthlvlupcost;
    }

    public void setStrengthlvlupcost(int strengthlvlupcost2) {
        this.strengthlvlupcost = strengthlvlupcost2;
    }

    public int getLvlupcostadd() {
        return this.lvlupcostadd;
    }

    public void setLvlupcostadd(int lvlupcostadd2) {
        this.lvlupcostadd = lvlupcostadd2;
    }

    public String getHurteffect() {
        return this.hurteffect;
    }

    public void setHurteffect(String hurteffect2) {
        this.hurteffect = hurteffect2;
    }

    public String getSound() {
        return this.sound;
    }

    public void setSound(String sound2) {
        this.sound = sound2;
    }

    public Levels[] getLeves() {
        return this.leves;
    }

    public int getFlyRange(int level) {
        return getLeves()[level].getFlyRange();
    }

    public int getJianSheRange(int level) {
        return getLeves()[level].getJiansheRange();
    }

    public int getExplodeRange(int level) {
        return getLeves()[level].getExplodeRange();
    }

    public int getRange(int level) {
        return getLeves()[level].getRange();
    }

    public int getAOERange(int level) {
        return getLeves()[level].getAOERange();
    }

    public int getAttackType(int level) {
        return getLeves()[level].getAttacktype();
    }

    public int getPower(int level) {
        return getLeves()[level].getPower();
    }

    public int getMoney(int level) {
        return getLeves()[level].getMoney();
    }

    public int getJianShePower(int level) {
        return getLeves()[level].getJianshePower();
    }

    public float getCD(int level) {
        return getLeves()[level].getCD();
    }

    public float getRotateSpeed(int level) {
        return getLeves()[level].getRotatespeed();
    }

    public Levels.Buff getBuff(int level) {
        return getLeves()[level].getBuff();
    }

    public int getModeOffsetY(int level) {
        return getLeves()[level].getModeOffsetY();
    }

    public String getAnimName(int level) {
        return getBullet()[level].getAnimname();
    }

    public String getExplodeAnimName(int level) {
        return getExplodeBullet()[level].getAnimname();
    }

    public int getExplodeSpeed(int level) {
        return getExplodeBullet()[level].getSpeed();
    }

    public String getHitAnimName(int level) {
        return getBullet()[level].getHitAnimname();
    }

    public int getBulletCount(int level) {
        return getLeves()[level].getBulletCount();
    }

    public String getAnimName() {
        return this.bullet2.getAnimname();
    }

    public void setLeves(Levels[] leves2) {
        this.leves = leves2;
    }

    public Bullet[] getBullet() {
        return this.bullet;
    }

    public void setBullet(Bullet[] bullet3) {
        this.bullet = bullet3;
    }

    public FruitAdd getFruitadd() {
        return this.fruitadd;
    }

    public void setFruitadd(FruitAdd fruitadd2) {
        this.fruitadd = fruitadd2;
    }

    public static class Talk {
        String content;
        int who;

        public int getWho() {
            return this.who;
        }

        public void setWho(int who2) {
            this.who = who2;
        }

        public String getContent() {
            return this.content;
        }

        public void setContent(String content2) {
            this.content = content2;
        }

        public String toString() {
            return "Talk [who=" + this.who + ", content=" + this.content + "]";
        }
    }
}
