package com.sg.td.data;

import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GRes;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Sound;
import com.sg.td.data.LevelData;
import com.sg.td.data.TowerData;
import java.lang.reflect.Array;

public class Mydata implements GameConstant {
    public static ObjectMap<String, BuildingData> deckData = new ObjectMap<>();
    public static ObjectMap<String, LevelData> levelData = new ObjectMap<>();
    public static ObjectMap<String, MonsterData> monsterData = new ObjectMap<>();
    public static ObjectMap<String, TowerData> towerData = new ObjectMap<>();

    public static void loadTowerData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Tower.json");
        if (fileString == null) {
            System.out.println("data/Tower.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String towerName = value.name;
            TowerData data = new TowerData();
            JsonValue v = value.get("Levels");
            data.leves = new TowerData.Levels[v.size];
            for (int j = 0; j < v.size; j++) {
                JsonValue leves = v.get(j);
                data.leves[j] = new TowerData.Levels();
                data.leves[j].money = getValueInt(leves, "Money");
                data.leves[j].range = getValueInt(leves, HttpRequestHeader.Range);
                data.leves[j].AOERange = getValueInt(leves, "AOERange");
                data.leves[j].jiansheRange = getValueInt(leves, "JiansheRange");
                data.leves[j].explodeRange = getValueInt(leves, "ExplodeRange");
                data.leves[j].jianshePower = getValueInt(leves, "JianshePower");
                data.leves[j].CD = getValueFloat(leves, "CD");
                data.leves[j].rotatespeed = getValueFloat(leves, "Rotatespeed");
                data.leves[j].rotatespeed = getValueFloat(leves, "RotateSpeed");
                data.leves[j].attacktype = getValueInt(leves, "AttackType");
                data.leves[j].bulletwidth = getValueInt(leves, "BulletWidth");
                data.leves[j].power = getValueInt(leves, "Power");
                data.leves[j].modeOffsetY = getValueInt(leves, "modeOffsetY");
                data.leves[j].bulletCount = getValueInt(leves, "BulletCount");
                data.leves[j].bulletOffsetY = getValueInt(leves, "bulletOffsetY");
                data.leves[j].bgscale = getValueString(leves, "BgScale");
                data.leves[j].setFlyRange(getValueInt(value, "FlyRange"));
                data.leves[j].anim = getValueString(leves, "Anim");
                data.leves[j].animname = getValueString(leves, "AnimName");
                JsonValue buffValue = leves.get("Buff");
                TowerData.Levels.Buff buff = new TowerData.Levels.Buff();
                buff.setType(getValueInt(buffValue, "Type"));
                buff.setRate(getValueFloat(buffValue, "Rate"));
                buff.setValue(getValueFloat(buffValue, "Value"));
                buff.setLife(getValueFloat(buffValue, "Life"));
                buff.setConfictId(getValueInt(buffValue, "ConflictId"));
                buff.setPriority(getValueInt(buffValue, "Priority"));
                buff.setEfct(getValueString(buffValue, "Efct"));
                data.leves[j].setBuff(buff);
            }
            data.setIndex(i);
            data.name = getValueString(value, "Name");
            data.icon = getValueString(value, "Icon");
            data.descp = getValueString(value, "Descp");
            data.descp1 = getValueString(value, "Descp1");
            data.descp2 = getValueString(value, "Descp2");
            data.descp3 = getValueString(value, "Descp3");
            data.Bg = getValueString(value, "Bg");
            data.hashitanim = getValueInt(value, "HasHitAnim");
            data.strengthvalue = getValueFloat(value, "StrengthValue");
            data.strengthlvlupcost = getValueInt(value, "StrengthLvlUPCost");
            data.lvlupcostadd = getValueInt(value, "LvlUPCostAdd");
            data.hurteffect = getValueString(value, "HurtEffect");
            String sound = getValueString(value, "Sound");
            data.setSound(sound);
            data.setHitSoundId(Sound.getSoundId(sound));
            String sound2 = getValueString(value, "SoundTow");
            data.setSoundTow(sound2);
            data.setHitSoundTowId(Sound.getSoundId(sound2));
            String sound3 = getValueString(value, "SoundCome");
            data.setSoundCome(sound3);
            data.setHitSoundComeId(Sound.getSoundId(sound3));
            String sound4 = getValueString(value, "SoundBack");
            data.setSoundBack(sound4);
            data.setHitSoundBackId(Sound.getSoundId(sound4));
            JsonValue v1 = value.get("Bullet");
            if (v1 != null) {
                data.bullet2 = new TowerData.Bullet();
                data.bullet2.anim = getValueString(v1, "Anim");
                data.bullet2.animname = getValueString(v1, "AnimName");
                data.bullet2.speed = getValueInt(v1, "Speed");
                data.bullet2.aspeed = getValueInt(v1, "ASpeed");
                data.bullet = new TowerData.Bullet[v1.size];
                for (int j2 = 0; j2 < v1.size; j2++) {
                    JsonValue bullet = v1.get(j2);
                    data.bullet[j2] = new TowerData.Bullet();
                    data.bullet[j2].anim = getValueString(bullet, "Anim");
                    data.bullet[j2].animname = getValueString(bullet, "AnimName");
                    data.bullet[j2].hitAnimname = getValueString(bullet, "HitAnimName");
                    data.bullet[j2].scale = getValueString(bullet, "Scale");
                    data.bullet[j2].animrepeat = getValueInt(bullet, "AnimRepeat");
                    data.bullet[j2].speed = getValueInt(bullet, "Speed");
                    data.bullet[j2].life = getValueFloat(bullet, "Life");
                    data.bullet[j2].space = getValueInt(bullet, "Space");
                }
            }
            JsonValue vv = value.get("ExplodeBullet");
            if (vv != null) {
                data.explodeBullet = new TowerData.Bullet[vv.size];
                for (int j3 = 0; j3 < vv.size; j3++) {
                    JsonValue bullet2 = vv.get(j3);
                    data.explodeBullet[j3] = new TowerData.Bullet();
                    data.explodeBullet[j3].anim = getValueString(bullet2, "Anim");
                    data.explodeBullet[j3].animname = getValueString(bullet2, "AnimName");
                    data.explodeBullet[j3].hitAnimname = getValueString(bullet2, "HitAnimName");
                    data.explodeBullet[j3].scale = getValueString(bullet2, "Scale");
                    data.explodeBullet[j3].animrepeat = getValueInt(bullet2, "AnimRepeat");
                    data.explodeBullet[j3].speed = getValueInt(bullet2, "Speed");
                    data.explodeBullet[j3].life = getValueFloat(bullet2, "Life");
                }
            }
            data.hurteffect = getValueString(value, "HurtEffect");
            JsonValue v2 = value.get("FruitAdd");
            data.fruitadd = new TowerData.FruitAdd();
            data.fruitadd.attack = getValueFloat(v2, "Attack");
            data.fruitadd.speed = getValueFloat(v2, "Speed");
            data.fruitadd.range = getValueInt(v2, HttpRequestHeader.Range);
            data.sound = getValueString(value, "Sound");
            JsonValue v3 = value.get("Talk");
            if (v3 != null) {
                data.talks = new TowerData.Talk[v3.size];
                for (int j4 = 0; j4 < v3.size; j4++) {
                    JsonValue talk = v3.get(j4);
                    data.talks[j4] = new TowerData.Talk();
                    data.talks[j4].setWho(getValueInt(talk, "Who"));
                    data.talks[j4].setContent(getValueString(talk, "Content"));
                }
            }
            data.setTubiaoName(getValueString(value, "TuBiao"));
            data.setHuitubiaoName(getValueString(value, "HuiTuBiao"));
            data.setShuomingName(getValueString(value, "ShuoMing"));
            towerData.put(towerName, data);
        }
    }

    public static void loadLevelData(int rank) {
        loadLevel(GameConstant.LEVEL + rank);
    }

    public static void loadLevel(String name) {
        String fullName = PAK_ASSETS.DATA_PATH + name + ".json";
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile(fullName);
        if (fileString == null) {
            System.out.println(fullName + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        LevelData data = new LevelData();
        data.setType(getValueInt(arrayJsonValue, "Type"));
        data.setMoney(getValueInt(arrayJsonValue, "Money"));
        data.setIntroObj(getValueString(arrayJsonValue, "IntroObj"));
        data.setFortHP(getValueInt(arrayJsonValue, "FortHP"));
        data.setNormalAddHp(getValueFloat(arrayJsonValue, "NormalAddHp"));
        data.setHardAddHp(getValueFloat(arrayJsonValue, "HardAddHp"));
        JsonValue v1 = arrayJsonValue.get("Monsters");
        for (int i = 0; i < v1.size; i++) {
            JsonValue v2 = v1.get(i);
            LevelData.Monsters[] monsters = new LevelData.Monsters[v2.size];
            for (int j = 0; j < v2.size; j++) {
                LevelData.Monsters m = new LevelData.Monsters();
                JsonValue value = v2.get(j);
                m.type = getValueString(value, "Type");
                m.hp = getValueInt(value, "HP");
                m.speed = getValueInt(value, "Speed");
                m.count = getValueInt(value, "Count");
                m.time = getValueFloat(value, "Time");
                m.nextTime = getValueFloat(value, "NextTime");
                m.wave = getValueInt(value, "Wave");
                m.flyFruit = getValueString(value, "FlyFruit");
                m.setHoneyMonster(getValueString(value, "HoneyMonster"));
                m.setDropOnceKey(getValueString(value, "DropOnceKey"));
                m.setDrop(getValueString(value, "Drop"));
                JsonValue v3 = value.get("Skill");
                if (v3 != null) {
                    LevelData.Skill[] skills = new LevelData.Skill[v3.size];
                    for (int k = 0; k < v3.size; k++) {
                        JsonValue v = v3.get(k);
                        LevelData.Skill skill = new LevelData.Skill();
                        skill.setSkillType(getValueInt(v, "SkillType"));
                        skill.setMonsterType(getValueString(v, "MonsterType"));
                        skill.setHp(getValueInt(v, "HP"));
                        skill.setSpeed(getValueInt(v, "Speed"));
                        skill.setMonsterTime(getValueFloat(v, "MonsterTime"));
                        skill.setMonsterCount(getValueInt(v, "MonsterCount"));
                        skills[k] = skill;
                    }
                    m.setSkills(skills);
                }
                monsters[j] = m;
            }
            data.monsters.add(monsters);
        }
        JsonValue v22 = arrayJsonValue.get("StarTime");
        data.starTime = (int[][]) Array.newInstance(Integer.TYPE, v22.size, 2);
        for (int i2 = 0; i2 < v22.size; i2++) {
            JsonValue value2 = v22.get(i2);
            data.starTime[i2][0] = getValueInt(value2, "Star2");
            data.starTime[i2][1] = getValueInt(value2, "Star3");
        }
        JsonValue v32 = arrayJsonValue.get("Towers");
        if (v32 != null) {
            for (int i3 = 0; i3 < v32.size; i3++) {
                data.towers.add(v32.get(i3).toString());
            }
        }
        JsonValue v5 = arrayJsonValue.get("HideTowers");
        if (v5 != null) {
            for (int i4 = 0; i4 < v5.size; i4++) {
                String valueName = v5.get(i4).toString();
                System.out.println("HideTowers:" + valueName);
                data.hideTowers.add(valueName);
            }
        }
        JsonValue v4 = arrayJsonValue.get("RandomTowers");
        if (v4 != null) {
            for (int i5 = 0; i5 < v4.size; i5++) {
                data.randomTowers.add(v4.get(i5).name);
            }
        }
        levelData.put(name, data);
    }

    public static void loadMonsterData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Monster.json");
        if (fileString == null) {
            System.out.println("data/Monster.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String monsterName = value.name;
            MonsterData data = new MonsterData();
            data.anim = getValueString(value, "Anim");
            data.animName = getValueString(value, "AnimName");
            data.hp = getValueInt(value, "HP");
            data.money = getValueInt(value, "Money");
            data.setSpeed(getValueInt(value, "Speed"));
            data.setDamage(getValueInt(value, "Damage"));
            data.setCollideWidth(getValueInt(value, "CollideWidth"));
            data.setCollideHeight(getValueInt(value, "CollideHeight"));
            data.setOffsetY(getValueInt(value, "OffsetY"));
            data.setShadowOffsetY(getValueInt(value, "ShadowOffsetY"));
            data.setEffectOffsetY(getValueInt(value, "EffectOffsetY"));
            data.setHpBarY(getValueInt(value, "HPBarY"));
            data.setBackOffsetY(getValueInt(value, "BackOffsetY"));
            data.setScale(getValueFloat(value, "Scale"));
            data.setIsRandom(getValueInt(value, "IsRandom"));
            if (data.isRandom()) {
                for (int j = 1; j < value.size; j++) {
                    data.randoms.add(value.get(j).name);
                }
            }
            monsterData.put(monsterName, data);
        }
    }

    public static void loadDeckData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Building.json");
        if (fileString == null) {
            System.out.println("data/Building.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            JsonValue value = arrayJsonValue.get(i);
            String deckName = value.name;
            BuildingData data = new BuildingData();
            data.setName(getValueString(value, "Name"));
            data.setIcon(getValueString(value, "Icon"));
            data.setScale(getValueFloat(value, "Scale"));
            data.setMoney(getValueInt(value, "Money"));
            data.setHp(getValueInt(value, "HP"));
            data.setImageWidth(getValueInt(value, "ImageWidth"));
            data.setImageHeight(getValueInt(value, "ImageHeight"));
            data.setCollideWidth(getValueInt(value, "CollideWidth"));
            data.setCollideHeight(getValueInt(value, "CollideHeight"));
            data.setHpBarY(getValueInt(value, "HPBarY"));
            data.setModeOffsetY(getValueInt(value, "modeOffsetY"));
            data.setTubiaoName(getValueString(value, "TuBiao"));
            data.setShuomingName(getValueString(value, "ShuoMing"));
            data.setZiName(getValueString(value, "Zi"));
            JsonValue v3 = value.get("Talk");
            if (v3 != null) {
                data.talks = new TowerData.Talk[v3.size];
                for (int j = 0; j < v3.size; j++) {
                    JsonValue talk = v3.get(j);
                    data.talks[j] = new TowerData.Talk();
                    data.talks[j].setWho(getValueInt(talk, "Who"));
                    data.talks[j].setContent(getValueString(talk, "Content"));
                }
            }
            deckData.put(deckName, data);
        }
    }

    static String getValueString(JsonValue value, String name) {
        try {
            return value.getString(name);
        } catch (Exception e) {
            return "null";
        }
    }

    static int getValueInt(JsonValue value, String name) {
        try {
            return value.getInt(name);
        } catch (Exception e) {
            return 0;
        }
    }

    static float getValueFloat(JsonValue value, String name) {
        try {
            return value.getFloat(name);
        } catch (Exception e) {
            return Animation.CurveTimeline.LINEAR;
        }
    }
}
