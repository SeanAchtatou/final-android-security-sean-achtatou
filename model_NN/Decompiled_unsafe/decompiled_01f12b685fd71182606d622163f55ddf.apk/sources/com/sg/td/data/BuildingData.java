package com.sg.td.data;

import com.sg.td.data.TowerData;

public class BuildingData {
    int ImageHeight;
    int ImageWidth;
    int collideHeight;
    int collideWidth;
    int hp;
    int hpBarY;
    String icon;
    int modeOffsetY;
    int money;
    String name;
    float scale;
    String shuomingName;
    TowerData.Talk[] talks;
    String tubiaoName;
    String ziName;

    public String getZiName() {
        return this.ziName;
    }

    public void setZiName(String ziName2) {
        this.ziName = ziName2;
    }

    public String getTubiaoName() {
        return this.tubiaoName;
    }

    public void setTubiaoName(String tubiaoName2) {
        this.tubiaoName = tubiaoName2;
    }

    public String getShuomingName() {
        return this.shuomingName;
    }

    public void setShuomingName(String shuomingName2) {
        this.shuomingName = shuomingName2;
    }

    public TowerData.Talk[] getTalks() {
        return this.talks;
    }

    public void setTalks(TowerData.Talk[] talks2) {
        this.talks = talks2;
    }

    public int getModeOffsetY() {
        return this.modeOffsetY;
    }

    public void setModeOffsetY(int modeOffsetY2) {
        this.modeOffsetY = modeOffsetY2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public float getScale() {
        return this.scale;
    }

    public void setScale(float scale2) {
        this.scale = scale2;
    }

    public int getMoney() {
        return this.money;
    }

    public void setMoney(int money2) {
        this.money = money2;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp2) {
        this.hp = hp2;
    }

    public int getImageWidth() {
        return this.ImageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.ImageWidth = imageWidth;
    }

    public int getImageHeight() {
        return this.ImageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.ImageHeight = imageHeight;
    }

    public int getCollideWidth() {
        return this.collideWidth;
    }

    public void setCollideWidth(int collideWidth2) {
        this.collideWidth = collideWidth2;
    }

    public int getCollideHeight() {
        return this.collideHeight;
    }

    public void setCollideHeight(int collideHeight2) {
        this.collideHeight = collideHeight2;
    }

    public int getHpBarY() {
        return this.hpBarY;
    }

    public void setHpBarY(int hpBarY2) {
        this.hpBarY = hpBarY2;
    }

    public String toString() {
        return "DeckData [name=" + this.name + ", icon=" + this.icon + ", scale=" + this.scale + ", money=" + this.money + ", hp=" + this.hp + ", collideWidth=" + this.collideWidth + ", collideHeight=" + this.collideHeight + ", hpBarY=" + this.hpBarY + "]";
    }
}
