package com.sg.td.data;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GRes;
import com.sg.pak.PAK_ASSETS;

public class MyBigMapPlace {
    public static ObjectMap<Integer, Place> bigMapPlaceData = new ObjectMap<>();

    public static class Place {
        int name;
        int x;
        int y;

        public int getName() {
            return this.name;
        }

        public int getX() {
            return this.x;
        }

        public int getY() {
            return this.y;
        }
    }

    public static void laodData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/bigMapPlace.json");
        if (fileString == null) {
            System.out.println("data/bigMapPlace.json  = null");
            return;
        }
        JsonValue value = reader.parse(fileString).get("data");
        for (int i = 0; i < value.size; i++) {
            JsonValue jv = value.get(i);
            Place point = new Place();
            point.x = jv.getInt("x");
            point.y = jv.getInt("y");
            point.name = PAK_ASSETS.IMG_MAP0;
            bigMapPlaceData.put(Integer.valueOf(i), point);
        }
    }
}
