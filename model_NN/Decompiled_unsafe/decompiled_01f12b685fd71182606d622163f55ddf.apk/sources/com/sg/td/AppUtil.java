package com.sg.td;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Process;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppUtil {
    private static final int TOUCH_COUNT = 10;
    private static final int TOUCH_TIME = 1500;
    private static int count = 0;
    private static long lastTime = 0;

    public static void checkInfo(Context context) {
        long current = System.currentTimeMillis();
        if (current - lastTime > 1500) {
            count = 0;
            lastTime = System.currentTimeMillis();
        }
        int i = count + 1;
        count = i;
        if (i >= 10) {
            new AlertDialog.Builder(context).setCancelable(false).setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setMessage(System.getProperty("sgdebug")).create().show();
            count = 0;
            lastTime = current;
        }
    }

    public static String getCurrentProcessName(Context context) {
        Iterator<ActivityManager.RunningAppProcessInfo> iter = getAppProcessInfo(context);
        if (iter == null) {
            return null;
        }
        while (iter.hasNext()) {
            ActivityManager.RunningAppProcessInfo appProcessInfo = iter.next();
            if (appProcessInfo.pid == Process.myPid()) {
                return appProcessInfo.processName;
            }
        }
        return null;
    }

    public static int getPPid(int pid) {
        Class<Process> process = Process.class;
        try {
            return ((Integer) process.getDeclaredMethod("getParentPid", Integer.TYPE).invoke(null, Integer.valueOf(pid))).intValue();
        } catch (Throwable e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void exitGameProcess(Activity context) {
        try {
            ArrayList<Integer> list = new ArrayList<>();
            Iterator<ActivityManager.RunningAppProcessInfo> iter = getAppProcessInfo(context);
            if (iter != null) {
                String packageName = context.getPackageName();
                while (iter.hasNext()) {
                    ActivityManager.RunningAppProcessInfo appProcessInof = iter.next();
                    if (appProcessInof.processName.equals(packageName)) {
                        list.add(Integer.valueOf(appProcessInof.pid));
                    }
                }
            }
            if (!context.isFinishing()) {
                context.finish();
            }
            Iterator<Integer> iterProcess = list.iterator();
            while (iterProcess.hasNext()) {
                int pid = ((Integer) iterProcess.next()).intValue();
                if (pid != 0) {
                    Process.killProcess(pid);
                }
            }
            System.exit(0);
        } catch (Exception e) {
        }
    }

    public static void exitSdkPluginProcess(Context context) {
        int pid = 0;
        try {
            Iterator<ActivityManager.RunningAppProcessInfo> iter = getAppProcessInfo(context);
            if (iter != null) {
                String packageName = context.getPackageName();
                while (true) {
                    if (!iter.hasNext()) {
                        break;
                    }
                    ActivityManager.RunningAppProcessInfo appProcessInfo = iter.next();
                    if (appProcessInfo.processName.equals(packageName + ":gcsdk")) {
                        pid = appProcessInfo.pid;
                        break;
                    }
                }
            }
            if (pid != 0) {
                Process.killProcess(pid);
            }
        } catch (Exception e) {
        }
    }

    public static Iterator<ActivityManager.RunningAppProcessInfo> getAppProcessInfo(Context context) {
        List<ActivityManager.RunningAppProcessInfo> list = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.iterator();
    }
}
