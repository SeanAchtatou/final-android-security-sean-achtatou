package com.sg.td.message;

import com.kbz.AssetManger.GRes;
import com.sg.GameEntry.GameMain;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GameSpecial {
    public static boolean XLeftTop;
    public static boolean addgetRightTop;
    public static boolean atuoPop;
    public static int button;
    public static boolean continuousPop;
    public static boolean delay;
    public static boolean extendedX;
    public static boolean getLeftTop;
    public static boolean getRightCorner;
    public static boolean hand;
    public static boolean hitAnyWhere;
    static String isABCDEF;
    public static String isFail;
    static String isMMorJD;
    public static boolean isRightlingqu;
    public static boolean landPop;
    public static int price;
    public static boolean shopPop;

    public static void initCloudNew() {
        isABCDEF = GameMain.dialog.initSGManager()[0];
        isMMorJD = GameMain.dialog.initSGManager()[1];
        isFail = GameMain.dialog.initSGManager()[3];
        if (isABCDEF != null) {
            MySwitch.isCaseA = Integer.parseInt(isABCDEF);
            if (isMMorJD != null) {
                MySwitch.caseJiOrMM = Integer.parseInt(isMMorJD);
            }
            if (MySwitch.caseJiOrMM == 2 || MySwitch.isSimId == -1) {
                MySwitch.Yifen = false;
            }
        }
        System.out.println("===========MySwitch.isCaseA::" + MySwitch.isCaseA);
        initCaseA();
        changegift();
        if (MySwitch.isCaseA != 0) {
            MySwitch.isHaveNotice = true;
        }
    }

    public static void initCaseA() {
        switch (MySwitch.isCaseA) {
            case 0:
                price = 0;
                button = 0;
                continuousPop = false;
                delay = false;
                hitAnyWhere = false;
                landPop = false;
                hand = false;
                atuoPop = false;
                shopPop = false;
                return;
            case 1:
                price = 2;
                button = 1;
                delay = true;
                hitAnyWhere = true;
                landPop = false;
                continuousPop = false;
                atuoPop = true;
                shopPop = true;
                XLeftTop = true;
                addgetRightTop = true;
                getRightCorner = true;
                isRightlingqu = true;
                return;
            case 2:
                price = 2;
                button = 2;
                continuousPop = false;
                delay = false;
                hitAnyWhere = false;
                landPop = false;
                hand = false;
                atuoPop = true;
                shopPop = true;
                XLeftTop = false;
                addgetRightTop = false;
                getRightCorner = true;
                return;
            case 3:
                price = 2;
                button = 1;
                continuousPop = false;
                delay = true;
                hitAnyWhere = false;
                landPop = false;
                hand = false;
                atuoPop = true;
                shopPop = true;
                XLeftTop = true;
                addgetRightTop = true;
                getRightCorner = true;
                isRightlingqu = true;
                return;
            case 4:
                price = 0;
                button = 2;
                continuousPop = false;
                delay = false;
                hitAnyWhere = false;
                landPop = true;
                hand = false;
                atuoPop = true;
                shopPop = true;
                XLeftTop = false;
                getRightCorner = true;
                return;
            case 5:
                price = 0;
                button = 2;
                continuousPop = false;
                delay = false;
                landPop = false;
                hand = false;
                atuoPop = true;
                shopPop = true;
                XLeftTop = false;
                addgetRightTop = false;
                getRightCorner = true;
                getLeftTop = false;
                return;
            default:
                return;
        }
    }

    private static void changegift() {
        System.out.println("---------------------MySwitch.giftType::" + MySwitch.giftType);
        switch (Integer.parseInt(MySwitch.giftType)) {
            case 0:
                GMessage.PP_TEMPgift = 12;
                break;
            case 1:
                GMessage.PP_TEMPgift = 13;
                break;
            case 2:
                GMessage.PP_TEMPgift = 14;
                break;
        }
        if (MySwitch.caseJiOrMM == 2) {
            GMessage.PP_TEMPgift = 7;
        }
        if (MySwitch.isSimId != 0) {
            GMessage.PP_TEMPgift = 7;
        }
    }

    public static String getChannel(String fileName, String propertyName) {
        InputStream file = GRes.openFileHandle(fileName).read();
        Properties prop = new Properties();
        try {
            prop.load(file);
            return prop.getProperty(propertyName);
        } catch (IOException e) {
            return null;
        }
    }
}
