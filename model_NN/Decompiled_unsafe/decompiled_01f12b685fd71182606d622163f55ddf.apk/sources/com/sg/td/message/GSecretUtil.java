package com.sg.td.message;

import com.datalab.tools.Constant;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class GSecretUtil {
    public static boolean[] giftGet = new boolean[200];
    private static byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};
    private static byte[] key = new byte[8];

    public static int checkNum(String num) {
        String value = null;
        try {
            value = decodeByDES(num.toUpperCase());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            int id = Integer.parseInt(value);
            if (id < 1000000) {
                return 0;
            }
            if (id >= 1000000 && id < 1400000) {
                return 1;
            }
            if (id >= 1400000 && id < 1500000) {
                return 2;
            }
            if (id >= 1500000 && id < 2500000) {
                return 3;
            }
            if (id >= 2500000 && id < 2900000) {
                return 4;
            }
            if (id >= 2900000 && id < 3000000) {
                return 5;
            }
            if (id >= 3000000 && id < 4000000) {
                return 6;
            }
            if (id >= 4000000 && id < 4500000) {
                return 7;
            }
            if (id >= 4500000 && id < 5000000) {
                return 8;
            }
            if (id >= 5000000 && id < 5200000) {
                return 9;
            }
            if (id >= 5200000 && id < 5300000) {
                return 10;
            }
            if (id >= 5300000 && id < 6300000) {
                return 11;
            }
            for (int i = 0; i < 5; i++) {
                if (id >= 6300000 + (i * 100000) && id < 6400000 + (i * 100000)) {
                    return i + 12;
                }
            }
            if (id >= 6800000 && id < 7000000) {
                return 17;
            }
            for (int i2 = 0; i2 < 6; i2++) {
                if (id >= 7000000 + (i2 * 100000) && id < 7100000 + (i2 * 100000)) {
                    return i2 + 18;
                }
            }
            if (id >= 7700000 && id < 8200000) {
                return 24;
            }
            int group = id / Constant.DEFAULT_LIMIT;
            if (group >= giftGet.length) {
                group = giftGet.length + ((id - (giftGet.length * Constant.DEFAULT_LIMIT)) / 50000);
            }
            if (id < 0 || group >= giftGet.length) {
                return -1;
            }
            if (id == 0 || !value.startsWith("0")) {
                return group;
            }
            return -1;
        } catch (Exception e) {
            return -1;
        }
    }

    public static String decodeByDES(String sInfo) {
        IvParameterSpec zeroIv = new IvParameterSpec(iv);
        byte[] cipherByte = new byte[1];
        try {
            setKey("td");
            SecretKeySpec key2 = new SecretKeySpec(key, "DES");
            Cipher c1 = Cipher.getInstance("DES/CBC/PKCS5Padding");
            c1.init(2, key2, zeroIv);
            cipherByte = c1.doFinal(hex2byte(sInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(cipherByte);
    }

    public static void setKey(String encryptKey) {
        for (int i = 0; i < key.length; i++) {
            if (i < encryptKey.length()) {
                key[i] = (byte) encryptKey.charAt(i);
            } else {
                key[i] = 0;
            }
        }
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        for (byte b2 : b) {
            String stmp = Integer.toHexString(b2 & 255);
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs.toUpperCase();
    }

    public static byte[] hex2byte(String hex) {
        byte[] ret = new byte[8];
        byte[] tmp = hex.getBytes();
        for (int i = 0; i < 8; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[(i * 2) + 1]);
        }
        return ret;
    }

    public static byte uniteBytes(byte src0, byte src1) {
        return (byte) (((byte) (Byte.decode("0x" + new String(new byte[]{src0})).byteValue() << 4)) ^ Byte.decode("0x" + new String(new byte[]{src1})).byteValue());
    }

    public static boolean isGiftGet(int id) {
        return giftGet[id];
    }

    public static void setGiftGet(int id) {
        giftGet[id] = true;
    }
}
