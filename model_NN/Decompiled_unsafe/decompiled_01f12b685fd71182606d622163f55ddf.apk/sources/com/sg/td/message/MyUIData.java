package com.sg.td.message;

public class MyUIData {
    public static boolean[] isDuihuan = new boolean[4];
    public static boolean isGethdlb = false;
    public static int rechargeAmount;
    public static boolean updataJCHD;

    public static boolean isUpdataJCHD() {
        return updataJCHD;
    }

    public static void setUpdataJCHD(boolean updataJCHD2) {
        updataJCHD = updataJCHD2;
    }

    public static int getRechargeAmount() {
        return rechargeAmount;
    }

    public static void setRechargeAmount(int rechargeAmount2) {
        rechargeAmount = rechargeAmount2;
    }

    public static boolean isGethdlb() {
        return isGethdlb;
    }

    public static void setGethdlb(boolean isGethdlb2) {
        isGethdlb = isGethdlb2;
    }
}
