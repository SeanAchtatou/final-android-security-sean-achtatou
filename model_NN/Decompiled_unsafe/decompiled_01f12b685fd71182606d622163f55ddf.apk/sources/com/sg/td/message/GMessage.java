package com.sg.td.message;

import com.android.volley.DefaultRetryPolicy;
import com.datalab.tools.Constant;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.td.UI.MyGet;
import com.sg.td.UI.MyGift;
import com.sg.td.UI.MyTip;
import com.sg.td.record.Get;
import com.sg.td.record.LoadGet;

public class GMessage implements GameConstant {
    public static String[] BUY_INFO = {"复活X1元素之力X1额外赠送恐龙币X200", "复活X6元素之力X6额外赠送恐龙币X3000", "复活X2元素之力X3额外赠送恐龙币X1000", "复活X4元素之力X6额外赠送恐龙币X5000", "复活X11元素之力X18额外赠送恐龙币X10000", "钻石X300额外赠送钻石X300", "钻石X900额外赠送钻石X900", "钻石X2175额外赠送钻石X2175", "恐龙币X3000额外赠送恐龙币X3000", "恐龙币X9000额外赠送恐龙币X9000", "恐龙币X18750额外赠送恐龙币X18750", "获得铠甲雷古曼"};
    public static String[] BUY_NAME = {"幸运红包", "新手红包", "成长礼包", "战斗宝盒", "博士秘藏宝箱", "补充钻石（小）", "补充钻石（中）", "补充钻石（大）", "补充恐龙币（小）", "补充恐龙币（中）", "补充恐龙币（大）", "铠甲雷古曼"};
    public static String[] BUY_NAME_X1 = {"幸运红包", "新手红包", "成长礼包", "战斗宝盒", "博士秘藏宝箱", "补充钻石（小）", "补充钻石（中）", "补充钻石（大）", "补充恐龙币（小）", "补充恐龙币（中）", "补充恐龙币（大）", "铠甲雷古曼", "", "", "", "", "", "", "", "", "道具礼包", "元素之力X1", "复活心X1", "体力X1", "无限重玩30天", "无限体力30天", "斗龙手环X1", "斗龙眼镜", "零钱罐X1", "弹簧鞋X1"};
    public static int[] BUY_PRICE = {0, 6, 6, 15, 25, 6, 15, 29, 6, 15, 25, 20, 14, 21, 28};
    public static int[] BUY_PRICE_OPPO = {10, PAK_ASSETS.IMG_G02, PAK_ASSETS.IMG_G02, GameConstant.SKILL_DAMAGE, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, PAK_ASSETS.IMG_G02, GameConstant.SKILL_DAMAGE, 2900, PAK_ASSETS.IMG_G02, GameConstant.SKILL_DAMAGE, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 2000};
    public static String[] BUY_PRICE_UC = {"0.01", "6", "6", "15", "25", "6", "15", "29", "6", "15", "25", "20"};
    public static String[] BUY_PRICE_VIVO = {"0.10", "6.00", "6.00", "15.00", "25.00", "6.00", "15.00", "29.00", "6.00", "15.00", "25.00", "20.00"};
    public static String[] BUY_PRICE_X1 = {"0.1", "6", "6", "15", "25", "6", "15", "29", "6", "15", "25", "20", "0", "0", "0", "0", "0", "0", "0", "0", "800钻", "100钻", "200钻", "1000恐龙币", "2000钻", "2000钻", "400恐龙币", "400恐龙币", "400恐龙币", "400恐龙币"};
    public static String[] BUY_PRICE_XiaoMI = {"10", "600", "600", "1500", "2500", "600", "1500", "2900", "600", "1500", "2500", "2000"};
    public static float[] BUY_PRICE_anzhi = {0.1f, 6.0f, 6.0f, 15.0f, 25.0f, 6.0f, 15.0f, 29.0f, 6.0f, 15.0f, 25.0f, 20.0f};
    public static float[] BUY_PRICE_f = {0.1f, 6.0f, 6.0f, 15.0f, 25.0f, 6.0f, 15.0f, 29.0f, 6.0f, 15.0f, 25.0f, 20.0f, 14.0f, 21.0f, 28.0f};
    public static float[] BUY_PRICE_f_jd = {0.01f, 6.0f, 6.0f, 15.0f, 25.0f, 6.0f, 15.0f, 29.0f, 6.0f, 15.0f, 25.0f, 20.0f, 14.0f, 21.0f, 28.0f};
    public static int[] BUY_PRICE_jd = {0, 6, 6, 15, 25, 6, 15, 29, 6, 15, 25, 20, 14, 21, 28};
    public static String[] PAYCODE_DX = {"TOOL1", "TOOL2", "TOOL3", "TOOL4", "TOOL5", "TOOL6", "TOOL7", "TOOL8", "TOOL9", "TOOL10", "TOOL11", "TOOL12"};
    public static final String[] PAYCODE_LT = {"001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012"};
    public static String[] PAYCODE_YD = {"001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012"};
    public static byte PP_TEMPgift = 12;
    public static final byte PP_bcklbd = 10;
    public static final byte PP_bcklbx = 8;
    public static final byte PP_bcklbz = 9;
    public static final byte PP_bczsd = 7;
    public static final byte PP_bczsx = 5;
    public static final byte PP_bczsz = 6;
    public static final byte PP_bsmmbx = 4;
    public static final byte PP_changwan = 13;
    public static final byte PP_chongwan = 24;
    public static final byte PP_czlb = 2;
    public static final byte PP_djlb = 20;
    public static final byte PP_fengmi = 21;
    public static final byte PP_fuhuoxin = 22;
    public static final byte PP_huoli = 12;
    public static final byte PP_kjlgm = 11;
    public static final byte PP_lingqianguan = 28;
    public static final byte PP_shouxuan = 26;
    public static final byte PP_tili = 23;
    public static final byte PP_wuxiantili = 25;
    public static final byte PP_xie = 29;
    public static final byte PP_xshb = 1;
    public static final byte PP_xyhb = 0;
    public static final byte PP_yanjing = 27;
    public static final byte PP_zdbh = 3;
    public static final byte PP_zhizun = 14;
    static int failNum = 0;
    public static boolean[] isBuyed = new boolean[30];
    public static int isCalled7;
    public static MianGiftContonl mfContonl;
    public static int payIndex;

    public interface MianGiftContonl {
        void No();

        void Yes();
    }

    public static void send(int id) {
        payIndex = id;
        if (MySwitch.isCaseA != 0 && payIndex == PP_TEMPgift) {
            isCalled7++;
            RankData.saveRecord();
        }
        if (!isBuyed[payIndex]) {
            if (MySwitch.test) {
                if (id > 19) {
                    BuyProp();
                } else {
                    SendSuccess();
                }
            } else if (id > 19) {
                BuyProp();
            } else {
                GameMain.dialog.sendMessage(id);
            }
        }
    }

    public static void SendSuccess() {
        LoadGet.loadGetDate();
        Get get = LoadGet.getData.get("Shop");
        MyUIData.setRechargeAmount(MyUIData.getRechargeAmount() + BUY_PRICE[payIndex]);
        RankData.saveRecord();
        switch (payIndex) {
            case 0:
                isBuyed[payIndex] = true;
                new MyGet(get, 0, 1);
                RankData.addHoneyNum(1);
                RankData.addHeartNum(1);
                RankData.addCakeNum(200);
                break;
            case 1:
                isBuyed[payIndex] = true;
                new MyGet(get, 5, 1);
                RankData.addHoneyNum(6);
                RankData.addHeartNum(6);
                RankData.addCakeNum(3000);
                break;
            case 2:
                new MyGet(get, 1, 1);
                RankData.addHoneyNum(3);
                RankData.addHeartNum(2);
                RankData.addCakeNum(1000);
                break;
            case 3:
                new MyGet(get, 3, 1);
                RankData.addHoneyNum(6);
                RankData.addHeartNum(4);
                RankData.addCakeNum(5000);
                break;
            case 4:
                new MyGet(get, 4, 1);
                RankData.addHoneyNum(18);
                RankData.addHeartNum(11);
                RankData.addCakeNum(Constant.DEFAULT_LIMIT);
                break;
            case 5:
                new MyGet(get, 18, 1);
                RankData.addDiamondNum(PAK_ASSETS.IMG_G02);
                break;
            case 6:
                new MyGet(get, 19, 1);
                RankData.addDiamondNum(1800);
                break;
            case 7:
                new MyGet(get, 20, 1);
                RankData.addDiamondNum(4350);
                break;
            case 8:
                new MyGet(get, 15, 1);
                RankData.addCakeNum(6000);
                break;
            case 9:
                new MyGet(get, 16, 1);
                RankData.addCakeNum(18000);
                break;
            case 10:
                new MyGet(get, 17, 1);
                RankData.addCakeNum(37500);
                break;
            case 11:
                RankData.bearOpen(3);
                MySwitch.isKJLGM = true;
                new MyGet(get, 21, 1);
                break;
            case 12:
                RankData.addHoneyNum(1);
                RankData.addHeartNum(1);
                RankData.addCakeNum(200);
                RankData.addDiamondNum(1800);
                new MyGet(get, 22, 1);
                jiangji();
                break;
            case 13:
                RankData.addHoneyNum(3);
                RankData.addHeartNum(3);
                RankData.addCakeNum(1000);
                RankData.addDiamondNum(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS);
                new MyGet(get, 23, 1);
                jiangji();
                break;
            case 14:
                RankData.addHoneyNum(10);
                RankData.addHeartNum(5);
                RankData.addCakeNum(5000);
                RankData.addDiamondNum(4000);
                new MyGet(get, 24, 1);
                jiangji();
                break;
        }
        if (mfContonl != null) {
            mfContonl.Yes();
            mfContonl = null;
        }
    }

    public static void BuyProp() {
        Get get = LoadGet.getData.get("Shop");
        switch (payIndex) {
            case 20:
                if (!RankData.spendDiamond(PAK_ASSETS.IMG_UP011)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(7);
                        break;
                    } else {
                        MyTip.Notenought(false);
                        break;
                    }
                } else {
                    new MyGet(get, 2, 1);
                    RankData.addProps(1, 5);
                    RankData.addProps(0, 5);
                    RankData.addProps(2, 5);
                    RankData.addProps(3, 5);
                    RankData.addCakeNum(2000);
                    break;
                }
            case 21:
                if (!RankData.spendDiamond(100)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(7);
                        break;
                    } else {
                        MyTip.Notenought(false);
                        break;
                    }
                } else {
                    new MyGet(get, 6, 1);
                    RankData.addHoneyNum(1);
                    break;
                }
            case 22:
                if (!RankData.spendDiamond(200)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(7);
                        break;
                    } else {
                        MyTip.Notenought(false);
                        break;
                    }
                } else {
                    new MyGet(get, 7, 1);
                    RankData.addHeartNum(1);
                    break;
                }
            case 23:
                if (!RankData.spendCake(1000)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(10);
                        break;
                    } else {
                        MyTip.Notenought(true);
                        break;
                    }
                } else {
                    new MyGet(get, 8, 1);
                    RankData.buyPower(1);
                    break;
                }
            case 24:
                if (!RankData.spendDiamond(2000)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(7);
                        break;
                    } else {
                        MyTip.Notenought(false);
                        break;
                    }
                } else {
                    new MyGet(get, 9, 1);
                    RankData.buyEndlessReplay(30);
                    break;
                }
            case 25:
                if (!RankData.spendDiamond(2000)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(7);
                        break;
                    } else {
                        MyTip.Notenought(false);
                        break;
                    }
                } else {
                    new MyGet(get, 10, 1);
                    RankData.buyEndlessPower(30);
                    break;
                }
            case 26:
                if (!RankData.spendCake(400)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(10);
                        break;
                    } else {
                        MyTip.Notenought(true);
                        break;
                    }
                } else {
                    new MyGet(get, 11, 1);
                    RankData.addProps(0, 1);
                    break;
                }
            case 27:
                if (!RankData.spendCake(400)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(10);
                        break;
                    } else {
                        MyTip.Notenought(true);
                        break;
                    }
                } else {
                    new MyGet(get, 12, 1);
                    RankData.addProps(1, 1);
                    break;
                }
            case 28:
                if (!RankData.spendCake(400)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(10);
                        break;
                    } else {
                        MyTip.Notenought(true);
                        break;
                    }
                } else {
                    new MyGet(get, 13, 1);
                    RankData.addProps(2, 1);
                    break;
                }
            case 29:
                if (!RankData.spendCake(400)) {
                    if (MySwitch.isCaseA != 0) {
                        new MyGift(10);
                        break;
                    } else {
                        MyTip.Notenought(true);
                        break;
                    }
                } else {
                    new MyGet(get, 14, 1);
                    RankData.addProps(3, 1);
                    break;
                }
        }
        if (mfContonl != null) {
            mfContonl.Yes();
            mfContonl = null;
        }
    }

    public static void sendFail() {
        failNum++;
        if (GameSpecial.continuousPop && failNum == 1 && MySwitch.isSimId == 0 && MySwitch.caseJiOrMM == 1) {
            GameMain.dialog.sendMessage(payIndex);
            return;
        }
        failNum = 0;
        System.out.println("payIndex:" + payIndex);
        switch (payIndex) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            default:
                return;
            case 12:
            case 13:
            case 14:
                System.out.println("gmessage isCalled7:" + isCalled7);
                if (isCalled7 == 1 && MySwitch.isCaseA != 0) {
                    MyTip.FristBuyedFail();
                }
                jiangji();
                return;
        }
    }

    public static void jiangji() {
        if (MySwitch.isCaseA != 0) {
            if (payIndex == 14) {
                PP_TEMPgift = 13;
            } else if (payIndex == 13) {
                System.out.println("畅玩礼包失败1");
                PP_TEMPgift = 12;
            } else if (payIndex == 12) {
                System.out.println("畅玩礼包失败2");
                PP_TEMPgift = 14;
            }
        }
    }

    public static boolean getIsBuyed(int id) {
        return isBuyed[id];
    }

    public static MianGiftContonl getMfContonl() {
        return mfContonl;
    }

    public static void setMfContonl(MianGiftContonl mfContonl2) {
        if (mfContonl != null) {
            mfContonl = null;
        }
        mfContonl = mfContonl2;
    }
}
