package com.sg.td.message;

import com.badlogic.gdx.graphics.Color;
import com.kbz.Actors.ActorText;
import com.kbz.esotericsoftware.spine.Animation;

public class MyUIGetFont {
    public static Color achieveDescp1Color = new Color(0.1f, 0.31f, 0.55f, 1.0f);
    public static Color achievecomplete1Color = new Color(0.54f, 0.22f, 0.79f, 1.0f);
    public static Color jdColor = new Color(1.0f, 0.99f, 0.47f, 1.0f);
    public static Color jihuomaColor = new Color(0.98f, Animation.CurveTimeline.LINEAR, 0.21f, 1.0f);
    public static Color maintopdataColor = new Color(0.48f, 0.93f, 1.0f, 1.0f);
    public static Color paoColor = new Color(1.0f, 0.59f, 0.05f, 1.0f);
    public static Color paoNextPowerColor = new Color(0.78f, 0.42f, 0.97f, 1.0f);
    public static Color priceColor = new Color(1.0f, 1.0f, 1.0f, 0.2f);

    public static void getMainTopfont(ActorText actor, Float size, Color color) {
        actor.setFontScaleXY(size.floatValue());
        actor.setColor(color);
    }
}
