package com.sg.td.message;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.tools.MyGroup;

public class GameUITools extends MyGroup {
    public static boolean isDragged;
    /* access modifiers changed from: private */
    public static float mapChoiceX;
    /* access modifiers changed from: private */
    public static float mapOX;
    /* access modifiers changed from: private */
    public static float mapX;
    /* access modifiers changed from: private */
    public static int point;

    public static InputListener getMoveListener(Group group, float width, float offWidth, float power, boolean isX) {
        final float f = width;
        final float f2 = offWidth;
        final boolean z = isX;
        final Group group2 = group;
        final float f3 = power;
        return new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (f < f2) {
                    return false;
                }
                if (!z) {
                    x = y;
                }
                float unused = GameUITools.mapX = x;
                float unused2 = GameUITools.mapChoiceX = z ? group2.getX() : group2.getY();
                return true;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
                if ((r3 ? r4.getX() : r4.getY() - com.sg.td.message.GameUITools.access$100()) < -15.0f) goto L_0x0021;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent r5, float r6, float r7, int r8) {
                /*
                    r4 = this;
                    r3 = 0
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x004e
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x000b:
                    r1 = 1097859072(0x41700000, float:15.0)
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 > 0) goto L_0x0021
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x005a
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x001b:
                    r1 = -1049624576(0xffffffffc1700000, float:-15.0)
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 >= 0) goto L_0x0024
                L_0x0021:
                    r0 = 1
                    com.sg.td.message.GameUITools.isDragged = r0
                L_0x0024:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x0066
                L_0x0028:
                    float r0 = com.sg.td.message.GameUITools.mapX
                    float r0 = r6 - r0
                    float unused = com.sg.td.message.GameUITools.mapOX = r0
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x0068
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x003b:
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 != 0) goto L_0x006f
                    float r0 = com.sg.td.message.GameUITools.mapOX
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 >= 0) goto L_0x006f
                L_0x004d:
                    return
                L_0x004e:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    float r1 = com.sg.td.message.GameUITools.mapChoiceX
                    float r0 = r0 - r1
                    goto L_0x000b
                L_0x005a:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    float r1 = com.sg.td.message.GameUITools.mapChoiceX
                    float r0 = r0 - r1
                    goto L_0x001b
                L_0x0066:
                    r6 = r7
                    goto L_0x0028
                L_0x0068:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x003b
                L_0x006f:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00ae
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x0079:
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 != 0) goto L_0x0085
                    float r0 = com.sg.td.message.GameUITools.mapOX
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 > 0) goto L_0x004d
                L_0x0085:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00b5
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x008f:
                    float r1 = com.sg.td.message.GameUITools.mapOX
                    float r0 = r0 + r1
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
                    if (r0 >= 0) goto L_0x00c8
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00bc
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    r0.setPosition(r1, r3)
                    goto L_0x004d
                L_0x00ae:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x0079
                L_0x00b5:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x008f
                L_0x00bc:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r1 = r1
                    float r2 = r2
                    float r1 = r1 - r2
                    float r1 = -r1
                    r0.setPosition(r3, r1)
                    goto L_0x004d
                L_0x00c8:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00e2
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getX()
                L_0x00d2:
                    float r1 = com.sg.td.message.GameUITools.mapOX
                    float r0 = r0 + r1
                    int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
                    if (r0 <= 0) goto L_0x00e9
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    r0.setPosition(r3, r3)
                    goto L_0x004d
                L_0x00e2:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    float r0 = r0.getY()
                    goto L_0x00d2
                L_0x00e9:
                    boolean r0 = r3
                    if (r0 == 0) goto L_0x00ff
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                    float r2 = com.sg.td.message.GameUITools.mapOX
                    float r1 = r1 + r2
                    r0.setPosition(r1, r3)
                    goto L_0x004d
                L_0x00ff:
                    com.badlogic.gdx.scenes.scene2d.Group r0 = r4
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    float r2 = com.sg.td.message.GameUITools.mapOX
                    float r1 = r1 + r2
                    r0.setPosition(r3, r1)
                    goto L_0x004d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.sg.td.message.GameUITools.AnonymousClass1.touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent, float, float, int):void");
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                float y2;
                Action move1;
                GameUITools.isDragged = false;
                if (z) {
                    y2 = group2.getX();
                } else {
                    y2 = group2.getY();
                }
                if (y2 != (-(f - f2)) || GameUITools.mapOX >= Animation.CurveTimeline.LINEAR) {
                    if ((z ? group2.getX() : group2.getY()) != Animation.CurveTimeline.LINEAR || GameUITools.mapOX <= Animation.CurveTimeline.LINEAR) {
                        if ((z ? group2.getX() : group2.getY()) >= (-(f - f2))) {
                            if ((z ? group2.getX() : group2.getY()) > Animation.CurveTimeline.LINEAR) {
                                group2.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                            } else {
                                if (z) {
                                    group2.setPosition(group2.getX() + GameUITools.mapOX, Animation.CurveTimeline.LINEAR);
                                } else {
                                    group2.setPosition(Animation.CurveTimeline.LINEAR, group2.getY() + GameUITools.mapOX);
                                }
                                if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) < Animation.CurveTimeline.LINEAR) {
                                    if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) > (-(f - f2))) {
                                        if (z) {
                                            move1 = Actions.moveBy(GameUITools.mapOX * f3, Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                        } else {
                                            move1 = Actions.moveBy(Animation.CurveTimeline.LINEAR, GameUITools.mapOX * f3, 0.5f, Interpolation.pow5Out);
                                        }
                                        group2.addAction(move1);
                                    }
                                }
                                if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) >= Animation.CurveTimeline.LINEAR) {
                                    move1 = Actions.moveTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                } else if (z) {
                                    move1 = Actions.moveTo(-(f - f2), Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                } else {
                                    move1 = Actions.moveTo(Animation.CurveTimeline.LINEAR, -(f - f2), 0.5f, Interpolation.pow5Out);
                                }
                                group2.addAction(move1);
                            }
                        } else if (z) {
                            group2.setPosition(-(f - f2), Animation.CurveTimeline.LINEAR);
                        } else {
                            group2.setPosition(Animation.CurveTimeline.LINEAR, -(f - f2));
                        }
                        float unused = GameUITools.mapX = Animation.CurveTimeline.LINEAR;
                        float unused2 = GameUITools.mapOX = Animation.CurveTimeline.LINEAR;
                    }
                }
            }
        };
    }

    public static InputListener getMapMoveListener(Group group, float width, float offWidth, float power, boolean isX) {
        final float f = width;
        final float f2 = offWidth;
        final boolean z = isX;
        final Group group2 = group;
        final float f3 = power;
        return new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (pointer != 0 || f < f2) {
                    return false;
                }
                if (!z) {
                    x = y;
                }
                float unused = GameUITools.mapX = x;
                float unused2 = GameUITools.mapChoiceX = z ? group2.getX() : group2.getY();
                return true;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
                if ((r3 ? r4.getX() : r4.getY() - com.sg.td.message.GameUITools.access$100()) < -15.0f) goto L_0x003c;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent r6, float r7, float r8, int r9) {
                /*
                    r5 = this;
                    r4 = 0
                    java.io.PrintStream r1 = java.lang.System.out
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "pointer::"
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.StringBuilder r2 = r2.append(r9)
                    java.lang.String r2 = r2.toString()
                    r1.println(r2)
                    if (r9 == 0) goto L_0x001c
                L_0x001b:
                    return
                L_0x001c:
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x00c5
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                L_0x0026:
                    r2 = 1097859072(0x41700000, float:15.0)
                    int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                    if (r1 > 0) goto L_0x003c
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x00d2
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                L_0x0036:
                    r2 = -1049624576(0xffffffffc1700000, float:-15.0)
                    int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                    if (r1 >= 0) goto L_0x003f
                L_0x003c:
                    r1 = 1
                    com.sg.td.message.GameUITools.isDragged = r1
                L_0x003f:
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x00df
                L_0x0043:
                    float r1 = com.sg.td.message.GameUITools.mapX
                    float r1 = r7 - r1
                    float unused = com.sg.td.message.GameUITools.mapOX = r1
                    float r0 = com.sg.td.message.GameUITools.getStopY()
                    java.io.PrintStream r1 = java.lang.System.out
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "stopXXX::"
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.StringBuilder r2 = r2.append(r0)
                    java.lang.String r2 = r2.toString()
                    r1.println(r2)
                    java.io.PrintStream r1 = java.lang.System.out
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "group.getY()::"
                    java.lang.StringBuilder r2 = r2.append(r3)
                    com.badlogic.gdx.scenes.scene2d.Group r3 = r4
                    float r3 = r3.getY()
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    r1.println(r2)
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x00e2
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                L_0x0090:
                    float r2 = r1
                    float r3 = r2
                    float r2 = r2 - r3
                    float r2 = -r2
                    int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                    if (r1 != 0) goto L_0x00a2
                    float r1 = com.sg.td.message.GameUITools.mapOX
                    int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
                    if (r1 < 0) goto L_0x001b
                L_0x00a2:
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x00e9
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                L_0x00ac:
                    r2 = -998244352(0xffffffffc4800000, float:-1024.0)
                    int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                    if (r1 >= 0) goto L_0x001b
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
                    if (r1 <= 0) goto L_0x00f0
                    java.io.PrintStream r1 = java.lang.System.out
                    java.lang.String r2 = "bunengtuo"
                    r1.println(r2)
                    goto L_0x001b
                L_0x00c5:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    float r2 = com.sg.td.message.GameUITools.mapChoiceX
                    float r1 = r1 - r2
                    goto L_0x0026
                L_0x00d2:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    float r2 = com.sg.td.message.GameUITools.mapChoiceX
                    float r1 = r1 - r2
                    goto L_0x0036
                L_0x00df:
                    r7 = r8
                    goto L_0x0043
                L_0x00e2:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    goto L_0x0090
                L_0x00e9:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    goto L_0x00ac
                L_0x00f0:
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x011a
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                L_0x00fa:
                    float r2 = com.sg.td.message.GameUITools.mapOX
                    float r1 = r1 + r2
                    float r2 = r1
                    float r3 = r2
                    float r2 = r2 - r3
                    float r2 = -r2
                    int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
                    if (r1 >= 0) goto L_0x012e
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x0121
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r2 = r1
                    float r3 = r2
                    float r2 = r2 - r3
                    float r2 = -r2
                    r1.setPosition(r2, r4)
                    goto L_0x001b
                L_0x011a:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    goto L_0x00fa
                L_0x0121:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r2 = r1
                    float r3 = r2
                    float r2 = r2 - r3
                    float r2 = -r2
                    r1.setPosition(r4, r2)
                    goto L_0x001b
                L_0x012e:
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x0148
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getX()
                L_0x0138:
                    float r2 = com.sg.td.message.GameUITools.mapOX
                    float r1 = r1 + r2
                    int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
                    if (r1 <= 0) goto L_0x014f
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    r1.setPosition(r4, r4)
                    goto L_0x001b
                L_0x0148:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    float r1 = r1.getY()
                    goto L_0x0138
                L_0x014f:
                    boolean r1 = r3
                    if (r1 == 0) goto L_0x0165
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    com.badlogic.gdx.scenes.scene2d.Group r2 = r4
                    float r2 = r2.getX()
                    float r3 = com.sg.td.message.GameUITools.mapOX
                    float r2 = r2 + r3
                    r1.setPosition(r2, r4)
                    goto L_0x001b
                L_0x0165:
                    com.badlogic.gdx.scenes.scene2d.Group r1 = r4
                    com.badlogic.gdx.scenes.scene2d.Group r2 = r4
                    float r2 = r2.getY()
                    float r3 = com.sg.td.message.GameUITools.mapOX
                    float r2 = r2 + r3
                    r1.setPosition(r4, r2)
                    goto L_0x001b
                */
                throw new UnsupportedOperationException("Method not decompiled: com.sg.td.message.GameUITools.AnonymousClass2.touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent, float, float, int):void");
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Action move1;
                GameUITools.isDragged = false;
                if (GameUITools.point == 0) {
                    float stopXX = GameUITools.getStopY();
                    if ((z ? group2.getX() : group2.getY()) != (-(f - f2)) || GameUITools.mapOX >= Animation.CurveTimeline.LINEAR) {
                        if ((z ? group2.getX() : group2.getY()) >= -1024.0f) {
                            group2.setPosition(Animation.CurveTimeline.LINEAR, -1025.0f);
                            return;
                        }
                        if (group2.getY() > stopXX) {
                            System.out.println("UP stopX:" + stopXX);
                            group2.setPosition(Animation.CurveTimeline.LINEAR, stopXX);
                        } else {
                            if ((z ? group2.getX() : group2.getY()) >= (-(f - f2))) {
                                if ((z ? group2.getX() : group2.getY()) > Animation.CurveTimeline.LINEAR) {
                                    group2.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                                } else {
                                    if (z) {
                                        group2.setPosition(group2.getX() + GameUITools.mapOX, Animation.CurveTimeline.LINEAR);
                                    } else {
                                        group2.setPosition(Animation.CurveTimeline.LINEAR, group2.getY() + GameUITools.mapOX);
                                    }
                                    if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) < Animation.CurveTimeline.LINEAR) {
                                        if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) > (-(f - f2))) {
                                            if (z) {
                                                move1 = Actions.moveBy(GameUITools.mapOX * f3, Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                            } else {
                                                move1 = Actions.moveBy(Animation.CurveTimeline.LINEAR, GameUITools.mapOX * f3, 0.5f, Interpolation.pow5Out);
                                            }
                                            group2.addAction(move1);
                                        }
                                    }
                                    if ((z ? group2.getX() : group2.getY()) + GameUITools.mapOX + (GameUITools.mapOX * f3) >= Animation.CurveTimeline.LINEAR) {
                                        move1 = Actions.moveTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                    } else if (z) {
                                        move1 = Actions.moveTo(-(f - f2), Animation.CurveTimeline.LINEAR, 0.5f, Interpolation.pow5Out);
                                    } else {
                                        move1 = Actions.moveTo(Animation.CurveTimeline.LINEAR, -(f - f2), 0.5f, Interpolation.pow5Out);
                                    }
                                    group2.addAction(move1);
                                }
                            } else if (z) {
                                group2.setPosition(-(f - f2), Animation.CurveTimeline.LINEAR);
                            } else {
                                group2.setPosition(Animation.CurveTimeline.LINEAR, -(f - f2));
                            }
                        }
                        float unused = GameUITools.mapX = Animation.CurveTimeline.LINEAR;
                        float unused2 = GameUITools.mapOX = Animation.CurveTimeline.LINEAR;
                    }
                }
            }
        };
    }

    public static float getStopY() {
        int rolerank = RankData.getRoleRankIndex();
        System.out.println("getStopY rolerank:" + rolerank);
        if (rolerank > 0 && rolerank < 7) {
            return -5067.0f;
        }
        if (6 < rolerank && rolerank < 13) {
            return -4516.0f;
        }
        if (12 < rolerank && rolerank < 19) {
            return -4054.0f;
        }
        if (18 < rolerank && rolerank < 25) {
            return -3281.0f;
        }
        if (24 < rolerank && rolerank < 31) {
            return -2818.0f;
        }
        if (30 < rolerank && rolerank < 37) {
            return -2235.0f;
        }
        if (36 < rolerank && rolerank < 43) {
            return -1639.0f;
        }
        if (42 < rolerank && rolerank < 49) {
            return -1164.0f;
        }
        if (rolerank > 49) {
            return -1085.0f;
        }
        return Animation.CurveTimeline.LINEAR;
    }

    public static void GetbackgroundImage(int x, int y, int circulationNum, Group group, boolean f, boolean isHavecloud) {
        int midNum = circulationNum;
        int bgX = x;
        int bgY = y;
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC001, bgX, bgY, 1, group);
        for (int i = 0; i <= midNum; i++) {
            new ActorImage((int) PAK_ASSETS.IMG_PUBLIC002, bgX, bgY + 64 + (64 * i), 1, group);
        }
        new ActorImage(f ? PAK_ASSETS.IMG_PUBLIC004 : PAK_ASSETS.IMG_PUBLIC003, bgX, bgY + 128 + (64 * midNum), 1, group);
        if (isHavecloud) {
            new ActorImage((int) PAK_ASSETS.IMG_PUBLIC006, bgX, ((bgY + 64) + (64 * midNum)) - 15, 1, group);
        }
    }

    public void init() {
    }

    public void exit() {
    }
}
