package com.sg.td;

import com.sg.pak.GameConstant;
import com.sg.td.actor.TowerFrag;
import com.sg.tools.GameTime;

public class WaveData implements GameConstant {
    int bossHp;
    int deckCleanNum_rank_save;
    int eatNum_rank_save;
    int homeBlood;
    boolean roleBuild;
    int roleX;
    int roleY;
    int time_min;
    int time_sec;
    Deck[] waveDeck;
    TowerFrag[] waveFrag;
    int waveMoney;
    int waveNum;
    Tower[] waveTower;

    /* access modifiers changed from: protected */
    public void save() {
        Tower tower;
        this.waveNum = Rank.level.wave;
        this.waveMoney = Rank.money;
        this.homeBlood = Rank.home.curBlood;
        this.waveTower = new Tower[Rank.tower.size()];
        this.time_min = GameTime.getGameTime_min();
        this.time_sec = GameTime.getGameTime_sec();
        this.eatNum_rank_save = RankData.eatNum_rank;
        this.deckCleanNum_rank_save = RankData.deckCleanNum_rank;
        Rank.rankUI.saveStar();
        for (int i = 0; i < Rank.tower.size(); i++) {
            if (Rank.tower.get(i).isDead() || Rank.tower.get(i).isRole || Rank.tower.get(i).isRandom) {
                this.waveTower[i] = null;
            } else {
                if (Rank.tower.get(i).isBox) {
                    tower = new TowerBox(Rank.tower.get(i).animationPack, Rank.tower.get(i).animationName);
                } else {
                    tower = new Tower(Rank.tower.get(i).animationPack, Rank.tower.get(i).getAnimaName_stop());
                }
                tower.init(Rank.tower.get(i).x, Rank.tower.get(i).y, Rank.tower.get(i).id, Rank.tower.get(i).name);
                tower.setLevel(Rank.tower.get(i).level);
                tower.initLevleData();
                tower.setXY();
                if (Rank.tower.get(i).isBox) {
                    tower.setRange(Rank.tower.get(i).getRange());
                    tower.setCD(Rank.tower.get(i).getCD());
                    tower.setPower(Rank.tower.get(i).getPower());
                } else {
                    tower.setEatCDTime(Rank.tower.get(i).getEatCDTime());
                    tower.setEatPowerTime(Rank.tower.get(i).getEatPowerTime());
                    tower.setEatRangeTime(Rank.tower.get(i).getEatRangeTime());
                }
                tower.setBlock(Rank.tower.get(i).isBlock());
                this.waveTower[i] = tower;
            }
        }
        this.waveDeck = new Deck[Rank.deck.size()];
        int index = 0;
        for (int i2 = 0; i2 < Rank.deck.size(); i2++) {
            if (!Rank.deck.get(i2).isDead()) {
                Deck deck = new Deck();
                deck.init(Rank.deck.get(i2).x, Rank.deck.get(i2).y, Rank.deck.get(i2).type, index, Rank.deck.get(i2).blockIndex, Rank.deck.get(i2).flip, Rank.deck.get(i2).dropTower, Rank.deck.get(i2).dropString);
                deck.setDropIndex(Rank.deck.get(i2).dropIndex);
                deck.hp = Rank.deck.get(i2).hp;
                index++;
                this.waveDeck[i2] = deck;
            }
        }
        this.waveFrag = new TowerFrag[Rank.frag.size()];
        for (int i3 = 0; i3 < Rank.frag.size(); i3++) {
            this.waveFrag[i3] = new TowerFrag(Rank.frag.get(i3).x, Rank.frag.get(i3).y);
        }
        if (Rank.role != null) {
            this.roleX = Rank.role.x;
            this.roleY = Rank.role.y;
            this.roleBuild = true;
        } else {
            this.roleBuild = false;
        }
        if (Rank.boss != null) {
            this.bossHp = Rank.boss.hp;
        }
    }

    /* access modifiers changed from: protected */
    public void reset() {
        Rank.clearFocus();
        Rank.level.wave = this.waveNum;
        Rank.money = this.waveMoney;
        Rank.home.curBlood = this.homeBlood;
        GameTime.resetTime(this.time_min, this.time_sec);
        RankData.eatNum_rank = this.eatNum_rank_save;
        RankData.deckCleanNum_rank = this.deckCleanNum_rank_save;
        Rank.rankUI.resetStar();
        Rank.clearFocus();
        for (int i = 0; i < Rank.buff.size(); i++) {
            Rank.buff.get(i).remove();
        }
        Rank.buff.removeAllElements();
        int i2 = 0;
        while (Rank.bullet.size() > 0) {
            Rank.bullet.get(i2).removeEffect();
            Rank.bullet.get(i2).removeStage();
            i2 = (i2 - 1) + 1;
        }
        Rank.bullet.clear();
        for (int i3 = 0; i3 < Rank.flyFood.size(); i3++) {
            Rank.flyFood.get(i3).removeStage();
        }
        Rank.flyFood.removeAllElements();
        int i4 = 0;
        while (Rank.fruit.size() > 0) {
            Rank.fruit.get(i4).removeStage();
            i4 = (i4 - 1) + 1;
        }
        Rank.fruit.removeAllElements();
        int i5 = 0;
        while (i5 < Rank.enemy.size()) {
            if (!Rank.enemy.get(i5).boss) {
                Rank.enemy.get(i5).removeStage_enemy();
                Rank.enemy.get(i5).blood.free();
                Rank.enemy.remove(i5);
                i5--;
            }
            i5++;
        }
        Rank.level.initEnemy();
        int i6 = 0;
        while (i6 < Rank.tower.size()) {
            if (!Rank.tower.get(i6).isRole) {
                Rank.tower.get(i6).removeStage();
                Rank.tower.remove(i6);
                i6--;
            } else {
                Rank.tower.get(i6).clearTarget();
            }
            i6++;
        }
        for (int i7 = 0; i7 < this.waveTower.length; i7++) {
            if (this.waveTower[i7] != null) {
                this.waveTower[i7].addStage();
                Rank.tower.add(this.waveTower[i7]);
            }
        }
        for (int i8 = 0; i8 < Rank.deck.size(); i8++) {
            Rank.deck.get(i8).removeStage();
            Rank.deck.get(i8).blood.free();
        }
        Rank.deck.clear();
        for (int i9 = 0; i9 < this.waveDeck.length; i9++) {
            if (this.waveDeck[i9] != null) {
                this.waveDeck[i9].addDeck();
                Rank.deck.add(this.waveDeck[i9]);
            }
        }
        Rank.setGride();
        for (int i10 = 0; i10 < Rank.frag.size(); i10++) {
            Rank.frag.get(i10).removeStage();
        }
        Rank.frag.removeAllElements();
        for (int i11 = 0; i11 < this.waveFrag.length; i11++) {
            if (this.waveFrag[i11] != null) {
                this.waveFrag[i11].addToStage();
                Rank.frag.add(this.waveFrag[i11]);
            }
        }
        if (!this.roleBuild) {
            if (Rank.role != null) {
                Rank.role.roleRemoveStage();
                Rank.tower.remove(Rank.role);
                Rank.role = null;
                Rank.setGride();
            }
        } else if (Rank.role != null) {
            Rank.role.setMove(this.roleX, this.roleY);
        }
        if (Rank.boss != null) {
            Rank.boss.hp = this.bossHp;
            Rank.boss.removeGuaiEffect_all();
            Rank.boss.setPosition_boss();
            Rank.boss.blood.canNotSee(true);
            Rank.boss.setSee();
        }
        if (Rank.boss.boxArray != null) {
            for (int i12 = 0; i12 < Rank.boss.boxArray.size(); i12++) {
                if (Rank.boss.boxArray.get(i12) != null) {
                    Rank.boss.boxArray.get(i12).removeBox();
                }
            }
        }
        if (Rank.boss.iceArray != null) {
            for (int i13 = 0; i13 < Rank.boss.iceArray.size(); i13++) {
                if (Rank.boss.iceArray.get(i13) != null) {
                    Rank.boss.iceArray.get(i13).removeIce();
                }
            }
        }
    }
}
