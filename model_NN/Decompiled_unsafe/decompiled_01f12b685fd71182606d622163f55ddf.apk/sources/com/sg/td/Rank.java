package com.sg.td;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Sort;
import com.kbz.Sound.GameSound;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.UI.MyJiDi;
import com.sg.td.UI.MySupply;
import com.sg.td.UI.MyUpgrade;
import com.sg.td.actor.Buff;
import com.sg.td.actor.DropFood;
import com.sg.td.actor.FlyFood;
import com.sg.td.actor.Focus;
import com.sg.td.actor.Gride;
import com.sg.td.actor.Icon;
import com.sg.td.actor.Select;
import com.sg.td.actor.TowerFrag;
import com.sg.td.data.LevelData;
import com.sg.td.data.Mydata;
import com.sg.td.data.TowerData;
import com.sg.td.group.AchieveTip;
import com.sg.td.group.AwardUI;
import com.sg.td.group.BossUI;
import com.sg.td.group.Fail;
import com.sg.td.group.Home;
import com.sg.td.group.Illustration;
import com.sg.td.group.MidMenu;
import com.sg.td.group.RankUI;
import com.sg.td.group.ReTry;
import com.sg.td.group.ReadyGo;
import com.sg.td.group.Relive;
import com.sg.td.group.ShowProp;
import com.sg.td.group.ShowStar;
import com.sg.td.group.ShowTalk;
import com.sg.td.group.ShowTower;
import com.sg.td.group.WaveBegin;
import com.sg.td.group.Win;
import com.sg.td.kill.Kill;
import com.sg.util.GameLayer;
import com.sg.util.GameScreen;
import com.sg.util.GameStage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Vector;

public class Rank extends GameScreen implements GameConstant {
    public static boolean ENDLESS_MODE = false;
    public static int FortHP;
    public static int MODE;
    public static int TYPE;
    public static float addHp;
    public static ArrayList<AwardUI> award = new ArrayList<>();
    public static Boss boss;
    public static BossUI bossUI;
    public static Vector<Buff> buff = new Vector<>();
    public static Building building;
    public static ArrayList<Bullet> bullet = new ArrayList<>();
    public static boolean consumePower;
    public static boolean cry;
    public static LevelData data;
    public static ArrayList<Deck> deck = new ArrayList<>();
    public static Vector<Enemy> enemy = new Vector<>();
    static boolean enemyPause;
    public static Vector<FlyFood> flyFood = new Vector<>();
    static Focus foc;
    public static int focus;
    static byte focusType;
    public static Vector<TowerFrag> frag = new Vector<>();
    public static Vector<DropFood> fruit = new Vector<>();
    public static byte gameSpeed = 1;
    public static Vector<Gride> gride = new Vector<>();
    public static float hardAddHp;
    public static Vector<String> hideTower = new Vector<>();
    public static Home home;
    public static Vector<Icon> icon = new Vector<>();
    public static Illustration illustration;
    public static String introObj;
    public static boolean introTower;
    public static Kill kill;
    static boolean lastPause;
    public static Level level;
    public static Map map;
    public static String mapName;
    public static Rank me;
    public static int money;
    public static float normalAddHp;
    static float oneSecond;
    static boolean pause;
    public static int rank = 1;
    public static int rankId = 0;
    public static boolean rankInitComplete;
    public static RankUI rankUI;
    public static boolean ranking;
    public static TowerRole role;
    static boolean roleMove;
    static Select select;
    public static Gride selectGride;
    static boolean selectTower;
    public static int selectTowerIndex = -1;
    static boolean selectTowerPressed;
    static int selectTowerPressedX;
    static int selectTowerPressedY;
    public static ShowProp showProp;
    public static ShowStar showStar;
    static Sort sort = Sort.instance();
    public static byte systemEvent;
    static boolean systemPause;
    public static ShowTalk talk;
    public static TowerData.Talk[] talks;
    static float time;
    public static ArrayList<AchieveTip> tip = new ArrayList<>();
    public static Tools tools;
    public static Vector<Tower> tower = new Vector<>();
    public static WaveData waveData;
    boolean pauseBeforeState;

    public void init() {
        me = this;
        initRank();
        loadData();
        setMapName(GameConstant.LEVEL + rank);
        rankId = rank - 1;
        data = Mydata.levelData.get(mapName);
        money = (RankData.useBlank() ? data.getMoney() / 2 : 0) + data.getMoney();
        TYPE = data.getType();
        FortHP = data.getFortHP();
        normalAddHp = data.getNormalAddHp();
        hardAddHp = data.getHardAddHp();
        addHp = hardAddHp - normalAddHp;
        System.out.println("addHp:" + addHp);
        if (TYPE == 3) {
            ENDLESS_MODE = true;
        } else {
            ENDLESS_MODE = false;
        }
        MODE = RankData.getMode();
        introObj = data.getIntroObj();
        level = new Level(mapName);
        map = new Map(GameConstant.LEVEL + rank + ".tmx");
        map.init();
        home = new Home();
        boss = new Boss();
        tools = new Tools();
        Enemy.setPath(Map.path);
        level.initEnemy();
        building = new Building();
        building.initDeck();
        waveData = new WaveData();
        ranking = true;
        setGride();
        setRankSystemEvent();
        playRankMusic();
        rankInitComplete = true;
    }

    /* access modifiers changed from: package-private */
    public void setRankSystemEvent() {
        System.out.println("Rank.introObj:" + introObj);
        if (Mydata.towerData.get(introObj) != null) {
            if (Mydata.towerData.get(introObj).getTalks() == null) {
                setSystemEvent((byte) 7);
            } else {
                talks = new TowerData.Talk[Mydata.towerData.get(introObj).getTalks().length];
                talks = Mydata.towerData.get(introObj).getTalks();
                setSystemEvent((byte) 14);
            }
            introTower = true;
        } else if (Mydata.deckData.get(introObj) != null) {
            if (Mydata.deckData.get(introObj).getTalks() == null) {
                setSystemEvent((byte) 7);
            } else {
                talks = new TowerData.Talk[Mydata.deckData.get(introObj).getTalks().length];
                talks = Mydata.deckData.get(introObj).getTalks();
                setSystemEvent((byte) 14);
            }
            introTower = false;
        } else {
            setSystemEvent((byte) 2);
        }
    }

    public static int getRank() {
        return rank;
    }

    public static void setRank(int id) {
        rank = id;
    }

    public static boolean isRanking() {
        return ranking;
    }

    public static boolean isEasyMode() {
        return MODE == 0;
    }

    public static boolean isHardMode() {
        return MODE == 1;
    }

    /* access modifiers changed from: package-private */
    public void initRank() {
        clear();
        clearSystemEvent();
        pause = false;
        lastPause = false;
        gameSpeed = 1;
        selectTower = false;
        selectTowerIndex = -1;
        select = null;
        clearFocus();
        money = 0;
        role = null;
        roleMove = false;
        showProp = null;
        showStar = null;
        rankInitComplete = false;
        cry = false;
        rankUI = null;
        consumePower = false;
        introTower = false;
        kill = null;
        RankData.initSkillNum();
        RankData.initNum_rank();
        Message.initMessageData();
    }

    public static void clear() {
        System.gc();
        enemy.removeAllElements();
        tower.removeAllElements();
        icon.removeAllElements();
        fruit.removeAllElements();
        gride.removeAllElements();
        flyFood.removeAllElements();
        deck.clear();
        buff.removeAllElements();
        tip.clear();
        award.clear();
        bullet.clear();
        hideTower.clear();
        frag.clear();
    }

    /* access modifiers changed from: package-private */
    public void playRankMusic() {
        if (ENDLESS_MODE) {
            GameSound.playMusic(0);
        } else {
            GameSound.playMusic(3);
        }
    }

    public static void setGride() {
        if (!roleMove) {
            Map.addGride(0);
        }
    }

    public static void setFlashGride() {
        Map.addGride(1);
    }

    /* access modifiers changed from: package-private */
    public void setMapName(String mapName2) {
        mapName = mapName2;
    }

    public static String getMapName() {
        return mapName;
    }

    /* access modifiers changed from: package-private */
    public void moveFlyBuff(float delta) {
        for (int i = 0; i < flyFood.size(); i++) {
            flyFood.get(i).fly(delta);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean touchBuff(int x, int y) {
        for (int i = 0; i < flyFood.size(); i++) {
            if (flyFood.get(i).canTouch(x, y)) {
                flyFood.get(i).drop();
                Sound.playSound(4);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void loadData() {
        Mydata.loadLevelData(rank);
        Mydata.loadMonsterData();
        Mydata.loadDeckData();
    }

    public void run(float delta) {
        float delta2 = delta * ((float) getGameSpeed());
        runAward();
        runTip(delta2);
        if (rankUI != null) {
            rankUI.runPause();
            rankUI.run();
        }
        building.runTowerUp();
        if (role != null) {
            role.move_line_follow();
        }
        building.runTowerRadom(delta2);
        if (!isPause()) {
            if (!isEnemyPause()) {
                enemyRun(delta2);
                boss.move();
            }
            sort();
            RankData.teach.run();
            level.cheackWaveOver(delta2);
            level.runEnemySort();
            building.runTower(delta2);
            runBullet(delta2);
            runDeck(delta2);
            runFruit(delta2);
            moveFlyBuff(delta2);
            home.run();
            runBuff(delta2);
            iconRun();
            runFocus();
            runSelect();
            runEndMode();
        }
    }

    /* access modifiers changed from: package-private */
    public void sort() {
        try {
            sort.sort(GameStage.getLayer(GameLayer.sprite).getChildren(), new Comparator<Actor>() {
                public int compare(Actor o1, Actor o2) {
                    if (o1.getY() < o2.getY()) {
                        return -1;
                    }
                    if (o1.getY() > o2.getY()) {
                        return 1;
                    }
                    return 0;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void runFocus() {
        if (foc != null) {
            foc.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void runSelect() {
        if (select != null) {
            select.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void runEndMode() {
        if (ENDLESS_MODE) {
            if (bossUI != null) {
                bossUI.run();
            }
            if (boss.isDead()) {
                boss.setNotSee();
                setSystemEvent((byte) 11);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void iconRun() {
        for (int i = 0; i < icon.size(); i++) {
            icon.get(i).run();
        }
    }

    /* access modifiers changed from: package-private */
    public void enemyRun(float delta) {
        for (int i = 0; i < enemy.size(); i++) {
            enemy.get(i).run(delta);
        }
    }

    /* access modifiers changed from: package-private */
    public void runBullet(float delta) {
        int i = 0;
        while (i < bullet.size()) {
            if (bullet.get(i).outScreen() && bullet.get(i).canRemove()) {
                bullet.remove(i);
                i--;
            } else if (bullet.get(i).tower.isBlock()) {
                bullet.get(i).removeStage();
                i--;
            } else {
                bullet.get(i).move(delta);
            }
            i++;
        }
    }

    /* access modifiers changed from: package-private */
    public void runDeck(float delta) {
        for (int i = 0; i < deck.size(); i++) {
            if (deck.get(i) != null) {
                deck.get(i).run(delta);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void runFruit(float delta) {
        for (int i = 0; i < fruit.size(); i++) {
            fruit.get(i).run(delta);
        }
    }

    /* access modifiers changed from: package-private */
    public void runBuff(float delta) {
        for (int i = 0; i < buff.size(); i++) {
            buff.get(i).run(delta);
        }
    }

    public static void runTip(float delta) {
        time += delta;
        if (time > oneSecond) {
            int i = 0;
            while (true) {
                if (i >= tip.size()) {
                    break;
                } else if (!tip.get(i).show) {
                    tip.get(i).add();
                    break;
                } else {
                    i++;
                }
            }
            oneSecond += 2.0f;
        }
    }

    /* access modifiers changed from: package-private */
    public void runAward() {
        int i = 0;
        while (i < award.size() && award.get(i).canSee && !award.get(i).showing) {
            if (!award.get(i).show) {
                award.get(i).add();
                return;
            }
            i++;
        }
    }

    public static boolean selectFocus(int x, int y) {
        return level.selectEnemy(x, y) || selectDeck(x, y);
    }

    static boolean selectDeck(int x, int y) {
        if (deck == null) {
            return false;
        }
        for (int i = 0; i < deck.size(); i++) {
            if (deck.get(i) != null) {
                String type = deck.get(i).type;
                if (!"Hero".equals(type) && !"Tower".equals(type)) {
                    int dw = deck.get(i).w;
                    int dh = deck.get(i).h;
                    int dx = deck.get(i).x;
                    int dy = deck.get(i).y;
                    if (deck.get(i).canAttack() && Tools.hit(x, y, 1, 1, dx, dy, dw, dh)) {
                        if (deck.get(i).isFocus()) {
                            clearFocus();
                        } else {
                            setFocus((byte) 2, i);
                        }
                        System.out.println("触摸到装饰物 type:" + type);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void setFocus(byte type, int i) {
        focusType = type;
        focus = i;
        if (foc != null) {
            foc.free();
        }
        foc = new Focus();
    }

    public static boolean isEnemyFocus() {
        return focusType == 1 && focus > -1;
    }

    public static boolean isDeckFocus() {
        return focusType == 2 && focus > -1;
    }

    public static void clearFocus() {
        focus = -1;
        focusType = 0;
        if (foc != null) {
            foc.free();
        }
    }

    public static void exitRank() {
        ranking = false;
        gameSpeed = 1;
    }

    public void dispose() {
        ranking = false;
        gameSpeed = 1;
    }

    public boolean gTouchDown(int screenX, int screenY, int pointer) {
        int dx = Map.getScreenX(screenX);
        int dy = Map.getScreenY(screenY);
        if (isWin() || isFail()) {
            return false;
        }
        if (isTalk()) {
            talk.touchTalk();
            Sound.playSound(69);
            return false;
        } else if (isTeach()) {
            RankData.teach.touchDown(screenX, screenY);
            return true;
        } else if (isSystemEventPause()) {
            return false;
        } else {
            selectTowerPressed = false;
            if (selectTower) {
                selectTower = false;
                select.free();
                return false;
            } else if (isFragArea(dx, dy) && !isBuildIconArea(dx, dy)) {
                return false;
            } else {
                if (selectTower(dx, dy)) {
                    selectTowerPressed = true;
                    selectTowerPressedX = screenX;
                    selectTowerPressedY = screenY;
                }
                return super.gTouchDown(screenX, screenY, pointer);
            }
        }
    }

    public boolean gTouchUp(int screenX, int screenY, int pointer, int button) {
        int dx = Map.getScreenX(screenX);
        int dy = Map.getScreenY(screenY);
        if (isTeach()) {
            RankData.teach.touchUp(screenX, screenY);
        } else if (!touchBuff(screenX, screenY)) {
            if (!icon.isEmpty()) {
                if (!createNewTower(screenX, screenY)) {
                    removeIcon();
                }
                System.out.println("种植炮塔");
            } else if (selectTowerPressed && selectTowerPressedY - screenY > 10) {
                selectTowerPressed = false;
                System.out.println("滑动炮塔");
                building.updateTower();
            } else if (selectTower(dx, dy)) {
                if (selectTowerPressed) {
                    System.out.println("选中炮塔");
                    select = new Select();
                    selectTower = true;
                    selectTowerPressed = false;
                    Sound.playSound(69);
                }
            } else if (selectFocus(screenX, screenY)) {
                System.out.println("选择优先攻击的目标");
            } else if (Map.isOutMap(screenX, screenY)) {
                System.out.println("出界");
            } else {
                if (!Map.isPhy(screenX, screenY)) {
                    if (roleMove) {
                        roleMove = false;
                        setRoleMove(dx, dy);
                        setConsumePower();
                    } else {
                        if (icon.isEmpty()) {
                            addTowerIcon(dx, dy);
                            addSelectGride(dx, dy);
                            Sound.playSound(69);
                        }
                        System.out.println("选中空地");
                    }
                } else if (isHomeArea(dx, dy)) {
                    setHomeUpEvent();
                    Sound.playSound(69);
                    System.out.println("选中基地");
                } else {
                    Sound.playSound(26);
                }
                roleMove = false;
                setGride();
            }
        }
        return true;
    }

    public boolean gTouchDragged(int screenX, int screenY, int pointer) {
        return true;
    }

    public static boolean selectEmptyFloor(int dx, int dy) {
        if (!icon.isEmpty()) {
            return false;
        }
        addTowerIcon(dx, dy);
        addSelectGride(dx, dy);
        Sound.playSound(69);
        return true;
    }

    public static void buildTower(int dx, int dy) {
        createNewTower(dx, dy);
        removeIcon();
    }

    static void addSelectGride(int x, int y) {
        selectGride = new Gride(x, y, 0);
    }

    static void removeSelectGride() {
        selectGride.remove();
    }

    /* access modifiers changed from: package-private */
    public void setRoleMove(int aimX, int aimY) {
        role.setMove(aimX, aimY);
        if (!RankData.useShose()) {
            money -= 50;
        }
    }

    static void addTowerIcon(int dx, int dy) {
        int rowNum;
        int startX;
        String towerName;
        int num = Mydata.levelData.get(getMapName()).towers.size();
        int total = num + Mydata.levelData.get(getMapName()).hideTowers.size() + (role == null ? 1 : 0);
        int buildX = dx;
        int buildY = dy;
        int rowId = ((dy - 183) / Map.getTileHight()) + 1;
        int row = ((total - 1) / 5) + 1;
        int dy2 = dy - (80 * row);
        if (total > 5) {
            rowNum = 5;
        } else {
            rowNum = total;
        }
        int columnId = ((dx - 40) / Map.getTileWidth()) + 1;
        if (rowId >= 1 && rowId <= 3) {
            dy2 += (row + 1) * 80;
        }
        if (columnId >= 1 && columnId <= 3) {
            startX = dx;
        } else if (columnId >= 6 && columnId <= 8) {
            startX = dx - ((rowNum - 1) * 75);
        } else if (rowNum % 2 == 0) {
            startX = (dx - ((rowNum / 2) * 75)) + 37;
        } else {
            startX = dx - ((rowNum / 2) * 75);
        }
        for (int i = 0; i < total; i++) {
            boolean hide = false;
            if (role == null) {
                if (i == 0) {
                    towerName = ROLE_NAME[RankData.getSelectRoleIndex()];
                } else if (i < num + 1) {
                    towerName = Mydata.levelData.get(getMapName()).towers.get(i - 1);
                } else {
                    towerName = Mydata.levelData.get(getMapName()).hideTowers.get((i - num) - 1);
                    hide = true;
                }
            } else if (i < num) {
                towerName = Mydata.levelData.get(getMapName()).towers.get(i);
            } else {
                towerName = Mydata.levelData.get(getMapName()).hideTowers.get(i - num);
                hide = true;
            }
            Icon c = new Icon(towerName, ((i % 5) * 75) + startX, ((i / 5) * 80) + dy2, Mydata.towerData.get(towerName).getMoney(0), towerName, hide);
            c.init(buildX, buildY);
            icon.add(c);
        }
    }

    static void addTowerIcon_test(int dx, int dy) {
        int rowNum;
        int startX;
        String[] tNames = {"Random", "MoneyBox", "HuoChai", "PenSheQi", "LengDong", "HuiXuanBiao", "HuoGuo", "PingGuoZhaDan", "MuChui", "LengGuang", "YuSan", "ZuQiu", "YanHua"};
        int num = tNames.length;
        int buildX = dx;
        int buildY = dy;
        int rowId = ((dy - 183) / Map.getTileHight()) + 1;
        int row = (((num + 0) - 1) / 5) + 1;
        int dy2 = dy - (80 * row);
        if (num > 5) {
            rowNum = 5;
        } else {
            rowNum = num;
        }
        int columnId = ((dx - 40) / Map.getTileWidth()) + 1;
        if (rowId >= 1 && rowId <= 3) {
            dy2 += (row + 1) * 80;
        }
        if (columnId >= 1 && columnId <= 3) {
            startX = dx;
        } else if (columnId >= 6 && columnId <= 8) {
            startX = dx - ((rowNum - 1) * 75);
        } else if (rowNum % 2 == 0) {
            startX = (dx - ((rowNum / 2) * 75)) + 37;
        } else {
            startX = dx - ((rowNum / 2) * 75);
        }
        for (int i = 0; i < tNames.length; i++) {
            String towerName = tNames[i];
            Icon c = new Icon(towerName, ((i % 5) * 75) + startX, ((i / 5) * 80) + dy2, Mydata.towerData.get(towerName).getMoney(0), towerName, false);
            c.init(buildX, buildY);
            icon.add(c);
        }
    }

    public static boolean createNewTower(int x, int y) {
        for (int i = 0; i < icon.size(); i++) {
            Icon c = icon.get(i);
            if (Tools.hit(x, y, 1, 1, c.x - (c.w / 2), c.y - (c.h / 2), c.w, c.h)) {
                c.createTower();
                setConsumePower();
                return true;
            }
        }
        return false;
    }

    public static boolean isBuildIconArea(int x, int y) {
        for (int i = 0; i < icon.size(); i++) {
            Icon c = icon.get(i);
            if (Tools.hit(x, y, 1, 1, c.x - (c.w / 2), c.y - (c.h / 2), c.w, c.h)) {
                return true;
            }
        }
        return false;
    }

    public static void iconsMove(int bx, int by) {
        for (int i = 0; i < icon.size(); i++) {
            icon.get(i).setRemove();
            icon.get(i).group.addAction(Actions.sequence(Actions.parallel(Actions.scaleTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.2f)), Actions.run(new Runnable() {
                public void run() {
                    Rank.removeIcon();
                }
            })));
        }
    }

    public static void removeIcon() {
        for (int i = 0; i < icon.size(); i++) {
            icon.get(i).removeStage();
        }
        icon.removeAllElements();
        removeSelectGride();
    }

    public static boolean isTowerArea(int x, int y) {
        for (int i = 0; i < tower.size(); i++) {
            Tower t = tower.get(i);
            if (!t.isDead() && !t.isBlock()) {
                int tx = Map.getScreenX(t.x);
                int ty = Map.getScreenY(t.y);
                if (x == tx && y == ty) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean selectTower(int x, int y) {
        for (int i = 0; i < tower.size(); i++) {
            Tower t = tower.get(i);
            if (!t.isDead() && !t.isBlock()) {
                int tx = Map.getScreenX(t.x);
                int ty = Map.getScreenY(t.y);
                if (x == tx && y == ty) {
                    selectTowerIndex = i;
                    System.out.println("选中了某个塔selectTowerIndex:" + selectTowerIndex);
                    return true;
                }
            }
        }
        return false;
    }

    static boolean isFragArea(int x, int y) {
        for (int i = 0; i < frag.size(); i++) {
            TowerFrag f = frag.get(i);
            int fx = Map.getScreenX(f.x);
            int fy = Map.getScreenY(f.y);
            if (x == fx && y == fy) {
                System.out.println("选中了碎片");
                return true;
            }
        }
        return false;
    }

    public static boolean isDeckArea(int x, int y) {
        if (deck == null) {
            return false;
        }
        for (int i = 0; i < deck.size(); i++) {
            if (deck.get(i) != null && !deck.get(i).isDead() && deck.get(i).isDeck(x, y)) {
                return true;
            }
        }
        return false;
    }

    static boolean isHomeArea(int x, int y) {
        if (home == null || home.isFull()) {
            return false;
        }
        int hx = home.getHomeX();
        int hy = home.getHomeY();
        if (x == hx && y == hy) {
            return true;
        }
        return false;
    }

    public static void setEnemyPause(boolean p) {
        enemyPause = p;
    }

    public static boolean isEnemyPause() {
        return enemyPause;
    }

    public static void setPause(boolean p) {
        lastPause = pause;
        pause = p;
    }

    public static boolean isPause() {
        return pause;
    }

    public static void setGameSpeed(byte speed) {
        gameSpeed = speed;
        if (role != null) {
            role.setRoleAniSpeed();
        }
    }

    public static byte getGameSpeed() {
        return gameSpeed;
    }

    public static void setSystemEvent(byte event) {
        if (systemEvent != 0) {
            System.out.println("系统事件不为空 event:" + ((int) event) + " systemEvent: " + ((int) systemEvent));
            return;
        }
        System.out.println("设置系统事件event:" + ((int) event));
        systemEvent = event;
        switch (systemEvent) {
            case 2:
                new ReadyGo();
                break;
            case 3:
                waveData.save();
                new WaveBegin();
                if (showProp == null) {
                    showProp = new ShowProp();
                }
                setEnemyPause(true);
                setSystemPause(false);
                return;
            case 4:
                new Relive();
                killFree();
                break;
            case 5:
                new Fail();
                break;
            case 6:
                new Win();
                break;
            case 7:
                new ShowTower();
                break;
            case 8:
                waveData.reset();
                killFree();
                new ReTry();
                clearSystemEvent();
                setSystemEvent((byte) 3);
                return;
            case 9:
                Message.showAD();
                new MidMenu();
                killPause(true);
                break;
            case 10:
                illustration = new Illustration();
                setConsumePower();
                break;
            case 11:
            case 18:
                rankUI.free();
                if (bossUI != null) {
                    bossUI.free();
                }
                if (systemEvent == 11) {
                    showStar = new ShowStar((byte) 6);
                } else {
                    showStar = new ShowStar((byte) 5);
                }
                killFree();
                break;
            case 14:
                talk = new ShowTalk();
                break;
            case 15:
                new MySupply(0);
                killPause(true);
                break;
            case 16:
                new MySupply(1);
                break;
        }
        setPause(true);
        setSystemPause(true);
    }

    public static void setRoleUpEvent() {
        int roleId = RankData.selectRoleIndex;
        System.out.println("=======roleId:" + roleId);
        new MyUpgrade(roleId);
        setPause(true);
        setSystemPause(true);
    }

    public static void clearRoleUpEvent() {
        setPause(lastPause);
        setSystemPause(false);
    }

    public static void setHomeUpEvent() {
        new MyJiDi();
        setPause(true);
        setSystemPause(true);
    }

    public static void clearHomeUpEvent() {
        if (isRanking() && home != null) {
            setPause(lastPause);
            setSystemPause(false);
        }
    }

    public static void resetHomeHp() {
        if (isRanking() && home != null) {
            home.initBlood();
        }
    }

    public static void putSkill(boolean count) {
        clearSystemEvent();
        if (RankData.getHoneyNum() >= 1) {
            setSystemEvent((byte) 10);
            RankData.useHoney();
            killFree();
        }
        killPause(false);
        if (count) {
            RankData.addSkillNum();
            RankData.addUseSkill();
        }
    }

    public static void putSkill_free() {
        clearSystemEvent();
        setSystemEvent((byte) 10);
    }

    public static void repalyWave() {
        clearSystemEvent();
        if (RankData.getEndlessReplayDays() > 0) {
            setSystemEvent((byte) 8);
        }
    }

    public static void clearSystemEvent() {
        setPause(false);
        setSystemPause(false);
        systemEvent = 0;
    }

    public static boolean isSystemEvent() {
        return systemEvent != 0;
    }

    public static boolean isTeach() {
        return systemEvent == 13;
    }

    public static boolean isTalk() {
        return systemEvent == 14;
    }

    public static boolean isWin() {
        return systemEvent == 6 || systemEvent == 11;
    }

    public static boolean isFail() {
        return systemEvent == 5 || systemEvent == 18;
    }

    public static boolean isMidSystem() {
        return systemEvent == 9;
    }

    public static boolean isSystemEventPause() {
        return systemPause;
    }

    public static void setSystemPause(boolean p) {
        systemPause = p;
    }

    public static void setConsumePower() {
        consumePower = true;
    }

    public static boolean isConsumePower() {
        return consumePower;
    }

    public static void killFree() {
        if (kill != null) {
            kill.free();
            kill = null;
        }
    }

    public static void killPause(boolean pause2) {
        if (kill != null) {
            kill.setPause(pause2);
        }
    }

    public static void illustrationPause(boolean pause2) {
        if (illustration != null) {
            illustration.setPause(pause2);
        }
    }

    public void pause() {
        super.pause();
        if (!isPause()) {
            setPause(true);
            this.pauseBeforeState = false;
        } else {
            this.pauseBeforeState = true;
        }
        killPause(true);
        illustrationPause(true);
        System.out.println("Rank pause()...");
    }

    public void resume() {
        super.resume();
        System.out.println("pauseBeforeState::" + this.pauseBeforeState);
        if (!this.pauseBeforeState) {
            setPause(false);
        }
        killPause(false);
        illustrationPause(false);
        System.out.println("Rank resume()...");
    }
}
