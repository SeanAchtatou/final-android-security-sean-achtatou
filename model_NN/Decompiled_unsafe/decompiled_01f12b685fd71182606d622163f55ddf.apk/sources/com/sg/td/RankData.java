package com.sg.td;

import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.kbz.Animation.GAnimationManager;
import com.kbz.AssetManger.GRes;
import com.kbz.Particle.GParticleSystem;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Effect;
import com.sg.td.data.LevelData;
import com.sg.td.data.MyBigMapPlace;
import com.sg.td.data.Mydata;
import com.sg.td.group.AchieveTip;
import com.sg.td.group.AwardUI;
import com.sg.td.group.Teach;
import com.sg.td.message.MySwitch;
import com.sg.td.record.AchieveData;
import com.sg.td.record.LoadAchieve;
import com.sg.td.record.LoadBear;
import com.sg.td.record.LoadJiDi;
import com.sg.td.record.LoadStrength;
import com.sg.td.record.LoadTargetData;
import com.sg.td.record.Record;
import com.sg.td.record.Strength;
import com.sg.td.record.TargetData;
import com.sg.tools.OnlineTime;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class RankData implements GameConstant {
    public static int[] achieveComplete = new int[80];
    public static AchieveData.Achieve[] battleAchieves;
    public static int buildNum;
    public static int buyEndlessPaly_Day;
    public static int buyEndlessPaly_Month;
    public static int buyEndlessPaly_Year;
    public static int buyEndlessPower_Day;
    public static int buyEndlessPower_Month;
    public static int buyEndlessPower_Year;
    public static int cakeNum = 2000;
    public static int cakeNum_all;
    public static boolean[] cleanDeck = new boolean[54];
    public static boolean[] cleanDeck_hard = new boolean[54];
    public static boolean[] cleanFood = new boolean[54];
    public static boolean[] cleanFood_hard = new boolean[54];
    static int[] clipTimeLength = {58, PAK_ASSETS.IMG_JIGUANGPENSHEQI, PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_MAP11, PAK_ASSETS.IMG_PUBLIC008};
    public static AchieveData.Achieve[] collectionAchieves;
    public static int completeNum;
    public static int deckCleanNum_rank;
    public static int deckNum;
    public static int deckNum_rank;
    public static int diamondNum;
    public static int eatAllThroughTime;
    public static int eatAllThroughTime_everday;
    public static int eatNum;
    public static int eatNum_rank;
    public static int eatNum_someRank;
    public static int endlessPalyDays;
    public static int endlessPowerDays;
    public static int[] everyDayTargertIndex = new int[3];
    public static int eveyDayGetIndex;
    public static boolean firstPaly = true;
    public static AchieveData.Achieve[] foodAchieves;
    public static int foodNum_rank;
    public static boolean[] fullBlood = new boolean[54];
    public static int fullBloodThroughTime_everday;
    public static boolean[] fullBlood_hard = new boolean[54];
    public static boolean giveMoney;
    private static HashMap<Integer, GParticleSystem> hashMapParticleSystem = new HashMap<>();
    public static int heartNum;
    public static int heartNum_all;
    public static String[] hideTowerIcons;
    public static int historyMinute;
    public static int historyPercent;
    public static int historySecond;
    public static int historyStar;
    public static int honeyNum;
    public static int honeyNum_all;
    public static int jidiLevel = 1;
    public static int lastDay;
    static int lastMinus;
    public static int lastMonth;
    public static int lastOnlineTime;
    static int lastRankNum = -1;
    public static int lastSelectRoleIndex;
    public static int lastYear;
    public static int luckDrawTimes;
    public static byte mode = 0;
    public static int oneBloodThroughTime;
    public static int onlineTime;
    public static int pickFoodTime_everday;
    public static int pickNum;
    public static int powerCard = 5;
    public static int propNum_all;
    public static int[] props_num = new int[4];
    public static boolean[] props_use = new boolean[4];
    public static String questIntro;
    public static AchieveData.Achieve[] rankAchieves;
    public static boolean[] rankDrop = new boolean[54];
    public static int rankMoney;
    public static boolean rankReturnMain;
    public static int[][] rankStar_easy = ((int[][]) Array.newInstance(Integer.TYPE, 54, 4));
    public static int[][] rankStar_hard = ((int[][]) Array.newInstance(Integer.TYPE, 54, 4));
    static boolean rankThreeOver;
    static boolean rankTowOver;
    public static String[] rankTowerIcons;
    public static int[] receiveAward = new int[4];
    static int[] receiveTime = {5, 20, 60, 120, 121};
    public static Record record;
    public static int[] roleLevel = {1, 0, 0, 0};
    public static int roleRankIndex = 1;
    public static int roleRankIndex_hard = 1;
    public static int selectRoleIndex;
    public static int sellTowerTime_everday;
    public static int skillNum_use;
    public static int takeTargetAward;
    public static int[] targetComplete = new int[5];
    public static TargetData targets;
    public static Teach teach;
    public static int time2;
    public static int time3;
    public static long timeStart;
    public static int todayGetIndex;
    public static boolean[] towerClock = new boolean[12];
    public static LinkedHashMap<String, Integer> towerEatNum = new LinkedHashMap<>();
    public static LinkedHashMap<String, Integer> towerHurtNum = new LinkedHashMap<>();
    public static int[] towerLevel = new int[12];
    public static String[] towerNames;
    public static int useSkillTime_everday;

    public static boolean spendCake(int money) {
        if (cakeNum < money) {
            return false;
        }
        cakeNum -= money;
        record.writeRecord(1);
        return true;
    }

    public static boolean spendDiamond(int diamond) {
        if (diamondNum < diamond) {
            return false;
        }
        diamondNum -= diamond;
        record.writeRecord(1);
        return true;
    }

    public static boolean spendHeart(int num) {
        if (heartNum < num) {
            return false;
        }
        heartNum -= num;
        record.writeRecord(1);
        return true;
    }

    public static int getCakeNum() {
        return cakeNum;
    }

    public static int getDiamondNum() {
        return diamondNum;
    }

    public static int getHoneyNum() {
        return honeyNum;
    }

    public static void useHoney() {
        honeyNum--;
        record.writeRecord(1);
    }

    public static int getHeartNum() {
        return heartNum;
    }

    public static int getPowerNum() {
        return powerCard;
    }

    public static int getPropNum(int index) {
        return props_num[index];
    }

    public static int getStarNum() {
        int num = 0;
        if (getMode() == 0) {
            for (int[] iArr : rankStar_easy) {
                num += iArr[0];
            }
        }
        if (getMode() == 1) {
            for (int[] iArr2 : rankStar_hard) {
                num += iArr2[0];
            }
        }
        return num;
    }

    public static int getTotalStarNum() {
        return 162;
    }

    public static void addCakeNum(int num) {
        cakeNum += num;
        cakeNum_all += num;
        record.writeRecord(1);
        cheakCollectionAchieve();
    }

    public static void addCakeNum_free(int num) {
        cakeNum += num;
        record.writeRecord(1);
    }

    public static void addDiamondNum(int num) {
        diamondNum += num;
        record.writeRecord(1);
    }

    public static void addHoneyNum(int num) {
        honeyNum += num;
        honeyNum_all += num;
        record.writeRecord(1);
        cheakCollectionAchieve();
    }

    public static void addHeartNum(int num) {
        heartNum += num;
        heartNum_all += num;
        record.writeRecord(1);
        cheakCollectionAchieve();
    }

    public static void addProps(int index, int num) {
        int[] iArr = props_num;
        iArr[index] = iArr[index] + num;
        addPropNum_all(num);
        record.writeRecord(1);
    }

    public static void buyEndlessReplay(int days) {
        if (getEndlessReplayDays() == 0) {
            Calendar c = Calendar.getInstance();
            buyEndlessPaly_Year = c.get(1);
            buyEndlessPaly_Month = c.get(2) + 1;
            buyEndlessPaly_Day = c.get(5);
            endlessPalyDays = 0;
            System.out.println("购买无限重玩 记录起始时间");
        }
        endlessPalyDays += days;
        System.out.println("购买几天：" + days + " 无限重玩天数：" + endlessPalyDays);
        record.writeRecord(1);
    }

    public static int getEndlessReplayDays() {
        int days = checkDay(Calendar.getInstance(), buyEndlessPaly_Year, buyEndlessPaly_Month, buyEndlessPaly_Day);
        if (days == -1) {
            System.out.println("从来没有购买过");
            return 0;
        }
        System.out.println("使用天数：" + days);
        System.out.println("无限重玩剩余天数:" + (endlessPalyDays - days));
        return Math.max(0, endlessPalyDays - days);
    }

    public static void buyPower(int num) {
        endlessPowerDays = getEndlessPowerDays();
        if (endlessPowerDays <= 0) {
            powerCard += num;
            record.writeRecord(1);
        }
    }

    public static void consumePower() {
        if (getEndlessPowerDays() <= 0) {
            if (powerCard == 5) {
                timeStart = getTime();
                System.out.println("消耗体力：" + timeStart);
            }
            powerCard--;
            record.writeRecord(1);
        }
    }

    public static void buyEndlessPower(int days) {
        if (getEndlessPowerDays() == 0) {
            Calendar c = Calendar.getInstance();
            buyEndlessPower_Year = c.get(1);
            buyEndlessPower_Month = c.get(2) + 1;
            buyEndlessPower_Day = c.get(5);
            endlessPowerDays = 0;
        }
        endlessPowerDays += days;
        record.writeRecord(1);
    }

    public static int getEndlessPowerDays() {
        int days = checkDay(Calendar.getInstance(), buyEndlessPower_Year, buyEndlessPower_Month, buyEndlessPower_Day);
        if (days == -1) {
            return 0;
        }
        return Math.max(0, endlessPowerDays - days);
    }

    public static boolean powerEnough() {
        return getEndlessPowerDays() > 0 || getPowerNum() > 0;
    }

    public static long runPower() {
        if (getEndlessPowerDays() > 0 || powerCard >= 5) {
            return -1;
        }
        long delta = getTime() - timeStart;
        if (delta < 0) {
            timeStart = getTime();
            delta = 0;
        }
        int num = (int) (delta / 300000);
        if (num > 0) {
            powerCard = Math.min(5, powerCard + num);
            timeStart += (long) (GameConstant.LIFE_AUTO_TIME * num);
            record.writeRecord(1);
            System.out.println("增长体力:" + powerCard);
        }
        return 300000 - delta;
    }

    public static long getTime() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static void addEatNum() {
        eatNum++;
        addPickFood();
        cheakFoodAchieve();
    }

    public static void addFoodNum_rank(int num) {
        if (!Rank.rankInitComplete) {
            foodNum_rank += num;
        }
    }

    public static void addDeckNum_rank(int num) {
        if (!Rank.rankInitComplete) {
            deckNum_rank += num;
        }
    }

    public static void initNum_rank() {
        eatNum_rank = 0;
        foodNum_rank = 0;
        eatNum_someRank = 0;
        deckCleanNum_rank = 0;
        deckNum_rank = 0;
    }

    public static void pickAllFood() {
        eatNum_rank++;
        if (cleanFood()) {
            eatAllThroughTime++;
            new Effect().addEffect(36, 320, (int) PAK_ASSETS.IMG_2X1, 4);
        }
    }

    public static boolean cleanFood() {
        if (foodNum_rank != 0 && eatNum_rank == foodNum_rank) {
            return true;
        }
        return false;
    }

    public static boolean cleanDeck() {
        if (deckNum_rank != 0 && deckCleanNum_rank == deckNum_rank) {
            return true;
        }
        return false;
    }

    public static void addPickNum() {
        pickNum++;
        addPickFood();
        cheakFoodAchieve();
    }

    public static void addBuildNum() {
        buildNum++;
        cheakBattleAchieve();
    }

    public static void addSkillNum() {
        skillNum_use++;
        cheakBattleAchieve();
    }

    public static void initSkillNum() {
        skillNum_use = 0;
    }

    public static void addDeckNum() {
        deckNum++;
        cheakBattleAchieve();
        deckCleanNum_rank++;
        if (cleanDeck()) {
            new Effect().addEffect(35, 320, (int) PAK_ASSETS.IMG_2X1, 4);
        }
    }

    public static void addPropNum_all(int num) {
        propNum_all += num;
        cheakCollectionAchieve();
    }

    public static boolean someRankPutSkill(int num) {
        if (skillNum_use >= num) {
            return true;
        }
        return false;
    }

    public static void oneBloodThrough() {
        if (Rank.home.getHomeBlood() == 1) {
            oneBloodThroughTime++;
        }
    }

    public static void addTowerEatNum(String towerName) {
        towerEatNum.put(towerName, Integer.valueOf(towerEatNum.get(towerName).intValue() + 1));
        eatNum_someRank++;
        cheakFoodAchieve();
    }

    public static int getBearEatNum() {
        return towerEatNum.get("Bear1").intValue() + towerEatNum.get("Bear2").intValue() + towerEatNum.get("Bear3").intValue() + towerEatNum.get("Bear4").intValue();
    }

    public static int getBearHurtNum() {
        return towerHurtNum.get("Bear1").intValue() + towerHurtNum.get("Bear2").intValue() + towerHurtNum.get("Bear3").intValue() + towerHurtNum.get("Bear4").intValue();
    }

    public static void addTowerHurtNum(String towerName) {
        towerHurtNum.put(towerName, Integer.valueOf(towerHurtNum.get(towerName).intValue() + 1));
        cheakBattleAchieve();
    }

    public static boolean someTowerEatFoods(int num) {
        return eatNum_someRank >= num;
    }

    public static boolean rankThrough(int rank, int difficulty) {
        return getRankOpenIndex(difficulty) >= rank;
    }

    public static boolean rankThroughWithStar(int rank, int difficulty) {
        if (difficulty == 0) {
            for (int i = 0; i < rank; i++) {
                if (rankStar_easy[i][0] < 3) {
                    return false;
                }
            }
        }
        if (difficulty == 1) {
            for (int i2 = 0; i2 < rank; i2++) {
                if (rankStar_hard[i2][0] < 3) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int getNewRoleNum() {
        int n = 0;
        for (int i = 1; i < roleLevel.length; i++) {
            if (roleLevel[i] > 0) {
                n++;
            }
        }
        return n;
    }

    public static int getFullRoleNum() {
        int n = 0;
        for (int i : roleLevel) {
            if (i >= 10) {
                n++;
            }
        }
        return n;
    }

    public static void saveStar(int star, int minute, int second, int percent) {
        if (Rank.isEasyMode()) {
            if (isNewRecord(minute, second)) {
                rankStar_easy[Rank.rankId][1] = minute;
                rankStar_easy[Rank.rankId][2] = second;
                rankStar_easy[Rank.rankId][3] = percent;
            }
            if (star > rankStar_easy[Rank.rankId][0]) {
                rankStar_easy[Rank.rankId][0] = star;
            }
        }
        if (Rank.isHardMode()) {
            if (isNewRecord(minute, second)) {
                rankStar_hard[Rank.rankId][1] = minute;
                rankStar_hard[Rank.rankId][2] = second;
                rankStar_hard[Rank.rankId][3] = percent;
            }
            if (star > rankStar_hard[Rank.rankId][0]) {
                rankStar_hard[Rank.rankId][0] = star;
            }
        }
    }

    public static void initGameData() {
        MyBigMapPlace.laodData();
        loadAnimation();
        initTowerData();
        teach = new Teach();
        Mydata.loadTowerData();
        LoadJiDi.loadJidiDate();
        LoadBear.loadBearDate();
        LoadStrength.loadStrengthData();
        LoadAchieve.loadAchieveData();
        rankAchieves = LoadAchieve.achieveData.get("Rank").achieves;
        foodAchieves = LoadAchieve.achieveData.get("Food").achieves;
        battleAchieves = LoadAchieve.achieveData.get("Battle").achieves;
        collectionAchieves = LoadAchieve.achieveData.get("Collection").achieves;
        LoadTargetData.loadTargetDate();
        targets = LoadTargetData.targetData;
        towerLevel[0] = 1;
        record = new Record();
        record.readRecord(1);
        towerClock[0] = true;
        resetOnlineTime();
        resetLuckDrawTimes();
        resetTeachData();
        initParticleSystem();
    }

    static void initTowerData() {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Tower.json");
        if (fileString == null) {
            System.out.println("data/Tower.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            String towerName = arrayJsonValue.get(i).name;
            towerEatNum.put(towerName, 0);
            towerHurtNum.put(towerName, 0);
        }
    }

    static void loadAnimation() {
        for (String load : ANIMATION_NAME) {
            GAnimationManager.load(load);
        }
    }

    public static void initParticleSystem() {
        Tools.loadTextureAtlas(11);
        Tools.loadTextureAtlas(12);
    }

    public static GParticleSystem getPartical(int particalIndex) {
        if (particalIndex == -1) {
            return null;
        }
        GParticleSystem kbz = hashMapParticleSystem.get(Integer.valueOf(particalIndex));
        if (kbz != null) {
            return kbz;
        }
        hashMapParticleSystem.put(Integer.valueOf(particalIndex), new GParticleSystem(particalIndex));
        return hashMapParticleSystem.get(Integer.valueOf(particalIndex));
    }

    public static int getParticalIndex(String particalName) {
        for (int i = 0; i < DATAPARTICAL_NAME.length; i++) {
            if (DATAPARTICAL_NAME[i].equals(particalName)) {
                return i;
            }
        }
        return -1;
    }

    public static void readTowerData(int[] num1, int[] num2) {
        JsonReader reader = new JsonReader();
        String fileString = GRes.readTextFile("data/Tower.json");
        if (fileString == null) {
            System.out.println("data/Tower.json" + "  = null");
            return;
        }
        JsonValue arrayJsonValue = reader.parse(fileString);
        for (int i = 0; i < arrayJsonValue.size; i++) {
            String towerName = arrayJsonValue.get(i).name;
            towerEatNum.put(towerName, Integer.valueOf(num1[i]));
            towerHurtNum.put(towerName, Integer.valueOf(num2[i]));
        }
    }

    public static void cheakRankAchieve() {
        for (AchieveData.Achieve achieve : rankAchieves) {
            checkAchieve(achieve);
        }
    }

    public static void cheakFoodAchieve() {
        for (AchieveData.Achieve achieve : foodAchieves) {
            checkAchieve(achieve);
        }
    }

    public static void cheakBattleAchieve() {
        for (AchieveData.Achieve achieve : battleAchieves) {
            checkAchieve(achieve);
        }
    }

    public static void cheakCollectionAchieve() {
        for (AchieveData.Achieve achieve : collectionAchieves) {
            checkAchieve(achieve);
        }
    }

    static boolean checkAchieve(AchieveData.Achieve achieve) {
        achieve.getDegreeDescp();
        if (!achieve.isComplete() || achieveComplete[achieve.getId()] != 0) {
            return false;
        }
        System.err.println("成就达成：" + achieve.getDescp());
        achieveComplete[achieve.getId()] = 1;
        completeNum++;
        cheakCollectionAchieve();
        record.writeRecord(1);
        addCompleteTip(achieve.getDescp(), true);
        return true;
    }

    static void addCompleteTip(String text, boolean achieve) {
        AchieveTip tip = new AchieveTip();
        tip.init(text, achieve);
        Rank.tip.add(tip);
    }

    public static int[] getAchieveNum(int index) {
        if (index == 0) {
            return getAchieveCompleteNum(rankAchieves);
        }
        if (index == 1) {
            return getAchieveCompleteNum(foodAchieves);
        }
        if (index == 2) {
            return getAchieveCompleteNum(battleAchieves);
        }
        if (index == 3) {
            return getAchieveCompleteNum(collectionAchieves);
        }
        return null;
    }

    public static int[] getAchieveCompleteNum(AchieveData.Achieve[] achieves) {
        int[] num = new int[2];
        for (int i = 0; i < achieves.length; i++) {
            if (achieves[i].isRecieve()) {
                num[0] = num[0] + 1;
            }
            if (achieves[i].isRecieve() || achieves[i].isTaken()) {
                num[1] = num[1] + 1;
            }
        }
        return num;
    }

    public int getFoodAchieveCompleteNum() {
        int num = 0;
        for (AchieveData.Achieve unfinished : foodAchieves) {
            if (!unfinished.unfinished()) {
                num++;
            }
        }
        return num;
    }

    public int getBattleAchieveCompleteNum() {
        int num = 0;
        for (AchieveData.Achieve unfinished : battleAchieves) {
            if (!unfinished.unfinished()) {
                num++;
            }
        }
        return num;
    }

    public int getCollectionAchieveCompleteNum() {
        int num = 0;
        for (AchieveData.Achieve unfinished : collectionAchieves) {
            if (!unfinished.unfinished()) {
                num++;
            }
        }
        return num;
    }

    public static void awardMoney() {
        rankMoney = 0;
        int[] money = {50, 100, 200, 40};
        String[] icon = {"tip003", "tip004", "tip005", "tip006", "tip007"};
        int id = Rank.rankId;
        if (firstThrough()) {
            System.out.println("星星过关奖励");
            if (Rank.rankUI.starNum == 1) {
                award(money[0], icon[0]);
            }
            if (Rank.rankUI.starNum == 2) {
                award(money[1], icon[0]);
            }
            if (Rank.rankUI.starNum == 3) {
                award(money[2], icon[0]);
            }
        } else {
            System.out.println("重复过关奖励");
            award(money[3], icon[4]);
        }
        if (Rank.isEasyMode()) {
            if (!fullBlood[id] && Rank.home.isFullBlood()) {
                System.out.println("首次血满过关奖励");
                award(money[0], icon[1]);
                fullBlood[id] = true;
            }
            if (!cleanDeck[id] && cleanDeck()) {
                System.out.println("首次障碍全部清除奖励");
                award(money[0], icon[2]);
                cleanDeck[id] = true;
            }
            if (!cleanFood[id] && cleanFood()) {
                System.out.println("首次食物全部拾取奖励");
                award(money[0], icon[3]);
                cleanFood[id] = true;
            }
        } else {
            if (!fullBlood_hard[id] && Rank.home.isFullBlood()) {
                System.out.println("首次血满过关奖励");
                award(money[0], icon[1]);
                fullBlood_hard[id] = true;
            }
            if (!cleanDeck_hard[id] && cleanDeck()) {
                System.out.println("首次障碍全部清除奖励");
                award(money[0], icon[2]);
                cleanDeck_hard[id] = true;
            }
            if (!cleanFood_hard[id] && cleanFood()) {
                System.out.println("首次食物全部拾取奖励");
                award(money[0], icon[3]);
                cleanFood_hard[id] = true;
            }
        }
        firstPlayFinish();
        someRankEnd(Rank.rankId);
        someRankEndThree(Rank.rankId);
        record.writeRecord(1);
    }

    static void award(int money, String icon) {
        rankMoney += money;
        AwardUI get = new AwardUI();
        get.init(money, icon, Rank.award.size());
        Rank.award.add(get);
    }

    public static void showAwardUI() {
        for (int i = 0; i < Rank.award.size(); i++) {
            Rank.award.get(i).canSee = true;
        }
    }

    public static boolean showAwardEnd() {
        for (int i = 0; i < Rank.award.size(); i++) {
            if (Rank.award.get(i).appear) {
                return false;
            }
        }
        return true;
    }

    public static void addFullBloodThrough() {
        if (Rank.home.isFullBlood()) {
            fullBloodThroughTime_everday++;
            cheakTargetComplete();
        }
    }

    public static void addSellTower() {
        sellTowerTime_everday++;
        cheakTargetComplete();
    }

    public static void addPickFood() {
        pickFoodTime_everday++;
        cheakTargetComplete();
    }

    public static void addEatAll() {
        if (cleanFood()) {
            eatAllThroughTime_everday++;
            cheakTargetComplete();
        }
    }

    public static void addUseSkill() {
        useSkillTime_everday++;
        cheakTargetComplete();
    }

    public static void resetTargetData() {
        fullBloodThroughTime_everday = 0;
        sellTowerTime_everday = 0;
        pickFoodTime_everday = 0;
        eatAllThroughTime_everday = 0;
        useSkillTime_everday = 0;
    }

    public static void cheakTargetComplete() {
        TargetData.Target[] allTargets = targets.getTargets();
        for (int i = 0; i < allTargets.length; i++) {
            allTargets[i].getDegreeDescp();
            if (allTargets[i].isComplete() && targetComplete[i] == 0) {
                targetComplete[i] = 1;
                record.writeRecord(1);
                if (showTaskTip(allTargets[i].getId())) {
                    addCompleteTip(allTargets[i].getDesp(), false);
                    System.err.println(allTargets[i].getDesp() + allTargets[i].getDegreeDescp() + "完成！");
                }
            }
        }
        if (checkAllTargetComplete() && takeTargetAward == 0) {
            takeTargetAward = 1;
        }
    }

    static boolean showTaskTip(int id) {
        for (int index : everyDayTargertIndex) {
            if (id == index) {
                return true;
            }
        }
        return false;
    }

    public static boolean canTakeTargetAward() {
        return takeTargetAward == 1;
    }

    public static boolean haveTakeTargetAward() {
        return takeTargetAward == 2;
    }

    public static boolean takeTargetAward(int diamond) {
        if (!canTakeTargetAward()) {
            return false;
        }
        takeTargetAward = 2;
        addDiamondNum(diamond);
        return true;
    }

    static boolean checkAllTargetComplete() {
        TargetData.Target[] allTargets = targets.getTargets();
        for (int index : everyDayTargertIndex) {
            if (allTargets[index].unfinish()) {
                return false;
            }
        }
        return true;
    }

    public static void getEveryDayTarget() {
        Calendar c = Calendar.getInstance();
        if (!isSameDay()) {
            int i = 0;
            while (i < 3) {
                int index = Tools.nextInt(targetComplete.length);
                if (!targets.getTargets()[index].isSelect()) {
                    everyDayTargertIndex[i] = index;
                    targets.getTargets()[index].setSelect(true);
                    i++;
                    System.out.println("每日任务的索引：" + index);
                }
            }
            lastYear = c.get(1);
            lastMonth = c.get(2) + 1;
            lastDay = c.get(5);
            for (int j = 0; j < targetComplete.length; j++) {
                targetComplete[j] = 0;
            }
            resetTargetData();
            takeTargetAward = 0;
            record.writeRecord(1);
            return;
        }
        System.out.println("与上次登陆时同一天");
    }

    private static int checkDay(Calendar c, int year, int month, int day) {
        int tempMonth;
        int curYear = c.get(1);
        int curMonth = c.get(2) + 1;
        int curDay = c.get(5);
        if (curYear - year >= 2 || (tempMonth = (((curYear - year) * 12) + curMonth) - month) >= 2) {
            return -1;
        }
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return ((tempMonth * 31) + curDay) - day;
        }
        if (month == 4 || month == 6 || month == 8 || month == 9 || month == 11) {
            return ((tempMonth * 30) + curDay) - day;
        }
        return ((tempMonth * 28) + curDay) - day;
    }

    public static void initRankData(int index) {
        Mydata.loadLevelData(index);
        String mapName = GameConstant.LEVEL + index;
        LevelData data = Mydata.levelData.get(mapName);
        int num = Mydata.levelData.get(mapName).towers.size();
        int num2 = Mydata.levelData.get(mapName).hideTowers.size();
        towerNames = new String[(num + num2)];
        rankTowerIcons = new String[num];
        hideTowerIcons = new String[num2];
        for (int i = 0; i < num; i++) {
            String towerName = Mydata.levelData.get(mapName).towers.get(i);
            rankTowerIcons[i] = Mydata.towerData.get(towerName).getTubiaoName();
            towerNames[i] = towerName;
        }
        for (int i2 = 0; i2 < num2; i2++) {
            String towerName2 = Mydata.levelData.get(mapName).hideTowers.get(i2);
            hideTowerIcons[i2] = Mydata.towerData.get(towerName2).getTubiaoName();
            towerNames[i2 + num] = towerName2;
        }
        if (getMode() == 0) {
            time3 = data.starTime[0][1];
            time2 = data.starTime[0][0];
        } else {
            time3 = data.starTime[1][1];
            time2 = data.starTime[1][0];
        }
        if (data.getType() == 1) {
            questIntro = "保护地基，阻止千帆的行动";
        } else {
            questIntro = "保护基地的同时，击败千帆";
        }
        historyStar = rankStar_easy[index - 1][0];
        historyMinute = rankStar_easy[index - 1][1];
        historySecond = rankStar_easy[index - 1][2];
        historyPercent = rankStar_easy[index - 1][3];
    }

    public static boolean firstThrough() {
        if (Rank.isEasyMode()) {
            if (rankStar_easy[Rank.rankId][0] == 0) {
                return true;
            }
            return false;
        } else if (rankStar_hard[Rank.rankId][0] != 0) {
            return false;
        } else {
            return true;
        }
    }

    public static int[] getHistoryMaxScore() {
        if (Rank.isEasyMode()) {
            if (firstThrough()) {
                return null;
            }
            return new int[]{rankStar_easy[Rank.rankId][1], rankStar_easy[Rank.rankId][2]};
        } else if (firstThrough()) {
            return null;
        } else {
            return new int[]{rankStar_hard[Rank.rankId][1], rankStar_hard[Rank.rankId][2]};
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0029 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isNewRecord(int r7, int r8) {
        /*
            r6 = 2
            r2 = 1
            boolean r3 = com.sg.td.Rank.isEasyMode()
            if (r3 == 0) goto L_0x002b
            boolean r3 = firstThrough()
            if (r3 == 0) goto L_0x000f
        L_0x000e:
            return r2
        L_0x000f:
            int[][] r3 = com.sg.td.RankData.rankStar_easy
            int r4 = com.sg.td.Rank.rankId
            r3 = r3[r4]
            r3 = r3[r2]
            int r3 = r3 * 60
            int[][] r4 = com.sg.td.RankData.rankStar_easy
            int r5 = com.sg.td.Rank.rankId
            r4 = r4[r5]
            r4 = r4[r6]
            int r0 = r3 + r4
            int r3 = r7 * 60
            int r1 = r3 + r8
            if (r1 < r0) goto L_0x000e
        L_0x0029:
            r2 = 0
            goto L_0x000e
        L_0x002b:
            boolean r3 = com.sg.td.Rank.isHardMode()
            if (r3 == 0) goto L_0x0029
            boolean r3 = firstThrough()
            if (r3 != 0) goto L_0x000e
            int[][] r3 = com.sg.td.RankData.rankStar_hard
            int r4 = com.sg.td.Rank.rankId
            r3 = r3[r4]
            r3 = r3[r2]
            int r3 = r3 * 60
            int[][] r4 = com.sg.td.RankData.rankStar_hard
            int r5 = com.sg.td.Rank.rankId
            r4 = r4[r5]
            r4 = r4[r6]
            int r0 = r3 + r4
            int r3 = r7 * 60
            int r1 = r3 + r8
            if (r1 >= r0) goto L_0x0029
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sg.td.RankData.isNewRecord(int, int):boolean");
    }

    public static int getPercent(int min, int sec) {
        int starTime2 = Rank.rankUI.starTime2;
        int useTime = (min * 60) + sec;
        if (((double) useTime) < ((double) starTime2) * 0.8d) {
            return 99;
        }
        if (((double) useTime) < ((double) starTime2) * 1.1d) {
            return 85;
        }
        if (((double) useTime) < ((double) starTime2) * 1.3d) {
            return 72;
        }
        return 63;
    }

    public static boolean bearOpen(int bearId) {
        if (roleLevel[bearId] != 0) {
            return false;
        }
        roleLevel[bearId] = 1;
        record.writeRecord(1);
        cheakCollectionAchieve();
        return true;
    }

    public static boolean bearUpdate(int bearId) {
        if (roleLevel[bearId] >= 10) {
            return false;
        }
        int[] iArr = roleLevel;
        iArr[bearId] = iArr[bearId] + 1;
        record.writeRecord(1);
        cheakCollectionAchieve();
        return true;
    }

    public static boolean oneKeyFullLevel(int bearId) {
        if (roleLevel[bearId] >= 10) {
            return false;
        }
        roleLevel[bearId] = 10;
        record.writeRecord(1);
        cheakCollectionAchieve();
        return true;
    }

    public static boolean roleFullLevel(int bearId) {
        return roleLevel[bearId] == 10;
    }

    public static boolean isRoleOpen(int bearId) {
        return roleLevel[bearId] > 0;
    }

    public static int getRoleUpdateTimes(int bearId) {
        if (roleFullLevel(bearId) || !isRoleOpen(bearId)) {
            return 0;
        }
        return 10 - roleLevel[bearId];
    }

    public static void unClockTower(int index) {
        if (index < towerClock.length && !towerClock[index]) {
            towerClock[index] = true;
            towerLevel[index] = ((Rank.rank - 1) / 3) + 1;
            record.writeRecord(1);
        }
    }

    public static boolean canStrength(int index) {
        if (towerLevel[index] < getRecommendLevel()) {
            return true;
        }
        return false;
    }

    public static int getNextRank(int index) {
        int nowRankIndex = getRankOpenIndex(0);
        if (towerLevel[index] >= getRecommendLevel()) {
            return (towerLevel[index] * 3) - nowRankIndex;
        }
        return (((nowRankIndex / 3) * 3) + 3) - nowRankIndex;
    }

    public static int getRankOpenIndex() {
        if (getMode() == 0) {
            for (int i = 0; i < rankStar_easy.length; i++) {
                if (rankStar_easy[i][0] == 0) {
                    return i;
                }
            }
        } else if (getMode() == 1) {
            for (int i2 = 0; i2 < rankStar_hard.length; i2++) {
                if (rankStar_hard[i2][0] == 0) {
                    return i2;
                }
            }
        }
        return 54;
    }

    public static int getRankOpenIndex(int mode2) {
        if (mode2 == 0) {
            for (int i = 0; i < rankStar_easy.length; i++) {
                if (rankStar_easy[i][0] == 0) {
                    return i;
                }
            }
        } else if (mode2 == 1) {
            for (int i2 = 0; i2 < rankStar_hard.length; i2++) {
                if (rankStar_hard[i2][0] == 0) {
                    return i2;
                }
            }
        }
        return 54;
    }

    public static void updateTower(int index) {
        int[] iArr = towerLevel;
        iArr[index] = iArr[index] + 1;
        record.writeRecord(1);
    }

    public static boolean updateTowerFull(int index) {
        if (towerLevel[index] >= 18) {
            return true;
        }
        return false;
    }

    public static boolean isTowerClock(int index) {
        return towerLevel[index] == 0;
    }

    public static boolean towerCanStrength(String towerName) {
        if (towerName.equals("Random")) {
            return randomCanStrength();
        }
        if (!LoadStrength.strengthData.get(towerName).notLock()) {
            return false;
        }
        return LoadStrength.strengthData.get(towerName).isStreng();
    }

    static boolean randomCanStrength() {
        LevelData data = Mydata.levelData.get(GameConstant.LEVEL + Rank.rank);
        for (int i = 0; i < data.randomTowers.size(); i++) {
            if (towerCanStrength(data.randomTowers.get(i))) {
                return true;
            }
        }
        return false;
    }

    public static ArrayList<Strength> getNeedStrengthTowers() {
        ArrayList<Strength> strengths = new ArrayList<>();
        for (int i = 0; i < towerNames.length; i++) {
            if (towerNames[i].equals("Random")) {
                LevelData data = Mydata.levelData.get(GameConstant.LEVEL + Rank.rank);
                for (int j = 0; j < data.randomTowers.size(); j++) {
                    if (towerCanStrength(data.randomTowers.get(j))) {
                        strengths.add(LoadStrength.strengthData.get(data.randomTowers.get(j)));
                    }
                }
            } else if (towerCanStrength(towerNames[i])) {
                strengths.add(LoadStrength.strengthData.get(towerNames[i]));
            }
        }
        return strengths;
    }

    public static int getRecommendLevel() {
        return (getRankOpenIndex(0) / 3) + 1;
    }

    public static boolean isSameDay() {
        if (checkDay(Calendar.getInstance(), lastYear, lastMonth, lastDay) == 0) {
            return true;
        }
        return false;
    }

    public static int getGetIndex() {
        return eveyDayGetIndex;
    }

    public static int getTodayGetIndex() {
        return todayGetIndex;
    }

    public static void eveyDayGet() {
        todayGetIndex = eveyDayGetIndex;
        eveyDayGetIndex++;
        if (eveyDayGetIndex >= 7) {
            eveyDayGetIndex = 0;
        }
        record.writeRecord(1);
    }

    public static void updateJiDi() {
        if (jidiLevel < 11) {
            jidiLevel++;
            record.writeRecord(1);
            Rank.resetHomeHp();
            cheakCollectionAchieve();
        }
    }

    public static boolean rankOpen(int rank) {
        if (getMode() == 0) {
            if (rankStar_easy[rank - 1][0] > 0) {
                return true;
            }
            return false;
        } else if (rankStar_hard[rank - 1][0] <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public static int getRankStar(int rank) {
        if (getMode() == 0) {
            return rankStar_easy[rank - 1][0];
        }
        return rankStar_hard[rank - 1][0];
    }

    public static boolean cleanDeck(int rank) {
        if (getMode() == 0) {
            return cleanDeck[rank - 1];
        }
        return cleanDeck_hard[rank - 1];
    }

    public static boolean cleanFood(int rank) {
        if (getMode() == 0) {
            return cleanFood[rank - 1];
        }
        return cleanFood_hard[rank - 1];
    }

    public static boolean fullBloodThrough(int rank) {
        if (getMode() == 0) {
            return fullBlood[rank - 1];
        }
        return fullBlood_hard[rank - 1];
    }

    public static int getRoleRankIndex() {
        if (getMode() == 0) {
            if (isThroughGame(0)) {
                return 54;
            }
            return roleRankIndex;
        } else if (!isThroughGame(1)) {
            return roleRankIndex_hard;
        } else {
            return 54;
        }
    }

    public static int getCursorRankIndex() {
        return getRankOpenIndex() + 1;
    }

    public static boolean drawLock(int rankNumber) {
        if (getMode() != 1 && !isThroughGame(0) && rankNumber % 6 == 1 && rankNumber != 1) {
            return isUnlockRank(rankNumber);
        }
        return false;
    }

    public static boolean isUnlockRank(int rankNumber) {
        if ((getCursorRankIndex() != rankNumber || rankNumber != roleRankIndex) && getCursorRankIndex() <= rankNumber) {
            return true;
        }
        return false;
    }

    public static boolean reachOpenStar(int rankNumber) {
        return getStarNum() >= getUnlockStarNum(rankNumber);
    }

    public static boolean nextRankOpen(int rankNumber) {
        if (getRankStar(rankNumber - 1) <= 0) {
            return false;
        }
        return true;
    }

    public static int getUnlockStarNum(int rankNumber) {
        return ((rankNumber - 1) / 6) * 15;
    }

    public static int getNeedStarNum(int rankNumber) {
        return getUnlockStarNum(rankNumber) - getStarNum();
    }

    public static void unlockRank() {
        if (getMode() == 0) {
            roleRankIndex = getCursorRankIndex();
        } else {
            roleRankIndex_hard = getCursorRankIndex();
        }
        record.writeRecord(1);
    }

    public static void setRoleRankIndex() {
        int rankIndex;
        if (getRankOpenIndex() % 6 != 0 || !Rank.isEasyMode()) {
            rankIndex = getCursorRankIndex();
        } else {
            rankIndex = getCursorRankIndex() - 1;
        }
        if (Rank.isEasyMode() && roleRankIndex < rankIndex) {
            roleRankIndex = rankIndex;
        }
        if (Rank.isHardMode() && roleRankIndex_hard < rankIndex) {
            roleRankIndex_hard = rankIndex;
        }
    }

    public static boolean isThroughGame(int mode2) {
        if (mode2 == 0) {
            for (int[] iArr : rankStar_easy) {
                if (iArr[0] == 0) {
                    return false;
                }
            }
            return true;
        } else if (mode2 != 1) {
            return false;
        } else {
            for (int[] iArr2 : rankStar_hard) {
                if (iArr2[0] == 0) {
                    return false;
                }
            }
            return true;
        }
    }

    public static boolean drawRankInfo(int rankNumber) {
        int rankId = rankNumber - 1;
        if (mode == 0) {
            if (rankStar_easy[rankId][0] != 0) {
                return true;
            }
            return false;
        } else if (rankStar_hard[rankId][0] == 0) {
            return false;
        } else {
            return true;
        }
    }

    public static void initProp(int rankNum) {
        if (lastRankNum != rankNum) {
            for (int i = 0; i < props_use.length; i++) {
                props_use[i] = false;
            }
        }
        lastRankNum = rankNum;
    }

    public static int getPropsNum(int index) {
        return props_num[index];
    }

    public static boolean buyProp(int index, int money) {
        if (cakeNum < money) {
            return false;
        }
        cakeNum -= money;
        int[] iArr = props_num;
        iArr[index] = iArr[index] + 1;
        addPropNum_all(1);
        record.writeRecord(1);
        return true;
    }

    public static void selectUseProp(int index) {
        props_use[index] = !props_use[index];
    }

    public static boolean isSelectUseProp(int index) {
        return props_use[index];
    }

    public static void consumeProp() {
        for (int i = 0; i < props_use.length; i++) {
            if (props_use[i]) {
                int[] iArr = props_num;
                iArr[i] = iArr[i] - 1;
                props_use[i] = false;
            }
        }
        record.writeRecord(1);
    }

    public static int getUsePropNum() {
        int num = 0;
        for (boolean z : props_use) {
            if (z) {
                num++;
            }
        }
        return num;
    }

    public static boolean useWatch() {
        return props_use[0];
    }

    public static boolean useGLasses() {
        return props_use[1];
    }

    public static boolean useBlank() {
        return props_use[2];
    }

    public static boolean useShose() {
        return props_use[3];
    }

    public static int getOnlineTime() {
        onlineTime = lastOnlineTime + OnlineTime.getOnLineMinus();
        return onlineTime;
    }

    public static int getWaitTime() {
        if (onlineTime < receiveTime[0]) {
            return receiveTime[0] - onlineTime;
        }
        if (onlineTime < receiveTime[1]) {
            return receiveTime[1] - onlineTime;
        }
        if (onlineTime < receiveTime[2]) {
            return receiveTime[2] - onlineTime;
        }
        if (onlineTime < receiveTime[3]) {
            return receiveTime[3] - onlineTime;
        }
        return 0;
    }

    public static boolean showOnlineTip() {
        for (int i : receiveAward) {
            if (i == 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean canReceive(int index) {
        return receiveAward[index] == 1;
    }

    public static void receiveAward(int index) {
        receiveAward[index] = 2;
        record.writeRecord(1);
    }

    public static boolean haveTaken(int index) {
        return receiveAward[index] == 2;
    }

    public static int getClipTimeLength() {
        if (getOnlineTime() == 0) {
            return 0;
        }
        for (int i = 0; i < receiveTime.length; i++) {
            if (getOnlineTime() == receiveTime[i]) {
                return clipTimeLength[i];
            }
        }
        if (getOnlineTime() > receiveTime[4]) {
            return clipTimeLength[4];
        }
        if (getOnlineTime() > receiveTime[3]) {
            return clipTimeLength[3] + (((clipTimeLength[4] - clipTimeLength[3]) * (getOnlineTime() - receiveTime[3])) / (receiveTime[4] - receiveTime[3]));
        }
        if (getOnlineTime() > receiveTime[2]) {
            return clipTimeLength[2] + (((clipTimeLength[3] - clipTimeLength[2]) * (getOnlineTime() - receiveTime[2])) / (receiveTime[3] - receiveTime[2]));
        }
        if (getOnlineTime() > receiveTime[1]) {
            return clipTimeLength[1] + (((clipTimeLength[2] - clipTimeLength[1]) * (getOnlineTime() - receiveTime[1])) / (receiveTime[2] - receiveTime[1]));
        }
        if (getOnlineTime() <= receiveTime[0]) {
            return clipTimeLength[0] / (receiveTime[0] - getOnlineTime());
        }
        return (((clipTimeLength[1] - clipTimeLength[0]) * (getOnlineTime() - receiveTime[0])) / (receiveTime[1] - receiveTime[0])) + clipTimeLength[0];
    }

    public static void runOnline() {
        for (int i = 0; i < receiveAward.length; i++) {
            if (!haveTaken(i) && getOnlineTime() >= receiveTime[i] && receiveAward[i] != 1) {
                receiveAward[i] = 1;
                record.writeRecord(1);
            }
        }
    }

    public static void resetOnlineTime() {
        int i = 0;
        while (i < receiveAward.length) {
            if (receiveAward[i] != 1) {
                i++;
            } else {
                return;
            }
        }
        int i2 = 0;
        while (i2 < receiveAward.length) {
            if (receiveAward[i2] != 0) {
                i2++;
            } else {
                return;
            }
        }
        if (!isSameDay()) {
            lastOnlineTime = 0;
            for (int i3 = 0; i3 < receiveAward.length; i3++) {
                receiveAward[i3] = 0;
            }
            record.writeRecord(1);
            System.out.println("重置在线领取");
        }
    }

    public static void quiteSave() {
        lastOnlineTime = getOnlineTime();
        OnlineTime.initOnlineTime();
        if (record != null) {
            record.writeRecord(1);
        }
    }

    public static void resetLuckDrawTimes() {
        if (!isSameDay()) {
            luckDrawTimes = 0;
            record.writeRecord(1);
        }
    }

    public static boolean isLuckFree() {
        return luckDrawTimes == 0;
    }

    public static void addLuckDrawTimes() {
        luckDrawTimes++;
        record.writeRecord(1);
    }

    public static int getRoleSpineId(int roleIndex) {
        switch (roleIndex) {
            case 0:
                return 21;
            case 1:
                return 20;
            case 2:
                return 22;
            case 3:
                return 19;
            default:
                return 0;
        }
    }

    public static int getFullBloodThroughs() {
        int num = 0;
        for (boolean z : fullBlood) {
            if (z) {
                num++;
            }
        }
        return num;
    }

    public static int getCleanDeckThroughs() {
        int num = 0;
        for (boolean z : cleanDeck) {
            if (z) {
                num++;
            }
        }
        return num;
    }

    public static int getcleanFoodThroughs() {
        int num = 0;
        for (boolean z : cleanFood) {
            if (z) {
                num++;
            }
        }
        return num;
    }

    public static int getRoleNum() {
        int num = 0;
        for (int i : roleLevel) {
            if (i >= 1) {
                num++;
            }
        }
        return num;
    }

    public static void resetRankData() {
        record.resetRankData();
    }

    public static void setLastSelectRoleIndex(int index) {
        lastSelectRoleIndex = index;
    }

    public static boolean intoRoleUpTeach() {
        return getRankOpenIndex() == 3 && roleLevel[0] == 1;
    }

    public static boolean isTeachRoleUpEnd() {
        if (Teach.hasTeach[15] || getRankOpenIndex() > 3 || roleLevel[0] > 1 || isThroughGame(0)) {
            return true;
        }
        return false;
    }

    public static boolean isRankReturnMain() {
        return rankReturnMain;
    }

    public static void setRankReturnMain(boolean rankReturn) {
        rankReturnMain = rankReturn;
    }

    public static int getReturnRoleIndex() {
        setRankReturnMain(false);
        if (intoRoleUpTeach()) {
            return 0;
        }
        if (!isTeachRoleUpEnd()) {
            return lastSelectRoleIndex;
        }
        if (roleLevel[3] != 0) {
            return lastSelectRoleIndex;
        }
        if (MySwitch.isCaseA == 0) {
            return lastSelectRoleIndex;
        }
        return 3;
    }

    public static void addSignTeach() {
        teach.initTeach(23);
    }

    public static boolean isFirstPlay() {
        return firstPaly;
    }

    public static void resetTeachData() {
        if (isFirstPlay()) {
            teach.resetTeachOne();
            System.out.println("重置教学数据....");
        }
    }

    public static void firstPlayFinish() {
        if (isFirstPlay()) {
            firstPaly = false;
        }
    }

    public static boolean openHardMode() {
        if (!isThroughGame(0) && getRankOpenIndex(0) < 20) {
            return false;
        }
        return true;
    }

    public static void setMode(byte m) {
        mode = m;
    }

    public static int getMode() {
        return mode;
    }

    public static void giveMoney() {
        if (!giveMoney) {
            addCakeNum_free(2000);
            giveMoney = true;
        }
    }

    public static boolean rankDrop(int rankNum) {
        return rankDrop[rankNum - 1];
    }

    public static void pickDrop(int rankNum) {
        if (!rankDrop[rankNum - 1]) {
            rankDrop[rankNum - 1] = true;
            record.writeRecord(1);
        }
    }

    public static int getSelectRoleIndex() {
        return selectRoleIndex;
    }

    public static void someRankEnd(int rankId) {
        if (rankId == 1) {
            rankTowOver = true;
        } else {
            rankTowOver = false;
        }
    }

    public static void someRankEndThree(int rankId) {
        if (rankId == 2) {
            rankThreeOver = true;
        } else {
            rankThreeOver = false;
        }
    }

    public static boolean rankTowOver() {
        if (!rankTowOver) {
            return false;
        }
        rankTowOver = false;
        return true;
    }

    public static boolean rankThreeOver() {
        if (!rankThreeOver) {
            return false;
        }
        rankThreeOver = false;
        return true;
    }

    public static void saveRecord() {
        record.writeRecord(1);
    }

    public static boolean showTowerStrengButton() {
        return Teach.hasTeach[17];
    }

    public static boolean showTaskTip() {
        if (canTakeTargetAward()) {
            return true;
        }
        TargetData.Target[] allTargets = targets.getTargets();
        for (int index : everyDayTargertIndex) {
            if (allTargets[index].isReceive()) {
                return true;
            }
        }
        return false;
    }

    public static boolean showAchieveTip() {
        for (int i : achieveComplete) {
            if (i == 1) {
                return true;
            }
        }
        return false;
    }
}
