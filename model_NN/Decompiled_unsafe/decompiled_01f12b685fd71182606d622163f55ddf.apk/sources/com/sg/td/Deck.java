package com.sg.td;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Blood;
import com.sg.td.actor.Bomb;
import com.sg.td.actor.DropFood;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Money;
import com.sg.td.actor.SubTime;
import com.sg.td.data.BuildingData;
import com.sg.td.data.Mydata;
import com.sg.util.GameStage;
import java.util.ArrayList;
import java.util.Map;

public class Deck implements GameConstant {
    int blockIndex = -1;
    Blood blood;
    boolean bomb;
    boolean bubble;
    boolean cage;
    int curStatus;
    BuildingData data;
    boolean deck;
    int dropIndex;
    ArrayList<String> dropList;
    String dropString;
    Map<String, String> dropTower;
    boolean flip;
    boolean food;
    public int h;
    public int hitX;
    public int hitY;
    public int hp;
    public int hpBarY;
    int hpMax;
    boolean ice;
    Effect iceEffect;
    ActorImage icon;
    public int imageH;
    public int imageW;
    int index;
    ActorImage jiazi1;
    ActorImage jiazi2;
    int level;
    int modeOffsetY;
    int money;
    int offRange;
    ActorImage paopao;
    boolean random;
    float scale = 1.0f;
    int showHpTime;
    String type;
    public int w;
    public int x;
    public int y;

    public void init(int x2, int y2, String type2, int index2, int blockIndex2, boolean flip2, Map<String, String> dropTower2, String dropString2) {
        this.x = x2;
        this.y = y2;
        this.type = type2;
        this.index = index2;
        this.blockIndex = blockIndex2;
        this.flip = flip2;
        this.dropTower = dropTower2;
        this.dropString = dropString2;
        this.curStatus = 6;
        this.data = Mydata.deckData.get(type2);
        initBuildingData();
        initBlood();
        initDrops();
        calcDropTower();
    }

    /* access modifiers changed from: package-private */
    public void initBuildingData() {
        this.imageW = this.data.getImageWidth();
        this.imageH = this.data.getImageHeight();
        this.w = this.data.getCollideWidth();
        this.h = this.data.getCollideHeight();
        this.modeOffsetY = this.data.getModeOffsetY();
        this.hpBarY = this.data.getHpBarY();
        this.money = this.data.getMoney();
        this.blood = new Blood();
        this.scale = this.data.getScale();
        if (this.scale == Animation.CurveTimeline.LINEAR) {
            this.scale = 1.0f;
        }
        this.hitX = this.x + (this.w / 2);
        this.hitY = (this.y + (this.h / 2)) - this.modeOffsetY;
        this.offRange = this.imageW / 2;
        initAttribute();
    }

    /* access modifiers changed from: package-private */
    public void initBlood() {
        int baseHp = this.data.getHp();
        int i = (int) (Rank.isEasyMode() ? (float) baseHp : ((float) baseHp) + (((float) baseHp) * Rank.addHp));
        this.hpMax = i;
        this.hp = i;
    }

    public void setDropIndex(int dIndex) {
        this.dropIndex = dIndex;
    }

    /* access modifiers changed from: package-private */
    public void initAttribute() {
        if (this.type.indexOf("Meat") != -1 || this.type.indexOf("Orange") != -1 || this.type.indexOf("Pear") != -1 || this.type.indexOf("IceCream") != -1) {
            this.food = true;
            RankData.addFoodNum_rank(1);
        } else if (this.type.indexOf("Random") != -1) {
            this.random = true;
            RankData.addFoodNum_rank(1);
        } else if (this.type.indexOf("Bomb") != -1) {
            this.bomb = true;
        }
        if (!(this.type.indexOf("Low") == -1 && this.type.indexOf("High") == -1 && this.type.indexOf("Money") == -1 && this.type.indexOf("Cage") == -1)) {
            this.deck = true;
        }
        if (this.type.indexOf("Net") != -1) {
            this.ice = true;
        }
        if (this.type.indexOf("Bubble") != -1) {
            this.bubble = true;
        }
        if (this.type.indexOf("Cage") != -1) {
            this.cage = true;
        }
        if (this.food || this.deck) {
            this.deck = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void initDrops() {
        if (this.dropString != null) {
            this.dropList = new ArrayList<>();
            String[] s = this.dropString.split("#");
            int i = 0;
            if (this.bomb) {
                this.level = Integer.parseInt(s[0]);
                i = 1;
            }
            while (i < s.length) {
                this.dropList.add(s[i]);
                i++;
            }
        }
        if (this.bomb && this.dropTower.get("Tower") != null) {
            this.level = Integer.parseInt(this.dropTower.get("Tower").split(",")[1]);
        }
        if (!this.bomb && this.dropList != null && this.dropList.get(0).indexOf("Money") == -1) {
            RankData.addFoodNum_rank(this.dropList.size());
        }
        if (this.dropList != null) {
            RankData.addDeckNum_rank(this.dropList.size());
        }
    }

    /* access modifiers changed from: package-private */
    public void calcDropTower() {
        String[] name = {"Tower", "Tower1", "Tower2", "Tower3", "Tower4"};
        for (String dropName : name) {
            String dropName2 = getDropName(dropName);
            if (!(dropName2 == null || Mydata.deckData.get(dropName2) == null)) {
                RankData.addFoodNum_rank(1);
                RankData.addDeckNum_rank(1);
            }
        }
    }

    public void addDeck() {
        int x2 = this.hitX;
        int y2 = this.hitY;
        if (this.ice) {
            this.iceEffect = new Effect();
            this.iceEffect.addEffect(0, x2, y2, 2);
        }
        if (this.dropIndex != 0) {
            if (this.type.indexOf("_22") != -1) {
                this.jiazi1 = new ActorImage(58, x2, y2 + 28, 1, 1);
                this.jiazi1.setOrigin(this.jiazi1.getWidth() / 2.0f, Animation.CurveTimeline.LINEAR);
                this.jiazi1.setScale(1.2f);
            } else {
                this.jiazi1 = new ActorImage(58, x2, y2 + 8, 1, 1);
            }
        }
        this.icon = new ActorImage(this.data.getIcon(), x2, y2, 1, 1);
        if (this.food || this.random) {
            this.paopao = new ActorImage((int) PAK_ASSETS.IMG_PARTICLE_TIME, x2, y2, 1, 1);
        }
        if (this.flip) {
            this.icon.setFlip(true, false);
        }
        if (this.icon != null) {
            this.icon.setScale(this.scale);
        }
        if (this.paopao != null) {
            this.paopao.setScale(this.scale);
        }
        if (this.dropIndex != 0) {
            if (this.icon != null) {
                addUpAction(this.icon);
            }
            if (this.paopao != null) {
                addUpAction(this.paopao);
            }
            if (this.type.indexOf("_22") != -1) {
                this.jiazi2 = new ActorImage(59, x2, y2 + 8 + 45, 1, 1);
                this.jiazi2.setOrigin(this.jiazi2.getWidth() / 2.0f, Animation.CurveTimeline.LINEAR);
                this.jiazi2.setScale(1.2f);
                return;
            }
            this.jiazi2 = new ActorImage(59, x2, y2 + 8 + 25, 1, 1);
        }
    }

    /* access modifiers changed from: package-private */
    public void run(float delta) {
        addHpLine();
    }

    public void hurt(int vaule, String eftType, int hitSound) {
        this.hp -= vaule;
        this.showHpTime = PAK_ASSETS.IMG_SHUOMINGZI03;
        if (this.hp <= 0) {
            if (this.curStatus != 1) {
                setDead();
            }
            buildCreate();
            Rank.setGride();
        }
        if (this.deck) {
            addAttackAction();
        }
        if (eftType != null && !eftType.equals("null")) {
            new Effect().addEffect(eftType, this.hitX, this.hitY, 3);
        }
        Sound.playSound(hitSound);
    }

    /* access modifiers changed from: package-private */
    public void setDead() {
        this.hp = 0;
        this.curStatus = 1;
        clearSelected();
        this.blood.free();
        if (this.bubble) {
            Sound.playSound(57);
        }
        if (this.food) {
            dropFood(this.type);
        }
        if (this.bomb) {
            dropBomb();
        }
        if (this.random) {
            dropRandom();
        }
        if (this.ice) {
            this.iceEffect.remove();
        }
        if (this.cage) {
            Rank.hideTower.add(getDropName("Tower"));
        }
        if (this.blockIndex != -1) {
            Rank.tower.get(this.blockIndex).relieveBlock();
            Sound.playSound(15);
        }
        new Money().init(this.x + (this.w / 2), this.y, this.money, false);
        dropTower();
        dropDeck();
        removeStage();
        new Effect().addEffect(54, this.hitX, this.hitY, 3);
        if (!this.ice) {
            RankData.addDeckNum();
        }
    }

    /* access modifiers changed from: package-private */
    public void removeStage() {
        if (this.icon != null) {
            GameStage.removeActor(this.icon);
        }
        if (this.paopao != null) {
            GameStage.removeActor(this.paopao);
        }
        if (this.jiazi1 != null) {
            GameStage.removeActor(this.jiazi1);
        }
        if (this.jiazi2 != null) {
            GameStage.removeActor(this.jiazi2);
        }
        if (this.ice) {
            this.iceEffect.remove();
        }
    }

    /* access modifiers changed from: package-private */
    public void buildCreate() {
        if (this.dropList != null && this.dropIndex <= this.dropList.size() - 1) {
            String name = this.dropList.get(this.dropIndex);
            if (this.bomb) {
                this.type = name.split(",")[0];
                this.level = Integer.parseInt(name.split(",")[1]);
            } else {
                this.type = name;
            }
            this.curStatus = 6;
            this.data = Mydata.deckData.get(this.type);
            initBuildingData();
            initBlood();
            this.dropIndex++;
            addDeck();
            clearSelected();
        }
    }

    /* access modifiers changed from: package-private */
    public void dropTower() {
        if (this.dropTower != null) {
            int ox = this.x;
            int oy = this.y;
            dropSubTime();
            drop_tower(getDropName("Tower"), Map.getScreenX((this.w / 4) + ox), Map.getScreenY(((this.h * 3) / 4) + oy));
            drop_tower(getDropName("Tower1"), Map.getScreenX((this.w / 4) + ox), Map.getScreenY(((this.h * 3) / 4) + oy));
            drop_tower(getDropName("Tower2"), Map.getScreenX(((this.w * 3) / 4) + ox), Map.getScreenY(((this.h * 3) / 4) + oy));
            drop_tower(getDropName("Tower3"), Map.getScreenX((this.w / 4) + ox), Map.getScreenY((this.h / 4) + oy));
            drop_tower(getDropName("Tower4"), Map.getScreenX(((this.w * 3) / 4) + ox), Map.getScreenY((this.h / 4) + oy));
        }
    }

    /* access modifiers changed from: package-private */
    public void dropDeck() {
        if (this.dropTower != null) {
            drop_deck(getDropName("Tower"), this.x, this.y);
            drop_deck(getDropName("Tower1"), this.x, this.y + (this.h / 2));
            drop_deck(getDropName("Tower2"), this.x + (this.w / 2), this.y + (this.h / 2));
            drop_deck(getDropName("Tower3"), this.x, this.y);
            drop_deck(getDropName("Tower4"), this.x + (this.w / 2), this.y);
        }
    }

    /* access modifiers changed from: package-private */
    public String getRandomFood() {
        String[] tString = "Orange#IceCream#Meat".split("#");
        return tString[Tools.nextInt(tString.length)];
    }

    /* access modifiers changed from: package-private */
    public String getDropName(String key) {
        if (!(this.dropTower == null || this.dropTower.get(key) == null)) {
            String[] tString = this.dropTower.get(key).split("#");
            int random2 = Tools.nextInt(tString.length);
            for (int i = 0; i < tString.length; i++) {
                if (random2 == i) {
                    String[] ss = tString[i].split(",");
                    return new String[]{ss[0], ss[1], ss[2]}[0];
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void drop_tower(String dropName, int dx, int dy) {
        if (dropName != null && Mydata.towerData.get(dropName) != null) {
            Rank.building.buildTower(dx, dy, dropName, false);
        }
    }

    /* access modifiers changed from: package-private */
    public void drop_deck(String dropName, int dx, int dy) {
        if (dropName != null && Mydata.deckData.get(dropName) != null) {
            if (dropName.endsWith("_22")) {
                Rank.building.addDeck(this.x, this.y, dropName, -1, false, null, null);
            } else {
                Rank.building.addDeck(dx, dy, dropName, -1, false, null, null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dropSubTime() {
        String dropString2 = this.dropTower.get("Tower");
        if (dropString2 != null) {
            String[] tString = dropString2.split(",");
            if (tString[0].equals("SubTime")) {
                new SubTime(this.hitX, this.hitY, Integer.parseInt(tString[1]));
            } else if (tString[0].equals("FruitBomb")) {
                this.level = Integer.parseInt(tString[1]);
                dropBomb();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dropRandom() {
        String sub = this.type.substring(this.type.length() - 3, this.type.length());
        String dropName = null;
        if ("_11".endsWith(sub)) {
            dropName = getRandomFood() + sub;
        } else if ("_22".endsWith(sub)) {
            dropName = getRandomFood() + sub;
        }
        if (dropName != null && Mydata.deckData.get(dropName) != null) {
            dropFood(dropName);
        }
    }

    /* access modifiers changed from: package-private */
    public void dropFood(String dropName) {
        new DropFood(Mydata.deckData.get(dropName).getIcon(), this.x + (this.w / 2), this.y + (this.h / 2)).init(this.scale, dropName);
    }

    /* access modifiers changed from: package-private */
    public void dropBomb() {
        new Bomb(this.x + (this.w / 2), this.y + (this.h / 2), this.level).init(this.scale, this.type);
    }

    /* access modifiers changed from: package-private */
    public boolean canAttack() {
        if (isDead()) {
            return false;
        }
        return true;
    }

    public boolean isDead() {
        return this.hp <= 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isFocus() {
        System.out.println("Rank.focusType:" + ((int) Rank.focusType));
        System.out.println("Rank.focus:" + Rank.focus + "  index:" + this.index);
        return Rank.focusType == 2 && Rank.focus == this.index;
    }

    /* access modifiers changed from: package-private */
    public void clearSelected() {
        if (isFocus()) {
            Rank.clearFocus();
            System.out.println("deck die clearFocus");
        }
    }

    /* access modifiers changed from: package-private */
    public void addHpLine() {
        this.showHpTime -= Rank.getGameSpeed();
        if (this.showHpTime <= 0) {
            this.blood.canNotSee(false);
        } else if (this.hp > 0 && this.hp < this.hpMax) {
            this.blood.draw(Color.BLACK, (this.x + (this.w / 2)) - 30, this.hitY - this.hpBarY, 60.0f, false);
            this.blood.draw(Color.GREEN, (this.x + (this.w / 2)) - 30, this.hitY - this.hpBarY, (float) ((this.hp * 60) / this.hpMax), false);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isDeck(int px, int py) {
        return Tools.pointAndRect(px, py, this.x, this.y, this.w, this.h);
    }

    public void addAttackAction() {
        if (this.icon.getActions().size == 0) {
            this.icon.setOrigin(this.icon.getWidth() / 2.0f, this.icon.getHeight());
            this.icon.clearActions();
            this.icon.setScale(this.scale, this.scale);
            this.icon.addAction(Actions.sequence(Actions.scaleTo(1.0f, this.scale - 0.05f, 0.03f, Interpolation.sineOut), Actions.scaleTo(this.scale + 0.05f, 0.9f, 0.07f, Interpolation.sineOut), Actions.scaleTo(this.scale, this.scale, 0.1f, Interpolation.sineIn)));
        }
    }

    /* access modifiers changed from: package-private */
    public void addUpAction(ActorImage actor) {
        float waitTime = 0.8f / ((float) Rank.getGameSpeed());
        float time = 0.5f / ((float) Rank.getGameSpeed());
        actor.setScale(0.1f);
        actor.addAction(Actions.sequence(Actions.delay(waitTime), Actions.parallel(Actions.scaleTo(this.scale, this.scale, time), Actions.moveBy(Animation.CurveTimeline.LINEAR, -80.0f, time)), Actions.moveBy(Animation.CurveTimeline.LINEAR, 80.0f, time)));
    }

    public boolean inAttackArea(int towerX, int towerY, int towerRange) {
        if (this.imageW == this.imageH) {
            if (!Tools.inAttackRange(this.hitX, this.hitY, towerX, towerY, towerRange, this.offRange)) {
                return false;
            }
        } else {
            if (!Tools.circleAndRect(towerX, towerY, towerRange, this.hitX - (this.imageW / 2), this.hitY - (this.imageH / 2), this.imageW, this.imageH)) {
                return false;
            }
        }
        return true;
    }

    public boolean inBulletArea_hit(int bulletX, int bulletY) {
        if (this.imageW == this.imageH) {
            return Tools.circleAndCircle(bulletX, bulletY, 1, this.hitX, this.hitY, this.offRange);
        }
        return Tools.circleAndRect(bulletX, bulletY, 1, this.hitX - (this.imageW / 2), this.hitY - (this.imageH / 2), this.imageW, this.imageH);
    }
}
