package com.sg.td.group;

import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class BossUI extends MyGroup implements GameConstant {
    ActorClipImage hp;

    public void init() {
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU042, 19, 93, 12, this);
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU040, 109, 93, 12, this);
        this.hp = new ActorClipImage(PAK_ASSETS.IMG_ZHANDOU041, 109, 93, 12, this);
        this.hp.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.hp.getWidth(), 34.0f);
        GameStage.addActor(this, 1);
    }

    public void run() {
        this.hp.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (this.hp.getWidth() * ((float) Rank.boss.hp)) / ((float) Rank.boss.hpMax), 34.0f);
    }

    public void exit() {
    }
}
