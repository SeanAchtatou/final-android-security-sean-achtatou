package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.tools.MyGroup;
import com.sg.tools.NumActor;
import com.sg.util.GameStage;

public class WaveBegin extends MyGroup implements GameConstant {
    static int[] sound_lastWave = {46, 30, 38, 46};
    ActorImage cryImage;
    float gameTime;
    NumActor numActor;
    int number = 3;
    int x;
    int y;

    public void init() {
        setTransform(true);
        this.number = 3;
        this.x = 320;
        this.y = PAK_ASSETS.IMG_2X1;
        if (Rank.level.wave != Rank.level.waveMax - 1 || Rank.ENDLESS_MODE) {
            ActorImage wavebg = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU024, this.x, this.y, 1, this);
            NumActor waveNum = new NumActor();
            waveNum.setPosition((float) (this.x - 45), (float) this.y);
            waveNum.set(PAK_ASSETS.IMG_ZHANDOU026, Rank.level.showWave, -5, 4);
            addActor(wavebg);
            addActor(waveNum);
            addAction();
        } else {
            new Effect().addEffect(39, this.x, this.y, 3);
            playSound_lastWave();
        }
        this.numActor = new NumActor();
        this.numActor.setPosition((float) Map.getBossHomeX(), (float) (Map.getBossHomeY() + 15));
        this.numActor.set(PAK_ASSETS.IMG_ZHANDOU036, this.number, -3, 4);
        GameStage.addActor(this, 4);
        GameStage.addActor(this.numActor, 4);
        Rank.boss.setAnimation_Angry();
        showStart();
    }

    /* access modifiers changed from: package-private */
    public void addAction() {
        setOrigin((float) this.x, (float) this.y);
        setScale(0.1f);
        addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.3f), Actions.delay(1.0f), Actions.moveTo(getX(), getY() - 50.0f, 0.3f), Actions.scaleTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR)));
    }

    public void run(float delta) {
        if (!Rank.isPause()) {
            if (this.number == 2 && Rank.rank == 1 && !Rank.isTeach()) {
                RankData.teach.initBuildTeach();
            }
            if (this.number == 0) {
                free();
                return;
            }
            this.gameTime += ((float) Rank.getGameSpeed()) * delta;
            if (this.gameTime >= 1.0f) {
                this.gameTime = Animation.CurveTimeline.LINEAR;
                NumActor numActor2 = this.numActor;
                int i = this.number - 1;
                this.number = i;
                numActor2.changeNum(i);
            }
        }
    }

    public void exit() {
        Rank.clearSystemEvent();
        Rank.setEnemyPause(false);
        GameStage.removeActor(this.numActor);
        if (Rank.ENDLESS_MODE && !Rank.cry) {
            new BossCry();
            Rank.cry = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void showStart() {
        int aimX = Map.getBossHomeX();
        int aimY = Map.getBossHomeY();
        int ox = Map.path[0][0];
        int oy = Map.path[0][1];
        int ox1 = Map.path[1][0];
        int oy1 = Map.path[1][1];
        int dir = 0;
        if (ox == ox1) {
            dir = oy1 > oy ? 2 : 0;
        } else if (oy == oy1) {
            dir = ox1 > ox ? 1 : 3;
        }
        switch (dir) {
            case 0:
                aimY -= Map.tileHight;
                break;
            case 1:
                aimX += Map.tileWidth;
                break;
            case 2:
                aimY += Map.tileHight;
                break;
            case 3:
                aimX -= Map.tileWidth;
                break;
        }
        Effect effect = new Effect();
        effect.addEffect(53, aimX, aimY, 3);
        effect.setRotation((float) ((((dir * 90) + 180) % PAK_ASSETS.IMG_XINJILU) + 180));
    }

    /* access modifiers changed from: package-private */
    public void playSound_lastWave() {
        if (Rank.role != null) {
            Sound.playSound(sound_lastWave[Rank.role.getRoleIndex()]);
            return;
        }
        Sound.playSound(sound_lastWave[RankData.getSelectRoleIndex()]);
    }
}
