package com.sg.td.group;

import com.kbz.Actors.ActorImage;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Mask;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class Bg extends MyGroup implements GameConstant {
    int num;
    int x;
    int y;

    public Bg(int x2, int y2, int num2) {
        this.x = x2;
        this.y = y2;
        this.num = num2;
        initBg();
    }

    public void initBg() {
        addActor(new Mask());
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC001, this.x, this.y, 5, this);
        for (int i = 0; i < this.num; i++) {
            new ActorImage((int) PAK_ASSETS.IMG_PUBLIC002, this.x, this.y + 68 + (i * 64), 5, this);
        }
        new ActorImage((int) PAK_ASSETS.IMG_PUBLIC003, this.x, this.y + 68 + (this.num * 64), 1, this);
        new ActorImage((int) PAK_ASSETS.IMG_SHUOMING001, (this.x - 310) + 10, this.y + 97, 10, this);
        GameStage.addActor(this, 4);
    }

    public void exit() {
    }

    public void init() {
    }
}
