package com.sg.td.group;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Boss;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.data.Mydata;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class ShowTower extends MyGroup implements GameConstant {
    MyImage kownButton;
    float time;

    public void init() {
        addShow(320, PAK_ASSETS.IMG_DAJI06);
        Sound.playSound(81);
    }

    /* access modifiers changed from: package-private */
    public void addShow(int x, int y) {
        addActor(new Mask());
        new ActorImage((int) PAK_ASSETS.IMG_SHUOMINGKUANG, x - 182, y, 12, this);
        if (!Rank.introTower) {
            new ActorImage(Mydata.deckData.get(Rank.introObj).getShuomingName(), x, y + 82, 1, this);
            String tubioa = Mydata.deckData.get(Rank.introObj).getTubiaoName();
            if (tubioa.equals("donghua")) {
                addActor(new Boss.BossSpine(x, y + PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 18));
            } else {
                new ActorImage(tubioa, x, y + 192, 1, this);
            }
            new ActorImage(Mydata.deckData.get(Rank.introObj).getZiName(), x, y + PAK_ASSETS.IMG_XINJILU, 1, this);
        } else {
            String descp2 = "";
            String descp3 = "";
            if (Mydata.towerData.get(Rank.introObj) != null) {
                String descp1 = Mydata.towerData.get(Rank.introObj).getDescp1();
                descp2 = Mydata.towerData.get(Rank.introObj).getDescp2();
                descp3 = Mydata.towerData.get(Rank.introObj).getDescp3();
                RankData.unClockTower(Mydata.towerData.get(Rank.introObj).getIndex());
                ActorText text1 = new ActorText(descp1, (int) PAK_ASSETS.IMG_JIDONGSHEXIAN, (int) PAK_ASSETS.IMG_GONGGAO004, 12, this);
                if (descp1.trim().length() > 11) {
                    text1.setFontScaleXY(1.0f);
                } else {
                    text1.setFontScaleXY(1.1f);
                }
                text1.setWidth(250);
                text1.setColor(new Color(Color.BLACK));
            }
            new ActorImage(Mydata.towerData.get(Rank.introObj).getShuomingName(), x, y + 82, 1, this);
            new ActorImage(Mydata.towerData.get(Rank.introObj).getTubiaoName(), x, y + 162, 1, this);
            new ActorImage((int) PAK_ASSETS.IMG_SHUOMINGTIAO, x, y + PAK_ASSETS.IMG_SMOKE2, 1, this);
            new ActorImage((int) PAK_ASSETS.IMG_SHUOMING003, x - 59, y + PAK_ASSETS.IMG_TIPS01, 1, this);
            ActorText text2 = new ActorText(descp2, (int) PAK_ASSETS.IMG_TIPS03, (int) PAK_ASSETS.IMG_E03A, 12, this);
            text2.setWidth(100);
            text2.setFontScaleXY(1.2f);
            ActorText text3 = new ActorText(descp3, (int) PAK_ASSETS.IMG_TIPS03, (int) PAK_ASSETS.IMG_ZHANDOU023, 12, this);
            text3.setWidth(100);
            if (descp3.length() > 4) {
                text3.setFontScaleXY(1.0f);
            } else {
                text3.setFontScaleXY(1.2f);
            }
        }
        this.kownButton = new MyImage((int) PAK_ASSETS.IMG_SHUOMING004, (float) (x - 116), (float) (y + 500), 0);
        this.kownButton.setTouchable(Touchable.enabled);
        this.kownButton.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ShowTower.this.kownButton.setTextureRegion((int) PAK_ASSETS.IMG_SHUOMING005);
                Sound.playButtonPressed();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                ShowTower.this.kownButton.setTextureRegion((int) PAK_ASSETS.IMG_SHUOMING004);
                ShowTower.this.free();
            }
        });
        GameStage.addActor(this, 4);
    }

    public void run(float delta) {
        this.time += delta;
        if (this.time > 1.0f) {
            this.kownButton.setScale(Animation.CurveTimeline.LINEAR);
            this.kownButton.setOrigin(this.kownButton.getWidth() / 2.0f, this.kownButton.getHeight() / 2.0f);
            this.kownButton.addAction(Actions.sequence(Actions.scaleTo(1.3f, 1.3f, 0.1f), Actions.scaleTo(1.0f, 1.0f, 0.1f)));
            GameStage.addActor(this.kownButton, 4);
        }
    }

    public void exit() {
        Rank.clearSystemEvent();
        Rank.setSystemEvent((byte) 2);
        GameStage.removeActor(this.kownButton);
    }
}
