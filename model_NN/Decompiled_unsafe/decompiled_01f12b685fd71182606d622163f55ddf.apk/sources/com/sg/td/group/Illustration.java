package com.sg.td.group;

import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.Sound;
import com.sg.td.actor.Mask;
import com.sg.td.kill.Kill1;
import com.sg.td.kill.Kill2;
import com.sg.td.kill.Kill3;
import com.sg.td.kill.Kill4;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class Illustration extends MyGroup implements GameConstant {
    static int[] dazhaoSound = {43, 28, 36, 35};
    KillSpine killSpine;
    int roleIndex;

    public void init() {
        this.roleIndex = Rank.role.getRoleIndex();
        int spinId = 1;
        if (this.roleIndex == 1) {
            spinId = 0;
        } else if (this.roleIndex == 2) {
            spinId = 3;
        } else if (this.roleIndex == 3) {
            spinId = 2;
        }
        this.killSpine = new KillSpine(320, PAK_ASSETS.IMG_2X1, spinId);
        addActor(new Mask());
        addActor(this.killSpine);
        GameStage.addActor(this, 4);
        playSound_dazhao(this.roleIndex);
    }

    /* access modifiers changed from: package-private */
    public void playSound_dazhao(int roleIndex2) {
        Sound.playSound(dazhaoSound[roleIndex2]);
    }

    public void run(float delta) {
        if (this.killSpine.isEnd()) {
            free();
            if (this.roleIndex == 0) {
                new Kill1();
            } else if (this.roleIndex == 1) {
                new Kill2();
            } else if (this.roleIndex == 2) {
                new Kill3();
            } else if (this.roleIndex == 3) {
                new Kill4();
            }
        }
    }

    public void exit() {
        Rank.clearSystemEvent();
        Rank.illustration = null;
    }

    class KillSpine extends MySpine implements GameConstant {
        public KillSpine(int x, int y, int spineID) {
            init(SPINE_NAME);
            createSpineRole(spineID, 1.0f);
            setMix(0.3f);
            initMotion(motion_bisha);
            setStatus(4);
            setPosition((float) x, (float) y);
            setId();
            setAniSpeed(1.0f);
            setAnimation("animation", false);
        }

        public void run(float delta) {
            updata();
            this.index++;
        }

        public void act(float delta) {
            super.act(delta);
            run(delta);
        }
    }
}
