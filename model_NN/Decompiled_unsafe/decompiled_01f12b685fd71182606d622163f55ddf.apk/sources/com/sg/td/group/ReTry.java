package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.tools.MyGroup;
import com.sg.tools.NumActor;
import com.sg.util.GameStage;

public class ReTry extends MyGroup implements GameConstant {
    int x;
    int y;

    public void init() {
        setTransform(true);
        this.x = 320;
        this.y = PAK_ASSETS.IMG_MAP4;
        ActorImage bg = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU025, this.x, this.y, 1, this);
        NumActor waveNum = new NumActor();
        waveNum.setPosition((float) (this.x + 35), (float) this.y);
        waveNum.set(PAK_ASSETS.IMG_ZHANDOU026, Rank.level.showWave, 0, 4);
        addActor(bg);
        addActor(waveNum);
        addAction();
        GameStage.addActor(this, 4);
    }

    /* access modifiers changed from: package-private */
    public void addAction() {
        setOrigin((float) this.x, (float) this.y);
        setScale(0.1f);
        addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.3f), Actions.delay(1.0f), Actions.moveTo(getX(), getY() - 50.0f, 0.3f), Actions.scaleTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR), Actions.run(new Runnable() {
            public void run() {
                ReTry.this.free();
            }
        })));
    }

    public void exit() {
    }
}
