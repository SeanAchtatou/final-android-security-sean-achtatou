package com.sg.td.group;

import cn.egame.terminal.paysdk.FailedCode;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Message;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class AwardUI extends MyGroup implements GameConstant {
    public boolean appear;
    public boolean canSee;
    int index;
    Mask mask;
    public boolean show;
    public boolean showing;
    float time;
    int x = 320;
    int y = PAK_ASSETS.IMG_QIANDAO011;

    public void init(final int money, String icon, final int index2) {
        setTransform(true);
        this.appear = true;
        this.mask = new Mask();
        this.show = false;
        this.index = index2;
        addActor(new Effect().getEffect_Diejia(58, this.x, this.y));
        addActor(new MyImage(icon + ".png", (float) this.x, (float) (this.y + FailedCode.REASON_CODE_INIT_FAILED), 4));
        new ActorImage((int) PAK_ASSETS.IMG_SHOP015, this.x, this.y, 1, this);
        GNumSprite timeSprite = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN007, "X" + money, "X", -2, 4);
        timeSprite.setPosition((float) this.x, (float) (this.y + 80));
        addActor(timeSprite);
        final MyImage getButton = new MyImage((int) PAK_ASSETS.IMG_ONLINE006, (float) (this.x - 74), (float) (this.y + 190), 0);
        addActor(getButton);
        getButton.setTouchable(Touchable.enabled);
        getButton.setName("get");
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                if (!"get".equals(event.getTarget().getName())) {
                    return true;
                }
                getButton.setTextureRegion((int) PAK_ASSETS.IMG_ONLINE007);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ("get".equals(event.getTarget().getName())) {
                    RankData.addCakeNum(money);
                    Message.showGiftMessage(index2);
                    AwardUI.this.showing = false;
                    AwardUI.this.appear = false;
                    AwardUI.this.free();
                }
            }
        });
        Sound.playSound(16);
    }

    public void add() {
        GameStage.addActor(this.mask, 4);
        GameStage.addActor(this, 4);
        setScale(Animation.CurveTimeline.LINEAR);
        setOrigin((float) this.x, (float) this.y);
        addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.3f), Actions.scaleTo(1.3f, 1.3f, 0.1f), Actions.scaleTo(1.0f, 1.0f, 0.1f)));
        this.show = true;
        this.showing = true;
    }

    public void run(float delta) {
    }

    public void init() {
    }

    public void exit() {
        GameStage.removeActor(this.mask);
    }
}
