package com.sg.td.group;

import cn.egame.terminal.paysdk.FailedCode;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.CHBezierToAction;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.Tools;
import com.sg.td.TowerRole;
import com.sg.td.UI.MyEquipment;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Honey;
import com.sg.td.actor.Mask;
import com.sg.td.message.GameSpecial;
import com.sg.td.message.MySwitch;
import com.sg.td.spine.RoleSpine;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.tools.MySprite;
import com.sg.util.GameStage;

public class Teach extends MyGroup implements GameConstant {
    public static boolean[] hasTeach = new boolean[25];
    boolean addUseFree;
    int id = -1;
    int[] imageIndex = {-1, -1, -1, -1, -1, -1, -1, 768, 771, 770, 769, -1};
    boolean pickHoney;
    ActorImage pickImage;
    ActorImage quanActorImage;
    boolean selectTowerPressed;
    int selectTowerPressedY;
    public int[][] teachArea = {new int[]{PAK_ASSETS.IMG_SHUAGUAI01, 975, 99, 54}, new int[]{178, 926, PAK_ASSETS.IMG_BUFF_JINBI, 116}, new int[]{320, PAK_ASSETS.IMG_I02, 70, 70}, new int[]{320, PAK_ASSETS.IMG_ONLINE006, 70, 70}, new int[]{320, PAK_ASSETS.IMG_I02, 70, 70}, new int[]{320, PAK_ASSETS.IMG_QIANDAO004, 70, 70}, new int[]{PAK_ASSETS.IMG_GONGGAO011, 1023, 112, 112}, new int[0], new int[0], new int[0], new int[0], new int[]{PAK_ASSETS.IMG_3X1, 10, 75, 80}, new int[]{PAK_ASSETS.IMG_GONGGAO011, 1023, 112, 112}, new int[]{PAK_ASSETS.IMG_UIFLASH, 10, 75, 80}, new int[]{250, PAK_ASSETS.IMG_I02, 70, 140}, new int[]{PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING, 885, 200, 78}, new int[]{69, PAK_ASSETS.IMG_UP007, PAK_ASSETS.IMG_JUEHUIXUANFU, 94}, new int[]{PAK_ASSETS.IMG_016, PAK_ASSETS.IMG_UP003, 70, 70}, new int[]{44, PAK_ASSETS.IMG_PAO006, 130, 50}, new int[]{46, 253, 129, 129}, new int[]{201, 926, PAK_ASSETS.IMG_SHENSHENYILONGGONG, 85}, new int[0], new int[]{PAK_ASSETS.IMG_UI_A014, 931, PAK_ASSETS.IMG_BUFF_JINBI, 106}, new int[]{195, 865, PAK_ASSETS.IMG_BUFF_JINBI, 114}};
    boolean teaching;
    float time;

    public void addGameBeginTeach() {
        if (!this.teaching) {
            initTeach(0);
        }
    }

    public void giveWatchProp() {
        if (Rank.rank == 5 && !hasTeach[19]) {
            RankData.addProps(0, 1);
        }
    }

    public void addEquipTeach() {
        if (this.teaching) {
            return;
        }
        if (Rank.rank == 4 && RankData.getNeedStrengthTowers().size() != 0) {
            initTeach(17);
        } else if (Rank.rank == 5) {
            initTeach(19);
        }
    }

    public void addRoleUpTeach() {
        if (!this.teaching && RankData.intoRoleUpTeach()) {
            initTeach(15);
        }
    }

    public void init() {
    }

    /* access modifiers changed from: package-private */
    public void addBg(int x, int y) {
        addActor(new Mask(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 640.0f, (float) (y - 110)));
        addActor(new Mask(Animation.CurveTimeline.LINEAR, (float) (y + 110), 640.0f, (float) (1136 - y)));
        addActor(new Mask(Animation.CurveTimeline.LINEAR, (float) (y - 110), (float) (x - 110), 220.0f));
        addActor(new Mask((float) (x + 110), (float) (y - 110), (float) ((640 - x) - 110), 220.0f));
        this.quanActorImage = new ActorImage(774, x, y, 1, this);
        this.quanActorImage.setTouchable(Touchable.enabled);
    }

    public void add(int x, int y, int tx, int ty, String content, int imageIndex2) {
        clear();
        addBg(x, y);
        if (imageIndex2 != -1) {
            this.pickImage = new ActorImage(imageIndex2, x, y, 1, this);
            this.pickImage.setScale(Animation.CurveTimeline.LINEAR);
            this.pickImage.addAction(Actions.sequence(Actions.scaleTo(1.3f, 1.3f, 0.2f), Actions.scaleTo(1.0f, 1.0f, 0.1f)));
            addActor(this.pickImage);
        }
        new Effect().addEffect_Diejia(89, x, y, this);
        String anniName = "dianji";
        if (this.id == 4) {
            anniName = "huadong";
        }
        MySprite sprite = new MySprite(ANIMATION_NAME[0] + ".json", anniName);
        sprite.setPosition((float) x, (float) y);
        sprite.setTouchable(Touchable.disabled);
        addActor(sprite);
        if (content != null) {
            new ActorImage((int) PAK_ASSETS.IMG_JIAOXUE008, tx, ty, 10, this);
            new ActorText(content, tx + 175, ty - 130, 1, this);
        }
        GameStage.addActor(this, 4);
    }

    public void initTeach(int teachIndex) {
        if (!hasTeach[teachIndex]) {
            System.out.println("initTeach teachIndex:" + teachIndex);
            this.id = teachIndex;
            this.teaching = true;
            int tx = this.teachArea[this.id][0];
            int ty = this.teachArea[this.id][1];
            int tw = this.teachArea[this.id][2];
            int th = this.teachArea[this.id][3];
            switch (this.id) {
                case 0:
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty - 65, "点击进入游戏", -1);
                    this.quanActorImage.setTouchable(Touchable.enabled);
                    Sound.playSound(24);
                    return;
                case 1:
                    add(tx + (tw / 2), ty + (th / 2), 0, 0, null, -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    return;
                case 2:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty, "点击建造" + "[RED]" + "炎火巨剑" + "[WHITE]！", -1);
                    Sound.playSound(24);
                    return;
                case 3:
                    add(tx + (tw / 2), ty + (th / 2), 0, 0, null, -1);
                    return;
                case 4:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty - 70, "向上滑动" + "[RED]" + "升级" + "[WHITE]！", -1);
                    Sound.playSound(20);
                    return;
                case 5:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty, "点击攻击" + "[RED]" + "气泡" + "[WHITE]！", -1);
                    Sound.playSound(24);
                    return;
                case 6:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), PAK_ASSETS.IMG_DAJI05, ty, "点击" + "[RED]" + "释放斗龙技" + "[WHITE]！", -1);
                    giveSKill();
                    Sound.playSound(19);
                    return;
                case 7:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), Math.min(Math.max(0, ((tw / 2) + tx) - 175), (int) PAK_ASSETS.IMG_DAJI05), ty, "点击拾取" + "[RED]" + "元素之力" + "[WHITE]！", this.imageIndex[this.id]);
                    Sound.playSound(22);
                    return;
                case 8:
                case 9:
                case 10:
                    Rank.setSystemEvent((byte) 13);
                    this.time = Animation.CurveTimeline.LINEAR;
                    this.addUseFree = false;
                    add(tx + (tw / 2), ty + (th / 2), Math.min(Math.max(0, ((tw / 2) + tx) - 175), (int) PAK_ASSETS.IMG_DAJI05), ty, "点击拾取" + "[RED]" + "试玩宝箱" + "[WHITE]！", this.imageIndex[this.id]);
                    Sound.playSound(22);
                    return;
                case 11:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), PAK_ASSETS.IMG_DAJI05, ty + th + 260, "点击" + "[RED]" + "重玩" + "[WHITE]" + "这一波" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.enabled);
                    Sound.playSound(19);
                    return;
                case 12:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), PAK_ASSETS.IMG_DAJI05, ty, "点击免费释放" + "[RED]" + "斗龙技" + "[WHITE]！", -1);
                    Sound.playSound(19);
                    return;
                case 13:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), PAK_ASSETS.IMG_DAJI05, ty + th + 260, "点击" + "[RED]" + "加速游戏" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    Sound.playSound(24);
                    return;
                case 14:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty, "[RED]" + "点击千帆，火力全开" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    Sound.playSound(21);
                    return;
                case 15:
                    RankData.giveMoney();
                    add(tx + (tw / 2), ty + (th / 2), -20, 1156, "点击进行" + "[RED]" + "宝贝龙升级" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    Sound.playSound(24);
                    return;
                case 16:
                    add(tx + (tw / 2), ty + (th / 2), 0, 0, null, -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    return;
                case 17:
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty, "点击进行" + "[RED]" + "炮塔强化" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    Sound.playSound(24);
                    return;
                case 18:
                    add(tx + (tw / 2), ty + (th / 2), 0, 0, null, -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    return;
                case 19:
                    add(tx + (tw / 2), ty + (th / 2), 0, ty, "点击使用" + "[RED]" + "斗龙装备" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    Sound.playSound(23);
                    return;
                case 20:
                    add(tx + (tw / 2), ty + (th / 2), 0, 0, null, -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    return;
                case 21:
                    Rank.setSystemEvent((byte) 13);
                    add(tx + (tw / 2), ty + (th / 2), ((tw / 2) + tx) - 175, ty, "点击让" + "[RED]" + "飞行食物" + "[WHITE]" + "落下" + "[WHITE]！", -1);
                    this.quanActorImage.setTouchable(Touchable.disabled);
                    Sound.playSound(19);
                    return;
                case 22:
                    if (MySwitch.isCaseA == 0) {
                        this.teaching = false;
                        addHand((tw / 2) + tx, (th / 2) + ty);
                        hasTeach[this.id] = true;
                        GameStage.addActor(this, 4);
                        return;
                    }
                    hasTeach[this.id] = true;
                    return;
                case 23:
                    clear();
                    remove();
                    addHand((tw / 2) + tx, (th / 2) + ty);
                    GameStage.addActor(this, 4);
                    Sound.playSound(24);
                    return;
                default:
                    return;
            }
        }
    }

    public void initBuildTeach() {
        if (!hasTeach[2]) {
            Rank.clearSystemEvent();
            initTeach(2);
        }
    }

    public void run() {
        if (Rank.isRanking()) {
            if (Rank.rank == 1) {
                if (Rank.level.wave == 1 && !Rank.isSystemEvent() && Rank.isTowerArea(this.teachArea[4][0] + (Map.tileWidth / 2), this.teachArea[4][1] + (Map.tileHight / 2))) {
                    initTeach(4);
                }
                if (Rank.level.wave == 2 && !Rank.isSystemEvent() && Rank.isDeckArea(this.teachArea[5][0] + 35, this.teachArea[5][1] + 35)) {
                    initTeach(5);
                }
                if (Rank.level.wave == 3 && Rank.level.isPassPath_1() && !Rank.isSystemEvent()) {
                    initTeach(6);
                }
            }
            if (Rank.rank == 2) {
                if (Rank.level.wave == 2 && Rank.level.isPassPath_1()) {
                    initTeach(12);
                }
                if (Rank.level.wave == 3 && !Rank.isSystemEvent()) {
                    initTeach(13);
                }
            }
            if (Rank.rank == 4 && Rank.level.wave == 0 && !Rank.isSystemEvent()) {
                initTeach(14);
            }
            if (Rank.home.isHurt() && !Rank.isSystemEvent()) {
                initTeach(11);
            }
        }
    }

    public void run(float delta) {
        if (this.id == 7 || this.id == 8 || this.id == 9 || this.id == 10) {
            this.time += delta;
            if (this.time > 2.0f && !this.addUseFree) {
                switch (this.id) {
                    case 7:
                        pickHoney();
                        return;
                    case 8:
                        addUseFree(776, 3);
                        return;
                    case 9:
                        addUseFree(775, 2);
                        return;
                    case 10:
                        addUseFree(PAK_ASSETS.IMG_SHIYONG003, 1);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void giveSKill() {
        RankData.addHoneyNum(1);
    }

    public void touchDown(int x, int y) {
        if (Rank.isTeach() && hitArea(this.id, x, y)) {
            int dx = Map.getScreenX(x);
            int dy = Map.getScreenY(y);
            switch (this.id) {
                case 0:
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 1:
                case 5:
                case 6:
                case 8:
                case 9:
                case 10:
                case 12:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                default:
                    return;
                case 2:
                    if (!Rank.icon.isEmpty()) {
                        Rank.removeIcon();
                    }
                    if (Rank.selectEmptyFloor(dx, dy)) {
                        hasTeach[this.id] = true;
                        initTeach(3);
                        return;
                    }
                    return;
                case 3:
                    Rank.buildTower(dx, dy);
                    free();
                    return;
                case 4:
                    if (Rank.selectTower(dx, dy)) {
                        this.selectTowerPressed = true;
                        this.selectTowerPressedY = y;
                        return;
                    }
                    return;
                case 7:
                    pickHoney();
                    return;
                case 11:
                    System.out.println("teach重玩本关");
                    hasTeach[this.id] = true;
                    free();
                    Rank.setSystemEvent((byte) 8);
                    return;
                case 13:
                    System.out.println("teach加速");
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 14:
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 21:
                    hasTeach[this.id] = true;
                    free();
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void pickHoney() {
        float vx;
        if (!this.pickHoney) {
            System.out.println("拾取元素之力");
            Action run = Actions.run(new Runnable() {
                public void run() {
                    Teach.hasTeach[Teach.this.id] = true;
                    Teach.this.giveSKill();
                    RankData.pickDrop(Rank.getRank());
                    Rank.rankUI.addShake();
                    Teach.this.free();
                }
            });
            if (this.teachArea[7][0] + 200 > 640) {
                vx = 578.0f;
            } else {
                vx = (float) (this.teachArea[7][0] + 200);
            }
            this.pickImage.addAction(Actions.sequence(Actions.parallel(CHBezierToAction.obtain(new Bezier<>(new Vector2(vx, (float) (this.teachArea[7][1] + FailedCode.REASON_CODE_INIT_FAILED)), new Vector2(516.0f, 1018.0f)), 0.7f), Actions.scaleTo(0.2f, 0.2f, 0.7f)), run));
            this.pickHoney = true;
            Honey.playSound_pick();
        }
    }

    /* access modifiers changed from: package-private */
    public void addUseFree(int imageIndex2, final int roleIndex) {
        clear();
        addActor(new Mask());
        new ActorImage(imageIndex2, 0, (int) PAK_ASSETS.IMG_MONEYBOX_SHUOMING, 12, this);
        final MyImage use = new MyImage((int) PAK_ASSETS.IMG_SHIYONG004, 175.0f, 674.0f, 0);
        if (GameSpecial.getRightCorner) {
            use.setPosition(305.0f, 674.0f);
        }
        addActor(use);
        use.setTouchable(Touchable.enabled);
        use.setName("use");
        if (GameSpecial.getRightCorner) {
            addHand(PAK_ASSETS.IMG_JIESUO002, PAK_ASSETS.IMG_SHOP016);
        } else {
            addHand(320, PAK_ASSETS.IMG_SHOP016);
        }
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!"use".equals(event.getTarget().getName())) {
                    return true;
                }
                use.setTextureRegion((int) PAK_ASSETS.IMG_SHIYONG005);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ("use".equals(event.getTarget().getName())) {
                    use.setTextureRegion((int) PAK_ASSETS.IMG_SHIYONG004);
                    Teach.this.showGetRole(roleIndex);
                    Rank.rankUI.setfreeUse(true);
                }
            }
        });
        this.addUseFree = true;
        Sound.playSound(18);
    }

    /* access modifiers changed from: package-private */
    public void addHand(int x, int y) {
        new Effect().addEffect_Diejia(89, x, y, this);
        MySprite sprite = new MySprite(ANIMATION_NAME[0] + ".json", "dianji");
        sprite.setPosition((float) x, (float) y);
        sprite.setTouchable(Touchable.disabled);
        addActor(sprite);
    }

    /* access modifiers changed from: package-private */
    public void showGetRole(final int roleIndex) {
        clear();
        addActor(new Mask());
        new Effect().addEffect_Diejia(58, 320, (int) PAK_ASSETS.IMG_2X1, this);
        RoleSpine role = new RoleSpine(320, PAK_ASSETS.IMG_2X1, RankData.getRoleSpineId(roleIndex));
        role.setScale(1.2f);
        addActor(role);
        role.addAction(Actions.sequence(Actions.delay(1.5f), Actions.run(new Runnable() {
            public void run() {
                Rank.building.changeRole(roleIndex);
                TowerRole.playSound_show(roleIndex);
                Teach.this.free();
            }
        })));
        ActorText actorText = new ActorText(new String[]{"获得宝贝龙雷古曼", "获得铠甲卡维利", "  获得铠甲索里", "获得铠甲雷古曼"}[roleIndex], (int) PAK_ASSETS.IMG_JUXINGNENGLIANGTA, (int) PAK_ASSETS.IMG_ZHANDOU021, 12, this);
        actorText.setWidth(200);
        actorText.setFontScaleXY(1.3f);
        Sound.playSound(16);
    }

    public void touchUp(int x, int y) {
        if (Rank.isTeach()) {
            System.out.println("touchUp id:" + this.id);
            int dx = Map.getScreenX(x);
            int dy = Map.getScreenY(y);
            if (this.id == 4 && this.selectTowerPressed && this.selectTowerPressedY - y > 10) {
                this.selectTowerPressed = false;
                Rank.building.updateTower();
                hasTeach[this.id] = true;
                free();
            }
            if (hitArea(this.id, x, y)) {
                System.out.println("touchUp hitArea id:" + this.id);
                switch (this.id) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 7:
                    case 11:
                    default:
                        return;
                    case 5:
                        Rank.selectFocus(dx, dy);
                        hasTeach[this.id] = true;
                        free();
                        return;
                    case 6:
                        hasTeach[this.id] = true;
                        free();
                        Rank.putSkill(false);
                        return;
                    case 8:
                        if (!this.addUseFree) {
                            addUseFree(776, 3);
                            return;
                        }
                        return;
                    case 9:
                        if (!this.addUseFree) {
                            addUseFree(775, 2);
                            return;
                        }
                        return;
                    case 10:
                        if (!this.addUseFree) {
                            addUseFree(PAK_ASSETS.IMG_SHIYONG003, 1);
                            return;
                        }
                        return;
                    case 12:
                        hasTeach[this.id] = true;
                        free();
                        Rank.putSkill_free();
                        Rank.rankUI.setfreeUse(false);
                        System.out.println("12教学 释放斗龙技");
                        return;
                }
            }
        }
    }

    public void touchDown_UI(int x, int y) {
        if (hitArea(this.id, x, y)) {
            int i = this.id;
        }
    }

    public void touchUp_UI(int x, int y) {
        if (hitArea(this.id, x, y)) {
            switch (this.id) {
                case 0:
                    hasTeach[this.id] = true;
                    new MyEquipment();
                    initTeach(1);
                    return;
                case 1:
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 15:
                    hasTeach[this.id] = true;
                    initTeach(16);
                    return;
                case 16:
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 17:
                    hasTeach[this.id] = true;
                    initTeach(18);
                    return;
                case 18:
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 19:
                    hasTeach[this.id] = true;
                    initTeach(20);
                    return;
                case 20:
                    hasTeach[this.id] = true;
                    free();
                    return;
                case 23:
                    hasTeach[this.id] = true;
                    free();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hitArea(int id2, int x, int y) {
        if (id2 < 0 || id2 >= this.teachArea.length) {
            return false;
        }
        return Tools.inArea(this.teachArea[id2], new int[]{x, y});
    }

    public void exit() {
        Rank.clearSystemEvent();
        this.teaching = false;
        RankData.record.writeRecord(1);
    }

    public void resetTeachOne() {
        for (int i = 0; i <= 6; i++) {
            hasTeach[i] = false;
        }
    }
}
