package com.sg.td.group;

import com.sg.td.Rank;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class ShowStar extends MyGroup {
    byte event;
    float time;

    public ShowStar(byte event2) {
        this.event = event2;
    }

    public void init() {
        GameStage.addActor(this, 4);
    }

    public void run(float delta) {
        this.time += delta;
        if (this.time > 1.5f) {
            free();
        }
    }

    public void exit() {
        Rank.showStar = null;
        Rank.clearSystemEvent();
        Rank.setSystemEvent(this.event);
    }
}
