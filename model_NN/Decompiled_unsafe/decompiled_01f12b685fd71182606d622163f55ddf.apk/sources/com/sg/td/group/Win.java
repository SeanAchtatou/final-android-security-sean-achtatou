package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorImage;
import com.kbz.Sound.GameSound;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.Tools;
import com.sg.td.UI.Mymainmenu2;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.tools.GNumSprite;
import com.sg.tools.GameTime;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class Win extends MyGroup implements GameConstant {
    static int[] sound_win = {48, 32, 40, 48};
    boolean addButton;
    boolean change;
    int gameMin;
    int gamePercent;
    int gameSec;
    boolean isNew;
    int[][] loca = {new int[]{178, PAK_ASSETS.IMG_JIESUAN009, 73, PAK_ASSETS.IMG_JIESUAN004}, new int[]{320, 413, 73, PAK_ASSETS.IMG_JIESUAN003}, new int[]{PAK_ASSETS.IMG_LOAD2, PAK_ASSETS.IMG_JIESUAN009, 74, PAK_ASSETS.IMG_JIESUAN005}};
    Mask mask;
    int num = 0;
    boolean playNew;
    int second;
    int star;
    int starId;
    float time;
    GNumSprite timeSprite;

    public void init() {
        this.playNew = false;
        this.mask = new Mask();
        addActor(this.mask);
        new ActorImage((int) PAK_ASSETS.IMG_SHENGLI, 0, 0, 12, this).setTouchable(Touchable.enabled);
        new Effect().addEffect(86, 320, (int) PAK_ASSETS.IMG_2X1, this);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUAN002, 0, 0, 12, this);
        GameStage.addActor(this, 4);
        this.star = Rank.rankUI.starNum;
        System.out.println("胜利获得星星数目：" + this.star);
        this.gameMin = GameTime.getGameTime_min();
        this.gameSec = GameTime.getGameTime_sec();
        this.gamePercent = RankData.getPercent(this.gameMin, this.gameSec);
        RankData.consumeProp();
        addScore();
        saveData();
        addMoney(PAK_ASSETS.IMG_TX_JIANSU01, PAK_ASSETS.IMG_SHOP016);
        GameSound.playMusicOnce(4);
        playSound_win();
    }

    public void run(float delta) {
        this.time += delta;
        if (this.time > ((float) this.second)) {
            if (this.starId < this.star) {
                addStar(this.starId);
                this.starId++;
            }
            this.second++;
        }
        if (this.starId == this.star) {
            this.change = true;
            addCompare();
            this.starId++;
        }
        if (this.second % 2 == 0) {
            runCompare();
        }
        if (RankData.showAwardEnd() && !this.addButton) {
            addButton();
            this.addButton = true;
        }
        if (this.time > 3.0f && this.playNew) {
            Sound.playSound(56);
            this.playNew = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void addStar(int id) {
        int imgId = this.loca[id][3];
        int eftId = this.loca[id][2];
        int x = this.loca[id][0];
        int y = this.loca[id][1];
        new ActorImage(imgId, x, y, 1, this);
        addActor(new Effect().getEffect_Diejia(eftId, x, y));
        if (id == 0) {
            Sound.playSound(75);
        } else if (id == 1) {
            Sound.playSound(76);
        } else if (id == 2) {
            Sound.playSound(77);
        }
    }

    /* access modifiers changed from: package-private */
    public void addCompare() {
        this.timeSprite = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN006, this.num + "%", "%", -2, 4);
        this.timeSprite.setPosition((float) PAK_ASSETS.IMG_016DJ, (float) PAK_ASSETS.IMG_SHEZHI003);
        this.timeSprite.setOrigin((float) PAK_ASSETS.IMG_016DJ, (float) PAK_ASSETS.IMG_SHEZHI003);
        this.timeSprite.addAction(Actions.sequence(Actions.scaleTo(1.3f, 1.3f, 0.3f), Actions.delay(1.0f), Actions.scaleTo(1.0f, 1.0f, 0.3f)));
        addActor(this.timeSprite);
    }

    /* access modifiers changed from: package-private */
    public void runCompare() {
        if (this.change) {
            this.num += Tools.nextInt(3, 6);
            if (this.num > this.gamePercent) {
                this.num = this.gamePercent;
                this.change = false;
                RankData.showAwardUI();
                if (this.isNew) {
                    new ActorImage((int) PAK_ASSETS.IMG_XINJILU, (int) PAK_ASSETS.IMG_PAO002, (int) PAK_ASSETS.IMG_ZANTING004, 1, this);
                    this.playNew = true;
                }
            }
            this.timeSprite.setNum(this.num + "%");
        }
    }

    /* access modifiers changed from: package-private */
    public void addScore() {
        int[] score = RankData.getHistoryMaxScore();
        if (score == null) {
            new ActorImage((int) PAK_ASSETS.IMG_JIESUAN016, (int) PAK_ASSETS.IMG_DAJI07, (int) PAK_ASSETS.IMG_K04A, 12, this);
        } else {
            GNumSprite timeSprite1 = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN006, score[0] + "", "%", -2, 4);
            GNumSprite timeSprite2 = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN006, score[1] + "", "%", -2, 4);
            timeSprite1.setPosition(318.0f, 618.0f);
            timeSprite2.setPosition(410.0f, 618.0f);
            addActor(timeSprite1);
            addActor(timeSprite2);
        }
        GNumSprite timeSprite3 = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN006, this.gameMin + "", "%", -2, 4);
        GNumSprite timeSprite4 = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN006, this.gameSec + "", "%", -2, 4);
        timeSprite3.setPosition(318.0f, 651.0f);
        timeSprite4.setPosition(410.0f, 651.0f);
        addActor(timeSprite3);
        addActor(timeSprite4);
        if (RankData.isNewRecord(this.gameMin, this.gameSec) && !this.isNew) {
            addActor(new Effect().getEffect_Diejia(77, PAK_ASSETS.IMG_PAO002, PAK_ASSETS.IMG_ZANTING004));
            this.isNew = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void addMoney(int x, int y) {
        GNumSprite timeSprite2 = new GNumSprite((int) PAK_ASSETS.IMG_JIESUAN007, "X" + RankData.rankMoney, "X", -2, 0);
        timeSprite2.setPosition((float) x, (float) y);
        addActor(timeSprite2);
    }

    /* access modifiers changed from: package-private */
    public void addButton() {
        ActorImage close = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC005, (int) PAK_ASSETS.IMG_E03, 59, 1, this);
        close.setName("close");
        close.setTouchable(Touchable.enabled);
        final MyImage promote = new MyImage((int) PAK_ASSETS.IMG_JIESUAN008, 38.0f, 929.0f, 0);
        promote.setName("promote");
        promote.setTouchable(Touchable.enabled);
        addActor(promote);
        final MyImage next = new MyImage((int) PAK_ASSETS.IMG_JIESUAN010, 358.0f, 929.0f, 0);
        next.setName("next");
        next.setTouchable(Touchable.enabled);
        addActor(next);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUAN015, (int) PAK_ASSETS.IMG_SHUOMINGZI04, 886, 12, this).addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.5f))));
        if (!Teach.hasTeach[22]) {
            RankData.teach.initTeach(22);
        }
        addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Actor actor = event.getTarget();
                String targetName = actor.getName();
                if ("close".equals(targetName)) {
                    actor.setScale(0.7f);
                    Sound.playButtonClosed();
                } else if ("promote".equals(targetName)) {
                    promote.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN009);
                    Sound.playButtonPressed();
                } else if ("next".equals(targetName)) {
                    next.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN011);
                    Sound.playButtonPressed();
                }
                return super.touchDown(event, x, y, pointer, button);
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                String targetName = event.getTarget().getName();
                if ("close".equals(targetName)) {
                    GameStage.clearAllLayers();
                    Rank.exitRank();
                    RankData.setRankReturnMain(true);
                    GameMain.toScreen(new Mymainmenu2());
                } else if ("promote".equals(targetName)) {
                    promote.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN008);
                    Rank.setRoleUpEvent();
                } else if ("next".equals(targetName)) {
                    next.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN010);
                    Win.this.free();
                    GameStage.clearAllLayers();
                    Rank.exitRank();
                    RankData.setRankReturnMain(true);
                    GameMain.toScreen(new Mymainmenu2());
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void saveData() {
        RankData.awardMoney();
        RankData.saveStar(this.star, this.gameMin, this.gameSec, this.gamePercent);
        RankData.oneBloodThrough();
        RankData.addFullBloodThrough();
        RankData.addEatAll();
        RankData.cheakRankAchieve();
        RankData.cheakFoodAchieve();
        RankData.cheakBattleAchieve();
        RankData.setRoleRankIndex();
        RankData.record.writeRecord(1);
    }

    /* access modifiers changed from: package-private */
    public void playSound_win() {
        if (Rank.role != null) {
            Sound.playSound(sound_win[Rank.role.getRoleIndex()]);
            return;
        }
        Sound.playSound(sound_win[RankData.getSelectRoleIndex()]);
    }

    public void exit() {
    }
}
