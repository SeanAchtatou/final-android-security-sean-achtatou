package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Effect;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class AchieveTip extends MyGroup implements GameConstant {
    public boolean show;
    float time;
    int x = 320;
    int y = PAK_ASSETS.IMG_2X1;

    public void init(String tip, boolean achieve) {
        setTransform(true);
        this.show = false;
        addActor(new Effect().getEffect_Diejia(58, this.x, this.y));
        if (achieve) {
            new ActorImage((int) PAK_ASSETS.IMG_TIP001, this.x, this.y, 1, this);
        } else {
            new ActorImage((int) PAK_ASSETS.IMG_TIP0012, this.x, this.y, 1, this);
        }
        new ActorText(tip, this.x, this.y + 20, 1, this).getLable().setScale(1.2f);
    }

    public void add() {
        GameStage.addActor(this, 5);
        setScale(Animation.CurveTimeline.LINEAR);
        setOrigin((float) this.x, (float) this.y);
        addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.3f), Actions.scaleTo(1.3f, 1.3f, 0.1f), Actions.scaleTo(1.0f, 1.0f, 0.1f), Actions.delay(1.3f), Actions.parallel(Actions.moveBy(Animation.CurveTimeline.LINEAR, -150.0f, 0.3f), Actions.alpha(Animation.CurveTimeline.LINEAR, 0.3f))));
        this.show = true;
    }

    public void run(float delta) {
        if (this.show) {
            this.time += delta;
            if (this.time > 2.3f) {
                free();
            }
        }
    }

    public void init() {
    }

    public void exit() {
    }
}
