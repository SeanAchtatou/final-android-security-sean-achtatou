package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.UI.MySupply;
import com.sg.td.actor.Mask;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class Relive extends MyGroup implements GameConstant {
    static int[] sound_retry = {49, 33, 41, 49};
    Bg bg;
    Mask mask;
    ActorText text;

    public void init() {
        int spinId;
        this.bg = new Bg(320, PAK_ASSETS.IMG_DAJI06A, 5);
        int bgY = this.bg.y;
        ActorImage close = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC005, (int) PAK_ASSETS.IMG_I03, bgY + 8, 1, this);
        close.setName("close");
        close.setTouchable(Touchable.enabled);
        new ActorImage((int) PAK_ASSETS.IMG_FUHUO001, 184, bgY + 43, 12, this);
        if (Rank.role == null) {
            spinId = RankData.getRoleSpineId(RankData.getSelectRoleIndex());
        } else {
            spinId = Rank.role.getSpineId();
        }
        addActor(new RoleSpine(153, PAK_ASSETS.IMG_TIP003, spinId));
        new ActorImage((int) PAK_ASSETS.IMG_FUHUO002, 247, bgY + 180, 12, this);
        GNumSprite numSprite = new GNumSprite((int) PAK_ASSETS.IMG_FUHUO003, "X1", "X", 2, 0);
        numSprite.setPosition(439.0f, (float) (bgY + 204));
        addActor(numSprite);
        this.text = new ActorText("(剩余" + RankData.getHeartNum() + ")", (int) PAK_ASSETS.IMG_JIESUO008, bgY + PAK_ASSETS.IMG_BUFFCHIBANG, 1, this);
        this.text.setWidth(100);
        final MyImage btn = new MyImage((int) PAK_ASSETS.IMG_FUHUO004, 276.0f, 605.0f, 0);
        btn.setTouchable(Touchable.enabled);
        btn.setName("relive");
        addActor(btn);
        GameStage.addActor(this, 4);
        addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                String targetName = event.getTarget().getName();
                if ("relive".equals(targetName)) {
                    btn.setTextureRegion((int) PAK_ASSETS.IMG_FUHUO005);
                    Sound.playButtonPressed();
                } else if ("close".equals(targetName)) {
                    Sound.playButtonClosed();
                }
                return super.touchDown(event, x, y, pointer, button);
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                String targetName = event.getTarget().getName();
                if ("close".equals(targetName)) {
                    Relive.this.free();
                    Rank.setSystemEvent((byte) 18);
                } else if ("relive".equals(targetName)) {
                    btn.setTextureRegion((int) PAK_ASSETS.IMG_FUHUO004);
                    if (RankData.getHeartNum() <= 0) {
                        new MySupply(5);
                        return;
                    }
                    Rank.level.killAllEnemy();
                    RankData.spendHeart(1);
                    Sound.playSound(68);
                    Relive.this.free();
                }
            }
        });
        playSound_retry();
    }

    public void run(float delta) {
        this.text.setText("(剩余" + RankData.getHeartNum() + ")");
    }

    /* access modifiers changed from: package-private */
    public void playSound_retry() {
        if (Rank.role != null) {
            Sound.playSound(sound_retry[Rank.role.getRoleIndex()]);
            return;
        }
        Sound.playSound(sound_retry[RankData.getSelectRoleIndex()]);
    }

    public void exit() {
        this.bg.free();
        Rank.clearSystemEvent();
    }

    public class RoleSpine extends MySpine implements GameConstant {
        MySpine roleSpine;

        public RoleSpine(int x, int y, int spineID) {
            int y2 = y + ((Map.tileHight / 2) - 10);
            init(SPINE_NAME);
            createSpineRole(spineID, 0.8f);
            setMix(0.5f);
            initMotion(motion_leiguman);
            setStatus(6);
            setPosition((float) x, (float) y2);
            setId();
            setAniSpeed(1.0f);
            setFilpX(true);
        }

        public void run(float delta) {
            updata();
            this.index++;
        }

        public void act(float delta) {
            super.act(delta);
            run(delta);
        }
    }
}
