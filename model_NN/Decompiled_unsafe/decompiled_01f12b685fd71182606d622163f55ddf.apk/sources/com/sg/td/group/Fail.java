package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.UI.MySupply;
import com.sg.td.UI.Mymainmenu2;
import com.sg.td.actor.Effect;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class Fail extends MyGroup implements GameConstant {
    public void init() {
        new ActorImage((int) PAK_ASSETS.IMG_SHIBAI, 0, 0, 12, this).setTouchable(Touchable.enabled);
        new Effect().addEffect(87, 320, (int) PAK_ASSETS.IMG_2X1, this);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUAN014, 0, 26, 12, this);
        ActorImage close = new ActorImage((int) PAK_ASSETS.IMG_PUBLIC005, (int) PAK_ASSETS.IMG_E03, 59, 1, this);
        close.setName("close");
        close.setTouchable(Touchable.enabled);
        final MyImage promote = new MyImage((int) PAK_ASSETS.IMG_JIESUAN008, 38.0f, 929.0f, 0);
        addActor(promote);
        promote.setName("promote");
        promote.setTouchable(Touchable.enabled);
        final MyImage replay = new MyImage(443, 358.0f, 929.0f, 0);
        addActor(replay);
        replay.setName("replay");
        replay.setTouchable(Touchable.enabled);
        new ActorImage((int) PAK_ASSETS.IMG_JIESUAN015, (int) PAK_ASSETS.IMG_SHUOMINGZI04, 886, 12, this).addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.5f))));
        GameStage.addActor(this, 4);
        addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Actor actor = event.getTarget();
                String targetName = actor.getName();
                if ("close".equals(targetName)) {
                    actor.setScale(0.7f);
                    Sound.playButtonClosed();
                } else if ("promote".equals(targetName)) {
                    promote.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN009);
                    Sound.playButtonPressed();
                } else if ("replay".equals(targetName)) {
                    replay.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN013);
                    Sound.playButtonPressed();
                }
                return super.touchDown(event, x, y, pointer, button);
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                String targetName = event.getTarget().getName();
                if ("close".equals(targetName)) {
                    GameStage.clearAllLayers();
                    Rank.exitRank();
                    RankData.setRankReturnMain(true);
                    GameMain.toScreen(new Mymainmenu2());
                } else if ("promote".equals(targetName)) {
                    promote.setTextureRegion((int) PAK_ASSETS.IMG_JIESUAN008);
                    Rank.setRoleUpEvent();
                } else if ("replay".equals(targetName)) {
                    replay.setTextureRegion(443);
                    if (!RankData.powerEnough()) {
                        new MySupply(2);
                        return;
                    }
                    Fail.this.free();
                    GameStage.clearAllLayers();
                    GameMain.toScreen(new Rank());
                }
            }
        });
        RankData.consumeProp();
        RankData.consumePower();
        Sound.playSound(53);
        Sound.playSound(65);
    }

    public void exit() {
    }
}
