package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class BossCry extends MyGroup implements GameConstant {
    ActorImage cryImage;
    float time;

    public void init() {
        this.cryImage = new ActorImage(772, Math.min(Map.getBossHomeX() - 5, (int) PAK_ASSETS.IMG_ZHUANPAN005), Map.getBossHomeY() - 92, 10);
        this.cryImage.addAction(Actions.repeat(-1, Actions.sequence(Actions.scaleTo(1.2f, 1.2f, 0.5f), Actions.scaleTo(1.0f, 1.0f, 0.5f))));
        GameStage.addActor(this, 3);
        addActor(this.cryImage);
    }

    public void run(float delta) {
        this.time += delta;
        if (this.time > 5.0f) {
            free();
        }
    }

    public void exit() {
    }
}
