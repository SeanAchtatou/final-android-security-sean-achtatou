package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Mask;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class ReadyGo extends MyGroup implements GameConstant {
    public void init() {
        addActor(new Mask());
        addActor(new Effect().getEffect(34, 320, (int) PAK_ASSETS.IMG_2X1));
        GameStage.addActor(this, 4);
        addAction(Actions.sequence(Actions.delay(2.0f), Actions.run(new Runnable() {
            public void run() {
                ReadyGo.this.free();
            }
        })));
        Sound.playSound(67);
    }

    public void exit() {
        Rank.clearSystemEvent();
        Rank.building.initDefaultTower();
        Rank.rankUI = new RankUI();
        if (Rank.ENDLESS_MODE) {
            Rank.bossUI = new BossUI();
            Sound.playSound(64);
        }
        Rank.setGride();
        Rank.setSystemEvent((byte) 3);
    }
}
