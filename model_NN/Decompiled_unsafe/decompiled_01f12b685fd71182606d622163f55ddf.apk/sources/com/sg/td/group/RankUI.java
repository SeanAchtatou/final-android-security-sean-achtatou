package com.sg.td.group;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Message;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.actor.Effect;
import com.sg.tools.GNumSprite;
import com.sg.tools.GameTime;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.tools.MySprite;
import com.sg.util.GameStage;

public class RankUI extends MyGroup implements GameConstant {
    ActorImage bgImage;
    int bx;
    int by;
    int curState = -1;
    boolean drawTip;
    Group effectGroup;
    ActorImage endlessImage;
    public boolean freeUse;
    int lastState = -1;
    int lastTime;
    int length2;
    int length3;
    GNumSprite moenySprite;
    int num;
    boolean pause = true;
    MyImage pauseButton;
    ActorImage pauseImage;
    int roleIndex;
    int saveLastTime;
    int saveStarNum;
    boolean saveStarVisible2;
    boolean saveStarVisible3;
    Group skillGroup;
    GNumSprite skillNumSprite;
    public boolean soundTip;
    ActorImage starActor2;
    ActorImage starActor3;
    public int starNum;
    public int starTime2;
    public int starTime3;
    public boolean starVisible2;
    public boolean starVisible3;
    int starX2;
    int starX3;
    int starY;
    private int tempNum;
    int time;
    ActorClipImage timeHp;
    GNumSprite timeSprite;
    int totalLength;
    GNumSprite waveSprite;

    public void init() {
        this.skillGroup = new Group();
        this.effectGroup = new Group();
        this.starNum = 3;
        if (Rank.isEasyMode()) {
            this.starTime3 = Rank.data.starTime[0][1];
            this.starTime2 = Rank.data.starTime[0][0];
        } else {
            this.starTime3 = Rank.data.starTime[1][1];
            this.starTime2 = Rank.data.starTime[1][0];
        }
        this.bx = 134;
        this.by = 1086;
        this.length3 = 150;
        this.length2 = PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING;
        this.totalLength = PAK_ASSETS.IMG_LIGHTD01;
        this.starVisible3 = true;
        this.starVisible2 = true;
        setfreeUse(false);
        GameTime.initGameTime();
        addBottomUI();
        addTopUI();
        addTopButton();
        addTimeBar();
        addStar();
        addSkillNum();
        GameStage.addActor(this, 3);
    }

    public void exit() {
        this.skillGroup.clear();
        this.skillGroup.remove();
        this.effectGroup.clear();
        this.effectGroup.remove();
    }

    /* access modifiers changed from: package-private */
    public void addBottomUI() {
        GameStage.addActor(new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU008, 0, (int) GameConstant.SCREEN_HEIGHT, 10), 1);
        final MyImage midMenu = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU010, 5.0f, 1035.0f, 0);
        addActor(midMenu);
        midMenu.setTouchable(Touchable.enabled);
        midMenu.setName("mid");
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("name:" + event.getTarget().getName());
                if (!"mid".equals(event.getTarget().getName())) {
                    return true;
                }
                midMenu.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU011);
                Rank.clearSystemEvent();
                Rank.setSystemEvent((byte) 9);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ("mid".equals(event.getTarget().getName())) {
                    midMenu.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU010);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void addTimeBar() {
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU012, this.bx, this.by, this);
        this.timeHp = new ActorClipImage((int) PAK_ASSETS.IMG_ZHANDOU013, this.bx, this.by + 1, this);
        this.timeHp.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.totalLength, 25.0f);
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU046, 149, 1050, this);
        this.timeSprite = new GNumSprite((int) PAK_ASSETS.IMG_ZHANDOU047, "00:00", ":", -2, 0);
        this.timeSprite.setPosition(402.0f, 1053.0f);
        addActor(this.timeSprite);
    }

    /* access modifiers changed from: package-private */
    public void addStar() {
        this.starY = this.by + 10;
        ActorImage starActor1 = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU014, this.bx, this.starY, 1, this);
        starActor1.setScale(Animation.CurveTimeline.LINEAR);
        this.starX3 = this.bx + (this.totalLength - this.length3);
        this.starActor3 = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU014, this.starX3, this.starY, 1, this);
        this.starActor3.setScale(Animation.CurveTimeline.LINEAR);
        this.starX2 = this.bx + (this.totalLength - this.length2);
        this.starActor2 = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU014, this.starX2, this.starY, 1, this);
        this.starActor2.setScale(Animation.CurveTimeline.LINEAR);
        starActor1.addAction(Actions.scaleTo(1.0f, 1.0f, 0.3f));
        this.starActor2.addAction(Actions.scaleTo(1.0f, 1.0f, 0.3f));
        this.starActor3.addAction(Actions.scaleTo(1.0f, 1.0f, 0.3f));
    }

    /* access modifiers changed from: package-private */
    public void resetStarPosition() {
        this.starActor3.setPosition((float) this.starX3, (float) this.starY, 1);
        this.starActor2.setPosition((float) this.starX2, (float) this.starY, 1);
    }

    /* access modifiers changed from: package-private */
    public void addTopUI() {
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU001, -3, 15, 12, this);
        this.pauseImage = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU044, (int) PAK_ASSETS.IMG_READYGO01, 55, 1);
        this.moenySprite = new GNumSprite((int) PAK_ASSETS.IMG_ZHANDOU070, Rank.money + "", (String) null, -2, 4);
        this.moenySprite.setOrigin(this.moenySprite.getNumImageWidth() / 2.0f, this.moenySprite.getHeight() / 2.0f);
        this.moenySprite.setPosition(124.0f, 52.0f);
        addActor(this.moenySprite);
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU016, 200, 15, 12, this);
        if (Rank.ENDLESS_MODE) {
            this.endlessImage = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU045, (int) PAK_ASSETS.IMG_DAJI05, 37, 12, this);
            return;
        }
        this.waveSprite = new GNumSprite((int) PAK_ASSETS.IMG_ZHANDOU043, Rank.level.showWave + "/" + Rank.level.waveMax, "/", -2, 4);
        this.waveSprite.setPosition(332.0f, 55.0f);
        addActor(this.waveSprite);
    }

    public void run() {
        runTop();
        runTime();
        runSkillIcon();
        runSkillNum();
    }

    /* access modifiers changed from: package-private */
    public void runTop() {
        if (this.moenySprite != null) {
            this.moenySprite.setNum(Rank.money + "");
        }
        runInfernoTaskNum();
        if (!Rank.isPause() && this.waveSprite != null) {
            this.waveSprite.setNum(Rank.level.showWave + "/" + Rank.level.waveMax);
        }
    }

    public void runPause() {
        if (this.pause != Rank.isPause()) {
            this.pause = Rank.isPause();
            if (this.pause) {
                addImage();
                this.pauseButton.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU017);
                return;
            }
            removeImage();
            this.pauseButton.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU004);
        }
    }

    public void runTime() {
        if (!Rank.isPause()) {
            this.time = GameTime.getGameTimeSeconds();
            this.timeSprite.setNum((GameTime.getGameTime_min() >= 10 ? "" : "0") + GameTime.getGameTime_min() + ":" + (GameTime.getGameTime_sec() >= 10 ? "" : "0") + GameTime.getGameTime_sec());
            if (this.starNum == 3) {
                int moveX = (this.length3 * this.time) / this.starTime3;
                this.timeHp.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) (this.totalLength - moveX), 25.0f);
                if (moveX >= this.length3) {
                    moveStar(this.starActor3);
                    this.starVisible3 = false;
                }
            } else if (this.starNum == 2) {
                int moveX2 = ((this.length2 - this.length3) * (this.time - this.lastTime)) / (this.starTime2 - this.starTime3);
                this.timeHp.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) ((this.totalLength - this.length3) - moveX2), 25.0f);
                if (this.length3 + moveX2 >= this.length2) {
                    moveStar(this.starActor2);
                    this.starVisible2 = false;
                }
            } else if (this.starNum == 1) {
                this.timeHp.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) Math.max(0, (this.totalLength - this.length2) - (((this.totalLength - this.length2) * (this.time - this.lastTime)) / this.starTime3)), 25.0f);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void moveStar(final ActorImage img) {
        img.addAction(Actions.sequence(Actions.moveTo(320.0f, 568.0f, 0.2f), Actions.delay(0.3f), Actions.run(new Runnable() {
            public void run() {
                img.setVisible(false);
            }
        })));
        this.starNum--;
        this.lastTime = this.time;
        System.out.println("星星数目减少：" + this.starNum);
    }

    /* access modifiers changed from: package-private */
    public void removeStar(ActorImage img) {
        removeActor(img);
    }

    /* access modifiers changed from: package-private */
    public void addTopButton() {
        final MyImage speedImage = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU019, 421.0f, 10.0f, 0);
        this.pauseButton = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU004, 496.0f, 10.0f, 0);
        final MyImage retryImage = new MyImage(640, 570.0f, 10.0f, 0);
        addActor(speedImage);
        addActor(this.pauseButton);
        addActor(retryImage);
        speedImage.setTouchable(Touchable.enabled);
        this.pauseButton.setTouchable(Touchable.enabled);
        retryImage.setTouchable(Touchable.enabled);
        speedImage.setName("speed");
        this.pauseButton.setName("pause");
        retryImage.setName("retry");
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                if ("pause".equals(event.getTarget().getName())) {
                    if (Rank.isPause()) {
                        RankUI.this.pauseButton.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU004);
                    } else {
                        RankUI.this.pauseButton.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU017);
                    }
                }
                if ("speed".equals(event.getTarget().getName())) {
                    if (Rank.getGameSpeed() == 1) {
                        speedImage.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU006);
                    } else {
                        speedImage.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU019);
                    }
                }
                if ("retry".equals(event.getTarget().getName())) {
                    retryImage.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU003);
                    if (RankData.getEndlessReplayDays() > 0) {
                        Rank.clearSystemEvent();
                        Rank.setSystemEvent((byte) 8);
                    } else {
                        Rank.clearSystemEvent();
                        Rank.setSystemEvent((byte) 16);
                    }
                }
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ("pause".equals(event.getTarget().getName())) {
                    if (Rank.isPause()) {
                        RankUI.this.pauseButton.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU004);
                        Rank.setPause(false);
                        Rank.killPause(false);
                        RankUI.this.removeImage();
                    } else {
                        Message.showAD();
                        RankUI.this.pauseButton.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU017);
                        Rank.setPause(true);
                        Rank.killPause(true);
                        RankUI.this.addImage();
                    }
                }
                if ("speed".equals(event.getTarget().getName())) {
                    if (Rank.getGameSpeed() == 1) {
                        Rank.setGameSpeed((byte) 2);
                        speedImage.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU006);
                    } else {
                        Rank.setGameSpeed((byte) 1);
                        speedImage.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU019);
                    }
                }
                if ("retry".equals(event.getTarget().getName())) {
                    retryImage.setTextureRegion(640);
                }
            }
        });
    }

    public void addImage() {
        if (Rank.ENDLESS_MODE) {
            removeActor(this.endlessImage);
        } else {
            removeActor(this.waveSprite);
        }
        addActor(this.pauseImage);
    }

    public void removeImage() {
        removeActor(this.pauseImage);
        if (Rank.ENDLESS_MODE) {
            addActor(this.endlessImage);
        } else {
            addActor(this.waveSprite);
        }
    }

    /* access modifiers changed from: package-private */
    public void runSkillIcon() {
        int num2 = RankData.getHoneyNum();
        if (Rank.role != null && num2 != 0) {
            this.curState = 0;
        } else if (Rank.role == null) {
            this.curState = 1;
        } else {
            this.curState = 2;
        }
        if (this.freeUse) {
            this.curState = 3;
        }
        if (this.curState != this.lastState) {
            addSkillIcon();
            this.lastState = this.curState;
        }
        if (!(Rank.role == null || this.roleIndex == Rank.role.getRoleIndex())) {
            this.roleIndex = Rank.role.getRoleIndex();
            addSkillIcon();
        }
        drawTipUse(PAK_ASSETS.IMG_E02A, 1082, this.effectGroup);
    }

    /* access modifiers changed from: package-private */
    public void addSkillIcon() {
        int iconIndex;
        if (this.curState == 0 || this.curState == 3) {
            this.skillGroup.clear();
            MyImage icon = new MyImage(getSkillImageIndex(), (float) PAK_ASSETS.IMG_GONGGAO011, (float) 1023, 0);
            icon.setTouchable(Touchable.enabled);
            icon.setName("skill");
            this.skillGroup.addActor(icon);
            drawFreeUse();
            this.skillGroup.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if ("skill".equals(event.getTarget().getName())) {
                        if (RankUI.this.freeUse) {
                            Rank.putSkill_free();
                            RankUI.this.setfreeUse(false);
                        } else {
                            Rank.putSkill(true);
                        }
                    }
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if ("skill".equals(event.getTarget().getName())) {
                    }
                }
            });
        } else {
            this.skillGroup.clear();
            MyImage icon2 = new MyImage(getSkillImageIndex_hui(), (float) PAK_ASSETS.IMG_GONGGAO011, (float) 1023, 0);
            icon2.setName("buySkill");
            this.skillGroup.addActor(icon2);
            if (this.curState == 1) {
                iconIndex = PAK_ASSETS.IMG_ZHANDOU057;
            } else {
                iconIndex = PAK_ASSETS.IMG_ZHANDOU062;
                icon2.setTouchable(Touchable.enabled);
            }
            MyImage icon22 = new MyImage(iconIndex, (float) PAK_ASSETS.IMG_E01, (float) 1079, 4);
            icon22.addAction(Actions.repeat(-1, Actions.sequence(Actions.alpha(0.3f, 0.5f), Actions.alpha(1.0f, 0.5f))));
            this.skillGroup.addActor(icon22);
            this.skillGroup.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (!"buySkill".equals(event.getTarget().getName())) {
                        return true;
                    }
                    Rank.setSystemEvent((byte) 15);
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if ("buySkill".equals(event.getTarget().getName())) {
                    }
                }
            });
        }
        GameStage.addActor(this.skillGroup, 2);
    }

    /* access modifiers changed from: package-private */
    public void drawFreeUse() {
        if (this.freeUse) {
            new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU060, (int) PAK_ASSETS.IMG_PUBLIC014, 989, 1, this.skillGroup).addAction(Actions.repeat(-1, Actions.sequence(Actions.scaleTo(1.1f, 1.1f, 0.5f), Actions.scaleTo(1.0f, 1.0f, 0.5f))));
        }
    }

    /* access modifiers changed from: package-private */
    public void drawTipUse(int x, int y, Group group) {
        if (this.curState != 0 && this.curState != 3 && this.curState != 2) {
            group.clear();
        } else if (!Rank.level.isPassPath_2()) {
            this.drawTip = false;
            group.clear();
        } else if (!this.drawTip) {
            new Effect().addEffect(9, x, y, group);
            if (this.curState == 0 || this.curState == 2) {
                drawFigure(x, y, true, group);
            }
            GameStage.addActor(group, 3);
            if (!this.soundTip) {
                Sound.playSound(23);
                this.soundTip = true;
            }
            this.drawTip = true;
        }
    }

    public void resetSoundTip() {
        this.soundTip = false;
    }

    /* access modifiers changed from: package-private */
    public void drawFigure(int x, int y, boolean trans, Group group) {
        int y2 = y - 30;
        new Effect().addEffect_Diejia(89, x, y2, group);
        MySprite sprite = new MySprite(ANIMATION_NAME[0] + ".json", "dianji");
        sprite.setPosition((float) x, (float) y2);
        sprite.setTouchable(Touchable.disabled);
        if (trans) {
            sprite.addAction(Actions.rotateBy(220.0f));
        }
        group.addActor(sprite);
    }

    /* access modifiers changed from: package-private */
    public void addSkillNum() {
        int num2 = RankData.getHoneyNum();
        this.bgImage = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU058, (int) PAK_ASSETS.IMG_TIP003, 1034, 1, this);
        this.skillNumSprite = new GNumSprite((int) PAK_ASSETS.IMG_ZHANDOU059, num2 + "", (String) null, -4, 4);
        this.skillNumSprite.setPosition((float) PAK_ASSETS.IMG_TIP003, (float) 1034);
        addActor(this.skillNumSprite);
    }

    /* access modifiers changed from: package-private */
    public void runSkillNum() {
        int num2 = Math.min(99, RankData.getHoneyNum());
        if (this.freeUse || num2 == 0) {
            this.skillNumSprite.setNum((String) null);
            this.bgImage.setVisible(false);
            return;
        }
        this.skillNumSprite.setNum(num2 + "");
        this.bgImage.setVisible(true);
    }

    /* access modifiers changed from: package-private */
    public int getSkillImageIndex() {
        if (Rank.role == null) {
            return PAK_ASSETS.IMG_ZHANDOU048;
        }
        switch (Rank.role.getRoleIndex()) {
            case 0:
                return PAK_ASSETS.IMG_ZHANDOU048;
            case 1:
                return PAK_ASSETS.IMG_ZHANDOU049;
            case 2:
                return PAK_ASSETS.IMG_ZHANDOU050;
            default:
                return PAK_ASSETS.IMG_ZHANDOU051;
        }
    }

    /* access modifiers changed from: package-private */
    public int getSkillImageIndex_hui() {
        if (Rank.role == null) {
            return PAK_ASSETS.IMG_ZHANDOU053;
        }
        switch (Rank.role.getRoleIndex()) {
            case 0:
                return PAK_ASSETS.IMG_ZHANDOU053;
            case 1:
                return PAK_ASSETS.IMG_ZHANDOU054;
            case 2:
                return PAK_ASSETS.IMG_ZHANDOU055;
            default:
                return PAK_ASSETS.IMG_ZHANDOU056;
        }
    }

    private void runInfernoTaskNum() {
        boolean isadd = false;
        if (Rank.money != this.tempNum) {
            isadd = true;
        }
        this.tempNum = Rank.money;
        if (isadd && this.moenySprite.getActions().size == 0) {
            this.moenySprite.clearActions();
            this.moenySprite.setScale(1.0f, 1.0f);
            this.moenySprite.addAction(Actions.sequence(Actions.scaleTo(1.5f, 1.5f, 0.2f, Interpolation.swingOut), Actions.scaleTo(1.0f, 1.0f, Animation.CurveTimeline.LINEAR)));
        }
    }

    public void setfreeUse(boolean free) {
        this.freeUse = free;
    }

    public void addShake() {
        this.skillGroup.addAction(Actions.repeat(1, Actions.sequence(Actions.moveBy((float) 6, (float) 6, 0.015f, Interpolation.bounceOut), Actions.moveBy((float) (-6), (float) (-6), 0.015f, Interpolation.bounceOut), Actions.moveBy((float) (-6), (float) (-6), 0.015f, Interpolation.bounceOut), Actions.moveBy((float) 6, (float) 6, 0.015f, Interpolation.bounceOut))));
    }

    public void saveStar() {
        this.saveStarNum = this.starNum;
        this.saveLastTime = this.lastTime;
        this.saveStarVisible3 = this.starVisible3;
        this.saveStarVisible2 = this.starVisible2;
        System.out.println("saveStar starNum:" + this.starNum + "  lastTime:" + this.lastTime + "  starVisible3:" + this.starVisible3 + "  starVisible2:" + this.starVisible2);
    }

    public void resetStar() {
        this.starVisible3 = this.saveStarVisible3;
        this.starVisible2 = this.saveStarVisible2;
        this.starActor3.setVisible(this.starVisible3);
        this.starActor2.setVisible(this.starVisible2);
        resetStarPosition();
        this.starNum = this.saveStarNum;
        this.lastTime = this.saveLastTime;
    }
}
