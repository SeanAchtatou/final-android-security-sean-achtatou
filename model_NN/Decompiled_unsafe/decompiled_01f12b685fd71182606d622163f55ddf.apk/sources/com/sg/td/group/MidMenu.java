package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kbz.Actors.ActorText;
import com.kbz.Actors.SimpleButton;
import com.sg.GameEntry.GameMain;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.UI.MyGift;
import com.sg.td.UI.MySupply;
import com.sg.td.UI.Mymainmenu2;
import com.sg.td.message.GMessage;
import com.sg.td.message.GameSpecial;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class MidMenu extends MyGroup implements GameConstant {
    Bg bg;
    int endlessDays;
    boolean endlessPower;
    int powerNum;
    GNumSprite powerNumSprite;
    int x;
    int y;

    public void init() {
        this.bg = new Bg(320, 186, 5);
        this.x = 181;
        this.y = PAK_ASSETS.IMG_QIANDAITUBIAO_SHUOMING;
        this.powerNum = RankData.getPowerNum();
        addMidButton(this.x, this.y);
        GameStage.addActor(this, 4);
        if (GameSpecial.atuoPop) {
            new MyGift(GMessage.PP_TEMPgift);
        }
    }

    /* access modifiers changed from: package-private */
    public void addMidButton(int x2, int y2) {
        if (RankData.getEndlessPowerDays() > 0) {
            this.endlessPower = true;
            this.endlessDays = RankData.getEndlessPowerDays();
        }
        final MyImage continueImage = new MyImage((int) PAK_ASSETS.IMG_ZANTING002, (float) x2, (float) y2, 0);
        final MyImage newgameImage = new MyImage((int) PAK_ASSETS.IMG_ZANTING006, (float) x2, (float) (y2 + 102), 0);
        MyImage icon = new MyImage((int) PAK_ASSETS.IMG_ZANTING008, (float) (x2 + 250), (float) (y2 + 102 + 46), 4);
        final MyImage backImage = new MyImage((int) PAK_ASSETS.IMG_ZANTING004, (float) x2, (float) (y2 + 204), 0);
        MyImage icon2 = new MyImage((int) PAK_ASSETS.IMG_ZANTING008, (float) (x2 + 250), (float) (y2 + 204 + 46), 4);
        MyImage icon3 = new MyImage((int) PAK_ASSETS.IMG_ZANTING001, (float) (x2 - 125), (float) (y2 + PAK_ASSETS.IMG_BUFF_JINBI), 0);
        MyImage endlessImage = new MyImage((int) PAK_ASSETS.IMG_CHUANGGUAN007, (float) (x2 + 22), (float) (y2 + PAK_ASSETS.IMG_PPSG04), 4);
        this.powerNumSprite = new GNumSprite((int) PAK_ASSETS.IMG_CHUANGGUAN009, this.powerNum + "", (String) null, -2, 4);
        this.powerNumSprite.setPosition(191.0f, 558.0f);
        MyImage endlessDaysImage = new MyImage((int) PAK_ASSETS.IMG_CHUANGGUAN008, (float) (x2 + PAK_ASSETS.IMG_DZ_PINGGUOZHADAN), (float) (y2 + PAK_ASSETS.IMG_ICESG05), 0);
        GNumSprite daysSprite = new GNumSprite((int) PAK_ASSETS.IMG_CHUANGGUAN009, this.endlessDays + "", (String) null, -2, 0);
        daysSprite.setPosition((float) (x2 + PAK_ASSETS.IMG_DZ_PINGGUOZHADAN + 125), (float) (y2 + PAK_ASSETS.IMG_KAWEILI04));
        addActor(continueImage);
        addActor(newgameImage);
        addActor(backImage);
        addActor(icon3);
        if (!this.endlessPower) {
            if (Rank.isConsumePower()) {
                addActor(icon);
                addActor(icon2);
            }
            addActor(this.powerNumSprite);
        } else {
            addActor(endlessImage);
            addActor(endlessDaysImage);
            addActor(daysSprite);
        }
        continueImage.setTouchable(Touchable.enabled);
        newgameImage.setTouchable(Touchable.enabled);
        backImage.setTouchable(Touchable.enabled);
        continueImage.setName("continue");
        newgameImage.setName("new");
        backImage.setName("back");
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                if ("continue".equals(event.getTarget().getName())) {
                    continueImage.setTextureRegion((int) PAK_ASSETS.IMG_ZANTING003);
                }
                if ("new".equals(event.getTarget().getName())) {
                    newgameImage.setTextureRegion((int) PAK_ASSETS.IMG_ZANTING007);
                }
                if (!"back".equals(event.getTarget().getName())) {
                    return true;
                }
                backImage.setTextureRegion((int) PAK_ASSETS.IMG_ZANTING005);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ("continue".equals(event.getTarget().getName())) {
                    continueImage.setTextureRegion((int) PAK_ASSETS.IMG_ZANTING002);
                    MidMenu.this.free();
                }
                if ("new".equals(event.getTarget().getName())) {
                    newgameImage.setTextureRegion((int) PAK_ASSETS.IMG_ZANTING006);
                    if (!RankData.powerEnough()) {
                        new MySupply(2);
                        return;
                    }
                    if (Rank.isConsumePower()) {
                        RankData.consumeProp();
                        RankData.consumePower();
                    }
                    MidMenu.this.free();
                    GameStage.clearAllLayers();
                    GameMain.toScreen(new Rank());
                }
                if ("back".equals(event.getTarget().getName())) {
                    backImage.setTextureRegion((int) PAK_ASSETS.IMG_ZANTING004);
                    MidMenu.this.exitMid();
                    MidMenu.this.secondSrueBack();
                }
            }
        });
    }

    public void secondSrueBack() {
        this.bg = new Bg(320, 186, 3);
        GameStage.addActor(this, 4);
        ActorText interformat = new ActorText("是否返回主界面？", 249, (int) PAK_ASSETS.IMG_DAJI03, 1, this);
        interformat.setWidth(300);
        interformat.setFontScaleXY(1.5f);
        SimpleButton sure = new SimpleButton(50, PAK_ASSETS.IMG_SHENGJITIPS04, 1, new int[]{PAK_ASSETS.IMG_TIP011, PAK_ASSETS.IMG_TIP013});
        SimpleButton quxiao = new SimpleButton(PAK_ASSETS.IMG_TIPS04, PAK_ASSETS.IMG_SHENGJITIPS04, 1, new int[]{PAK_ASSETS.IMG_TIP012, PAK_ASSETS.IMG_TIP014});
        addActor(sure);
        addActor(quxiao);
        sure.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (!RankData.powerEnough()) {
                    new MySupply(2);
                    return;
                }
                if (Rank.isConsumePower()) {
                    RankData.consumeProp();
                    RankData.consumePower();
                }
                MidMenu.this.free();
                GameStage.clearAllLayers();
                Rank.exitRank();
                RankData.setRankReturnMain(true);
                GameMain.toScreen(new Mymainmenu2());
            }
        });
        quxiao.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Sound.playButtonPressed();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                MidMenu.this.free();
            }
        });
    }

    public void run(float delta) {
        if (!this.endlessPower && RankData.getEndlessPowerDays() > 0) {
            clear();
            addMidButton(this.x, this.y);
        }
        this.powerNum = RankData.getPowerNum();
        this.powerNumSprite.setNum(this.powerNum);
    }

    public void exit() {
        Rank.clearSystemEvent();
        this.bg.free();
        Rank.killPause(false);
    }

    public void exitMid() {
        this.bg.free();
        remove();
        clear();
    }
}
