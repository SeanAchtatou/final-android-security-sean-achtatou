package com.sg.td.group;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.RankData;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class ShowProp extends MyGroup implements GameConstant {
    MyImage bgActorImage;
    Group propGroup;

    public void init() {
        int startX;
        setTransform(true);
        this.propGroup = new Group();
        int[] imageIndex = {133, 132, 130, 131};
        int num = RankData.getUsePropNum();
        if (num != 0) {
            this.bgActorImage = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU061, (float) 136, (float) PAK_ASSETS.IMG_MAP1, 0);
            addActor(this.bgActorImage);
            if (num % 2 == 0) {
                startX = 320 - ((num / 2) * 80);
            } else {
                startX = 320 - (((num / 2) * 80) + 40);
            }
            int j = 0;
            for (int i = 0; i < RankData.props_use.length; i++) {
                if (RankData.props_use[i]) {
                    ActorImage actorImage = new ActorImage(imageIndex[i], (j * 80) + startX + 40, (int) PAK_ASSETS.IMG_A02, 1);
                    actorImage.setScale(0.6f);
                    this.propGroup.addActor(actorImage);
                    j++;
                }
            }
            addActor(this.propGroup);
            GameStage.addActor(this, 4);
            setScale(Animation.CurveTimeline.LINEAR);
            setOrigin(320.0f, (float) PAK_ASSETS.IMG_2X1);
            addAction(Actions.sequence(Actions.scaleTo(1.3f, 1.3f, 0.5f), Actions.scaleTo(1.0f, 1.0f, 0.1f), Actions.delay(0.7f), Actions.run(new Runnable() {
                public void run() {
                    ShowProp.this.addDown();
                }
            }), Actions.parallel(Actions.scaleTo(0.6f, 0.6f, 0.5f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 412.0f, 0.5f))));
        }
    }

    /* access modifiers changed from: package-private */
    public void addDown() {
        removeActor(this.bgActorImage);
    }

    public void exit() {
    }
}
