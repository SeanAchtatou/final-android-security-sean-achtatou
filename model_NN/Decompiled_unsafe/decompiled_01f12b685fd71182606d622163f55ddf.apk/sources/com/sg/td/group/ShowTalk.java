package com.sg.td.group;

import com.kbz.Actors.ActorImage;
import com.kbz.Actors.ActorText;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.actor.Mask;
import com.sg.td.data.TowerData;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class ShowTalk extends MyGroup implements GameConstant {
    int lastTalkIndex;
    int talkIndex;
    TowerData.Talk[] talks;

    public void init() {
        this.talks = Rank.talks;
        this.talkIndex = 0;
        this.lastTalkIndex = 0;
        add();
    }

    /* access modifiers changed from: package-private */
    public void add() {
        ActorText text;
        clear();
        int who = this.talks[this.talkIndex].getWho();
        StringBuffer info = new StringBuffer(this.talks[this.talkIndex].getContent());
        if (info.length() > 18) {
            info.insert(18, "\n");
        }
        if (info.length() > 36) {
            info.insert(36, "\n");
        }
        String content = info.toString().replace("#", "[RED]").replace("%", "[]").replace("]\n", "] \n");
        addActor(new Mask());
        new ActorImage(773, 320, (int) PAK_ASSETS.IMG_2X1, 1, this);
        if (this.talkIndex % 2 == 0) {
            new ActorImage(getImageIndex(who), 2, (int) PAK_ASSETS.IMG_ZANTING001, 10, this);
            text = new ActorText(content, 156, (int) PAK_ASSETS.IMG_ONLINE004, 12, this);
        } else {
            new ActorImage(getImageIndex(who), (int) PAK_ASSETS.IMG_ZANTING008, (int) PAK_ASSETS.IMG_ZANTING001, 18, this);
            text = new ActorText(content, 53, (int) PAK_ASSETS.IMG_ONLINE004, 12, this);
        }
        text.setFontScaleXY(1.3f);
        text.setWrap(false);
        text.setWidth(PAK_ASSETS.IMG_JIESUO002);
        GameStage.addActor(this, 3);
    }

    public void touchTalk() {
        this.talkIndex++;
        if (this.talkIndex >= this.talks.length) {
            free();
        } else if (this.talkIndex != this.lastTalkIndex) {
            add();
        }
    }

    /* access modifiers changed from: package-private */
    public int getImageIndex(int who) {
        switch (who) {
            case 0:
                return PAK_ASSETS.IMG_JIAOXUE002;
            case 1:
                return PAK_ASSETS.IMG_JIAOXUE003;
            case 2:
                return PAK_ASSETS.IMG_JIAOXUE004;
            case 3:
                return PAK_ASSETS.IMG_JIAOXUE005;
            default:
                return PAK_ASSETS.IMG_JIAOXUE002;
        }
    }

    public void exit() {
        Rank.clearSystemEvent();
        Rank.setSystemEvent((byte) 7);
    }
}
