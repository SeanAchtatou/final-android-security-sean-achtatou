package com.sg.td.group;

import com.kbz.Actors.ActorImage;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.actor.Up;
import com.sg.td.record.LoadJiDi;
import com.sg.tools.MyGroup;
import com.sg.tools.MySprite;
import com.sg.tools.NumActor;
import com.sg.util.GameStage;

public class Home extends MyGroup implements GameConstant {
    public int curBlood;
    MySprite home;
    int initState;
    int lastState;
    int level;
    NumActor life;
    int state;
    int totalBlood;
    Up upIcon;
    int x;
    int y;

    public void init() {
        initState(0);
        this.x = Map.getMyHomeX();
        this.y = Map.getMyHomeY();
        this.home = new MySprite(ANIMATION_NAME[1] + ".json", homeState[0]);
        this.home.setPosition((float) this.x, (float) this.y);
        GameStage.addActor(this.home, 2);
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU028, this.x, this.y, 1, this);
        this.life = new NumActor();
        this.life.setPosition((float) (this.x + 5), (float) this.y);
        this.life.set(PAK_ASSETS.IMG_ZHANDOU029, this.curBlood, 0, 4);
        addActor(this.life);
        initBlood();
        GameStage.addActor(this, 6);
        addUpIcon();
    }

    /* access modifiers changed from: package-private */
    public void addUpIcon() {
        this.upIcon = new Up(this.x, this.y - 100);
        if (this.level < 11) {
            this.upIcon.canSee();
        }
    }

    public int getHomeX() {
        return this.x;
    }

    public int getHomeY() {
        return this.y;
    }

    public boolean isFull() {
        return this.level == 11;
    }

    public void exit() {
    }

    public void resetBlood() {
        this.curBlood = this.totalBlood;
        this.life.changeNum(Math.max(0, this.curBlood));
    }

    public void initBlood() {
        this.level = RankData.jidiLevel;
        int life2 = LoadJiDi.jidiData.get(this.level + "").getLife();
        this.curBlood = life2;
        this.totalBlood = life2;
        this.life.changeNum(Math.max(0, this.curBlood));
    }

    /* access modifiers changed from: package-private */
    public void initState(int state2) {
        this.state = state2;
        this.initState = state2;
        this.lastState = state2;
    }

    public void run() {
        this.state = this.initState;
        if (this.curBlood != this.totalBlood) {
            this.state++;
        }
        if (((double) this.curBlood) <= ((double) this.totalBlood) * 0.4d) {
            this.state++;
        }
        if (((double) this.curBlood) <= ((double) this.totalBlood) * 0.2d) {
            this.state++;
        }
        if (this.state != this.lastState) {
            this.home.changeAnimation(homeState[this.state]);
            this.lastState = this.state;
            Rank.boss.setAnimation_Happy();
        }
        runUpIcon();
        this.life.changeNum(Math.max(0, this.curBlood));
    }

    public void hurtHome(int damage) {
        this.curBlood -= damage;
        this.life.changeNum(Math.max(0, this.curBlood));
        if (this.curBlood <= 0) {
            Rank.setSystemEvent((byte) 4);
        }
    }

    public boolean isHurt() {
        return this.curBlood != this.totalBlood;
    }

    public int getHomeBlood() {
        return this.curBlood;
    }

    public boolean isFullBlood() {
        return this.curBlood == this.totalBlood;
    }

    /* access modifiers changed from: package-private */
    public void runUpIcon() {
        if (isFull()) {
            this.upIcon.canNotSee();
        } else {
            this.upIcon.canSee();
        }
    }
}
