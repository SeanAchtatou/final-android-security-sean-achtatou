package com.sg.td;

import com.kbz.MapData.MapTileLayer;
import com.kbz.spine.MySpine;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Blood;
import com.sg.td.actor.Box;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Ice;
import com.sg.td.data.MonsterData;
import com.sg.util.GameStage;
import java.util.ArrayList;
import java.util.Vector;

public class Boss extends Enemy implements GameConstant {
    int SPACE = 50;
    int aimX;
    int aimY;
    public BossSpine bossSpine;
    ArrayList<Box> boxArray = new ArrayList<>();
    int dir;
    ArrayList<Effect> effectArray = new ArrayList<>();
    ArrayList<Ice> iceArray = new ArrayList<>();
    int initX;
    int initY;
    int moveX;
    int moveY;
    boolean putSkill = false;
    int skillIndex;
    int[] skillType;
    boolean sound;
    int targetX;
    int targetY;
    int towerIndex;
    String type;

    public Boss() {
        this.ADD_Y = 25;
        int bossHomeX = Map.getBossHomeX();
        this.initX = bossHomeX;
        this.x = bossHomeX;
        int bossHomeY = Map.getBossHomeY();
        this.initY = bossHomeY;
        this.y = bossHomeY;
        this.bossSpine = new BossSpine(this.x, this.y + this.ADD_Y, 18);
        GameStage.addActor(this.bossSpine, 2);
        this.curStatus = 2;
        this.blood = new Blood();
        int i = Rank.FortHP;
        this.hpMax = i;
        this.hp = i;
        this.boss = true;
        this.large = true;
        this.visible = true;
        this.sizeNormal = true;
        this.w = 85;
        this.h = 120;
        setHitXY();
        this.offRange = this.w / 2;
    }

    public void init(int[] skillType2, int hp, int speed, MonsterData m) {
        this.skillType = skillType2;
        this.initSpeed = speed;
        this.speed = speed;
        this.curStatus = 4;
        this.putSkill = false;
        this.index = 0;
        this.skillIndex = 0;
        this.dir = 1;
        this.aimX = -100;
        this.aimY = PAK_ASSETS.IMG_2X1;
        setTargetXY();
        Rank.clearFocus();
        this.sound = false;
    }

    /* access modifiers changed from: package-private */
    public void setHitXY() {
        this.hitX = this.x;
        this.hitY = this.y - (Map.tileHight / 3);
    }

    public void move() {
        for (int i = 0; i < this.boxArray.size(); i++) {
            if (this.boxArray.get(i) != null) {
                this.boxArray.get(i).move();
            }
        }
        for (int i2 = 0; i2 < this.iceArray.size(); i2++) {
            if (this.iceArray.get(i2) != null) {
                this.iceArray.get(i2).move();
            }
        }
        if (this.curStatus == 4) {
            if ((this.dir != 1 || this.x < this.SPACE + 640) && (this.dir != 3 || this.x > (-this.SPACE))) {
                move_line_follow();
                putObject();
                setStop();
                setFaceDir();
                return;
            }
            getAimXY();
            if (this.dir == 1) {
                this.dir = 3;
            } else {
                this.dir = 1;
            }
            setTargetXY();
        }
    }

    private void move_line_follow() {
        int a = this.targetX - this.x;
        int b = this.targetY - this.y;
        float r = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (int) ((((float) a) * getStep()) / r);
        }
        if (b != 0) {
            this.moveY = (int) ((((float) b) * getStep()) / r);
        }
        if (Math.abs(a) > Math.abs(this.moveX) || Math.abs(b) > Math.abs(this.moveY)) {
            this.x += this.moveX;
            this.y += this.moveY;
        } else {
            this.x = this.targetX;
            this.y = this.targetY;
        }
        this.bossSpine.setPosition((float) this.x, (float) (this.y + this.ADD_Y));
        if (!this.sound) {
            Sound.playSound(62);
            this.sound = true;
        }
        setHitXY();
    }

    /* access modifiers changed from: package-private */
    public void putObject() {
        if (this.skillIndex >= this.skillType.length || Math.abs(this.x - this.aimX) > 10) {
            return;
        }
        if (this.skillType[this.skillIndex] == 1 && !this.putSkill) {
            Ice ice = new Ice(66, this.x, this.y, this.towerIndex, this.effectArray.size() - 1);
            ice.init(this.aimX, this.aimY, getStep());
            this.iceArray.add(ice);
            this.putSkill = true;
            this.skillIndex++;
            System.out.println("put ice....");
            Sound.playSound(63);
        } else if (this.skillType[this.skillIndex] == 2 && !this.putSkill) {
            Box box = new Box(PAK_ASSETS.IMG_MEN, this.x, this.y, this.skillIndex, this.effectArray.size() - 1);
            box.init(this.aimX, this.aimY, getStep());
            this.boxArray.add(box);
            this.putSkill = true;
            this.skillIndex++;
            System.out.println("put box....");
            Sound.playSound(63);
        }
    }

    /* access modifiers changed from: package-private */
    public void getAimXY() {
        if (this.skillIndex < this.skillType.length) {
            if (this.skillType[this.skillIndex] == 1) {
                this.towerIndex = Rank.building.getRandomTowerIndex();
                if (this.towerIndex != -1) {
                    this.aimX = Rank.tower.get(this.towerIndex).x;
                    this.aimY = Rank.tower.get(this.towerIndex).y;
                    addChuGuaiEffect();
                    this.putSkill = false;
                } else {
                    this.skillIndex++;
                }
            }
            if (this.skillIndex < this.skillType.length && this.skillType[this.skillIndex] == 2) {
                Vector<int[]> xy = Map.getMonsterXY();
                int index = Tools.nextInt(xy.size());
                this.aimX = xy.get(index)[0] + (MapTileLayer.tileWidth / 2) + 40;
                this.aimY = xy.get(index)[1] + (MapTileLayer.tileHight / 2) + 183;
                addChuGuaiEffect();
                this.putSkill = false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addChuGuaiEffect() {
        Effect effect = new Effect();
        effect.addEffect_Diejia(37, this.aimX, this.aimY, 3);
        this.effectArray.add(effect);
    }

    public void removeGuaiEffect(int effectIndex) {
        if (this.effectArray.get(effectIndex) != null) {
            this.effectArray.get(effectIndex).remove_Diejia();
        }
    }

    public void removeGuaiEffect_all() {
        for (int i = 0; i < this.effectArray.size(); i++) {
            if (this.effectArray.get(i) != null) {
                this.effectArray.get(i).remove_Diejia();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setTargetXY() {
        if (this.skillIndex == this.skillType.length) {
            this.targetX = this.initX;
            this.targetY = this.initY;
        } else if (this.dir == 1) {
            this.targetX = this.SPACE + 640;
            this.targetY = this.y + Tools.nextInt(-this.SPACE, this.SPACE);
        } else {
            this.targetX = -this.SPACE;
            this.targetY = this.aimY + Tools.nextInt(-this.SPACE, this.SPACE);
        }
    }

    /* access modifiers changed from: package-private */
    public void setStop() {
        if (this.skillIndex >= this.skillType.length && this.x == this.initX && this.y == this.initY) {
            this.curStatus = 2;
            flip(false, false);
        }
    }

    public boolean isStop() {
        return this.curStatus == 2;
    }

    public boolean isFly() {
        return this.curStatus == 4;
    }

    public void setAnimation_Stop() {
        this.bossSpine.setStatus(1);
    }

    public void setAnimation_PutEnemy() {
        this.bossSpine.setStatus(4);
        Sound.playSound(66);
    }

    public void setAnimation_Happy() {
        this.bossSpine.setStatus(3);
        Sound.playSound(66);
    }

    public void setAnimation_Angry() {
        this.bossSpine.setStatus(101);
    }

    public void setNotSee() {
        this.bossSpine.setVisible(false);
    }

    public void setSee() {
        this.bossSpine.setVisible(true);
    }

    /* access modifiers changed from: package-private */
    public void setFaceDir() {
        switch (this.dir) {
            case 1:
            case 4:
            case 6:
                this.bossSpine.setFilpX(true);
                return;
            case 2:
            case 3:
            case 5:
            case 7:
                this.bossSpine.setFilpX(false);
                return;
            default:
                return;
        }
    }

    public void setPosition_boss() {
        int bossHomeX = Map.getBossHomeX();
        this.initX = bossHomeX;
        this.x = bossHomeX;
        int bossHomeY = Map.getBossHomeY();
        this.initY = bossHomeY;
        this.y = bossHomeY;
        this.bossSpine.setPosition((float) this.x, (float) (this.y + this.ADD_Y));
    }

    /* access modifiers changed from: package-private */
    public void removeBoss() {
        if (this.hp <= 0) {
            GameStage.removeActor(this.bossSpine);
        }
    }

    public static class BossSpine extends MySpine implements GameConstant {
        public BossSpine(int x, int y, int spineID) {
            init(SPINE_NAME);
            createSpineRole(spineID, 1.0f);
            setMix(0.3f);
            initMotion(motion_boss);
            setStatus(1);
            setPosition((float) x, (float) y);
            setId();
            setAniSpeed(1.0f);
        }

        public void run(float delta) {
            updata();
            this.index++;
        }

        public void act(float delta) {
            super.act(delta);
            run(delta);
        }
    }
}
