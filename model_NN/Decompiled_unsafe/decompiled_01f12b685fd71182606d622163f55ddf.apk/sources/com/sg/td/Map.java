package com.sg.td;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.MapData.MapTileLayer;
import com.kbz.MapData.tmxMap;
import com.sg.GameSprites.GameSprite;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.actor.Gride;
import com.sg.util.GameStage;
import java.lang.reflect.Array;

public class Map extends tmxMap implements GameConstant {
    public static final int MapOffsetX = 40;
    public static final int MapOffsetY = 183;
    protected static Group group;
    public static int[][] path;
    GameSprite sprite;

    public static Group getMapTileGroup() {
        group.setX(group.getX() + 40.0f);
        group.setY(group.getY() + 183.0f);
        return group;
    }

    public static int getMyHomeX() {
        return path[path.length - 1][0];
    }

    public static int getMyHomeY() {
        return path[path.length - 1][1];
    }

    public static int getBossHomeX() {
        return ((int) getHomeX()) + (MapTileLayer.tileWidth / 2) + 40;
    }

    public static int getBossHomeY() {
        return ((int) getHomeY()) + (MapTileLayer.tileHight / 2) + 183;
    }

    public Map(int tmxName) {
    }

    public Map(String tmxName) {
        group = new Group();
        setGroup(group);
        GameStage.addActor(new ActorImage(PAK_ASSETS.IMG_IMAGE01), 0);
        MapTileLayer.setMapImagePath("maptiles");
        initTmxMap(tmxName);
        GameStage.addActor(getMapTileGroup(), 1);
    }

    public void init() {
        addCloud();
        setPath(getPolyline());
    }

    /* access modifiers changed from: package-private */
    public void addCloud() {
        for (int i = 0; i < this.cloudData.size(); i++) {
            MapTileLayer.Cloud c = (MapTileLayer.Cloud) this.cloudData.get(i);
            String type = c.getType();
            int x = c.getX();
            int y = c.getY();
            if ("Cloud_11".equals(type)) {
                new ActorImage((int) PAK_ASSETS.IMG_1X1, x + 35, y + 60, 1, group);
            } else if ("Cloud_12".equals(type)) {
                new ActorImage((int) PAK_ASSETS.IMG_1X1, x + 35, (y + 60) - 70, 1, group);
                new ActorImage((int) PAK_ASSETS.IMG_1X1, x + 35, (y + 60) - 35, 1, group);
                new ActorImage((int) PAK_ASSETS.IMG_1X1, x + 35, y + 60, 1, group);
            } else if ("Cloud_21_2".equals(type)) {
                new ActorImage((int) PAK_ASSETS.IMG_2X12, x + 70, y + 70, 1, group);
            } else if ("Cloud_31".equals(type)) {
                new ActorImage((int) PAK_ASSETS.IMG_3X1, x + 105, y + 70, 1, group);
            } else if ("Cloud_41".equals(type)) {
                new ActorImage((int) PAK_ASSETS.IMG_4X1, x + 140, y + 70, 1, group);
            } else if ("Cloud_21".equals(type)) {
                new ActorImage((int) PAK_ASSETS.IMG_2X1, x + 70, y + 70, 1, group);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setPath(float[] pos) {
        path = (int[][]) Array.newInstance(Integer.TYPE, pos.length / 2, 2);
        for (int i = 0; i < path.length; i++) {
            if (i == 0) {
                path[i][0] = getBossHomeX();
                path[i][1] = getBossHomeY();
            } else {
                path[i][0] = (short) ((int) (pos[i * 2] + ((float) (MapTileLayer.tileWidth / 2)) + 40.0f));
                path[i][1] = (short) ((int) (pos[(i * 2) + 1] + ((float) (MapTileLayer.tileHight / 2)) + 183.0f));
            }
        }
    }

    static int getDataIndex(int x, int y) {
        int mapX = x - 40;
        int mapY = y - 183;
        if (mapX < 0 || mapX > getMapWidth() || mapY < 0 || mapY > getMapHight()) {
            return -1;
        }
        return (mapX / getTileWidth()) + ((mapY / getTileHight()) * getWidthNum());
    }

    public static boolean isPhy(int x, int y) {
        return isPhy(getDataIndex(x, y));
    }

    public static boolean isBlock(int x, int y) {
        return isBlock(getDataIndex(x, y));
    }

    public static boolean isRoad(int x, int y) {
        return isRoad(getDataIndex(x, y));
    }

    public static boolean isPhy(int index) {
        if (index < 0 || index > phyData.length) {
            return false;
        }
        if (phyData[index][0] == -1 && phyData[index][1] == -1) {
            return false;
        }
        return true;
    }

    public static boolean isBlock(int index) {
        if (index < 0 || index >= blockData.length) {
            return false;
        }
        if (blockData[index][0] == -1 && blockData[index][1] == -1) {
            return false;
        }
        return true;
    }

    public static boolean isRoad(int index) {
        if (index < 0 || index >= roadData.length) {
            return false;
        }
        if (roadData[index][0] == -1 && roadData[index][1] == -1) {
            return false;
        }
        return true;
    }

    static void addGride(int type) {
        Gride g;
        for (int i = 0; i < Rank.gride.size(); i++) {
            Rank.gride.get(i).remove();
        }
        Rank.gride.removeAllElements();
        for (int i2 = 0; i2 < getHightNum(); i2++) {
            for (int j = 0; j < getWidthNum(); j++) {
                if (!isPhy((getWidthNum() * i2) + j)) {
                    int x = (getTileWidth() * j) + (getTileWidth() / 2) + 40;
                    int y = (getTileHight() * i2) + (getTileHight() / 2) + 183;
                    if (!Rank.isTowerArea(x, y) && !Rank.isFragArea(x, y) && !Rank.isDeckArea(x, y)) {
                        if (type == 0) {
                            g = new Gride(x, y);
                        } else {
                            g = new Gride(x, y, 0);
                        }
                        Rank.gride.add(g);
                    }
                }
            }
        }
    }

    static int getFullTileX(int x) {
        return ((x / tileWidth) * tileWidth) + (tileWidth / 2);
    }

    static int getFullTileY(int y) {
        return ((y / tileHight) * tileHight) + (tileHight / 2);
    }

    public static int getScreenX(int x) {
        return getFullTileX(x - 40) + 40;
    }

    public static int getScreenY(int y) {
        return getFullTileY(y - 183) + 183;
    }

    static boolean isOutMap(int x, int y) {
        if (x <= 40 || x >= getMapWidth() + 40 || y <= 183 || y >= getMapHight() + 183) {
            return true;
        }
        return false;
    }

    public static void shakeScreen(int count, int x, int y) {
        GameStage.getStage().addAction(Actions.repeat(count, Actions.sequence(Actions.moveBy((float) x, (float) y, 0.015f, Interpolation.bounceOut), Actions.moveBy((float) (-x), (float) (-y), 0.015f, Interpolation.bounceOut), Actions.moveBy((float) (-x), (float) (-y), 0.015f, Interpolation.bounceOut), Actions.moveBy((float) x, (float) y, 0.015f, Interpolation.bounceOut))));
    }

    public static void getShake(Stage stage, float x, float y) {
        stage.getRoot().setOrigin(x, y);
        if (stage.getRoot().getActions().size == 0) {
            stage.addAction(Actions.sequence(Actions.moveBy(-10.0f, -10.0f, 0.02f, Interpolation.bounceOut), Actions.moveBy(10.0f, 10.0f, 0.02f, Interpolation.bounceOut)));
        }
    }

    public void clean() {
        GameStage.removeActor(group);
        group.clear();
        getTiledMap().dispose();
    }
}
