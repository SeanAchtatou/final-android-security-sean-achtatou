package com.sg.td.actor;

import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.util.GameStage;

public class Ice extends Box {
    boolean block = false;
    int blockIndex;

    public Ice(int imgName, int x, int y, int blockIndex2, int eftIndex) {
        super(imgName, x, y, 0, eftIndex);
        this.blockIndex = blockIndex2;
    }

    public void move() {
        if (!remove()) {
            super.move_line_follow();
            blockTower();
            if (this.block) {
                removeIce();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void blockTower() {
        if (!this.block && this.x == this.aimX && this.y == this.aimY) {
            Rank.tower.get(this.blockIndex).setBlock(true);
            this.block = true;
            Rank.building.addDeck(this.x - (Map.tileWidth / 2), this.y - (Map.tileHight / 2), "Net", this.blockIndex, false, null, null);
            Rank.boss.removeGuaiEffect(this.tipEffectIndex);
        }
    }

    public void removeIce() {
        GameStage.removeActor(this.image);
        this.remove = true;
    }
}
