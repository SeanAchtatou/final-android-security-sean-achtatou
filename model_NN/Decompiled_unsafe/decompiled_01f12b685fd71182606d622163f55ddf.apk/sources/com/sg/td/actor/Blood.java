package com.sg.td.actor;

import com.badlogic.gdx.graphics.Color;
import com.kbz.Actors.ActorClipImage;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class Blood extends MyGroup implements GameConstant {
    boolean addStage;
    ActorImage bg;
    ActorImage bg_large;
    ActorClipImage blood;
    ActorClipImage blood_large;

    public void init() {
        this.bg = new ActorImage(PAK_ASSETS.IMG_ZHANDOU068);
        this.bg_large = new ActorImage(PAK_ASSETS.IMG_ZHANDOU066);
        this.blood = new ActorClipImage(PAK_ASSETS.IMG_ZHANDOU069, this);
        this.blood_large = new ActorClipImage(PAK_ASSETS.IMG_ZHANDOU067, this);
    }

    public void draw(Color color, int x, int y, float w, boolean large) {
        if (large) {
            if (color == Color.BLACK) {
                this.bg_large.setPosition((float) x, (float) (y - 11));
                addActor(this.bg_large);
            } else {
                this.blood_large.setPosition((float) x, (float) (y - 11));
                this.blood_large.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, w, 11.0f);
                addActor(this.blood_large);
            }
        } else if (color == Color.BLACK) {
            this.bg.setPosition((float) x, (float) (y - 11));
            addActor(this.bg);
        } else {
            this.blood.setPosition((float) x, (float) (y - 11));
            this.blood.setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, w, 11.0f);
            addActor(this.blood);
        }
        canSee(large);
        if (!this.addStage) {
            this.addStage = true;
            GameStage.addActor(this, 3);
        }
    }

    public void canSee(boolean large) {
        if (large) {
            this.bg_large.setVisible(true);
            this.blood_large.setVisible(true);
            return;
        }
        this.bg.setVisible(true);
        this.blood.setVisible(true);
    }

    public void canNotSee(boolean large) {
        if (large) {
            this.bg_large.setVisible(false);
            this.blood_large.setVisible(false);
            return;
        }
        this.bg.setVisible(false);
        this.blood.setVisible(false);
    }

    public void exit() {
    }
}
