package com.sg.td.actor;

import cn.egame.terminal.paysdk.FailedCode;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.CHBezierToAction;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.util.GameStage;

public class Honey implements GameConstant {
    static int[] sound_pick = {44, 29, 37, 44};
    boolean pick;
    ActorImage pickImage;
    int x;
    int y;

    public Honey(int x2, int y2) {
        this.x = x2;
        this.y = y2;
        this.pickImage = new ActorImage(768, x2, y2, 1);
        this.pickImage.addAction(Actions.sequence(Actions.scaleTo(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR), Actions.parallel(Actions.moveBy(Animation.CurveTimeline.LINEAR, -100.0f, 0.2f), Actions.scaleTo(1.3f, 1.3f, 0.2f)), Actions.parallel(Actions.moveBy(Animation.CurveTimeline.LINEAR, 100.0f, 0.1f), Actions.scaleTo(1.0f, 1.0f, 0.1f)), Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -15.0f, 0.2f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 15.0f, 0.2f)))));
        this.pickImage.setTouchable(Touchable.enabled);
        this.pickImage.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Honey.this.pickUp();
            }
        });
        GameStage.addActor(this.pickImage, 3);
    }

    /* access modifiers changed from: package-private */
    public void pickUp() {
        float vx;
        playSound_pick();
        RunnableAction run = Actions.run(new Runnable() {
            public void run() {
                RankData.addHoneyNum(1);
                GameStage.removeActor(Honey.this.pickImage);
                Rank.rankUI.addShake();
            }
        });
        if (this.x + 200 > 640) {
            vx = 580.0f;
        } else {
            vx = (float) (this.x + 200);
        }
        Bezier<Vector2> bezierParam = new Bezier<>(new Vector2(vx, (float) (this.y + FailedCode.REASON_CODE_INIT_FAILED)), new Vector2(516.0f, 1018.0f));
        if (!this.pick) {
            this.pick = true;
            this.pickImage.addAction(Actions.sequence(Actions.parallel(CHBezierToAction.obtain(bezierParam, 0.7f), Actions.scaleTo(0.2f, 0.2f, 0.7f)), run));
        }
    }

    public static void playSound_pick() {
        if (Rank.role != null) {
            Sound.playSound(sound_pick[Rank.role.getRoleIndex()]);
            return;
        }
        Sound.playSound(sound_pick[0]);
    }
}
