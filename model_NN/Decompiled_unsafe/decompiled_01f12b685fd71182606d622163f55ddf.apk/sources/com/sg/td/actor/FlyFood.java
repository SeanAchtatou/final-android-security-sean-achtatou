package com.sg.td.actor;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.Particle.GameParticle;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Tools;
import com.sg.td.Tower;
import com.sg.util.GameStage;

public class FlyFood implements GameConstant {
    int dir;
    boolean drop;
    int dropSpeed = 10;
    boolean eat;
    Group flyGroup = new Group();
    ActorImage fooImage;
    float g = 9.8f;
    public int h;
    int index;
    float initY;
    boolean isMove;
    int moveFrequency;
    String name;
    GameParticle particle;
    float[] s = {0.5f, 0.8f, 1.0f, 2.0f};
    float scale;
    boolean show;
    int space;
    float speed_x;
    int speed_y;
    float time;
    public int w;
    public float x;
    public float y;

    public void init(String name2, int dir2, int space2, int speed_x2, int speed_y2) {
        this.name = name2;
        this.space = space2;
        this.dir = dir2;
        this.speed_x = (float) speed_x2;
        this.speed_y = speed_y2;
        this.h = 70;
        this.w = 70;
        this.isMove = true;
        this.show = false;
        if (dir2 == -1) {
            this.x = ((float) (-this.w)) / 4.0f;
        } else {
            this.x = 640.0f + (((float) this.w) / 4.0f);
        }
        this.y = (float) (953 - space2);
        this.particle = new Effect().getEffect(11, 0, 0);
        this.flyGroup.addActor(this.particle);
        this.fooImage = new ActorImage(getImageIndex(), 0, 0, 1);
        this.scale = 0.5f;
        this.fooImage.setScale(this.scale);
        this.flyGroup.addActor(this.fooImage);
        this.flyGroup.setOrigin(this.x, this.y);
        this.flyGroup.setVisible(false);
        GameStage.addActor(this.flyGroup, 3);
    }

    /* access modifiers changed from: package-private */
    public int getImageIndex() {
        if (this.name.equals("Meat")) {
            return 55;
        }
        if (this.name.equals("Orange")) {
            return 56;
        }
        if (this.name.equals("Pear")) {
            return 57;
        }
        if (this.name.equals("IceCream")) {
            return 54;
        }
        return 56;
    }

    public void fly(float delta) {
        if (this.isMove) {
            if (this.drop) {
                move(delta);
            } else {
                resetDir();
                if (this.dir == -1) {
                    this.x += (this.speed_x * ((float) Rank.getGameSpeed())) / 60.0f;
                } else {
                    this.x -= (this.speed_x * ((float) Rank.getGameSpeed())) / 60.0f;
                }
                this.y -= ((float) (this.speed_y * Rank.getGameSpeed())) / 60.0f;
            }
            this.flyGroup.setPosition(this.x, this.y);
            this.flyGroup.setVisible(true);
            outScreen();
            if (this.drop && !this.eat) {
                eatByTower();
            }
            if (this.drop && !this.eat) {
                eatByFloor();
            }
            addTeach();
        }
    }

    /* access modifiers changed from: package-private */
    public void move(float delta) {
        this.time += delta;
        this.y = (float) ((int) (this.initY + ((((this.g * this.time) * this.time) / 2.0f) * 50.0f)));
    }

    /* access modifiers changed from: package-private */
    public void outScreen() {
        if ((this.drop && this.y > 953.0f) || this.y < -100.0f || this.eat) {
            removeStage();
            Rank.flyFood.remove(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void resetDir() {
        if (this.dir == -1) {
            if (this.x + ((this.speed_x * ((float) Rank.getGameSpeed())) / 10.0f) > 640.0f - (((float) this.w) / 4.0f)) {
                this.dir = 1;
            }
        } else if (this.x - ((this.speed_x * ((float) Rank.getGameSpeed())) / 10.0f) < ((float) this.w) / 4.0f) {
            this.dir = -1;
        }
    }

    public void removeStage() {
        GameStage.removeActor(this.flyGroup);
    }

    public void drop() {
        this.isMove = false;
        this.flyGroup.removeActor(this.particle);
        this.fooImage.addAction(Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -60.0f, 0.1f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 60.0f, 0.1f), Actions.run(new Runnable() {
            public void run() {
                FlyFood.this.drop = true;
                FlyFood.this.isMove = true;
            }
        })));
        RankData.addPickNum();
        this.initY = this.y;
    }

    /* access modifiers changed from: package-private */
    public void eatByTower() {
        for (int i = 0; i < Rank.tower.size(); i++) {
            Tower tower = Rank.tower.get(i);
            if (!tower.isDead()) {
                int tx = tower.x;
                int ty = tower.y;
                int cw = tower.w;
                int ch = tower.h;
                if (Tools.hit((int) this.x, (int) (this.y - ((float) (ch / 2))), 1, 1, tx - (cw / 2), ty - (ch / 2), cw, ch)) {
                    tower.addBuff(this.name);
                    this.isMove = false;
                    this.fooImage.addAction(Actions.sequence(Actions.scaleTo(this.scale + 0.5f, this.scale + 0.3f, 0.3f, Interpolation.sineIn), Actions.scaleTo(this.scale, this.scale - 0.1f, 0.2f, Interpolation.sineOut), Actions.run(new Runnable() {
                        public void run() {
                            FlyFood.this.eat = true;
                            FlyFood.this.isMove = true;
                        }
                    })));
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addTeach() {
        if (Rank.rank == 8) {
            if (Tools.inArea(new int[]{PAK_ASSETS.IMG_BULLET01, 253, 140, 140}, new int[]{(int) this.x, (int) this.y})) {
                RankData.teach.teachArea[21] = new int[]{(int) (this.x - ((float) (this.w / 2))), (int) (this.y - ((float) (this.h / 2))), this.w, this.h};
                RankData.teach.initTeach(21);
            }
        }
    }

    public boolean canTouch(int tx, int ty) {
        if (!this.eat) {
            if (Tools.hit(tx, ty, 1, 1, (int) (this.x - ((float) (this.w / 2))), (int) (this.y - ((float) (this.h / 2))), this.w, this.h)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void eatByFloor() {
        if (Map.isBlock((int) this.x, (int) (this.y + ((float) (this.h / 2))))) {
            this.fooImage.setVisible(false);
            this.fooImage.addAction(Actions.sequence(Actions.repeat(3, Actions.sequence(Actions.delay(0.3f, Actions.hide()), Actions.delay(0.3f, Actions.show()))), Actions.run(new Runnable() {
                public void run() {
                    FlyFood.this.eat = true;
                    FlyFood.this.removeStage();
                }
            })));
            this.isMove = false;
        }
    }
}
