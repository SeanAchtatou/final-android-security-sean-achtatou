package com.sg.td.actor;

import com.kbz.Particle.GameParticle;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;

public class EatBuff implements GameConstant {
    GameParticle particle;

    public EatBuff(int x, int y) {
        this.particle = new Effect().getEffect(12, x, y);
        GameStage.addActor(this.particle, 6);
    }

    public void setPosition(int x, int y) {
        this.particle.setPosition((float) x, (float) y);
    }

    public void remove() {
        GameStage.removeActor(this.particle);
    }
}
