package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameStage;

public class Up implements GameConstant {
    boolean addStage;
    ActorImage upImage;

    public Up(int x, int y) {
        this.upImage = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU022, x, y, 3);
        this.upImage.addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -5.0f, 0.2f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 5.0f, 0.2f))));
        canNotSee();
    }

    public void canSee() {
        if (!this.upImage.isVisible()) {
            this.upImage.setVisible(true);
            if (!this.addStage) {
                GameStage.addActor(this.upImage, 3);
            }
        }
    }

    public void canNotSee() {
        if (this.upImage.isVisible()) {
            this.upImage.setVisible(false);
        }
    }

    public void removeStage() {
        GameStage.removeActor(this.upImage);
    }

    public void setPosition(int x, int y) {
        this.upImage.setPosition((float) x, (float) y, 3);
    }
}
