package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;

public class Gride extends ActorImage implements GameConstant {
    public Gride(int x, int y) {
        super((int) PAK_ASSETS.IMG_BUILD_GRID, x, y, 1, 1);
    }

    public Gride(int x, int y, int type) {
        super((int) PAK_ASSETS.IMG_BUILD_GRID2, x, y, 1, 1);
        addAction(Actions.repeat(-1, Actions.sequence(Actions.alpha(0.3f, 0.3f), Actions.alpha(1.0f, 0.3f))));
    }
}
