package com.sg.td.actor;

import cn.egame.terminal.paysdk.FailedCode;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.CHBezierToAction;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.tools.GNumSprite;
import com.sg.tools.GameTime;
import com.sg.util.GameStage;

public class SubTime implements GameConstant {
    Group moneyGroup = new Group();

    public SubTime(int x, int y, int time) {
        float vx;
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU063, x, y - 30, 1, this.moneyGroup);
        GNumSprite moneySprite = new GNumSprite((int) PAK_ASSETS.IMG_ZHANDOU065, time + "'", "'", -2, 1);
        moneySprite.setPosition((float) (x + 5), (float) (y - 30));
        this.moneyGroup.addActor(moneySprite);
        this.moneyGroup.setOrigin((float) x, (float) y);
        this.moneyGroup.setScale(0.2f);
        this.moneyGroup.setAlpha(Animation.CurveTimeline.LINEAR);
        this.moneyGroup.addAction(Actions.sequence(Actions.parallel(Actions.moveBy(Animation.CurveTimeline.LINEAR, -30.0f, 0.2f), Actions.scaleTo(1.0f, 1.0f, 0.2f), Actions.alpha(1.0f, 0.2f)), Actions.delay(0.6f), Actions.moveBy(Animation.CurveTimeline.LINEAR, -30.0f, 0.2f), Actions.run(new Runnable() {
            public void run() {
                SubTime.this.moneyGroup.remove();
                SubTime.this.moneyGroup.clear();
            }
        })));
        final ActorImage clockImage = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU064, x, y, 1);
        final int i = time;
        RunnableAction run2 = Actions.run(new Runnable() {
            public void run() {
                GameTime.reduceTime(i);
                GameStage.removeActor(clockImage);
            }
        });
        if (x + 200 > 640) {
            vx = 578.0f;
        } else {
            vx = (float) (x + 200);
        }
        Bezier<Vector2> bezierParam = new Bezier<>(new Vector2(vx, (float) (y + FailedCode.REASON_CODE_INIT_FAILED)), new Vector2(320.0f, 1105.0f));
        clockImage.setScale(0.5f);
        clockImage.addAction(Actions.sequence(Actions.scaleTo(1.0f, 1.0f, 0.4f), Actions.delay(0.1f), Actions.scaleTo(0.5f, 0.5f, 0.2f), Actions.delay(0.2f), Actions.parallel(CHBezierToAction.obtain(bezierParam, 0.6f), Actions.scaleTo(0.3f, 0.3f, 0.6f)), run2));
        GameStage.addActor(this.moneyGroup, 3);
        GameStage.addActor(clockImage, 3);
    }
}
