package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.data.Mydata;
import com.sg.tools.MyImage;
import com.sg.tools.NumActor;
import com.sg.util.GameStage;

public class Icon implements GameConstant {
    String animationName;
    ActorImage bg;
    int buildX;
    int buildY;
    MyImage clock;
    public Group group = new Group();
    public Group group2 = new Group();
    public int h;
    boolean hide;
    String huitubiao;
    MyImage icon;
    int imgID;
    String imgName;
    int money;
    boolean remove;
    String tubiao;
    public int w;
    public int x;
    public int y;

    public Icon(String imgName2, int x2, int y2, int money2, String animationName2, boolean hide2) {
        this.tubiao = Mydata.towerData.get(imgName2).getTubiaoName();
        this.huitubiao = Mydata.towerData.get(imgName2).getHuitubiaoName();
        setHide(hide2);
        if (Rank.hideTower.contains(imgName2)) {
            setHide(false);
        }
        if (isHide()) {
            this.icon = new MyImage(this.huitubiao, (float) x2, (float) y2, 4);
            this.clock = new MyImage((int) PAK_ASSETS.IMG_SUO, (float) x2, (float) y2, 4);
        } else if (Rank.money >= money2) {
            this.icon = new MyImage(this.tubiao, (float) x2, (float) y2, 4);
        } else {
            this.icon = new MyImage(this.huitubiao, (float) x2, (float) y2, 4);
        }
        this.bg = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU027, x2 - 5, y2 + 32, 1, this.group);
        this.money = money2;
        NumActor num = new NumActor();
        num.setPosition((float) (x2 + 5), (float) (y2 + 32));
        num.set(PAK_ASSETS.IMG_ZHANDOU036, money2, -3, 4);
        this.group.addActor(this.icon);
        if (this.clock != null) {
            this.group.addActor(this.clock);
        }
        this.group2.addActor(this.bg);
        this.group2.addActor(num);
        GameStage.addActor(this.group, 3);
        GameStage.addActor(this.group2, 3);
        this.x = x2;
        this.y = y2;
        this.imgName = imgName2;
        this.animationName = animationName2;
    }

    public void init(int bx, int by) {
        this.w = Map.tileWidth;
        this.h = Map.tileHight;
        this.buildX = bx;
        this.buildY = by;
        this.group.setOrigin((float) this.x, (float) this.y);
        this.group.setScale(0.1f);
        this.group.addAction(Actions.scaleTo(0.65f, 0.65f, 0.3f));
        this.group2.setOrigin((float) this.x, (float) this.y);
        this.group2.setScale(0.1f);
        this.group2.addAction(Actions.scaleTo(1.0f, 1.0f, 0.3f));
    }

    public void run() {
        if (Rank.hideTower.contains(this.imgName)) {
            setHide(false);
        }
        if (!isHide()) {
            if (this.clock != null) {
                this.group.removeActor(this.clock);
            }
            if (Rank.money >= this.money) {
                this.icon.setTextureRegion(this.tubiao);
            } else {
                this.icon.setTextureRegion(this.huitubiao);
            }
        }
    }

    public void relieveClock() {
        this.hide = false;
    }

    public boolean isHide() {
        return this.hide;
    }

    public void setHide(boolean hide2) {
        this.hide = hide2;
    }

    public void createTower() {
        if (!isRemove() && !isHide() && Rank.money >= this.money) {
            Rank.money = Math.max(0, Rank.money - this.money);
            Rank.iconsMove(this.buildX - 35, this.buildY - 35);
            Rank.building.buildTower(this.buildX, this.buildY, this.animationName, true);
        }
    }

    public void removeStage() {
        GameStage.removeActor(this.group);
        GameStage.removeActor(this.group2);
    }

    public void setRemove() {
        this.remove = true;
    }

    /* access modifiers changed from: package-private */
    public boolean isRemove() {
        return this.remove;
    }
}
