package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.Particle.GEffectGroup;
import com.kbz.Particle.GameParticle;
import com.sg.td.RankData;
import com.sg.util.GameStage;

public class Effect {
    GEffectGroup gEffectGroup;
    GameParticle particle;

    public void addEffect(int id, int x, int y, int layer) {
        this.particle = RankData.getPartical(id).create((float) x, (float) y);
        GameStage.addActor(this.particle, layer);
    }

    public void addEffect(String name, int x, int y, int layer) {
        this.particle = RankData.getPartical(RankData.getParticalIndex(name)).create((float) x, (float) y);
        GameStage.addActor(this.particle, layer);
    }

    public GameParticle getEffect(int id, int x, int y) {
        this.particle = RankData.getPartical(id).create((float) x, (float) y);
        return this.particle;
    }

    public GameParticle getEffect(String name, int x, int y) {
        this.particle = RankData.getPartical(RankData.getParticalIndex(name)).create((float) x, (float) y);
        return this.particle;
    }

    public void addEffect(int id, int x, int y, Group group) {
        this.particle = RankData.getPartical(id).create((float) x, (float) y);
        group.addActor(this.particle);
    }

    public void setScale(float scale) {
        this.particle.setScale(scale, scale);
    }

    public void remove() {
        GameStage.removeActor(this.particle);
    }

    public void setPosition(int x, int y) {
        this.particle.setPosition((float) x, (float) y);
    }

    public void setOrigin_center_bottom() {
        this.particle.setOrigin(this.particle.getWidth() / 2.0f, this.particle.getHeight());
    }

    public void setRotation(float degrees) {
        this.particle.setTransform(true);
        this.particle.setRotation(degrees);
    }

    public void addAction(Action action) {
        this.particle.addAction(action);
    }

    public void addEffect_Diejia(int id, int x, int y, int layer) {
        this.gEffectGroup = new GEffectGroup();
        this.particle = RankData.getPartical(id).create(this.gEffectGroup, (float) x, (float) y);
        GameStage.addActor(this.gEffectGroup, layer);
    }

    public void addEffect_Diejia(int id, int x, int y, Group group) {
        this.gEffectGroup = new GEffectGroup();
        group.addActor(RankData.getPartical(id).create(this.gEffectGroup, (float) x, (float) y));
    }

    public void remove_Diejia() {
        GameStage.removeActor(this.gEffectGroup);
    }

    public Actor getEffect_Diejia(int id, int x, int y) {
        this.gEffectGroup = new GEffectGroup();
        RankData.getPartical(id).create(this.gEffectGroup, (float) x, (float) y);
        return this.gEffectGroup;
    }
}
