package com.sg.td.actor;

import com.sg.pak.GameConstant;
import com.sg.tools.MySprite;
import com.sg.util.GameStage;

public class TowerFrag extends MySprite implements GameConstant {
    public int x;
    public int y;

    public TowerFrag(int x2, int y2) {
        super(ANIMATION_NAME[3] + ".json", "MoneyBox_die");
        this.x = x2;
        this.y = y2;
        setPosition((float) x2, (float) (y2 + 17));
    }

    public void addToStage() {
        GameStage.addActor(this, 2);
    }
}
