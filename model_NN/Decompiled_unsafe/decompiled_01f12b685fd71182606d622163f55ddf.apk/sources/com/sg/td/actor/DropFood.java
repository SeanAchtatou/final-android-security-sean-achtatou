package com.sg.td.actor;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.sg.pak.GameConstant;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Sound;
import com.sg.td.Tools;
import com.sg.td.Tower;
import com.sg.td.data.BuildingData;
import com.sg.td.data.Mydata;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class DropFood extends MyImage implements GameConstant {
    boolean drop;
    boolean eat;
    float g = 9.8f;
    int h;
    int index;
    int initX;
    int initY;
    boolean moving;
    float scale;
    float time;
    String type;
    int w;
    int x;
    int y;

    public DropFood() {
    }

    public DropFood(String imgName, int x2, int y2) {
        super(imgName, (float) x2, (float) y2, 4);
        GameStage.addActor(this, 3);
        this.initX = x2;
        this.x = x2;
        this.initY = y2;
        this.y = y2;
        Rank.fruit.add(this);
    }

    public void init(float scale2, String deckName) {
        this.type = deckName;
        this.scale = scale2;
        BuildingData data = Mydata.deckData.get(this.type);
        this.w = data.getCollideWidth();
        this.h = data.getCollideHeight();
        setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
        addAction(Actions.scaleTo(scale2, scale2));
        addAction(Actions.sequence(Actions.scaleTo(scale2 + 0.5f, scale2 + 0.5f, 0.5f), Actions.scaleTo(scale2, scale2, 0.5f), Actions.run(new Runnable() {
            public void run() {
                DropFood.this.addFruit();
            }
        })));
    }

    public void run(float delta) {
        if (this.moving) {
            move(delta);
            eatByTower();
        }
        if (this.moving) {
            eatByFloor();
        }
        outScreen();
    }

    /* access modifiers changed from: package-private */
    public void move(float delta) {
        this.time += delta;
        this.y = (int) (((float) this.initY) + ((((this.g * this.time) * this.time) / 2.0f) * 50.0f));
        setPosition((float) this.x, (float) this.y);
        if (this.y - this.initY > this.h) {
            this.drop = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void eatByTower() {
        for (int i = 0; i < Rank.tower.size(); i++) {
            Tower tower = Rank.tower.get(i);
            if (!tower.isDead()) {
                int tx = tower.x;
                int ty = tower.y;
                int tw = tower.w;
                int th = tower.h;
                if (Tools.hit(this.x - (this.w / 4), this.y, 1, 1, tx - (tw / 2), ty - (th / 2), tw, th) || Tools.hit(this.x + (this.w / 4), this.y, 1, 1, tx - (tw / 2), ty - (th / 2), tw, th)) {
                    tower.addBuff(this.type);
                    if (!this.eat) {
                        RankData.addEatNum();
                        RankData.pickAllFood();
                        this.eat = true;
                    }
                    addAction(Actions.sequence(Actions.scaleTo(this.scale + 0.5f, this.scale + 0.3f, 0.3f, Interpolation.sineIn), Actions.scaleTo(this.scale, this.scale - 0.1f, 0.2f, Interpolation.sineOut), Actions.run(new Runnable() {
                        public void run() {
                            DropFood.this.removeStage();
                        }
                    })));
                    this.moving = false;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void eatByFloor() {
        if (Map.isBlock(this.x, this.y + (this.h / 2))) {
            setSee(false);
            addAction(Actions.sequence(Actions.repeat(3, Actions.sequence(Actions.delay(0.3f, Actions.hide()), Actions.delay(0.3f, Actions.show()))), Actions.run(new Runnable() {
                public void run() {
                    DropFood.this.removeStage();
                }
            })));
            this.moving = false;
        }
    }

    public void setSee(boolean visible) {
        setVisible(visible);
    }

    /* access modifiers changed from: package-private */
    public void addFruit() {
        this.moving = true;
        Sound.playSound(4);
    }

    public void removeStage() {
        GameStage.removeActor(this);
        Rank.fruit.remove(this);
    }

    /* access modifiers changed from: package-private */
    public void outScreen() {
        if (this.y > 996) {
            removeStage();
        }
    }
}
