package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.tools.GNumSprite;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class Money extends MyGroup implements GameConstant {
    int money;
    int x;
    int y;

    public void init(int x2, int y2, int money2, boolean isMoneyBox) {
        new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU027, x2, y2, 1, this);
        GNumSprite moneyActor = new GNumSprite((int) PAK_ASSETS.IMG_ZHANDOU036, money2 + "", (String) null, -2, 4);
        moneyActor.setPosition((float) (x2 + 15), (float) y2);
        addActor(moneyActor);
        setOrigin((float) x2, (float) y2);
        setScale(0.2f);
        setAlpha(Animation.CurveTimeline.LINEAR);
        this.x = x2;
        this.y = y2;
        this.money = money2;
        Rank.money += money2;
        RunnableAction run = Actions.run(new Runnable() {
            public void run() {
                Money.this.free();
            }
        });
        if (!isMoneyBox) {
            addAction(Actions.sequence(Actions.parallel(Actions.moveBy(Animation.CurveTimeline.LINEAR, -20.0f, 0.2f), Actions.scaleTo(1.0f, 1.0f, 0.2f), Actions.alpha(1.0f, 0.2f)), Actions.delay(0.3f), Actions.moveBy(Animation.CurveTimeline.LINEAR, -20.0f, 0.2f), run));
        } else {
            addAction(Actions.sequence(Actions.parallel(Actions.scaleTo(1.0f, 1.0f, 0.2f), Actions.alpha(1.0f, 0.2f)), Actions.delay(0.3f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 40.0f, 0.2f), run));
        }
        GameStage.addActor(this, 3);
    }

    public void init() {
    }

    public void exit() {
        GameStage.removeActor(this);
    }
}
