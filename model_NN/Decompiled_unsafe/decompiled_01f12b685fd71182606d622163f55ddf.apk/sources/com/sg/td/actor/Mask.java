package com.sg.td.actor;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.tools.GShapeSprite;
import com.sg.util.GameStage;

public class Mask extends GShapeSprite implements GameConstant {
    public Mask() {
        createRectangle(true, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 640.0f, 1136.0f);
        setColor(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.8f);
        setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public Mask(float x, float y, float w, float h) {
        createRectangle(true, x, y, w, h);
        setColor(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.5f);
    }

    public void removeMask(int lay) {
        GameStage.removeActor(this);
    }
}
