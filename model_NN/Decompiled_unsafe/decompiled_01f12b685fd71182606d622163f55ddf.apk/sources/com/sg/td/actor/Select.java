package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.td.RankData;
import com.sg.td.Tower;
import com.sg.td.TowerRole;
import com.sg.tools.MyGroup;
import com.sg.tools.MyImage;
import com.sg.tools.NumActor;
import com.sg.util.GameStage;

public class Select extends MyGroup implements GameConstant {
    MyImage del;
    MyImage money1;
    MyImage money2;
    int r;
    MyImage roleMove;
    MyImage roleUp;
    MyImage scope;
    Tower tower;
    MyImage up;
    int x;
    int y;

    public void init() {
        this.tower = Rank.tower.get(Rank.selectTowerIndex);
        this.x = this.tower.x;
        this.y = this.tower.y;
        float scale = ((float) this.tower.getRange()) / 235.0f;
        this.scope = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU030, (float) this.x, (float) this.y, 4);
        this.scope.setOrigin(this.scope.getWidth() / 2.0f, this.scope.getHeight() / 2.0f);
        this.scope.setScale(scale);
        addActor(this.scope);
        if (this.tower instanceof TowerRole) {
            addRoleSelect();
        } else {
            addTowerSelect();
        }
    }

    /* access modifiers changed from: package-private */
    public void addRoleSelect() {
        if (RankData.isRoleOpen(Rank.role.getRoleIndex())) {
            if (RankData.roleFullLevel(Rank.role.getRoleIndex())) {
                this.roleUp = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU031, (float) (this.x - 34), (float) ((this.y - 80) - 40), 0);
            } else {
                this.roleUp = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU033, (float) (this.x - 34), (float) ((this.y - 80) - 40), 0);
                this.roleUp.setTouchable(Touchable.enabled);
            }
            this.roleUp.setName("roleUp");
            addActor(this.roleUp);
        }
        if (Rank.money >= 50 || RankData.useShose()) {
            this.roleMove = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU035, (float) (this.x - 35), (float) (this.y + 20), 0);
            this.roleMove.setTouchable(Touchable.enabled);
        } else {
            this.roleMove = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU052, (float) (this.x - 35), (float) (this.y + 20), 0);
            this.roleMove.setTouchable(Touchable.disabled);
        }
        this.roleMove.setName("roleMove");
        addActor(this.roleMove);
        if (!RankData.useShose()) {
            this.money2 = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU027, (float) (this.x - 10), (float) (this.y + 87), 4);
            addActor(this.money2);
            NumActor moneyActor2 = new NumActor();
            moneyActor2.set(PAK_ASSETS.IMG_ZHANDOU036, 50, -3, 4);
            moneyActor2.setPosition((float) this.x, (float) (this.y + 87));
            addActor(moneyActor2);
        }
        GameStage.addActor(this, 3);
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if ("roleUp".equals(event.getTarget().getName())) {
                    Rank.building.updateRole();
                    return true;
                } else if (!"roleMove".equals(event.getTarget().getName())) {
                    return true;
                } else {
                    Rank.building.moveSlectRole();
                    return true;
                }
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void addTowerSelect() {
        if (this.tower.getUpdatePrice() > 0) {
            if (Rank.money >= this.tower.getUpdatePrice()) {
                this.up = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU033, (float) (this.x - 34), (float) ((this.y - 80) - 40), 0);
                this.up.setTouchable(Touchable.enabled);
            } else {
                this.up = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU032, (float) (this.x - 34), (float) ((this.y - 80) - 40), 0);
            }
            this.up.setName("up");
            addActor(this.up);
            this.money1 = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU027, (float) (this.x - 10), (float) (this.y - 60), 4);
            addActor(this.money1);
            NumActor moneyActor = new NumActor();
            moneyActor.set(PAK_ASSETS.IMG_ZHANDOU036, this.tower.getUpdatePrice(), -3, 4);
            moneyActor.setPosition((float) this.x, (float) (this.y - 60));
            addActor(moneyActor);
        } else {
            this.up = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU031, (float) (this.x - 34), (float) ((this.y - 80) - 40), 0);
            addActor(this.up);
        }
        this.del = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU034, (float) (this.x - 35), (float) (this.y + 30), 0);
        this.del.setTouchable(Touchable.enabled);
        this.del.setName("del");
        addActor(this.del);
        this.money2 = new MyImage((int) PAK_ASSETS.IMG_ZHANDOU027, (float) (this.x - 10), (float) (this.y + 97), 4);
        addActor(this.money2);
        NumActor moneyActor2 = new NumActor();
        moneyActor2.set(PAK_ASSETS.IMG_ZHANDOU036, this.tower.getDeletPrice(), -3, 4);
        moneyActor2.setPosition((float) this.x, (float) (this.y + 97));
        addActor(moneyActor2);
        GameStage.addActor(this, 3);
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ("del".equals(event.getTarget().getName())) {
                    Rank.building.removeSlectTower();
                } else if ("up".equals(event.getTarget().getName())) {
                    Rank.building.updateTower();
                }
            }
        });
    }

    public void run() {
        if ((this.tower instanceof TowerRole) || this.tower.getUpdatePrice() <= 0) {
            return;
        }
        if (Rank.money >= this.tower.getUpdatePrice()) {
            this.up.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU033);
            this.up.setTouchable(Touchable.enabled);
            return;
        }
        this.up.setTextureRegion((int) PAK_ASSETS.IMG_ZHANDOU032);
        this.up.setTouchable(Touchable.disabled);
    }

    public void exit() {
    }
}
