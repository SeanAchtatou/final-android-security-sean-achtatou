package com.sg.td.actor;

import com.sg.pak.GameConstant;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.tools.MyImage;
import com.sg.util.GameStage;

public class Box implements GameConstant {
    int aimX;
    int aimY;
    Effect chuGuaiEffect;
    boolean create;
    MyImage image;
    int moveX;
    int moveY;
    boolean remove;
    int skillIndex;
    float step;
    int tipEffectIndex;
    int x;
    int y;

    public Box() {
    }

    public Box(int imgName, int x2, int y2, int index, int eftIndex) {
        this.x = (Map.tileWidth / 2) + x2;
        this.y = (Map.tileHight / 2) + y2;
        this.skillIndex = index;
        this.tipEffectIndex = eftIndex;
        System.out.println("eftIndex:" + eftIndex);
        this.image = new MyImage(imgName, (float) x2, (float) y2, 4);
        GameStage.addActor(this.image, 1);
    }

    public void init(int aimX2, int aimY2, float step2) {
        this.aimX = aimX2;
        this.aimY = aimY2;
        this.step = step2;
        this.create = false;
        initMoveData();
    }

    public void move() {
        if (!remove()) {
            move_line_follow();
            if (this.create && Rank.level.isAllShow()) {
                removeBox();
            }
            if (!this.create && this.x == this.aimX && this.y == this.aimY) {
                createMonster();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void initMoveData() {
        int a = this.aimX - this.x;
        int b = this.aimY - this.y;
        float r = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (int) ((((float) a) * this.step) / r);
        }
        if (b != 0) {
            this.moveY = (int) ((((float) b) * this.step) / r);
        }
    }

    /* access modifiers changed from: protected */
    public void move_line_follow() {
        int a = this.aimX - this.x;
        int b = this.aimY - this.y;
        float r = (float) Math.sqrt((double) ((a * a) + (b * b)));
        if (a != 0) {
            this.moveX = (int) ((((float) a) * this.step) / r);
        }
        if (b != 0) {
            this.moveY = (int) ((((float) b) * this.step) / r);
        }
        if (Math.abs(a) > Math.abs(this.moveX) || Math.abs(b) > Math.abs(this.moveY)) {
            this.x += this.moveX;
            this.y += this.moveY;
        } else {
            this.x = this.aimX;
            this.y = this.aimY;
        }
        this.image.setPosition((float) this.x, (float) this.y);
    }

    /* access modifiers changed from: package-private */
    public void createMonster() {
        Rank.level.createEnemy(this.aimX, this.aimY, this.skillIndex);
        addChuGuaiEffect();
        this.create = true;
    }

    public void removeBox() {
        GameStage.removeActor(this.image);
        this.remove = true;
        removeGuaiEffect();
    }

    /* access modifiers changed from: package-private */
    public void addChuGuaiEffect() {
        this.chuGuaiEffect = new Effect();
        this.chuGuaiEffect.addEffect_Diejia(38, this.x, this.y, 3);
        Rank.boss.removeGuaiEffect(this.tipEffectIndex);
    }

    /* access modifiers changed from: package-private */
    public void removeGuaiEffect() {
        if (this.chuGuaiEffect != null) {
            this.chuGuaiEffect.remove_Diejia();
        }
    }

    public boolean remove() {
        return this.remove;
    }
}
