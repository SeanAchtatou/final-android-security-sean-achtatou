package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.Sound;
import com.sg.td.data.Mydata;
import com.sg.util.GameStage;

public class Bomb extends DropFood {
    static String imgName = "bomb0.png";
    int AOERange;
    int level;
    int power;
    String type = "Bomb";

    public Bomb(int x, int y, int level2) {
        super(imgName, x, y);
        System.out.println(imgName);
        this.level = level2;
        this.power = Mydata.towerData.get(this.type).getPower(level2 - 1);
        this.AOERange = Mydata.towerData.get(this.type).getAOERange(level2 - 1);
        System.out.println("bomb level:" + level2 + "  power:" + this.power + "  AOERange:" + this.AOERange);
    }

    public void run(float delta) {
        if (this.moving) {
            move(delta);
            hit();
        }
        outScreen();
    }

    /* access modifiers changed from: package-private */
    public void hit() {
        if ((this.drop && Rank.isDeckArea(this.x, this.y - (this.h / 2)) && isPlatform(this.x, this.y - (this.h / 2))) || (Rank.level.isEnemyArea(this.x, this.y - (this.h / 2)) && isPlatform(this.x, this.y - (this.h / 2)))) {
            addAction(Actions.sequence(Actions.alpha(Animation.CurveTimeline.LINEAR), Actions.delay(0.2f), Actions.alpha(1.0f), Actions.scaleTo(1.2f, 1.2f, 0.2f), Actions.scaleTo(1.0f, 1.0f, 0.2f), Actions.run(new Runnable() {
                public void run() {
                    Bomb.this.bomb();
                }
            })));
            new Effect().addEffect(55, this.x, this.y, 3);
            this.moving = false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isPlatform(int x, int y) {
        if (!Map.isPhy(x, y) || Map.isRoad(x, y)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void bomb() {
        GameStage.removeActor(this);
        for (int i = 0; i < Rank.enemy.size(); i++) {
            if (!Rank.enemy.get(i).isDead() && Rank.enemy.get(i).inAttackArea(this.x, this.y, this.AOERange)) {
                Rank.enemy.get(i).hurt(this.power, null, -1, this.type);
            }
        }
        for (int i2 = 0; i2 < Rank.deck.size(); i2++) {
            if (!Rank.deck.get(i2).isDead() && Rank.deck.get(i2).inAttackArea(this.x, this.y, this.AOERange)) {
                Rank.deck.get(i2).hurt(this.power, null, -1);
            }
        }
        Sound.playSound(2);
    }
}
