package com.sg.td.actor;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.Actors.ActorImage;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Rank;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class Focus extends MyGroup {
    ActorImage image;

    public void init() {
        int[] xy = getFocusXY();
        this.image = new ActorImage((int) PAK_ASSETS.IMG_ZHANDOU021, xy[0], xy[1], 3, this);
        this.image.addAction(Actions.repeat(-1, Actions.sequence(Actions.moveBy(Animation.CurveTimeline.LINEAR, -5.0f, 0.2f), Actions.moveBy(Animation.CurveTimeline.LINEAR, 5.0f, 0.2f))));
        GameStage.addActor(this, 3);
    }

    public void run() {
        if (!Rank.isDeckFocus()) {
            int[] xy = getFocusXY();
            this.image.setPosition((float) xy[0], (float) xy[1], 3);
        }
    }

    /* access modifiers changed from: package-private */
    public int[] getFocusXY() {
        int[] xy = new int[2];
        int x = 0;
        int y = 0;
        if (Rank.isDeckFocus()) {
            x = Rank.deck.get(Rank.focus).hitX;
            y = Rank.deck.get(Rank.focus).hitY - Rank.deck.get(Rank.focus).hpBarY;
        }
        if (Rank.isEnemyFocus()) {
            x = Rank.enemy.get(Rank.focus).x;
            y = Rank.enemy.get(Rank.focus).y - Rank.enemy.get(Rank.focus).h;
        }
        xy[0] = x;
        xy[1] = y;
        return xy;
    }

    public void exit() {
    }
}
