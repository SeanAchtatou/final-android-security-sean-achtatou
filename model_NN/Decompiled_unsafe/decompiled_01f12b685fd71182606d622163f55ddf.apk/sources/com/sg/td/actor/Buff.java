package com.sg.td.actor;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.td.Tools;

public class Buff implements GameConstant {
    Effect buffEffect;
    String efct;
    int enemyIndex = -1;
    int enemyType;
    int index;
    float life;
    float lifeTime;
    float rate;
    boolean remove;
    float runTime;
    String towerName;
    int type;
    float value;
    int x;
    int y;

    public void init(int enemyType2, int enemyIndex2, int type2, float rate2, float value2, float life2, String tName) {
        if (type2 != 1 || enemyType2 != 1) {
            if (((type2 != 2 && type2 != 6) || ((float) Tools.nextInt(100)) / 100.0f <= rate2) && !aimHasBuff(enemyType2, enemyIndex2, type2, value2, life2)) {
                this.enemyType = enemyType2;
                this.enemyIndex = enemyIndex2;
                this.type = type2;
                this.rate = rate2;
                this.value = value2;
                this.life = life2;
                this.towerName = tName;
                initXY();
                initParticle();
                addBuff();
                Rank.buff.add(this);
                this.index = Rank.buff.size();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void initXY() {
        if (this.enemyType == 0) {
            if (this.type == 2) {
                this.x = Rank.enemy.get(this.enemyIndex).x;
                this.y = Rank.enemy.get(this.enemyIndex).y - (Rank.enemy.get(this.enemyIndex).h / 2);
                return;
            }
            this.x = Rank.enemy.get(this.enemyIndex).x;
            this.y = Rank.enemy.get(this.enemyIndex).ADD_Y + Rank.enemy.get(this.enemyIndex).y;
        } else if (this.enemyType == 1) {
            this.x = (Rank.deck.get(this.enemyIndex).w / 2) + Rank.deck.get(this.enemyIndex).x;
            this.y = Rank.deck.get(this.enemyIndex).h + Rank.deck.get(this.enemyIndex).y;
        }
    }

    /* access modifiers changed from: package-private */
    public void resetBuffTime(float value2, float life2) {
        if (value2 > this.value) {
            this.value = value2;
        }
        if (life2 > this.life) {
            this.life = life2;
        }
        this.lifeTime = Animation.CurveTimeline.LINEAR;
    }

    /* access modifiers changed from: package-private */
    public void initParticle() {
        this.buffEffect = new Effect();
        if (this.type == 1) {
            this.buffEffect.addEffect(18, this.x, this.y, 2);
        }
        if (this.type == 3) {
            this.buffEffect.addEffect(19, this.x, this.y, 2);
        }
        if (this.enemyType == 0 && this.type == 2) {
            this.buffEffect.addEffect(21, this.x, this.y, 2);
        }
        if (this.type == 5) {
            this.buffEffect.addEffect_Diejia(2, this.x, this.y, 2);
        }
        if (this.enemyType == 0 && this.type == 6) {
            this.buffEffect.addEffect_Diejia(20, this.x, this.y, 6);
        }
    }

    /* access modifiers changed from: package-private */
    public void addBuff() {
        if (this.enemyType == 0) {
            if (this.type == 1) {
                Rank.enemy.get(this.enemyIndex).addBuffSlow(this.value);
            }
            if (this.type == 2 || this.type == 6) {
                Rank.enemy.get(this.enemyIndex).addBuffStop();
            }
            if (this.type == 3) {
                Rank.enemy.get(this.enemyIndex).addBuffFire();
            }
        }
    }

    public void run(float delta) {
        if (!this.remove) {
            if (this.enemyType == 0 && this.enemyIndex >= Rank.enemy.size()) {
                remove();
            } else if (this.enemyType != 1 || this.enemyIndex < Rank.deck.size()) {
                this.runTime += delta;
                this.lifeTime += delta;
                if (this.type == 3 && this.runTime > 0.5f) {
                    if (this.enemyType == 0) {
                        Rank.enemy.get(this.enemyIndex).hurt((int) this.value, null, -1, this.towerName);
                    }
                    if (this.enemyType == 1) {
                        Rank.deck.get(this.enemyIndex).hurt((int) this.value, null, -1);
                    }
                    this.runTime = Animation.CurveTimeline.LINEAR;
                }
                if (this.lifeTime > this.life) {
                    if (this.enemyType == 0) {
                        if (this.type == 1) {
                            Rank.enemy.get(this.enemyIndex).removeBuffSlow();
                        }
                        if (this.type == 6 || this.type == 2) {
                            Rank.enemy.get(this.enemyIndex).removeBuffStop();
                        }
                        if (this.type == 3) {
                            Rank.enemy.get(this.enemyIndex).removeBuffFire();
                        }
                    }
                    remove();
                }
                move();
            } else {
                remove();
            }
        }
    }

    public void remove() {
        this.buffEffect.remove();
        this.buffEffect.remove_Diejia();
        this.remove = true;
    }

    /* access modifiers changed from: package-private */
    public void move() {
        if (this.enemyType == 0) {
            if (Rank.enemy.get(this.enemyIndex).isDead()) {
                remove();
            } else {
                if (this.type == 2) {
                    this.x = Rank.enemy.get(this.enemyIndex).x;
                    this.y = Rank.enemy.get(this.enemyIndex).y - (Rank.enemy.get(this.enemyIndex).h / 2);
                } else {
                    this.x = Rank.enemy.get(this.enemyIndex).x;
                    this.y = Rank.enemy.get(this.enemyIndex).ADD_Y + Rank.enemy.get(this.enemyIndex).y;
                }
                this.buffEffect.setPosition(this.x, this.y);
            }
        }
        if (this.enemyType == 1 && Rank.deck.get(this.enemyIndex).isDead()) {
            remove();
        }
    }

    public boolean isSameBuff(int enemyType2, int enemyIndex2, int type2) {
        return this.enemyType == enemyType2 && this.enemyIndex == enemyIndex2 && this.type == type2;
    }

    public boolean aimHasBuff(int enemyType2, int enemyIndex2, int type2, float value2, float life2) {
        int i = 0;
        while (i < Rank.buff.size()) {
            if (Rank.buff.get(i).remove || !Rank.buff.get(i).isSameBuff(enemyType2, enemyIndex2, type2)) {
                i++;
            } else {
                Rank.buff.get(i).resetBuffTime(value2, life2);
                return true;
            }
        }
        return false;
    }
}
