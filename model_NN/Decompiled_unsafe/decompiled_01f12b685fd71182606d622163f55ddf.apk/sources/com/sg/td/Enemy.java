package com.sg.td;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Sort;
import com.kbz.Particle.GEffectGroup;
import com.kbz.Particle.GameParticle;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Boss;
import com.sg.td.actor.Blood;
import com.sg.td.actor.Effect;
import com.sg.td.actor.Honey;
import com.sg.td.actor.Money;
import com.sg.td.data.MonsterData;
import com.sg.tools.GameTime;
import com.sg.tools.MyImage;
import com.sg.tools.MySprite;
import com.sg.util.GameStage;

public class Enemy extends MySprite implements GameConstant {
    static int[][] path;
    static Sort sort = Sort.instance();
    static int totalDis;
    public int ADD_Y;
    String animName;
    int base;
    protected Blood blood;
    public boolean boss;
    boolean bossMode;
    Boss.BossSpine bossSpine;
    public boolean boss_enemy;
    public int box_x;
    public int box_y;
    boolean buffFire;
    boolean buffSlow;
    boolean buffStop;
    public int curPoint;
    public int curStatus;
    int damage;
    int dir;
    String dropString;
    int faceDir;
    GEffectGroup gEffectGroup;
    int getMoney;
    public int h;
    boolean hasBuff;
    public int hitX;
    public int hitY;
    public int hp;
    public int hpMax;
    public int index;
    public int initSpeed;
    boolean large;
    int lastDir;
    int lastX;
    int lastY;
    MonsterData mdata;
    int money;
    int moveDis;
    public int movePos;
    String name;
    float nextStep;
    public int offRange;
    GameParticle particle;
    int showHpTime;
    boolean sizeNormal;
    public int slowSpeed;
    boolean small;
    public int speed;
    float step;
    short stepNum;
    public int tempMovePos;
    float time;
    public boolean visible;
    public int w;
    int waitTime;
    public int wave;
    public int x;
    public int y;
    MyImage yingziImage;

    public Enemy() {
        this.visible = false;
        this.waitTime = -1;
        this.ADD_Y = 1;
    }

    public Enemy(String animationPack, String animationName) {
        super(animationPack, animationName);
        this.visible = false;
        this.waitTime = -1;
        this.ADD_Y = 1;
    }

    public Enemy(boolean boss2) {
        this.visible = false;
        this.waitTime = -1;
        this.ADD_Y = 1;
        this.x = path[0][0];
        this.y = path[0][1];
        this.bossSpine = new Boss.BossSpine(this.x, this.y + 25, 18);
        this.boss_enemy = true;
    }

    public void setMonsterData(MonsterData m) {
        this.mdata = m;
    }

    public static void setPath(int[][] runPath) {
        path = runPath;
        totalDis = 0;
        for (int i = 0; i < runPath.length - 1; i++) {
            totalDis += Math.abs(runPath[i + 1][0] - runPath[i][0]) + Math.abs(runPath[i + 1][1] - runPath[i][1]);
        }
    }

    public float getStepScale() {
        return 140.0f / (60.0f * ((float) Rank.getGameSpeed()));
    }

    /* access modifiers changed from: protected */
    public int getSpeed() {
        if (hasBuffStop()) {
            return 0;
        }
        if (hasBuffSlow()) {
            return this.slowSpeed;
        }
        return this.speed;
    }

    public int getInitSpeed() {
        return this.initSpeed;
    }

    public String getDrop() {
        return this.dropString;
    }

    public int getOffRange() {
        return this.offRange;
    }

    public boolean isBossEnemy() {
        return this.boss_enemy;
    }

    public void init(String name2, int index2, int hp2, int speed2, float time2, float nextTime, String drop, int wave2) {
        float f;
        boolean z = true;
        this.name = name2;
        this.index = index2;
        this.wave = wave2;
        if (Rank.isEasyMode()) {
            f = (float) hp2;
        } else {
            f = ((float) hp2) + (((float) hp2) * Rank.addHp);
        }
        int hp3 = (int) f;
        this.hp = hp3;
        this.hpMax = hp3;
        this.initSpeed = speed2;
        this.speed = speed2;
        this.time = time2;
        this.dropString = drop;
        this.visible = false;
        this.sizeNormal = false;
        this.boss = false;
        int i = path[0][0];
        this.lastX = i;
        this.x = i;
        int i2 = path[0][1];
        this.lastY = i2;
        this.y = i2;
        this.curStatus = 0;
        this.blood = new Blood();
        this.w = this.mdata.getCollideWidth();
        this.h = this.mdata.getCollideHeight();
        setHitXY();
        this.offRange = this.w / 2;
        this.money = this.mdata.getMoney();
        this.animName = this.mdata.getAnimName();
        this.damage = this.mdata.getDamage();
        this.large = this.damage >= 2;
        if (this.damage != 1) {
            z = false;
        }
        this.small = z;
        float step2 = getStep();
        this.nextStep = step2;
        this.step = step2;
        getNextDir();
        initBase();
        this.bossMode = false;
        this.yingziImage = new MyImage((int) PAK_ASSETS.IMG_YINGZI, (float) this.x, (float) this.y, 4);
        this.yingziImage.setOrigin(this.yingziImage.getWidth() / 2.0f, this.yingziImage.getHeight() / 2.0f);
        if (name2.indexOf("Boss") != -1) {
            this.yingziImage.setScale(0.8f, 0.7f);
        } else {
            this.yingziImage.setScale(0.6f, 0.5f);
        }
    }

    /* access modifiers changed from: package-private */
    public void setHitXY() {
        this.hitX = this.x;
        this.hitY = this.y - (Map.tileHight / 2);
    }

    /* access modifiers changed from: package-private */
    public void resetXY(int x2, int y2, int curPoint2) {
        this.x = x2;
        this.y = y2;
        this.curPoint = curPoint2;
        this.box_x = x2;
        this.box_y = y2;
        this.bossMode = true;
        getNextDir();
    }

    /* access modifiers changed from: package-private */
    public void initBase() {
        switch (this.dir) {
            case 0:
            case 2:
                this.base = this.y;
                return;
            case 1:
            case 3:
                this.base = this.x;
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public float getStep() {
        return ((float) (Rank.gameSpeed * getSpeed())) / 60.0f;
    }

    /* access modifiers changed from: package-private */
    public int getNextDir() {
        int i;
        int i2 = 2;
        if (this.curPoint >= path.length - 1) {
            return -1;
        }
        int ox = path[this.curPoint][0];
        int oy = path[this.curPoint][1];
        int ox1 = path[this.curPoint + 1][0];
        int oy1 = path[this.curPoint + 1][1];
        int ox2 = path[this.curPoint + 2 >= path.length + -1 ? this.curPoint + 1 : this.curPoint + 2][0];
        if (ox == ox1) {
            if (oy1 > oy) {
                i = 2;
            } else {
                i = 0;
            }
            this.dir = i;
        } else if (oy == oy1) {
            this.dir = ox1 > ox ? 1 : 3;
        }
        if (oy == oy1) {
            if (ox1 > ox) {
                i2 = 1;
            }
            this.faceDir = i2;
        } else if (ox == ox1) {
            if (oy1 > oy) {
                if (ox2 > ox1) {
                    i2 = 1;
                }
                this.faceDir = i2;
            } else {
                this.faceDir = ox2 > ox1 ? 0 : 3;
            }
        }
        if (this.lastDir != this.dir) {
            if (this.dir == 3 || this.dir == 0) {
                if (this.bossSpine != null) {
                    this.bossSpine.setFilpX(false);
                } else {
                    flip(false, false);
                }
            } else if (this.bossSpine != null) {
                this.bossSpine.setFilpX(true);
            } else {
                flip(true, false);
            }
            this.lastDir = this.faceDir;
        }
        return this.dir;
    }

    public void run(float delta) {
        if (!this.boss) {
            this.nextStep = getStep();
            getNextDir();
            hurtHome();
            if (this.curStatus == 0) {
                move();
                resetXY();
            }
            runShow();
            runComeOnEffect(this.x, this.y);
        }
        addHpLine();
    }

    public void move() {
        if (this.curPoint + 1 <= path.length - 1) {
            if (this.step != this.nextStep) {
                this.tempMovePos = this.movePos;
                this.stepNum = 0;
                this.step = this.nextStep;
            }
            this.stepNum = (short) (this.stepNum + 1);
            this.movePos = (int) (((float) this.tempMovePos) + (((float) this.stepNum) * this.step));
            switch (this.dir) {
                case 0:
                    if (!this.visible && this.waitTime < 0) {
                        if (this.base - this.movePos > path[this.curPoint][1]) {
                            this.y = this.base - this.movePos;
                            break;
                        } else {
                            this.y = path[this.curPoint][1];
                            setShow();
                            break;
                        }
                    } else if (path[this.curPoint][1] - this.movePos >= path[this.curPoint + 1][1]) {
                        this.y = path[this.curPoint][1] - this.movePos;
                        break;
                    } else {
                        this.y = path[this.curPoint + 1][1];
                        setWheel();
                        break;
                    }
                    break;
                case 1:
                    if (!this.visible && this.waitTime < 0) {
                        if (this.base + this.movePos < path[this.curPoint][0]) {
                            this.x = this.base + this.movePos;
                            break;
                        } else {
                            this.x = path[this.curPoint][0];
                            setShow();
                            break;
                        }
                    } else if (path[this.curPoint][0] + this.movePos <= path[this.curPoint + 1][0]) {
                        this.x = path[this.curPoint][0] + this.movePos;
                        break;
                    } else {
                        this.x = path[this.curPoint + 1][0];
                        setWheel();
                        break;
                    }
                    break;
                case 2:
                    if (!this.visible && this.waitTime < 0) {
                        if (this.base + this.movePos < path[this.curPoint][1]) {
                            this.y = this.base + this.movePos;
                            break;
                        } else {
                            this.y = path[this.curPoint][1];
                            setShow();
                            break;
                        }
                    } else if (path[this.curPoint][1] + this.movePos <= path[this.curPoint + 1][1]) {
                        this.y = path[this.curPoint][1] + this.movePos;
                        break;
                    } else {
                        this.y = path[this.curPoint + 1][1];
                        setWheel();
                        break;
                    }
                    break;
                case 3:
                    if (!this.visible && this.waitTime < 0) {
                        if (this.base - this.movePos > path[this.curPoint][0]) {
                            this.x = this.base - this.movePos;
                            break;
                        } else {
                            this.x = path[this.curPoint][0];
                            setShow();
                            break;
                        }
                    } else if (path[this.curPoint][0] - this.movePos >= path[this.curPoint + 1][0]) {
                        this.x = path[this.curPoint][0] - this.movePos;
                        break;
                    } else {
                        this.x = path[this.curPoint + 1][0];
                        setWheel();
                        break;
                    }
                    break;
            }
            if (this.bossSpine != null) {
                this.bossSpine.setPosition((float) this.x, (float) this.y);
                this.yingziImage.setPosition((float) this.x, (float) (this.y + 4));
            } else {
                setPosition((float) this.x, (float) this.y);
                this.yingziImage.setPosition((float) this.x, (float) (this.y + 4));
            }
            addMoveDistance(this.x, this.y);
            setHitXY();
        }
    }

    /* access modifiers changed from: package-private */
    public void addMoveDistance(int x2, int y2) {
        if (this.visible || this.waitTime >= 0) {
            this.moveDis += Math.abs(this.lastX - x2) + Math.abs(this.lastY - y2);
            this.lastX = x2;
            this.lastY = y2;
            return;
        }
        this.lastX = x2;
        this.lastY = y2;
    }

    public int getMoveDis() {
        return this.moveDis;
    }

    /* access modifiers changed from: package-private */
    public void resetXY() {
        if (this.dir != 1 && this.dir != 2) {
            return;
        }
        if (this.animName.equals("moster_Fast2")) {
            setPosition((float) (this.x - 22), (float) this.y);
        } else if (this.animName.equals("moster_Slow4")) {
            setPosition((float) (this.x - 15), (float) this.y);
        }
    }

    /* access modifiers changed from: package-private */
    public void setShow() {
        if (GameTime.getGameReMainTimeLimit() == Animation.CurveTimeline.LINEAR) {
            GameTime.initGameReMainTimeLimit(this.time);
            this.waitTime = 1 / Rank.getGameSpeed();
            this.stepNum = 0;
            this.movePos = 0;
            this.tempMovePos = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void runShow() {
        if (this.waitTime > 0) {
            this.waitTime--;
        } else if (this.waitTime == 0 && !this.visible) {
            if (this.bossMode) {
                if (!this.bossMode) {
                    return;
                }
                if ((this.dir != 0 || this.y > this.box_y) && ((this.dir != 2 || this.y < this.box_y) && ((this.dir != 3 || this.x > this.box_x) && (this.dir != 1 || this.x < this.box_x)))) {
                    return;
                }
            }
            this.waitTime = -1;
            this.visible = true;
            GameStage.addActor(this.yingziImage, 1);
            GameStage.addActor(this, 2);
            addComeOnEffect(this.x, this.y);
            if (this.bossSpine != null) {
                Rank.boss.setNotSee();
                GameStage.addActor(this.bossSpine, 2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setWheel() {
        this.curPoint++;
        this.stepNum = 0;
        this.movePos = 0;
        this.tempMovePos = 0;
    }

    /* access modifiers changed from: package-private */
    public void hurtHome() {
        if (this.curPoint >= path.length - 1 && !isDead()) {
            Rank.home.hurtHome(this.damage);
            setDead(null);
            playHurtSound();
        }
    }

    /* access modifiers changed from: package-private */
    public void playHurtSound() {
        if (this.large) {
            Sound.playSound(0);
        }
        if (this.small) {
            Sound.playSound(73);
        }
    }

    /* access modifiers changed from: package-private */
    public void playDeadSound() {
        if (this.large) {
            Sound.playSound(1);
        }
        if (this.small) {
            Sound.playSound(74);
        }
    }

    /* access modifiers changed from: package-private */
    public void setDead(String towerName) {
        clearSelectedEnemy(this.index);
        this.curStatus = 1;
        this.hp = 0;
        this.blood.free();
        removeStage_enemy();
        if (towerName != null) {
            RankData.addTowerHurtNum(towerName);
        }
    }

    /* access modifiers changed from: package-private */
    public void drop() {
        int teachId = -1;
        if (this.animName != null && this.animName.equals("monster_money") && this.dropString.equals("null")) {
            new Honey(this.x, this.y);
            RankData.pickDrop(Rank.getRank());
        }
        if (this.dropString != null) {
            if (this.dropString.equals("HoneyOnce,1")) {
                teachId = 7;
            } else if (this.dropString.equals("Bear3TryOut,1")) {
                teachId = 8;
            } else if (this.dropString.equals("Bear4TryOut,1")) {
                teachId = 9;
            } else if (this.dropString.equals("Bear2TryOut,1")) {
                teachId = 10;
            }
            if (teachId != -1) {
                RankData.teach.teachArea[teachId] = new int[]{this.x - 70, this.y - 70, 140, 140};
                RankData.teach.initTeach(teachId);
            }
        }
    }

    public void removeStage_enemy() {
        if (this.bossSpine != null) {
            GameStage.removeActor(this.bossSpine);
        } else {
            GameStage.removeActor(this);
        }
        if (this.yingziImage != null) {
            GameStage.removeActor(this.yingziImage);
        }
    }

    public boolean isDead() {
        return this.curStatus == 1 && this.hp <= 0;
    }

    /* access modifiers changed from: package-private */
    public void clearSelectedEnemy(int index2) {
        if (isFocus()) {
            Rank.clearFocus();
        }
    }

    public boolean isFocus() {
        return Rank.focusType == 1 && Rank.focus == this.index;
    }

    public void hurt(int value, String eftType, int hitSound, String towerName) {
        if (this.visible) {
            this.hp -= value;
            this.showHpTime = PAK_ASSETS.IMG_SHUOMINGZI03;
            if (this.hp <= 0 && !isDead()) {
                setDead(towerName);
                addDieEffect();
                playDeadSound();
                drop();
                new Money().init(this.x, this.y - this.h, this.money, false);
            }
            if (eftType != null && !eftType.equals("null")) {
                new Effect().addEffect(eftType, this.hitX, this.hitY, 3);
            }
            Sound.playSound(hitSound);
        }
    }

    /* access modifiers changed from: package-private */
    public void addDieEffect() {
        new Effect().addEffect_Diejia(22, this.hitX, this.hitY, 3);
    }

    public boolean canAttack() {
        if (isDead() || !this.visible || !this.sizeNormal) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void addHpLine() {
        if (!isDead()) {
            this.showHpTime -= Rank.getGameSpeed();
            if (this.showHpTime <= 0) {
                this.blood.canNotSee(this.large);
            } else if (this.visible && this.hp > 0 && this.hp < this.hpMax) {
                if (this.large || this.boss) {
                    this.blood.draw(Color.BLACK, this.x - 35, this.y - this.h, 70.0f, this.large);
                    this.blood.draw(Color.GREEN, this.x - 35, this.y - this.h, (float) ((this.hp * 70) / this.hpMax), this.large);
                    return;
                }
                this.blood.draw(Color.BLACK, this.x - 30, this.y - this.h, 60.0f, this.large);
                this.blood.draw(Color.GREEN, this.x - 30, this.y - this.h, (float) ((this.hp * 60) / this.hpMax), this.large);
            }
        }
    }

    public boolean getVisible() {
        return this.visible;
    }

    /* access modifiers changed from: package-private */
    public void addComeOnEffect(int x2, int y2) {
        this.gEffectGroup = new GEffectGroup();
        this.particle = new Effect().getEffect(23, x2, y2);
        this.gEffectGroup.addActor(this.particle);
        GameStage.addActor(this.gEffectGroup, 4);
        setScale(Animation.CurveTimeline.LINEAR);
        this.yingziImage.setScale(Animation.CurveTimeline.LINEAR);
        if ((!Rank.ENDLESS_MODE && this.index == 0) || (Rank.ENDLESS_MODE && this.index == 1)) {
            Rank.boss.setAnimation_PutEnemy();
        }
        comeOn();
    }

    /* access modifiers changed from: package-private */
    public void runComeOnEffect(int x2, int y2) {
        if (this.particle == null) {
            return;
        }
        if (this.particle.isComplete()) {
            GameStage.removeActor(this.gEffectGroup);
            this.particle = null;
            return;
        }
        this.particle.setPosition((float) x2, (float) y2);
    }

    /* access modifiers changed from: package-private */
    public float getScaleSpeed() {
        return 0.3f / ((float) Rank.getGameSpeed());
    }

    /* access modifiers changed from: package-private */
    public float getDelaySpeed() {
        return 0.3f / ((float) Rank.getGameSpeed());
    }

    /* access modifiers changed from: package-private */
    public void comeOn() {
        addAction(Actions.sequence(Actions.delay(getDelaySpeed()), Actions.scaleTo(1.0f, 1.0f, getScaleSpeed()), Actions.run(new Runnable() {
            public void run() {
                Enemy.this.sizeNormal = true;
            }
        })));
        if (this.name.indexOf("Boss") != -1) {
            this.yingziImage.addAction(Actions.sequence(Actions.delay(getDelaySpeed()), Actions.scaleTo(0.8f, 0.7f, getScaleSpeed())));
        } else {
            this.yingziImage.addAction(Actions.sequence(Actions.delay(getDelaySpeed()), Actions.scaleTo(0.6f, 0.5f, getScaleSpeed())));
        }
        if (this.index == Rank.enemy.size() - 1) {
            Rank.boss.setAnimation_Stop();
        }
    }

    /* access modifiers changed from: package-private */
    public void visiable() {
        this.visible = true;
    }

    public int getHp() {
        return this.hpMax;
    }

    public boolean inAttackArea(int towerX, int towerY, int towerRange) {
        if (this.w == this.h) {
            return Tools.inAttackRange(this.hitX, this.hitY, towerX, towerY, towerRange, getOffRange());
        }
        return Tools.circleAndRect(towerX, towerY, towerRange, this.hitX - (this.w / 2), this.hitY - (this.h / 2), this.w, this.h);
    }

    public void addBuffFire() {
        this.buffFire = true;
    }

    public void removeBuffFire() {
        this.buffFire = false;
    }

    public boolean hasBuffFire() {
        return this.buffFire;
    }

    public void addBuffSlow(float value) {
        this.buffSlow = true;
        this.slowSpeed = (int) (((float) this.initSpeed) + (((float) this.initSpeed) * value));
    }

    public void removeBuffSlow() {
        this.buffSlow = false;
    }

    public boolean hasBuffSlow() {
        return this.buffSlow;
    }

    public void addBuffStop() {
        this.buffStop = true;
    }

    public void removeBuffStop() {
        this.buffStop = false;
    }

    public boolean hasBuffStop() {
        return this.buffStop;
    }
}
