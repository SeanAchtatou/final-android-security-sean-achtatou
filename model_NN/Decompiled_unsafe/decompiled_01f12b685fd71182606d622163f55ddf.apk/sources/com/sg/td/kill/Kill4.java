package com.sg.td.kill;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.td.actor.Buff;

public class Kill4 extends Kill {
    public void initEffect() {
        this.OneSecond = Animation.CurveTimeline.LINEAR;
        this.effect_id1 = 3;
        this.dieJia1 = true;
        this.dieJia2 = false;
        this.effect_id2 = 4;
        this.layer = 3;
        Rank.role.setStatus(7);
        Rank.role.setAttackType(17);
    }

    public void run(float delta) {
        super.run(delta);
        if (!Rank.isPause()) {
            this.time += ((float) Rank.getGameSpeed()) * delta;
            if ((this.OneSecond == Animation.CurveTimeline.LINEAR || this.OneSecond > 0.5f) && this.putNum < 10) {
                hurtEnemy();
                setNormalAim();
                shakeStage();
                this.OneSecond = Animation.CurveTimeline.LINEAR;
                this.putNum++;
            }
            this.OneSecond += ((float) Rank.getGameSpeed()) * delta;
            if (this.time > 5.0f) {
                free();
                System.out.println("结束");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void hurtEnemy() {
        killEnemy(GameConstant.SKILL1_EFFECT);
    }

    public void killEnemy(String eftType) {
        if (Rank.enemy != null) {
            for (int aimIndex : Rank.level.enemySort) {
                if (Rank.enemy.get(aimIndex).canAttack() && !Rank.enemy.get(aimIndex).boss) {
                    Rank.enemy.get(aimIndex).hurt(getSkillAttack(Rank.enemy.get(aimIndex).getHp() / 3), eftType, -1, getBearName());
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setNormalAim() {
        for (int index : Rank.level.enemySort) {
            if (!Rank.enemy.get(index).isFocus() && Rank.enemy.get(index).canAttack() && !Rank.enemy.get(index).boss) {
                addBuff(0, index);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addBuff(int aimType, int enemyIndex) {
        if (aimType == 0 && Rank.enemy.get(enemyIndex).isDead()) {
            return;
        }
        if (aimType != 1 || !Rank.deck.get(enemyIndex).isDead()) {
            new Buff().init(aimType, enemyIndex, 3, 0.02f, (float) getSkillAttack(), 3.0f, getBearName());
        }
    }

    public void exit() {
        Rank.role.setStatus(2);
        Rank.role.setAttackType(16);
    }
}
