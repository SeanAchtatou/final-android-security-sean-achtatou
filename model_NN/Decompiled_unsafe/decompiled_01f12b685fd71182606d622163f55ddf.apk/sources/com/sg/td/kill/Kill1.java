package com.sg.td.kill;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.td.Tools;
import com.sg.td.actor.Buff;

public class Kill1 extends Kill implements GameConstant {
    float OneSecond;
    float time;

    public void initEffect() {
        this.effect_id1 = 8;
        this.dieJia1 = true;
        this.effect_id2 = 7;
        this.dieJia2 = false;
        this.layer = 3;
        Rank.role.setStatus(7);
    }

    public void run(float delta) {
        super.run(delta);
        if (!Rank.isPause()) {
            this.time += ((float) Rank.getGameSpeed()) * delta;
            if ((this.OneSecond == Animation.CurveTimeline.LINEAR || this.OneSecond > 1.0f) && this.putNum < 6) {
                hurtEnemy();
                hurtRandomEnemy();
                shakeStage();
                this.OneSecond = Animation.CurveTimeline.LINEAR;
            }
            this.OneSecond += ((float) Rank.getGameSpeed()) * delta;
            if (this.time > 4.8f) {
                free();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void hurtEnemy() {
        killEnemy(GameConstant.SKILL1_EFFECT);
    }

    public void killEnemy(String eftType) {
        int value;
        if (Rank.enemy != null) {
            int num = 0;
            int i = 0;
            while (i < Rank.level.enemySort.length) {
                int aimIndex = Rank.level.enemySort[i];
                if (num < 10) {
                    if (Rank.enemy.get(aimIndex).canAttack() && !Rank.enemy.get(aimIndex).boss) {
                        num++;
                        if (Rank.enemy.get(aimIndex).isBossEnemy()) {
                            value = getSkillAttack(Rank.enemy.get(aimIndex).getHp() / 10);
                        } else {
                            value = getSkillAttack(Rank.enemy.get(aimIndex).getHp() / 6);
                        }
                        Rank.enemy.get(aimIndex).hurt(value, eftType, -1, getBearName());
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void hurtRandomEnemy() {
        setNormalAim();
    }

    /* access modifiers changed from: package-private */
    public void setNormalAim() {
        for (int i = 0; i < 3; i++) {
            int index = Tools.nextInt(Rank.level.enemySort.length);
            if (!Rank.enemy.get(index).isFocus() && Rank.enemy.get(index).canAttack() && !Rank.enemy.get(index).hasBuffFire() && !Rank.enemy.get(index).boss) {
                addBuff(0, index);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addBuff(int aimType, int enemyIndex) {
        if (aimType == 0 && Rank.enemy.get(enemyIndex).isDead()) {
            return;
        }
        if (aimType != 1 || !Rank.deck.get(enemyIndex).isDead()) {
            new Buff().init(aimType, enemyIndex, 3, 0.02f, 10.0f, 6.0f, getBearName());
        }
    }

    public void exit() {
        if (Rank.role != null) {
            Rank.role.setStatus(2);
        }
    }
}
