package com.sg.td.kill;

import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.td.Map;
import com.sg.td.Rank;
import com.sg.td.actor.Effect;
import com.sg.tools.MyGroup;
import com.sg.util.GameStage;

public class Kill extends MyGroup implements GameConstant {
    float OneSecond;
    boolean dieJia1;
    boolean dieJia2;
    int effect_id1 = -1;
    int effect_id2 = -1;
    int layer;
    int putNum;
    float time;

    public void initEffect() {
    }

    public void init() {
        initEffect();
        if (this.effect_id1 != -1) {
            if (this.dieJia1) {
                addActor(new Effect().getEffect_Diejia(this.effect_id1, Rank.role.x, Rank.role.y));
            } else {
                addActor(new Effect().getEffect(this.effect_id1, Rank.role.x, Rank.role.y));
            }
        }
        if (this.effect_id2 != -1) {
            if (this.dieJia2) {
                addActor(new Effect().getEffect_Diejia(this.effect_id2, 320, PAK_ASSETS.IMG_2X1));
            } else {
                addActor(new Effect().getEffect(this.effect_id2, 320, (int) PAK_ASSETS.IMG_2X1));
            }
        }
        GameStage.addActor(this, 3);
        Rank.kill = this;
    }

    /* access modifiers changed from: package-private */
    public int getSkillAttack() {
        return Rank.role.getDamage(GameConstant.SKILL_DAMAGE);
    }

    /* access modifiers changed from: package-private */
    public int getSkillAttack(int value) {
        return Rank.role.getDamage(value);
    }

    /* access modifiers changed from: package-private */
    public String getBearName() {
        return Rank.role.name;
    }

    public void run(float delta) {
        if (Rank.isWin() || Rank.isFail()) {
            free();
        }
    }

    public void free() {
        super.free();
        System.out.println("kill free...");
        Rank.kill = null;
    }

    public static void shakeStage() {
        Map.getShake(GameStage.getStage(), 320.0f, 568.0f);
    }

    public void exit() {
    }
}
