package com.sg.td.kill;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.td.actor.Buff;

public class Kill2 extends Kill implements GameConstant {
    float second;
    float time;

    public void initEffect() {
        this.effect_id1 = 1;
        this.dieJia1 = true;
        this.effect_id2 = -1;
        this.layer = 3;
        Rank.role.setStatus(7);
        Rank.role.setAttackType(11);
    }

    public void run(float delta) {
        super.run(delta);
        if (!Rank.isPause()) {
            this.time += ((float) Rank.getGameSpeed()) * delta;
            if ((this.second == Animation.CurveTimeline.LINEAR || this.second > 0.8f) && this.putNum < 6) {
                hurtRandomEnemy();
                shakeStage();
                this.second = Animation.CurveTimeline.LINEAR;
                this.putNum++;
            }
            this.second += ((float) Rank.getGameSpeed()) * delta;
            if (this.time > 5.0f) {
                free();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void hurtRandomEnemy() {
        setNormalAim();
    }

    /* access modifiers changed from: package-private */
    public void setNormalAim() {
        int value;
        int num = 0;
        int i = 0;
        while (i < Rank.level.enemySort.length) {
            int aimIndex = Rank.level.enemySort[i];
            if (num < 5) {
                if (Rank.enemy.get(aimIndex).canAttack() && !Rank.enemy.get(aimIndex).boss) {
                    num++;
                    System.out.println("num:" + num);
                    if (Rank.enemy.get(aimIndex).isBossEnemy()) {
                        value = getSkillAttack(Rank.enemy.get(aimIndex).getHp() / 6);
                    } else {
                        value = getSkillAttack((Rank.enemy.get(aimIndex).getHp() * 2) / 3);
                    }
                    System.out.println("Kill2 value:" + value);
                    Rank.enemy.get(aimIndex).hurt(value, null, -1, getBearName());
                    addBuff(0, aimIndex);
                }
                i++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addBuff(int aimType, int enemyIndex) {
        if ((aimType != 0 || !Rank.enemy.get(enemyIndex).isDead()) && aimType != 1) {
            new Buff().init(aimType, enemyIndex, 5, 0.02f, 10.0f, 5.0f, getBearName());
        }
    }

    public void exit() {
        Rank.role.setStatus(2);
        Rank.role.setAttackType(11);
    }
}
