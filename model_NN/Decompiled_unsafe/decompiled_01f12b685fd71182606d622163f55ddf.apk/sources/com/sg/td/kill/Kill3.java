package com.sg.td.kill;

import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.td.Rank;
import com.sg.td.Tools;
import com.sg.td.actor.Buff;

public class Kill3 extends Kill implements GameConstant {
    float spanTime = 0.3f;

    public void initEffect() {
        this.effect_id1 = 5;
        this.dieJia1 = true;
        this.effect_id2 = -1;
        this.layer = 3;
        Rank.role.setStatus(7);
        Rank.role.setAttackType(15);
    }

    public void run(float delta) {
        super.run(delta);
        if (!Rank.isPause()) {
            if (this.time == Animation.CurveTimeline.LINEAR) {
                shakeStage();
                stopEnemy();
            }
            this.time += ((float) Rank.getGameSpeed()) * delta;
            if (this.time > 1.0f) {
                if ((this.OneSecond == Animation.CurveTimeline.LINEAR || this.OneSecond > 0.5f) && this.putNum < 10) {
                    attack_random();
                    shakeStage();
                    this.OneSecond = Animation.CurveTimeline.LINEAR;
                    this.putNum++;
                }
                this.OneSecond += ((float) Rank.getGameSpeed()) * delta;
            }
            if (this.time > 5.0f) {
                free();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void stopEnemy() {
        if (Rank.level.enemySort != null) {
            int num = 0;
            int i = 0;
            while (i < Rank.level.enemySort.length) {
                int aimIndex = Rank.level.enemySort[i];
                if (num < 10) {
                    if (Rank.enemy.get(aimIndex).canAttack() && !Rank.enemy.get(aimIndex).boss) {
                        num++;
                        new Buff().init(0, aimIndex, 6, 1.0f, 10.0f, 6.0f, getBearName());
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void attack_random() {
        int value;
        if (Rank.level.enemySort != null) {
            int[] aim = new int[Rank.level.enemySort.length];
            for (int i = 0; i < Rank.level.enemySort.length; i++) {
                int aimIndex = Rank.level.enemySort[i];
                if (!Rank.enemy.get(aimIndex).canAttack()) {
                    aim[i] = -1;
                } else if (Rank.enemy.get(aimIndex).boss) {
                    aim[i] = -1;
                } else {
                    aim[i] = aimIndex;
                }
            }
            int num = Math.min(finish(aim), Tools.nextInt(2, 3));
            for (int j = 0; j < num; j++) {
                int index = Tools.nextInt(aim.length);
                while (aim[index] == -1) {
                    index = Tools.nextInt(aim.length);
                }
                if (Rank.enemy.get(index).isBossEnemy()) {
                    value = getSkillAttack(Rank.enemy.get(index).getHp() / 5);
                } else {
                    value = getSkillAttack((Rank.enemy.get(index).getHp() * 4) / 5);
                }
                Rank.role.attackEnemy_Bear3(aim[index], value);
            }
        }
    }

    static int finish(int[] aim) {
        int num = 0;
        for (int i : aim) {
            if (i != -1) {
                num++;
            }
        }
        return num;
    }

    public void exit() {
        Rank.role.setStatus(2);
        Rank.role.setAttackType(12);
    }
}
