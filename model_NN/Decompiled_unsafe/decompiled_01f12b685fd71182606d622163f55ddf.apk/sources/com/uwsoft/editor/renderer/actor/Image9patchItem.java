package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.Body;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.Image9patchVO;

public class Image9patchItem extends ImageItem implements IBaseItem {
    private Body body;
    private Image9patchVO image9patchVO;
    private final int[] splits;

    public Image9patchItem(Image9patchVO vo, Essentials e) {
        this(vo, e, (TextureAtlas.AtlasRegion) e.rm.getTextureRegion(vo.imageName));
    }

    private Image9patchItem(Image9patchVO vo, Essentials e, TextureAtlas.AtlasRegion atlasRegion) {
        super(vo, e, new NinePatch(atlasRegion, atlasRegion.splits[0], atlasRegion.splits[1], atlasRegion.splits[2], atlasRegion.splits[3]));
        this.splits = atlasRegion.splits;
        this.image9patchVO = vo;
    }

    public Image9patchItem(Image9patchVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        this.image9patchVO.width = this.image9patchVO.width == Animation.CurveTimeline.LINEAR ? getWidth() : this.image9patchVO.width;
        this.image9patchVO.height = this.image9patchVO.height == Animation.CurveTimeline.LINEAR ? getHeight() : this.image9patchVO.height;
        setParentItem(parent);
        setWidth(this.image9patchVO.width);
        setHeight(this.image9patchVO.height);
    }

    public void setScaleX(float scaleX) {
        float value = getWidth() * scaleX;
        if (this.splits != null && value < ((float) (this.splits[0] + this.splits[1]))) {
            value = (float) (this.splits[0] + this.splits[1]);
        }
        setWidth(value);
    }

    public void setScaleY(float scaleY) {
        float value = getHeight() * scaleY;
        if (this.splits != null && value < ((float) (this.splits[2] + this.splits[3]))) {
            value = (float) (this.splits[2] + this.splits[3]);
        }
        setHeight(value);
    }

    public float getMinWidth() {
        return (float) (this.splits[0] + this.splits[1]);
    }

    public float getMinHeight() {
        return (float) (this.splits[0] + this.splits[1]);
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public void setScale(float scaleX, float scaleY) {
        setScaleX(scaleX);
        setScaleY(scaleY);
    }

    public void renew() {
        setWidth(this.image9patchVO.width * this.mulX);
        setHeight(this.image9patchVO.height * this.mulY);
        super.renew();
    }

    public void updateDataVO() {
        this.image9patchVO.width = getWidth() / this.mulX;
        this.image9patchVO.height = getHeight() / this.mulY;
        super.updateDataVO();
    }

    public void applyResolution(float mulX, float mulY) {
        Image9patchVO image9patchVO2 = (Image9patchVO) this.dataVO;
        setWidth(image9patchVO2.width * mulX);
        setHeight(image9patchVO2.height * mulY);
        super.applyResolution(mulX, mulY);
    }
}
