package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.LabelVO;
import com.uwsoft.editor.renderer.resources.IResourceRetriever;
import com.uwsoft.editor.renderer.utils.CustomVariables;

public class LabelItem extends Label implements IBaseItem {
    private static BitmapFont font;
    private static int labelDefaultSize = 12;
    private static Label.LabelStyle style;
    private Body body;
    private CustomVariables customVariables;
    public LabelVO dataVO;
    public Essentials essentials;
    private String fontName;
    private int fontSize;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;

    public LabelItem(LabelVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        setParentItem(parent);
    }

    public LabelItem(LabelVO vo, Essentials e) {
        super(vo.text, generateStyle(e.rm, vo.style, vo.size));
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.customVariables = new CustomVariables();
        this.dataVO = vo;
        this.essentials = e;
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        setRotation(this.dataVO.rotation);
        if (this.dataVO.zIndex < 0) {
            this.dataVO.zIndex = 0;
        }
        if (this.dataVO.tint == null) {
            setTint(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        } else {
            setTint(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        }
        if (this.dataVO.align == 0) {
            setAlignment(1);
        } else {
            setAlignment(this.dataVO.align);
        }
        if (this.dataVO.width == Animation.CurveTimeline.LINEAR) {
            this.dataVO.width = getTextBounds().width / this.mulX;
        }
        if (this.dataVO.height == Animation.CurveTimeline.LINEAR) {
            this.dataVO.height = getTextBounds().height / this.mulY;
        }
        renew();
    }

    private static Label.LabelStyle generateStyle(IResourceRetriever rManager, String fontName2, int size) {
        if (size == 0) {
            size = labelDefaultSize;
        }
        style = new Label.LabelStyle(rManager.getBitmapFont(fontName2, size), null);
        return style;
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public LabelVO getDataVO() {
        return this.dataVO;
    }

    public void renew() {
        if (this.fontName == null || !this.fontName.equals(this.dataVO.style) || this.fontSize != this.dataVO.size) {
            if (this.dataVO.size == 0) {
                this.dataVO.size = labelDefaultSize;
            }
            font = this.essentials.rm.getBitmapFont(this.dataVO.style, this.dataVO.size);
            style = new Label.LabelStyle(font, null);
            setStyle(style);
            this.fontName = this.dataVO.style;
            this.fontSize = this.dataVO.size;
            if (this.dataVO.width == Animation.CurveTimeline.LINEAR) {
                this.dataVO.width = getTextBounds().width / this.mulX;
            }
            if (this.dataVO.height == Animation.CurveTimeline.LINEAR) {
                this.dataVO.height = getTextBounds().height / this.mulY;
            }
        }
        setText(this.dataVO.text);
        this.customVariables.loadFromString(this.dataVO.customVars);
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setRotation(this.dataVO.rotation);
        setColor(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]);
        setScale(this.dataVO.scaleX, this.dataVO.scaleY);
        setAlignment(this.dataVO.align);
        setWidth(this.dataVO.width * this.mulY);
        setHeight(this.dataVO.height * this.mulY);
        setWrap(true);
    }

    public float getScaleX() {
        return getFontScaleX();
    }

    public float getScaleY() {
        return getFontScaleY();
    }

    public void setScaleX(float x) {
        if (x > Animation.CurveTimeline.LINEAR) {
            setFontScaleX(x);
        }
    }

    public void setScaleY(float y) {
        if (y > Animation.CurveTimeline.LINEAR) {
            setFontScaleY(y);
        }
    }

    public void setScale(float x, float y) {
        if (x > Animation.CurveTimeline.LINEAR && y > Animation.CurveTimeline.LINEAR) {
            setFontScale(x, y);
        }
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void updateDataVO() {
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.customVars = this.customVariables.saveAsString();
        this.dataVO.scaleX = getScaleX();
        this.dataVO.scaleY = getScaleY();
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setScale(this.dataVO.scaleX, this.dataVO.scaleY);
        setWidth(this.dataVO.width * mulX2);
        setHeight(this.dataVO.height * mulY2);
        updateDataVO();
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public void setStyle(String styleName, int size) {
        this.dataVO.style = styleName;
        this.dataVO.size = size;
    }

    public void setAlign(int align) {
        this.dataVO.align = align;
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }
}
