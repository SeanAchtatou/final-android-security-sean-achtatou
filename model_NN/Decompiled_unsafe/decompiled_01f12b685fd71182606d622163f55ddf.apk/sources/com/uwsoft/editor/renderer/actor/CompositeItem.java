package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.CompositeItemVO;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.Image9patchVO;
import com.uwsoft.editor.renderer.data.LayerItemVO;
import com.uwsoft.editor.renderer.data.MainItemVO;
import com.uwsoft.editor.renderer.data.PhysicsBodyDataVO;
import com.uwsoft.editor.renderer.data.ResolutionEntryVO;
import com.uwsoft.editor.renderer.data.SimpleImageVO;
import com.uwsoft.editor.renderer.physics.PhysicsBodyLoader;
import com.uwsoft.editor.renderer.script.IScript;
import com.uwsoft.editor.renderer.utils.CustomVariables;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

public class CompositeItem extends Group implements IBaseItem {
    private Comparator<IBaseItem> ZIndexComparator;
    private Body body;
    private CustomVariables customVariables;
    public CompositeItemVO dataVO;
    public Essentials essentials;
    private boolean isLockedByLayer;
    private HashMap<String, IBaseItem> itemIdMap;
    private HashMap<String, ArrayList<IBaseItem>> itemLayerMap;
    private ArrayList<IBaseItem> items;
    protected int layerIndex;
    private HashMap<String, LayerItemVO> layerMap;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;
    private Rectangle scissorBounds;
    private ArrayList<IScript> scripts;

    public CompositeItem(CompositeItemVO vo, Essentials essentials2, CompositeItem parent) {
        this(vo, essentials2);
        setParentItem(parent);
    }

    public CompositeItem(CompositeItemVO vo, Essentials essentials2) {
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.items = new ArrayList<>();
        this.itemIdMap = new HashMap<>();
        this.layerMap = new HashMap<>();
        this.itemLayerMap = new HashMap<>();
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.customVariables = new CustomVariables();
        this.ZIndexComparator = new Comparator<IBaseItem>() {
            public int compare(IBaseItem obj1, IBaseItem obj2) {
                return obj1.getLayerIndex() == obj2.getLayerIndex() ? obj1.getDataVO().zIndex - obj2.getDataVO().zIndex : obj1.getLayerIndex() - obj2.getLayerIndex();
            }
        };
        this.scripts = new ArrayList<>(3);
        this.essentials = essentials2;
        this.dataVO = vo;
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        setScaleX(this.dataVO.scaleX);
        setScaleY(this.dataVO.scaleY);
        this.customVariables.loadFromString(this.dataVO.customVars);
        setRotation(this.dataVO.rotation);
        if (this.dataVO.zIndex < 0) {
            this.dataVO.zIndex = 0;
        }
        if (this.dataVO.tint == null) {
            setTint(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        } else {
            setTint(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        }
        this.dataVO = vo;
        reAssemble();
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public void addScript(IScript iScript) {
        this.scripts.add(iScript);
        iScript.init(this);
    }

    public void addScript(ArrayList<IScript> iScripts) {
        this.scripts.addAll(iScripts);
        Iterator i$ = iScripts.iterator();
        while (i$.hasNext()) {
            i$.next().init(this);
        }
    }

    public void addScriptTo(String name, ArrayList<IScript> iScripts) {
        Iterator i$ = this.items.iterator();
        while (i$.hasNext()) {
            IBaseItem itm = i$.next();
            if (itm instanceof CompositeItem) {
                if (itm.getDataVO().itemName.equals(name)) {
                    ((CompositeItem) itm).addScript(iScripts);
                }
                ((CompositeItem) itm).addScriptTo(name, iScripts);
            }
        }
    }

    public void addScriptTo(String name, IScript iScript) {
        Iterator i$ = this.items.iterator();
        while (i$.hasNext()) {
            IBaseItem itm = i$.next();
            if (itm instanceof CompositeItem) {
                if (itm.getDataVO().itemName.equals(name)) {
                    ((CompositeItem) itm).addScript(iScript);
                }
                ((CompositeItem) itm).addScriptTo(name, iScript);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.uwsoft.editor.renderer.script.IScript} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addScriptTo(java.lang.String r7, java.lang.Class<? extends com.uwsoft.editor.renderer.script.IScript> r8) {
        /*
            r6 = this;
            java.util.ArrayList<com.uwsoft.editor.renderer.actor.IBaseItem> r5 = r6.items
            java.util.Iterator r2 = r5.iterator()
        L_0x0006:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x003e
            java.lang.Object r4 = r2.next()
            com.uwsoft.editor.renderer.actor.IBaseItem r4 = (com.uwsoft.editor.renderer.actor.IBaseItem) r4
            boolean r5 = r4 instanceof com.uwsoft.editor.renderer.actor.CompositeItem
            if (r5 == 0) goto L_0x0006
            com.uwsoft.editor.renderer.data.MainItemVO r5 = r4.getDataVO()
            java.lang.String r5 = r5.itemName
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0033
            r3 = 0
            java.lang.Object r5 = com.badlogic.gdx.utils.reflect.ClassReflection.newInstance(r8)     // Catch:{ ReflectionException -> 0x0039 }
            r0 = r5
            com.uwsoft.editor.renderer.script.IScript r0 = (com.uwsoft.editor.renderer.script.IScript) r0     // Catch:{ ReflectionException -> 0x0039 }
            r3 = r0
        L_0x002b:
            if (r3 == 0) goto L_0x0033
            r5 = r4
            com.uwsoft.editor.renderer.actor.CompositeItem r5 = (com.uwsoft.editor.renderer.actor.CompositeItem) r5
            r5.addScript(r3)
        L_0x0033:
            com.uwsoft.editor.renderer.actor.CompositeItem r4 = (com.uwsoft.editor.renderer.actor.CompositeItem) r4
            r4.addScriptTo(r7, r8)
            goto L_0x0006
        L_0x0039:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002b
        L_0x003e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uwsoft.editor.renderer.actor.CompositeItem.addScriptTo(java.lang.String, java.lang.Class):void");
    }

    public void removeScript(IScript iScript) {
        this.scripts.remove(iScript);
        iScript.dispose();
    }

    public void removeScript(ArrayList<IScript> iScripts) {
        this.scripts.removeAll(iScripts);
        Iterator i$ = iScripts.iterator();
        while (i$.hasNext()) {
            i$.next().dispose();
        }
    }

    public void removeScript(String name, ArrayList<IScript> iScripts) {
        Iterator i$ = this.items.iterator();
        while (i$.hasNext()) {
            IBaseItem itm = i$.next();
            if (itm instanceof CompositeItem) {
                if (itm.getDataVO().itemName.equals(name)) {
                    ((CompositeItem) itm).removeScript(iScripts);
                }
                ((CompositeItem) itm).removeScript(name, iScripts);
            }
        }
    }

    private void reAssemble() {
        clear();
        if (this.items != null) {
            for (int i = 0; i < this.items.size(); i++) {
                this.items.get(i).dispose();
            }
        }
        this.items.clear();
        this.itemIdMap.clear();
        this.itemLayerMap.clear();
        for (int i2 = 0; i2 < this.dataVO.composite.sImages.size(); i2++) {
            SimpleImageVO tmpVo = this.dataVO.composite.sImages.get(i2);
            ImageItem itm = new ImageItem(tmpVo, this.essentials, this);
            inventorize(itm);
            addActor(itm);
            itm.setZIndex(tmpVo.zIndex);
        }
        for (int i3 = 0; i3 < this.dataVO.composite.sImage9patchs.size(); i3++) {
            Image9patchVO tmpVo2 = this.dataVO.composite.sImage9patchs.get(i3);
            Image9patchItem itm2 = new Image9patchItem(tmpVo2, this.essentials, this);
            inventorize(itm2);
            addActor(itm2);
            itm2.setZIndex(tmpVo2.zIndex);
        }
        for (int i4 = 0; i4 < this.dataVO.composite.sTextBox.size(); i4++) {
            TextBoxItem itm3 = new TextBoxItem(this.dataVO.composite.sTextBox.get(i4), this.essentials, this);
            inventorize(itm3);
            addActor(itm3);
            itm3.setZIndex(itm3.dataVO.zIndex);
        }
        for (int i5 = 0; i5 < this.dataVO.composite.sButtons.size(); i5++) {
            TextButtonItem itm4 = new TextButtonItem(this.dataVO.composite.sButtons.get(i5), this.essentials, this);
            inventorize(itm4);
            addActor(itm4);
            itm4.setZIndex(itm4.dataVO.zIndex);
        }
        for (int i6 = 0; i6 < this.dataVO.composite.sLabels.size(); i6++) {
            LabelItem itm5 = new LabelItem(this.dataVO.composite.sLabels.get(i6), this.essentials, this);
            inventorize(itm5);
            addActor(itm5);
            itm5.setZIndex(itm5.dataVO.zIndex);
        }
        for (int i7 = 0; i7 < this.dataVO.composite.sCheckBoxes.size(); i7++) {
            CheckBoxItem itm6 = new CheckBoxItem(this.dataVO.composite.sCheckBoxes.get(i7), this.essentials, this);
            inventorize(itm6);
            addActor(itm6);
            itm6.setZIndex(itm6.dataVO.zIndex);
        }
        for (int i8 = 0; i8 < this.dataVO.composite.sSelectBoxes.size(); i8++) {
            SelectBoxItem itm7 = new SelectBoxItem(this.dataVO.composite.sSelectBoxes.get(i8), this.essentials, this);
            inventorize(itm7);
            addActor(itm7);
            itm7.setZIndex(itm7.dataVO.zIndex);
        }
        for (int i9 = 0; i9 < this.dataVO.composite.sComposites.size(); i9++) {
            CompositeItem itm8 = new CompositeItem(this.dataVO.composite.sComposites.get(i9), this.essentials, this);
            inventorize(itm8);
            addActor(itm8);
            itm8.setZIndex(itm8.dataVO.zIndex);
        }
        for (int i10 = 0; i10 < this.dataVO.composite.sParticleEffects.size(); i10++) {
            ParticleItem itm9 = new ParticleItem(this.dataVO.composite.sParticleEffects.get(i10), this.essentials, this);
            inventorize(itm9);
            addActor(itm9);
            itm9.setZIndex(itm9.dataVO.zIndex);
        }
        if (this.essentials.rayHandler != null) {
            for (int i11 = 0; i11 < this.dataVO.composite.sLights.size(); i11++) {
                LightActor itm10 = new LightActor(this.dataVO.composite.sLights.get(i11), this.essentials, this);
                inventorize(itm10);
                addActor(itm10);
            }
        }
        if (this.essentials.spineReflectionHelper != null) {
            for (int i12 = 0; i12 < this.dataVO.composite.sSpineAnimations.size(); i12++) {
                SpineActor itm11 = new SpineActor(this.dataVO.composite.sSpineAnimations.get(i12), this.essentials, this);
                inventorize(itm11);
                addActor(itm11);
                itm11.setZIndex(itm11.dataVO.zIndex);
            }
        }
        for (int i13 = 0; i13 < this.dataVO.composite.sSpriteAnimations.size(); i13++) {
            SpriteAnimation itm12 = new SpriteAnimation(this.dataVO.composite.sSpriteAnimations.get(i13), this.essentials, this);
            inventorize(itm12);
            itm12.start();
            addActor(itm12);
            itm12.setZIndex(itm12.dataVO.zIndex);
        }
        for (int i14 = 0; i14 < this.dataVO.composite.sSpriterAnimations.size(); i14++) {
            SpriterActor itm13 = new SpriterActor(this.dataVO.composite.sSpriterAnimations.get(i14), this.essentials, this);
            inventorize(itm13);
            addActor(itm13);
            itm13.setZIndex(itm13.dataVO.zIndex);
        }
        if (this.dataVO.composite.layers.size() == 0) {
            LayerItemVO layerVO = new LayerItemVO();
            layerVO.layerName = "Default";
            this.dataVO.composite.layers.add(layerVO);
        }
        recalculateSize();
        sortZindexes();
        reAssembleLayers();
    }

    /* access modifiers changed from: protected */
    public void setStage(Stage stage) {
        super.setStage(stage);
        if (stage != null) {
            assemblePhysics();
        }
    }

    private void inventorize(IBaseItem itm) {
        String id = itm.getDataVO().itemIdentifier;
        String layerName = itm.getDataVO().layerName;
        if (id.length() > 0) {
            this.itemIdMap.put(id, itm);
        }
        if (layerName.length() > 0) {
            if (!this.itemLayerMap.containsKey(layerName)) {
                this.itemLayerMap.put(layerName, new ArrayList());
            }
            this.itemLayerMap.get(layerName).add(itm);
        }
        this.items.add(itm);
    }

    public ArrayList<CompositeItem> findItemsByName(String itemName) {
        return findItemsByName(itemName, this, new ArrayList<>());
    }

    private ArrayList<CompositeItem> findItemsByName(String itemName, CompositeItem itm, ArrayList<CompositeItem> recursiveArray) {
        for (int i = 0; i < itm.items.size(); i++) {
            if (itm.items.get(i).getClass().getSimpleName().equals("CompositeItem")) {
                CompositeItem tmp = (CompositeItem) itm.items.get(i);
                if (tmp.dataVO.itemName.equals(itemName)) {
                    recursiveArray.add(tmp);
                }
                findItemsByName(itemName, tmp, recursiveArray);
            }
        }
        return recursiveArray;
    }

    public CheckBoxItem getCheckBoxById(String itemId) {
        return (CheckBoxItem) this.itemIdMap.get(itemId);
    }

    public CompositeItem getCompositeById(String itemId) {
        return (CompositeItem) this.itemIdMap.get(itemId);
    }

    public IBaseItem getItemById(String itemId) {
        return this.itemIdMap.get(itemId);
    }

    public ImageItem getImageById(String itemId) {
        return (ImageItem) this.itemIdMap.get(itemId);
    }

    public LabelItem getLabelById(String itemId) {
        return (LabelItem) this.itemIdMap.get(itemId);
    }

    public ParticleItem getParticleById(String itemId) {
        return (ParticleItem) this.itemIdMap.get(itemId);
    }

    public SelectBoxItem getSelectBoxById(String itemId) {
        return (SelectBoxItem) this.itemIdMap.get(itemId);
    }

    public TextBoxItem getTextBoxById(String itemId) {
        return (TextBoxItem) this.itemIdMap.get(itemId);
    }

    public TextButtonItem getTextButtonById(String itemId) {
        return (TextButtonItem) this.itemIdMap.get(itemId);
    }

    public LightActor getLightActorById(String itemId) {
        return (LightActor) this.itemIdMap.get(itemId);
    }

    public SpriteAnimation getSpriteAnimationById(String itemId) {
        return (SpriteAnimation) this.itemIdMap.get(itemId);
    }

    public SpriterActor getSpriterActorById(String itemId) {
        return (SpriterActor) this.itemIdMap.get(itemId);
    }

    public SpineActor getSpineActorById(String itemId) {
        return (SpineActor) this.itemIdMap.get(itemId);
    }

    public Image9patchItem getNinePatchById(String itemId) {
        return (Image9patchItem) this.itemIdMap.get(itemId);
    }

    public void addItem(IBaseItem item) {
        this.dataVO.composite.addItem(item.getDataVO());
        item.setParentItem(this);
        inventorize(item);
        addActor((Actor) item);
        item.updateDataVO();
        item.applyResolution(this.mulX, this.mulY);
        recalculateSize();
        sortZindexes();
        reAssembleLayers();
        if (item.getDataVO().physicsBodyData != null && Integer.parseInt(item.getDataVO().meshId) >= 0) {
            Vector2 toStageVec = new Vector2();
            toStageVec.set(item.getDataVO().x * this.mulX, item.getDataVO().y * this.mulY);
            localToStageCoordinates(toStageVec);
            toStageVec.scl(0.1f);
            if (item.getBody() != null) {
                this.essentials.world.destroyBody(item.getBody());
                item.setBody(null);
            }
            item.setBody(PhysicsBodyLoader.createBody(this.essentials.world, item.getDataVO().physicsBodyData, this.essentials.rm.getProjectVO().meshes.get(item.getDataVO().meshId), new Vector2(this.mulX, this.mulY)));
            item.getBody().setTransform(toStageVec.x, toStageVec.y, (float) Math.toRadians((double) item.getDataVO().rotation));
        }
    }

    public void loadFromVO(CompositeItemVO vo) {
        this.dataVO = vo;
        reAssemble();
        recalculateSize();
        sortZindexes();
        reAssembleLayers();
    }

    public ArrayList<IBaseItem> getItems() {
        return this.items;
    }

    public void reAssembleLayers() {
        this.layerMap.clear();
        for (int i = 0; i < this.dataVO.composite.layers.size(); i++) {
            LayerItemVO layer = this.dataVO.composite.layers.get(i);
            setLayerChildrenVisibilty(layer.layerName, layer.isVisible);
            setLayerChildrenLock(layer.layerName, layer.isLocked || !layer.isVisible);
            this.layerMap.put(layer.layerName, layer);
        }
    }

    private void assemblePhysics() {
        if (this.essentials.world != null) {
            Vector2 mulVec = new Vector2(this.mulX, this.mulY);
            Iterator i$ = this.items.iterator();
            while (i$.hasNext()) {
                IBaseItem item = i$.next();
                if (item.getBody() != null) {
                    this.essentials.world.destroyBody(item.getBody());
                    item.setBody(null);
                }
                MainItemVO itemVO = item.getDataVO();
                PhysicsBodyDataVO bodyData = itemVO.physicsBodyData;
                if (!(Integer.parseInt(itemVO.meshId) < 0 || bodyData == null || this.essentials.rm.getProjectVO().meshes.get(itemVO.meshId) == null)) {
                    item.setBody(PhysicsBodyLoader.createBody(this.essentials.world, bodyData, this.essentials.rm.getProjectVO().meshes.get(itemVO.meshId), mulVec));
                    item.getBody().setUserData(item);
                }
            }
            positionPhysics();
        }
    }

    public void positionPhysics() {
        Vector2 toStageVec = new Vector2();
        Iterator i$ = this.items.iterator();
        while (i$.hasNext()) {
            IBaseItem item = i$.next();
            if (item.getBody() != null) {
                MainItemVO itemVO = item.getDataVO();
                toStageVec.set(itemVO.x * this.mulX, itemVO.y * this.mulY);
                localToStageCoordinates(toStageVec);
                toStageVec.scl(0.1f);
                item.getBody().setTransform(toStageVec.x, toStageVec.y, (float) Math.toRadians((double) item.getDataVO().rotation));
            }
        }
    }

    public void recalculateSize() {
        float lowerX = Animation.CurveTimeline.LINEAR;
        float lowerY = Animation.CurveTimeline.LINEAR;
        float upperX = Animation.CurveTimeline.LINEAR;
        float upperY = Animation.CurveTimeline.LINEAR;
        for (int i = 0; i < this.items.size(); i++) {
            Actor value = (Actor) this.items.get(i);
            if (i == 0) {
                if (value.getScaleX() <= Animation.CurveTimeline.LINEAR || value.getWidth() * value.getScaleX() <= Animation.CurveTimeline.LINEAR) {
                    upperX = value.getX();
                    lowerX = value.getX() + (value.getWidth() * value.getScaleX());
                } else {
                    lowerX = value.getX();
                    upperX = value.getX() + (value.getWidth() * value.getScaleX());
                }
                if (value.getScaleY() <= Animation.CurveTimeline.LINEAR || value.getHeight() * value.getScaleY() <= Animation.CurveTimeline.LINEAR) {
                    upperY = value.getY();
                    lowerY = value.getY() + (value.getHeight() * value.getScaleY());
                } else {
                    lowerY = value.getY();
                    upperY = value.getY() + (value.getHeight() * value.getScaleY());
                }
            }
            if (value.getScaleX() <= Animation.CurveTimeline.LINEAR || value.getWidth() <= Animation.CurveTimeline.LINEAR) {
                if (upperX < value.getX()) {
                    upperX = value.getX();
                }
                if (lowerX > value.getX() + (value.getWidth() * value.getScaleX())) {
                    lowerX = value.getX() + (value.getWidth() * value.getScaleX());
                }
            } else {
                if (lowerX > value.getX()) {
                    lowerX = value.getX();
                }
                if (upperX < value.getX() + (value.getWidth() * value.getScaleX())) {
                    upperX = value.getX() + (value.getWidth() * value.getScaleX());
                }
            }
            if (value.getScaleY() <= Animation.CurveTimeline.LINEAR || value.getHeight() * value.getScaleY() <= Animation.CurveTimeline.LINEAR) {
                if (upperY < value.getY()) {
                    upperY = value.getY();
                }
                if (lowerY > value.getY() + (value.getHeight() * value.getScaleY())) {
                    lowerY = value.getY() + (value.getHeight() * value.getScaleY());
                }
            } else {
                if (lowerY > value.getY()) {
                    lowerY = value.getY();
                }
                if (upperY < value.getY() + (value.getHeight() * value.getScaleY())) {
                    upperY = value.getY() + (value.getHeight() * value.getScaleY());
                }
            }
        }
        setWidth(upperX - Animation.CurveTimeline.LINEAR);
        setHeight(upperY - Animation.CurveTimeline.LINEAR);
    }

    public void sortZindexes() {
        if (this.items != null) {
            for (int i = 0; i < this.items.size(); i++) {
                this.items.get(i).setLayerIndex(getlayerIndexByName(this.items.get(i).getDataVO().layerName));
            }
            Collections.sort(this.items, this.ZIndexComparator);
            for (int i2 = 0; i2 < this.items.size(); i2++) {
                IBaseItem value = this.items.get(i2);
                if (value.getDataVO().zIndex < 0) {
                    value.getDataVO().zIndex = 0;
                }
                ((Actor) value).setZIndex(i2);
                value.getDataVO().zIndex = i2;
            }
        }
    }

    private int getlayerIndexByName(String layerName) {
        ArrayList<LayerItemVO> layers = this.dataVO.composite.layers;
        for (int i = 0; i < layers.size(); i++) {
            if (layers.get(i).layerName.equals(layerName)) {
                return i;
            }
        }
        return 0;
    }

    public CompositeItemVO getDataVO() {
        return this.dataVO;
    }

    public void removeItem(IBaseItem item) {
        this.items.remove(item);
        this.dataVO.composite.removeItem(item.getDataVO());
        item.dispose();
    }

    public boolean isComposite() {
        return true;
    }

    public void applyResolution(String resolutionName) {
        ResolutionEntryVO curResolution;
        if (resolutionName != null && !resolutionName.isEmpty() && (curResolution = this.essentials.rm.getProjectVO().getResolution(resolutionName)) != null) {
            float mulX2 = ((float) curResolution.width) / ((float) this.essentials.rm.getProjectVO().originalResolution.width);
            applyResolution(mulX2, mulX2);
        }
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        for (int i = 0; i < this.items.size(); i++) {
            this.items.get(i).applyResolution(mulX2, mulY2);
            if (this.items.get(i).getBody() != null) {
                this.essentials.world.destroyBody(this.items.get(i).getBody());
                this.items.get(i).setBody(null);
            }
        }
        if (getBody() != null) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        updateDataVO();
        recalculateSize();
        assemblePhysics();
    }

    public void act(float delta) {
        for (int i = 0; i < this.scripts.size(); i++) {
            this.scripts.get(i).act(delta);
        }
        if (!(this.essentials.world == null || this.body == null || this.dataVO.physicsBodyData == null || this.dataVO.physicsBodyData.bodyType <= 0 || this.essentials.physicsStopped)) {
            setX(this.body.getPosition().x / 0.1f);
            setY(this.body.getPosition().y / 0.1f);
            setRotation(this.body.getAngle() * 57.295776f);
        }
        super.act(delta);
    }

    public void setScissors(float x, float y, float w, float h) {
        this.dataVO.scissorX = x / this.mulX;
        this.dataVO.scissorY = y / this.mulY;
        this.dataVO.scissorWidth = w / this.mulX;
        this.dataVO.scissorHeight = h / this.mulY;
        this.scissorBounds = new Rectangle(this.dataVO.scissorX * this.mulX, this.dataVO.scissorY * this.mulY, this.dataVO.scissorWidth * this.mulX, this.dataVO.scissorHeight * this.mulY);
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.dataVO.scissorWidth <= Animation.CurveTimeline.LINEAR || this.dataVO.scissorHeight <= Animation.CurveTimeline.LINEAR) {
            super.draw(batch, parentAlpha);
            return;
        }
        if (isTransform()) {
            applyTransform(batch, computeTransform());
        }
        Rectangle calculatedScissorBounds = (Rectangle) Pools.obtain(Rectangle.class);
        getStage().calculateScissors(this.scissorBounds, calculatedScissorBounds);
        if (ScissorStack.pushScissors(calculatedScissorBounds)) {
            drawChildren(batch, parentAlpha);
            ScissorStack.popScissors();
            if (isTransform()) {
                resetTransform(batch);
            }
            Pools.free(calculatedScissorBounds);
        }
    }

    public boolean layerExists(String layerName) {
        return this.layerMap.containsKey(layerName);
    }

    public void setLayerVisibilty(String layerName, boolean visible) {
        LayerItemVO layer = this.layerMap.get(layerName);
        if (layer != null) {
            layer.isVisible = visible;
            setLayerChildrenVisibilty(layerName, visible);
        }
    }

    private void setLayerChildrenVisibilty(String layerName, boolean visible) {
        ArrayList<IBaseItem> items2 = this.itemLayerMap.get(layerName);
        if (items2 != null) {
            for (int i = 0; i < items2.size(); i++) {
                ((Actor) items2.get(i)).setVisible(visible);
            }
        }
    }

    public void setLayerLock(String layerName, boolean isLocked) {
        for (int i = 0; i < this.dataVO.composite.layers.size(); i++) {
            if (this.dataVO.composite.layers.get(i).layerName.equals(layerName)) {
                this.dataVO.composite.layers.get(i).isLocked = isLocked;
                setLayerChildrenLock(layerName, isLocked);
            }
        }
    }

    public void setLayerChildrenLock(String layerName, boolean isLocked) {
        for (int i = 0; i < this.items.size(); i++) {
            IBaseItem item = this.items.get(i);
            if (item.getDataVO().layerName.equals(layerName)) {
                item.setLockByLayer(isLocked);
                ((Actor) item).setTouchable(isLocked ? Touchable.disabled : Touchable.enabled);
            }
        }
    }

    public void renew() {
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setScaleX(this.dataVO.scaleX);
        setScaleY(this.dataVO.scaleY);
        setRotation(this.dataVO.rotation);
        setColor(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]);
        this.customVariables.loadFromString(this.dataVO.customVars);
        this.scissorBounds = new Rectangle(this.dataVO.scissorX * this.mulX, this.dataVO.scissorY * this.mulY, this.dataVO.scissorWidth * this.mulX, this.dataVO.scissorHeight * this.mulY);
    }

    public void updateDataVO() {
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        for (int i = 0; i < this.items.size(); i++) {
            this.items.get(i).updateDataVO();
        }
        sortZindexes();
        this.dataVO.scaleX = getScaleX();
        this.dataVO.scaleY = getScaleY();
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || this.body == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
        for (int i = 0; i < this.items.size(); i++) {
            this.items.get(i).dispose();
        }
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }

    public <T extends IBaseItem> T getById(String itemId, Class<T> itemType) {
        return (IBaseItem) itemType.cast(this.itemIdMap.get(itemId));
    }

    public ArrayList<IBaseItem> getItemsByLayerName(String layerName) {
        return this.itemLayerMap.get(layerName);
    }
}
