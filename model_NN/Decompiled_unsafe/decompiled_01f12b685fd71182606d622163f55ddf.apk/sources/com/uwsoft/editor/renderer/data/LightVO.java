package com.uwsoft.editor.renderer.data;

import com.kbz.esotericsoftware.spine.Animation;

public class LightVO extends MainItemVO {
    public float coneDegree = 30.0f;
    public float directionDegree = Animation.CurveTimeline.LINEAR;
    public float distance = 300.0f;
    public boolean isStatic = true;
    public boolean isXRay = true;
    public int rays = 12;
    public LightType type;

    public enum LightType {
        POINT,
        CONE
    }

    public LightVO() {
        this.tint = new float[4];
        this.tint[0] = 1.0f;
        this.tint[1] = 1.0f;
        this.tint[2] = 1.0f;
        this.tint[3] = 1.0f;
    }

    public LightVO(LightVO vo) {
        super(vo);
        this.type = vo.type;
        this.rays = vo.rays;
        this.distance = vo.distance;
        this.directionDegree = vo.directionDegree;
        this.coneDegree = vo.coneDegree;
        this.isStatic = vo.isStatic;
        this.isXRay = vo.isXRay;
    }
}
