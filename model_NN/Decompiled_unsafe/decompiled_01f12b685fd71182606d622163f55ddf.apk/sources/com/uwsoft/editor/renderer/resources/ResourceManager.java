package com.uwsoft.editor.renderer.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Json;
import com.uwsoft.editor.renderer.data.CompositeItemVO;
import com.uwsoft.editor.renderer.data.CompositeVO;
import com.uwsoft.editor.renderer.data.ProjectInfoVO;
import com.uwsoft.editor.renderer.data.ResolutionEntryVO;
import com.uwsoft.editor.renderer.data.SceneVO;
import com.uwsoft.editor.renderer.utils.MySkin;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class ResourceManager implements IResourceLoader, IResourceRetriever {
    protected HashMap<FontSizePair, BitmapFont> bitmapFonts = new HashMap<>();
    public String fontsPath = "freetypefonts";
    protected HashSet<FontSizePair> fontsToLoad = new HashSet<>();
    protected HashMap<String, SceneVO> loadedSceneVOs = new HashMap<>();
    protected TextureAtlas mainPack;
    public String packResolutionName = "orig";
    protected HashSet<String> particleEffectNamesToLoad = new HashSet<>();
    protected HashMap<String, ParticleEffect> particleEffects = new HashMap<>();
    public String particleEffectsPath = "particles";
    protected ArrayList<String> preparedSceneNames = new ArrayList<>();
    protected ProjectInfoVO projectVO;
    protected float resMultiplier;
    public String scenesPath = "scenes";
    protected HashMap<String, TextureAtlas> skeletonAtlases = new HashMap<>();
    protected HashMap<String, FileHandle> skeletonJSON = new HashMap<>();
    protected HashSet<String> spineAnimNamesToLoad = new HashSet<>();
    public String spineAnimationsPath = "spine_animations";
    protected HashSet<String> spriteAnimNamesToLoad = new HashSet<>();
    protected HashMap<String, TextureAtlas> spriteAnimations = new HashMap<>();
    public String spriteAnimationsPath = "sprite_animations";
    protected HashSet<String> spriterAnimNamesToLoad = new HashSet<>();
    protected HashMap<String, FileHandle> spriterAnimations = new HashMap<>();
    public String spriterAnimationsPath = "spriter_animations";

    public void setWorkingResolution(String resolution) {
        this.packResolutionName = resolution;
    }

    public void initAllResources() {
        loadProjectVO();
        for (int i = 0; i < this.projectVO.scenes.size(); i++) {
            loadSceneVO(this.projectVO.scenes.get(i).sceneName);
            scheduleScene(this.projectVO.scenes.get(i).sceneName);
        }
        prepareAssetsToLoad();
        loadAssets();
    }

    public void initScene(String sceneName) {
        loadSceneVO(sceneName);
        scheduleScene(sceneName);
        prepareAssetsToLoad();
        loadAssets();
    }

    public void unLoadScene(String sceneName) {
        unScheduleScene(sceneName);
        this.loadedSceneVOs.remove(sceneName);
        loadAssets();
    }

    public void scheduleScene(String name) {
        if (this.loadedSceneVOs.containsKey(name)) {
            this.preparedSceneNames.add(name);
        }
    }

    public void unScheduleScene(String name) {
        this.preparedSceneNames.remove(name);
    }

    public void prepareAssetsToLoad() {
        this.particleEffectNamesToLoad.clear();
        this.spineAnimNamesToLoad.clear();
        this.spriteAnimNamesToLoad.clear();
        this.spriterAnimNamesToLoad.clear();
        this.fontsToLoad.clear();
        Iterator<String> it = this.preparedSceneNames.iterator();
        while (it.hasNext()) {
            String preparedSceneName = it.next();
            CompositeVO composite = this.loadedSceneVOs.get(preparedSceneName).composite;
            if (composite != null) {
                String[] particleEffects2 = composite.getRecursiveParticleEffectsList();
                String[] spineAnimations = composite.getRecursiveSpineAnimationList();
                String[] spriteAnimations2 = composite.getRecursiveSpriteAnimationList();
                String[] spriterAnimations2 = composite.getRecursiveSpriterAnimationList();
                FontSizePair[] fonts = composite.getRecursiveFontList();
                for (CompositeItemVO library : this.loadedSceneVOs.get(preparedSceneName).libraryItems.values()) {
                    Collections.addAll(this.fontsToLoad, library.composite.getRecursiveFontList());
                    Collections.addAll(this.particleEffectNamesToLoad, library.composite.getRecursiveParticleEffectsList());
                }
                Collections.addAll(this.particleEffectNamesToLoad, particleEffects2);
                Collections.addAll(this.spineAnimNamesToLoad, spineAnimations);
                Collections.addAll(this.spriteAnimNamesToLoad, spriteAnimations2);
                Collections.addAll(this.spriterAnimNamesToLoad, spriterAnimations2);
                Collections.addAll(this.fontsToLoad, fonts);
            }
        }
    }

    public void loadAssets() {
        loadAtlasPack();
        loadParticleEffects();
        loadSpineAnimations();
        loadSpriteAnimations();
        loadSpriterAnimations();
        loadFonts();
    }

    public void loadAtlasPack() {
        FileHandle packFile = Gdx.files.internal(this.packResolutionName + File.separator + "pack.atlas");
        if (packFile.exists()) {
            this.mainPack = new TextureAtlas(packFile);
        }
    }

    public void loadParticleEffects() {
        for (String key : this.particleEffects.keySet()) {
            if (!this.particleEffectNamesToLoad.contains(key)) {
                this.particleEffects.remove(key);
            }
        }
        Iterator i$ = this.particleEffectNamesToLoad.iterator();
        while (i$.hasNext()) {
            String name = i$.next();
            ParticleEffect effect = new ParticleEffect();
            effect.load(Gdx.files.internal(this.particleEffectsPath + File.separator + name), this.mainPack, "");
            this.particleEffects.put(name, effect);
        }
    }

    public void loadSpriteAnimations() {
        for (String key : this.spriteAnimations.keySet()) {
            if (!this.spriteAnimNamesToLoad.contains(key)) {
                this.spriteAnimations.remove(key);
            }
        }
        Iterator i$ = this.spriteAnimNamesToLoad.iterator();
        while (i$.hasNext()) {
            String name = i$.next();
            this.spriteAnimations.put(name, new TextureAtlas(Gdx.files.internal(this.packResolutionName + File.separator + this.spriteAnimationsPath + File.separator + name + File.separator + name + ".atlas")));
        }
    }

    public void loadSpriterAnimations() {
        for (String key : this.spriterAnimations.keySet()) {
            if (!this.spriterAnimNamesToLoad.contains(key)) {
                this.spriterAnimations.remove(key);
            }
        }
        Iterator i$ = this.spriterAnimNamesToLoad.iterator();
        while (i$.hasNext()) {
            String name = i$.next();
            this.spriterAnimations.put(name, Gdx.files.internal("orig" + File.separator + this.spriterAnimationsPath + File.separator + name + File.separator + name + ".scml"));
        }
    }

    public void loadSpineAnimation(String name) {
        this.skeletonAtlases.put(name, new TextureAtlas(Gdx.files.internal(this.packResolutionName + File.separator + this.spineAnimationsPath + File.separator + name + File.separator + name + ".atlas")));
        this.skeletonJSON.put(name, Gdx.files.internal("orig" + File.separator + this.spineAnimationsPath + File.separator + name + File.separator + name + ".json"));
    }

    public void loadSpineAnimations() {
        Iterator it = this.skeletonAtlases.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = it.next();
            if (this.spineAnimNamesToLoad.contains(pairs.getKey())) {
                this.spineAnimNamesToLoad.remove(pairs.getKey());
            } else {
                it.remove();
                this.skeletonJSON.remove(pairs.getKey());
            }
        }
        Iterator i$ = this.spineAnimNamesToLoad.iterator();
        while (i$.hasNext()) {
            loadSpineAnimation(i$.next());
        }
    }

    public void loadFonts() {
        ResolutionEntryVO curResolution = getProjectVO().getResolution(this.packResolutionName);
        this.resMultiplier = 1.0f;
        if (!this.packResolutionName.equals("orig")) {
            if (curResolution.base == 0) {
                this.resMultiplier = ((float) curResolution.width) / ((float) getProjectVO().originalResolution.width);
            } else {
                this.resMultiplier = ((float) curResolution.height) / ((float) getProjectVO().originalResolution.height);
            }
        }
        for (FontSizePair pair : this.bitmapFonts.keySet()) {
            if (!this.fontsToLoad.contains(pair)) {
                this.bitmapFonts.remove(pair);
            }
        }
        Iterator i$ = this.fontsToLoad.iterator();
        while (i$.hasNext()) {
            loadFont(i$.next());
        }
    }

    public void loadFont(FontSizePair pair) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(this.fontsPath + File.separator + pair.fontName + ".ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Math.round(((float) pair.fontSize) * this.resMultiplier);
        this.bitmapFonts.put(pair, generator.generateFont(parameter));
    }

    public SceneVO loadSceneVO(String sceneName) {
        SceneVO sceneVO = (SceneVO) new Json().fromJson(SceneVO.class, Gdx.files.internal(this.scenesPath + File.separator + sceneName + ".dt").readString());
        this.loadedSceneVOs.put(sceneName, sceneVO);
        return sceneVO;
    }

    public void unLoadSceneVO(String sceneName) {
        this.loadedSceneVOs.remove(sceneName);
    }

    public ProjectInfoVO loadProjectVO() {
        this.projectVO = (ProjectInfoVO) new Json().fromJson(ProjectInfoVO.class, Gdx.files.internal("project.dt").readString());
        return this.projectVO;
    }

    public TextureRegion getTextureRegion(String name) {
        return this.mainPack.findRegion(name);
    }

    public ParticleEffect getParticleEffect(String name) {
        return new ParticleEffect(this.particleEffects.get(name));
    }

    public TextureAtlas getSkeletonAtlas(String name) {
        return this.skeletonAtlases.get(name);
    }

    public FileHandle getSkeletonJSON(String name) {
        return this.skeletonJSON.get(name);
    }

    public TextureAtlas getSpriteAnimation(String name) {
        return this.spriteAnimations.get(name);
    }

    public BitmapFont getBitmapFont(String name, int size) {
        return this.bitmapFonts.get(new FontSizePair(name, size));
    }

    public MySkin getSkin() {
        return null;
    }

    public SceneVO getSceneVO(String sceneName) {
        return this.loadedSceneVOs.get(sceneName);
    }

    public ProjectInfoVO getProjectVO() {
        return this.projectVO;
    }

    public void dispose() {
        this.mainPack.dispose();
    }

    public FileHandle getSCMLFile(String name) {
        return this.spriterAnimations.get(name);
    }
}
