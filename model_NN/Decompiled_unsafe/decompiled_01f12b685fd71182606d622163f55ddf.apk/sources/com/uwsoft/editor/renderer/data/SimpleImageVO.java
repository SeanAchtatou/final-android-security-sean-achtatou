package com.uwsoft.editor.renderer.data;

public class SimpleImageVO extends MainItemVO {
    public String imageName = "";

    public SimpleImageVO() {
    }

    public SimpleImageVO(SimpleImageVO vo) {
        super(vo);
        this.imageName = new String(vo.imageName);
    }
}
