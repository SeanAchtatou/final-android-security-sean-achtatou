package com.uwsoft.editor.renderer.data;

import com.uwsoft.editor.renderer.resources.FontSizePair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

public class CompositeVO {
    public ArrayList<LayerItemVO> layers = new ArrayList<>();
    public ArrayList<ButtonVO> sButtons = new ArrayList<>(1);
    public ArrayList<CheckBoxVO> sCheckBoxes = new ArrayList<>(1);
    public ArrayList<CompositeItemVO> sComposites = new ArrayList<>(1);
    public ArrayList<Image9patchVO> sImage9patchs = new ArrayList<>(1);
    public ArrayList<SimpleImageVO> sImages = new ArrayList<>(1);
    public ArrayList<LabelVO> sLabels = new ArrayList<>(1);
    public ArrayList<LightVO> sLights = new ArrayList<>(1);
    public ArrayList<ParticleEffectVO> sParticleEffects = new ArrayList<>(1);
    public ArrayList<SelectBoxVO> sSelectBoxes = new ArrayList<>(1);
    public ArrayList<SpineVO> sSpineAnimations = new ArrayList<>(1);
    public ArrayList<SpriteAnimationVO> sSpriteAnimations = new ArrayList<>(1);
    public ArrayList<SpriterVO> sSpriterAnimations = new ArrayList<>(1);
    public ArrayList<TextBoxVO> sTextBox = new ArrayList<>(1);

    public CompositeVO() {
    }

    public CompositeVO(CompositeVO vo) {
        if (vo != null) {
            update(vo);
        }
    }

    public void update(CompositeVO vo) {
        clear();
        for (int i = 0; i < vo.sImages.size(); i++) {
            this.sImages.add(new SimpleImageVO(vo.sImages.get(i)));
        }
        for (int i2 = 0; i2 < vo.sImage9patchs.size(); i2++) {
            this.sImage9patchs.add(new Image9patchVO(vo.sImage9patchs.get(i2)));
        }
        for (int i3 = 0; i3 < vo.sTextBox.size(); i3++) {
            this.sTextBox.add(new TextBoxVO(vo.sTextBox.get(i3)));
        }
        for (int i4 = 0; i4 < vo.sButtons.size(); i4++) {
            this.sButtons.add(new ButtonVO(vo.sButtons.get(i4)));
        }
        for (int i5 = 0; i5 < vo.sLabels.size(); i5++) {
            this.sLabels.add(new LabelVO(vo.sLabels.get(i5)));
        }
        for (int i6 = 0; i6 < vo.sComposites.size(); i6++) {
            this.sComposites.add(new CompositeItemVO(vo.sComposites.get(i6)));
        }
        for (int i7 = 0; i7 < vo.sCheckBoxes.size(); i7++) {
            this.sCheckBoxes.add(new CheckBoxVO(vo.sCheckBoxes.get(i7)));
        }
        for (int i8 = 0; i8 < vo.sSelectBoxes.size(); i8++) {
            this.sSelectBoxes.add(new SelectBoxVO(vo.sSelectBoxes.get(i8)));
        }
        for (int i9 = 0; i9 < vo.sParticleEffects.size(); i9++) {
            this.sParticleEffects.add(new ParticleEffectVO(vo.sParticleEffects.get(i9)));
        }
        for (int i10 = 0; i10 < vo.sLights.size(); i10++) {
            this.sLights.add(new LightVO(vo.sLights.get(i10)));
        }
        for (int i11 = 0; i11 < vo.sSpineAnimations.size(); i11++) {
            this.sSpineAnimations.add(new SpineVO(vo.sSpineAnimations.get(i11)));
        }
        for (int i12 = 0; i12 < vo.sSpriteAnimations.size(); i12++) {
            this.sSpriteAnimations.add(new SpriteAnimationVO(vo.sSpriteAnimations.get(i12)));
        }
        for (int i13 = 0; i13 < vo.sSpriterAnimations.size(); i13++) {
            this.sSpriterAnimations.add(new SpriterVO(vo.sSpriterAnimations.get(i13)));
        }
        this.layers.clear();
        for (int i14 = 0; i14 < vo.layers.size(); i14++) {
            this.layers.add(new LayerItemVO(vo.layers.get(i14)));
        }
    }

    public void addItem(MainItemVO vo) {
        String className = vo.getClass().getSimpleName();
        if (className.equals("SimpleImageVO")) {
            this.sImages.add((SimpleImageVO) vo);
        }
        if (className.equals("Image9patchVO")) {
            this.sImage9patchs.add((Image9patchVO) vo);
        }
        if (className.equals("TextBoxVO")) {
            this.sTextBox.add((TextBoxVO) vo);
        }
        if (className.equals("ButtonVO")) {
            this.sButtons.add((ButtonVO) vo);
        }
        if (className.equals("LabelVO")) {
            this.sLabels.add((LabelVO) vo);
        }
        if (className.equals("CompositeItemVO")) {
            this.sComposites.add((CompositeItemVO) vo);
        }
        if (className.equals("CheckBoxVO")) {
            this.sCheckBoxes.add((CheckBoxVO) vo);
        }
        if (className.equals("SelectBoxVO")) {
            this.sSelectBoxes.add((SelectBoxVO) vo);
        }
        if (className.equals("ParticleEffectVO")) {
            this.sParticleEffects.add((ParticleEffectVO) vo);
        }
        if (className.equals("LightVO")) {
            this.sLights.add((LightVO) vo);
        }
        if (className.equals("SpineVO")) {
            this.sSpineAnimations.add((SpineVO) vo);
        }
        if (className.equals("SpriterVO")) {
            this.sSpriterAnimations.add((SpriterVO) vo);
        }
        if (className.equals("SpriteAnimationVO")) {
            this.sSpriteAnimations.add((SpriteAnimationVO) vo);
        }
    }

    public void removeItem(MainItemVO vo) {
        String className = vo.getClass().getSimpleName();
        if (className.equals("SimpleImageVO")) {
            this.sImages.remove((SimpleImageVO) vo);
        }
        if (className.equals("Image9patchVO")) {
            this.sImage9patchs.remove((Image9patchVO) vo);
        }
        if (className.equals("TextBoxVO")) {
            this.sTextBox.remove((TextBoxVO) vo);
        }
        if (className.equals("ButtonVO")) {
            this.sButtons.remove((ButtonVO) vo);
        }
        if (className.equals("LabelVO")) {
            this.sLabels.remove((LabelVO) vo);
        }
        if (className.equals("CompositeItemVO")) {
            this.sComposites.remove((CompositeItemVO) vo);
        }
        if (className.equals("CheckBoxVO")) {
            this.sCheckBoxes.remove((CheckBoxVO) vo);
        }
        if (className.equals("SelectBoxVO")) {
            this.sSelectBoxes.remove((SelectBoxVO) vo);
        }
        if (className.equals("ParticleEffectVO")) {
            this.sParticleEffects.remove((ParticleEffectVO) vo);
        }
        if (className.equals("LightVO")) {
            this.sLights.remove((LightVO) vo);
        }
        if (className.equals("SpineVO")) {
            this.sSpineAnimations.remove((SpineVO) vo);
        }
        if (className.equals("SpriteAnimationVO")) {
            this.sSpriteAnimations.remove((SpriteAnimationVO) vo);
        }
        if (className.equals("SpriterVO")) {
            this.sSpriterAnimations.remove((SpriterVO) vo);
        }
    }

    public void clear() {
        this.sImages.clear();
        this.sTextBox.clear();
        this.sButtons.clear();
        this.sLabels.clear();
        this.sComposites.clear();
        this.sCheckBoxes.clear();
        this.sSelectBoxes.clear();
        this.sParticleEffects.clear();
        this.sLights.clear();
        this.sSpineAnimations.clear();
        this.sSpriteAnimations.clear();
        this.sSpriterAnimations.clear();
    }

    public boolean isEmpty() {
        return this.sComposites.size() == 0 && this.sImage9patchs.size() == 0 && this.sImages.size() == 0 && this.sSpriteAnimations.size() == 0 && this.sButtons.size() == 0 && this.sCheckBoxes.size() == 0 && this.sLabels.size() == 0 && this.sLights.size() == 0 && this.sParticleEffects.size() == 0 && this.sCheckBoxes.size() == 0 && this.sSpriteAnimations.size() == 0 && this.sSpriterAnimations.size() == 0 && this.sSpineAnimations.size() == 0 && this.sSelectBoxes.size() == 0 && this.sTextBox.size() == 0;
    }

    public String[] getRecursiveParticleEffectsList() {
        HashSet<String> list = new HashSet<>();
        Iterator i$ = this.sParticleEffects.iterator();
        while (i$.hasNext()) {
            list.add(i$.next().particleName);
        }
        Iterator i$2 = this.sComposites.iterator();
        while (i$2.hasNext()) {
            Collections.addAll(list, i$2.next().composite.getRecursiveParticleEffectsList());
        }
        String[] finalList = new String[list.size()];
        list.toArray(finalList);
        return finalList;
    }

    public String[] getRecursiveSpineAnimationList() {
        HashSet<String> list = new HashSet<>();
        Iterator i$ = this.sSpineAnimations.iterator();
        while (i$.hasNext()) {
            list.add(i$.next().animationName);
        }
        Iterator i$2 = this.sComposites.iterator();
        while (i$2.hasNext()) {
            Collections.addAll(list, i$2.next().composite.getRecursiveSpineAnimationList());
        }
        String[] finalList = new String[list.size()];
        list.toArray(finalList);
        return finalList;
    }

    public String[] getRecursiveSpriteAnimationList() {
        HashSet<String> list = new HashSet<>();
        Iterator i$ = this.sSpriteAnimations.iterator();
        while (i$.hasNext()) {
            list.add(i$.next().animationName);
        }
        Iterator i$2 = this.sComposites.iterator();
        while (i$2.hasNext()) {
            Collections.addAll(list, i$2.next().composite.getRecursiveSpriteAnimationList());
        }
        String[] finalList = new String[list.size()];
        list.toArray(finalList);
        return finalList;
    }

    public String[] getRecursiveSpriterAnimationList() {
        HashSet<String> list = new HashSet<>();
        Iterator i$ = this.sSpriterAnimations.iterator();
        while (i$.hasNext()) {
            list.add(i$.next().animationName);
        }
        Iterator i$2 = this.sComposites.iterator();
        while (i$2.hasNext()) {
            Collections.addAll(list, i$2.next().composite.getRecursiveSpriterAnimationList());
        }
        String[] finalList = new String[list.size()];
        list.toArray(finalList);
        return finalList;
    }

    public FontSizePair[] getRecursiveFontList() {
        HashSet<FontSizePair> list = new HashSet<>();
        Iterator i$ = this.sLabels.iterator();
        while (i$.hasNext()) {
            LabelVO sLabel = i$.next();
            list.add(new FontSizePair(sLabel.style.isEmpty() ? "arial" : sLabel.style, sLabel.size == 0 ? 12 : sLabel.size));
        }
        Iterator i$2 = this.sComposites.iterator();
        while (i$2.hasNext()) {
            Collections.addAll(list, i$2.next().composite.getRecursiveFontList());
        }
        FontSizePair[] finalList = new FontSizePair[list.size()];
        list.toArray(finalList);
        return finalList;
    }

    public ArrayList<MainItemVO> getAllItems() {
        return getAllItemsRecursive(new ArrayList<>(), this);
    }

    private ArrayList<MainItemVO> getAllItemsRecursive(ArrayList<MainItemVO> itemsList, CompositeVO compositeVo) {
        Iterator i$ = compositeVo.sButtons.iterator();
        while (i$.hasNext()) {
            itemsList.add(i$.next());
        }
        Iterator i$2 = compositeVo.sCheckBoxes.iterator();
        while (i$2.hasNext()) {
            itemsList.add(i$2.next());
        }
        Iterator i$3 = compositeVo.sImage9patchs.iterator();
        while (i$3.hasNext()) {
            itemsList.add(i$3.next());
        }
        Iterator i$4 = compositeVo.sImages.iterator();
        while (i$4.hasNext()) {
            itemsList.add(i$4.next());
        }
        Iterator i$5 = compositeVo.sLabels.iterator();
        while (i$5.hasNext()) {
            itemsList.add(i$5.next());
        }
        Iterator i$6 = compositeVo.sLights.iterator();
        while (i$6.hasNext()) {
            itemsList.add(i$6.next());
        }
        Iterator i$7 = compositeVo.sParticleEffects.iterator();
        while (i$7.hasNext()) {
            itemsList.add(i$7.next());
        }
        Iterator i$8 = compositeVo.sSelectBoxes.iterator();
        while (i$8.hasNext()) {
            itemsList.add(i$8.next());
        }
        Iterator i$9 = compositeVo.sSpineAnimations.iterator();
        while (i$9.hasNext()) {
            itemsList.add(i$9.next());
        }
        Iterator i$10 = compositeVo.sSpriteAnimations.iterator();
        while (i$10.hasNext()) {
            itemsList.add(i$10.next());
        }
        Iterator i$11 = compositeVo.sSpriterAnimations.iterator();
        while (i$11.hasNext()) {
            itemsList.add(i$11.next());
        }
        Iterator i$12 = compositeVo.sTextBox.iterator();
        while (i$12.hasNext()) {
            itemsList.add(i$12.next());
        }
        Iterator i$13 = compositeVo.sComposites.iterator();
        while (i$13.hasNext()) {
            CompositeItemVO vo = i$13.next();
            itemsList = getAllItemsRecursive(itemsList, vo.composite);
            itemsList.add(vo);
        }
        return itemsList;
    }
}
