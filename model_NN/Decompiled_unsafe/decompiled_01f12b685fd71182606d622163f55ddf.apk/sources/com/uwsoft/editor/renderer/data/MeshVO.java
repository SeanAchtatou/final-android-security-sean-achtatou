package com.uwsoft.editor.renderer.data;

import com.badlogic.gdx.math.Vector2;

public class MeshVO {
    public PhysicsBodyDataVO initialProperties;
    public Vector2[][] minPolygonData;

    public MeshVO clone() {
        MeshVO newVo = new MeshVO();
        Vector2[][] target = new Vector2[this.minPolygonData.length][];
        for (int i = 0; i < this.minPolygonData.length; i++) {
            target[i] = new Vector2[this.minPolygonData[i].length];
            for (int j = 0; j < this.minPolygonData[i].length; j++) {
                target[i][j] = this.minPolygonData[i][j].cpy();
            }
        }
        newVo.minPolygonData = target;
        if (this.initialProperties != null) {
            newVo.initialProperties = new PhysicsBodyDataVO(this.initialProperties);
        }
        return newVo;
    }
}
