package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.MainItemVO;
import com.uwsoft.editor.renderer.data.SpriteAnimationVO;
import com.uwsoft.editor.renderer.utils.CustomVariables;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpriteAnimation extends Actor implements IBaseItem {
    private TextureAtlas.AtlasRegion[] animationAtlasRegions;
    public AnimationCompleteListener animationCompleteListener;
    private Map<String, Animation> animations;
    private Body body;
    private String currentAnimationName;
    private CustomVariables customVariables;
    public SpriteAnimationVO dataVO;
    private int endFrame;
    private final Essentials essentials;
    private int firstFrame;
    private float frameDuration;
    private int frameHeight;
    private int frameIndex;
    private int frameWidth;
    private int framesCount;
    private boolean isLockedByLayer;
    private float lastFrame;
    protected int layerIndex;
    public boolean looping;
    public float mulX;
    public float mulY;
    private float normalSpeed;
    private CompositeItem parentItem;
    private boolean paused;
    private int playingTo;
    protected boolean reverse;

    public interface AnimationCompleteListener {
        void complete(SpriteAnimation spriteAnimation);
    }

    public SpriteAnimation(SpriteAnimationVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        setParentItem(parent);
    }

    public SpriteAnimation(SpriteAnimationVO vo, Essentials e) {
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.reverse = false;
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.playingTo = -1;
        this.normalSpeed = 1.0f;
        this.paused = true;
        this.animations = new HashMap();
        this.customVariables = new CustomVariables();
        this.currentAnimationName = "";
        this.essentials = e;
        this.dataVO = vo;
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        setScaleX(this.dataVO.scaleX);
        setScaleY(this.dataVO.scaleY);
        this.customVariables.loadFromString(this.dataVO.customVars);
        this.animations = Animation.constructJsonObject(this.dataVO.animations);
        setRotation(this.dataVO.rotation);
        if (this.dataVO.zIndex < 0) {
            this.dataVO.zIndex = 0;
        }
        if (this.dataVO.tint == null) {
            setTint(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        } else {
            setTint(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        }
        initSpriteAnimation();
    }

    public String getCurrentAnimationName() {
        return this.currentAnimationName;
    }

    private void initSpriteAnimation() {
        this.lastFrame = Animation.CurveTimeline.LINEAR;
        this.frameIndex = -1;
        setOriginX(Animation.CurveTimeline.LINEAR);
        setOriginY(Animation.CurveTimeline.LINEAR);
        Array<TextureAtlas.AtlasRegion> regions = this.essentials.rm.getSpriteAnimation(this.dataVO.animationName).getRegions();
        this.animationAtlasRegions = new TextureAtlas.AtlasRegion[regions.size];
        this.frameHeight = regions.get(0).originalHeight;
        this.frameWidth = regions.get(0).originalWidth;
        setWidth((float) this.frameWidth);
        setHeight((float) this.frameHeight);
        this.framesCount = regions.size;
        for (int i = 0; i < regions.size; i++) {
            this.animationAtlasRegions[regNameToFrame(regions.get(i).name) - 1] = regions.get(i);
        }
        setAnimation(this.dataVO.fps, true);
    }

    public int getFramesCount() {
        return this.animationAtlasRegions.length;
    }

    public void setAnimation(boolean looping2) {
        setAnimation(24, looping2);
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(1.0f, 1.0f, 1.0f, getColor().a * parentAlpha);
        Batch batch2 = batch;
        batch2.draw(this.animationAtlasRegions[this.frameIndex], this.animationAtlasRegions[this.frameIndex].offsetX + getX(), this.animationAtlasRegions[this.frameIndex].offsetY + getY(), getOriginX(), getOriginY(), (float) this.animationAtlasRegions[this.frameIndex].getRegionWidth(), (float) this.animationAtlasRegions[this.frameIndex].getRegionHeight(), getScaleX(), getScaleY(), getRotation());
        super.draw(batch, parentAlpha);
    }

    public TextureAtlas.AtlasRegion getAtlasRegionAt(int index) {
        return this.animationAtlasRegions[index];
    }

    public void setAnimation(String name) {
        Animation animation = this.animations.get(name);
        if (animation != null) {
            setAnimation(animation.startFrame, animation.endFrame, this.dataVO.fps, this.looping);
            this.currentAnimationName = name;
        }
    }

    public void setAnimation(int fps, boolean looping2) {
        boolean z = false;
        this.firstFrame = 0;
        this.endFrame = this.framesCount - 1;
        this.playingTo = this.endFrame;
        this.frameIndex = 0;
        this.frameDuration = 1.0f / ((float) fps);
        this.looping = looping2;
        if (this.firstFrame > this.endFrame) {
            z = true;
        }
        this.reverse = z;
    }

    public void setAnimation(int firstFrame2, int endFrame2, int fps, boolean looping2) {
        this.firstFrame = firstFrame2;
        this.endFrame = endFrame2;
        this.playingTo = this.endFrame;
        this.frameIndex = firstFrame2;
        this.frameDuration = 1.0f / ((float) fps);
        this.looping = looping2;
        if (firstFrame2 > endFrame2) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
    }

    private int regNameToFrame(String name) {
        Matcher matcher = Pattern.compile("[^0-9]+([0-9]+)$").matcher(name);
        if (matcher.find()) {
            return Integer.parseInt(matcher.group(1));
        }
        throw new RuntimeException("Frame name should be something like this '*0001', but not " + name + ".");
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public MainItemVO getDataVO() {
        return this.dataVO;
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void renew() {
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setScaleX(this.dataVO.scaleX);
        setScaleY(this.dataVO.scaleY);
        setRotation(this.dataVO.rotation);
        setColor(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]);
        if (this.currentAnimationName.isEmpty()) {
            setAnimation(this.dataVO.fps, this.looping);
        } else {
            setAnimation(this.currentAnimationName);
        }
        this.customVariables.loadFromString(this.dataVO.customVars);
        this.animations = Animation.constructJsonObject(this.dataVO.animations);
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public void updateDataVO() {
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.customVars = this.customVariables.saveAsString();
        this.dataVO.animations = Animation.constructJsonString(this.animations);
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        updateDataVO();
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parent) {
        this.parentItem = parent;
    }

    public void setScale(float scale) {
        super.setScale(scale, scale);
        this.dataVO.scaleX = scale;
        this.dataVO.scaleY = scale;
        renew();
    }

    public void act(float delta) {
        if (!this.paused) {
            float usedFrameDuration = this.frameDuration / this.normalSpeed;
            this.lastFrame += delta;
            if (this.lastFrame > usedFrameDuration) {
                if (!this.paused) {
                    if (this.reverse) {
                        this.frameIndex--;
                    } else {
                        this.frameIndex++;
                    }
                }
                this.lastFrame = Animation.CurveTimeline.LINEAR;
                if ((this.frameIndex > this.playingTo && !this.reverse) || (this.frameIndex < this.playingTo && this.reverse)) {
                    if (this.looping) {
                        this.frameIndex = this.firstFrame;
                    } else if (this.animationCompleteListener != null) {
                        this.animationCompleteListener.complete(this);
                        this.paused = true;
                        if (this.frameIndex > this.endFrame) {
                            this.frameIndex = this.endFrame;
                        }
                    } else {
                        this.paused = true;
                        if (this.frameIndex > this.endFrame) {
                            this.frameIndex = this.endFrame;
                        }
                    }
                }
                if (!this.looping && this.playingTo != -1 && this.frameIndex == this.playingTo) {
                    this.playingTo = -1;
                    this.paused = true;
                    if (this.animationCompleteListener != null) {
                        this.animationCompleteListener.complete(this);
                    }
                }
            }
            super.act(delta);
        }
    }

    public void pause() {
        this.paused = true;
    }

    public void start() {
        this.frameIndex = 0;
        this.paused = false;
    }

    public void resume() {
        this.paused = false;
    }

    public void playTo(int frame) {
        int playingToCurr = frame;
        if (playingToCurr >= this.framesCount) {
            playingToCurr = this.framesCount - 1;
        }
        if (playingToCurr < 0) {
            playingToCurr = 0;
        }
        if (this.frameIndex != playingToCurr) {
            if (this.frameIndex > playingToCurr) {
                this.reverse = true;
            } else {
                this.reverse = false;
            }
            this.playingTo = playingToCurr;
            this.paused = false;
        }
    }

    public void setCurrFrame(int frame) {
        this.frameIndex = frame;
        this.playingTo = -1;
        if (this.frameIndex >= this.framesCount) {
            this.frameIndex = this.framesCount - 1;
        }
        if (this.frameIndex < 0) {
            this.frameIndex = 0;
        }
    }

    private int getFrameNumberFromName(String name) {
        String result = "";
        for (int iter = 0; Character.isDigit(name.charAt(iter)); iter++) {
            result = result + name.charAt(iter);
        }
        return Integer.parseInt(result);
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public void setNormalSpeed(float speed) {
        this.normalSpeed = speed;
    }

    public int getCurrentFrameIndex() {
        return this.frameIndex;
    }

    public void setCompletionListener(AnimationCompleteListener animationCompleteListener2) {
        this.animationCompleteListener = animationCompleteListener2;
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }

    public Map<String, Animation> getAnimations() {
        return this.animations;
    }

    public static class Animation {
        public int endFrame;
        public String name;
        public int startFrame;

        public Animation(int startFrame2, int endFrame2, String name2) {
            this.startFrame = startFrame2;
            this.endFrame = endFrame2;
            this.name = name2;
        }

        public Animation() {
        }

        public static String constructJsonString(Map<String, Animation> animations) {
            Json json = new Json();
            json.setOutputType(JsonWriter.OutputType.json);
            return json.toJson(animations);
        }

        public static Map<String, Animation> constructJsonObject(String animations) {
            if (animations.isEmpty()) {
                return new HashMap();
            }
            return (Map) new Json().fromJson(HashMap.class, animations);
        }
    }
}
