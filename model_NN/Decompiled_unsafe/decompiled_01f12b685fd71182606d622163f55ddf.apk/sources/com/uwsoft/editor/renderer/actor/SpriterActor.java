package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.Player;
import com.brashmonkey.spriter.Rectangle;
import com.brashmonkey.spriter.SCMLReader;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.MainItemVO;
import com.uwsoft.editor.renderer.data.SpriterVO;
import com.uwsoft.editor.renderer.utils.CustomVariables;
import com.uwsoft.editor.renderer.utils.LibGdxDrawer;
import com.uwsoft.editor.renderer.utils.LibGdxLoader;
import java.util.ArrayList;

public class SpriterActor extends Actor implements IBaseItem {
    private ArrayList<String> animations;
    private Body body;
    private int currentAnimationIndex;
    private String currentAnimationName;
    private int currentEntityIndex;
    private CustomVariables customVariables;
    private Data data;
    public SpriterVO dataVO;
    private LibGdxDrawer drawer;
    private ArrayList<String> entities;
    private final Essentials essentials;
    private int frameHeight;
    private int frameWidth;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public boolean looping;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;
    private Player player;
    protected boolean reverse;

    public SpriterActor(SpriterVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        setParentItem(parent);
    }

    public SpriterActor(SpriterVO vo, Essentials e) {
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.reverse = false;
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.customVariables = new CustomVariables();
        this.currentAnimationName = "";
        this.animations = new ArrayList<>();
        this.entities = new ArrayList<>();
        this.currentEntityIndex = 0;
        this.essentials = e;
        this.dataVO = vo;
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        this.customVariables.loadFromString(this.dataVO.customVars);
        setRotation(this.dataVO.rotation);
        if (this.dataVO.zIndex < 0) {
            this.dataVO.zIndex = 0;
        }
        if (this.dataVO.tint == null) {
            setTint(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        } else {
            setTint(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        }
        initSpriteAnimation();
    }

    private void initSpriteAnimation() {
        setOriginX(Animation.CurveTimeline.LINEAR);
        setOriginY(Animation.CurveTimeline.LINEAR);
        FileHandle handle = this.essentials.rm.getSCMLFile(this.dataVO.animationName);
        this.data = new SCMLReader(handle.read()).getData();
        LibGdxLoader loader = new LibGdxLoader(this.data);
        loader.load(handle.file());
        this.drawer = new LibGdxDrawer(loader, new ShapeRenderer());
        this.currentAnimationIndex = this.dataVO.animation;
        this.currentEntityIndex = this.dataVO.entity;
        initPlayer();
    }

    private void initPlayer() {
        this.player = new Player(this.data.getEntity(this.currentEntityIndex));
        this.player.setAnimation(this.currentAnimationIndex);
        this.player.setScale(this.dataVO.scale * this.mulX);
        setRectangle();
    }

    private void setRectangle() {
        this.player.update();
        Rectangle bbox = this.player.getBoundingRectangle(null);
        this.frameWidth = (int) bbox.size.width;
        this.frameHeight = (int) bbox.size.height;
        setWidth((float) this.frameWidth);
        setHeight((float) this.frameHeight);
    }

    public void act(float delta) {
        super.act(delta);
        this.player.update();
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(1.0f, 1.0f, 1.0f, getColor().a * parentAlpha);
        super.draw(batch, parentAlpha);
        this.player.setPosition(getX(), getY());
        this.player.setPivot(getWidth() / 2.0f, getHeight() / 2.0f);
        this.player.setScale(this.dataVO.scale * this.mulX);
        this.player.rotate(getRotation() - this.player.getAngle());
        this.drawer.beforeDraw(this.player, batch);
    }

    public void renew() {
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        this.player.setScale(this.dataVO.scale * this.mulX);
        setRotation(this.dataVO.rotation);
        this.customVariables.loadFromString(this.dataVO.customVars);
        setRectangle();
    }

    public void setSpriterScale(float scale) {
        this.dataVO.scale = scale;
        renew();
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public MainItemVO getDataVO() {
        return this.dataVO;
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public void updateDataVO() {
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.entity = this.currentEntityIndex;
        this.dataVO.animation = this.currentAnimationIndex;
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        updateDataVO();
        initPlayer();
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parent) {
        this.parentItem = parent;
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }

    public Body getBody() {
        return null;
    }

    public void setBody(Body body2) {
    }

    public void dispose() {
    }

    public ArrayList<String> getAnimations() {
        this.animations = new ArrayList<>();
        for (int i = 0; i < this.data.getEntity(this.currentEntityIndex).animations(); i++) {
            this.animations.add(this.data.getEntity(this.currentEntityIndex).getAnimation(i).name);
        }
        return this.animations;
    }

    public void setAnimation(int i) {
        this.currentAnimationIndex = i;
        updateDataVO();
        initPlayer();
    }

    public void setEntity(int i) {
        this.currentEntityIndex = i;
        setAnimation(0);
        updateDataVO();
        initPlayer();
    }

    public ArrayList<String> getEntities() {
        this.entities = this.data.getEntities();
        return this.entities;
    }

    public String getCurrentEntityName() {
        return this.entities.get(this.currentEntityIndex);
    }

    public int getCurrentEntityIndex() {
        return this.currentEntityIndex;
    }

    public String getCurrentAnimationName() {
        return this.currentAnimationName;
    }

    public int getCurrentAnimationIndex() {
        return this.currentAnimationIndex;
    }
}
