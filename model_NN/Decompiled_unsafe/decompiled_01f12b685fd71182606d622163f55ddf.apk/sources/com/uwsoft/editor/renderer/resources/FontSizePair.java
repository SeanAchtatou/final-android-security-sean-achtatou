package com.uwsoft.editor.renderer.resources;

public class FontSizePair {
    public String fontName;
    public int fontSize;

    public FontSizePair(String name, int size) {
        this.fontName = name;
        this.fontSize = size;
    }

    public boolean equals(Object arg0) {
        FontSizePair arg = (FontSizePair) arg0;
        if (!arg.fontName.equals(this.fontName) || arg.fontSize != this.fontSize) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this.fontName + "_" + this.fontSize;
    }

    public int hashCode() {
        return (this.fontName + "_" + this.fontSize).hashCode();
    }
}
