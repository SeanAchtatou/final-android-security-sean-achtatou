package com.uwsoft.editor.renderer.script;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.uwsoft.editor.renderer.actor.CompositeItem;

public class SimpleButtonScript implements IScript {
    public static final int TEXT_EFFECT_DOWN = 2;
    public static final int TEXT_EFFECT_NONE = 0;
    public static final int TEXT_EFFECT_PUSH = 1;
    protected CompositeItem buttonHolder;
    protected boolean isDown = false;
    protected boolean isToggled = false;
    protected final DelayedRemovalArray<ClickListener> listeners = new DelayedRemovalArray<>(0);
    protected float origTextScaleX;
    protected float origTextScaleY;
    protected float origTextY;
    protected int textEffect = 0;

    public static SimpleButtonScript selfInit(CompositeItem item) {
        SimpleButtonScript script = new SimpleButtonScript();
        item.addScript(script);
        return script;
    }

    public int getTextEffect() {
        return this.textEffect;
    }

    public void setTextEffect(int textEffect2) {
        this.textEffect = textEffect2;
    }

    public boolean isDown() {
        return this.isDown;
    }

    public boolean isToggled() {
        return this.isToggled;
    }

    public void init(CompositeItem item) {
        this.buttonHolder = item;
        String text = item.getCustomVariables().getStringVariable("text");
        if (item.getLabelById("text") != null) {
            if (text != null) {
                item.getLabelById("text").setText(text);
            }
            item.getLabelById("text").setAlignment(1);
            this.origTextY = item.getLabelById("text").getY();
            this.origTextScaleX = item.getLabelById("text").getScaleX();
            this.origTextScaleY = item.getLabelById("text").getScaleY();
        }
        this.buttonHolder.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                SimpleButtonScript.this.isDown = true;
                for (int i = 0; i < SimpleButtonScript.this.listeners.size; i++) {
                    SimpleButtonScript.this.listeners.get(i).touchDown(event, x, y, pointer, button);
                }
                return super.touchDown(event, x, y, pointer, button);
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                boolean z = false;
                SimpleButtonScript.this.isDown = false;
                SimpleButtonScript simpleButtonScript = SimpleButtonScript.this;
                if (!SimpleButtonScript.this.isToggled) {
                    z = true;
                }
                simpleButtonScript.isToggled = z;
                for (int i = 0; i < SimpleButtonScript.this.listeners.size; i++) {
                    SimpleButtonScript.this.listeners.get(i).touchUp(event, x, y, pointer, button);
                }
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    public void dispose() {
        clearListeners();
    }

    public void act(float delta) {
        if (this.isDown) {
            this.buttonHolder.setLayerVisibilty("checked", false);
            this.buttonHolder.setLayerVisibilty("normal", false);
            this.buttonHolder.setLayerVisibilty("pressed", true);
        } else {
            if (!this.isToggled || !this.buttonHolder.layerExists("checked")) {
                this.buttonHolder.setLayerVisibilty("normal", true);
                this.buttonHolder.setLayerVisibilty("checked", false);
            } else {
                this.buttonHolder.setLayerVisibilty("checked", true);
                this.buttonHolder.setLayerVisibilty("normal", false);
            }
            this.buttonHolder.setLayerVisibilty("pressed", false);
        }
        if (this.buttonHolder.getLabelById("text") == null) {
            return;
        }
        if (this.textEffect == 2) {
            if (this.isDown) {
                this.buttonHolder.getLabelById("text").setY(this.origTextY - (5.0f * this.buttonHolder.mulY));
            } else {
                this.buttonHolder.getLabelById("text").setY(this.origTextY);
            }
        } else if (this.textEffect != 1) {
        } else {
            if (this.isDown) {
                this.buttonHolder.getLabelById("text").setScale(0.9f);
                return;
            }
            this.buttonHolder.getLabelById("text").setScaleX(this.origTextScaleX);
            this.buttonHolder.getLabelById("text").setScaleY(this.origTextScaleY);
        }
    }

    public void setToggle(boolean toggle) {
        this.isToggled = toggle;
    }

    public boolean addListener(ClickListener listener) {
        if (this.listeners.contains(listener, true)) {
            return false;
        }
        this.listeners.add(listener);
        return true;
    }

    public boolean removeListener(ClickListener listener) {
        return this.listeners.removeValue(listener, true);
    }

    public Array<ClickListener> getListeners() {
        return this.listeners;
    }

    public void clearListeners() {
        this.listeners.clear();
    }

    public CompositeItem getItem() {
        return this.buttonHolder;
    }
}
