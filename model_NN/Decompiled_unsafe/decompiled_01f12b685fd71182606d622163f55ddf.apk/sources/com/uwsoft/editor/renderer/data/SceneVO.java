package com.uwsoft.editor.renderer.data;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import java.util.Arrays;
import java.util.HashMap;

public class SceneVO {
    public float[] ambientColor = {1.0f, 1.0f, 1.0f, 1.0f};
    public CompositeVO composite;
    public HashMap<String, CompositeItemVO> libraryItems = new HashMap<>();
    public PhysicsPropertiesVO physicsPropertiesVO = new PhysicsPropertiesVO();
    public String sceneName = "";

    public SceneVO() {
    }

    public SceneVO(SceneVO vo) {
        this.sceneName = new String(vo.sceneName);
        this.composite = new CompositeVO(vo.composite);
        this.ambientColor = Arrays.copyOf(vo.ambientColor, vo.ambientColor.length);
        this.physicsPropertiesVO = new PhysicsPropertiesVO(vo.physicsPropertiesVO);
    }

    public String constructJsonString() {
        Json json = new Json();
        json.setOutputType(JsonWriter.OutputType.json);
        return json.toJson(this);
    }
}
