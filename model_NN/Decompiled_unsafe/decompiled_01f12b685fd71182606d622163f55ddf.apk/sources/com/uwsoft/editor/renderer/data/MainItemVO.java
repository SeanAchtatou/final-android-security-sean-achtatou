package com.uwsoft.editor.renderer.data;

import java.util.Arrays;

public class MainItemVO {
    public String customVars = "";
    public boolean isFlipedH = false;
    public boolean isFlipedV = false;
    public String itemIdentifier = "";
    public String itemName = "";
    public String layerName = "";
    public String meshId = "-1";
    public PhysicsBodyDataVO physicsBodyData = null;
    public float rotation;
    public float scaleX = 1.0f;
    public float scaleY = 1.0f;
    public String[] tags = null;
    public float[] tint = null;
    public float x;
    public float y;
    public int zIndex = 0;

    public MainItemVO() {
    }

    public MainItemVO(MainItemVO vo) {
        this.itemIdentifier = new String(vo.itemIdentifier);
        this.itemName = new String(vo.itemName);
        if (this.tags != null) {
            this.tags = (String[]) Arrays.copyOf(vo.tags, vo.tags.length);
        }
        this.customVars = new String(vo.customVars);
        this.x = vo.x;
        this.y = vo.y;
        this.rotation = vo.rotation;
        this.zIndex = vo.zIndex;
        this.layerName = new String(vo.layerName);
        if (vo.tint != null) {
            this.tint = Arrays.copyOf(vo.tint, vo.tint.length);
        }
        this.isFlipedH = vo.isFlipedH;
        this.isFlipedV = vo.isFlipedV;
        this.scaleX = vo.scaleX;
        this.scaleY = vo.scaleY;
        this.meshId = vo.meshId;
        if (vo.physicsBodyData != null) {
            this.physicsBodyData = new PhysicsBodyDataVO(vo.physicsBodyData);
        }
    }
}
