package com.uwsoft.editor.renderer.data;

public class CheckBoxVO extends MainItemVO {
    public String style = "";
    public String text = "Label";

    public CheckBoxVO() {
    }

    public CheckBoxVO(CheckBoxVO vo) {
        super(vo);
        this.text = new String(vo.text);
        this.style = new String(vo.style);
    }
}
