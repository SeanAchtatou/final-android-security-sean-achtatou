package com.uwsoft.editor.renderer.resources;

public interface IResourceLoader extends IAssetLoader, IDataLoader {
}
