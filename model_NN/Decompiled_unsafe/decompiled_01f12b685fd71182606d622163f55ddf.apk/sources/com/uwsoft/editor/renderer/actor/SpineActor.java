package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.SpineVO;
import com.uwsoft.editor.renderer.spine.SpineDataHelper;
import com.uwsoft.editor.renderer.spine.SpineReflectionHelper;
import com.uwsoft.editor.renderer.utils.CustomVariables;
import java.lang.reflect.InvocationTargetException;

public class SpineActor extends Actor implements IBaseItem {
    private Body body;
    private CustomVariables customVariables;
    public SpineVO dataVO;
    private Essentials essentials;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;
    private SpineDataHelper spineData;
    private SpineReflectionHelper spineReflectionHelper;

    public SpineActor(SpineVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        setParentItem(parent);
    }

    public SpineActor(SpineVO vo, Essentials e) {
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.customVariables = new CustomVariables();
        this.essentials = e;
        this.spineReflectionHelper = this.essentials.spineReflectionHelper;
        this.dataVO = vo;
        initSpine();
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        setScaleX(this.dataVO.scaleX);
        setScaleY(this.dataVO.scaleY);
        this.customVariables.loadFromString(this.dataVO.customVars);
        setRotation(this.dataVO.rotation);
        if (this.dataVO.zIndex < 0) {
            this.dataVO.zIndex = 0;
        }
        if (this.dataVO.tint == null) {
            setTint(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        } else {
            setTint(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        }
    }

    private void initSpine() {
        this.spineData = new SpineDataHelper();
        try {
            this.spineData.initSpine(this.dataVO, this.essentials.rm, this.spineReflectionHelper, this.mulX);
        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | InvocationTargetException e) {
            System.out.println("Reflection problem");
            e.printStackTrace();
        }
        setWidth(this.spineData.width);
        setHeight(this.spineData.height);
    }

    public Array<Object> getAnimations() {
        return this.spineData.getAnimations();
    }

    public void setAnimation(String animName) {
        this.spineData.setAnimation(animName);
        this.dataVO.currentAnimationName = animName;
    }

    public Object getState() {
        return this.spineData.stateObject;
    }

    public void draw(Batch batch, float parentAlpha) {
        this.spineData.draw(batch, parentAlpha);
        super.draw(batch, parentAlpha);
    }

    public void act(float delta) {
        this.spineData.act(delta, getX(), getY());
        super.act(delta);
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public SpineVO getDataVO() {
        return this.dataVO;
    }

    public void renew() {
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setScaleX(this.dataVO.scaleX);
        setScaleY(this.dataVO.scaleY);
        setRotation(this.dataVO.rotation);
        setColor(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]);
        this.customVariables.loadFromString(this.dataVO.customVars);
        initSpine();
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void updateDataVO() {
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        updateDataVO();
        initSpine();
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public void setScale(float scale) {
        super.setScale(scale, scale);
        this.dataVO.scaleX = scale;
        renew();
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }

    public String getCurrentAnimationName() {
        return this.dataVO.currentAnimationName;
    }
}
