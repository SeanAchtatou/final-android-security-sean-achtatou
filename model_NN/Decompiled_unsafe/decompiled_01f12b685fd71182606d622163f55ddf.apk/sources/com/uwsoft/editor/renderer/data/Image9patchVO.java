package com.uwsoft.editor.renderer.data;

import com.kbz.esotericsoftware.spine.Animation;

public class Image9patchVO extends SimpleImageVO {
    public float height = Animation.CurveTimeline.LINEAR;
    public float width = Animation.CurveTimeline.LINEAR;

    public Image9patchVO() {
    }

    public Image9patchVO(Image9patchVO vo) {
        super(vo);
        this.width = vo.width;
        this.height = vo.height;
    }
}
