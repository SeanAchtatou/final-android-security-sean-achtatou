package com.uwsoft.editor.renderer.data;

public class CompositeItemVO extends MainItemVO {
    public CompositeVO composite;
    public float scissorHeight;
    public float scissorWidth;
    public float scissorX;
    public float scissorY;

    public CompositeItemVO() {
        this.composite = new CompositeVO();
    }

    public CompositeItemVO(CompositeVO vo) {
        this.composite = new CompositeVO(vo);
    }

    public CompositeItemVO(CompositeItemVO vo) {
        super(vo);
        this.composite = new CompositeVO(vo.composite);
    }

    public void update(CompositeItemVO vo) {
        this.composite = new CompositeVO(vo.composite);
    }

    public CompositeItemVO clone() {
        CompositeItemVO tmp = new CompositeItemVO();
        tmp.composite = this.composite;
        tmp.isFlipedH = this.isFlipedH;
        tmp.isFlipedV = this.isFlipedV;
        tmp.itemName = this.itemName;
        tmp.layerName = this.layerName;
        tmp.rotation = this.rotation;
        tmp.tint = this.tint;
        tmp.x = this.x;
        tmp.y = this.y;
        tmp.zIndex = this.zIndex;
        tmp.scissorX = this.scissorX;
        tmp.scissorY = this.scissorY;
        tmp.scissorWidth = this.scissorWidth;
        tmp.scissorHeight = this.scissorHeight;
        return tmp;
    }
}
