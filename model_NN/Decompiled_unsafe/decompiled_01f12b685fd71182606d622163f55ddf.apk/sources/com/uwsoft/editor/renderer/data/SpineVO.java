package com.uwsoft.editor.renderer.data;

public class SpineVO extends MainItemVO {
    public String animationName = "";
    public String currentAnimationName = "";

    public SpineVO() {
    }

    public SpineVO(SpineVO vo) {
        super(vo);
        this.animationName = vo.animationName;
        this.currentAnimationName = vo.currentAnimationName;
    }
}
