package com.uwsoft.editor.renderer.data;

import com.badlogic.gdx.math.Vector2;

public class PhysicsBodyDataVO {
    public boolean allowSleep;
    public boolean awake;
    public int bodyType;
    public boolean bullet;
    public Vector2 centerOfMass;
    public float damping;
    public float density;
    public float friction;
    public float gravityScale;
    public float mass;
    public float restitution;
    public float rotationalInertia;

    public PhysicsBodyDataVO() {
        this.bodyType = 0;
        this.centerOfMass = new Vector2();
    }

    public PhysicsBodyDataVO(PhysicsBodyDataVO vo) {
        this.bodyType = 0;
        this.bodyType = vo.bodyType;
        this.mass = vo.mass;
        this.centerOfMass = vo.centerOfMass.cpy();
        this.rotationalInertia = vo.rotationalInertia;
        this.damping = vo.damping;
        this.gravityScale = vo.gravityScale;
        this.allowSleep = vo.allowSleep;
        this.awake = vo.awake;
        this.bullet = vo.bullet;
        this.density = vo.density;
        this.friction = vo.friction;
        this.restitution = vo.restitution;
    }
}
