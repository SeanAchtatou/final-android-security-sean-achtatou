package com.uwsoft.editor.renderer.data;

public class SpriteAnimationVO extends MainItemVO {
    public String animationName = "";
    public String animations = "";
    public int fps = 24;

    public SpriteAnimationVO() {
    }

    public SpriteAnimationVO(SpriteAnimationVO vo) {
        super(vo);
        this.animationName = vo.animationName;
        this.fps = vo.fps;
        this.animations = vo.animations;
    }
}
