package com.uwsoft.editor.renderer.data;

public class SpriterVO extends MainItemVO {
    public int animation;
    public String animationName = "";
    public int entity;
    public float scale = 1.0f;

    public SpriterVO() {
    }

    public SpriterVO(SpriterVO vo) {
        super(vo);
        this.entity = vo.entity;
        this.animation = vo.animation;
        this.animationName = vo.animationName;
        this.scale = vo.scale;
    }
}
