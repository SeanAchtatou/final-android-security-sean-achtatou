package com.uwsoft.editor.renderer.utils;

import java.util.HashMap;
import java.util.Map;

public class CustomVariables {
    private HashMap<String, String> variables = new HashMap<>();

    public void loadFromString(String varString) {
        this.variables.clear();
        String[] vars = varString.split(";");
        for (String split : vars) {
            String[] tmp = split.split(":");
            if (tmp.length > 1) {
                setVariable(tmp[0], tmp[1]);
            }
        }
    }

    public String saveAsString() {
        String result = "";
        for (Map.Entry<String, String> entry : this.variables.entrySet()) {
            result = result + ((String) entry.getKey()) + ":" + ((String) entry.getValue()) + ";";
        }
        if (result.length() > 0) {
            return result.substring(0, result.length() - 1);
        }
        return result;
    }

    public void setVariable(String key, String value) {
        this.variables.put(key, value);
    }

    public void removeVariable(String key) {
        this.variables.remove(key);
    }

    public String getStringVariable(String key) {
        return this.variables.get(key);
    }

    public Integer getIntegerVariable(String key) {
        try {
            return Integer.valueOf(Integer.parseInt(this.variables.get(key)));
        } catch (Exception e) {
            return null;
        }
    }

    public Float getFloatVariable(String key) {
        try {
            return Float.valueOf(Float.parseFloat(this.variables.get(key)));
        } catch (Exception e) {
            return null;
        }
    }

    public HashMap<String, String> getHashMap() {
        return this.variables;
    }

    public int getCount() {
        return this.variables.size();
    }
}
