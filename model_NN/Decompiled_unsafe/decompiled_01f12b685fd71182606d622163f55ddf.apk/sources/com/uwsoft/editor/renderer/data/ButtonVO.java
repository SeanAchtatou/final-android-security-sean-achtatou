package com.uwsoft.editor.renderer.data;

public class ButtonVO extends MainItemVO {
    public String style = "";
    public String text = "Button";

    public ButtonVO() {
    }

    public ButtonVO(ButtonVO vo) {
        super(vo);
        this.text = new String(vo.text);
        this.style = new String(vo.style);
    }
}
