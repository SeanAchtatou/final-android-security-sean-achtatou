package com.uwsoft.editor.renderer.data;

public class ParticleEffectVO extends MainItemVO {
    public float particleHeight = 100.0f;
    public String particleName = "";
    public float particleWidth = 100.0f;

    public ParticleEffectVO() {
    }

    public ParticleEffectVO(ParticleEffectVO vo) {
        super(vo);
        this.particleName = new String(vo.particleName);
    }
}
