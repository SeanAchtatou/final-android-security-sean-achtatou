package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.TextBoxVO;
import com.uwsoft.editor.renderer.utils.CustomVariables;

public class TextBoxItem extends TextField implements IBaseItem {
    private Body body;
    private CustomVariables customVariables;
    public TextBoxVO dataVO;
    public Essentials essentials;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;

    public TextBoxItem(TextBoxVO vo, Essentials rm, CompositeItem parent) {
        this(vo, rm);
        setParentItem(parent);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TextBoxItem(com.uwsoft.editor.renderer.data.TextBoxVO r8, com.uwsoft.editor.renderer.data.Essentials r9) {
        /*
            r7 = this;
            r6 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            java.lang.String r1 = r8.defaultText
            com.uwsoft.editor.renderer.resources.IResourceRetriever r0 = r9.rm
            com.uwsoft.editor.renderer.utils.MySkin r2 = r0.getSkin()
            java.lang.String r0 = r8.style
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0079
            java.lang.String r0 = "default"
        L_0x0015:
            r7.<init>(r1, r2, r0)
            r7.mulX = r3
            r7.mulY = r3
            r7.layerIndex = r6
            r7.isLockedByLayer = r6
            r0 = 0
            r7.parentItem = r0
            com.uwsoft.editor.renderer.utils.CustomVariables r0 = new com.uwsoft.editor.renderer.utils.CustomVariables
            r0.<init>()
            r7.customVariables = r0
            r7.essentials = r9
            r7.dataVO = r8
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            float r0 = r0.x
            r7.setX(r0)
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            float r0 = r0.y
            r7.setY(r0)
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            float r0 = r0.scaleX
            r7.setScaleX(r0)
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            float r0 = r0.scaleY
            r7.setScaleY(r0)
            com.uwsoft.editor.renderer.utils.CustomVariables r0 = r7.customVariables
            com.uwsoft.editor.renderer.data.TextBoxVO r1 = r7.dataVO
            java.lang.String r1 = r1.customVars
            r0.loadFromString(r1)
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            float r0 = r0.rotation
            r7.setRotation(r0)
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            int r0 = r0.zIndex
            if (r0 >= 0) goto L_0x0064
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            r0.zIndex = r6
        L_0x0064:
            com.uwsoft.editor.renderer.data.TextBoxVO r0 = r7.dataVO
            float[] r0 = r0.tint
            if (r0 != 0) goto L_0x007c
            com.badlogic.gdx.graphics.Color r0 = new com.badlogic.gdx.graphics.Color
            r0.<init>(r3, r3, r3, r3)
            r7.setTint(r0)
        L_0x0072:
            r7.setFocusTraversal(r6)
            r7.renew()
            return
        L_0x0079:
            java.lang.String r0 = r8.style
            goto L_0x0015
        L_0x007c:
            com.badlogic.gdx.graphics.Color r0 = new com.badlogic.gdx.graphics.Color
            com.uwsoft.editor.renderer.data.TextBoxVO r1 = r7.dataVO
            float[] r1 = r1.tint
            r1 = r1[r6]
            com.uwsoft.editor.renderer.data.TextBoxVO r2 = r7.dataVO
            float[] r2 = r2.tint
            r3 = 1
            r2 = r2[r3]
            com.uwsoft.editor.renderer.data.TextBoxVO r3 = r7.dataVO
            float[] r3 = r3.tint
            r4 = 2
            r3 = r3[r4]
            com.uwsoft.editor.renderer.data.TextBoxVO r4 = r7.dataVO
            float[] r4 = r4.tint
            r5 = 3
            r4 = r4[r5]
            r0.<init>(r1, r2, r3, r4)
            r7.setTint(r0)
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uwsoft.editor.renderer.actor.TextBoxItem.<init>(com.uwsoft.editor.renderer.data.TextBoxVO, com.uwsoft.editor.renderer.data.Essentials):void");
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public TextBoxVO getDataVO() {
        return this.dataVO;
    }

    public void renew() {
        setText(this.dataVO.defaultText);
        if (this.dataVO.width > Animation.CurveTimeline.LINEAR) {
            setWidth(this.dataVO.width);
        }
        if (this.dataVO.height > Animation.CurveTimeline.LINEAR) {
            setHeight(this.dataVO.height);
        }
        invalidate();
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setScaleX(this.dataVO.scaleX * this.mulX);
        setScaleY(this.dataVO.scaleY * this.mulY);
        setRotation(this.dataVO.rotation);
        this.customVariables.loadFromString(this.dataVO.customVars);
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void updateDataVO() {
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public void applyResolution(float mulX2, float mulY2) {
        setScaleX(this.dataVO.scaleX * mulX2);
        setScaleY(this.dataVO.scaleY * mulY2);
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        updateDataVO();
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public void setStyle(TextField.TextFieldStyle lst, String styleName) {
        setStyle(lst);
        this.dataVO.style = styleName;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }
}
