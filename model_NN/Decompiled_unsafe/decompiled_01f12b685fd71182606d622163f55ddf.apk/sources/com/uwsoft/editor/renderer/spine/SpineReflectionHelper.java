package com.uwsoft.editor.renderer.spine;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.esotericsoftware.reflectasm.ConstructorAccess;
import com.esotericsoftware.reflectasm.MethodAccess;
import java.lang.reflect.Constructor;

public class SpineReflectionHelper {
    public Class<?> animationClass;
    public MethodAccess animationClassMethodAccess;
    public Constructor<?> animationStateConstructorAccess;
    public Class<?> animationStateDataClass;
    public Constructor<?> animationStateDataConstructorAccess;
    public int applyMethodIndex;
    public int getAnimNameMethodIndex;
    public int getAnimationMethodIndex;
    public int getAttachmentMethodIndex;
    public int getSlotsMethodIndex;
    public int getWorldVerticesIndex;
    public boolean isSpineAviable = true;
    public Class<?> regionAttachmentClass;
    public MethodAccess regionAttachmentMethodAccess;
    public int setAnimationMethodIndex;
    public int setPositionMethodIndex;
    public Class<?> skeletonClass;
    public MethodAccess skeletonClassMethodAccess;
    public Constructor<?> skeletonConstructorAccess;
    private Class<?> skeletonDataClass;
    public MethodAccess skeletonDataClassMethodAccess;
    public Class<?> skeletonJsonClass;
    public Constructor<?> skeletonJsonConstructorAccess;
    private Class<?> skeletonRendererClass;
    private ConstructorAccess<?> skeletonRendererConstructorAccess;
    public int skeletonRendererDrawMethodIndex;
    public MethodAccess skeletonRendererMethodAccess;
    public Object skeletonRendererObject;
    public Class<?> slotClass;
    public MethodAccess slotClassMethodAccess;
    public Class<?> stateClass;
    public MethodAccess stateObjectMethodAccess;
    public int updateMethodIndex;
    public int updateWorldTransformMethodIndex;
    public int updateWorldVerticesMethodIndex;

    public SpineReflectionHelper() {
        try {
            this.skeletonJsonClass = Class.forName("com.esotericsoftware.spine.SkeletonJson");
            try {
                this.skeletonJsonConstructorAccess = this.skeletonJsonClass.getConstructor(TextureAtlas.class);
            } catch (NoSuchMethodException | SecurityException e) {
            }
            try {
                this.skeletonClass = Class.forName("com.esotericsoftware.spine.Skeleton");
                try {
                    this.skeletonDataClass = Class.forName("com.esotericsoftware.spine.SkeletonData");
                    try {
                        this.skeletonConstructorAccess = this.skeletonClass.getConstructor(this.skeletonDataClass);
                    } catch (NoSuchMethodException | SecurityException e2) {
                    }
                    try {
                        this.animationStateDataClass = Class.forName("com.esotericsoftware.spine.AnimationStateData");
                        try {
                            this.animationStateDataConstructorAccess = this.animationStateDataClass.getConstructor(this.skeletonDataClass);
                        } catch (NoSuchMethodException | SecurityException e3) {
                        }
                        try {
                            this.stateClass = Class.forName("com.esotericsoftware.spine.AnimationState");
                            try {
                                this.animationStateConstructorAccess = this.stateClass.getConstructor(this.animationStateDataClass);
                            } catch (NoSuchMethodException | SecurityException e4) {
                                this.isSpineAviable = false;
                            }
                            try {
                                this.animationClass = Class.forName("com.esotericsoftware.spine.Animation");
                                try {
                                    this.skeletonClassMethodAccess = MethodAccess.get(this.skeletonClass);
                                    try {
                                        this.skeletonRendererClass = Class.forName("com.esotericsoftware.spine.SkeletonRenderer");
                                        try {
                                            this.slotClass = Class.forName("com.esotericsoftware.spine.Slot");
                                            this.updateWorldTransformMethodIndex = this.skeletonClassMethodAccess.getIndex("updateWorldTransform");
                                            this.setPositionMethodIndex = this.skeletonClassMethodAccess.getIndex("setPosition");
                                            this.skeletonDataClassMethodAccess = MethodAccess.get(this.skeletonDataClass);
                                            this.getAnimationMethodIndex = this.skeletonDataClassMethodAccess.getIndex("getAnimations");
                                            this.stateObjectMethodAccess = MethodAccess.get(this.stateClass);
                                            this.setAnimationMethodIndex = this.stateObjectMethodAccess.getIndex("setAnimation", Integer.TYPE, String.class, Boolean.TYPE);
                                            this.updateMethodIndex = this.stateObjectMethodAccess.getIndex("update");
                                            this.applyMethodIndex = this.stateObjectMethodAccess.getIndex("apply");
                                            this.animationClassMethodAccess = MethodAccess.get(this.animationClass);
                                            this.getAnimNameMethodIndex = this.animationClassMethodAccess.getIndex("getName");
                                            this.getSlotsMethodIndex = this.skeletonClassMethodAccess.getIndex("getSlots");
                                            this.slotClassMethodAccess = MethodAccess.get(this.slotClass);
                                            this.getAttachmentMethodIndex = this.slotClassMethodAccess.getIndex("getAttachment");
                                            try {
                                                this.regionAttachmentClass = Class.forName("com.esotericsoftware.spine.attachments.RegionAttachment");
                                                this.regionAttachmentMethodAccess = MethodAccess.get(this.regionAttachmentClass);
                                                this.updateWorldVerticesMethodIndex = this.regionAttachmentMethodAccess.getIndex("updateWorldVertices");
                                                this.getWorldVerticesIndex = this.regionAttachmentMethodAccess.getIndex("getWorldVertices");
                                                this.skeletonRendererConstructorAccess = ConstructorAccess.get(this.skeletonRendererClass);
                                                this.skeletonRendererMethodAccess = MethodAccess.get(this.skeletonRendererClass);
                                                this.skeletonRendererDrawMethodIndex = this.skeletonRendererMethodAccess.getIndex("draw", Batch.class, this.skeletonClass);
                                                this.skeletonRendererObject = this.skeletonRendererConstructorAccess.newInstance();
                                            } catch (ClassNotFoundException e5) {
                                                this.isSpineAviable = false;
                                            }
                                        } catch (ClassNotFoundException e6) {
                                            this.isSpineAviable = false;
                                        }
                                    } catch (ClassNotFoundException e7) {
                                        this.isSpineAviable = false;
                                    }
                                } catch (Exception e8) {
                                    this.isSpineAviable = false;
                                }
                            } catch (ClassNotFoundException e9) {
                                this.isSpineAviable = false;
                            }
                        } catch (ClassNotFoundException e10) {
                            this.isSpineAviable = false;
                        }
                    } catch (ClassNotFoundException e11) {
                        this.isSpineAviable = false;
                    }
                } catch (ClassNotFoundException e12) {
                    this.isSpineAviable = false;
                }
            } catch (ClassNotFoundException e13) {
                this.isSpineAviable = false;
            }
        } catch (ClassNotFoundException e14) {
            this.isSpineAviable = false;
        }
    }
}
