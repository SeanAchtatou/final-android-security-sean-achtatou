package com.uwsoft.editor.renderer.spine;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.reflectasm.MethodAccess;
import com.uwsoft.editor.renderer.data.SpineVO;
import com.uwsoft.editor.renderer.resources.IResourceRetriever;
import java.lang.reflect.InvocationTargetException;

public class SpineDataHelper {
    public float height;
    private float minX;
    private float minY;
    private SpineReflectionHelper reflectionData;
    private Object renderer;
    private Object skeletonData;
    private Object skeletonObject;
    public Object stateObject;
    public float width;

    public void initSpine(SpineVO dataVO, IResourceRetriever rm, SpineReflectionHelper refData, float mulX) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.renderer = refData.skeletonRendererObject;
        this.reflectionData = refData;
        Object skeletonJsonObject = this.reflectionData.skeletonJsonConstructorAccess.newInstance(rm.getSkeletonAtlas(dataVO.animationName));
        MethodAccess methodAccess = MethodAccess.get(this.reflectionData.skeletonJsonClass);
        methodAccess.invoke(skeletonJsonObject, "setScale", Float.valueOf(dataVO.scaleX * mulX));
        this.skeletonData = methodAccess.invoke(skeletonJsonObject, "readSkeletonData", rm.getSkeletonJSON(dataVO.animationName));
        this.skeletonObject = this.reflectionData.skeletonConstructorAccess.newInstance(this.skeletonData);
        Object animationStateDataObject = this.reflectionData.animationStateDataConstructorAccess.newInstance(this.skeletonData);
        computeBoundBox();
        this.stateObject = this.reflectionData.animationStateConstructorAccess.newInstance(animationStateDataObject);
        setAnimation((String) this.reflectionData.animationClassMethodAccess.invoke(((Array) this.reflectionData.skeletonDataClassMethodAccess.invoke(this.skeletonData, this.reflectionData.getAnimationMethodIndex, new Object[0])).get(0), this.reflectionData.getAnimNameMethodIndex, new Object[0]));
    }

    private void computeBoundBox() {
        this.reflectionData.skeletonClassMethodAccess.invoke(this.skeletonObject, this.reflectionData.updateWorldTransformMethodIndex, new Object[0]);
        this.minX = Float.MAX_VALUE;
        this.minY = Float.MAX_VALUE;
        float maxX = Float.MIN_VALUE;
        float maxY = Float.MIN_VALUE;
        Array<Object> slots = (Array) this.reflectionData.skeletonClassMethodAccess.invoke(this.skeletonObject, this.reflectionData.getSlotsMethodIndex, new Object[0]);
        int n = slots.size;
        for (int i = 0; i < n; i++) {
            Object slot = slots.get(i);
            Object attachment = this.reflectionData.slotClassMethodAccess.invoke(slot, this.reflectionData.getAttachmentMethodIndex, new Object[0]);
            if (attachment != null && this.reflectionData.regionAttachmentClass.isInstance(attachment)) {
                this.reflectionData.regionAttachmentMethodAccess.invoke(attachment, this.reflectionData.updateWorldVerticesMethodIndex, slot, false);
                float[] vertices = (float[]) this.reflectionData.regionAttachmentMethodAccess.invoke(attachment, this.reflectionData.getWorldVerticesIndex, new Object[0]);
                int nn = vertices.length;
                for (int ii = 0; ii < nn; ii += 5) {
                    this.minX = Math.min(this.minX, vertices[ii]);
                    this.minY = Math.min(this.minY, vertices[ii + 1]);
                    maxX = Math.max(maxX, vertices[ii]);
                    maxY = Math.max(maxY, vertices[ii + 1]);
                }
            }
        }
        this.width = maxX - this.minX;
        this.height = maxY - this.minY;
    }

    public void setAnimation(String animName) {
        if (animName == null) {
            System.out.println("NO ANIM NAME");
            return;
        }
        this.reflectionData.stateObjectMethodAccess.invoke(this.stateObject, this.reflectionData.setAnimationMethodIndex, 0, animName, true);
    }

    public Array<Object> getAnimations() {
        return (Array) this.reflectionData.skeletonDataClassMethodAccess.invoke(this.skeletonData, this.reflectionData.getAnimationMethodIndex, new Object[0]);
    }

    public void draw(Batch batch, float parentAlpha) {
        this.reflectionData.skeletonRendererMethodAccess.invoke(this.renderer, this.reflectionData.skeletonRendererDrawMethodIndex, batch, this.skeletonObject);
    }

    public void act(float delta, float x, float y) {
        this.reflectionData.skeletonClassMethodAccess.invoke(this.skeletonObject, this.reflectionData.updateWorldTransformMethodIndex, new Object[0]);
        this.reflectionData.stateObjectMethodAccess.invoke(this.stateObject, this.reflectionData.updateMethodIndex, Float.valueOf(delta));
        this.reflectionData.stateObjectMethodAccess.invoke(this.stateObject, this.reflectionData.applyMethodIndex, this.skeletonObject);
        this.reflectionData.skeletonClassMethodAccess.invoke(this.skeletonObject, this.reflectionData.setPositionMethodIndex, Float.valueOf(x - this.minX), Float.valueOf(y - this.minY));
    }
}
