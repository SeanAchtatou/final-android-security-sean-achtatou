package com.uwsoft.editor.renderer.data;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

public class ProjectInfoVO {
    public HashMap<String, String> assetMeshMap = new HashMap<>();
    public HashMap<String, MeshVO> meshes = new HashMap<>();
    public ResolutionEntryVO originalResolution = new ResolutionEntryVO();
    public ArrayList<ResolutionEntryVO> resolutions = new ArrayList<>();
    public ArrayList<SceneVO> scenes = new ArrayList<>();

    public String constructJsonString() {
        Json json = new Json();
        json.setOutputType(JsonWriter.OutputType.json);
        String str = json.toJson(this);
        json.prettyPrint(str);
        return str;
    }

    public ResolutionEntryVO getResolution(String name) {
        Iterator i$ = this.resolutions.iterator();
        while (i$.hasNext()) {
            ResolutionEntryVO resolution = i$.next();
            if (resolution.name.equalsIgnoreCase(name)) {
                return resolution;
            }
        }
        return null;
    }

    public String addNewMesh(MeshVO vo) {
        int key = -1;
        if (!(this.meshes == null || this.meshes.size() == 0)) {
            key = Integer.parseInt((String) Collections.max(this.meshes.keySet(), new Comparator<String>() {
                public int compare(String o1, String o2) {
                    return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
                }
            }));
        }
        int key2 = key + 1;
        this.meshes.put(key2 + "", vo);
        return key2 + "";
    }

    public String cloneMesh(String meshId) {
        MeshVO vo = this.meshes.get(meshId);
        return vo == null ? meshId : addNewMesh(vo.clone());
    }
}
