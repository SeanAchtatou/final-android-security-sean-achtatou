package com.uwsoft.editor.renderer.utils;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.brashmonkey.spriter.Drawer;
import com.brashmonkey.spriter.Loader;
import com.brashmonkey.spriter.Player;
import com.brashmonkey.spriter.Timeline;

public class LibGdxDrawer extends Drawer<Sprite> {
    Batch batch;
    ShapeRenderer renderer;

    public LibGdxDrawer(Loader<Sprite> loader, ShapeRenderer renderer2) {
        super(loader);
        this.renderer = renderer2;
    }

    public void setColor(float r, float g, float b, float a) {
        this.renderer.setColor(r, g, b, a);
    }

    public void rectangle(float x, float y, float width, float height) {
        this.renderer.rect(x, y, width, height);
    }

    public void line(float x1, float y1, float x2, float y2) {
        this.renderer.line(x1, y1, x2, y2);
    }

    public void circle(float x, float y, float radius) {
        this.renderer.circle(x, y, radius);
    }

    public void beforeDraw(Player player, Batch batch2) {
        this.batch = batch2;
        draw(player);
    }

    public void draw(Timeline.Key.Object object) {
        Sprite sprite = (Sprite) this.loader.get(object.ref);
        float newPivotX = sprite.getWidth() * object.pivot.x;
        float newX = object.position.x - newPivotX;
        float newPivotY = sprite.getHeight() * object.pivot.y;
        sprite.setX(newX);
        sprite.setY(object.position.y - newPivotY);
        sprite.setOrigin(newPivotX, newPivotY);
        sprite.setRotation(object.angle);
        sprite.setColor(1.0f, 1.0f, 1.0f, object.alpha);
        sprite.setScale(object.scale.x, object.scale.y);
        sprite.draw(this.batch);
    }
}
