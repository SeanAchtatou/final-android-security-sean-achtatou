package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.uwsoft.editor.renderer.data.CheckBoxVO;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.utils.CustomVariables;

public class CheckBoxItem extends CheckBox implements IBaseItem {
    private Body body;
    private CustomVariables customVariables;
    public CheckBoxVO dataVO;
    public Essentials essentials;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;

    public CheckBoxItem(CheckBoxVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        setParentItem(parent);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CheckBoxItem(com.uwsoft.editor.renderer.data.CheckBoxVO r7, com.uwsoft.editor.renderer.data.Essentials r8) {
        /*
            r6 = this;
            r4 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            java.lang.String r1 = r7.text
            com.uwsoft.editor.renderer.resources.IResourceRetriever r0 = r8.rm
            com.uwsoft.editor.renderer.utils.MySkin r2 = r0.getSkin()
            java.lang.String r0 = r7.style
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0073
            java.lang.String r0 = "default"
        L_0x0015:
            r6.<init>(r1, r2, r0)
            r6.mulX = r3
            r6.mulY = r3
            r6.layerIndex = r4
            com.uwsoft.editor.renderer.utils.CustomVariables r0 = new com.uwsoft.editor.renderer.utils.CustomVariables
            r0.<init>()
            r6.customVariables = r0
            r6.isLockedByLayer = r4
            r0 = 0
            r6.parentItem = r0
            r6.dataVO = r7
            r6.essentials = r8
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            float r0 = r0.x
            r6.setX(r0)
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            float r0 = r0.y
            r6.setY(r0)
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            float r0 = r0.scaleX
            r6.setScaleX(r0)
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            float r0 = r0.scaleY
            r6.setScaleY(r0)
            com.uwsoft.editor.renderer.utils.CustomVariables r0 = r6.customVariables
            com.uwsoft.editor.renderer.data.CheckBoxVO r1 = r6.dataVO
            java.lang.String r1 = r1.customVars
            r0.loadFromString(r1)
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            float r0 = r0.rotation
            r6.setRotation(r0)
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            int r0 = r0.zIndex
            if (r0 >= 0) goto L_0x0064
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            r0.zIndex = r4
        L_0x0064:
            com.uwsoft.editor.renderer.data.CheckBoxVO r0 = r6.dataVO
            float[] r0 = r0.tint
            if (r0 != 0) goto L_0x0076
            com.badlogic.gdx.graphics.Color r0 = new com.badlogic.gdx.graphics.Color
            r0.<init>(r3, r3, r3, r3)
            r6.setTint(r0)
        L_0x0072:
            return
        L_0x0073:
            java.lang.String r0 = r7.style
            goto L_0x0015
        L_0x0076:
            com.badlogic.gdx.graphics.Color r0 = new com.badlogic.gdx.graphics.Color
            com.uwsoft.editor.renderer.data.CheckBoxVO r1 = r6.dataVO
            float[] r1 = r1.tint
            r1 = r1[r4]
            com.uwsoft.editor.renderer.data.CheckBoxVO r2 = r6.dataVO
            float[] r2 = r2.tint
            r3 = 1
            r2 = r2[r3]
            com.uwsoft.editor.renderer.data.CheckBoxVO r3 = r6.dataVO
            float[] r3 = r3.tint
            r4 = 2
            r3 = r3[r4]
            com.uwsoft.editor.renderer.data.CheckBoxVO r4 = r6.dataVO
            float[] r4 = r4.tint
            r5 = 3
            r4 = r4[r5]
            r0.<init>(r1, r2, r3, r4)
            r6.setTint(r0)
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uwsoft.editor.renderer.actor.CheckBoxItem.<init>(com.uwsoft.editor.renderer.data.CheckBoxVO, com.uwsoft.editor.renderer.data.Essentials):void");
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public CheckBoxVO getDataVO() {
        return this.dataVO;
    }

    public void renew() {
        setText(this.dataVO.text);
        pack();
        layout();
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        setScaleX(this.dataVO.scaleX * this.mulX);
        setScaleY(this.dataVO.scaleY * this.mulY);
        setRotation(this.dataVO.rotation);
        setColor(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]);
        this.customVariables.loadFromString(this.dataVO.customVars);
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void updateDataVO() {
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        this.dataVO.rotation = getRotation();
        if (getZIndex() >= 0) {
            this.dataVO.zIndex = getZIndex();
        }
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public void applyResolution(float mulX2, float mulY2) {
        setScaleX(this.dataVO.scaleX * mulX2);
        setScaleY(this.dataVO.scaleY * mulY2);
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        updateDataVO();
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setStyle(CheckBox.CheckBoxStyle lst, String styleName) {
        setStyle(lst);
        this.dataVO.style = styleName;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || this.body == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }
}
