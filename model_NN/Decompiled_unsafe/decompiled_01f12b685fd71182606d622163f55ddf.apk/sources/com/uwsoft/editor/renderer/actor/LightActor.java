package com.uwsoft.editor.renderer.actor;

import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.LightVO;
import com.uwsoft.editor.renderer.utils.CustomVariables;

public class LightActor extends Actor implements IBaseItem {
    private Body body;
    private CustomVariables customVariables;
    private LightVO dataVO;
    private Image debugImg;
    private float direction;
    public Essentials essentials;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public Light lightObject;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;
    public RayHandler rayHandler;
    private Vector2 tmpVector;

    public LightActor(LightVO data, Essentials e, CompositeItem parent) {
        this(data, e);
        setParentItem(parent);
    }

    public LightActor(LightVO data, Essentials e) {
        this.lightObject = null;
        this.rayHandler = null;
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.tmpVector = new Vector2();
        this.customVariables = new CustomVariables();
        this.essentials = e;
        this.rayHandler = this.essentials.rayHandler;
        this.dataVO = data;
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        this.customVariables.loadFromString(this.dataVO.customVars);
        if (this.dataVO.type == LightVO.LightType.POINT) {
            createPointLight();
        } else {
            createConeLight();
        }
        setWidth(40.0f);
        setHeight(40.0f);
    }

    public void createPointLight() {
        this.lightObject = new PointLight(this.rayHandler, this.dataVO.rays);
        this.lightObject.setColor(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        this.lightObject.setDistance(this.dataVO.distance * this.mulX * 0.1f);
        this.lightObject.setPosition(this.dataVO.x * this.mulX * 0.1f, this.dataVO.y * this.mulY * 0.1f);
        this.lightObject.setStaticLight(this.dataVO.isStatic);
        this.lightObject.setActive(true);
        this.lightObject.setXray(this.dataVO.isXRay);
    }

    public void createConeLight() {
        this.lightObject = new ConeLight(this.rayHandler, this.dataVO.rays, Color.WHITE, 1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.lightObject.setColor(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        this.lightObject.setDistance(this.dataVO.distance * this.mulX * 0.1f);
        this.lightObject.setPosition(this.dataVO.x * this.mulX * 0.1f, this.dataVO.y * this.mulY * 0.1f);
        this.lightObject.setStaticLight(this.dataVO.isStatic);
        this.direction = this.dataVO.directionDegree;
        this.lightObject.setDirection(this.direction);
        ((ConeLight) this.lightObject).setConeDegree(this.dataVO.coneDegree);
        this.lightObject.setActive(true);
        this.lightObject.setXray(this.dataVO.isXRay);
    }

    public void act(float delta) {
        float relativeX = getX();
        float relativeY = getY();
        float relativeRotation = Animation.CurveTimeline.LINEAR;
        for (Group currParent = getParent(); currParent != null; currParent = currParent.getParent()) {
            relativeX += currParent.getX();
            relativeY += currParent.getY();
            relativeRotation += currParent.getRotation();
        }
        if (this.lightObject != null) {
            float yy = Animation.CurveTimeline.LINEAR;
            float xx = Animation.CurveTimeline.LINEAR;
            if (relativeRotation != Animation.CurveTimeline.LINEAR) {
                float xx2 = (getX() * MathUtils.cosDeg(relativeRotation)) - (getY() * MathUtils.sinDeg(relativeRotation));
                yy = getY() - ((getY() * MathUtils.cosDeg(relativeRotation)) + (getX() * MathUtils.sinDeg(relativeRotation)));
                xx = getX() - xx2;
            }
            this.lightObject.setPosition(((relativeX - xx) + 20.0f) * 0.1f, ((relativeY - yy) + 20.0f) * 0.1f);
        }
        if (this.dataVO.type == LightVO.LightType.CONE) {
            this.lightObject.setDirection(this.direction + relativeRotation);
        }
        super.act(delta);
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    public void changeDistance(int amount) {
        this.lightObject.setDistance(((float) amount) * 0.1f);
    }

    public LightVO getDataVO() {
        return this.dataVO;
    }

    public void renew() {
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        removeLights();
        if (this.dataVO.type == LightVO.LightType.POINT) {
            createPointLight();
        } else {
            createConeLight();
        }
        this.customVariables.loadFromString(this.dataVO.customVars);
    }

    public float getRotation() {
        if (this.dataVO.type == LightVO.LightType.POINT) {
            return Animation.CurveTimeline.LINEAR;
        }
        return super.getRotation();
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void updateDataVO() {
        this.dataVO.distance = (((float) ((int) this.lightObject.getDistance())) / this.mulX) / 0.1f;
        System.out.println("dataVO.distance = " + this.dataVO.distance);
        this.dataVO.directionDegree = this.direction;
        if (this.dataVO.type == LightVO.LightType.CONE) {
            this.dataVO.coneDegree = ((ConeLight) this.lightObject).getConeDegree();
        }
        this.dataVO.x = getX() / this.mulX;
        this.dataVO.y = getY() / this.mulY;
        if (this.dataVO.layerName == null || this.dataVO.layerName.equals("")) {
            this.dataVO.layerName = "Default";
        }
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        if (this.lightObject != null) {
            this.lightObject.setDistance(this.dataVO.distance * this.mulX * 0.1f);
        }
        updateDataVO();
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public void setColor(Color color) {
        if (color != null) {
            this.lightObject.setColor(color);
        }
    }

    public Color getColor() {
        return this.lightObject.getColor();
    }

    public void setRotation(float degrees) {
        if (this.dataVO.type == LightVO.LightType.CONE) {
            this.lightObject.setDirection(this.direction + degrees);
        }
    }

    public void rotateBy(float ammount) {
        if (this.dataVO.type == LightVO.LightType.POINT) {
            this.lightObject.setDistance(this.lightObject.getDistance() + (0.1f * ammount));
            return;
        }
        this.direction += ammount;
        this.lightObject.setDirection(this.direction);
    }

    public void dispose() {
        removeLights();
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public void removeLights() {
        if (this.lightObject != null) {
            this.lightObject.remove();
            this.lightObject = null;
        }
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }
}
