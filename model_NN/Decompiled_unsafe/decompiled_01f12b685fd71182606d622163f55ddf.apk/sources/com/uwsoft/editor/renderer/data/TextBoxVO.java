package com.uwsoft.editor.renderer.data;

import com.kbz.esotericsoftware.spine.Animation;

public class TextBoxVO extends MainItemVO {
    public String defaultText = "";
    public float height = Animation.CurveTimeline.LINEAR;
    public String style = "";
    public float width = Animation.CurveTimeline.LINEAR;

    public TextBoxVO() {
    }

    public TextBoxVO(TextBoxVO vo) {
        super(vo);
        this.defaultText = new String(vo.defaultText);
        this.width = vo.width;
        this.height = vo.height;
        this.style = new String(vo.style);
    }
}
