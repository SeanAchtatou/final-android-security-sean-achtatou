package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.esotericsoftware.spine.Animation;

public class ParticleActor extends Actor {
    private float lastDelta = Animation.CurveTimeline.LINEAR;
    private ParticleEffect particleEffect;

    public ParticleActor(ParticleEffect effect) {
        setVisible(true);
        this.particleEffect = effect;
    }

    public void setPosition(float xx, float yy) {
        setX(xx);
        setY(yy);
        this.particleEffect.setPosition(getX(), getY());
    }

    public void start() {
        this.particleEffect.reset();
        this.particleEffect.start();
    }

    public void stop() {
        for (int j = 0; j < this.particleEffect.getEmitters().size; j++) {
            this.particleEffect.getEmitters().get(j).setContinuous(false);
            this.particleEffect.getEmitters().get(j).allowCompletion();
        }
    }

    public ParticleEffect getParticleEffect() {
        return this.particleEffect;
    }

    public void act(float delta) {
        this.lastDelta = delta;
        this.particleEffect.setPosition(getX(), getY());
    }

    public void fastForward(float time) {
        act(time);
    }

    public void draw(Batch batch, float parentAlpha) {
        this.particleEffect.draw(batch, this.lastDelta);
    }
}
