package com.uwsoft.editor.renderer;

import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.SceneVO;
import com.uwsoft.editor.renderer.resources.ResourceManager;
import com.uwsoft.editor.renderer.spine.SpineReflectionHelper;

public class Overlap2DStage extends Stage {
    public Essentials essentials;
    public boolean physiscStopped = false;
    public SceneLoader sceneLoader;
    private float timeAcc = Animation.CurveTimeline.LINEAR;

    public Overlap2DStage() {
        initStage();
    }

    public Overlap2DStage(Viewport viewPort) {
        super(viewPort);
        initStage();
    }

    /* access modifiers changed from: protected */
    public void initStage() {
        this.essentials = new Essentials();
        SpineReflectionHelper refdata = new SpineReflectionHelper();
        if (refdata.isSpineAviable) {
            this.essentials.spineReflectionHelper = refdata;
        }
        initLightsConfiguration();
    }

    public void loadScene(String name) {
        this.sceneLoader.loadScene(name);
        setAmbienceInfo(this.sceneLoader.getSceneVO());
        addActor(this.sceneLoader.getRoot());
    }

    public void initSceneLoader() {
        ResourceManager rm = new ResourceManager();
        rm.initAllResources();
        this.sceneLoader = new SceneLoader(this.essentials);
        this.essentials.rayHandler.setWorld(this.essentials.world);
        this.essentials.rm = rm;
    }

    public void initSceneLoader(ResourceManager rm) {
        this.sceneLoader = new SceneLoader(this.essentials);
        this.essentials.rayHandler.setWorld(this.essentials.world);
        this.essentials.rm = rm;
    }

    /* access modifiers changed from: protected */
    public void initLightsConfiguration() {
        RayHandler.setGammaCorrection(true);
        RayHandler.useDiffuseLight(true);
        RayHandler rayHandler = new RayHandler(null);
        rayHandler.setAmbientLight(1.0f, 1.0f, 1.0f, 1.0f);
        rayHandler.setCulling(true);
        rayHandler.setBlur(true);
        rayHandler.setBlurNum(3);
        rayHandler.setShadows(true);
        rayHandler.setCombinedMatrix(getCamera().combined);
        this.essentials.rayHandler = rayHandler;
    }

    public void act(float delta) {
        if (this.essentials.world != null && !this.physiscStopped) {
            while (this.timeAcc < delta) {
                this.timeAcc += 0.016666668f;
                this.essentials.world.step(0.016666668f, 10, 10);
            }
            this.timeAcc -= delta;
        }
        if (this.essentials.rayHandler != null) {
            this.essentials.rayHandler.setCombinedMatrix(getCamera().combined.scl(10.0f));
        }
        super.act(delta);
    }

    public void draw() {
        super.draw();
        if (this.essentials.rayHandler != null) {
            this.essentials.rayHandler.updateAndRender();
        }
    }

    public void setAmbienceInfo(SceneVO vo) {
        this.essentials.rayHandler.setAmbientLight(new Color(vo.ambientColor[0], vo.ambientColor[1], vo.ambientColor[2], vo.ambientColor[3]));
    }

    public World getWorld() {
        return this.essentials.world;
    }
}
