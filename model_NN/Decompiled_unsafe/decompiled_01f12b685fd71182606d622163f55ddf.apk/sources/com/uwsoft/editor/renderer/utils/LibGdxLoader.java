package com.uwsoft.editor.renderer.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.FileReference;
import com.brashmonkey.spriter.Loader;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.Opcodes;

public class LibGdxLoader extends Loader<Sprite> implements Disposable {
    public static int standardAtlasHeight = Opcodes.ACC_STRICT;
    public static int standardAtlasWidth = Opcodes.ACC_STRICT;
    private int atlasHeight;
    private int atlasWidth;
    private boolean pack;
    private PixmapPacker packer;
    private HashMap<FileReference, Pixmap> pixmaps;
    private HashMap<Pixmap, Boolean> pixmapsToDispose;

    public LibGdxLoader(Data data) {
        this(data, true);
    }

    public LibGdxLoader(Data data, boolean pack2) {
        this(data, standardAtlasWidth, standardAtlasHeight);
        this.pack = pack2;
    }

    public LibGdxLoader(Data data, int atlasWidth2, int atlasHeight2) {
        super(data);
        this.pack = true;
        this.atlasWidth = atlasWidth2;
        this.atlasHeight = atlasHeight2;
        this.pixmaps = new HashMap<>();
        this.pixmapsToDispose = new HashMap<>();
    }

    /* access modifiers changed from: protected */
    public Sprite loadResource(FileReference ref) {
        FileHandle f;
        String path = this.root + "/" + new File(this.data.getFile(ref).name).getName();
        System.out.println(path);
        switch (Gdx.app.getType()) {
            case iOS:
                f = Gdx.files.absolute(path);
                break;
            default:
                f = Gdx.files.internal(path);
                break;
        }
        if (!f.exists()) {
            throw new GdxRuntimeException("Could not find file handle " + path + "! Please check your paths.");
        }
        if (this.packer == null && this.pack) {
            this.packer = new PixmapPacker(this.atlasWidth, this.atlasHeight, Pixmap.Format.RGBA8888, 2, true);
        }
        this.pixmaps.put(ref, new Pixmap(f));
        return null;
    }

    /* access modifiers changed from: protected */
    public void generatePackedSprites() {
        if (this.packer != null) {
            TextureAtlas tex = this.packer.generateTextureAtlas(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear, false);
            Set<FileReference> keys = this.resources.keySet();
            disposeNonPackedTextures();
            for (FileReference ref : keys) {
                TextureRegion texReg = tex.findRegion(this.data.getFile(ref).name);
                texReg.setRegionWidth((int) this.data.getFile(ref).size.width);
                texReg.setRegionHeight((int) this.data.getFile(ref).size.height);
                this.resources.put(ref, new Sprite(texReg));
            }
        }
    }

    private void disposeNonPackedTextures() {
        for (Map.Entry<FileReference, R> entry : this.resources.entrySet()) {
            ((Sprite) entry.getValue()).getTexture().dispose();
        }
    }

    public void dispose() {
        if (!this.pack || this.packer == null) {
            disposeNonPackedTextures();
        } else {
            this.packer.dispose();
        }
        super.dispose();
    }

    /* access modifiers changed from: protected */
    public void finishLoading() {
        for (FileReference ref : this.resources.keySet()) {
            Pixmap pix = this.pixmaps.get(ref);
            this.pixmapsToDispose.put(pix, false);
            createSprite(ref, pix);
            if (this.packer != null) {
                this.packer.pack(this.data.getFile(ref).name, pix);
            }
        }
        if (this.pack) {
            generatePackedSprites();
        }
        disposePixmaps();
    }

    /* access modifiers changed from: protected */
    public void createSprite(FileReference ref, Pixmap image) {
        Texture tex = new Texture(image);
        tex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        this.resources.put(ref, new Sprite(new TextureRegion(tex, (int) this.data.getFile(ref.folder, ref.file).size.width, (int) this.data.getFile(ref.folder, ref.file).size.height)));
        this.pixmapsToDispose.put(image, true);
    }

    /* access modifiers changed from: protected */
    public void disposePixmaps() {
        Pixmap[] maps = new Pixmap[this.pixmapsToDispose.size()];
        this.pixmapsToDispose.keySet().toArray(maps);
        for (Pixmap pix : maps) {
            while (this.pixmapsToDispose.get(pix).booleanValue()) {
                try {
                    pix.dispose();
                    this.pixmapsToDispose.put(pix, false);
                } catch (GdxRuntimeException e) {
                    System.err.println("Pixmap was already disposed!");
                }
            }
        }
        this.pixmapsToDispose.clear();
    }
}
