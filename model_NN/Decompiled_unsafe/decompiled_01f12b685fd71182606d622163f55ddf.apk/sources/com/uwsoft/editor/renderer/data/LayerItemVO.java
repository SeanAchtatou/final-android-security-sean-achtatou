package com.uwsoft.editor.renderer.data;

public class LayerItemVO {
    public boolean isLocked = false;
    public boolean isVisible = true;
    public String layerName = "";

    public LayerItemVO() {
    }

    public LayerItemVO(String name) {
        this.layerName = new String(name);
    }

    public LayerItemVO(LayerItemVO vo) {
        this.layerName = new String(vo.layerName);
        this.isLocked = vo.isLocked;
        this.isVisible = vo.isVisible;
    }
}
