package com.uwsoft.editor.renderer.data;

import com.kbz.esotericsoftware.spine.Animation;
import java.util.ArrayList;

public class SelectBoxVO extends MainItemVO {
    public float height = Animation.CurveTimeline.LINEAR;
    public ArrayList<String> list = new ArrayList<>();
    public String style = "";
    public float width = Animation.CurveTimeline.LINEAR;

    public SelectBoxVO() {
    }

    public SelectBoxVO(SelectBoxVO vo) {
        super(vo);
        this.width = vo.width;
        this.height = vo.height;
        this.style = new String(vo.style);
    }
}
