package com.uwsoft.editor.renderer.data;

import com.kbz.esotericsoftware.spine.Animation;

public class LabelVO extends MainItemVO {
    public int align;
    public float height = Animation.CurveTimeline.LINEAR;
    public boolean multiline = false;
    public int size;
    public String style = "";
    public String text = "Label";
    public float width = Animation.CurveTimeline.LINEAR;

    public LabelVO() {
    }

    public LabelVO(LabelVO vo) {
        super(vo);
        this.text = new String(vo.text);
        this.style = new String(vo.style);
        this.size = vo.size;
        this.align = vo.align;
        this.width = vo.width;
        this.height = vo.height;
        this.multiline = vo.multiline;
    }
}
