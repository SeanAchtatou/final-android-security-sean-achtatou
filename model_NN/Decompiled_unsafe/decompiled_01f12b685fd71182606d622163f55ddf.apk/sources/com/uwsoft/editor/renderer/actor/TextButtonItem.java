package com.uwsoft.editor.renderer.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.uwsoft.editor.renderer.data.ButtonVO;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.utils.CustomVariables;

public class TextButtonItem extends TextButton implements IBaseItem {
    private Body body;
    private CustomVariables customVariables;
    public ButtonVO dataVO;
    public Essentials essentials;
    private boolean isLockedByLayer;
    protected int layerIndex;
    public float mulX;
    public float mulY;
    private CompositeItem parentItem;

    public TextButtonItem(ButtonVO vo, Essentials e, CompositeItem parent) {
        this(vo, e);
        setParentItem(parent);
    }

    public TextButtonItem(ButtonVO vo, Essentials e) {
        super(vo.text, e.rm.getSkin());
        this.mulX = 1.0f;
        this.mulY = 1.0f;
        this.layerIndex = 0;
        this.isLockedByLayer = false;
        this.parentItem = null;
        this.customVariables = new CustomVariables();
        this.dataVO = vo;
        this.essentials = e;
        setX(this.dataVO.x);
        setY(this.dataVO.y);
        this.customVariables.loadFromString(this.dataVO.customVars);
        getLabel().setFontScale(this.dataVO.scaleX, this.dataVO.scaleY);
        setRotation(this.dataVO.rotation);
        if (this.dataVO.zIndex < 0) {
            this.dataVO.zIndex = 0;
        }
        if (this.dataVO.tint == null) {
            setTint(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        } else {
            setTint(new Color(this.dataVO.tint[0], this.dataVO.tint[1], this.dataVO.tint[2], this.dataVO.tint[3]));
        }
        pack();
        layout();
    }

    public void setTint(Color tint) {
        getDataVO().tint = new float[]{tint.r, tint.g, tint.b, tint.a};
        setColor(tint);
    }

    public ButtonVO getDataVO() {
        return this.dataVO;
    }

    public void updateDataVO() {
        this.dataVO.x = getX();
        this.dataVO.y = getY();
        this.dataVO.customVars = this.customVariables.saveAsString();
    }

    public void renew() {
        setText(this.dataVO.text);
        this.customVariables.loadFromString(this.dataVO.customVars);
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        getLabel().setFontScale(this.dataVO.scaleX * this.mulX, this.dataVO.scaleY * this.mulY);
        setRotation(this.dataVO.rotation);
        pack();
        layout();
    }

    public boolean isLockedByLayer() {
        return this.isLockedByLayer;
    }

    public void setLockByLayer(boolean isLocked) {
        this.isLockedByLayer = isLocked;
    }

    public boolean isComposite() {
        return false;
    }

    public void applyResolution(float mulX2, float mulY2) {
        this.mulX = mulX2;
        this.mulY = mulY2;
        setX(this.dataVO.x * this.mulX);
        setY(this.dataVO.y * this.mulY);
        getLabel().setFontScale(this.dataVO.scaleX * this.mulX, this.dataVO.scaleY * this.mulY);
        updateDataVO();
        pack();
        layout();
    }

    public int getLayerIndex() {
        return this.layerIndex;
    }

    public void setLayerIndex(int index) {
        this.layerIndex = index;
    }

    public CompositeItem getParentItem() {
        return this.parentItem;
    }

    public void setParentItem(CompositeItem parentItem2) {
        this.parentItem = parentItem2;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        this.body = body2;
    }

    public void dispose() {
        if (!(this.essentials.world == null || getBody() == null)) {
            this.essentials.world.destroyBody(getBody());
        }
        setBody(null);
    }

    public CustomVariables getCustomVariables() {
        return this.customVariables;
    }
}
