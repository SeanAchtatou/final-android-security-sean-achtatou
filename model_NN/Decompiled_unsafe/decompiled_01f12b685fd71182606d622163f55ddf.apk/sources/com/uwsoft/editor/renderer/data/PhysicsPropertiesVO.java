package com.uwsoft.editor.renderer.data;

import com.kbz.esotericsoftware.spine.Animation;

public class PhysicsPropertiesVO {
    public boolean enabled;
    public float gravityX;
    public float gravityY;
    public float sleepVelocity;

    public PhysicsPropertiesVO() {
        this.gravityX = Animation.CurveTimeline.LINEAR;
        this.gravityY = Animation.CurveTimeline.LINEAR;
        this.sleepVelocity = Animation.CurveTimeline.LINEAR;
        this.enabled = false;
    }

    public PhysicsPropertiesVO(PhysicsPropertiesVO physicsPropertiesVO) {
        this.gravityX = physicsPropertiesVO.gravityX;
        this.gravityY = physicsPropertiesVO.gravityY;
        this.sleepVelocity = physicsPropertiesVO.sleepVelocity;
        this.enabled = physicsPropertiesVO.enabled;
    }
}
