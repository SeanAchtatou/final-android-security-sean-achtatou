package com.uwsoft.editor.renderer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.IBaseItem;
import com.uwsoft.editor.renderer.data.CompositeItemVO;
import com.uwsoft.editor.renderer.data.CompositeVO;
import com.uwsoft.editor.renderer.data.Essentials;
import com.uwsoft.editor.renderer.data.PhysicsPropertiesVO;
import com.uwsoft.editor.renderer.data.SceneVO;
import com.uwsoft.editor.renderer.data.SimpleImageVO;
import com.uwsoft.editor.renderer.resources.IResourceRetriever;
import com.uwsoft.editor.renderer.resources.ResourceManager;
import com.uwsoft.editor.renderer.script.IScript;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class SceneLoader {
    private String curResolution = "orig";
    public Essentials essentials;
    public CompositeItem sceneActor;
    private SceneVO sceneVO;

    public SceneLoader() {
        ResourceManager rm = new ResourceManager();
        rm.initAllResources();
        Essentials emptyEssentuials = new Essentials();
        emptyEssentuials.rm = rm;
        this.essentials = emptyEssentuials;
    }

    public SceneLoader(String resolution) {
        ResourceManager rm = new ResourceManager();
        rm.setWorkingResolution(resolution);
        rm.initAllResources();
        Essentials emptyEssentuials = new Essentials();
        emptyEssentuials.rm = rm;
        this.essentials = emptyEssentuials;
        this.curResolution = resolution;
    }

    public SceneLoader(IResourceRetriever rm) {
        Essentials emptyEssentuials = new Essentials();
        emptyEssentuials.rm = rm;
        this.essentials = emptyEssentuials;
    }

    public SceneLoader(Essentials e) {
        this.essentials = e;
    }

    public void setResolution(String resolutionName) {
        this.curResolution = resolutionName;
        if (this.sceneActor != null) {
            this.sceneActor.applyResolution(resolutionName);
        }
    }

    public SceneVO getSceneVO() {
        return this.sceneVO;
    }

    public SceneVO loadScene(String sceneName, boolean createActors) {
        this.sceneVO = this.essentials.rm.getSceneVO(sceneName);
        PhysicsPropertiesVO physicsProperties = this.sceneVO.physicsPropertiesVO;
        if (this.sceneVO.physicsPropertiesVO != null && this.sceneVO.physicsPropertiesVO.enabled) {
            this.essentials.world = new World(new Vector2(physicsProperties.gravityX, physicsProperties.gravityY), true);
            this.essentials.rayHandler.setWorld(this.essentials.world);
        }
        invalidateSceneVO(this.sceneVO);
        if (createActors) {
            this.sceneActor = getSceneAsActor();
            if (!this.curResolution.equals("orig")) {
                this.sceneActor.applyResolution(this.curResolution);
            }
        }
        setAmbienceInfo(this.sceneVO);
        return this.sceneVO;
    }

    public SceneVO loadScene(String sceneName) {
        return loadScene(sceneName, true);
    }

    public void invalidateSceneVO(SceneVO vo) {
        removeMissingImages(vo.composite);
    }

    public void removeMissingImages(CompositeVO vo) {
        if (vo != null) {
            Iterator i$ = vo.sImages.iterator();
            while (i$.hasNext()) {
                SimpleImageVO img = i$.next();
                if (this.essentials.rm.getTextureRegion(img.imageName) == null) {
                    vo.sImages.remove(img);
                }
            }
            Iterator i$2 = vo.sComposites.iterator();
            while (i$2.hasNext()) {
                removeMissingImages(i$2.next().composite);
            }
        }
    }

    public void setAmbienceInfo(SceneVO vo) {
        if (this.essentials.rayHandler != null && vo.ambientColor != null) {
            this.essentials.rayHandler.setAmbientLight(new Color(vo.ambientColor[0], vo.ambientColor[1], vo.ambientColor[2], vo.ambientColor[3]));
        }
    }

    public CompositeItem getSceneAsActor() {
        CompositeItemVO vo = new CompositeItemVO(this.sceneVO.composite);
        if (vo.composite == null) {
            vo.composite = new CompositeVO();
        }
        return new CompositeItem(vo, this.essentials);
    }

    public CompositeItem getLibraryAsActor(String name) {
        CompositeItemVO vo = new CompositeItemVO(this.sceneVO.libraryItems.get(name));
        if (vo.composite == null) {
            vo.composite = new CompositeVO();
        }
        CompositeItem cnt = new CompositeItem(vo, this.essentials);
        cnt.dataVO.itemName = name;
        cnt.applyResolution(this.curResolution);
        cnt.setX(Animation.CurveTimeline.LINEAR);
        cnt.setY(Animation.CurveTimeline.LINEAR);
        return cnt;
    }

    public CompositeItem getCompositeElementById(String id) {
        return getCompositeElement(this.sceneActor.getCompositeById(id).getDataVO());
    }

    public CompositeItem getCompositeElement(CompositeItemVO vo) {
        return new CompositeItem(vo, this.essentials);
    }

    public void addScriptTo(String name, IScript iScript) {
        this.sceneActor.addScriptTo(name, iScript);
    }

    public void addScriptTo(String name, ArrayList<IScript> iScripts) {
        this.sceneActor.addScriptTo(name, iScripts);
    }

    public IResourceRetriever getRm() {
        return this.essentials.rm;
    }

    public CompositeItem getRoot() {
        return this.sceneActor;
    }

    public void inject(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        System.out.println(fields.length);
        for (Field field : fields) {
            System.out.println(field.getName());
            if (IBaseItem.class.isAssignableFrom(field.getType())) {
                Class<?> type = field.getType();
                Class<?> realType = field.getType();
                System.out.println(Arrays.toString(field.getDeclaredAnnotations()));
                if (field.isAnnotationPresent(Overlap2D.class)) {
                    System.out.println("annotation found");
                    IBaseItem result = getRoot().getById(field.getName(), type);
                    System.out.println(result);
                    try {
                        field.set(object, realType.cast(result));
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
            }
        }
    }
}
