package com.uwsoft.editor.renderer.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.kbz.esotericsoftware.spine.Animation;
import com.uwsoft.editor.renderer.data.MeshVO;
import com.uwsoft.editor.renderer.data.PhysicsBodyDataVO;

public class PhysicsBodyLoader {
    public static final float SCALE = 0.1f;
    private final World world;

    public PhysicsBodyLoader(World world2) {
        this.world = world2;
    }

    public Body createBody(PhysicsBodyDataVO data, MeshVO mesh, Vector2 mulVec) {
        return createBody(this.world, data, mesh, mulVec);
    }

    public static Body createBody(World world2, PhysicsBodyDataVO data, MeshVO mesh, Vector2 mulVec) {
        FixtureDef fixtureDef = new FixtureDef();
        if (data != null) {
            fixtureDef.density = data.density;
            fixtureDef.friction = data.friction;
            fixtureDef.restitution = data.restitution;
        }
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        if (data.bodyType == 0) {
            bodyDef.type = BodyDef.BodyType.StaticBody;
        } else if (data.bodyType == 1) {
            bodyDef.type = BodyDef.BodyType.KinematicBody;
        } else {
            bodyDef.type = BodyDef.BodyType.DynamicBody;
        }
        Body body = world2.createBody(bodyDef);
        PolygonShape polygonShape = new PolygonShape();
        for (int i = 0; i < mesh.minPolygonData.length; i++) {
            float[] verts = new float[(mesh.minPolygonData[i].length * 2)];
            for (int j = 0; j < verts.length; j += 2) {
                verts[j] = mesh.minPolygonData[i][j / 2].x * mulVec.x * 0.1f;
                verts[j + 1] = mesh.minPolygonData[i][j / 2].y * mulVec.y * 0.1f;
            }
            polygonShape.set(verts);
            fixtureDef.shape = polygonShape;
            body.createFixture(fixtureDef);
        }
        return body;
    }
}
