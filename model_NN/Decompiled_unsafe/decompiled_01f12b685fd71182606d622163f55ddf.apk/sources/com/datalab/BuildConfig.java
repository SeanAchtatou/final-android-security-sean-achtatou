package com.datalab;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.datalab";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "xiaomi";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
    public static final String channel = "120";
    public static final String channelId = "-1";
    public static final String game = "dlzs3";
    public static final String gameVersion = "1.3.0";
}
