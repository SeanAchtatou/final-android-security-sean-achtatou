package com.datalab.tools;

import android.content.Context;
import android.content.SharedPreferences;
import com.datalab.SGManager;
import java.util.Calendar;

public class PayLimit {
    public static final String DAY_OF_MONTH = "day";
    private static final String LIMIT_NAME = "limit";
    public static final String MONTH = "month";
    public static final String YEAR = "year";

    public static boolean openLimit() {
        return !"0".equals(SGManager.getResult("limit"));
    }

    public static boolean isLimit(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("limit", 0);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(1);
        int month = calendar.get(2);
        int monthday = calendar.get(5);
        int oldYear = preferences.getInt(YEAR, 2016);
        int oldMonth = preferences.getInt(MONTH, 0);
        int oldMonthDay = preferences.getInt(DAY_OF_MONTH, 1);
        String moneys = SGManager.getResult("limit");
        int moneyLimit = Constant.DEFAULT_LIMIT;
        if (moneys != null) {
            try {
                moneyLimit = Integer.parseInt(moneys) * 100;
            } catch (Exception e) {
            }
        }
        int money = preferences.getInt("limit", 0);
        if (monthday == oldMonthDay && month == oldMonth && year == oldYear) {
            return money >= moneyLimit;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(YEAR, year);
        editor.putInt(MONTH, month);
        editor.putInt(DAY_OF_MONTH, monthday);
        editor.putInt("limit", 0);
        editor.commit();
        return false;
    }

    public static void addMoney(Context context, int money) {
        SharedPreferences preferences = context.getSharedPreferences("limit", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("limit", preferences.getInt("limit", 0) + money);
        editor.commit();
    }
}
