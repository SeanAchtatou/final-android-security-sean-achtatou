package com.datalab.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.wali.gamecenter.report.io.HttpConnectionManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public final class DataSubmitter {
    private static final String CPU = encodeURL(Build.CPU_ABI);
    private static final String MODEL = encodeURL(Build.MODEL);
    private static final String OS = encodeURL("Android " + Build.VERSION.RELEASE);
    private static final String TAG = "BindService";
    private static final String URL = "http://switch.jssg.com.cn:8080/WebTest/DataSubmit";
    private static final int VERSION = 1;
    private static DataSubmitter instance;
    private ActionPool actionPool = new ActionPool();
    /* access modifiers changed from: private */
    public Context activity;
    private String channel;
    private long firstTime = System.currentTimeMillis();
    private String game;
    private String iccid;
    /* access modifiers changed from: private */
    public long uid = -1;
    private String version;

    private DataSubmitter(Context activity2, String game2, String version2, String channel2) {
        this.activity = activity2;
        this.iccid = encodeURL(getICCID(activity2));
        this.game = encodeURL(game2);
        this.version = encodeURL(version2);
        this.channel = encodeURL(channel2);
        new Thread(this.actionPool).start();
    }

    private String getICCID(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
    }

    public static DataSubmitter newInstance(Context activity2, String game2, String version2, String channel2) {
        if (instance == null) {
            instance = new DataSubmitter(activity2, game2, version2, channel2);
        } else {
            instance.activity = activity2;
            instance.iccid = encodeURL(instance.getICCID(activity2));
            instance.game = encodeURL(game2);
            instance.version = encodeURL(version2);
            instance.channel = encodeURL(channel2);
            instance.firstTime = System.currentTimeMillis();
        }
        return instance;
    }

    public static DataSubmitter getInstance() {
        return instance;
    }

    public static String merge(String[] parameter) {
        if (parameter == null) {
            return "";
        }
        StringBuilder buffer = new StringBuilder();
        for (int k = 0; k < parameter.length; k++) {
            buffer.append(parameter[k]);
            if (k < parameter.length - 1) {
                buffer.append(',');
            }
        }
        return buffer.toString();
    }

    /* access modifiers changed from: private */
    public boolean regist() {
        if (!isOnline()) {
            return false;
        }
        HttpURLConnection con = null;
        InputStream in = null;
        OutputStream out = null;
        try {
            HttpURLConnection con2 = (HttpURLConnection) new URL(URL).openConnection();
            con2.setRequestMethod(Net.HttpMethods.POST);
            con2.setConnectTimeout(HttpConnectionManager.GPRS_WAIT_TIMEOUT);
            con2.setReadTimeout(HttpConnectionManager.GPRS_WAIT_TIMEOUT);
            con2.setDoInput(true);
            con2.setDoOutput(true);
            byte[] data = ("uid=" + this.uid + "&content=" + encodeURL(Base64.encodeToString(("os=" + OS + "&cpu=" + CPU + "&model=" + MODEL + "&iccid=" + this.iccid + "&game=" + this.game + "&version=" + this.version + "&ch=" + this.channel + "&firstTime=" + this.firstTime).getBytes("UTF-8"), 2))).getBytes("UTF-8");
            con2.setRequestProperty(HttpRequestHeader.UserAgent, "Data Acquisition SDK");
            con2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con2.setRequestProperty(HttpRequestHeader.Accept, "*/*");
            con2.setRequestProperty(HttpRequestHeader.AcceptLanguage, "zh-cn");
            con2.setRequestProperty(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
            con2.setRequestProperty("Content-Length", String.valueOf(data.length));
            con2.setRequestProperty("Connection", "Close");
            OutputStream out2 = con2.getOutputStream();
            out2.write(data);
            out2.flush();
            if (con2.getResponseCode() == 200) {
                InputStream in2 = con2.getInputStream();
                byte[] data2 = new byte[con2.getContentLength()];
                in2.read(data2);
                String num = new String(data2, "UTF-8");
                Log.i(TAG, num);
                if (isNumber(num)) {
                    this.uid = Long.parseLong(num);
                }
                if (out2 != null) {
                    try {
                        out2.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (in2 != null) {
                    try {
                        in2.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
                if (con2 != null) {
                    con2.disconnect();
                }
                return true;
            }
            if (out2 != null) {
                try {
                    out2.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            if (con2 != null) {
                con2.disconnect();
            }
            return false;
        } catch (Exception e5) {
            e5.printStackTrace();
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e7) {
                    e7.printStackTrace();
                }
            }
            if (con != null) {
                con.disconnect();
            }
        } catch (Throwable th) {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e8) {
                    e8.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e9) {
                    e9.printStackTrace();
                }
            }
            if (con != null) {
                con.disconnect();
            }
            throw th;
        }
    }

    @Deprecated
    public void submit(String action, String extra) {
        this.actionPool.add(new Action(action, extra, System.currentTimeMillis()));
    }

    /* access modifiers changed from: private */
    public boolean isOnline() {
        NetworkInfo info = ((ConnectivityManager) this.activity.getSystemService("connectivity")).getActiveNetworkInfo();
        return info != null && info.isAvailable();
    }

    private static boolean isNumber(String str) {
        if (str == null || str.equals("")) {
            return false;
        }
        int len = str.length();
        for (int k = 0; k < len; k++) {
            if (!Character.isDigit(str.charAt(k))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static String encodeURL(String str) {
        if (str == null) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private final class Action {
        public final String action;
        public final String extra;
        public final long millis;

        public Action(String action2, String extra2, long millis2) {
            this.action = action2;
            this.extra = extra2;
            this.millis = millis2;
        }

        public boolean submit() {
            if (!DataSubmitter.this.isOnline()) {
                return false;
            }
            HttpURLConnection con = null;
            InputStream in = null;
            OutputStream out = null;
            try {
                HttpURLConnection con2 = (HttpURLConnection) new URL(DataSubmitter.URL).openConnection();
                con2.setRequestMethod(Net.HttpMethods.POST);
                con2.setConnectTimeout(HttpConnectionManager.GPRS_WAIT_TIMEOUT);
                con2.setReadTimeout(HttpConnectionManager.GPRS_WAIT_TIMEOUT);
                con2.setDoInput(true);
                con2.setDoOutput(true);
                byte[] data = ("uid=" + DataSubmitter.this.uid + "&content=" + DataSubmitter.encodeURL(Base64.encodeToString(("action=" + this.action + "&extra=" + this.extra + "&millis=" + this.millis).getBytes("UTF-8"), 2))).getBytes("UTF-8");
                con2.setRequestProperty(HttpRequestHeader.UserAgent, "Data Acquisition SDK");
                con2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                con2.setRequestProperty(HttpRequestHeader.Accept, "*/*");
                con2.setRequestProperty(HttpRequestHeader.AcceptLanguage, "zh-cn");
                con2.setRequestProperty(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                con2.setRequestProperty("Content-Length", String.valueOf(data.length));
                con2.setRequestProperty("Connection", "Close");
                OutputStream out2 = con2.getOutputStream();
                out2.write(data);
                out2.flush();
                if (con2.getResponseCode() == 200) {
                    System.out.println("submit ok");
                    if (out2 != null) {
                        try {
                            out2.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (con2 != null) {
                        con2.disconnect();
                    }
                    return true;
                }
                if (out2 != null) {
                    try {
                        out2.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                if (con2 != null) {
                    con2.disconnect();
                }
                return false;
            } catch (Exception e5) {
                e5.printStackTrace();
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e7) {
                        e7.printStackTrace();
                    }
                }
                if (con != null) {
                    con.disconnect();
                }
            } catch (Throwable th) {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e8) {
                        e8.printStackTrace();
                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e9) {
                        e9.printStackTrace();
                    }
                }
                if (con != null) {
                    con.disconnect();
                }
                throw th;
            }
        }
    }

    private class ActionPool implements Runnable {
        private static final long MAX_INTERVAL = 10000;
        private static final long MIN_INTERVAL = 300;
        private static final int SIZE = 500;
        private final Action[] list = new Action[500];
        private boolean modified = false;

        public ActionPool() {
            load();
        }

        /* JADX WARNING: Removed duplicated region for block: B:44:0x0092 A[SYNTHETIC, Splitter:B:44:0x0092] */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0097 A[SYNTHETIC, Splitter:B:47:0x0097] */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x00a8 A[SYNTHETIC, Splitter:B:55:0x00a8] */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x00ad A[SYNTHETIC, Splitter:B:58:0x00ad] */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x00be A[SYNTHETIC, Splitter:B:66:0x00be] */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x00c3 A[SYNTHETIC, Splitter:B:69:0x00c3] */
        /* JADX WARNING: Removed duplicated region for block: B:89:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void load() {
            /*
                r13 = this;
                r9 = 0
                r6 = 0
                com.datalab.tools.DataSubmitter r0 = com.datalab.tools.DataSubmitter.this     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x00a5, all -> 0x00bb }
                android.content.Context r0 = r0.activity     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x00a5, all -> 0x00bb }
                java.lang.String r1 = "action.lst"
                java.io.FileInputStream r9 = r0.openFileInput(r1)     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x00a5, all -> 0x00bb }
                java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x00a5, all -> 0x00bb }
                r7.<init>(r9)     // Catch:{ FileNotFoundException -> 0x008f, IOException -> 0x00a5, all -> 0x00bb }
                byte r0 = r7.readByte()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r1 = 97
                if (r0 != r1) goto L_0x0032
                byte r0 = r7.readByte()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r1 = 99
                if (r0 != r1) goto L_0x0032
                byte r0 = r7.readByte()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r1 = 116(0x74, float:1.63E-43)
                if (r0 != r1) goto L_0x0032
                int r0 = r7.readInt()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r1 = 1
                if (r0 == r1) goto L_0x0048
            L_0x0032:
                if (r7 == 0) goto L_0x0037
                r7.close()     // Catch:{ IOException -> 0x003e }
            L_0x0037:
                if (r9 == 0) goto L_0x003c
                r9.close()     // Catch:{ IOException -> 0x0043 }
            L_0x003c:
                r6 = r7
            L_0x003d:
                return
            L_0x003e:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x0037
            L_0x0043:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x003c
            L_0x0048:
                com.datalab.tools.DataSubmitter r0 = com.datalab.tools.DataSubmitter.this     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                long r2 = r7.readLong()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                long unused = r0.uid = r2     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                int r11 = r7.readInt()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r10 = 0
            L_0x0056:
                if (r10 >= r11) goto L_0x0078
                boolean r0 = r7.readBoolean()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                if (r0 == 0) goto L_0x0075
                com.datalab.tools.DataSubmitter$Action[] r12 = r13.list     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                com.datalab.tools.DataSubmitter$Action r0 = new com.datalab.tools.DataSubmitter$Action     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                com.datalab.tools.DataSubmitter r1 = com.datalab.tools.DataSubmitter.this     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                java.lang.String r2 = r7.readUTF()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                java.lang.String r3 = r7.readUTF()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                long r4 = r7.readLong()     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r0.<init>(r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
                r12[r10] = r0     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00d4, all -> 0x00d1 }
            L_0x0075:
                int r10 = r10 + 1
                goto L_0x0056
            L_0x0078:
                if (r7 == 0) goto L_0x007d
                r7.close()     // Catch:{ IOException -> 0x0084 }
            L_0x007d:
                if (r9 == 0) goto L_0x00da
                r9.close()     // Catch:{ IOException -> 0x0089 }
                r6 = r7
                goto L_0x003d
            L_0x0084:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x007d
            L_0x0089:
                r8 = move-exception
                r8.printStackTrace()
                r6 = r7
                goto L_0x003d
            L_0x008f:
                r0 = move-exception
            L_0x0090:
                if (r6 == 0) goto L_0x0095
                r6.close()     // Catch:{ IOException -> 0x00a0 }
            L_0x0095:
                if (r9 == 0) goto L_0x003d
                r9.close()     // Catch:{ IOException -> 0x009b }
                goto L_0x003d
            L_0x009b:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x003d
            L_0x00a0:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x0095
            L_0x00a5:
                r0 = move-exception
            L_0x00a6:
                if (r6 == 0) goto L_0x00ab
                r6.close()     // Catch:{ IOException -> 0x00b6 }
            L_0x00ab:
                if (r9 == 0) goto L_0x003d
                r9.close()     // Catch:{ IOException -> 0x00b1 }
                goto L_0x003d
            L_0x00b1:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x003d
            L_0x00b6:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x00ab
            L_0x00bb:
                r0 = move-exception
            L_0x00bc:
                if (r6 == 0) goto L_0x00c1
                r6.close()     // Catch:{ IOException -> 0x00c7 }
            L_0x00c1:
                if (r9 == 0) goto L_0x00c6
                r9.close()     // Catch:{ IOException -> 0x00cc }
            L_0x00c6:
                throw r0
            L_0x00c7:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x00c1
            L_0x00cc:
                r8 = move-exception
                r8.printStackTrace()
                goto L_0x00c6
            L_0x00d1:
                r0 = move-exception
                r6 = r7
                goto L_0x00bc
            L_0x00d4:
                r0 = move-exception
                r6 = r7
                goto L_0x00a6
            L_0x00d7:
                r0 = move-exception
                r6 = r7
                goto L_0x0090
            L_0x00da:
                r6 = r7
                goto L_0x003d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.datalab.tools.DataSubmitter.ActionPool.load():void");
        }

        /* JADX WARNING: Removed duplicated region for block: B:18:0x0070 A[SYNTHETIC, Splitter:B:18:0x0070] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0075 A[SYNTHETIC, Splitter:B:21:0x0075] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x00a3 A[SYNTHETIC, Splitter:B:45:0x00a3] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x00a8 A[SYNTHETIC, Splitter:B:48:0x00a8] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x00b9 A[SYNTHETIC, Splitter:B:56:0x00b9] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x00be A[SYNTHETIC, Splitter:B:59:0x00be] */
        /* JADX WARNING: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x006b=Splitter:B:15:0x006b, B:42:0x009e=Splitter:B:42:0x009e} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void save() {
            /*
                r8 = this;
                r3 = 0
                r0 = 0
                com.datalab.tools.DataSubmitter r5 = com.datalab.tools.DataSubmitter.this     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x009d }
                android.content.Context r5 = r5.activity     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x009d }
                java.lang.String r6 = "action.lst"
                r7 = 0
                java.io.FileOutputStream r3 = r5.openFileOutput(r6, r7)     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x009d }
                java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x009d }
                r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00d2, IOException -> 0x009d }
                r5 = 97
                r1.writeByte(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = 99
                r1.writeByte(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = 116(0x74, float:1.63E-43)
                r1.writeByte(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = 1
                r1.writeInt(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                com.datalab.tools.DataSubmitter r5 = com.datalab.tools.DataSubmitter.this     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                long r6 = r5.uid     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r1.writeLong(r6)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                com.datalab.tools.DataSubmitter$Action[] r5 = r8.list     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                int r5 = r5.length     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r1.writeInt(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r4 = 0
            L_0x0037:
                com.datalab.tools.DataSubmitter$Action[] r5 = r8.list     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                int r5 = r5.length     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                if (r4 >= r5) goto L_0x0079
                com.datalab.tools.DataSubmitter$Action[] r5 = r8.list     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = r5[r4]     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                if (r5 != 0) goto L_0x0049
                r5 = 0
                r1.writeBoolean(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
            L_0x0046:
                int r4 = r4 + 1
                goto L_0x0037
            L_0x0049:
                r5 = 1
                r1.writeBoolean(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                com.datalab.tools.DataSubmitter$Action[] r5 = r8.list     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = r5[r4]     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                java.lang.String r5 = r5.action     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r1.writeUTF(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                com.datalab.tools.DataSubmitter$Action[] r5 = r8.list     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = r5[r4]     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                java.lang.String r5 = r5.extra     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r1.writeUTF(r5)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                com.datalab.tools.DataSubmitter$Action[] r5 = r8.list     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r5 = r5[r4]     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                long r6 = r5.millis     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                r1.writeLong(r6)     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                goto L_0x0046
            L_0x0069:
                r2 = move-exception
                r0 = r1
            L_0x006b:
                r2.printStackTrace()     // Catch:{ all -> 0x00b6 }
                if (r0 == 0) goto L_0x0073
                r0.close()     // Catch:{ IOException -> 0x0093 }
            L_0x0073:
                if (r3 == 0) goto L_0x0078
                r3.close()     // Catch:{ IOException -> 0x0098 }
            L_0x0078:
                return
            L_0x0079:
                r5 = 0
                r8.modified = r5     // Catch:{ FileNotFoundException -> 0x0069, IOException -> 0x00cf, all -> 0x00cc }
                if (r1 == 0) goto L_0x0081
                r1.close()     // Catch:{ IOException -> 0x0088 }
            L_0x0081:
                if (r3 == 0) goto L_0x00d4
                r3.close()     // Catch:{ IOException -> 0x008d }
                r0 = r1
                goto L_0x0078
            L_0x0088:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x0081
            L_0x008d:
                r2 = move-exception
                r2.printStackTrace()
                r0 = r1
                goto L_0x0078
            L_0x0093:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x0073
            L_0x0098:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x0078
            L_0x009d:
                r2 = move-exception
            L_0x009e:
                r2.printStackTrace()     // Catch:{ all -> 0x00b6 }
                if (r0 == 0) goto L_0x00a6
                r0.close()     // Catch:{ IOException -> 0x00b1 }
            L_0x00a6:
                if (r3 == 0) goto L_0x0078
                r3.close()     // Catch:{ IOException -> 0x00ac }
                goto L_0x0078
            L_0x00ac:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x0078
            L_0x00b1:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x00a6
            L_0x00b6:
                r5 = move-exception
            L_0x00b7:
                if (r0 == 0) goto L_0x00bc
                r0.close()     // Catch:{ IOException -> 0x00c2 }
            L_0x00bc:
                if (r3 == 0) goto L_0x00c1
                r3.close()     // Catch:{ IOException -> 0x00c7 }
            L_0x00c1:
                throw r5
            L_0x00c2:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x00bc
            L_0x00c7:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x00c1
            L_0x00cc:
                r5 = move-exception
                r0 = r1
                goto L_0x00b7
            L_0x00cf:
                r2 = move-exception
                r0 = r1
                goto L_0x009e
            L_0x00d2:
                r2 = move-exception
                goto L_0x006b
            L_0x00d4:
                r0 = r1
                goto L_0x0078
            */
            throw new UnsupportedOperationException("Method not decompiled: com.datalab.tools.DataSubmitter.ActionPool.save():void");
        }

        public boolean add(Action action) {
            if (action == null) {
                return false;
            }
            for (int k = 0; k < this.list.length; k++) {
                if (this.list[k] == null) {
                    this.list[k] = action;
                    this.modified = true;
                    return true;
                }
            }
            synchronized (DataSubmitter.this) {
                action.submit();
            }
            return true;
        }

        public void update() {
            long millisStart;
            long millisStart2 = System.currentTimeMillis();
            synchronized (DataSubmitter.this) {
                if (this.list.length > 0 && DataSubmitter.this.isOnline()) {
                    if (DataSubmitter.this.uid == -1) {
                        boolean unused = DataSubmitter.this.regist();
                        this.modified = true;
                    } else {
                        for (int k = 0; k < this.list.length; k++) {
                            if (this.list[k] != null) {
                                System.out.println("submit");
                                if (this.list[k].submit()) {
                                    this.list[k] = null;
                                    save();
                                }
                                try {
                                    Thread.sleep(MIN_INTERVAL);
                                } catch (InterruptedException e) {
                                }
                            }
                        }
                    }
                    if (this.modified) {
                        save();
                    }
                }
                millisStart = MAX_INTERVAL - (System.currentTimeMillis() - millisStart2);
            }
            try {
                Thread.sleep(Math.max((long) MIN_INTERVAL, millisStart));
            } catch (InterruptedException e2) {
            }
        }

        public void run() {
            update();
        }
    }
}
