package com.datalab.tools;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.datalab.sms.NotifierService;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Enumeration;
import org.apache.http.conn.util.InetAddressUtils;

public class Utils {
    public static final String EXT_CONTENT = "content";
    private static final int HOUR = 12;
    private static final int MINUTE = 0;
    public static final String TAG = ("FictionFactory:" + Utils.class.getSimpleName());

    public static void showNotification(Context context, String string) {
        ManifestInfo info = getInfo(context);
        Notification n = new Notification(info.icon, info.appName, System.currentTimeMillis());
        n.flags = 16;
        n.setLatestEventInfo(context, info.appName, string, PendingIntent.getActivity(context, 0, info.startIntent, 0));
        n.defaults = 5;
        ((NotificationManager) context.getSystemService("notification")).notify(100, n);
    }

    public static void sendNotification(Context context) {
        Intent intent = new Intent(context, NotifierService.class);
        intent.putExtra("content", "新的福利礼包已经送到,快来领取哦!");
        ((AlarmManager) context.getSystemService("alarm")).set(0, getTriggerAtTime(), PendingIntent.getService(context, 0, intent, 268435456));
    }

    public static void cancelNotification(Context context) {
        Intent intent = new Intent(context, NotifierService.class);
        intent.putExtra("content", "新的福利礼包已经送到,快来领取哦!");
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getService(context, 0, intent, 268435456));
    }

    private static long getTriggerAtTime() {
        Calendar goal = Calendar.getInstance();
        goal.set(11, 12);
        goal.set(12, 0);
        goal.set(13, 0);
        Calendar current = Calendar.getInstance();
        long dif = current.getTimeInMillis() - goal.getTimeInMillis();
        Log.d("SGManager", "goal" + goal.getTimeInMillis() + " current" + current.getTimeInMillis() + " system" + System.currentTimeMillis());
        if (dif >= 0) {
            goal.add(5, 1);
        }
        return goal.getTimeInMillis();
    }

    public static boolean isConnectingToInternet(Context paramContext) {
        NetworkInfo[] arrayOfNetworkInfo;
        ConnectivityManager localConnectivityManager = (ConnectivityManager) paramContext.getSystemService("connectivity");
        if (!(localConnectivityManager == null || (arrayOfNetworkInfo = localConnectivityManager.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : arrayOfNetworkInfo) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Intent getStarIntent(Context context) {
        return context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
    }

    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            Log.e("SGManage", "Exception", e);
        }
        return versionName;
    }

    public static String getAppName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return packageManager.getApplicationLabel(packageManager.getApplicationInfo(context.getPackageName(), 0)).toString();
        } catch (Exception e) {
            Log.e("SGManage", "Exception", e);
            return "";
        }
    }

    public static int getApkIconInt(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).icon;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 17301543;
        }
    }

    private static ManifestInfo getInfo(Context context) {
        String pagName = context.getPackageName();
        PackageManager pm = context.getPackageManager();
        ManifestInfo info = new ManifestInfo();
        info.pagName = pagName;
        info.startIntent = pm.getLaunchIntentForPackage(pagName);
        info.startIntent.setFlags(536870912);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo(pagName, 0);
            info.appName = pm.getApplicationLabel(applicationInfo).toString();
            info.icon = applicationInfo.icon;
            info.largeIcon = applicationInfo.loadIcon(pm);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info;
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(inetAddress.getHostAddress())) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("SGManager", ex.toString());
        }
        return null;
    }

    private static class ManifestInfo {
        public String appName;
        public int icon;
        public Drawable largeIcon;
        public String pagName;
        public Intent startIntent;

        private ManifestInfo() {
        }
    }
}
