package com.datalab.tools;

import android.content.Context;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public class Dynamicload {
    private static DexClassLoader dl;

    public static void init(Context context) {
        try {
            InputStream is = context.getResources().getAssets().open("DataAcquisition.png");
            OutputStream os = context.openFileOutput("dexout.jar", 0);
            decrypt(is, os);
            os.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            File file = context.getFileStreamPath("dexout.jar");
            File optimizedDirectoryDir = context.getDir("temp", 0);
            dl = new DexClassLoader(file.getAbsolutePath(), optimizedDirectoryDir.getAbsolutePath(), null, context.getClassLoader());
            System.out.println("Dynamicload init success");
        } catch (Exception e2) {
            System.out.println("Dynamicload init failed");
            e2.printStackTrace();
        }
    }

    public static Class loadClass(String className) throws Exception {
        if (dl != null) {
            return dl.loadClass(className);
        }
        throw new RuntimeException("Dynamicload is not init!");
    }

    private static void decrypt(InputStream is, OutputStream out) throws Exception {
        byte[] buffer = new byte[1024];
        byte[] buffer2 = new byte[1024];
        while (true) {
            int r = is.read(buffer);
            if (r != -1) {
                for (int i = 0; i < r; i++) {
                    byte b = buffer[i];
                    buffer2[i] = b == 0 ? -1 : (byte) (b - 1);
                }
                out.write(buffer2, 0, r);
            } else {
                return;
            }
        }
    }
}
