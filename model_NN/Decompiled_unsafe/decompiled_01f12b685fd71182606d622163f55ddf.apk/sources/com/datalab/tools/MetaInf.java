package com.datalab.tools;

public class MetaInf {
    static boolean isShow = false;
    String CHANNEL = "aaa";
    private int caseId = -1;
    private int channelId = -1;
    String channelPrefix = "META-INF/sg_";
    boolean isNull = true;
    String[] kbz = {"kkk", "bbb", "zzz"};

    private static void showToastMessage(String aaa) {
        System.out.println(aaa);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadMetaInf(java.lang.String r12) {
        /*
            r11 = this;
            r10 = -1
            r5 = 0
            r0 = 0
            java.util.zip.ZipInputStream r6 = new java.util.zip.ZipInputStream     // Catch:{ IOException -> 0x00e3 }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00e3 }
            r7.<init>(r12)     // Catch:{ IOException -> 0x00e3 }
            r6.<init>(r7)     // Catch:{ IOException -> 0x00e3 }
        L_0x000d:
            java.util.zip.ZipEntry r0 = r6.getNextEntry()     // Catch:{ IOException -> 0x0044 }
            if (r0 == 0) goto L_0x0092
            java.lang.String r4 = r0.getName()     // Catch:{ IOException -> 0x0044 }
            boolean r7 = r0.isDirectory()     // Catch:{ IOException -> 0x0044 }
            if (r7 != 0) goto L_0x000d
            java.lang.String r7 = r11.channelPrefix     // Catch:{ IOException -> 0x0044 }
            int r7 = r4.indexOf(r7)     // Catch:{ IOException -> 0x0044 }
            if (r7 == r10) goto L_0x000d
            java.lang.String r7 = r11.CHANNEL     // Catch:{ IOException -> 0x0044 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0044 }
            r8.<init>()     // Catch:{ IOException -> 0x0044 }
            java.lang.String r9 = ","
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0044 }
            java.lang.StringBuilder r8 = r8.append(r4)     // Catch:{ IOException -> 0x0044 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0044 }
            java.lang.String r7 = r7.concat(r8)     // Catch:{ IOException -> 0x0044 }
            r11.CHANNEL = r7     // Catch:{ IOException -> 0x0044 }
            r7 = 0
            r11.isNull = r7     // Catch:{ IOException -> 0x0044 }
            goto L_0x000d
        L_0x0044:
            r1 = move-exception
            r5 = r6
        L_0x0046:
            java.lang.String r7 = "bbb"
            r11.CHANNEL = r7
            r1.printStackTrace()
        L_0x004d:
            boolean r7 = r11.isNull
            if (r7 != 0) goto L_0x00bc
            java.lang.String r7 = r11.CHANNEL
            java.lang.String r8 = ","
            java.lang.String[] r7 = r7.split(r8)
            r11.kbz = r7
            r2 = 1
        L_0x005c:
            java.lang.String[] r7 = r11.kbz
            int r7 = r7.length
            if (r2 >= r7) goto L_0x00bc
            java.lang.String[] r7 = r11.kbz
            r7 = r7[r2]
            java.lang.String r8 = "_"
            int r3 = r7.indexOf(r8)
            java.lang.String[] r7 = r11.kbz
            r7 = r7[r2]
            java.lang.String r8 = "_A"
            int r7 = r7.indexOf(r8)
            if (r7 == r10) goto L_0x0097
            java.lang.String[] r7 = r11.kbz
            java.lang.String[] r8 = r11.kbz
            r8 = r8[r2]
            int r9 = r3 + 2
            java.lang.String r8 = r8.substring(r9)
            r7[r2] = r8
            java.lang.String[] r7 = r11.kbz
            r7 = r7[r2]
            int r7 = java.lang.Integer.parseInt(r7)
            r11.caseId = r7
        L_0x008f:
            int r2 = r2 + 1
            goto L_0x005c
        L_0x0092:
            r6.close()     // Catch:{ IOException -> 0x0044 }
            r5 = r6
            goto L_0x004d
        L_0x0097:
            java.lang.String[] r7 = r11.kbz
            r7 = r7[r2]
            java.lang.String r8 = "_C"
            int r7 = r7.indexOf(r8)
            if (r7 == r10) goto L_0x008f
            java.lang.String[] r7 = r11.kbz
            java.lang.String[] r8 = r11.kbz
            r8 = r8[r2]
            int r9 = r3 + 2
            java.lang.String r8 = r8.substring(r9)
            r7[r2] = r8
            java.lang.String[] r7 = r11.kbz
            r7 = r7[r2]
            int r7 = java.lang.Integer.parseInt(r7)
            r11.channelId = r7
            goto L_0x008f
        L_0x00bc:
            boolean r7 = com.datalab.tools.MetaInf.isShow
            if (r7 == 0) goto L_0x00e2
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            int r8 = r11.getMetaInfCase()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = " channelId "
            java.lang.StringBuilder r7 = r7.append(r8)
            int r8 = r11.getMetaInfChannelId()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            showToastMessage(r7)
        L_0x00e2:
            return
        L_0x00e3:
            r1 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.datalab.tools.MetaInf.loadMetaInf(java.lang.String):void");
    }

    public static void setShow(boolean isShow2) {
        isShow = isShow2;
    }

    public void setChannelId(int channelId2) {
        this.channelId = channelId2;
    }

    public void setCaseId(int caseId2) {
        this.caseId = caseId2;
    }

    public int getMetaInfCase() {
        return this.caseId;
    }

    public int getMetaInfChannelId() {
        return this.channelId;
    }
}
