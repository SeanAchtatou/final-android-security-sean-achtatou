package com.datalab.tools;

import java.util.HashMap;
import java.util.Map;

public final class Constant {
    public static final int A = 0;
    public static final String AB = "ab";
    public static final int B = 1;
    public static final int C = 2;
    public static final String CASE_0 = "0";
    public static final String CASE_1 = "1";
    public static final String CASE_NOTHING = "-1";
    public static final int D = 3;
    public static final int DEFAULT_LIMIT = 10000;
    public static final int E = 4;
    public static final int F = 5;
    public static final String IP_KEY = "cloud";
    public static final String IP_VALUE = "1";
    public static final String LIMIT = "limit";
    public static final String MF = "mf";
    public static final String MF_1 = "1";
    public static final String MF_CASE = "mf_case";
    public static final String MF_DEFAULT = "0";
    public static final String SIGN = "sign";
    public static final String SIGN_NOTHING = "-1";
    public static final String S_A = "0";
    public static final String S_B = "1";
    public static final String S_C = "2";
    public static final String S_D = "3";
    public static final String S_E = "4";
    public static final String S_F = "5";
    private static final Map<String, String> signMap = new HashMap(4);

    static {
        signMap.put("23a4c6cb9fa74d8372926003f6f9d2a", "1");
        signMap.put("386e84e93658768461d78b25c2760bdd", "1");
        signMap.put("3fc79d037da635a6fdb87c72db6f5d84", "1");
        signMap.put("ec6d50d4d4b4da582b6b6bfc3cbc3756", "0");
        signMap.put("19979dcda885b246a43b4d16af48e790", "1");
        signMap.put("2ac278ba2f4a2e0db499ec9dfa650f9", "1");
        signMap.put("3dd0653dc9e04e71fc8481da03ae5f1a", "1");
        signMap.put("f07c579e1b133c4f72aa4edea7ea6631", "1");
    }

    public static String getSign(String sign) {
        String result = signMap.get(sign);
        return result == null ? "-1" : result;
    }
}
