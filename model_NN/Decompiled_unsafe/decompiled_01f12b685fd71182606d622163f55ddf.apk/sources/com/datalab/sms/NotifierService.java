package com.datalab.sms;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.datalab.tools.Utils;

public class NotifierService extends Service {
    private static final String TAG = NotifierService.class.getName();

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return 2;
        }
        String content = intent.getExtras().getString("content");
        if (content == null) {
            content = "点我继续游戏";
        }
        Utils.showNotification(this, content);
        Utils.sendNotification(this);
        Log.d(TAG, "开始发送定时通知" + content);
        return super.onStartCommand(intent, flags, startId);
    }
}
