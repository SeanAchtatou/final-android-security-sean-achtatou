package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.Serializable;

public class Quaternion implements Serializable {
    private static final long serialVersionUID = -7661875440774897168L;
    private static Quaternion tmp1 = new Quaternion(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    private static Quaternion tmp2 = new Quaternion(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    public float w;
    public float x;
    public float y;
    public float z;

    public Quaternion(float x2, float y2, float z2, float w2) {
        set(x2, y2, z2, w2);
    }

    public Quaternion() {
        idt();
    }

    public Quaternion(Quaternion quaternion) {
        set(quaternion);
    }

    public Quaternion(Vector3 axis, float angle) {
        set(axis, angle);
    }

    public Quaternion set(float x2, float y2, float z2, float w2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
        this.w = w2;
        return this;
    }

    public Quaternion set(Quaternion quaternion) {
        return set(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
    }

    public Quaternion set(Vector3 axis, float angle) {
        return setFromAxis(axis.x, axis.y, axis.z, angle);
    }

    public Quaternion cpy() {
        return new Quaternion(this);
    }

    public static final float len(float x2, float y2, float z2, float w2) {
        return (float) Math.sqrt((double) ((x2 * x2) + (y2 * y2) + (z2 * z2) + (w2 * w2)));
    }

    public float len() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w)));
    }

    public String toString() {
        return "[" + this.x + "|" + this.y + "|" + this.z + "|" + this.w + "]";
    }

    public Quaternion setEulerAngles(float yaw, float pitch, float roll) {
        return setEulerAnglesRad(yaw * 0.017453292f, pitch * 0.017453292f, 0.017453292f * roll);
    }

    public Quaternion setEulerAnglesRad(float yaw, float pitch, float roll) {
        float hr = roll * 0.5f;
        float shr = (float) Math.sin((double) hr);
        float chr = (float) Math.cos((double) hr);
        float hp = pitch * 0.5f;
        float shp = (float) Math.sin((double) hp);
        float chp = (float) Math.cos((double) hp);
        float hy = yaw * 0.5f;
        float shy = (float) Math.sin((double) hy);
        float chy = (float) Math.cos((double) hy);
        float chy_shp = chy * shp;
        float shy_chp = shy * chp;
        float chy_chp = chy * chp;
        float shy_shp = shy * shp;
        this.x = (chy_shp * chr) + (shy_chp * shr);
        this.y = (shy_chp * chr) - (chy_shp * shr);
        this.z = (chy_chp * shr) - (shy_shp * chr);
        this.w = (chy_chp * chr) + (shy_shp * shr);
        return this;
    }

    public int getGimbalPole() {
        float t = (this.y * this.x) + (this.z * this.w);
        if (t > 0.499f) {
            return 1;
        }
        return t < -0.499f ? -1 : 0;
    }

    public float getRollRad() {
        int pole = getGimbalPole();
        return pole == 0 ? MathUtils.atan2(((this.w * this.z) + (this.y * this.x)) * 2.0f, 1.0f - (((this.x * this.x) + (this.z * this.z)) * 2.0f)) : ((float) pole) * 2.0f * MathUtils.atan2(this.y, this.w);
    }

    public float getRoll() {
        return getRollRad() * 57.295776f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, int, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public float getPitchRad() {
        int pole = getGimbalPole();
        return pole == 0 ? (float) Math.asin((double) MathUtils.clamp(2.0f * ((this.w * this.x) - (this.z * this.y)), -1.0f, 1.0f)) : ((float) pole) * 3.1415927f * 0.5f;
    }

    public float getPitch() {
        return getPitchRad() * 57.295776f;
    }

    public float getYawRad() {
        return getGimbalPole() == 0 ? MathUtils.atan2(((this.y * this.w) + (this.x * this.z)) * 2.0f, 1.0f - (((this.y * this.y) + (this.x * this.x)) * 2.0f)) : Animation.CurveTimeline.LINEAR;
    }

    public float getYaw() {
        return getYawRad() * 57.295776f;
    }

    public static final float len2(float x2, float y2, float z2, float w2) {
        return (x2 * x2) + (y2 * y2) + (z2 * z2) + (w2 * w2);
    }

    public float len2() {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z) + (this.w * this.w);
    }

    public Quaternion nor() {
        float len = len2();
        if (len != Animation.CurveTimeline.LINEAR && !MathUtils.isEqual(len, 1.0f)) {
            float len2 = (float) Math.sqrt((double) len);
            this.w /= len2;
            this.x /= len2;
            this.y /= len2;
            this.z /= len2;
        }
        return this;
    }

    public Quaternion conjugate() {
        this.x = -this.x;
        this.y = -this.y;
        this.z = -this.z;
        return this;
    }

    public Vector3 transform(Vector3 v) {
        tmp2.set(this);
        tmp2.conjugate();
        tmp2.mulLeft(tmp1.set(v.x, v.y, v.z, Animation.CurveTimeline.LINEAR)).mulLeft(this);
        v.x = tmp2.x;
        v.y = tmp2.y;
        v.z = tmp2.z;
        return v;
    }

    public Quaternion mul(Quaternion other) {
        float newX = (((this.w * other.x) + (this.x * other.w)) + (this.y * other.z)) - (this.z * other.y);
        float newY = (((this.w * other.y) + (this.y * other.w)) + (this.z * other.x)) - (this.x * other.z);
        float newZ = (((this.w * other.z) + (this.z * other.w)) + (this.x * other.y)) - (this.y * other.x);
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = (((this.w * other.w) - (this.x * other.x)) - (this.y * other.y)) - (this.z * other.z);
        return this;
    }

    public Quaternion mul(float x2, float y2, float z2, float w2) {
        float newX = (((this.w * x2) + (this.x * w2)) + (this.y * z2)) - (this.z * y2);
        float newY = (((this.w * y2) + (this.y * w2)) + (this.z * x2)) - (this.x * z2);
        float newZ = (((this.w * z2) + (this.z * w2)) + (this.x * y2)) - (this.y * x2);
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = (((this.w * w2) - (this.x * x2)) - (this.y * y2)) - (this.z * z2);
        return this;
    }

    public Quaternion mulLeft(Quaternion other) {
        float newX = (((other.w * this.x) + (other.x * this.w)) + (other.y * this.z)) - (other.z * this.y);
        float newY = (((other.w * this.y) + (other.y * this.w)) + (other.z * this.x)) - (other.x * this.z);
        float newZ = (((other.w * this.z) + (other.z * this.w)) + (other.x * this.y)) - (other.y * this.x);
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = (((other.w * this.w) - (other.x * this.x)) - (other.y * this.y)) - (other.z * this.z);
        return this;
    }

    public Quaternion mulLeft(float x2, float y2, float z2, float w2) {
        float newX = (((this.x * w2) + (this.w * x2)) + (this.z * y2)) - (z2 * y2);
        float newY = (((this.y * w2) + (this.w * y2)) + (this.x * z2)) - (x2 * z2);
        float newZ = (((this.z * w2) + (this.w * z2)) + (this.y * x2)) - (y2 * x2);
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = (((this.w * w2) - (this.x * x2)) - (this.y * y2)) - (z2 * z2);
        return this;
    }

    public Quaternion add(Quaternion quaternion) {
        this.x += quaternion.x;
        this.y += quaternion.y;
        this.z += quaternion.z;
        this.w += quaternion.w;
        return this;
    }

    public Quaternion add(float qx, float qy, float qz, float qw) {
        this.x += qx;
        this.y += qy;
        this.z += qz;
        this.w += qw;
        return this;
    }

    public void toMatrix(float[] matrix) {
        float xx = this.x * this.x;
        float xy = this.x * this.y;
        float xz = this.x * this.z;
        float xw = this.x * this.w;
        float yy = this.y * this.y;
        float yz = this.y * this.z;
        float yw = this.y * this.w;
        float zz = this.z * this.z;
        float zw = this.z * this.w;
        matrix[0] = 1.0f - ((yy + zz) * 2.0f);
        matrix[4] = (xy - zw) * 2.0f;
        matrix[8] = (xz + yw) * 2.0f;
        matrix[12] = 0.0f;
        matrix[1] = (xy + zw) * 2.0f;
        matrix[5] = 1.0f - ((xx + zz) * 2.0f);
        matrix[9] = (yz - xw) * 2.0f;
        matrix[13] = 0.0f;
        matrix[2] = (xz - yw) * 2.0f;
        matrix[6] = (yz + xw) * 2.0f;
        matrix[10] = 1.0f - ((xx + yy) * 2.0f);
        matrix[14] = 0.0f;
        matrix[3] = 0.0f;
        matrix[7] = 0.0f;
        matrix[11] = 0.0f;
        matrix[15] = 1.0f;
    }

    public Quaternion idt() {
        return set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
    }

    public boolean isIdentity() {
        return MathUtils.isZero(this.x) && MathUtils.isZero(this.y) && MathUtils.isZero(this.z) && MathUtils.isEqual(this.w, 1.0f);
    }

    public boolean isIdentity(float tolerance) {
        return MathUtils.isZero(this.x, tolerance) && MathUtils.isZero(this.y, tolerance) && MathUtils.isZero(this.z, tolerance) && MathUtils.isEqual(this.w, 1.0f, tolerance);
    }

    public Quaternion setFromAxis(Vector3 axis, float degrees) {
        return setFromAxis(axis.x, axis.y, axis.z, degrees);
    }

    public Quaternion setFromAxisRad(Vector3 axis, float radians) {
        return setFromAxisRad(axis.x, axis.y, axis.z, radians);
    }

    public Quaternion setFromAxis(float x2, float y2, float z2, float degrees) {
        return setFromAxisRad(x2, y2, z2, 0.017453292f * degrees);
    }

    public Quaternion setFromAxisRad(float x2, float y2, float z2, float radians) {
        float d = Vector3.len(x2, y2, z2);
        if (d == Animation.CurveTimeline.LINEAR) {
            return idt();
        }
        float d2 = 1.0f / d;
        float l_ang = radians < Animation.CurveTimeline.LINEAR ? 6.2831855f - ((-radians) % 6.2831855f) : radians % 6.2831855f;
        float l_sin = (float) Math.sin((double) (l_ang / 2.0f));
        return set(d2 * x2 * l_sin, d2 * y2 * l_sin, d2 * z2 * l_sin, (float) Math.cos((double) (l_ang / 2.0f))).nor();
    }

    public Quaternion setFromMatrix(boolean normalizeAxes, Matrix4 matrix) {
        return setFromAxes(normalizeAxes, matrix.val[0], matrix.val[4], matrix.val[8], matrix.val[1], matrix.val[5], matrix.val[9], matrix.val[2], matrix.val[6], matrix.val[10]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.Quaternion.setFromMatrix(boolean, com.badlogic.gdx.math.Matrix4):com.badlogic.gdx.math.Quaternion
     arg types: [int, com.badlogic.gdx.math.Matrix4]
     candidates:
      com.badlogic.gdx.math.Quaternion.setFromMatrix(boolean, com.badlogic.gdx.math.Matrix3):com.badlogic.gdx.math.Quaternion
      com.badlogic.gdx.math.Quaternion.setFromMatrix(boolean, com.badlogic.gdx.math.Matrix4):com.badlogic.gdx.math.Quaternion */
    public Quaternion setFromMatrix(Matrix4 matrix) {
        return setFromMatrix(false, matrix);
    }

    public Quaternion setFromMatrix(boolean normalizeAxes, Matrix3 matrix) {
        return setFromAxes(normalizeAxes, matrix.val[0], matrix.val[3], matrix.val[6], matrix.val[1], matrix.val[4], matrix.val[7], matrix.val[2], matrix.val[5], matrix.val[8]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.Quaternion.setFromMatrix(boolean, com.badlogic.gdx.math.Matrix3):com.badlogic.gdx.math.Quaternion
     arg types: [int, com.badlogic.gdx.math.Matrix3]
     candidates:
      com.badlogic.gdx.math.Quaternion.setFromMatrix(boolean, com.badlogic.gdx.math.Matrix4):com.badlogic.gdx.math.Quaternion
      com.badlogic.gdx.math.Quaternion.setFromMatrix(boolean, com.badlogic.gdx.math.Matrix3):com.badlogic.gdx.math.Quaternion */
    public Quaternion setFromMatrix(Matrix3 matrix) {
        return setFromMatrix(false, matrix);
    }

    public Quaternion setFromAxes(float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz) {
        return setFromAxes(false, xx, xy, xz, yx, yy, yz, zx, zy, zz);
    }

    public Quaternion setFromAxes(boolean normalizeAxes, float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz) {
        if (normalizeAxes) {
            float lx = 1.0f / Vector3.len(xx, xy, xz);
            float ly = 1.0f / Vector3.len(yx, yy, yz);
            float lz = 1.0f / Vector3.len(zx, zy, zz);
            xx *= lx;
            xy *= lx;
            xz *= lx;
            yy *= ly;
            yz = yz * ly * ly;
            zx *= lz;
            zy *= lz;
            zz *= lz;
        }
        float t = xx + yy + zz;
        if (t >= Animation.CurveTimeline.LINEAR) {
            float s = (float) Math.sqrt((double) (1.0f + t));
            this.w = 0.5f * s;
            float s2 = 0.5f / s;
            this.x = (zy - yz) * s2;
            this.y = (xz - zx) * s2;
            this.z = (yx - xy) * s2;
        } else if (xx > yy && xx > zz) {
            float s3 = (float) Math.sqrt(((1.0d + ((double) xx)) - ((double) yy)) - ((double) zz));
            this.x = 0.5f * s3;
            float s4 = 0.5f / s3;
            this.y = (yx + xy) * s4;
            this.z = (xz + zx) * s4;
            this.w = (zy - yz) * s4;
        } else if (yy > zz) {
            float s5 = (float) Math.sqrt(((1.0d + ((double) yy)) - ((double) xx)) - ((double) zz));
            this.y = 0.5f * s5;
            float s6 = 0.5f / s5;
            this.x = (yx + xy) * s6;
            this.z = (zy + yz) * s6;
            this.w = (xz - zx) * s6;
        } else {
            float s7 = (float) Math.sqrt(((1.0d + ((double) zz)) - ((double) xx)) - ((double) yy));
            this.z = 0.5f * s7;
            float s8 = 0.5f / s7;
            this.x = (xz + zx) * s8;
            this.y = (zy + yz) * s8;
            this.w = (yx - xy) * s8;
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, int, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public Quaternion setFromCross(Vector3 v1, Vector3 v2) {
        return setFromAxisRad((v1.y * v2.z) - (v1.z * v2.y), (v1.z * v2.x) - (v1.x * v2.z), (v1.x * v2.y) - (v1.y * v2.x), (float) Math.acos((double) MathUtils.clamp(v1.dot(v2), -1.0f, 1.0f)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, int, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public Quaternion setFromCross(float x1, float y1, float z1, float x2, float y2, float z2) {
        return setFromAxisRad((y1 * z2) - (z1 * y2), (z1 * x2) - (x1 * z2), (x1 * y2) - (y1 * x2), (float) Math.acos((double) MathUtils.clamp(Vector3.dot(x1, y1, z1, x2, y2, z2), -1.0f, 1.0f)));
    }

    public Quaternion slerp(Quaternion end, float alpha) {
        float absDot;
        float d = (this.x * end.x) + (this.y * end.y) + (this.z * end.z) + (this.w * end.w);
        if (d < Animation.CurveTimeline.LINEAR) {
            absDot = -d;
        } else {
            absDot = d;
        }
        float scale0 = 1.0f - alpha;
        float scale1 = alpha;
        if (((double) (1.0f - absDot)) > 0.1d) {
            float angle = (float) Math.acos((double) absDot);
            float invSinTheta = 1.0f / ((float) Math.sin((double) angle));
            scale0 = ((float) Math.sin((double) ((1.0f - alpha) * angle))) * invSinTheta;
            scale1 = ((float) Math.sin((double) (alpha * angle))) * invSinTheta;
        }
        if (d < Animation.CurveTimeline.LINEAR) {
            scale1 = -scale1;
        }
        this.x = (this.x * scale0) + (end.x * scale1);
        this.y = (this.y * scale0) + (end.y * scale1);
        this.z = (this.z * scale0) + (end.z * scale1);
        this.w = (this.w * scale0) + (end.w * scale1);
        return this;
    }

    public Quaternion slerp(Quaternion[] q) {
        float w2 = 1.0f / ((float) q.length);
        set(q[0]).exp(w2);
        for (int i = 1; i < q.length; i++) {
            mul(tmp1.set(q[i]).exp(w2));
        }
        nor();
        return this;
    }

    public Quaternion slerp(Quaternion[] q, float[] w2) {
        set(q[0]).exp(w2[0]);
        for (int i = 1; i < q.length; i++) {
            mul(tmp1.set(q[i]).exp(w2[i]));
        }
        nor();
        return this;
    }

    public Quaternion exp(float alpha) {
        float coeff;
        float norm = len();
        float normExp = (float) Math.pow((double) norm, (double) alpha);
        float theta = (float) Math.acos((double) (this.w / norm));
        if (((double) Math.abs(theta)) < 0.001d) {
            coeff = (normExp * alpha) / norm;
        } else {
            coeff = (float) ((((double) normExp) * Math.sin((double) (alpha * theta))) / (((double) norm) * Math.sin((double) theta)));
        }
        this.w = (float) (((double) normExp) * Math.cos((double) (alpha * theta)));
        this.x *= coeff;
        this.y *= coeff;
        this.z *= coeff;
        nor();
        return this;
    }

    public int hashCode() {
        return ((((((NumberUtils.floatToRawIntBits(this.w) + 31) * 31) + NumberUtils.floatToRawIntBits(this.x)) * 31) + NumberUtils.floatToRawIntBits(this.y)) * 31) + NumberUtils.floatToRawIntBits(this.z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Quaternion)) {
            return false;
        }
        Quaternion other = (Quaternion) obj;
        if (NumberUtils.floatToRawIntBits(this.w) == NumberUtils.floatToRawIntBits(other.w) && NumberUtils.floatToRawIntBits(this.x) == NumberUtils.floatToRawIntBits(other.x) && NumberUtils.floatToRawIntBits(this.y) == NumberUtils.floatToRawIntBits(other.y) && NumberUtils.floatToRawIntBits(this.z) == NumberUtils.floatToRawIntBits(other.z)) {
            return true;
        }
        return false;
    }

    public static final float dot(float x1, float y1, float z1, float w1, float x2, float y2, float z2, float w2) {
        return (x1 * x2) + (y1 * y2) + (z1 * z2) + (w1 * w2);
    }

    public float dot(Quaternion other) {
        return (this.x * other.x) + (this.y * other.y) + (this.z * other.z) + (this.w * other.w);
    }

    public float dot(float x2, float y2, float z2, float w2) {
        return (this.x * x2) + (this.y * y2) + (this.z * z2) + (this.w * w2);
    }

    public Quaternion mul(float scalar) {
        this.x *= scalar;
        this.y *= scalar;
        this.z *= scalar;
        this.w *= scalar;
        return this;
    }

    public float getAxisAngle(Vector3 axis) {
        return getAxisAngleRad(axis) * 57.295776f;
    }

    public float getAxisAngleRad(Vector3 axis) {
        if (this.w > 1.0f) {
            nor();
        }
        float angle = (float) (2.0d * Math.acos((double) this.w));
        double s = Math.sqrt((double) (1.0f - (this.w * this.w)));
        if (s < 9.999999974752427E-7d) {
            axis.x = this.x;
            axis.y = this.y;
            axis.z = this.z;
        } else {
            axis.x = (float) (((double) this.x) / s);
            axis.y = (float) (((double) this.y) / s);
            axis.z = (float) (((double) this.z) / s);
        }
        return angle;
    }

    public float getAngleRad() {
        return (float) (Math.acos((double) (this.w > 1.0f ? this.w / len() : this.w)) * 2.0d);
    }

    public float getAngle() {
        return getAngleRad() * 57.295776f;
    }

    public void getSwingTwist(float axisX, float axisY, float axisZ, Quaternion swing, Quaternion twist) {
        float d = Vector3.dot(this.x, this.y, this.z, axisX, axisY, axisZ);
        twist.set(axisX * d, axisY * d, axisZ * d, this.w).nor();
        swing.set(twist).conjugate().mulLeft(this);
    }

    public void getSwingTwist(Vector3 axis, Quaternion swing, Quaternion twist) {
        getSwingTwist(axis.x, axis.y, axis.z, swing, twist);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, int, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public float getAngleAroundRad(float axisX, float axisY, float axisZ) {
        float d = Vector3.dot(this.x, this.y, this.z, axisX, axisY, axisZ);
        float l2 = len2(axisX * d, axisY * d, axisZ * d, this.w);
        return MathUtils.isZero(l2) ? Animation.CurveTimeline.LINEAR : (float) (2.0d * Math.acos((double) MathUtils.clamp((float) (((double) this.w) / Math.sqrt((double) l2)), -1.0f, 1.0f)));
    }

    public float getAngleAroundRad(Vector3 axis) {
        return getAngleAroundRad(axis.x, axis.y, axis.z);
    }

    public float getAngleAround(float axisX, float axisY, float axisZ) {
        return getAngleAroundRad(axisX, axisY, axisZ) * 57.295776f;
    }

    public float getAngleAround(Vector3 axis) {
        return getAngleAround(axis.x, axis.y, axis.z);
    }
}
