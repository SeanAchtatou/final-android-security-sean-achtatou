package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.Serializable;

public class Rectangle implements Shape2D, Serializable {
    private static final long serialVersionUID = 5733252015138115702L;
    public static final Rectangle tmp = new Rectangle();
    public static final Rectangle tmp2 = new Rectangle();
    public float height;
    public float width;
    public float x;
    public float y;

    public Rectangle() {
    }

    public Rectangle(float x2, float y2, float width2, float height2) {
        this.x = x2;
        this.y = y2;
        this.width = width2;
        this.height = height2;
    }

    public Rectangle(Rectangle rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
    }

    public Rectangle set(float x2, float y2, float width2, float height2) {
        this.x = x2;
        this.y = y2;
        this.width = width2;
        this.height = height2;
        return this;
    }

    public float getX() {
        return this.x;
    }

    public Rectangle setX(float x2) {
        this.x = x2;
        return this;
    }

    public float getY() {
        return this.y;
    }

    public Rectangle setY(float y2) {
        this.y = y2;
        return this;
    }

    public float getWidth() {
        return this.width;
    }

    public Rectangle setWidth(float width2) {
        this.width = width2;
        return this;
    }

    public float getHeight() {
        return this.height;
    }

    public Rectangle setHeight(float height2) {
        this.height = height2;
        return this;
    }

    public Vector2 getPosition(Vector2 position) {
        return position.set(this.x, this.y);
    }

    public Rectangle setPosition(Vector2 position) {
        this.x = position.x;
        this.y = position.y;
        return this;
    }

    public Rectangle setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
        return this;
    }

    public Rectangle setSize(float width2, float height2) {
        this.width = width2;
        this.height = height2;
        return this;
    }

    public Rectangle setSize(float sizeXY) {
        this.width = sizeXY;
        this.height = sizeXY;
        return this;
    }

    public Vector2 getSize(Vector2 size) {
        return size.set(this.width, this.height);
    }

    public boolean contains(float x2, float y2) {
        return this.x <= x2 && this.x + this.width >= x2 && this.y <= y2 && this.y + this.height >= y2;
    }

    public boolean contains(Vector2 point) {
        return contains(point.x, point.y);
    }

    public boolean contains(Rectangle rectangle) {
        float xmin = rectangle.x;
        float xmax = xmin + rectangle.width;
        float ymin = rectangle.y;
        float ymax = ymin + rectangle.height;
        return xmin > this.x && xmin < this.x + this.width && xmax > this.x && xmax < this.x + this.width && ymin > this.y && ymin < this.y + this.height && ymax > this.y && ymax < this.y + this.height;
    }

    public boolean overlaps(Rectangle r) {
        return this.x < r.x + r.width && this.x + this.width > r.x && this.y < r.y + r.height && this.y + this.height > r.y;
    }

    public Rectangle set(Rectangle rect) {
        this.x = rect.x;
        this.y = rect.y;
        this.width = rect.width;
        this.height = rect.height;
        return this;
    }

    public Rectangle merge(Rectangle rect) {
        float minX = Math.min(this.x, rect.x);
        float maxX = Math.max(this.x + this.width, rect.x + rect.width);
        this.x = minX;
        this.width = maxX - minX;
        float minY = Math.min(this.y, rect.y);
        float maxY = Math.max(this.y + this.height, rect.y + rect.height);
        this.y = minY;
        this.height = maxY - minY;
        return this;
    }

    public Rectangle merge(float x2, float y2) {
        float minX = Math.min(this.x, x2);
        float maxX = Math.max(this.x + this.width, x2);
        this.x = minX;
        this.width = maxX - minX;
        float minY = Math.min(this.y, y2);
        float maxY = Math.max(this.y + this.height, y2);
        this.y = minY;
        this.height = maxY - minY;
        return this;
    }

    public Rectangle merge(Vector2 vec) {
        return merge(vec.x, vec.y);
    }

    public Rectangle merge(Vector2[] vecs) {
        float minX = this.x;
        float maxX = this.x + this.width;
        float minY = this.y;
        float maxY = this.y + this.height;
        for (Vector2 v : vecs) {
            minX = Math.min(minX, v.x);
            maxX = Math.max(maxX, v.x);
            minY = Math.min(minY, v.y);
            maxY = Math.max(maxY, v.y);
        }
        this.x = minX;
        this.width = maxX - minX;
        this.y = minY;
        this.height = maxY - minY;
        return this;
    }

    public float getAspectRatio() {
        if (this.height == Animation.CurveTimeline.LINEAR) {
            return Float.NaN;
        }
        return this.width / this.height;
    }

    public Vector2 getCenter(Vector2 vector) {
        vector.x = this.x + (this.width / 2.0f);
        vector.y = this.y + (this.height / 2.0f);
        return vector;
    }

    public Rectangle setCenter(float x2, float y2) {
        setPosition(x2 - (this.width / 2.0f), y2 - (this.height / 2.0f));
        return this;
    }

    public Rectangle setCenter(Vector2 position) {
        setPosition(position.x - (this.width / 2.0f), position.y - (this.height / 2.0f));
        return this;
    }

    public Rectangle fitOutside(Rectangle rect) {
        float ratio = getAspectRatio();
        if (ratio > rect.getAspectRatio()) {
            setSize(rect.height * ratio, rect.height);
        } else {
            setSize(rect.width, rect.width / ratio);
        }
        setPosition((rect.x + (rect.width / 2.0f)) - (this.width / 2.0f), (rect.y + (rect.height / 2.0f)) - (this.height / 2.0f));
        return this;
    }

    public Rectangle fitInside(Rectangle rect) {
        float ratio = getAspectRatio();
        if (ratio < rect.getAspectRatio()) {
            setSize(rect.height * ratio, rect.height);
        } else {
            setSize(rect.width, rect.width / ratio);
        }
        setPosition((rect.x + (rect.width / 2.0f)) - (this.width / 2.0f), (rect.y + (rect.height / 2.0f)) - (this.height / 2.0f));
        return this;
    }

    public String toString() {
        return String.valueOf(this.x) + "," + this.y + "," + this.width + "," + this.height;
    }

    public float area() {
        return this.width * this.height;
    }

    public float perimeter() {
        return 2.0f * (this.width + this.height);
    }

    public int hashCode() {
        return ((((((NumberUtils.floatToRawIntBits(this.height) + 31) * 31) + NumberUtils.floatToRawIntBits(this.width)) * 31) + NumberUtils.floatToRawIntBits(this.x)) * 31) + NumberUtils.floatToRawIntBits(this.y);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Rectangle other = (Rectangle) obj;
        if (NumberUtils.floatToRawIntBits(this.height) != NumberUtils.floatToRawIntBits(other.height)) {
            return false;
        }
        if (NumberUtils.floatToRawIntBits(this.width) != NumberUtils.floatToRawIntBits(other.width)) {
            return false;
        }
        if (NumberUtils.floatToRawIntBits(this.x) != NumberUtils.floatToRawIntBits(other.x)) {
            return false;
        }
        if (NumberUtils.floatToRawIntBits(this.y) != NumberUtils.floatToRawIntBits(other.y)) {
            return false;
        }
        return true;
    }
}
