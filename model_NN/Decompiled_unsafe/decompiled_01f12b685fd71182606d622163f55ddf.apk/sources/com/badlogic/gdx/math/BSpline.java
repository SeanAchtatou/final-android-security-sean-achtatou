package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;

public class BSpline<T extends Vector<T>> implements Path<T> {
    private static final float d6 = 0.16666667f;
    public boolean continuous;
    public T[] controlPoints;
    public int degree;
    public Array<T> knots;
    public int spanCount;
    private T tmp;
    private T tmp2;
    private T tmp3;

    public static <T extends Vector<T>> T cubic(T out, float t, T[] points, boolean continuous2, T tmp4) {
        int n = continuous2 ? points.length : points.length - 3;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return cubic(out, i, u - ((float) i), points, continuous2, tmp4);
    }

    public static <T extends Vector<T>> T cubic_derivative(T out, float t, T[] points, boolean continuous2, T tmp4) {
        int n = continuous2 ? points.length : points.length - 3;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return cubic(out, i, u - ((float) i), points, continuous2, tmp4);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T cubic(T r10, int r11, float r12, T[] r13, boolean r14, T r15) {
        /*
            r9 = 1065353216(0x3f800000, float:1.0)
            r8 = 1077936128(0x40400000, float:3.0)
            r7 = 1042983595(0x3e2aaaab, float:0.16666667)
            int r1 = r13.length
            float r0 = r9 - r12
            float r2 = r12 * r12
            float r3 = r2 * r12
            r4 = r13[r11]
            com.badlogic.gdx.math.Vector r4 = r10.set(r4)
            float r5 = r8 * r3
            r6 = 1086324736(0x40c00000, float:6.0)
            float r6 = r6 * r2
            float r5 = r5 - r6
            r6 = 1082130432(0x40800000, float:4.0)
            float r5 = r5 + r6
            float r5 = r5 * r7
            r4.scl(r5)
            if (r14 != 0) goto L_0x0025
            if (r11 <= 0) goto L_0x003b
        L_0x0025:
            int r4 = r1 + r11
            int r4 = r4 + -1
            int r4 = r4 % r1
            r4 = r13[r4]
            com.badlogic.gdx.math.Vector r4 = r15.set(r4)
            float r5 = r0 * r0
            float r5 = r5 * r0
            float r5 = r5 * r7
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            r10.add(r4)
        L_0x003b:
            if (r14 != 0) goto L_0x0041
            int r4 = r1 + -1
            if (r11 >= r4) goto L_0x005c
        L_0x0041:
            int r4 = r11 + 1
            int r4 = r4 % r1
            r4 = r13[r4]
            com.badlogic.gdx.math.Vector r4 = r15.set(r4)
            r5 = -1069547520(0xffffffffc0400000, float:-3.0)
            float r5 = r5 * r3
            float r6 = r8 * r2
            float r5 = r5 + r6
            float r6 = r8 * r12
            float r5 = r5 + r6
            float r5 = r5 + r9
            float r5 = r5 * r7
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            r10.add(r4)
        L_0x005c:
            if (r14 != 0) goto L_0x0062
            int r4 = r1 + -2
            if (r11 >= r4) goto L_0x0074
        L_0x0062:
            int r4 = r11 + 2
            int r4 = r4 % r1
            r4 = r13[r4]
            com.badlogic.gdx.math.Vector r4 = r15.set(r4)
            float r5 = r3 * r7
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            r10.add(r4)
        L_0x0074:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.BSpline.cubic(com.badlogic.gdx.math.Vector, int, float, com.badlogic.gdx.math.Vector[], boolean, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T cubic_derivative(T r9, int r10, float r11, T[] r12, boolean r13, T r14) {
        /*
            r8 = 1065353216(0x3f800000, float:1.0)
            r7 = 1056964608(0x3f000000, float:0.5)
            int r1 = r12.length
            float r0 = r8 - r11
            float r2 = r11 * r11
            float r3 = r2 * r11
            r4 = r12[r10]
            com.badlogic.gdx.math.Vector r4 = r9.set(r4)
            r5 = 1069547520(0x3fc00000, float:1.5)
            float r5 = r5 * r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r6 * r11
            float r5 = r5 - r6
            r4.scl(r5)
            if (r13 != 0) goto L_0x001f
            if (r10 <= 0) goto L_0x0037
        L_0x001f:
            int r4 = r1 + r10
            int r4 = r4 + -1
            int r4 = r4 % r1
            r4 = r12[r4]
            com.badlogic.gdx.math.Vector r4 = r14.set(r4)
            float r5 = r8 - r11
            float r5 = r5 * r7
            float r6 = r8 - r11
            float r5 = r5 * r6
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            r9.add(r4)
        L_0x0037:
            if (r13 != 0) goto L_0x003d
            int r4 = r1 + -1
            if (r10 >= r4) goto L_0x0052
        L_0x003d:
            int r4 = r10 + 1
            int r4 = r4 % r1
            r4 = r12[r4]
            com.badlogic.gdx.math.Vector r4 = r14.set(r4)
            r5 = -1077936128(0xffffffffbfc00000, float:-1.5)
            float r5 = r5 * r2
            float r5 = r5 + r11
            float r5 = r5 + r7
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            r9.add(r4)
        L_0x0052:
            if (r13 != 0) goto L_0x0058
            int r4 = r1 + -2
            if (r10 >= r4) goto L_0x006a
        L_0x0058:
            int r4 = r10 + 2
            int r4 = r4 % r1
            r4 = r12[r4]
            com.badlogic.gdx.math.Vector r4 = r14.set(r4)
            float r5 = r7 * r2
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            r9.add(r4)
        L_0x006a:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.BSpline.cubic_derivative(com.badlogic.gdx.math.Vector, int, float, com.badlogic.gdx.math.Vector[], boolean, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    public static <T extends Vector<T>> T calculate(T out, float t, T[] points, int degree2, boolean continuous2, T tmp4) {
        int n = continuous2 ? points.length : points.length - degree2;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return calculate(out, i, u - ((float) i), points, degree2, continuous2, tmp4);
    }

    public static <T extends Vector<T>> T derivative(T out, float t, T[] points, int degree2, boolean continuous2, T tmp4) {
        int n = continuous2 ? points.length : points.length - degree2;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return derivative(out, i, u - ((float) i), points, degree2, continuous2, tmp4);
    }

    public static <T extends Vector<T>> T calculate(T out, int i, float u, T[] points, int degree2, boolean continuous2, T tmp4) {
        switch (degree2) {
            case 3:
                return cubic(out, i, u, points, continuous2, tmp4);
            default:
                return out;
        }
    }

    public static <T extends Vector<T>> T derivative(T out, int i, float u, T[] points, int degree2, boolean continuous2, T tmp4) {
        switch (degree2) {
            case 3:
                return cubic_derivative(out, i, u, points, continuous2, tmp4);
            default:
                return out;
        }
    }

    public BSpline() {
    }

    public BSpline(T[] controlPoints2, int degree2, boolean continuous2) {
        set(controlPoints2, degree2, continuous2);
    }

    public BSpline set(T[] controlPoints2, int degree2, boolean continuous2) {
        if (this.tmp == null) {
            this.tmp = controlPoints2[0].cpy();
        }
        if (this.tmp2 == null) {
            this.tmp2 = controlPoints2[0].cpy();
        }
        if (this.tmp3 == null) {
            this.tmp3 = controlPoints2[0].cpy();
        }
        this.controlPoints = controlPoints2;
        this.degree = degree2;
        this.continuous = continuous2;
        this.spanCount = continuous2 ? controlPoints2.length : controlPoints2.length - degree2;
        if (this.knots == null) {
            this.knots = new Array<>(this.spanCount);
        } else {
            this.knots.clear();
            this.knots.ensureCapacity(this.spanCount);
        }
        for (int i = 0; i < this.spanCount; i++) {
            this.knots.add(calculate(controlPoints2[0].cpy(), continuous2 ? i : (int) (((float) i) + (0.5f * ((float) degree2))), Animation.CurveTimeline.LINEAR, controlPoints2, degree2, continuous2, this.tmp));
        }
        return this;
    }

    public T valueAt(T out, float t) {
        int n = this.spanCount;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return valueAt(out, i, u - ((float) i));
    }

    public T valueAt(T out, int span, float u) {
        return calculate(out, this.continuous ? span : span + ((int) (((float) this.degree) * 0.5f)), u, this.controlPoints, this.degree, this.continuous, this.tmp);
    }

    public T derivativeAt(T out, float t) {
        int n = this.spanCount;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return derivativeAt(out, i, u - ((float) i));
    }

    public T derivativeAt(T out, int span, float u) {
        return derivative(out, this.continuous ? span : span + ((int) (((float) this.degree) * 0.5f)), u, this.controlPoints, this.degree, this.continuous, this.tmp);
    }

    public int nearest(T in) {
        return nearest(in, 0, this.spanCount);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int nearest(T r8, int r9, int r10) {
        /*
            r7 = this;
        L_0x0000:
            if (r9 < 0) goto L_0x0016
            int r5 = r7.spanCount
            int r4 = r9 % r5
            com.badlogic.gdx.utils.Array<T> r5 = r7.knots
            java.lang.Object r5 = r5.get(r4)
            com.badlogic.gdx.math.Vector r5 = (com.badlogic.gdx.math.Vector) r5
            float r1 = r8.dst2(r5)
            r2 = 1
        L_0x0013:
            if (r2 < r10) goto L_0x001a
            return r4
        L_0x0016:
            int r5 = r7.spanCount
            int r9 = r9 + r5
            goto L_0x0000
        L_0x001a:
            int r5 = r9 + r2
            int r6 = r7.spanCount
            int r3 = r5 % r6
            com.badlogic.gdx.utils.Array<T> r5 = r7.knots
            java.lang.Object r5 = r5.get(r3)
            com.badlogic.gdx.math.Vector r5 = (com.badlogic.gdx.math.Vector) r5
            float r0 = r8.dst2(r5)
            int r5 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r5 >= 0) goto L_0x0032
            r1 = r0
            r4 = r3
        L_0x0032:
            int r2 = r2 + 1
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.BSpline.nearest(com.badlogic.gdx.math.Vector, int, int):int");
    }

    public float approximate(T v) {
        return approximate(v, nearest(v));
    }

    public float approximate(T in, int start, int count) {
        return approximate(in, nearest(in, start, count));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float approximate(T r21, int r22) {
        /*
            r20 = this;
            r11 = r22
            r0 = r20
            com.badlogic.gdx.utils.Array<T> r0 = r0.knots
            r17 = r0
            r0 = r17
            java.lang.Object r12 = r0.get(r11)
            com.badlogic.gdx.math.Vector r12 = (com.badlogic.gdx.math.Vector) r12
            r0 = r20
            com.badlogic.gdx.utils.Array<T> r0 = r0.knots
            r18 = r0
            if (r11 <= 0) goto L_0x008d
            int r17 = r11 + -1
        L_0x001a:
            r0 = r18
            r1 = r17
            java.lang.Object r14 = r0.get(r1)
            com.badlogic.gdx.math.Vector r14 = (com.badlogic.gdx.math.Vector) r14
            r0 = r20
            com.badlogic.gdx.utils.Array<T> r0 = r0.knots
            r17 = r0
            int r18 = r11 + 1
            r0 = r20
            int r0 = r0.spanCount
            r19 = r0
            int r18 = r18 % r19
            java.lang.Object r13 = r17.get(r18)
            com.badlogic.gdx.math.Vector r13 = (com.badlogic.gdx.math.Vector) r13
            r0 = r21
            float r10 = r0.dst2(r14)
            r0 = r21
            float r9 = r0.dst2(r13)
            int r17 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r17 >= 0) goto L_0x0096
            r6 = r12
            r7 = r13
            r8 = r21
        L_0x004e:
            float r3 = r6.dst2(r7)
            float r4 = r8.dst2(r7)
            float r5 = r8.dst2(r6)
            double r0 = (double) r3
            r18 = r0
            double r18 = java.lang.Math.sqrt(r18)
            r0 = r18
            float r2 = (float) r0
            float r17 = r4 + r3
            float r17 = r17 - r5
            r18 = 1073741824(0x40000000, float:2.0)
            float r18 = r18 * r2
            float r15 = r17 / r18
            float r17 = r2 - r15
            float r17 = r17 / r2
            r18 = 0
            r19 = 1065353216(0x3f800000, float:1.0)
            float r16 = com.badlogic.gdx.math.MathUtils.clamp(r17, r18, r19)
            float r0 = (float) r11
            r17 = r0
            float r17 = r17 + r16
            r0 = r20
            int r0 = r0.spanCount
            r18 = r0
            r0 = r18
            float r0 = (float) r0
            r18 = r0
            float r17 = r17 / r18
            return r17
        L_0x008d:
            r0 = r20
            int r0 = r0.spanCount
            r17 = r0
            int r17 = r17 + -1
            goto L_0x001a
        L_0x0096:
            r6 = r14
            r7 = r12
            r8 = r21
            if (r11 <= 0) goto L_0x009f
            int r11 = r11 + -1
        L_0x009e:
            goto L_0x004e
        L_0x009f:
            r0 = r20
            int r0 = r0.spanCount
            r17 = r0
            int r11 = r17 + -1
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.BSpline.approximate(com.badlogic.gdx.math.Vector, int):float");
    }

    public float locate(T v) {
        return approximate((Vector) v);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float approxLength(int r7) {
        /*
            r6 = this;
            r1 = 0
            r0 = 0
        L_0x0002:
            if (r0 < r7) goto L_0x0005
            return r1
        L_0x0005:
            T r2 = r6.tmp2
            T r3 = r6.tmp3
            r2.set(r3)
            T r2 = r6.tmp3
            float r3 = (float) r0
            float r4 = (float) r7
            r5 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r5
            float r3 = r3 / r4
            r6.valueAt(r2, r3)
            if (r0 <= 0) goto L_0x0022
            T r2 = r6.tmp2
            T r3 = r6.tmp3
            float r2 = r2.dst(r3)
            float r1 = r1 + r2
        L_0x0022:
            int r0 = r0 + 1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.BSpline.approxLength(int):float");
    }
}
