package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ShortArray;
import com.kbz.esotericsoftware.spine.Animation;

public class ConvexHull {
    private final FloatArray hull = new FloatArray();
    private final IntArray indices = new IntArray();
    private final ShortArray originalIndices = new ShortArray(false, 0);
    private final IntArray quicksortStack = new IntArray();
    private float[] sortedPoints;

    public FloatArray computePolygon(FloatArray points, boolean sorted) {
        return computePolygon(points.items, 0, points.size, sorted);
    }

    public FloatArray computePolygon(float[] polygon, boolean sorted) {
        return computePolygon(polygon, 0, polygon.length, sorted);
    }

    public FloatArray computePolygon(float[] points, int offset, int count, boolean sorted) {
        int end = offset + count;
        if (!sorted) {
            if (this.sortedPoints == null || this.sortedPoints.length < count) {
                this.sortedPoints = new float[count];
            }
            System.arraycopy(points, offset, this.sortedPoints, 0, count);
            points = this.sortedPoints;
            offset = 0;
            sort(points, count);
        }
        FloatArray hull2 = this.hull;
        hull2.clear();
        for (int i = offset; i < end; i += 2) {
            float x = points[i];
            float y = points[i + 1];
            while (hull2.size >= 4 && ccw(x, y) <= Animation.CurveTimeline.LINEAR) {
                hull2.size -= 2;
            }
            hull2.add(x);
            hull2.add(y);
        }
        int t = hull2.size + 2;
        for (int i2 = end - 4; i2 >= offset; i2 -= 2) {
            float x2 = points[i2];
            float y2 = points[i2 + 1];
            while (hull2.size >= t && ccw(x2, y2) <= Animation.CurveTimeline.LINEAR) {
                hull2.size -= 2;
            }
            hull2.add(x2);
            hull2.add(y2);
        }
        return hull2;
    }

    public IntArray computeIndices(FloatArray points, boolean sorted, boolean yDown) {
        return computeIndices(points.items, 0, points.size, sorted, yDown);
    }

    public IntArray computeIndices(float[] polygon, boolean sorted, boolean yDown) {
        return computeIndices(polygon, 0, polygon.length, sorted, yDown);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v0, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v2, resolved type: int} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.IntArray computeIndices(float[] r18, int r19, int r20, boolean r21, boolean r22) {
        /*
            r17 = this;
            int r4 = r19 + r20
            if (r21 != 0) goto L_0x003f
            r0 = r17
            float[] r15 = r0.sortedPoints
            if (r15 == 0) goto L_0x0013
            r0 = r17
            float[] r15 = r0.sortedPoints
            int r15 = r15.length
            r0 = r20
            if (r15 >= r0) goto L_0x001b
        L_0x0013:
            r0 = r20
            float[] r15 = new float[r0]
            r0 = r17
            r0.sortedPoints = r15
        L_0x001b:
            r0 = r17
            float[] r15 = r0.sortedPoints
            r16 = 0
            r0 = r18
            r1 = r19
            r2 = r16
            r3 = r20
            java.lang.System.arraycopy(r0, r1, r15, r2, r3)
            r0 = r17
            float[] r0 = r0.sortedPoints
            r18 = r0
            r19 = 0
            r0 = r17
            r1 = r18
            r2 = r20
            r3 = r22
            r0.sortWithIndices(r1, r2, r3)
        L_0x003f:
            r0 = r17
            com.badlogic.gdx.utils.IntArray r8 = r0.indices
            r8.clear()
            r0 = r17
            com.badlogic.gdx.utils.FloatArray r5 = r0.hull
            r5.clear()
            r6 = r19
            int r7 = r6 / 2
        L_0x0051:
            if (r6 < r4) goto L_0x006f
            int r6 = r4 + -4
            int r7 = r6 / 2
            int r15 = r5.size
            int r12 = r15 + 2
        L_0x005b:
            r0 = r19
            if (r6 >= r0) goto L_0x00a4
            if (r21 != 0) goto L_0x006e
            r0 = r17
            com.badlogic.gdx.utils.ShortArray r15 = r0.originalIndices
            short[] r11 = r15.items
            int[] r9 = r8.items
            r6 = 0
            int r10 = r8.size
        L_0x006c:
            if (r6 < r10) goto L_0x00d5
        L_0x006e:
            return r8
        L_0x006f:
            r13 = r18[r6]
            int r15 = r6 + 1
            r14 = r18[r15]
        L_0x0075:
            int r15 = r5.size
            r16 = 4
            r0 = r16
            if (r15 < r0) goto L_0x0089
            r0 = r17
            float r15 = r0.ccw(r13, r14)
            r16 = 0
            int r15 = (r15 > r16 ? 1 : (r15 == r16 ? 0 : -1))
            if (r15 <= 0) goto L_0x0097
        L_0x0089:
            r5.add(r13)
            r5.add(r14)
            r8.add(r7)
            int r6 = r6 + 2
            int r7 = r7 + 1
            goto L_0x0051
        L_0x0097:
            int r15 = r5.size
            int r15 = r15 + -2
            r5.size = r15
            int r15 = r8.size
            int r15 = r15 + -1
            r8.size = r15
            goto L_0x0075
        L_0x00a4:
            r13 = r18[r6]
            int r15 = r6 + 1
            r14 = r18[r15]
        L_0x00aa:
            int r15 = r5.size
            if (r15 < r12) goto L_0x00ba
            r0 = r17
            float r15 = r0.ccw(r13, r14)
            r16 = 0
            int r15 = (r15 > r16 ? 1 : (r15 == r16 ? 0 : -1))
            if (r15 <= 0) goto L_0x00c8
        L_0x00ba:
            r5.add(r13)
            r5.add(r14)
            r8.add(r7)
            int r6 = r6 + -2
            int r7 = r7 + -1
            goto L_0x005b
        L_0x00c8:
            int r15 = r5.size
            int r15 = r15 + -2
            r5.size = r15
            int r15 = r8.size
            int r15 = r15 + -1
            r8.size = r15
            goto L_0x00aa
        L_0x00d5:
            r15 = r9[r6]
            short r15 = r11[r15]
            r9[r6] = r15
            int r6 = r6 + 1
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.ConvexHull.computeIndices(float[], int, int, boolean, boolean):com.badlogic.gdx.utils.IntArray");
    }

    private float ccw(float p3x, float p3y) {
        FloatArray hull2 = this.hull;
        int size = hull2.size;
        float p1x = hull2.get(size - 4);
        float p1y = hull2.get(size - 3);
        return ((hull2.get(size - 2) - p1x) * (p3y - p1y)) - ((hull2.peek() - p1y) * (p3x - p1x));
    }

    private void sort(float[] values, int count) {
        IntArray stack = this.quicksortStack;
        stack.add(0);
        stack.add((count - 1) - 1);
        while (stack.size > 0) {
            int upper = stack.pop();
            int lower = stack.pop();
            if (upper > lower) {
                int i = quicksortPartition(values, lower, upper);
                if (i - lower > upper - i) {
                    stack.add(lower);
                    stack.add(i - 2);
                }
                stack.add(i + 2);
                stack.add(upper);
                if (upper - i >= i - lower) {
                    stack.add(lower);
                    stack.add(i - 2);
                }
            }
        }
    }

    private int quicksortPartition(float[] values, int lower, int upper) {
        float x = values[lower];
        float y = values[lower + 1];
        int up = upper;
        int down = lower;
        while (down < up) {
            while (down < up && values[down] <= x) {
                down += 2;
            }
            while (true) {
                if (values[up] > x || (values[up] == x && values[up + 1] < y)) {
                    up -= 2;
                }
            }
            if (down < up) {
                float temp = values[down];
                values[down] = values[up];
                values[up] = temp;
                float temp2 = values[down + 1];
                values[down + 1] = values[up + 1];
                values[up + 1] = temp2;
            }
        }
        values[lower] = values[up];
        values[up] = x;
        values[lower + 1] = values[up + 1];
        values[up + 1] = y;
        return up;
    }

    private void sortWithIndices(float[] values, int count, boolean yDown) {
        int pointCount = count / 2;
        this.originalIndices.clear();
        this.originalIndices.ensureCapacity(pointCount);
        short[] originalIndicesArray = this.originalIndices.items;
        for (short i = 0; i < pointCount; i = (short) (i + 1)) {
            originalIndicesArray[i] = i;
        }
        IntArray stack = this.quicksortStack;
        stack.add(0);
        stack.add((count - 1) - 1);
        while (stack.size > 0) {
            int upper = stack.pop();
            int lower = stack.pop();
            if (upper > lower) {
                int i2 = quicksortPartitionWithIndices(values, lower, upper, yDown, originalIndicesArray);
                if (i2 - lower > upper - i2) {
                    stack.add(lower);
                    stack.add(i2 - 2);
                }
                stack.add(i2 + 2);
                stack.add(upper);
                if (upper - i2 >= i2 - lower) {
                    stack.add(lower);
                    stack.add(i2 - 2);
                }
            }
        }
    }

    private int quicksortPartitionWithIndices(float[] values, int lower, int upper, boolean yDown, short[] originalIndices2) {
        float x = values[lower];
        float y = values[lower + 1];
        int up = upper;
        int down = lower;
        while (down < up) {
            while (down < up && values[down] <= x) {
                down += 2;
            }
            if (!yDown) {
                while (true) {
                    if (values[up] <= x && (values[up] != x || values[up + 1] <= y)) {
                        break;
                    }
                    up -= 2;
                }
            } else {
                while (true) {
                    if (values[up] <= x && (values[up] != x || values[up + 1] >= y)) {
                        break;
                    }
                    up -= 2;
                }
            }
            if (down < up) {
                float temp = values[down];
                values[down] = values[up];
                values[up] = temp;
                float temp2 = values[down + 1];
                values[down + 1] = values[up + 1];
                values[up + 1] = temp2;
                short tempIndex = originalIndices2[down / 2];
                originalIndices2[down / 2] = originalIndices2[up / 2];
                originalIndices2[up / 2] = tempIndex;
            }
        }
        values[lower] = values[up];
        values[up] = x;
        values[lower + 1] = values[up + 1];
        values[up + 1] = y;
        short tempIndex2 = originalIndices2[lower / 2];
        originalIndices2[lower / 2] = originalIndices2[up / 2];
        originalIndices2[up / 2] = tempIndex2;
        return up;
    }
}
