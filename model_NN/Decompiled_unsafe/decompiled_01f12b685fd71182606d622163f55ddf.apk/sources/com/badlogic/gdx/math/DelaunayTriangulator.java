package com.badlogic.gdx.math;

import com.badlogic.gdx.utils.BooleanArray;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ShortArray;

public class DelaunayTriangulator {
    private static final int COMPLETE = 1;
    private static final float EPSILON = 1.0E-6f;
    private static final int INCOMPLETE = 2;
    private static final int INSIDE = 0;
    private final Vector2 centroid = new Vector2();
    private final BooleanArray complete = new BooleanArray(false, 16);
    private final IntArray edges = new IntArray();
    private final ShortArray originalIndices = new ShortArray(false, 0);
    private final IntArray quicksortStack = new IntArray();
    private float[] sortedPoints;
    private final float[] superTriangle = new float[6];
    private final ShortArray triangles = new ShortArray(false, 16);

    public ShortArray computeTriangles(FloatArray points, boolean sorted) {
        return computeTriangles(points.items, 0, points.size, sorted);
    }

    public ShortArray computeTriangles(float[] polygon, boolean sorted) {
        return computeTriangles(polygon, 0, polygon.length, sorted);
    }

    public ShortArray computeTriangles(float[] points, int offset, int count, boolean sorted) {
        float x1;
        float y1;
        float x2;
        float y2;
        float x3;
        float y3;
        ShortArray triangles2 = this.triangles;
        triangles2.clear();
        if (count >= 6) {
            triangles2.ensureCapacity(count);
            if (!sorted) {
                if (this.sortedPoints == null || this.sortedPoints.length < count) {
                    this.sortedPoints = new float[count];
                }
                System.arraycopy(points, offset, this.sortedPoints, 0, count);
                points = this.sortedPoints;
                offset = 0;
                sort(points, count);
            }
            int end = offset + count;
            float xmin = points[0];
            float ymin = points[1];
            float xmax = xmin;
            float ymax = ymin;
            int i = offset + 2;
            while (i < end) {
                float value = points[i];
                if (value < xmin) {
                    xmin = value;
                }
                if (value > xmax) {
                    xmax = value;
                }
                int i2 = i + 1;
                float value2 = points[i2];
                if (value2 < ymin) {
                    ymin = value2;
                }
                if (value2 > ymax) {
                    ymax = value2;
                }
                i = i2 + 1;
            }
            float dx = xmax - xmin;
            float dy = ymax - ymin;
            if (dx <= dy) {
                dx = dy;
            }
            float dmax = dx * 20.0f;
            float xmid = (xmax + xmin) / 2.0f;
            float ymid = (ymax + ymin) / 2.0f;
            float[] superTriangle2 = this.superTriangle;
            superTriangle2[0] = xmid - dmax;
            superTriangle2[1] = ymid - dmax;
            superTriangle2[2] = xmid;
            superTriangle2[3] = ymid + dmax;
            superTriangle2[4] = xmid + dmax;
            superTriangle2[5] = ymid - dmax;
            IntArray edges2 = this.edges;
            edges2.ensureCapacity(count / 2);
            BooleanArray complete2 = this.complete;
            complete2.clear();
            complete2.ensureCapacity(count);
            triangles2.add(end);
            triangles2.add(end + 2);
            triangles2.add(end + 4);
            complete2.add(false);
            for (int pointIndex = offset; pointIndex < end; pointIndex += 2) {
                float x = points[pointIndex];
                float y = points[pointIndex + 1];
                short[] trianglesArray = triangles2.items;
                boolean[] completeArray = complete2.items;
                for (int triangleIndex = triangles2.size - 1; triangleIndex >= 0; triangleIndex -= 3) {
                    int completeIndex = triangleIndex / 3;
                    if (!completeArray[completeIndex]) {
                        short s = trianglesArray[triangleIndex - 2];
                        short s2 = trianglesArray[triangleIndex - 1];
                        short s3 = trianglesArray[triangleIndex];
                        if (s >= end) {
                            int i3 = s - end;
                            x1 = superTriangle2[i3];
                            y1 = superTriangle2[i3 + 1];
                        } else {
                            x1 = points[s];
                            y1 = points[s + 1];
                        }
                        if (s2 >= end) {
                            int i4 = s2 - end;
                            x2 = superTriangle2[i4];
                            y2 = superTriangle2[i4 + 1];
                        } else {
                            x2 = points[s2];
                            y2 = points[s2 + 1];
                        }
                        if (s3 >= end) {
                            int i5 = s3 - end;
                            x3 = superTriangle2[i5];
                            y3 = superTriangle2[i5 + 1];
                        } else {
                            x3 = points[s3];
                            y3 = points[s3 + 1];
                        }
                        switch (circumCircle(x, y, x1, y1, x2, y2, x3, y3)) {
                            case 0:
                                edges2.add(s);
                                edges2.add(s2);
                                edges2.add(s2);
                                edges2.add(s3);
                                edges2.add(s3);
                                edges2.add(s);
                                triangles2.removeIndex(triangleIndex);
                                triangles2.removeIndex(triangleIndex - 1);
                                triangles2.removeIndex(triangleIndex - 2);
                                complete2.removeIndex(completeIndex);
                                break;
                            case 1:
                                completeArray[completeIndex] = true;
                                break;
                        }
                    }
                }
                int[] edgesArray = edges2.items;
                int n = edges2.size;
                for (int i6 = 0; i6 < n; i6 += 2) {
                    int p1 = edgesArray[i6];
                    if (p1 != -1) {
                        int p2 = edgesArray[i6 + 1];
                        boolean skip = false;
                        for (int ii = i6 + 2; ii < n; ii += 2) {
                            if (p1 == edgesArray[ii + 1] && p2 == edgesArray[ii]) {
                                skip = true;
                                edgesArray[ii] = -1;
                            }
                        }
                        if (!skip) {
                            triangles2.add(p1);
                            triangles2.add(edgesArray[i6 + 1]);
                            triangles2.add(pointIndex);
                            complete2.add(false);
                        }
                    }
                }
                edges2.clear();
            }
            short[] trianglesArray2 = triangles2.items;
            for (int i7 = triangles2.size - 1; i7 >= 0; i7 -= 3) {
                if (trianglesArray2[i7] >= end || trianglesArray2[i7 - 1] >= end || trianglesArray2[i7 - 2] >= end) {
                    triangles2.removeIndex(i7);
                    triangles2.removeIndex(i7 - 1);
                    triangles2.removeIndex(i7 - 2);
                }
            }
            if (!sorted) {
                short[] originalIndicesArray = this.originalIndices.items;
                int n2 = triangles2.size;
                for (int i8 = 0; i8 < n2; i8++) {
                    trianglesArray2[i8] = (short) (originalIndicesArray[trianglesArray2[i8] / 2] * 2);
                }
            }
            if (offset == 0) {
                int n3 = triangles2.size;
                for (int i9 = 0; i9 < n3; i9++) {
                    trianglesArray2[i9] = (short) (trianglesArray2[i9] / 2);
                }
            } else {
                int n4 = triangles2.size;
                for (int i10 = 0; i10 < n4; i10++) {
                    trianglesArray2[i10] = (short) ((trianglesArray2[i10] - offset) / 2);
                }
            }
        }
        return triangles2;
    }

    private int circumCircle(float xp, float yp, float x1, float y1, float x2, float y2, float x3, float y3) {
        float xc;
        float yc;
        float y1y2 = Math.abs(y1 - y2);
        float y2y3 = Math.abs(y2 - y3);
        if (y1y2 >= 1.0E-6f) {
            float m1 = (-(x2 - x1)) / (y2 - y1);
            float mx1 = (x1 + x2) / 2.0f;
            float my1 = (y1 + y2) / 2.0f;
            if (y2y3 < 1.0E-6f) {
                xc = (x3 + x2) / 2.0f;
                yc = ((xc - mx1) * m1) + my1;
            } else {
                float m2 = (-(x3 - x2)) / (y3 - y2);
                xc = ((((m1 * mx1) - (m2 * ((x2 + x3) / 2.0f))) + ((y2 + y3) / 2.0f)) - my1) / (m1 - m2);
                yc = ((xc - mx1) * m1) + my1;
            }
        } else if (y2y3 < 1.0E-6f) {
            return 2;
        } else {
            xc = (x2 + x1) / 2.0f;
            yc = ((xc - ((x2 + x3) / 2.0f)) * ((-(x3 - x2)) / (y3 - y2))) + ((y2 + y3) / 2.0f);
        }
        float dx = x2 - xc;
        float dy = y2 - yc;
        float rsqr = (dx * dx) + (dy * dy);
        float dx2 = xp - xc;
        float dx3 = dx2 * dx2;
        float dy2 = yp - yc;
        if (((dy2 * dy2) + dx3) - rsqr <= 1.0E-6f) {
            return 0;
        }
        return (xp <= xc || dx3 <= rsqr) ? 2 : 1;
    }

    private void sort(float[] values, int count) {
        int pointCount = count / 2;
        this.originalIndices.clear();
        this.originalIndices.ensureCapacity(pointCount);
        short[] originalIndicesArray = this.originalIndices.items;
        for (short i = 0; i < pointCount; i = (short) (i + 1)) {
            originalIndicesArray[i] = i;
        }
        IntArray stack = this.quicksortStack;
        stack.add(0);
        stack.add((count - 1) - 1);
        while (stack.size > 0) {
            int upper = stack.pop();
            int lower = stack.pop();
            if (upper > lower) {
                int i2 = quicksortPartition(values, lower, upper, originalIndicesArray);
                if (i2 - lower > upper - i2) {
                    stack.add(lower);
                    stack.add(i2 - 2);
                }
                stack.add(i2 + 2);
                stack.add(upper);
                if (upper - i2 >= i2 - lower) {
                    stack.add(lower);
                    stack.add(i2 - 2);
                }
            }
        }
    }

    private int quicksortPartition(float[] values, int lower, int upper, short[] originalIndices2) {
        float value = values[lower];
        int up = upper;
        int down = lower + 2;
        while (down < up) {
            while (down < up && values[down] <= value) {
                down += 2;
            }
            while (values[up] > value) {
                up -= 2;
            }
            if (down < up) {
                float tempValue = values[down];
                values[down] = values[up];
                values[up] = tempValue;
                float tempValue2 = values[down + 1];
                values[down + 1] = values[up + 1];
                values[up + 1] = tempValue2;
                short tempIndex = originalIndices2[down / 2];
                originalIndices2[down / 2] = originalIndices2[up / 2];
                originalIndices2[up / 2] = tempIndex;
            }
        }
        values[lower] = values[up];
        values[up] = value;
        float tempValue3 = values[lower + 1];
        values[lower + 1] = values[up + 1];
        values[up + 1] = tempValue3;
        short tempIndex2 = originalIndices2[lower / 2];
        originalIndices2[lower / 2] = originalIndices2[up / 2];
        originalIndices2[up / 2] = tempIndex2;
        return up;
    }

    public void trim(ShortArray triangles2, float[] points, float[] hull, int offset, int count) {
        short[] trianglesArray = triangles2.items;
        for (int i = triangles2.size - 1; i >= 0; i -= 3) {
            int p1 = trianglesArray[i - 2] * 2;
            int p2 = trianglesArray[i - 1] * 2;
            int p3 = trianglesArray[i] * 2;
            GeometryUtils.triangleCentroid(points[p1], points[p1 + 1], points[p2], points[p2 + 1], points[p3], points[p3 + 1], this.centroid);
            if (!Intersector.isPointInPolygon(hull, offset, count, this.centroid.x, this.centroid.y)) {
                triangles2.removeIndex(i);
                triangles2.removeIndex(i - 1);
                triangles2.removeIndex(i - 2);
            }
        }
    }
}
