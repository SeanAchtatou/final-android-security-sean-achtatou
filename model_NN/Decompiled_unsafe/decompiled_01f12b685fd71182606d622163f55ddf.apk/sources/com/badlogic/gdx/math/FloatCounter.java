package com.badlogic.gdx.math;

import com.kbz.esotericsoftware.spine.Animation;

public class FloatCounter {
    public float average;
    public int count;
    public float latest;
    public float max;
    public final WindowedMean mean;
    public float min;
    public float total;
    public float value;

    public FloatCounter(int windowSize) {
        this.mean = windowSize > 1 ? new WindowedMean(windowSize) : null;
        reset();
    }

    public void put(float value2) {
        this.latest = value2;
        this.total += value2;
        this.count++;
        this.average = this.total / ((float) this.count);
        if (this.mean != null) {
            this.mean.addValue(value2);
            this.value = this.mean.getMean();
        } else {
            this.value = this.latest;
        }
        if (this.mean == null || this.mean.hasEnoughData()) {
            if (this.value < this.min) {
                this.min = this.value;
            }
            if (this.value > this.max) {
                this.max = this.value;
            }
        }
    }

    public void reset() {
        this.count = 0;
        this.total = Animation.CurveTimeline.LINEAR;
        this.min = Float.MAX_VALUE;
        this.max = Float.MIN_VALUE;
        this.average = Animation.CurveTimeline.LINEAR;
        this.latest = Animation.CurveTimeline.LINEAR;
        this.value = Animation.CurveTimeline.LINEAR;
        if (this.mean != null) {
            this.mean.clear();
        }
    }
}
