package com.badlogic.gdx.math;

public class GridPoint3 {
    public int x;
    public int y;
    public int z;

    public GridPoint3() {
    }

    public GridPoint3(int x2, int y2, int z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
    }

    public GridPoint3(GridPoint3 point) {
        this.x = point.x;
        this.y = point.y;
        this.z = point.z;
    }

    public GridPoint3 set(GridPoint3 point) {
        this.x = point.x;
        this.y = point.y;
        this.z = point.z;
        return this;
    }

    public GridPoint3 set(int x2, int y2, int z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
        return this;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != getClass()) {
            return false;
        }
        GridPoint3 g = (GridPoint3) o;
        if (this.x == g.x && this.y == g.y && this.z == g.z) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((this.x + 17) * 17) + this.y) * 17) + this.z;
    }
}
