package com.badlogic.gdx.math;

public interface Path<T> {
    float approxLength(int i);

    float approximate(Object obj);

    T derivativeAt(Object obj, float f);

    float locate(Object obj);

    T valueAt(Object obj, float f);
}
