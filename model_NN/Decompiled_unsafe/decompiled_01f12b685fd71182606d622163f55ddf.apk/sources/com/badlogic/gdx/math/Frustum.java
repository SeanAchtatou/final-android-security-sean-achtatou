package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.kbz.esotericsoftware.spine.Animation;

public class Frustum {
    protected static final Vector3[] clipSpacePlanePoints = {new Vector3(-1.0f, -1.0f, -1.0f), new Vector3(1.0f, -1.0f, -1.0f), new Vector3(1.0f, 1.0f, -1.0f), new Vector3(-1.0f, 1.0f, -1.0f), new Vector3(-1.0f, -1.0f, 1.0f), new Vector3(1.0f, -1.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f), new Vector3(-1.0f, 1.0f, 1.0f)};
    protected static final float[] clipSpacePlanePointsArray = new float[24];
    private static final Vector3 tmpV = new Vector3();
    public final Vector3[] planePoints = {new Vector3(), new Vector3(), new Vector3(), new Vector3(), new Vector3(), new Vector3(), new Vector3(), new Vector3()};
    protected final float[] planePointsArray = new float[24];
    public final Plane[] planes = new Plane[6];

    static {
        int i = 0;
        Vector3[] vector3Arr = clipSpacePlanePoints;
        int length = vector3Arr.length;
        int j = 0;
        while (i < length) {
            Vector3 v = vector3Arr[i];
            int j2 = j + 1;
            clipSpacePlanePointsArray[j] = v.x;
            int j3 = j2 + 1;
            clipSpacePlanePointsArray[j2] = v.y;
            clipSpacePlanePointsArray[j3] = v.z;
            i++;
            j = j3 + 1;
        }
    }

    public Frustum() {
        for (int i = 0; i < 6; i++) {
            this.planes[i] = new Plane(new Vector3(), (float) Animation.CurveTimeline.LINEAR);
        }
    }

    public void update(Matrix4 inverseProjectionView) {
        System.arraycopy(clipSpacePlanePointsArray, 0, this.planePointsArray, 0, clipSpacePlanePointsArray.length);
        Matrix4.prj(inverseProjectionView.val, this.planePointsArray, 0, 8, 3);
        int i = 0;
        int j = 0;
        while (true) {
            int j2 = j;
            if (i >= 8) {
                this.planes[0].set(this.planePoints[1], this.planePoints[0], this.planePoints[2]);
                this.planes[1].set(this.planePoints[4], this.planePoints[5], this.planePoints[7]);
                this.planes[2].set(this.planePoints[0], this.planePoints[4], this.planePoints[3]);
                this.planes[3].set(this.planePoints[5], this.planePoints[1], this.planePoints[6]);
                this.planes[4].set(this.planePoints[2], this.planePoints[3], this.planePoints[6]);
                this.planes[5].set(this.planePoints[4], this.planePoints[0], this.planePoints[1]);
                return;
            }
            Vector3 v = this.planePoints[i];
            int j3 = j2 + 1;
            v.x = this.planePointsArray[j2];
            int j4 = j3 + 1;
            v.y = this.planePointsArray[j3];
            j = j4 + 1;
            v.z = this.planePointsArray[j4];
            i++;
        }
    }

    public boolean pointInFrustum(Vector3 point) {
        for (Plane testPoint : this.planes) {
            if (testPoint.testPoint(point) == Plane.PlaneSide.Back) {
                return false;
            }
        }
        return true;
    }

    public boolean pointInFrustum(float x, float y, float z) {
        for (Plane testPoint : this.planes) {
            if (testPoint.testPoint(x, y, z) == Plane.PlaneSide.Back) {
                return false;
            }
        }
        return true;
    }

    public boolean sphereInFrustum(Vector3 center, float radius) {
        for (int i = 0; i < 6; i++) {
            if ((this.planes[i].normal.x * center.x) + (this.planes[i].normal.y * center.y) + (this.planes[i].normal.z * center.z) < (-radius) - this.planes[i].d) {
                return false;
            }
        }
        return true;
    }

    public boolean sphereInFrustum(float x, float y, float z, float radius) {
        for (int i = 0; i < 6; i++) {
            if ((this.planes[i].normal.x * x) + (this.planes[i].normal.y * y) + (this.planes[i].normal.z * z) < (-radius) - this.planes[i].d) {
                return false;
            }
        }
        return true;
    }

    public boolean sphereInFrustumWithoutNearFar(Vector3 center, float radius) {
        for (int i = 2; i < 6; i++) {
            if ((this.planes[i].normal.x * center.x) + (this.planes[i].normal.y * center.y) + (this.planes[i].normal.z * center.z) < (-radius) - this.planes[i].d) {
                return false;
            }
        }
        return true;
    }

    public boolean sphereInFrustumWithoutNearFar(float x, float y, float z, float radius) {
        for (int i = 2; i < 6; i++) {
            if ((this.planes[i].normal.x * x) + (this.planes[i].normal.y * y) + (this.planes[i].normal.z * z) < (-radius) - this.planes[i].d) {
                return false;
            }
        }
        return true;
    }

    public boolean boundsInFrustum(BoundingBox bounds) {
        int len2 = this.planes.length;
        for (int i = 0; i < len2; i++) {
            if (this.planes[i].testPoint(bounds.getCorner000(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner001(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner010(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner011(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner100(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner101(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner110(tmpV)) == Plane.PlaneSide.Back && this.planes[i].testPoint(bounds.getCorner111(tmpV)) == Plane.PlaneSide.Back) {
                return false;
            }
        }
        return true;
    }

    public boolean boundsInFrustum(Vector3 center, Vector3 dimensions) {
        return boundsInFrustum(center.x, center.y, center.z, dimensions.x / 2.0f, dimensions.y / 2.0f, dimensions.z / 2.0f);
    }

    public boolean boundsInFrustum(float x, float y, float z, float halfWidth, float halfHeight, float halfDepth) {
        int len2 = this.planes.length;
        for (int i = 0; i < len2; i++) {
            if (this.planes[i].testPoint(x + halfWidth, y + halfHeight, z + halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x + halfWidth, y + halfHeight, z - halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x + halfWidth, y - halfHeight, z + halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x + halfWidth, y - halfHeight, z - halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x - halfWidth, y + halfHeight, z + halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x - halfWidth, y + halfHeight, z - halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x - halfWidth, y - halfHeight, z + halfDepth) == Plane.PlaneSide.Back && this.planes[i].testPoint(x - halfWidth, y - halfHeight, z - halfDepth) == Plane.PlaneSide.Back) {
                return false;
            }
        }
        return true;
    }
}
