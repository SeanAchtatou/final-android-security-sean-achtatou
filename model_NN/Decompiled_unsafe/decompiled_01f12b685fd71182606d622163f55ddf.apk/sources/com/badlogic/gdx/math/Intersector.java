package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Arrays;
import java.util.List;

public final class Intersector {
    static Vector3 best = new Vector3();
    private static final Vector3 dir = new Vector3();
    private static final Vector3 i = new Vector3();
    static Vector3 intersection = new Vector3();
    private static final Plane p = new Plane(new Vector3(), (float) Animation.CurveTimeline.LINEAR);
    private static final Vector3 start = new Vector3();
    static Vector3 tmp = new Vector3();
    static Vector3 tmp1 = new Vector3();
    static Vector3 tmp2 = new Vector3();
    static Vector3 tmp3 = new Vector3();
    private static final Vector3 v0 = new Vector3();
    private static final Vector3 v1 = new Vector3();
    private static final Vector3 v2 = new Vector3();
    static Vector2 v2tmp = new Vector2();

    public static class MinimumTranslationVector {
        public float depth = Animation.CurveTimeline.LINEAR;
        public Vector2 normal = new Vector2();
    }

    public static boolean isPointInTriangle(Vector3 point, Vector3 t1, Vector3 t2, Vector3 t3) {
        v0.set(t1).sub(point);
        v1.set(t2).sub(point);
        v2.set(t3).sub(point);
        float ab = v0.dot(v1);
        float ac = v0.dot(v2);
        float bc = v1.dot(v2);
        if ((bc * ac) - (v2.dot(v2) * ab) < Animation.CurveTimeline.LINEAR) {
            return false;
        }
        if ((ab * bc) - (ac * v1.dot(v1)) >= Animation.CurveTimeline.LINEAR) {
            return true;
        }
        return false;
    }

    public static boolean isPointInTriangle(Vector2 p2, Vector2 a, Vector2 b, Vector2 c) {
        boolean side12;
        boolean z;
        boolean z2;
        float px1 = p2.x - a.x;
        float py1 = p2.y - a.y;
        if (((b.x - a.x) * py1) - ((b.y - a.y) * px1) > Animation.CurveTimeline.LINEAR) {
            side12 = true;
        } else {
            side12 = false;
        }
        if (((c.x - a.x) * py1) - ((c.y - a.y) * px1) > Animation.CurveTimeline.LINEAR) {
            z = true;
        } else {
            z = false;
        }
        if (z == side12) {
            return false;
        }
        if (((c.x - b.x) * (p2.y - b.y)) - ((c.y - b.y) * (p2.x - b.x)) > Animation.CurveTimeline.LINEAR) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2 == side12) {
            return true;
        }
        return false;
    }

    public static boolean isPointInTriangle(float px, float py, float ax, float ay, float bx, float by, float cx, float cy) {
        float px1 = px - ax;
        float py1 = py - ay;
        boolean side12 = ((bx - ax) * py1) - ((by - ay) * px1) > Animation.CurveTimeline.LINEAR;
        if ((((cx - ax) * py1) - ((cy - ay) * px1) > Animation.CurveTimeline.LINEAR) == side12) {
            return false;
        }
        if ((((cx - bx) * (py - by)) - ((cy - by) * (px - bx)) > Animation.CurveTimeline.LINEAR) != side12) {
            return false;
        }
        return true;
    }

    public static boolean intersectSegmentPlane(Vector3 start2, Vector3 end, Plane plane, Vector3 intersection2) {
        Vector3 dir2 = v0.set(end).sub(start2);
        float t = (-(start2.dot(plane.getNormal()) + plane.getD())) / dir2.dot(plane.getNormal());
        if (t < Animation.CurveTimeline.LINEAR || t > 1.0f) {
            return false;
        }
        intersection2.set(start2).add(dir2.scl(t));
        return true;
    }

    public static int pointLineSide(Vector2 linePoint1, Vector2 linePoint2, Vector2 point) {
        return (int) Math.signum(((linePoint2.x - linePoint1.x) * (point.y - linePoint1.y)) - ((linePoint2.y - linePoint1.y) * (point.x - linePoint1.x)));
    }

    public static int pointLineSide(float linePoint1X, float linePoint1Y, float linePoint2X, float linePoint2Y, float pointX, float pointY) {
        return (int) Math.signum(((linePoint2X - linePoint1X) * (pointY - linePoint1Y)) - ((linePoint2Y - linePoint1Y) * (pointX - linePoint1X)));
    }

    public static boolean isPointInPolygon(Array<Vector2> polygon, Vector2 point) {
        Vector2 lastVertice = polygon.peek();
        boolean oddNodes = false;
        for (int i2 = 0; i2 < polygon.size; i2++) {
            Vector2 vertice = polygon.get(i2);
            if (((vertice.y < point.y && lastVertice.y >= point.y) || (lastVertice.y < point.y && vertice.y >= point.y)) && vertice.x + (((point.y - vertice.y) / (lastVertice.y - vertice.y)) * (lastVertice.x - vertice.x)) < point.x) {
                oddNodes = !oddNodes;
            }
            lastVertice = vertice;
        }
        return oddNodes;
    }

    public static boolean isPointInPolygon(float[] polygon, int offset, int count, float x, float y) {
        boolean oddNodes = false;
        int j = (offset + count) - 2;
        int n = j;
        for (int i2 = offset; i2 <= n; i2 += 2) {
            float yi = polygon[i2 + 1];
            float yj = polygon[j + 1];
            if ((yi < y && yj >= y) || (yj < y && yi >= y)) {
                float xi = polygon[i2];
                if ((((y - yi) / (yj - yi)) * (polygon[j] - xi)) + xi < x) {
                    oddNodes = !oddNodes;
                }
            }
            j = i2;
        }
        return oddNodes;
    }

    public static float distanceLinePoint(float startX, float startY, float endX, float endY, float pointX, float pointY) {
        return Math.abs(((pointX - startX) * (endY - startY)) - ((pointY - startY) * (endX - startX))) / ((float) Math.sqrt((double) (((endX - startX) * (endX - startX)) + ((endY - startY) * (endY - startY)))));
    }

    public static float distanceSegmentPoint(float startX, float startY, float endX, float endY, float pointX, float pointY) {
        return nearestSegmentPoint(startX, startY, endX, endY, pointX, pointY, v2tmp).dst(pointX, pointY);
    }

    public static float distanceSegmentPoint(Vector2 start2, Vector2 end, Vector2 point) {
        return nearestSegmentPoint(start2, end, point, v2tmp).dst(point);
    }

    public static Vector2 nearestSegmentPoint(Vector2 start2, Vector2 end, Vector2 point, Vector2 nearest) {
        float length2 = start2.dst2(end);
        if (length2 == Animation.CurveTimeline.LINEAR) {
            return nearest.set(start2);
        }
        float t = (((point.x - start2.x) * (end.x - start2.x)) + ((point.y - start2.y) * (end.y - start2.y))) / length2;
        if (t < Animation.CurveTimeline.LINEAR) {
            return nearest.set(start2);
        }
        if (t > 1.0f) {
            return nearest.set(end);
        }
        return nearest.set(start2.x + ((end.x - start2.x) * t), start2.y + ((end.y - start2.y) * t));
    }

    public static Vector2 nearestSegmentPoint(float startX, float startY, float endX, float endY, float pointX, float pointY, Vector2 nearest) {
        float xDiff = endX - startX;
        float yDiff = endY - startY;
        float length2 = (xDiff * xDiff) + (yDiff * yDiff);
        if (length2 == Animation.CurveTimeline.LINEAR) {
            return nearest.set(startX, startY);
        }
        float t = (((pointX - startX) * (endX - startX)) + ((pointY - startY) * (endY - startY))) / length2;
        if (t < Animation.CurveTimeline.LINEAR) {
            return nearest.set(startX, startY);
        }
        if (t > 1.0f) {
            return nearest.set(endX, endY);
        }
        return nearest.set(((endX - startX) * t) + startX, ((endY - startY) * t) + startY);
    }

    public static boolean intersectSegmentCircle(Vector2 start2, Vector2 end, Vector2 center, float squareRadius) {
        tmp.set(end.x - start2.x, end.y - start2.y, Animation.CurveTimeline.LINEAR);
        tmp1.set(center.x - start2.x, center.y - start2.y, Animation.CurveTimeline.LINEAR);
        float l = tmp.len();
        float u = tmp1.dot(tmp.nor());
        if (u <= Animation.CurveTimeline.LINEAR) {
            tmp2.set(start2.x, start2.y, Animation.CurveTimeline.LINEAR);
        } else if (u >= l) {
            tmp2.set(end.x, end.y, Animation.CurveTimeline.LINEAR);
        } else {
            tmp3.set(tmp.scl(u));
            tmp2.set(tmp3.x + start2.x, tmp3.y + start2.y, Animation.CurveTimeline.LINEAR);
        }
        float x = center.x - tmp2.x;
        float y = center.y - tmp2.y;
        if ((x * x) + (y * y) <= squareRadius) {
            return true;
        }
        return false;
    }

    public static float intersectSegmentCircleDisplace(Vector2 start2, Vector2 end, Vector2 point, float radius, Vector2 displacement) {
        float d = start2.dst(end);
        float u = (((point.x - start2.x) * (end.x - start2.x)) + ((point.y - start2.y) * (end.y - start2.y))) / (d * d);
        if (u < Animation.CurveTimeline.LINEAR || u > 1.0f) {
            return Float.POSITIVE_INFINITY;
        }
        tmp.set(end.x, end.y, Animation.CurveTimeline.LINEAR).sub(start2.x, start2.y, Animation.CurveTimeline.LINEAR);
        tmp2.set(start2.x, start2.y, Animation.CurveTimeline.LINEAR).add(tmp.scl(u));
        float d2 = tmp2.dst(point.x, point.y, Animation.CurveTimeline.LINEAR);
        if (d2 >= radius) {
            return Float.POSITIVE_INFINITY;
        }
        displacement.set(point).sub(tmp2.x, tmp2.y).nor();
        return d2;
    }

    public static float intersectRayRay(Vector2 start1, Vector2 direction1, Vector2 start2, Vector2 direction2) {
        float difx = start2.x - start1.x;
        float dify = start2.y - start1.y;
        float d1xd2 = (direction1.x * direction2.y) - (direction1.y * direction2.x);
        if (d1xd2 == Animation.CurveTimeline.LINEAR) {
            return Float.POSITIVE_INFINITY;
        }
        return (difx * (direction2.y / d1xd2)) - (dify * (direction2.x / d1xd2));
    }

    public static boolean intersectRayPlane(Ray ray, Plane plane, Vector3 intersection2) {
        float denom = ray.direction.dot(plane.getNormal());
        if (denom != Animation.CurveTimeline.LINEAR) {
            float t = (-(ray.origin.dot(plane.getNormal()) + plane.getD())) / denom;
            if (t < Animation.CurveTimeline.LINEAR) {
                return false;
            }
            if (intersection2 != null) {
                intersection2.set(ray.origin).add(v0.set(ray.direction).scl(t));
            }
            return true;
        } else if (plane.testPoint(ray.origin) != Plane.PlaneSide.OnPlane) {
            return false;
        } else {
            if (intersection2 != null) {
                intersection2.set(ray.origin);
            }
            return true;
        }
    }

    public static float intersectLinePlane(float x, float y, float z, float x2, float y2, float z2, Plane plane, Vector3 intersection2) {
        Vector3 direction = tmp.set(x2, y2, z2).sub(x, y, z);
        Vector3 origin = tmp2.set(x, y, z);
        float denom = direction.dot(plane.getNormal());
        if (denom != Animation.CurveTimeline.LINEAR) {
            float t = (-(origin.dot(plane.getNormal()) + plane.getD())) / denom;
            if (intersection2 == null) {
                return t;
            }
            intersection2.set(origin).add(direction.scl(t));
            return t;
        } else if (plane.testPoint(origin) != Plane.PlaneSide.OnPlane) {
            return -1.0f;
        } else {
            if (intersection2 == null) {
                return Animation.CurveTimeline.LINEAR;
            }
            intersection2.set(origin);
            return Animation.CurveTimeline.LINEAR;
        }
    }

    public static boolean intersectRayTriangle(Ray ray, Vector3 t1, Vector3 t2, Vector3 t3, Vector3 intersection2) {
        p.set(t1, t2, t3);
        if (!intersectRayPlane(ray, p, i)) {
            return false;
        }
        v0.set(t3).sub(t1);
        v1.set(t2).sub(t1);
        v2.set(i).sub(t1);
        float dot00 = v0.dot(v0);
        float dot01 = v0.dot(v1);
        float dot02 = v0.dot(v2);
        float dot11 = v1.dot(v1);
        float dot12 = v1.dot(v2);
        float denom = (dot00 * dot11) - (dot01 * dot01);
        if (denom == Animation.CurveTimeline.LINEAR) {
            return false;
        }
        float u = ((dot11 * dot02) - (dot01 * dot12)) / denom;
        float v = ((dot00 * dot12) - (dot01 * dot02)) / denom;
        if (u < Animation.CurveTimeline.LINEAR || v < Animation.CurveTimeline.LINEAR || u + v > 1.0f) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(i);
        }
        return true;
    }

    public static boolean intersectRaySphere(Ray ray, Vector3 center, float radius, Vector3 intersection2) {
        float len = ray.direction.dot(center.x - ray.origin.x, center.y - ray.origin.y, center.z - ray.origin.z);
        if (len < Animation.CurveTimeline.LINEAR) {
            return false;
        }
        float dst2 = center.dst2(ray.origin.x + (ray.direction.x * len), ray.origin.y + (ray.direction.y * len), ray.origin.z + (ray.direction.z * len));
        float r2 = radius * radius;
        if (dst2 > r2) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(ray.direction).scl(len - ((float) Math.sqrt((double) (r2 - dst2)))).add(ray.origin);
        }
        return true;
    }

    public static boolean intersectRayBounds(Ray ray, BoundingBox box, Vector3 intersection2) {
        v0.set(ray.origin).sub(box.min);
        v1.set(ray.origin).sub(box.max);
        if (v0.x > Animation.CurveTimeline.LINEAR && v0.y > Animation.CurveTimeline.LINEAR && v0.z > Animation.CurveTimeline.LINEAR && v1.x < Animation.CurveTimeline.LINEAR && v1.y < Animation.CurveTimeline.LINEAR && v1.z < Animation.CurveTimeline.LINEAR) {
            return true;
        }
        float lowest = Animation.CurveTimeline.LINEAR;
        boolean hit = false;
        if (ray.origin.x <= box.min.x && ray.direction.x > Animation.CurveTimeline.LINEAR) {
            float t = (box.min.x - ray.origin.x) / ray.direction.x;
            if (t >= Animation.CurveTimeline.LINEAR) {
                v2.set(ray.direction).scl(t).add(ray.origin);
                if (v2.y >= box.min.y && v2.y <= box.max.y && v2.z >= box.min.z && v2.z <= box.max.z && (0 == 0 || t < Animation.CurveTimeline.LINEAR)) {
                    hit = true;
                    lowest = t;
                }
            }
        }
        if (ray.origin.x >= box.max.x && ray.direction.x < Animation.CurveTimeline.LINEAR) {
            float t2 = (box.max.x - ray.origin.x) / ray.direction.x;
            if (t2 >= Animation.CurveTimeline.LINEAR) {
                v2.set(ray.direction).scl(t2).add(ray.origin);
                if (v2.y >= box.min.y && v2.y <= box.max.y && v2.z >= box.min.z && v2.z <= box.max.z && (!hit || t2 < lowest)) {
                    hit = true;
                    lowest = t2;
                }
            }
        }
        if (ray.origin.y <= box.min.y && ray.direction.y > Animation.CurveTimeline.LINEAR) {
            float t3 = (box.min.y - ray.origin.y) / ray.direction.y;
            if (t3 >= Animation.CurveTimeline.LINEAR) {
                v2.set(ray.direction).scl(t3).add(ray.origin);
                if (v2.x >= box.min.x && v2.x <= box.max.x && v2.z >= box.min.z && v2.z <= box.max.z && (!hit || t3 < lowest)) {
                    hit = true;
                    lowest = t3;
                }
            }
        }
        if (ray.origin.y >= box.max.y && ray.direction.y < Animation.CurveTimeline.LINEAR) {
            float t4 = (box.max.y - ray.origin.y) / ray.direction.y;
            if (t4 >= Animation.CurveTimeline.LINEAR) {
                v2.set(ray.direction).scl(t4).add(ray.origin);
                if (v2.x >= box.min.x && v2.x <= box.max.x && v2.z >= box.min.z && v2.z <= box.max.z && (!hit || t4 < lowest)) {
                    hit = true;
                    lowest = t4;
                }
            }
        }
        if (ray.origin.z <= box.min.z && ray.direction.z > Animation.CurveTimeline.LINEAR) {
            float t5 = (box.min.z - ray.origin.z) / ray.direction.z;
            if (t5 >= Animation.CurveTimeline.LINEAR) {
                v2.set(ray.direction).scl(t5).add(ray.origin);
                if (v2.x >= box.min.x && v2.x <= box.max.x && v2.y >= box.min.y && v2.y <= box.max.y && (!hit || t5 < lowest)) {
                    hit = true;
                    lowest = t5;
                }
            }
        }
        if (ray.origin.z >= box.max.z && ray.direction.z < Animation.CurveTimeline.LINEAR) {
            float t6 = (box.max.z - ray.origin.z) / ray.direction.z;
            if (t6 >= Animation.CurveTimeline.LINEAR) {
                v2.set(ray.direction).scl(t6).add(ray.origin);
                if (v2.x >= box.min.x && v2.x <= box.max.x && v2.y >= box.min.y && v2.y <= box.max.y && (!hit || t6 < lowest)) {
                    hit = true;
                    lowest = t6;
                }
            }
        }
        if (!hit || intersection2 == null) {
            return hit;
        }
        intersection2.set(ray.direction).scl(lowest).add(ray.origin);
        return hit;
    }

    public static boolean intersectRayBoundsFast(Ray ray, BoundingBox box) {
        return intersectRayBoundsFast(ray, box.getCenter(tmp1), box.getDimensions(tmp2));
    }

    public static boolean intersectRayBoundsFast(Ray ray, Vector3 center, Vector3 dimensions) {
        float divX = 1.0f / ray.direction.x;
        float divY = 1.0f / ray.direction.y;
        float divZ = 1.0f / ray.direction.z;
        float minx = ((center.x - (dimensions.x * 0.5f)) - ray.origin.x) * divX;
        float maxx = ((center.x + (dimensions.x * 0.5f)) - ray.origin.x) * divX;
        if (minx > maxx) {
            float t = minx;
            minx = maxx;
            maxx = t;
        }
        float miny = ((center.y - (dimensions.y * 0.5f)) - ray.origin.y) * divY;
        float maxy = ((center.y + (dimensions.y * 0.5f)) - ray.origin.y) * divY;
        if (miny > maxy) {
            float t2 = miny;
            miny = maxy;
            maxy = t2;
        }
        float minz = ((center.z - (dimensions.z * 0.5f)) - ray.origin.z) * divZ;
        float maxz = ((center.z + (dimensions.z * 0.5f)) - ray.origin.z) * divZ;
        if (minz > maxz) {
            float t3 = minz;
            minz = maxz;
            maxz = t3;
        }
        float min = Math.max(Math.max(minx, miny), minz);
        float max = Math.min(Math.min(maxx, maxy), maxz);
        return max >= Animation.CurveTimeline.LINEAR && max >= min;
    }

    public static boolean intersectRayTriangles(Ray ray, float[] triangles, Vector3 intersection2) {
        float min_dist = Float.MAX_VALUE;
        boolean hit = false;
        if ((triangles.length / 3) % 3 != 0) {
            throw new RuntimeException("triangle list size is not a multiple of 3");
        }
        for (int i2 = 0; i2 < triangles.length - 6; i2 += 9) {
            if (intersectRayTriangle(ray, tmp1.set(triangles[i2], triangles[i2 + 1], triangles[i2 + 2]), tmp2.set(triangles[i2 + 3], triangles[i2 + 4], triangles[i2 + 5]), tmp3.set(triangles[i2 + 6], triangles[i2 + 7], triangles[i2 + 8]), tmp)) {
                float dist = ray.origin.dst2(tmp);
                if (dist < min_dist) {
                    min_dist = dist;
                    best.set(tmp);
                    hit = true;
                }
            }
        }
        if (!hit) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(best);
        }
        return true;
    }

    public static boolean intersectRayTriangles(Ray ray, float[] vertices, short[] indices, int vertexSize, Vector3 intersection2) {
        float min_dist = Float.MAX_VALUE;
        boolean hit = false;
        if (indices.length % 3 != 0) {
            throw new RuntimeException("triangle list size is not a multiple of 3");
        }
        for (int i2 = 0; i2 < indices.length; i2 += 3) {
            int i1 = indices[i2] * vertexSize;
            int i22 = indices[i2 + 1] * vertexSize;
            int i3 = indices[i2 + 2] * vertexSize;
            if (intersectRayTriangle(ray, tmp1.set(vertices[i1], vertices[i1 + 1], vertices[i1 + 2]), tmp2.set(vertices[i22], vertices[i22 + 1], vertices[i22 + 2]), tmp3.set(vertices[i3], vertices[i3 + 1], vertices[i3 + 2]), tmp)) {
                float dist = ray.origin.dst2(tmp);
                if (dist < min_dist) {
                    min_dist = dist;
                    best.set(tmp);
                    hit = true;
                }
            }
        }
        if (!hit) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(best);
        }
        return true;
    }

    public static boolean intersectRayTriangles(Ray ray, List<Vector3> triangles, Vector3 intersection2) {
        float min_dist = Float.MAX_VALUE;
        boolean hit = false;
        if (triangles.size() % 3 != 0) {
            throw new RuntimeException("triangle list size is not a multiple of 3");
        }
        for (int i2 = 0; i2 < triangles.size() - 2; i2 += 3) {
            if (intersectRayTriangle(ray, triangles.get(i2), triangles.get(i2 + 1), triangles.get(i2 + 2), tmp)) {
                float dist = ray.origin.dst2(tmp);
                if (dist < min_dist) {
                    min_dist = dist;
                    best.set(tmp);
                    hit = true;
                }
            }
        }
        if (!hit) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(best);
        }
        return true;
    }

    public static boolean intersectLines(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Vector2 intersection2) {
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;
        float x3 = p3.x;
        float y3 = p3.y;
        float x4 = p4.x;
        float y4 = p4.y;
        float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
        if (d == Animation.CurveTimeline.LINEAR) {
            return false;
        }
        if (intersection2 != null) {
            float ua = (((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3))) / d;
            intersection2.set(((x2 - x1) * ua) + x1, ((y2 - y1) * ua) + y1);
        }
        return true;
    }

    public static boolean intersectLines(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Vector2 intersection2) {
        float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
        if (d == Animation.CurveTimeline.LINEAR) {
            return false;
        }
        if (intersection2 != null) {
            float ua = (((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3))) / d;
            intersection2.set(((x2 - x1) * ua) + x1, ((y2 - y1) * ua) + y1);
        }
        return true;
    }

    public static boolean intersectLinePolygon(Vector2 p1, Vector2 p2, Polygon polygon) {
        float[] vertices = polygon.getTransformedVertices();
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;
        int n = vertices.length;
        float x3 = vertices[n - 2];
        float y3 = vertices[n - 1];
        for (int i2 = 0; i2 < n; i2 += 2) {
            float x4 = vertices[i2];
            float y4 = vertices[i2 + 1];
            float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
            if (d != Animation.CurveTimeline.LINEAR) {
                float ua = (((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3))) / d;
                if (ua >= Animation.CurveTimeline.LINEAR && ua <= 1.0f) {
                    return true;
                }
            }
            x3 = x4;
            y3 = y4;
        }
        return false;
    }

    public static boolean intersectRectangles(Rectangle rectangle1, Rectangle rectangle2, Rectangle intersection2) {
        if (!rectangle1.overlaps(rectangle2)) {
            return false;
        }
        intersection2.x = Math.max(rectangle1.x, rectangle2.x);
        intersection2.width = Math.min(rectangle1.x + rectangle1.width, rectangle2.x + rectangle2.width) - intersection2.x;
        intersection2.y = Math.max(rectangle1.y, rectangle2.y);
        intersection2.height = Math.min(rectangle1.y + rectangle1.height, rectangle2.y + rectangle2.height) - intersection2.y;
        return true;
    }

    public static boolean intersectSegmentPolygon(Vector2 p1, Vector2 p2, Polygon polygon) {
        float[] vertices = polygon.getTransformedVertices();
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;
        int n = vertices.length;
        float x3 = vertices[n - 2];
        float y3 = vertices[n - 1];
        for (int i2 = 0; i2 < n; i2 += 2) {
            float x4 = vertices[i2];
            float y4 = vertices[i2 + 1];
            float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
            if (d != Animation.CurveTimeline.LINEAR) {
                float yd = y1 - y3;
                float xd = x1 - x3;
                float ua = (((x4 - x3) * yd) - ((y4 - y3) * xd)) / d;
                if (ua >= Animation.CurveTimeline.LINEAR && ua <= 1.0f) {
                    float ub = (((x2 - x1) * yd) - ((y2 - y1) * xd)) / d;
                    if (ub >= Animation.CurveTimeline.LINEAR && ub <= 1.0f) {
                        return true;
                    }
                }
            }
            x3 = x4;
            y3 = y4;
        }
        return false;
    }

    public static boolean intersectSegments(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Vector2 intersection2) {
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;
        float x3 = p3.x;
        float y3 = p3.y;
        float x4 = p4.x;
        float y4 = p4.y;
        float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
        if (d == Animation.CurveTimeline.LINEAR) {
            return false;
        }
        float yd = y1 - y3;
        float xd = x1 - x3;
        float ua = (((x4 - x3) * yd) - ((y4 - y3) * xd)) / d;
        if (ua < Animation.CurveTimeline.LINEAR || ua > 1.0f) {
            return false;
        }
        float ub = (((x2 - x1) * yd) - ((y2 - y1) * xd)) / d;
        if (ub < Animation.CurveTimeline.LINEAR || ub > 1.0f) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(((x2 - x1) * ua) + x1, ((y2 - y1) * ua) + y1);
        }
        return true;
    }

    public static boolean intersectSegments(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Vector2 intersection2) {
        float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
        if (d == Animation.CurveTimeline.LINEAR) {
            return false;
        }
        float yd = y1 - y3;
        float xd = x1 - x3;
        float ua = (((x4 - x3) * yd) - ((y4 - y3) * xd)) / d;
        if (ua < Animation.CurveTimeline.LINEAR || ua > 1.0f) {
            return false;
        }
        float ub = (((x2 - x1) * yd) - ((y2 - y1) * xd)) / d;
        if (ub < Animation.CurveTimeline.LINEAR || ub > 1.0f) {
            return false;
        }
        if (intersection2 != null) {
            intersection2.set(((x2 - x1) * ua) + x1, ((y2 - y1) * ua) + y1);
        }
        return true;
    }

    static float det(float a, float b, float c, float d) {
        return (a * d) - (b * c);
    }

    static double detd(double a, double b, double c, double d) {
        return (a * d) - (b * c);
    }

    public static boolean overlaps(Circle c1, Circle c2) {
        return c1.overlaps(c2);
    }

    public static boolean overlaps(Rectangle r1, Rectangle r2) {
        return r1.overlaps(r2);
    }

    public static boolean overlaps(Circle c, Rectangle r) {
        float closestX = c.x;
        float closestY = c.y;
        if (c.x < r.x) {
            closestX = r.x;
        } else if (c.x > r.x + r.width) {
            closestX = r.x + r.width;
        }
        if (c.y < r.y) {
            closestY = r.y;
        } else if (c.y > r.y + r.height) {
            closestY = r.y + r.height;
        }
        float closestX2 = closestX - c.x;
        float closestY2 = closestY - c.y;
        if ((closestX2 * closestX2) + (closestY2 * closestY2) < c.radius * c.radius) {
            return true;
        }
        return false;
    }

    public static boolean overlapConvexPolygons(Polygon p1, Polygon p2) {
        return overlapConvexPolygons(p1, p2, (MinimumTranslationVector) null);
    }

    public static boolean overlapConvexPolygons(Polygon p1, Polygon p2, MinimumTranslationVector mtv) {
        return overlapConvexPolygons(p1.getTransformedVertices(), p2.getTransformedVertices(), mtv);
    }

    public static boolean overlapConvexPolygons(float[] verts1, float[] verts2, MinimumTranslationVector mtv) {
        return overlapConvexPolygons(verts1, 0, verts1.length, verts2, 0, verts2.length, mtv);
    }

    public static boolean overlapConvexPolygons(float[] verts1, int offset1, int count1, float[] verts2, int offset2, int count2, MinimumTranslationVector mtv) {
        float overlap = Float.MAX_VALUE;
        float smallestAxisX = Animation.CurveTimeline.LINEAR;
        float smallestAxisY = Animation.CurveTimeline.LINEAR;
        int end1 = offset1 + count1;
        int end2 = offset2 + count2;
        for (int i2 = offset1; i2 < end1; i2 += 2) {
            float x1 = verts1[i2];
            float y1 = verts1[i2 + 1];
            float x2 = verts1[(i2 + 2) % count1];
            float y2 = verts1[(i2 + 3) % count1];
            float axisX = y1 - y2;
            float axisY = -(x1 - x2);
            float length = (float) Math.sqrt((double) ((axisX * axisX) + (axisY * axisY)));
            float axisX2 = axisX / length;
            float axisY2 = axisY / length;
            float min1 = (verts1[0] * axisX2) + (verts1[1] * axisY2);
            float max1 = min1;
            for (int j = offset1; j < end1; j += 2) {
                float p2 = (verts1[j] * axisX2) + (verts1[j + 1] * axisY2);
                if (p2 < min1) {
                    min1 = p2;
                } else if (p2 > max1) {
                    max1 = p2;
                }
            }
            int numInNormalDir = 0;
            float min2 = (verts2[0] * axisX2) + (verts2[1] * axisY2);
            float max2 = min2;
            for (int j2 = offset2; j2 < end2; j2 += 2) {
                numInNormalDir -= pointLineSide(x1, y1, x2, y2, verts2[j2], verts2[j2 + 1]);
                float p3 = (verts2[j2] * axisX2) + (verts2[j2 + 1] * axisY2);
                if (p3 < min2) {
                    min2 = p3;
                } else if (p3 > max2) {
                    max2 = p3;
                }
            }
            if ((min1 > min2 || max1 < min2) && (min2 > min1 || max2 < min1)) {
                return false;
            }
            float o = Math.min(max1, max2) - Math.max(min1, min2);
            if ((min1 < min2 && max1 > max2) || (min2 < min1 && max2 > max1)) {
                float mins = Math.abs(min1 - min2);
                float maxs = Math.abs(max1 - max2);
                o = mins < maxs ? o + mins : o + maxs;
            }
            if (o < overlap) {
                overlap = o;
                smallestAxisX = numInNormalDir >= 0 ? axisX2 : -axisX2;
                if (numInNormalDir >= 0) {
                    smallestAxisY = axisY2;
                } else {
                    smallestAxisY = -axisY2;
                }
            }
        }
        for (int i3 = offset2; i3 < end2; i3 += 2) {
            float x12 = verts2[i3];
            float y12 = verts2[i3 + 1];
            float x22 = verts2[(i3 + 2) % count2];
            float y22 = verts2[(i3 + 3) % count2];
            float axisX3 = y12 - y22;
            float axisY3 = -(x12 - x22);
            float length2 = (float) Math.sqrt((double) ((axisX3 * axisX3) + (axisY3 * axisY3)));
            float axisX4 = axisX3 / length2;
            float axisY4 = axisY3 / length2;
            int numInNormalDir2 = 0;
            float min12 = (verts1[0] * axisX4) + (verts1[1] * axisY4);
            float max12 = min12;
            for (int j3 = offset1; j3 < end1; j3 += 2) {
                float p4 = (verts1[j3] * axisX4) + (verts1[j3 + 1] * axisY4);
                numInNormalDir2 -= pointLineSide(x12, y12, x22, y22, verts1[j3], verts1[j3 + 1]);
                if (p4 < min12) {
                    min12 = p4;
                } else if (p4 > max12) {
                    max12 = p4;
                }
            }
            float min22 = (verts2[0] * axisX4) + (verts2[1] * axisY4);
            float max22 = min22;
            for (int j4 = offset2; j4 < end2; j4 += 2) {
                float p5 = (verts2[j4] * axisX4) + (verts2[j4 + 1] * axisY4);
                if (p5 < min22) {
                    min22 = p5;
                } else if (p5 > max22) {
                    max22 = p5;
                }
            }
            if ((min12 > min22 || max12 < min22) && (min22 > min12 || max22 < min12)) {
                return false;
            }
            float o2 = Math.min(max12, max22) - Math.max(min12, min22);
            if ((min12 < min22 && max12 > max22) || (min22 < min12 && max22 > max12)) {
                float mins2 = Math.abs(min12 - min22);
                float maxs2 = Math.abs(max12 - max22);
                o2 = mins2 < maxs2 ? o2 + mins2 : o2 + maxs2;
            }
            if (o2 < overlap) {
                overlap = o2;
                smallestAxisX = numInNormalDir2 < 0 ? axisX4 : -axisX4;
                if (numInNormalDir2 < 0) {
                    smallestAxisY = axisY4;
                } else {
                    smallestAxisY = -axisY4;
                }
            }
        }
        if (mtv != null) {
            mtv.normal.set(smallestAxisX, smallestAxisY);
            mtv.depth = overlap;
        }
        return true;
    }

    public static void splitTriangle(float[] triangle, Plane plane, SplitTriangle split) {
        boolean r1;
        boolean r2;
        boolean r3;
        int i2;
        boolean z;
        boolean z2 = true;
        int stride = triangle.length / 3;
        if (plane.testPoint(triangle[0], triangle[1], triangle[2]) == Plane.PlaneSide.Back) {
            r1 = true;
        } else {
            r1 = false;
        }
        if (plane.testPoint(triangle[stride + 0], triangle[stride + 1], triangle[stride + 2]) == Plane.PlaneSide.Back) {
            r2 = true;
        } else {
            r2 = false;
        }
        if (plane.testPoint(triangle[(stride * 2) + 0], triangle[(stride * 2) + 1], triangle[(stride * 2) + 2]) == Plane.PlaneSide.Back) {
            r3 = true;
        } else {
            r3 = false;
        }
        split.reset();
        if (r1 == r2 && r2 == r3) {
            split.total = 1;
            if (r1) {
                split.numBack = 1;
                System.arraycopy(triangle, 0, split.back, 0, triangle.length);
                return;
            }
            split.numFront = 1;
            System.arraycopy(triangle, 0, split.front, 0, triangle.length);
            return;
        }
        split.total = 3;
        if (r1) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        split.numFront = (r3 ? 1 : 0) + i2 + (r2 ? 1 : 0);
        split.numBack = split.total - split.numFront;
        split.setSide(r1);
        int second = stride;
        if (r1 != r2) {
            splitEdge(triangle, 0, second, stride, plane, split.edgeSplit, 0);
            split.add(triangle, 0, stride);
            split.add(split.edgeSplit, 0, stride);
            if (split.getSide()) {
                z = false;
            } else {
                z = true;
            }
            split.setSide(z);
            split.add(split.edgeSplit, 0, stride);
        } else {
            split.add(triangle, 0, stride);
        }
        int first = stride;
        int second2 = stride + stride;
        if (r2 != r3) {
            splitEdge(triangle, first, second2, stride, plane, split.edgeSplit, 0);
            split.add(triangle, first, stride);
            split.add(split.edgeSplit, 0, stride);
            split.setSide(!split.getSide());
            split.add(split.edgeSplit, 0, stride);
        } else {
            split.add(triangle, first, stride);
        }
        int first2 = stride + stride;
        if (r3 != r1) {
            splitEdge(triangle, first2, 0, stride, plane, split.edgeSplit, 0);
            split.add(triangle, first2, stride);
            split.add(split.edgeSplit, 0, stride);
            if (split.getSide()) {
                z2 = false;
            }
            split.setSide(z2);
            split.add(split.edgeSplit, 0, stride);
        } else {
            split.add(triangle, first2, stride);
        }
        if (split.numFront == 2) {
            System.arraycopy(split.front, stride * 2, split.front, stride * 3, stride * 2);
            System.arraycopy(split.front, 0, split.front, stride * 5, stride);
            return;
        }
        System.arraycopy(split.back, stride * 2, split.back, stride * 3, stride * 2);
        System.arraycopy(split.back, 0, split.back, stride * 5, stride);
    }

    private static void splitEdge(float[] vertices, int s, int e, int stride, Plane plane, float[] split, int offset) {
        float t = intersectLinePlane(vertices[s], vertices[s + 1], vertices[s + 2], vertices[e], vertices[e + 1], vertices[e + 2], plane, intersection);
        split[offset + 0] = intersection.x;
        split[offset + 1] = intersection.y;
        split[offset + 2] = intersection.z;
        for (int i2 = 3; i2 < stride; i2++) {
            float a = vertices[s + i2];
            split[offset + i2] = ((vertices[e + i2] - a) * t) + a;
        }
    }

    public static void main(String[] args) {
        Plane plane = new Plane(new Vector3(1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR), (float) Animation.CurveTimeline.LINEAR);
        SplitTriangle split = new SplitTriangle(3);
        splitTriangle(new float[]{-10.0f, Animation.CurveTimeline.LINEAR, 10.0f, -1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, -10.0f, Animation.CurveTimeline.LINEAR, 10.0f}, plane, split);
        System.out.println(split);
        splitTriangle(new float[]{-10.0f, Animation.CurveTimeline.LINEAR, 10.0f, 10.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, -10.0f, Animation.CurveTimeline.LINEAR, -10.0f}, plane, split);
        System.out.println(split);
        Circle c1 = new Circle(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
        Circle c2 = new Circle(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
        Circle c3 = new Circle(2.0f, Animation.CurveTimeline.LINEAR, 1.0f);
        Circle c4 = new Circle(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 2.0f);
        System.out.println("Circle test cases");
        System.out.println(c1.overlaps(c1));
        System.out.println(c1.overlaps(c2));
        System.out.println(c1.overlaps(c3));
        System.out.println(c1.overlaps(c4));
        System.out.println(c4.overlaps(c1));
        System.out.println(c1.contains(Animation.CurveTimeline.LINEAR, 1.0f));
        System.out.println(c1.contains(Animation.CurveTimeline.LINEAR, 2.0f));
        System.out.println(c1.contains(c1));
        System.out.println(c1.contains(c4));
        System.out.println(c4.contains(c1));
        System.out.println("Rectangle test cases");
        Rectangle r1 = new Rectangle(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 1.0f);
        Rectangle r2 = new Rectangle(1.0f, Animation.CurveTimeline.LINEAR, 2.0f, 1.0f);
        System.out.println(r1.overlaps(r1));
        System.out.println(r1.overlaps(r2));
        System.out.println(r1.contains(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
        System.out.println("BoundingBox test cases");
        BoundingBox b1 = new BoundingBox(Vector3.Zero, new Vector3(1.0f, 1.0f, 1.0f));
        BoundingBox b2 = new BoundingBox(new Vector3(1.0f, 1.0f, 1.0f), new Vector3(2.0f, 2.0f, 2.0f));
        System.out.println(b1.contains(Vector3.Zero));
        System.out.println(b1.contains(b1));
        System.out.println(b1.contains(b2));
    }

    public static class SplitTriangle {
        public float[] back;
        int backOffset = 0;
        float[] edgeSplit;
        public float[] front;
        boolean frontCurrent = false;
        int frontOffset = 0;
        public int numBack;
        public int numFront;
        public int total;

        public SplitTriangle(int numAttributes) {
            this.front = new float[(numAttributes * 3 * 2)];
            this.back = new float[(numAttributes * 3 * 2)];
            this.edgeSplit = new float[numAttributes];
        }

        public String toString() {
            return "SplitTriangle [front=" + Arrays.toString(this.front) + ", back=" + Arrays.toString(this.back) + ", numFront=" + this.numFront + ", numBack=" + this.numBack + ", total=" + this.total + "]";
        }

        /* access modifiers changed from: package-private */
        public void setSide(boolean front2) {
            this.frontCurrent = front2;
        }

        /* access modifiers changed from: package-private */
        public boolean getSide() {
            return this.frontCurrent;
        }

        /* access modifiers changed from: package-private */
        public void add(float[] vertex, int offset, int stride) {
            if (this.frontCurrent) {
                System.arraycopy(vertex, offset, this.front, this.frontOffset, stride);
                this.frontOffset += stride;
                return;
            }
            System.arraycopy(vertex, offset, this.back, this.backOffset, stride);
            this.backOffset += stride;
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            this.frontCurrent = false;
            this.frontOffset = 0;
            this.backOffset = 0;
            this.numFront = 0;
            this.numBack = 0;
            this.total = 0;
        }
    }
}
