package com.badlogic.gdx.math;

public class GridPoint2 {
    public int x;
    public int y;

    public GridPoint2() {
    }

    public GridPoint2(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public GridPoint2(GridPoint2 point) {
        this.x = point.x;
        this.y = point.y;
    }

    public GridPoint2 set(GridPoint2 point) {
        this.x = point.x;
        this.y = point.y;
        return this;
    }

    public GridPoint2 set(int x2, int y2) {
        this.x = x2;
        this.y = y2;
        return this;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != getClass()) {
            return false;
        }
        GridPoint2 g = (GridPoint2) o;
        if (this.x == g.x && this.y == g.y) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((this.x + 53) * 53) + this.y;
    }
}
