package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class Bezier<T extends Vector<T>> implements Path<T> {
    public Array<T> points = new Array<>();
    private T tmp;
    private T tmp2;
    private T tmp3;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T linear(T r2, float r3, T r4, T r5, T r6) {
        /*
            com.badlogic.gdx.math.Vector r0 = r2.set(r4)
            r1 = 1065353216(0x3f800000, float:1.0)
            float r1 = r1 - r3
            com.badlogic.gdx.math.Vector r0 = r0.scl(r1)
            com.badlogic.gdx.math.Vector r1 = r6.set(r5)
            com.badlogic.gdx.math.Vector r1 = r1.scl(r3)
            com.badlogic.gdx.math.Vector r0 = r0.add(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.linear(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T linear_derivative(T r1, float r2, T r3, T r4, T r5) {
        /*
            com.badlogic.gdx.math.Vector r0 = r1.set(r4)
            com.badlogic.gdx.math.Vector r0 = r0.sub(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.linear_derivative(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T quadratic(T r4, float r5, T r6, T r7, T r8, T r9) {
        /*
            r1 = 1065353216(0x3f800000, float:1.0)
            float r0 = r1 - r5
            com.badlogic.gdx.math.Vector r1 = r4.set(r6)
            float r2 = r0 * r0
            com.badlogic.gdx.math.Vector r1 = r1.scl(r2)
            com.badlogic.gdx.math.Vector r2 = r9.set(r7)
            r3 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 * r0
            float r3 = r3 * r5
            com.badlogic.gdx.math.Vector r2 = r2.scl(r3)
            com.badlogic.gdx.math.Vector r1 = r1.add(r2)
            com.badlogic.gdx.math.Vector r2 = r9.set(r8)
            float r3 = r5 * r5
            com.badlogic.gdx.math.Vector r2 = r2.scl(r3)
            com.badlogic.gdx.math.Vector r1 = r1.add(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.quadratic(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T quadratic_derivative(T r4, float r5, T r6, T r7, T r8, T r9) {
        /*
            r3 = 1073741824(0x40000000, float:2.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            float r0 = r2 - r5
            com.badlogic.gdx.math.Vector r1 = r4.set(r7)
            com.badlogic.gdx.math.Vector r1 = r1.sub(r6)
            com.badlogic.gdx.math.Vector r1 = r1.scl(r3)
            float r2 = r2 - r5
            com.badlogic.gdx.math.Vector r1 = r1.scl(r2)
            com.badlogic.gdx.math.Vector r2 = r9.set(r8)
            com.badlogic.gdx.math.Vector r2 = r2.sub(r7)
            com.badlogic.gdx.math.Vector r2 = r2.scl(r5)
            com.badlogic.gdx.math.Vector r2 = r2.scl(r3)
            com.badlogic.gdx.math.Vector r1 = r1.add(r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.quadratic_derivative(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T cubic(T r7, float r8, T r9, T r10, T r11, T r12, T r13) {
        /*
            r6 = 1077936128(0x40400000, float:3.0)
            r3 = 1065353216(0x3f800000, float:1.0)
            float r0 = r3 - r8
            float r1 = r0 * r0
            float r2 = r8 * r8
            com.badlogic.gdx.math.Vector r3 = r7.set(r9)
            float r4 = r1 * r0
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            com.badlogic.gdx.math.Vector r4 = r13.set(r10)
            float r5 = r6 * r1
            float r5 = r5 * r8
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            com.badlogic.gdx.math.Vector r3 = r3.add(r4)
            com.badlogic.gdx.math.Vector r4 = r13.set(r11)
            float r5 = r6 * r0
            float r5 = r5 * r2
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            com.badlogic.gdx.math.Vector r3 = r3.add(r4)
            com.badlogic.gdx.math.Vector r4 = r13.set(r12)
            float r5 = r2 * r8
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            com.badlogic.gdx.math.Vector r3 = r3.add(r4)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.cubic(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T cubic_derivative(T r8, float r9, T r10, T r11, T r12, T r13, T r14) {
        /*
            r7 = 1077936128(0x40400000, float:3.0)
            r3 = 1065353216(0x3f800000, float:1.0)
            float r0 = r3 - r9
            float r1 = r0 * r0
            float r2 = r9 * r9
            com.badlogic.gdx.math.Vector r3 = r8.set(r11)
            com.badlogic.gdx.math.Vector r3 = r3.sub(r10)
            float r4 = r1 * r7
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            com.badlogic.gdx.math.Vector r4 = r14.set(r12)
            com.badlogic.gdx.math.Vector r4 = r4.sub(r11)
            float r5 = r0 * r9
            r6 = 1086324736(0x40c00000, float:6.0)
            float r5 = r5 * r6
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            com.badlogic.gdx.math.Vector r3 = r3.add(r4)
            com.badlogic.gdx.math.Vector r4 = r14.set(r13)
            com.badlogic.gdx.math.Vector r4 = r4.sub(r12)
            float r5 = r2 * r7
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            com.badlogic.gdx.math.Vector r3 = r3.add(r4)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.cubic_derivative(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    public Bezier() {
    }

    public Bezier(T... points2) {
        set(points2);
    }

    public Bezier(T[] points2, int offset, int length) {
        set(points2, offset, length);
    }

    public Bezier(Array<T> points2, int offset, int length) {
        set(points2, offset, length);
    }

    public Bezier set(T... points2) {
        return set(points2, 0, points2.length);
    }

    public Bezier set(T[] points2, int offset, int length) {
        if (length < 2 || length > 4) {
            throw new GdxRuntimeException("Only first, second and third degree Bezier curves are supported.");
        }
        if (this.tmp == null) {
            this.tmp = points2[0].cpy();
        }
        if (this.tmp2 == null) {
            this.tmp2 = points2[0].cpy();
        }
        if (this.tmp3 == null) {
            this.tmp3 = points2[0].cpy();
        }
        this.points.clear();
        this.points.addAll(points2, offset, length);
        return this;
    }

    public Bezier set(Array<T> points2, int offset, int length) {
        if (length < 2 || length > 4) {
            throw new GdxRuntimeException("Only first, second and third degree Bezier curves are supported.");
        }
        if (this.tmp == null) {
            this.tmp = ((Vector) points2.get(0)).cpy();
        }
        this.points.clear();
        this.points.addAll(points2, offset, length);
        return this;
    }

    public T valueAt(T out, float t) {
        int n = this.points.size;
        if (n == 2) {
            linear(out, t, (Vector) this.points.get(0), (Vector) this.points.get(1), this.tmp);
        } else if (n == 3) {
            quadratic(out, t, (Vector) this.points.get(0), (Vector) this.points.get(1), (Vector) this.points.get(2), this.tmp);
        } else if (n == 4) {
            cubic(out, t, (Vector) this.points.get(0), (Vector) this.points.get(1), (Vector) this.points.get(2), (Vector) this.points.get(3), this.tmp);
        }
        return out;
    }

    public T derivativeAt(T out, float t) {
        int n = this.points.size;
        if (n == 2) {
            linear_derivative(out, t, (Vector) this.points.get(0), (Vector) this.points.get(1), this.tmp);
        } else if (n == 3) {
            quadratic_derivative(out, t, (Vector) this.points.get(0), (Vector) this.points.get(1), (Vector) this.points.get(2), this.tmp);
        } else if (n == 4) {
            cubic_derivative(out, t, (Vector) this.points.get(0), (Vector) this.points.get(1), (Vector) this.points.get(2), (Vector) this.points.get(3), this.tmp);
        }
        return out;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float approximate(T r12) {
        /*
            r11 = this;
            com.badlogic.gdx.utils.Array<T> r8 = r11.points
            r9 = 0
            java.lang.Object r4 = r8.get(r9)
            com.badlogic.gdx.math.Vector r4 = (com.badlogic.gdx.math.Vector) r4
            com.badlogic.gdx.utils.Array<T> r8 = r11.points
            com.badlogic.gdx.utils.Array<T> r9 = r11.points
            int r9 = r9.size
            int r9 = r9 + -1
            java.lang.Object r5 = r8.get(r9)
            com.badlogic.gdx.math.Vector r5 = (com.badlogic.gdx.math.Vector) r5
            r6 = r12
            float r1 = r4.dst2(r5)
            float r2 = r6.dst2(r5)
            float r3 = r6.dst2(r4)
            double r8 = (double) r1
            double r8 = java.lang.Math.sqrt(r8)
            float r0 = (float) r8
            float r8 = r2 + r1
            float r8 = r8 - r3
            r9 = 1073741824(0x40000000, float:2.0)
            float r9 = r9 * r0
            float r7 = r8 / r9
            float r8 = r0 - r7
            float r8 = r8 / r0
            r9 = 0
            r10 = 1065353216(0x3f800000, float:1.0)
            float r8 = com.badlogic.gdx.math.MathUtils.clamp(r8, r9, r10)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.approximate(com.badlogic.gdx.math.Vector):float");
    }

    public float locate(T v) {
        return approximate((Vector) v);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float approxLength(int r7) {
        /*
            r6 = this;
            r1 = 0
            r0 = 0
        L_0x0002:
            if (r0 < r7) goto L_0x0005
            return r1
        L_0x0005:
            T r2 = r6.tmp2
            T r3 = r6.tmp3
            r2.set(r3)
            T r2 = r6.tmp3
            float r3 = (float) r0
            float r4 = (float) r7
            r5 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r5
            float r3 = r3 / r4
            r6.valueAt(r2, r3)
            if (r0 <= 0) goto L_0x0022
            T r2 = r6.tmp2
            T r3 = r6.tmp3
            float r2 = r2.dst(r3)
            float r1 = r1 + r2
        L_0x0022:
            int r0 = r0 + 1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.Bezier.approxLength(int):float");
    }
}
