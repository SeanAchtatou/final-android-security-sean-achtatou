package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Vector;

public interface Vector<T extends Vector<T>> {
    T add(Vector vector);

    T clamp(float f, float f2);

    T cpy();

    float dot(Vector vector);

    float dst(Vector vector);

    float dst2(Vector vector);

    boolean epsilonEquals(Vector vector, float f);

    boolean hasOppositeDirection(Vector vector);

    boolean hasSameDirection(Vector vector);

    T interpolate(Vector vector, float f, Interpolation interpolation);

    boolean isCollinear(Vector vector);

    boolean isCollinear(Vector vector, float f);

    boolean isCollinearOpposite(Vector vector);

    boolean isCollinearOpposite(Vector vector, float f);

    boolean isOnLine(Vector vector);

    boolean isOnLine(Vector vector, float f);

    boolean isPerpendicular(Vector vector);

    boolean isPerpendicular(Vector vector, float f);

    boolean isUnit();

    boolean isUnit(float f);

    boolean isZero();

    boolean isZero(float f);

    float len();

    float len2();

    T lerp(Vector vector, float f);

    T limit(float f);

    T limit2(float f);

    T mulAdd(Vector vector, float f);

    T mulAdd(Vector vector, Vector vector2);

    T nor();

    T scl(float f);

    T scl(Vector vector);

    T set(Vector vector);

    T setLength(float f);

    T setLength2(float f);

    T setZero();

    T sub(Vector vector);
}
