package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Vector;

public class CatmullRomSpline<T extends Vector<T>> implements Path<T> {
    public boolean continuous;
    public T[] controlPoints;
    public int spanCount;
    private T tmp;
    private T tmp2;
    private T tmp3;

    public static <T extends Vector<T>> T calculate(T out, float t, T[] points, boolean continuous2, T tmp4) {
        int n = continuous2 ? points.length : points.length - 3;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return calculate(out, i, u - ((float) i), points, continuous2, tmp4);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T calculate(T r7, int r8, float r9, T[] r10, boolean r11, T r12) {
        /*
            r6 = 1056964608(0x3f000000, float:0.5)
            int r0 = r10.length
            float r1 = r9 * r9
            float r2 = r1 * r9
            r3 = r10[r8]
            com.badlogic.gdx.math.Vector r3 = r7.set(r3)
            r4 = 1069547520(0x3fc00000, float:1.5)
            float r4 = r4 * r2
            r5 = 1075838976(0x40200000, float:2.5)
            float r5 = r5 * r1
            float r4 = r4 - r5
            r5 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 + r5
            r3.scl(r4)
            if (r11 != 0) goto L_0x001e
            if (r8 <= 0) goto L_0x0037
        L_0x001e:
            int r3 = r0 + r8
            int r3 = r3 + -1
            int r3 = r3 % r0
            r3 = r10[r3]
            com.badlogic.gdx.math.Vector r3 = r12.set(r3)
            r4 = -1090519040(0xffffffffbf000000, float:-0.5)
            float r4 = r4 * r2
            float r4 = r4 + r1
            float r5 = r6 * r9
            float r4 = r4 - r5
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            r7.add(r3)
        L_0x0037:
            if (r11 != 0) goto L_0x003d
            int r3 = r0 + -1
            if (r8 >= r3) goto L_0x0057
        L_0x003d:
            int r3 = r8 + 1
            int r3 = r3 % r0
            r3 = r10[r3]
            com.badlogic.gdx.math.Vector r3 = r12.set(r3)
            r4 = -1077936128(0xffffffffbfc00000, float:-1.5)
            float r4 = r4 * r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r5 * r1
            float r4 = r4 + r5
            float r5 = r6 * r9
            float r4 = r4 + r5
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            r7.add(r3)
        L_0x0057:
            if (r11 != 0) goto L_0x005d
            int r3 = r0 + -2
            if (r8 >= r3) goto L_0x0072
        L_0x005d:
            int r3 = r8 + 2
            int r3 = r3 % r0
            r3 = r10[r3]
            com.badlogic.gdx.math.Vector r3 = r12.set(r3)
            float r4 = r6 * r2
            float r5 = r6 * r1
            float r4 = r4 - r5
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            r7.add(r3)
        L_0x0072:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.CatmullRomSpline.calculate(com.badlogic.gdx.math.Vector, int, float, com.badlogic.gdx.math.Vector[], boolean, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    public static <T extends Vector<T>> T derivative(T out, float t, T[] points, boolean continuous2, T tmp4) {
        int n = continuous2 ? points.length : points.length - 3;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return derivative(out, i, u - ((float) i), points, continuous2, tmp4);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.math.Vector<T>> T derivative(T r7, int r8, float r9, T[] r10, boolean r11, T r12) {
        /*
            r6 = 1083179008(0x40900000, float:4.5)
            r5 = 1069547520(0x3fc00000, float:1.5)
            int r0 = r10.length
            float r1 = r9 * r9
            r2 = r10[r8]
            com.badlogic.gdx.math.Vector r2 = r7.set(r2)
            float r3 = -r9
            r4 = 1084227584(0x40a00000, float:5.0)
            float r3 = r3 * r4
            float r4 = r1 * r6
            float r3 = r3 + r4
            r2.scl(r3)
            if (r11 != 0) goto L_0x001b
            if (r8 <= 0) goto L_0x0036
        L_0x001b:
            int r2 = r0 + r8
            int r2 = r2 + -1
            int r2 = r2 % r0
            r2 = r10[r2]
            com.badlogic.gdx.math.Vector r2 = r12.set(r2)
            r3 = -1090519040(0xffffffffbf000000, float:-0.5)
            r4 = 1073741824(0x40000000, float:2.0)
            float r4 = r4 * r9
            float r3 = r3 + r4
            float r4 = r1 * r5
            float r3 = r3 - r4
            com.badlogic.gdx.math.Vector r2 = r2.scl(r3)
            r7.add(r2)
        L_0x0036:
            if (r11 != 0) goto L_0x003c
            int r2 = r0 + -1
            if (r8 >= r2) goto L_0x0055
        L_0x003c:
            int r2 = r8 + 1
            int r2 = r2 % r0
            r2 = r10[r2]
            com.badlogic.gdx.math.Vector r2 = r12.set(r2)
            r3 = 1056964608(0x3f000000, float:0.5)
            r4 = 1082130432(0x40800000, float:4.0)
            float r4 = r4 * r9
            float r3 = r3 + r4
            float r4 = r1 * r6
            float r3 = r3 - r4
            com.badlogic.gdx.math.Vector r2 = r2.scl(r3)
            r7.add(r2)
        L_0x0055:
            if (r11 != 0) goto L_0x005b
            int r2 = r0 + -2
            if (r8 >= r2) goto L_0x006f
        L_0x005b:
            int r2 = r8 + 2
            int r2 = r2 % r0
            r2 = r10[r2]
            com.badlogic.gdx.math.Vector r2 = r12.set(r2)
            float r3 = -r9
            float r4 = r1 * r5
            float r3 = r3 + r4
            com.badlogic.gdx.math.Vector r2 = r2.scl(r3)
            r7.add(r2)
        L_0x006f:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.CatmullRomSpline.derivative(com.badlogic.gdx.math.Vector, int, float, com.badlogic.gdx.math.Vector[], boolean, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    public CatmullRomSpline() {
    }

    public CatmullRomSpline(T[] controlPoints2, boolean continuous2) {
        set(controlPoints2, continuous2);
    }

    public CatmullRomSpline set(T[] controlPoints2, boolean continuous2) {
        if (this.tmp == null) {
            this.tmp = controlPoints2[0].cpy();
        }
        if (this.tmp2 == null) {
            this.tmp2 = controlPoints2[0].cpy();
        }
        if (this.tmp3 == null) {
            this.tmp3 = controlPoints2[0].cpy();
        }
        this.controlPoints = controlPoints2;
        this.continuous = continuous2;
        this.spanCount = continuous2 ? controlPoints2.length : controlPoints2.length - 3;
        return this;
    }

    public T valueAt(T out, float t) {
        int n = this.spanCount;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return valueAt(out, i, u - ((float) i));
    }

    public T valueAt(T out, int span, float u) {
        return calculate(out, this.continuous ? span : span + 1, u, this.controlPoints, this.continuous, this.tmp);
    }

    public T derivativeAt(T out, float t) {
        int n = this.spanCount;
        float u = t * ((float) n);
        int i = t >= 1.0f ? n - 1 : (int) u;
        return derivativeAt(out, i, u - ((float) i));
    }

    public T derivativeAt(T out, int span, float u) {
        return derivative(out, this.continuous ? span : span + 1, u, this.controlPoints, this.continuous, this.tmp);
    }

    public int nearest(T in) {
        return nearest(in, 0, this.spanCount);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int nearest(T r8, int r9, int r10) {
        /*
            r7 = this;
        L_0x0000:
            if (r9 < 0) goto L_0x0012
            int r5 = r7.spanCount
            int r4 = r9 % r5
            T[] r5 = r7.controlPoints
            r5 = r5[r4]
            float r1 = r8.dst2(r5)
            r2 = 1
        L_0x000f:
            if (r2 < r10) goto L_0x0016
            return r4
        L_0x0012:
            int r5 = r7.spanCount
            int r9 = r9 + r5
            goto L_0x0000
        L_0x0016:
            int r5 = r9 + r2
            int r6 = r7.spanCount
            int r3 = r5 % r6
            T[] r5 = r7.controlPoints
            r5 = r5[r3]
            float r0 = r8.dst2(r5)
            int r5 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r5 >= 0) goto L_0x002a
            r1 = r0
            r4 = r3
        L_0x002a:
            int r2 = r2 + 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.CatmullRomSpline.nearest(com.badlogic.gdx.math.Vector, int, int):int");
    }

    public float approximate(T v) {
        return approximate(v, nearest(v));
    }

    public float approximate(T in, int start, int count) {
        return approximate(in, nearest(in, start, count));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float approximate(T r21, int r22) {
        /*
            r20 = this;
            r11 = r22
            r0 = r20
            T[] r0 = r0.controlPoints
            r17 = r0
            r12 = r17[r11]
            r0 = r20
            T[] r0 = r0.controlPoints
            r18 = r0
            if (r11 <= 0) goto L_0x007b
            int r17 = r11 + -1
        L_0x0014:
            r14 = r18[r17]
            r0 = r20
            T[] r0 = r0.controlPoints
            r17 = r0
            int r18 = r11 + 1
            r0 = r20
            int r0 = r0.spanCount
            r19 = r0
            int r18 = r18 % r19
            r13 = r17[r18]
            r0 = r21
            float r10 = r0.dst2(r14)
            r0 = r21
            float r9 = r0.dst2(r13)
            int r17 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r17 >= 0) goto L_0x0084
            r6 = r12
            r7 = r13
            r8 = r21
        L_0x003c:
            float r3 = r6.dst2(r7)
            float r4 = r8.dst2(r7)
            float r5 = r8.dst2(r6)
            double r0 = (double) r3
            r18 = r0
            double r18 = java.lang.Math.sqrt(r18)
            r0 = r18
            float r2 = (float) r0
            float r17 = r4 + r3
            float r17 = r17 - r5
            r18 = 1073741824(0x40000000, float:2.0)
            float r18 = r18 * r2
            float r15 = r17 / r18
            float r17 = r2 - r15
            float r17 = r17 / r2
            r18 = 0
            r19 = 1065353216(0x3f800000, float:1.0)
            float r16 = com.badlogic.gdx.math.MathUtils.clamp(r17, r18, r19)
            float r0 = (float) r11
            r17 = r0
            float r17 = r17 + r16
            r0 = r20
            int r0 = r0.spanCount
            r18 = r0
            r0 = r18
            float r0 = (float) r0
            r18 = r0
            float r17 = r17 / r18
            return r17
        L_0x007b:
            r0 = r20
            int r0 = r0.spanCount
            r17 = r0
            int r17 = r17 + -1
            goto L_0x0014
        L_0x0084:
            r6 = r14
            r7 = r12
            r8 = r21
            if (r11 <= 0) goto L_0x008d
            int r11 = r11 + -1
        L_0x008c:
            goto L_0x003c
        L_0x008d:
            r0 = r20
            int r0 = r0.spanCount
            r17 = r0
            int r11 = r17 + -1
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.CatmullRomSpline.approximate(com.badlogic.gdx.math.Vector, int):float");
    }

    public float locate(T v) {
        return approximate((Vector) v);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float approxLength(int r7) {
        /*
            r6 = this;
            r1 = 0
            r0 = 0
        L_0x0002:
            if (r0 < r7) goto L_0x0005
            return r1
        L_0x0005:
            T r2 = r6.tmp2
            T r3 = r6.tmp3
            r2.set(r3)
            T r2 = r6.tmp3
            float r3 = (float) r0
            float r4 = (float) r7
            r5 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r5
            float r3 = r3 / r4
            r6.valueAt(r2, r3)
            if (r0 <= 0) goto L_0x0022
            T r2 = r6.tmp2
            T r3 = r6.tmp3
            float r2 = r2.dst(r3)
            float r1 = r1 + r2
        L_0x0022:
            int r0 = r0 + 1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.math.CatmullRomSpline.approxLength(int):float");
    }
}
