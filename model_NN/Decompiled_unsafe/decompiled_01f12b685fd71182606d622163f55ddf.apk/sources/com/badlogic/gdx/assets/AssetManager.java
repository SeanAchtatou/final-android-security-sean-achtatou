package com.badlogic.gdx.assets;

import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.I18NBundleLoader;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.PixmapLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonRegionLoader;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.UBJsonReader;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.ThreadUtils;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import java.util.Iterator;
import java.util.Stack;

public class AssetManager implements Disposable {
    final ObjectMap<String, Array<String>> assetDependencies;
    final ObjectMap<String, Class> assetTypes;
    final ObjectMap<Class, ObjectMap<String, RefCountedContainer>> assets;
    final AsyncExecutor executor;
    final ObjectSet<String> injected;
    AssetErrorListener listener;
    final Array<AssetDescriptor> loadQueue;
    int loaded;
    final ObjectMap<Class, ObjectMap<String, AssetLoader>> loaders;
    Logger log;
    final Stack<AssetLoadingTask> tasks;
    int toLoad;

    public AssetManager() {
        this(new InternalFileHandleResolver());
    }

    public AssetManager(FileHandleResolver resolver) {
        this.assets = new ObjectMap<>();
        this.assetTypes = new ObjectMap<>();
        this.assetDependencies = new ObjectMap<>();
        this.injected = new ObjectSet<>();
        this.loaders = new ObjectMap<>();
        this.loadQueue = new Array<>();
        this.tasks = new Stack<>();
        this.listener = null;
        this.loaded = 0;
        this.toLoad = 0;
        this.log = new Logger("AssetManager", 0);
        setLoader(BitmapFont.class, new BitmapFontLoader(resolver));
        setLoader(Music.class, new MusicLoader(resolver));
        setLoader(Pixmap.class, new PixmapLoader(resolver));
        setLoader(Sound.class, new SoundLoader(resolver));
        setLoader(TextureAtlas.class, new TextureAtlasLoader(resolver));
        setLoader(Texture.class, new TextureLoader(resolver));
        setLoader(Skin.class, new SkinLoader(resolver));
        setLoader(ParticleEffect.class, new ParticleEffectLoader(resolver));
        setLoader(com.badlogic.gdx.graphics.g3d.particles.ParticleEffect.class, new com.badlogic.gdx.graphics.g3d.particles.ParticleEffectLoader(resolver));
        setLoader(PolygonRegion.class, new PolygonRegionLoader(resolver));
        setLoader(I18NBundle.class, new I18NBundleLoader(resolver));
        setLoader(Model.class, ".g3dj", new G3dModelLoader(new JsonReader(), resolver));
        setLoader(Model.class, ".g3db", new G3dModelLoader(new UBJsonReader(), resolver));
        setLoader(Model.class, ".obj", new ObjLoader(resolver));
        this.executor = new AsyncExecutor(1);
    }

    public synchronized <T> T get(String fileName) {
        T asset;
        Class<T> type = this.assetTypes.get(fileName);
        if (type == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        ObjectMap<String, RefCountedContainer> assetsByType = this.assets.get(type);
        if (assetsByType == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        RefCountedContainer assetContainer = (RefCountedContainer) assetsByType.get(fileName);
        if (assetContainer == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        asset = assetContainer.getObject(type);
        if (asset == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        return asset;
    }

    public synchronized <T> T get(String fileName, Class<T> type) {
        T asset;
        ObjectMap<String, RefCountedContainer> assetsByType = this.assets.get(type);
        if (assetsByType == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        RefCountedContainer assetContainer = (RefCountedContainer) assetsByType.get(fileName);
        if (assetContainer == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        asset = assetContainer.getObject(type);
        if (asset == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        return asset;
    }

    public synchronized <T> Array<T> getAll(Class<T> type, Array<T> out) {
        ObjectMap<String, RefCountedContainer> assetsByType = this.assets.get(type);
        if (assetsByType != null) {
            Iterator it = assetsByType.entries().iterator();
            while (it.hasNext()) {
                out.add(((RefCountedContainer) ((ObjectMap.Entry) it.next()).value).getObject(type));
            }
        }
        return out;
    }

    public synchronized <T> T get(AssetDescriptor<T> assetDescriptor) {
        return get(assetDescriptor.fileName, assetDescriptor.type);
    }

    public synchronized void unload(String fileName) {
        if (this.tasks.size() > 0) {
            AssetLoadingTask currAsset = this.tasks.firstElement();
            if (currAsset.assetDesc.fileName.equals(fileName)) {
                currAsset.cancel = true;
                this.log.debug("Unload (from tasks): " + fileName);
            }
        }
        int foundIndex = -1;
        int i = 0;
        while (true) {
            if (i >= this.loadQueue.size) {
                break;
            } else if (this.loadQueue.get(i).fileName.equals(fileName)) {
                foundIndex = i;
                break;
            } else {
                i++;
            }
        }
        if (foundIndex != -1) {
            this.toLoad--;
            this.loadQueue.removeIndex(foundIndex);
            this.log.debug("Unload (from queue): " + fileName);
        } else {
            Class type = this.assetTypes.get(fileName);
            if (type == null) {
                throw new GdxRuntimeException("Asset not loaded: " + fileName);
            }
            RefCountedContainer assetRef = (RefCountedContainer) this.assets.get(type).get(fileName);
            assetRef.decRefCount();
            if (assetRef.getRefCount() <= 0) {
                this.log.debug("Unload (dispose): " + fileName);
                if (assetRef.getObject(Object.class) instanceof Disposable) {
                    ((Disposable) assetRef.getObject(Object.class)).dispose();
                }
                this.assetTypes.remove(fileName);
                this.assets.get(type).remove(fileName);
            } else {
                this.log.debug("Unload (decrement): " + fileName);
            }
            Array<String> dependencies = this.assetDependencies.get(fileName);
            if (dependencies != null) {
                Iterator it = dependencies.iterator();
                while (it.hasNext()) {
                    String dependency = (String) it.next();
                    if (isLoaded(dependency)) {
                        unload(dependency);
                    }
                }
            }
            if (assetRef.getRefCount() <= 0) {
                this.assetDependencies.remove(fileName);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0021 A[SYNTHETIC] */
    public synchronized <T> boolean containsAsset(T r8) {
        /*
            r7 = this;
            r4 = 0
            monitor-enter(r7)
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.ObjectMap<java.lang.String, com.badlogic.gdx.assets.RefCountedContainer>> r3 = r7.assets     // Catch:{ all -> 0x003f }
            java.lang.Class r5 = r8.getClass()     // Catch:{ all -> 0x003f }
            java.lang.Object r2 = r3.get(r5)     // Catch:{ all -> 0x003f }
            com.badlogic.gdx.utils.ObjectMap r2 = (com.badlogic.gdx.utils.ObjectMap) r2     // Catch:{ all -> 0x003f }
            if (r2 != 0) goto L_0x0013
            r3 = r4
        L_0x0011:
            monitor-exit(r7)
            return r3
        L_0x0013:
            com.badlogic.gdx.utils.ObjectMap$Keys r3 = r2.keys()     // Catch:{ all -> 0x003f }
            java.util.Iterator r5 = r3.iterator()     // Catch:{ all -> 0x003f }
        L_0x001b:
            boolean r3 = r5.hasNext()     // Catch:{ all -> 0x003f }
            if (r3 != 0) goto L_0x0023
            r3 = r4
            goto L_0x0011
        L_0x0023:
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x003f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x003f }
            java.lang.Object r3 = r2.get(r0)     // Catch:{ all -> 0x003f }
            com.badlogic.gdx.assets.RefCountedContainer r3 = (com.badlogic.gdx.assets.RefCountedContainer) r3     // Catch:{ all -> 0x003f }
            java.lang.Class<java.lang.Object> r6 = java.lang.Object.class
            java.lang.Object r1 = r3.getObject(r6)     // Catch:{ all -> 0x003f }
            if (r1 == r8) goto L_0x003d
            boolean r3 = r8.equals(r1)     // Catch:{ all -> 0x003f }
            if (r3 == 0) goto L_0x001b
        L_0x003d:
            r3 = 1
            goto L_0x0011
        L_0x003f:
            r3 = move-exception
            monitor-exit(r7)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.assets.AssetManager.containsAsset(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public synchronized <T> java.lang.String getAssetFileName(T r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.ObjectMap<java.lang.String, com.badlogic.gdx.assets.RefCountedContainer>> r4 = r8.assets     // Catch:{ all -> 0x004b }
            com.badlogic.gdx.utils.ObjectMap$Keys r4 = r4.keys()     // Catch:{ all -> 0x004b }
            java.util.Iterator r5 = r4.iterator()     // Catch:{ all -> 0x004b }
        L_0x000b:
            boolean r4 = r5.hasNext()     // Catch:{ all -> 0x004b }
            if (r4 != 0) goto L_0x0014
            r1 = 0
        L_0x0012:
            monitor-exit(r8)
            return r1
        L_0x0014:
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x004b }
            java.lang.Class r0 = (java.lang.Class) r0     // Catch:{ all -> 0x004b }
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.ObjectMap<java.lang.String, com.badlogic.gdx.assets.RefCountedContainer>> r4 = r8.assets     // Catch:{ all -> 0x004b }
            java.lang.Object r3 = r4.get(r0)     // Catch:{ all -> 0x004b }
            com.badlogic.gdx.utils.ObjectMap r3 = (com.badlogic.gdx.utils.ObjectMap) r3     // Catch:{ all -> 0x004b }
            com.badlogic.gdx.utils.ObjectMap$Keys r4 = r3.keys()     // Catch:{ all -> 0x004b }
            java.util.Iterator r6 = r4.iterator()     // Catch:{ all -> 0x004b }
        L_0x002a:
            boolean r4 = r6.hasNext()     // Catch:{ all -> 0x004b }
            if (r4 == 0) goto L_0x000b
            java.lang.Object r1 = r6.next()     // Catch:{ all -> 0x004b }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x004b }
            java.lang.Object r4 = r3.get(r1)     // Catch:{ all -> 0x004b }
            com.badlogic.gdx.assets.RefCountedContainer r4 = (com.badlogic.gdx.assets.RefCountedContainer) r4     // Catch:{ all -> 0x004b }
            java.lang.Class<java.lang.Object> r7 = java.lang.Object.class
            java.lang.Object r2 = r4.getObject(r7)     // Catch:{ all -> 0x004b }
            if (r2 == r9) goto L_0x0012
            boolean r4 = r9.equals(r2)     // Catch:{ all -> 0x004b }
            if (r4 == 0) goto L_0x002a
            goto L_0x0012
        L_0x004b:
            r4 = move-exception
            monitor-exit(r8)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.assets.AssetManager.getAssetFileName(java.lang.Object):java.lang.String");
    }

    public synchronized boolean isLoaded(String fileName) {
        boolean containsKey;
        if (fileName == null) {
            containsKey = false;
        } else {
            containsKey = this.assetTypes.containsKey(fileName);
        }
        return containsKey;
    }

    public synchronized boolean isLoaded(String fileName, Class type) {
        boolean z = false;
        synchronized (this) {
            ObjectMap<String, RefCountedContainer> assetsByType = this.assets.get(type);
            if (assetsByType != null) {
                RefCountedContainer assetContainer = (RefCountedContainer) assetsByType.get(fileName);
                if (!(assetContainer == null || assetContainer.getObject(type) == null)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public <T> AssetLoader getLoader(Class<T> type) {
        return getLoader(type, null);
    }

    public <T> AssetLoader getLoader(Class<T> type, String fileName) {
        ObjectMap<String, AssetLoader> loaders2 = this.loaders.get(type);
        if (loaders2 == null || loaders2.size < 1) {
            return null;
        }
        if (fileName == null) {
            return (AssetLoader) loaders2.get("");
        }
        AssetLoader result = null;
        int l = -1;
        Iterator it = loaders2.entries().iterator();
        while (it.hasNext()) {
            ObjectMap.Entry<String, AssetLoader> entry = (ObjectMap.Entry) it.next();
            if (((String) entry.key).length() > l && fileName.endsWith((String) entry.key)) {
                result = entry.value;
                l = ((String) entry.key).length();
            }
        }
        return result;
    }

    public synchronized <T> void load(String fileName, Class<T> type) {
        load(fileName, type, null);
    }

    public synchronized <T> void load(String fileName, Class<T> type, AssetLoaderParameters<T> parameter) {
        if (getLoader(type, fileName) == null) {
            throw new GdxRuntimeException("No loader for type: " + ClassReflection.getSimpleName(type));
        }
        if (this.loadQueue.size == 0) {
            this.loaded = 0;
            this.toLoad = 0;
        }
        int i = 0;
        while (i < this.loadQueue.size) {
            AssetDescriptor desc = this.loadQueue.get(i);
            if (!desc.fileName.equals(fileName) || desc.type.equals(type)) {
                i++;
            } else {
                throw new GdxRuntimeException("Asset with name '" + fileName + "' already in preload queue, but has different type (expected: " + ClassReflection.getSimpleName(type) + ", found: " + ClassReflection.getSimpleName(desc.type) + ")");
            }
        }
        int i2 = 0;
        while (i2 < this.tasks.size()) {
            AssetDescriptor desc2 = this.tasks.get(i2).assetDesc;
            if (!desc2.fileName.equals(fileName) || desc2.type.equals(type)) {
                i2++;
            } else {
                throw new GdxRuntimeException("Asset with name '" + fileName + "' already in task list, but has different type (expected: " + ClassReflection.getSimpleName(type) + ", found: " + ClassReflection.getSimpleName(desc2.type) + ")");
            }
        }
        Class otherType = this.assetTypes.get(fileName);
        if (otherType == null || otherType.equals(type)) {
            this.toLoad++;
            AssetDescriptor assetDesc = new AssetDescriptor(fileName, type, parameter);
            this.loadQueue.add(assetDesc);
            this.log.debug("Queued: " + assetDesc);
        } else {
            throw new GdxRuntimeException("Asset with name '" + fileName + "' already loaded, but has different type (expected: " + ClassReflection.getSimpleName(type) + ", found: " + ClassReflection.getSimpleName(otherType) + ")");
        }
    }

    public synchronized void load(AssetDescriptor desc) {
        load(desc.fileName, desc.type, desc.params);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        if (r4.tasks.size() == 0) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean update() {
        /*
            r4 = this;
            r2 = 0
            r1 = 1
            monitor-enter(r4)
            java.util.Stack<com.badlogic.gdx.assets.AssetLoadingTask> r3 = r4.tasks     // Catch:{ Throwable -> 0x0027 }
            int r3 = r3.size()     // Catch:{ Throwable -> 0x0027 }
            if (r3 != 0) goto L_0x0033
        L_0x000b:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.assets.AssetDescriptor> r3 = r4.loadQueue     // Catch:{ Throwable -> 0x0027 }
            int r3 = r3.size     // Catch:{ Throwable -> 0x0027 }
            if (r3 == 0) goto L_0x0019
            java.util.Stack<com.badlogic.gdx.assets.AssetLoadingTask> r3 = r4.tasks     // Catch:{ Throwable -> 0x0027 }
            int r3 = r3.size()     // Catch:{ Throwable -> 0x0027 }
            if (r3 == 0) goto L_0x0023
        L_0x0019:
            java.util.Stack<com.badlogic.gdx.assets.AssetLoadingTask> r3 = r4.tasks     // Catch:{ Throwable -> 0x0027 }
            int r3 = r3.size()     // Catch:{ Throwable -> 0x0027 }
            if (r3 != 0) goto L_0x0033
        L_0x0021:
            monitor-exit(r4)
            return r1
        L_0x0023:
            r4.nextTask()     // Catch:{ Throwable -> 0x0027 }
            goto L_0x000b
        L_0x0027:
            r0 = move-exception
            r4.handleTaskError(r0)     // Catch:{ all -> 0x0049 }
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.assets.AssetDescriptor> r3 = r4.loadQueue     // Catch:{ all -> 0x0049 }
            int r3 = r3.size     // Catch:{ all -> 0x0049 }
            if (r3 == 0) goto L_0x0021
            r1 = r2
            goto L_0x0021
        L_0x0033:
            boolean r3 = r4.updateTask()     // Catch:{ Throwable -> 0x0027 }
            if (r3 == 0) goto L_0x0047
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.assets.AssetDescriptor> r3 = r4.loadQueue     // Catch:{ Throwable -> 0x0027 }
            int r3 = r3.size     // Catch:{ Throwable -> 0x0027 }
            if (r3 != 0) goto L_0x0047
            java.util.Stack<com.badlogic.gdx.assets.AssetLoadingTask> r3 = r4.tasks     // Catch:{ Throwable -> 0x0027 }
            int r3 = r3.size()     // Catch:{ Throwable -> 0x0027 }
            if (r3 == 0) goto L_0x0021
        L_0x0047:
            r1 = r2
            goto L_0x0021
        L_0x0049:
            r1 = move-exception
            monitor-exit(r4)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.assets.AssetManager.update():boolean");
    }

    public boolean update(int millis) {
        boolean done;
        long endTime = TimeUtils.millis() + ((long) millis);
        while (true) {
            done = update();
            if (done || TimeUtils.millis() > endTime) {
                return done;
            }
            ThreadUtils.yield();
        }
        return done;
    }

    public void finishLoading() {
        this.log.debug("Waiting for loading to complete...");
        while (!update()) {
            ThreadUtils.yield();
        }
        this.log.debug("Loading complete.");
    }

    public void finishLoadingAsset(String fileName) {
        this.log.debug("Waiting for asset to be loaded: " + fileName);
        while (!isLoaded(fileName)) {
            update();
            ThreadUtils.yield();
        }
        this.log.debug("Asset loaded: " + fileName);
    }

    /* access modifiers changed from: package-private */
    public synchronized void injectDependencies(String parentAssetFilename, Array<AssetDescriptor> dependendAssetDescs) {
        ObjectSet<String> injected2 = this.injected;
        Iterator<AssetDescriptor> it = dependendAssetDescs.iterator();
        while (it.hasNext()) {
            AssetDescriptor desc = it.next();
            if (!injected2.contains(desc.fileName)) {
                injected2.add(desc.fileName);
                injectDependency(parentAssetFilename, desc);
            }
        }
        injected2.clear();
    }

    private synchronized void injectDependency(String parentAssetFilename, AssetDescriptor dependendAssetDesc) {
        Array<String> dependencies = this.assetDependencies.get(parentAssetFilename);
        if (dependencies == null) {
            dependencies = new Array<>();
            this.assetDependencies.put(parentAssetFilename, dependencies);
        }
        dependencies.add(dependendAssetDesc.fileName);
        if (isLoaded(dependendAssetDesc.fileName)) {
            this.log.debug("Dependency already loaded: " + dependendAssetDesc);
            ((RefCountedContainer) this.assets.get(this.assetTypes.get(dependendAssetDesc.fileName)).get(dependendAssetDesc.fileName)).incRefCount();
            incrementRefCountedDependencies(dependendAssetDesc.fileName);
        } else {
            this.log.info("Loading dependency: " + dependendAssetDesc);
            addTask(dependendAssetDesc);
        }
    }

    private void nextTask() {
        AssetDescriptor assetDesc = this.loadQueue.removeIndex(0);
        if (isLoaded(assetDesc.fileName)) {
            this.log.debug("Already loaded: " + assetDesc);
            ((RefCountedContainer) this.assets.get(this.assetTypes.get(assetDesc.fileName)).get(assetDesc.fileName)).incRefCount();
            incrementRefCountedDependencies(assetDesc.fileName);
            if (!(assetDesc.params == null || assetDesc.params.loadedCallback == null)) {
                assetDesc.params.loadedCallback.finishedLoading(this, assetDesc.fileName, assetDesc.type);
            }
            this.loaded++;
            return;
        }
        this.log.info("Loading: " + assetDesc);
        addTask(assetDesc);
    }

    private void addTask(AssetDescriptor assetDesc) {
        AssetLoader loader = getLoader(assetDesc.type, assetDesc.fileName);
        if (loader == null) {
            throw new GdxRuntimeException("No loader for type: " + ClassReflection.getSimpleName(assetDesc.type));
        }
        this.tasks.push(new AssetLoadingTask(this, assetDesc, loader, this.executor));
    }

    /* access modifiers changed from: protected */
    public <T> void addAsset(String fileName, Class<T> type, T asset) {
        this.assetTypes.put(fileName, type);
        ObjectMap<String, RefCountedContainer> typeToAssets = this.assets.get(type);
        if (typeToAssets == null) {
            typeToAssets = new ObjectMap<>();
            this.assets.put(type, typeToAssets);
        }
        typeToAssets.put(fileName, new RefCountedContainer(asset));
    }

    private boolean updateTask() {
        AssetLoadingTask task = this.tasks.peek();
        if (!task.cancel && !task.update()) {
            return false;
        }
        if (this.tasks.size() == 1) {
            this.loaded++;
        }
        this.tasks.pop();
        if (task.cancel) {
            return true;
        }
        addAsset(task.assetDesc.fileName, task.assetDesc.type, task.getAsset());
        if (!(task.assetDesc.params == null || task.assetDesc.params.loadedCallback == null)) {
            task.assetDesc.params.loadedCallback.finishedLoading(this, task.assetDesc.fileName, task.assetDesc.type);
        }
        this.log.debug("Loaded: " + (((float) (TimeUtils.nanoTime() - task.startTime)) / 1000000.0f) + "ms " + task.assetDesc);
        return true;
    }

    private void incrementRefCountedDependencies(String parent) {
        Array<String> dependencies = this.assetDependencies.get(parent);
        if (dependencies != null) {
            Iterator it = dependencies.iterator();
            while (it.hasNext()) {
                String dependency = (String) it.next();
                ((RefCountedContainer) this.assets.get(this.assetTypes.get(dependency)).get(dependency)).incRefCount();
                incrementRefCountedDependencies(dependency);
            }
        }
    }

    private void handleTaskError(Throwable t) {
        this.log.error("Error loading asset.", t);
        if (this.tasks.isEmpty()) {
            throw new GdxRuntimeException(t);
        }
        AssetLoadingTask task = this.tasks.pop();
        AssetDescriptor assetDesc = task.assetDesc;
        if (task.dependenciesLoaded && task.dependencies != null) {
            Iterator<AssetDescriptor> it = task.dependencies.iterator();
            while (it.hasNext()) {
                unload(it.next().fileName);
            }
        }
        this.tasks.clear();
        if (this.listener != null) {
            this.listener.error(assetDesc, t);
            return;
        }
        throw new GdxRuntimeException(t);
    }

    public synchronized <T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, AssetLoader<T, P> loader) {
        setLoader(type, null, loader);
    }

    public synchronized <T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, String suffix, AssetLoader<T, P> loader) {
        if (type == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (loader == null) {
            throw new IllegalArgumentException("loader cannot be null.");
        } else {
            this.log.debug("Loader set: " + ClassReflection.getSimpleName(type) + " -> " + ClassReflection.getSimpleName(loader.getClass()));
            ObjectMap<String, AssetLoader> loaders2 = this.loaders.get(type);
            if (loaders2 == null) {
                ObjectMap<Class, ObjectMap<String, AssetLoader>> objectMap = this.loaders;
                loaders2 = new ObjectMap<>();
                objectMap.put(type, loaders2);
            }
            if (suffix == null) {
                suffix = "";
            }
            loaders2.put(suffix, loader);
        }
    }

    public synchronized int getLoadedAssets() {
        return this.assetTypes.size;
    }

    public synchronized int getQueuedAssets() {
        return this.loadQueue.size + this.tasks.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public synchronized float getProgress() {
        float f = 1.0f;
        synchronized (this) {
            if (this.toLoad != 0) {
                f = Math.min(1.0f, ((float) this.loaded) / ((float) this.toLoad));
            }
        }
        return f;
    }

    public synchronized void setErrorListener(AssetErrorListener listener2) {
        this.listener = listener2;
    }

    public synchronized void dispose() {
        this.log.debug("Disposing.");
        clear();
        this.executor.dispose();
    }

    public synchronized void clear() {
        this.loadQueue.clear();
        do {
        } while (!update());
        ObjectIntMap<String> dependencyCount = new ObjectIntMap<>();
        while (this.assetTypes.size > 0) {
            dependencyCount.clear();
            Array<String> assets2 = this.assetTypes.keys().toArray();
            Iterator<String> it = assets2.iterator();
            while (it.hasNext()) {
                dependencyCount.put(it.next(), 0);
            }
            Iterator<String> it2 = assets2.iterator();
            while (it2.hasNext()) {
                Array<String> dependencies = this.assetDependencies.get(it2.next());
                if (dependencies != null) {
                    Iterator it3 = dependencies.iterator();
                    while (it3.hasNext()) {
                        String dependency = (String) it3.next();
                        dependencyCount.put(dependency, dependencyCount.get(dependency, 0) + 1);
                    }
                }
            }
            Iterator<String> it4 = assets2.iterator();
            while (it4.hasNext()) {
                String asset = it4.next();
                if (dependencyCount.get(asset, 0) == 0) {
                    unload(asset);
                }
            }
        }
        this.assets.clear();
        this.assetTypes.clear();
        this.assetDependencies.clear();
        this.loaded = 0;
        this.toLoad = 0;
        this.loadQueue.clear();
        this.tasks.clear();
    }

    public Logger getLogger() {
        return this.log;
    }

    public void setLogger(Logger logger) {
        this.log = logger;
    }

    public synchronized int getReferenceCount(String fileName) {
        Class type;
        type = this.assetTypes.get(fileName);
        if (type == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        return ((RefCountedContainer) this.assets.get(type).get(fileName)).getRefCount();
    }

    public synchronized void setReferenceCount(String fileName, int refCount) {
        Class type = this.assetTypes.get(fileName);
        if (type == null) {
            throw new GdxRuntimeException("Asset not loaded: " + fileName);
        }
        ((RefCountedContainer) this.assets.get(type).get(fileName)).setRefCount(refCount);
    }

    public synchronized String getDiagnostics() {
        StringBuffer buffer;
        buffer = new StringBuffer();
        Iterator it = this.assetTypes.keys().iterator();
        while (it.hasNext()) {
            String fileName = (String) it.next();
            buffer.append(fileName);
            buffer.append(", ");
            Class type = this.assetTypes.get(fileName);
            Array<String> dependencies = this.assetDependencies.get(fileName);
            buffer.append(ClassReflection.getSimpleName(type));
            buffer.append(", refs: ");
            buffer.append(((RefCountedContainer) this.assets.get(type).get(fileName)).getRefCount());
            if (dependencies != null) {
                buffer.append(", deps: [");
                Iterator it2 = dependencies.iterator();
                while (it2.hasNext()) {
                    buffer.append((String) it2.next());
                    buffer.append(",");
                }
                buffer.append("]");
            }
            buffer.append("\n");
        }
        return buffer.toString();
    }

    public synchronized Array<String> getAssetNames() {
        return this.assetTypes.keys().toArray();
    }

    public synchronized Array<String> getDependencies(String fileName) {
        return this.assetDependencies.get(fileName);
    }

    public synchronized Class getAssetType(String fileName) {
        return this.assetTypes.get(fileName);
    }
}
