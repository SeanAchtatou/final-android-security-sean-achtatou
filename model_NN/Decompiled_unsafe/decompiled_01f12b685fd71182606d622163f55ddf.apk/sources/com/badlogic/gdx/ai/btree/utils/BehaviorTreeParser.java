package com.badlogic.gdx.ai.btree.utils;

import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.Metadata;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.ai.btree.branch.Parallel;
import com.badlogic.gdx.ai.btree.branch.Selector;
import com.badlogic.gdx.ai.btree.branch.Sequence;
import com.badlogic.gdx.ai.btree.decorator.AlwaysFail;
import com.badlogic.gdx.ai.btree.decorator.AlwaysSucceed;
import com.badlogic.gdx.ai.btree.decorator.Include;
import com.badlogic.gdx.ai.btree.decorator.Invert;
import com.badlogic.gdx.ai.btree.decorator.UntilFail;
import com.badlogic.gdx.ai.btree.decorator.UntilSuccess;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import java.io.InputStream;
import java.io.Reader;

public class BehaviorTreeParser<E> {
    public static final int DEBUG_HIGH = 2;
    public static final int DEBUG_LOW = 1;
    public static final int DEBUG_NONE = 0;
    private ConcreteBehaviorTreeReader<E> btReader;
    public int debug;

    public BehaviorTreeParser() {
        this(0);
    }

    public BehaviorTreeParser(int debug2) {
        this.debug = debug2;
        this.btReader = new ConcreteBehaviorTreeReader<>(this);
    }

    public BehaviorTree<E> parse(String string, Object obj) {
        this.btReader.parse(string);
        return createBehaviorTree(this.btReader.root, obj);
    }

    public BehaviorTree<E> parse(InputStream input, Object obj) {
        this.btReader.parse(input);
        return createBehaviorTree(this.btReader.root, obj);
    }

    public BehaviorTree<E> parse(FileHandle file, Object obj) {
        this.btReader.parse(file);
        return createBehaviorTree(this.btReader.root, obj);
    }

    public BehaviorTree<E> parse(Reader reader, Object obj) {
        this.btReader.parse(reader);
        return createBehaviorTree(this.btReader.root, obj);
    }

    /* access modifiers changed from: protected */
    public BehaviorTree<E> createBehaviorTree(Task<E> root, E object) {
        if (this.debug > 1) {
            printTree(root, 0);
        }
        return new BehaviorTree<>(root, object);
    }

    /* access modifiers changed from: protected */
    public void printTree(Task<E> task, int indent) {
        for (int i = 0; i < indent; i++) {
            System.out.print(' ');
        }
        System.out.println(task.getClass().getSimpleName());
        for (int i2 = 0; i2 < task.getChildCount(); i2++) {
            printTree(task.getChild(i2), indent + 2);
        }
    }

    static class ConcreteBehaviorTreeReader<E> extends BehaviorTreeReader {
        private static final ObjectMap<String, String> DEFAULT_IMPORTS = new ObjectMap<>();
        private static final String[] STATEMENTS = {"import", "root"};
        private static final int TAG_IMPORT = 0;
        private static final int TAG_NONE = -1;
        private static final int TAG_ROOT = 1;
        BehaviorTreeParser<E> btParser;
        int currentDepth;
        boolean isTask;
        StackedTask<E> prevTask;
        Task<E> root;
        int rootIndent;
        Array<StackedTask<E>> stack = new Array<>();
        int step;
        int tagType;
        ObjectMap<String, String> userImports = new ObjectMap<>();

        static {
            for (Class<?> c : new Class[]{AlwaysFail.class, AlwaysSucceed.class, Include.class, Invert.class, Parallel.class, Selector.class, Sequence.class, UntilFail.class, UntilSuccess.class}) {
                String fqcn = c.getName();
                String cn2 = c.getSimpleName();
                DEFAULT_IMPORTS.put(Character.toLowerCase(cn2.charAt(0)) + (cn2.length() > 1 ? cn2.substring(1) : ""), fqcn);
            }
        }

        ConcreteBehaviorTreeReader(BehaviorTreeParser<E> btParser2) {
            this.btParser = btParser2;
        }

        public void parse(char[] data, int offset, int length) {
            this.debug = this.btParser.debug > 0;
            this.tagType = -1;
            this.isTask = false;
            this.userImports.clear();
            this.root = null;
            this.prevTask = null;
            this.currentDepth = -1;
            this.step = 1;
            this.stack.clear();
            super.parse(data, offset, length);
            popAndCheckMinChildren(0);
            if (this.root == null) {
                throw new GdxRuntimeException("The tree must have at least the task");
            }
        }

        /* access modifiers changed from: protected */
        public void startStatement(int indent, String name) {
            if (this.btParser.debug > 1) {
                System.out.println(this.lineNumber + ": <" + indent + "> task name '" + name + "'");
            }
            if (this.tagType == 1) {
                openTask(indent, name);
                return;
            }
            boolean validStatement = openTag(name);
            if (!validStatement) {
                if (this.btParser.debug > 1) {
                    System.out.println("validStatement: " + validStatement);
                    System.out.println("getImport(name): " + getImport(name));
                }
                if (getImport(name) != null) {
                    this.tagType = 1;
                    openTask(indent, name);
                    return;
                }
                throw new GdxRuntimeException("Unknown tag '" + name + "'");
            }
        }

        /* access modifiers changed from: protected */
        public void attribute(String name, Object value) {
            if (this.btParser.debug > 1) {
                System.out.println(this.lineNumber + ": attribute '" + name + " : " + value + "'");
            }
            if (this.isTask) {
                if (!attributeTask(name, value)) {
                    throw new GdxRuntimeException(this.prevTask.name + ": unknown attribute '" + name + "'");
                }
            } else if (!attributeTag(name, value)) {
                throw new GdxRuntimeException(STATEMENTS[this.tagType] + ": unknown attribute '" + name + "'");
            }
        }

        private boolean attributeTask(String name, Object value) {
            if (!this.prevTask.metadata.hasAttribute(name)) {
                return false;
            }
            setField(getField(this.prevTask.task.getClass(), name), this.prevTask.task, value);
            return true;
        }

        private Field getField(Class<?> clazz, String name) {
            try {
                return ClassReflection.getField(clazz, name);
            } catch (ReflectionException e) {
                throw new GdxRuntimeException(e);
            }
        }

        private void setField(Field field, Task<E> task, Object value) {
            field.setAccessible(true);
            try {
                field.set(task, castValue(field, value));
            } catch (ReflectionException e) {
                throw new GdxRuntimeException(e);
            }
        }

        private Object castValue(Field field, Object value) {
            Class<?> type = field.getType();
            Object ret = null;
            if (value instanceof Number) {
                Number numberValue = (Number) value;
                if (type == Integer.TYPE || type == Integer.class) {
                    ret = Integer.valueOf(numberValue.intValue());
                } else if (type == Float.TYPE || type == Float.class) {
                    ret = Float.valueOf(numberValue.floatValue());
                } else if (type == Long.TYPE || type == Long.class) {
                    ret = Long.valueOf(numberValue.longValue());
                } else if (type == Double.TYPE || type == Double.class) {
                    ret = Double.valueOf(numberValue.doubleValue());
                } else if (type == Short.TYPE || type == Short.class) {
                    ret = Short.valueOf(numberValue.shortValue());
                } else if (type == Byte.TYPE || type == Byte.class) {
                    ret = Byte.valueOf(numberValue.byteValue());
                }
            } else if (value instanceof Boolean) {
                if (type == Boolean.TYPE || type == Boolean.class) {
                    ret = value;
                }
            } else if (value instanceof String) {
                String stringValue = (String) value;
                if (type == String.class) {
                    ret = value;
                } else if (type == Character.TYPE || type == Character.class) {
                    if (stringValue.length() != 1) {
                        throw new GdxRuntimeException("Invalid character '" + value + "'");
                    }
                    ret = Character.valueOf(stringValue.charAt(0));
                }
            }
            if (ret == null) {
                throwAttributeTypeException(this.prevTask.name, field.getName(), type.getSimpleName());
            }
            return ret;
        }

        private boolean attributeTag(String name, Object value) {
            if (this.tagType != 0) {
                return true;
            }
            if (value instanceof String) {
                addImport(name, (String) value);
                return true;
            }
            throwAttributeTypeException(STATEMENTS[this.tagType], name, "String");
            return true;
        }

        private void throwAttributeTypeException(String statement, String name, String expectedType) {
            throw new GdxRuntimeException(statement + ": attribute '" + name + "' must be of type " + expectedType);
        }

        /* access modifiers changed from: protected */
        public void endStatement() {
            if (this.isTask) {
                this.isTask = this.stack.size != 0;
            }
        }

        private void addImport(String alias, String task) {
            if (task == null) {
                throw new GdxRuntimeException("import: missing task class name.");
            }
            if (alias == null) {
                try {
                    alias = ClassReflection.forName(task).getSimpleName();
                } catch (ReflectionException e) {
                    throw new GdxRuntimeException("import: class not found '" + task + "'");
                }
            }
            if (getImport(alias) != null) {
                throw new GdxRuntimeException("import: alias '" + alias + "' previously defined already.");
            }
            this.userImports.put(alias, task);
        }

        private String getImport(String as) {
            String className = DEFAULT_IMPORTS.get(as);
            return className != null ? className : this.userImports.get(as);
        }

        private boolean openTag(String name) {
            for (int i = 0; i < STATEMENTS.length; i++) {
                if (name.equals(STATEMENTS[i])) {
                    this.tagType = i;
                    return true;
                }
            }
            return false;
        }

        private void openTask(int indent, String name) {
            int indent2;
            this.isTask = true;
            String className = getImport(name);
            if (className == null) {
                className = name;
            }
            try {
                Task<E> task = (Task) ClassReflection.newInstance(ClassReflection.forName(className));
                if (this.prevTask == null) {
                    this.root = task;
                    this.rootIndent = indent;
                    indent2 = 0;
                } else {
                    indent2 = indent - this.rootIndent;
                    if (this.prevTask.task == this.root) {
                        this.step = indent2;
                    }
                    if (indent2 > this.currentDepth) {
                        this.stack.add(this.prevTask);
                    } else if (indent2 <= this.currentDepth) {
                        popAndCheckMinChildren(this.stack.size - ((this.currentDepth - indent2) / this.step));
                    }
                    StackedTask<E> stackedParent = this.stack.peek();
                    int maxChildren = stackedParent.metadata.getMaxChildren();
                    if (stackedParent.task.getChildCount() >= maxChildren) {
                        throw new GdxRuntimeException(stackedParent.name + ": max number of children exceeded (" + (stackedParent.task.getChildCount() + 1) + " > " + maxChildren + ")");
                    }
                    stackedParent.task.addChild(task);
                }
                this.prevTask = new StackedTask<>(name, task);
                this.currentDepth = indent2;
            } catch (ReflectionException e) {
                throw new GdxRuntimeException("Cannot parse behavior tree!!!", e);
            }
        }

        private void popAndCheckMinChildren(int upToFloor) {
            if (this.prevTask != null) {
                checkMinChildren(this.prevTask);
            }
            while (this.stack.size > upToFloor) {
                checkMinChildren(this.stack.pop());
            }
        }

        private void checkMinChildren(StackedTask<E> stackedTask) {
            int minChildren = stackedTask.metadata.getMinChildren();
            if (stackedTask.task.getChildCount() < minChildren) {
                throw new GdxRuntimeException(stackedTask.name + ": not enough children (" + stackedTask.task.getChildCount() + " < " + minChildren + ")");
            }
        }

        private static class StackedTask<E> {
            Metadata metadata;
            String name;
            Task<E> task;

            StackedTask(String name2, Task<E> task2) {
                this.name = name2;
                this.task = task2;
                this.metadata = task2.getMetadata();
            }
        }
    }
}
