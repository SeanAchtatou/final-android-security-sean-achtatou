package com.badlogic.gdx.ai.steer.limiters;

public class LinearLimiter extends NullLimiter {
    private float maxLinearAcceleration;
    private float maxLinearSpeed;

    public LinearLimiter(float maxLinearAcceleration2, float maxLinearSpeed2) {
        this.maxLinearAcceleration = maxLinearAcceleration2;
        this.maxLinearSpeed = maxLinearSpeed2;
    }

    public float getMaxLinearSpeed() {
        return this.maxLinearSpeed;
    }

    public void setMaxLinearSpeed(float maxLinearSpeed2) {
        this.maxLinearSpeed = maxLinearSpeed2;
    }

    public float getMaxLinearAcceleration() {
        return this.maxLinearAcceleration;
    }

    public void setMaxLinearAcceleration(float maxLinearAcceleration2) {
        this.maxLinearAcceleration = maxLinearAcceleration2;
    }
}
