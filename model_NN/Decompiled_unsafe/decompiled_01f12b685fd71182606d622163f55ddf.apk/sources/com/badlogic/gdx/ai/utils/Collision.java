package com.badlogic.gdx.ai.utils;

import com.badlogic.gdx.math.Vector;

public class Collision<T extends Vector<T>> {
    public T normal;
    public T point;

    public Collision(T point2, T normal2) {
        this.point = point2;
        this.normal = normal2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.utils.Collision<T> set(com.badlogic.gdx.ai.utils.Collision<T> r3) {
        /*
            r2 = this;
            T r0 = r2.point
            T r1 = r3.point
            r0.set(r1)
            T r0 = r2.normal
            T r1 = r3.normal
            r0.set(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.utils.Collision.set(com.badlogic.gdx.ai.utils.Collision):com.badlogic.gdx.ai.utils.Collision");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.utils.Collision<T> set(T r2, T r3) {
        /*
            r1 = this;
            T r0 = r1.point
            r0.set(r2)
            T r0 = r1.normal
            r0.set(r3)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.utils.Collision.set(com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.ai.utils.Collision");
    }
}
