package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;

public class MatchVelocity<T extends Vector<T>> extends SteeringBehavior<T> {
    protected Steerable<T> target;
    protected float timeToTarget;

    public MatchVelocity(Steerable<T> owner) {
        this(owner, null);
    }

    public MatchVelocity(Steerable<T> owner, Steerable<T> target2) {
        this(owner, target2, 0.1f);
    }

    public MatchVelocity(Steerable<T> owner, Steerable<T> target2, float timeToTarget2) {
        super(owner);
        this.target = target2;
        this.timeToTarget = timeToTarget2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r4) {
        /*
            r3 = this;
            T r0 = r4.linear
            com.badlogic.gdx.ai.steer.Steerable<T> r1 = r3.target
            com.badlogic.gdx.math.Vector r1 = r1.getLinearVelocity()
            com.badlogic.gdx.math.Vector r0 = r0.set(r1)
            com.badlogic.gdx.ai.steer.Steerable r1 = r3.owner
            com.badlogic.gdx.math.Vector r1 = r1.getLinearVelocity()
            com.badlogic.gdx.math.Vector r0 = r0.sub(r1)
            r1 = 1065353216(0x3f800000, float:1.0)
            float r2 = r3.timeToTarget
            float r1 = r1 / r2
            com.badlogic.gdx.math.Vector r0 = r0.scl(r1)
            com.badlogic.gdx.ai.steer.Limiter r1 = r3.getActualLimiter()
            float r1 = r1.getMaxLinearAcceleration()
            r0.limit(r1)
            r0 = 0
            r4.angular = r0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.MatchVelocity.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public Steerable<T> getTarget() {
        return this.target;
    }

    public MatchVelocity<T> setTarget(Steerable steerable) {
        this.target = steerable;
        return this;
    }

    public float getTimeToTarget() {
        return this.timeToTarget;
    }

    public MatchVelocity<T> setTimeToTarget(float timeToTarget2) {
        this.timeToTarget = timeToTarget2;
        return this;
    }

    public MatchVelocity<T> setOwner(Steerable steerable) {
        this.owner = steerable;
        return this;
    }

    public MatchVelocity<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public MatchVelocity<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
