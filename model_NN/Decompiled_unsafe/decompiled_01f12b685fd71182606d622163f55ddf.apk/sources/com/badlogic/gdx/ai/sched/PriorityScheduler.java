package com.badlogic.gdx.ai.sched;

import com.badlogic.gdx.ai.sched.SchedulerBase;
import com.badlogic.gdx.utils.TimeUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class PriorityScheduler extends SchedulerBase<PrioritySchedulableRecord> {
    protected int frame = 0;

    public PriorityScheduler(int dryRunFrames) {
        super(dryRunFrames);
    }

    public void run(long timeToRun) {
        this.frame++;
        this.runList.size = 0;
        float totalPriority = Animation.CurveTimeline.LINEAR;
        for (int i = 0; i < this.schedulableRecords.size; i++) {
            PrioritySchedulableRecord record = (PrioritySchedulableRecord) this.schedulableRecords.get(i);
            if ((this.frame + record.phase) % record.frequency == 0) {
                this.runList.add(record);
                totalPriority += record.priority;
            }
        }
        long lastTime = TimeUtils.nanoTime();
        int numToRun = this.runList.size;
        for (int i2 = 0; i2 < numToRun; i2++) {
            long currentTime = TimeUtils.nanoTime();
            timeToRun -= currentTime - lastTime;
            PrioritySchedulableRecord record2 = (PrioritySchedulableRecord) this.runList.get(i2);
            record2.schedulable.run((long) ((((float) timeToRun) * record2.priority) / totalPriority));
            lastTime = currentTime;
        }
    }

    public void addWithAutomaticPhasing(Schedulable schedulable, int frequency) {
        addWithAutomaticPhasing(schedulable, frequency, 1.0f);
    }

    public void addWithAutomaticPhasing(Schedulable schedulable, int frequency, float priority) {
        add(schedulable, frequency, calculatePhase(frequency), priority);
    }

    public void add(Schedulable schedulable, int frequency, int phase) {
        add(schedulable, frequency, phase, 1.0f);
    }

    public void add(Schedulable schedulable, int frequency, int phase, float priority) {
        this.schedulableRecords.add(new PrioritySchedulableRecord(schedulable, frequency, phase, priority));
    }

    static class PrioritySchedulableRecord extends SchedulerBase.SchedulableRecord {
        float priority;

        PrioritySchedulableRecord(Schedulable schedulable, int frequency, int phase, float priority2) {
            super(schedulable, frequency, phase);
            this.priority = priority2;
        }
    }
}
