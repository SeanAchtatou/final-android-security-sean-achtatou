package com.badlogic.gdx.ai.steer;

import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class SteerableAdapter<T extends Vector<T>> implements Steerable<T> {
    public float getMaxLinearSpeed() {
        return Animation.CurveTimeline.LINEAR;
    }

    public void setMaxLinearSpeed(float maxLinearSpeed) {
    }

    public float getMaxLinearAcceleration() {
        return Animation.CurveTimeline.LINEAR;
    }

    public void setMaxLinearAcceleration(float maxLinearAcceleration) {
    }

    public float getMaxAngularSpeed() {
        return Animation.CurveTimeline.LINEAR;
    }

    public void setMaxAngularSpeed(float maxAngularSpeed) {
    }

    public float getMaxAngularAcceleration() {
        return Animation.CurveTimeline.LINEAR;
    }

    public void setMaxAngularAcceleration(float maxAngularAcceleration) {
    }

    public T getPosition() {
        return null;
    }

    public float getOrientation() {
        return Animation.CurveTimeline.LINEAR;
    }

    public T getLinearVelocity() {
        return null;
    }

    public float getAngularVelocity() {
        return Animation.CurveTimeline.LINEAR;
    }

    public float getBoundingRadius() {
        return Animation.CurveTimeline.LINEAR;
    }

    public boolean isTagged() {
        return false;
    }

    public void setTagged(boolean tagged) {
    }

    public T newVector() {
        return null;
    }

    public float vectorToAngle(T t) {
        return Animation.CurveTimeline.LINEAR;
    }

    public T angleToVector(T t, float angle) {
        return null;
    }
}
