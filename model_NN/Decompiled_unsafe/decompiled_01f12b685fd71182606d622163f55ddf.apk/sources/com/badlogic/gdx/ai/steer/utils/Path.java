package com.badlogic.gdx.ai.steer.utils;

import com.badlogic.gdx.ai.steer.utils.Path.PathParam;
import com.badlogic.gdx.math.Vector;

public interface Path<T extends Vector<T>, P extends PathParam> {

    public interface PathParam {
        float getDistance();

        void setDistance(float f);
    }

    float calculateDistance(T t, P p);

    void calculateTargetPosition(T t, P p, float f);

    P createParam();

    T getEndPoint();

    float getLength();

    T getStartPoint();

    boolean isOpen();
}
