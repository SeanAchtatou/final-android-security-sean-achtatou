package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.GroupBehavior;
import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class Cohesion<T extends Vector<T>> extends GroupBehavior<T> implements Proximity.ProximityCallback<T> {
    private T centerOfMass;

    public Cohesion(Steerable<T> owner, Proximity<T> proximity) {
        super(owner, proximity);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r5) {
        /*
            r4 = this;
            r5.setZero()
            T r1 = r5.linear
            r4.centerOfMass = r1
            com.badlogic.gdx.ai.steer.Proximity r1 = r4.proximity
            int r0 = r1.findNeighbors(r4)
            if (r0 <= 0) goto L_0x0033
            T r1 = r4.centerOfMass
            r2 = 1065353216(0x3f800000, float:1.0)
            float r3 = (float) r0
            float r2 = r2 / r3
            r1.scl(r2)
            T r1 = r4.centerOfMass
            com.badlogic.gdx.ai.steer.Steerable r2 = r4.owner
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            com.badlogic.gdx.math.Vector r1 = r1.sub(r2)
            com.badlogic.gdx.math.Vector r1 = r1.nor()
            com.badlogic.gdx.ai.steer.Limiter r2 = r4.getActualLimiter()
            float r2 = r2.getMaxLinearAcceleration()
            r1.scl(r2)
        L_0x0033:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Cohesion.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean reportNeighbor(com.badlogic.gdx.ai.steer.Steerable<T> r3) {
        /*
            r2 = this;
            T r0 = r2.centerOfMass
            com.badlogic.gdx.math.Vector r1 = r3.getPosition()
            r0.add(r1)
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Cohesion.reportNeighbor(com.badlogic.gdx.ai.steer.Steerable):boolean");
    }

    public Cohesion<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Cohesion<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Cohesion<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
