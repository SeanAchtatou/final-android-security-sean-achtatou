package com.badlogic.gdx.ai.btree.decorator;

import com.badlogic.gdx.ai.btree.Decorator;
import com.badlogic.gdx.ai.btree.Task;

public class AlwaysSucceed<E> extends Decorator<E> {
    public AlwaysSucceed() {
    }

    public AlwaysSucceed(Task<E> task) {
        super(task);
    }

    public void childSuccess(Task<E> task) {
        this.control.childSuccess(this);
    }

    public void childFail(Task<E> task) {
        this.control.childSuccess(this);
    }
}
