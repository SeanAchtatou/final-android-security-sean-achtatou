package com.badlogic.gdx.ai.pfa;

public interface Heuristic<N> {
    float estimate(N n, N n2);
}
