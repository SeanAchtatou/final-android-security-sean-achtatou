package com.badlogic.gdx.ai.pfa;

public interface HierarchicalGraph<N> extends Graph<N> {
    N convertNodeBetweenLevels(int i, N n, int i2);

    int getLevelCount();

    void setLevel(int i);
}
