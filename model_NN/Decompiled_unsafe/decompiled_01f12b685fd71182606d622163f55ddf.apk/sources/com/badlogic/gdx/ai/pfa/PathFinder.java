package com.badlogic.gdx.ai.pfa;

public interface PathFinder<N> {
    boolean search(PathFinderRequest<N> pathFinderRequest, long j);

    boolean searchConnectionPath(N n, N n2, Heuristic<N> heuristic, GraphPath<Connection<N>> graphPath);

    boolean searchNodePath(N n, N n2, Heuristic<N> heuristic, GraphPath<N> graphPath);
}
