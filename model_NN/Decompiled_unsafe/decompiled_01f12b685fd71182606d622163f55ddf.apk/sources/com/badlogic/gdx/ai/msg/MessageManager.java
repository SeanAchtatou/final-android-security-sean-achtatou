package com.badlogic.gdx.ai.msg;

public class MessageManager extends MessageDispatcher {
    private static final MessageManager instance = new MessageManager();

    private MessageManager() {
    }

    public static MessageManager getInstance() {
        return instance;
    }
}
