package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector;

public class Hide<T extends Vector<T>> extends Arrive<T> implements Proximity.ProximityCallback<T> {
    private T bestHidingSpot;
    private float distance2ToClosest;
    protected float distanceFromBoundary;
    protected Proximity<T> proximity;
    private T toObstacle;

    public Hide(Steerable<T> owner) {
        this(owner, null);
    }

    public Hide(Steerable<T> owner, Steerable<T> target) {
        this(owner, target, null);
    }

    public Hide(Steerable<T> owner, Steerable<T> target, Proximity<T> proximity2) {
        super(owner, target);
        this.proximity = proximity2;
        this.bestHidingSpot = owner.newVector();
        this.toObstacle = null;
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        this.distance2ToClosest = Float.POSITIVE_INFINITY;
        this.toObstacle = steering.linear;
        return this.proximity.findNeighbors(this) == 0 ? steering.setZero() : arrive(steering, this.bestHidingSpot);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean reportNeighbor(com.badlogic.gdx.ai.steer.Steerable<T> r6) {
        /*
            r5 = this;
            com.badlogic.gdx.math.Vector r2 = r6.getPosition()
            float r3 = r6.getBoundingRadius()
            com.badlogic.gdx.ai.steer.Steerable r4 = r5.target
            com.badlogic.gdx.math.Vector r4 = r4.getPosition()
            com.badlogic.gdx.math.Vector r1 = r5.getHidingPosition(r2, r3, r4)
            com.badlogic.gdx.ai.steer.Steerable r2 = r5.owner
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            float r0 = r1.dst2(r2)
            float r2 = r5.distance2ToClosest
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x002b
            r5.distance2ToClosest = r0
            T r2 = r5.bestHidingSpot
            r2.set(r1)
            r2 = 1
        L_0x002a:
            return r2
        L_0x002b:
            r2 = 0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Hide.reportNeighbor(com.badlogic.gdx.ai.steer.Steerable):boolean");
    }

    public Proximity<T> getProximity() {
        return this.proximity;
    }

    public Hide<T> setProximity(Proximity<T> proximity2) {
        this.proximity = proximity2;
        return this;
    }

    public float getDistanceFromBoundary() {
        return this.distanceFromBoundary;
    }

    public Hide<T> setDistanceFromBoundary(float distanceFromBoundary2) {
        this.distanceFromBoundary = distanceFromBoundary2;
        return this;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected T getHidingPosition(T r3, float r4, T r5) {
        /*
            r2 = this;
            float r1 = r2.distanceFromBoundary
            float r0 = r4 + r1
            T r1 = r2.toObstacle
            com.badlogic.gdx.math.Vector r1 = r1.set(r3)
            com.badlogic.gdx.math.Vector r1 = r1.sub(r5)
            r1.nor()
            T r1 = r2.toObstacle
            com.badlogic.gdx.math.Vector r1 = r1.scl(r0)
            com.badlogic.gdx.math.Vector r1 = r1.add(r3)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Hide.getHidingPosition(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.math.Vector):com.badlogic.gdx.math.Vector");
    }

    public Hide<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Hide<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Hide<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public Hide<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public Hide<T> setArrivalTolerance(float arrivalTolerance) {
        this.arrivalTolerance = arrivalTolerance;
        return this;
    }

    public Hide<T> setDecelerationRadius(float decelerationRadius) {
        this.decelerationRadius = decelerationRadius;
        return this;
    }

    public Hide<T> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }
}
