package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.steer.utils.RayConfiguration;
import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class RaycastObstacleAvoidance<T extends Vector<T>> extends SteeringBehavior<T> {
    protected float distanceFromBoundary;
    private Collision<T> minOutputCollision;
    private Collision<T> outputCollision;
    protected RayConfiguration<T> rayConfiguration;
    protected RaycastCollisionDetector<T> raycastCollisionDetector;

    public RaycastObstacleAvoidance(Steerable<T> owner) {
        this(owner, null);
    }

    public RaycastObstacleAvoidance(Steerable<T> owner, RayConfiguration<T> rayConfiguration2) {
        this(owner, rayConfiguration2, null);
    }

    public RaycastObstacleAvoidance(Steerable<T> owner, RayConfiguration<T> rayConfiguration2, RaycastCollisionDetector<T> raycastCollisionDetector2) {
        this(owner, rayConfiguration2, raycastCollisionDetector2, Animation.CurveTimeline.LINEAR);
    }

    public RaycastObstacleAvoidance(Steerable<T> owner, RayConfiguration<T> rayConfiguration2, RaycastCollisionDetector<T> raycastCollisionDetector2, float distanceFromBoundary2) {
        super(owner);
        this.rayConfiguration = rayConfiguration2;
        this.raycastCollisionDetector = raycastCollisionDetector2;
        this.distanceFromBoundary = distanceFromBoundary2;
        this.outputCollision = new Collision<>(owner.newVector(), owner.newVector());
        this.minOutputCollision = new Collision<>(owner.newVector(), owner.newVector());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r12) {
        /*
            r11 = this;
            com.badlogic.gdx.ai.steer.Steerable r7 = r11.owner
            com.badlogic.gdx.math.Vector r5 = r7.getPosition()
            r4 = 2139095040(0x7f800000, float:Infinity)
            com.badlogic.gdx.ai.steer.utils.RayConfiguration<T> r7 = r11.rayConfiguration
            com.badlogic.gdx.ai.utils.Ray[] r3 = r7.updateRays()
            r2 = 0
        L_0x000f:
            int r7 = r3.length
            if (r2 >= r7) goto L_0x0036
            com.badlogic.gdx.ai.utils.RaycastCollisionDetector<T> r7 = r11.raycastCollisionDetector
            com.badlogic.gdx.ai.utils.Collision<T> r8 = r11.outputCollision
            r9 = r3[r2]
            boolean r0 = r7.findCollision(r8, r9)
            if (r0 == 0) goto L_0x0033
            com.badlogic.gdx.ai.utils.Collision<T> r7 = r11.outputCollision
            T r7 = r7.point
            float r1 = r5.dst2(r7)
            int r7 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r7 >= 0) goto L_0x0033
            r4 = r1
            com.badlogic.gdx.ai.utils.Collision<T> r6 = r11.outputCollision
            com.badlogic.gdx.ai.utils.Collision<T> r7 = r11.minOutputCollision
            r11.outputCollision = r7
            r11.minOutputCollision = r6
        L_0x0033:
            int r2 = r2 + 1
            goto L_0x000f
        L_0x0036:
            r7 = 2139095040(0x7f800000, float:Infinity)
            int r7 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r7 != 0) goto L_0x0041
            com.badlogic.gdx.ai.steer.SteeringAcceleration r12 = r12.setZero()
        L_0x0040:
            return r12
        L_0x0041:
            T r7 = r12.linear
            com.badlogic.gdx.ai.utils.Collision<T> r8 = r11.minOutputCollision
            T r8 = r8.point
            com.badlogic.gdx.math.Vector r7 = r7.set(r8)
            com.badlogic.gdx.ai.utils.Collision<T> r8 = r11.minOutputCollision
            T r8 = r8.normal
            com.badlogic.gdx.ai.steer.Steerable r9 = r11.owner
            float r9 = r9.getBoundingRadius()
            float r10 = r11.distanceFromBoundary
            float r9 = r9 + r10
            com.badlogic.gdx.math.Vector r7 = r7.mulAdd(r8, r9)
            com.badlogic.gdx.ai.steer.Steerable r8 = r11.owner
            com.badlogic.gdx.math.Vector r8 = r8.getPosition()
            com.badlogic.gdx.math.Vector r7 = r7.sub(r8)
            com.badlogic.gdx.math.Vector r7 = r7.nor()
            com.badlogic.gdx.ai.steer.Limiter r8 = r11.getActualLimiter()
            float r8 = r8.getMaxLinearAcceleration()
            r7.scl(r8)
            r7 = 0
            r12.angular = r7
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.RaycastObstacleAvoidance.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public RayConfiguration<T> getRayConfiguration() {
        return this.rayConfiguration;
    }

    public RaycastObstacleAvoidance<T> setRayConfiguration(RayConfiguration<T> rayConfiguration2) {
        this.rayConfiguration = rayConfiguration2;
        return this;
    }

    public RaycastCollisionDetector<T> getRaycastCollisionDetector() {
        return this.raycastCollisionDetector;
    }

    public RaycastObstacleAvoidance<T> setRaycastCollisionDetector(RaycastCollisionDetector<T> raycastCollisionDetector2) {
        this.raycastCollisionDetector = raycastCollisionDetector2;
        return this;
    }

    public float getDistanceFromBoundary() {
        return this.distanceFromBoundary;
    }

    public RaycastObstacleAvoidance<T> setDistanceFromBoundary(float distanceFromBoundary2) {
        this.distanceFromBoundary = distanceFromBoundary2;
        return this;
    }

    public RaycastObstacleAvoidance<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public RaycastObstacleAvoidance<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public RaycastObstacleAvoidance<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
