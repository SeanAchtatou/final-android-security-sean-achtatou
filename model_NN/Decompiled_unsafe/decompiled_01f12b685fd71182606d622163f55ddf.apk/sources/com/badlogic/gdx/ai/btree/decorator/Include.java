package com.badlogic.gdx.ai.btree.decorator;

import com.badlogic.gdx.ai.btree.Decorator;
import com.badlogic.gdx.ai.btree.Metadata;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.ai.btree.TaskCloneException;
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeLibraryManager;

public class Include<E> extends Decorator<E> {
    public static final Metadata METADATA = new Metadata("subtree", "lazy");
    public boolean lazy;
    public String subtree;

    public Include() {
    }

    public Include(String subtree2) {
        this.subtree = subtree2;
    }

    public Include(String subtree2, boolean lazy2) {
        this.subtree = subtree2;
        this.lazy = lazy2;
    }

    public void setControl(Task<E> control) {
        if (!this.lazy) {
            throw new UnsupportedOperationException("A non-lazy " + Include.class.getSimpleName() + " isn't meant to be run!");
        }
        if (this.child == null) {
            this.child = createSubtreeRootTask();
        }
        super.setControl(control);
    }

    public Task<E> cloneTask() {
        if (this.lazy) {
            return super.cloneTask();
        }
        return createSubtreeRootTask();
    }

    /* access modifiers changed from: protected */
    public Task<E> copyTo(Task<E> task) {
        if (!this.lazy) {
            throw new TaskCloneException("A non-lazy " + getClass().getSimpleName() + " should never be copied.");
        }
        Include<E> include = (Include) task;
        include.subtree = this.subtree;
        include.lazy = this.lazy;
        return task;
    }

    private Task<E> createSubtreeRootTask() {
        return BehaviorTreeLibraryManager.getInstance().createRootTask(this.subtree);
    }
}
