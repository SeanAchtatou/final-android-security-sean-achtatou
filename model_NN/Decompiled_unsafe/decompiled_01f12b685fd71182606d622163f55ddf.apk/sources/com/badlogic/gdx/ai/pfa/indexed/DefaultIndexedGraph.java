package com.badlogic.gdx.ai.pfa.indexed;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;

public class DefaultIndexedGraph<N extends IndexedNode<N>> implements IndexedGraph<N> {
    protected Array<N> nodes;

    public /* bridge */ /* synthetic */ Array getConnections(Object x0) {
        return getConnections((IndexedNode) ((IndexedNode) x0));
    }

    public DefaultIndexedGraph() {
        this(new Array());
    }

    public DefaultIndexedGraph(int capacity) {
        this(new Array(capacity));
    }

    public DefaultIndexedGraph(Array<N> nodes2) {
        this.nodes = nodes2;
    }

    public Array<Connection<N>> getConnections(N fromNode) {
        return ((IndexedNode) this.nodes.get(fromNode.getIndex())).getConnections();
    }

    public int getNodeCount() {
        return this.nodes.size;
    }
}
