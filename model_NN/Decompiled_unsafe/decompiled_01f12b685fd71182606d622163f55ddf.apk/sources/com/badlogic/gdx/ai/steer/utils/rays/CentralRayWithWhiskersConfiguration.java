package com.badlogic.gdx.ai.steer.utils.rays;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class CentralRayWithWhiskersConfiguration<T extends Vector<T>> extends RayConfigurationBase<T> {
    private float rayLength;
    private float whiskerAngle;
    private float whiskerLength;

    public CentralRayWithWhiskersConfiguration(Steerable<T> owner, float rayLength2, float whiskerLength2, float whiskerAngle2) {
        super(owner, 3);
        this.rayLength = rayLength2;
        this.whiskerLength = whiskerLength2;
        this.whiskerAngle = whiskerAngle2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.utils.Ray<T>[] updateRays() {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r4 = 0
            com.badlogic.gdx.ai.steer.Steerable r3 = r7.owner
            com.badlogic.gdx.math.Vector r0 = r3.getPosition()
            com.badlogic.gdx.ai.steer.Steerable r3 = r7.owner
            com.badlogic.gdx.math.Vector r1 = r3.getLinearVelocity()
            com.badlogic.gdx.ai.steer.Steerable r3 = r7.owner
            float r2 = r3.vectorToAngle(r1)
            com.badlogic.gdx.ai.utils.Ray[] r3 = r7.rays
            r3 = r3[r4]
            T r3 = r3.start
            r3.set(r0)
            com.badlogic.gdx.ai.utils.Ray[] r3 = r7.rays
            r3 = r3[r4]
            T r3 = r3.end
            com.badlogic.gdx.math.Vector r3 = r3.set(r1)
            com.badlogic.gdx.math.Vector r3 = r3.nor()
            float r4 = r7.rayLength
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            r3.add(r0)
            com.badlogic.gdx.ai.utils.Ray[] r3 = r7.rays
            r3 = r3[r5]
            T r3 = r3.start
            r3.set(r0)
            com.badlogic.gdx.ai.steer.Steerable r3 = r7.owner
            com.badlogic.gdx.ai.utils.Ray[] r4 = r7.rays
            r4 = r4[r5]
            T r4 = r4.end
            float r5 = r7.whiskerAngle
            float r5 = r2 - r5
            com.badlogic.gdx.math.Vector r3 = r3.angleToVector(r4, r5)
            float r4 = r7.whiskerLength
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            r3.add(r0)
            com.badlogic.gdx.ai.utils.Ray[] r3 = r7.rays
            r3 = r3[r6]
            T r3 = r3.start
            r3.set(r0)
            com.badlogic.gdx.ai.steer.Steerable r3 = r7.owner
            com.badlogic.gdx.ai.utils.Ray[] r4 = r7.rays
            r4 = r4[r6]
            T r4 = r4.end
            float r5 = r7.whiskerAngle
            float r5 = r5 + r2
            com.badlogic.gdx.math.Vector r3 = r3.angleToVector(r4, r5)
            float r4 = r7.whiskerLength
            com.badlogic.gdx.math.Vector r3 = r3.scl(r4)
            r3.add(r0)
            com.badlogic.gdx.ai.utils.Ray[] r3 = r7.rays
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.rays.CentralRayWithWhiskersConfiguration.updateRays():com.badlogic.gdx.ai.utils.Ray[]");
    }

    public float getRayLength() {
        return this.rayLength;
    }

    public void setRayLength(float rayLength2) {
        this.rayLength = rayLength2;
    }

    public float getWhiskerLength() {
        return this.whiskerLength;
    }

    public void setWhiskerLength(float whiskerLength2) {
        this.whiskerLength = whiskerLength2;
    }

    public float getWhiskerAngle() {
        return this.whiskerAngle;
    }

    public void setWhiskerAngle(float whiskerAngle2) {
        this.whiskerAngle = whiskerAngle2;
    }
}
