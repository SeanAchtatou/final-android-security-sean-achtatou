package com.badlogic.gdx.ai.btree;

import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;

public class Metadata {
    ObjectSet<String> attributes;
    int maxChildren;
    int minChildren;

    public Metadata() {
        this(0, 0, (ObjectSet<String>) null);
    }

    public Metadata(String... attributes2) {
        this(0, 0, newObjectSet(attributes2));
    }

    public Metadata(ObjectSet<String> attributes2) {
        this(0, 0, attributes2);
    }

    public Metadata(int numChildrean) {
        this(numChildrean, numChildrean, (ObjectSet<String>) null);
    }

    public Metadata(int numChildrean, String... attributes2) {
        this(numChildrean, numChildrean, newObjectSet(attributes2));
    }

    public Metadata(int numChildrean, ObjectSet<String> attributes2) {
        this(numChildrean, numChildrean, attributes2);
    }

    public Metadata(int minChildren2, int maxChildren2) {
        this(minChildren2, maxChildren2, (ObjectSet<String>) null);
    }

    public Metadata(int minChildren2, int maxChildren2, String... attributes2) {
        this(minChildren2, maxChildren2, newObjectSet(attributes2));
    }

    public Metadata(Metadata other) {
        this(other, (String[]) null, (String[]) null);
    }

    public Metadata(Metadata other, String... addAttributes) {
        this(other, addAttributes, (String[]) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Metadata(Metadata other, String[] addAttributes, String... removeAttributes) {
        this(other.getMaxChildren(), other.getMaxChildren(), other.getAttributes() == null ? new ObjectSet() : new ObjectSet(other.getAttributes()));
        if (addAttributes != null) {
            for (String add : addAttributes) {
                getAttributes().add(add);
            }
        }
        if (removeAttributes != null) {
            for (String remove : removeAttributes) {
                getAttributes().remove(remove);
            }
        }
    }

    public Metadata(int minChildren2, int maxChildren2, ObjectSet<String> attributes2) {
        this.minChildren = minChildren2 < 0 ? 0 : minChildren2;
        this.maxChildren = maxChildren2 < 0 ? Integer.MAX_VALUE : maxChildren2;
        this.attributes = attributes2;
    }

    public int getMinChildren() {
        return this.minChildren;
    }

    public int getMaxChildren() {
        return this.maxChildren;
    }

    public ObjectSet<String> getAttributes() {
        return this.attributes;
    }

    public boolean hasAttribute(String name) {
        ObjectSet<String> attr = this.attributes;
        return attr != null && attr.contains(name);
    }

    public boolean isLeaf() {
        return getMinChildren() == 0 && getMaxChildren() == 0;
    }

    private static ObjectSet<String> newObjectSet(String... attributes2) {
        ObjectSet<String> attr = new ObjectSet<>();
        attr.addAll(attributes2);
        return attr;
    }

    public static Metadata findMetadata(Class<? extends Task> actualTaskClass) {
        try {
            Field metadataField = ClassReflection.getField(actualTaskClass, "METADATA");
            metadataField.setAccessible(true);
            return (Metadata) metadataField.get(null);
        } catch (ReflectionException e) {
            return null;
        }
    }
}
