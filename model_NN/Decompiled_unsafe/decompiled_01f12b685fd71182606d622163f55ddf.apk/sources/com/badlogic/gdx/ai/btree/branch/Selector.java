package com.badlogic.gdx.ai.btree.branch;

import com.badlogic.gdx.ai.btree.BranchTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.utils.Array;

public class Selector<E> extends BranchTask<E> {
    public Selector() {
        super(new Array());
    }

    public Selector(Task<E>... tasks) {
        super(new Array(tasks));
    }

    public Selector(Array<Task<E>> tasks) {
        super(tasks);
    }

    public void childFail(Task<E> runningTask) {
        super.childFail(runningTask);
        int i = this.actualTask + 1;
        this.actualTask = i;
        if (i < this.children.size) {
            run(this.object);
        } else {
            fail();
        }
    }

    public void childSuccess(Task<E> task) {
        success();
    }
}
