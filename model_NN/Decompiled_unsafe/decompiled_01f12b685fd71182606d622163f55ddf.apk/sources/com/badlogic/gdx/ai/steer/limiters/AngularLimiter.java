package com.badlogic.gdx.ai.steer.limiters;

public class AngularLimiter extends NullLimiter {
    private float maxAngularAcceleration;
    private float maxAngularSpeed;

    public AngularLimiter(float maxAngularAcceleration2, float maxAngularSpeed2) {
        this.maxAngularAcceleration = maxAngularAcceleration2;
        this.maxAngularSpeed = maxAngularSpeed2;
    }

    public float getMaxAngularSpeed() {
        return this.maxAngularSpeed;
    }

    public void setMaxAngularSpeed(float maxAngularSpeed2) {
        this.maxAngularSpeed = maxAngularSpeed2;
    }

    public float getMaxAngularAcceleration() {
        return this.maxAngularAcceleration;
    }

    public void setMaxAngularAcceleration(float maxAngularAcceleration2) {
        this.maxAngularAcceleration = maxAngularAcceleration2;
    }
}
