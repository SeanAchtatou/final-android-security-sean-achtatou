package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.ai.utils.Ray;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.math.Vector;

public class PathSmoother<N, V extends Vector<V>> {
    Ray<V> ray;
    RaycastCollisionDetector<V> raycastCollisionDetector;

    public PathSmoother(RaycastCollisionDetector<V> raycastCollisionDetector2) {
        this.raycastCollisionDetector = raycastCollisionDetector2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int smoothPath(com.badlogic.gdx.ai.pfa.SmoothableGraphPath<N, V> r9) {
        /*
            r8 = this;
            r5 = 0
            int r2 = r9.getCount()
            r6 = 2
            if (r2 > r6) goto L_0x0009
        L_0x0008:
            return r5
        L_0x0009:
            com.badlogic.gdx.ai.utils.Ray<V> r6 = r8.ray
            if (r6 != 0) goto L_0x0020
            com.badlogic.gdx.math.Vector r4 = r9.getNodePosition(r5)
            com.badlogic.gdx.ai.utils.Ray r5 = new com.badlogic.gdx.ai.utils.Ray
            com.badlogic.gdx.math.Vector r6 = r4.cpy()
            com.badlogic.gdx.math.Vector r7 = r4.cpy()
            r5.<init>(r6, r7)
            r8.ray = r5
        L_0x0020:
            r3 = 1
            r1 = 2
            r0 = 0
        L_0x0023:
            if (r1 >= r2) goto L_0x0051
            com.badlogic.gdx.ai.utils.Ray<V> r5 = r8.ray
            T r5 = r5.start
            int r6 = r3 + -1
            com.badlogic.gdx.math.Vector r6 = r9.getNodePosition(r6)
            r5.set(r6)
            com.badlogic.gdx.ai.utils.Ray<V> r5 = r8.ray
            T r5 = r5.end
            com.badlogic.gdx.math.Vector r6 = r9.getNodePosition(r1)
            r5.set(r6)
            com.badlogic.gdx.ai.utils.RaycastCollisionDetector<V> r5 = r8.raycastCollisionDetector
            com.badlogic.gdx.ai.utils.Ray<V> r6 = r8.ray
            boolean r0 = r5.collides(r6)
            if (r0 == 0) goto L_0x004e
            int r5 = r1 + -1
            r9.swapNodes(r3, r5)
            int r3 = r3 + 1
        L_0x004e:
            int r1 = r1 + 1
            goto L_0x0023
        L_0x0051:
            int r5 = r1 + -1
            r9.swapNodes(r3, r5)
            int r5 = r3 + 1
            r9.truncatePath(r5)
            int r5 = r1 - r3
            int r5 = r5 + -1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.pfa.PathSmoother.smoothPath(com.badlogic.gdx.ai.pfa.SmoothableGraphPath):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean smoothPath(com.badlogic.gdx.ai.pfa.PathSmootherRequest<N, V> r13, long r14) {
        /*
            r12 = this;
            long r4 = com.badlogic.gdx.utils.TimeUtils.nanoTime()
            com.badlogic.gdx.ai.pfa.SmoothableGraphPath<N, V> r6 = r13.path
            int r1 = r6.getCount()
            r8 = 2
            if (r1 > r8) goto L_0x000f
            r8 = 1
        L_0x000e:
            return r8
        L_0x000f:
            boolean r8 = r13.isNew
            if (r8 == 0) goto L_0x0036
            r8 = 0
            r13.isNew = r8
            com.badlogic.gdx.ai.utils.Ray<V> r8 = r12.ray
            if (r8 != 0) goto L_0x0030
            com.badlogic.gdx.ai.pfa.SmoothableGraphPath<N, V> r8 = r13.path
            r9 = 0
            com.badlogic.gdx.math.Vector r7 = r8.getNodePosition(r9)
            com.badlogic.gdx.ai.utils.Ray r8 = new com.badlogic.gdx.ai.utils.Ray
            com.badlogic.gdx.math.Vector r9 = r7.cpy()
            com.badlogic.gdx.math.Vector r10 = r7.cpy()
            r8.<init>(r9, r10)
            r12.ray = r8
        L_0x0030:
            r8 = 1
            r13.outputIndex = r8
            r8 = 2
            r13.inputIndex = r8
        L_0x0036:
            int r8 = r13.inputIndex
            if (r8 >= r1) goto L_0x0086
            long r2 = com.badlogic.gdx.utils.TimeUtils.nanoTime()
            long r8 = r2 - r4
            long r14 = r14 - r8
            r8 = 100
            int r8 = (r14 > r8 ? 1 : (r14 == r8 ? 0 : -1))
            if (r8 > 0) goto L_0x0049
            r8 = 0
            goto L_0x000e
        L_0x0049:
            com.badlogic.gdx.ai.utils.Ray<V> r8 = r12.ray
            T r8 = r8.start
            int r9 = r13.outputIndex
            int r9 = r9 + -1
            com.badlogic.gdx.math.Vector r9 = r6.getNodePosition(r9)
            r8.set(r9)
            com.badlogic.gdx.ai.utils.Ray<V> r8 = r12.ray
            T r8 = r8.end
            int r9 = r13.inputIndex
            com.badlogic.gdx.math.Vector r9 = r6.getNodePosition(r9)
            r8.set(r9)
            com.badlogic.gdx.ai.utils.RaycastCollisionDetector<V> r8 = r12.raycastCollisionDetector
            com.badlogic.gdx.ai.utils.Ray<V> r9 = r12.ray
            boolean r0 = r8.collides(r9)
            if (r0 == 0) goto L_0x007e
            int r8 = r13.outputIndex
            int r9 = r13.inputIndex
            int r9 = r9 + -1
            r6.swapNodes(r8, r9)
            int r8 = r13.outputIndex
            int r8 = r8 + 1
            r13.outputIndex = r8
        L_0x007e:
            int r8 = r13.inputIndex
            int r8 = r8 + 1
            r13.inputIndex = r8
            r4 = r2
            goto L_0x0036
        L_0x0086:
            int r8 = r13.outputIndex
            int r9 = r13.inputIndex
            int r9 = r9 + -1
            r6.swapNodes(r8, r9)
            int r8 = r13.outputIndex
            int r8 = r8 + 1
            r6.truncatePath(r8)
            r8 = 1
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.pfa.PathSmoother.smoothPath(com.badlogic.gdx.ai.pfa.PathSmootherRequest, long):boolean");
    }
}
