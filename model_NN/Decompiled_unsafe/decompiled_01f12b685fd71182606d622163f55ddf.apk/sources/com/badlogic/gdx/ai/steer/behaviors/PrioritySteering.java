package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;

public class PrioritySteering<T extends Vector<T>> extends SteeringBehavior<T> {
    protected Array<SteeringBehavior<T>> behaviors;
    protected float epsilon;

    public PrioritySteering(Steerable<T> owner) {
        this(owner, 0.001f);
    }

    public PrioritySteering(Steerable<T> owner, float epsilon2) {
        super(owner);
        this.behaviors = new Array<>();
        this.epsilon = epsilon2;
    }

    public PrioritySteering<T> add(SteeringBehavior<T> behavior) {
        this.behaviors.add(behavior);
        return this;
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        float epsilonSquared = this.epsilon * this.epsilon;
        int n = this.behaviors.size;
        for (int i = 0; i < n; i++) {
            this.behaviors.get(i).calculateSteering(steering);
            if (steering.calculateSquareMagnitude() > epsilonSquared) {
                return steering;
            }
        }
        return n <= 0 ? steering.setZero() : steering;
    }

    public float getEpsilon() {
        return this.epsilon;
    }

    public PrioritySteering<T> setEpsilon(float epsilon2) {
        this.epsilon = epsilon2;
        return this;
    }

    public PrioritySteering<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public PrioritySteering<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public PrioritySteering<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
