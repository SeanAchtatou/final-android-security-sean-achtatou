package com.badlogic.gdx.ai.btree;

public class BehaviorTree<E> extends Task<E> {
    private Task<E> rootTask;

    public BehaviorTree() {
        this(null, null);
    }

    public BehaviorTree(Task<E> rootTask2) {
        this(rootTask2, null);
    }

    public BehaviorTree(Task<E> rootTask2, E object) {
        this.rootTask = rootTask2;
        this.object = object;
    }

    public E getObject() {
        return this.object;
    }

    public void setObject(E object) {
        this.object = object;
    }

    public void addChild(Task<E> child) {
        if (this.rootTask != null) {
            throw new IllegalStateException("A behavior tree cannot have more than one root task");
        }
        this.rootTask = child;
    }

    public int getChildCount() {
        return this.rootTask == null ? 0 : 1;
    }

    public Task<E> getChild(int i) {
        if (i == 0 && this.rootTask != null) {
            return this.rootTask;
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + i + " >= " + getChildCount());
    }

    public void step() {
        if (this.runningTask != null) {
            this.runningTask.run(this.object);
            return;
        }
        this.rootTask.setControl(this);
        this.rootTask.object = this.object;
        this.rootTask.start(this.object);
        this.rootTask.run(this.object);
    }

    public void run(E e) {
    }

    /* access modifiers changed from: protected */
    public Task<E> copyTo(Task<E> task) {
        ((BehaviorTree) task).rootTask = this.rootTask.cloneTask();
        return task;
    }
}
