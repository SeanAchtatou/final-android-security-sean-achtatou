package com.badlogic.gdx.ai.utils;

import com.badlogic.gdx.utils.reflect.ArrayReflection;

public class CircularBuffer<T> {
    private int head;
    private T[] items;
    private boolean resizable;
    private int tail;

    public CircularBuffer(int initialCapacity) {
        this(initialCapacity, true);
    }

    public CircularBuffer(int initialCapacity, boolean resizable2) {
        this.items = (Object[]) new Object[initialCapacity];
        this.resizable = resizable2;
        this.head = 0;
        this.tail = 0;
    }

    public boolean store(T item) {
        if (isFull()) {
            if (!this.resizable) {
                return false;
            }
            resize(Math.max(8, (int) (((float) this.items.length) * 1.75f)));
        }
        T[] tArr = this.items;
        int i = this.tail;
        this.tail = i + 1;
        tArr[i] = item;
        if (this.tail == this.items.length) {
            this.tail = 0;
        }
        return true;
    }

    public T read() {
        if (this.head == this.tail) {
            return null;
        }
        T[] tArr = this.items;
        int i = this.head;
        this.head = i + 1;
        T item = tArr[i];
        if (this.head != this.items.length) {
            return item;
        }
        this.head = 0;
        return item;
    }

    public boolean isEmpty() {
        return this.head == this.tail;
    }

    public int size() {
        return this.tail >= this.head ? this.tail - this.head : (this.items.length - this.head) + this.tail;
    }

    private boolean isFull() {
        if (this.tail + 1 == this.head) {
            return true;
        }
        if (this.tail == this.items.length - 1 && this.head == 0) {
            return true;
        }
        return false;
    }

    public boolean isResizable() {
        return this.resizable;
    }

    public void setResizable(boolean resizable2) {
        this.resizable = resizable2;
    }

    /* access modifiers changed from: protected */
    public void resize(int newSize) {
        System.out.println("resize: " + newSize);
        T[] items2 = this.items;
        T[] newItems = (Object[]) ((Object[]) ArrayReflection.newInstance(items2.getClass().getComponentType(), newSize));
        if (this.tail >= this.head) {
            System.arraycopy(items2, this.head, newItems, 0, (this.tail - this.head) - 1);
            this.tail -= this.head;
            this.head = 0;
        } else {
            System.arraycopy(items2, this.head, newItems, 0, items2.length - this.head);
            System.arraycopy(items2, 0, newItems, items2.length - this.head, this.tail);
            this.tail = (items2.length - this.head) + this.tail;
            this.head = 0;
        }
        this.items = newItems;
    }
}
