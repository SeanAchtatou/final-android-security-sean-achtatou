package com.badlogic.gdx.ai.pfa.indexed;

import com.badlogic.gdx.ai.pfa.Graph;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;

public interface IndexedGraph<N extends IndexedNode<N>> extends Graph<N> {
    int getNodeCount();
}
