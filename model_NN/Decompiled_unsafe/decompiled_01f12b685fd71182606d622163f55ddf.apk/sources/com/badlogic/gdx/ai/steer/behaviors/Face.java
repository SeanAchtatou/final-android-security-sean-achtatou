package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector;

public class Face<T extends Vector<T>> extends ReachOrientation<T> {
    public Face(Steerable<T> owner) {
        this(owner, null);
    }

    public Face(Steerable<T> owner, Steerable<T> target) {
        super(owner, target);
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        return face(steering, this.target.getPosition());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> face(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r5, T r6) {
        /*
            r4 = this;
            T r2 = r5.linear
            com.badlogic.gdx.math.Vector r2 = r2.set(r6)
            com.badlogic.gdx.ai.steer.Steerable r3 = r4.owner
            com.badlogic.gdx.math.Vector r3 = r3.getPosition()
            com.badlogic.gdx.math.Vector r1 = r2.sub(r3)
            r2 = 897988541(0x358637bd, float:1.0E-6)
            boolean r2 = r1.isZero(r2)
            if (r2 == 0) goto L_0x001e
            com.badlogic.gdx.ai.steer.SteeringAcceleration r2 = r5.setZero()
        L_0x001d:
            return r2
        L_0x001e:
            com.badlogic.gdx.ai.steer.Steerable r2 = r4.owner
            float r0 = r2.vectorToAngle(r1)
            com.badlogic.gdx.ai.steer.SteeringAcceleration r2 = r4.reachOrientation(r5, r0)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Face.face(com.badlogic.gdx.ai.steer.SteeringAcceleration, com.badlogic.gdx.math.Vector):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public Face<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Face<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Face<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public Face<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public Face<T> setAlignTolerance(float alignTolerance) {
        this.alignTolerance = alignTolerance;
        return this;
    }

    public Face<T> setDecelerationRadius(float decelerationRadius) {
        this.decelerationRadius = decelerationRadius;
        return this;
    }

    public Face<T> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }
}
