package com.badlogic.gdx.ai.pfa;

public class DefaultConnection<N> implements Connection<N> {
    protected N fromNode;
    protected N toNode;

    public DefaultConnection(N fromNode2, N toNode2) {
        this.fromNode = fromNode2;
        this.toNode = toNode2;
    }

    public float getCost() {
        return 1.0f;
    }

    public N getFromNode() {
        return this.fromNode;
    }

    public N getToNode() {
        return this.toNode;
    }
}
