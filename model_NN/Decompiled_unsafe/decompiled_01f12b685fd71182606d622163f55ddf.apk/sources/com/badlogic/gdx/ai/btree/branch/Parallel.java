package com.badlogic.gdx.ai.btree.branch;

import com.badlogic.gdx.ai.btree.BranchTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.utils.Array;
import java.util.Iterator;

public class Parallel<E> extends BranchTask<E> {
    private int notDone;
    private final Array<Task<E>> runningTasks;
    private boolean success;

    public Parallel() {
        this(new Array());
    }

    public Parallel(Task<E>... tasks) {
        this(new Array(tasks));
    }

    public Parallel(Array<Task<E>> tasks) {
        super(tasks);
        this.success = true;
        this.runningTasks = new Array<>(false, tasks.size > 0 ? tasks.size : 8);
    }

    public void start(E object) {
        this.object = object;
        this.runningTasks.clear();
        this.success = true;
    }

    public void childRunning(Task<E> task, Task<E> reporter) {
        if (!this.runningTasks.contains(reporter, true)) {
            this.runningTasks.add(reporter);
        }
        this.notDone--;
        this.control.childRunning(this, this);
    }

    public void run(E object) {
        this.notDone = this.children.size;
        this.object = object;
        Iterator i$ = this.children.iterator();
        while (i$.hasNext()) {
            Task<E> child = (Task) i$.next();
            if (this.runningTasks.contains(child, true)) {
                child.run(object);
            } else {
                child.setControl(this);
                child.start(object);
                child.run(object);
            }
        }
    }

    public void childSuccess(Task<E> runningTask) {
        boolean z = true;
        this.runningTasks.removeValue(runningTask, true);
        if (!this.success) {
            z = false;
        }
        this.success = z;
        this.notDone--;
        if (this.runningTasks.size != 0 || this.notDone != 0) {
            return;
        }
        if (this.success) {
            success();
        } else {
            fail();
        }
    }

    public void childFail(Task<E> runningTask) {
        this.runningTasks.removeValue(runningTask, true);
        this.success = false;
        this.notDone--;
        if (this.runningTasks.size != 0 || this.notDone != 0) {
            return;
        }
        if (this.success) {
            success();
        } else {
            fail();
        }
    }
}
