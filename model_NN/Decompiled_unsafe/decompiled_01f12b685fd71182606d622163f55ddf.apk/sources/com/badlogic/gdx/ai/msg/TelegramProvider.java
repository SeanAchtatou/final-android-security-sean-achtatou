package com.badlogic.gdx.ai.msg;

public interface TelegramProvider {
    Object provideMessageInfo(int i, Telegraph telegraph);
}
