package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class ReachOrientation<T extends Vector<T>> extends SteeringBehavior<T> {
    protected float alignTolerance;
    protected float decelerationRadius;
    protected Steerable<T> target;
    protected float timeToTarget;

    public ReachOrientation(Steerable<T> owner) {
        this(owner, null);
    }

    public ReachOrientation(Steerable<T> owner, Steerable<T> target2) {
        super(owner);
        this.timeToTarget = 0.1f;
        this.target = target2;
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        return reachOrientation(steering, this.target.getOrientation());
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> reachOrientation(SteeringAcceleration<T> steering, float targetOrientation) {
        float rotationSize;
        float rotation = (targetOrientation - this.owner.getOrientation()) % 6.2831855f;
        if (rotation > 3.1415927f) {
            rotation -= 6.2831855f;
        }
        if (rotation < Animation.CurveTimeline.LINEAR) {
            rotationSize = -rotation;
        } else {
            rotationSize = rotation;
        }
        if (rotationSize < this.alignTolerance) {
            return steering.setZero();
        }
        Limiter actualLimiter = getActualLimiter();
        float targetRotation = actualLimiter.getMaxAngularSpeed();
        if (rotationSize <= this.decelerationRadius) {
            targetRotation *= rotationSize / this.decelerationRadius;
        }
        steering.angular = ((targetRotation * (rotation / rotationSize)) - this.owner.getAngularVelocity()) / this.timeToTarget;
        float angularAcceleration = steering.angular < Animation.CurveTimeline.LINEAR ? -steering.angular : steering.angular;
        if (angularAcceleration > actualLimiter.getMaxAngularAcceleration()) {
            steering.angular *= actualLimiter.getMaxAngularAcceleration() / angularAcceleration;
        }
        steering.linear.setZero();
        return steering;
    }

    public Steerable<T> getTarget() {
        return this.target;
    }

    public ReachOrientation<T> setTarget(Steerable steerable) {
        this.target = steerable;
        return this;
    }

    public float getAlignTolerance() {
        return this.alignTolerance;
    }

    public ReachOrientation<T> setAlignTolerance(float alignTolerance2) {
        this.alignTolerance = alignTolerance2;
        return this;
    }

    public float getDecelerationRadius() {
        return this.decelerationRadius;
    }

    public ReachOrientation<T> setDecelerationRadius(float decelerationRadius2) {
        this.decelerationRadius = decelerationRadius2;
        return this;
    }

    public float getTimeToTarget() {
        return this.timeToTarget;
    }

    public ReachOrientation<T> setTimeToTarget(float timeToTarget2) {
        this.timeToTarget = timeToTarget2;
        return this;
    }

    public ReachOrientation<T> setOwner(Steerable steerable) {
        this.owner = steerable;
        return this;
    }

    public ReachOrientation<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public ReachOrientation<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
