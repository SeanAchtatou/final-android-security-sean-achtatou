package com.badlogic.gdx.ai.steer;

import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class SteeringAcceleration<T extends Vector<T>> {
    public float angular;
    public T linear;

    public SteeringAcceleration(T linear2) {
        this(linear2, Animation.CurveTimeline.LINEAR);
    }

    public SteeringAcceleration(T linear2, float angular2) {
        if (linear2 == null) {
            throw new IllegalArgumentException("Linear acceleration cannot be null");
        }
        this.linear = linear2;
        this.angular = angular2;
    }

    public boolean isZero() {
        return this.angular == Animation.CurveTimeline.LINEAR && this.linear.isZero();
    }

    public SteeringAcceleration<T> setZero() {
        this.linear.setZero();
        this.angular = Animation.CurveTimeline.LINEAR;
        return this;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.steer.SteeringAcceleration<T> add(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r3) {
        /*
            r2 = this;
            T r0 = r2.linear
            T r1 = r3.linear
            r0.add(r1)
            float r0 = r2.angular
            float r1 = r3.angular
            float r0 = r0 + r1
            r2.angular = r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.SteeringAcceleration.add(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.steer.SteeringAcceleration<T> scl(float r2) {
        /*
            r1 = this;
            T r0 = r1.linear
            r0.scl(r2)
            float r0 = r1.angular
            float r0 = r0 * r2
            r1.angular = r0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.SteeringAcceleration.scl(float):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.steer.SteeringAcceleration<T> mulAdd(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r3, float r4) {
        /*
            r2 = this;
            T r0 = r2.linear
            T r1 = r3.linear
            r0.mulAdd(r1, r4)
            float r0 = r2.angular
            float r1 = r3.angular
            float r1 = r1 * r4
            float r0 = r0 + r1
            r2.angular = r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.SteeringAcceleration.mulAdd(com.badlogic.gdx.ai.steer.SteeringAcceleration, float):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public float calculateSquareMagnitude() {
        return this.linear.len2() + (this.angular * this.angular);
    }

    public float calculateMagnitude() {
        return (float) Math.sqrt((double) calculateSquareMagnitude());
    }
}
