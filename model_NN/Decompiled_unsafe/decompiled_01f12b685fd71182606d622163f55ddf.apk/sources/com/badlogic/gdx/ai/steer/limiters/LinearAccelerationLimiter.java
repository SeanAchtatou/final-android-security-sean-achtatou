package com.badlogic.gdx.ai.steer.limiters;

public class LinearAccelerationLimiter extends NullLimiter {
    private float maxLinearAcceleration;

    public LinearAccelerationLimiter(float maxLinearAcceleration2) {
        this.maxLinearAcceleration = maxLinearAcceleration2;
    }

    public float getMaxLinearAcceleration() {
        return this.maxLinearAcceleration;
    }

    public void setMaxLinearAcceleration(float maxLinearAcceleration2) {
        this.maxLinearAcceleration = maxLinearAcceleration2;
    }
}
