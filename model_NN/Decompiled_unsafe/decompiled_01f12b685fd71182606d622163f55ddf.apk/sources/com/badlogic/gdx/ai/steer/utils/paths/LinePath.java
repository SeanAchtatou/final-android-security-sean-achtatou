package com.badlogic.gdx.ai.steer.utils.paths;

import com.badlogic.gdx.ai.steer.utils.Path;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;

public class LinePath<T extends Vector<T>> implements Path<T, LinePathParam> {
    private boolean isOpen;
    private T nearestPointOnCurrentSegment;
    private T nearestPointOnPath;
    private float pathLength;
    private Array<Segment<T>> segments;
    private T tmp1;
    private T tmp2;
    private T tmp3;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateDistance(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.paths.LinePath$LinePathParam):float
     arg types: [com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.Path$PathParam]
     candidates:
      com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateDistance(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.Path$PathParam):float
      com.badlogic.gdx.ai.steer.utils.Path.calculateDistance(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.Path$PathParam):float
      com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateDistance(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.paths.LinePath$LinePathParam):float */
    public /* bridge */ /* synthetic */ float calculateDistance(Vector x0, Path.PathParam x1) {
        return calculateDistance(x0, (LinePathParam) ((LinePathParam) x1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateTargetPosition(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.paths.LinePath$LinePathParam, float):void
     arg types: [com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.Path$PathParam, float]
     candidates:
      com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateTargetPosition(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.Path$PathParam, float):void
      com.badlogic.gdx.ai.steer.utils.Path.calculateTargetPosition(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.Path$PathParam, float):void
      com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateTargetPosition(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.paths.LinePath$LinePathParam, float):void */
    public /* bridge */ /* synthetic */ void calculateTargetPosition(Vector x0, Path.PathParam x1, float x2) {
        calculateTargetPosition(x0, (LinePathParam) ((LinePathParam) x1), x2);
    }

    public LinePath(Array<T> waypoints) {
        this(waypoints, false);
    }

    public LinePath(Array<T> waypoints, boolean isOpen2) {
        this.isOpen = isOpen2;
        createPath(waypoints);
        this.nearestPointOnCurrentSegment = ((Vector) waypoints.first()).cpy();
        this.nearestPointOnPath = ((Vector) waypoints.first()).cpy();
        this.tmp1 = ((Vector) waypoints.first()).cpy();
        this.tmp2 = ((Vector) waypoints.first()).cpy();
        this.tmp3 = ((Vector) waypoints.first()).cpy();
    }

    public boolean isOpen() {
        return this.isOpen;
    }

    public float getLength() {
        return this.pathLength;
    }

    public T getStartPoint() {
        return this.segments.first().begin;
    }

    public T getEndPoint() {
        return this.segments.peek().end;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float calculatePointSegmentSquareDistance(T r6, T r7, T r8, T r9) {
        /*
            r5 = this;
            T r3 = r5.tmp1
            r3.set(r7)
            T r3 = r5.tmp2
            r3.set(r8)
            T r3 = r5.tmp3
            r3.set(r9)
            T r3 = r5.tmp2
            com.badlogic.gdx.math.Vector r0 = r3.sub(r7)
            T r3 = r5.tmp3
            com.badlogic.gdx.math.Vector r3 = r3.sub(r7)
            float r3 = r3.dot(r0)
            float r4 = r0.len2()
            float r2 = r3 / r4
            r3 = 0
            r4 = 1065353216(0x3f800000, float:1.0)
            float r2 = com.badlogic.gdx.math.MathUtils.clamp(r2, r3, r4)
            T r3 = r5.tmp1
            com.badlogic.gdx.math.Vector r4 = r0.scl(r2)
            com.badlogic.gdx.math.Vector r3 = r3.add(r4)
            r6.set(r3)
            T r3 = r5.tmp1
            r3.set(r6)
            T r3 = r5.tmp1
            com.badlogic.gdx.math.Vector r1 = r3.sub(r9)
            float r3 = r1.len2()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculatePointSegmentSquareDistance(com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):float");
    }

    public LinePathParam createParam() {
        return new LinePathParam();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float calculateDistance(T r10, com.badlogic.gdx.ai.steer.utils.paths.LinePath.LinePathParam r11) {
        /*
            r9 = this;
            r5 = 2139095040(0x7f800000, float:Infinity)
            r3 = 0
            r1 = 0
        L_0x0004:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.ai.steer.utils.paths.LinePath$Segment<T>> r6 = r9.segments
            int r6 = r6.size
            if (r1 >= r6) goto L_0x002e
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.ai.steer.utils.paths.LinePath$Segment<T>> r6 = r9.segments
            java.lang.Object r4 = r6.get(r1)
            com.badlogic.gdx.ai.steer.utils.paths.LinePath$Segment r4 = (com.badlogic.gdx.ai.steer.utils.paths.LinePath.Segment) r4
            T r6 = r9.nearestPointOnCurrentSegment
            T r7 = r4.begin
            T r8 = r4.end
            float r0 = r9.calculatePointSegmentSquareDistance(r6, r7, r8, r10)
            int r6 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r6 >= 0) goto L_0x002b
            T r6 = r9.nearestPointOnPath
            T r7 = r9.nearestPointOnCurrentSegment
            r6.set(r7)
            r5 = r0
            r3 = r4
            r11.segmentIndex = r1
        L_0x002b:
            int r1 = r1 + 1
            goto L_0x0004
        L_0x002e:
            float r6 = r3.cumulativeLength
            T r7 = r9.nearestPointOnPath
            T r8 = r3.end
            float r7 = r7.dst(r8)
            float r2 = r6 - r7
            r11.setDistance(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateDistance(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.paths.LinePath$LinePathParam):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void calculateTargetPosition(T r7, com.badlogic.gdx.ai.steer.utils.paths.LinePath.LinePathParam r8, float r9) {
        /*
            r6 = this;
            r5 = 0
            boolean r4 = r6.isOpen
            if (r4 == 0) goto L_0x0048
            int r4 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r4 >= 0) goto L_0x003f
            r9 = 0
        L_0x000a:
            r0 = 0
            r2 = 0
        L_0x000c:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.ai.steer.utils.paths.LinePath$Segment<T>> r4 = r6.segments
            int r4 = r4.size
            if (r2 >= r4) goto L_0x0021
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.ai.steer.utils.paths.LinePath$Segment<T>> r4 = r6.segments
            java.lang.Object r3 = r4.get(r2)
            com.badlogic.gdx.ai.steer.utils.paths.LinePath$Segment r3 = (com.badlogic.gdx.ai.steer.utils.paths.LinePath.Segment) r3
            float r4 = r3.cumulativeLength
            int r4 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r4 < 0) goto L_0x005f
            r0 = r3
        L_0x0021:
            float r4 = r0.cumulativeLength
            float r1 = r4 - r9
            T r4 = r0.begin
            com.badlogic.gdx.math.Vector r4 = r7.set(r4)
            T r5 = r0.end
            com.badlogic.gdx.math.Vector r4 = r4.sub(r5)
            float r5 = r0.length
            float r5 = r1 / r5
            com.badlogic.gdx.math.Vector r4 = r4.scl(r5)
            T r5 = r0.end
            r4.add(r5)
            return
        L_0x003f:
            float r4 = r6.pathLength
            int r4 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x000a
            float r9 = r6.pathLength
            goto L_0x000a
        L_0x0048:
            int r4 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r4 >= 0) goto L_0x0055
            float r4 = r6.pathLength
            float r5 = r6.pathLength
            float r5 = r9 % r5
            float r9 = r4 + r5
            goto L_0x000a
        L_0x0055:
            float r4 = r6.pathLength
            int r4 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x000a
            float r4 = r6.pathLength
            float r9 = r9 % r4
            goto L_0x000a
        L_0x005f:
            int r2 = r2 + 1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.paths.LinePath.calculateTargetPosition(com.badlogic.gdx.math.Vector, com.badlogic.gdx.ai.steer.utils.paths.LinePath$LinePathParam, float):void");
    }

    public void createPath(Array<T> waypoints) {
        T curr;
        if (waypoints == null || waypoints.size < 2) {
            throw new IllegalArgumentException("waypoints cannot be null and must contain at least two (2) waypoints");
        }
        this.segments = new Array<>(waypoints.size);
        this.pathLength = Animation.CurveTimeline.LINEAR;
        T curr2 = (Vector) waypoints.first();
        for (int i = 1; i <= waypoints.size; i++) {
            T prev = curr2;
            if (i < waypoints.size) {
                curr = waypoints.get(i);
            } else if (!this.isOpen) {
                curr = waypoints.first();
            } else {
                return;
            }
            curr2 = (Vector) curr;
            Segment<T> segment = new Segment<>(prev, curr2);
            this.pathLength += segment.length;
            segment.cumulativeLength = this.pathLength;
            this.segments.add(segment);
        }
    }

    public Array<Segment<T>> getSegments() {
        return this.segments;
    }

    public static class LinePathParam implements Path.PathParam {
        float distance;
        int segmentIndex;

        public float getDistance() {
            return this.distance;
        }

        public void setDistance(float distance2) {
            this.distance = distance2;
        }
    }

    public static class Segment<T extends Vector<T>> {
        T begin;
        float cumulativeLength;
        T end;
        float length;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        Segment(T r2, T r3) {
            /*
                r1 = this;
                r1.<init>()
                r1.begin = r2
                r1.end = r3
                float r0 = r2.dst(r3)
                r1.length = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.paths.LinePath.Segment.<init>(com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):void");
        }

        public T getBegin() {
            return this.begin;
        }

        public T getEnd() {
            return this.end;
        }

        public float getLength() {
            return this.length;
        }

        public float getCumulativeLength() {
            return this.cumulativeLength;
        }
    }
}
