package com.badlogic.gdx.ai.steer;

import com.badlogic.gdx.math.Vector;

public interface Proximity<T extends Vector<T>> {

    public interface ProximityCallback<T extends Vector<T>> {
        boolean reportNeighbor(Steerable<T> steerable);
    }

    int findNeighbors(ProximityCallback<T> proximityCallback);

    Steerable<T> getOwner();

    void setOwner(Steerable<T> steerable);
}
