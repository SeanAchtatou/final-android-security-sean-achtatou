package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.math.Vector;

public class PathSmootherRequest<N, V extends Vector<V>> {
    public int inputIndex;
    public boolean isNew = true;
    public int outputIndex;
    public SmoothableGraphPath<N, V> path;

    public void refresh(SmoothableGraphPath<N, V> path2) {
        this.path = path2;
        this.isNew = true;
    }
}
