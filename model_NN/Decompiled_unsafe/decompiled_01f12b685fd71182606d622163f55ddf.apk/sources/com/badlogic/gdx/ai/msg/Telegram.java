package com.badlogic.gdx.ai.msg;

import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.Animation;

public class Telegram implements Comparable<Telegram>, Pool.Poolable {
    public Object extraInfo;
    public int message;
    public Telegraph receiver;
    public Telegraph sender;
    private float timestamp;

    public float getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(float timestamp2) {
        this.timestamp = timestamp2;
    }

    public void reset() {
        this.sender = null;
        this.receiver = null;
        this.message = 0;
        this.extraInfo = null;
        this.timestamp = Animation.CurveTimeline.LINEAR;
    }

    public int compareTo(Telegram other) {
        if (equals(other)) {
            return 0;
        }
        return this.timestamp - other.timestamp < Animation.CurveTimeline.LINEAR ? -1 : 1;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((this.message + 31) * 31) + (this.receiver == null ? 0 : this.receiver.hashCode())) * 31;
        if (this.sender != null) {
            i = this.sender.hashCode();
        }
        return ((hashCode + i) * 31) + Float.floatToIntBits(this.timestamp);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Telegram other = (Telegram) obj;
        if (this.message != other.message) {
            return false;
        }
        if (Float.floatToIntBits(this.timestamp) != Float.floatToIntBits(other.timestamp)) {
            return false;
        }
        if (this.sender == null) {
            if (other.sender != null) {
                return false;
            }
        } else if (!this.sender.equals(other.sender)) {
            return false;
        }
        if (this.receiver == null) {
            if (other.receiver != null) {
                return false;
            }
            return true;
        } else if (!this.receiver.equals(other.receiver)) {
            return false;
        } else {
            return true;
        }
    }
}
