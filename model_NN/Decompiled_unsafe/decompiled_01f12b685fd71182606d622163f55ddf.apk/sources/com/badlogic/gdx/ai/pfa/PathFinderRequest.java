package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.msg.Telegraph;

public class PathFinderRequest<N> {
    public static final int SEARCH_DONE = 2;
    public static final int SEARCH_FINALIZED = 3;
    public static final int SEARCH_INITIALIZED = 1;
    public static final int SEARCH_NEW = 0;
    public Telegraph client;
    public MessageDispatcher dispatcher;
    public N endNode;
    public int executionFrames;
    public Heuristic<N> heuristic;
    public boolean pathFound;
    public int responseMessageCode;
    public GraphPath<N> resultPath;
    public N startNode;
    public int status;
    public boolean statusChanged;

    public PathFinderRequest() {
    }

    public PathFinderRequest(N startNode2, N endNode2, Heuristic<N> heuristic2, GraphPath<N> resultPath2) {
        this(startNode2, endNode2, heuristic2, resultPath2, MessageManager.getInstance());
    }

    public PathFinderRequest(N startNode2, N endNode2, Heuristic<N> heuristic2, GraphPath<N> resultPath2, MessageDispatcher dispatcher2) {
        this.startNode = startNode2;
        this.endNode = endNode2;
        this.heuristic = heuristic2;
        this.resultPath = resultPath2;
        this.dispatcher = dispatcher2;
        this.executionFrames = 0;
        this.pathFound = false;
        this.status = 0;
        this.statusChanged = false;
    }

    public void changeStatus(int newStatus) {
        this.status = newStatus;
        this.statusChanged = true;
    }

    public boolean initializeSearch(long timeToRun) {
        return true;
    }

    public boolean search(PathFinder<N> pathFinder, long timeToRun) {
        return pathFinder.search(this, timeToRun);
    }

    public boolean finalizeSearch(long timeToRun) {
        return true;
    }
}
