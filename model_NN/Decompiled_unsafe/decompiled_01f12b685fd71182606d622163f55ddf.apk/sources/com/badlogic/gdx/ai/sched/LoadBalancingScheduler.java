package com.badlogic.gdx.ai.sched;

import com.badlogic.gdx.ai.sched.SchedulerBase;
import com.badlogic.gdx.utils.TimeUtils;

public class LoadBalancingScheduler extends SchedulerBase<SchedulerBase.SchedulableRecord> {
    protected int frame = 0;

    public LoadBalancingScheduler(int dryRunFrames) {
        super(dryRunFrames);
    }

    public void addWithAutomaticPhasing(Schedulable schedulable, int frequency) {
        add(schedulable, frequency, calculatePhase(frequency));
    }

    public void add(Schedulable schedulable, int frequency, int phase) {
        this.schedulableRecords.add(new SchedulerBase.SchedulableRecord(schedulable, frequency, phase));
    }

    public void run(long timeToRun) {
        this.frame++;
        this.runList.size = 0;
        for (int i = 0; i < this.schedulableRecords.size; i++) {
            SchedulerBase.SchedulableRecord record = (SchedulerBase.SchedulableRecord) this.schedulableRecords.get(i);
            if ((this.frame + record.phase) % record.frequency == 0) {
                this.runList.add(record);
            }
        }
        long lastTime = TimeUtils.nanoTime();
        int numToRun = this.runList.size;
        for (int i2 = 0; i2 < numToRun; i2++) {
            long currentTime = TimeUtils.nanoTime();
            timeToRun -= currentTime - lastTime;
            ((SchedulerBase.SchedulableRecord) this.runList.get(i2)).schedulable.run(timeToRun / ((long) (numToRun - i2)));
            lastTime = currentTime;
        }
    }
}
