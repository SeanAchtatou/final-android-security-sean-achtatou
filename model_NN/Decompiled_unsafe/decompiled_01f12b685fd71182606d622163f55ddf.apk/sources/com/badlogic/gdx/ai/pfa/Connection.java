package com.badlogic.gdx.ai.pfa;

public interface Connection<N> {
    float getCost();

    N getFromNode();

    N getToNode();
}
