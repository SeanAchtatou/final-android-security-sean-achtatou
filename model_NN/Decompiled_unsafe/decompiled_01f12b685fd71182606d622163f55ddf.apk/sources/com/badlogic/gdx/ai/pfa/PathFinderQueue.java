package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.ai.sched.Schedulable;
import com.badlogic.gdx.ai.utils.CircularBuffer;
import com.badlogic.gdx.utils.TimeUtils;

public class PathFinderQueue<N> implements Schedulable, Telegraph {
    public static final long TIME_TOLERANCE = 100;
    PathFinderRequest<N> currentRequest = null;
    PathFinder<N> pathFinder;
    PathFinderRequestControl<N> requestControl = new PathFinderRequestControl<>();
    CircularBuffer<PathFinderRequest<N>> requestQueue = new CircularBuffer<>(16);

    public PathFinderQueue(PathFinder<N> pathFinder2) {
        this.pathFinder = pathFinder2;
    }

    public void run(long timeToRun) {
        this.requestControl.lastTime = TimeUtils.nanoTime();
        this.requestControl.timeToRun = timeToRun;
        this.requestControl.timeTolerance = 100;
        this.requestControl.pathFinder = this.pathFinder;
        this.requestControl.server = this;
        if (this.currentRequest == null) {
            this.currentRequest = this.requestQueue.read();
        }
        while (this.currentRequest != null && this.requestControl.execute(this.currentRequest)) {
            this.currentRequest = this.requestQueue.read();
        }
    }

    public boolean handleMessage(Telegram telegram) {
        PathFinderRequest<N> pfr = (PathFinderRequest) telegram.extraInfo;
        pfr.client = telegram.sender;
        pfr.status = 0;
        pfr.statusChanged = true;
        pfr.executionFrames = 0;
        this.requestQueue.store(pfr);
        return true;
    }

    public int size() {
        return this.requestQueue.size();
    }
}
