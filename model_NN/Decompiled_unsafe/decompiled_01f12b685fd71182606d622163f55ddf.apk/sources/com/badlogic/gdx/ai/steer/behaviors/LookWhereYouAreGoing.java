package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector;

public class LookWhereYouAreGoing<T extends Vector<T>> extends ReachOrientation<T> {
    public LookWhereYouAreGoing(Steerable<T> owner) {
        super(owner);
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        if (this.owner.getLinearVelocity().isZero(1.0E-6f)) {
            return steering.setZero();
        }
        return reachOrientation(steering, this.owner.vectorToAngle(this.owner.getLinearVelocity()));
    }

    public LookWhereYouAreGoing<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public LookWhereYouAreGoing<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public LookWhereYouAreGoing<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public LookWhereYouAreGoing<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public LookWhereYouAreGoing<T> setAlignTolerance(float alignTolerance) {
        this.alignTolerance = alignTolerance;
        return this;
    }

    public LookWhereYouAreGoing<T> setDecelerationRadius(float decelerationRadius) {
        this.decelerationRadius = decelerationRadius;
        return this;
    }

    public LookWhereYouAreGoing<T> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }
}
