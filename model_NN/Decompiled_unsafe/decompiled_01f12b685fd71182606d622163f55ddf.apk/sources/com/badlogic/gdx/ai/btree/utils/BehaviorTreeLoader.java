package com.badlogic.gdx.ai.btree.utils;

import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StreamUtils;
import java.io.Reader;

public class BehaviorTreeLoader extends AsynchronousAssetLoader<BehaviorTree, BehaviorTreeParameter> {
    BehaviorTree behaviorTree;

    public BehaviorTreeLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    public void loadAsync(AssetManager manager, String fileName, FileHandle file, BehaviorTreeParameter parameter) {
        this.behaviorTree = null;
        Object blackboard = null;
        if (parameter != null) {
            blackboard = parameter.blackboard;
            BehaviorTreeParser parser = parameter.parser;
        }
        Reader reader = null;
        try {
            reader = file.reader();
            this.behaviorTree = (parameter.parser == null ? new BehaviorTreeParser() : parameter.parser).parse(reader, blackboard);
        } finally {
            StreamUtils.closeQuietly(reader);
        }
    }

    public BehaviorTree loadSync(AssetManager manager, String fileName, FileHandle file, BehaviorTreeParameter parameter) {
        BehaviorTree bundle = this.behaviorTree;
        this.behaviorTree = null;
        return bundle;
    }

    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, BehaviorTreeParameter parameter) {
        return null;
    }

    public static class BehaviorTreeParameter extends AssetLoaderParameters<BehaviorTree> {
        public final Object blackboard;
        public final BehaviorTreeParser parser;

        public BehaviorTreeParameter() {
            this(null);
        }

        public BehaviorTreeParameter(Object blackboard2) {
            this(blackboard2, null);
        }

        public BehaviorTreeParameter(Object blackboard2, BehaviorTreeParser parser2) {
            this.blackboard = blackboard2;
            this.parser = parser2;
        }
    }
}
