package com.badlogic.gdx.ai.steer.utils.rays;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class SingleRayConfiguration<T extends Vector<T>> extends RayConfigurationBase<T> {
    private float length;

    public SingleRayConfiguration(Steerable<T> owner, float length2) {
        super(owner, 1);
        this.length = length2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.utils.Ray<T>[] updateRays() {
        /*
            r3 = this;
            r2 = 0
            com.badlogic.gdx.ai.utils.Ray[] r0 = r3.rays
            r0 = r0[r2]
            T r0 = r0.start
            com.badlogic.gdx.ai.steer.Steerable r1 = r3.owner
            com.badlogic.gdx.math.Vector r1 = r1.getPosition()
            r0.set(r1)
            com.badlogic.gdx.ai.utils.Ray[] r0 = r3.rays
            r0 = r0[r2]
            T r0 = r0.end
            com.badlogic.gdx.ai.steer.Steerable r1 = r3.owner
            com.badlogic.gdx.math.Vector r1 = r1.getLinearVelocity()
            com.badlogic.gdx.math.Vector r0 = r0.set(r1)
            com.badlogic.gdx.math.Vector r0 = r0.nor()
            float r1 = r3.length
            com.badlogic.gdx.math.Vector r0 = r0.scl(r1)
            com.badlogic.gdx.ai.utils.Ray[] r1 = r3.rays
            r1 = r1[r2]
            T r1 = r1.start
            r0.add(r1)
            com.badlogic.gdx.ai.utils.Ray[] r0 = r3.rays
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.rays.SingleRayConfiguration.updateRays():com.badlogic.gdx.ai.utils.Ray[]");
    }

    public float getLength() {
        return this.length;
    }

    public void setLength(float length2) {
        this.length = length2;
    }
}
