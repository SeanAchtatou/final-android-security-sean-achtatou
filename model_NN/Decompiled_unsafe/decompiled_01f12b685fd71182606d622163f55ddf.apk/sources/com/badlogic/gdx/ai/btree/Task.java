package com.badlogic.gdx.ai.btree;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

public abstract class Task<E> {
    public static final Metadata METADATA = new Metadata();
    protected Array<Task<E>> children;
    protected Task<E> control;
    protected E object;
    protected Task<E> runningTask;

    /* access modifiers changed from: protected */
    public abstract Task<E> copyTo(Task<E> task);

    public abstract void run(E e);

    public void addChild(Task<E> child) {
        this.children.add(child);
    }

    public int getChildCount() {
        return this.children.size;
    }

    public Task<E> getChild(int i) {
        return this.children.get(i);
    }

    public void setControl(Task<E> control2) {
        this.control = control2;
    }

    public void start(E e) {
    }

    public void end(E e) {
    }

    public final void running() {
        this.control.childRunning(this, this);
    }

    public void success() {
        end(this.object);
        this.control.childSuccess(this);
    }

    public void fail() {
        end(this.object);
        this.control.childFail(this);
    }

    public void childSuccess(Task<E> task) {
        this.runningTask = null;
    }

    public void childFail(Task<E> task) {
        this.runningTask = null;
    }

    public void childRunning(Task<E> runningTask2, Task<E> task) {
        this.runningTask = runningTask2;
    }

    public Metadata getMetadata() {
        return Metadata.findMetadata(getClass());
    }

    public Task<E> cloneTask() {
        try {
            return copyTo((Task) ClassReflection.newInstance(getClass()));
        } catch (ReflectionException e) {
            throw new TaskCloneException(e);
        }
    }
}
