package com.badlogic.gdx.ai.steer.utils.rays;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class ParallelSideRayConfiguration<T extends Vector<T>> extends RayConfigurationBase<T> {
    private static final float HALF_PI = 1.5707964f;
    private float length;
    private float sideOffset;

    public ParallelSideRayConfiguration(Steerable<T> owner, float length2, float sideOffset2) {
        super(owner, 2);
        this.length = length2;
        this.sideOffset = sideOffset2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.utils.Ray<T>[] updateRays() {
        /*
            r7 = this;
            r6 = 1070141403(0x3fc90fdb, float:1.5707964)
            r5 = 1
            r4 = 0
            com.badlogic.gdx.ai.steer.Steerable r1 = r7.owner
            com.badlogic.gdx.ai.steer.Steerable r2 = r7.owner
            com.badlogic.gdx.math.Vector r2 = r2.getLinearVelocity()
            float r0 = r1.vectorToAngle(r2)
            com.badlogic.gdx.ai.steer.Steerable r1 = r7.owner
            com.badlogic.gdx.ai.utils.Ray[] r2 = r7.rays
            r2 = r2[r4]
            T r2 = r2.start
            float r3 = r0 - r6
            com.badlogic.gdx.math.Vector r1 = r1.angleToVector(r2, r3)
            float r2 = r7.sideOffset
            com.badlogic.gdx.math.Vector r1 = r1.scl(r2)
            com.badlogic.gdx.ai.steer.Steerable r2 = r7.owner
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            r1.add(r2)
            com.badlogic.gdx.ai.utils.Ray[] r1 = r7.rays
            r1 = r1[r4]
            T r1 = r1.end
            com.badlogic.gdx.ai.steer.Steerable r2 = r7.owner
            com.badlogic.gdx.math.Vector r2 = r2.getLinearVelocity()
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            com.badlogic.gdx.math.Vector r1 = r1.nor()
            float r2 = r7.length
            r1.scl(r2)
            com.badlogic.gdx.ai.steer.Steerable r1 = r7.owner
            com.badlogic.gdx.ai.utils.Ray[] r2 = r7.rays
            r2 = r2[r5]
            T r2 = r2.start
            float r3 = r0 + r6
            com.badlogic.gdx.math.Vector r1 = r1.angleToVector(r2, r3)
            float r2 = r7.sideOffset
            com.badlogic.gdx.math.Vector r1 = r1.scl(r2)
            com.badlogic.gdx.ai.steer.Steerable r2 = r7.owner
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            r1.add(r2)
            com.badlogic.gdx.ai.utils.Ray[] r1 = r7.rays
            r1 = r1[r5]
            T r1 = r1.end
            com.badlogic.gdx.ai.utils.Ray[] r2 = r7.rays
            r2 = r2[r4]
            T r2 = r2.end
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            com.badlogic.gdx.ai.utils.Ray[] r2 = r7.rays
            r2 = r2[r5]
            T r2 = r2.start
            r1.add(r2)
            com.badlogic.gdx.ai.utils.Ray[] r1 = r7.rays
            r1 = r1[r4]
            T r1 = r1.end
            com.badlogic.gdx.ai.utils.Ray[] r2 = r7.rays
            r2 = r2[r4]
            T r2 = r2.start
            r1.add(r2)
            com.badlogic.gdx.ai.utils.Ray[] r1 = r7.rays
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.rays.ParallelSideRayConfiguration.updateRays():com.badlogic.gdx.ai.utils.Ray[]");
    }

    public float getLength() {
        return this.length;
    }

    public void setLength(float length2) {
        this.length = length2;
    }

    public float getSideOffset() {
        return this.sideOffset;
    }

    public void setSideOffset(float sideOffset2) {
        this.sideOffset = sideOffset2;
    }
}
