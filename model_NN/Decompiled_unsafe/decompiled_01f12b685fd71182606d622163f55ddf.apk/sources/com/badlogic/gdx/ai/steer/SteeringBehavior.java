package com.badlogic.gdx.ai.steer;

import com.badlogic.gdx.math.Vector;

public abstract class SteeringBehavior<T extends Vector<T>> {
    protected boolean enabled;
    protected Limiter limiter;
    protected Steerable<T> owner;

    /* access modifiers changed from: protected */
    public abstract SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steeringAcceleration);

    public SteeringBehavior(Steerable<T> owner2) {
        this(owner2, null, true);
    }

    public SteeringBehavior(Steerable<T> owner2, Limiter limiter2) {
        this(owner2, limiter2, true);
    }

    public SteeringBehavior(Steerable<T> owner2, boolean enabled2) {
        this(owner2, null, enabled2);
    }

    public SteeringBehavior(Steerable<T> owner2, Limiter limiter2, boolean enabled2) {
        this.owner = owner2;
        this.limiter = limiter2;
        this.enabled = enabled2;
    }

    public SteeringAcceleration<T> calculateSteering(SteeringAcceleration<T> steering) {
        return isEnabled() ? calculateRealSteering(steering) : steering.setZero();
    }

    public Steerable<T> getOwner() {
        return this.owner;
    }

    public SteeringBehavior<T> setOwner(Steerable<T> owner2) {
        this.owner = owner2;
        return this;
    }

    public Limiter getLimiter() {
        return this.limiter;
    }

    public SteeringBehavior<T> setLimiter(Limiter limiter2) {
        this.limiter = limiter2;
        return this;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public SteeringBehavior<T> setEnabled(boolean enabled2) {
        this.enabled = enabled2;
        return this;
    }

    /* access modifiers changed from: protected */
    public Limiter getActualLimiter() {
        return this.limiter == null ? this.owner : this.limiter;
    }
}
