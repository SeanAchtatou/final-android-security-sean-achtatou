package com.badlogic.gdx.ai.pfa.indexed;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.ai.pfa.PathFinder;
import com.badlogic.gdx.ai.pfa.PathFinderRequest;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BinaryHeap;
import com.badlogic.gdx.utils.TimeUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class IndexedAStarPathFinder<N extends IndexedNode<N>> implements PathFinder<N> {
    private static final int CLOSED = 2;
    private static final int OPEN = 1;
    private static final int UNVISITED = 0;
    NodeRecord<N> current;
    IndexedGraph<N> graph;
    public Metrics metrics;
    NodeRecord<N>[] nodeRecords;
    BinaryHeap<NodeRecord<N>> openList;
    private int searchId;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder.searchConnectionPath(com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean
     arg types: [java.lang.Object, java.lang.Object, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath]
     candidates:
      com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder.searchConnectionPath(java.lang.Object, java.lang.Object, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean
      com.badlogic.gdx.ai.pfa.PathFinder.searchConnectionPath(java.lang.Object, java.lang.Object, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean
      com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder.searchConnectionPath(com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean */
    public /* bridge */ /* synthetic */ boolean searchConnectionPath(Object x0, Object x1, Heuristic x2, GraphPath x3) {
        return searchConnectionPath((IndexedNode) ((IndexedNode) x0), (IndexedNode) ((IndexedNode) x1), x2, x3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder.searchNodePath(com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean
     arg types: [java.lang.Object, java.lang.Object, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath]
     candidates:
      com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder.searchNodePath(java.lang.Object, java.lang.Object, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean
      com.badlogic.gdx.ai.pfa.PathFinder.searchNodePath(java.lang.Object, java.lang.Object, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean
      com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder.searchNodePath(com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.indexed.IndexedNode, com.badlogic.gdx.ai.pfa.Heuristic, com.badlogic.gdx.ai.pfa.GraphPath):boolean */
    public /* bridge */ /* synthetic */ boolean searchNodePath(Object x0, Object x1, Heuristic x2, GraphPath x3) {
        return searchNodePath((IndexedNode) ((IndexedNode) x0), (IndexedNode) ((IndexedNode) x1), x2, x3);
    }

    public IndexedAStarPathFinder(IndexedGraph<N> graph2) {
        this(graph2, false);
    }

    public IndexedAStarPathFinder(IndexedGraph<N> graph2, boolean calculateMetrics) {
        this.graph = graph2;
        this.nodeRecords = (NodeRecord[]) new NodeRecord[graph2.getNodeCount()];
        this.openList = new BinaryHeap<>();
        if (calculateMetrics) {
            this.metrics = new Metrics();
        }
    }

    public boolean searchConnectionPath(N startNode, N endNode, Heuristic<N> heuristic, GraphPath<Connection<N>> outPath) {
        search(startNode, endNode, heuristic);
        if (this.current.node != endNode) {
            return false;
        }
        generateConnectionPath(startNode, outPath);
        return true;
    }

    public boolean searchNodePath(N startNode, N endNode, Heuristic<N> heuristic, GraphPath<N> outPath) {
        search(startNode, endNode, heuristic);
        if (this.current.node != endNode) {
            return false;
        }
        generateNodePath(startNode, outPath);
        return true;
    }

    /* access modifiers changed from: protected */
    public void search(N startNode, N endNode, Heuristic<N> heuristic) {
        initSearch(startNode, endNode, heuristic);
        do {
            this.current = this.openList.pop();
            this.current.category = 2;
            if (this.current.node != endNode) {
                visitChildren(endNode, heuristic);
            } else {
                return;
            }
        } while (this.openList.size > 0);
    }

    public boolean search(PathFinderRequest<N> request, long timeToRun) {
        long lastTime = TimeUtils.nanoTime();
        if (request.statusChanged) {
            initSearch((IndexedNode) request.startNode, (IndexedNode) request.endNode, request.heuristic);
            request.statusChanged = false;
        }
        do {
            long currentTime = TimeUtils.nanoTime();
            timeToRun -= currentTime - lastTime;
            if (timeToRun <= 100) {
                return false;
            }
            this.current = this.openList.pop();
            this.current.category = 2;
            if (this.current.node == request.endNode) {
                request.pathFound = true;
                generateNodePath((IndexedNode) request.startNode, request.resultPath);
                return true;
            }
            visitChildren((IndexedNode) request.endNode, request.heuristic);
            lastTime = currentTime;
        } while (this.openList.size > 0);
        request.pathFound = false;
        return true;
    }

    /* access modifiers changed from: protected */
    public void initSearch(N startNode, N endNode, Heuristic<N> heuristic) {
        if (this.metrics != null) {
            this.metrics.reset();
        }
        int i = this.searchId + 1;
        this.searchId = i;
        if (i < 0) {
            this.searchId = 1;
        }
        this.openList.clear();
        NodeRecord<N> startRecord = getNodeRecord(startNode);
        startRecord.node = startNode;
        startRecord.connection = null;
        startRecord.costSoFar = Animation.CurveTimeline.LINEAR;
        addToOpenList(startRecord, heuristic.estimate(startNode, endNode));
        this.current = null;
    }

    /* access modifiers changed from: protected */
    public void visitChildren(N endNode, Heuristic<N> heuristic) {
        float nodeHeuristic;
        Array<Connection<N>> connections = this.graph.getConnections(this.current.node);
        for (int i = 0; i < connections.size; i++) {
            if (this.metrics != null) {
                this.metrics.visitedNodes++;
            }
            Connection<N> connection = connections.get(i);
            N node = (IndexedNode) connection.getToNode();
            float nodeCost = this.current.costSoFar + connection.getCost();
            NodeRecord<N> nodeRecord = getNodeRecord(node);
            if (nodeRecord.category != 2) {
                if (nodeRecord.category != 1) {
                    nodeHeuristic = heuristic.estimate(node, endNode);
                } else if (nodeRecord.costSoFar > nodeCost) {
                    this.openList.remove(nodeRecord);
                    nodeHeuristic = nodeRecord.getEstimatedTotalCost() - nodeRecord.costSoFar;
                }
                nodeRecord.costSoFar = nodeCost;
                nodeRecord.connection = connection;
                addToOpenList(nodeRecord, nodeCost + nodeHeuristic);
            } else if (nodeRecord.costSoFar > nodeCost) {
                nodeHeuristic = nodeRecord.getEstimatedTotalCost() - nodeRecord.costSoFar;
                nodeRecord.costSoFar = nodeCost;
                nodeRecord.connection = connection;
                addToOpenList(nodeRecord, nodeCost + nodeHeuristic);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void generateConnectionPath(N startNode, GraphPath<Connection<N>> outPath) {
        while (this.current.node != startNode) {
            outPath.add(this.current.connection);
            this.current = this.nodeRecords[((IndexedNode) this.current.connection.getFromNode()).getIndex()];
        }
        outPath.reverse();
    }

    /* access modifiers changed from: protected */
    public void generateNodePath(N startNode, GraphPath<N> outPath) {
        while (this.current.connection != null) {
            outPath.add(this.current.node);
            this.current = this.nodeRecords[((IndexedNode) this.current.connection.getFromNode()).getIndex()];
        }
        outPath.add(startNode);
        outPath.reverse();
    }

    /* access modifiers changed from: protected */
    public void addToOpenList(NodeRecord<N> nodeRecord, float estimatedTotalCost) {
        this.openList.add(nodeRecord, estimatedTotalCost);
        nodeRecord.category = 1;
        if (this.metrics != null) {
            this.metrics.openListAdditions++;
            this.metrics.openListPeak = Math.max(this.metrics.openListPeak, this.openList.size);
        }
    }

    /* access modifiers changed from: protected */
    public NodeRecord<N> getNodeRecord(N node) {
        int index = node.getIndex();
        NodeRecord<N> nr = this.nodeRecords[index];
        if (nr != null) {
            if (nr.searchId != this.searchId) {
                nr.category = 0;
                nr.searchId = this.searchId;
            }
            return nr;
        }
        NodeRecord<N>[] nodeRecordArr = this.nodeRecords;
        NodeRecord<N> nr2 = new NodeRecord<>();
        nodeRecordArr[index] = nr2;
        nr2.node = node;
        nr2.searchId = this.searchId;
        return nr2;
    }

    static class NodeRecord<N extends IndexedNode<N>> extends BinaryHeap.Node {
        int category;
        Connection<N> connection;
        float costSoFar;
        N node;
        int searchId;

        public NodeRecord() {
            super(Animation.CurveTimeline.LINEAR);
        }

        public float getEstimatedTotalCost() {
            return getValue();
        }
    }

    public static class Metrics {
        public int openListAdditions;
        public int openListPeak;
        public int visitedNodes;

        public void reset() {
            this.visitedNodes = 0;
            this.openListAdditions = 0;
            this.openListPeak = 0;
        }
    }
}
