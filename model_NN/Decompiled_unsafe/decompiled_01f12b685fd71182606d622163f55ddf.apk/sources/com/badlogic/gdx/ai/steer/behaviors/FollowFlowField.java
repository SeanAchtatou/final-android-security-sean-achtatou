package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class FollowFlowField<T extends Vector<T>> extends SteeringBehavior<T> {
    protected FlowField<T> flowField;
    protected float predictionTime;

    public interface FlowField<T extends Vector<T>> {
        T lookup(T t);
    }

    public FollowFlowField(Steerable<T> owner) {
        this(owner, null);
    }

    public FollowFlowField(Steerable<T> owner, FlowField<T> flowField2) {
        this(owner, flowField2, Animation.CurveTimeline.LINEAR);
    }

    public FollowFlowField(Steerable<T> owner, FlowField<T> flowField2, float predictionTime2) {
        super(owner);
        this.flowField = flowField2;
        this.predictionTime = predictionTime2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r7) {
        /*
            r6 = this;
            float r3 = r6.predictionTime
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x0036
            com.badlogic.gdx.ai.steer.Steerable r3 = r6.owner
            com.badlogic.gdx.math.Vector r2 = r3.getPosition()
        L_0x000d:
            com.badlogic.gdx.ai.steer.behaviors.FollowFlowField$FlowField<T> r3 = r6.flowField
            com.badlogic.gdx.math.Vector r1 = r3.lookup(r2)
            com.badlogic.gdx.ai.steer.Limiter r0 = r6.getActualLimiter()
            r7.setZero()
            T r3 = r7.linear
            float r4 = r0.getMaxLinearSpeed()
            com.badlogic.gdx.math.Vector r3 = r3.mulAdd(r1, r4)
            com.badlogic.gdx.ai.steer.Steerable r4 = r6.owner
            com.badlogic.gdx.math.Vector r4 = r4.getLinearVelocity()
            com.badlogic.gdx.math.Vector r3 = r3.sub(r4)
            float r4 = r0.getMaxLinearAcceleration()
            r3.limit(r4)
            return r7
        L_0x0036:
            T r3 = r7.linear
            com.badlogic.gdx.ai.steer.Steerable r4 = r6.owner
            com.badlogic.gdx.math.Vector r4 = r4.getPosition()
            com.badlogic.gdx.math.Vector r3 = r3.set(r4)
            com.badlogic.gdx.ai.steer.Steerable r4 = r6.owner
            com.badlogic.gdx.math.Vector r4 = r4.getLinearVelocity()
            float r5 = r6.predictionTime
            com.badlogic.gdx.math.Vector r2 = r3.mulAdd(r4, r5)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.FollowFlowField.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public FlowField<T> getFlowField() {
        return this.flowField;
    }

    public FollowFlowField<T> setFlowField(FlowField<T> flowField2) {
        this.flowField = flowField2;
        return this;
    }

    public float getPredictionTime() {
        return this.predictionTime;
    }

    public FollowFlowField<T> setPredictionTime(float predictionTime2) {
        this.predictionTime = predictionTime2;
        return this;
    }

    public FollowFlowField<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public FollowFlowField<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public FollowFlowField<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
