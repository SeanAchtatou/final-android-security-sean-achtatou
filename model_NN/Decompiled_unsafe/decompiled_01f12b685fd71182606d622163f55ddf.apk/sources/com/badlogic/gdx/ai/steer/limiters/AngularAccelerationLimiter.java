package com.badlogic.gdx.ai.steer.limiters;

public class AngularAccelerationLimiter extends NullLimiter {
    private float maxAngularAcceleration;

    public AngularAccelerationLimiter(float maxAngularAcceleration2) {
        this.maxAngularAcceleration = maxAngularAcceleration2;
    }

    public float getMaxAngularAcceleration() {
        return this.maxAngularAcceleration;
    }

    public void setMaxAngularAcceleration(float maxAngularAcceleration2) {
        this.maxAngularAcceleration = maxAngularAcceleration2;
    }
}
