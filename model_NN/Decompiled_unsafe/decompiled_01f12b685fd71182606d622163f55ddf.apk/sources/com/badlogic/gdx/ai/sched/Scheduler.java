package com.badlogic.gdx.ai.sched;

public interface Scheduler extends Schedulable {
    void add(Schedulable schedulable, int i, int i2);

    void addWithAutomaticPhasing(Schedulable schedulable, int i);
}
