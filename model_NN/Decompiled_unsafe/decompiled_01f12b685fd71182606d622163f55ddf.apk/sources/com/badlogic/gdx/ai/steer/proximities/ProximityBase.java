package com.badlogic.gdx.ai.steer.proximities;

import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;

public abstract class ProximityBase<T extends Vector<T>> implements Proximity<T> {
    protected Array<? extends Steerable<T>> agents;
    protected Steerable<T> owner;

    public ProximityBase(Steerable<T> owner2, Array<? extends Steerable<T>> agents2) {
        this.owner = owner2;
        this.agents = agents2;
    }

    public Steerable<T> getOwner() {
        return this.owner;
    }

    public void setOwner(Steerable<T> owner2) {
        this.owner = owner2;
    }

    public Array<? extends Steerable<T>> getAgents() {
        return this.agents;
    }

    public void setAgents(Array<Steerable<T>> agents2) {
        this.agents = agents2;
    }
}
