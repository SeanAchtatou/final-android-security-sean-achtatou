package com.badlogic.gdx.ai.btree.branch;

import com.badlogic.gdx.ai.btree.BranchTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.utils.Array;

public class Sequence<E> extends BranchTask<E> {
    public Sequence() {
        super(new Array());
    }

    public Sequence(Array<Task<E>> tasks) {
        super(tasks);
    }

    public Sequence(Task<E>... tasks) {
        super(new Array(tasks));
    }

    public void childSuccess(Task<E> runningTask) {
        super.childSuccess(runningTask);
        int i = this.actualTask + 1;
        this.actualTask = i;
        if (i < this.children.size) {
            run(this.object);
        } else {
            success();
        }
    }

    public void childFail(Task<E> task) {
        fail();
    }
}
