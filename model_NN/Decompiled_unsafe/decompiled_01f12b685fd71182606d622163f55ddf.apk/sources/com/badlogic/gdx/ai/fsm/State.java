package com.badlogic.gdx.ai.fsm;

import com.badlogic.gdx.ai.msg.Telegram;

public interface State<E> {
    void enter(E e);

    void exit(E e);

    boolean onMessage(E e, Telegram telegram);

    void update(E e);
}
