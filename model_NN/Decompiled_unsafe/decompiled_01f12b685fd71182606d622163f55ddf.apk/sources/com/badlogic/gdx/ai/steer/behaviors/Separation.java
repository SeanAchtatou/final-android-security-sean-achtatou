package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.GroupBehavior;
import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector;

public class Separation<T extends Vector<T>> extends GroupBehavior<T> implements Proximity.ProximityCallback<T> {
    float decayCoefficient = 1.0f;
    private T linear;
    private T toAgent;

    public Separation(Steerable<T> owner, Proximity<T> proximity) {
        super(owner, proximity);
        this.toAgent = owner.newVector();
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        steering.setZero();
        this.linear = steering.linear;
        this.proximity.findNeighbors(this);
        return steering;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean reportNeighbor(com.badlogic.gdx.ai.steer.Steerable<T> r9) {
        /*
            r8 = this;
            T r3 = r8.toAgent
            com.badlogic.gdx.ai.steer.Steerable r4 = r8.owner
            com.badlogic.gdx.math.Vector r4 = r4.getPosition()
            com.badlogic.gdx.math.Vector r3 = r3.set(r4)
            com.badlogic.gdx.math.Vector r4 = r9.getPosition()
            r3.sub(r4)
            T r3 = r8.toAgent
            float r0 = r3.len2()
            com.badlogic.gdx.ai.steer.Limiter r3 = r8.getActualLimiter()
            float r1 = r3.getMaxLinearAcceleration()
            float r3 = r8.getDecayCoefficient()
            float r2 = r3 / r0
            int r3 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x002c
            r2 = r1
        L_0x002c:
            T r3 = r8.linear
            T r4 = r8.toAgent
            double r6 = (double) r0
            double r6 = java.lang.Math.sqrt(r6)
            float r5 = (float) r6
            float r5 = r2 / r5
            r3.mulAdd(r4, r5)
            r3 = 1
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Separation.reportNeighbor(com.badlogic.gdx.ai.steer.Steerable):boolean");
    }

    public float getDecayCoefficient() {
        return this.decayCoefficient;
    }

    public Separation<T> setDecayCoefficient(float decayCoefficient2) {
        this.decayCoefficient = decayCoefficient2;
        return this;
    }

    public Separation<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Separation<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Separation<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
