package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;

public class Pursue<T extends Vector<T>> extends SteeringBehavior<T> {
    protected float maxPredictionTime;
    protected Steerable<T> target;

    public Pursue(Steerable<T> owner, Steerable<T> target2) {
        this(owner, target2, 1.0f);
    }

    public Pursue(Steerable<T> owner, Steerable<T> target2, float maxPredictionTime2) {
        super(owner);
        this.target = target2;
        this.maxPredictionTime = maxPredictionTime2;
    }

    /* access modifiers changed from: protected */
    public float getActualMaxLinearAcceleration() {
        return getActualLimiter().getMaxLinearAcceleration();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r10) {
        /*
            r9 = this;
            r8 = 0
            com.badlogic.gdx.ai.steer.Steerable<T> r5 = r9.target
            com.badlogic.gdx.math.Vector r4 = r5.getPosition()
            T r5 = r10.linear
            com.badlogic.gdx.math.Vector r5 = r5.set(r4)
            com.badlogic.gdx.ai.steer.Steerable r6 = r9.owner
            com.badlogic.gdx.math.Vector r6 = r6.getPosition()
            com.badlogic.gdx.math.Vector r5 = r5.sub(r6)
            float r1 = r5.len2()
            com.badlogic.gdx.ai.steer.Steerable r5 = r9.owner
            com.badlogic.gdx.math.Vector r5 = r5.getLinearVelocity()
            float r3 = r5.len2()
            float r0 = r9.maxPredictionTime
            int r5 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r5 <= 0) goto L_0x003c
            float r2 = r1 / r3
            float r5 = r9.maxPredictionTime
            float r6 = r9.maxPredictionTime
            float r5 = r5 * r6
            int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x003c
            double r6 = (double) r2
            double r6 = java.lang.Math.sqrt(r6)
            float r0 = (float) r6
        L_0x003c:
            T r5 = r10.linear
            com.badlogic.gdx.math.Vector r5 = r5.set(r4)
            com.badlogic.gdx.ai.steer.Steerable<T> r6 = r9.target
            com.badlogic.gdx.math.Vector r6 = r6.getLinearVelocity()
            com.badlogic.gdx.math.Vector r5 = r5.mulAdd(r6, r0)
            com.badlogic.gdx.ai.steer.Steerable r6 = r9.owner
            com.badlogic.gdx.math.Vector r6 = r6.getPosition()
            com.badlogic.gdx.math.Vector r5 = r5.sub(r6)
            com.badlogic.gdx.math.Vector r5 = r5.nor()
            float r6 = r9.getActualMaxLinearAcceleration()
            r5.scl(r6)
            r10.angular = r8
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Pursue.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public Steerable<T> getTarget() {
        return this.target;
    }

    public Pursue<T> setTarget(Steerable steerable) {
        this.target = steerable;
        return this;
    }

    public float getMaxPredictionTime() {
        return this.maxPredictionTime;
    }

    public Pursue<T> setMaxPredictionTime(float maxPredictionTime2) {
        this.maxPredictionTime = maxPredictionTime2;
        return this;
    }

    public Pursue<T> setOwner(Steerable steerable) {
        this.owner = steerable;
        return this;
    }

    public Pursue<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Pursue<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
