package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class Interpose<T extends Vector<T>> extends Arrive<T> {
    protected Steerable<T> agentA;
    protected Steerable<T> agentB;
    private T internalTargetPosition;
    protected float interpositionRatio;

    public Interpose(Steerable<T> owner, Steerable<T> agentA2, Steerable<T> agentB2) {
        this(owner, agentA2, agentB2, 0.5f);
    }

    public Interpose(Steerable<T> owner, Steerable<T> agentA2, Steerable<T> agentB2, float interpositionRatio2) {
        super(owner);
        this.agentA = agentA2;
        this.agentB = agentB2;
        this.interpositionRatio = interpositionRatio2;
        this.internalTargetPosition = owner.newVector();
    }

    public Steerable<T> getAgentA() {
        return this.agentA;
    }

    public Interpose<T> setAgentA(Steerable<T> agentA2) {
        this.agentA = agentA2;
        return this;
    }

    public Steerable<T> getAgentB() {
        return this.agentB;
    }

    public Interpose<T> setAgentB(Steerable<T> agentB2) {
        this.agentB = agentB2;
        return this;
    }

    public float getInterpositionRatio() {
        return this.interpositionRatio;
    }

    public Interpose<T> setInterpositionRatio(float interpositionRatio2) {
        this.interpositionRatio = interpositionRatio2;
        return this;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r4) {
        /*
            r3 = this;
            T r1 = r3.internalTargetPosition
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentB
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentA
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            com.badlogic.gdx.math.Vector r1 = r1.sub(r2)
            float r2 = r3.interpositionRatio
            com.badlogic.gdx.math.Vector r1 = r1.scl(r2)
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentA
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            r1.add(r2)
            com.badlogic.gdx.ai.steer.Steerable r1 = r3.owner
            com.badlogic.gdx.math.Vector r1 = r1.getPosition()
            T r2 = r3.internalTargetPosition
            float r1 = r1.dst(r2)
            com.badlogic.gdx.ai.steer.Limiter r2 = r3.getActualLimiter()
            float r2 = r2.getMaxLinearSpeed()
            float r0 = r1 / r2
            T r1 = r4.linear
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentA
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentA
            com.badlogic.gdx.math.Vector r2 = r2.getLinearVelocity()
            r1.mulAdd(r2, r0)
            T r1 = r3.internalTargetPosition
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentB
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r3.agentB
            com.badlogic.gdx.math.Vector r2 = r2.getLinearVelocity()
            r1.mulAdd(r2, r0)
            T r1 = r3.internalTargetPosition
            T r2 = r4.linear
            com.badlogic.gdx.math.Vector r1 = r1.sub(r2)
            float r2 = r3.interpositionRatio
            com.badlogic.gdx.math.Vector r1 = r1.scl(r2)
            T r2 = r4.linear
            r1.add(r2)
            T r1 = r3.internalTargetPosition
            com.badlogic.gdx.ai.steer.SteeringAcceleration r1 = r3.arrive(r4, r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Interpose.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public T getInternalTargetPosition() {
        return this.internalTargetPosition;
    }

    public Interpose<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Interpose<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Interpose<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public Interpose<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public Interpose<T> setArrivalTolerance(float arrivalTolerance) {
        this.arrivalTolerance = arrivalTolerance;
        return this;
    }

    public Interpose<T> setDecelerationRadius(float decelerationRadius) {
        this.decelerationRadius = decelerationRadius;
        return this;
    }

    public Interpose<T> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }
}
