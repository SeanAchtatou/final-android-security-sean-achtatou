package com.badlogic.gdx.ai.btree.utils;

import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.utils.ObjectMap;

public class BehaviorTreeLibrary {
    protected BehaviorTreeParser<?> parser;
    protected ObjectMap<String, BehaviorTree<?>> repository;
    protected FileHandleResolver resolver;

    public BehaviorTreeLibrary() {
        this(0);
    }

    public BehaviorTreeLibrary(int parseDebugLevel) {
        this(new InternalFileHandleResolver(), parseDebugLevel);
    }

    public BehaviorTreeLibrary(FileHandleResolver resolver2) {
        this(resolver2, 0);
    }

    public BehaviorTreeLibrary(FileHandleResolver resolver2, int parseDebugLevel) {
        this(resolver2, null, parseDebugLevel);
    }

    private BehaviorTreeLibrary(FileHandleResolver resolver2, AssetManager assetManager, int parseDebugLevel) {
        this.resolver = resolver2;
        this.repository = new ObjectMap<>();
        this.parser = new BehaviorTreeParser<>(parseDebugLevel);
    }

    public <T> Task<T> createRootTask(String treeReference) {
        return retrieveArchetypeTree(treeReference).getChild(0).cloneTask();
    }

    public <T> BehaviorTree<T> createBehaviorTree(String treeReference) {
        return createBehaviorTree(treeReference, null);
    }

    public <T> BehaviorTree<T> createBehaviorTree(String treeReference, T blackboard) {
        BehaviorTree<T> bt = (BehaviorTree) retrieveArchetypeTree(treeReference).cloneTask();
        bt.setObject(blackboard);
        return bt;
    }

    /* access modifiers changed from: protected */
    public BehaviorTree<?> retrieveArchetypeTree(String treeReference) {
        BehaviorTree<?> archetypeTree = this.repository.get(treeReference);
        if (archetypeTree != null) {
            return archetypeTree;
        }
        BehaviorTree<?> archetypeTree2 = this.parser.parse(this.resolver.resolve(treeReference), (Object) null);
        registerArchetypeTree(treeReference, archetypeTree2);
        return archetypeTree2;
    }

    public void registerArchetypeTree(String treeReference, BehaviorTree<?> archetypeTree) {
        if (archetypeTree == null) {
            throw new IllegalArgumentException("The registered archetype must not be null.");
        }
        this.repository.put(treeReference, archetypeTree);
    }
}
