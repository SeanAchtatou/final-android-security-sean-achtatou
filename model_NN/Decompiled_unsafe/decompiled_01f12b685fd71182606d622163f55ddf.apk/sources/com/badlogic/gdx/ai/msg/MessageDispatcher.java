package com.badlogic.gdx.ai.msg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.kbz.esotericsoftware.spine.Animation;

public class MessageDispatcher {
    private static final String LOG_TAG = MessageDispatcher.class.getSimpleName();
    private static final Pool<Telegram> pool = new Pool<Telegram>(16) {
        /* access modifiers changed from: protected */
        public Telegram newObject() {
            return new Telegram();
        }
    };
    private float currentTime;
    private boolean debugEnabled;
    private IntMap<Array<Telegraph>> msgListeners = new IntMap<>();
    private IntMap<Array<TelegramProvider>> msgProviders = new IntMap<>();
    private PriorityQueue<Telegram> queue = new PriorityQueue<>();

    public interface PendingMessageCallback {
        void report(float f, Telegraph telegraph, Telegraph telegraph2, int i, Object obj);
    }

    public float getCurrentTime() {
        return this.currentTime;
    }

    public boolean isDebugEnabled() {
        return this.debugEnabled;
    }

    public void setDebugEnabled(boolean debugEnabled2) {
        this.debugEnabled = debugEnabled2;
    }

    public void addListener(Telegraph listener, int msg) {
        Array<Telegraph> listeners = this.msgListeners.get(msg);
        if (listeners == null) {
            listeners = new Array<>(false, 16);
            this.msgListeners.put(msg, listeners);
        }
        listeners.add(listener);
        Array<TelegramProvider> providers = this.msgProviders.get(msg);
        if (providers != null) {
            for (int i = 0; i < providers.size; i++) {
                TelegramProvider provider = (TelegramProvider) providers.get(i);
                Object info = provider.provideMessageInfo(msg, listener);
                if (info != null) {
                    if (ClassReflection.isInstance(Telegraph.class, provider)) {
                        dispatchMessage(Animation.CurveTimeline.LINEAR, (Telegraph) provider, listener, msg, info);
                    } else {
                        dispatchMessage(Animation.CurveTimeline.LINEAR, null, listener, msg, info);
                    }
                }
            }
        }
    }

    public void addListeners(Telegraph listener, int... msgs) {
        for (int msg : msgs) {
            addListener(listener, msg);
        }
    }

    public void addProvider(TelegramProvider provider, int msg) {
        Array<TelegramProvider> providers = this.msgProviders.get(msg);
        if (providers == null) {
            providers = new Array<>(false, 16);
            this.msgProviders.put(msg, providers);
        }
        providers.add(provider);
    }

    public void addProviders(TelegramProvider provider, int... msgs) {
        for (int msg : msgs) {
            addProvider(provider, msg);
        }
    }

    public void removeListener(Telegraph listener, int msg) {
        Array<Telegraph> listeners = this.msgListeners.get(msg);
        if (listeners != null) {
            listeners.removeValue(listener, true);
        }
    }

    public void removeListener(Telegraph listener, int... msgs) {
        for (int msg : msgs) {
            removeListener(listener, msg);
        }
    }

    public void clearListeners(int msg) {
        this.msgListeners.remove(msg);
    }

    public void clearListeners(int... msgs) {
        for (int msg : msgs) {
            clearListeners(msg);
        }
    }

    public void clearListeners() {
        this.msgListeners.clear();
    }

    public void clearProviders(int msg) {
        this.msgProviders.remove(msg);
    }

    public void clearProviders(int... msgs) {
        for (int msg : msgs) {
            clearProviders(msg);
        }
    }

    public void clearProviders() {
        this.msgProviders.clear();
    }

    public void clearQueue() {
        for (int i = 0; i < this.queue.size(); i++) {
            pool.free(this.queue.get(i));
        }
        this.queue.clear();
        this.currentTime = Animation.CurveTimeline.LINEAR;
    }

    public void clear() {
        clearQueue();
        clearListeners();
        clearProviders();
    }

    public void dispatchMessage(Telegraph sender, int msg) {
        dispatchMessage(Animation.CurveTimeline.LINEAR, sender, null, msg, null);
    }

    public void dispatchMessage(Telegraph sender, int msg, Object extraInfo) {
        dispatchMessage(Animation.CurveTimeline.LINEAR, sender, null, msg, extraInfo);
    }

    public void dispatchMessage(Telegraph sender, Telegraph receiver, int msg) {
        dispatchMessage(Animation.CurveTimeline.LINEAR, sender, receiver, msg, null);
    }

    public void dispatchMessage(Telegraph sender, Telegraph receiver, int msg, Object extraInfo) {
        dispatchMessage(Animation.CurveTimeline.LINEAR, sender, receiver, msg, extraInfo);
    }

    public void dispatchMessage(float delay, Telegraph sender, int msg) {
        dispatchMessage(delay, sender, null, msg, null);
    }

    public void dispatchMessage(float delay, Telegraph sender, int msg, Object extraInfo) {
        dispatchMessage(delay, sender, null, msg, extraInfo);
    }

    public void dispatchMessage(float delay, Telegraph sender, Telegraph receiver, int msg) {
        dispatchMessage(delay, sender, receiver, msg, null);
    }

    public void dispatchMessage(float delay, Telegraph sender, Telegraph receiver, int msg, Object extraInfo) {
        Telegram telegram = pool.obtain();
        telegram.sender = sender;
        telegram.receiver = receiver;
        telegram.message = msg;
        telegram.extraInfo = extraInfo;
        if (delay <= Animation.CurveTimeline.LINEAR) {
            if (this.debugEnabled) {
                Gdx.app.log(LOG_TAG, "Instant telegram dispatched at time: " + this.currentTime + " by " + sender + " for " + receiver + ". Msg is " + msg);
            }
            discharge(telegram);
            return;
        }
        telegram.setTimestamp(this.currentTime + delay);
        boolean added = this.queue.add(telegram);
        if (!added) {
            pool.free(telegram);
        }
        if (!this.debugEnabled) {
            return;
        }
        if (added) {
            Gdx.app.log(LOG_TAG, "Delayed telegram from " + sender + " for " + receiver + " recorded at time " + this.currentTime + ". Msg is " + msg);
        } else {
            Gdx.app.log(LOG_TAG, "Delayed telegram from " + sender + " for " + receiver + " rejected by the queue. Msg is " + msg);
        }
    }

    public void update(float deltaTime) {
        this.currentTime += deltaTime;
        while (true) {
            Telegram telegram = this.queue.peek();
            if (telegram != null && telegram.getTimestamp() <= this.currentTime) {
                if (this.debugEnabled) {
                    Gdx.app.log(LOG_TAG, "Queued telegram ready for dispatch: Sent to " + telegram.receiver + ". Msg is " + telegram.message);
                }
                discharge(telegram);
                this.queue.poll();
            } else {
                return;
            }
        }
    }

    public void scanQueue(PendingMessageCallback callback) {
        int queueSize = this.queue.size();
        for (int i = 0; i < queueSize; i++) {
            Telegram telegram = this.queue.get(i);
            callback.report(telegram.getTimestamp() - this.currentTime, telegram.sender, telegram.receiver, telegram.message, telegram.extraInfo);
        }
    }

    private void discharge(Telegram telegram) {
        if (telegram.receiver == null) {
            int handledCount = 0;
            Array<Telegraph> listeners = this.msgListeners.get(telegram.message);
            if (listeners != null) {
                for (int i = 0; i < listeners.size; i++) {
                    if (((Telegraph) listeners.get(i)).handleMessage(telegram)) {
                        handledCount++;
                    }
                }
            }
            if (this.debugEnabled && handledCount == 0) {
                Gdx.app.log(LOG_TAG, "Message " + telegram.message + " not handled");
            }
        } else if (!telegram.receiver.handleMessage(telegram) && this.debugEnabled) {
            Gdx.app.log(LOG_TAG, "Message " + telegram.message + " not handled");
        }
        pool.free(telegram);
    }
}
