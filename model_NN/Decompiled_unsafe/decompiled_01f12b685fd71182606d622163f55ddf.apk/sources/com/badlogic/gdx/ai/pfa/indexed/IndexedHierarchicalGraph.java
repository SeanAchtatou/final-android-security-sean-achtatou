package com.badlogic.gdx.ai.pfa.indexed;

import com.badlogic.gdx.ai.pfa.HierarchicalGraph;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;

public abstract class IndexedHierarchicalGraph<N extends IndexedNode<N>> extends DefaultIndexedGraph<N> implements HierarchicalGraph<N> {
    protected int level;
    protected int levelCount;

    public abstract N convertNodeBetweenLevels(int i, N n, int i2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.ai.pfa.indexed.IndexedHierarchicalGraph.convertNodeBetweenLevels(int, com.badlogic.gdx.ai.pfa.indexed.IndexedNode, int):N
     arg types: [int, java.lang.Object, int]
     candidates:
      com.badlogic.gdx.ai.pfa.indexed.IndexedHierarchicalGraph.convertNodeBetweenLevels(int, java.lang.Object, int):java.lang.Object
      com.badlogic.gdx.ai.pfa.HierarchicalGraph.convertNodeBetweenLevels(int, java.lang.Object, int):N
      com.badlogic.gdx.ai.pfa.indexed.IndexedHierarchicalGraph.convertNodeBetweenLevels(int, com.badlogic.gdx.ai.pfa.indexed.IndexedNode, int):N */
    public /* bridge */ /* synthetic */ Object convertNodeBetweenLevels(int x0, Object x1, int x2) {
        return convertNodeBetweenLevels(x0, (IndexedNode) ((IndexedNode) x1), x2);
    }

    public IndexedHierarchicalGraph(int levelCount2) {
        this(levelCount2, new Array());
    }

    public IndexedHierarchicalGraph(int levelCount2, int capacity) {
        this(levelCount2, new Array(capacity));
    }

    public IndexedHierarchicalGraph(int levelCount2, Array<N> nodes) {
        super(nodes);
        this.levelCount = levelCount2;
        this.level = 0;
    }

    public int getLevelCount() {
        return this.levelCount;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }
}
