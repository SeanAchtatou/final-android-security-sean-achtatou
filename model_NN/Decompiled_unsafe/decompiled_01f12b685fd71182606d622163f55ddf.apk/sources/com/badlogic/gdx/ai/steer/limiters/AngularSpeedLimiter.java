package com.badlogic.gdx.ai.steer.limiters;

public class AngularSpeedLimiter extends NullLimiter {
    private float maxAngularSpeed;

    public AngularSpeedLimiter(float maxAngularSpeed2) {
        this.maxAngularSpeed = maxAngularSpeed2;
    }

    public float getMaxAngularSpeed() {
        return this.maxAngularSpeed;
    }

    public void setMaxAngularSpeed(float maxAngularSpeed2) {
        this.maxAngularSpeed = maxAngularSpeed2;
    }
}
