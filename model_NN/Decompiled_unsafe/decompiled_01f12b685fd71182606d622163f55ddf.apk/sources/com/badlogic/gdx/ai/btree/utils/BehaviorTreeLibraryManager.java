package com.badlogic.gdx.ai.btree.utils;

import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.Task;

public class BehaviorTreeLibraryManager {
    private static BehaviorTreeLibraryManager instance = new BehaviorTreeLibraryManager();
    protected BehaviorTreeLibrary library;

    private BehaviorTreeLibraryManager() {
        setLibrary(new BehaviorTreeLibrary());
    }

    public static BehaviorTreeLibraryManager getInstance() {
        return instance;
    }

    public BehaviorTreeLibrary getLibrary() {
        return this.library;
    }

    public void setLibrary(BehaviorTreeLibrary library2) {
        this.library = library2;
    }

    public <T> Task<T> createRootTask(String treeReference) {
        return this.library.createRootTask(treeReference);
    }

    public <T> BehaviorTree<T> createBehaviorTree(String treeReference) {
        return this.library.createBehaviorTree(treeReference);
    }

    public <T> BehaviorTree<T> createBehaviorTree(String treeReference, T blackboard) {
        return this.library.createBehaviorTree(treeReference, blackboard);
    }
}
