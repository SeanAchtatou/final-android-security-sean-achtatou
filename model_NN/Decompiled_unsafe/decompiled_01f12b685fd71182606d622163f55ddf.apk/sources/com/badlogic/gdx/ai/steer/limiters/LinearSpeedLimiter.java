package com.badlogic.gdx.ai.steer.limiters;

public class LinearSpeedLimiter extends NullLimiter {
    private float maxLinearSpeed;

    public LinearSpeedLimiter(float maxLinearSpeed2) {
        this.maxLinearSpeed = maxLinearSpeed2;
    }

    public float getMaxLinearSpeed() {
        return this.maxLinearSpeed;
    }

    public void setMaxLinearSpeed(float maxLinearSpeed2) {
        this.maxLinearSpeed = maxLinearSpeed2;
    }
}
