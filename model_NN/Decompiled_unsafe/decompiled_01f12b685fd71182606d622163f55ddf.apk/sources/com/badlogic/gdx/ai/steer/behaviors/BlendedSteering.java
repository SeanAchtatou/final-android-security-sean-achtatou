package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;

public class BlendedSteering<T extends Vector<T>> extends SteeringBehavior<T> {
    protected Array<BehaviorAndWeight<T>> list = new Array<>();
    private SteeringAcceleration<T> steering;

    public BlendedSteering(Steerable<T> owner) {
        super(owner);
        this.steering = new SteeringAcceleration<>(owner.newVector());
    }

    public BlendedSteering<T> add(SteeringBehavior<T> behavior, float weight) {
        return add(new BehaviorAndWeight(behavior, weight));
    }

    public BlendedSteering<T> add(BehaviorAndWeight<T> item) {
        item.behavior.setOwner(this.owner);
        this.list.add(item);
        return this;
    }

    public BehaviorAndWeight<T> get(int index) {
        return this.list.get(index);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r7) {
        /*
            r6 = this;
            r7.setZero()
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.ai.steer.behaviors.BlendedSteering$BehaviorAndWeight<T>> r4 = r6.list
            int r3 = r4.size
            r2 = 0
        L_0x0008:
            if (r2 >= r3) goto L_0x0023
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.ai.steer.behaviors.BlendedSteering$BehaviorAndWeight<T>> r4 = r6.list
            java.lang.Object r1 = r4.get(r2)
            com.badlogic.gdx.ai.steer.behaviors.BlendedSteering$BehaviorAndWeight r1 = (com.badlogic.gdx.ai.steer.behaviors.BlendedSteering.BehaviorAndWeight) r1
            com.badlogic.gdx.ai.steer.SteeringBehavior<T> r4 = r1.behavior
            com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r5 = r6.steering
            r4.calculateSteering(r5)
            com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r4 = r6.steering
            float r5 = r1.weight
            r7.mulAdd(r4, r5)
            int r2 = r2 + 1
            goto L_0x0008
        L_0x0023:
            com.badlogic.gdx.ai.steer.Limiter r0 = r6.getActualLimiter()
            T r4 = r7.linear
            float r5 = r0.getMaxLinearAcceleration()
            r4.limit(r5)
            float r4 = r7.angular
            float r5 = r0.getMaxAngularAcceleration()
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 <= 0) goto L_0x0040
            float r4 = r0.getMaxAngularAcceleration()
            r7.angular = r4
        L_0x0040:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.BlendedSteering.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public BlendedSteering<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public BlendedSteering<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public BlendedSteering<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public static class BehaviorAndWeight<T extends Vector<T>> {
        protected SteeringBehavior<T> behavior;
        protected float weight;

        public BehaviorAndWeight(SteeringBehavior<T> behavior2, float weight2) {
            this.behavior = behavior2;
            this.weight = weight2;
        }

        public SteeringBehavior<T> getBehavior() {
            return this.behavior;
        }

        public void setBehavior(SteeringBehavior<T> behavior2) {
            this.behavior = behavior2;
        }

        public float getWeight() {
            return this.weight;
        }

        public void setWeight(float weight2) {
            this.weight = weight2;
        }
    }
}
