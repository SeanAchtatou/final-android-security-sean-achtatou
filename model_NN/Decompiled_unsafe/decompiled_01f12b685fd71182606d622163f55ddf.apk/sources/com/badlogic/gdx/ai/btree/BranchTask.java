package com.badlogic.gdx.ai.btree;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public abstract class BranchTask<E> extends Task<E> {
    public static final Metadata METADATA = new Metadata(1, -1, "deterministic");
    protected int actualTask;
    public boolean deterministic = true;
    protected Task<E> taskRunning;

    public BranchTask(Array<Task<E>> tasks) {
        this.children = tasks;
    }

    public void childRunning(Task<E> task, Task<E> task2) {
        this.runningTask = task;
        this.control.childRunning(task, this);
    }

    public void run(E object) {
        int lastTask;
        if (this.runningTask != null) {
            this.runningTask.run(object);
            return;
        }
        this.object = object;
        if (this.actualTask < this.children.size) {
            if (!this.deterministic && this.actualTask < this.children.size - 1) {
                this.children.swap(this.actualTask, MathUtils.random(this.actualTask, lastTask));
            }
            this.runningTask = (Task) this.children.get(this.actualTask);
            this.runningTask.setControl(this);
            this.runningTask.object = object;
            this.runningTask.start(object);
            run(object);
            return;
        }
        end(object);
    }

    public void start(E e) {
        this.actualTask = 0;
        this.runningTask = null;
    }

    /* access modifiers changed from: protected */
    public Task<E> copyTo(Task<E> task) {
        BranchTask<E> branch = (BranchTask) task;
        branch.deterministic = this.deterministic;
        if (this.children != null) {
            for (int i = 0; i < this.children.size; i++) {
                branch.children.add(((Task) this.children.get(i)).cloneTask());
            }
        }
        return task;
    }
}
