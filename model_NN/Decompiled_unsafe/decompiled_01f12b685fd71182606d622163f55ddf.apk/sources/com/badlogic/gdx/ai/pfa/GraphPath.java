package com.badlogic.gdx.ai.pfa;

public interface GraphPath<N> extends Iterable<N> {
    void add(N n);

    void clear();

    N get(int i);

    int getCount();

    void reverse();
}
