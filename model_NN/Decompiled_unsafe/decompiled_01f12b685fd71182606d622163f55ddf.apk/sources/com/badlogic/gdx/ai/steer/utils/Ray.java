package com.badlogic.gdx.ai.steer.utils;

import com.badlogic.gdx.math.Vector;

public class Ray<T extends Vector<T>> {
    public T end;
    public T start;

    public Ray(T start2, T end2) {
        this.start = start2;
        this.end = end2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.steer.utils.Ray<T> set(com.badlogic.gdx.ai.steer.utils.Ray<T> r3) {
        /*
            r2 = this;
            T r0 = r2.start
            T r1 = r3.start
            r0.set(r1)
            T r0 = r2.end
            T r1 = r3.end
            r0.set(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.Ray.set(com.badlogic.gdx.ai.steer.utils.Ray):com.badlogic.gdx.ai.steer.utils.Ray");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.steer.utils.Ray<T> set(T r2, T r3) {
        /*
            r1 = this;
            T r0 = r1.start
            r0.set(r2)
            T r0 = r1.end
            r0.set(r3)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.utils.Ray.set(com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):com.badlogic.gdx.ai.steer.utils.Ray");
    }
}
