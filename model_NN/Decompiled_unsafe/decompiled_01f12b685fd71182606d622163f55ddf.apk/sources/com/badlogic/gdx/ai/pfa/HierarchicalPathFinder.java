package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.utils.TimeUtils;

public class HierarchicalPathFinder<N> implements PathFinder<N> {
    public static boolean DEBUG = false;
    HierarchicalGraph<N> graph;
    PathFinder<N> levelPathFinder;
    LevelPathFinderRequest<N> levelRequest = null;
    PathFinderRequestControl<N> levelRequestControl = null;

    public HierarchicalPathFinder(HierarchicalGraph<N> graph2, PathFinder<N> levelPathFinder2) {
        this.graph = graph2;
        this.levelPathFinder = levelPathFinder2;
    }

    public boolean searchNodePath(N startNode, N endNode, Heuristic<N> heuristic, GraphPath<N> outPath) {
        N currentEndNodeParent;
        if (startNode == endNode) {
            return true;
        }
        N currentEndNode = endNode;
        int levelOfNodes = 0;
        int currentLevel = this.graph.getLevelCount() - 1;
        while (currentLevel >= 0) {
            N currentStartNode = this.graph.convertNodeBetweenLevels(0, startNode, currentLevel);
            currentEndNode = this.graph.convertNodeBetweenLevels(levelOfNodes, currentEndNode, currentLevel);
            if (currentLevel == 0 && (currentEndNodeParent = this.graph.convertNodeBetweenLevels(0, currentEndNode, 1)) == this.graph.convertNodeBetweenLevels(0, endNode, 1) && currentEndNodeParent == this.graph.convertNodeBetweenLevels(0, startNode, 1)) {
                currentEndNode = endNode;
            }
            levelOfNodes = currentLevel;
            currentLevel--;
            if (currentStartNode != currentEndNode) {
                this.graph.setLevel(levelOfNodes);
                outPath.clear();
                if (!this.levelPathFinder.searchNodePath(currentStartNode, currentEndNode, heuristic, outPath)) {
                    return false;
                }
                currentEndNode = outPath.get(1);
            }
        }
        return true;
    }

    public boolean searchConnectionPath(N startNode, N endNode, Heuristic<N> heuristic, GraphPath<Connection<N>> outPath) {
        N currentEndNodeParent;
        if (startNode == endNode) {
            return true;
        }
        N currentEndNode = endNode;
        int levelOfNodes = 0;
        int currentLevel = this.graph.getLevelCount() - 1;
        while (currentLevel >= 0) {
            N currentStartNode = this.graph.convertNodeBetweenLevels(0, startNode, currentLevel);
            currentEndNode = this.graph.convertNodeBetweenLevels(levelOfNodes, currentEndNode, currentLevel);
            if (currentLevel == 0 && (currentEndNodeParent = this.graph.convertNodeBetweenLevels(0, currentEndNode, 1)) == this.graph.convertNodeBetweenLevels(0, endNode, 1) && currentEndNodeParent == this.graph.convertNodeBetweenLevels(0, startNode, 1)) {
                currentEndNode = endNode;
            }
            levelOfNodes = currentLevel;
            currentLevel--;
            if (currentStartNode != currentEndNode) {
                this.graph.setLevel(levelOfNodes);
                outPath.clear();
                if (!this.levelPathFinder.searchConnectionPath(currentStartNode, currentEndNode, heuristic, outPath)) {
                    return false;
                }
                currentEndNode = outPath.get(0).getToNode();
            }
        }
        return true;
    }

    public boolean search(PathFinderRequest<N> request, long timeToRun) {
        if (DEBUG) {
            System.out.println("Enter interruptible HPF; request.status = " + request.status);
        }
        if (this.levelRequest == null) {
            this.levelRequest = new LevelPathFinderRequest<>();
            this.levelRequestControl = new PathFinderRequestControl<>();
        }
        if (request.statusChanged) {
            if (DEBUG) {
                System.out.println("-- statusChanged");
            }
            if (request.startNode == request.endNode) {
                return true;
            }
            this.levelRequestControl.lastTime = TimeUtils.nanoTime();
            this.levelRequestControl.timeToRun = timeToRun;
            this.levelRequestControl.timeTolerance = 100;
            this.levelRequestControl.server = null;
            this.levelRequestControl.pathFinder = this.levelPathFinder;
            this.levelRequest.hpf = this;
            this.levelRequest.hpfRequest = request;
            this.levelRequest.status = 0;
            this.levelRequest.statusChanged = true;
            this.levelRequest.heuristic = request.heuristic;
            this.levelRequest.resultPath = request.resultPath;
            this.levelRequest.startNode = request.startNode;
            this.levelRequest.endNode = request.endNode;
            this.levelRequest.levelOfNodes = 0;
            this.levelRequest.currentLevel = this.graph.getLevelCount() - 1;
        }
        while (this.levelRequest.currentLevel >= 0) {
            if (!this.levelRequestControl.execute(this.levelRequest)) {
                return false;
            }
            this.levelRequest.executionFrames = 0;
            this.levelRequest.status = 0;
            this.levelRequest.statusChanged = true;
            if (!this.levelRequest.pathFound) {
                return true;
            }
        }
        if (!DEBUG) {
            return true;
        }
        System.out.println("-- before exit");
        return true;
    }

    static class LevelPathFinderRequest<N> extends PathFinderRequest<N> {
        int currentLevel;
        HierarchicalPathFinder<N> hpf;
        PathFinderRequest<N> hpfRequest;
        int levelOfNodes;

        LevelPathFinderRequest() {
        }

        public boolean initializeSearch(long timeToRun) {
            N currentEndNodeParent;
            this.executionFrames = 0;
            this.pathFound = false;
            this.status = 0;
            this.statusChanged = false;
            do {
                this.startNode = this.hpf.graph.convertNodeBetweenLevels(0, this.hpfRequest.startNode, this.currentLevel);
                this.endNode = this.hpf.graph.convertNodeBetweenLevels(this.levelOfNodes, this.endNode, this.currentLevel);
                if (this.currentLevel == 0 && (currentEndNodeParent = this.hpf.graph.convertNodeBetweenLevels(0, this.endNode, 1)) == this.hpf.graph.convertNodeBetweenLevels(0, this.hpfRequest.endNode, 1) && currentEndNodeParent == this.hpf.graph.convertNodeBetweenLevels(0, this.hpfRequest.startNode, 1)) {
                    this.endNode = this.hpfRequest.endNode;
                }
                if (HierarchicalPathFinder.DEBUG) {
                    System.out.println("LevelPathFinder initializeSearch");
                }
                this.levelOfNodes = this.currentLevel;
                this.currentLevel--;
                if (this.startNode != this.endNode) {
                    break;
                }
            } while (this.currentLevel >= 0);
            this.hpf.graph.setLevel(this.levelOfNodes);
            this.resultPath.clear();
            return true;
        }

        public boolean search(PathFinder<N> pathFinder, long timeToRun) {
            if (HierarchicalPathFinder.DEBUG) {
                System.out.println("LevelPathFinder search; status: " + this.status);
            }
            return super.search(pathFinder, timeToRun);
        }

        public boolean finalizeSearch(long timeToRun) {
            this.hpfRequest.pathFound = this.pathFound;
            if (this.pathFound) {
                this.endNode = this.resultPath.get(1);
            }
            if (HierarchicalPathFinder.DEBUG) {
                System.out.println("LevelPathFinder finalizeSearch; status: " + this.status);
            }
            return true;
        }
    }
}
