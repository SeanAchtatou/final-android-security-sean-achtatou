package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector;

public class Arrive<T extends Vector<T>> extends SteeringBehavior<T> {
    protected float arrivalTolerance;
    protected float decelerationRadius;
    protected Steerable<T> target;
    protected float timeToTarget;

    public Arrive(Steerable<T> owner) {
        this(owner, null);
    }

    public Arrive(Steerable<T> owner, Steerable<T> target2) {
        super(owner);
        this.timeToTarget = 0.1f;
        this.target = target2;
    }

    /* access modifiers changed from: protected */
    public SteeringAcceleration<T> calculateRealSteering(SteeringAcceleration<T> steering) {
        return arrive(steering, this.target.getPosition());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> arrive(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r9, T r10) {
        /*
            r8 = this;
            T r5 = r9.linear
            com.badlogic.gdx.math.Vector r5 = r5.set(r10)
            com.badlogic.gdx.ai.steer.Steerable r6 = r8.owner
            com.badlogic.gdx.math.Vector r6 = r6.getPosition()
            com.badlogic.gdx.math.Vector r4 = r5.sub(r6)
            float r1 = r4.len()
            float r5 = r8.arrivalTolerance
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x001f
            com.badlogic.gdx.ai.steer.SteeringAcceleration r9 = r9.setZero()
        L_0x001e:
            return r9
        L_0x001f:
            com.badlogic.gdx.ai.steer.Limiter r0 = r8.getActualLimiter()
            float r2 = r0.getMaxLinearSpeed()
            float r5 = r8.decelerationRadius
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x0032
            float r5 = r8.decelerationRadius
            float r5 = r1 / r5
            float r2 = r2 * r5
        L_0x0032:
            float r5 = r2 / r1
            com.badlogic.gdx.math.Vector r3 = r4.scl(r5)
            com.badlogic.gdx.ai.steer.Steerable r5 = r8.owner
            com.badlogic.gdx.math.Vector r5 = r5.getLinearVelocity()
            com.badlogic.gdx.math.Vector r5 = r3.sub(r5)
            r6 = 1065353216(0x3f800000, float:1.0)
            float r7 = r8.timeToTarget
            float r6 = r6 / r7
            com.badlogic.gdx.math.Vector r5 = r5.scl(r6)
            float r6 = r0.getMaxLinearAcceleration()
            r5.limit(r6)
            r5 = 0
            r9.angular = r5
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Arrive.arrive(com.badlogic.gdx.ai.steer.SteeringAcceleration, com.badlogic.gdx.math.Vector):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public Steerable<T> getTarget() {
        return this.target;
    }

    public Arrive<T> setTarget(Steerable<T> target2) {
        this.target = target2;
        return this;
    }

    public float getArrivalTolerance() {
        return this.arrivalTolerance;
    }

    public Arrive<T> setArrivalTolerance(float arrivalTolerance2) {
        this.arrivalTolerance = arrivalTolerance2;
        return this;
    }

    public float getDecelerationRadius() {
        return this.decelerationRadius;
    }

    public Arrive<T> setDecelerationRadius(float decelerationRadius2) {
        this.decelerationRadius = decelerationRadius2;
        return this;
    }

    public float getTimeToTarget() {
        return this.timeToTarget;
    }

    public Arrive<T> setTimeToTarget(float timeToTarget2) {
        this.timeToTarget = timeToTarget2;
        return this;
    }

    public Arrive<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Arrive<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Arrive<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
