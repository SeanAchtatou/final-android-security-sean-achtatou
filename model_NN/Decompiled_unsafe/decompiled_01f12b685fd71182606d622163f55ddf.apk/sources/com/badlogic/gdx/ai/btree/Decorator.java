package com.badlogic.gdx.ai.btree;

public abstract class Decorator<E> extends Task<E> {
    public static final Metadata METADATA = new Metadata(1);
    protected Task<E> child;

    public Decorator() {
    }

    public Decorator(Task<E> child2) {
        this.child = child2;
    }

    public void addChild(Task<E> child2) {
        this.child = child2;
    }

    public int getChildCount() {
        return this.child == null ? 0 : 1;
    }

    public Task<E> getChild(int i) {
        return this.child;
    }

    public void run(E object) {
        this.object = object;
        this.child.run(object);
    }

    public void end(E object) {
        this.child.end(object);
    }

    public void start(E object) {
        this.child.setControl(this);
        this.child.start(object);
    }

    public void childRunning(Task<E> runningTask, Task<E> task) {
        this.control.childRunning(runningTask, this);
    }

    public void childFail(Task<E> task) {
        this.control.childFail(this);
    }

    public void childSuccess(Task<E> task) {
        this.control.childSuccess(this);
    }

    /* access modifiers changed from: protected */
    public Task<E> copyTo(Task<E> task) {
        ((Decorator) task).child = this.child.cloneTask();
        return task;
    }
}
