package com.badlogic.gdx.ai.btree;

public abstract class LeafTask<E> extends Task<E> {
    public void addChild(Task<E> task) {
    }

    public int getChildCount() {
        return 0;
    }

    public Task<E> getChild(int i) {
        throw new ArrayIndexOutOfBoundsException("A leaf task can not have any child");
    }

    public final void childRunning(Task<E> task, Task<E> task2) {
    }

    public final void childFail(Task<E> task) {
    }

    public final void childSuccess(Task<E> task) {
    }
}
