package com.badlogic.gdx.ai.utils;

import com.badlogic.gdx.math.Vector;

public interface RaycastCollisionDetector<T extends Vector<T>> {
    boolean collides(Ray<T> ray);

    boolean findCollision(Collision<T> collision, Ray<T> ray);
}
