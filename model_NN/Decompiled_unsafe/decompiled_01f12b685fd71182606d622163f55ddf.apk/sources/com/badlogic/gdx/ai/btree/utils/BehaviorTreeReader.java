package com.badlogic.gdx.ai.btree.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.SerializationException;
import com.badlogic.gdx.utils.StreamUtils;
import com.sg.GameSprites.GameSpriteType;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public abstract class BehaviorTreeReader {
    private static final byte[] _btree_actions = init__btree_actions_0();
    private static final byte[] _btree_eof_actions = init__btree_eof_actions_0();
    private static final byte[] _btree_index_offsets = init__btree_index_offsets_0();
    private static final byte[] _btree_indicies = init__btree_indicies_0();
    private static final byte[] _btree_key_offsets = init__btree_key_offsets_0();
    private static final byte[] _btree_range_lengths = init__btree_range_lengths_0();
    private static final byte[] _btree_single_lengths = init__btree_single_lengths_0();
    private static final byte[] _btree_trans_actions = init__btree_trans_actions_0();
    private static final char[] _btree_trans_keys = init__btree_trans_keys_0();
    private static final byte[] _btree_trans_targs = init__btree_trans_targs_0();
    static final int btree_en_main = 4;
    static final int btree_error = 0;
    static final int btree_first_final = 4;
    static final int btree_start = 4;
    public boolean debug = false;
    protected int lineNumber;

    /* access modifiers changed from: protected */
    public abstract void attribute(String str, Object obj);

    /* access modifiers changed from: protected */
    public abstract void endStatement();

    /* access modifiers changed from: protected */
    public abstract void startStatement(int i, String str);

    public void parse(String string) {
        char[] data = string.toCharArray();
        parse(data, 0, data.length);
    }

    public void parse(Reader reader) {
        try {
            char[] data = new char[1024];
            int offset = 0;
            while (true) {
                int length = reader.read(data, offset, data.length - offset);
                if (length == -1) {
                    parse(data, 0, offset);
                    StreamUtils.closeQuietly(reader);
                    return;
                } else if (length == 0) {
                    char[] newData = new char[(data.length * 2)];
                    System.arraycopy(data, 0, newData, 0, data.length);
                    data = newData;
                } else {
                    offset += length;
                }
            }
        } catch (IOException ex) {
            throw new SerializationException(ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(reader);
            throw th;
        }
    }

    public void parse(InputStream input) {
        try {
            parse(new InputStreamReader(input, "UTF-8"));
            StreamUtils.closeQuietly(input);
        } catch (IOException ex) {
            throw new SerializationException(ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(input);
            throw th;
        }
    }

    public void parse(FileHandle file) {
        try {
            parse(file.reader("UTF-8"));
        } catch (Exception ex) {
            throw new SerializationException("Error parsing file: " + file, ex);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c2 A[Catch:{ NumberFormatException -> 0x06bf, RuntimeException -> 0x0650 }] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0039 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void parse(char[] r43, int r44, int r45) {
        /*
            r42 = this;
            r28 = r44
            r30 = r45
            r23 = r30
            r31 = 0
            r25 = 0
            r32 = 0
            r35 = 0
            r26 = 0
            r34 = 0
            r29 = 0
            r19 = 0
            r37 = 1
            r0 = r37
            r1 = r42
            r1.lineNumber = r0
            r0 = r42
            boolean r0 = r0.debug
            r37 = r0
            if (r37 == 0) goto L_0x002b
            java.io.PrintStream r37 = java.lang.System.out
            r37.println()
        L_0x002b:
            r22 = 4
            r17 = 0
            r10 = 0
        L_0x0030:
            switch(r10) {
                case 0: goto L_0x007b;
                case 1: goto L_0x0087;
                case 2: goto L_0x0551;
                case 3: goto L_0x0033;
                case 4: goto L_0x0561;
                default: goto L_0x0033;
            }
        L_0x0033:
            r0 = r28
            r1 = r30
            if (r0 < r1) goto L_0x003d
            if (r32 == 0) goto L_0x0801
            if (r35 != 0) goto L_0x0801
        L_0x003d:
            com.badlogic.gdx.utils.SerializationException r37 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            r38.<init>()
            java.lang.String r39 = "Error parsing behavior tree on line "
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r42
            int r0 = r0.lineNumber
            r39 = r0
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = " near: "
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = new java.lang.String
            int r40 = r30 - r28
            r0 = r39
            r1 = r43
            r2 = r28
            r3 = r40
            r0.<init>(r1, r2, r3)
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r37
            r1 = r38
            r2 = r29
            r0.<init>(r1, r2)
            throw r37
        L_0x007b:
            r0 = r28
            r1 = r30
            if (r0 != r1) goto L_0x0083
            r10 = 4
            goto L_0x0030
        L_0x0083:
            if (r22 != 0) goto L_0x0087
            r10 = 5
            goto L_0x0030
        L_0x0087:
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_key_offsets     // Catch:{ RuntimeException -> 0x0650 }
            byte r11 = r37[r22]     // Catch:{ RuntimeException -> 0x0650 }
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_index_offsets     // Catch:{ RuntimeException -> 0x0650 }
            byte r17 = r37[r22]     // Catch:{ RuntimeException -> 0x0650 }
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_single_lengths     // Catch:{ RuntimeException -> 0x0650 }
            byte r12 = r37[r22]     // Catch:{ RuntimeException -> 0x0650 }
            if (r12 <= 0) goto L_0x00a1
            r13 = r11
            int r37 = r11 + r12
            int r18 = r37 + -1
        L_0x009a:
            r0 = r18
            if (r0 >= r13) goto L_0x00eb
            int r11 = r11 + r12
            int r17 = r17 + r12
        L_0x00a1:
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_range_lengths     // Catch:{ RuntimeException -> 0x0650 }
            byte r12 = r37[r22]     // Catch:{ RuntimeException -> 0x0650 }
            if (r12 <= 0) goto L_0x00b4
            r13 = r11
            int r37 = r12 << 1
            int r37 = r37 + r11
            int r18 = r37 + -2
        L_0x00ae:
            r0 = r18
            if (r0 >= r13) goto L_0x0114
            int r17 = r17 + r12
        L_0x00b4:
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_indicies     // Catch:{ RuntimeException -> 0x0650 }
            byte r17 = r37[r17]     // Catch:{ RuntimeException -> 0x0650 }
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_targs     // Catch:{ RuntimeException -> 0x0650 }
            byte r22 = r37[r17]     // Catch:{ RuntimeException -> 0x0650 }
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_actions     // Catch:{ RuntimeException -> 0x0650 }
            byte r37 = r37[r17]     // Catch:{ RuntimeException -> 0x0650 }
            if (r37 == 0) goto L_0x0551
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_actions     // Catch:{ RuntimeException -> 0x0650 }
            byte r8 = r37[r17]     // Catch:{ RuntimeException -> 0x0650 }
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_actions     // Catch:{ RuntimeException -> 0x0650 }
            int r9 = r8 + 1
            byte r15 = r37[r8]     // Catch:{ RuntimeException -> 0x0650 }
            r16 = r15
            r20 = r19
            r33 = r32
        L_0x00d2:
            int r15 = r16 + -1
            if (r16 <= 0) goto L_0x054d
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_actions     // Catch:{ RuntimeException -> 0x0217 }
            int r8 = r9 + 1
            byte r37 = r37[r9]     // Catch:{ RuntimeException -> 0x0217 }
            switch(r37) {
                case 0: goto L_0x0145;
                case 1: goto L_0x0322;
                case 2: goto L_0x034f;
                case 3: goto L_0x037e;
                case 4: goto L_0x03bc;
                case 5: goto L_0x03c4;
                case 6: goto L_0x0463;
                case 7: goto L_0x0478;
                case 8: goto L_0x04eb;
                default: goto L_0x00df;
            }
        L_0x00df:
            r19 = r20
            r32 = r33
        L_0x00e3:
            r16 = r15
            r9 = r8
            r20 = r19
            r33 = r32
            goto L_0x00d2
        L_0x00eb:
            int r37 = r18 - r13
            int r37 = r37 >> 1
            int r14 = r13 + r37
            char r37 = r43[r28]     // Catch:{ RuntimeException -> 0x0650 }
            char[] r38 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_keys     // Catch:{ RuntimeException -> 0x0650 }
            char r38 = r38[r14]     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r38
            if (r0 >= r1) goto L_0x0100
            int r18 = r14 + -1
            goto L_0x009a
        L_0x0100:
            char r37 = r43[r28]     // Catch:{ RuntimeException -> 0x0650 }
            char[] r38 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_keys     // Catch:{ RuntimeException -> 0x0650 }
            char r38 = r38[r14]     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r38
            if (r0 <= r1) goto L_0x010f
            int r13 = r14 + 1
            goto L_0x009a
        L_0x010f:
            int r37 = r14 - r11
            int r17 = r17 + r37
            goto L_0x00b4
        L_0x0114:
            int r37 = r18 - r13
            int r37 = r37 >> 1
            r37 = r37 & -2
            int r14 = r13 + r37
            char r37 = r43[r28]     // Catch:{ RuntimeException -> 0x0650 }
            char[] r38 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_keys     // Catch:{ RuntimeException -> 0x0650 }
            char r38 = r38[r14]     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r38
            if (r0 >= r1) goto L_0x012b
            int r18 = r14 + -2
            goto L_0x00ae
        L_0x012b:
            char r37 = r43[r28]     // Catch:{ RuntimeException -> 0x0650 }
            char[] r38 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_trans_keys     // Catch:{ RuntimeException -> 0x0650 }
            int r39 = r14 + 1
            char r38 = r38[r39]     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r38
            if (r0 <= r1) goto L_0x013d
            int r13 = r14 + 2
            goto L_0x00ae
        L_0x013d:
            int r37 = r14 - r11
            int r37 = r37 >> 1
            int r17 = r17 + r37
            goto L_0x00b4
        L_0x0145:
            java.lang.String r36 = new java.lang.String     // Catch:{ RuntimeException -> 0x0217 }
            int r37 = r28 - r31
            r0 = r36
            r1 = r43
            r2 = r31
            r3 = r37
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0217 }
            r31 = r28
            if (r26 == 0) goto L_0x0160
            r0 = r42
            r1 = r36
            java.lang.String r36 = r0.unescape(r1)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x0160:
            if (r34 == 0) goto L_0x02df
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x0194
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0217 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "string: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "="
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r36
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0217 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x0194:
            java.lang.String r37 = "true"
            boolean r37 = r36.equals(r37)     // Catch:{ RuntimeException -> 0x0217 }
            if (r37 == 0) goto L_0x01d9
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x01c6
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0217 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "boolean: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "=true"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0217 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x01c6:
            java.lang.Boolean r37 = java.lang.Boolean.TRUE     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r42
            r1 = r20
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x01d1:
            r34 = 0
            r19 = r20
            r32 = r33
            goto L_0x00e3
        L_0x01d9:
            java.lang.String r37 = "false"
            boolean r37 = r36.equals(r37)     // Catch:{ RuntimeException -> 0x0217 }
            if (r37 == 0) goto L_0x0220
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x020b
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0217 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "boolean: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "=false"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0217 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x020b:
            java.lang.Boolean r37 = java.lang.Boolean.FALSE     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r42
            r1 = r20
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0217 }
            goto L_0x01d1
        L_0x0217:
            r24 = move-exception
            r19 = r20
            r32 = r33
        L_0x021c:
            r29 = r24
            goto L_0x0033
        L_0x0220:
            java.lang.String r37 = "null"
            boolean r37 = r36.equals(r37)     // Catch:{ RuntimeException -> 0x0217 }
            if (r37 == 0) goto L_0x0234
            r37 = 0
            r0 = r42
            r1 = r20
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0217 }
            goto L_0x01d1
        L_0x0234:
            r37 = 46
            int r37 = r36.indexOf(r37)     // Catch:{ NumberFormatException -> 0x028c }
            r38 = -1
            r0 = r37
            r1 = r38
            if (r0 == r1) goto L_0x0295
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ NumberFormatException -> 0x028c }
            r37 = r0
            if (r37 == 0) goto L_0x0278
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x028c }
            r38.<init>()     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.String r39 = "double: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.String r39 = "="
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x028c }
            double r40 = java.lang.Double.parseDouble(r36)     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r38
            r1 = r40
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.String r38 = r38.toString()     // Catch:{ NumberFormatException -> 0x028c }
            r37.println(r38)     // Catch:{ NumberFormatException -> 0x028c }
        L_0x0278:
            java.lang.Double r37 = new java.lang.Double     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r37
            r1 = r36
            r0.<init>(r1)     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r42
            r1 = r20
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ NumberFormatException -> 0x028c }
            goto L_0x01d1
        L_0x028c:
            r27 = move-exception
            com.badlogic.gdx.utils.GdxRuntimeException r37 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = "Attribute value must be a number, a boolean, a string or null"
            r37.<init>(r38)     // Catch:{ RuntimeException -> 0x0217 }
            throw r37     // Catch:{ RuntimeException -> 0x0217 }
        L_0x0295:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ NumberFormatException -> 0x028c }
            r37 = r0
            if (r37 == 0) goto L_0x02cb
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x028c }
            r38.<init>()     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.String r39 = "double: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.String r39 = "="
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x028c }
            double r40 = java.lang.Double.parseDouble(r36)     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r38
            r1 = r40
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x028c }
            java.lang.String r38 = r38.toString()     // Catch:{ NumberFormatException -> 0x028c }
            r37.println(r38)     // Catch:{ NumberFormatException -> 0x028c }
        L_0x02cb:
            java.lang.Long r37 = new java.lang.Long     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r37
            r1 = r36
            r0.<init>(r1)     // Catch:{ NumberFormatException -> 0x028c }
            r0 = r42
            r1 = r20
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ NumberFormatException -> 0x028c }
            goto L_0x01d1
        L_0x02df:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x0317
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0217 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "string: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "=\""
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r36
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "\""
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0217 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x0317:
            r0 = r42
            r1 = r20
            r2 = r36
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0217 }
            goto L_0x01d1
        L_0x0322:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x0331
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = "unquotedChars"
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x0331:
            r31 = r28
            r26 = 0
            r34 = 1
        L_0x0337:
            char r37 = r43[r28]     // Catch:{ RuntimeException -> 0x0217 }
            switch(r37) {
                case 9: goto L_0x0344;
                case 10: goto L_0x0344;
                case 13: goto L_0x0344;
                case 32: goto L_0x0344;
                case 92: goto L_0x034c;
                default: goto L_0x033c;
            }     // Catch:{ RuntimeException -> 0x0217 }
        L_0x033c:
            int r28 = r28 + 1
            r0 = r28
            r1 = r23
            if (r0 != r1) goto L_0x0337
        L_0x0344:
            int r28 = r28 + -1
            r19 = r20
            r32 = r33
            goto L_0x00e3
        L_0x034c:
            r26 = 1
            goto L_0x033c
        L_0x034f:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x035e
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = "quotedChars"
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
        L_0x035e:
            int r28 = r28 + 1
            r31 = r28
            r26 = 0
        L_0x0364:
            char r37 = r43[r28]     // Catch:{ RuntimeException -> 0x0217 }
            switch(r37) {
                case 34: goto L_0x0371;
                case 92: goto L_0x0379;
                default: goto L_0x0369;
            }
        L_0x0369:
            int r28 = r28 + 1
            r0 = r28
            r1 = r23
            if (r0 != r1) goto L_0x0364
        L_0x0371:
            int r28 = r28 + -1
            r19 = r20
            r32 = r33
            goto L_0x00e3
        L_0x0379:
            r26 = 1
            int r28 = r28 + 1
            goto L_0x0369
        L_0x037e:
            r25 = 0
            r32 = 0
            r35 = 0
            r0 = r42
            int r0 = r0.lineNumber     // Catch:{ RuntimeException -> 0x082c }
            r37 = r0
            int r37 = r37 + 1
            r0 = r37
            r1 = r42
            r1.lineNumber = r0     // Catch:{ RuntimeException -> 0x082c }
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x082c }
            r37 = r0
            if (r37 == 0) goto L_0x0831
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x082c }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x082c }
            r38.<init>()     // Catch:{ RuntimeException -> 0x082c }
            java.lang.String r39 = "****NEWLINE**** "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x082c }
            r0 = r42
            int r0 = r0.lineNumber     // Catch:{ RuntimeException -> 0x082c }
            r39 = r0
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x082c }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x082c }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x082c }
            r19 = r20
            goto L_0x00e3
        L_0x03bc:
            int r25 = r25 + 1
            r19 = r20
            r32 = r33
            goto L_0x00e3
        L_0x03c4:
            r35 = 1
            if (r33 == 0) goto L_0x03cb
            r42.endStatement()     // Catch:{ RuntimeException -> 0x0217 }
        L_0x03cb:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x00df
            java.io.PrintStream r38 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0217 }
            r37.<init>()     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "endLine: indent: "
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r37
            r1 = r25
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = " taskName: "
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r37
            r1 = r33
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = " data["
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r37
            r1 = r28
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r39 = "] = "
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r39 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r28
            r1 = r23
            if (r0 < r1) goto L_0x043b
            java.lang.String r37 = "EOF"
        L_0x0422:
            r0 = r39
            r1 = r37
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r37 = r37.toString()     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r38
            r1 = r37
            r0.println(r1)     // Catch:{ RuntimeException -> 0x0217 }
            r19 = r20
            r32 = r33
            goto L_0x00e3
        L_0x043b:
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0217 }
            r37.<init>()     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r40 = "\""
            r0 = r37
            r1 = r40
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            char r40 = r43[r28]     // Catch:{ RuntimeException -> 0x0217 }
            r0 = r37
            r1 = r40
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r40 = "\""
            r0 = r37
            r1 = r40
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r37 = r37.toString()     // Catch:{ RuntimeException -> 0x0217 }
            goto L_0x0422
        L_0x0463:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0217 }
            r37 = r0
            if (r37 == 0) goto L_0x00df
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0217 }
            java.lang.String r38 = "# Comment"
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0217 }
            r19 = r20
            r32 = r33
            goto L_0x00e3
        L_0x0478:
            r31 = r28
        L_0x047a:
            int r28 = r28 + 1
            r0 = r28
            r1 = r23
            if (r0 != r1) goto L_0x04a0
        L_0x0482:
            java.lang.String r32 = new java.lang.String     // Catch:{ RuntimeException -> 0x0217 }
            int r37 = r28 - r31
            r0 = r32
            r1 = r43
            r2 = r31
            r3 = r37
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0217 }
            int r28 = r28 + -1
            r0 = r42
            r1 = r25
            r2 = r32
            r0.startStatement(r1, r2)     // Catch:{ RuntimeException -> 0x082c }
            r19 = r20
            goto L_0x00e3
        L_0x04a0:
            char r21 = r43[r28]     // Catch:{ RuntimeException -> 0x0217 }
            r37 = 97
            r0 = r21
            r1 = r37
            if (r0 < r1) goto L_0x04b2
            r37 = 122(0x7a, float:1.71E-43)
            r0 = r21
            r1 = r37
            if (r0 <= r1) goto L_0x047a
        L_0x04b2:
            r37 = 65
            r0 = r21
            r1 = r37
            if (r0 < r1) goto L_0x04c2
            r37 = 90
            r0 = r21
            r1 = r37
            if (r0 <= r1) goto L_0x047a
        L_0x04c2:
            r37 = 48
            r0 = r21
            r1 = r37
            if (r0 < r1) goto L_0x04d2
            r37 = 57
            r0 = r21
            r1 = r37
            if (r0 <= r1) goto L_0x047a
        L_0x04d2:
            r37 = 46
            r0 = r21
            r1 = r37
            if (r0 == r1) goto L_0x047a
            r37 = 95
            r0 = r21
            r1 = r37
            if (r0 == r1) goto L_0x047a
            r37 = 63
            r0 = r21
            r1 = r37
            if (r0 == r1) goto L_0x047a
            goto L_0x0482
        L_0x04eb:
            r31 = r28
        L_0x04ed:
            int r28 = r28 + 1
            r0 = r28
            r1 = r23
            if (r0 != r1) goto L_0x050a
        L_0x04f5:
            java.lang.String r19 = new java.lang.String     // Catch:{ RuntimeException -> 0x0217 }
            int r37 = r28 - r31
            r0 = r19
            r1 = r43
            r2 = r31
            r3 = r37
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0217 }
            int r28 = r28 + -1
            r32 = r33
            goto L_0x00e3
        L_0x050a:
            char r21 = r43[r28]     // Catch:{ RuntimeException -> 0x0217 }
            r37 = 97
            r0 = r21
            r1 = r37
            if (r0 < r1) goto L_0x051c
            r37 = 122(0x7a, float:1.71E-43)
            r0 = r21
            r1 = r37
            if (r0 <= r1) goto L_0x04ed
        L_0x051c:
            r37 = 65
            r0 = r21
            r1 = r37
            if (r0 < r1) goto L_0x052c
            r37 = 90
            r0 = r21
            r1 = r37
            if (r0 <= r1) goto L_0x04ed
        L_0x052c:
            r37 = 48
            r0 = r21
            r1 = r37
            if (r0 < r1) goto L_0x053c
            r37 = 57
            r0 = r21
            r1 = r37
            if (r0 <= r1) goto L_0x04ed
        L_0x053c:
            r37 = 95
            r0 = r21
            r1 = r37
            if (r0 == r1) goto L_0x04ed
            r37 = 63
            r0 = r21
            r1 = r37
            if (r0 == r1) goto L_0x04ed
            goto L_0x04f5
        L_0x054d:
            r19 = r20
            r32 = r33
        L_0x0551:
            if (r22 != 0) goto L_0x0556
            r10 = 5
            goto L_0x0030
        L_0x0556:
            int r28 = r28 + 1
            r0 = r28
            r1 = r30
            if (r0 == r1) goto L_0x0561
            r10 = 1
            goto L_0x0030
        L_0x0561:
            r0 = r28
            r1 = r23
            if (r0 != r1) goto L_0x0033
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_eof_actions     // Catch:{ RuntimeException -> 0x0650 }
            byte r4 = r37[r22]     // Catch:{ RuntimeException -> 0x0650 }
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_actions     // Catch:{ RuntimeException -> 0x0650 }
            int r5 = r4 + 1
            byte r6 = r37[r4]     // Catch:{ RuntimeException -> 0x0650 }
            r7 = r6
        L_0x0572:
            int r6 = r7 + -1
            if (r7 <= 0) goto L_0x0033
            byte[] r37 = com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader._btree_actions     // Catch:{ RuntimeException -> 0x0650 }
            int r4 = r5 + 1
            byte r37 = r37[r5]     // Catch:{ RuntimeException -> 0x0650 }
            switch(r37) {
                case 0: goto L_0x0582;
                case 5: goto L_0x0755;
                case 6: goto L_0x07f0;
                default: goto L_0x057f;
            }     // Catch:{ RuntimeException -> 0x0650 }
        L_0x057f:
            r7 = r6
            r5 = r4
            goto L_0x0572
        L_0x0582:
            java.lang.String r36 = new java.lang.String     // Catch:{ RuntimeException -> 0x0650 }
            int r37 = r28 - r31
            r0 = r36
            r1 = r43
            r2 = r31
            r3 = r37
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0650 }
            r31 = r28
            if (r26 == 0) goto L_0x059d
            r0 = r42
            r1 = r36
            java.lang.String r36 = r0.unescape(r1)     // Catch:{ RuntimeException -> 0x0650 }
        L_0x059d:
            if (r34 == 0) goto L_0x0712
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0650 }
            r37 = r0
            if (r37 == 0) goto L_0x05d1
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0650 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "string: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "="
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r36
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0650 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0650 }
        L_0x05d1:
            java.lang.String r37 = "true"
            boolean r37 = r36.equals(r37)     // Catch:{ RuntimeException -> 0x0650 }
            if (r37 == 0) goto L_0x0612
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0650 }
            r37 = r0
            if (r37 == 0) goto L_0x0603
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0650 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "boolean: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "=true"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0650 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0650 }
        L_0x0603:
            java.lang.Boolean r37 = java.lang.Boolean.TRUE     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r42
            r1 = r19
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0650 }
        L_0x060e:
            r34 = 0
            goto L_0x057f
        L_0x0612:
            java.lang.String r37 = "false"
            boolean r37 = r36.equals(r37)     // Catch:{ RuntimeException -> 0x0650 }
            if (r37 == 0) goto L_0x0653
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0650 }
            r37 = r0
            if (r37 == 0) goto L_0x0644
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0650 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "boolean: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "=false"
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0650 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0650 }
        L_0x0644:
            java.lang.Boolean r37 = java.lang.Boolean.FALSE     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r42
            r1 = r19
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0650 }
            goto L_0x060e
        L_0x0650:
            r24 = move-exception
            goto L_0x021c
        L_0x0653:
            java.lang.String r37 = "null"
            boolean r37 = r36.equals(r37)     // Catch:{ RuntimeException -> 0x0650 }
            if (r37 == 0) goto L_0x0667
            r37 = 0
            r0 = r42
            r1 = r19
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0650 }
            goto L_0x060e
        L_0x0667:
            r37 = 46
            int r37 = r36.indexOf(r37)     // Catch:{ NumberFormatException -> 0x06bf }
            r38 = -1
            r0 = r37
            r1 = r38
            if (r0 == r1) goto L_0x06c8
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ NumberFormatException -> 0x06bf }
            r37 = r0
            if (r37 == 0) goto L_0x06ab
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x06bf }
            r38.<init>()     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.String r39 = "double: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.String r39 = "="
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x06bf }
            double r40 = java.lang.Double.parseDouble(r36)     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r38
            r1 = r40
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.String r38 = r38.toString()     // Catch:{ NumberFormatException -> 0x06bf }
            r37.println(r38)     // Catch:{ NumberFormatException -> 0x06bf }
        L_0x06ab:
            java.lang.Double r37 = new java.lang.Double     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r37
            r1 = r36
            r0.<init>(r1)     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r42
            r1 = r19
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ NumberFormatException -> 0x06bf }
            goto L_0x060e
        L_0x06bf:
            r27 = move-exception
            com.badlogic.gdx.utils.GdxRuntimeException r37 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r38 = "Attribute value must be a number, a boolean, a string or null"
            r37.<init>(r38)     // Catch:{ RuntimeException -> 0x0650 }
            throw r37     // Catch:{ RuntimeException -> 0x0650 }
        L_0x06c8:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ NumberFormatException -> 0x06bf }
            r37 = r0
            if (r37 == 0) goto L_0x06fe
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x06bf }
            r38.<init>()     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.String r39 = "double: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.String r39 = "="
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ NumberFormatException -> 0x06bf }
            double r40 = java.lang.Double.parseDouble(r36)     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r38
            r1 = r40
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x06bf }
            java.lang.String r38 = r38.toString()     // Catch:{ NumberFormatException -> 0x06bf }
            r37.println(r38)     // Catch:{ NumberFormatException -> 0x06bf }
        L_0x06fe:
            java.lang.Long r37 = new java.lang.Long     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r37
            r1 = r36
            r0.<init>(r1)     // Catch:{ NumberFormatException -> 0x06bf }
            r0 = r42
            r1 = r19
            r2 = r37
            r0.attribute(r1, r2)     // Catch:{ NumberFormatException -> 0x06bf }
            goto L_0x060e
        L_0x0712:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0650 }
            r37 = r0
            if (r37 == 0) goto L_0x074a
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.StringBuilder r38 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0650 }
            r38.<init>()     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "string: "
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "=\""
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r36
            java.lang.StringBuilder r38 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "\""
            java.lang.StringBuilder r38 = r38.append(r39)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r38 = r38.toString()     // Catch:{ RuntimeException -> 0x0650 }
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0650 }
        L_0x074a:
            r0 = r42
            r1 = r19
            r2 = r36
            r0.attribute(r1, r2)     // Catch:{ RuntimeException -> 0x0650 }
            goto L_0x060e
        L_0x0755:
            r35 = 1
            if (r32 == 0) goto L_0x075c
            r42.endStatement()     // Catch:{ RuntimeException -> 0x0650 }
        L_0x075c:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0650 }
            r37 = r0
            if (r37 == 0) goto L_0x057f
            java.io.PrintStream r38 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0650 }
            r37.<init>()     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "endLine: indent: "
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r25
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = " taskName: "
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r32
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = " data["
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r28
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r39 = "] = "
            r0 = r37
            r1 = r39
            java.lang.StringBuilder r39 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r28
            r1 = r23
            if (r0 < r1) goto L_0x07c8
            java.lang.String r37 = "EOF"
        L_0x07b3:
            r0 = r39
            r1 = r37
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r37 = r37.toString()     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r38
            r1 = r37
            r0.println(r1)     // Catch:{ RuntimeException -> 0x0650 }
            goto L_0x057f
        L_0x07c8:
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0650 }
            r37.<init>()     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r40 = "\""
            r0 = r37
            r1 = r40
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            char r40 = r43[r28]     // Catch:{ RuntimeException -> 0x0650 }
            r0 = r37
            r1 = r40
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r40 = "\""
            r0 = r37
            r1 = r40
            java.lang.StringBuilder r37 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r37 = r37.toString()     // Catch:{ RuntimeException -> 0x0650 }
            goto L_0x07b3
        L_0x07f0:
            r0 = r42
            boolean r0 = r0.debug     // Catch:{ RuntimeException -> 0x0650 }
            r37 = r0
            if (r37 == 0) goto L_0x057f
            java.io.PrintStream r37 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0650 }
            java.lang.String r38 = "# Comment"
            r37.println(r38)     // Catch:{ RuntimeException -> 0x0650 }
            goto L_0x057f
        L_0x0801:
            if (r29 == 0) goto L_0x082b
            com.badlogic.gdx.utils.SerializationException r37 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            r38.<init>()
            java.lang.String r39 = "Error parsing behavior tree: "
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r39 = new java.lang.String
            r0 = r39
            r1 = r43
            r0.<init>(r1)
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r38 = r38.toString()
            r0 = r37
            r1 = r38
            r2 = r29
            r0.<init>(r1, r2)
            throw r37
        L_0x082b:
            return
        L_0x082c:
            r24 = move-exception
            r19 = r20
            goto L_0x021c
        L_0x0831:
            r19 = r20
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.btree.utils.BehaviorTreeReader.parse(char[], int, int):void");
    }

    private static byte[] init__btree_actions_0() {
        return new byte[]{0, 1, 0, 1, 1, 1, 2, 1, 4, 1, 5, 1, 7, 1, 8, 2, 0, 5, 2, 5, 3, 2, 6, 5, 3, 0, 5, 3, 3, 6, 5, 3};
    }

    private static byte[] init__btree_key_offsets_0() {
        return new byte[]{0, 0, 4, 11, 12, 22, 27, 28, GameSpriteType.f41TYPE_ENEMY_, 43};
    }

    private static char[] init__btree_trans_keys_0() {
        return new char[]{9, 13, ' ', ':', 9, 10, 13, ' ', '\"', '#', ':', '\"', 9, 10, 13, ' ', '#', '_', 'A', 'Z', 'a', 'z', 9, 10, 13, ' ', '#', 10, 9, 10, 13, ' ', '#', 9, 10, 13, ' ', '#', '_', 'A', 'Z', 'a', 'z', 9, 10, 13, ' ', '#', 0};
    }

    private static byte[] init__btree_single_lengths_0() {
        return new byte[]{0, 4, 7, 1, 6, 5, 1, 5, 6, 5};
    }

    private static byte[] init__btree_range_lengths_0() {
        return new byte[]{0, 0, 0, 0, 2, 0, 0, 0, 2, 0};
    }

    private static byte[] init__btree_index_offsets_0() {
        return new byte[]{0, 0, 5, 13, 15, 24, 30, 32, 38, GameSpriteType.f2TYPE_BOSS_1};
    }

    private static byte[] init__btree_indicies_0() {
        return new byte[]{0, 0, 0, 2, 1, 2, 1, 2, 2, 4, 1, 1, 3, 5, 1, 6, 7, 8, 6, 9, 10, 10, 10, 1, 8, 7, 8, 8, 9, 1, 11, 9, 12, 7, 12, 12, 9, 1, 12, 7, 12, 12, 9, 13, 13, 13, 1, 14, 15, 14, 14, 16, 1, 0};
    }

    private static byte[] init__btree_trans_targs_0() {
        return new byte[]{1, 0, 2, 9, 3, 7, 4, 4, 5, 6, 7, 4, 8, 1, 8, 4, 6};
    }

    private static byte[] init__btree_trans_actions_0() {
        return new byte[]{0, 0, 0, 3, 5, 1, 7, 18, 0, 0, 11, 28, 0, 13, 1, 24, 1};
    }

    private static byte[] init__btree_eof_actions_0() {
        return new byte[]{0, 0, 0, 0, 9, 9, 21, 9, 9, 15};
    }

    private String unescape(String value) {
        int length = value.length();
        StringBuilder buffer = new StringBuilder(length + 16);
        int i = 0;
        while (true) {
            if (i < length) {
                int i2 = i + 1;
                char c = value.charAt(i);
                if (c != '\\') {
                    buffer.append(c);
                    i = i2;
                } else if (i2 != length) {
                    i = i2 + 1;
                    char c2 = value.charAt(i2);
                    if (c2 == 'u') {
                        buffer.append(Character.toChars(Integer.parseInt(value.substring(i, i + 4), 16)));
                        i += 4;
                    } else {
                        switch (c2) {
                            case '\"':
                            case '/':
                            case '\\':
                                break;
                            case 'b':
                                c2 = 8;
                                break;
                            case 'f':
                                c2 = 12;
                                break;
                            case 'n':
                                c2 = 10;
                                break;
                            case 'r':
                                c2 = 13;
                                break;
                            case 't':
                                c2 = 9;
                                break;
                            default:
                                throw new SerializationException("Illegal escaped character: \\" + c2);
                        }
                        buffer.append(c2);
                    }
                }
            }
        }
        return buffer.toString();
    }
}
