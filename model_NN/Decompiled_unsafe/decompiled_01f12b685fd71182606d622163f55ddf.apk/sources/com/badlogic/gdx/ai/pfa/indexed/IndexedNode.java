package com.badlogic.gdx.ai.pfa.indexed;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;

public interface IndexedNode<N extends IndexedNode<N>> {
    Array<Connection<N>> getConnections();

    int getIndex();
}
