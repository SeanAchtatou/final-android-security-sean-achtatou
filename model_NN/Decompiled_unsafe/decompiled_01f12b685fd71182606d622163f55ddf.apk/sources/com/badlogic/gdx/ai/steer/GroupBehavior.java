package com.badlogic.gdx.ai.steer;

import com.badlogic.gdx.math.Vector;

public abstract class GroupBehavior<T extends Vector<T>> extends SteeringBehavior<T> {
    protected Proximity<T> proximity;

    public GroupBehavior(Steerable<T> owner, Proximity<T> proximity2) {
        super(owner);
        this.proximity = proximity2;
    }

    public Proximity<T> getProximity() {
        return this.proximity;
    }

    public void setProximity(Proximity<T> proximity2) {
        this.proximity = proximity2;
    }
}
