package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.utils.Path;
import com.badlogic.gdx.ai.steer.utils.Path.PathParam;
import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class FollowPath<T extends Vector<T>, P extends Path.PathParam> extends Arrive<T> {
    protected boolean arriveEnabled;
    private T internalTargetPosition;
    protected Path<T, P> path;
    protected float pathOffset;
    protected P pathParam;
    protected float predictionTime;

    public FollowPath(Steerable<T> owner, Path<T, P> path2) {
        this(owner, path2, Animation.CurveTimeline.LINEAR);
    }

    public FollowPath(Steerable<T> owner, Path<T, P> path2, float pathOffset2) {
        this(owner, path2, pathOffset2, Animation.CurveTimeline.LINEAR);
    }

    public FollowPath(Steerable<T> owner, Path<T, P> path2, float pathOffset2, float predictionTime2) {
        super(owner);
        this.path = path2;
        this.pathParam = path2.createParam();
        this.pathOffset = pathOffset2;
        this.predictionTime = predictionTime2;
        this.arriveEnabled = true;
        this.internalTargetPosition = owner.newVector();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r8) {
        /*
            r7 = this;
            r6 = 0
            float r3 = r7.predictionTime
            int r3 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r3 != 0) goto L_0x0048
            com.badlogic.gdx.ai.steer.Steerable r3 = r7.owner
            com.badlogic.gdx.math.Vector r1 = r3.getPosition()
        L_0x000d:
            com.badlogic.gdx.ai.steer.utils.Path<T, P> r3 = r7.path
            P r4 = r7.pathParam
            float r0 = r3.calculateDistance(r1, r4)
            float r3 = r7.pathOffset
            float r2 = r0 + r3
            com.badlogic.gdx.ai.steer.utils.Path<T, P> r3 = r7.path
            T r4 = r7.internalTargetPosition
            P r5 = r7.pathParam
            r3.calculateTargetPosition(r4, r5, r2)
            boolean r3 = r7.arriveEnabled
            if (r3 == 0) goto L_0x006e
            com.badlogic.gdx.ai.steer.utils.Path<T, P> r3 = r7.path
            boolean r3 = r3.isOpen()
            if (r3 == 0) goto L_0x006e
            float r3 = r7.pathOffset
            int r3 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r3 < 0) goto L_0x0061
            com.badlogic.gdx.ai.steer.utils.Path<T, P> r3 = r7.path
            float r3 = r3.getLength()
            float r4 = r7.decelerationRadius
            float r3 = r3 - r4
            int r3 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r3 <= 0) goto L_0x006e
            T r3 = r7.internalTargetPosition
            com.badlogic.gdx.ai.steer.SteeringAcceleration r8 = r7.arrive(r8, r3)
        L_0x0047:
            return r8
        L_0x0048:
            T r3 = r8.linear
            com.badlogic.gdx.ai.steer.Steerable r4 = r7.owner
            com.badlogic.gdx.math.Vector r4 = r4.getPosition()
            com.badlogic.gdx.math.Vector r3 = r3.set(r4)
            com.badlogic.gdx.ai.steer.Steerable r4 = r7.owner
            com.badlogic.gdx.math.Vector r4 = r4.getLinearVelocity()
            float r5 = r7.predictionTime
            com.badlogic.gdx.math.Vector r1 = r3.mulAdd(r4, r5)
            goto L_0x000d
        L_0x0061:
            float r3 = r7.decelerationRadius
            int r3 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x006e
            T r3 = r7.internalTargetPosition
            com.badlogic.gdx.ai.steer.SteeringAcceleration r8 = r7.arrive(r8, r3)
            goto L_0x0047
        L_0x006e:
            T r3 = r8.linear
            T r4 = r7.internalTargetPosition
            com.badlogic.gdx.math.Vector r3 = r3.set(r4)
            com.badlogic.gdx.ai.steer.Steerable r4 = r7.owner
            com.badlogic.gdx.math.Vector r4 = r4.getPosition()
            com.badlogic.gdx.math.Vector r3 = r3.sub(r4)
            com.badlogic.gdx.math.Vector r3 = r3.nor()
            com.badlogic.gdx.ai.steer.Limiter r4 = r7.getActualLimiter()
            float r4 = r4.getMaxLinearAcceleration()
            r3.scl(r4)
            r8.angular = r6
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.FollowPath.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public Path<T, P> getPath() {
        return this.path;
    }

    public FollowPath<T, P> setPath(Path<T, P> path2) {
        this.path = path2;
        return this;
    }

    public float getPathOffset() {
        return this.pathOffset;
    }

    public boolean isArriveEnabled() {
        return this.arriveEnabled;
    }

    public float getPredictionTime() {
        return this.predictionTime;
    }

    public FollowPath<T, P> setPredictionTime(float predictionTime2) {
        this.predictionTime = predictionTime2;
        return this;
    }

    public FollowPath<T, P> setArriveEnabled(boolean arriveEnabled2) {
        this.arriveEnabled = arriveEnabled2;
        return this;
    }

    public FollowPath<T, P> setPathOffset(float pathOffset2) {
        this.pathOffset = pathOffset2;
        return this;
    }

    public P getPathParam() {
        return this.pathParam;
    }

    public T getInternalTargetPosition() {
        return this.internalTargetPosition;
    }

    public FollowPath<T, P> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public FollowPath<T, P> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public FollowPath<T, P> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public FollowPath<T, P> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public FollowPath<T, P> setArrivalTolerance(float arrivalTolerance) {
        this.arrivalTolerance = arrivalTolerance;
        return this;
    }

    public FollowPath<T, P> setDecelerationRadius(float decelerationRadius) {
        this.decelerationRadius = decelerationRadius;
        return this;
    }

    public FollowPath<T, P> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }
}
