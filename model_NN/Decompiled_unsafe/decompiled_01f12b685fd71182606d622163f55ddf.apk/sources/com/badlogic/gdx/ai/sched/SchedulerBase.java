package com.badlogic.gdx.ai.sched;

import com.badlogic.gdx.ai.sched.SchedulerBase.SchedulableRecord;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;

public abstract class SchedulerBase<T extends SchedulableRecord> implements Scheduler {
    protected int dryRunFrames;
    protected IntArray phaseCounters = new IntArray();
    protected Array<T> runList = new Array<>();
    protected Array<T> schedulableRecords = new Array<>();

    public SchedulerBase(int dryRunFrames2) {
        this.dryRunFrames = dryRunFrames2;
    }

    /* access modifiers changed from: protected */
    public int calculatePhase(int frequency) {
        if (frequency > this.phaseCounters.size) {
            this.phaseCounters.ensureCapacity(frequency - this.phaseCounters.size);
        }
        int[] items = this.phaseCounters.items;
        this.phaseCounters.size = frequency;
        for (int i = 0; i < frequency; i++) {
            items[i] = 0;
        }
        for (int frame = 0; frame < this.dryRunFrames; frame++) {
            int slot = frame % frequency;
            for (int i2 = 0; i2 < this.schedulableRecords.size; i2++) {
                T record = (SchedulableRecord) this.schedulableRecords.get(i2);
                if ((frame - record.phase) % record.frequency == 0) {
                    items[slot] = items[slot] + 1;
                }
            }
        }
        int minValue = Integer.MAX_VALUE;
        int minValueAt = -1;
        for (int i3 = 0; i3 < frequency; i3++) {
            if (items[i3] < minValue) {
                minValue = items[i3];
                minValueAt = i3;
            }
        }
        return minValueAt;
    }

    protected static class SchedulableRecord {
        int frequency;
        int phase;
        Schedulable schedulable;

        SchedulableRecord(Schedulable schedulable2, int frequency2, int phase2) {
            this.schedulable = schedulable2;
            this.frequency = frequency2;
            this.phase = phase2;
        }
    }
}
