package com.badlogic.gdx.ai.fsm;

import com.badlogic.gdx.ai.msg.Telegram;

public class DefaultStateMachine<E> implements StateMachine<E> {
    protected State<E> currentState;
    protected State<E> globalState;
    protected E owner;
    protected State<E> previousState;

    public DefaultStateMachine(E owner2) {
        this(owner2, null, null);
    }

    public DefaultStateMachine(E owner2, State<E> initialState) {
        this(owner2, initialState, null);
    }

    public DefaultStateMachine(E owner2, State<E> initialState, State<E> globalState2) {
        this.owner = owner2;
        setInitialState(initialState);
        setGlobalState(globalState2);
    }

    public void setInitialState(State<E> state) {
        this.previousState = null;
        this.currentState = state;
    }

    public void setGlobalState(State<E> state) {
        this.globalState = state;
    }

    public State<E> getCurrentState() {
        return this.currentState;
    }

    public State<E> getGlobalState() {
        return this.globalState;
    }

    public State<E> getPreviousState() {
        return this.previousState;
    }

    public void update() {
        if (this.globalState != null) {
            this.globalState.update(this.owner);
        }
        if (this.currentState != null) {
            this.currentState.update(this.owner);
        }
    }

    public void changeState(State<E> newState) {
        this.previousState = this.currentState;
        if (this.currentState != null) {
            this.currentState.exit(this.owner);
        }
        this.currentState = newState;
        if (this.currentState != null) {
            this.currentState.enter(this.owner);
        }
    }

    public boolean revertToPreviousState() {
        if (this.previousState == null) {
            return false;
        }
        changeState(this.previousState);
        return true;
    }

    public boolean isInState(State<E> state) {
        return this.currentState == state;
    }

    public boolean handleMessage(Telegram telegram) {
        if (this.currentState != null && this.currentState.onMessage(this.owner, telegram)) {
            return true;
        }
        if (this.globalState == null || !this.globalState.onMessage(this.owner, telegram)) {
            return false;
        }
        return true;
    }
}
