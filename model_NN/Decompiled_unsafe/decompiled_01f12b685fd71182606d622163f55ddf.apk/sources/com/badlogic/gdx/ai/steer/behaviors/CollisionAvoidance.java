package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.GroupBehavior;
import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class CollisionAvoidance<T extends Vector<T>> extends GroupBehavior<T> implements Proximity.ProximityCallback<T> {
    private float firstDistance;
    private float firstMinSeparation;
    private Steerable<T> firstNeighbor;
    private T firstRelativePosition;
    private T firstRelativeVelocity;
    private T relativePosition;
    private T relativeVelocity;
    private float shortestTime;

    public CollisionAvoidance(Steerable<T> owner, Proximity<T> proximity) {
        super(owner, proximity);
        this.firstRelativePosition = owner.newVector();
        this.firstRelativeVelocity = owner.newVector();
        this.relativeVelocity = owner.newVector();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r6) {
        /*
            r5 = this;
            r4 = 0
            r1 = 2139095040(0x7f800000, float:Infinity)
            r5.shortestTime = r1
            r1 = 0
            r5.firstNeighbor = r1
            r5.firstMinSeparation = r4
            r5.firstDistance = r4
            T r1 = r6.linear
            r5.relativePosition = r1
            com.badlogic.gdx.ai.steer.Proximity r1 = r5.proximity
            int r0 = r1.findNeighbors(r5)
            if (r0 == 0) goto L_0x001c
            com.badlogic.gdx.ai.steer.Steerable<T> r1 = r5.firstNeighbor
            if (r1 != 0) goto L_0x0021
        L_0x001c:
            com.badlogic.gdx.ai.steer.SteeringAcceleration r6 = r6.setZero()
        L_0x0020:
            return r6
        L_0x0021:
            float r1 = r5.firstMinSeparation
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 <= 0) goto L_0x003a
            float r1 = r5.firstDistance
            com.badlogic.gdx.ai.steer.Steerable r2 = r5.owner
            float r2 = r2.getBoundingRadius()
            com.badlogic.gdx.ai.steer.Steerable<T> r3 = r5.firstNeighbor
            float r3 = r3.getBoundingRadius()
            float r2 = r2 + r3
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x0064
        L_0x003a:
            T r1 = r5.relativePosition
            com.badlogic.gdx.ai.steer.Steerable<T> r2 = r5.firstNeighbor
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            com.badlogic.gdx.ai.steer.Steerable r2 = r5.owner
            com.badlogic.gdx.math.Vector r2 = r2.getPosition()
            r1.sub(r2)
        L_0x004f:
            T r1 = r5.relativePosition
            com.badlogic.gdx.math.Vector r1 = r1.nor()
            com.badlogic.gdx.ai.steer.Limiter r2 = r5.getActualLimiter()
            float r2 = r2.getMaxLinearAcceleration()
            float r2 = -r2
            r1.scl(r2)
            r6.angular = r4
            goto L_0x0020
        L_0x0064:
            T r1 = r5.relativePosition
            T r2 = r5.firstRelativePosition
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            T r2 = r5.firstRelativeVelocity
            float r3 = r5.shortestTime
            r1.mulAdd(r2, r3)
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean reportNeighbor(com.badlogic.gdx.ai.steer.Steerable<T> r9) {
        /*
            r8 = this;
            r4 = 0
            T r5 = r8.relativePosition
            com.badlogic.gdx.math.Vector r6 = r9.getPosition()
            com.badlogic.gdx.math.Vector r5 = r5.set(r6)
            com.badlogic.gdx.ai.steer.Steerable r6 = r8.owner
            com.badlogic.gdx.math.Vector r6 = r6.getPosition()
            r5.sub(r6)
            T r5 = r8.relativeVelocity
            com.badlogic.gdx.math.Vector r6 = r9.getLinearVelocity()
            com.badlogic.gdx.math.Vector r5 = r5.set(r6)
            com.badlogic.gdx.ai.steer.Steerable r6 = r8.owner
            com.badlogic.gdx.math.Vector r6 = r6.getLinearVelocity()
            r5.sub(r6)
            T r5 = r8.relativeVelocity
            float r2 = r5.len2()
            T r5 = r8.relativePosition
            T r6 = r8.relativeVelocity
            float r5 = r5.dot(r6)
            float r5 = -r5
            float r3 = r5 / r2
            r5 = 0
            int r5 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r5 <= 0) goto L_0x0043
            float r5 = r8.shortestTime
            int r5 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r5 < 0) goto L_0x0044
        L_0x0043:
            return r4
        L_0x0044:
            T r5 = r8.relativePosition
            float r0 = r5.len()
            double r6 = (double) r2
            double r6 = java.lang.Math.sqrt(r6)
            float r5 = (float) r6
            float r5 = r5 * r3
            float r1 = r0 - r5
            com.badlogic.gdx.ai.steer.Steerable r5 = r8.owner
            float r5 = r5.getBoundingRadius()
            float r6 = r9.getBoundingRadius()
            float r5 = r5 + r6
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x0043
            r8.shortestTime = r3
            r8.firstNeighbor = r9
            r8.firstMinSeparation = r1
            r8.firstDistance = r0
            T r4 = r8.firstRelativePosition
            T r5 = r8.relativePosition
            r4.set(r5)
            T r4 = r8.firstRelativeVelocity
            T r5 = r8.relativeVelocity
            r4.set(r5)
            r4 = 1
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance.reportNeighbor(com.badlogic.gdx.ai.steer.Steerable):boolean");
    }

    public CollisionAvoidance<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public CollisionAvoidance<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public CollisionAvoidance<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }
}
