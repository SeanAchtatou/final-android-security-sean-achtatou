package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class Wander<T extends Vector<T>> extends Face<T> {
    protected boolean faceEnabled;
    private T internalTargetPosition;
    private T wanderCenter;
    protected float wanderOffset;
    protected float wanderOrientation;
    protected float wanderRadius;
    protected float wanderRate;

    public Wander(Steerable<T> owner) {
        super(owner);
        this.internalTargetPosition = owner.newVector();
        this.wanderCenter = owner.newVector();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r7) {
        /*
            r6 = this;
            float r2 = r6.wanderOrientation
            float r3 = r6.wanderRate
            float r3 = com.badlogic.gdx.math.MathUtils.randomTriangular(r3)
            float r2 = r2 + r3
            r6.wanderOrientation = r2
            float r2 = r6.wanderOrientation
            com.badlogic.gdx.ai.steer.Steerable r3 = r6.owner
            float r3 = r3.getOrientation()
            float r1 = r2 + r3
            T r2 = r6.wanderCenter
            com.badlogic.gdx.ai.steer.Steerable r3 = r6.owner
            com.badlogic.gdx.math.Vector r3 = r3.getPosition()
            com.badlogic.gdx.math.Vector r2 = r2.set(r3)
            com.badlogic.gdx.ai.steer.Steerable r3 = r6.owner
            T r4 = r7.linear
            com.badlogic.gdx.ai.steer.Steerable r5 = r6.owner
            float r5 = r5.getOrientation()
            com.badlogic.gdx.math.Vector r3 = r3.angleToVector(r4, r5)
            float r4 = r6.wanderOffset
            r2.mulAdd(r3, r4)
            T r2 = r6.internalTargetPosition
            T r3 = r6.wanderCenter
            com.badlogic.gdx.math.Vector r2 = r2.set(r3)
            com.badlogic.gdx.ai.steer.Steerable r3 = r6.owner
            T r4 = r7.linear
            com.badlogic.gdx.math.Vector r3 = r3.angleToVector(r4, r1)
            float r4 = r6.wanderRadius
            r2.mulAdd(r3, r4)
            com.badlogic.gdx.ai.steer.Limiter r2 = r6.getActualLimiter()
            float r0 = r2.getMaxLinearAcceleration()
            boolean r2 = r6.faceEnabled
            if (r2 == 0) goto L_0x006c
            T r2 = r6.internalTargetPosition
            r6.face(r7, r2)
            com.badlogic.gdx.ai.steer.Steerable r2 = r6.owner
            T r3 = r7.linear
            com.badlogic.gdx.ai.steer.Steerable r4 = r6.owner
            float r4 = r4.getOrientation()
            com.badlogic.gdx.math.Vector r2 = r2.angleToVector(r3, r4)
            r2.scl(r0)
        L_0x006b:
            return r7
        L_0x006c:
            T r2 = r7.linear
            T r3 = r6.internalTargetPosition
            com.badlogic.gdx.math.Vector r2 = r2.set(r3)
            com.badlogic.gdx.ai.steer.Steerable r3 = r6.owner
            com.badlogic.gdx.math.Vector r3 = r3.getPosition()
            com.badlogic.gdx.math.Vector r2 = r2.sub(r3)
            com.badlogic.gdx.math.Vector r2 = r2.nor()
            r2.scl(r0)
            r2 = 0
            r7.angular = r2
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Wander.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public float getWanderOffset() {
        return this.wanderOffset;
    }

    public Wander<T> setWanderOffset(float wanderOffset2) {
        this.wanderOffset = wanderOffset2;
        return this;
    }

    public float getWanderRadius() {
        return this.wanderRadius;
    }

    public Wander<T> setWanderRadius(float wanderRadius2) {
        this.wanderRadius = wanderRadius2;
        return this;
    }

    public float getWanderRate() {
        return this.wanderRate;
    }

    public Wander<T> setWanderRate(float wanderRate2) {
        this.wanderRate = wanderRate2;
        return this;
    }

    public float getWanderOrientation() {
        return this.wanderOrientation;
    }

    public Wander<T> setWanderOrientation(float wanderOrientation2) {
        this.wanderOrientation = wanderOrientation2;
        return this;
    }

    public boolean isFaceEnabled() {
        return this.faceEnabled;
    }

    public Wander<T> setFaceEnabled(boolean faceEnabled2) {
        this.faceEnabled = faceEnabled2;
        return this;
    }

    public T getInternalTargetPosition() {
        return this.internalTargetPosition;
    }

    public T getWanderCenter() {
        return this.wanderCenter;
    }

    public Wander<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Wander<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Wander<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public Wander<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public Wander<T> setAlignTolerance(float alignTolerance) {
        this.alignTolerance = alignTolerance;
        return this;
    }

    public Wander<T> setDecelerationRadius(float decelerationRadius) {
        this.decelerationRadius = decelerationRadius;
        return this;
    }

    public Wander<T> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }
}
