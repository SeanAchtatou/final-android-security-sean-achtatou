package com.badlogic.gdx.ai.steer.limiters;

import com.badlogic.gdx.ai.steer.Limiter;

public class FullLimiter implements Limiter {
    private float maxAngularAcceleration;
    private float maxAngularSpeed;
    private float maxLinearAcceleration;
    private float maxLinearSpeed;

    public FullLimiter(float maxLinearAcceleration2, float maxLinearSpeed2, float maxAngularAcceleration2, float maxAngularSpeed2) {
        this.maxLinearAcceleration = maxLinearAcceleration2;
        this.maxLinearSpeed = maxLinearSpeed2;
        this.maxAngularAcceleration = maxAngularAcceleration2;
        this.maxAngularSpeed = maxAngularSpeed2;
    }

    public float getMaxLinearSpeed() {
        return this.maxLinearSpeed;
    }

    public void setMaxLinearSpeed(float maxLinearSpeed2) {
        this.maxLinearSpeed = maxLinearSpeed2;
    }

    public float getMaxLinearAcceleration() {
        return this.maxLinearAcceleration;
    }

    public void setMaxLinearAcceleration(float maxLinearAcceleration2) {
        this.maxLinearAcceleration = maxLinearAcceleration2;
    }

    public float getMaxAngularSpeed() {
        return this.maxAngularSpeed;
    }

    public void setMaxAngularSpeed(float maxAngularSpeed2) {
        this.maxAngularSpeed = maxAngularSpeed2;
    }

    public float getMaxAngularAcceleration() {
        return this.maxAngularAcceleration;
    }

    public void setMaxAngularAcceleration(float maxAngularAcceleration2) {
        this.maxAngularAcceleration = maxAngularAcceleration2;
    }
}
