package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.utils.Array;
import java.util.Iterator;

public class DefaultGraphPath<N> implements GraphPath<N> {
    public final Array<N> nodes;

    public DefaultGraphPath() {
        this(new Array());
    }

    public DefaultGraphPath(int capacity) {
        this(new Array(capacity));
    }

    public DefaultGraphPath(Array<N> nodes2) {
        this.nodes = nodes2;
    }

    public void clear() {
        this.nodes.clear();
    }

    public int getCount() {
        return this.nodes.size;
    }

    public void add(N node) {
        this.nodes.add(node);
    }

    public N get(int index) {
        return this.nodes.get(index);
    }

    public void reverse() {
        this.nodes.reverse();
    }

    public Iterator<N> iterator() {
        return this.nodes.iterator();
    }
}
