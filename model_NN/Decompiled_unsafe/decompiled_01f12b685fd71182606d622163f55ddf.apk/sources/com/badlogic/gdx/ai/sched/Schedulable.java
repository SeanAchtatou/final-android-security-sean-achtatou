package com.badlogic.gdx.ai.sched;

public interface Schedulable {
    void run(long j);
}
