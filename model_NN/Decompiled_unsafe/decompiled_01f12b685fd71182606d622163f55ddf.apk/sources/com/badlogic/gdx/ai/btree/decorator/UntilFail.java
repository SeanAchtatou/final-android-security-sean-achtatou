package com.badlogic.gdx.ai.btree.decorator;

import com.badlogic.gdx.ai.btree.Decorator;
import com.badlogic.gdx.ai.btree.Task;

public class UntilFail<E> extends Decorator<E> {
    public UntilFail() {
    }

    public UntilFail(Task<E> task) {
        super(task);
    }

    public void childSuccess(Task<E> task) {
        start(this.object);
        run(this.object);
    }

    public void childFail(Task<E> task) {
        this.control.childFail(this);
    }
}
