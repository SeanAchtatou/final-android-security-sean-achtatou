package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.utils.Array;

public interface Graph<N> {
    Array<Connection<N>> getConnections(N n);
}
