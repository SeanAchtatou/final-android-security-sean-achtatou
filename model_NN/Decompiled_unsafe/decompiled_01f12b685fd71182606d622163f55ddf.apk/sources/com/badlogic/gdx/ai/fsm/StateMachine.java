package com.badlogic.gdx.ai.fsm;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;

public interface StateMachine<E> extends Telegraph {
    void changeState(State<E> state);

    State<E> getCurrentState();

    State<E> getGlobalState();

    State<E> getPreviousState();

    boolean handleMessage(Telegram telegram);

    boolean isInState(State<E> state);

    boolean revertToPreviousState();

    void setGlobalState(State<E> state);

    void setInitialState(State<E> state);

    void update();
}
