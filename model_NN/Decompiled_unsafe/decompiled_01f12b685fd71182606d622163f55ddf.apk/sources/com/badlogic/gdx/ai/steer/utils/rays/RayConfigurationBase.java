package com.badlogic.gdx.ai.steer.utils.rays;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.utils.RayConfiguration;
import com.badlogic.gdx.ai.utils.Ray;
import com.badlogic.gdx.math.Vector;

public abstract class RayConfigurationBase<T extends Vector<T>> implements RayConfiguration<T> {
    protected Steerable<T> owner;
    protected Ray<T>[] rays;

    public RayConfigurationBase(Steerable<T> owner2, int numRays) {
        this.owner = owner2;
        this.rays = new Ray[numRays];
        for (int i = 0; i < numRays; i++) {
            this.rays[i] = new Ray<>(owner2.newVector(), owner2.newVector());
        }
    }

    public Steerable<T> getOwner() {
        return this.owner;
    }

    public void setOwner(Steerable<T> owner2) {
        this.owner = owner2;
    }

    public Ray<T>[] getRays() {
        return this.rays;
    }

    public void setRays(Ray<T>[] rays2) {
        this.rays = rays2;
    }
}
