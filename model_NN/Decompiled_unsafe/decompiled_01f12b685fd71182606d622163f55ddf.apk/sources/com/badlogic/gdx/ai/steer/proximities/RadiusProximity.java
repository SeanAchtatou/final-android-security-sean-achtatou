package com.badlogic.gdx.ai.steer.proximities;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;

public class RadiusProximity<T extends Vector<T>> extends ProximityBase<T> {
    private long frameId = 0;
    protected float radius;

    public RadiusProximity(Steerable<T> owner, Array<? extends Steerable<T>> agents, float radius2) {
        super(owner, agents);
        this.radius = radius2;
    }

    public float getRadius() {
        return this.radius;
    }

    public void setRadius(float radius2) {
        this.radius = radius2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int findNeighbors(com.badlogic.gdx.ai.steer.Proximity.ProximityCallback<T> r13) {
        /*
            r12 = this;
            com.badlogic.gdx.utils.Array r7 = r12.agents
            int r0 = r7.size
            r3 = 0
            long r8 = r12.frameId
            com.badlogic.gdx.Graphics r7 = com.badlogic.gdx.Gdx.graphics
            long r10 = r7.getFrameId()
            int r7 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r7 == 0) goto L_0x0058
            com.badlogic.gdx.Graphics r7 = com.badlogic.gdx.Gdx.graphics
            long r8 = r7.getFrameId()
            r12.frameId = r8
            com.badlogic.gdx.ai.steer.Steerable r7 = r12.owner
            com.badlogic.gdx.math.Vector r4 = r7.getPosition()
            r2 = 0
        L_0x0020:
            if (r2 >= r0) goto L_0x0078
            com.badlogic.gdx.utils.Array r7 = r12.agents
            java.lang.Object r1 = r7.get(r2)
            com.badlogic.gdx.ai.steer.Steerable r1 = (com.badlogic.gdx.ai.steer.Steerable) r1
            com.badlogic.gdx.ai.steer.Steerable r7 = r12.owner
            if (r1 == r7) goto L_0x0053
            com.badlogic.gdx.math.Vector r7 = r1.getPosition()
            float r6 = r4.dst2(r7)
            float r7 = r12.radius
            float r8 = r1.getBoundingRadius()
            float r5 = r7 + r8
            float r7 = r5 * r5
            int r7 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r7 >= 0) goto L_0x0053
            boolean r7 = r13.reportNeighbor(r1)
            if (r7 == 0) goto L_0x0053
            r7 = 1
            r1.setTagged(r7)
            int r3 = r3 + 1
        L_0x0050:
            int r2 = r2 + 1
            goto L_0x0020
        L_0x0053:
            r7 = 0
            r1.setTagged(r7)
            goto L_0x0050
        L_0x0058:
            r2 = 0
        L_0x0059:
            if (r2 >= r0) goto L_0x0078
            com.badlogic.gdx.utils.Array r7 = r12.agents
            java.lang.Object r1 = r7.get(r2)
            com.badlogic.gdx.ai.steer.Steerable r1 = (com.badlogic.gdx.ai.steer.Steerable) r1
            com.badlogic.gdx.ai.steer.Steerable r7 = r12.owner
            if (r1 == r7) goto L_0x0075
            boolean r7 = r1.isTagged()
            if (r7 == 0) goto L_0x0075
            boolean r7 = r13.reportNeighbor(r1)
            if (r7 == 0) goto L_0x0075
            int r3 = r3 + 1
        L_0x0075:
            int r2 = r2 + 1
            goto L_0x0059
        L_0x0078:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.proximities.RadiusProximity.findNeighbors(com.badlogic.gdx.ai.steer.Proximity$ProximityCallback):int");
    }
}
