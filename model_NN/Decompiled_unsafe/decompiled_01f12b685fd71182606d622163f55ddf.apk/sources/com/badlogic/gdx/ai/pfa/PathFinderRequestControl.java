package com.badlogic.gdx.ai.pfa;

import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.utils.TimeUtils;

public class PathFinderRequestControl<N> {
    public static final boolean DEBUG = false;
    long lastTime;
    PathFinder<N> pathFinder;
    Telegraph server;
    long timeToRun;
    long timeTolerance;

    public boolean execute(PathFinderRequest<N> request) {
        request.executionFrames++;
        do {
            if (request.status == 0) {
                long currentTime = TimeUtils.nanoTime();
                this.timeToRun -= currentTime - this.lastTime;
                if (this.timeToRun <= this.timeTolerance || !request.initializeSearch(this.timeToRun)) {
                    return false;
                }
                request.changeStatus(1);
                this.lastTime = currentTime;
            }
            if (request.status == 1) {
                long currentTime2 = TimeUtils.nanoTime();
                this.timeToRun -= currentTime2 - this.lastTime;
                if (this.timeToRun <= this.timeTolerance || !request.search(this.pathFinder, this.timeToRun)) {
                    return false;
                }
                request.changeStatus(2);
                this.lastTime = currentTime2;
            }
            if (request.status != 2) {
                break;
            }
            long currentTime3 = TimeUtils.nanoTime();
            this.timeToRun -= currentTime3 - this.lastTime;
            if (this.timeToRun > this.timeTolerance && request.finalizeSearch(this.timeToRun)) {
                request.changeStatus(3);
                if (this.server != null) {
                    (request.dispatcher != null ? request.dispatcher : MessageManager.getInstance()).dispatchMessage(this.server, request.client, request.responseMessageCode, request);
                }
                this.lastTime = currentTime3;
                if (!request.statusChanged) {
                    break;
                }
            } else {
                return false;
            }
        } while (request.status == 0);
        return true;
    }
}
