package com.badlogic.gdx.ai.steer;

import com.badlogic.gdx.math.Vector;

public interface Steerable<T extends Vector<T>> extends Limiter {
    T angleToVector(T t, float f);

    float getAngularVelocity();

    float getBoundingRadius();

    T getLinearVelocity();

    float getOrientation();

    T getPosition();

    boolean isTagged();

    T newVector();

    void setTagged(boolean z);

    float vectorToAngle(T t);
}
