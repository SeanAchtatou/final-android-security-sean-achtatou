package com.badlogic.gdx.ai.btree.decorator;

import com.badlogic.gdx.ai.btree.Decorator;
import com.badlogic.gdx.ai.btree.Task;

public class AlwaysFail<E> extends Decorator<E> {
    public AlwaysFail() {
    }

    public AlwaysFail(Task<E> task) {
        super(task);
    }

    public void childSuccess(Task<E> task) {
        this.control.childFail(this);
    }

    public void childFail(Task<E> task) {
        this.control.childFail(this);
    }
}
