package com.badlogic.gdx.ai.msg;

public interface Telegraph {
    boolean handleMessage(Telegram telegram);
}
