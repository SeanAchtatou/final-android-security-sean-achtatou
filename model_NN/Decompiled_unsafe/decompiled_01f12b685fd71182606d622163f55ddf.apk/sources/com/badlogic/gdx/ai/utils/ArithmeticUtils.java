package com.badlogic.gdx.ai.utils;

public final class ArithmeticUtils {
    private ArithmeticUtils() {
    }

    public static int gcdPositive(int a, int b) {
        int shift;
        int a2;
        if (a == 0) {
            return b;
        }
        if (b == 0) {
            return a;
        }
        int aTwos = Integer.numberOfTrailingZeros(a);
        int a3 = a >> aTwos;
        int bTwos = Integer.numberOfTrailingZeros(b);
        int b2 = b >> bTwos;
        if (aTwos <= bTwos) {
            shift = aTwos;
        } else {
            shift = bTwos;
        }
        while (a3 != b2) {
            int delta = a3 - b2;
            if (a3 <= b2) {
                b2 = a3;
            }
            if (delta < 0) {
                a2 = -delta;
            } else {
                a2 = delta;
            }
            a3 = a2 >> Integer.numberOfTrailingZeros(a2);
        }
        return a3 << shift;
    }

    public static int lcmPositive(int a, int b) throws ArithmeticException {
        if (a == 0 || b == 0) {
            return 0;
        }
        int lcm = Math.abs(mulAndCheck(a / gcdPositive(a, b), b));
        if (lcm != Integer.MIN_VALUE) {
            return lcm;
        }
        throw new ArithmeticException("overflow: lcm(" + a + ", " + b + ") > 2^31");
    }

    public static int gcdPositive(int... args) {
        if (args == null || args.length < 2) {
            throw new IllegalArgumentException("gcdPositive requires at least two arguments");
        }
        int result = args[0];
        int n = args.length;
        for (int i = 1; i < n; i++) {
            result = gcdPositive(result, args[i]);
        }
        return result;
    }

    public static int lcmPositive(int... args) {
        if (args == null || args.length < 2) {
            throw new IllegalArgumentException("lcmPositive requires at least two arguments");
        }
        int result = args[0];
        int n = args.length;
        for (int i = 1; i < n; i++) {
            result = lcmPositive(result, args[i]);
        }
        return result;
    }

    public static int mulAndCheck(int x, int y) throws ArithmeticException {
        long m = ((long) x) * ((long) y);
        if (m >= -2147483648L && m <= 2147483647L) {
            return (int) m;
        }
        throw new ArithmeticException();
    }
}
