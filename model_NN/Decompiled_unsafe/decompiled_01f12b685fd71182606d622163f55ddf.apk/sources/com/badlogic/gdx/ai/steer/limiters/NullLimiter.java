package com.badlogic.gdx.ai.steer.limiters;

import com.badlogic.gdx.ai.steer.Limiter;

public class NullLimiter implements Limiter {
    public static final NullLimiter NEUTRAL_LIMITER = new NullLimiter() {
        public float getMaxLinearSpeed() {
            return Float.POSITIVE_INFINITY;
        }

        public float getMaxLinearAcceleration() {
            return Float.POSITIVE_INFINITY;
        }

        public float getMaxAngularSpeed() {
            return Float.POSITIVE_INFINITY;
        }

        public float getMaxAngularAcceleration() {
            return Float.POSITIVE_INFINITY;
        }
    };

    public float getMaxLinearSpeed() {
        throw new UnsupportedOperationException();
    }

    public void setMaxLinearSpeed(float maxLinearSpeed) {
        throw new UnsupportedOperationException();
    }

    public float getMaxLinearAcceleration() {
        throw new UnsupportedOperationException();
    }

    public void setMaxLinearAcceleration(float maxLinearAcceleration) {
        throw new UnsupportedOperationException();
    }

    public float getMaxAngularSpeed() {
        throw new UnsupportedOperationException();
    }

    public void setMaxAngularSpeed(float maxAngularSpeed) {
        throw new UnsupportedOperationException();
    }

    public float getMaxAngularAcceleration() {
        throw new UnsupportedOperationException();
    }

    public void setMaxAngularAcceleration(float maxAngularAcceleration) {
        throw new UnsupportedOperationException();
    }
}
