package com.badlogic.gdx.ai.steer;

public interface Limiter {
    float getMaxAngularAcceleration();

    float getMaxAngularSpeed();

    float getMaxLinearAcceleration();

    float getMaxLinearSpeed();

    void setMaxAngularAcceleration(float f);

    void setMaxAngularSpeed(float f);

    void setMaxLinearAcceleration(float f);

    void setMaxLinearSpeed(float f);
}
