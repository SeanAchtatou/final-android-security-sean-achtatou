package com.badlogic.gdx.ai.msg;

import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectSet;
import java.lang.Comparable;

public class PriorityQueue<E extends Comparable<E>> {
    private static final double CAPACITY_RATIO_HI = 2.0d;
    private static final double CAPACITY_RATIO_LOW = 1.5d;
    private static final int DEFAULT_INITIAL_CAPACITY = 11;
    private Object[] queue;
    private ObjectSet<E> set;
    private int size;
    private boolean uniqueness;

    public PriorityQueue() {
        this(11);
    }

    public PriorityQueue(int initialCapacity) {
        this.size = 0;
        this.queue = new Object[initialCapacity];
        this.set = new ObjectSet<>(initialCapacity);
    }

    public boolean getUniqueness() {
        return this.uniqueness;
    }

    public void setUniqueness(boolean uniqueness2) {
        this.uniqueness = uniqueness2;
    }

    public boolean add(E e) {
        if (e == null) {
            throw new NullPointerException();
        } else if (this.uniqueness && !this.set.add(e)) {
            return false;
        } else {
            int i = this.size;
            if (i >= this.queue.length) {
                growToSize(i + 1);
            }
            this.size = i + 1;
            if (i == 0) {
                this.queue[0] = e;
            } else {
                siftUp(i, e);
            }
            return true;
        }
    }

    public E peek() {
        if (this.size == 0) {
            return null;
        }
        return (Comparable) this.queue[0];
    }

    public E get(int index) {
        if (index >= this.size) {
            return null;
        }
        return (Comparable) this.queue[index];
    }

    public int size() {
        return this.size;
    }

    public void clear() {
        for (int i = 0; i < this.size; i++) {
            this.queue[i] = null;
        }
        this.size = 0;
        this.set.clear();
    }

    public E poll() {
        if (this.size == 0) {
            return null;
        }
        int s = this.size - 1;
        this.size = s;
        E result = (Comparable) this.queue[0];
        E x = (Comparable) this.queue[s];
        this.queue[s] = null;
        if (s != 0) {
            siftDown(0, x);
        }
        if (!this.uniqueness) {
            return result;
        }
        this.set.remove(result);
        return result;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void siftUp(int r4, E r5) {
        /*
            r3 = this;
        L_0x0000:
            if (r4 <= 0) goto L_0x0012
            int r2 = r4 + -1
            int r1 = r2 >>> 1
            java.lang.Object[] r2 = r3.queue
            r0 = r2[r1]
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r2 = r5.compareTo(r0)
            if (r2 < 0) goto L_0x0017
        L_0x0012:
            java.lang.Object[] r2 = r3.queue
            r2[r4] = r5
            return
        L_0x0017:
            java.lang.Object[] r2 = r3.queue
            r2[r4] = r0
            r4 = r1
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.msg.PriorityQueue.siftUp(int, java.lang.Comparable):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void siftDown(int r6, E r7) {
        /*
            r5 = this;
            int r4 = r5.size
            int r2 = r4 >>> 1
        L_0x0004:
            if (r6 >= r2) goto L_0x002f
            int r4 = r6 << 1
            int r1 = r4 + 1
            java.lang.Object[] r4 = r5.queue
            r0 = r4[r1]
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1 + 1
            int r4 = r5.size
            if (r3 >= r4) goto L_0x0029
            java.lang.Object[] r4 = r5.queue
            r4 = r4[r3]
            java.lang.Comparable r4 = (java.lang.Comparable) r4
            int r4 = r0.compareTo(r4)
            if (r4 <= 0) goto L_0x0029
            java.lang.Object[] r4 = r5.queue
            r1 = r3
            r0 = r4[r3]
            java.lang.Comparable r0 = (java.lang.Comparable) r0
        L_0x0029:
            int r4 = r7.compareTo(r0)
            if (r4 > 0) goto L_0x0034
        L_0x002f:
            java.lang.Object[] r4 = r5.queue
            r4[r6] = r7
            return
        L_0x0034:
            java.lang.Object[] r4 = r5.queue
            r4[r6] = r0
            r6 = r1
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.msg.PriorityQueue.siftDown(int, java.lang.Comparable):void");
    }

    private void growToSize(int minCapacity) {
        if (minCapacity < 0) {
            throw new GdxRuntimeException("Capacity upper limit exceeded.");
        }
        int oldCapacity = this.queue.length;
        int newCapacity = (int) (oldCapacity < 64 ? ((double) (oldCapacity + 1)) * CAPACITY_RATIO_HI : ((double) oldCapacity) * CAPACITY_RATIO_LOW);
        if (newCapacity < 0) {
            newCapacity = Integer.MAX_VALUE;
        }
        if (newCapacity < minCapacity) {
            newCapacity = minCapacity;
        }
        Object[] newQueue = new Object[newCapacity];
        System.arraycopy(this.queue, 0, newQueue, 0, this.size);
        this.queue = newQueue;
    }
}
