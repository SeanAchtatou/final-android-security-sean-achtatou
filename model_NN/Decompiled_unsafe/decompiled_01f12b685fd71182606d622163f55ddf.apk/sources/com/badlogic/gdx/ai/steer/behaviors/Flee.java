package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector;

public class Flee<T extends Vector<T>> extends Seek<T> {
    public Flee(Steerable<T> owner) {
        this(owner, null);
    }

    public Flee(Steerable<T> owner, Steerable<T> target) {
        super(owner, target);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r3) {
        /*
            r2 = this;
            T r0 = r3.linear
            com.badlogic.gdx.ai.steer.Steerable r1 = r2.owner
            com.badlogic.gdx.math.Vector r1 = r1.getPosition()
            com.badlogic.gdx.math.Vector r0 = r0.set(r1)
            com.badlogic.gdx.ai.steer.Steerable r1 = r2.target
            com.badlogic.gdx.math.Vector r1 = r1.getPosition()
            com.badlogic.gdx.math.Vector r0 = r0.sub(r1)
            com.badlogic.gdx.math.Vector r0 = r0.nor()
            com.badlogic.gdx.ai.steer.Limiter r1 = r2.getActualLimiter()
            float r1 = r1.getMaxLinearAcceleration()
            r0.scl(r1)
            r0 = 0
            r3.angular = r0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Flee.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    public Flee<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Flee<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Flee<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public Flee<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }
}
