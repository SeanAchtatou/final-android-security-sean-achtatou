package com.badlogic.gdx.ai.fsm;

import com.badlogic.gdx.utils.Array;

public class StackStateMachine<E> extends DefaultStateMachine<E> {
    private Array<State<E>> stateStack;

    public StackStateMachine(E owner) {
        this(owner, null, null);
    }

    public StackStateMachine(E owner, State<E> initialState) {
        this(owner, initialState, null);
    }

    public StackStateMachine(E owner, State<E> initialState, State<E> globalState) {
        super(owner, initialState, globalState);
    }

    public void setInitialState(State<E> state) {
        if (this.stateStack == null) {
            this.stateStack = new Array<>();
        }
        this.stateStack.clear();
        this.currentState = state;
    }

    public State<E> getCurrentState() {
        return this.currentState;
    }

    public State<E> getPreviousState() {
        if (this.stateStack.size == 0) {
            return null;
        }
        return this.stateStack.peek();
    }

    public void changeState(State<E> newState) {
        changeState(newState, true);
    }

    public boolean revertToPreviousState() {
        if (this.stateStack.size == 0) {
            return false;
        }
        changeState(this.stateStack.pop(), false);
        return true;
    }

    private void changeState(State<E> newState, boolean pushCurrentStateToStack) {
        if (pushCurrentStateToStack && this.currentState != null) {
            this.stateStack.add(this.currentState);
        }
        if (this.currentState != null) {
            this.currentState.exit(this.owner);
        }
        this.currentState = newState;
        this.currentState.enter(this.owner);
    }
}
