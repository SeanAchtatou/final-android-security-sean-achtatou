package com.badlogic.gdx.ai.steer.behaviors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.steer.Limiter;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteerableAdapter;
import com.badlogic.gdx.math.Vector;
import com.kbz.esotericsoftware.spine.Animation;

public class Jump<T extends Vector<T>> extends MatchVelocity<T> {
    public static boolean DEBUG_ENABLED = false;
    protected float airborneTime = Animation.CurveTimeline.LINEAR;
    protected JumpCallback callback;
    protected T gravity;
    protected GravityComponentHandler<T> gravityComponentHandler;
    private boolean isJumpAchievable;
    protected JumpDescriptor<T> jumpDescriptor;
    private JumpTarget<T> jumpTarget;
    protected float maxVerticalVelocity;
    private T planarVelocity;
    protected float takeoffPositionTolerance;
    protected float takeoffVelocityTolerance;

    public interface GravityComponentHandler<T extends Vector<T>> {
        float getComponent(T t);

        void setComponent(T t, float f);
    }

    public interface JumpCallback {
        void reportAchievability(boolean z);

        void takeoff(float f, float f2);
    }

    public Jump(Steerable<T> owner, JumpDescriptor<T> jumpDescriptor2, T gravity2, GravityComponentHandler<T> gravityComponentHandler2, JumpCallback callback2) {
        super(owner);
        this.gravity = gravity2;
        this.gravityComponentHandler = gravityComponentHandler2;
        setJumpDescriptor(jumpDescriptor2);
        this.callback = callback2;
        this.jumpTarget = new JumpTarget<>(owner);
        this.planarVelocity = owner.newVector();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.badlogic.gdx.ai.steer.SteeringAcceleration<T> calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration<T> r6) {
        /*
            r5 = this;
            com.badlogic.gdx.ai.steer.Steerable r0 = r5.target
            if (r0 != 0) goto L_0x0011
            com.badlogic.gdx.ai.steer.Steerable r0 = r5.calculateTarget()
            r5.target = r0
            com.badlogic.gdx.ai.steer.behaviors.Jump$JumpCallback r0 = r5.callback
            boolean r1 = r5.isJumpAchievable
            r0.reportAchievability(r1)
        L_0x0011:
            boolean r0 = r5.isJumpAchievable
            if (r0 != 0) goto L_0x001a
            com.badlogic.gdx.ai.steer.SteeringAcceleration r0 = r6.setZero()
        L_0x0019:
            return r0
        L_0x001a:
            com.badlogic.gdx.ai.steer.Steerable r0 = r5.owner
            com.badlogic.gdx.math.Vector r0 = r0.getPosition()
            com.badlogic.gdx.ai.steer.Steerable r1 = r5.target
            com.badlogic.gdx.math.Vector r1 = r1.getPosition()
            float r2 = r5.takeoffPositionTolerance
            boolean r0 = r0.epsilonEquals(r1, r2)
            if (r0 == 0) goto L_0x00b7
            boolean r0 = com.badlogic.gdx.ai.steer.behaviors.Jump.DEBUG_ENABLED
            if (r0 == 0) goto L_0x003b
            com.badlogic.gdx.Application r0 = com.badlogic.gdx.Gdx.app
            java.lang.String r1 = "Jump"
            java.lang.String r2 = "Good position!!!"
            r0.log(r1, r2)
        L_0x003b:
            com.badlogic.gdx.ai.steer.Steerable r0 = r5.owner
            com.badlogic.gdx.math.Vector r0 = r0.getLinearVelocity()
            com.badlogic.gdx.ai.steer.Steerable r1 = r5.target
            com.badlogic.gdx.math.Vector r1 = r1.getLinearVelocity()
            float r2 = r5.takeoffVelocityTolerance
            boolean r0 = r0.epsilonEquals(r1, r2)
            if (r0 == 0) goto L_0x006d
            boolean r0 = com.badlogic.gdx.ai.steer.behaviors.Jump.DEBUG_ENABLED
            if (r0 == 0) goto L_0x005c
            com.badlogic.gdx.Application r0 = com.badlogic.gdx.Gdx.app
            java.lang.String r1 = "Jump"
            java.lang.String r2 = "Good Velocity!!!"
            r0.log(r1, r2)
        L_0x005c:
            r0 = 0
            r5.isJumpAchievable = r0
            com.badlogic.gdx.ai.steer.behaviors.Jump$JumpCallback r0 = r5.callback
            float r1 = r5.maxVerticalVelocity
            float r2 = r5.airborneTime
            r0.takeoff(r1, r2)
            com.badlogic.gdx.ai.steer.SteeringAcceleration r0 = r6.setZero()
            goto L_0x0019
        L_0x006d:
            boolean r0 = com.badlogic.gdx.ai.steer.behaviors.Jump.DEBUG_ENABLED
            if (r0 == 0) goto L_0x00b7
            com.badlogic.gdx.Application r0 = com.badlogic.gdx.Gdx.app
            java.lang.String r1 = "Jump"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Bad Velocity: Speed diff. = "
            java.lang.StringBuilder r2 = r2.append(r3)
            T r3 = r5.planarVelocity
            com.badlogic.gdx.ai.steer.Steerable r4 = r5.target
            com.badlogic.gdx.math.Vector r4 = r4.getLinearVelocity()
            com.badlogic.gdx.math.Vector r3 = r3.set(r4)
            com.badlogic.gdx.ai.steer.Steerable r4 = r5.owner
            com.badlogic.gdx.math.Vector r4 = r4.getLinearVelocity()
            com.badlogic.gdx.math.Vector r3 = r3.sub(r4)
            float r3 = r3.len()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ", diff = ("
            java.lang.StringBuilder r2 = r2.append(r3)
            T r3 = r5.planarVelocity
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.log(r1, r2)
        L_0x00b7:
            com.badlogic.gdx.ai.steer.SteeringAcceleration r0 = super.calculateRealSteering(r6)
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Jump.calculateRealSteering(com.badlogic.gdx.ai.steer.SteeringAcceleration):com.badlogic.gdx.ai.steer.SteeringAcceleration");
    }

    private Steerable<T> calculateTarget() {
        this.jumpTarget.position = this.jumpDescriptor.takeoffPosition;
        this.airborneTime = calculateAirborneTimeAndVelocity(this.jumpTarget.linearVelocity, this.jumpDescriptor, getActualLimiter().getMaxLinearSpeed());
        this.isJumpAchievable = this.airborneTime >= Animation.CurveTimeline.LINEAR;
        return this.jumpTarget;
    }

    public float calculateAirborneTimeAndVelocity(T outVelocity, JumpDescriptor<T> jumpDescriptor2, float maxLinearSpeed) {
        float g = this.gravityComponentHandler.getComponent(this.gravity);
        float sqrtTerm = (float) Math.sqrt((double) ((2.0f * g * this.gravityComponentHandler.getComponent(jumpDescriptor2.delta)) + (this.maxVerticalVelocity * this.maxVerticalVelocity)));
        float time = ((-this.maxVerticalVelocity) + sqrtTerm) / g;
        if (DEBUG_ENABLED) {
            Gdx.app.log("Jump", "1st jump time = " + time);
        }
        if (!checkAirborneTimeAndCalculateVelocity(outVelocity, time, jumpDescriptor2, maxLinearSpeed)) {
            time = ((-this.maxVerticalVelocity) - sqrtTerm) / g;
            if (DEBUG_ENABLED) {
                Gdx.app.log("Jump", "2nd jump time = " + time);
            }
            if (!checkAirborneTimeAndCalculateVelocity(outVelocity, time, jumpDescriptor2, maxLinearSpeed)) {
                return -1.0f;
            }
        }
        return time;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean checkAirborneTimeAndCalculateVelocity(T r6, float r7, com.badlogic.gdx.ai.steer.behaviors.Jump.JumpDescriptor<T> r8, float r9) {
        /*
            r5 = this;
            T r1 = r5.planarVelocity
            T r2 = r8.delta
            com.badlogic.gdx.math.Vector r1 = r1.set(r2)
            r2 = 1065353216(0x3f800000, float:1.0)
            float r2 = r2 / r7
            r1.scl(r2)
            com.badlogic.gdx.ai.steer.behaviors.Jump$GravityComponentHandler<T> r1 = r5.gravityComponentHandler
            T r2 = r5.planarVelocity
            r3 = 0
            r1.setComponent(r2, r3)
            T r1 = r5.planarVelocity
            float r1 = r1.len2()
            float r2 = r9 * r9
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x0061
            com.badlogic.gdx.ai.steer.behaviors.Jump$GravityComponentHandler<T> r1 = r5.gravityComponentHandler
            float r0 = r1.getComponent(r6)
            com.badlogic.gdx.ai.steer.behaviors.Jump$GravityComponentHandler<T> r1 = r5.gravityComponentHandler
            T r2 = r5.planarVelocity
            com.badlogic.gdx.math.Vector r2 = r6.set(r2)
            r1.setComponent(r2, r0)
            boolean r1 = com.badlogic.gdx.ai.steer.behaviors.Jump.DEBUG_ENABLED
            if (r1 == 0) goto L_0x005f
            com.badlogic.gdx.Application r1 = com.badlogic.gdx.Gdx.app
            java.lang.String r2 = "Jump"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "targetLinearVelocity = "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r4 = "; targetLinearSpeed = "
            java.lang.StringBuilder r3 = r3.append(r4)
            float r4 = r6.len()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.log(r2, r3)
        L_0x005f:
            r1 = 1
        L_0x0060:
            return r1
        L_0x0061:
            r1 = 0
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Jump.checkAirborneTimeAndCalculateVelocity(com.badlogic.gdx.math.Vector, float, com.badlogic.gdx.ai.steer.behaviors.Jump$JumpDescriptor, float):boolean");
    }

    public JumpDescriptor<T> getJumpDescriptor() {
        return this.jumpDescriptor;
    }

    public Jump<T> setJumpDescriptor(JumpDescriptor<T> jumpDescriptor2) {
        this.jumpDescriptor = jumpDescriptor2;
        this.target = null;
        this.isJumpAchievable = false;
        return this;
    }

    public T getGravity() {
        return this.gravity;
    }

    public Jump<T> setGravity(T gravity2) {
        this.gravity = gravity2;
        return this;
    }

    public float getMaxVerticalVelocity() {
        return this.maxVerticalVelocity;
    }

    public Jump<T> setMaxVerticalVelocity(float maxVerticalVelocity2) {
        this.maxVerticalVelocity = maxVerticalVelocity2;
        return this;
    }

    public float getTakeoffPositionTolerance() {
        return this.takeoffPositionTolerance;
    }

    public Jump<T> setTakeoffPositionTolerance(float takeoffPositionTolerance2) {
        this.takeoffPositionTolerance = takeoffPositionTolerance2;
        return this;
    }

    public float getTakeoffVelocityTolerance() {
        return this.takeoffVelocityTolerance;
    }

    public Jump<T> setTakeoffVelocityTolerance(float takeoffVelocityTolerance2) {
        this.takeoffVelocityTolerance = takeoffVelocityTolerance2;
        return this;
    }

    public Jump<T> setTakeoffTolerance(float takeoffTolerance) {
        setTakeoffPositionTolerance(takeoffTolerance);
        setTakeoffVelocityTolerance(takeoffTolerance);
        return this;
    }

    public Jump<T> setOwner(Steerable<T> owner) {
        this.owner = owner;
        return this;
    }

    public Jump<T> setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public Jump<T> setLimiter(Limiter limiter) {
        this.limiter = limiter;
        return this;
    }

    public Jump<T> setTarget(Steerable<T> target) {
        this.target = target;
        return this;
    }

    public Jump<T> setTimeToTarget(float timeToTarget) {
        this.timeToTarget = timeToTarget;
        return this;
    }

    private static class JumpTarget<T extends Vector<T>> extends SteerableAdapter<T> {
        T linearVelocity;
        T position = null;

        public JumpTarget(Steerable<T> other) {
            this.linearVelocity = other.newVector();
        }

        public T getPosition() {
            return this.position;
        }

        public T getLinearVelocity() {
            return this.linearVelocity;
        }
    }

    public static class JumpDescriptor<T extends Vector<T>> {
        public T delta;
        public T landingPosition;
        public T takeoffPosition;

        public JumpDescriptor(T takeoffPosition2, T landingPosition2) {
            this.takeoffPosition = takeoffPosition2;
            this.landingPosition = landingPosition2;
            this.delta = landingPosition2.cpy();
            set(takeoffPosition2, landingPosition2);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void set(T r2, T r3) {
            /*
                r1 = this;
                T r0 = r1.takeoffPosition
                r0.set(r2)
                T r0 = r1.landingPosition
                r0.set(r3)
                T r0 = r1.delta
                com.badlogic.gdx.math.Vector r0 = r0.set(r3)
                r0.sub(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.ai.steer.behaviors.Jump.JumpDescriptor.set(com.badlogic.gdx.math.Vector, com.badlogic.gdx.math.Vector):void");
        }
    }
}
