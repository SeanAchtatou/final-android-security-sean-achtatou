package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

public class InputListener implements EventListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$InputEvent$Type;
    static boolean onlyOnePress;
    private static final Vector2 tmpCoords = new Vector2();

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$InputEvent$Type() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$InputEvent$Type;
        if (iArr == null) {
            iArr = new int[InputEvent.Type.values().length];
            try {
                iArr[InputEvent.Type.enter.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[InputEvent.Type.exit.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[InputEvent.Type.keyDown.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[InputEvent.Type.keyTyped.ordinal()] = 10;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[InputEvent.Type.keyUp.ordinal()] = 9;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[InputEvent.Type.mouseMoved.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[InputEvent.Type.scrolled.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[InputEvent.Type.touchDown.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[InputEvent.Type.touchDragged.ordinal()] = 3;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[InputEvent.Type.touchUp.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$InputEvent$Type = iArr;
        }
        return iArr;
    }

    public static void setOnePress(boolean isOne) {
        onlyOnePress = isOne;
    }

    public boolean handle(Event e) {
        if (!(e instanceof InputEvent)) {
            return false;
        }
        InputEvent event = (InputEvent) e;
        if (onlyOnePress && event.getPointer() > 0) {
            return false;
        }
        switch ($SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$InputEvent$Type()[event.getType().ordinal()]) {
            case 8:
                return keyDown(event, event.getKeyCode());
            case 9:
                return keyUp(event, event.getKeyCode());
            case 10:
                return keyTyped(event, event.getCharacter());
            default:
                event.toCoordinates(event.getListenerActor(), tmpCoords);
                switch ($SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$InputEvent$Type()[event.getType().ordinal()]) {
                    case 1:
                        return touchDown(event, tmpCoords.x, tmpCoords.y, event.getPointer(), event.getButton());
                    case 2:
                        touchUp(event, tmpCoords.x, tmpCoords.y, event.getPointer(), event.getButton());
                        return true;
                    case 3:
                        touchDragged(event, tmpCoords.x, tmpCoords.y, event.getPointer());
                        return true;
                    case 4:
                        return mouseMoved(event, tmpCoords.x, tmpCoords.y);
                    case 5:
                        enter(event, tmpCoords.x, tmpCoords.y, event.getPointer(), event.getRelatedActor());
                        return false;
                    case 6:
                        exit(event, tmpCoords.x, tmpCoords.y, event.getPointer(), event.getRelatedActor());
                        return false;
                    case 7:
                        return scrolled(event, tmpCoords.x, tmpCoords.y, event.getScrollAmount());
                    default:
                        return false;
                }
        }
    }

    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return false;
    }

    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
    }

    public void touchDragged(InputEvent event, float x, float y, int pointer) {
    }

    public boolean mouseMoved(InputEvent event, float x, float y) {
        return false;
    }

    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
    }

    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
    }

    public boolean scrolled(InputEvent event, float x, float y, int amount) {
        return false;
    }

    public boolean keyDown(InputEvent event, int keycode) {
        return false;
    }

    public boolean keyUp(InputEvent event, int keycode) {
        return false;
    }

    public boolean keyTyped(InputEvent event, char character) {
        return false;
    }
}
