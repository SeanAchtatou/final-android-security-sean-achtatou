package com.badlogic.gdx.scenes.scene2d.actions;

import com.kbz.esotericsoftware.spine.Animation;

public abstract class RelativeTemporalAction extends TemporalAction {
    private float lastPercent;

    /* access modifiers changed from: protected */
    public abstract void updateRelative(float f);

    /* access modifiers changed from: protected */
    public void begin() {
        this.lastPercent = Animation.CurveTimeline.LINEAR;
    }

    /* access modifiers changed from: protected */
    public void update(float percent) {
        updateRelative(percent - this.lastPercent);
        this.lastPercent = percent;
    }
}
