package com.badlogic.gdx.scenes.scene2d.utils;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;

public abstract class FocusListener implements EventListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$utils$FocusListener$FocusEvent$Type;

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$utils$FocusListener$FocusEvent$Type() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$utils$FocusListener$FocusEvent$Type;
        if (iArr == null) {
            iArr = new int[FocusEvent.Type.values().length];
            try {
                iArr[FocusEvent.Type.keyboard.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[FocusEvent.Type.scroll.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$utils$FocusListener$FocusEvent$Type = iArr;
        }
        return iArr;
    }

    public boolean handle(Event event) {
        if (event instanceof FocusEvent) {
            FocusEvent focusEvent = (FocusEvent) event;
            switch ($SWITCH_TABLE$com$badlogic$gdx$scenes$scene2d$utils$FocusListener$FocusEvent$Type()[focusEvent.getType().ordinal()]) {
                case 1:
                    keyboardFocusChanged(focusEvent, event.getTarget(), focusEvent.isFocused());
                    break;
                case 2:
                    scrollFocusChanged(focusEvent, event.getTarget(), focusEvent.isFocused());
                    break;
            }
        }
        return false;
    }

    public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
    }

    public void scrollFocusChanged(FocusEvent event, Actor actor, boolean focused) {
    }

    public static class FocusEvent extends Event {
        private boolean focused;
        private Actor relatedActor;
        private Type type;

        public enum Type {
            keyboard,
            scroll
        }

        public void reset() {
            super.reset();
            this.relatedActor = null;
        }

        public boolean isFocused() {
            return this.focused;
        }

        public void setFocused(boolean focused2) {
            this.focused = focused2;
        }

        public Type getType() {
            return this.type;
        }

        public void setType(Type focusType) {
            this.type = focusType;
        }

        public Actor getRelatedActor() {
            return this.relatedActor;
        }

        public void setRelatedActor(Actor relatedActor2) {
            this.relatedActor = relatedActor2;
        }
    }
}
