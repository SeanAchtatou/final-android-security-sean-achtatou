package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.SerializationException;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Method;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class Skin implements Disposable {
    TextureAtlas atlas;
    ObjectMap<Class, ObjectMap<String, Object>> resources = new ObjectMap<>();

    public static class TintedDrawable {
        public Color color;
        public String name;
    }

    public Skin() {
    }

    public Skin(FileHandle skinFile) {
        FileHandle atlasFile = skinFile.sibling(String.valueOf(skinFile.nameWithoutExtension()) + ".atlas");
        if (atlasFile.exists()) {
            this.atlas = new TextureAtlas(atlasFile);
            addRegions(this.atlas);
        }
        load(skinFile);
    }

    public Skin(FileHandle skinFile, TextureAtlas atlas2) {
        this.atlas = atlas2;
        addRegions(atlas2);
        load(skinFile);
    }

    public Skin(TextureAtlas atlas2) {
        this.atlas = atlas2;
        addRegions(atlas2);
    }

    public void load(FileHandle skinFile) {
        try {
            getJsonLoader(skinFile).fromJson(Skin.class, skinFile);
        } catch (SerializationException ex) {
            throw new SerializationException("Error reading file: " + skinFile, ex);
        }
    }

    public void addRegions(TextureAtlas atlas2) {
        Array<TextureAtlas.AtlasRegion> regions = atlas2.getRegions();
        int n = regions.size;
        for (int i = 0; i < n; i++) {
            TextureAtlas.AtlasRegion region = regions.get(i);
            add(region.name, region, TextureRegion.class);
        }
    }

    public void add(String name, Object resource) {
        add(name, resource, resource.getClass());
    }

    public void add(String name, Object resource, Class type) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (resource == null) {
            throw new IllegalArgumentException("resource cannot be null.");
        } else {
            ObjectMap<String, Object> typeResources = this.resources.get(type);
            if (typeResources == null) {
                typeResources = new ObjectMap<>();
                this.resources.put(type, typeResources);
            }
            typeResources.put(name, resource);
        }
    }

    public void remove(String name, Class type) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null.");
        }
        this.resources.get(type).remove(name);
    }

    public <T> T get(Class<T> type) {
        return get("default", type);
    }

    public <T> T get(String name, Class<T> type) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (type == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (type == Drawable.class) {
            return getDrawable(name);
        } else {
            if (type == TextureRegion.class) {
                return getRegion(name);
            }
            if (type == NinePatch.class) {
                return getPatch(name);
            }
            if (type == Sprite.class) {
                return getSprite(name);
            }
            ObjectMap<String, Object> typeResources = this.resources.get(type);
            if (typeResources == null) {
                throw new GdxRuntimeException("No " + type.getName() + " registered with name: " + name);
            }
            Object resource = typeResources.get(name);
            if (resource != null) {
                return resource;
            }
            throw new GdxRuntimeException("No " + type.getName() + " registered with name: " + name);
        }
    }

    public <T> T optional(String name, Class<T> type) {
        if (name == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (type == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else {
            ObjectMap<String, Object> typeResources = this.resources.get(type);
            if (typeResources == null) {
                return null;
            }
            return typeResources.get(name);
        }
    }

    public boolean has(String name, Class type) {
        ObjectMap<String, Object> typeResources = this.resources.get(type);
        if (typeResources == null) {
            return false;
        }
        return typeResources.containsKey(name);
    }

    public <T> ObjectMap<String, T> getAll(Class<T> type) {
        return this.resources.get(type);
    }

    public Color getColor(String name) {
        return (Color) get(name, Color.class);
    }

    public BitmapFont getFont(String name) {
        return (BitmapFont) get(name, BitmapFont.class);
    }

    public TextureRegion getRegion(String name) {
        TextureRegion region = (TextureRegion) optional(name, TextureRegion.class);
        if (region != null) {
            return region;
        }
        Texture texture = (Texture) optional(name, Texture.class);
        if (texture == null) {
            throw new GdxRuntimeException("No TextureRegion or Texture registered with name: " + name);
        }
        TextureRegion region2 = new TextureRegion(texture);
        add(name, region2, TextureRegion.class);
        return region2;
    }

    public TiledDrawable getTiledDrawable(String name) {
        TiledDrawable tiled = (TiledDrawable) optional(name, TiledDrawable.class);
        if (tiled != null) {
            return tiled;
        }
        TiledDrawable tiled2 = new TiledDrawable(getRegion(name));
        tiled2.setName(name);
        add(name, tiled2, TiledDrawable.class);
        return tiled2;
    }

    public NinePatch getPatch(String name) {
        NinePatch patch;
        int[] splits;
        NinePatch patch2 = (NinePatch) optional(name, NinePatch.class);
        if (patch2 != null) {
            return patch2;
        }
        try {
            TextureRegion region = getRegion(name);
            if ((region instanceof TextureAtlas.AtlasRegion) && (splits = ((TextureAtlas.AtlasRegion) region).splits) != null) {
                NinePatch patch3 = new NinePatch(region, splits[0], splits[1], splits[2], splits[3]);
                try {
                    int[] pads = ((TextureAtlas.AtlasRegion) region).pads;
                    if (pads != null) {
                        patch3.setPadding(pads[0], pads[1], pads[2], pads[3]);
                    }
                    patch2 = patch3;
                } catch (GdxRuntimeException e) {
                    throw new GdxRuntimeException("No NinePatch, TextureRegion, or Texture registered with name: " + name);
                }
            }
            if (patch2 == null) {
                patch = new NinePatch(region);
            } else {
                patch = patch2;
            }
            add(name, patch, NinePatch.class);
            return patch;
        } catch (GdxRuntimeException e2) {
            throw new GdxRuntimeException("No NinePatch, TextureRegion, or Texture registered with name: " + name);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002f A[SYNTHETIC, Splitter:B:14:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0054  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.graphics.g2d.Sprite getSprite(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.Class<com.badlogic.gdx.graphics.g2d.Sprite> r6 = com.badlogic.gdx.graphics.g2d.Sprite.class
            java.lang.Object r3 = r9.optional(r10, r6)
            com.badlogic.gdx.graphics.g2d.Sprite r3 = (com.badlogic.gdx.graphics.g2d.Sprite) r3
            if (r3 == 0) goto L_0x000c
            r4 = r3
        L_0x000b:
            return r4
        L_0x000c:
            com.badlogic.gdx.graphics.g2d.TextureRegion r5 = r9.getRegion(r10)     // Catch:{ GdxRuntimeException -> 0x003b }
            boolean r6 = r5 instanceof com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion     // Catch:{ GdxRuntimeException -> 0x003b }
            if (r6 == 0) goto L_0x0056
            r0 = r5
            com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasRegion r0 = (com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion) r0     // Catch:{ GdxRuntimeException -> 0x003b }
            r2 = r0
            boolean r6 = r2.rotate     // Catch:{ GdxRuntimeException -> 0x003b }
            if (r6 != 0) goto L_0x0028
            int r6 = r2.packedWidth     // Catch:{ GdxRuntimeException -> 0x003b }
            int r7 = r2.originalWidth     // Catch:{ GdxRuntimeException -> 0x003b }
            if (r6 != r7) goto L_0x0028
            int r6 = r2.packedHeight     // Catch:{ GdxRuntimeException -> 0x003b }
            int r7 = r2.originalHeight     // Catch:{ GdxRuntimeException -> 0x003b }
            if (r6 == r7) goto L_0x0056
        L_0x0028:
            com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasSprite r4 = new com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasSprite     // Catch:{ GdxRuntimeException -> 0x003b }
            r4.<init>(r2)     // Catch:{ GdxRuntimeException -> 0x003b }
        L_0x002d:
            if (r4 != 0) goto L_0x0054
            com.badlogic.gdx.graphics.g2d.Sprite r3 = new com.badlogic.gdx.graphics.g2d.Sprite     // Catch:{ GdxRuntimeException -> 0x0051 }
            r3.<init>(r5)     // Catch:{ GdxRuntimeException -> 0x0051 }
        L_0x0034:
            java.lang.Class<com.badlogic.gdx.graphics.g2d.Sprite> r6 = com.badlogic.gdx.graphics.g2d.Sprite.class
            r9.add(r10, r3, r6)     // Catch:{ GdxRuntimeException -> 0x003b }
            r4 = r3
            goto L_0x000b
        L_0x003b:
            r1 = move-exception
        L_0x003c:
            com.badlogic.gdx.utils.GdxRuntimeException r6 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "No NinePatch, TextureRegion, or Texture registered with name: "
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r10)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
            throw r6
        L_0x0051:
            r1 = move-exception
            r3 = r4
            goto L_0x003c
        L_0x0054:
            r3 = r4
            goto L_0x0034
        L_0x0056:
            r4 = r3
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.Skin.getSprite(java.lang.String):com.badlogic.gdx.graphics.g2d.Sprite");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0033 A[SYNTHETIC, Splitter:B:13:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.scenes.scene2d.utils.Drawable getDrawable(java.lang.String r11) {
        /*
            r10 = this;
            java.lang.Class<com.badlogic.gdx.scenes.scene2d.utils.Drawable> r7 = com.badlogic.gdx.scenes.scene2d.utils.Drawable.class
            java.lang.Object r1 = r10.optional(r11, r7)
            com.badlogic.gdx.scenes.scene2d.utils.Drawable r1 = (com.badlogic.gdx.scenes.scene2d.utils.Drawable) r1
            if (r1 == 0) goto L_0x000c
            r2 = r1
        L_0x000b:
            return r2
        L_0x000c:
            java.lang.Class<com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable> r7 = com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable.class
            java.lang.Object r1 = r10.optional(r11, r7)
            com.badlogic.gdx.scenes.scene2d.utils.Drawable r1 = (com.badlogic.gdx.scenes.scene2d.utils.Drawable) r1
            if (r1 == 0) goto L_0x0018
            r2 = r1
            goto L_0x000b
        L_0x0018:
            com.badlogic.gdx.graphics.g2d.TextureRegion r6 = r10.getRegion(r11)     // Catch:{ GdxRuntimeException -> 0x0099 }
            boolean r7 = r6 instanceof com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion     // Catch:{ GdxRuntimeException -> 0x0099 }
            if (r7 == 0) goto L_0x00a0
            r0 = r6
            com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasRegion r0 = (com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion) r0     // Catch:{ GdxRuntimeException -> 0x0099 }
            r4 = r0
            int[] r7 = r4.splits     // Catch:{ GdxRuntimeException -> 0x0099 }
            if (r7 == 0) goto L_0x005a
            com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable r2 = new com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable     // Catch:{ GdxRuntimeException -> 0x0099 }
            com.badlogic.gdx.graphics.g2d.NinePatch r7 = r10.getPatch(r11)     // Catch:{ GdxRuntimeException -> 0x0099 }
            r2.<init>(r7)     // Catch:{ GdxRuntimeException -> 0x0099 }
        L_0x0031:
            if (r2 != 0) goto L_0x009e
            com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable r1 = new com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable     // Catch:{ GdxRuntimeException -> 0x009b }
            r1.<init>(r6)     // Catch:{ GdxRuntimeException -> 0x009b }
        L_0x0038:
            if (r1 != 0) goto L_0x0049
            java.lang.Class<com.badlogic.gdx.graphics.g2d.NinePatch> r7 = com.badlogic.gdx.graphics.g2d.NinePatch.class
            java.lang.Object r3 = r10.optional(r11, r7)
            com.badlogic.gdx.graphics.g2d.NinePatch r3 = (com.badlogic.gdx.graphics.g2d.NinePatch) r3
            if (r3 == 0) goto L_0x0074
            com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable r1 = new com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable
            r1.<init>(r3)
        L_0x0049:
            boolean r7 = r1 instanceof com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable
            if (r7 == 0) goto L_0x0053
            r7 = r1
            com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable r7 = (com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable) r7
            r7.setName(r11)
        L_0x0053:
            java.lang.Class<com.badlogic.gdx.scenes.scene2d.utils.Drawable> r7 = com.badlogic.gdx.scenes.scene2d.utils.Drawable.class
            r10.add(r11, r1, r7)
            r2 = r1
            goto L_0x000b
        L_0x005a:
            boolean r7 = r4.rotate     // Catch:{ GdxRuntimeException -> 0x0099 }
            if (r7 != 0) goto L_0x006a
            int r7 = r4.packedWidth     // Catch:{ GdxRuntimeException -> 0x0099 }
            int r8 = r4.originalWidth     // Catch:{ GdxRuntimeException -> 0x0099 }
            if (r7 != r8) goto L_0x006a
            int r7 = r4.packedHeight     // Catch:{ GdxRuntimeException -> 0x0099 }
            int r8 = r4.originalHeight     // Catch:{ GdxRuntimeException -> 0x0099 }
            if (r7 == r8) goto L_0x00a0
        L_0x006a:
            com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable r2 = new com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable     // Catch:{ GdxRuntimeException -> 0x0099 }
            com.badlogic.gdx.graphics.g2d.Sprite r7 = r10.getSprite(r11)     // Catch:{ GdxRuntimeException -> 0x0099 }
            r2.<init>(r7)     // Catch:{ GdxRuntimeException -> 0x0099 }
            goto L_0x0031
        L_0x0074:
            java.lang.Class<com.badlogic.gdx.graphics.g2d.Sprite> r7 = com.badlogic.gdx.graphics.g2d.Sprite.class
            java.lang.Object r5 = r10.optional(r11, r7)
            com.badlogic.gdx.graphics.g2d.Sprite r5 = (com.badlogic.gdx.graphics.g2d.Sprite) r5
            if (r5 == 0) goto L_0x0084
            com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable r1 = new com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
            r1.<init>(r5)
            goto L_0x0049
        L_0x0084:
            com.badlogic.gdx.utils.GdxRuntimeException r7 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "No Drawable, NinePatch, TextureRegion, Texture, or Sprite registered with name: "
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r11)
            java.lang.String r8 = r8.toString()
            r7.<init>(r8)
            throw r7
        L_0x0099:
            r7 = move-exception
            goto L_0x0038
        L_0x009b:
            r7 = move-exception
            r1 = r2
            goto L_0x0038
        L_0x009e:
            r1 = r2
            goto L_0x0038
        L_0x00a0:
            r2 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.Skin.getDrawable(java.lang.String):com.badlogic.gdx.scenes.scene2d.utils.Drawable");
    }

    public String find(Object resource) {
        if (resource == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        ObjectMap<String, Object> typeResources = this.resources.get(resource.getClass());
        if (typeResources == null) {
            return null;
        }
        return (String) typeResources.findKey(resource, true);
    }

    public Drawable newDrawable(String name) {
        return newDrawable(getDrawable(name));
    }

    public Drawable newDrawable(String name, float r, float g, float b, float a) {
        return newDrawable(getDrawable(name), new Color(r, g, b, a));
    }

    public Drawable newDrawable(String name, Color tint) {
        return newDrawable(getDrawable(name), tint);
    }

    public Drawable newDrawable(Drawable drawable) {
        if (drawable instanceof TextureRegionDrawable) {
            return new TextureRegionDrawable((TextureRegionDrawable) drawable);
        }
        if (drawable instanceof NinePatchDrawable) {
            return new NinePatchDrawable((NinePatchDrawable) drawable);
        }
        if (drawable instanceof SpriteDrawable) {
            return new SpriteDrawable((SpriteDrawable) drawable);
        }
        throw new GdxRuntimeException("Unable to copy, unknown drawable type: " + drawable.getClass());
    }

    public Drawable newDrawable(Drawable drawable, float r, float g, float b, float a) {
        return newDrawable(drawable, new Color(r, g, b, a));
    }

    public Drawable newDrawable(Drawable drawable, Color tint) {
        BaseDrawable tint2;
        if (drawable instanceof TextureRegionDrawable) {
            tint2 = ((TextureRegionDrawable) drawable).tint(tint);
        } else if (drawable instanceof NinePatchDrawable) {
            tint2 = ((NinePatchDrawable) drawable).tint(tint);
        } else if (drawable instanceof SpriteDrawable) {
            tint2 = ((SpriteDrawable) drawable).tint(tint);
        } else {
            throw new GdxRuntimeException("Unable to copy, unknown drawable type: " + drawable.getClass());
        }
        if (tint2 instanceof BaseDrawable) {
            BaseDrawable named = tint2;
            if (drawable instanceof BaseDrawable) {
                named.setName(String.valueOf(((BaseDrawable) drawable).getName()) + " (" + tint + ")");
            } else {
                named.setName(" (" + tint + ")");
            }
        }
        return tint2;
    }

    public void setEnabled(Actor actor, boolean enabled) {
        Method method = findMethod(actor.getClass(), "getStyle");
        if (method != null) {
            try {
                Object style = method.invoke(actor, new Object[0]);
                String name = find(style);
                if (name != null) {
                    Object style2 = get(String.valueOf(name.replace("-disabled", "")) + (enabled ? "" : "-disabled"), style.getClass());
                    Method method2 = findMethod(actor.getClass(), "setStyle");
                    if (method2 != null) {
                        try {
                            method2.invoke(actor, style2);
                        } catch (Exception e) {
                        }
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    public TextureAtlas getAtlas() {
        return this.atlas;
    }

    public void dispose() {
        if (this.atlas != null) {
            this.atlas.dispose();
        }
        Iterator it = this.resources.values().iterator();
        while (it.hasNext()) {
            Iterator it2 = ((ObjectMap) it.next()).values().iterator();
            while (it2.hasNext()) {
                Object resource = it2.next();
                if (resource instanceof Disposable) {
                    ((Disposable) resource).dispose();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Json getJsonLoader(final FileHandle skinFile) {
        Json json = new Json() {
            public <T> T readValue(Class<T> type, Class elementType, JsonValue jsonData) {
                if (!jsonData.isString() || ClassReflection.isAssignableFrom(CharSequence.class, type)) {
                    return super.readValue(type, elementType, jsonData);
                }
                return Skin.this.get(jsonData.asString(), type);
            }
        };
        json.setTypeName(null);
        json.setUsePrototypes(false);
        json.setSerializer(Skin.class, new Json.ReadOnlySerializer<Skin>() {
            public Skin read(Json json, JsonValue typeToValueMap, Class ignored) {
                JsonValue valueMap = typeToValueMap.child;
                while (valueMap != null) {
                    try {
                        readNamedObjects(json, ClassReflection.forName(valueMap.name()), valueMap);
                        valueMap = valueMap.next;
                    } catch (ReflectionException ex) {
                        throw new SerializationException(ex);
                    }
                }
                return this;
            }

            private void readNamedObjects(Json json, Class type, JsonValue valueMap) {
                Class addType;
                if (type == TintedDrawable.class) {
                    addType = Drawable.class;
                } else {
                    addType = type;
                }
                for (JsonValue valueEntry = valueMap.child; valueEntry != null; valueEntry = valueEntry.next) {
                    Object object = json.readValue(type, valueEntry);
                    if (object != null) {
                        try {
                            Skin.this.add(valueEntry.name(), object, addType);
                        } catch (Exception ex) {
                            throw new SerializationException("Error reading " + ClassReflection.getSimpleName(type) + ": " + valueEntry.name(), ex);
                        }
                    }
                }
            }
        });
        json.setSerializer(BitmapFont.class, new Json.ReadOnlySerializer<BitmapFont>() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.badlogic.gdx.utils.Json.readValue(java.lang.String, java.lang.Class, java.lang.Object, com.badlogic.gdx.utils.JsonValue):T
             arg types: [java.lang.String, java.lang.Class, int, com.badlogic.gdx.utils.JsonValue]
             candidates:
              com.badlogic.gdx.utils.Json.readValue(java.lang.Class, java.lang.Class, java.lang.Object, com.badlogic.gdx.utils.JsonValue):T
              com.badlogic.gdx.utils.Json.readValue(java.lang.String, java.lang.Class, java.lang.Class, com.badlogic.gdx.utils.JsonValue):T
              com.badlogic.gdx.utils.Json.readValue(java.lang.String, java.lang.Class, java.lang.Object, com.badlogic.gdx.utils.JsonValue):T */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.badlogic.gdx.utils.Json.readValue(java.lang.String, java.lang.Class, java.lang.Object, com.badlogic.gdx.utils.JsonValue):T
             arg types: [java.lang.String, java.lang.Class, boolean, com.badlogic.gdx.utils.JsonValue]
             candidates:
              com.badlogic.gdx.utils.Json.readValue(java.lang.Class, java.lang.Class, java.lang.Object, com.badlogic.gdx.utils.JsonValue):T
              com.badlogic.gdx.utils.Json.readValue(java.lang.String, java.lang.Class, java.lang.Class, com.badlogic.gdx.utils.JsonValue):T
              com.badlogic.gdx.utils.Json.readValue(java.lang.String, java.lang.Class, java.lang.Object, com.badlogic.gdx.utils.JsonValue):T */
            public BitmapFont read(Json json, JsonValue jsonData, Class type) {
                BitmapFont font;
                String path = (String) json.readValue("file", String.class, jsonData);
                int scaledSize = ((Integer) json.readValue("scaledSize", Integer.TYPE, (Object) -1, jsonData)).intValue();
                Boolean flip = (Boolean) json.readValue("flip", Boolean.class, (Object) false, jsonData);
                Boolean markupEnabled = (Boolean) json.readValue("markupEnabled", Boolean.class, (Object) false, jsonData);
                FileHandle fontFile = skinFile.parent().child(path);
                if (!fontFile.exists()) {
                    fontFile = Gdx.files.internal(path);
                }
                if (!fontFile.exists()) {
                    throw new SerializationException("Font file not found: " + fontFile);
                }
                String regionName = fontFile.nameWithoutExtension();
                try {
                    TextureRegion region = (TextureRegion) this.optional(regionName, TextureRegion.class);
                    if (region != null) {
                        font = new BitmapFont(fontFile, region, flip.booleanValue());
                    } else {
                        FileHandle imageFile = fontFile.parent().child(String.valueOf(regionName) + ".png");
                        if (imageFile.exists()) {
                            font = new BitmapFont(fontFile, imageFile, flip.booleanValue());
                        } else {
                            font = new BitmapFont(fontFile, flip.booleanValue());
                        }
                    }
                    font.getData().markupEnabled = markupEnabled.booleanValue();
                    if (scaledSize != -1) {
                        font.getData().setScale(((float) scaledSize) / font.getCapHeight());
                    }
                    return font;
                } catch (RuntimeException ex) {
                    throw new SerializationException("Error loading bitmap font: " + fontFile, ex);
                }
            }
        });
        json.setSerializer(Color.class, new Json.ReadOnlySerializer<Color>() {
            public Color read(Json json, JsonValue jsonData, Class type) {
                if (jsonData.isString()) {
                    return (Color) Skin.this.get(jsonData.asString(), Color.class);
                }
                String hex = (String) json.readValue("hex", String.class, (Object) null, jsonData);
                if (hex != null) {
                    return Color.valueOf(hex);
                }
                return new Color(((Float) json.readValue("r", Float.TYPE, Float.valueOf((float) Animation.CurveTimeline.LINEAR), jsonData)).floatValue(), ((Float) json.readValue("g", Float.TYPE, Float.valueOf((float) Animation.CurveTimeline.LINEAR), jsonData)).floatValue(), ((Float) json.readValue("b", Float.TYPE, Float.valueOf((float) Animation.CurveTimeline.LINEAR), jsonData)).floatValue(), ((Float) json.readValue("a", Float.TYPE, Float.valueOf(1.0f), jsonData)).floatValue());
            }
        });
        json.setSerializer(TintedDrawable.class, new Json.ReadOnlySerializer() {
            public Object read(Json json, JsonValue jsonData, Class type) {
                String name = (String) json.readValue("name", String.class, jsonData);
                Color color = (Color) json.readValue("color", Color.class, jsonData);
                Drawable drawable = Skin.this.newDrawable(name, color);
                if (drawable instanceof BaseDrawable) {
                    ((BaseDrawable) drawable).setName(String.valueOf(jsonData.name) + " (" + name + ", " + color + ")");
                }
                return drawable;
            }
        });
        return json;
    }

    private static Method findMethod(Class type, String name) {
        for (Method method : ClassReflection.getMethods(type)) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        return null;
    }
}
