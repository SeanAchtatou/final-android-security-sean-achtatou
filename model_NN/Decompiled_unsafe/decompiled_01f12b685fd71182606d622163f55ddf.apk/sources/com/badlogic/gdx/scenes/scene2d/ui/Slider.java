package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;

public class Slider extends ProgressBar {
    int draggingPointer;
    private Interpolation visualInterpolationInverse;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Slider(float min, float max, float stepSize, boolean vertical, Skin skin) {
        this(min, max, stepSize, vertical, (SliderStyle) skin.get("default-" + (vertical ? "vertical" : "horizontal"), SliderStyle.class));
    }

    public Slider(float min, float max, float stepSize, boolean vertical, Skin skin, String styleName) {
        this(min, max, stepSize, vertical, (SliderStyle) skin.get(styleName, SliderStyle.class));
    }

    public Slider(float min, float max, float stepSize, boolean vertical, SliderStyle style) {
        super(min, max, stepSize, vertical, style);
        this.draggingPointer = -1;
        this.visualInterpolationInverse = Interpolation.linear;
        this.shiftIgnoresSnap = true;
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (Slider.this.disabled || Slider.this.draggingPointer != -1) {
                    return false;
                }
                Slider.this.draggingPointer = pointer;
                Slider.this.calculatePositionAndValue(x, y);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (pointer == Slider.this.draggingPointer) {
                    Slider.this.draggingPointer = -1;
                    if (!Slider.this.calculatePositionAndValue(x, y)) {
                        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
                        Slider.this.fire(changeEvent);
                        Pools.free(changeEvent);
                    }
                }
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                Slider.this.calculatePositionAndValue(x, y);
            }
        });
    }

    public void setStyle(SliderStyle style) {
        if (style == null) {
            throw new NullPointerException("style cannot be null");
        } else if (!(style instanceof SliderStyle)) {
            throw new IllegalArgumentException("style must be a SliderStyle.");
        } else {
            super.setStyle((ProgressBar.ProgressBarStyle) style);
        }
    }

    public SliderStyle getStyle() {
        return (SliderStyle) super.getStyle();
    }

    /* access modifiers changed from: package-private */
    public boolean calculatePositionAndValue(float x, float y) {
        float value;
        SliderStyle style = getStyle();
        Drawable knob = (!this.disabled || style.disabledKnob == null) ? style.knob : style.disabledKnob;
        Drawable bg = (!this.disabled || style.disabledBackground == null) ? style.background : style.disabledBackground;
        float oldPosition = this.position;
        float min = getMinValue();
        float max = getMaxValue();
        if (this.vertical) {
            float height = (getHeight() - bg.getTopHeight()) - bg.getBottomHeight();
            float knobHeight = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinHeight();
            this.position = (y - bg.getBottomHeight()) - (0.5f * knobHeight);
            value = min + ((max - min) * this.visualInterpolationInverse.apply(this.position / (height - knobHeight)));
            this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
            this.position = Math.min(height - knobHeight, this.position);
        } else {
            float width = (getWidth() - bg.getLeftWidth()) - bg.getRightWidth();
            float knobWidth = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinWidth();
            this.position = (x - bg.getLeftWidth()) - (0.5f * knobWidth);
            value = min + ((max - min) * this.visualInterpolationInverse.apply(this.position / (width - knobWidth)));
            this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
            this.position = Math.min(width - knobWidth, this.position);
        }
        boolean valueSet = setValue(value);
        if (value == value) {
            this.position = oldPosition;
        }
        return valueSet;
    }

    public boolean isDragging() {
        return this.draggingPointer != -1;
    }

    public void setVisualInterpolationInverse(Interpolation interpolation) {
        this.visualInterpolationInverse = interpolation;
    }

    public static class SliderStyle extends ProgressBar.ProgressBarStyle {
        public SliderStyle() {
        }

        public SliderStyle(Drawable background, Drawable knob) {
            super(background, knob);
        }

        public SliderStyle(SliderStyle style) {
            super(style);
        }
    }
}
