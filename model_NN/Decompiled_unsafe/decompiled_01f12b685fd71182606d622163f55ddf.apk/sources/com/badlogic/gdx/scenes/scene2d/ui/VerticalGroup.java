package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.Layout;
import com.badlogic.gdx.utils.SnapshotArray;
import com.kbz.esotericsoftware.spine.Animation;

public class VerticalGroup extends WidgetGroup {
    private int align;
    private float fill;
    private float padBottom;
    private float padLeft;
    private float padRight;
    private float padTop;
    private float prefHeight;
    private float prefWidth;
    private boolean reverse;
    private boolean round = true;
    private boolean sizeInvalid = true;
    private float spacing;

    public VerticalGroup() {
        setTouchable(Touchable.childrenOnly);
    }

    public void invalidate() {
        super.invalidate();
        this.sizeInvalid = true;
    }

    private void computeSize() {
        this.sizeInvalid = false;
        SnapshotArray<Actor> children = getChildren();
        int n = children.size;
        this.prefWidth = Animation.CurveTimeline.LINEAR;
        this.prefHeight = this.padTop + this.padBottom + (this.spacing * ((float) (n - 1)));
        for (int i = 0; i < n; i++) {
            Actor child = children.get(i);
            if (child instanceof Layout) {
                Layout layout = (Layout) child;
                this.prefWidth = Math.max(this.prefWidth, layout.getPrefWidth());
                this.prefHeight += layout.getPrefHeight();
            } else {
                this.prefWidth = Math.max(this.prefWidth, child.getWidth());
                this.prefHeight += child.getHeight();
            }
        }
        this.prefWidth += this.padLeft + this.padRight;
        if (this.round) {
            this.prefWidth = (float) Math.round(this.prefWidth);
            this.prefHeight = (float) Math.round(this.prefHeight);
        }
    }

    public void layout() {
        float width;
        float height;
        float width2;
        float spacing2 = this.spacing;
        float padLeft2 = this.padLeft;
        int align2 = this.align;
        boolean reverse2 = this.reverse;
        boolean round2 = this.round;
        float groupWidth = (getWidth() - padLeft2) - this.padRight;
        float y = reverse2 ? this.padBottom : (getHeight() - this.padTop) + spacing2;
        SnapshotArray<Actor> children = getChildren();
        int n = children.size;
        for (int i = 0; i < n; i++) {
            Actor child = children.get(i);
            if (child instanceof Layout) {
                Layout layout = (Layout) child;
                if (this.fill > Animation.CurveTimeline.LINEAR) {
                    width2 = groupWidth * this.fill;
                } else {
                    width2 = Math.min(layout.getPrefWidth(), groupWidth);
                }
                width = Math.max(width2, layout.getMinWidth());
                float maxWidth = layout.getMaxWidth();
                if (maxWidth > Animation.CurveTimeline.LINEAR && width > maxWidth) {
                    width = maxWidth;
                }
                height = layout.getPrefHeight();
            } else {
                width = child.getWidth();
                height = child.getHeight();
                if (this.fill > Animation.CurveTimeline.LINEAR) {
                    width *= this.fill;
                }
            }
            float x = padLeft2;
            if ((align2 & 16) != 0) {
                x += groupWidth - width;
            } else if ((align2 & 8) == 0) {
                x += (groupWidth - width) / 2.0f;
            }
            if (!reverse2) {
                y -= height + spacing2;
            }
            if (round2) {
                child.setBounds((float) Math.round(x), (float) Math.round(y), (float) Math.round(width), (float) Math.round(height));
            } else {
                child.setBounds(x, y, width, height);
            }
            if (reverse2) {
                y += height + spacing2;
            }
        }
    }

    public float getPrefWidth() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.prefWidth;
    }

    public float getPrefHeight() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.prefHeight;
    }

    public void setRound(boolean round2) {
        this.round = round2;
    }

    public VerticalGroup reverse() {
        reverse(true);
        return this;
    }

    public VerticalGroup reverse(boolean reverse2) {
        this.reverse = reverse2;
        return this;
    }

    public boolean getReverse() {
        return this.reverse;
    }

    public VerticalGroup space(float spacing2) {
        this.spacing = spacing2;
        return this;
    }

    public float getSpace() {
        return this.spacing;
    }

    public VerticalGroup pad(float pad) {
        this.padTop = pad;
        this.padLeft = pad;
        this.padBottom = pad;
        this.padRight = pad;
        return this;
    }

    public VerticalGroup pad(float top, float left, float bottom, float right) {
        this.padTop = top;
        this.padLeft = left;
        this.padBottom = bottom;
        this.padRight = right;
        return this;
    }

    public VerticalGroup padTop(float padTop2) {
        this.padTop = padTop2;
        return this;
    }

    public VerticalGroup padLeft(float padLeft2) {
        this.padLeft = padLeft2;
        return this;
    }

    public VerticalGroup padBottom(float padBottom2) {
        this.padBottom = padBottom2;
        return this;
    }

    public VerticalGroup padRight(float padRight2) {
        this.padRight = padRight2;
        return this;
    }

    public float getPadTop() {
        return this.padTop;
    }

    public float getPadLeft() {
        return this.padLeft;
    }

    public float getPadBottom() {
        return this.padBottom;
    }

    public float getPadRight() {
        return this.padRight;
    }

    public VerticalGroup align(int align2) {
        this.align = align2;
        return this;
    }

    public VerticalGroup center() {
        this.align = 1;
        return this;
    }

    public VerticalGroup left() {
        this.align |= 8;
        this.align &= -17;
        return this;
    }

    public VerticalGroup right() {
        this.align |= 16;
        this.align &= -9;
        return this;
    }

    public int getAlign() {
        return this.align;
    }

    public VerticalGroup fill() {
        this.fill = 1.0f;
        return this;
    }

    public VerticalGroup fill(float fill2) {
        this.fill = fill2;
        return this;
    }

    public float getFill() {
        return this.fill;
    }
}
