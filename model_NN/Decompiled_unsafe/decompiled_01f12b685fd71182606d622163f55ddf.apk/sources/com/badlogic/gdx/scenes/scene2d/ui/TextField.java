package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.UIUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Clipboard;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.kbz.esotericsoftware.spine.Animation;

public class TextField extends Widget implements Disableable {
    private static final char BACKSPACE = '\b';
    private static final char BULLET = '';
    private static final char DELETE = '';
    protected static final char ENTER_ANDROID = '\n';
    protected static final char ENTER_DESKTOP = '\r';
    private static final char TAB = '\t';
    public static float keyRepeatInitialTime = 0.4f;
    public static float keyRepeatTime = 0.1f;
    private static final Vector2 tmp1 = new Vector2();
    private static final Vector2 tmp2 = new Vector2();
    private static final Vector2 tmp3 = new Vector2();
    private float blinkTime;
    private Clipboard clipboard;
    protected int cursor;
    boolean cursorOn;
    boolean disabled;
    protected CharSequence displayText;
    TextFieldFilter filter;
    boolean focusTraversal;
    private float fontOffset;
    protected final FloatArray glyphPositions;
    protected boolean hasSelection;
    InputListener inputListener;
    KeyRepeatTask keyRepeatTask;
    OnscreenKeyboard keyboard;
    long lastBlink;
    protected final GlyphLayout layout;
    TextFieldListener listener;
    private int maxLength;
    private String messageText;
    boolean onlyFontChars;
    private StringBuilder passwordBuffer;
    private char passwordCharacter;
    boolean passwordMode;
    float renderOffset;
    protected int selectionStart;
    private float selectionWidth;
    private float selectionX;
    TextFieldStyle style;
    protected String text;
    private int textHAlign;
    protected float textHeight;
    protected float textOffset;
    private int visibleTextEnd;
    private int visibleTextStart;
    protected boolean writeEnters;

    public interface OnscreenKeyboard {
        void show(boolean z);
    }

    public interface TextFieldListener {
        void keyTyped(TextField textField, char c);
    }

    public TextField(String text2, Skin skin) {
        this(text2, (TextFieldStyle) skin.get(TextFieldStyle.class));
    }

    public TextField(String text2, Skin skin, String styleName) {
        this(text2, (TextFieldStyle) skin.get(styleName, TextFieldStyle.class));
    }

    public TextField(String text2, TextFieldStyle style2) {
        this.layout = new GlyphLayout();
        this.glyphPositions = new FloatArray();
        this.keyboard = new DefaultOnscreenKeyboard();
        this.focusTraversal = true;
        this.onlyFontChars = true;
        this.textHAlign = 8;
        this.passwordCharacter = BULLET;
        this.maxLength = 0;
        this.blinkTime = 0.32f;
        this.cursorOn = true;
        this.keyRepeatTask = new KeyRepeatTask();
        setStyle(style2);
        this.clipboard = Gdx.app.getClipboard();
        initialize();
        setText(text2);
        setSize(getPrefWidth(), getPrefHeight());
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        InputListener createInputListener = createInputListener();
        this.inputListener = createInputListener;
        addListener(createInputListener);
    }

    /* access modifiers changed from: protected */
    public InputListener createInputListener() {
        return new TextFieldClickListener();
    }

    /* access modifiers changed from: protected */
    public int letterUnderCursor(float x) {
        float x2 = x - (this.renderOffset + this.textOffset);
        int index = this.glyphPositions.size - 1;
        float[] glyphPositions2 = this.glyphPositions.items;
        int i = 0;
        int n = this.glyphPositions.size;
        while (true) {
            if (i >= n) {
                break;
            } else if (glyphPositions2[i] > x2) {
                index = i - 1;
                break;
            } else {
                i++;
            }
        }
        return Math.max(0, index);
    }

    /* access modifiers changed from: protected */
    public boolean isWordCharacter(char c) {
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9');
    }

    /* access modifiers changed from: protected */
    public int[] wordUnderCursor(int at) {
        String text2 = this.text;
        int start = at;
        int right = text2.length();
        int left = 0;
        int index = start;
        while (true) {
            if (index >= right) {
                break;
            } else if (!isWordCharacter(text2.charAt(index))) {
                right = index;
                break;
            } else {
                index++;
            }
        }
        int index2 = start - 1;
        while (true) {
            if (index2 <= -1) {
                break;
            } else if (!isWordCharacter(text2.charAt(index2))) {
                left = index2 + 1;
                break;
            } else {
                index2--;
            }
        }
        return new int[]{left, right};
    }

    /* access modifiers changed from: package-private */
    public int[] wordUnderCursor(float x) {
        return wordUnderCursor(letterUnderCursor(x));
    }

    /* access modifiers changed from: package-private */
    public boolean withinMaxLength(int size) {
        return this.maxLength <= 0 || size < this.maxLength;
    }

    public void setMaxLength(int maxLength2) {
        this.maxLength = maxLength2;
    }

    public int getMaxLength() {
        return this.maxLength;
    }

    public void setOnlyFontChars(boolean onlyFontChars2) {
        this.onlyFontChars = onlyFontChars2;
    }

    public void setStyle(TextFieldStyle style2) {
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        this.style = style2;
        this.textHeight = style2.font.getCapHeight() - (style2.font.getDescent() * 2.0f);
        invalidateHierarchy();
    }

    public TextFieldStyle getStyle() {
        return this.style;
    }

    /* access modifiers changed from: protected */
    public void calculateOffsets() {
        float visibleWidth = getWidth();
        if (this.style.background != null) {
            visibleWidth -= this.style.background.getLeftWidth() + this.style.background.getRightWidth();
        }
        float distance = this.glyphPositions.get(this.cursor) - Math.abs(this.renderOffset);
        if (distance <= Animation.CurveTimeline.LINEAR) {
            if (this.cursor > 0) {
                this.renderOffset = -this.glyphPositions.get(this.cursor - 1);
            } else {
                this.renderOffset = Animation.CurveTimeline.LINEAR;
            }
        } else if (distance > visibleWidth) {
            this.renderOffset -= distance - visibleWidth;
        }
        this.visibleTextStart = 0;
        this.textOffset = Animation.CurveTimeline.LINEAR;
        float start = Math.abs(this.renderOffset);
        int glyphCount = this.glyphPositions.size;
        float[] glyphPositions2 = this.glyphPositions.items;
        float startPos = Animation.CurveTimeline.LINEAR;
        int i = 0;
        while (true) {
            if (i >= glyphCount) {
                break;
            } else if (glyphPositions2[i] >= start) {
                this.visibleTextStart = i;
                startPos = glyphPositions2[i];
                this.textOffset = startPos - start;
                break;
            } else {
                i++;
            }
        }
        this.visibleTextEnd = Math.min(this.displayText.length(), this.cursor + 1);
        while (this.visibleTextEnd <= this.displayText.length() && glyphPositions2[this.visibleTextEnd] - startPos <= visibleWidth) {
            this.visibleTextEnd++;
        }
        this.visibleTextEnd = Math.max(0, this.visibleTextEnd - 1);
        if (this.hasSelection) {
            int minIndex = Math.min(this.cursor, this.selectionStart);
            int maxIndex = Math.max(this.cursor, this.selectionStart);
            float minX = Math.max(glyphPositions2[minIndex], startPos);
            float maxX = Math.min(glyphPositions2[maxIndex], glyphPositions2[this.visibleTextEnd]);
            this.selectionX = minX;
            this.selectionWidth = maxX - minX;
        }
        if (this.textHAlign == 1 || this.textHAlign == 16) {
            this.textOffset = visibleWidth - (glyphPositions2[this.visibleTextEnd] - startPos);
            if (this.textHAlign == 1) {
                this.textOffset = (float) Math.round(this.textOffset * 0.5f);
            }
            if (this.hasSelection) {
                this.selectionX += this.textOffset;
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        Color fontColor;
        Drawable background;
        BitmapFont messageFont;
        Stage stage = getStage();
        boolean focused = stage != null && stage.getKeyboardFocus() == this;
        BitmapFont font = this.style.font;
        if (!this.disabled || this.style.disabledFontColor == null) {
            fontColor = (!focused || this.style.focusedFontColor == null) ? this.style.fontColor : this.style.focusedFontColor;
        } else {
            fontColor = this.style.disabledFontColor;
        }
        Drawable selection = this.style.selection;
        Drawable cursorPatch = this.style.cursor;
        if (!this.disabled || this.style.disabledBackground == null) {
            background = (!focused || this.style.focusedBackground == null) ? this.style.background : this.style.focusedBackground;
        } else {
            background = this.style.disabledBackground;
        }
        Color color = getColor();
        float x = getX();
        float y = getY();
        float width = getWidth();
        float height = getHeight();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        float bgLeftWidth = Animation.CurveTimeline.LINEAR;
        if (background != null) {
            background.draw(batch, x, y, width, height);
            bgLeftWidth = background.getLeftWidth();
        }
        float textY = getTextY(font, background);
        calculateOffsets();
        if (focused && this.hasSelection && selection != null) {
            drawSelection(selection, batch, font, x + bgLeftWidth, y + textY);
        }
        float yOffset = font.isFlipped() ? -this.textHeight : Animation.CurveTimeline.LINEAR;
        if (this.displayText.length() != 0) {
            font.setColor(fontColor.r, fontColor.g, fontColor.b, fontColor.a * parentAlpha);
            drawText(batch, font, x + bgLeftWidth, y + textY + yOffset);
        } else if (!focused && this.messageText != null) {
            if (this.style.messageFontColor != null) {
                font.setColor(this.style.messageFontColor.r, this.style.messageFontColor.g, this.style.messageFontColor.b, this.style.messageFontColor.a * parentAlpha);
            } else {
                font.setColor(0.7f, 0.7f, 0.7f, parentAlpha);
            }
            if (this.style.messageFont != null) {
                messageFont = this.style.messageFont;
            } else {
                messageFont = font;
            }
            messageFont.draw(batch, this.messageText, x + bgLeftWidth, y + textY + yOffset);
        }
        if (focused && !this.disabled) {
            blink();
            if (this.cursorOn && cursorPatch != null) {
                drawCursor(cursorPatch, batch, font, x + bgLeftWidth, y + textY);
            }
        }
    }

    /* access modifiers changed from: protected */
    public float getTextY(BitmapFont font, Drawable background) {
        float height = getHeight();
        float textY = (this.textHeight / 2.0f) + font.getDescent();
        if (background == null) {
            return (float) ((int) ((height / 2.0f) + textY));
        }
        float bottom = background.getBottomHeight();
        return (float) ((int) ((((height - background.getTopHeight()) - bottom) / 2.0f) + textY + bottom));
    }

    /* access modifiers changed from: protected */
    public void drawSelection(Drawable selection, Batch batch, BitmapFont font, float x, float y) {
        selection.draw(batch, (((this.selectionX + x) + this.renderOffset) + this.fontOffset) - 1.0f, (y - this.textHeight) - font.getDescent(), this.selectionWidth, this.textHeight + (font.getDescent() / 2.0f));
    }

    /* access modifiers changed from: protected */
    public void drawText(Batch batch, BitmapFont font, float x, float y) {
        font.draw(batch, this.displayText, x + this.textOffset, y, this.visibleTextStart, this.visibleTextEnd, Animation.CurveTimeline.LINEAR, 8, false);
    }

    /* access modifiers changed from: protected */
    public void drawCursor(Drawable cursorPatch, Batch batch, BitmapFont font, float x, float y) {
        cursorPatch.draw(batch, ((((this.textOffset + x) + this.glyphPositions.get(this.cursor)) - this.glyphPositions.items[this.visibleTextStart]) + this.fontOffset) - 1.0f, (y - this.textHeight) - font.getDescent(), cursorPatch.getMinWidth(), this.textHeight + (font.getDescent() / 2.0f));
    }

    /* access modifiers changed from: package-private */
    public void updateDisplayText() {
        BitmapFont font = this.style.font;
        BitmapFont.BitmapFontData data = font.getData();
        String text2 = this.text;
        int textLength = text2.length();
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < textLength; i++) {
            char c = text2.charAt(i);
            if (!data.hasGlyph(c)) {
                c = ' ';
            }
            buffer.append(c);
        }
        String newDisplayText = buffer.toString();
        if (!this.passwordMode || !data.hasGlyph(this.passwordCharacter)) {
            this.displayText = newDisplayText;
        } else {
            if (this.passwordBuffer == null) {
                this.passwordBuffer = new StringBuilder(newDisplayText.length());
            }
            if (this.passwordBuffer.length() > textLength) {
                this.passwordBuffer.setLength(textLength);
            } else {
                for (int i2 = this.passwordBuffer.length(); i2 < textLength; i2++) {
                    this.passwordBuffer.append(this.passwordCharacter);
                }
            }
            this.displayText = this.passwordBuffer;
        }
        this.layout.setText(font, this.displayText);
        this.glyphPositions.clear();
        float x = Animation.CurveTimeline.LINEAR;
        if (this.layout.runs.size > 0) {
            GlyphLayout.GlyphRun run = this.layout.runs.first();
            Array<BitmapFont.Glyph> array = run.glyphs;
            FloatArray xAdvances = run.xAdvances;
            this.fontOffset = xAdvances.first();
            int n = xAdvances.size;
            for (int i3 = 1; i3 < n; i3++) {
                this.glyphPositions.add(x);
                x += xAdvances.get(i3);
            }
        }
        this.glyphPositions.add(x);
        if (this.selectionStart > newDisplayText.length()) {
            this.selectionStart = textLength;
        }
    }

    private void blink() {
        boolean z = true;
        if (!Gdx.graphics.isContinuousRendering()) {
            this.cursorOn = true;
            return;
        }
        long time = TimeUtils.nanoTime();
        if (((float) (time - this.lastBlink)) / 1.0E9f > this.blinkTime) {
            if (this.cursorOn) {
                z = false;
            }
            this.cursorOn = z;
            this.lastBlink = time;
        }
    }

    public void copy() {
        if (this.hasSelection && !this.passwordMode) {
            this.clipboard.setContents(this.text.substring(Math.min(this.cursor, this.selectionStart), Math.max(this.cursor, this.selectionStart)));
        }
    }

    public void cut() {
        if (this.hasSelection && !this.passwordMode) {
            copy();
            this.cursor = delete();
        }
    }

    /* access modifiers changed from: package-private */
    public void paste() {
        paste(this.clipboard.getContents());
    }

    /* access modifiers changed from: package-private */
    public void paste(String content) {
        if (content != null) {
            StringBuilder buffer = new StringBuilder();
            int textLength = this.text.length();
            BitmapFont.BitmapFontData data = this.style.font.getData();
            int n = content.length();
            for (int i = 0; i < n && withinMaxLength(buffer.length() + textLength); i++) {
                char c = content.charAt(i);
                if ((this.writeEnters && (c == 10 || c == 13)) || ((!this.onlyFontChars || data.hasGlyph(c)) && (this.filter == null || this.filter.acceptChar(this, c)))) {
                    buffer.append(c);
                }
            }
            String content2 = buffer.toString();
            if (this.hasSelection) {
                this.cursor = delete(false);
            }
            this.text = insert(this.cursor, content2, this.text);
            updateDisplayText();
            this.cursor += content2.length();
        }
    }

    /* access modifiers changed from: package-private */
    public String insert(int position, CharSequence text2, String to) {
        if (to.length() == 0) {
            return text2.toString();
        }
        return String.valueOf(to.substring(0, position)) + ((Object) text2) + to.substring(position, to.length());
    }

    /* access modifiers changed from: package-private */
    public int delete() {
        return delete(true);
    }

    /* access modifiers changed from: package-private */
    public int delete(boolean updateText) {
        return delete(this.selectionStart, this.cursor, updateText);
    }

    /* access modifiers changed from: package-private */
    public int delete(int from, int to, boolean updateText) {
        int minIndex = Math.min(from, to);
        int maxIndex = Math.max(from, to);
        this.text = String.valueOf(minIndex > 0 ? this.text.substring(0, minIndex) : "") + (maxIndex < this.text.length() ? this.text.substring(maxIndex, this.text.length()) : "");
        if (updateText) {
            updateDisplayText();
        }
        clearSelection();
        return minIndex;
    }

    public void next(boolean up) {
        Stage stage = getStage();
        if (stage != null) {
            getParent().localToStageCoordinates(tmp1.set(getX(), getY()));
            TextField textField = findNextTextField(stage.getActors(), null, tmp2, tmp1, up);
            if (textField == null) {
                if (up) {
                    tmp1.set(Float.MIN_VALUE, Float.MIN_VALUE);
                } else {
                    tmp1.set(Float.MAX_VALUE, Float.MAX_VALUE);
                }
                textField = findNextTextField(getStage().getActors(), null, tmp2, tmp1, up);
            }
            if (textField != null) {
                stage.setKeyboardFocus(textField);
            } else {
                Gdx.input.setOnscreenKeyboardVisible(false);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: com.badlogic.gdx.scenes.scene2d.Actor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v0, resolved type: com.badlogic.gdx.scenes.scene2d.ui.TextField} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: com.badlogic.gdx.scenes.scene2d.ui.TextField} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.badlogic.gdx.scenes.scene2d.ui.TextField findNextTextField(com.badlogic.gdx.utils.Array<com.badlogic.gdx.scenes.scene2d.Actor> r12, com.badlogic.gdx.scenes.scene2d.ui.TextField r13, com.badlogic.gdx.math.Vector2 r14, com.badlogic.gdx.math.Vector2 r15, boolean r16) {
        /*
            r11 = this;
            r8 = 0
            int r9 = r12.size
        L_0x0003:
            if (r8 < r9) goto L_0x0006
            return r13
        L_0x0006:
            java.lang.Object r6 = r12.get(r8)
            com.badlogic.gdx.scenes.scene2d.Actor r6 = (com.badlogic.gdx.scenes.scene2d.Actor) r6
            if (r6 != r11) goto L_0x0011
        L_0x000e:
            int r8 = r8 + 1
            goto L_0x0003
        L_0x0011:
            boolean r0 = r6 instanceof com.badlogic.gdx.scenes.scene2d.ui.TextField
            if (r0 == 0) goto L_0x007f
            r10 = r6
            com.badlogic.gdx.scenes.scene2d.ui.TextField r10 = (com.badlogic.gdx.scenes.scene2d.ui.TextField) r10
            boolean r0 = r10.isDisabled()
            if (r0 != 0) goto L_0x000e
            boolean r0 = r10.focusTraversal
            if (r0 == 0) goto L_0x000e
            com.badlogic.gdx.scenes.scene2d.Group r0 = r6.getParent()
            com.badlogic.gdx.math.Vector2 r1 = com.badlogic.gdx.scenes.scene2d.ui.TextField.tmp3
            float r2 = r6.getX()
            float r3 = r6.getY()
            com.badlogic.gdx.math.Vector2 r1 = r1.set(r2, r3)
            com.badlogic.gdx.math.Vector2 r7 = r0.localToStageCoordinates(r1)
            float r0 = r7.y
            float r1 = r15.y
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x007b
            float r0 = r7.y
            float r1 = r15.y
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0050
            float r0 = r7.x
            float r1 = r15.x
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x007b
        L_0x0050:
            r0 = 0
        L_0x0051:
            r0 = r0 ^ r16
            if (r0 == 0) goto L_0x000e
            if (r13 == 0) goto L_0x0074
            float r0 = r7.y
            float r1 = r14.y
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x007d
            float r0 = r7.y
            float r1 = r14.y
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x006f
            float r0 = r7.x
            float r1 = r14.x
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x007d
        L_0x006f:
            r0 = 0
        L_0x0070:
            r0 = r0 ^ r16
            if (r0 == 0) goto L_0x000e
        L_0x0074:
            r13 = r6
            com.badlogic.gdx.scenes.scene2d.ui.TextField r13 = (com.badlogic.gdx.scenes.scene2d.ui.TextField) r13
            r14.set(r7)
            goto L_0x000e
        L_0x007b:
            r0 = 1
            goto L_0x0051
        L_0x007d:
            r0 = 1
            goto L_0x0070
        L_0x007f:
            boolean r0 = r6 instanceof com.badlogic.gdx.scenes.scene2d.Group
            if (r0 == 0) goto L_0x000e
            com.badlogic.gdx.scenes.scene2d.Group r6 = (com.badlogic.gdx.scenes.scene2d.Group) r6
            com.badlogic.gdx.utils.SnapshotArray r1 = r6.getChildren()
            r0 = r11
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r16
            com.badlogic.gdx.scenes.scene2d.ui.TextField r13 = r0.findNextTextField(r1, r2, r3, r4, r5)
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.TextField.findNextTextField(com.badlogic.gdx.utils.Array, com.badlogic.gdx.scenes.scene2d.ui.TextField, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.math.Vector2, boolean):com.badlogic.gdx.scenes.scene2d.ui.TextField");
    }

    public InputListener getDefaultInputListener() {
        return this.inputListener;
    }

    public void setTextFieldListener(TextFieldListener listener2) {
        this.listener = listener2;
    }

    public void setTextFieldFilter(TextFieldFilter filter2) {
        this.filter = filter2;
    }

    public TextFieldFilter getTextFieldFilter() {
        return this.filter;
    }

    public void setFocusTraversal(boolean focusTraversal2) {
        this.focusTraversal = focusTraversal2;
    }

    public String getMessageText() {
        return this.messageText;
    }

    public void setMessageText(String messageText2) {
        this.messageText = messageText2;
    }

    public void appendText(String str) {
        if (str == null) {
            throw new IllegalArgumentException("text cannot be null.");
        }
        clearSelection();
        this.cursor = this.text.length();
        paste(str);
    }

    public void setText(String str) {
        if (str == null) {
            throw new IllegalArgumentException("text cannot be null.");
        } else if (!str.equals(this.text)) {
            clearSelection();
            this.text = "";
            paste(str);
            this.cursor = 0;
        }
    }

    public String getText() {
        return this.text;
    }

    public int getSelectionStart() {
        return this.selectionStart;
    }

    public String getSelection() {
        return this.hasSelection ? this.text.substring(Math.min(this.selectionStart, this.cursor), Math.max(this.selectionStart, this.cursor)) : "";
    }

    public void setSelection(int selectionStart2, int selectionEnd) {
        if (selectionStart2 < 0) {
            throw new IllegalArgumentException("selectionStart must be >= 0");
        } else if (selectionEnd < 0) {
            throw new IllegalArgumentException("selectionEnd must be >= 0");
        } else {
            int selectionStart3 = Math.min(this.text.length(), selectionStart2);
            int selectionEnd2 = Math.min(this.text.length(), selectionEnd);
            if (selectionEnd2 == selectionStart3) {
                clearSelection();
                return;
            }
            if (selectionEnd2 < selectionStart3) {
                int temp = selectionEnd2;
                selectionEnd2 = selectionStart3;
                selectionStart3 = temp;
            }
            this.hasSelection = true;
            this.selectionStart = selectionStart3;
            this.cursor = selectionEnd2;
        }
    }

    public void selectAll() {
        setSelection(0, this.text.length());
    }

    public void clearSelection() {
        this.hasSelection = false;
    }

    public void setCursorPosition(int cursorPosition) {
        if (cursorPosition < 0) {
            throw new IllegalArgumentException("cursorPosition must be >= 0");
        }
        clearSelection();
        this.cursor = Math.min(cursorPosition, this.text.length());
    }

    public int getCursorPosition() {
        return this.cursor;
    }

    public OnscreenKeyboard getOnscreenKeyboard() {
        return this.keyboard;
    }

    public void setOnscreenKeyboard(OnscreenKeyboard keyboard2) {
        this.keyboard = keyboard2;
    }

    public void setClipboard(Clipboard clipboard2) {
        this.clipboard = clipboard2;
    }

    public float getPrefWidth() {
        return 150.0f;
    }

    public float getPrefHeight() {
        float prefHeight = this.textHeight;
        if (this.style.background != null) {
            return Math.max(this.style.background.getBottomHeight() + prefHeight + this.style.background.getTopHeight(), this.style.background.getMinHeight());
        }
        return prefHeight;
    }

    public void setAlignment(int alignment) {
        if (alignment == 8 || alignment == 1 || alignment == 16) {
            this.textHAlign = alignment;
        }
    }

    public void setPasswordMode(boolean passwordMode2) {
        this.passwordMode = passwordMode2;
        updateDisplayText();
    }

    public boolean isPasswordMode() {
        return this.passwordMode;
    }

    public void setPasswordCharacter(char passwordCharacter2) {
        this.passwordCharacter = passwordCharacter2;
        if (this.passwordMode) {
            updateDisplayText();
        }
    }

    public void setBlinkTime(float blinkTime2) {
        this.blinkTime = blinkTime2;
    }

    public void setDisabled(boolean disabled2) {
        this.disabled = disabled2;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    /* access modifiers changed from: protected */
    public void moveCursor(boolean forward, boolean jump) {
        int limit;
        int charOffset = 0;
        if (forward) {
            limit = this.text.length();
        } else {
            limit = 0;
        }
        if (!forward) {
            charOffset = -1;
        }
        do {
            if (forward) {
                int i = this.cursor + 1;
                this.cursor = i;
                if (i >= limit) {
                    return;
                }
            } else {
                int i2 = this.cursor - 1;
                this.cursor = i2;
                if (i2 <= limit) {
                    return;
                }
            }
            if (!jump) {
                return;
            }
        } while (continueCursor(this.cursor, charOffset));
    }

    /* access modifiers changed from: protected */
    public boolean continueCursor(int index, int offset) {
        return isWordCharacter(this.text.charAt(index + offset));
    }

    class KeyRepeatTask extends Timer.Task {
        int keycode;

        KeyRepeatTask() {
        }

        public void run() {
            TextField.this.inputListener.keyDown(null, this.keycode);
        }
    }

    public interface TextFieldFilter {
        boolean acceptChar(TextField textField, char c);

        public static class DigitsOnlyFilter implements TextFieldFilter {
            public boolean acceptChar(TextField textField, char c) {
                return Character.isDigit(c);
            }
        }
    }

    public static class DefaultOnscreenKeyboard implements OnscreenKeyboard {
        public void show(boolean visible) {
            Gdx.input.setOnscreenKeyboardVisible(visible);
        }
    }

    public class TextFieldClickListener extends ClickListener {
        public TextFieldClickListener() {
        }

        public void clicked(InputEvent event, float x, float y) {
            int count = getTapCount() % 4;
            if (count == 0) {
                TextField.this.clearSelection();
            }
            if (count == 2) {
                int[] array = TextField.this.wordUnderCursor(x);
                TextField.this.setSelection(array[0], array[1]);
            }
            if (count == 3) {
                TextField.this.selectAll();
            }
        }

        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if (!super.touchDown(event, x, y, pointer, button)) {
                return false;
            }
            if (pointer == 0 && button != 0) {
                return false;
            }
            if (TextField.this.disabled) {
                return true;
            }
            setCursorPosition(x, y);
            TextField.this.selectionStart = TextField.this.cursor;
            Stage stage = TextField.this.getStage();
            if (stage != null) {
                stage.setKeyboardFocus(TextField.this);
            }
            TextField.this.keyboard.show(true);
            TextField.this.hasSelection = true;
            return true;
        }

        public void touchDragged(InputEvent event, float x, float y, int pointer) {
            super.touchDragged(event, x, y, pointer);
            setCursorPosition(x, y);
        }

        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            if (TextField.this.selectionStart == TextField.this.cursor) {
                TextField.this.hasSelection = false;
            }
            super.touchUp(event, x, y, pointer, button);
        }

        /* access modifiers changed from: protected */
        public void setCursorPosition(float x, float y) {
            TextField.this.lastBlink = 0;
            TextField.this.cursorOn = false;
            TextField.this.cursor = TextField.this.letterUnderCursor(x);
        }

        /* access modifiers changed from: protected */
        public void goHome(boolean jump) {
            TextField.this.cursor = 0;
        }

        /* access modifiers changed from: protected */
        public void goEnd(boolean jump) {
            TextField.this.cursor = TextField.this.text.length();
        }

        public boolean keyDown(InputEvent event, int keycode) {
            boolean jump;
            if (TextField.this.disabled) {
                return false;
            }
            TextField.this.lastBlink = 0;
            TextField.this.cursorOn = false;
            Stage stage = TextField.this.getStage();
            if (stage == null || stage.getKeyboardFocus() != TextField.this) {
                return false;
            }
            boolean repeat = false;
            boolean ctrl = UIUtils.ctrl();
            if (!ctrl || TextField.this.passwordMode) {
                jump = false;
            } else {
                jump = true;
            }
            if (ctrl) {
                if (keycode == 50) {
                    TextField.this.paste();
                    repeat = true;
                }
                if (keycode == 31 || keycode == 133) {
                    TextField.this.copy();
                    return true;
                } else if (keycode == 52 || keycode == 67) {
                    TextField.this.cut();
                    return true;
                } else if (keycode == 29) {
                    TextField.this.selectAll();
                    return true;
                }
            }
            if (UIUtils.shift()) {
                if (keycode == 133) {
                    TextField.this.paste();
                }
                if (keycode == 112 && TextField.this.hasSelection) {
                    TextField.this.copy();
                    TextField.this.delete();
                }
                int temp = TextField.this.cursor;
                if (keycode == 21) {
                    TextField.this.moveCursor(false, jump);
                    repeat = true;
                } else if (keycode == 22) {
                    TextField.this.moveCursor(true, jump);
                    repeat = true;
                } else if (keycode == 3) {
                    goHome(jump);
                } else if (keycode == 132) {
                    goEnd(jump);
                }
                if (!TextField.this.hasSelection) {
                    TextField.this.selectionStart = temp;
                    TextField.this.hasSelection = true;
                }
            } else {
                if (keycode == 21) {
                    TextField.this.moveCursor(false, jump);
                    TextField.this.clearSelection();
                    repeat = true;
                }
                if (keycode == 22) {
                    TextField.this.moveCursor(true, jump);
                    TextField.this.clearSelection();
                    repeat = true;
                }
                if (keycode == 3) {
                    goHome(jump);
                    TextField.this.clearSelection();
                }
                if (keycode == 132) {
                    goEnd(jump);
                    TextField.this.clearSelection();
                }
            }
            TextField.this.cursor = MathUtils.clamp(TextField.this.cursor, 0, TextField.this.text.length());
            if (repeat) {
                scheduleKeyRepeatTask(keycode);
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void scheduleKeyRepeatTask(int keycode) {
            if (!TextField.this.keyRepeatTask.isScheduled() || TextField.this.keyRepeatTask.keycode != keycode) {
                TextField.this.keyRepeatTask.keycode = keycode;
                TextField.this.keyRepeatTask.cancel();
                Timer.schedule(TextField.this.keyRepeatTask, TextField.keyRepeatInitialTime, TextField.keyRepeatTime);
            }
        }

        public boolean keyUp(InputEvent event, int keycode) {
            if (TextField.this.disabled) {
                return false;
            }
            TextField.this.keyRepeatTask.cancel();
            return true;
        }

        public boolean keyTyped(InputEvent event, char character) {
            if (TextField.this.disabled) {
                return false;
            }
            switch (character) {
                case 8:
                case 9:
                case 10:
                case 13:
                    break;
                case 11:
                case 12:
                default:
                    if (character < ' ') {
                        return false;
                    }
                    break;
            }
            Stage stage = TextField.this.getStage();
            if (stage == null || stage.getKeyboardFocus() != TextField.this) {
                return false;
            }
            if ((character == 9 || character == 10) && TextField.this.focusTraversal) {
                TextField.this.next(UIUtils.shift());
            } else {
                boolean delete = character == 127;
                boolean backspace = character == 8;
                boolean enter = character == 13 || character == 10;
                boolean add = enter ? TextField.this.writeEnters : !TextField.this.onlyFontChars || TextField.this.style.font.getData().hasGlyph(character);
                boolean remove = backspace || delete;
                if (add || remove) {
                    if (TextField.this.hasSelection) {
                        TextField.this.cursor = TextField.this.delete(false);
                    } else {
                        if (backspace && TextField.this.cursor > 0) {
                            TextField textField = TextField.this;
                            StringBuilder sb = new StringBuilder(String.valueOf(TextField.this.text.substring(0, TextField.this.cursor - 1)));
                            String str = TextField.this.text;
                            TextField textField2 = TextField.this;
                            int i = textField2.cursor;
                            textField2.cursor = i - 1;
                            textField.text = sb.append(str.substring(i)).toString();
                            TextField.this.renderOffset = Animation.CurveTimeline.LINEAR;
                        }
                        if (delete && TextField.this.cursor < TextField.this.text.length()) {
                            TextField.this.text = String.valueOf(TextField.this.text.substring(0, TextField.this.cursor)) + TextField.this.text.substring(TextField.this.cursor + 1);
                        }
                    }
                    if (add && !remove) {
                        if ((!enter && TextField.this.filter != null && !TextField.this.filter.acceptChar(TextField.this, character)) || !TextField.this.withinMaxLength(TextField.this.text.length())) {
                            return true;
                        }
                        String insertion = enter ? "\n" : String.valueOf(character);
                        TextField textField3 = TextField.this;
                        TextField textField4 = TextField.this;
                        TextField textField5 = TextField.this;
                        int i2 = textField5.cursor;
                        textField5.cursor = i2 + 1;
                        textField3.text = textField4.insert(i2, insertion, TextField.this.text);
                    }
                    TextField.this.updateDisplayText();
                }
            }
            if (TextField.this.listener != null) {
                TextField.this.listener.keyTyped(TextField.this, character);
            }
            return true;
        }
    }

    public static class TextFieldStyle {
        public Drawable background;
        public Drawable cursor;
        public Drawable disabledBackground;
        public Color disabledFontColor;
        public Drawable focusedBackground;
        public Color focusedFontColor;
        public BitmapFont font;
        public Color fontColor;
        public BitmapFont messageFont;
        public Color messageFontColor;
        public Drawable selection;

        public TextFieldStyle() {
        }

        public TextFieldStyle(BitmapFont font2, Color fontColor2, Drawable cursor2, Drawable selection2, Drawable background2) {
            this.background = background2;
            this.cursor = cursor2;
            this.font = font2;
            this.fontColor = fontColor2;
            this.selection = selection2;
        }

        public TextFieldStyle(TextFieldStyle style) {
            this.messageFont = style.messageFont;
            if (style.messageFontColor != null) {
                this.messageFontColor = new Color(style.messageFontColor);
            }
            this.background = style.background;
            this.focusedBackground = style.focusedBackground;
            this.disabledBackground = style.disabledBackground;
            this.cursor = style.cursor;
            this.font = style.font;
            if (style.fontColor != null) {
                this.fontColor = new Color(style.fontColor);
            }
            if (style.focusedFontColor != null) {
                this.focusedFontColor = new Color(style.focusedFontColor);
            }
            if (style.disabledFontColor != null) {
                this.disabledFontColor = new Color(style.disabledFontColor);
            }
            this.selection = style.selection;
        }
    }
}
