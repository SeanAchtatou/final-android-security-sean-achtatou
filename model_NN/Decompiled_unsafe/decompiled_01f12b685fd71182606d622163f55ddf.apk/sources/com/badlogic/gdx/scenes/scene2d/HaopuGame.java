package com.badlogic.gdx.scenes.scene2d;

public class HaopuGame {
    public static boolean isSort;
    private boolean isPause;
    public int layer;

    public int getLayer() {
        return this.layer;
    }

    public void setLayer(int lay) {
        this.layer = lay;
        isSort = true;
    }

    public static void setisSort(boolean Sort) {
        isSort = Sort;
    }

    public static boolean getisSort() {
        return isSort;
    }

    public boolean isPause() {
        return this.isPause;
    }

    public void setPause(boolean isPause2) {
        this.isPause = isPause2;
    }
}
