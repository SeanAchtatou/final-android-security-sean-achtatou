package com.badlogic.gdx.scenes.scene2d.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.kbz.esotericsoftware.spine.Animation;

public class SpriteDrawable extends BaseDrawable implements TransformDrawable {
    private static Color tmpColor = new Color();
    private Sprite sprite;

    public SpriteDrawable() {
    }

    public SpriteDrawable(Sprite sprite2) {
        setSprite(sprite2);
    }

    public SpriteDrawable(SpriteDrawable drawable) {
        super(drawable);
        setSprite(drawable.sprite);
    }

    public void draw(Batch batch, float x, float y, float width, float height) {
        draw(batch, x, y, width / 2.0f, height / 2.0f, width, height, 1.0f, 1.0f, Animation.CurveTimeline.LINEAR);
    }

    public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
        this.sprite.setOrigin(originX, originY);
        this.sprite.setRotation(rotation);
        this.sprite.setScale(scaleX, scaleY);
        this.sprite.setBounds(x, y, width, height);
        Color color = this.sprite.getColor();
        this.sprite.setColor(tmpColor.set(color).mul(batch.getColor()));
        this.sprite.draw(batch);
        this.sprite.setColor(color);
    }

    public void setSprite(Sprite sprite2) {
        this.sprite = sprite2;
        setMinWidth(sprite2.getWidth());
        setMinHeight(sprite2.getHeight());
    }

    public Sprite getSprite() {
        return this.sprite;
    }

    public SpriteDrawable tint(Color tint) {
        Sprite sprite2;
        SpriteDrawable drawable = new SpriteDrawable(this);
        Sprite sprite3 = drawable.getSprite();
        if (sprite3 instanceof TextureAtlas.AtlasSprite) {
            sprite2 = new TextureAtlas.AtlasSprite((TextureAtlas.AtlasSprite) sprite3);
        } else {
            sprite2 = new Sprite(sprite3);
        }
        sprite2.setColor(tint);
        drawable.setSprite(sprite2);
        return drawable;
    }
}
