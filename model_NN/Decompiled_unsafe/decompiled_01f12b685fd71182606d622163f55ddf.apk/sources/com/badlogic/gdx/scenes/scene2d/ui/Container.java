package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kbz.esotericsoftware.spine.Animation;

public class Container<T extends Actor> extends WidgetGroup {
    private T actor;
    private int align;
    private Drawable background;
    private boolean clip;
    private float fillX;
    private float fillY;
    private Value maxHeight;
    private Value maxWidth;
    private Value minHeight;
    private Value minWidth;
    private Value padBottom;
    private Value padLeft;
    private Value padRight;
    private Value padTop;
    private Value prefHeight;
    private Value prefWidth;
    private boolean round;

    public Container() {
        this.minWidth = Value.minWidth;
        this.minHeight = Value.minHeight;
        this.prefWidth = Value.prefWidth;
        this.prefHeight = Value.prefHeight;
        this.maxWidth = Value.zero;
        this.maxHeight = Value.zero;
        this.padTop = Value.zero;
        this.padLeft = Value.zero;
        this.padBottom = Value.zero;
        this.padRight = Value.zero;
        this.round = true;
        setTouchable(Touchable.childrenOnly);
        setTransform(false);
    }

    public Container(T actor2) {
        this();
        setActor(actor2);
    }

    public void draw(Batch batch, float parentAlpha) {
        validate();
        if (isTransform()) {
            applyTransform(batch, computeTransform());
            drawBackground(batch, parentAlpha, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            if (this.clip) {
                batch.flush();
                float padLeft2 = this.padLeft.get(this);
                float padBottom2 = this.padBottom.get(this);
                if (clipBegin(padLeft2, padBottom2, (getWidth() - padLeft2) - this.padRight.get(this), (getHeight() - padBottom2) - this.padTop.get(this))) {
                    drawChildren(batch, parentAlpha);
                    batch.flush();
                    clipEnd();
                }
            } else {
                drawChildren(batch, parentAlpha);
            }
            resetTransform(batch);
            return;
        }
        drawBackground(batch, parentAlpha, getX(), getY());
        super.draw(batch, parentAlpha);
    }

    /* access modifiers changed from: protected */
    public void drawBackground(Batch batch, float parentAlpha, float x, float y) {
        if (this.background != null) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            this.background.draw(batch, x, y, getWidth(), getHeight());
        }
    }

    public void setBackground(Drawable background2) {
        setBackground(background2, true);
    }

    public void setBackground(Drawable background2, boolean adjustPadding) {
        if (this.background != background2) {
            this.background = background2;
            if (adjustPadding) {
                if (background2 == null) {
                    pad(Value.zero);
                } else {
                    pad(background2.getTopHeight(), background2.getLeftWidth(), background2.getBottomHeight(), background2.getRightWidth());
                }
                invalidate();
            }
        }
    }

    public Container<T> background(Drawable background2) {
        setBackground(background2);
        return this;
    }

    public Drawable getBackground() {
        return this.background;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void layout() {
        /*
            r17 = this;
            r0 = r17
            T r15 = r0.actor
            if (r15 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.padLeft
            r0 = r17
            float r9 = r15.get(r0)
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.padBottom
            r0 = r17
            float r8 = r15.get(r0)
            float r15 = r17.getWidth()
            float r15 = r15 - r9
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r0 = r0.padRight
            r16 = r0
            float r16 = r16.get(r17)
            float r2 = r15 - r16
            float r15 = r17.getHeight()
            float r15 = r15 - r8
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r0 = r0.padTop
            r16 = r0
            float r16 = r16.get(r17)
            float r1 = r15 - r16
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.minWidth
            r0 = r17
            T r0 = r0.actor
            r16 = r0
            float r7 = r15.get(r16)
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.minHeight
            r0 = r17
            T r0 = r0.actor
            r16 = r0
            float r6 = r15.get(r16)
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.prefWidth
            r0 = r17
            T r0 = r0.actor
            r16 = r0
            float r11 = r15.get(r16)
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.prefHeight
            r0 = r17
            T r0 = r0.actor
            r16 = r0
            float r10 = r15.get(r16)
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.maxWidth
            r0 = r17
            T r0 = r0.actor
            r16 = r0
            float r5 = r15.get(r16)
            r0 = r17
            com.badlogic.gdx.scenes.scene2d.ui.Value r15 = r0.maxHeight
            r0 = r17
            T r0 = r0.actor
            r16 = r0
            float r4 = r15.get(r16)
            r0 = r17
            float r15 = r0.fillX
            r16 = 0
            int r15 = (r15 > r16 ? 1 : (r15 == r16 ? 0 : -1))
            if (r15 <= 0) goto L_0x011b
            r0 = r17
            float r15 = r0.fillX
            float r12 = r2 * r15
        L_0x00a1:
            int r15 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r15 >= 0) goto L_0x00a6
            r12 = r7
        L_0x00a6:
            r15 = 0
            int r15 = (r5 > r15 ? 1 : (r5 == r15 ? 0 : -1))
            if (r15 <= 0) goto L_0x00b0
            int r15 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r15 <= 0) goto L_0x00b0
            r12 = r5
        L_0x00b0:
            r0 = r17
            float r15 = r0.fillY
            r16 = 0
            int r15 = (r15 > r16 ? 1 : (r15 == r16 ? 0 : -1))
            if (r15 <= 0) goto L_0x0120
            r0 = r17
            float r15 = r0.fillY
            float r3 = r1 * r15
        L_0x00c0:
            int r15 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r15 >= 0) goto L_0x00c5
            r3 = r6
        L_0x00c5:
            r15 = 0
            int r15 = (r4 > r15 ? 1 : (r4 == r15 ? 0 : -1))
            if (r15 <= 0) goto L_0x00cf
            int r15 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r15 <= 0) goto L_0x00cf
            r3 = r4
        L_0x00cf:
            r13 = r9
            r0 = r17
            int r15 = r0.align
            r15 = r15 & 16
            if (r15 == 0) goto L_0x0125
            float r15 = r2 - r12
            float r13 = r13 + r15
        L_0x00db:
            r14 = r8
            r0 = r17
            int r15 = r0.align
            r15 = r15 & 2
            if (r15 == 0) goto L_0x0135
            float r15 = r1 - r3
            float r14 = r14 + r15
        L_0x00e7:
            r0 = r17
            boolean r15 = r0.round
            if (r15 == 0) goto L_0x0101
            int r15 = java.lang.Math.round(r13)
            float r13 = (float) r15
            int r15 = java.lang.Math.round(r14)
            float r14 = (float) r15
            int r15 = java.lang.Math.round(r12)
            float r12 = (float) r15
            int r15 = java.lang.Math.round(r3)
            float r3 = (float) r15
        L_0x0101:
            r0 = r17
            T r15 = r0.actor
            r15.setBounds(r13, r14, r12, r3)
            r0 = r17
            T r15 = r0.actor
            boolean r15 = r15 instanceof com.badlogic.gdx.scenes.scene2d.utils.Layout
            if (r15 == 0) goto L_0x0006
            r0 = r17
            T r15 = r0.actor
            com.badlogic.gdx.scenes.scene2d.utils.Layout r15 = (com.badlogic.gdx.scenes.scene2d.utils.Layout) r15
            r15.validate()
            goto L_0x0006
        L_0x011b:
            float r12 = java.lang.Math.min(r11, r2)
            goto L_0x00a1
        L_0x0120:
            float r3 = java.lang.Math.min(r10, r1)
            goto L_0x00c0
        L_0x0125:
            r0 = r17
            int r15 = r0.align
            r15 = r15 & 8
            if (r15 != 0) goto L_0x00db
            float r15 = r2 - r12
            r16 = 1073741824(0x40000000, float:2.0)
            float r15 = r15 / r16
            float r13 = r13 + r15
            goto L_0x00db
        L_0x0135:
            r0 = r17
            int r15 = r0.align
            r15 = r15 & 4
            if (r15 != 0) goto L_0x00e7
            float r15 = r1 - r3
            r16 = 1073741824(0x40000000, float:2.0)
            float r15 = r15 / r16
            float r14 = r14 + r15
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.Container.layout():void");
    }

    public void setActor(T actor2) {
        if (actor2 == this) {
            throw new IllegalArgumentException("actor cannot be the Container.");
        } else if (actor2 != this.actor) {
            if (this.actor != null) {
                super.removeActor(this.actor);
            }
            this.actor = actor2;
            if (actor2 != null) {
                super.addActor(actor2);
            }
        }
    }

    public T getActor() {
        return this.actor;
    }

    public void addActor(Actor actor2) {
        throw new UnsupportedOperationException("Use Container#setActor.");
    }

    public void addActorAt(int index, Actor actor2) {
        throw new UnsupportedOperationException("Use Container#setActor.");
    }

    public void addActorBefore(Actor actorBefore, Actor actor2) {
        throw new UnsupportedOperationException("Use Container#setActor.");
    }

    public void addActorAfter(Actor actorAfter, Actor actor2) {
        throw new UnsupportedOperationException("Use Container#setActor.");
    }

    public boolean removeActor(Actor actor2) {
        if (actor2 != this.actor) {
            return false;
        }
        setActor(null);
        return true;
    }

    public Container<T> size(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.minWidth = size;
        this.minHeight = size;
        this.prefWidth = size;
        this.prefHeight = size;
        this.maxWidth = size;
        this.maxHeight = size;
        return this;
    }

    public Container<T> size(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.minWidth = width;
            this.minHeight = height;
            this.prefWidth = width;
            this.prefHeight = height;
            this.maxWidth = width;
            this.maxHeight = height;
            return this;
        }
    }

    public Container<T> size(float size) {
        size(new Value.Fixed(size));
        return this;
    }

    public Container<T> size(float width, float height) {
        size(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Container<T> width(Value width) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        }
        this.minWidth = width;
        this.prefWidth = width;
        this.maxWidth = width;
        return this;
    }

    public Container<T> width(float width) {
        width(new Value.Fixed(width));
        return this;
    }

    public Container<T> height(Value height) {
        if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        }
        this.minHeight = height;
        this.prefHeight = height;
        this.maxHeight = height;
        return this;
    }

    public Container<T> height(float height) {
        height(new Value.Fixed(height));
        return this;
    }

    public Container<T> minSize(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.minWidth = size;
        this.minHeight = size;
        return this;
    }

    public Container<T> minSize(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.minWidth = width;
            this.minHeight = height;
            return this;
        }
    }

    public Container<T> minWidth(Value minWidth2) {
        if (minWidth2 == null) {
            throw new IllegalArgumentException("minWidth cannot be null.");
        }
        this.minWidth = minWidth2;
        return this;
    }

    public Container<T> minHeight(Value minHeight2) {
        if (minHeight2 == null) {
            throw new IllegalArgumentException("minHeight cannot be null.");
        }
        this.minHeight = minHeight2;
        return this;
    }

    public Container<T> minSize(float size) {
        minSize(new Value.Fixed(size));
        return this;
    }

    public Container<T> minSize(float width, float height) {
        minSize(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Container<T> minWidth(float minWidth2) {
        this.minWidth = new Value.Fixed(minWidth2);
        return this;
    }

    public Container<T> minHeight(float minHeight2) {
        this.minHeight = new Value.Fixed(minHeight2);
        return this;
    }

    public Container<T> prefSize(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.prefWidth = size;
        this.prefHeight = size;
        return this;
    }

    public Container<T> prefSize(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.prefWidth = width;
            this.prefHeight = height;
            return this;
        }
    }

    public Container<T> prefWidth(Value prefWidth2) {
        if (prefWidth2 == null) {
            throw new IllegalArgumentException("prefWidth cannot be null.");
        }
        this.prefWidth = prefWidth2;
        return this;
    }

    public Container<T> prefHeight(Value prefHeight2) {
        if (prefHeight2 == null) {
            throw new IllegalArgumentException("prefHeight cannot be null.");
        }
        this.prefHeight = prefHeight2;
        return this;
    }

    public Container<T> prefSize(float width, float height) {
        prefSize(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Container<T> prefSize(float size) {
        prefSize(new Value.Fixed(size));
        return this;
    }

    public Container<T> prefWidth(float prefWidth2) {
        this.prefWidth = new Value.Fixed(prefWidth2);
        return this;
    }

    public Container<T> prefHeight(float prefHeight2) {
        this.prefHeight = new Value.Fixed(prefHeight2);
        return this;
    }

    public Container<T> maxSize(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.maxWidth = size;
        this.maxHeight = size;
        return this;
    }

    public Container<T> maxSize(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.maxWidth = width;
            this.maxHeight = height;
            return this;
        }
    }

    public Container<T> maxWidth(Value maxWidth2) {
        if (maxWidth2 == null) {
            throw new IllegalArgumentException("maxWidth cannot be null.");
        }
        this.maxWidth = maxWidth2;
        return this;
    }

    public Container<T> maxHeight(Value maxHeight2) {
        if (maxHeight2 == null) {
            throw new IllegalArgumentException("maxHeight cannot be null.");
        }
        this.maxHeight = maxHeight2;
        return this;
    }

    public Container<T> maxSize(float size) {
        maxSize(new Value.Fixed(size));
        return this;
    }

    public Container<T> maxSize(float width, float height) {
        maxSize(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Container<T> maxWidth(float maxWidth2) {
        this.maxWidth = new Value.Fixed(maxWidth2);
        return this;
    }

    public Container<T> maxHeight(float maxHeight2) {
        this.maxHeight = new Value.Fixed(maxHeight2);
        return this;
    }

    public Container<T> pad(Value pad) {
        if (pad == null) {
            throw new IllegalArgumentException("pad cannot be null.");
        }
        this.padTop = pad;
        this.padLeft = pad;
        this.padBottom = pad;
        this.padRight = pad;
        return this;
    }

    public Container<T> pad(Value top, Value left, Value bottom, Value right) {
        if (top == null) {
            throw new IllegalArgumentException("top cannot be null.");
        } else if (left == null) {
            throw new IllegalArgumentException("left cannot be null.");
        } else if (bottom == null) {
            throw new IllegalArgumentException("bottom cannot be null.");
        } else if (right == null) {
            throw new IllegalArgumentException("right cannot be null.");
        } else {
            this.padTop = top;
            this.padLeft = left;
            this.padBottom = bottom;
            this.padRight = right;
            return this;
        }
    }

    public Container<T> padTop(Value padTop2) {
        if (padTop2 == null) {
            throw new IllegalArgumentException("padTop cannot be null.");
        }
        this.padTop = padTop2;
        return this;
    }

    public Container<T> padLeft(Value padLeft2) {
        if (padLeft2 == null) {
            throw new IllegalArgumentException("padLeft cannot be null.");
        }
        this.padLeft = padLeft2;
        return this;
    }

    public Container<T> padBottom(Value padBottom2) {
        if (padBottom2 == null) {
            throw new IllegalArgumentException("padBottom cannot be null.");
        }
        this.padBottom = padBottom2;
        return this;
    }

    public Container<T> padRight(Value padRight2) {
        if (padRight2 == null) {
            throw new IllegalArgumentException("padRight cannot be null.");
        }
        this.padRight = padRight2;
        return this;
    }

    public Container<T> pad(float pad) {
        Value value = new Value.Fixed(pad);
        this.padTop = value;
        this.padLeft = value;
        this.padBottom = value;
        this.padRight = value;
        return this;
    }

    public Container<T> pad(float top, float left, float bottom, float right) {
        this.padTop = new Value.Fixed(top);
        this.padLeft = new Value.Fixed(left);
        this.padBottom = new Value.Fixed(bottom);
        this.padRight = new Value.Fixed(right);
        return this;
    }

    public Container<T> padTop(float padTop2) {
        this.padTop = new Value.Fixed(padTop2);
        return this;
    }

    public Container<T> padLeft(float padLeft2) {
        this.padLeft = new Value.Fixed(padLeft2);
        return this;
    }

    public Container<T> padBottom(float padBottom2) {
        this.padBottom = new Value.Fixed(padBottom2);
        return this;
    }

    public Container<T> padRight(float padRight2) {
        this.padRight = new Value.Fixed(padRight2);
        return this;
    }

    public Container<T> fill() {
        this.fillX = 1.0f;
        this.fillY = 1.0f;
        return this;
    }

    public Container<T> fillX() {
        this.fillX = 1.0f;
        return this;
    }

    public Container<T> fillY() {
        this.fillY = 1.0f;
        return this;
    }

    public Container<T> fill(float x, float y) {
        this.fillX = x;
        this.fillY = y;
        return this;
    }

    public Container<T> fill(boolean x, boolean y) {
        float f;
        float f2 = 1.0f;
        if (x) {
            f = 1.0f;
        } else {
            f = 0.0f;
        }
        this.fillX = f;
        if (!y) {
            f2 = 0.0f;
        }
        this.fillY = f2;
        return this;
    }

    public Container<T> fill(boolean fill) {
        float f;
        float f2 = 1.0f;
        if (fill) {
            f = 1.0f;
        } else {
            f = 0.0f;
        }
        this.fillX = f;
        if (!fill) {
            f2 = 0.0f;
        }
        this.fillY = f2;
        return this;
    }

    public Container<T> align(int align2) {
        this.align = align2;
        return this;
    }

    public Container<T> center() {
        this.align = 1;
        return this;
    }

    public Container<T> top() {
        this.align |= 2;
        this.align &= -5;
        return this;
    }

    public Container<T> left() {
        this.align |= 8;
        this.align &= -17;
        return this;
    }

    public Container<T> bottom() {
        this.align |= 4;
        this.align &= -3;
        return this;
    }

    public Container<T> right() {
        this.align |= 16;
        this.align &= -9;
        return this;
    }

    public float getMinWidth() {
        return this.minWidth.get(this.actor) + this.padLeft.get(this) + this.padRight.get(this);
    }

    public Value getMinHeightValue() {
        return this.minHeight;
    }

    public float getMinHeight() {
        return this.minHeight.get(this.actor) + this.padTop.get(this) + this.padBottom.get(this);
    }

    public Value getPrefWidthValue() {
        return this.prefWidth;
    }

    public float getPrefWidth() {
        float v = this.prefWidth.get(this.actor);
        if (this.background != null) {
            v = Math.max(v, this.background.getMinWidth());
        }
        return this.padLeft.get(this) + v + this.padRight.get(this);
    }

    public Value getPrefHeightValue() {
        return this.prefHeight;
    }

    public float getPrefHeight() {
        float v = this.prefHeight.get(this.actor);
        if (this.background != null) {
            v = Math.max(v, this.background.getMinHeight());
        }
        return this.padTop.get(this) + v + this.padBottom.get(this);
    }

    public Value getMaxWidthValue() {
        return this.maxWidth;
    }

    public float getMaxWidth() {
        float v = this.maxWidth.get(this.actor);
        if (v > Animation.CurveTimeline.LINEAR) {
            return v + this.padLeft.get(this) + this.padRight.get(this);
        }
        return v;
    }

    public Value getMaxHeightValue() {
        return this.maxHeight;
    }

    public float getMaxHeight() {
        float v = this.maxHeight.get(this.actor);
        if (v > Animation.CurveTimeline.LINEAR) {
            return v + this.padTop.get(this) + this.padBottom.get(this);
        }
        return v;
    }

    public Value getPadTopValue() {
        return this.padTop;
    }

    public float getPadTop() {
        return this.padTop.get(this);
    }

    public Value getPadLeftValue() {
        return this.padLeft;
    }

    public float getPadLeft() {
        return this.padLeft.get(this);
    }

    public Value getPadBottomValue() {
        return this.padBottom;
    }

    public float getPadBottom() {
        return this.padBottom.get(this);
    }

    public Value getPadRightValue() {
        return this.padRight;
    }

    public float getPadRight() {
        return this.padRight.get(this);
    }

    public float getPadX() {
        return this.padLeft.get(this) + this.padRight.get(this);
    }

    public float getPadY() {
        return this.padTop.get(this) + this.padBottom.get(this);
    }

    public float getFillX() {
        return this.fillX;
    }

    public float getFillY() {
        return this.fillY;
    }

    public int getAlign() {
        return this.align;
    }

    public void setRound(boolean round2) {
        this.round = round2;
    }

    public void setClip(boolean enabled) {
        this.clip = enabled;
        setTransform(enabled);
        invalidate();
    }

    public boolean getClip() {
        return this.clip;
    }

    public Actor hit(float x, float y, boolean touchable) {
        if (!this.clip || ((!touchable || getTouchable() != Touchable.disabled) && x >= Animation.CurveTimeline.LINEAR && x < getWidth() && y >= Animation.CurveTimeline.LINEAR && y < getHeight())) {
            return super.hit(x, y, touchable);
        }
        return null;
    }

    public void drawDebug(ShapeRenderer shapes) {
        validate();
        if (isTransform()) {
            applyTransform(shapes, computeTransform());
            if (this.clip) {
                shapes.flush();
                float padLeft2 = this.padLeft.get(this);
                float padBottom2 = this.padBottom.get(this);
                if (this.background == null ? clipBegin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, getWidth(), getHeight()) : clipBegin(padLeft2, padBottom2, (getWidth() - padLeft2) - this.padRight.get(this), (getHeight() - padBottom2) - this.padTop.get(this))) {
                    drawDebugChildren(shapes);
                    clipEnd();
                }
            } else {
                drawDebugChildren(shapes);
            }
            resetTransform(shapes);
            return;
        }
        super.drawDebug(shapes);
    }
}
