package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.StringBuilder;
import com.kbz.esotericsoftware.spine.Animation;

public class Label extends Widget {
    private static final Color tempColor = new Color();
    private BitmapFontCache cache;
    private boolean ellipsis;
    private float fontScaleX;
    private float fontScaleY;
    private int labelAlign;
    private float lastPrefHeight;
    private final GlyphLayout layout;
    private int lineAlign;
    private final Vector2 prefSize;
    private boolean prefSizeInvalid;
    private LabelStyle style;
    private final StringBuilder text;
    private boolean wrap;

    public Label(CharSequence text2, Skin skin) {
        this(text2, (LabelStyle) skin.get(LabelStyle.class));
    }

    public Label(CharSequence text2, Skin skin, String styleName) {
        this(text2, (LabelStyle) skin.get(styleName, LabelStyle.class));
    }

    public Label(CharSequence text2, Skin skin, String fontName, Color color) {
        this(text2, new LabelStyle(skin.getFont(fontName), color));
    }

    public Label(CharSequence text2, Skin skin, String fontName, String colorName) {
        this(text2, new LabelStyle(skin.getFont(fontName), skin.getColor(colorName)));
    }

    public Label(CharSequence text2, LabelStyle style2) {
        this.layout = new GlyphLayout();
        this.prefSize = new Vector2();
        this.text = new StringBuilder();
        this.labelAlign = 8;
        this.lineAlign = 8;
        this.prefSizeInvalid = true;
        this.fontScaleX = 1.0f;
        this.fontScaleY = 1.0f;
        if (text2 != null) {
            this.text.append(text2);
        }
        setStyle(style2);
        if (text2 != null && text2.length() > 0) {
            setSize(getPrefWidth(), getPrefHeight());
        }
    }

    public void setStyle(LabelStyle style2) {
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        } else if (style2.font == null) {
            throw new IllegalArgumentException("Missing LabelStyle font.");
        } else {
            this.style = style2;
            this.cache = style2.font.newFontCache();
            invalidateHierarchy();
        }
    }

    public LabelStyle getStyle() {
        return this.style;
    }

    public void setText(CharSequence newText) {
        if (newText == null) {
            newText = "";
        }
        if (newText instanceof StringBuilder) {
            if (!this.text.equals(newText)) {
                this.text.setLength(0);
                this.text.append((StringBuilder) newText);
            } else {
                return;
            }
        } else if (!textEquals(newText)) {
            this.text.setLength(0);
            this.text.append(newText);
        } else {
            return;
        }
        invalidateHierarchy();
    }

    public boolean textEquals(CharSequence other) {
        int length = this.text.length;
        char[] chars = this.text.chars;
        if (length != other.length()) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (chars[i] != other.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public StringBuilder getText() {
        return this.text;
    }

    public void invalidate() {
        super.invalidate();
        this.prefSizeInvalid = true;
    }

    private void scaleAndComputePrefSize() {
        BitmapFont font = this.cache.getFont();
        float oldScaleX = font.getScaleX();
        float oldScaleY = font.getScaleY();
        if (!(this.fontScaleX == 1.0f && this.fontScaleY == 1.0f)) {
            font.getData().setScale(this.fontScaleX, this.fontScaleY);
        }
        computePrefSize();
        if (this.fontScaleX != 1.0f || this.fontScaleY != 1.0f) {
            font.getData().setScale(oldScaleX, oldScaleY);
        }
    }

    private void computePrefSize() {
        this.prefSizeInvalid = false;
        if (!this.wrap || this.ellipsis) {
            this.layout.setText(this.cache.getFont(), this.text);
        } else {
            float width = getWidth();
            if (this.style.background != null) {
                width -= this.style.background.getLeftWidth() + this.style.background.getRightWidth();
            }
            this.layout.setText(this.cache.getFont(), this.text, Color.WHITE, width, 8, true);
        }
        this.prefSize.set(this.layout.width, this.layout.height);
    }

    public void layout() {
        float textHeight;
        float textWidth;
        float y;
        BitmapFont font = this.cache.getFont();
        float oldScaleX = font.getScaleX();
        float oldScaleY = font.getScaleY();
        if (!(this.fontScaleX == 1.0f && this.fontScaleY == 1.0f)) {
            font.getData().setScale(this.fontScaleX, this.fontScaleY);
        }
        boolean wrap2 = this.wrap && !this.ellipsis;
        if (wrap2) {
            float prefHeight = getPrefHeight();
            if (prefHeight != this.lastPrefHeight) {
                this.lastPrefHeight = prefHeight;
                invalidateHierarchy();
            }
        }
        float width = getWidth();
        float height = getHeight();
        Drawable background = this.style.background;
        float x = Animation.CurveTimeline.LINEAR;
        float y2 = Animation.CurveTimeline.LINEAR;
        if (background != null) {
            x = background.getLeftWidth();
            y2 = background.getBottomHeight();
            width -= background.getLeftWidth() + background.getRightWidth();
            height -= background.getBottomHeight() + background.getTopHeight();
        }
        GlyphLayout layout2 = this.layout;
        if (wrap2 || this.text.indexOf("\n") != -1) {
            layout2.setText(font, this.text, 0, this.text.length, Color.WHITE, width, this.lineAlign, wrap2, this.ellipsis ? "..." : null);
            textWidth = layout2.width;
            textHeight = layout2.height;
            if ((this.labelAlign & 8) == 0) {
                if ((this.labelAlign & 16) != 0) {
                    x += width - textWidth;
                } else {
                    x += (width - textWidth) / 2.0f;
                }
            }
        } else {
            textWidth = width;
            textHeight = font.getData().capHeight;
        }
        if ((this.labelAlign & 2) != 0) {
            y = y2 + (this.cache.getFont().isFlipped() ? Animation.CurveTimeline.LINEAR : height - textHeight) + this.style.font.getDescent();
        } else if ((this.labelAlign & 4) != 0) {
            y = (y2 + (this.cache.getFont().isFlipped() ? height - textHeight : Animation.CurveTimeline.LINEAR)) - this.style.font.getDescent();
        } else {
            y = y2 + ((height - textHeight) / 2.0f);
        }
        if (!this.cache.getFont().isFlipped()) {
            y += textHeight;
        }
        layout2.setText(font, this.text, 0, this.text.length, Color.WHITE, textWidth, this.lineAlign, wrap2, this.ellipsis ? "..." : null);
        this.cache.setText(layout2, x, y);
        if (this.fontScaleX != 1.0f || this.fontScaleY != 1.0f) {
            font.getData().setScale(oldScaleX, oldScaleY);
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        validate();
        Color color = tempColor.set(getColor());
        color.a *= parentAlpha;
        if (this.style.background != null) {
            batch.setColor(color.r, color.g, color.b, color.a);
            this.style.background.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
        if (this.style.fontColor != null) {
            color.mul(this.style.fontColor);
        }
        this.cache.tint(color);
        this.cache.setPosition(getX(), getY());
        this.cache.draw(batch);
    }

    public float getPrefWidth() {
        if (this.wrap) {
            return Animation.CurveTimeline.LINEAR;
        }
        if (this.prefSizeInvalid) {
            scaleAndComputePrefSize();
        }
        float width = this.prefSize.x;
        Drawable background = this.style.background;
        if (background != null) {
            return width + background.getLeftWidth() + background.getRightWidth();
        }
        return width;
    }

    public float getPrefHeight() {
        if (this.prefSizeInvalid) {
            scaleAndComputePrefSize();
        }
        float height = this.prefSize.y - ((this.style.font.getDescent() * this.fontScaleY) * 2.0f);
        Drawable background = this.style.background;
        if (background != null) {
            return height + background.getTopHeight() + background.getBottomHeight();
        }
        return height;
    }

    public GlyphLayout getGlyphLayout() {
        return this.layout;
    }

    public void setWrap(boolean wrap2) {
        this.wrap = wrap2;
        invalidateHierarchy();
    }

    public void setAlignment(int alignment) {
        setAlignment(alignment, alignment);
    }

    public void setAlignment(int labelAlign2, int lineAlign2) {
        this.labelAlign = labelAlign2;
        if ((lineAlign2 & 8) != 0) {
            this.lineAlign = 8;
        } else if ((lineAlign2 & 16) != 0) {
            this.lineAlign = 16;
        } else {
            this.lineAlign = 1;
        }
        invalidate();
    }

    public void setFontScale(float fontScale) {
        this.fontScaleX = fontScale;
        this.fontScaleY = fontScale;
        invalidateHierarchy();
    }

    public void setFontScale(float fontScaleX2, float fontScaleY2) {
        this.fontScaleX = fontScaleX2;
        this.fontScaleY = fontScaleY2;
        invalidateHierarchy();
    }

    public float getFontScaleX() {
        return this.fontScaleX;
    }

    public void setFontScaleX(float fontScaleX2) {
        this.fontScaleX = fontScaleX2;
        invalidateHierarchy();
    }

    public float getFontScaleY() {
        return this.fontScaleY;
    }

    public void setFontScaleY(float fontScaleY2) {
        this.fontScaleY = fontScaleY2;
        invalidateHierarchy();
    }

    public void setEllipsis(boolean ellipsis2) {
        this.ellipsis = ellipsis2;
    }

    /* access modifiers changed from: protected */
    public BitmapFontCache getBitmapFontCache() {
        return this.cache;
    }

    public String toString() {
        return String.valueOf(super.toString()) + ": " + ((Object) this.text);
    }

    public static class LabelStyle {
        public Drawable background;
        public BitmapFont font;
        public Color fontColor;

        public LabelStyle() {
        }

        public LabelStyle(BitmapFont font2, Color fontColor2) {
            this.font = font2;
            this.fontColor = fontColor2;
        }

        public LabelStyle(LabelStyle style) {
            this.font = style.font;
            if (style.fontColor != null) {
                this.fontColor = new Color(style.fontColor);
            }
            this.background = style.background;
        }
    }
}
