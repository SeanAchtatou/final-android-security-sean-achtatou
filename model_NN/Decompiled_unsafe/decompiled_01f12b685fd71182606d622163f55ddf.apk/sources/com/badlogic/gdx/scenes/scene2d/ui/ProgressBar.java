package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;

public class ProgressBar extends Widget implements Disableable {
    private float animateDuration;
    private float animateFromValue;
    private Interpolation animateInterpolation;
    private float animateTime;
    boolean disabled;
    private float max;
    private float min;
    float position;
    boolean shiftIgnoresSnap;
    private float[] snapValues;
    private float stepSize;
    private ProgressBarStyle style;
    private float threshold;
    private float value;
    final boolean vertical;
    private Interpolation visualInterpolation;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ProgressBar(float min2, float max2, float stepSize2, boolean vertical2, Skin skin) {
        this(min2, max2, stepSize2, vertical2, (ProgressBarStyle) skin.get("default-" + (vertical2 ? "vertical" : "horizontal"), ProgressBarStyle.class));
    }

    public ProgressBar(float min2, float max2, float stepSize2, boolean vertical2, Skin skin, String styleName) {
        this(min2, max2, stepSize2, vertical2, (ProgressBarStyle) skin.get(styleName, ProgressBarStyle.class));
    }

    public ProgressBar(float min2, float max2, float stepSize2, boolean vertical2, ProgressBarStyle style2) {
        this.animateInterpolation = Interpolation.linear;
        this.visualInterpolation = Interpolation.linear;
        if (min2 > max2) {
            throw new IllegalArgumentException("max must be > min. min,max: " + min2 + ", " + max2);
        } else if (stepSize2 <= Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("stepSize must be > 0: " + stepSize2);
        } else {
            setStyle(style2);
            this.min = min2;
            this.max = max2;
            this.stepSize = stepSize2;
            this.vertical = vertical2;
            this.value = min2;
            setSize(getPrefWidth(), getPrefHeight());
        }
    }

    public void setStyle(ProgressBarStyle style2) {
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        this.style = style2;
        invalidateHierarchy();
    }

    public ProgressBarStyle getStyle() {
        return this.style;
    }

    public void act(float delta) {
        super.act(delta);
        if (this.animateTime > Animation.CurveTimeline.LINEAR) {
            this.animateTime -= delta;
            Stage stage = getStage();
            if (stage != null && stage.getActionsRequestRendering()) {
                Gdx.graphics.requestRendering();
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        ProgressBarStyle style2 = this.style;
        boolean disabled2 = this.disabled;
        Drawable knob = (!disabled2 || style2.disabledKnob == null) ? style2.knob : style2.disabledKnob;
        Drawable bg = (!disabled2 || style2.disabledBackground == null) ? style2.background : style2.disabledBackground;
        Drawable knobBefore = (!disabled2 || style2.disabledKnobBefore == null) ? style2.knobBefore : style2.disabledKnobBefore;
        Drawable knobAfter = (!disabled2 || style2.disabledKnobAfter == null) ? style2.knobAfter : style2.disabledKnobAfter;
        Color color = getColor();
        float x = getX();
        float y = getY();
        float width = getWidth();
        float height = getHeight();
        float knobHeight = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinHeight();
        float knobWidth = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinWidth();
        float percent = getVisualPercent();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        if (this.vertical) {
            bg.draw(batch, x + ((float) ((int) ((width - bg.getMinWidth()) * 0.5f))), y, bg.getMinWidth(), height);
            float positionHeight = height - (bg.getTopHeight() + bg.getBottomHeight());
            float knobHeightHalf = Animation.CurveTimeline.LINEAR;
            if (this.min != this.max) {
                if (knob == null) {
                    knobHeightHalf = knobBefore == null ? Animation.CurveTimeline.LINEAR : knobBefore.getMinHeight() * 0.5f;
                    this.position = (positionHeight - knobHeightHalf) * percent;
                    this.position = Math.min(positionHeight - knobHeightHalf, this.position);
                } else {
                    knobHeightHalf = knobHeight * 0.5f;
                    this.position = (positionHeight - knobHeight) * percent;
                    this.position = Math.min(positionHeight - knobHeight, this.position) + bg.getBottomHeight();
                }
                this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
            }
            if (knobBefore != null) {
                float offset = Animation.CurveTimeline.LINEAR;
                if (bg != null) {
                    offset = bg.getTopHeight();
                }
                knobBefore.draw(batch, x + ((float) ((int) ((width - knobBefore.getMinWidth()) * 0.5f))), y + offset, knobBefore.getMinWidth(), (float) ((int) (this.position + knobHeightHalf)));
            }
            if (knobAfter != null) {
                knobAfter.draw(batch, x + ((float) ((int) ((width - knobAfter.getMinWidth()) * 0.5f))), y + ((float) ((int) (this.position + knobHeightHalf))), knobAfter.getMinWidth(), height - ((float) ((int) (this.position + knobHeightHalf))));
            }
            if (knob != null) {
                knob.draw(batch, x + ((float) ((int) ((width - knobWidth) * 0.5f))), (float) ((int) (this.position + y)), knobWidth, knobHeight);
                return;
            }
            return;
        }
        bg.draw(batch, x, y + ((float) ((int) ((height - bg.getMinHeight()) * 0.5f))), width, bg.getMinHeight());
        float positionWidth = width - (bg.getLeftWidth() + bg.getRightWidth());
        float knobWidthHalf = Animation.CurveTimeline.LINEAR;
        if (this.min != this.max) {
            if (knob == null) {
                knobWidthHalf = knobBefore == null ? Animation.CurveTimeline.LINEAR : knobBefore.getMinWidth() * 0.5f;
                this.position = (positionWidth - knobWidthHalf) * percent;
                this.position = Math.min(positionWidth - knobWidthHalf, this.position);
            } else {
                knobWidthHalf = knobWidth * 0.5f;
                this.position = (positionWidth - knobWidth) * percent;
                this.position = Math.min(positionWidth - knobWidth, this.position) + bg.getLeftWidth();
            }
            this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
        }
        if (knobBefore != null) {
            float offset2 = Animation.CurveTimeline.LINEAR;
            if (bg != null) {
                offset2 = bg.getLeftWidth();
            }
            knobBefore.draw(batch, x + offset2, y + ((float) ((int) ((height - knobBefore.getMinHeight()) * 0.5f))), (float) ((int) (this.position + knobWidthHalf)), knobBefore.getMinHeight());
        }
        if (knobAfter != null) {
            knobAfter.draw(batch, x + ((float) ((int) (this.position + knobWidthHalf))), y + ((float) ((int) ((height - knobAfter.getMinHeight()) * 0.5f))), width - ((float) ((int) (this.position + knobWidthHalf))), knobAfter.getMinHeight());
        }
        if (knob != null) {
            knob.draw(batch, (float) ((int) (this.position + x)), (float) ((int) (((height - knobHeight) * 0.5f) + y)), knobWidth, knobHeight);
        }
    }

    public float getValue() {
        return this.value;
    }

    public float getVisualValue() {
        if (this.animateTime > Animation.CurveTimeline.LINEAR) {
            return this.animateInterpolation.apply(this.animateFromValue, this.value, 1.0f - (this.animateTime / this.animateDuration));
        }
        return this.value;
    }

    public float getPercent() {
        return (this.value - this.min) / (this.max - this.min);
    }

    public float getVisualPercent() {
        return this.visualInterpolation.apply((getVisualValue() - this.min) / (this.max - this.min));
    }

    /* access modifiers changed from: protected */
    public float getKnobPosition() {
        return this.position;
    }

    public boolean setValue(float value2) {
        float value3 = clamp(((float) Math.round(value2 / this.stepSize)) * this.stepSize);
        if (!this.shiftIgnoresSnap || (!Gdx.input.isKeyPressed(59) && !Gdx.input.isKeyPressed(60))) {
            value3 = snap(value3);
        }
        float oldValue = this.value;
        if (value3 == oldValue) {
            return false;
        }
        float oldVisualValue = getVisualValue();
        this.value = value3;
        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
        boolean cancelled = fire(changeEvent);
        if (cancelled) {
            this.value = oldValue;
        } else if (this.animateDuration > Animation.CurveTimeline.LINEAR) {
            this.animateFromValue = oldVisualValue;
            this.animateTime = this.animateDuration;
        }
        Pools.free(changeEvent);
        if (!cancelled) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public float clamp(float value2) {
        return MathUtils.clamp(value2, this.min, this.max);
    }

    public void setRange(float min2, float max2) {
        if (min2 > max2) {
            throw new IllegalArgumentException("min must be <= max");
        }
        this.min = min2;
        this.max = max2;
        if (this.value < min2) {
            setValue(min2);
        } else if (this.value > max2) {
            setValue(max2);
        }
    }

    public void setStepSize(float stepSize2) {
        if (stepSize2 <= Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("steps must be > 0: " + stepSize2);
        }
        this.stepSize = stepSize2;
    }

    public float getPrefWidth() {
        if (!this.vertical) {
            return 140.0f;
        }
        Drawable knob = (!this.disabled || this.style.disabledKnob == null) ? this.style.knob : this.style.disabledKnob;
        return Math.max(knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinWidth(), ((!this.disabled || this.style.disabledBackground == null) ? this.style.background : this.style.disabledBackground).getMinWidth());
    }

    public float getPrefHeight() {
        if (this.vertical) {
            return 140.0f;
        }
        Drawable knob = (!this.disabled || this.style.disabledKnob == null) ? this.style.knob : this.style.disabledKnob;
        return Math.max(knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinHeight(), ((!this.disabled || this.style.disabledBackground == null) ? this.style.background : this.style.disabledBackground).getMinHeight());
    }

    public float getMinValue() {
        return this.min;
    }

    public float getMaxValue() {
        return this.max;
    }

    public float getStepSize() {
        return this.stepSize;
    }

    public void setAnimateDuration(float duration) {
        this.animateDuration = duration;
    }

    public void setAnimateInterpolation(Interpolation animateInterpolation2) {
        if (animateInterpolation2 == null) {
            throw new IllegalArgumentException("animateInterpolation cannot be null.");
        }
        this.animateInterpolation = animateInterpolation2;
    }

    public void setVisualInterpolation(Interpolation interpolation) {
        this.visualInterpolation = interpolation;
    }

    public void setSnapToValues(float[] values, float threshold2) {
        this.snapValues = values;
        this.threshold = threshold2;
    }

    private float snap(float value2) {
        if (this.snapValues == null) {
            return value2;
        }
        for (int i = 0; i < this.snapValues.length; i++) {
            if (Math.abs(value2 - this.snapValues[i]) <= this.threshold) {
                return this.snapValues[i];
            }
        }
        return value2;
    }

    public void setDisabled(boolean disabled2) {
        this.disabled = disabled2;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public static class ProgressBarStyle {
        public Drawable background;
        public Drawable disabledBackground;
        public Drawable disabledKnob;
        public Drawable disabledKnobAfter;
        public Drawable disabledKnobBefore;
        public Drawable knob;
        public Drawable knobAfter;
        public Drawable knobBefore;

        public ProgressBarStyle() {
        }

        public ProgressBarStyle(Drawable background2, Drawable knob2) {
            this.background = background2;
            this.knob = knob2;
        }

        public ProgressBarStyle(ProgressBarStyle style) {
            this.background = style.background;
            this.disabledBackground = style.disabledBackground;
            this.knob = style.knob;
            this.disabledKnob = style.disabledKnob;
            this.knobBefore = style.knobBefore;
            this.knobAfter = style.knobAfter;
            this.disabledKnobBefore = style.disabledKnobBefore;
            this.disabledKnobAfter = style.disabledKnobAfter;
        }
    }
}
