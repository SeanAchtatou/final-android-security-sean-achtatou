package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.Cullable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.Layout;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.kbz.esotericsoftware.spine.Animation;

public class ScrollPane extends WidgetGroup {
    float amountX;
    float amountY;
    float areaHeight;
    float areaWidth;
    boolean cancelTouchFocus;
    private boolean clamp;
    private boolean disableX;
    private boolean disableY;
    int draggingPointer;
    float fadeAlpha;
    float fadeAlphaSeconds;
    float fadeDelay;
    float fadeDelaySeconds;
    private boolean fadeScrollBars;
    boolean flickScroll;
    private ActorGestureListener flickScrollListener;
    float flingTime;
    float flingTimer;
    private boolean forceScrollX;
    private boolean forceScrollY;
    final Rectangle hKnobBounds;
    final Rectangle hScrollBounds;
    boolean hScrollOnBottom;
    final Vector2 lastPoint;
    float maxX;
    float maxY;
    private float overscrollDistance;
    private float overscrollSpeedMax;
    private float overscrollSpeedMin;
    private boolean overscrollX;
    private boolean overscrollY;
    private final Rectangle scissorBounds;
    boolean scrollX;
    boolean scrollY;
    private boolean scrollbarsOnTop;
    private boolean smoothScrolling;
    private ScrollPaneStyle style;
    boolean touchScrollH;
    boolean touchScrollV;
    final Rectangle vKnobBounds;
    final Rectangle vScrollBounds;
    boolean vScrollOnRight;
    private boolean variableSizeKnobs;
    float velocityX;
    float velocityY;
    float visualAmountX;
    float visualAmountY;
    private Actor widget;
    private final Rectangle widgetAreaBounds;
    private final Rectangle widgetCullingArea;

    public ScrollPane(Actor widget2) {
        this(widget2, new ScrollPaneStyle());
    }

    public ScrollPane(Actor widget2, Skin skin) {
        this(widget2, (ScrollPaneStyle) skin.get(ScrollPaneStyle.class));
    }

    public ScrollPane(Actor widget2, Skin skin, String styleName) {
        this(widget2, (ScrollPaneStyle) skin.get(styleName, ScrollPaneStyle.class));
    }

    public ScrollPane(Actor widget2, ScrollPaneStyle style2) {
        this.hScrollBounds = new Rectangle();
        this.vScrollBounds = new Rectangle();
        this.hKnobBounds = new Rectangle();
        this.vKnobBounds = new Rectangle();
        this.widgetAreaBounds = new Rectangle();
        this.widgetCullingArea = new Rectangle();
        this.scissorBounds = new Rectangle();
        this.vScrollOnRight = true;
        this.hScrollOnBottom = true;
        this.lastPoint = new Vector2();
        this.fadeScrollBars = true;
        this.smoothScrolling = true;
        this.fadeAlphaSeconds = 1.0f;
        this.fadeDelaySeconds = 1.0f;
        this.cancelTouchFocus = true;
        this.flickScroll = true;
        this.overscrollX = true;
        this.overscrollY = true;
        this.flingTime = 1.0f;
        this.overscrollDistance = 50.0f;
        this.overscrollSpeedMin = 30.0f;
        this.overscrollSpeedMax = 200.0f;
        this.clamp = true;
        this.variableSizeKnobs = true;
        this.draggingPointer = -1;
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        this.style = style2;
        setWidget(widget2);
        setSize(150.0f, 150.0f);
        addCaptureListener(new InputListener() {
            private float handlePosition;

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                int i = -1;
                if (ScrollPane.this.draggingPointer != -1) {
                    return false;
                }
                if (pointer == 0 && button != 0) {
                    return false;
                }
                ScrollPane.this.getStage().setScrollFocus(ScrollPane.this);
                if (!ScrollPane.this.flickScroll) {
                    ScrollPane.this.resetFade();
                }
                if (ScrollPane.this.fadeAlpha == Animation.CurveTimeline.LINEAR) {
                    return false;
                }
                if (ScrollPane.this.scrollX && ScrollPane.this.hScrollBounds.contains(x, y)) {
                    event.stop();
                    ScrollPane.this.resetFade();
                    if (ScrollPane.this.hKnobBounds.contains(x, y)) {
                        ScrollPane.this.lastPoint.set(x, y);
                        this.handlePosition = ScrollPane.this.hKnobBounds.x;
                        ScrollPane.this.touchScrollH = true;
                        ScrollPane.this.draggingPointer = pointer;
                        return true;
                    }
                    ScrollPane scrollPane = ScrollPane.this;
                    float f = ScrollPane.this.amountX;
                    float f2 = ScrollPane.this.areaWidth;
                    if (x >= ScrollPane.this.hKnobBounds.x) {
                        i = 1;
                    }
                    scrollPane.setScrollX((((float) i) * f2) + f);
                    return true;
                } else if (!ScrollPane.this.scrollY || !ScrollPane.this.vScrollBounds.contains(x, y)) {
                    return false;
                } else {
                    event.stop();
                    ScrollPane.this.resetFade();
                    if (ScrollPane.this.vKnobBounds.contains(x, y)) {
                        ScrollPane.this.lastPoint.set(x, y);
                        this.handlePosition = ScrollPane.this.vKnobBounds.y;
                        ScrollPane.this.touchScrollV = true;
                        ScrollPane.this.draggingPointer = pointer;
                        return true;
                    }
                    ScrollPane scrollPane2 = ScrollPane.this;
                    float f3 = ScrollPane.this.amountY;
                    float f4 = ScrollPane.this.areaHeight;
                    if (y < ScrollPane.this.vKnobBounds.y) {
                        i = 1;
                    }
                    scrollPane2.setScrollY((((float) i) * f4) + f3);
                    return true;
                }
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (pointer == ScrollPane.this.draggingPointer) {
                    ScrollPane.this.cancel();
                }
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (pointer == ScrollPane.this.draggingPointer) {
                    if (ScrollPane.this.touchScrollH) {
                        float scrollH = this.handlePosition + (x - ScrollPane.this.lastPoint.x);
                        this.handlePosition = scrollH;
                        float scrollH2 = Math.min((ScrollPane.this.hScrollBounds.x + ScrollPane.this.hScrollBounds.width) - ScrollPane.this.hKnobBounds.width, Math.max(ScrollPane.this.hScrollBounds.x, scrollH));
                        float total = ScrollPane.this.hScrollBounds.width - ScrollPane.this.hKnobBounds.width;
                        if (total != Animation.CurveTimeline.LINEAR) {
                            ScrollPane.this.setScrollPercentX((scrollH2 - ScrollPane.this.hScrollBounds.x) / total);
                        }
                        ScrollPane.this.lastPoint.set(x, y);
                    } else if (ScrollPane.this.touchScrollV) {
                        float scrollV = this.handlePosition + (y - ScrollPane.this.lastPoint.y);
                        this.handlePosition = scrollV;
                        float scrollV2 = Math.min((ScrollPane.this.vScrollBounds.y + ScrollPane.this.vScrollBounds.height) - ScrollPane.this.vKnobBounds.height, Math.max(ScrollPane.this.vScrollBounds.y, scrollV));
                        float total2 = ScrollPane.this.vScrollBounds.height - ScrollPane.this.vKnobBounds.height;
                        if (total2 != Animation.CurveTimeline.LINEAR) {
                            ScrollPane.this.setScrollPercentY(1.0f - ((scrollV2 - ScrollPane.this.vScrollBounds.y) / total2));
                        }
                        ScrollPane.this.lastPoint.set(x, y);
                    }
                }
            }

            public boolean mouseMoved(InputEvent event, float x, float y) {
                if (ScrollPane.this.flickScroll) {
                    return false;
                }
                ScrollPane.this.resetFade();
                return false;
            }
        });
        this.flickScrollListener = new ActorGestureListener() {
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                ScrollPane.this.resetFade();
                ScrollPane.this.amountX -= deltaX;
                ScrollPane.this.amountY += deltaY;
                ScrollPane.this.clamp();
                ScrollPane.this.cancelTouchFocusedChild(event);
            }

            public void fling(InputEvent event, float x, float y, int button) {
                if (Math.abs(x) > 150.0f) {
                    ScrollPane.this.flingTimer = ScrollPane.this.flingTime;
                    ScrollPane.this.velocityX = x;
                    ScrollPane.this.cancelTouchFocusedChild(event);
                }
                if (Math.abs(y) > 150.0f) {
                    ScrollPane.this.flingTimer = ScrollPane.this.flingTime;
                    ScrollPane.this.velocityY = -y;
                    ScrollPane.this.cancelTouchFocusedChild(event);
                }
            }

            public boolean handle(Event event) {
                if (!super.handle(event)) {
                    return false;
                }
                if (((InputEvent) event).getType() == InputEvent.Type.touchDown) {
                    ScrollPane.this.flingTimer = Animation.CurveTimeline.LINEAR;
                }
                return true;
            }
        };
        addListener(this.flickScrollListener);
        addListener(new InputListener() {
            public boolean scrolled(InputEvent event, float x, float y, int amount) {
                ScrollPane.this.resetFade();
                if (ScrollPane.this.scrollY) {
                    ScrollPane.this.setScrollY(ScrollPane.this.amountY + (ScrollPane.this.getMouseWheelY() * ((float) amount)));
                } else if (!ScrollPane.this.scrollX) {
                    return false;
                } else {
                    ScrollPane.this.setScrollX(ScrollPane.this.amountX + (ScrollPane.this.getMouseWheelX() * ((float) amount)));
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void resetFade() {
        this.fadeAlpha = this.fadeAlphaSeconds;
        this.fadeDelay = this.fadeDelaySeconds;
    }

    /* access modifiers changed from: package-private */
    public void cancelTouchFocusedChild(InputEvent event) {
        Stage stage;
        if (this.cancelTouchFocus && (stage = getStage()) != null) {
            stage.cancelTouchFocusExcept(this.flickScrollListener, this);
        }
    }

    public void cancel() {
        this.draggingPointer = -1;
        this.touchScrollH = false;
        this.touchScrollV = false;
        this.flickScrollListener.getGestureDetector().cancel();
    }

    /* access modifiers changed from: package-private */
    public void clamp() {
        if (this.clamp) {
            scrollX(this.overscrollX ? MathUtils.clamp(this.amountX, -this.overscrollDistance, this.maxX + this.overscrollDistance) : MathUtils.clamp(this.amountX, (float) Animation.CurveTimeline.LINEAR, this.maxX));
            scrollY(this.overscrollY ? MathUtils.clamp(this.amountY, -this.overscrollDistance, this.maxY + this.overscrollDistance) : MathUtils.clamp(this.amountY, (float) Animation.CurveTimeline.LINEAR, this.maxY));
        }
    }

    public void setStyle(ScrollPaneStyle style2) {
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        this.style = style2;
        invalidateHierarchy();
    }

    public ScrollPaneStyle getStyle() {
        return this.style;
    }

    public void act(float delta) {
        Stage stage;
        super.act(delta);
        boolean panning = this.flickScrollListener.getGestureDetector().isPanning();
        boolean animating = false;
        if (this.fadeAlpha > Animation.CurveTimeline.LINEAR && this.fadeScrollBars && !panning && !this.touchScrollH && !this.touchScrollV) {
            this.fadeDelay -= delta;
            if (this.fadeDelay <= Animation.CurveTimeline.LINEAR) {
                this.fadeAlpha = Math.max((float) Animation.CurveTimeline.LINEAR, this.fadeAlpha - delta);
            }
            animating = true;
        }
        if (this.flingTimer > Animation.CurveTimeline.LINEAR) {
            resetFade();
            float alpha = this.flingTimer / this.flingTime;
            this.amountX -= (this.velocityX * alpha) * delta;
            this.amountY -= (this.velocityY * alpha) * delta;
            clamp();
            if (this.amountX == (-this.overscrollDistance)) {
                this.velocityX = Animation.CurveTimeline.LINEAR;
            }
            if (this.amountX >= this.maxX + this.overscrollDistance) {
                this.velocityX = Animation.CurveTimeline.LINEAR;
            }
            if (this.amountY == (-this.overscrollDistance)) {
                this.velocityY = Animation.CurveTimeline.LINEAR;
            }
            if (this.amountY >= this.maxY + this.overscrollDistance) {
                this.velocityY = Animation.CurveTimeline.LINEAR;
            }
            this.flingTimer -= delta;
            if (this.flingTimer <= Animation.CurveTimeline.LINEAR) {
                this.velocityX = Animation.CurveTimeline.LINEAR;
                this.velocityY = Animation.CurveTimeline.LINEAR;
            }
            animating = true;
        }
        if (!this.smoothScrolling || this.flingTimer > Animation.CurveTimeline.LINEAR || this.touchScrollH || this.touchScrollV || panning) {
            if (this.visualAmountX != this.amountX) {
                visualScrollX(this.amountX);
            }
            if (this.visualAmountY != this.amountY) {
                visualScrollY(this.amountY);
            }
        } else {
            if (this.visualAmountX != this.amountX) {
                if (this.visualAmountX < this.amountX) {
                    visualScrollX(Math.min(this.amountX, this.visualAmountX + Math.max(200.0f * delta, (this.amountX - this.visualAmountX) * 7.0f * delta)));
                } else {
                    visualScrollX(Math.max(this.amountX, this.visualAmountX - Math.max(200.0f * delta, ((this.visualAmountX - this.amountX) * 7.0f) * delta)));
                }
                animating = true;
            }
            if (this.visualAmountY != this.amountY) {
                if (this.visualAmountY < this.amountY) {
                    visualScrollY(Math.min(this.amountY, this.visualAmountY + Math.max(200.0f * delta, (this.amountY - this.visualAmountY) * 7.0f * delta)));
                } else {
                    visualScrollY(Math.max(this.amountY, this.visualAmountY - Math.max(200.0f * delta, ((this.visualAmountY - this.amountY) * 7.0f) * delta)));
                }
                animating = true;
            }
        }
        if (!panning) {
            if (this.overscrollX && this.scrollX) {
                if (this.amountX < Animation.CurveTimeline.LINEAR) {
                    resetFade();
                    this.amountX += (this.overscrollSpeedMin + (((this.overscrollSpeedMax - this.overscrollSpeedMin) * (-this.amountX)) / this.overscrollDistance)) * delta;
                    if (this.amountX > Animation.CurveTimeline.LINEAR) {
                        scrollX(Animation.CurveTimeline.LINEAR);
                    }
                    animating = true;
                } else if (this.amountX > this.maxX) {
                    resetFade();
                    this.amountX -= (this.overscrollSpeedMin + (((this.overscrollSpeedMax - this.overscrollSpeedMin) * (-(this.maxX - this.amountX))) / this.overscrollDistance)) * delta;
                    if (this.amountX < this.maxX) {
                        scrollX(this.maxX);
                    }
                    animating = true;
                }
            }
            if (this.overscrollY && this.scrollY) {
                if (this.amountY < Animation.CurveTimeline.LINEAR) {
                    resetFade();
                    this.amountY += (this.overscrollSpeedMin + (((this.overscrollSpeedMax - this.overscrollSpeedMin) * (-this.amountY)) / this.overscrollDistance)) * delta;
                    if (this.amountY > Animation.CurveTimeline.LINEAR) {
                        scrollY(Animation.CurveTimeline.LINEAR);
                    }
                    animating = true;
                } else if (this.amountY > this.maxY) {
                    resetFade();
                    this.amountY -= (this.overscrollSpeedMin + (((this.overscrollSpeedMax - this.overscrollSpeedMin) * (-(this.maxY - this.amountY))) / this.overscrollDistance)) * delta;
                    if (this.amountY < this.maxY) {
                        scrollY(this.maxY);
                    }
                    animating = true;
                }
            }
        }
        if (animating && (stage = getStage()) != null && stage.getActionsRequestRendering()) {
            Gdx.graphics.requestRendering();
        }
    }

    public void layout() {
        float widgetWidth;
        float widgetHeight;
        float widgetHeight2;
        float boundsY;
        float boundsX;
        Drawable bg = this.style.background;
        Drawable hScrollKnob = this.style.hScrollKnob;
        Drawable vScrollKnob = this.style.vScrollKnob;
        float bgLeftWidth = Animation.CurveTimeline.LINEAR;
        float bgRightWidth = Animation.CurveTimeline.LINEAR;
        float bgTopHeight = Animation.CurveTimeline.LINEAR;
        float bgBottomHeight = Animation.CurveTimeline.LINEAR;
        if (bg != null) {
            bgLeftWidth = bg.getLeftWidth();
            bgRightWidth = bg.getRightWidth();
            bgTopHeight = bg.getTopHeight();
            bgBottomHeight = bg.getBottomHeight();
        }
        float width = getWidth();
        float height = getHeight();
        float scrollbarHeight = Animation.CurveTimeline.LINEAR;
        if (hScrollKnob != null) {
            scrollbarHeight = hScrollKnob.getMinHeight();
        }
        if (this.style.hScroll != null) {
            scrollbarHeight = Math.max(scrollbarHeight, this.style.hScroll.getMinHeight());
        }
        float scrollbarWidth = Animation.CurveTimeline.LINEAR;
        if (vScrollKnob != null) {
            scrollbarWidth = vScrollKnob.getMinWidth();
        }
        if (this.style.vScroll != null) {
            scrollbarWidth = Math.max(scrollbarWidth, this.style.vScroll.getMinWidth());
        }
        this.areaWidth = (width - bgLeftWidth) - bgRightWidth;
        this.areaHeight = (height - bgTopHeight) - bgBottomHeight;
        if (this.widget != null) {
            if (this.widget instanceof Layout) {
                Layout layout = (Layout) this.widget;
                widgetWidth = layout.getPrefWidth();
                widgetHeight = layout.getPrefHeight();
            } else {
                widgetWidth = this.widget.getWidth();
                widgetHeight = this.widget.getHeight();
            }
            this.scrollX = this.forceScrollX || (widgetWidth > this.areaWidth && !this.disableX);
            this.scrollY = this.forceScrollY || (widgetHeight > this.areaHeight && !this.disableY);
            boolean fade = this.fadeScrollBars;
            if (!fade) {
                if (this.scrollY) {
                    this.areaWidth = this.areaWidth - scrollbarWidth;
                    if (!this.scrollX && widgetWidth > this.areaWidth && !this.disableX) {
                        this.scrollX = true;
                    }
                }
                if (this.scrollX) {
                    this.areaHeight = this.areaHeight - scrollbarHeight;
                    if (!this.scrollY && widgetHeight > this.areaHeight && !this.disableY) {
                        this.scrollY = true;
                        this.areaWidth = this.areaWidth - scrollbarWidth;
                    }
                }
            }
            this.widgetAreaBounds.set(bgLeftWidth, bgBottomHeight, this.areaWidth, this.areaHeight);
            if (fade) {
                if (this.scrollX && this.scrollY) {
                    this.areaHeight = this.areaHeight - scrollbarHeight;
                    this.areaWidth = this.areaWidth - scrollbarWidth;
                }
            } else if (this.scrollbarsOnTop) {
                if (this.scrollX) {
                    this.widgetAreaBounds.height += scrollbarHeight;
                }
                if (this.scrollY) {
                    this.widgetAreaBounds.width += scrollbarWidth;
                }
            } else {
                if (this.scrollX && this.hScrollOnBottom) {
                    this.widgetAreaBounds.y += scrollbarHeight;
                }
                if (this.scrollY && !this.vScrollOnRight) {
                    this.widgetAreaBounds.x += scrollbarWidth;
                }
            }
            float widgetWidth2 = this.disableX ? this.areaWidth : Math.max(this.areaWidth, widgetWidth);
            if (this.disableY) {
                widgetHeight2 = this.areaHeight;
            } else {
                widgetHeight2 = Math.max(this.areaHeight, widgetHeight);
            }
            this.maxX = widgetWidth2 - this.areaWidth;
            this.maxY = widgetHeight2 - this.areaHeight;
            if (fade && this.scrollX && this.scrollY) {
                this.maxY = this.maxY - scrollbarHeight;
                this.maxX = this.maxX - scrollbarWidth;
            }
            scrollX(MathUtils.clamp(this.amountX, (float) Animation.CurveTimeline.LINEAR, this.maxX));
            scrollY(MathUtils.clamp(this.amountY, (float) Animation.CurveTimeline.LINEAR, this.maxY));
            if (this.scrollX) {
                if (hScrollKnob != null) {
                    float hScrollHeight = this.style.hScroll != null ? this.style.hScroll.getMinHeight() : hScrollKnob.getMinHeight();
                    this.hScrollBounds.set(this.vScrollOnRight ? bgLeftWidth : bgLeftWidth + scrollbarWidth, this.hScrollOnBottom ? bgBottomHeight : (height - bgTopHeight) - hScrollHeight, this.areaWidth, hScrollHeight);
                    if (this.variableSizeKnobs) {
                        this.hKnobBounds.width = Math.max(hScrollKnob.getMinWidth(), (float) ((int) ((this.hScrollBounds.width * this.areaWidth) / widgetWidth2)));
                    } else {
                        this.hKnobBounds.width = hScrollKnob.getMinWidth();
                    }
                    this.hKnobBounds.height = hScrollKnob.getMinHeight();
                    this.hKnobBounds.x = this.hScrollBounds.x + ((float) ((int) ((this.hScrollBounds.width - this.hKnobBounds.width) * getScrollPercentX())));
                    this.hKnobBounds.y = this.hScrollBounds.y;
                } else {
                    this.hScrollBounds.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    this.hKnobBounds.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                }
            }
            if (this.scrollY) {
                if (vScrollKnob != null) {
                    float vScrollWidth = this.style.vScroll != null ? this.style.vScroll.getMinWidth() : vScrollKnob.getMinWidth();
                    if (this.hScrollOnBottom) {
                        boundsY = (height - bgTopHeight) - this.areaHeight;
                    } else {
                        boundsY = bgBottomHeight;
                    }
                    if (this.vScrollOnRight) {
                        boundsX = (width - bgRightWidth) - vScrollWidth;
                    } else {
                        boundsX = bgLeftWidth;
                    }
                    this.vScrollBounds.set(boundsX, boundsY, vScrollWidth, this.areaHeight);
                    this.vKnobBounds.width = vScrollKnob.getMinWidth();
                    if (this.variableSizeKnobs) {
                        this.vKnobBounds.height = Math.max(vScrollKnob.getMinHeight(), (float) ((int) ((this.vScrollBounds.height * this.areaHeight) / widgetHeight2)));
                    } else {
                        this.vKnobBounds.height = vScrollKnob.getMinHeight();
                    }
                    if (this.vScrollOnRight) {
                        this.vKnobBounds.x = (width - bgRightWidth) - vScrollKnob.getMinWidth();
                    } else {
                        this.vKnobBounds.x = bgLeftWidth;
                    }
                    this.vKnobBounds.y = this.vScrollBounds.y + ((float) ((int) ((this.vScrollBounds.height - this.vKnobBounds.height) * (1.0f - getScrollPercentY()))));
                } else {
                    this.vScrollBounds.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    this.vKnobBounds.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                }
            }
            this.widget.setSize(widgetWidth2, widgetHeight2);
            if (this.widget instanceof Layout) {
                ((Layout) this.widget).validate();
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        float y;
        if (this.widget != null) {
            validate();
            applyTransform(batch, computeTransform());
            if (this.scrollX) {
                this.hKnobBounds.x = this.hScrollBounds.x + ((float) ((int) ((this.hScrollBounds.width - this.hKnobBounds.width) * getVisualScrollPercentX())));
            }
            if (this.scrollY) {
                this.vKnobBounds.y = this.vScrollBounds.y + ((float) ((int) ((this.vScrollBounds.height - this.vKnobBounds.height) * (1.0f - getVisualScrollPercentY()))));
            }
            float y2 = this.widgetAreaBounds.y;
            if (!this.scrollY) {
                y = y2 - ((float) ((int) this.maxY));
            } else {
                y = y2 - ((float) ((int) (this.maxY - this.visualAmountY)));
            }
            float x = this.widgetAreaBounds.x;
            if (this.scrollX) {
                x -= (float) ((int) this.visualAmountX);
            }
            if (!this.fadeScrollBars && this.scrollbarsOnTop) {
                if (this.scrollX && this.hScrollOnBottom) {
                    float scrollbarHeight = Animation.CurveTimeline.LINEAR;
                    if (this.style.hScrollKnob != null) {
                        scrollbarHeight = this.style.hScrollKnob.getMinHeight();
                    }
                    if (this.style.hScroll != null) {
                        scrollbarHeight = Math.max(scrollbarHeight, this.style.hScroll.getMinHeight());
                    }
                    y += scrollbarHeight;
                }
                if (this.scrollY && !this.vScrollOnRight) {
                    float scrollbarWidth = Animation.CurveTimeline.LINEAR;
                    if (this.style.hScrollKnob != null) {
                        scrollbarWidth = this.style.hScrollKnob.getMinWidth();
                    }
                    if (this.style.hScroll != null) {
                        scrollbarWidth = Math.max(scrollbarWidth, this.style.hScroll.getMinWidth());
                    }
                    x += scrollbarWidth;
                }
            }
            this.widget.setPosition(x, y);
            if (this.widget instanceof Cullable) {
                this.widgetCullingArea.x = (-this.widget.getX()) + this.widgetAreaBounds.x;
                this.widgetCullingArea.y = (-this.widget.getY()) + this.widgetAreaBounds.y;
                this.widgetCullingArea.width = this.widgetAreaBounds.width;
                this.widgetCullingArea.height = this.widgetAreaBounds.height;
                ((Cullable) this.widget).setCullingArea(this.widgetCullingArea);
            }
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            if (this.style.background != null) {
                this.style.background.draw(batch, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, getWidth(), getHeight());
                batch.flush();
            }
            getStage().calculateScissors(this.widgetAreaBounds, this.scissorBounds);
            if (ScissorStack.pushScissors(this.scissorBounds)) {
                drawChildren(batch, parentAlpha);
                batch.flush();
                ScissorStack.popScissors();
            }
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha * Interpolation.fade.apply(this.fadeAlpha / this.fadeAlphaSeconds));
            if (this.scrollX && this.scrollY && this.style.corner != null) {
                this.style.corner.draw(batch, this.hScrollBounds.width + this.hScrollBounds.x, this.hScrollBounds.y, this.vScrollBounds.width, this.vScrollBounds.y);
            }
            if (this.scrollX) {
                if (this.style.hScroll != null) {
                    this.style.hScroll.draw(batch, this.hScrollBounds.x, this.hScrollBounds.y, this.hScrollBounds.width, this.hScrollBounds.height);
                }
                if (this.style.hScrollKnob != null) {
                    this.style.hScrollKnob.draw(batch, this.hKnobBounds.x, this.hKnobBounds.y, this.hKnobBounds.width, this.hKnobBounds.height);
                }
            }
            if (this.scrollY) {
                if (this.style.vScroll != null) {
                    this.style.vScroll.draw(batch, this.vScrollBounds.x, this.vScrollBounds.y, this.vScrollBounds.width, this.vScrollBounds.height);
                }
                if (this.style.vScrollKnob != null) {
                    this.style.vScrollKnob.draw(batch, this.vKnobBounds.x, this.vKnobBounds.y, this.vKnobBounds.width, this.vKnobBounds.height);
                }
            }
            resetTransform(batch);
        }
    }

    public void fling(float flingTime2, float velocityX2, float velocityY2) {
        this.flingTimer = flingTime2;
        this.velocityX = velocityX2;
        this.velocityY = velocityY2;
    }

    public float getPrefWidth() {
        if (!(this.widget instanceof Layout)) {
            return 150.0f;
        }
        float width = ((Layout) this.widget).getPrefWidth();
        if (this.style.background != null) {
            width += this.style.background.getLeftWidth() + this.style.background.getRightWidth();
        }
        if (!this.forceScrollY) {
            return width;
        }
        float scrollbarWidth = Animation.CurveTimeline.LINEAR;
        if (this.style.vScrollKnob != null) {
            scrollbarWidth = this.style.vScrollKnob.getMinWidth();
        }
        if (this.style.vScroll != null) {
            scrollbarWidth = Math.max(scrollbarWidth, this.style.vScroll.getMinWidth());
        }
        return width + scrollbarWidth;
    }

    public float getPrefHeight() {
        if (!(this.widget instanceof Layout)) {
            return 150.0f;
        }
        float height = ((Layout) this.widget).getPrefHeight();
        if (this.style.background != null) {
            height += this.style.background.getTopHeight() + this.style.background.getBottomHeight();
        }
        if (!this.forceScrollX) {
            return height;
        }
        float scrollbarHeight = Animation.CurveTimeline.LINEAR;
        if (this.style.hScrollKnob != null) {
            scrollbarHeight = this.style.hScrollKnob.getMinHeight();
        }
        if (this.style.hScroll != null) {
            scrollbarHeight = Math.max(scrollbarHeight, this.style.hScroll.getMinHeight());
        }
        return height + scrollbarHeight;
    }

    public float getMinWidth() {
        return Animation.CurveTimeline.LINEAR;
    }

    public float getMinHeight() {
        return Animation.CurveTimeline.LINEAR;
    }

    public void setWidget(Actor widget2) {
        if (widget2 == this) {
            throw new IllegalArgumentException("widget cannot be the ScrollPane.");
        }
        if (this.widget != null) {
            super.removeActor(this.widget);
        }
        this.widget = widget2;
        if (widget2 != null) {
            super.addActor(widget2);
        }
    }

    public Actor getWidget() {
        return this.widget;
    }

    public void addActor(Actor actor) {
        throw new UnsupportedOperationException("Use ScrollPane#setWidget.");
    }

    public void addActorAt(int index, Actor actor) {
        throw new UnsupportedOperationException("Use ScrollPane#setWidget.");
    }

    public void addActorBefore(Actor actorBefore, Actor actor) {
        throw new UnsupportedOperationException("Use ScrollPane#setWidget.");
    }

    public void addActorAfter(Actor actorAfter, Actor actor) {
        throw new UnsupportedOperationException("Use ScrollPane#setWidget.");
    }

    public boolean removeActor(Actor actor) {
        if (actor != this.widget) {
            return false;
        }
        setWidget(null);
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Actor hit(float x, float y, boolean touchable) {
        if (x < Animation.CurveTimeline.LINEAR || x >= getWidth() || y < Animation.CurveTimeline.LINEAR || y >= getHeight()) {
            return null;
        }
        if (!this.scrollX || !this.hScrollBounds.contains(x, y)) {
            return (!this.scrollY || !this.vScrollBounds.contains(x, y)) ? super.hit(x, y, touchable) : this;
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void scrollX(float pixelsX) {
        this.amountX = pixelsX;
    }

    /* access modifiers changed from: protected */
    public void scrollY(float pixelsY) {
        this.amountY = pixelsY;
    }

    /* access modifiers changed from: protected */
    public void visualScrollX(float pixelsX) {
        this.visualAmountX = pixelsX;
    }

    /* access modifiers changed from: protected */
    public void visualScrollY(float pixelsY) {
        this.visualAmountY = pixelsY;
    }

    /* access modifiers changed from: protected */
    public float getMouseWheelX() {
        return Math.max(this.areaWidth * 0.9f, this.maxX * 0.1f) / 4.0f;
    }

    /* access modifiers changed from: protected */
    public float getMouseWheelY() {
        return Math.max(this.areaHeight * 0.9f, this.maxY * 0.1f) / 4.0f;
    }

    public void setScrollX(float pixels) {
        scrollX(MathUtils.clamp(pixels, (float) Animation.CurveTimeline.LINEAR, this.maxX));
    }

    public float getScrollX() {
        return this.amountX;
    }

    public void setScrollY(float pixels) {
        scrollY(MathUtils.clamp(pixels, (float) Animation.CurveTimeline.LINEAR, this.maxY));
    }

    public float getScrollY() {
        return this.amountY;
    }

    public void updateVisualScroll() {
        this.visualAmountX = this.amountX;
        this.visualAmountY = this.amountY;
    }

    public float getVisualScrollX() {
        return !this.scrollX ? Animation.CurveTimeline.LINEAR : this.visualAmountX;
    }

    public float getVisualScrollY() {
        return !this.scrollY ? Animation.CurveTimeline.LINEAR : this.visualAmountY;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public float getVisualScrollPercentX() {
        return MathUtils.clamp(this.visualAmountX / this.maxX, (float) Animation.CurveTimeline.LINEAR, 1.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public float getVisualScrollPercentY() {
        return MathUtils.clamp(this.visualAmountY / this.maxY, (float) Animation.CurveTimeline.LINEAR, 1.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public float getScrollPercentX() {
        return MathUtils.clamp(this.amountX / this.maxX, (float) Animation.CurveTimeline.LINEAR, 1.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public void setScrollPercentX(float percentX) {
        scrollX(this.maxX * MathUtils.clamp(percentX, (float) Animation.CurveTimeline.LINEAR, 1.0f));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public float getScrollPercentY() {
        return MathUtils.clamp(this.amountY / this.maxY, (float) Animation.CurveTimeline.LINEAR, 1.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public void setScrollPercentY(float percentY) {
        scrollY(this.maxY * MathUtils.clamp(percentY, (float) Animation.CurveTimeline.LINEAR, 1.0f));
    }

    public void setFlickScroll(boolean flickScroll2) {
        if (this.flickScroll != flickScroll2) {
            this.flickScroll = flickScroll2;
            if (flickScroll2) {
                addListener(this.flickScrollListener);
            } else {
                removeListener(this.flickScrollListener);
            }
            invalidate();
        }
    }

    public void setFlickScrollTapSquareSize(float halfTapSquareSize) {
        this.flickScrollListener.getGestureDetector().setTapSquareSize(halfTapSquareSize);
    }

    public void scrollTo(float x, float y, float width, float height) {
        scrollTo(x, y, width, height, false, false);
    }

    public void scrollTo(float x, float y, float width, float height, boolean centerHorizontal, boolean centerVertical) {
        float amountX2 = this.amountX;
        if (centerHorizontal) {
            amountX2 = (x - (this.areaWidth / 2.0f)) + (width / 2.0f);
        } else {
            if (x + width > this.areaWidth + amountX2) {
                amountX2 = (x + width) - this.areaWidth;
            }
            if (x < amountX2) {
                amountX2 = x;
            }
        }
        scrollX(MathUtils.clamp(amountX2, (float) Animation.CurveTimeline.LINEAR, this.maxX));
        float amountY2 = this.amountY;
        if (centerVertical) {
            amountY2 = ((this.maxY - y) + (this.areaHeight / 2.0f)) - (height / 2.0f);
        } else {
            if (amountY2 > ((this.maxY - y) - height) + this.areaHeight) {
                amountY2 = ((this.maxY - y) - height) + this.areaHeight;
            }
            if (amountY2 < this.maxY - y) {
                amountY2 = this.maxY - y;
            }
        }
        scrollY(MathUtils.clamp(amountY2, (float) Animation.CurveTimeline.LINEAR, this.maxY));
    }

    public float getMaxX() {
        return this.maxX;
    }

    public float getMaxY() {
        return this.maxY;
    }

    public float getScrollBarHeight() {
        if (!this.scrollX) {
            return Animation.CurveTimeline.LINEAR;
        }
        float height = Animation.CurveTimeline.LINEAR;
        if (this.style.hScrollKnob != null) {
            height = this.style.hScrollKnob.getMinHeight();
        }
        if (this.style.hScroll != null) {
            return Math.max(height, this.style.hScroll.getMinHeight());
        }
        return height;
    }

    public float getScrollBarWidth() {
        if (!this.scrollY) {
            return Animation.CurveTimeline.LINEAR;
        }
        float width = Animation.CurveTimeline.LINEAR;
        if (this.style.vScrollKnob != null) {
            width = this.style.vScrollKnob.getMinWidth();
        }
        if (this.style.vScroll != null) {
            return Math.max(width, this.style.vScroll.getMinWidth());
        }
        return width;
    }

    public float getScrollWidth() {
        return this.areaWidth;
    }

    public float getScrollHeight() {
        return this.areaHeight;
    }

    public boolean isScrollX() {
        return this.scrollX;
    }

    public boolean isScrollY() {
        return this.scrollY;
    }

    public void setScrollingDisabled(boolean x, boolean y) {
        this.disableX = x;
        this.disableY = y;
    }

    public boolean isLeftEdge() {
        return !this.scrollX || this.amountX <= Animation.CurveTimeline.LINEAR;
    }

    public boolean isRightEdge() {
        return !this.scrollX || this.amountX >= this.maxX;
    }

    public boolean isTopEdge() {
        return !this.scrollY || this.amountY <= Animation.CurveTimeline.LINEAR;
    }

    public boolean isBottomEdge() {
        return !this.scrollY || this.amountY >= this.maxY;
    }

    public boolean isDragging() {
        return this.draggingPointer != -1;
    }

    public boolean isPanning() {
        return this.flickScrollListener.getGestureDetector().isPanning();
    }

    public boolean isFlinging() {
        return this.flingTimer > Animation.CurveTimeline.LINEAR;
    }

    public void setVelocityX(float velocityX2) {
        this.velocityX = velocityX2;
    }

    public float getVelocityX() {
        if (this.flingTimer <= Animation.CurveTimeline.LINEAR) {
            return Animation.CurveTimeline.LINEAR;
        }
        float alpha = this.flingTimer / this.flingTime;
        float alpha2 = alpha * alpha * alpha;
        return this.velocityX * alpha2 * alpha2 * alpha2;
    }

    public void setVelocityY(float velocityY2) {
        this.velocityY = velocityY2;
    }

    public float getVelocityY() {
        return this.velocityY;
    }

    public void setOverscroll(boolean overscrollX2, boolean overscrollY2) {
        this.overscrollX = overscrollX2;
        this.overscrollY = overscrollY2;
    }

    public void setupOverscroll(float distance, float speedMin, float speedMax) {
        this.overscrollDistance = distance;
        this.overscrollSpeedMin = speedMin;
        this.overscrollSpeedMax = speedMax;
    }

    public void setForceScroll(boolean x, boolean y) {
        this.forceScrollX = x;
        this.forceScrollY = y;
    }

    public boolean isForceScrollX() {
        return this.forceScrollX;
    }

    public boolean isForceScrollY() {
        return this.forceScrollY;
    }

    public void setFlingTime(float flingTime2) {
        this.flingTime = flingTime2;
    }

    public void setClamp(boolean clamp2) {
        this.clamp = clamp2;
    }

    public void setScrollBarPositions(boolean bottom, boolean right) {
        this.hScrollOnBottom = bottom;
        this.vScrollOnRight = right;
    }

    public void setFadeScrollBars(boolean fadeScrollBars2) {
        if (this.fadeScrollBars != fadeScrollBars2) {
            this.fadeScrollBars = fadeScrollBars2;
            if (!fadeScrollBars2) {
                this.fadeAlpha = this.fadeAlphaSeconds;
            }
            invalidate();
        }
    }

    public void setupFadeScrollBars(float fadeAlphaSeconds2, float fadeDelaySeconds2) {
        this.fadeAlphaSeconds = fadeAlphaSeconds2;
        this.fadeDelaySeconds = fadeDelaySeconds2;
    }

    public void setSmoothScrolling(boolean smoothScrolling2) {
        this.smoothScrolling = smoothScrolling2;
    }

    public void setScrollbarsOnTop(boolean scrollbarsOnTop2) {
        this.scrollbarsOnTop = scrollbarsOnTop2;
        invalidate();
    }

    public boolean getVariableSizeKnobs() {
        return this.variableSizeKnobs;
    }

    public void setVariableSizeKnobs(boolean variableSizeKnobs2) {
        this.variableSizeKnobs = variableSizeKnobs2;
    }

    public void setCancelTouchFocus(boolean cancelTouchFocus2) {
        this.cancelTouchFocus = cancelTouchFocus2;
    }

    public void drawDebug(ShapeRenderer shapes) {
        shapes.flush();
        applyTransform(shapes, computeTransform());
        if (ScissorStack.pushScissors(this.scissorBounds)) {
            drawDebugChildren(shapes);
            ScissorStack.popScissors();
        }
        resetTransform(shapes);
    }

    public String toString() {
        if (this.widget == null) {
            return super.toString();
        }
        return String.valueOf(super.toString()) + ": " + this.widget.toString();
    }

    public static class ScrollPaneStyle {
        public Drawable background;
        public Drawable corner;
        public Drawable hScroll;
        public Drawable hScrollKnob;
        public Drawable vScroll;
        public Drawable vScrollKnob;

        public ScrollPaneStyle() {
        }

        public ScrollPaneStyle(Drawable background2, Drawable hScroll2, Drawable hScrollKnob2, Drawable vScroll2, Drawable vScrollKnob2) {
            this.background = background2;
            this.hScroll = hScroll2;
            this.hScrollKnob = hScrollKnob2;
            this.vScroll = vScroll2;
            this.vScrollKnob = vScrollKnob2;
        }

        public ScrollPaneStyle(ScrollPaneStyle style) {
            this.background = style.background;
            this.hScroll = style.hScroll;
            this.hScrollKnob = style.hScrollKnob;
            this.vScroll = style.vScroll;
            this.vScrollKnob = style.vScrollKnob;
        }
    }
}
