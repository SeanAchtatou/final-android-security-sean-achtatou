package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kbz.esotericsoftware.spine.Animation;

public class Window extends Table {
    private static final int MOVE = 32;
    private static final Vector2 tmpPosition = new Vector2();
    private static final Vector2 tmpSize = new Vector2();
    boolean dragging;
    boolean drawTitleTable;
    boolean isModal;
    boolean isMovable;
    boolean isResizable;
    boolean keepWithinStage;
    int resizeBorder;
    private WindowStyle style;
    Label titleLabel;
    Table titleTable;

    public Window(String title, Skin skin) {
        this(title, (WindowStyle) skin.get(WindowStyle.class));
        setSkin(skin);
    }

    public Window(String title, Skin skin, String styleName) {
        this(title, (WindowStyle) skin.get(styleName, WindowStyle.class));
        setSkin(skin);
    }

    public Window(String title, WindowStyle style2) {
        this.isMovable = true;
        this.resizeBorder = 8;
        this.keepWithinStage = true;
        if (title == null) {
            throw new IllegalArgumentException("title cannot be null.");
        }
        setTouchable(Touchable.enabled);
        setClip(true);
        this.titleLabel = new Label(title, new Label.LabelStyle(style2.titleFont, style2.titleFontColor));
        this.titleTable = new Table() {
            public void draw(Batch batch, float parentAlpha) {
                if (Window.this.drawTitleTable) {
                    super.draw(batch, parentAlpha);
                }
            }
        };
        this.titleTable.add(this.titleLabel).expandX().fillX();
        addActor(this.titleTable);
        setStyle(style2);
        setWidth(150.0f);
        setHeight(150.0f);
        addCaptureListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Window.this.toFront();
                return false;
            }
        });
        addListener(new InputListener() {
            int edge;
            float lastX;
            float lastY;
            float startX;
            float startY;

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                boolean z;
                if (button == 0) {
                    int border = Window.this.resizeBorder;
                    float width = Window.this.getWidth();
                    float height = Window.this.getHeight();
                    this.edge = 0;
                    if (Window.this.isResizable) {
                        if (x < ((float) border)) {
                            this.edge |= 8;
                        }
                        if (x > width - ((float) border)) {
                            this.edge |= 16;
                        }
                        if (y < ((float) border)) {
                            this.edge |= 4;
                        }
                        if (y > height - ((float) border)) {
                            this.edge |= 2;
                        }
                        if (this.edge != 0) {
                            border += 25;
                        }
                        if (x < ((float) border)) {
                            this.edge |= 8;
                        }
                        if (x > width - ((float) border)) {
                            this.edge |= 16;
                        }
                        if (y < ((float) border)) {
                            this.edge |= 4;
                        }
                        if (y > height - ((float) border)) {
                            this.edge |= 2;
                        }
                    }
                    if (Window.this.isMovable && this.edge == 0 && y <= height && y >= height - Window.this.getPadTop() && x >= Animation.CurveTimeline.LINEAR && x <= width) {
                        this.edge = 32;
                    }
                    Window window = Window.this;
                    if (this.edge != 0) {
                        z = true;
                    } else {
                        z = false;
                    }
                    window.dragging = z;
                    this.startX = x;
                    this.startY = y;
                    this.lastX = x;
                    this.lastY = y;
                }
                if (this.edge != 0 || Window.this.isModal) {
                    return true;
                }
                return false;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Window.this.dragging = false;
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (Window.this.dragging) {
                    float width = Window.this.getWidth();
                    float height = Window.this.getHeight();
                    float windowX = Window.this.getX();
                    float windowY = Window.this.getY();
                    float minWidth = Window.this.getMinWidth();
                    float maxWidth = Window.this.getMaxWidth();
                    float minHeight = Window.this.getMinHeight();
                    float maxHeight = Window.this.getMaxHeight();
                    Stage stage = Window.this.getStage();
                    boolean clampPosition = Window.this.keepWithinStage && Window.this.getParent() == stage.getRoot();
                    if ((this.edge & 32) != 0) {
                        windowX += x - this.startX;
                        windowY += y - this.startY;
                    }
                    if ((this.edge & 8) != 0) {
                        float amountX = x - this.startX;
                        if (width - amountX < minWidth) {
                            amountX = -(minWidth - width);
                        }
                        if (clampPosition && windowX + amountX < Animation.CurveTimeline.LINEAR) {
                            amountX = -windowX;
                        }
                        width -= amountX;
                        windowX += amountX;
                    }
                    if ((this.edge & 4) != 0) {
                        float amountY = y - this.startY;
                        if (height - amountY < minHeight) {
                            amountY = -(minHeight - height);
                        }
                        if (clampPosition && windowY + amountY < Animation.CurveTimeline.LINEAR) {
                            amountY = -windowY;
                        }
                        height -= amountY;
                        windowY += amountY;
                    }
                    if ((this.edge & 16) != 0) {
                        float amountX2 = x - this.lastX;
                        if (width + amountX2 < minWidth) {
                            amountX2 = minWidth - width;
                        }
                        if (clampPosition && windowX + width + amountX2 > stage.getWidth()) {
                            amountX2 = (stage.getWidth() - windowX) - width;
                        }
                        width += amountX2;
                    }
                    if ((this.edge & 2) != 0) {
                        float amountY2 = y - this.lastY;
                        if (height + amountY2 < minHeight) {
                            amountY2 = minHeight - height;
                        }
                        if (clampPosition && windowY + height + amountY2 > stage.getHeight()) {
                            amountY2 = (stage.getHeight() - windowY) - height;
                        }
                        height += amountY2;
                    }
                    this.lastX = x;
                    this.lastY = y;
                    Window.this.setBounds((float) Math.round(windowX), (float) Math.round(windowY), (float) Math.round(width), (float) Math.round(height));
                }
            }

            public boolean mouseMoved(InputEvent event, float x, float y) {
                return Window.this.isModal;
            }

            public boolean scrolled(InputEvent event, float x, float y, int amount) {
                return Window.this.isModal;
            }

            public boolean keyDown(InputEvent event, int keycode) {
                return Window.this.isModal;
            }

            public boolean keyUp(InputEvent event, int keycode) {
                return Window.this.isModal;
            }

            public boolean keyTyped(InputEvent event, char character) {
                return Window.this.isModal;
            }
        });
    }

    public void setStyle(WindowStyle style2) {
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        this.style = style2;
        setBackground(style2.background);
        this.titleLabel.setStyle(new Label.LabelStyle(style2.titleFont, style2.titleFontColor));
        invalidateHierarchy();
    }

    public WindowStyle getStyle() {
        return this.style;
    }

    /* access modifiers changed from: package-private */
    public void keepWithinStage() {
        if (this.keepWithinStage) {
            Stage stage = getStage();
            Camera camera = stage.getCamera();
            if (camera instanceof OrthographicCamera) {
                OrthographicCamera orthographicCamera = (OrthographicCamera) camera;
                float parentWidth = stage.getWidth();
                float parentHeight = stage.getHeight();
                if (getX(16) - camera.position.x > (parentWidth / 2.0f) / orthographicCamera.zoom) {
                    setPosition(camera.position.x + ((parentWidth / 2.0f) / orthographicCamera.zoom), getY(16), 16);
                }
                if (getX(8) - camera.position.x < ((-parentWidth) / 2.0f) / orthographicCamera.zoom) {
                    setPosition(camera.position.x - ((parentWidth / 2.0f) / orthographicCamera.zoom), getY(8), 8);
                }
                if (getY(2) - camera.position.y > (parentHeight / 2.0f) / orthographicCamera.zoom) {
                    setPosition(getX(2), camera.position.y + ((parentHeight / 2.0f) / orthographicCamera.zoom), 2);
                }
                if (getY(4) - camera.position.y < ((-parentHeight) / 2.0f) / orthographicCamera.zoom) {
                    setPosition(getX(4), camera.position.y - ((parentHeight / 2.0f) / orthographicCamera.zoom), 4);
                }
            } else if (getParent() == stage.getRoot()) {
                float parentWidth2 = stage.getWidth();
                float parentHeight2 = stage.getHeight();
                if (getX() < Animation.CurveTimeline.LINEAR) {
                    setX(Animation.CurveTimeline.LINEAR);
                }
                if (getRight() > parentWidth2) {
                    setX(parentWidth2 - getWidth());
                }
                if (getY() < Animation.CurveTimeline.LINEAR) {
                    setY(Animation.CurveTimeline.LINEAR);
                }
                if (getTop() > parentHeight2) {
                    setY(parentHeight2 - getHeight());
                }
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        Stage stage = getStage();
        if (stage.getKeyboardFocus() == null) {
            stage.setKeyboardFocus(this);
        }
        keepWithinStage();
        if (this.style.stageBackground != null) {
            stageToLocalCoordinates(tmpPosition.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
            stageToLocalCoordinates(tmpSize.set(stage.getWidth(), stage.getHeight()));
            drawStageBackground(batch, parentAlpha, getX() + tmpPosition.x, getY() + tmpPosition.y, getX() + tmpSize.x, getY() + tmpSize.y);
        }
        super.draw(batch, parentAlpha);
    }

    /* access modifiers changed from: protected */
    public void drawStageBackground(Batch batch, float parentAlpha, float x, float y, float width, float height) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.style.stageBackground.draw(batch, x, y, width, height);
    }

    /* access modifiers changed from: protected */
    public void drawBackground(Batch batch, float parentAlpha, float x, float y) {
        super.drawBackground(batch, parentAlpha, x, y);
        this.titleTable.getColor().a = getColor().a;
        float padTop = getPadTop();
        float padLeft = getPadLeft();
        this.titleTable.setSize((getWidth() - padLeft) - getPadRight(), padTop);
        this.titleTable.setPosition(padLeft, getHeight() - padTop);
        this.drawTitleTable = true;
        this.titleTable.draw(batch, parentAlpha);
        this.drawTitleTable = false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public Actor hit(float x, float y, boolean touchable) {
        Actor hit = super.hit(x, y, touchable);
        if (hit == null && this.isModal && (!touchable || getTouchable() == Touchable.enabled)) {
            return this;
        }
        float height = getHeight();
        if (hit == null || hit == this) {
            return hit;
        }
        if (y <= height && y >= height - getPadTop() && x >= Animation.CurveTimeline.LINEAR && x <= getWidth()) {
            Actor current = hit;
            while (current.getParent() != this) {
                current = current.getParent();
            }
            if (getCell(current) != null) {
                return this;
            }
        }
        return hit;
    }

    public boolean isMovable() {
        return this.isMovable;
    }

    public void setMovable(boolean isMovable2) {
        this.isMovable = isMovable2;
    }

    public boolean isModal() {
        return this.isModal;
    }

    public void setModal(boolean isModal2) {
        this.isModal = isModal2;
    }

    public void setKeepWithinStage(boolean keepWithinStage2) {
        this.keepWithinStage = keepWithinStage2;
    }

    public boolean isResizable() {
        return this.isResizable;
    }

    public void setResizable(boolean isResizable2) {
        this.isResizable = isResizable2;
    }

    public void setResizeBorder(int resizeBorder2) {
        this.resizeBorder = resizeBorder2;
    }

    public boolean isDragging() {
        return this.dragging;
    }

    public float getPrefWidth() {
        return Math.max(super.getPrefWidth(), this.titleLabel.getPrefWidth() + getPadLeft() + getPadRight());
    }

    public Table getTitleTable() {
        return this.titleTable;
    }

    public Label getTitleLabel() {
        return this.titleLabel;
    }

    public static class WindowStyle {
        public Drawable background;
        public Drawable stageBackground;
        public BitmapFont titleFont;
        public Color titleFontColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

        public WindowStyle() {
        }

        public WindowStyle(BitmapFont titleFont2, Color titleFontColor2, Drawable background2) {
            this.background = background2;
            this.titleFont = titleFont2;
            this.titleFontColor.set(titleFontColor2);
        }

        public WindowStyle(WindowStyle style) {
            this.background = style.background;
            this.titleFont = style.titleFont;
            this.titleFontColor = new Color(style.titleFontColor);
        }
    }
}
