package com.badlogic.gdx.scenes.scene2d.utils;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.OrderedSet;
import com.badlogic.gdx.utils.Pools;
import java.util.Iterator;

public class Selection<T> implements Disableable, Iterable<T> {
    private Actor actor;
    boolean isDisabled;
    T lastSelected;
    boolean multiple;
    private final OrderedSet<T> old = new OrderedSet<>();
    private boolean programmaticChangeEvents = true;
    boolean required;
    final OrderedSet<T> selected = new OrderedSet<>();
    private boolean toggle;

    public void setActor(Actor actor2) {
        this.actor = actor2;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:42:0x0073=Splitter:B:42:0x0073, B:23:0x003d=Splitter:B:23:0x003d, B:52:0x008f=Splitter:B:52:0x008f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void choose(T r4) {
        /*
            r3 = this;
            r1 = 1
            if (r4 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "item cannot be null."
            r1.<init>(r2)
            throw r1
        L_0x000b:
            boolean r2 = r3.isDisabled
            if (r2 == 0) goto L_0x0010
        L_0x000f:
            return
        L_0x0010:
            r3.snapshot()
            boolean r2 = r3.toggle     // Catch:{ all -> 0x0092 }
            if (r2 != 0) goto L_0x0027
            boolean r2 = r3.required     // Catch:{ all -> 0x0092 }
            if (r2 != 0) goto L_0x0021
            com.badlogic.gdx.utils.OrderedSet<T> r2 = r3.selected     // Catch:{ all -> 0x0092 }
            int r2 = r2.size     // Catch:{ all -> 0x0092 }
            if (r2 == r1) goto L_0x0027
        L_0x0021:
            boolean r2 = com.badlogic.gdx.scenes.scene2d.utils.UIUtils.ctrl()     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x0052
        L_0x0027:
            com.badlogic.gdx.utils.OrderedSet<T> r2 = r3.selected     // Catch:{ all -> 0x0092 }
            boolean r2 = r2.contains(r4)     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x0052
            boolean r2 = r3.required     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x003d
            com.badlogic.gdx.utils.OrderedSet<T> r2 = r3.selected     // Catch:{ all -> 0x0092 }
            int r2 = r2.size     // Catch:{ all -> 0x0092 }
            if (r2 != r1) goto L_0x003d
            r3.cleanup()
            goto L_0x000f
        L_0x003d:
            com.badlogic.gdx.utils.OrderedSet<T> r1 = r3.selected     // Catch:{ all -> 0x0092 }
            r1.remove(r4)     // Catch:{ all -> 0x0092 }
            r1 = 0
            r3.lastSelected = r1     // Catch:{ all -> 0x0092 }
        L_0x0045:
            boolean r1 = r3.fireChangeEvent()     // Catch:{ all -> 0x0092 }
            if (r1 == 0) goto L_0x004e
            r3.revert()     // Catch:{ all -> 0x0092 }
        L_0x004e:
            r3.cleanup()
            goto L_0x000f
        L_0x0052:
            r0 = 0
            boolean r2 = r3.multiple     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x0061
            boolean r2 = r3.toggle     // Catch:{ all -> 0x0092 }
            if (r2 != 0) goto L_0x007f
            boolean r2 = com.badlogic.gdx.scenes.scene2d.utils.UIUtils.ctrl()     // Catch:{ all -> 0x0092 }
            if (r2 != 0) goto L_0x007f
        L_0x0061:
            com.badlogic.gdx.utils.OrderedSet<T> r2 = r3.selected     // Catch:{ all -> 0x0092 }
            int r2 = r2.size     // Catch:{ all -> 0x0092 }
            if (r2 != r1) goto L_0x0073
            com.badlogic.gdx.utils.OrderedSet<T> r2 = r3.selected     // Catch:{ all -> 0x0092 }
            boolean r2 = r2.contains(r4)     // Catch:{ all -> 0x0092 }
            if (r2 == 0) goto L_0x0073
            r3.cleanup()
            goto L_0x000f
        L_0x0073:
            com.badlogic.gdx.utils.OrderedSet<T> r2 = r3.selected     // Catch:{ all -> 0x0092 }
            int r2 = r2.size     // Catch:{ all -> 0x0092 }
            if (r2 <= 0) goto L_0x008d
            r0 = r1
        L_0x007a:
            com.badlogic.gdx.utils.OrderedSet<T> r1 = r3.selected     // Catch:{ all -> 0x0092 }
            r1.clear()     // Catch:{ all -> 0x0092 }
        L_0x007f:
            com.badlogic.gdx.utils.OrderedSet<T> r1 = r3.selected     // Catch:{ all -> 0x0092 }
            boolean r1 = r1.add(r4)     // Catch:{ all -> 0x0092 }
            if (r1 != 0) goto L_0x008f
            if (r0 != 0) goto L_0x008f
            r3.cleanup()
            goto L_0x000f
        L_0x008d:
            r0 = 0
            goto L_0x007a
        L_0x008f:
            r3.lastSelected = r4     // Catch:{ all -> 0x0092 }
            goto L_0x0045
        L_0x0092:
            r1 = move-exception
            r3.cleanup()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.utils.Selection.choose(java.lang.Object):void");
    }

    public boolean hasItems() {
        return this.selected.size > 0;
    }

    public boolean isEmpty() {
        return this.selected.size == 0;
    }

    public int size() {
        return this.selected.size;
    }

    public OrderedSet<T> items() {
        return this.selected;
    }

    public T first() {
        if (this.selected.size == 0) {
            return null;
        }
        return this.selected.first();
    }

    /* access modifiers changed from: package-private */
    public void snapshot() {
        this.old.clear();
        this.old.addAll(this.selected);
    }

    /* access modifiers changed from: package-private */
    public void revert() {
        this.selected.clear();
        this.selected.addAll(this.old);
    }

    /* access modifiers changed from: package-private */
    public void cleanup() {
        this.old.clear(32);
    }

    public void set(T item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null.");
        } else if (this.selected.size != 1 || this.selected.first() != item) {
            snapshot();
            this.selected.clear();
            this.selected.add(item);
            if (!this.programmaticChangeEvents || !fireChangeEvent()) {
                this.lastSelected = item;
            } else {
                revert();
            }
            cleanup();
        }
    }

    public void setAll(Array<T> items) {
        boolean added = false;
        snapshot();
        this.selected.clear();
        int n = items.size;
        for (int i = 0; i < n; i++) {
            T item = items.get(i);
            if (item == null) {
                throw new IllegalArgumentException("item cannot be null.");
            }
            if (this.selected.add(item)) {
                added = true;
            }
        }
        if (!added || !this.programmaticChangeEvents || !fireChangeEvent()) {
            this.lastSelected = items.peek();
        } else {
            revert();
        }
        cleanup();
    }

    public void add(T item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null.");
        } else if (this.selected.add(item)) {
            if (!this.programmaticChangeEvents || !fireChangeEvent()) {
                this.lastSelected = item;
            } else {
                this.selected.remove(item);
            }
        }
    }

    public void addAll(Array<T> items) {
        boolean added = false;
        snapshot();
        int n = items.size;
        for (int i = 0; i < n; i++) {
            T item = items.get(i);
            if (item == null) {
                throw new IllegalArgumentException("item cannot be null.");
            }
            if (this.selected.add(item)) {
                added = true;
            }
        }
        if (!added || !this.programmaticChangeEvents || !fireChangeEvent()) {
            this.lastSelected = items.peek();
        } else {
            revert();
        }
        cleanup();
    }

    public void remove(T item) {
        if (item == null) {
            throw new IllegalArgumentException("item cannot be null.");
        } else if (this.selected.remove(item)) {
            if (!this.programmaticChangeEvents || !fireChangeEvent()) {
                this.lastSelected = null;
            } else {
                this.selected.add(item);
            }
        }
    }

    public void removeAll(Array<T> items) {
        boolean removed = false;
        snapshot();
        int n = items.size;
        for (int i = 0; i < n; i++) {
            T item = items.get(i);
            if (item == null) {
                throw new IllegalArgumentException("item cannot be null.");
            }
            if (this.selected.remove(item)) {
                removed = true;
            }
        }
        if (!removed || !this.programmaticChangeEvents || !fireChangeEvent()) {
            this.lastSelected = null;
        } else {
            revert();
        }
        cleanup();
    }

    public void clear() {
        if (this.selected.size != 0) {
            snapshot();
            this.selected.clear();
            if (!this.programmaticChangeEvents || !fireChangeEvent()) {
                this.lastSelected = null;
            } else {
                revert();
            }
            cleanup();
        }
    }

    public boolean fireChangeEvent() {
        if (this.actor == null) {
            return false;
        }
        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
        try {
            return this.actor.fire(changeEvent);
        } finally {
            Pools.free(changeEvent);
        }
    }

    public boolean contains(T item) {
        if (item == null) {
            return false;
        }
        return this.selected.contains(item);
    }

    public T getLastSelected() {
        return this.lastSelected != null ? this.lastSelected : this.selected.first();
    }

    public Iterator<T> iterator() {
        return this.selected.iterator();
    }

    public Array<T> toArray() {
        return this.selected.iterator().toArray();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.badlogic.gdx.utils.Array, com.badlogic.gdx.utils.Array<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.Array<T> toArray(com.badlogic.gdx.utils.Array<T> r2) {
        /*
            r1 = this;
            com.badlogic.gdx.utils.OrderedSet<T> r0 = r1.selected
            com.badlogic.gdx.utils.OrderedSet$OrderedSetIterator r0 = r0.iterator()
            com.badlogic.gdx.utils.Array r0 = r0.toArray(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.utils.Selection.toArray(com.badlogic.gdx.utils.Array):com.badlogic.gdx.utils.Array");
    }

    public void setDisabled(boolean isDisabled2) {
        this.isDisabled = isDisabled2;
    }

    public boolean isDisabled() {
        return this.isDisabled;
    }

    public boolean getToggle() {
        return this.toggle;
    }

    public void setToggle(boolean toggle2) {
        this.toggle = toggle2;
    }

    public boolean getMultiple() {
        return this.multiple;
    }

    public void setMultiple(boolean multiple2) {
        this.multiple = multiple2;
    }

    public boolean getRequired() {
        return this.required;
    }

    public void setRequired(boolean required2) {
        this.required = required2;
    }

    public void setProgrammaticChangeEvents(boolean programmaticChangeEvents2) {
        this.programmaticChangeEvents = programmaticChangeEvents2;
    }

    public String toString() {
        return this.selected.toString();
    }
}
