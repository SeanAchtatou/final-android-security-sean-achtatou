package com.badlogic.gdx.scenes.scene2d.utils;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kbz.esotericsoftware.spine.Animation;

public class TiledDrawable extends TextureRegionDrawable {
    public TiledDrawable() {
    }

    public TiledDrawable(TextureRegion region) {
        super(region);
    }

    public TiledDrawable(TextureRegionDrawable drawable) {
        super(drawable);
    }

    public void draw(Batch batch, float x, float y, float width, float height) {
        TextureRegion region = getRegion();
        float regionWidth = (float) region.getRegionWidth();
        float regionHeight = (float) region.getRegionHeight();
        int fullX = (int) (width / regionWidth);
        int fullY = (int) (height / regionHeight);
        float remainingX = width - (((float) fullX) * regionWidth);
        float remainingY = height - (((float) fullY) * regionHeight);
        float startX = x;
        float startY = y;
        float f = (x + width) - remainingX;
        float f2 = (y + height) - remainingY;
        for (int i = 0; i < fullX; i++) {
            y = startY;
            for (int ii = 0; ii < fullY; ii++) {
                batch.draw(region, x, y, regionWidth, regionHeight);
                y += regionHeight;
            }
            x += regionWidth;
        }
        Texture texture = region.getTexture();
        float u = region.getU();
        float v2 = region.getV2();
        if (remainingX > Animation.CurveTimeline.LINEAR) {
            float u2 = u + (remainingX / ((float) texture.getWidth()));
            float v = region.getV();
            y = startY;
            for (int ii2 = 0; ii2 < fullY; ii2++) {
                batch.draw(texture, x, y, remainingX, regionHeight, u, v2, u2, v);
                y = y + regionHeight;
            }
            if (remainingY > Animation.CurveTimeline.LINEAR) {
                batch.draw(texture, x, y, remainingX, remainingY, u, v2, u2, v2 - (remainingY / ((float) texture.getHeight())));
            }
        }
        if (remainingY > Animation.CurveTimeline.LINEAR) {
            float u22 = region.getU2();
            float v3 = v2 - (remainingY / ((float) texture.getHeight()));
            float x2 = startX;
            for (int i2 = 0; i2 < fullX; i2++) {
                batch.draw(texture, x2, y, regionWidth, remainingY, u, v2, u22, v3);
                x2 += regionWidth;
            }
        }
    }
}
