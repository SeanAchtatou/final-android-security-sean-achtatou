package com.badlogic.gdx.scenes.scene2d.actions;

public class MoveToAction extends TemporalAction {
    private int alignment = 12;
    private float endX;
    private float endY;
    private float startX;
    private float startY;

    /* access modifiers changed from: protected */
    public void begin() {
        this.startX = this.target.getX(this.alignment);
        this.startY = this.target.getY(this.alignment);
    }

    /* access modifiers changed from: protected */
    public void update(float percent) {
        this.target.setPosition(this.startX + ((this.endX - this.startX) * percent), this.startY + ((this.endY - this.startY) * percent), this.alignment);
    }

    public void reset() {
        super.reset();
        this.alignment = 12;
    }

    public void setPosition(float x, float y) {
        this.endX = x;
        this.endY = y;
    }

    public void setPosition(float x, float y, int alignment2) {
        this.endX = x;
        this.endY = y;
        this.alignment = alignment2;
    }

    public float getX() {
        return this.endX;
    }

    public void setX(float x) {
        this.endX = x;
    }

    public float getY() {
        return this.endY;
    }

    public void setY(float y) {
        this.endY = y;
    }

    public int getAlignment() {
        return this.alignment;
    }

    public void setAlignment(int alignment2) {
        this.alignment = alignment2;
    }
}
