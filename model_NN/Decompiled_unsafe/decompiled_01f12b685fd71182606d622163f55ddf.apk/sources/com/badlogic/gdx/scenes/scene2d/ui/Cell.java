package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.Animation;

public class Cell<T extends Actor> implements Pool.Poolable {
    private static final Integer bottomi = 4;
    private static final Integer centeri = onei;
    private static final Integer lefti = 8;
    private static final Float onef = Float.valueOf(1.0f);
    private static final Integer onei = 1;
    private static final Integer righti = 16;
    private static final Integer topi = 2;
    private static final Float zerof = Float.valueOf((float) Animation.CurveTimeline.LINEAR);
    private static final Integer zeroi = 0;
    Actor actor;
    float actorHeight;
    float actorWidth;
    float actorX;
    float actorY;
    Integer align;
    int cellAboveIndex;
    Integer colspan;
    int column;
    float computedPadBottom;
    float computedPadLeft;
    float computedPadRight;
    float computedPadTop;
    boolean endRow;
    Integer expandX;
    Integer expandY;
    Float fillX;
    Float fillY;
    Value maxHeight;
    Value maxWidth;
    Value minHeight;
    Value minWidth;
    Value padBottom;
    Value padLeft;
    Value padRight;
    Value padTop;
    Value prefHeight;
    Value prefWidth;
    int row;
    Value spaceBottom;
    Value spaceLeft;
    Value spaceRight;
    Value spaceTop;
    private Table table;
    Boolean uniformX;
    Boolean uniformY;

    public Cell() {
        reset();
    }

    public void setLayout(Table table2) {
        this.table = table2;
    }

    public <A extends Actor> Cell<A> setActor(A newActor) {
        if (this.actor != newActor) {
            if (this.actor != null) {
                this.actor.remove();
            }
            this.actor = newActor;
            if (newActor != null) {
                this.table.addActor(newActor);
            }
        }
        return this;
    }

    public Cell<T> clearActor() {
        setActor(null);
        return this;
    }

    public T getActor() {
        return this.actor;
    }

    public boolean hasActor() {
        return this.actor != null;
    }

    public Cell<T> size(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.minWidth = size;
        this.minHeight = size;
        this.prefWidth = size;
        this.prefHeight = size;
        this.maxWidth = size;
        this.maxHeight = size;
        return this;
    }

    public Cell<T> size(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.minWidth = width;
            this.minHeight = height;
            this.prefWidth = width;
            this.prefHeight = height;
            this.maxWidth = width;
            this.maxHeight = height;
            return this;
        }
    }

    public Cell<T> size(float size) {
        size(new Value.Fixed(size));
        return this;
    }

    public Cell<T> size(float width, float height) {
        size(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Cell<T> width(Value width) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        }
        this.minWidth = width;
        this.prefWidth = width;
        this.maxWidth = width;
        return this;
    }

    public Cell<T> width(float width) {
        width(new Value.Fixed(width));
        return this;
    }

    public Cell<T> height(Value height) {
        if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        }
        this.minHeight = height;
        this.prefHeight = height;
        this.maxHeight = height;
        return this;
    }

    public Cell<T> height(float height) {
        height(new Value.Fixed(height));
        return this;
    }

    public Cell<T> minSize(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.minWidth = size;
        this.minHeight = size;
        return this;
    }

    public Cell<T> minSize(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.minWidth = width;
            this.minHeight = height;
            return this;
        }
    }

    public Cell<T> minWidth(Value minWidth2) {
        if (minWidth2 == null) {
            throw new IllegalArgumentException("minWidth cannot be null.");
        }
        this.minWidth = minWidth2;
        return this;
    }

    public Cell<T> minHeight(Value minHeight2) {
        if (minHeight2 == null) {
            throw new IllegalArgumentException("minHeight cannot be null.");
        }
        this.minHeight = minHeight2;
        return this;
    }

    public Cell<T> minSize(float size) {
        minSize(new Value.Fixed(size));
        return this;
    }

    public Cell<T> minSize(float width, float height) {
        minSize(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Cell<T> minWidth(float minWidth2) {
        this.minWidth = new Value.Fixed(minWidth2);
        return this;
    }

    public Cell<T> minHeight(float minHeight2) {
        this.minHeight = new Value.Fixed(minHeight2);
        return this;
    }

    public Cell<T> prefSize(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.prefWidth = size;
        this.prefHeight = size;
        return this;
    }

    public Cell<T> prefSize(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.prefWidth = width;
            this.prefHeight = height;
            return this;
        }
    }

    public Cell<T> prefWidth(Value prefWidth2) {
        if (prefWidth2 == null) {
            throw new IllegalArgumentException("prefWidth cannot be null.");
        }
        this.prefWidth = prefWidth2;
        return this;
    }

    public Cell<T> prefHeight(Value prefHeight2) {
        if (prefHeight2 == null) {
            throw new IllegalArgumentException("prefHeight cannot be null.");
        }
        this.prefHeight = prefHeight2;
        return this;
    }

    public Cell<T> prefSize(float width, float height) {
        prefSize(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Cell<T> prefSize(float size) {
        prefSize(new Value.Fixed(size));
        return this;
    }

    public Cell<T> prefWidth(float prefWidth2) {
        this.prefWidth = new Value.Fixed(prefWidth2);
        return this;
    }

    public Cell<T> prefHeight(float prefHeight2) {
        this.prefHeight = new Value.Fixed(prefHeight2);
        return this;
    }

    public Cell<T> maxSize(Value size) {
        if (size == null) {
            throw new IllegalArgumentException("size cannot be null.");
        }
        this.maxWidth = size;
        this.maxHeight = size;
        return this;
    }

    public Cell<T> maxSize(Value width, Value height) {
        if (width == null) {
            throw new IllegalArgumentException("width cannot be null.");
        } else if (height == null) {
            throw new IllegalArgumentException("height cannot be null.");
        } else {
            this.maxWidth = width;
            this.maxHeight = height;
            return this;
        }
    }

    public Cell<T> maxWidth(Value maxWidth2) {
        if (maxWidth2 == null) {
            throw new IllegalArgumentException("maxWidth cannot be null.");
        }
        this.maxWidth = maxWidth2;
        return this;
    }

    public Cell<T> maxHeight(Value maxHeight2) {
        if (maxHeight2 == null) {
            throw new IllegalArgumentException("maxHeight cannot be null.");
        }
        this.maxHeight = maxHeight2;
        return this;
    }

    public Cell<T> maxSize(float size) {
        maxSize(new Value.Fixed(size));
        return this;
    }

    public Cell<T> maxSize(float width, float height) {
        maxSize(new Value.Fixed(width), new Value.Fixed(height));
        return this;
    }

    public Cell<T> maxWidth(float maxWidth2) {
        this.maxWidth = new Value.Fixed(maxWidth2);
        return this;
    }

    public Cell<T> maxHeight(float maxHeight2) {
        this.maxHeight = new Value.Fixed(maxHeight2);
        return this;
    }

    public Cell<T> space(Value space) {
        if (space == null) {
            throw new IllegalArgumentException("space cannot be null.");
        }
        this.spaceTop = space;
        this.spaceLeft = space;
        this.spaceBottom = space;
        this.spaceRight = space;
        return this;
    }

    public Cell<T> space(Value top, Value left, Value bottom, Value right) {
        if (top == null) {
            throw new IllegalArgumentException("top cannot be null.");
        } else if (left == null) {
            throw new IllegalArgumentException("left cannot be null.");
        } else if (bottom == null) {
            throw new IllegalArgumentException("bottom cannot be null.");
        } else if (right == null) {
            throw new IllegalArgumentException("right cannot be null.");
        } else {
            this.spaceTop = top;
            this.spaceLeft = left;
            this.spaceBottom = bottom;
            this.spaceRight = right;
            return this;
        }
    }

    public Cell<T> spaceTop(Value spaceTop2) {
        if (spaceTop2 == null) {
            throw new IllegalArgumentException("spaceTop cannot be null.");
        }
        this.spaceTop = spaceTop2;
        return this;
    }

    public Cell<T> spaceLeft(Value spaceLeft2) {
        if (spaceLeft2 == null) {
            throw new IllegalArgumentException("spaceLeft cannot be null.");
        }
        this.spaceLeft = spaceLeft2;
        return this;
    }

    public Cell<T> spaceBottom(Value spaceBottom2) {
        if (spaceBottom2 == null) {
            throw new IllegalArgumentException("spaceBottom cannot be null.");
        }
        this.spaceBottom = spaceBottom2;
        return this;
    }

    public Cell<T> spaceRight(Value spaceRight2) {
        if (spaceRight2 == null) {
            throw new IllegalArgumentException("spaceRight cannot be null.");
        }
        this.spaceRight = spaceRight2;
        return this;
    }

    public Cell<T> space(float space) {
        if (space < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("space cannot be < 0.");
        }
        space(new Value.Fixed(space));
        return this;
    }

    public Cell<T> space(float top, float left, float bottom, float right) {
        if (top < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("top cannot be < 0.");
        } else if (left < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("left cannot be < 0.");
        } else if (bottom < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("bottom cannot be < 0.");
        } else if (right < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("right cannot be < 0.");
        } else {
            space(new Value.Fixed(top), new Value.Fixed(left), new Value.Fixed(bottom), new Value.Fixed(right));
            return this;
        }
    }

    public Cell<T> spaceTop(float spaceTop2) {
        if (spaceTop2 < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("spaceTop cannot be < 0.");
        }
        this.spaceTop = new Value.Fixed(spaceTop2);
        return this;
    }

    public Cell<T> spaceLeft(float spaceLeft2) {
        if (spaceLeft2 < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("spaceLeft cannot be < 0.");
        }
        this.spaceLeft = new Value.Fixed(spaceLeft2);
        return this;
    }

    public Cell<T> spaceBottom(float spaceBottom2) {
        if (spaceBottom2 < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("spaceBottom cannot be < 0.");
        }
        this.spaceBottom = new Value.Fixed(spaceBottom2);
        return this;
    }

    public Cell<T> spaceRight(float spaceRight2) {
        if (spaceRight2 < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("spaceRight cannot be < 0.");
        }
        this.spaceRight = new Value.Fixed(spaceRight2);
        return this;
    }

    public Cell<T> pad(Value pad) {
        if (pad == null) {
            throw new IllegalArgumentException("pad cannot be null.");
        }
        this.padTop = pad;
        this.padLeft = pad;
        this.padBottom = pad;
        this.padRight = pad;
        return this;
    }

    public Cell<T> pad(Value top, Value left, Value bottom, Value right) {
        if (top == null) {
            throw new IllegalArgumentException("top cannot be null.");
        } else if (left == null) {
            throw new IllegalArgumentException("left cannot be null.");
        } else if (bottom == null) {
            throw new IllegalArgumentException("bottom cannot be null.");
        } else if (right == null) {
            throw new IllegalArgumentException("right cannot be null.");
        } else {
            this.padTop = top;
            this.padLeft = left;
            this.padBottom = bottom;
            this.padRight = right;
            return this;
        }
    }

    public Cell<T> padTop(Value padTop2) {
        if (padTop2 == null) {
            throw new IllegalArgumentException("padTop cannot be null.");
        }
        this.padTop = padTop2;
        return this;
    }

    public Cell<T> padLeft(Value padLeft2) {
        if (padLeft2 == null) {
            throw new IllegalArgumentException("padLeft cannot be null.");
        }
        this.padLeft = padLeft2;
        return this;
    }

    public Cell<T> padBottom(Value padBottom2) {
        if (padBottom2 == null) {
            throw new IllegalArgumentException("padBottom cannot be null.");
        }
        this.padBottom = padBottom2;
        return this;
    }

    public Cell<T> padRight(Value padRight2) {
        if (padRight2 == null) {
            throw new IllegalArgumentException("padRight cannot be null.");
        }
        this.padRight = padRight2;
        return this;
    }

    public Cell<T> pad(float pad) {
        pad(new Value.Fixed(pad));
        return this;
    }

    public Cell<T> pad(float top, float left, float bottom, float right) {
        pad(new Value.Fixed(top), new Value.Fixed(left), new Value.Fixed(bottom), new Value.Fixed(right));
        return this;
    }

    public Cell<T> padTop(float padTop2) {
        this.padTop = new Value.Fixed(padTop2);
        return this;
    }

    public Cell<T> padLeft(float padLeft2) {
        this.padLeft = new Value.Fixed(padLeft2);
        return this;
    }

    public Cell<T> padBottom(float padBottom2) {
        this.padBottom = new Value.Fixed(padBottom2);
        return this;
    }

    public Cell<T> padRight(float padRight2) {
        this.padRight = new Value.Fixed(padRight2);
        return this;
    }

    public Cell<T> fill() {
        this.fillX = onef;
        this.fillY = onef;
        return this;
    }

    public Cell<T> fillX() {
        this.fillX = onef;
        return this;
    }

    public Cell<T> fillY() {
        this.fillY = onef;
        return this;
    }

    public Cell<T> fill(float x, float y) {
        this.fillX = Float.valueOf(x);
        this.fillY = Float.valueOf(y);
        return this;
    }

    public Cell<T> fill(boolean x, boolean y) {
        this.fillX = x ? onef : zerof;
        this.fillY = y ? onef : zerof;
        return this;
    }

    public Cell<T> fill(boolean fill) {
        this.fillX = fill ? onef : zerof;
        this.fillY = fill ? onef : zerof;
        return this;
    }

    public Cell<T> align(int align2) {
        this.align = Integer.valueOf(align2);
        return this;
    }

    public Cell<T> center() {
        this.align = centeri;
        return this;
    }

    public Cell<T> top() {
        if (this.align == null) {
            this.align = topi;
        } else {
            this.align = Integer.valueOf((this.align.intValue() | 2) & -5);
        }
        return this;
    }

    public Cell<T> left() {
        if (this.align == null) {
            this.align = lefti;
        } else {
            this.align = Integer.valueOf((this.align.intValue() | 8) & -17);
        }
        return this;
    }

    public Cell<T> bottom() {
        if (this.align == null) {
            this.align = bottomi;
        } else {
            this.align = Integer.valueOf((this.align.intValue() | 4) & -3);
        }
        return this;
    }

    public Cell<T> right() {
        if (this.align == null) {
            this.align = righti;
        } else {
            this.align = Integer.valueOf((this.align.intValue() | 16) & -9);
        }
        return this;
    }

    public Cell<T> expand() {
        this.expandX = onei;
        this.expandY = onei;
        return this;
    }

    public Cell<T> expandX() {
        this.expandX = onei;
        return this;
    }

    public Cell<T> expandY() {
        this.expandY = onei;
        return this;
    }

    public Cell<T> expand(int x, int y) {
        this.expandX = Integer.valueOf(x);
        this.expandY = Integer.valueOf(y);
        return this;
    }

    public Cell<T> expand(boolean x, boolean y) {
        this.expandX = x ? onei : zeroi;
        this.expandY = y ? onei : zeroi;
        return this;
    }

    public Cell<T> colspan(int colspan2) {
        this.colspan = Integer.valueOf(colspan2);
        return this;
    }

    public Cell<T> uniform() {
        this.uniformX = Boolean.TRUE;
        this.uniformY = Boolean.TRUE;
        return this;
    }

    public Cell<T> uniformX() {
        this.uniformX = Boolean.TRUE;
        return this;
    }

    public Cell<T> uniformY() {
        this.uniformY = Boolean.TRUE;
        return this;
    }

    public Cell<T> uniform(boolean x, boolean y) {
        this.uniformX = Boolean.valueOf(x);
        this.uniformY = Boolean.valueOf(y);
        return this;
    }

    public void setActorBounds(float x, float y, float width, float height) {
        this.actorX = x;
        this.actorY = y;
        this.actorWidth = width;
        this.actorHeight = height;
    }

    public float getActorX() {
        return this.actorX;
    }

    public void setActorX(float actorX2) {
        this.actorX = actorX2;
    }

    public float getActorY() {
        return this.actorY;
    }

    public void setActorY(float actorY2) {
        this.actorY = actorY2;
    }

    public float getActorWidth() {
        return this.actorWidth;
    }

    public void setActorWidth(float actorWidth2) {
        this.actorWidth = actorWidth2;
    }

    public float getActorHeight() {
        return this.actorHeight;
    }

    public void setActorHeight(float actorHeight2) {
        this.actorHeight = actorHeight2;
    }

    public int getColumn() {
        return this.column;
    }

    public int getRow() {
        return this.row;
    }

    public Value getMinWidthValue() {
        return this.minWidth;
    }

    public float getMinWidth() {
        return this.minWidth.get(this.actor);
    }

    public Value getMinHeightValue() {
        return this.minHeight;
    }

    public float getMinHeight() {
        return this.minHeight.get(this.actor);
    }

    public Value getPrefWidthValue() {
        return this.prefWidth;
    }

    public float getPrefWidth() {
        return this.prefWidth.get(this.actor);
    }

    public Value getPrefHeightValue() {
        return this.prefHeight;
    }

    public float getPrefHeight() {
        return this.prefHeight.get(this.actor);
    }

    public Value getMaxWidthValue() {
        return this.maxWidth;
    }

    public float getMaxWidth() {
        return this.maxWidth.get(this.actor);
    }

    public Value getMaxHeightValue() {
        return this.maxHeight;
    }

    public float getMaxHeight() {
        return this.maxHeight.get(this.actor);
    }

    public Value getSpaceTopValue() {
        return this.spaceTop;
    }

    public float getSpaceTop() {
        return this.spaceTop.get(this.actor);
    }

    public Value getSpaceLeftValue() {
        return this.spaceLeft;
    }

    public float getSpaceLeft() {
        return this.spaceLeft.get(this.actor);
    }

    public Value getSpaceBottomValue() {
        return this.spaceBottom;
    }

    public float getSpaceBottom() {
        return this.spaceBottom.get(this.actor);
    }

    public Value getSpaceRightValue() {
        return this.spaceRight;
    }

    public float getSpaceRight() {
        return this.spaceRight.get(this.actor);
    }

    public Value getPadTopValue() {
        return this.padTop;
    }

    public float getPadTop() {
        return this.padTop.get(this.actor);
    }

    public Value getPadLeftValue() {
        return this.padLeft;
    }

    public float getPadLeft() {
        return this.padLeft.get(this.actor);
    }

    public Value getPadBottomValue() {
        return this.padBottom;
    }

    public float getPadBottom() {
        return this.padBottom.get(this.actor);
    }

    public Value getPadRightValue() {
        return this.padRight;
    }

    public float getPadRight() {
        return this.padRight.get(this.actor);
    }

    public float getPadX() {
        return this.padLeft.get(this.actor) + this.padRight.get(this.actor);
    }

    public float getPadY() {
        return this.padTop.get(this.actor) + this.padBottom.get(this.actor);
    }

    public float getFillX() {
        return this.fillX.floatValue();
    }

    public float getFillY() {
        return this.fillY.floatValue();
    }

    public int getAlign() {
        return this.align.intValue();
    }

    public int getExpandX() {
        return this.expandX.intValue();
    }

    public int getExpandY() {
        return this.expandY.intValue();
    }

    public int getColspan() {
        return this.colspan.intValue();
    }

    public boolean getUniformX() {
        return this.uniformX.booleanValue();
    }

    public boolean getUniformY() {
        return this.uniformY.booleanValue();
    }

    public boolean isEndRow() {
        return this.endRow;
    }

    public float getComputedPadTop() {
        return this.computedPadTop;
    }

    public float getComputedPadLeft() {
        return this.computedPadLeft;
    }

    public float getComputedPadBottom() {
        return this.computedPadBottom;
    }

    public float getComputedPadRight() {
        return this.computedPadRight;
    }

    public void row() {
        this.table.row();
    }

    public Table getTable() {
        return this.table;
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.minWidth = null;
        this.minHeight = null;
        this.prefWidth = null;
        this.prefHeight = null;
        this.maxWidth = null;
        this.maxHeight = null;
        this.spaceTop = null;
        this.spaceLeft = null;
        this.spaceBottom = null;
        this.spaceRight = null;
        this.padTop = null;
        this.padLeft = null;
        this.padBottom = null;
        this.padRight = null;
        this.fillX = null;
        this.fillY = null;
        this.align = null;
        this.expandX = null;
        this.expandY = null;
        this.colspan = null;
        this.uniformX = null;
        this.uniformY = null;
    }

    public void reset() {
        this.actor = null;
        this.table = null;
        this.endRow = false;
        this.cellAboveIndex = -1;
        this.minWidth = Value.minWidth;
        this.minHeight = Value.minHeight;
        this.prefWidth = Value.prefWidth;
        this.prefHeight = Value.prefHeight;
        this.maxWidth = Value.maxWidth;
        this.maxHeight = Value.maxHeight;
        this.spaceTop = Value.zero;
        this.spaceLeft = Value.zero;
        this.spaceBottom = Value.zero;
        this.spaceRight = Value.zero;
        this.padTop = Value.zero;
        this.padLeft = Value.zero;
        this.padBottom = Value.zero;
        this.padRight = Value.zero;
        this.fillX = zerof;
        this.fillY = zerof;
        this.align = centeri;
        this.expandX = zeroi;
        this.expandY = zeroi;
        this.colspan = onei;
        this.uniformX = null;
        this.uniformY = null;
    }

    /* access modifiers changed from: package-private */
    public void set(Cell defaults) {
        this.minWidth = defaults.minWidth;
        this.minHeight = defaults.minHeight;
        this.prefWidth = defaults.prefWidth;
        this.prefHeight = defaults.prefHeight;
        this.maxWidth = defaults.maxWidth;
        this.maxHeight = defaults.maxHeight;
        this.spaceTop = defaults.spaceTop;
        this.spaceLeft = defaults.spaceLeft;
        this.spaceBottom = defaults.spaceBottom;
        this.spaceRight = defaults.spaceRight;
        this.padTop = defaults.padTop;
        this.padLeft = defaults.padLeft;
        this.padBottom = defaults.padBottom;
        this.padRight = defaults.padRight;
        this.fillX = defaults.fillX;
        this.fillY = defaults.fillY;
        this.align = defaults.align;
        this.expandX = defaults.expandX;
        this.expandY = defaults.expandY;
        this.colspan = defaults.colspan;
        this.uniformX = defaults.uniformX;
        this.uniformY = defaults.uniformY;
    }

    /* access modifiers changed from: package-private */
    public void merge(Cell cell) {
        if (cell != null) {
            if (cell.minWidth != null) {
                this.minWidth = cell.minWidth;
            }
            if (cell.minHeight != null) {
                this.minHeight = cell.minHeight;
            }
            if (cell.prefWidth != null) {
                this.prefWidth = cell.prefWidth;
            }
            if (cell.prefHeight != null) {
                this.prefHeight = cell.prefHeight;
            }
            if (cell.maxWidth != null) {
                this.maxWidth = cell.maxWidth;
            }
            if (cell.maxHeight != null) {
                this.maxHeight = cell.maxHeight;
            }
            if (cell.spaceTop != null) {
                this.spaceTop = cell.spaceTop;
            }
            if (cell.spaceLeft != null) {
                this.spaceLeft = cell.spaceLeft;
            }
            if (cell.spaceBottom != null) {
                this.spaceBottom = cell.spaceBottom;
            }
            if (cell.spaceRight != null) {
                this.spaceRight = cell.spaceRight;
            }
            if (cell.padTop != null) {
                this.padTop = cell.padTop;
            }
            if (cell.padLeft != null) {
                this.padLeft = cell.padLeft;
            }
            if (cell.padBottom != null) {
                this.padBottom = cell.padBottom;
            }
            if (cell.padRight != null) {
                this.padRight = cell.padRight;
            }
            if (cell.fillX != null) {
                this.fillX = cell.fillX;
            }
            if (cell.fillY != null) {
                this.fillY = cell.fillY;
            }
            if (cell.align != null) {
                this.align = cell.align;
            }
            if (cell.expandX != null) {
                this.expandX = cell.expandX;
            }
            if (cell.expandY != null) {
                this.expandY = cell.expandY;
            }
            if (cell.colspan != null) {
                this.colspan = cell.colspan;
            }
            if (cell.uniformX != null) {
                this.uniformX = cell.uniformX;
            }
            if (cell.uniformY != null) {
                this.uniformY = cell.uniformY;
            }
        }
    }
}
