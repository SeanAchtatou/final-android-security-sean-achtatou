package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.Layout;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;

public class Table extends WidgetGroup {
    public static Value backgroundBottom = new Value() {
        public float get(Actor context) {
            Drawable background = ((Table) context).background;
            return background == null ? Animation.CurveTimeline.LINEAR : background.getBottomHeight();
        }
    };
    public static Value backgroundLeft = new Value() {
        public float get(Actor context) {
            Drawable background = ((Table) context).background;
            return background == null ? Animation.CurveTimeline.LINEAR : background.getLeftWidth();
        }
    };
    public static Value backgroundRight = new Value() {
        public float get(Actor context) {
            Drawable background = ((Table) context).background;
            return background == null ? Animation.CurveTimeline.LINEAR : background.getRightWidth();
        }
    };
    public static Value backgroundTop = new Value() {
        public float get(Actor context) {
            Drawable background = ((Table) context).background;
            return background == null ? Animation.CurveTimeline.LINEAR : background.getTopHeight();
        }
    };
    static final Pool<Cell> cellPool = new Pool<Cell>() {
        /* access modifiers changed from: protected */
        public Cell newObject() {
            return new Cell();
        }
    };
    private static float[] columnWeightedWidth;
    public static Color debugActorColor = new Color(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 1.0f);
    public static Color debugCellColor = new Color(1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
    public static Color debugTableColor = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 1.0f);
    private static float[] rowWeightedHeight;
    int align;
    Drawable background;
    private final Cell cellDefaults;
    private final Array<Cell> cells;
    private boolean clip;
    private final Array<Cell> columnDefaults;
    private float[] columnMinWidth;
    private float[] columnPrefWidth;
    private float[] columnWidth;
    private int columns;
    Debug debug;
    Array<DebugRect> debugRects;
    private float[] expandHeight;
    private float[] expandWidth;
    Value padBottom;
    Value padLeft;
    Value padRight;
    Value padTop;
    boolean round;
    private Cell rowDefaults;
    private float[] rowHeight;
    private float[] rowMinHeight;
    private float[] rowPrefHeight;
    private int rows;
    private boolean sizeInvalid;
    private Skin skin;
    private float tableMinHeight;
    private float tableMinWidth;
    private float tablePrefHeight;
    private float tablePrefWidth;

    public enum Debug {
        none,
        all,
        table,
        cell,
        actor
    }

    public static class DebugRect extends Rectangle {
        static Pool<DebugRect> pool = Pools.get(DebugRect.class);
        Color color;
    }

    public Table() {
        this(null);
    }

    public Table(Skin skin2) {
        this.cells = new Array<>(4);
        this.columnDefaults = new Array<>(2);
        this.sizeInvalid = true;
        this.padTop = backgroundTop;
        this.padLeft = backgroundLeft;
        this.padBottom = backgroundBottom;
        this.padRight = backgroundRight;
        this.align = 1;
        this.debug = Debug.none;
        this.round = true;
        this.skin = skin2;
        this.cellDefaults = obtainCell();
        setTransform(false);
        setTouchable(Touchable.childrenOnly);
    }

    private Cell obtainCell() {
        Cell cell = cellPool.obtain();
        cell.setLayout(this);
        return cell;
    }

    public void draw(Batch batch, float parentAlpha) {
        validate();
        if (isTransform()) {
            applyTransform(batch, computeTransform());
            drawBackground(batch, parentAlpha, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            if (this.clip) {
                batch.flush();
                float padLeft2 = this.padLeft.get(this);
                float padBottom2 = this.padBottom.get(this);
                if (clipBegin(padLeft2, padBottom2, (getWidth() - padLeft2) - this.padRight.get(this), (getHeight() - padBottom2) - this.padTop.get(this))) {
                    drawChildren(batch, parentAlpha);
                    batch.flush();
                    clipEnd();
                }
            } else {
                drawChildren(batch, parentAlpha);
            }
            resetTransform(batch);
            return;
        }
        drawBackground(batch, parentAlpha, getX(), getY());
        super.draw(batch, parentAlpha);
    }

    /* access modifiers changed from: protected */
    public void drawBackground(Batch batch, float parentAlpha, float x, float y) {
        if (this.background != null) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            this.background.draw(batch, x, y, getWidth(), getHeight());
        }
    }

    public void setBackground(String drawableName) {
        if (this.skin == null) {
            throw new IllegalStateException("Table must have a skin set to use this method.");
        }
        setBackground(this.skin.getDrawable(drawableName));
    }

    public void setBackground(Drawable background2) {
        if (this.background != background2) {
            float padTopOld = getPadTop();
            float padLeftOld = getPadLeft();
            float padBottomOld = getPadBottom();
            float padRightOld = getPadRight();
            this.background = background2;
            float padTopNew = getPadTop();
            float padLeftNew = getPadLeft();
            float padBottomNew = getPadBottom();
            float padRightNew = getPadRight();
            if (padTopOld + padBottomOld != padTopNew + padBottomNew || padLeftOld + padRightOld != padLeftNew + padRightNew) {
                invalidateHierarchy();
            } else if (padTopOld != padTopNew || padLeftOld != padLeftNew || padBottomOld != padBottomNew || padRightOld != padRightNew) {
                invalidate();
            }
        }
    }

    public Table background(Drawable background2) {
        setBackground(background2);
        return this;
    }

    public Table background(String drawableName) {
        setBackground(drawableName);
        return this;
    }

    public Drawable getBackground() {
        return this.background;
    }

    public Actor hit(float x, float y, boolean touchable) {
        if (!this.clip || ((!touchable || getTouchable() != Touchable.disabled) && x >= Animation.CurveTimeline.LINEAR && x < getWidth() && y >= Animation.CurveTimeline.LINEAR && y < getHeight())) {
            return super.hit(x, y, touchable);
        }
        return null;
    }

    public void setClip(boolean enabled) {
        this.clip = enabled;
        setTransform(enabled);
        invalidate();
    }

    public boolean getClip() {
        return this.clip;
    }

    public void invalidate() {
        this.sizeInvalid = true;
        super.invalidate();
    }

    public <T extends Actor> Cell<T> add(T actor) {
        Cell columnCell;
        Cell<T> cell = obtainCell();
        cell.actor = actor;
        Array<Cell> cells2 = this.cells;
        int cellCount = cells2.size;
        if (cellCount > 0) {
            Cell lastCell = cells2.peek();
            if (!lastCell.endRow) {
                cell.column = lastCell.column + lastCell.colspan.intValue();
                cell.row = lastCell.row;
            } else {
                cell.column = 0;
                cell.row = lastCell.row + 1;
            }
            if (cell.row > 0) {
                int i = cellCount - 1;
                loop0:
                while (true) {
                    if (i < 0) {
                        break;
                    }
                    Cell other = cells2.get(i);
                    int column = other.column;
                    int nn = column + other.colspan.intValue();
                    while (column < nn) {
                        if (column == cell.column) {
                            cell.cellAboveIndex = i;
                            break loop0;
                        }
                        column++;
                    }
                    i--;
                }
            }
        } else {
            cell.column = 0;
            cell.row = 0;
        }
        cells2.add(cell);
        cell.set(this.cellDefaults);
        if (cell.column < this.columnDefaults.size && (columnCell = this.columnDefaults.get(cell.column)) != null) {
            cell.merge(columnCell);
        }
        cell.merge(this.rowDefaults);
        if (actor != null) {
            addActor(actor);
        }
        return cell;
    }

    public void add(Actor... actors) {
        for (Actor add : actors) {
            add(add);
        }
    }

    public Cell<Label> add(String text) {
        if (this.skin != null) {
            return add(new Label(text, this.skin));
        }
        throw new IllegalStateException("Table must have a skin set to use this method.");
    }

    public Cell<Label> add(String text, String labelStyleName) {
        if (this.skin != null) {
            return add(new Label(text, (Label.LabelStyle) this.skin.get(labelStyleName, Label.LabelStyle.class)));
        }
        throw new IllegalStateException("Table must have a skin set to use this method.");
    }

    public Cell<Label> add(String text, String fontName, Color color) {
        if (this.skin != null) {
            return add(new Label(text, new Label.LabelStyle(this.skin.getFont(fontName), color)));
        }
        throw new IllegalStateException("Table must have a skin set to use this method.");
    }

    public Cell<Label> add(String text, String fontName, String colorName) {
        if (this.skin != null) {
            return add(new Label(text, new Label.LabelStyle(this.skin.getFont(fontName), this.skin.getColor(colorName))));
        }
        throw new IllegalStateException("Table must have a skin set to use this method.");
    }

    public Cell add() {
        return add((Actor) null);
    }

    public Cell<Stack> stack(Actor... actors) {
        Stack stack = new Stack();
        if (actors != null) {
            for (Actor addActor : actors) {
                stack.addActor(addActor);
            }
        }
        return add(stack);
    }

    public boolean removeActor(Actor actor) {
        return removeActor(actor, true);
    }

    public boolean removeActor(Actor actor, boolean unfocus) {
        if (!super.removeActor(actor, unfocus)) {
            return false;
        }
        Cell cell = getCell(actor);
        if (cell != null) {
            cell.actor = null;
        }
        return true;
    }

    public void clearChildren() {
        Array<Cell> cells2 = this.cells;
        for (int i = cells2.size - 1; i >= 0; i--) {
            Actor actor = cells2.get(i).actor;
            if (actor != null) {
                actor.remove();
            }
        }
        cellPool.freeAll(cells2);
        cells2.clear();
        this.rows = 0;
        this.columns = 0;
        if (this.rowDefaults != null) {
            cellPool.free(this.rowDefaults);
        }
        this.rowDefaults = null;
        super.clearChildren();
    }

    public void reset() {
        clear();
        this.padTop = backgroundTop;
        this.padLeft = backgroundLeft;
        this.padBottom = backgroundBottom;
        this.padRight = backgroundRight;
        this.align = 1;
        debug(Debug.none);
        this.cellDefaults.reset();
        int n = this.columnDefaults.size;
        for (int i = 0; i < n; i++) {
            Cell columnCell = this.columnDefaults.get(i);
            if (columnCell != null) {
                cellPool.free(columnCell);
            }
        }
        this.columnDefaults.clear();
    }

    public Cell row() {
        if (this.cells.size > 0) {
            endRow();
            invalidate();
        }
        if (this.rowDefaults != null) {
            cellPool.free(this.rowDefaults);
        }
        this.rowDefaults = obtainCell();
        this.rowDefaults.clear();
        return this.rowDefaults;
    }

    private void endRow() {
        Array<Cell> cells2 = this.cells;
        int rowColumns = 0;
        for (int i = cells2.size - 1; i >= 0; i--) {
            Cell cell = cells2.get(i);
            if (cell.endRow) {
                break;
            }
            rowColumns += cell.colspan.intValue();
        }
        this.columns = Math.max(this.columns, rowColumns);
        this.rows++;
        cells2.peek().endRow = true;
    }

    public Cell columnDefaults(int column) {
        Cell cell;
        if (this.columnDefaults.size > column) {
            cell = this.columnDefaults.get(column);
        } else {
            cell = null;
        }
        if (cell == null) {
            cell = obtainCell();
            cell.clear();
            if (column >= this.columnDefaults.size) {
                for (int i = this.columnDefaults.size; i < column; i++) {
                    this.columnDefaults.add(null);
                }
                this.columnDefaults.add(cell);
            } else {
                this.columnDefaults.set(column, cell);
            }
        }
        return cell;
    }

    public <T extends Actor> Cell<T> getCell(T actor) {
        Array<Cell> cells2 = this.cells;
        int n = cells2.size;
        for (int i = 0; i < n; i++) {
            Cell c = cells2.get(i);
            if (c.actor == actor) {
                return c;
            }
        }
        return null;
    }

    public Array<Cell> getCells() {
        return this.cells;
    }

    public float getPrefWidth() {
        if (this.sizeInvalid) {
            computeSize();
        }
        float width = this.tablePrefWidth;
        if (this.background != null) {
            return Math.max(width, this.background.getMinWidth());
        }
        return width;
    }

    public float getPrefHeight() {
        if (this.sizeInvalid) {
            computeSize();
        }
        float height = this.tablePrefHeight;
        if (this.background != null) {
            return Math.max(height, this.background.getMinHeight());
        }
        return height;
    }

    public float getMinWidth() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.tableMinWidth;
    }

    public float getMinHeight() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.tableMinHeight;
    }

    public Cell defaults() {
        return this.cellDefaults;
    }

    public Table pad(Value pad) {
        if (pad == null) {
            throw new IllegalArgumentException("pad cannot be null.");
        }
        this.padTop = pad;
        this.padLeft = pad;
        this.padBottom = pad;
        this.padRight = pad;
        this.sizeInvalid = true;
        return this;
    }

    public Table pad(Value top, Value left, Value bottom, Value right) {
        if (top == null) {
            throw new IllegalArgumentException("top cannot be null.");
        } else if (left == null) {
            throw new IllegalArgumentException("left cannot be null.");
        } else if (bottom == null) {
            throw new IllegalArgumentException("bottom cannot be null.");
        } else if (right == null) {
            throw new IllegalArgumentException("right cannot be null.");
        } else {
            this.padTop = top;
            this.padLeft = left;
            this.padBottom = bottom;
            this.padRight = right;
            this.sizeInvalid = true;
            return this;
        }
    }

    public Table padTop(Value padTop2) {
        if (padTop2 == null) {
            throw new IllegalArgumentException("padTop cannot be null.");
        }
        this.padTop = padTop2;
        this.sizeInvalid = true;
        return this;
    }

    public Table padLeft(Value padLeft2) {
        if (padLeft2 == null) {
            throw new IllegalArgumentException("padLeft cannot be null.");
        }
        this.padLeft = padLeft2;
        this.sizeInvalid = true;
        return this;
    }

    public Table padBottom(Value padBottom2) {
        if (padBottom2 == null) {
            throw new IllegalArgumentException("padBottom cannot be null.");
        }
        this.padBottom = padBottom2;
        this.sizeInvalid = true;
        return this;
    }

    public Table padRight(Value padRight2) {
        if (padRight2 == null) {
            throw new IllegalArgumentException("padRight cannot be null.");
        }
        this.padRight = padRight2;
        this.sizeInvalid = true;
        return this;
    }

    public Table pad(float pad) {
        pad(new Value.Fixed(pad));
        return this;
    }

    public Table pad(float top, float left, float bottom, float right) {
        this.padTop = new Value.Fixed(top);
        this.padLeft = new Value.Fixed(left);
        this.padBottom = new Value.Fixed(bottom);
        this.padRight = new Value.Fixed(right);
        this.sizeInvalid = true;
        return this;
    }

    public Table padTop(float padTop2) {
        this.padTop = new Value.Fixed(padTop2);
        this.sizeInvalid = true;
        return this;
    }

    public Table padLeft(float padLeft2) {
        this.padLeft = new Value.Fixed(padLeft2);
        this.sizeInvalid = true;
        return this;
    }

    public Table padBottom(float padBottom2) {
        this.padBottom = new Value.Fixed(padBottom2);
        this.sizeInvalid = true;
        return this;
    }

    public Table padRight(float padRight2) {
        this.padRight = new Value.Fixed(padRight2);
        this.sizeInvalid = true;
        return this;
    }

    public Table align(int align2) {
        this.align = align2;
        return this;
    }

    public Table center() {
        this.align = 1;
        return this;
    }

    public Table top() {
        this.align |= 2;
        this.align &= -5;
        return this;
    }

    public Table left() {
        this.align |= 8;
        this.align &= -17;
        return this;
    }

    public Table bottom() {
        this.align |= 4;
        this.align &= -3;
        return this;
    }

    public Table right() {
        this.align |= 16;
        this.align &= -9;
        return this;
    }

    public void setDebug(boolean enabled) {
        debug(enabled ? Debug.all : Debug.none);
    }

    public Table debug() {
        super.debug();
        return this;
    }

    public Table debugAll() {
        super.debugAll();
        return this;
    }

    public Table debugTable() {
        super.setDebug(true);
        if (this.debug != Debug.table) {
            this.debug = Debug.table;
            invalidate();
        }
        return this;
    }

    public Table debugCell() {
        super.setDebug(true);
        if (this.debug != Debug.cell) {
            this.debug = Debug.cell;
            invalidate();
        }
        return this;
    }

    public Table debugActor() {
        super.setDebug(true);
        if (this.debug != Debug.actor) {
            this.debug = Debug.actor;
            invalidate();
        }
        return this;
    }

    public Table debug(Debug debug2) {
        super.setDebug(debug2 != Debug.none);
        if (this.debug != debug2) {
            this.debug = debug2;
            if (debug2 == Debug.none) {
                clearDebugRects();
            } else {
                invalidate();
            }
        }
        return this;
    }

    public Debug getTableDebug() {
        return this.debug;
    }

    public Value getPadTopValue() {
        return this.padTop;
    }

    public float getPadTop() {
        return this.padTop.get(this);
    }

    public Value getPadLeftValue() {
        return this.padLeft;
    }

    public float getPadLeft() {
        return this.padLeft.get(this);
    }

    public Value getPadBottomValue() {
        return this.padBottom;
    }

    public float getPadBottom() {
        return this.padBottom.get(this);
    }

    public Value getPadRightValue() {
        return this.padRight;
    }

    public float getPadRight() {
        return this.padRight.get(this);
    }

    public float getPadX() {
        return this.padLeft.get(this) + this.padRight.get(this);
    }

    public float getPadY() {
        return this.padTop.get(this) + this.padBottom.get(this);
    }

    public int getAlign() {
        return this.align;
    }

    public int getRow(float y) {
        Array<Cell> cells2 = this.cells;
        int row = 0;
        float y2 = y + getPadTop();
        int i = 0;
        int n = cells2.size;
        if (n == 0) {
            return -1;
        }
        if (n == 1) {
            return 0;
        }
        loop1:
        while (true) {
            int i2 = i;
            while (true) {
                if (i2 >= n) {
                    break loop1;
                }
                i = i2 + 1;
                Cell c = cells2.get(i2);
                if (c.actorY + c.computedPadTop < y2) {
                    break loop1;
                } else if (c.endRow) {
                    row++;
                    i2 = i;
                }
            }
        }
        return row;
    }

    public void setSkin(Skin skin2) {
        this.skin = skin2;
    }

    public void setRound(boolean round2) {
        this.round = round2;
    }

    public int getRows() {
        return this.rows;
    }

    public int getColumns() {
        return this.columns;
    }

    private float[] ensureSize(float[] array, int size) {
        if (array == null || array.length < size) {
            return new float[size];
        }
        int n = array.length;
        for (int i = 0; i < n; i++) {
            array[i] = 0.0f;
        }
        return array;
    }

    public void layout() {
        float width = getWidth();
        float height = getHeight();
        layout(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, width, height);
        Array<Cell> cells2 = this.cells;
        if (this.round) {
            int n = cells2.size;
            for (int i = 0; i < n; i++) {
                Cell c = cells2.get(i);
                float actorWidth = (float) Math.round(c.actorWidth);
                float actorHeight = (float) Math.round(c.actorHeight);
                float actorX = (float) Math.round(c.actorX);
                float actorY = (height - ((float) Math.round(c.actorY))) - actorHeight;
                c.setActorBounds(actorX, actorY, actorWidth, actorHeight);
                Actor actor = c.actor;
                if (actor != null) {
                    actor.setBounds(actorX, actorY, actorWidth, actorHeight);
                }
            }
        } else {
            int n2 = cells2.size;
            for (int i2 = 0; i2 < n2; i2++) {
                Cell c2 = cells2.get(i2);
                float actorHeight2 = c2.actorHeight;
                float actorY2 = (height - c2.actorY) - actorHeight2;
                c2.setActorY(actorY2);
                Actor actor2 = c2.actor;
                if (actor2 != null) {
                    actor2.setBounds(c2.actorX, actorY2, c2.actorWidth, actorHeight2);
                }
            }
        }
        Array<Actor> children = getChildren();
        int n3 = children.size;
        for (int i3 = 0; i3 < n3; i3++) {
            Actor child = children.get(i3);
            if (child instanceof Layout) {
                ((Layout) child).validate();
            }
        }
    }

    private void computeSize() {
        float f;
        float f2;
        this.sizeInvalid = false;
        Array<Cell> cells2 = this.cells;
        int cellCount = cells2.size;
        if (cellCount > 0 && !cells2.peek().endRow) {
            endRow();
        }
        int columns2 = this.columns;
        int rows2 = this.rows;
        float[] columnMinWidth2 = ensureSize(this.columnMinWidth, columns2);
        this.columnMinWidth = columnMinWidth2;
        float[] rowMinHeight2 = ensureSize(this.rowMinHeight, rows2);
        this.rowMinHeight = rowMinHeight2;
        float[] columnPrefWidth2 = ensureSize(this.columnPrefWidth, columns2);
        this.columnPrefWidth = columnPrefWidth2;
        float[] rowPrefHeight2 = ensureSize(this.rowPrefHeight, rows2);
        this.rowPrefHeight = rowPrefHeight2;
        this.columnWidth = ensureSize(this.columnWidth, columns2);
        this.rowHeight = ensureSize(this.rowHeight, rows2);
        float[] expandWidth2 = ensureSize(this.expandWidth, columns2);
        this.expandWidth = expandWidth2;
        float[] expandHeight2 = ensureSize(this.expandHeight, rows2);
        this.expandHeight = expandHeight2;
        float spaceRightLast = Animation.CurveTimeline.LINEAR;
        for (int i = 0; i < cellCount; i++) {
            Cell c = cells2.get(i);
            int column = c.column;
            int row = c.row;
            int colspan = c.colspan.intValue();
            Actor a = c.actor;
            if (c.expandY.intValue() != 0 && expandHeight2[row] == Animation.CurveTimeline.LINEAR) {
                expandHeight2[row] = (float) c.expandY.intValue();
            }
            if (colspan == 1 && c.expandX.intValue() != 0 && expandWidth2[column] == Animation.CurveTimeline.LINEAR) {
                expandWidth2[column] = (float) c.expandX.intValue();
            }
            c.computedPadLeft = (column == 0 ? Animation.CurveTimeline.LINEAR : Math.max((float) Animation.CurveTimeline.LINEAR, c.spaceLeft.get(a) - spaceRightLast)) + c.padLeft.get(a);
            c.computedPadTop = c.padTop.get(a);
            if (c.cellAboveIndex != -1) {
                c.computedPadTop += Math.max((float) Animation.CurveTimeline.LINEAR, c.spaceTop.get(a) - cells2.get(c.cellAboveIndex).spaceBottom.get(a));
            }
            float spaceRight = c.spaceRight.get(a);
            float f3 = c.padRight.get(a);
            if (column + colspan == columns2) {
                f = Animation.CurveTimeline.LINEAR;
            } else {
                f = spaceRight;
            }
            c.computedPadRight = f + f3;
            float f4 = c.padBottom.get(a);
            if (row == rows2 - 1) {
                f2 = Animation.CurveTimeline.LINEAR;
            } else {
                f2 = c.spaceBottom.get(a);
            }
            c.computedPadBottom = f2 + f4;
            spaceRightLast = spaceRight;
            float prefWidth = c.prefWidth.get(a);
            float prefHeight = c.prefHeight.get(a);
            float minWidth = c.minWidth.get(a);
            float minHeight = c.minHeight.get(a);
            float maxWidth = c.maxWidth.get(a);
            float maxHeight = c.maxHeight.get(a);
            if (prefWidth < minWidth) {
                prefWidth = minWidth;
            }
            if (prefHeight < minHeight) {
                prefHeight = minHeight;
            }
            if (maxWidth > Animation.CurveTimeline.LINEAR && prefWidth > maxWidth) {
                prefWidth = maxWidth;
            }
            if (maxHeight > Animation.CurveTimeline.LINEAR && prefHeight > maxHeight) {
                prefHeight = maxHeight;
            }
            if (colspan == 1) {
                float hpadding = c.computedPadLeft + c.computedPadRight;
                columnPrefWidth2[column] = Math.max(columnPrefWidth2[column], prefWidth + hpadding);
                columnMinWidth2[column] = Math.max(columnMinWidth2[column], minWidth + hpadding);
            }
            float vpadding = c.computedPadTop + c.computedPadBottom;
            rowPrefHeight2[row] = Math.max(rowPrefHeight2[row], prefHeight + vpadding);
            rowMinHeight2[row] = Math.max(rowMinHeight2[row], minHeight + vpadding);
        }
        for (int i2 = 0; i2 < cellCount; i2++) {
            Cell c2 = cells2.get(i2);
            int expandX = c2.expandX.intValue();
            if (expandX != 0) {
                int column2 = c2.column;
                int nn = column2 + c2.colspan.intValue();
                int ii = column2;
                while (true) {
                    if (ii < nn) {
                        if (expandWidth2[ii] != Animation.CurveTimeline.LINEAR) {
                            break;
                        }
                        ii++;
                    } else {
                        for (int ii2 = column2; ii2 < nn; ii2++) {
                            expandWidth2[ii2] = (float) expandX;
                        }
                    }
                }
            }
        }
        for (int i3 = 0; i3 < cellCount; i3++) {
            Cell c3 = cells2.get(i3);
            int colspan2 = c3.colspan.intValue();
            if (colspan2 != 1) {
                int column3 = c3.column;
                Actor a2 = c3.actor;
                float minWidth2 = c3.minWidth.get(a2);
                float prefWidth2 = c3.prefWidth.get(a2);
                float maxWidth2 = c3.maxWidth.get(a2);
                if (prefWidth2 < minWidth2) {
                    prefWidth2 = minWidth2;
                }
                if (maxWidth2 > Animation.CurveTimeline.LINEAR && prefWidth2 > maxWidth2) {
                    prefWidth2 = maxWidth2;
                }
                float spannedMinWidth = -(c3.computedPadLeft + c3.computedPadRight);
                float spannedPrefWidth = spannedMinWidth;
                int ii3 = column3;
                int nn2 = ii3 + colspan2;
                while (ii3 < nn2) {
                    spannedMinWidth += columnMinWidth2[ii3];
                    spannedPrefWidth += columnPrefWidth2[ii3];
                    ii3++;
                }
                float totalExpandWidth = Animation.CurveTimeline.LINEAR;
                int ii4 = column3;
                int nn3 = ii4 + colspan2;
                while (ii4 < nn3) {
                    totalExpandWidth += expandWidth2[ii4];
                    ii4++;
                }
                float extraMinWidth = Math.max((float) Animation.CurveTimeline.LINEAR, minWidth2 - spannedMinWidth);
                float extraPrefWidth = Math.max((float) Animation.CurveTimeline.LINEAR, prefWidth2 - spannedPrefWidth);
                int ii5 = column3;
                int nn4 = ii5 + colspan2;
                while (ii5 < nn4) {
                    float ratio = totalExpandWidth == Animation.CurveTimeline.LINEAR ? 1.0f / ((float) colspan2) : expandWidth2[ii5] / totalExpandWidth;
                    columnMinWidth2[ii5] = columnMinWidth2[ii5] + (extraMinWidth * ratio);
                    columnPrefWidth2[ii5] = columnPrefWidth2[ii5] + (extraPrefWidth * ratio);
                    ii5++;
                }
            }
        }
        float uniformMinWidth = Animation.CurveTimeline.LINEAR;
        float uniformMinHeight = Animation.CurveTimeline.LINEAR;
        float uniformPrefWidth = Animation.CurveTimeline.LINEAR;
        float uniformPrefHeight = Animation.CurveTimeline.LINEAR;
        for (int i4 = 0; i4 < cellCount; i4++) {
            Cell c4 = cells2.get(i4);
            if (c4.uniformX == Boolean.TRUE && c4.colspan.intValue() == 1) {
                float hpadding2 = c4.computedPadLeft + c4.computedPadRight;
                uniformMinWidth = Math.max(uniformMinWidth, columnMinWidth2[c4.column] - hpadding2);
                uniformPrefWidth = Math.max(uniformPrefWidth, columnPrefWidth2[c4.column] - hpadding2);
            }
            if (c4.uniformY == Boolean.TRUE) {
                float vpadding2 = c4.computedPadTop + c4.computedPadBottom;
                uniformMinHeight = Math.max(uniformMinHeight, rowMinHeight2[c4.row] - vpadding2);
                uniformPrefHeight = Math.max(uniformPrefHeight, rowPrefHeight2[c4.row] - vpadding2);
            }
        }
        if (uniformPrefWidth > Animation.CurveTimeline.LINEAR || uniformPrefHeight > Animation.CurveTimeline.LINEAR) {
            for (int i5 = 0; i5 < cellCount; i5++) {
                Cell c5 = cells2.get(i5);
                if (uniformPrefWidth > Animation.CurveTimeline.LINEAR && c5.uniformX == Boolean.TRUE && c5.colspan.intValue() == 1) {
                    float hpadding3 = c5.computedPadLeft + c5.computedPadRight;
                    columnMinWidth2[c5.column] = uniformMinWidth + hpadding3;
                    columnPrefWidth2[c5.column] = uniformPrefWidth + hpadding3;
                }
                if (uniformPrefHeight > Animation.CurveTimeline.LINEAR && c5.uniformY == Boolean.TRUE) {
                    float vpadding3 = c5.computedPadTop + c5.computedPadBottom;
                    rowMinHeight2[c5.row] = uniformMinHeight + vpadding3;
                    rowPrefHeight2[c5.row] = uniformPrefHeight + vpadding3;
                }
            }
        }
        this.tableMinWidth = Animation.CurveTimeline.LINEAR;
        this.tableMinHeight = Animation.CurveTimeline.LINEAR;
        this.tablePrefWidth = Animation.CurveTimeline.LINEAR;
        this.tablePrefHeight = Animation.CurveTimeline.LINEAR;
        for (int i6 = 0; i6 < columns2; i6++) {
            this.tableMinWidth = this.tableMinWidth + columnMinWidth2[i6];
            this.tablePrefWidth = this.tablePrefWidth + columnPrefWidth2[i6];
        }
        for (int i7 = 0; i7 < rows2; i7++) {
            this.tableMinHeight = this.tableMinHeight + rowMinHeight2[i7];
            this.tablePrefHeight = this.tablePrefHeight + Math.max(rowMinHeight2[i7], rowPrefHeight2[i7]);
        }
        float hpadding4 = this.padLeft.get(this) + this.padRight.get(this);
        float vpadding4 = this.padTop.get(this) + this.padBottom.get(this);
        this.tableMinWidth = this.tableMinWidth + hpadding4;
        this.tableMinHeight = this.tableMinHeight + vpadding4;
        this.tablePrefWidth = Math.max(this.tablePrefWidth + hpadding4, this.tableMinWidth);
        this.tablePrefHeight = Math.max(this.tablePrefHeight + vpadding4, this.tableMinHeight);
    }

    private void layout(float layoutX, float layoutY, float layoutWidth, float layoutHeight) {
        float[] columnWeightedWidth2;
        float[] rowWeightedHeight2;
        float currentX;
        Array<Cell> cells2 = this.cells;
        int cellCount = cells2.size;
        if (this.sizeInvalid) {
            computeSize();
        }
        float padLeft2 = this.padLeft.get(this);
        float hpadding = padLeft2 + this.padRight.get(this);
        float padTop2 = this.padTop.get(this);
        float vpadding = padTop2 + this.padBottom.get(this);
        int columns2 = this.columns;
        int rows2 = this.rows;
        float[] expandWidth2 = this.expandWidth;
        float[] expandHeight2 = this.expandHeight;
        float[] columnWidth2 = this.columnWidth;
        float[] rowHeight2 = this.rowHeight;
        float totalExpandWidth = Animation.CurveTimeline.LINEAR;
        float totalExpandHeight = Animation.CurveTimeline.LINEAR;
        for (int i = 0; i < columns2; i++) {
            totalExpandWidth += expandWidth2[i];
        }
        for (int i2 = 0; i2 < rows2; i2++) {
            totalExpandHeight += expandHeight2[i2];
        }
        float totalGrowWidth = this.tablePrefWidth - this.tableMinWidth;
        if (totalGrowWidth == Animation.CurveTimeline.LINEAR) {
            columnWeightedWidth2 = this.columnMinWidth;
        } else {
            float extraWidth = Math.min(totalGrowWidth, Math.max((float) Animation.CurveTimeline.LINEAR, layoutWidth - this.tableMinWidth));
            columnWeightedWidth2 = ensureSize(columnWeightedWidth, columns2);
            columnWeightedWidth = columnWeightedWidth2;
            float[] columnMinWidth2 = this.columnMinWidth;
            float[] columnPrefWidth2 = this.columnPrefWidth;
            for (int i3 = 0; i3 < columns2; i3++) {
                columnWeightedWidth2[i3] = columnMinWidth2[i3] + (extraWidth * ((columnPrefWidth2[i3] - columnMinWidth2[i3]) / totalGrowWidth));
            }
        }
        float totalGrowHeight = this.tablePrefHeight - this.tableMinHeight;
        if (totalGrowHeight == Animation.CurveTimeline.LINEAR) {
            rowWeightedHeight2 = this.rowMinHeight;
        } else {
            rowWeightedHeight2 = ensureSize(rowWeightedHeight, rows2);
            rowWeightedHeight = rowWeightedHeight2;
            float extraHeight = Math.min(totalGrowHeight, Math.max((float) Animation.CurveTimeline.LINEAR, layoutHeight - this.tableMinHeight));
            float[] rowMinHeight2 = this.rowMinHeight;
            float[] rowPrefHeight2 = this.rowPrefHeight;
            for (int i4 = 0; i4 < rows2; i4++) {
                rowWeightedHeight2[i4] = rowMinHeight2[i4] + (extraHeight * ((rowPrefHeight2[i4] - rowMinHeight2[i4]) / totalGrowHeight));
            }
        }
        for (int i5 = 0; i5 < cellCount; i5++) {
            Cell c = cells2.get(i5);
            int column = c.column;
            int row = c.row;
            Actor a = c.actor;
            float spannedWeightedWidth = Animation.CurveTimeline.LINEAR;
            int colspan = c.colspan.intValue();
            int ii = column;
            int nn = ii + colspan;
            while (ii < nn) {
                spannedWeightedWidth += columnWeightedWidth2[ii];
                ii++;
            }
            float weightedHeight = rowWeightedHeight2[row];
            float prefWidth = c.prefWidth.get(a);
            float prefHeight = c.prefHeight.get(a);
            float minWidth = c.minWidth.get(a);
            float minHeight = c.minHeight.get(a);
            float maxWidth = c.maxWidth.get(a);
            float maxHeight = c.maxHeight.get(a);
            if (prefWidth < minWidth) {
                prefWidth = minWidth;
            }
            if (prefHeight < minHeight) {
                prefHeight = minHeight;
            }
            if (maxWidth > Animation.CurveTimeline.LINEAR && prefWidth > maxWidth) {
                prefWidth = maxWidth;
            }
            if (maxHeight > Animation.CurveTimeline.LINEAR && prefHeight > maxHeight) {
                prefHeight = maxHeight;
            }
            c.actorWidth = Math.min((spannedWeightedWidth - c.computedPadLeft) - c.computedPadRight, prefWidth);
            c.actorHeight = Math.min((weightedHeight - c.computedPadTop) - c.computedPadBottom, prefHeight);
            if (colspan == 1) {
                columnWidth2[column] = Math.max(columnWidth2[column], spannedWeightedWidth);
            }
            rowHeight2[row] = Math.max(rowHeight2[row], weightedHeight);
        }
        if (totalExpandWidth > Animation.CurveTimeline.LINEAR) {
            float extra = layoutWidth - hpadding;
            for (int i6 = 0; i6 < columns2; i6++) {
                extra -= columnWidth2[i6];
            }
            float used = Animation.CurveTimeline.LINEAR;
            int lastIndex = 0;
            for (int i7 = 0; i7 < columns2; i7++) {
                if (expandWidth2[i7] != Animation.CurveTimeline.LINEAR) {
                    float amount = (expandWidth2[i7] * extra) / totalExpandWidth;
                    columnWidth2[i7] = columnWidth2[i7] + amount;
                    used += amount;
                    lastIndex = i7;
                }
            }
            columnWidth2[lastIndex] = columnWidth2[lastIndex] + (extra - used);
        }
        if (totalExpandHeight > Animation.CurveTimeline.LINEAR) {
            float extra2 = layoutHeight - vpadding;
            for (int i8 = 0; i8 < rows2; i8++) {
                extra2 -= rowHeight2[i8];
            }
            float used2 = Animation.CurveTimeline.LINEAR;
            int lastIndex2 = 0;
            for (int i9 = 0; i9 < rows2; i9++) {
                if (expandHeight2[i9] != Animation.CurveTimeline.LINEAR) {
                    float amount2 = (expandHeight2[i9] * extra2) / totalExpandHeight;
                    rowHeight2[i9] = rowHeight2[i9] + amount2;
                    used2 += amount2;
                    lastIndex2 = i9;
                }
            }
            rowHeight2[lastIndex2] = rowHeight2[lastIndex2] + (extra2 - used2);
        }
        for (int i10 = 0; i10 < cellCount; i10++) {
            Cell c2 = cells2.get(i10);
            int colspan2 = c2.colspan.intValue();
            if (colspan2 != 1) {
                float extraWidth2 = Animation.CurveTimeline.LINEAR;
                int column2 = c2.column;
                int nn2 = column2 + colspan2;
                while (column2 < nn2) {
                    extraWidth2 += columnWeightedWidth2[column2] - columnWidth2[column2];
                    column2++;
                }
                float extraWidth3 = (extraWidth2 - Math.max((float) Animation.CurveTimeline.LINEAR, c2.computedPadLeft + c2.computedPadRight)) / ((float) colspan2);
                if (extraWidth3 > Animation.CurveTimeline.LINEAR) {
                    int column3 = c2.column;
                    int nn3 = column3 + colspan2;
                    while (column3 < nn3) {
                        columnWidth2[column3] = columnWidth2[column3] + extraWidth3;
                        column3++;
                    }
                }
            }
        }
        float tableWidth = hpadding;
        float tableHeight = vpadding;
        for (int i11 = 0; i11 < columns2; i11++) {
            tableWidth += columnWidth2[i11];
        }
        for (int i12 = 0; i12 < rows2; i12++) {
            tableHeight += rowHeight2[i12];
        }
        int align2 = this.align;
        float x = layoutX + padLeft2;
        if ((align2 & 16) != 0) {
            x += layoutWidth - tableWidth;
        } else if ((align2 & 8) == 0) {
            x += (layoutWidth - tableWidth) / 2.0f;
        }
        float y = layoutY + padTop2;
        if ((align2 & 4) != 0) {
            y += layoutHeight - tableHeight;
        } else if ((align2 & 2) == 0) {
            y += (layoutHeight - tableHeight) / 2.0f;
        }
        float currentX2 = x;
        float currentY = y;
        for (int i13 = 0; i13 < cellCount; i13++) {
            Cell c3 = cells2.get(i13);
            float spannedCellWidth = Animation.CurveTimeline.LINEAR;
            int column4 = c3.column;
            int nn4 = column4 + c3.colspan.intValue();
            while (column4 < nn4) {
                spannedCellWidth += columnWidth2[column4];
                column4++;
            }
            float spannedCellWidth2 = spannedCellWidth - (c3.computedPadLeft + c3.computedPadRight);
            float currentX3 = currentX2 + c3.computedPadLeft;
            float fillX = c3.fillX.floatValue();
            float fillY = c3.fillY.floatValue();
            if (fillX > Animation.CurveTimeline.LINEAR) {
                c3.actorWidth = Math.max(spannedCellWidth2 * fillX, c3.minWidth.get(c3.actor));
                float maxWidth2 = c3.maxWidth.get(c3.actor);
                if (maxWidth2 > Animation.CurveTimeline.LINEAR) {
                    c3.actorWidth = Math.min(c3.actorWidth, maxWidth2);
                }
            }
            if (fillY > Animation.CurveTimeline.LINEAR) {
                c3.actorHeight = Math.max(((rowHeight2[c3.row] * fillY) - c3.computedPadTop) - c3.computedPadBottom, c3.minHeight.get(c3.actor));
                float maxHeight2 = c3.maxHeight.get(c3.actor);
                if (maxHeight2 > Animation.CurveTimeline.LINEAR) {
                    c3.actorHeight = Math.min(c3.actorHeight, maxHeight2);
                }
            }
            int align3 = c3.align.intValue();
            if ((align3 & 8) != 0) {
                c3.actorX = currentX3;
            } else if ((align3 & 16) != 0) {
                c3.actorX = (currentX3 + spannedCellWidth2) - c3.actorWidth;
            } else {
                c3.actorX = ((spannedCellWidth2 - c3.actorWidth) / 2.0f) + currentX3;
            }
            if ((align3 & 2) != 0) {
                c3.actorY = c3.computedPadTop + currentY;
            } else if ((align3 & 4) != 0) {
                c3.actorY = ((rowHeight2[c3.row] + currentY) - c3.actorHeight) - c3.computedPadBottom;
            } else {
                c3.actorY = ((((rowHeight2[c3.row] - c3.actorHeight) + c3.computedPadTop) - c3.computedPadBottom) / 2.0f) + currentY;
            }
            if (c3.endRow) {
                currentX2 = x;
                currentY += rowHeight2[c3.row];
            } else {
                currentX2 = currentX3 + c3.computedPadRight + spannedCellWidth2;
            }
        }
        if (this.debug != Debug.none) {
            clearDebugRects();
            float currentX4 = x;
            float currentY2 = y;
            if (this.debug == Debug.table || this.debug == Debug.all) {
                addDebugRect(layoutX, layoutY, layoutWidth, layoutHeight, debugTableColor);
                addDebugRect(x, y, tableWidth - hpadding, tableHeight - vpadding, debugTableColor);
            }
            int i14 = 0;
            while (i14 < cellCount) {
                Cell c4 = cells2.get(i14);
                if (this.debug == Debug.actor || this.debug == Debug.all) {
                    addDebugRect(c4.actorX, c4.actorY, c4.actorWidth, c4.actorHeight, debugActorColor);
                }
                float spannedCellWidth3 = Animation.CurveTimeline.LINEAR;
                int column5 = c4.column;
                int nn5 = column5 + c4.colspan.intValue();
                while (column5 < nn5) {
                    spannedCellWidth3 += columnWidth2[column5];
                    column5++;
                }
                float spannedCellWidth4 = spannedCellWidth3 - (c4.computedPadLeft + c4.computedPadRight);
                float currentX5 = currentX4 + c4.computedPadLeft;
                if (this.debug == Debug.cell || this.debug == Debug.all) {
                    addDebugRect(currentX5, currentY2 + c4.computedPadTop, spannedCellWidth4, (rowHeight2[c4.row] - c4.computedPadTop) - c4.computedPadBottom, debugCellColor);
                }
                if (c4.endRow) {
                    currentX = x;
                    currentY2 += rowHeight2[c4.row];
                } else {
                    currentX = currentX5 + c4.computedPadRight + spannedCellWidth4;
                }
                i14++;
                currentX4 = currentX;
            }
        }
    }

    private void clearDebugRects() {
        if (this.debugRects != null) {
            DebugRect.pool.freeAll(this.debugRects);
            this.debugRects.clear();
        }
    }

    private void addDebugRect(float x, float y, float w, float h, Color color) {
        if (this.debugRects == null) {
            this.debugRects = new Array<>();
        }
        DebugRect rect = DebugRect.pool.obtain();
        rect.color = color;
        rect.set(x, (getHeight() - y) - h, w, h);
        this.debugRects.add(rect);
    }

    public void drawDebug(ShapeRenderer shapes) {
        if (isTransform()) {
            applyTransform(shapes, computeTransform());
            drawDebugRects(shapes);
            if (this.clip) {
                shapes.flush();
                float x = Animation.CurveTimeline.LINEAR;
                float y = Animation.CurveTimeline.LINEAR;
                float width = getWidth();
                float height = getHeight();
                if (this.background != null) {
                    x = this.padLeft.get(this);
                    y = this.padBottom.get(this);
                    width -= this.padRight.get(this) + x;
                    height -= this.padTop.get(this) + y;
                }
                if (clipBegin(x, y, width, height)) {
                    drawDebugChildren(shapes);
                    clipEnd();
                }
            } else {
                drawDebugChildren(shapes);
            }
            resetTransform(shapes);
            return;
        }
        drawDebugRects(shapes);
        super.drawDebug(shapes);
    }

    /* access modifiers changed from: protected */
    public void drawDebugBounds(ShapeRenderer shapes) {
    }

    private void drawDebugRects(ShapeRenderer shapes) {
        if (this.debugRects != null && getDebug()) {
            shapes.set(ShapeRenderer.ShapeType.Line);
            shapes.setColor(getStage().getDebugColor());
            float x = Animation.CurveTimeline.LINEAR;
            float y = Animation.CurveTimeline.LINEAR;
            if (!isTransform()) {
                x = getX();
                y = getY();
            }
            int n = this.debugRects.size;
            for (int i = 0; i < n; i++) {
                DebugRect debugRect = this.debugRects.get(i);
                shapes.setColor(debugRect.color);
                shapes.rect(debugRect.x + x, debugRect.y + y, debugRect.width, debugRect.height);
            }
        }
    }

    public Skin getSkin() {
        return this.skin;
    }
}
