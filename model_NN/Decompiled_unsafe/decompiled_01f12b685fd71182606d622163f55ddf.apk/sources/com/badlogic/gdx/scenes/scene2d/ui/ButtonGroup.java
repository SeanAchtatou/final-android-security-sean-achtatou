package com.badlogic.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Array;

public class ButtonGroup<T extends Button> {
    private final Array<T> buttons = new Array<>();
    private Array<T> checkedButtons = new Array<>(1);
    private T lastChecked;
    private int maxCheckCount = 1;
    private int minCheckCount = 1;
    private boolean uncheckLast = true;

    public ButtonGroup() {
    }

    public ButtonGroup(T... buttons2) {
        add(buttons2);
        this.minCheckCount = 1;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void add(T r5) {
        /*
            r4 = this;
            r1 = 0
            if (r5 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "button cannot be null."
            r1.<init>(r2)
            throw r1
        L_0x000b:
            r2 = 0
            r5.buttonGroup = r2
            boolean r2 = r5.isChecked()
            if (r2 != 0) goto L_0x002b
            com.badlogic.gdx.utils.Array<T> r2 = r4.buttons
            int r2 = r2.size
            int r3 = r4.minCheckCount
            if (r2 < r3) goto L_0x002b
            r0 = r1
        L_0x001d:
            r5.setChecked(r1)
            r5.buttonGroup = r4
            com.badlogic.gdx.utils.Array<T> r1 = r4.buttons
            r1.add(r5)
            r5.setChecked(r0)
            return
        L_0x002b:
            r0 = 1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup.add(com.badlogic.gdx.scenes.scene2d.ui.Button):void");
    }

    public void add(T... buttons2) {
        if (buttons2 == null) {
            throw new IllegalArgumentException("buttons cannot be null.");
        }
        for (T add : buttons2) {
            add(add);
        }
    }

    public void remove(T button) {
        if (button == null) {
            throw new IllegalArgumentException("button cannot be null.");
        }
        button.buttonGroup = null;
        this.buttons.removeValue(button, true);
        this.checkedButtons.removeValue(button, true);
    }

    public void remove(T... buttons2) {
        if (buttons2 == null) {
            throw new IllegalArgumentException("buttons cannot be null.");
        }
        for (T remove : buttons2) {
            remove(remove);
        }
    }

    public void clear() {
        this.buttons.clear();
        this.checkedButtons.clear();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void setChecked(java.lang.String r6) {
        /*
            r5 = this;
            if (r6 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "text cannot be null."
            r3.<init>(r4)
            throw r3
        L_0x000a:
            r1 = 0
            com.badlogic.gdx.utils.Array<T> r3 = r5.buttons
            int r2 = r3.size
        L_0x000f:
            if (r1 < r2) goto L_0x0012
        L_0x0011:
            return
        L_0x0012:
            com.badlogic.gdx.utils.Array<T> r3 = r5.buttons
            java.lang.Object r0 = r3.get(r1)
            com.badlogic.gdx.scenes.scene2d.ui.Button r0 = (com.badlogic.gdx.scenes.scene2d.ui.Button) r0
            boolean r3 = r0 instanceof com.badlogic.gdx.scenes.scene2d.ui.TextButton
            if (r3 == 0) goto L_0x0030
            r3 = r0
            com.badlogic.gdx.scenes.scene2d.ui.TextButton r3 = (com.badlogic.gdx.scenes.scene2d.ui.TextButton) r3
            java.lang.CharSequence r3 = r3.getText()
            boolean r3 = r6.contentEquals(r3)
            if (r3 == 0) goto L_0x0030
            r3 = 1
            r0.setChecked(r3)
            goto L_0x0011
        L_0x0030:
            int r1 = r1 + 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup.setChecked(java.lang.String):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected boolean canCheck(T r6, boolean r7) {
        /*
            r5 = this;
            r2 = 1
            r1 = 0
            boolean r3 = r6.isChecked
            if (r3 != r7) goto L_0x0007
        L_0x0006:
            return r1
        L_0x0007:
            if (r7 != 0) goto L_0x0018
            com.badlogic.gdx.utils.Array<T> r3 = r5.checkedButtons
            int r3 = r3.size
            int r4 = r5.minCheckCount
            if (r3 <= r4) goto L_0x0006
            com.badlogic.gdx.utils.Array<T> r1 = r5.checkedButtons
            r1.removeValue(r6, r2)
        L_0x0016:
            r1 = r2
            goto L_0x0006
        L_0x0018:
            int r3 = r5.maxCheckCount
            r4 = -1
            if (r3 == r4) goto L_0x0034
            com.badlogic.gdx.utils.Array<T> r3 = r5.checkedButtons
            int r3 = r3.size
            int r4 = r5.maxCheckCount
            if (r3 < r4) goto L_0x0034
            boolean r3 = r5.uncheckLast
            if (r3 == 0) goto L_0x0006
            int r0 = r5.minCheckCount
            r5.minCheckCount = r1
            T r3 = r5.lastChecked
            r3.setChecked(r1)
            r5.minCheckCount = r0
        L_0x0034:
            com.badlogic.gdx.utils.Array<T> r1 = r5.checkedButtons
            r1.add(r6)
            r5.lastChecked = r6
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup.canCheck(com.badlogic.gdx.scenes.scene2d.ui.Button, boolean):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void uncheckAll() {
        /*
            r6 = this;
            r5 = 0
            int r3 = r6.minCheckCount
            r6.minCheckCount = r5
            r1 = 0
            com.badlogic.gdx.utils.Array<T> r4 = r6.buttons
            int r2 = r4.size
        L_0x000a:
            if (r1 < r2) goto L_0x000f
            r6.minCheckCount = r3
            return
        L_0x000f:
            com.badlogic.gdx.utils.Array<T> r4 = r6.buttons
            java.lang.Object r0 = r4.get(r1)
            com.badlogic.gdx.scenes.scene2d.ui.Button r0 = (com.badlogic.gdx.scenes.scene2d.ui.Button) r0
            r0.setChecked(r5)
            int r1 = r1 + 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup.uncheckAll():void");
    }

    public T getChecked() {
        if (this.checkedButtons.size > 0) {
            return (Button) this.checkedButtons.get(0);
        }
        return null;
    }

    public int getCheckedIndex() {
        if (this.checkedButtons.size > 0) {
            return this.buttons.indexOf((Button) this.checkedButtons.get(0), true);
        }
        return -1;
    }

    public Array<T> getAllChecked() {
        return this.checkedButtons;
    }

    public Array<T> getButtons() {
        return this.buttons;
    }

    public void setMinCheckCount(int minCheckCount2) {
        this.minCheckCount = minCheckCount2;
    }

    public void setMaxCheckCount(int maxCheckCount2) {
        if (maxCheckCount2 == 0) {
            maxCheckCount2 = -1;
        }
        this.maxCheckCount = maxCheckCount2;
    }

    public void setUncheckLast(boolean uncheckLast2) {
        this.uncheckLast = uncheckLast2;
    }
}
