package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;

public class Actor extends HaopuGame {
    private final Array<Action> actions = new Array<>(0);
    private final DelayedRemovalArray<EventListener> captureListeners = new DelayedRemovalArray<>(0);
    final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private boolean debug;
    float height;
    private final DelayedRemovalArray<EventListener> listeners = new DelayedRemovalArray<>(0);
    private String name;
    float originX;
    float originY;
    Group parent;
    float rotation;
    float scaleX = 1.0f;
    float scaleY = 1.0f;
    private Stage stage;
    private Touchable touchable = Touchable.enabled;
    private Object userObject;
    private boolean visible = true;
    float width;
    float x;
    float y;

    public void draw(Batch batch, float parentAlpha) {
    }

    public void act(float delta) {
        if (!isPause()) {
            Array<Action> actions2 = this.actions;
            if (actions2.size > 0) {
                if (this.stage != null && this.stage.getActionsRequestRendering()) {
                    Gdx.graphics.requestRendering();
                }
                int i = 0;
                while (i < actions2.size) {
                    Action action = actions2.get(i);
                    if (action.act(delta) && i < actions2.size) {
                        int actionIndex = actions2.get(i) == action ? i : actions2.indexOf(action, true);
                        if (actionIndex != -1) {
                            actions2.removeIndex(actionIndex);
                            action.setActor(null);
                            i--;
                        }
                    }
                    i++;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002e, code lost:
        r6 = r9.isCancelled();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        r0.clear();
        com.badlogic.gdx.utils.Pools.free(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        notify(r9, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0063, code lost:
        if (r9.getBubbles() != false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0065, code lost:
        r6 = r9.isCancelled();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0068, code lost:
        r0.clear();
        com.badlogic.gdx.utils.Pools.free(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
        if (r9.isStopped() == false) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0076, code lost:
        r6 = r9.isCancelled();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0079, code lost:
        r0.clear();
        com.badlogic.gdx.utils.Pools.free(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0081, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r4 = r0.size;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0084, code lost:
        if (r3 < r4) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0086, code lost:
        r6 = r9.isCancelled();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0089, code lost:
        r0.clear();
        com.badlogic.gdx.utils.Pools.free(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        ((com.badlogic.gdx.scenes.scene2d.Group) r1[r3]).notify(r9, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009d, code lost:
        if (r9.isStopped() == false) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009f, code lost:
        r6 = r9.isCancelled();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00a2, code lost:
        r0.clear();
        com.badlogic.gdx.utils.Pools.free(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00aa, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
        notify(r9, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (r9.isStopped() == false) goto L_0x005b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean fire(com.badlogic.gdx.scenes.scene2d.Event r9) {
        /*
            r8 = this;
            com.badlogic.gdx.scenes.scene2d.Stage r6 = r9.getStage()
            if (r6 != 0) goto L_0x000d
            com.badlogic.gdx.scenes.scene2d.Stage r6 = r8.getStage()
            r9.setStage(r6)
        L_0x000d:
            r9.setTarget(r8)
            java.lang.Class<com.badlogic.gdx.utils.Array> r6 = com.badlogic.gdx.utils.Array.class
            java.lang.Object r0 = com.badlogic.gdx.utils.Pools.obtain(r6)
            com.badlogic.gdx.utils.Array r0 = (com.badlogic.gdx.utils.Array) r0
            com.badlogic.gdx.scenes.scene2d.Group r5 = r8.parent
        L_0x001a:
            if (r5 != 0) goto L_0x0039
            T[] r1 = r0.items     // Catch:{ all -> 0x00ad }
            int r6 = r0.size     // Catch:{ all -> 0x00ad }
            int r3 = r6 + -1
        L_0x0022:
            if (r3 >= 0) goto L_0x003f
            r6 = 1
            r8.notify(r9, r6)     // Catch:{ all -> 0x00ad }
            boolean r6 = r9.isStopped()     // Catch:{ all -> 0x00ad }
            if (r6 == 0) goto L_0x005b
            boolean r6 = r9.isCancelled()     // Catch:{ all -> 0x00ad }
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
        L_0x0038:
            return r6
        L_0x0039:
            r0.add(r5)
            com.badlogic.gdx.scenes.scene2d.Group r5 = r5.parent
            goto L_0x001a
        L_0x003f:
            r2 = r1[r3]     // Catch:{ all -> 0x00ad }
            com.badlogic.gdx.scenes.scene2d.Group r2 = (com.badlogic.gdx.scenes.scene2d.Group) r2     // Catch:{ all -> 0x00ad }
            r6 = 1
            r2.notify(r9, r6)     // Catch:{ all -> 0x00ad }
            boolean r6 = r9.isStopped()     // Catch:{ all -> 0x00ad }
            if (r6 == 0) goto L_0x0058
            boolean r6 = r9.isCancelled()     // Catch:{ all -> 0x00ad }
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
            goto L_0x0038
        L_0x0058:
            int r3 = r3 + -1
            goto L_0x0022
        L_0x005b:
            r6 = 0
            r8.notify(r9, r6)     // Catch:{ all -> 0x00ad }
            boolean r6 = r9.getBubbles()     // Catch:{ all -> 0x00ad }
            if (r6 != 0) goto L_0x0070
            boolean r6 = r9.isCancelled()     // Catch:{ all -> 0x00ad }
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
            goto L_0x0038
        L_0x0070:
            boolean r6 = r9.isStopped()     // Catch:{ all -> 0x00ad }
            if (r6 == 0) goto L_0x0081
            boolean r6 = r9.isCancelled()     // Catch:{ all -> 0x00ad }
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
            goto L_0x0038
        L_0x0081:
            r3 = 0
            int r4 = r0.size     // Catch:{ all -> 0x00ad }
        L_0x0084:
            if (r3 < r4) goto L_0x0091
            boolean r6 = r9.isCancelled()     // Catch:{ all -> 0x00ad }
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
            goto L_0x0038
        L_0x0091:
            r6 = r1[r3]     // Catch:{ all -> 0x00ad }
            com.badlogic.gdx.scenes.scene2d.Group r6 = (com.badlogic.gdx.scenes.scene2d.Group) r6     // Catch:{ all -> 0x00ad }
            r7 = 0
            r6.notify(r9, r7)     // Catch:{ all -> 0x00ad }
            boolean r6 = r9.isStopped()     // Catch:{ all -> 0x00ad }
            if (r6 == 0) goto L_0x00aa
            boolean r6 = r9.isCancelled()     // Catch:{ all -> 0x00ad }
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
            goto L_0x0038
        L_0x00aa:
            int r3 = r3 + 1
            goto L_0x0084
        L_0x00ad:
            r6 = move-exception
            r0.clear()
            com.badlogic.gdx.utils.Pools.free(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.scenes.scene2d.Actor.fire(com.badlogic.gdx.scenes.scene2d.Event):boolean");
    }

    public boolean notify(Event event, boolean capture) {
        if (event.getTarget() == null) {
            throw new IllegalArgumentException("The event target cannot be null.");
        }
        DelayedRemovalArray<EventListener> listeners2 = capture ? this.captureListeners : this.listeners;
        if (listeners2.size == 0) {
            return event.isCancelled();
        }
        event.setListenerActor(this);
        event.setCapture(capture);
        if (event.getStage() == null) {
            event.setStage(this.stage);
        }
        listeners2.begin();
        int n = listeners2.size;
        for (int i = 0; i < n; i++) {
            EventListener listener = listeners2.get(i);
            if (listener.handle(event)) {
                event.handle();
                if (event instanceof InputEvent) {
                    InputEvent inputEvent = (InputEvent) event;
                    if (inputEvent.getType() == InputEvent.Type.touchDown) {
                        event.getStage().addTouchFocus(listener, this, inputEvent.getTarget(), inputEvent.getPointer(), inputEvent.getButton());
                    }
                }
            }
        }
        listeners2.end();
        return event.isCancelled();
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public Actor hit(float x2, float y2, boolean touchable2) {
        if (touchable2 && this.touchable != Touchable.enabled) {
            return null;
        }
        if (x2 < Animation.CurveTimeline.LINEAR || x2 >= this.width || y2 < Animation.CurveTimeline.LINEAR || y2 >= this.height) {
            this = null;
        }
        return this;
    }

    public boolean remove() {
        if (this.parent != null) {
            return this.parent.removeActor(this, true);
        }
        return false;
    }

    public boolean addListener(EventListener listener) {
        if (this.listeners.contains(listener, true)) {
            return false;
        }
        this.listeners.add(listener);
        return true;
    }

    public boolean removeListener(EventListener listener) {
        return this.listeners.removeValue(listener, true);
    }

    public Array<EventListener> getListeners() {
        return this.listeners;
    }

    public boolean addCaptureListener(EventListener listener) {
        if (!this.captureListeners.contains(listener, true)) {
            this.captureListeners.add(listener);
        }
        return true;
    }

    public boolean removeCaptureListener(EventListener listener) {
        return this.captureListeners.removeValue(listener, true);
    }

    public Array<EventListener> getCaptureListeners() {
        return this.captureListeners;
    }

    public void addAction(Action action) {
        action.setActor(this);
        this.actions.add(action);
        if (this.stage != null && this.stage.getActionsRequestRendering()) {
            Gdx.graphics.requestRendering();
        }
    }

    public void removeAction(Action action) {
        if (this.actions.removeValue(action, true)) {
            action.setActor(null);
        }
    }

    public Array<Action> getActions() {
        return this.actions;
    }

    public void clearActions() {
        for (int i = this.actions.size - 1; i >= 0; i--) {
            this.actions.get(i).setActor(null);
        }
        this.actions.clear();
    }

    public void clearListeners() {
        this.listeners.clear();
        this.captureListeners.clear();
    }

    public void clear() {
        clearActions();
        clearListeners();
    }

    public Stage getStage() {
        return this.stage;
    }

    /* access modifiers changed from: protected */
    public void setStage(Stage stage2) {
        this.stage = stage2;
    }

    public boolean isDescendantOf(Actor actor) {
        if (actor == null) {
            throw new IllegalArgumentException("actor cannot be null.");
        }
        for (Actor parent2 = this; parent2 != null; parent2 = parent2.parent) {
            if (parent2 == actor) {
                return true;
            }
        }
        return false;
    }

    public boolean isAscendantOf(Actor actor) {
        if (actor == null) {
            throw new IllegalArgumentException("actor cannot be null.");
        }
        while (actor != null) {
            if (actor == this) {
                return true;
            }
            actor = actor.parent;
        }
        return false;
    }

    public boolean hasParent() {
        return this.parent != null;
    }

    public Group getParent() {
        return this.parent;
    }

    /* access modifiers changed from: protected */
    public void setParent(Group parent2) {
        this.parent = parent2;
    }

    public boolean isTouchable() {
        return this.touchable == Touchable.enabled;
    }

    public Touchable getTouchable() {
        return this.touchable;
    }

    public void setTouchable(Touchable touchable2) {
        this.touchable = touchable2;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible2) {
        this.visible = visible2;
    }

    public Object getUserObject() {
        return this.userObject;
    }

    public void setUserObject(Object userObject2) {
        this.userObject = userObject2;
    }

    public float getX() {
        return this.x;
    }

    public float getX(int alignment) {
        float x2 = this.x;
        if ((alignment & 16) != 0) {
            return x2 + this.width;
        }
        if ((alignment & 8) == 0) {
            return x2 + (this.width / 2.0f);
        }
        return x2;
    }

    public void setX(float x2) {
        if (this.x != x2) {
            this.x = x2;
            positionChanged();
        }
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        if (this.y != y2) {
            this.y = y2;
            positionChanged();
        }
    }

    public float getY(int alignment) {
        float y2 = this.y;
        if ((alignment & 2) != 0) {
            return y2 + this.height;
        }
        if ((alignment & 4) == 0) {
            return y2 + (this.height / 2.0f);
        }
        return y2;
    }

    public void setPosition(float x2, float y2) {
        if (this.x != x2 || this.y != y2) {
            this.x = x2;
            this.y = y2;
            positionChanged();
        }
    }

    public void setPosition(float x2, float y2, int alignment) {
        if ((alignment & 16) != 0) {
            x2 -= this.width;
        } else if ((alignment & 8) == 0) {
            x2 -= this.width / 2.0f;
        }
        if ((alignment & 2) != 0) {
            y2 -= this.height;
        } else if ((alignment & 4) == 0) {
            y2 -= this.height / 2.0f;
        }
        if (this.x != x2 || this.y != y2) {
            this.x = x2;
            this.y = y2;
            positionChanged();
        }
    }

    public void moveBy(float x2, float y2) {
        if (x2 != Animation.CurveTimeline.LINEAR || y2 != Animation.CurveTimeline.LINEAR) {
            this.x += x2;
            this.y += y2;
            positionChanged();
        }
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width2) {
        float oldWidth = this.width;
        this.width = width2;
        if (width2 != oldWidth) {
            sizeChanged();
        }
    }

    public float getHeight() {
        return this.height;
    }

    public void setHeight(float height2) {
        float oldHeight = this.height;
        this.height = height2;
        if (height2 != oldHeight) {
            sizeChanged();
        }
    }

    public float getTop() {
        return this.y + this.height;
    }

    public float getRight() {
        return this.x + this.width;
    }

    /* access modifiers changed from: protected */
    public void positionChanged() {
    }

    /* access modifiers changed from: protected */
    public void sizeChanged() {
    }

    public void setSize(float width2, float height2) {
        float oldWidth = this.width;
        float oldHeight = this.height;
        this.width = width2;
        this.height = height2;
        if (width2 != oldWidth || height2 != oldHeight) {
            sizeChanged();
        }
    }

    public void sizeBy(float size) {
        this.width += size;
        this.height += size;
        sizeChanged();
    }

    public void sizeBy(float width2, float height2) {
        this.width += width2;
        this.height += height2;
        sizeChanged();
    }

    public void setBounds(float x2, float y2, float width2, float height2) {
        if (!(this.x == x2 && this.y == y2)) {
            this.x = x2;
            this.y = y2;
            positionChanged();
        }
        if (this.width != width2 || this.height != height2) {
            this.width = width2;
            this.height = height2;
            sizeChanged();
        }
    }

    public float getOriginX() {
        return this.originX;
    }

    public void setOriginX(float originX2) {
        this.originX = originX2;
    }

    public float getOriginY() {
        return this.originY;
    }

    public void setOriginY(float originY2) {
        this.originY = originY2;
    }

    public void setOrigin(float originX2, float originY2) {
        this.originX = originX2;
        this.originY = originY2;
    }

    public void setOrigin(int alignment) {
        if ((alignment & 8) != 0) {
            this.originX = Animation.CurveTimeline.LINEAR;
        } else if ((alignment & 16) != 0) {
            this.originX = this.width;
        } else {
            this.originX = this.width / 2.0f;
        }
        if ((alignment & 4) != 0) {
            this.originY = Animation.CurveTimeline.LINEAR;
        } else if ((alignment & 2) != 0) {
            this.originY = this.height;
        } else {
            this.originY = this.height / 2.0f;
        }
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public void setScaleX(float scaleX2) {
        this.scaleX = scaleX2;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public void setScaleY(float scaleY2) {
        this.scaleY = scaleY2;
    }

    public void setScale(float scaleXY) {
        this.scaleX = scaleXY;
        this.scaleY = scaleXY;
    }

    public void setScale(float scaleX2, float scaleY2) {
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
    }

    public void scaleBy(float scale) {
        this.scaleX += scale;
        this.scaleY += scale;
    }

    public void scaleBy(float scaleX2, float scaleY2) {
        this.scaleX += scaleX2;
        this.scaleY += scaleY2;
    }

    public float getRotation() {
        return this.rotation;
    }

    public void setRotation(float degrees) {
        this.rotation = degrees;
    }

    public void rotateBy(float amountInDegrees) {
        this.rotation += amountInDegrees;
    }

    public void setColor(Color color2) {
        this.color.set(color2);
    }

    public void setColor(float r, float g, float b, float a) {
        this.color.set(r, g, b, a);
    }

    public Color getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void toFront() {
        setZIndex(Integer.MAX_VALUE);
    }

    public void toBack() {
        setZIndex(0);
    }

    public void setZIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("ZIndex cannot be < 0.");
        }
        Group parent2 = this.parent;
        if (parent2 != null) {
            Array<Actor> children = parent2.children;
            if (children.size != 1 && children.removeValue(this, true)) {
                if (index >= children.size) {
                    children.add(this);
                } else {
                    children.insert(index, this);
                }
            }
        }
    }

    public int getZIndex() {
        Group parent2 = this.parent;
        if (parent2 == null) {
            return -1;
        }
        return parent2.children.indexOf(this, true);
    }

    public boolean clipBegin() {
        return clipBegin(this.x, this.y, this.width, this.height);
    }

    public boolean clipBegin(float x2, float y2, float width2, float height2) {
        if (width2 <= Animation.CurveTimeline.LINEAR || height2 <= Animation.CurveTimeline.LINEAR) {
            return false;
        }
        Rectangle tableBounds = Rectangle.tmp;
        tableBounds.x = x2;
        tableBounds.y = y2;
        tableBounds.width = width2;
        tableBounds.height = height2;
        Stage stage2 = this.stage;
        Rectangle scissorBounds = (Rectangle) Pools.obtain(Rectangle.class);
        stage2.calculateScissors(tableBounds, scissorBounds);
        if (ScissorStack.pushScissors(scissorBounds)) {
            return true;
        }
        Pools.free(scissorBounds);
        return false;
    }

    public void clipEnd() {
        Pools.free(ScissorStack.popScissors());
    }

    public Vector2 screenToLocalCoordinates(Vector2 screenCoords) {
        Stage stage2 = this.stage;
        return stage2 == null ? screenCoords : stageToLocalCoordinates(stage2.screenToStageCoordinates(screenCoords));
    }

    public Vector2 stageToLocalCoordinates(Vector2 stageCoords) {
        if (this.parent != null) {
            this.parent.stageToLocalCoordinates(stageCoords);
        }
        parentToLocalCoordinates(stageCoords);
        return stageCoords;
    }

    public Vector2 localToStageCoordinates(Vector2 localCoords) {
        return localToAscendantCoordinates(null, localCoords);
    }

    public Vector2 localToParentCoordinates(Vector2 localCoords) {
        float rotation2 = -this.rotation;
        float scaleX2 = this.scaleX;
        float scaleY2 = this.scaleY;
        float x2 = this.x;
        float y2 = this.y;
        if (rotation2 != Animation.CurveTimeline.LINEAR) {
            float cos = (float) Math.cos((double) (0.017453292f * rotation2));
            float sin = (float) Math.sin((double) (0.017453292f * rotation2));
            float originX2 = this.originX;
            float originY2 = this.originY;
            float tox = (localCoords.x - originX2) * scaleX2;
            float toy = (localCoords.y - originY2) * scaleY2;
            localCoords.x = (tox * cos) + (toy * sin) + originX2 + x2;
            localCoords.y = ((-sin) * tox) + (toy * cos) + originY2 + y2;
        } else if (scaleX2 == 1.0f && scaleY2 == 1.0f) {
            localCoords.x += x2;
            localCoords.y += y2;
        } else {
            float originX3 = this.originX;
            float originY3 = this.originY;
            localCoords.x = ((localCoords.x - originX3) * scaleX2) + originX3 + x2;
            localCoords.y = ((localCoords.y - originY3) * scaleY2) + originY3 + y2;
        }
        return localCoords;
    }

    public Vector2 localToAscendantCoordinates(Actor ascendant, Vector2 localCoords) {
        Actor actor = this;
        while (actor != null) {
            actor.localToParentCoordinates(localCoords);
            actor = actor.parent;
            if (actor == ascendant) {
                break;
            }
        }
        return localCoords;
    }

    public Vector2 parentToLocalCoordinates(Vector2 parentCoords) {
        float rotation2 = this.rotation;
        float scaleX2 = this.scaleX;
        float scaleY2 = this.scaleY;
        float childX = this.x;
        float childY = this.y;
        if (rotation2 != Animation.CurveTimeline.LINEAR) {
            float cos = (float) Math.cos((double) (0.017453292f * rotation2));
            float sin = (float) Math.sin((double) (0.017453292f * rotation2));
            float originX2 = this.originX;
            float originY2 = this.originY;
            float tox = (parentCoords.x - childX) - originX2;
            float toy = (parentCoords.y - childY) - originY2;
            parentCoords.x = (((tox * cos) + (toy * sin)) / scaleX2) + originX2;
            parentCoords.y = ((((-sin) * tox) + (toy * cos)) / scaleY2) + originY2;
        } else if (scaleX2 == 1.0f && scaleY2 == 1.0f) {
            parentCoords.x -= childX;
            parentCoords.y -= childY;
        } else {
            float originX3 = this.originX;
            float originY3 = this.originY;
            parentCoords.x = (((parentCoords.x - childX) - originX3) / scaleX2) + originX3;
            parentCoords.y = (((parentCoords.y - childY) - originY3) / scaleY2) + originY3;
        }
        return parentCoords;
    }

    public void drawDebug(ShapeRenderer shapes) {
        drawDebugBounds(shapes);
    }

    /* access modifiers changed from: protected */
    public void drawDebugBounds(ShapeRenderer shapes) {
        if (this.debug) {
            shapes.set(ShapeRenderer.ShapeType.Line);
            shapes.setColor(this.stage.getDebugColor());
            shapes.rect(this.x, this.y, this.originX, this.originY, this.width, this.height, this.scaleX, this.scaleY, this.rotation);
        }
    }

    public void setDebug(boolean enabled) {
        this.debug = enabled;
        if (enabled) {
            Stage.debug = true;
        }
    }

    public boolean getDebug() {
        return this.debug;
    }

    public Actor debug() {
        setDebug(true);
        return this;
    }

    public String toString() {
        String name2 = this.name;
        if (name2 != null) {
            return name2;
        }
        String name3 = getClass().getName();
        int dotIndex = name3.lastIndexOf(46);
        if (dotIndex != -1) {
            return name3.substring(dotIndex + 1);
        }
        return name3;
    }

    public void setAlpha(float alpha) {
        setColor(getColor().r, getColor().g, getColor().b, alpha);
    }

    public void setCenterPosition(float x2, float y2) {
        this.x = x2 - (this.width / 2.0f);
        this.y = y2 - (this.height / 2.0f);
    }

    public float getCenterX() {
        return this.x + (this.width / 2.0f);
    }

    public float getCenterY() {
        return this.y + (this.height / 2.0f);
    }
}
