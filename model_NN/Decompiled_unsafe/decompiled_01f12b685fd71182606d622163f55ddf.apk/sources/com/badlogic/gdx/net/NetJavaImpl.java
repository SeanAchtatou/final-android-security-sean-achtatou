package com.badlogic.gdx.net;

import com.badlogic.gdx.Net;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.StreamUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NetJavaImpl {
    final ObjectMap<Net.HttpRequest, HttpURLConnection> connections = new ObjectMap<>();
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    final ObjectMap<Net.HttpRequest, Net.HttpResponseListener> listeners = new ObjectMap<>();

    static class HttpClientResponse implements Net.HttpResponse {
        private final HttpURLConnection connection;
        private HttpStatus status;

        public HttpClientResponse(HttpURLConnection connection2) throws IOException {
            this.connection = connection2;
            try {
                this.status = new HttpStatus(connection2.getResponseCode());
            } catch (IOException e) {
                this.status = new HttpStatus(-1);
            }
        }

        public byte[] getResult() {
            byte[] bArr;
            InputStream input = getInputStream();
            try {
                bArr = StreamUtils.copyStreamToByteArray(input, this.connection.getContentLength());
            } catch (IOException e) {
                bArr = StreamUtils.EMPTY_BYTES;
            } finally {
                StreamUtils.closeQuietly(input);
            }
            return bArr;
        }

        /* JADX INFO: finally extract failed */
        public String getResultAsString() {
            InputStream input = getInputStream();
            if (input == null) {
                return "";
            }
            try {
                String copyStreamToString = StreamUtils.copyStreamToString(input, this.connection.getContentLength());
                StreamUtils.closeQuietly(input);
                return copyStreamToString;
            } catch (IOException e) {
                StreamUtils.closeQuietly(input);
                return "";
            } catch (Throwable th) {
                StreamUtils.closeQuietly(input);
                throw th;
            }
        }

        public InputStream getResultAsStream() {
            return getInputStream();
        }

        public HttpStatus getStatus() {
            return this.status;
        }

        public String getHeader(String name) {
            return this.connection.getHeaderField(name);
        }

        public Map<String, List<String>> getHeaders() {
            return this.connection.getHeaderFields();
        }

        private InputStream getInputStream() {
            try {
                return this.connection.getInputStream();
            } catch (IOException e) {
                return this.connection.getErrorStream();
            }
        }
    }

    public void sendHttpRequest(Net.HttpRequest httpRequest, Net.HttpResponseListener httpResponseListener) {
        URL url;
        final boolean doingOutPut = true;
        if (httpRequest.getUrl() == null) {
            httpResponseListener.failed(new GdxRuntimeException("can't process a HTTP request without URL set"));
            return;
        }
        try {
            String method = httpRequest.getMethod();
            if (method.equalsIgnoreCase(Net.HttpMethods.GET)) {
                String queryString = "";
                String value = httpRequest.getContent();
                if (value != null && !"".equals(value)) {
                    queryString = "?" + value;
                }
                url = new URL(String.valueOf(httpRequest.getUrl()) + queryString);
            } else {
                url = new URL(httpRequest.getUrl());
            }
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (!method.equalsIgnoreCase(Net.HttpMethods.POST) && !method.equalsIgnoreCase(Net.HttpMethods.PUT)) {
                doingOutPut = false;
            }
            connection.setDoOutput(doingOutPut);
            connection.setDoInput(true);
            connection.setRequestMethod(method);
            HttpURLConnection.setFollowRedirects(httpRequest.getFollowRedirects());
            putIntoConnectionsAndListeners(httpRequest, httpResponseListener, connection);
            for (Map.Entry<String, String> header : httpRequest.getHeaders().entrySet()) {
                connection.addRequestProperty((String) header.getKey(), (String) header.getValue());
            }
            connection.setConnectTimeout(httpRequest.getTimeOut());
            connection.setReadTimeout(httpRequest.getTimeOut());
            final Net.HttpRequest httpRequest2 = httpRequest;
            final Net.HttpResponseListener httpResponseListener2 = httpResponseListener;
            this.executorService.submit(new Runnable() {
                public void run() {
                    OutputStream os;
                    OutputStreamWriter writer;
                    try {
                        if (doingOutPut) {
                            String contentAsString = httpRequest2.getContent();
                            if (contentAsString != null) {
                                writer = new OutputStreamWriter(connection.getOutputStream());
                                writer.write(contentAsString);
                                StreamUtils.closeQuietly(writer);
                            } else {
                                InputStream contentAsStream = httpRequest2.getContentStream();
                                if (contentAsStream != null) {
                                    os = connection.getOutputStream();
                                    StreamUtils.copyStream(contentAsStream, os);
                                    StreamUtils.closeQuietly(os);
                                }
                            }
                        }
                        connection.connect();
                        HttpClientResponse clientResponse = new HttpClientResponse(connection);
                        Net.HttpResponseListener listener = NetJavaImpl.this.getFromListeners(httpRequest2);
                        if (listener != null) {
                            listener.handleHttpResponse(clientResponse);
                        }
                        NetJavaImpl.this.removeFromConnectionsAndListeners(httpRequest2);
                        connection.disconnect();
                    } catch (Exception e) {
                        connection.disconnect();
                        try {
                            httpResponseListener2.failed(e);
                        } finally {
                            NetJavaImpl.this.removeFromConnectionsAndListeners(httpRequest2);
                        }
                    } catch (Throwable th) {
                        StreamUtils.closeQuietly(writer);
                        throw th;
                    }
                }
            });
        } catch (Exception e) {
            httpResponseListener.failed(e);
            removeFromConnectionsAndListeners(httpRequest);
        } catch (Throwable th) {
            removeFromConnectionsAndListeners(httpRequest);
            throw th;
        }
    }

    public void cancelHttpRequest(Net.HttpRequest httpRequest) {
        Net.HttpResponseListener httpResponseListener = getFromListeners(httpRequest);
        if (httpResponseListener != null) {
            httpResponseListener.cancelled();
            removeFromConnectionsAndListeners(httpRequest);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void removeFromConnectionsAndListeners(Net.HttpRequest httpRequest) {
        this.connections.remove(httpRequest);
        this.listeners.remove(httpRequest);
    }

    /* access modifiers changed from: package-private */
    public synchronized void putIntoConnectionsAndListeners(Net.HttpRequest httpRequest, Net.HttpResponseListener httpResponseListener, HttpURLConnection connection) {
        this.connections.put(httpRequest, connection);
        this.listeners.put(httpRequest, httpResponseListener);
    }

    /* access modifiers changed from: package-private */
    public synchronized Net.HttpResponseListener getFromListeners(Net.HttpRequest httpRequest) {
        return this.listeners.get(httpRequest);
    }
}
