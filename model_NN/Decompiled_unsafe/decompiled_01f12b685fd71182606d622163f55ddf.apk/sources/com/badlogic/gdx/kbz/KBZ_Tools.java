package com.badlogic.gdx.kbz;

public class KBZ_Tools {
    static String[] imageNameStr;
    static String[][] packNameStr;
    public boolean isDebug = true;

    public static void setPackName(String[][] packName, String[] imageName) {
        packNameStr = packName;
        imageNameStr = imageName;
    }

    public static String getPackName(int id) {
        for (int i = 0; i < packNameStr.length; i++) {
            if (id >= Integer.parseInt(packNameStr[i][0]) && id < Integer.parseInt(packNameStr[i][1])) {
                return packNameStr[i][2];
            }
        }
        return null;
    }

    public static int getImageId(String imageName) {
        for (int i = 0; i < imageNameStr.length; i++) {
            if (imageNameStr[i].equals(imageName)) {
                return i;
            }
        }
        return -1;
    }
}
