package com.badlogic.gdx.graphics;

import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;

public class Color {
    public static final Color BLACK = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
    public static final Color BLUE = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 1.0f);
    public static final Color CLEAR = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    public static final Color CYAN = new Color(Animation.CurveTimeline.LINEAR, 1.0f, 1.0f, 1.0f);
    public static final Color DARK_GRAY = new Color(0.25f, 0.25f, 0.25f, 1.0f);
    public static final Color GRAY = new Color(0.5f, 0.5f, 0.5f, 1.0f);
    public static final Color GREEN = new Color(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 1.0f);
    public static final Color LIGHT_GRAY = new Color(0.75f, 0.75f, 0.75f, 1.0f);
    public static final Color MAGENTA = new Color(1.0f, Animation.CurveTimeline.LINEAR, 1.0f, 1.0f);
    public static final Color MAROON = new Color(0.5f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
    public static final Color NAVY = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.5f, 1.0f);
    public static final Color OLIVE = new Color(0.5f, 0.5f, Animation.CurveTimeline.LINEAR, 1.0f);
    public static final Color ORANGE = new Color(1.0f, 0.78f, Animation.CurveTimeline.LINEAR, 1.0f);
    public static final Color PINK = new Color(1.0f, 0.68f, 0.68f, 1.0f);
    public static final Color PURPLE = new Color(0.5f, Animation.CurveTimeline.LINEAR, 0.5f, 1.0f);
    public static final Color RED = new Color(1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
    public static final Color TEAL = new Color(Animation.CurveTimeline.LINEAR, 0.5f, 0.5f, 1.0f);
    public static final Color WHITE = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public static final Color YELLOW = new Color(1.0f, 1.0f, Animation.CurveTimeline.LINEAR, 1.0f);
    public float a;
    public float b;
    public float g;
    public float r;

    public Color() {
    }

    public Color(int rgba8888) {
        rgba8888ToColor(this, rgba8888);
    }

    public Color(float r2, float g2, float b2, float a2) {
        this.r = r2;
        this.g = g2;
        this.b = b2;
        this.a = a2;
        clamp();
    }

    public Color(Color color) {
        set(color);
    }

    public Color set(Color color) {
        this.r = color.r;
        this.g = color.g;
        this.b = color.b;
        this.a = color.a;
        return this;
    }

    public Color mul(Color color) {
        this.r *= color.r;
        this.g *= color.g;
        this.b *= color.b;
        this.a *= color.a;
        return clamp();
    }

    public Color mul(float value) {
        this.r *= value;
        this.g *= value;
        this.b *= value;
        this.a *= value;
        return clamp();
    }

    public Color add(Color color) {
        this.r += color.r;
        this.g += color.g;
        this.b += color.b;
        this.a += color.a;
        return clamp();
    }

    public Color sub(Color color) {
        this.r -= color.r;
        this.g -= color.g;
        this.b -= color.b;
        this.a -= color.a;
        return clamp();
    }

    public Color clamp() {
        if (this.r < Animation.CurveTimeline.LINEAR) {
            this.r = Animation.CurveTimeline.LINEAR;
        } else if (this.r > 1.0f) {
            this.r = 1.0f;
        }
        if (this.g < Animation.CurveTimeline.LINEAR) {
            this.g = Animation.CurveTimeline.LINEAR;
        } else if (this.g > 1.0f) {
            this.g = 1.0f;
        }
        if (this.b < Animation.CurveTimeline.LINEAR) {
            this.b = Animation.CurveTimeline.LINEAR;
        } else if (this.b > 1.0f) {
            this.b = 1.0f;
        }
        if (this.a < Animation.CurveTimeline.LINEAR) {
            this.a = Animation.CurveTimeline.LINEAR;
        } else if (this.a > 1.0f) {
            this.a = 1.0f;
        }
        return this;
    }

    public Color set(float r2, float g2, float b2, float a2) {
        this.r = r2;
        this.g = g2;
        this.b = b2;
        this.a = a2;
        return clamp();
    }

    public Color set(int rgba) {
        rgba8888ToColor(this, rgba);
        return this;
    }

    public Color add(float r2, float g2, float b2, float a2) {
        this.r += r2;
        this.g += g2;
        this.b += b2;
        this.a += a2;
        return clamp();
    }

    public Color sub(float r2, float g2, float b2, float a2) {
        this.r -= r2;
        this.g -= g2;
        this.b -= b2;
        this.a -= a2;
        return clamp();
    }

    public Color mul(float r2, float g2, float b2, float a2) {
        this.r *= r2;
        this.g *= g2;
        this.b *= b2;
        this.a *= a2;
        return clamp();
    }

    public Color lerp(Color target, float t) {
        this.r += (target.r - this.r) * t;
        this.g += (target.g - this.g) * t;
        this.b += (target.b - this.b) * t;
        this.a += (target.a - this.a) * t;
        return clamp();
    }

    public Color lerp(float r2, float g2, float b2, float a2, float t) {
        this.r += (r2 - this.r) * t;
        this.g += (g2 - this.g) * t;
        this.b += (b2 - this.b) * t;
        this.a += (a2 - this.a) * t;
        return clamp();
    }

    public Color premultiplyAlpha() {
        this.r *= this.a;
        this.g *= this.a;
        this.b *= this.a;
        return this;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (toIntBits() != ((Color) o).toIntBits()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3 = 0;
        if (this.r != Animation.CurveTimeline.LINEAR) {
            result = NumberUtils.floatToIntBits(this.r);
        } else {
            result = 0;
        }
        int i4 = result * 31;
        if (this.g != Animation.CurveTimeline.LINEAR) {
            i = NumberUtils.floatToIntBits(this.g);
        } else {
            i = 0;
        }
        int i5 = (i4 + i) * 31;
        if (this.b != Animation.CurveTimeline.LINEAR) {
            i2 = NumberUtils.floatToIntBits(this.b);
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.a != Animation.CurveTimeline.LINEAR) {
            i3 = NumberUtils.floatToIntBits(this.a);
        }
        return i6 + i3;
    }

    public float toFloatBits() {
        return NumberUtils.intToFloatColor((((int) (this.a * 255.0f)) << 24) | (((int) (this.b * 255.0f)) << 16) | (((int) (this.g * 255.0f)) << 8) | ((int) (this.r * 255.0f)));
    }

    public int toIntBits() {
        return (((int) (this.a * 255.0f)) << 24) | (((int) (this.b * 255.0f)) << 16) | (((int) (this.g * 255.0f)) << 8) | ((int) (this.r * 255.0f));
    }

    public String toString() {
        String value = Integer.toHexString((((int) (this.r * 255.0f)) << 24) | (((int) (this.g * 255.0f)) << 16) | (((int) (this.b * 255.0f)) << 8) | ((int) (this.a * 255.0f)));
        while (value.length() < 8) {
            value = "0" + value;
        }
        return value;
    }

    public static Color valueOf(String hex) {
        return new Color(((float) Integer.valueOf(hex.substring(0, 2), 16).intValue()) / 255.0f, ((float) Integer.valueOf(hex.substring(2, 4), 16).intValue()) / 255.0f, ((float) Integer.valueOf(hex.substring(4, 6), 16).intValue()) / 255.0f, ((float) (hex.length() != 8 ? 255 : Integer.valueOf(hex.substring(6, 8), 16).intValue())) / 255.0f);
    }

    public static float toFloatBits(int r2, int g2, int b2, int a2) {
        return NumberUtils.intToFloatColor((a2 << 24) | (b2 << 16) | (g2 << 8) | r2);
    }

    public static float toFloatBits(float r2, float g2, float b2, float a2) {
        return NumberUtils.intToFloatColor((((int) (255.0f * a2)) << 24) | (((int) (255.0f * b2)) << 16) | (((int) (255.0f * g2)) << 8) | ((int) (255.0f * r2)));
    }

    public static int toIntBits(int r2, int g2, int b2, int a2) {
        return (a2 << 24) | (b2 << 16) | (g2 << 8) | r2;
    }

    public static int alpha(float alpha) {
        return (int) (255.0f * alpha);
    }

    public static int luminanceAlpha(float luminance, float alpha) {
        return (((int) (luminance * 255.0f)) << 8) | ((int) (255.0f * alpha));
    }

    public static int rgb565(float r2, float g2, float b2) {
        return (((int) (r2 * 31.0f)) << 11) | (((int) (63.0f * g2)) << 5) | ((int) (b2 * 31.0f));
    }

    public static int rgba4444(float r2, float g2, float b2, float a2) {
        return (((int) (r2 * 15.0f)) << 12) | (((int) (g2 * 15.0f)) << 8) | (((int) (b2 * 15.0f)) << 4) | ((int) (a2 * 15.0f));
    }

    public static int rgb888(float r2, float g2, float b2) {
        return (((int) (r2 * 255.0f)) << 16) | (((int) (g2 * 255.0f)) << 8) | ((int) (b2 * 255.0f));
    }

    public static int rgba8888(float r2, float g2, float b2, float a2) {
        return (((int) (r2 * 255.0f)) << 24) | (((int) (g2 * 255.0f)) << 16) | (((int) (b2 * 255.0f)) << 8) | ((int) (a2 * 255.0f));
    }

    public static int argb8888(float a2, float r2, float g2, float b2) {
        return (((int) (a2 * 255.0f)) << 24) | (((int) (r2 * 255.0f)) << 16) | (((int) (g2 * 255.0f)) << 8) | ((int) (b2 * 255.0f));
    }

    public static int rgb565(Color color) {
        return (((int) (color.r * 31.0f)) << 11) | (((int) (color.g * 63.0f)) << 5) | ((int) (color.b * 31.0f));
    }

    public static int rgba4444(Color color) {
        return (((int) (color.r * 15.0f)) << 12) | (((int) (color.g * 15.0f)) << 8) | (((int) (color.b * 15.0f)) << 4) | ((int) (color.a * 15.0f));
    }

    public static int rgb888(Color color) {
        return (((int) (color.r * 255.0f)) << 16) | (((int) (color.g * 255.0f)) << 8) | ((int) (color.b * 255.0f));
    }

    public static int rgba8888(Color color) {
        return (((int) (color.r * 255.0f)) << 24) | (((int) (color.g * 255.0f)) << 16) | (((int) (color.b * 255.0f)) << 8) | ((int) (color.a * 255.0f));
    }

    public static int argb8888(Color color) {
        return (((int) (color.a * 255.0f)) << 24) | (((int) (color.r * 255.0f)) << 16) | (((int) (color.g * 255.0f)) << 8) | ((int) (color.b * 255.0f));
    }

    public static void rgb565ToColor(Color color, int value) {
        color.r = ((float) ((63488 & value) >>> 11)) / 31.0f;
        color.g = ((float) ((value & 2016) >>> 5)) / 63.0f;
        color.b = ((float) ((value & 31) >>> 0)) / 31.0f;
    }

    public static void rgba4444ToColor(Color color, int value) {
        color.r = ((float) ((61440 & value) >>> 12)) / 15.0f;
        color.g = ((float) ((value & 3840) >>> 8)) / 15.0f;
        color.b = ((float) ((value & PAK_ASSETS.IMG_SHUOMINGZI03) >>> 4)) / 15.0f;
        color.a = ((float) (value & 15)) / 15.0f;
    }

    public static void rgb888ToColor(Color color, int value) {
        color.r = ((float) ((16711680 & value) >>> 16)) / 255.0f;
        color.g = ((float) ((65280 & value) >>> 8)) / 255.0f;
        color.b = ((float) (value & 255)) / 255.0f;
    }

    public static void rgba8888ToColor(Color color, int value) {
        color.r = ((float) ((-16777216 & value) >>> 24)) / 255.0f;
        color.g = ((float) ((16711680 & value) >>> 16)) / 255.0f;
        color.b = ((float) ((65280 & value) >>> 8)) / 255.0f;
        color.a = ((float) (value & 255)) / 255.0f;
    }

    public static void argb8888ToColor(Color color, int value) {
        color.a = ((float) ((-16777216 & value) >>> 24)) / 255.0f;
        color.r = ((float) ((16711680 & value) >>> 16)) / 255.0f;
        color.g = ((float) ((65280 & value) >>> 8)) / 255.0f;
        color.b = ((float) (value & 255)) / 255.0f;
    }

    public Color cpy() {
        return new Color(this);
    }
}
