package com.badlogic.gdx.graphics.g3d.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class ModelBuilder {
    private Array<MeshBuilder> builders = new Array<>();
    private Model model;
    private Node node;
    private Matrix4 tmpTransform = new Matrix4();

    private MeshBuilder getBuilder(VertexAttributes attributes) {
        Iterator<MeshBuilder> it = this.builders.iterator();
        while (it.hasNext()) {
            MeshBuilder mb = it.next();
            if (mb.getAttributes().equals(attributes) && mb.lastIndex() < 16383) {
                return mb;
            }
        }
        MeshBuilder result = new MeshBuilder();
        result.begin(attributes);
        this.builders.add(result);
        return result;
    }

    public void begin() {
        if (this.model != null) {
            throw new GdxRuntimeException("Call end() first");
        }
        this.node = null;
        this.model = new Model();
        this.builders.clear();
    }

    public Model end() {
        if (this.model == null) {
            throw new GdxRuntimeException("Call begin() first");
        }
        Model result = this.model;
        endnode();
        this.model = null;
        Iterator<MeshBuilder> it = this.builders.iterator();
        while (it.hasNext()) {
            it.next().end();
        }
        this.builders.clear();
        rebuildReferences(result);
        return result;
    }

    private void endnode() {
        if (this.node != null) {
            this.node = null;
        }
    }

    /* access modifiers changed from: protected */
    public Node node(Node node2) {
        if (this.model == null) {
            throw new GdxRuntimeException("Call begin() first");
        }
        endnode();
        this.model.nodes.add(node2);
        this.node = node2;
        return node2;
    }

    public Node node() {
        Node node2 = new Node();
        node(node2);
        node2.id = "node" + this.model.nodes.size;
        return node2;
    }

    public Node node(String id, Model model2) {
        Node node2 = new Node();
        node2.id = id;
        node2.addChildren(model2.nodes);
        node(node2);
        for (Disposable disposable : model2.getManagedDisposables()) {
            manage(disposable);
        }
        return node2;
    }

    public void manage(Disposable disposable) {
        if (this.model == null) {
            throw new GdxRuntimeException("Call begin() first");
        }
        this.model.manageDisposable(disposable);
    }

    public void part(MeshPart meshpart, Material material) {
        if (this.node == null) {
            node();
        }
        this.node.parts.add(new NodePart(meshpart, material));
    }

    public MeshPart part(String id, Mesh mesh, int primitiveType, int offset, int size, Material material) {
        MeshPart meshPart = new MeshPart();
        meshPart.id = id;
        meshPart.primitiveType = primitiveType;
        meshPart.mesh = mesh;
        meshPart.indexOffset = offset;
        meshPart.numVertices = size;
        part(meshPart, material);
        return meshPart;
    }

    public MeshPart part(String id, Mesh mesh, int primitiveType, Material material) {
        return part(id, mesh, primitiveType, 0, mesh.getNumIndices(), material);
    }

    public MeshPartBuilder part(String id, int primitiveType, VertexAttributes attributes, Material material) {
        MeshBuilder builder = getBuilder(attributes);
        part(builder.part(id, primitiveType), material);
        return builder;
    }

    public MeshPartBuilder part(String id, int primitiveType, long attributes, Material material) {
        return part(id, primitiveType, MeshBuilder.createAttributes(attributes), material);
    }

    public Model createBox(float width, float height, float depth, Material material, long attributes) {
        return createBox(width, height, depth, 4, material, attributes);
    }

    public Model createBox(float width, float height, float depth, int primitiveType, Material material, long attributes) {
        begin();
        part("box", primitiveType, attributes, material).box(width, height, depth);
        return end();
    }

    public Model createRect(float x00, float y00, float z00, float x10, float y10, float z10, float x11, float y11, float z11, float x01, float y01, float z01, float normalX, float normalY, float normalZ, Material material, long attributes) {
        return createRect(x00, y00, z00, x10, y10, z10, x11, y11, z11, x01, y01, z01, normalX, normalY, normalZ, 4, material, attributes);
    }

    public Model createRect(float x00, float y00, float z00, float x10, float y10, float z10, float x11, float y11, float z11, float x01, float y01, float z01, float normalX, float normalY, float normalZ, int primitiveType, Material material, long attributes) {
        begin();
        part("rect", primitiveType, attributes, material).rect(x00, y00, z00, x10, y10, z10, x11, y11, z11, x01, y01, z01, normalX, normalY, normalZ);
        return end();
    }

    public Model createCylinder(float width, float height, float depth, int divisions, Material material, long attributes) {
        return createCylinder(width, height, depth, divisions, 4, material, attributes);
    }

    public Model createCylinder(float width, float height, float depth, int divisions, int primitiveType, Material material, long attributes) {
        return createCylinder(width, height, depth, divisions, primitiveType, material, attributes, Animation.CurveTimeline.LINEAR, 360.0f);
    }

    public Model createCylinder(float width, float height, float depth, int divisions, Material material, long attributes, float angleFrom, float angleTo) {
        return createCylinder(width, height, depth, divisions, 4, material, attributes, angleFrom, angleTo);
    }

    public Model createCylinder(float width, float height, float depth, int divisions, int primitiveType, Material material, long attributes, float angleFrom, float angleTo) {
        begin();
        part("cylinder", primitiveType, attributes, material).cylinder(width, height, depth, divisions, angleFrom, angleTo);
        return end();
    }

    public Model createCone(float width, float height, float depth, int divisions, Material material, long attributes) {
        return createCone(width, height, depth, divisions, 4, material, attributes);
    }

    public Model createCone(float width, float height, float depth, int divisions, int primitiveType, Material material, long attributes) {
        return createCone(width, height, depth, divisions, primitiveType, material, attributes, Animation.CurveTimeline.LINEAR, 360.0f);
    }

    public Model createCone(float width, float height, float depth, int divisions, Material material, long attributes, float angleFrom, float angleTo) {
        return createCone(width, height, depth, divisions, 4, material, attributes, angleFrom, angleTo);
    }

    public Model createCone(float width, float height, float depth, int divisions, int primitiveType, Material material, long attributes, float angleFrom, float angleTo) {
        begin();
        part("cone", primitiveType, attributes, material).cone(width, height, depth, divisions, angleFrom, angleTo);
        return end();
    }

    public Model createSphere(float width, float height, float depth, int divisionsU, int divisionsV, Material material, long attributes) {
        return createSphere(width, height, depth, divisionsU, divisionsV, 4, material, attributes);
    }

    public Model createSphere(float width, float height, float depth, int divisionsU, int divisionsV, int primitiveType, Material material, long attributes) {
        return createSphere(width, height, depth, divisionsU, divisionsV, primitiveType, material, attributes, Animation.CurveTimeline.LINEAR, 360.0f, Animation.CurveTimeline.LINEAR, 180.0f);
    }

    public Model createSphere(float width, float height, float depth, int divisionsU, int divisionsV, Material material, long attributes, float angleUFrom, float angleUTo, float angleVFrom, float angleVTo) {
        return createSphere(width, height, depth, divisionsU, divisionsV, 4, material, attributes, angleUFrom, angleUTo, angleVFrom, angleVTo);
    }

    public Model createSphere(float width, float height, float depth, int divisionsU, int divisionsV, int primitiveType, Material material, long attributes, float angleUFrom, float angleUTo, float angleVFrom, float angleVTo) {
        begin();
        part("cylinder", primitiveType, attributes, material).sphere(width, height, depth, divisionsU, divisionsV, angleUFrom, angleUTo, angleVFrom, angleVTo);
        return end();
    }

    public Model createCapsule(float radius, float height, int divisions, Material material, long attributes) {
        return createCapsule(radius, height, divisions, 4, material, attributes);
    }

    public Model createCapsule(float radius, float height, int divisions, int primitiveType, Material material, long attributes) {
        begin();
        part("capsule", primitiveType, attributes, material).capsule(radius, height, divisions);
        return end();
    }

    public static void rebuildReferences(Model model2) {
        model2.materials.clear();
        model2.meshes.clear();
        model2.meshParts.clear();
        Iterator<Node> it = model2.nodes.iterator();
        while (it.hasNext()) {
            rebuildReferences(model2, it.next());
        }
    }

    private static void rebuildReferences(Model model2, Node node2) {
        Iterator<NodePart> it = node2.parts.iterator();
        while (it.hasNext()) {
            NodePart mpm = it.next();
            if (!model2.materials.contains(mpm.material, true)) {
                model2.materials.add(mpm.material);
            }
            if (!model2.meshParts.contains(mpm.meshPart, true)) {
                model2.meshParts.add(mpm.meshPart);
                if (!model2.meshes.contains(mpm.meshPart.mesh, true)) {
                    model2.meshes.add(mpm.meshPart.mesh);
                }
                model2.manageDisposable(mpm.meshPart.mesh);
            }
        }
        for (Node child : node2.getChildren()) {
            rebuildReferences(model2, child);
        }
    }

    public Model createXYZCoordinates(float axisLength, float capLength, float stemThickness, int divisions, int primitiveType, Material material, long attributes) {
        begin();
        Node node2 = node();
        MeshPartBuilder partBuilder = part("xyz", primitiveType, attributes, material);
        partBuilder.setColor(Color.RED);
        partBuilder.arrow(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, axisLength, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, capLength, stemThickness, divisions);
        partBuilder.setColor(Color.GREEN);
        partBuilder.arrow(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, axisLength, Animation.CurveTimeline.LINEAR, capLength, stemThickness, divisions);
        partBuilder.setColor(Color.BLUE);
        partBuilder.arrow(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, axisLength, capLength, stemThickness, divisions);
        return end();
    }

    public Model createXYZCoordinates(float axisLength, Material material, long attributes) {
        return createXYZCoordinates(axisLength, 0.1f, 0.1f, 5, 4, material, attributes);
    }

    public Model createArrow(float x1, float y1, float z1, float x2, float y2, float z2, float capLength, float stemThickness, int divisions, int primitiveType, Material material, long attributes) {
        begin();
        part("arrow", primitiveType, attributes, material).arrow(x1, y1, z1, x2, y2, z2, capLength, stemThickness, divisions);
        return end();
    }

    public Model createArrow(Vector3 from, Vector3 to, Material material, long attributes) {
        return createArrow(from.x, from.y, from.z, to.x, to.y, to.z, 0.1f, 0.1f, 5, 4, material, attributes);
    }

    public Model createLineGrid(int xDivisions, int zDivisions, float xSize, float zSize, Material material, long attributes) {
        begin();
        MeshPartBuilder partBuilder = part("lines", 1, attributes, material);
        float hxlength = (((float) xDivisions) * xSize) / 2.0f;
        float hzlength = (((float) zDivisions) * zSize) / 2.0f;
        float x1 = -hxlength;
        float z1 = hzlength;
        float x2 = -hxlength;
        float z2 = -hzlength;
        for (int i = 0; i <= xDivisions; i++) {
            partBuilder.line(x1, Animation.CurveTimeline.LINEAR, z1, x2, Animation.CurveTimeline.LINEAR, z2);
            x1 += xSize;
            x2 += xSize;
        }
        float x12 = -hxlength;
        float z12 = -hzlength;
        float x22 = hxlength;
        float z22 = -hzlength;
        for (int j = 0; j <= zDivisions; j++) {
            partBuilder.line(x12, Animation.CurveTimeline.LINEAR, z12, x22, Animation.CurveTimeline.LINEAR, z22);
            z12 += zSize;
            z22 += zSize;
        }
        return end();
    }
}
