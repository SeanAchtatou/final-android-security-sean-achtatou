package com.badlogic.gdx.graphics.g3d.utils;

import com.badlogic.gdx.graphics.GLTexture;
import com.badlogic.gdx.graphics.Texture;

public class TextureDescriptor<T extends GLTexture> implements Comparable<TextureDescriptor<T>> {
    public Texture.TextureFilter magFilter;
    public Texture.TextureFilter minFilter;
    public T texture;
    public Texture.TextureWrap uWrap;
    public Texture.TextureWrap vWrap;

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return compareTo((TextureDescriptor<Texture>) ((TextureDescriptor) obj));
    }

    public TextureDescriptor(T texture2, Texture.TextureFilter minFilter2, Texture.TextureFilter magFilter2, Texture.TextureWrap uWrap2, Texture.TextureWrap vWrap2) {
        this.texture = null;
        set(texture2, minFilter2, magFilter2, uWrap2, vWrap2);
    }

    public TextureDescriptor(T texture2) {
        this(texture2, null, null, null, null);
    }

    public TextureDescriptor() {
        this.texture = null;
    }

    public void set(T texture2, Texture.TextureFilter minFilter2, Texture.TextureFilter magFilter2, Texture.TextureWrap uWrap2, Texture.TextureWrap vWrap2) {
        this.texture = texture2;
        this.minFilter = minFilter2;
        this.magFilter = magFilter2;
        this.uWrap = uWrap2;
        this.vWrap = vWrap2;
    }

    public <V extends T> void set(TextureDescriptor<V> other) {
        this.texture = other.texture;
        this.minFilter = other.minFilter;
        this.magFilter = other.magFilter;
        this.uWrap = other.uWrap;
        this.vWrap = other.vWrap;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof TextureDescriptor)) {
            return false;
        }
        TextureDescriptor<?> other = (TextureDescriptor) obj;
        if (other.texture == this.texture && other.minFilter == this.minFilter && other.magFilter == this.magFilter && other.uWrap == this.uWrap && other.vWrap == this.vWrap) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        long textureObjectHandle = 811 * ((811 * ((811 * ((811 * ((811 * ((long) (this.texture == null ? 0 : this.texture.glTarget))) + ((long) (this.texture == null ? 0 : this.texture.getTextureObjectHandle())))) + ((long) (this.minFilter == null ? 0 : this.minFilter.getGLEnum())))) + ((long) (this.magFilter == null ? 0 : this.magFilter.getGLEnum())))) + ((long) (this.uWrap == null ? 0 : this.uWrap.getGLEnum())));
        if (this.vWrap != null) {
            i = this.vWrap.getGLEnum();
        }
        long result = textureObjectHandle + ((long) i);
        return (int) ((result >> 32) ^ result);
    }

    public int compareTo(TextureDescriptor<Texture> textureDescriptor) {
        int i = 0;
        if (textureDescriptor == this) {
            return 0;
        }
        int t1 = this.texture == null ? 0 : this.texture.glTarget;
        int t2 = textureDescriptor.texture == null ? 0 : textureDescriptor.texture.glTarget;
        if (t1 != t2) {
            return t1 - t2;
        }
        int h1 = this.texture == null ? 0 : this.texture.getTextureObjectHandle();
        int h2 = textureDescriptor.texture == null ? 0 : textureDescriptor.texture.getTextureObjectHandle();
        if (h1 != h2) {
            return h1 - h2;
        }
        if (this.minFilter != textureDescriptor.minFilter) {
            int gLEnum = this.minFilter == null ? 0 : this.minFilter.getGLEnum();
            if (textureDescriptor.minFilter != null) {
                i = textureDescriptor.minFilter.getGLEnum();
            }
            return gLEnum - i;
        } else if (this.magFilter != textureDescriptor.magFilter) {
            int gLEnum2 = this.magFilter == null ? 0 : this.magFilter.getGLEnum();
            if (textureDescriptor.magFilter != null) {
                i = textureDescriptor.magFilter.getGLEnum();
            }
            return gLEnum2 - i;
        } else if (this.uWrap != textureDescriptor.uWrap) {
            int gLEnum3 = this.uWrap == null ? 0 : this.uWrap.getGLEnum();
            if (textureDescriptor.uWrap != null) {
                i = textureDescriptor.uWrap.getGLEnum();
            }
            return gLEnum3 - i;
        } else if (this.vWrap == textureDescriptor.vWrap) {
            return 0;
        } else {
            int gLEnum4 = this.vWrap == null ? 0 : this.vWrap.getGLEnum();
            if (textureDescriptor.vWrap != null) {
                i = textureDescriptor.vWrap.getGLEnum();
            }
            return gLEnum4 - i;
        }
    }
}
