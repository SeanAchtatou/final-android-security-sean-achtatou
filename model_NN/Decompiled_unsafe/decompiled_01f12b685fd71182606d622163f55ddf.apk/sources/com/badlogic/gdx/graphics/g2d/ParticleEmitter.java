package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Writer;

public class ParticleEmitter {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnEllipseSide = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnShape = null;
    private static final int LB = 2;
    private static final int RB = 4;
    private static final int RT = 1;
    private static final int UPDATE_ANGLE = 4;
    private static final int UPDATE_GRAVITY = 64;
    private static final int UPDATE_ROTATION = 8;
    private static final int UPDATE_SCALE_X = 1;
    private static final int UPDATE_SCALE_Y = 2;
    private static final int UPDATE_TINT = 128;
    private static final int UPDATE_VELOCITY = 16;
    private static final int UPDATE_WIND = 32;
    public static float fixDeltaTime = 0.016666668f;
    private float accumulator;
    private boolean[] active;
    private int activeCount;
    private boolean additive = true;
    private boolean aligned;
    private boolean allowCompletion;
    private ScaledNumericValue angleValue = new ScaledNumericValue();
    private boolean attached;
    private BoundingBox bounds;
    private ScaledNumericValue centerValue = new ScaledNumericValue();
    private boolean continuous;
    private float delay;
    private float delayTimer;
    private RangedNumericValue delayValue = new RangedNumericValue();
    public float duration = 1.0f;
    public float durationTimer;
    private RangedNumericValue durationValue = new RangedNumericValue();
    private int emission;
    private int emissionDelta;
    private int emissionDiff;
    private ScaledNumericValue emissionValue = new ScaledNumericValue();
    private boolean firstUpdate;
    private boolean fixFrame = false;
    private boolean flipX;
    private boolean flipY;
    private ScaledNumericValue gravityValue = new ScaledNumericValue();
    private String imagePath;
    private int life;
    private int lifeDiff;
    private int lifeOffset;
    private int lifeOffsetDiff;
    private ScaledNumericValue lifeOffsetValue = new ScaledNumericValue();
    private ScaledNumericValue lifeValue = new ScaledNumericValue();
    private int maxParticleCount = 4;
    private int minParticleCount;
    private String name;
    private ParticleEditorFlip particleEditorFlip;
    private Particle[] particles;
    private boolean premultipliedAlpha = false;
    private ScaledNumericValue rotationValue = new ScaledNumericValue();
    private boolean scaleLink;
    private float spawnHeight;
    private float spawnHeightDiff;
    private ScaledNumericValue spawnHeightValue = new ScaledNumericValue();
    private SpawnShapeValue spawnShapeValue = new SpawnShapeValue();
    private float spawnWidth;
    private float spawnWidthDiff;
    private ScaledNumericValue spawnWidthValue = new ScaledNumericValue();
    private Sprite sprite;
    /* access modifiers changed from: private */
    public float spriteH;
    protected int spriteTransMode = 0;
    /* access modifiers changed from: private */
    public float spriteW;
    private GradientColorValue tintValue = new GradientColorValue();
    private ScaledNumericValue transparencyValue = new ScaledNumericValue();
    private int updateFlags;
    private ScaledNumericValue velocityValue = new ScaledNumericValue();
    private ScaledNumericValue windValue = new ScaledNumericValue();
    private float x;
    private RangedNumericValue xOffsetValue = new ScaledNumericValue();
    /* access modifiers changed from: private */
    public float xScaleFactor = 1.0f;
    private ScaledNumericValue xScaleValue = new ScaledNumericValue();
    private float y;
    private RangedNumericValue yOffsetValue = new ScaledNumericValue();
    /* access modifiers changed from: private */
    public float yScaleFactor = 1.0f;
    private ScaledNumericValue yScaleValue = new ScaledNumericValue();

    public enum SpawnEllipseSide {
        both,
        top,
        bottom
    }

    public enum SpawnShape {
        point,
        line,
        square,
        ellipse
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnEllipseSide() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnEllipseSide;
        if (iArr == null) {
            iArr = new int[SpawnEllipseSide.values().length];
            try {
                iArr[SpawnEllipseSide.both.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SpawnEllipseSide.bottom.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SpawnEllipseSide.top.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnEllipseSide = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnShape() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnShape;
        if (iArr == null) {
            iArr = new int[SpawnShape.values().length];
            try {
                iArr[SpawnShape.ellipse.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SpawnShape.line.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SpawnShape.point.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[SpawnShape.square.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnShape = iArr;
        }
        return iArr;
    }

    public void setScaleFactor(float xScaleFactor2, float yScaleFactor2) {
        if (xScaleFactor2 != Animation.CurveTimeline.LINEAR && yScaleFactor2 != Animation.CurveTimeline.LINEAR) {
            float px = xScaleFactor2 / this.xScaleFactor;
            float py = yScaleFactor2 / this.yScaleFactor;
            this.xScaleFactor = xScaleFactor2;
            this.yScaleFactor = yScaleFactor2;
            float xScaleFactor3 = xScaleFactor2 * px;
            float yScaleFactor3 = yScaleFactor2 * py;
            getSpawnWidth().setHigh(getSpawnWidth().getHighMin() * px, getSpawnWidth().getHighMax() * px);
            getSpawnWidth().setLow(getSpawnWidth().getLowMin() * px, getSpawnWidth().getLowMax() * px);
            getSpawnHeight().setHigh(getSpawnHeight().getHighMin() * py, getSpawnHeight().getHighMax() * py);
            getSpawnHeight().setLow(getSpawnHeight().getLowMin() * py, getSpawnHeight().getLowMax() * py);
            float scaleX = Math.abs(px);
            float scaleY = Math.abs(py);
            getXOffsetValue().setLow(getXOffsetValue().getLowMin() * scaleX, getXOffsetValue().getLowMax() * scaleX);
            getYOffsetValue().setLow(getYOffsetValue().getLowMin() * scaleY, getYOffsetValue().getLowMax() * scaleY);
        }
    }

    public void setSpriteTransMode(int spriteTransMode2) {
        this.spriteTransMode = spriteTransMode2;
        setSprite(this.sprite);
    }

    public int getSpriteTransMode() {
        return this.spriteTransMode;
    }

    public ParticleEmitter() {
        initialize();
    }

    public ParticleEmitter(BufferedReader reader) throws IOException {
        initialize();
        load(reader);
    }

    public ParticleEmitter(DataInputStream reader) throws IOException {
        initialize();
        load_bin(reader);
    }

    public ParticleEmitter(ParticleEmitter emitter) {
        this.sprite = emitter.sprite;
        this.name = emitter.name;
        setMaxParticleCount(emitter.maxParticleCount);
        this.minParticleCount = emitter.minParticleCount;
        this.delayValue.load(emitter.delayValue);
        this.durationValue.load(emitter.durationValue);
        this.emissionValue.load(emitter.emissionValue);
        this.lifeValue.load(emitter.lifeValue);
        this.lifeOffsetValue.load(emitter.lifeOffsetValue);
        this.xScaleValue.load(emitter.xScaleValue);
        this.yScaleValue.load(emitter.yScaleValue);
        this.rotationValue.load(emitter.rotationValue);
        this.velocityValue.load(emitter.velocityValue);
        this.angleValue.load(emitter.angleValue);
        this.windValue.load(emitter.windValue);
        this.gravityValue.load(emitter.gravityValue);
        this.transparencyValue.load(emitter.transparencyValue);
        this.tintValue.load(emitter.tintValue);
        this.xOffsetValue.load(emitter.xOffsetValue);
        this.yOffsetValue.load(emitter.yOffsetValue);
        this.spawnWidthValue.load(emitter.spawnWidthValue);
        this.spawnHeightValue.load(emitter.spawnHeightValue);
        this.spawnShapeValue.load(emitter.spawnShapeValue);
        this.centerValue.load(emitter.centerValue);
        this.attached = emitter.attached;
        this.continuous = emitter.continuous;
        this.aligned = emitter.aligned;
        this.scaleLink = emitter.scaleLink;
        this.additive = emitter.additive;
        this.fixFrame = emitter.fixFrame;
        this.premultipliedAlpha = emitter.premultipliedAlpha;
        this.spriteW = emitter.spriteW;
        this.spriteH = emitter.spriteH;
        this.spriteTransMode = emitter.spriteTransMode;
    }

    private void initialize() {
        this.durationValue.setAlwaysActive(true);
        this.emissionValue.setAlwaysActive(true);
        this.lifeValue.setAlwaysActive(true);
        this.xScaleValue.setAlwaysActive(true);
        this.yScaleValue.setAlwaysActive(true);
        this.transparencyValue.setAlwaysActive(true);
        this.spawnShapeValue.setAlwaysActive(true);
        this.spawnWidthValue.setAlwaysActive(true);
        this.spawnHeightValue.setAlwaysActive(true);
        this.centerValue.setAlwaysActive(true);
    }

    public void setMaxParticleCount(int maxParticleCount2) {
        this.maxParticleCount = maxParticleCount2;
        this.active = new boolean[maxParticleCount2];
        this.activeCount = 0;
        this.particles = new Particle[maxParticleCount2];
    }

    public void addParticle() {
        int activeCount2 = this.activeCount;
        if (activeCount2 != this.maxParticleCount) {
            boolean[] active2 = this.active;
            int n = active2.length;
            for (int i = 0; i < n; i++) {
                if (!active2[i]) {
                    activateParticle(i);
                    active2[i] = true;
                    this.activeCount = activeCount2 + 1;
                    return;
                }
            }
        }
    }

    public void addParticles(int count) {
        int count2 = Math.min(count, this.maxParticleCount - this.activeCount);
        if (count2 != 0) {
            boolean[] active2 = this.active;
            int index = 0;
            int n = active2.length;
            int i = 0;
            loop0:
            while (true) {
                if (i < count2) {
                    int index2 = index;
                    while (index2 < n) {
                        if (!active2[index2]) {
                            activateParticle(index2);
                            index = index2 + 1;
                            active2[index2] = true;
                            i++;
                        } else {
                            index2++;
                        }
                    }
                    break loop0;
                }
                break;
            }
            this.activeCount += count2;
        }
    }

    private void draw(Batch batch, float parentAlpha) {
        if (this.premultipliedAlpha) {
            batch.setBlendFunction(1, 771);
        } else if (this.additive) {
            batch.setBlendFunction(770, 1);
        }
        Particle[] particles2 = this.particles;
        boolean[] active2 = this.active;
        int n = active2.length;
        for (int i = 0; i < n; i++) {
            if (active2[i]) {
                if (parentAlpha == 1.0f) {
                    particles2[i].draw(batch);
                } else {
                    particles2[i].draw(batch, parentAlpha);
                }
            }
        }
        if (this.additive || this.premultipliedAlpha) {
            batch.setBlendFunction(770, 771);
        }
    }

    public void setParticleEditorFlip(ParticleEditorFlip particleEditorFlip2) {
        this.particleEditorFlip = particleEditorFlip2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void draw(Batch batch, float delta, float parentAlpha) {
        if (this.fixFrame && delta > Animation.CurveTimeline.LINEAR) {
            delta = fixDeltaTime;
        }
        this.accumulator = this.accumulator + Math.min(1000.0f * delta, 250.0f);
        if (this.accumulator < 1.0f) {
            draw(batch, parentAlpha);
            return;
        }
        int deltaMillis = (int) this.accumulator;
        this.accumulator = this.accumulator - ((float) deltaMillis);
        if (this.premultipliedAlpha) {
            batch.setBlendFunction(1, 771);
        } else if (this.additive) {
            batch.setBlendFunction(770, 1);
        }
        Particle[] particles2 = this.particles;
        boolean[] active2 = this.active;
        int activeCount2 = this.activeCount;
        int n = active2.length;
        for (int i = 0; i < n; i++) {
            if (active2[i]) {
                Particle particle = particles2[i];
                if (this.particleEditorFlip != null) {
                    this.particleEditorFlip.flip(particle);
                }
                if (!updateParticle(particle, delta, deltaMillis)) {
                    if (isContinuous()) {
                        if (parentAlpha == 1.0f) {
                            particles2[i].draw(batch);
                        } else {
                            particles2[i].draw(batch, parentAlpha);
                        }
                    }
                    active2[i] = false;
                    activeCount2--;
                } else if (parentAlpha == 1.0f) {
                    particles2[i].draw(batch);
                } else {
                    particles2[i].draw(batch, parentAlpha);
                }
            }
        }
        this.activeCount = activeCount2;
        if (this.additive || this.premultipliedAlpha) {
            batch.setBlendFunction(770, 771);
        }
        if (this.delayTimer < this.delay) {
            this.delayTimer = this.delayTimer + ((float) deltaMillis);
            return;
        }
        if (this.firstUpdate) {
            this.firstUpdate = false;
            addParticle();
        }
        if (this.durationTimer < this.duration) {
            this.durationTimer = this.durationTimer + ((float) deltaMillis);
        } else if (this.continuous && !this.allowCompletion) {
            restart();
        } else {
            return;
        }
        this.emissionDelta = this.emissionDelta + deltaMillis;
        float emissionTime = ((float) this.emission) + (((float) this.emissionDiff) * this.emissionValue.getScale(this.durationTimer / this.duration));
        if (emissionTime > Animation.CurveTimeline.LINEAR) {
            float emissionTime2 = 1000.0f / emissionTime;
            if (((float) this.emissionDelta) >= emissionTime2) {
                int emitCount = Math.min((int) (((float) this.emissionDelta) / emissionTime2), this.maxParticleCount - activeCount2);
                this.emissionDelta = (int) (((float) this.emissionDelta) - (((float) emitCount) * emissionTime2));
                this.emissionDelta = (int) (((float) this.emissionDelta) % emissionTime2);
                addParticles(emitCount);
            }
        }
        if (activeCount2 < this.minParticleCount) {
            addParticles(this.minParticleCount - activeCount2);
        }
    }

    public void start() {
        this.firstUpdate = true;
        this.allowCompletion = false;
        restart();
    }

    public void reset() {
        this.emissionDelta = 0;
        this.durationTimer = this.duration;
        boolean[] active2 = this.active;
        int n = active2.length;
        for (int i = 0; i < n; i++) {
            active2[i] = false;
        }
        this.activeCount = 0;
        start();
    }

    private void restart() {
        this.delay = this.delayValue.active ? this.delayValue.newLowValue() : 0.0f;
        this.delayTimer = Animation.CurveTimeline.LINEAR;
        this.durationTimer -= this.duration;
        this.duration = this.durationValue.newLowValue();
        this.emission = (int) this.emissionValue.newLowValue();
        this.emissionDiff = (int) this.emissionValue.newHighValue();
        if (!this.emissionValue.isRelative()) {
            this.emissionDiff -= this.emission;
        }
        this.life = (int) this.lifeValue.newLowValue();
        this.lifeDiff = (int) this.lifeValue.newHighValue();
        if (!this.lifeValue.isRelative()) {
            this.lifeDiff -= this.life;
        }
        this.lifeOffset = this.lifeOffsetValue.active ? (int) this.lifeOffsetValue.newLowValue() : 0;
        this.lifeOffsetDiff = (int) this.lifeOffsetValue.newHighValue();
        if (!this.lifeOffsetValue.isRelative()) {
            this.lifeOffsetDiff -= this.lifeOffset;
        }
        this.spawnWidth = this.spawnWidthValue.newLowValue();
        this.spawnWidthDiff = this.spawnWidthValue.newHighValue();
        if (!this.spawnWidthValue.isRelative()) {
            this.spawnWidthDiff -= this.spawnWidth;
        }
        this.spawnHeight = this.spawnHeightValue.newLowValue();
        this.spawnHeightDiff = this.spawnHeightValue.newHighValue();
        if (!this.spawnHeightValue.isRelative()) {
            this.spawnHeightDiff -= this.spawnHeight;
        }
        this.updateFlags = 0;
        if (this.angleValue.active && this.angleValue.timeline.length > 1) {
            this.updateFlags |= 4;
        }
        if (this.velocityValue.active) {
            this.updateFlags |= 16;
        }
        if (this.xScaleValue.timeline.length > 1) {
            this.updateFlags |= 1;
        }
        if (this.yScaleValue.timeline.length > 1) {
            this.updateFlags |= 2;
        }
        if (this.rotationValue.active && this.rotationValue.timeline.length > 1) {
            this.updateFlags |= 8;
        }
        if (this.windValue.active) {
            this.updateFlags |= 32;
        }
        if (this.gravityValue.active) {
            this.updateFlags |= 64;
        }
        if (this.tintValue.timeline.length > 1) {
            this.updateFlags |= 128;
        }
        if (this.centerValue.active && this.centerValue.timeline.length > 1) {
            this.updateFlags |= 8;
        }
    }

    public boolean isPremultipliedAlpha() {
        return this.premultipliedAlpha;
    }

    public void setPremultipliedAlpha(boolean premultipliedAlpha2) {
        this.premultipliedAlpha = premultipliedAlpha2;
    }

    /* access modifiers changed from: protected */
    public Particle newParticle(Sprite sprite2) {
        return new Particle(sprite2);
    }

    private void activateParticle(int index) {
        float px;
        float py;
        float spawnAngle;
        Particle particle = this.particles[index];
        if (particle == null) {
            Particle[] particleArr = this.particles;
            particle = newParticle(this.sprite);
            particleArr[index] = particle;
            particle.flip(this.flipX, this.flipY);
        }
        float percent = this.durationTimer / this.duration;
        int updateFlags2 = this.updateFlags;
        int scale = this.life + ((int) (((float) this.lifeDiff) * this.lifeValue.getScale(percent)));
        particle.life = scale;
        particle.currentLife = scale;
        if (this.velocityValue.active) {
            particle.velocity = this.velocityValue.newLowValue();
            particle.velocityDiff = this.velocityValue.newHighValue();
            if (!this.velocityValue.isRelative()) {
                particle.velocityDiff -= particle.velocity;
            }
        }
        particle.angle = this.angleValue.newLowValue();
        particle.angleDiff = this.angleValue.newHighValue();
        if (!this.angleValue.isRelative()) {
            particle.angleDiff -= particle.angle;
        }
        float angle = Animation.CurveTimeline.LINEAR;
        if ((updateFlags2 & 4) == 0) {
            angle = particle.angle + (particle.angleDiff * this.angleValue.getScale(Animation.CurveTimeline.LINEAR));
            particle.angle = angle;
            particle.angleCos = MathUtils.cosDeg(angle);
            particle.angleSin = MathUtils.sinDeg(angle);
        }
        float spriteWidth = this.spriteW;
        float spriteHeight = this.spriteH;
        particle.xScale = this.xScaleValue.newLowValue() / spriteWidth;
        particle.xScaleDiff = this.xScaleValue.newHighValue() / spriteWidth;
        particle.yScale = this.yScaleValue.newLowValue() / spriteHeight;
        particle.yScaleDiff = this.yScaleValue.newHighValue() / spriteHeight;
        if (isScaleLink()) {
            if (!this.xScaleValue.isRelative()) {
                particle.xScaleDiff -= particle.xScale;
            }
            particle.setScale(particle.xScale + (particle.xScaleDiff * this.xScaleValue.getScale(Animation.CurveTimeline.LINEAR)));
        } else {
            if (!this.xScaleValue.isRelative()) {
                particle.xScaleDiff -= particle.xScale;
            }
            particle.setScale(particle.xScale + (particle.xScaleDiff * this.xScaleValue.getScale(Animation.CurveTimeline.LINEAR)), particle.getScaleY());
            if (!this.yScaleValue.isRelative()) {
                particle.yScaleDiff -= particle.yScale;
            }
            particle.setScale(particle.getScaleX(), particle.yScale + (particle.yScaleDiff * this.yScaleValue.getScale(Animation.CurveTimeline.LINEAR)));
        }
        if (this.rotationValue.active) {
            particle.rotation = this.rotationValue.newLowValue();
            particle.rotationDiff = this.rotationValue.newHighValue();
            if (!this.rotationValue.isRelative()) {
                particle.rotationDiff -= particle.rotation;
            }
            float rotation = particle.rotation + (particle.rotationDiff * this.rotationValue.getScale(Animation.CurveTimeline.LINEAR));
            if (this.aligned) {
                rotation += angle;
            }
            particle.setRotation(rotation);
        }
        if (this.windValue.active) {
            particle.wind = this.windValue.newLowValue();
            particle.windDiff = this.windValue.newHighValue();
            if (!this.windValue.isRelative()) {
                particle.windDiff -= particle.wind;
            }
        }
        if (this.gravityValue.active) {
            particle.gravity = this.gravityValue.newLowValue();
            particle.gravityDiff = this.gravityValue.newHighValue();
            if (!this.gravityValue.isRelative()) {
                particle.gravityDiff -= particle.gravity;
            }
        }
        float[] color = particle.tint;
        if (color == null) {
            color = new float[3];
            particle.tint = color;
        }
        float[] temp = this.tintValue.getColor(Animation.CurveTimeline.LINEAR);
        color[0] = temp[0];
        color[1] = temp[1];
        color[2] = temp[2];
        particle.transparency = this.transparencyValue.newLowValue();
        particle.transparencyDiff = this.transparencyValue.newHighValue() - particle.transparency;
        float x2 = this.x;
        float y2 = this.y;
        particle.offsetY = Animation.CurveTimeline.LINEAR;
        particle.offsetX = Animation.CurveTimeline.LINEAR;
        if (this.xOffsetValue.active) {
            particle.offsetX = this.xOffsetValue.newLowValue();
        }
        if (this.yOffsetValue.active) {
            particle.offsetY = this.yOffsetValue.newLowValue();
        }
        switch ($SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnShape()[this.spawnShapeValue.shape.ordinal()]) {
            case 2:
                float width = this.spawnWidth + (this.spawnWidthDiff * this.spawnWidthValue.getScale(percent));
                float height = this.spawnHeight + (this.spawnHeightDiff * this.spawnHeightValue.getScale(percent));
                if (width == Animation.CurveTimeline.LINEAR) {
                    y2 += MathUtils.random() * height;
                    break;
                } else {
                    float lineX = width * MathUtils.random();
                    x2 += lineX;
                    y2 += (height / width) * lineX;
                    break;
                }
            case 3:
                float width2 = this.spawnWidth + (this.spawnWidthDiff * this.spawnWidthValue.getScale(percent));
                float height2 = this.spawnHeight + (this.spawnHeightDiff * this.spawnHeightValue.getScale(percent));
                x2 += MathUtils.random(width2) - (width2 / 2.0f);
                y2 += MathUtils.random(height2) - (height2 / 2.0f);
                break;
            case 4:
                float width3 = this.spawnWidth + (this.spawnWidthDiff * this.spawnWidthValue.getScale(percent));
                float radiusX = width3 / 2.0f;
                float radiusY = (this.spawnHeight + (this.spawnHeightDiff * this.spawnHeightValue.getScale(percent))) / 2.0f;
                if (!(radiusX == Animation.CurveTimeline.LINEAR || radiusY == Animation.CurveTimeline.LINEAR)) {
                    float scaleY = radiusX / radiusY;
                    if (!this.spawnShapeValue.edges) {
                        float radius2 = radiusX * radiusX;
                        do {
                            px = MathUtils.random(width3) - radiusX;
                            py = MathUtils.random(width3) - radiusX;
                        } while ((px * px) + (py * py) > radius2);
                        x2 += px;
                        y2 += py / scaleY;
                        break;
                    } else {
                        switch ($SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$ParticleEmitter$SpawnEllipseSide()[this.spawnShapeValue.side.ordinal()]) {
                            case 2:
                                spawnAngle = -MathUtils.random(179.0f);
                                break;
                            case 3:
                                spawnAngle = MathUtils.random(179.0f);
                                break;
                            default:
                                spawnAngle = MathUtils.random(360.0f);
                                break;
                        }
                        float cosDeg = MathUtils.cosDeg(spawnAngle);
                        float sinDeg = MathUtils.sinDeg(spawnAngle);
                        x2 += cosDeg * radiusX;
                        y2 += (sinDeg * radiusX) / scaleY;
                        if ((updateFlags2 & 4) == 0) {
                            particle.angle = spawnAngle;
                            particle.angleCos = cosDeg;
                            particle.angleSin = sinDeg;
                            break;
                        }
                    }
                }
                break;
        }
        particle.setBounds(x2 - (spriteWidth / 2.0f), y2 - (spriteHeight / 2.0f), this.sprite.getWidth(), this.sprite.getHeight());
        int offsetTime = (int) (((float) this.lifeOffset) + (((float) this.lifeOffsetDiff) * this.lifeOffsetValue.getScale(percent)));
        if (offsetTime > 0) {
            if (offsetTime >= particle.currentLife) {
                offsetTime = particle.currentLife - 1;
            }
            updateParticle(particle, ((float) offsetTime) / 1000.0f, offsetTime);
        }
    }

    private boolean updateParticle(Particle particle, float delta, int deltaMillis) {
        float[] color;
        float velocityX;
        float velocityY;
        int life2 = particle.currentLife - deltaMillis;
        if (life2 <= 0) {
            return false;
        }
        particle.currentLife = life2;
        float percent = 1.0f - (((float) particle.currentLife) / ((float) particle.life));
        int updateFlags2 = this.updateFlags;
        float xScale = particle.xScale + (particle.xScaleDiff * this.xScaleValue.getScale(percent));
        if (isScaleLink()) {
            particle.setScale(this.xScaleFactor * xScale, this.yScaleFactor * xScale);
        } else {
            particle.setScale(this.xScaleFactor * xScale, this.yScaleFactor * (particle.yScale + (particle.yScaleDiff * this.yScaleValue.getScale(percent))));
        }
        if ((updateFlags2 & 16) != 0) {
            float velocity = (particle.velocity + (particle.velocityDiff * this.velocityValue.getScale(percent))) * delta;
            if ((updateFlags2 & 4) != 0) {
                float angle = particle.angle + (particle.angleDiff * this.angleValue.getScale(percent));
                velocityX = velocity * MathUtils.cosDeg(angle);
                velocityY = velocity * MathUtils.sinDeg(angle);
                if ((updateFlags2 & 8) != 0) {
                    float rotation = particle.rotation + (particle.rotationDiff * this.rotationValue.getScale(percent));
                    if (this.aligned) {
                        rotation += angle;
                    }
                    particle.setRotation(rotation);
                }
            } else {
                velocityX = velocity * particle.angleCos;
                velocityY = velocity * particle.angleSin;
                if (this.aligned || (updateFlags2 & 8) != 0) {
                    float rotation2 = particle.rotation + (particle.rotationDiff * this.rotationValue.getScale(percent));
                    if (this.aligned) {
                        rotation2 += particle.angle;
                    }
                    particle.setRotation(rotation2);
                }
            }
            if ((updateFlags2 & 32) != 0) {
                velocityX += (particle.wind + (particle.windDiff * this.windValue.getScale(percent))) * delta;
            }
            if ((updateFlags2 & 64) != 0) {
                velocityY += (particle.gravity + (particle.gravityDiff * this.gravityValue.getScale(percent))) * delta;
            }
            particle.translate(this.xScaleFactor * velocityX, this.yScaleFactor * velocityY);
        } else if ((updateFlags2 & 8) != 0) {
            particle.setRotation(particle.rotation + (particle.rotationDiff * this.rotationValue.getScale(percent)));
        }
        if ((updateFlags2 & 128) != 0) {
            color = this.tintValue.getColor(percent);
        } else {
            color = particle.tint;
        }
        particle.setColor(color[0], color[1], color[2], particle.transparency + (particle.transparencyDiff * this.transparencyValue.getScale(percent)));
        if (this.premultipliedAlpha) {
            float alphaMultiplier = (float) (this.additive ? 0 : 1);
            float a = particle.transparency + (particle.transparencyDiff * this.transparencyValue.getScale(percent));
            particle.setColor(color[0] * a, color[1] * a, color[2] * a, a * alphaMultiplier);
        } else {
            particle.setColor(color[0], color[1], color[2], particle.transparency + (particle.transparencyDiff * this.transparencyValue.getScale(percent)));
        }
        return true;
    }

    public void setPosition(float x2, float y2) {
        if (this.attached) {
            float xAmount = x2 - this.x;
            float yAmount = y2 - this.y;
            boolean[] active2 = this.active;
            int n = active2.length;
            for (int i = 0; i < n; i++) {
                if (active2[i]) {
                    this.particles[i].translate(xAmount, yAmount);
                }
            }
        }
        this.x = x2;
        this.y = y2;
    }

    public void setSprite(Sprite sprite2) {
        this.sprite = sprite2;
        if (sprite2 != null) {
            this.spriteW = (float) sprite2.getRegionWidth();
            this.spriteH = (float) sprite2.getRegionHeight();
            if (!((this.spriteTransMode & 1) == 0 && (this.spriteTransMode & 4) == 0)) {
                this.spriteW *= 2.0f;
            }
            if (!((this.spriteTransMode & 2) == 0 && (this.spriteTransMode & 4) == 0)) {
                this.spriteH *= 2.0f;
            }
            float originX = sprite2.getOriginX();
            float originY = sprite2.getOriginY();
            Texture texture = sprite2.getTexture();
            int i = 0;
            int n = this.particles.length;
            while (i < n) {
                Particle particle = this.particles[i];
                if (particle != null) {
                    particle.setTexture(texture);
                    particle.setOrigin(originX, originY);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public void allowCompletion() {
        this.allowCompletion = true;
        this.durationTimer = this.duration;
    }

    public Sprite getSprite() {
        return this.sprite;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public ScaledNumericValue getLife() {
        return this.lifeValue;
    }

    public ScaledNumericValue getXScale() {
        return this.xScaleValue;
    }

    public ScaledNumericValue getYScale() {
        return this.yScaleValue;
    }

    public ScaledNumericValue getRotation() {
        return this.rotationValue;
    }

    public GradientColorValue getTint() {
        return this.tintValue;
    }

    public ScaledNumericValue getVelocity() {
        return this.velocityValue;
    }

    public ScaledNumericValue getWind() {
        return this.windValue;
    }

    public ScaledNumericValue getGravity() {
        return this.gravityValue;
    }

    public ScaledNumericValue getAngle() {
        return this.angleValue;
    }

    public ScaledNumericValue getEmission() {
        return this.emissionValue;
    }

    public ScaledNumericValue getTransparency() {
        return this.transparencyValue;
    }

    public RangedNumericValue getDuration() {
        return this.durationValue;
    }

    public RangedNumericValue getDelay() {
        return this.delayValue;
    }

    public ScaledNumericValue getLifeOffset() {
        return this.lifeOffsetValue;
    }

    public RangedNumericValue getXOffsetValue() {
        return this.xOffsetValue;
    }

    public RangedNumericValue getYOffsetValue() {
        return this.yOffsetValue;
    }

    public ScaledNumericValue getSpawnWidth() {
        return this.spawnWidthValue;
    }

    public ScaledNumericValue getSpawnHeight() {
        return this.spawnHeightValue;
    }

    public SpawnShapeValue getSpawnShape() {
        return this.spawnShapeValue;
    }

    public boolean isAttached() {
        return this.attached;
    }

    public void setAttached(boolean attached2) {
        this.attached = attached2;
    }

    public boolean isContinuous() {
        return this.continuous;
    }

    public void setContinuous(boolean continuous2) {
        this.continuous = continuous2;
    }

    public boolean isAligned() {
        return this.aligned;
    }

    public void setAligned(boolean aligned2) {
        this.aligned = aligned2;
    }

    public boolean isAdditive() {
        return this.additive;
    }

    public void setAdditive(boolean additive2) {
        this.additive = additive2;
    }

    public boolean isFixFrame() {
        return this.fixFrame;
    }

    public void setFixFrame(boolean fixFrame2) {
        this.fixFrame = fixFrame2;
    }

    public boolean isScaleLink() {
        return this.scaleLink;
    }

    public void setScaleLink(boolean scaleLink2) {
        this.scaleLink = scaleLink2;
    }

    public int getMinParticleCount() {
        return this.minParticleCount;
    }

    public void setMinParticleCount(int minParticleCount2) {
        this.minParticleCount = minParticleCount2;
    }

    public int getMaxParticleCount() {
        return this.maxParticleCount;
    }

    public boolean isComplete() {
        if (this.delayTimer >= this.delay && this.durationTimer >= this.duration && this.activeCount == 0) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public float getPercentComplete() {
        if (this.delayTimer < this.delay) {
            return Animation.CurveTimeline.LINEAR;
        }
        return Math.min(1.0f, this.durationTimer / this.duration);
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public int getActiveCount() {
        return this.activeCount;
    }

    public String getImagePath() {
        return this.imagePath;
    }

    public void setImagePath(String imagePath2) {
        this.imagePath = imagePath2;
    }

    public void setFlip(boolean flipX2, boolean flipY2) {
        this.flipX = flipX2;
        this.flipY = flipY2;
        if (this.particles != null) {
            for (Particle particle : this.particles) {
                if (particle != null) {
                    particle.flip(flipX2, flipY2);
                }
            }
        }
    }

    public void flipY() {
        this.angleValue.setHigh(-this.angleValue.getHighMin(), -this.angleValue.getHighMax());
        this.angleValue.setLow(-this.angleValue.getLowMin(), -this.angleValue.getLowMax());
        this.gravityValue.setHigh(-this.gravityValue.getHighMin(), -this.gravityValue.getHighMax());
        this.gravityValue.setLow(-this.gravityValue.getLowMin(), -this.gravityValue.getLowMax());
        this.windValue.setHigh(-this.windValue.getHighMin(), -this.windValue.getHighMax());
        this.windValue.setLow(-this.windValue.getLowMin(), -this.windValue.getLowMax());
        this.rotationValue.setHigh(-this.rotationValue.getHighMin(), -this.rotationValue.getHighMax());
        this.rotationValue.setLow(-this.rotationValue.getLowMin(), -this.rotationValue.getLowMax());
        this.yOffsetValue.setLow(-this.yOffsetValue.getLowMin(), -this.yOffsetValue.getLowMax());
    }

    public BoundingBox getBoundingBox() {
        if (this.bounds == null) {
            this.bounds = new BoundingBox();
        }
        Particle[] particles2 = this.particles;
        boolean[] active2 = this.active;
        BoundingBox bounds2 = this.bounds;
        bounds2.inf();
        int n = active2.length;
        for (int i = 0; i < n; i++) {
            if (active2[i]) {
                Rectangle r = particles2[i].getBoundingRectangle();
                bounds2.ext(r.x, r.y, Animation.CurveTimeline.LINEAR);
                bounds2.ext(r.x + r.width, r.y + r.height, Animation.CurveTimeline.LINEAR);
            }
        }
        return bounds2;
    }

    private static void writeString(DataOutputStream dos, String s) throws IOException {
        if (s == null) {
            dos.writeShort(0);
            return;
        }
        byte[] buff = s.trim().getBytes("UTF-8");
        dos.writeShort(buff.length);
        dos.write(buff);
    }

    private static String readString(DataInputStream dis) throws IOException {
        int len = dis.readShort();
        if (len == 0) {
            return null;
        }
        byte[] buff = new byte[len];
        dis.read(buff);
        return new String(buff, "UTF-8");
    }

    public void save_bin(DataOutputStream output) throws IOException {
        output.writeByte(8);
        writeString(output, this.name);
        this.delayValue.save_bin(output);
        this.durationValue.save_bin(output);
        output.writeInt(this.minParticleCount);
        output.writeInt(this.maxParticleCount);
        this.emissionValue.save_bin(output);
        this.lifeValue.save_bin(output);
        this.lifeOffsetValue.save_bin(output);
        this.xOffsetValue.save_bin(output);
        this.yOffsetValue.save_bin(output);
        this.spawnShapeValue.save_bin(output);
        this.spawnWidthValue.save_bin(output);
        this.spawnHeightValue.save_bin(output);
        this.xScaleValue.save_bin(output);
        this.yScaleValue.save_bin(output);
        this.velocityValue.save_bin(output);
        this.angleValue.save_bin(output);
        this.rotationValue.save_bin(output);
        this.windValue.save_bin(output);
        this.gravityValue.save_bin(output);
        this.tintValue.save_bin(output);
        this.transparencyValue.save_bin(output);
        output.writeBoolean(this.attached);
        output.writeBoolean(this.continuous);
        output.writeBoolean(this.aligned);
        output.writeBoolean(this.additive);
        output.writeBoolean(this.scaleLink);
        output.writeBoolean(this.fixFrame);
        output.writeBoolean(this.premultipliedAlpha);
        output.writeInt(this.spriteTransMode);
        writeString(output, this.imagePath);
    }

    public void save(Writer output) throws IOException {
        output.write("version: v6\n");
        output.write(String.valueOf(this.name) + "\n");
        output.write("- Delay -\n");
        this.delayValue.save(output);
        output.write("- Duration - \n");
        this.durationValue.save(output);
        output.write("- Count - \n");
        output.write("min: " + this.minParticleCount + "\n");
        output.write("max: " + this.maxParticleCount + "\n");
        output.write("- Emission - \n");
        this.emissionValue.save(output);
        output.write("- Life - \n");
        this.lifeValue.save(output);
        output.write("- Life Offset - \n");
        this.lifeOffsetValue.save(output);
        output.write("- X Offset - \n");
        this.xOffsetValue.save(output);
        output.write("- Y Offset - \n");
        this.yOffsetValue.save(output);
        output.write("- Spawn Shape - \n");
        this.spawnShapeValue.save(output);
        output.write("- Spawn Width - \n");
        this.spawnWidthValue.save(output);
        output.write("- Spawn Height - \n");
        this.spawnHeightValue.save(output);
        output.write("- X Scale - \n");
        this.xScaleValue.save(output);
        output.write("- Y Scale - \n");
        this.yScaleValue.save(output);
        output.write("- Velocity - \n");
        this.velocityValue.save(output);
        output.write("- Angle - \n");
        this.angleValue.save(output);
        output.write("- Rotation - \n");
        this.rotationValue.save(output);
        output.write("- Wind - \n");
        this.windValue.save(output);
        output.write("- Gravity - \n");
        this.gravityValue.save(output);
        output.write("- Tint - \n");
        this.tintValue.save(output);
        output.write("- Transparency - \n");
        this.transparencyValue.save(output);
        output.write("- Options - \n");
        output.write("attached: " + this.attached + "\n");
        output.write("continuous: " + this.continuous + "\n");
        output.write("aligned: " + this.aligned + "\n");
        output.write("additive: " + this.additive + "\n");
        output.write("scaleLink: " + this.scaleLink + "\n");
        output.write("fixFrame: " + this.fixFrame + "\n");
    }

    private void load_bin_v7(DataInputStream reader) throws IOException {
        this.name = readString(reader);
        this.delayValue.load_bin(reader);
        this.durationValue.load_bin(reader);
        setMinParticleCount(reader.readInt());
        setMaxParticleCount(reader.readInt());
        this.emissionValue.load_bin(reader);
        this.lifeValue.load_bin(reader);
        this.lifeOffsetValue.load_bin(reader);
        this.xOffsetValue.load_bin(reader);
        this.yOffsetValue.load_bin(reader);
        this.spawnShapeValue.load_bin(reader);
        this.spawnWidthValue.load_bin(reader);
        this.spawnHeightValue.load_bin(reader);
        this.xScaleValue.load_bin(reader);
        this.yScaleValue.load_bin(reader);
        this.velocityValue.load_bin(reader);
        this.angleValue.load_bin(reader);
        this.rotationValue.load_bin(reader);
        this.windValue.load_bin(reader);
        this.gravityValue.load_bin(reader);
        this.tintValue.load_bin(reader);
        this.transparencyValue.load_bin(reader);
        this.attached = reader.readBoolean();
        this.continuous = reader.readBoolean();
        this.aligned = reader.readBoolean();
        this.additive = reader.readBoolean();
        this.scaleLink = reader.readBoolean();
        this.fixFrame = reader.readBoolean();
        this.premultipliedAlpha = reader.readBoolean();
        this.imagePath = readString(reader);
    }

    private void load_bin_v8(DataInputStream reader) throws IOException {
        this.name = readString(reader);
        this.delayValue.load_bin(reader);
        this.durationValue.load_bin(reader);
        setMinParticleCount(reader.readInt());
        setMaxParticleCount(reader.readInt());
        this.emissionValue.load_bin(reader);
        this.lifeValue.load_bin(reader);
        this.lifeOffsetValue.load_bin(reader);
        this.xOffsetValue.load_bin(reader);
        this.yOffsetValue.load_bin(reader);
        this.spawnShapeValue.load_bin(reader);
        this.spawnWidthValue.load_bin(reader);
        this.spawnHeightValue.load_bin(reader);
        this.xScaleValue.load_bin(reader);
        this.yScaleValue.load_bin(reader);
        this.velocityValue.load_bin(reader);
        this.angleValue.load_bin(reader);
        this.rotationValue.load_bin(reader);
        this.windValue.load_bin(reader);
        this.gravityValue.load_bin(reader);
        this.tintValue.load_bin(reader);
        this.transparencyValue.load_bin(reader);
        this.attached = reader.readBoolean();
        this.continuous = reader.readBoolean();
        this.aligned = reader.readBoolean();
        this.additive = reader.readBoolean();
        this.scaleLink = reader.readBoolean();
        this.fixFrame = reader.readBoolean();
        this.premultipliedAlpha = reader.readBoolean();
        this.spriteTransMode = reader.readInt();
        this.imagePath = readString(reader);
    }

    /* access modifiers changed from: package-private */
    public void load_v6(BufferedReader reader) throws IOException {
        this.name = readString(reader, "name");
        reader.readLine();
        this.delayValue.load(reader);
        reader.readLine();
        this.durationValue.load(reader);
        reader.readLine();
        setMinParticleCount(readInt(reader, "minParticleCount"));
        setMaxParticleCount(readInt(reader, "maxParticleCount"));
        reader.readLine();
        this.emissionValue.load(reader);
        reader.readLine();
        this.lifeValue.load(reader);
        reader.readLine();
        this.lifeOffsetValue.load(reader);
        reader.readLine();
        this.xOffsetValue.load(reader);
        reader.readLine();
        this.yOffsetValue.load(reader);
        reader.readLine();
        this.spawnShapeValue.load(reader);
        reader.readLine();
        this.spawnWidthValue.load(reader);
        reader.readLine();
        this.spawnHeightValue.load(reader);
        reader.readLine();
        this.xScaleValue.load(reader);
        reader.readLine();
        this.yScaleValue.load(reader);
        reader.readLine();
        this.velocityValue.load(reader);
        reader.readLine();
        this.angleValue.load(reader);
        reader.readLine();
        this.rotationValue.load(reader);
        reader.readLine();
        this.windValue.load(reader);
        reader.readLine();
        this.gravityValue.load(reader);
        reader.readLine();
        this.tintValue.load(reader);
        reader.readLine();
        this.transparencyValue.load(reader);
        reader.readLine();
        this.attached = readBoolean(reader, "attached");
        this.continuous = readBoolean(reader, "continuous");
        this.aligned = readBoolean(reader, "aligned");
        this.additive = readBoolean(reader, "additive");
        this.scaleLink = readBoolean(reader, "scaleLink");
        this.fixFrame = readBoolean(reader, "fixFrame");
    }

    /* access modifiers changed from: package-private */
    public void load_v5(BufferedReader reader) throws IOException {
        reader.readLine();
        this.delayValue.load(reader);
        reader.readLine();
        this.durationValue.load(reader);
        reader.readLine();
        setMinParticleCount(readInt(reader, "minParticleCount"));
        setMaxParticleCount(readInt(reader, "maxParticleCount"));
        reader.readLine();
        this.emissionValue.load(reader);
        reader.readLine();
        this.lifeValue.load(reader);
        reader.readLine();
        this.lifeOffsetValue.load(reader);
        reader.readLine();
        this.xOffsetValue.load(reader);
        reader.readLine();
        this.yOffsetValue.load(reader);
        reader.readLine();
        this.spawnShapeValue.load(reader);
        reader.readLine();
        this.spawnWidthValue.load(reader);
        reader.readLine();
        this.spawnHeightValue.load(reader);
        reader.readLine();
        this.xScaleValue.load(reader);
        this.yScaleValue.load(this.xScaleValue);
        reader.readLine();
        this.velocityValue.load(reader);
        reader.readLine();
        this.angleValue.load(reader);
        reader.readLine();
        this.rotationValue.load(reader);
        reader.readLine();
        this.windValue.load(reader);
        reader.readLine();
        this.gravityValue.load(reader);
        reader.readLine();
        this.tintValue.load(reader);
        reader.readLine();
        this.transparencyValue.load(reader);
        reader.readLine();
        this.attached = readBoolean(reader, "attached");
        this.continuous = readBoolean(reader, "continuous");
        this.aligned = readBoolean(reader, "aligned");
        this.additive = readBoolean(reader, "additive");
        this.scaleLink = readBoolean(reader, "behind");
        this.scaleLink = true;
    }

    public void load(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        String version = readVesion(line);
        if (version == null) {
            this.name = line;
            load_v5(reader);
        } else if (version.equals("v6")) {
            load_v6(reader);
        }
    }

    private void load_bin(DataInputStream reader) throws IOException {
        switch (reader.readByte()) {
            case 7:
                load_bin_v7(reader);
                return;
            case 8:
                load_bin_v8(reader);
                return;
            default:
                return;
        }
    }

    static String readVesion(String line) throws IOException {
        if (line == null) {
            throw new IOException("Missing Vesion: ");
        } else if (!line.startsWith("version:")) {
            return null;
        } else {
            return line.substring(line.indexOf(":") + 1).trim();
        }
    }

    static String readString(BufferedReader reader, String name2) throws IOException {
        String line = reader.readLine();
        if (line != null) {
            return line.substring(line.indexOf(":") + 1).trim();
        }
        throw new IOException("Missing value: " + name2);
    }

    static boolean readBoolean(BufferedReader reader, String name2) throws IOException {
        return Boolean.parseBoolean(readString(reader, name2));
    }

    static int readInt(BufferedReader reader, String name2) throws IOException {
        return Integer.parseInt(readString(reader, name2));
    }

    static float readFloat(BufferedReader reader, String name2) throws IOException {
        return Float.parseFloat(readString(reader, name2));
    }

    private class Particle extends Sprite {
        protected float angle;
        protected float angleCos;
        protected float angleDiff;
        protected float angleSin;
        protected int currentLife;
        protected float gravity;
        protected float gravityDiff;
        protected int life;
        float offsetX;
        float offsetY;
        protected float rotation;
        protected float rotationDiff;
        protected float[] tint;
        protected float transparency;
        protected float transparencyDiff;
        protected float velocity;
        protected float velocityDiff;
        protected float wind;
        protected float windDiff;
        protected float xScale;
        protected float xScaleDiff;
        protected float yScale;
        protected float yScaleDiff;

        public void draw(Batch batch, float alphaModulation) {
            if (alphaModulation == 1.0f) {
                draw(batch);
                return;
            }
            float oldAlpha = getColor().a;
            setAlpha(oldAlpha * alphaModulation);
            draw(batch);
            setAlpha(oldAlpha);
        }

        public void draw(Batch batch) {
            float rotate = getRotation();
            float oldX = getX();
            float oldY = getY();
            if (rotate != Animation.CurveTimeline.LINEAR && ParticleEmitter.this.xScaleFactor * ParticleEmitter.this.yScaleFactor < Animation.CurveTimeline.LINEAR) {
                setRotation(360.0f - rotate);
            }
            if (ParticleEmitter.this.xScaleFactor < Animation.CurveTimeline.LINEAR) {
                setX(oldX - this.offsetX);
            } else {
                setX(this.offsetX + oldX);
            }
            if (ParticleEmitter.this.yScaleFactor < Animation.CurveTimeline.LINEAR) {
                setY(oldY - this.offsetY);
            } else {
                setY(this.offsetY + oldY);
            }
            if (ParticleEmitter.this.spriteTransMode == 0) {
                super.draw(batch);
            } else {
                float w = ParticleEmitter.this.spriteW / 2.0f;
                float h = ParticleEmitter.this.spriteH / 2.0f;
                setOrigin(w, h);
                super.draw(batch);
                if ((ParticleEmitter.this.spriteTransMode & 1) != 0) {
                    setOrigin(Animation.CurveTimeline.LINEAR, h);
                    translate(w, Animation.CurveTimeline.LINEAR);
                    flip(true, false);
                    super.draw(batch);
                    translate(-w, Animation.CurveTimeline.LINEAR);
                    flip(true, false);
                }
                if ((ParticleEmitter.this.spriteTransMode & 2) != 0) {
                    setOrigin(w, Animation.CurveTimeline.LINEAR);
                    translate(Animation.CurveTimeline.LINEAR, h);
                    flip(false, true);
                    super.draw(batch);
                    flip(false, true);
                    translate(Animation.CurveTimeline.LINEAR, -h);
                }
                if ((ParticleEmitter.this.spriteTransMode & 4) != 0) {
                    setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    translate(w, h);
                    flip(true, true);
                    super.draw(batch);
                    translate(-w, -h);
                    flip(true, true);
                }
            }
            setRotation(rotate);
            setPosition(oldX, oldY);
        }

        public Particle(Sprite sprite) {
            super(sprite);
        }
    }

    public static class ParticleValue {
        boolean active;
        boolean alwaysActive;

        public void setAlwaysActive(boolean alwaysActive2) {
            this.alwaysActive = alwaysActive2;
        }

        public boolean isAlwaysActive() {
            return this.alwaysActive;
        }

        public boolean isActive() {
            return this.alwaysActive || this.active;
        }

        public void setActive(boolean active2) {
            this.active = active2;
        }

        public void save_bin(DataOutputStream output) throws IOException {
            if (!this.alwaysActive) {
                output.writeBoolean(this.active);
            } else {
                this.active = true;
            }
        }

        public void save(Writer output) throws IOException {
            if (!this.alwaysActive) {
                output.write("active: " + this.active + "\n");
            } else {
                this.active = true;
            }
        }

        public void load_bin(DataInputStream reader) throws IOException {
            if (!this.alwaysActive) {
                this.active = reader.readBoolean();
            } else {
                this.active = true;
            }
        }

        public void load(BufferedReader reader) throws IOException {
            if (!this.alwaysActive) {
                this.active = ParticleEmitter.readBoolean(reader, "active");
            } else {
                this.active = true;
            }
        }

        public void load(ParticleValue value) {
            this.active = value.active;
            this.alwaysActive = value.alwaysActive;
        }
    }

    public static class NumericValue extends ParticleValue {
        private float value;

        public float getValue() {
            return this.value;
        }

        public void setValue(float value2) {
            this.value = value2;
        }

        public void save(Writer output) throws IOException {
            super.save(output);
            if (this.active) {
                output.write("value: " + this.value + "\n");
            }
        }

        public void load(BufferedReader reader) throws IOException {
            super.load(reader);
            if (this.active) {
                this.value = ParticleEmitter.readFloat(reader, "value");
            }
        }

        public void load(NumericValue value2) {
            super.load((ParticleValue) value2);
            this.value = value2.value;
        }
    }

    public static class RangedNumericValue extends ParticleValue {
        private float lowMax;
        private float lowMin;

        public float newLowValue() {
            return this.lowMin + ((this.lowMax - this.lowMin) * MathUtils.random());
        }

        public void setLow(float value) {
            this.lowMin = value;
            this.lowMax = value;
        }

        public void setLow(float min, float max) {
            this.lowMin = min;
            this.lowMax = max;
        }

        public float getLowMin() {
            return this.lowMin;
        }

        public void setLowMin(float lowMin2) {
            this.lowMin = lowMin2;
        }

        public float getLowMax() {
            return this.lowMax;
        }

        public void setLowMax(float lowMax2) {
            this.lowMax = lowMax2;
        }

        public void save(Writer output) throws IOException {
            super.save(output);
            if (this.active) {
                output.write("lowMin: " + this.lowMin + "\n");
                output.write("lowMax: " + this.lowMax + "\n");
            }
        }

        public void save_bin(DataOutputStream output) throws IOException {
            super.save_bin(output);
            if (this.active) {
                output.writeFloat(this.lowMin);
                output.writeFloat(this.lowMax);
            }
        }

        public void load_bin(DataInputStream reader) throws IOException {
            super.load_bin(reader);
            if (this.active) {
                this.lowMin = reader.readFloat();
                this.lowMax = reader.readFloat();
            }
        }

        public void load(BufferedReader reader) throws IOException {
            super.load(reader);
            if (this.active) {
                this.lowMin = ParticleEmitter.readFloat(reader, "lowMin");
                this.lowMax = ParticleEmitter.readFloat(reader, "lowMax");
            }
        }

        public void load(RangedNumericValue value) {
            super.load((ParticleValue) value);
            this.lowMax = value.lowMax;
            this.lowMin = value.lowMin;
        }
    }

    public static class ScaledNumericValue extends RangedNumericValue {
        private float highMax;
        private float highMin;
        private boolean relative;
        private float[] scaling = {1.0f};
        float[] timeline = {0.0f};

        public float newHighValue() {
            return this.highMin + ((this.highMax - this.highMin) * MathUtils.random());
        }

        public void setHigh(float value) {
            this.highMin = value;
            this.highMax = value;
        }

        public void setHigh(float min, float max) {
            this.highMin = min;
            this.highMax = max;
        }

        public float getHighMin() {
            return this.highMin;
        }

        public void setHighMin(float highMin2) {
            this.highMin = highMin2;
        }

        public float getHighMax() {
            return this.highMax;
        }

        public void setHighMax(float highMax2) {
            this.highMax = highMax2;
        }

        public float[] getScaling() {
            return this.scaling;
        }

        public void setScaling(float[] values) {
            this.scaling = values;
        }

        public float[] getTimeline() {
            return this.timeline;
        }

        public void setTimeline(float[] timeline2) {
            this.timeline = timeline2;
        }

        public boolean isRelative() {
            return this.relative;
        }

        public void setRelative(boolean relative2) {
            this.relative = relative2;
        }

        public float getScale(float percent) {
            int endIndex = -1;
            float[] timeline2 = this.timeline;
            int n = timeline2.length;
            int i = 1;
            while (true) {
                if (i >= n) {
                    break;
                } else if (timeline2[i] > percent) {
                    endIndex = i;
                    break;
                } else {
                    i++;
                }
            }
            if (endIndex == -1) {
                return this.scaling[n - 1];
            }
            float[] scaling2 = this.scaling;
            int startIndex = endIndex - 1;
            float startValue = scaling2[startIndex];
            float startTime = timeline2[startIndex];
            return ((scaling2[endIndex] - startValue) * ((percent - startTime) / (timeline2[endIndex] - startTime))) + startValue;
        }

        public void save_bin(DataOutputStream output) throws IOException {
            super.save_bin(output);
            if (this.active) {
                output.writeFloat(this.highMin);
                output.writeFloat(this.highMax);
                output.writeBoolean(this.relative);
                output.writeShort(this.scaling.length);
                for (float writeFloat : this.scaling) {
                    output.writeFloat(writeFloat);
                }
                output.writeShort(this.timeline.length);
                for (float writeFloat2 : this.timeline) {
                    output.writeFloat(writeFloat2);
                }
            }
        }

        public void save(Writer output) throws IOException {
            super.save(output);
            if (this.active) {
                output.write("highMin: " + this.highMin + "\n");
                output.write("highMax: " + this.highMax + "\n");
                output.write("relative: " + this.relative + "\n");
                output.write("scalingCount: " + this.scaling.length + "\n");
                for (int i = 0; i < this.scaling.length; i++) {
                    output.write("scaling" + i + ": " + this.scaling[i] + "\n");
                }
                output.write("timelineCount: " + this.timeline.length + "\n");
                for (int i2 = 0; i2 < this.timeline.length; i2++) {
                    output.write("timeline" + i2 + ": " + this.timeline[i2] + "\n");
                }
            }
        }

        public void load_bin(DataInputStream reader) throws IOException {
            super.load_bin(reader);
            if (this.active) {
                this.highMin = reader.readFloat();
                this.highMax = reader.readFloat();
                this.relative = reader.readBoolean();
                this.scaling = new float[reader.readShort()];
                for (int i = 0; i < this.scaling.length; i++) {
                    this.scaling[i] = reader.readFloat();
                }
                this.timeline = new float[reader.readShort()];
                for (int i2 = 0; i2 < this.timeline.length; i2++) {
                    this.timeline[i2] = reader.readFloat();
                }
            }
        }

        public void load(BufferedReader reader) throws IOException {
            super.load(reader);
            if (this.active) {
                this.highMin = ParticleEmitter.readFloat(reader, "highMin");
                this.highMax = ParticleEmitter.readFloat(reader, "highMax");
                this.relative = ParticleEmitter.readBoolean(reader, "relative");
                this.scaling = new float[ParticleEmitter.readInt(reader, "scalingCount")];
                for (int i = 0; i < this.scaling.length; i++) {
                    this.scaling[i] = ParticleEmitter.readFloat(reader, "scaling" + i);
                }
                this.timeline = new float[ParticleEmitter.readInt(reader, "timelineCount")];
                for (int i2 = 0; i2 < this.timeline.length; i2++) {
                    this.timeline[i2] = ParticleEmitter.readFloat(reader, "timeline" + i2);
                }
            }
        }

        public void load(ScaledNumericValue value) {
            super.load((RangedNumericValue) value);
            this.highMax = value.highMax;
            this.highMin = value.highMin;
            this.scaling = new float[value.scaling.length];
            System.arraycopy(value.scaling, 0, this.scaling, 0, this.scaling.length);
            this.timeline = new float[value.timeline.length];
            System.arraycopy(value.timeline, 0, this.timeline, 0, this.timeline.length);
            this.relative = value.relative;
        }
    }

    public static class GradientColorValue extends ParticleValue {
        private static float[] temp = new float[4];
        private float[] colors = {1.0f, 1.0f, 1.0f};
        float[] timeline = {0.0f};

        public GradientColorValue() {
            this.alwaysActive = true;
        }

        public float[] getTimeline() {
            return this.timeline;
        }

        public void setTimeline(float[] timeline2) {
            this.timeline = timeline2;
        }

        public float[] getColors() {
            return this.colors;
        }

        public void setColors(float[] colors2) {
            this.colors = colors2;
        }

        public float[] getColor(float percent) {
            int startIndex = 0;
            int endIndex = -1;
            float[] timeline2 = this.timeline;
            int n = timeline2.length;
            int i = 1;
            while (true) {
                if (i >= n) {
                    break;
                } else if (timeline2[i] > percent) {
                    endIndex = i;
                    break;
                } else {
                    startIndex = i;
                    i++;
                }
            }
            float startTime = timeline2[startIndex];
            int startIndex2 = startIndex * 3;
            float r1 = this.colors[startIndex2];
            float g1 = this.colors[startIndex2 + 1];
            float b1 = this.colors[startIndex2 + 2];
            if (endIndex == -1) {
                temp[0] = r1;
                temp[1] = g1;
                temp[2] = b1;
                return temp;
            }
            float factor = (percent - startTime) / (timeline2[endIndex] - startTime);
            int endIndex2 = endIndex * 3;
            temp[0] = ((this.colors[endIndex2] - r1) * factor) + r1;
            temp[1] = ((this.colors[endIndex2 + 1] - g1) * factor) + g1;
            temp[2] = ((this.colors[endIndex2 + 2] - b1) * factor) + b1;
            return temp;
        }

        public void save_bin(DataOutputStream output) throws IOException {
            super.save_bin(output);
            if (this.active) {
                output.writeShort(this.colors.length);
                for (float writeFloat : this.colors) {
                    output.writeFloat(writeFloat);
                }
                output.writeShort(this.timeline.length);
                for (float writeFloat2 : this.timeline) {
                    output.writeFloat(writeFloat2);
                }
            }
        }

        public void save(Writer output) throws IOException {
            super.save(output);
            if (this.active) {
                output.write("colorsCount: " + this.colors.length + "\n");
                for (int i = 0; i < this.colors.length; i++) {
                    output.write("colors" + i + ": " + this.colors[i] + "\n");
                }
                output.write("timelineCount: " + this.timeline.length + "\n");
                for (int i2 = 0; i2 < this.timeline.length; i2++) {
                    output.write("timeline" + i2 + ": " + this.timeline[i2] + "\n");
                }
            }
        }

        public void load_bin(DataInputStream reader) throws IOException {
            super.load_bin(reader);
            if (this.active) {
                this.colors = new float[reader.readShort()];
                for (int i = 0; i < this.colors.length; i++) {
                    this.colors[i] = reader.readFloat();
                }
                this.timeline = new float[reader.readShort()];
                for (int i2 = 0; i2 < this.timeline.length; i2++) {
                    this.timeline[i2] = reader.readFloat();
                }
            }
        }

        public void load(BufferedReader reader) throws IOException {
            super.load(reader);
            if (this.active) {
                this.colors = new float[ParticleEmitter.readInt(reader, "colorsCount")];
                for (int i = 0; i < this.colors.length; i++) {
                    this.colors[i] = ParticleEmitter.readFloat(reader, "colors" + i);
                }
                this.timeline = new float[ParticleEmitter.readInt(reader, "timelineCount")];
                for (int i2 = 0; i2 < this.timeline.length; i2++) {
                    this.timeline[i2] = ParticleEmitter.readFloat(reader, "timeline" + i2);
                }
            }
        }

        public void load(GradientColorValue value) {
            super.load((ParticleValue) value);
            this.colors = new float[value.colors.length];
            System.arraycopy(value.colors, 0, this.colors, 0, this.colors.length);
            this.timeline = new float[value.timeline.length];
            System.arraycopy(value.timeline, 0, this.timeline, 0, this.timeline.length);
        }
    }

    public static class SpawnShapeValue extends ParticleValue {
        boolean edges;
        SpawnShape shape = SpawnShape.point;
        SpawnEllipseSide side = SpawnEllipseSide.both;

        public SpawnShape getShape() {
            return this.shape;
        }

        public void setShape(SpawnShape shape2) {
            this.shape = shape2;
        }

        public boolean isEdges() {
            return this.edges;
        }

        public void setEdges(boolean edges2) {
            this.edges = edges2;
        }

        public SpawnEllipseSide getSide() {
            return this.side;
        }

        public void setSide(SpawnEllipseSide side2) {
            this.side = side2;
        }

        public void save_bin(DataOutputStream output) throws IOException {
            super.save_bin(output);
            if (this.active) {
                output.writeByte(this.shape.ordinal());
                if (this.shape == SpawnShape.ellipse) {
                    output.writeBoolean(this.edges);
                    output.writeByte(this.side.ordinal());
                }
            }
        }

        public void save(Writer output) throws IOException {
            super.save(output);
            if (this.active) {
                output.write("shape: " + this.shape + "\n");
                if (this.shape == SpawnShape.ellipse) {
                    output.write("edges: " + this.edges + "\n");
                    output.write("side: " + this.side + "\n");
                }
            }
        }

        public void load_bin(DataInputStream reader) throws IOException {
            super.load_bin(reader);
            if (this.active) {
                this.shape = SpawnShape.values()[reader.readByte()];
                if (this.shape == SpawnShape.ellipse) {
                    this.edges = reader.readBoolean();
                    this.side = SpawnEllipseSide.values()[reader.readByte()];
                }
            }
        }

        public void load(BufferedReader reader) throws IOException {
            super.load(reader);
            if (this.active) {
                this.shape = SpawnShape.valueOf(ParticleEmitter.readString(reader, "shape"));
                if (this.shape == SpawnShape.ellipse) {
                    this.edges = ParticleEmitter.readBoolean(reader, "edges");
                    this.side = SpawnEllipseSide.valueOf(ParticleEmitter.readString(reader, "side"));
                }
            }
        }

        public void load(SpawnShapeValue value) {
            super.load((ParticleValue) value);
            this.shape = value.shape;
            this.edges = value.edges;
            this.side = value.side;
        }
    }
}
