package com.badlogic.gdx.graphics.g3d.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.CubemapAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.DepthTestAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.AmbientCubemap;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class DefaultShader extends BaseShader {
    @Deprecated
    public static int defaultCullFace = GL20.GL_BACK;
    @Deprecated
    public static int defaultDepthFunc = 515;
    private static String defaultFragmentShader = null;
    private static String defaultVertexShader = null;
    protected static long implementedFlags = ((((BlendingAttribute.Type | TextureAttribute.Diffuse) | ColorAttribute.Diffuse) | ColorAttribute.Specular) | FloatAttribute.Shininess);
    private static final long optionalAttributes = (IntAttribute.CullFace | DepthTestAttribute.Type);
    private static final Attributes tmpAttributes = new Attributes();
    protected final AmbientCubemap ambientCubemap;
    protected final long attributesMask;
    private Camera camera;
    protected final Config config;
    protected int dirLightsColorOffset;
    protected int dirLightsDirectionOffset;
    protected int dirLightsLoc;
    protected int dirLightsSize;
    protected final DirectionalLight[] directionalLights;
    protected final boolean environmentCubemap;
    protected final boolean lighting;
    private boolean lightsSet;
    private Matrix3 normalMatrix;
    protected final PointLight[] pointLights;
    protected int pointLightsColorOffset;
    protected int pointLightsIntensityOffset;
    protected int pointLightsLoc;
    protected int pointLightsPositionOffset;
    protected int pointLightsSize;
    private Renderable renderable;
    protected final boolean shadowMap;
    private float time;
    private final Vector3 tmpV1;
    public final int u_alphaTest;
    protected final int u_ambientCubemap;
    public final int u_ambientTexture;
    public final int u_ambientUVTransform;
    public final int u_bones;
    public final int u_cameraDirection;
    public final int u_cameraPosition;
    public final int u_cameraUp;
    public final int u_diffuseColor;
    public final int u_diffuseTexture;
    public final int u_diffuseUVTransform;
    protected final int u_dirLights0color;
    protected final int u_dirLights0direction;
    protected final int u_dirLights1color;
    public final int u_emissiveColor;
    public final int u_emissiveTexture;
    public final int u_emissiveUVTransform;
    protected final int u_environmentCubemap;
    protected final int u_fogColor;
    public final int u_normalMatrix;
    public final int u_normalTexture;
    public final int u_normalUVTransform;
    public final int u_opacity;
    protected final int u_pointLights0color;
    protected final int u_pointLights0intensity;
    protected final int u_pointLights0position;
    protected final int u_pointLights1color;
    public final int u_projTrans;
    public final int u_projViewTrans;
    public final int u_projViewWorldTrans;
    public final int u_reflectionColor;
    public final int u_reflectionTexture;
    public final int u_reflectionUVTransform;
    protected final int u_shadowMapProjViewTrans;
    protected final int u_shadowPCFOffset;
    protected final int u_shadowTexture;
    public final int u_shininess;
    public final int u_specularColor;
    public final int u_specularTexture;
    public final int u_specularUVTransform;
    public final int u_time;
    public final int u_viewTrans;
    public final int u_viewWorldTrans;
    public final int u_worldTrans;
    private long vertexMask;

    public static class Inputs {
        public static final BaseShader.Uniform alphaTest = new BaseShader.Uniform("u_alphaTest");
        public static final BaseShader.Uniform ambientCube = new BaseShader.Uniform("u_ambientCubemap");
        public static final BaseShader.Uniform ambientTexture = new BaseShader.Uniform("u_ambientTexture", TextureAttribute.Ambient);
        public static final BaseShader.Uniform ambientUVTransform = new BaseShader.Uniform("u_ambientUVTransform", TextureAttribute.Ambient);
        public static final BaseShader.Uniform bones = new BaseShader.Uniform("u_bones");
        public static final BaseShader.Uniform cameraDirection = new BaseShader.Uniform("u_cameraDirection");
        public static final BaseShader.Uniform cameraPosition = new BaseShader.Uniform("u_cameraPosition");
        public static final BaseShader.Uniform cameraUp = new BaseShader.Uniform("u_cameraUp");
        public static final BaseShader.Uniform diffuseColor = new BaseShader.Uniform("u_diffuseColor", ColorAttribute.Diffuse);
        public static final BaseShader.Uniform diffuseTexture = new BaseShader.Uniform("u_diffuseTexture", TextureAttribute.Diffuse);
        public static final BaseShader.Uniform diffuseUVTransform = new BaseShader.Uniform("u_diffuseUVTransform", TextureAttribute.Diffuse);
        public static final BaseShader.Uniform dirLights = new BaseShader.Uniform("u_dirLights");
        public static final BaseShader.Uniform emissiveColor = new BaseShader.Uniform("u_emissiveColor", ColorAttribute.Emissive);
        public static final BaseShader.Uniform emissiveTexture = new BaseShader.Uniform("u_emissiveTexture", TextureAttribute.Emissive);
        public static final BaseShader.Uniform emissiveUVTransform = new BaseShader.Uniform("u_emissiveUVTransform", TextureAttribute.Emissive);
        public static final BaseShader.Uniform environmentCubemap = new BaseShader.Uniform("u_environmentCubemap");
        public static final BaseShader.Uniform normalMatrix = new BaseShader.Uniform("u_normalMatrix");
        public static final BaseShader.Uniform normalTexture = new BaseShader.Uniform("u_normalTexture", TextureAttribute.Normal);
        public static final BaseShader.Uniform normalUVTransform = new BaseShader.Uniform("u_normalUVTransform", TextureAttribute.Normal);
        public static final BaseShader.Uniform opacity = new BaseShader.Uniform("u_opacity", BlendingAttribute.Type);
        public static final BaseShader.Uniform pointLights = new BaseShader.Uniform("u_pointLights");
        public static final BaseShader.Uniform projTrans = new BaseShader.Uniform("u_projTrans");
        public static final BaseShader.Uniform projViewTrans = new BaseShader.Uniform("u_projViewTrans");
        public static final BaseShader.Uniform projViewWorldTrans = new BaseShader.Uniform("u_projViewWorldTrans");
        public static final BaseShader.Uniform reflectionColor = new BaseShader.Uniform("u_reflectionColor", ColorAttribute.Reflection);
        public static final BaseShader.Uniform reflectionTexture = new BaseShader.Uniform("u_reflectionTexture", TextureAttribute.Reflection);
        public static final BaseShader.Uniform reflectionUVTransform = new BaseShader.Uniform("u_reflectionUVTransform", TextureAttribute.Reflection);
        public static final BaseShader.Uniform shininess = new BaseShader.Uniform("u_shininess", FloatAttribute.Shininess);
        public static final BaseShader.Uniform specularColor = new BaseShader.Uniform("u_specularColor", ColorAttribute.Specular);
        public static final BaseShader.Uniform specularTexture = new BaseShader.Uniform("u_specularTexture", TextureAttribute.Specular);
        public static final BaseShader.Uniform specularUVTransform = new BaseShader.Uniform("u_specularUVTransform", TextureAttribute.Specular);
        public static final BaseShader.Uniform viewTrans = new BaseShader.Uniform("u_viewTrans");
        public static final BaseShader.Uniform viewWorldTrans = new BaseShader.Uniform("u_viewWorldTrans");
        public static final BaseShader.Uniform worldTrans = new BaseShader.Uniform("u_worldTrans");
    }

    public static class Config {
        public int defaultCullFace = -1;
        public int defaultDepthFunc = -1;
        public String fragmentShader = null;
        public boolean ignoreUnimplemented = true;
        public int numBones = 12;
        public int numDirectionalLights = 2;
        public int numPointLights = 5;
        public int numSpotLights = 0;
        public String vertexShader = null;

        public Config() {
        }

        public Config(String vertexShader2, String fragmentShader2) {
            this.vertexShader = vertexShader2;
            this.fragmentShader = fragmentShader2;
        }
    }

    public static class Setters {
        public static final BaseShader.Setter ambientTexture = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.context.textureBinder.bind(((TextureAttribute) combinedAttributes.get(TextureAttribute.Ambient)).textureDescription));
            }
        };
        public static final BaseShader.Setter ambientUVTransform = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                TextureAttribute ta = (TextureAttribute) combinedAttributes.get(TextureAttribute.Ambient);
                shader.set(inputID, ta.offsetU, ta.offsetV, ta.scaleU, ta.scaleV);
            }
        };
        public static final BaseShader.Setter cameraDirection = new BaseShader.GlobalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.camera.direction);
            }
        };
        public static final BaseShader.Setter cameraPosition = new BaseShader.GlobalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.camera.position.x, shader.camera.position.y, shader.camera.position.z, 1.1881f / (shader.camera.far * shader.camera.far));
            }
        };
        public static final BaseShader.Setter cameraUp = new BaseShader.GlobalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.camera.up);
            }
        };
        public static final BaseShader.Setter diffuseColor = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, ((ColorAttribute) combinedAttributes.get(ColorAttribute.Diffuse)).color);
            }
        };
        public static final BaseShader.Setter diffuseTexture = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.context.textureBinder.bind(((TextureAttribute) combinedAttributes.get(TextureAttribute.Diffuse)).textureDescription));
            }
        };
        public static final BaseShader.Setter diffuseUVTransform = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                TextureAttribute ta = (TextureAttribute) combinedAttributes.get(TextureAttribute.Diffuse);
                shader.set(inputID, ta.offsetU, ta.offsetV, ta.scaleU, ta.scaleV);
            }
        };
        public static final BaseShader.Setter emissiveColor = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, ((ColorAttribute) combinedAttributes.get(ColorAttribute.Emissive)).color);
            }
        };
        public static final BaseShader.Setter emissiveTexture = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.context.textureBinder.bind(((TextureAttribute) combinedAttributes.get(TextureAttribute.Emissive)).textureDescription));
            }
        };
        public static final BaseShader.Setter emissiveUVTransform = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                TextureAttribute ta = (TextureAttribute) combinedAttributes.get(TextureAttribute.Emissive);
                shader.set(inputID, ta.offsetU, ta.offsetV, ta.scaleU, ta.scaleV);
            }
        };
        public static final BaseShader.Setter environmentCubemap = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                if (combinedAttributes.has(CubemapAttribute.EnvironmentMap)) {
                    shader.set(inputID, shader.context.textureBinder.bind(((CubemapAttribute) combinedAttributes.get(CubemapAttribute.EnvironmentMap)).textureDescription));
                }
            }
        };
        public static final BaseShader.Setter normalMatrix = new BaseShader.LocalSetter() {
            private final Matrix3 tmpM = new Matrix3();

            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, this.tmpM.set(renderable.worldTransform).inv().transpose());
            }
        };
        public static final BaseShader.Setter normalTexture = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.context.textureBinder.bind(((TextureAttribute) combinedAttributes.get(TextureAttribute.Normal)).textureDescription));
            }
        };
        public static final BaseShader.Setter normalUVTransform = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                TextureAttribute ta = (TextureAttribute) combinedAttributes.get(TextureAttribute.Normal);
                shader.set(inputID, ta.offsetU, ta.offsetV, ta.scaleU, ta.scaleV);
            }
        };
        public static final BaseShader.Setter projTrans = new BaseShader.GlobalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.camera.projection);
            }
        };
        public static final BaseShader.Setter projViewTrans = new BaseShader.GlobalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.camera.combined);
            }
        };
        public static final BaseShader.Setter projViewWorldTrans = new BaseShader.LocalSetter() {
            final Matrix4 temp = new Matrix4();

            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, this.temp.set(shader.camera.combined).mul(renderable.worldTransform));
            }
        };
        public static final BaseShader.Setter reflectionColor = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, ((ColorAttribute) combinedAttributes.get(ColorAttribute.Reflection)).color);
            }
        };
        public static final BaseShader.Setter reflectionTexture = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.context.textureBinder.bind(((TextureAttribute) combinedAttributes.get(TextureAttribute.Reflection)).textureDescription));
            }
        };
        public static final BaseShader.Setter reflectionUVTransform = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                TextureAttribute ta = (TextureAttribute) combinedAttributes.get(TextureAttribute.Reflection);
                shader.set(inputID, ta.offsetU, ta.offsetV, ta.scaleU, ta.scaleV);
            }
        };
        public static final BaseShader.Setter shininess = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, ((FloatAttribute) combinedAttributes.get(FloatAttribute.Shininess)).value);
            }
        };
        public static final BaseShader.Setter specularColor = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, ((ColorAttribute) combinedAttributes.get(ColorAttribute.Specular)).color);
            }
        };
        public static final BaseShader.Setter specularTexture = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.context.textureBinder.bind(((TextureAttribute) combinedAttributes.get(TextureAttribute.Specular)).textureDescription));
            }
        };
        public static final BaseShader.Setter specularUVTransform = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                TextureAttribute ta = (TextureAttribute) combinedAttributes.get(TextureAttribute.Specular);
                shader.set(inputID, ta.offsetU, ta.offsetV, ta.scaleU, ta.scaleV);
            }
        };
        public static final BaseShader.Setter viewTrans = new BaseShader.GlobalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, shader.camera.view);
            }
        };
        public static final BaseShader.Setter viewWorldTrans = new BaseShader.LocalSetter() {
            final Matrix4 temp = new Matrix4();

            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, this.temp.set(shader.camera.view).mul(renderable.worldTransform));
            }
        };
        public static final BaseShader.Setter worldTrans = new BaseShader.LocalSetter() {
            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                shader.set(inputID, renderable.worldTransform);
            }
        };

        public static class Bones extends BaseShader.LocalSetter {
            private static final Matrix4 idtMatrix = new Matrix4();
            public final float[] bones;

            public Bones(int numBones) {
                this.bones = new float[(numBones * 16)];
            }

            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                float f;
                for (int i = 0; i < this.bones.length; i++) {
                    int idx = i / 16;
                    float[] fArr = this.bones;
                    if (renderable.bones == null || idx >= renderable.bones.length || renderable.bones[idx] == null) {
                        f = idtMatrix.val[i % 16];
                    } else {
                        f = renderable.bones[idx].val[i % 16];
                    }
                    fArr[i] = f;
                }
                shader.program.setUniformMatrix4fv(shader.loc(inputID), this.bones, 0, this.bones.length);
            }
        }

        public static class ACubemap extends BaseShader.LocalSetter {
            private static final float[] ones = {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f};
            private static final Vector3 tmpV1 = new Vector3();
            private final AmbientCubemap cacheAmbientCubemap = new AmbientCubemap();
            public final int dirLightsOffset;
            public final int pointLightsOffset;

            public ACubemap(int dirLightsOffset2, int pointLightsOffset2) {
                this.dirLightsOffset = dirLightsOffset2;
                this.pointLightsOffset = pointLightsOffset2;
            }

            public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
                if (renderable.environment == null) {
                    shader.program.setUniform3fv(shader.loc(inputID), ones, 0, ones.length);
                    return;
                }
                renderable.worldTransform.getTranslation(tmpV1);
                if (combinedAttributes.has(ColorAttribute.AmbientLight)) {
                    this.cacheAmbientCubemap.set(((ColorAttribute) combinedAttributes.get(ColorAttribute.AmbientLight)).color);
                }
                if (combinedAttributes.has(DirectionalLightsAttribute.Type)) {
                    Array<DirectionalLight> lights = ((DirectionalLightsAttribute) combinedAttributes.get(DirectionalLightsAttribute.Type)).lights;
                    for (int i = this.dirLightsOffset; i < lights.size; i++) {
                        this.cacheAmbientCubemap.add(lights.get(i).color, lights.get(i).direction);
                    }
                }
                if (combinedAttributes.has(PointLightsAttribute.Type)) {
                    Array<PointLight> lights2 = ((PointLightsAttribute) combinedAttributes.get(PointLightsAttribute.Type)).lights;
                    for (int i2 = this.pointLightsOffset; i2 < lights2.size; i2++) {
                        this.cacheAmbientCubemap.add(lights2.get(i2).color, lights2.get(i2).position, tmpV1, lights2.get(i2).intensity);
                    }
                }
                this.cacheAmbientCubemap.clamp();
                shader.program.setUniform3fv(shader.loc(inputID), this.cacheAmbientCubemap.data, 0, this.cacheAmbientCubemap.data.length);
            }
        }
    }

    public static String getDefaultVertexShader() {
        if (defaultVertexShader == null) {
            defaultVertexShader = Gdx.files.classpath("com/badlogic/gdx/graphics/g3d/shaders/default.vertex.glsl").readString();
        }
        return defaultVertexShader;
    }

    public static String getDefaultFragmentShader() {
        if (defaultFragmentShader == null) {
            defaultFragmentShader = Gdx.files.classpath("com/badlogic/gdx/graphics/g3d/shaders/default.fragment.glsl").readString();
        }
        return defaultFragmentShader;
    }

    public DefaultShader(Renderable renderable2) {
        this(renderable2, new Config());
    }

    public DefaultShader(Renderable renderable2, Config config2) {
        this(renderable2, config2, createPrefix(renderable2, config2));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DefaultShader(Renderable renderable2, Config config2, String prefix) {
        this(renderable2, config2, prefix, config2.vertexShader != null ? config2.vertexShader : getDefaultVertexShader(), config2.fragmentShader != null ? config2.fragmentShader : getDefaultFragmentShader());
    }

    public DefaultShader(Renderable renderable2, Config config2, String prefix, String vertexShader, String fragmentShader) {
        this(renderable2, config2, new ShaderProgram(String.valueOf(prefix) + vertexShader, String.valueOf(prefix) + fragmentShader));
    }

    public DefaultShader(Renderable renderable2, Config config2, ShaderProgram shaderProgram) {
        boolean z;
        boolean z2;
        int i;
        int i2;
        int i3;
        boolean z3 = true;
        int i4 = -1;
        int i5 = 0;
        this.u_dirLights0color = register(new BaseShader.Uniform("u_dirLights[0].color"));
        this.u_dirLights0direction = register(new BaseShader.Uniform("u_dirLights[0].direction"));
        this.u_dirLights1color = register(new BaseShader.Uniform("u_dirLights[1].color"));
        this.u_pointLights0color = register(new BaseShader.Uniform("u_pointLights[0].color"));
        this.u_pointLights0position = register(new BaseShader.Uniform("u_pointLights[0].position"));
        this.u_pointLights0intensity = register(new BaseShader.Uniform("u_pointLights[0].intensity"));
        this.u_pointLights1color = register(new BaseShader.Uniform("u_pointLights[1].color"));
        this.u_fogColor = register(new BaseShader.Uniform("u_fogColor"));
        this.u_shadowMapProjViewTrans = register(new BaseShader.Uniform("u_shadowMapProjViewTrans"));
        this.u_shadowTexture = register(new BaseShader.Uniform("u_shadowTexture"));
        this.u_shadowPCFOffset = register(new BaseShader.Uniform("u_shadowPCFOffset"));
        this.ambientCubemap = new AmbientCubemap();
        this.normalMatrix = new Matrix3();
        this.tmpV1 = new Vector3();
        Attributes attributes = combineAttributes(renderable2);
        this.config = config2;
        this.program = shaderProgram;
        if (renderable2.environment != null) {
            z = true;
        } else {
            z = false;
        }
        this.lighting = z;
        if (attributes.has(CubemapAttribute.EnvironmentMap) || (this.lighting && attributes.has(CubemapAttribute.EnvironmentMap))) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.environmentCubemap = z2;
        this.shadowMap = (!this.lighting || renderable2.environment.shadowMap == null) ? false : z3;
        this.renderable = renderable2;
        this.attributesMask = attributes.getMask() | optionalAttributes;
        this.vertexMask = renderable2.mesh.getVertexAttributes().getMask();
        if (!this.lighting || config2.numDirectionalLights <= 0) {
            i = 0;
        } else {
            i = config2.numDirectionalLights;
        }
        this.directionalLights = new DirectionalLight[i];
        for (int i6 = 0; i6 < this.directionalLights.length; i6++) {
            this.directionalLights[i6] = new DirectionalLight();
        }
        if (this.lighting && config2.numPointLights > 0) {
            i5 = config2.numPointLights;
        }
        this.pointLights = new PointLight[i5];
        for (int i7 = 0; i7 < this.pointLights.length; i7++) {
            this.pointLights[i7] = new PointLight();
        }
        if (config2.ignoreUnimplemented || (implementedFlags & this.attributesMask) == this.attributesMask) {
            this.u_projTrans = register(Inputs.projTrans, Setters.projTrans);
            this.u_viewTrans = register(Inputs.viewTrans, Setters.viewTrans);
            this.u_projViewTrans = register(Inputs.projViewTrans, Setters.projViewTrans);
            this.u_cameraPosition = register(Inputs.cameraPosition, Setters.cameraPosition);
            this.u_cameraDirection = register(Inputs.cameraDirection, Setters.cameraDirection);
            this.u_cameraUp = register(Inputs.cameraUp, Setters.cameraUp);
            this.u_time = register(new BaseShader.Uniform("u_time"));
            this.u_worldTrans = register(Inputs.worldTrans, Setters.worldTrans);
            this.u_viewWorldTrans = register(Inputs.viewWorldTrans, Setters.viewWorldTrans);
            this.u_projViewWorldTrans = register(Inputs.projViewWorldTrans, Setters.projViewWorldTrans);
            this.u_normalMatrix = register(Inputs.normalMatrix, Setters.normalMatrix);
            if (renderable2.bones == null || config2.numBones <= 0) {
                i2 = -1;
            } else {
                i2 = register(Inputs.bones, new Setters.Bones(config2.numBones));
            }
            this.u_bones = i2;
            this.u_shininess = register(Inputs.shininess, Setters.shininess);
            this.u_opacity = register(Inputs.opacity);
            this.u_diffuseColor = register(Inputs.diffuseColor, Setters.diffuseColor);
            this.u_diffuseTexture = register(Inputs.diffuseTexture, Setters.diffuseTexture);
            this.u_diffuseUVTransform = register(Inputs.diffuseUVTransform, Setters.diffuseUVTransform);
            this.u_specularColor = register(Inputs.specularColor, Setters.specularColor);
            this.u_specularTexture = register(Inputs.specularTexture, Setters.specularTexture);
            this.u_specularUVTransform = register(Inputs.specularUVTransform, Setters.specularUVTransform);
            this.u_emissiveColor = register(Inputs.emissiveColor, Setters.emissiveColor);
            this.u_emissiveTexture = register(Inputs.emissiveTexture, Setters.emissiveTexture);
            this.u_emissiveUVTransform = register(Inputs.emissiveUVTransform, Setters.emissiveUVTransform);
            this.u_reflectionColor = register(Inputs.reflectionColor, Setters.reflectionColor);
            this.u_reflectionTexture = register(Inputs.reflectionTexture, Setters.reflectionTexture);
            this.u_reflectionUVTransform = register(Inputs.reflectionUVTransform, Setters.reflectionUVTransform);
            this.u_normalTexture = register(Inputs.normalTexture, Setters.normalTexture);
            this.u_normalUVTransform = register(Inputs.normalUVTransform, Setters.normalUVTransform);
            this.u_ambientTexture = register(Inputs.ambientTexture, Setters.ambientTexture);
            this.u_ambientUVTransform = register(Inputs.ambientUVTransform, Setters.ambientUVTransform);
            this.u_alphaTest = register(Inputs.alphaTest);
            if (this.lighting) {
                i3 = register(Inputs.ambientCube, new Setters.ACubemap(config2.numDirectionalLights, config2.numPointLights));
            } else {
                i3 = -1;
            }
            this.u_ambientCubemap = i3;
            this.u_environmentCubemap = this.environmentCubemap ? register(Inputs.environmentCubemap, Setters.environmentCubemap) : i4;
            return;
        }
        throw new GdxRuntimeException("Some attributes not implemented yet (" + this.attributesMask + ")");
    }

    public void init() {
        ShaderProgram program = this.program;
        this.program = null;
        init(program, this.renderable);
        this.renderable = null;
        this.dirLightsLoc = loc(this.u_dirLights0color);
        this.dirLightsColorOffset = loc(this.u_dirLights0color) - this.dirLightsLoc;
        this.dirLightsDirectionOffset = loc(this.u_dirLights0direction) - this.dirLightsLoc;
        this.dirLightsSize = loc(this.u_dirLights1color) - this.dirLightsLoc;
        if (this.dirLightsSize < 0) {
            this.dirLightsSize = 0;
        }
        this.pointLightsLoc = loc(this.u_pointLights0color);
        this.pointLightsColorOffset = loc(this.u_pointLights0color) - this.pointLightsLoc;
        this.pointLightsPositionOffset = loc(this.u_pointLights0position) - this.pointLightsLoc;
        this.pointLightsIntensityOffset = has(this.u_pointLights0intensity) ? loc(this.u_pointLights0intensity) - this.pointLightsLoc : -1;
        this.pointLightsSize = loc(this.u_pointLights1color) - this.pointLightsLoc;
        if (this.pointLightsSize < 0) {
            this.pointLightsSize = 0;
        }
    }

    private static final boolean and(long mask, long flag) {
        return (mask & flag) == flag;
    }

    private static final boolean or(long mask, long flag) {
        return (mask & flag) != 0;
    }

    private static final Attributes combineAttributes(Renderable renderable2) {
        tmpAttributes.clear();
        if (renderable2.environment != null) {
            tmpAttributes.set(renderable2.environment);
        }
        if (renderable2.material != null) {
            tmpAttributes.set(renderable2.material);
        }
        return tmpAttributes;
    }

    public static String createPrefix(Renderable renderable2, Config config2) {
        String prefix;
        Attributes attributes = combineAttributes(renderable2);
        String prefix2 = "";
        long attributesMask2 = attributes.getMask();
        long vertexMask2 = renderable2.mesh.getVertexAttributes().getMask();
        if (and(vertexMask2, 1)) {
            prefix2 = String.valueOf(prefix2) + "#define positionFlag\n";
        }
        if (or(vertexMask2, 6)) {
            prefix2 = String.valueOf(prefix2) + "#define colorFlag\n";
        }
        if (and(vertexMask2, 256)) {
            prefix2 = String.valueOf(prefix2) + "#define binormalFlag\n";
        }
        if (and(vertexMask2, 128)) {
            prefix2 = String.valueOf(prefix2) + "#define tangentFlag\n";
        }
        if (and(vertexMask2, 8)) {
            prefix2 = String.valueOf(prefix2) + "#define normalFlag\n";
        }
        if ((and(vertexMask2, 8) || and(vertexMask2, 384)) && renderable2.environment != null) {
            prefix = String.valueOf(String.valueOf(String.valueOf(String.valueOf(prefix) + "#define lightingFlag\n") + "#define ambientCubemapFlag\n") + "#define numDirectionalLights " + config2.numDirectionalLights + "\n") + "#define numPointLights " + config2.numPointLights + "\n";
            if (attributes.has(ColorAttribute.Fog)) {
                prefix = String.valueOf(prefix) + "#define fogFlag\n";
            }
            if (renderable2.environment.shadowMap != null) {
                prefix = String.valueOf(prefix) + "#define shadowMapFlag\n";
            }
            if (attributes.has(CubemapAttribute.EnvironmentMap)) {
                prefix = String.valueOf(prefix) + "#define environmentCubemapFlag\n";
            }
        }
        int n = renderable2.mesh.getVertexAttributes().size();
        for (int i = 0; i < n; i++) {
            VertexAttribute attr = renderable2.mesh.getVertexAttributes().get(i);
            if (attr.usage == 64) {
                prefix = String.valueOf(prefix) + "#define boneWeight" + attr.unit + "Flag\n";
            } else if (attr.usage == 16) {
                prefix = String.valueOf(prefix) + "#define texCoord" + attr.unit + "Flag\n";
            }
        }
        if ((BlendingAttribute.Type & attributesMask2) == BlendingAttribute.Type) {
            prefix = String.valueOf(prefix) + "#define blendedFlag\n";
        }
        if ((TextureAttribute.Diffuse & attributesMask2) == TextureAttribute.Diffuse) {
            prefix = String.valueOf(String.valueOf(prefix) + "#define diffuseTextureFlag\n") + "#define diffuseTextureCoord texCoord0\n";
        }
        if ((TextureAttribute.Specular & attributesMask2) == TextureAttribute.Specular) {
            prefix = String.valueOf(String.valueOf(prefix) + "#define specularTextureFlag\n") + "#define specularTextureCoord texCoord0\n";
        }
        if ((TextureAttribute.Normal & attributesMask2) == TextureAttribute.Normal) {
            prefix = String.valueOf(String.valueOf(prefix) + "#define normalTextureFlag\n") + "#define normalTextureCoord texCoord0\n";
        }
        if ((TextureAttribute.Emissive & attributesMask2) == TextureAttribute.Emissive) {
            prefix = String.valueOf(String.valueOf(prefix) + "#define emissiveTextureFlag\n") + "#define emissiveTextureCoord texCoord0\n";
        }
        if ((TextureAttribute.Reflection & attributesMask2) == TextureAttribute.Reflection) {
            prefix = String.valueOf(String.valueOf(prefix) + "#define reflectionTextureFlag\n") + "#define reflectionTextureCoord texCoord0\n";
        }
        if ((TextureAttribute.Ambient & attributesMask2) == TextureAttribute.Ambient) {
            prefix = String.valueOf(String.valueOf(prefix) + "#define ambientTextureFlag\n") + "#define ambientTextureCoord texCoord0\n";
        }
        if ((ColorAttribute.Diffuse & attributesMask2) == ColorAttribute.Diffuse) {
            prefix = String.valueOf(prefix) + "#define diffuseColorFlag\n";
        }
        if ((ColorAttribute.Specular & attributesMask2) == ColorAttribute.Specular) {
            prefix = String.valueOf(prefix) + "#define specularColorFlag\n";
        }
        if ((ColorAttribute.Emissive & attributesMask2) == ColorAttribute.Emissive) {
            prefix = String.valueOf(prefix) + "#define emissiveColorFlag\n";
        }
        if ((ColorAttribute.Reflection & attributesMask2) == ColorAttribute.Reflection) {
            prefix = String.valueOf(prefix) + "#define reflectionColorFlag\n";
        }
        if ((FloatAttribute.Shininess & attributesMask2) == FloatAttribute.Shininess) {
            prefix = String.valueOf(prefix) + "#define shininessFlag\n";
        }
        if ((FloatAttribute.AlphaTest & attributesMask2) == FloatAttribute.AlphaTest) {
            prefix = String.valueOf(prefix) + "#define alphaTestFlag\n";
        }
        if (renderable2.bones == null || config2.numBones <= 0) {
            return prefix;
        }
        return String.valueOf(prefix) + "#define numBones " + config2.numBones + "\n";
    }

    public boolean canRender(Renderable renderable2) {
        boolean z;
        if (this.attributesMask == (combineAttributes(renderable2).getMask() | optionalAttributes) && this.vertexMask == renderable2.mesh.getVertexAttributes().getMask()) {
            if (renderable2.environment != null) {
                z = true;
            } else {
                z = false;
            }
            if (z == this.lighting) {
                return true;
            }
        }
        return false;
    }

    public int compareTo(Shader other) {
        if (other == null) {
            return -1;
        }
        if (other == this) {
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof DefaultShader) {
            return equals((DefaultShader) obj);
        }
        return false;
    }

    public boolean equals(DefaultShader obj) {
        return obj == this;
    }

    public void begin(Camera camera2, RenderContext context) {
        super.begin(camera2, context);
        for (DirectionalLight dirLight : this.directionalLights) {
            dirLight.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, -1.0f, Animation.CurveTimeline.LINEAR);
        }
        for (PointLight pointLight : this.pointLights) {
            pointLight.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        }
        this.lightsSet = false;
        if (has(this.u_time)) {
            int i = this.u_time;
            float deltaTime = this.time + Gdx.graphics.getDeltaTime();
            this.time = deltaTime;
            set(i, deltaTime);
        }
    }

    public void render(Renderable renderable2, Attributes combinedAttributes) {
        if (!combinedAttributes.has(BlendingAttribute.Type)) {
            this.context.setBlending(false, 770, 771);
        }
        bindMaterial(combinedAttributes);
        if (this.lighting) {
            bindLights(renderable2, combinedAttributes);
        }
        super.render(renderable2, combinedAttributes);
    }

    public void end() {
        super.end();
    }

    /* access modifiers changed from: protected */
    public void bindMaterial(Attributes attributes) {
        int cullFace = this.config.defaultCullFace == -1 ? defaultCullFace : this.config.defaultCullFace;
        int depthFunc = this.config.defaultDepthFunc == -1 ? defaultDepthFunc : this.config.defaultDepthFunc;
        float depthRangeNear = Animation.CurveTimeline.LINEAR;
        float depthRangeFar = 1.0f;
        boolean depthMask = true;
        Iterator<Attribute> it = attributes.iterator();
        while (it.hasNext()) {
            Attribute attr = it.next();
            long t = attr.type;
            if (BlendingAttribute.is(t)) {
                this.context.setBlending(true, ((BlendingAttribute) attr).sourceFunction, ((BlendingAttribute) attr).destFunction);
                set(this.u_opacity, ((BlendingAttribute) attr).opacity);
            } else if ((IntAttribute.CullFace & t) == IntAttribute.CullFace) {
                cullFace = ((IntAttribute) attr).value;
            } else if ((FloatAttribute.AlphaTest & t) == FloatAttribute.AlphaTest) {
                set(this.u_alphaTest, ((FloatAttribute) attr).value);
            } else if ((DepthTestAttribute.Type & t) == DepthTestAttribute.Type) {
                DepthTestAttribute dta = (DepthTestAttribute) attr;
                depthFunc = dta.depthFunc;
                depthRangeNear = dta.depthRangeNear;
                depthRangeFar = dta.depthRangeFar;
                depthMask = dta.depthMask;
            } else if (!this.config.ignoreUnimplemented) {
                throw new GdxRuntimeException("Unknown material attribute: " + attr.toString());
            }
        }
        this.context.setCullFace(cullFace);
        this.context.setDepthTest(depthFunc, depthRangeNear, depthRangeFar);
        this.context.setDepthMask(depthMask);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0026 A[EDGE_INSN: B:66:0x0026->B:11:0x0026 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00b4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0030 A[EDGE_INSN: B:70:0x0030->B:16:0x0030 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x014a A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void bindLights(com.badlogic.gdx.graphics.g3d.Renderable r14, com.badlogic.gdx.graphics.g3d.Attributes r15) {
        /*
            r13 = this;
            com.badlogic.gdx.graphics.g3d.Environment r4 = r14.environment
            java.lang.Class<com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute> r7 = com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute.class
            long r8 = com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute.Type
            com.badlogic.gdx.graphics.g3d.Attribute r1 = r15.get(r7, r8)
            com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute r1 = (com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute) r1
            if (r1 != 0) goto L_0x007d
            r0 = 0
        L_0x000f:
            java.lang.Class<com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute> r7 = com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute.class
            long r8 = com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute.Type
            com.badlogic.gdx.graphics.g3d.Attribute r5 = r15.get(r7, r8)
            com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute r5 = (com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute) r5
            if (r5 != 0) goto L_0x0080
            r6 = 0
        L_0x001c:
            int r7 = r13.dirLightsLoc
            if (r7 < 0) goto L_0x0026
            r2 = 0
        L_0x0021:
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            int r7 = r7.length
            if (r2 < r7) goto L_0x0083
        L_0x0026:
            int r7 = r13.pointLightsLoc
            if (r7 < 0) goto L_0x0030
            r2 = 0
        L_0x002b:
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r7 = r13.pointLights
            int r7 = r7.length
            if (r2 < r7) goto L_0x0135
        L_0x0030:
            long r8 = com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute.Fog
            boolean r7 = r15.has(r8)
            if (r7 == 0) goto L_0x0047
            int r8 = r13.u_fogColor
            long r10 = com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute.Fog
            com.badlogic.gdx.graphics.g3d.Attribute r7 = r15.get(r10)
            com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute r7 = (com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute) r7
            com.badlogic.gdx.graphics.Color r7 = r7.color
            r13.set(r8, r7)
        L_0x0047:
            com.badlogic.gdx.graphics.g3d.environment.ShadowMap r7 = r4.shadowMap
            if (r7 == 0) goto L_0x0079
            int r7 = r13.u_shadowMapProjViewTrans
            com.badlogic.gdx.graphics.g3d.environment.ShadowMap r8 = r4.shadowMap
            com.badlogic.gdx.math.Matrix4 r8 = r8.getProjViewTrans()
            r13.set(r7, r8)
            int r7 = r13.u_shadowTexture
            com.badlogic.gdx.graphics.g3d.environment.ShadowMap r8 = r4.shadowMap
            com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor r8 = r8.getDepthMap()
            r13.set(r7, r8)
            int r7 = r13.u_shadowPCFOffset
            r8 = 1065353216(0x3f800000, float:1.0)
            r9 = 1073741824(0x40000000, float:2.0)
            com.badlogic.gdx.graphics.g3d.environment.ShadowMap r10 = r4.shadowMap
            com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor r10 = r10.getDepthMap()
            T r10 = r10.texture
            int r10 = r10.getWidth()
            float r10 = (float) r10
            float r9 = r9 * r10
            float r8 = r8 / r9
            r13.set(r7, r8)
        L_0x0079:
            r7 = 1
            r13.lightsSet = r7
            return
        L_0x007d:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g3d.environment.DirectionalLight> r0 = r1.lights
            goto L_0x000f
        L_0x0080:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g3d.environment.PointLight> r6 = r5.lights
            goto L_0x001c
        L_0x0083:
            if (r0 == 0) goto L_0x0089
            int r7 = r0.size
            if (r2 < r7) goto L_0x0113
        L_0x0089:
            boolean r7 = r13.lightsSet
            if (r7 == 0) goto L_0x00b8
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            r7 = r7[r2]
            com.badlogic.gdx.graphics.Color r7 = r7.color
            float r7 = r7.r
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x00b8
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            r7 = r7[r2]
            com.badlogic.gdx.graphics.Color r7 = r7.color
            float r7 = r7.g
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x00b8
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            r7 = r7[r2]
            com.badlogic.gdx.graphics.Color r7 = r7.color
            float r7 = r7.b
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x00b8
        L_0x00b4:
            int r2 = r2 + 1
            goto L_0x0021
        L_0x00b8:
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            r7 = r7[r2]
            com.badlogic.gdx.graphics.Color r7 = r7.color
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 1065353216(0x3f800000, float:1.0)
            r7.set(r8, r9, r10, r11)
        L_0x00c6:
            int r7 = r13.dirLightsLoc
            int r8 = r13.dirLightsSize
            int r8 = r8 * r2
            int r3 = r7 + r8
            com.badlogic.gdx.graphics.glutils.ShaderProgram r7 = r13.program
            int r8 = r13.dirLightsColorOffset
            int r8 = r8 + r3
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r9 = r13.directionalLights
            r9 = r9[r2]
            com.badlogic.gdx.graphics.Color r9 = r9.color
            float r9 = r9.r
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r10 = r13.directionalLights
            r10 = r10[r2]
            com.badlogic.gdx.graphics.Color r10 = r10.color
            float r10 = r10.g
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r11 = r13.directionalLights
            r11 = r11[r2]
            com.badlogic.gdx.graphics.Color r11 = r11.color
            float r11 = r11.b
            r7.setUniformf(r8, r9, r10, r11)
            com.badlogic.gdx.graphics.glutils.ShaderProgram r7 = r13.program
            int r8 = r13.dirLightsDirectionOffset
            int r8 = r8 + r3
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r9 = r13.directionalLights
            r9 = r9[r2]
            com.badlogic.gdx.math.Vector3 r9 = r9.direction
            float r9 = r9.x
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r10 = r13.directionalLights
            r10 = r10[r2]
            com.badlogic.gdx.math.Vector3 r10 = r10.direction
            float r10 = r10.y
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r11 = r13.directionalLights
            r11 = r11[r2]
            com.badlogic.gdx.math.Vector3 r11 = r11.direction
            float r11 = r11.z
            r7.setUniformf(r8, r9, r10, r11)
            int r7 = r13.dirLightsSize
            if (r7 > 0) goto L_0x00b4
            goto L_0x0026
        L_0x0113:
            boolean r7 = r13.lightsSet
            if (r7 == 0) goto L_0x0127
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            r8 = r7[r2]
            java.lang.Object r7 = r0.get(r2)
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight r7 = (com.badlogic.gdx.graphics.g3d.environment.DirectionalLight) r7
            boolean r7 = r8.equals(r7)
            if (r7 != 0) goto L_0x00b4
        L_0x0127:
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight[] r7 = r13.directionalLights
            r8 = r7[r2]
            java.lang.Object r7 = r0.get(r2)
            com.badlogic.gdx.graphics.g3d.environment.DirectionalLight r7 = (com.badlogic.gdx.graphics.g3d.environment.DirectionalLight) r7
            r8.set(r7)
            goto L_0x00c6
        L_0x0135:
            if (r6 == 0) goto L_0x013b
            int r7 = r6.size
            if (r2 < r7) goto L_0x01c9
        L_0x013b:
            boolean r7 = r13.lightsSet
            if (r7 == 0) goto L_0x014e
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r7 = r13.pointLights
            r7 = r7[r2]
            float r7 = r7.intensity
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x014e
        L_0x014a:
            int r2 = r2 + 1
            goto L_0x002b
        L_0x014e:
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r7 = r13.pointLights
            r7 = r7[r2]
            r8 = 0
            r7.intensity = r8
        L_0x0155:
            int r7 = r13.pointLightsLoc
            int r8 = r13.pointLightsSize
            int r8 = r8 * r2
            int r3 = r7 + r8
            com.badlogic.gdx.graphics.glutils.ShaderProgram r7 = r13.program
            int r8 = r13.pointLightsColorOffset
            int r8 = r8 + r3
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r9 = r13.pointLights
            r9 = r9[r2]
            com.badlogic.gdx.graphics.Color r9 = r9.color
            float r9 = r9.r
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r10 = r13.pointLights
            r10 = r10[r2]
            float r10 = r10.intensity
            float r9 = r9 * r10
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r10 = r13.pointLights
            r10 = r10[r2]
            com.badlogic.gdx.graphics.Color r10 = r10.color
            float r10 = r10.g
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r11 = r13.pointLights
            r11 = r11[r2]
            float r11 = r11.intensity
            float r10 = r10 * r11
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r11 = r13.pointLights
            r11 = r11[r2]
            com.badlogic.gdx.graphics.Color r11 = r11.color
            float r11 = r11.b
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r12 = r13.pointLights
            r12 = r12[r2]
            float r12 = r12.intensity
            float r11 = r11 * r12
            r7.setUniformf(r8, r9, r10, r11)
            com.badlogic.gdx.graphics.glutils.ShaderProgram r7 = r13.program
            int r8 = r13.pointLightsPositionOffset
            int r8 = r8 + r3
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r9 = r13.pointLights
            r9 = r9[r2]
            com.badlogic.gdx.math.Vector3 r9 = r9.position
            float r9 = r9.x
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r10 = r13.pointLights
            r10 = r10[r2]
            com.badlogic.gdx.math.Vector3 r10 = r10.position
            float r10 = r10.y
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r11 = r13.pointLights
            r11 = r11[r2]
            com.badlogic.gdx.math.Vector3 r11 = r11.position
            float r11 = r11.z
            r7.setUniformf(r8, r9, r10, r11)
            int r7 = r13.pointLightsIntensityOffset
            if (r7 < 0) goto L_0x01c3
            com.badlogic.gdx.graphics.glutils.ShaderProgram r7 = r13.program
            int r8 = r13.pointLightsIntensityOffset
            int r8 = r8 + r3
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r9 = r13.pointLights
            r9 = r9[r2]
            float r9 = r9.intensity
            r7.setUniformf(r8, r9)
        L_0x01c3:
            int r7 = r13.pointLightsSize
            if (r7 > 0) goto L_0x014a
            goto L_0x0030
        L_0x01c9:
            boolean r7 = r13.lightsSet
            if (r7 == 0) goto L_0x01dd
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r7 = r13.pointLights
            r8 = r7[r2]
            java.lang.Object r7 = r6.get(r2)
            com.badlogic.gdx.graphics.g3d.environment.PointLight r7 = (com.badlogic.gdx.graphics.g3d.environment.PointLight) r7
            boolean r7 = r8.equals(r7)
            if (r7 != 0) goto L_0x014a
        L_0x01dd:
            com.badlogic.gdx.graphics.g3d.environment.PointLight[] r7 = r13.pointLights
            r8 = r7[r2]
            java.lang.Object r7 = r6.get(r2)
            com.badlogic.gdx.graphics.g3d.environment.PointLight r7 = (com.badlogic.gdx.graphics.g3d.environment.PointLight) r7
            r8.set(r7)
            goto L_0x0155
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g3d.shaders.DefaultShader.bindLights(com.badlogic.gdx.graphics.g3d.Renderable, com.badlogic.gdx.graphics.g3d.Attributes):void");
    }

    public void dispose() {
        this.program.dispose();
        super.dispose();
    }

    public int getDefaultCullFace() {
        return this.config.defaultCullFace == -1 ? defaultCullFace : this.config.defaultCullFace;
    }

    public void setDefaultCullFace(int cullFace) {
        this.config.defaultCullFace = cullFace;
    }

    public int getDefaultDepthFunc() {
        return this.config.defaultDepthFunc == -1 ? defaultDepthFunc : this.config.defaultDepthFunc;
    }

    public void setDefaultDepthFunc(int depthFunc) {
        this.config.defaultDepthFunc = depthFunc;
    }
}
