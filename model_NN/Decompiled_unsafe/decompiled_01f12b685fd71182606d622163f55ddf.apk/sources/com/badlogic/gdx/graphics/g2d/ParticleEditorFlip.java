package com.badlogic.gdx.graphics.g2d;

public interface ParticleEditorFlip {
    void flip(Sprite sprite);
}
