package com.badlogic.gdx.graphics.g3d.loader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.model.data.ModelData;
import com.badlogic.gdx.graphics.g3d.model.data.ModelMesh;
import com.badlogic.gdx.graphics.g3d.model.data.ModelMeshPart;
import com.badlogic.gdx.graphics.g3d.model.data.ModelNode;
import com.badlogic.gdx.graphics.g3d.model.data.ModelNodePart;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

public class ObjLoader extends ModelLoader<ObjLoaderParameters> {
    public static boolean logWarning = false;
    final Array<Group> groups;
    final FloatArray norms;
    final FloatArray uvs;
    final FloatArray verts;

    public static class ObjLoaderParameters extends ModelLoader.ModelParameters {
        public boolean flipV;

        public ObjLoaderParameters() {
        }

        public ObjLoaderParameters(boolean flipV2) {
            this.flipV = flipV2;
        }
    }

    public ObjLoader() {
        this(null);
    }

    public ObjLoader(FileHandleResolver resolver) {
        super(resolver);
        this.verts = new FloatArray(300);
        this.norms = new FloatArray(300);
        this.uvs = new FloatArray(200);
        this.groups = new Array<>(10);
    }

    public Model loadModel(FileHandle fileHandle, boolean flipV) {
        return loadModel(fileHandle, new ObjLoaderParameters(flipV));
    }

    public ModelData loadModelData(FileHandle file, ObjLoaderParameters parameters) {
        return loadModelData(file, parameters == null ? false : parameters.flipV);
    }

    /* access modifiers changed from: protected */
    public ModelData loadModelData(FileHandle file, boolean flipV) {
        char firstChar;
        if (logWarning) {
            Gdx.app.error("ObjLoader", "Wavefront (OBJ) is not fully supported, consult the documentation for more information");
        }
        MtlLoader mtl = new MtlLoader();
        Group activeGroup = new Group("default");
        this.groups.add(activeGroup);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.read()), 4096);
        int id = 0;
        while (true) {
            try {
                String line = bufferedReader.readLine();
                if (line != null) {
                    String[] tokens = line.split("\\s+");
                    if (tokens.length < 1) {
                        break;
                    } else if (!(tokens[0].length() == 0 || (firstChar = tokens[0].toLowerCase().charAt(0)) == '#')) {
                        if (firstChar == 'v') {
                            if (tokens[0].length() == 1) {
                                this.verts.add(Float.parseFloat(tokens[1]));
                                this.verts.add(Float.parseFloat(tokens[2]));
                                this.verts.add(Float.parseFloat(tokens[3]));
                            } else if (tokens[0].charAt(1) == 'n') {
                                this.norms.add(Float.parseFloat(tokens[1]));
                                this.norms.add(Float.parseFloat(tokens[2]));
                                this.norms.add(Float.parseFloat(tokens[3]));
                            } else if (tokens[0].charAt(1) == 't') {
                                this.uvs.add(Float.parseFloat(tokens[1]));
                                this.uvs.add(flipV ? 1.0f - Float.parseFloat(tokens[2]) : Float.parseFloat(tokens[2]));
                            }
                        } else if (firstChar == 'f') {
                            Array<Integer> faces = activeGroup.faces;
                            int i = 1;
                            while (i < tokens.length - 2) {
                                String[] parts = tokens[1].split("/");
                                faces.add(Integer.valueOf(getIndex(parts[0], this.verts.size)));
                                if (parts.length > 2) {
                                    if (i == 1) {
                                        activeGroup.hasNorms = true;
                                    }
                                    faces.add(Integer.valueOf(getIndex(parts[2], this.norms.size)));
                                }
                                if (parts.length > 1 && parts[1].length() > 0) {
                                    if (i == 1) {
                                        activeGroup.hasUVs = true;
                                    }
                                    faces.add(Integer.valueOf(getIndex(parts[1], this.uvs.size)));
                                }
                                int i2 = i + 1;
                                String[] parts2 = tokens[i2].split("/");
                                faces.add(Integer.valueOf(getIndex(parts2[0], this.verts.size)));
                                if (parts2.length > 2) {
                                    faces.add(Integer.valueOf(getIndex(parts2[2], this.norms.size)));
                                }
                                if (parts2.length > 1 && parts2[1].length() > 0) {
                                    faces.add(Integer.valueOf(getIndex(parts2[1], this.uvs.size)));
                                }
                                int i3 = i2 + 1;
                                String[] parts3 = tokens[i3].split("/");
                                faces.add(Integer.valueOf(getIndex(parts3[0], this.verts.size)));
                                if (parts3.length > 2) {
                                    faces.add(Integer.valueOf(getIndex(parts3[2], this.norms.size)));
                                }
                                if (parts3.length > 1 && parts3[1].length() > 0) {
                                    faces.add(Integer.valueOf(getIndex(parts3[1], this.uvs.size)));
                                }
                                activeGroup.numFaces++;
                                i = i3 - 1;
                            }
                        } else if (firstChar == 'o' || firstChar == 'g') {
                            if (tokens.length > 1) {
                                activeGroup = setActiveGroup(tokens[1]);
                            } else {
                                activeGroup = setActiveGroup("default");
                            }
                        } else if (tokens[0].equals("mtllib")) {
                            mtl.load(file.parent().child(tokens[1]));
                        } else if (tokens[0].equals("usemtl")) {
                            if (tokens.length == 1) {
                                activeGroup.materialName = "default";
                            } else {
                                activeGroup.materialName = tokens[1];
                            }
                        }
                    }
                } else {
                    break;
                }
            } catch (IOException e) {
                return null;
            }
        }
        bufferedReader.close();
        int i4 = 0;
        while (i4 < this.groups.size) {
            if (this.groups.get(i4).numFaces < 1) {
                this.groups.removeIndex(i4);
                i4--;
            }
            i4++;
        }
        if (this.groups.size < 1) {
            return null;
        }
        int numGroups = this.groups.size;
        ModelData data = new ModelData();
        for (int g = 0; g < numGroups; g++) {
            Group group = this.groups.get(g);
            Array<Integer> faces2 = group.faces;
            int numElements = faces2.size;
            int numFaces = group.numFaces;
            boolean hasNorms = group.hasNorms;
            boolean hasUVs = group.hasUVs;
            float[] finalVerts = new float[(((hasUVs ? 2 : 0) + (hasNorms ? 3 : 0) + 3) * numFaces * 3)];
            int vi = 0;
            int i5 = 0;
            while (i5 < numElements) {
                int i6 = i5 + 1;
                int vertIndex = faces2.get(i5).intValue() * 3;
                int vi2 = vi + 1;
                int vertIndex2 = vertIndex + 1;
                finalVerts[vi] = this.verts.get(vertIndex);
                int vi3 = vi2 + 1;
                finalVerts[vi2] = this.verts.get(vertIndex2);
                int vi4 = vi3 + 1;
                finalVerts[vi3] = this.verts.get(vertIndex2 + 1);
                if (hasNorms) {
                    i5 = i6 + 1;
                    int normIndex = faces2.get(i6).intValue() * 3;
                    int vi5 = vi4 + 1;
                    int normIndex2 = normIndex + 1;
                    finalVerts[vi4] = this.norms.get(normIndex);
                    int vi6 = vi5 + 1;
                    finalVerts[vi5] = this.norms.get(normIndex2);
                    vi = vi6 + 1;
                    finalVerts[vi6] = this.norms.get(normIndex2 + 1);
                } else {
                    vi = vi4;
                    i5 = i6;
                }
                if (hasUVs) {
                    int uvIndex = faces2.get(i5).intValue() * 2;
                    int vi7 = vi + 1;
                    finalVerts[vi] = this.uvs.get(uvIndex);
                    vi = vi7 + 1;
                    finalVerts[vi7] = this.uvs.get(uvIndex + 1);
                    i5++;
                }
            }
            int numIndices = numFaces * 3 >= 32767 ? 0 : numFaces * 3;
            short[] finalIndices = new short[numIndices];
            if (numIndices > 0) {
                for (int i7 = 0; i7 < numIndices; i7++) {
                    finalIndices[i7] = (short) i7;
                }
            }
            Array<VertexAttribute> attributes = new Array<>();
            attributes.add(new VertexAttribute(1, 3, ShaderProgram.POSITION_ATTRIBUTE));
            if (hasNorms) {
                attributes.add(new VertexAttribute(8, 3, ShaderProgram.NORMAL_ATTRIBUTE));
            }
            if (hasUVs) {
                attributes.add(new VertexAttribute(16, 2, "a_texCoord0"));
            }
            id++;
            String meshId = "mesh" + id;
            String partId = "part" + id;
            ModelNode node = new ModelNode();
            node.id = "node" + id;
            node.meshId = meshId;
            node.scale = new Vector3(1.0f, 1.0f, 1.0f);
            node.translation = new Vector3();
            node.rotation = new Quaternion();
            ModelNodePart pm = new ModelNodePart();
            pm.meshPartId = partId;
            pm.materialId = group.materialName;
            node.parts = new ModelNodePart[]{pm};
            ModelMeshPart part = new ModelMeshPart();
            part.id = partId;
            part.indices = finalIndices;
            part.primitiveType = 4;
            ModelMesh mesh = new ModelMesh();
            mesh.id = meshId;
            mesh.attributes = (VertexAttribute[]) attributes.toArray(VertexAttribute.class);
            mesh.vertices = finalVerts;
            mesh.parts = new ModelMeshPart[]{part};
            data.nodes.add(node);
            data.meshes.add(mesh);
            data.materials.add(mtl.getMaterial(group.materialName));
        }
        if (this.verts.size > 0) {
            this.verts.clear();
        }
        if (this.norms.size > 0) {
            this.norms.clear();
        }
        if (this.uvs.size > 0) {
            this.uvs.clear();
        }
        if (this.groups.size <= 0) {
            return data;
        }
        this.groups.clear();
        return data;
    }

    private Group setActiveGroup(String name) {
        Iterator<Group> it = this.groups.iterator();
        while (it.hasNext()) {
            Group group = it.next();
            if (group.name.equals(name)) {
                return group;
            }
        }
        Group group2 = new Group(name);
        this.groups.add(group2);
        return group2;
    }

    private int getIndex(String index, int size) {
        if (index == null || index.length() == 0) {
            return 0;
        }
        int idx = Integer.parseInt(index);
        if (idx < 0) {
            return size + idx;
        }
        return idx - 1;
    }

    private class Group {
        Array<Integer> faces = new Array<>(200);
        boolean hasNorms;
        boolean hasUVs;
        Material mat = new Material("");
        String materialName = "default";
        final String name;
        int numFaces = 0;

        Group(String name2) {
            this.name = name2;
        }
    }
}
