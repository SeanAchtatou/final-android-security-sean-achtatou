package com.badlogic.gdx.graphics.g3d.attributes;

import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.NumberUtils;

public class BlendingAttribute extends Attribute {
    public static final String Alias = "blended";
    public static final long Type = register(Alias);
    public boolean blended;
    public int destFunction;
    public float opacity;
    public int sourceFunction;

    public static final boolean is(long mask) {
        return (Type & mask) == mask;
    }

    public BlendingAttribute() {
        this((BlendingAttribute) null);
    }

    public BlendingAttribute(boolean blended2, int sourceFunc, int destFunc, float opacity2) {
        super(Type);
        this.opacity = 1.0f;
        this.blended = blended2;
        this.sourceFunction = sourceFunc;
        this.destFunction = destFunc;
        this.opacity = opacity2;
    }

    public BlendingAttribute(int sourceFunc, int destFunc, float opacity2) {
        this(true, sourceFunc, destFunc, opacity2);
    }

    public BlendingAttribute(int sourceFunc, int destFunc) {
        this(sourceFunc, destFunc, 1.0f);
    }

    public BlendingAttribute(boolean blended2, float opacity2) {
        this(blended2, 770, 771, opacity2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute.<init>(boolean, float):void
     arg types: [int, float]
     candidates:
      com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute.<init>(int, int):void
      com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute.<init>(boolean, float):void */
    public BlendingAttribute(float opacity2) {
        this(true, opacity2);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public BlendingAttribute(BlendingAttribute copyFrom) {
        this(copyFrom == null ? true : copyFrom.blended, copyFrom == null ? 770 : copyFrom.sourceFunction, copyFrom == null ? 771 : copyFrom.destFunction, copyFrom == null ? 1.0f : copyFrom.opacity);
    }

    public BlendingAttribute copy() {
        return new BlendingAttribute(this);
    }

    public int hashCode() {
        return (((((((super.hashCode() * 947) + (this.blended ? 1 : 0)) * 947) + this.sourceFunction) * 947) + this.destFunction) * 947) + NumberUtils.floatToRawIntBits(this.opacity);
    }

    public int compareTo(Attribute o) {
        if (this.type != o.type) {
            return (int) (this.type - o.type);
        }
        BlendingAttribute other = (BlendingAttribute) o;
        if (this.blended != other.blended) {
            if (!this.blended) {
                return -1;
            }
            return 1;
        } else if (this.sourceFunction != other.sourceFunction) {
            return this.sourceFunction - other.sourceFunction;
        } else {
            if (this.destFunction != other.destFunction) {
                return this.destFunction - other.destFunction;
            }
            if (MathUtils.isEqual(this.opacity, other.opacity)) {
                return 0;
            }
            if (this.opacity >= other.opacity) {
                return -1;
            }
            return 1;
        }
    }
}
