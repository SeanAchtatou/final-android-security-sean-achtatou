package com.badlogic.gdx.graphics.g3d;

import com.badlogic.gdx.utils.Array;

public abstract class Attribute implements Comparable<Attribute> {
    private static final Array<String> types = new Array<>();
    public final long type;
    private final int typeBit;

    public abstract Attribute copy();

    public static final long getAttributeType(String alias) {
        for (int i = 0; i < types.size; i++) {
            if (types.get(i).compareTo(alias) == 0) {
                return 1 << i;
            }
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0007  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String getAttributeAlias(long r8) {
        /*
            r6 = 0
            r0 = -1
        L_0x0003:
            int r1 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r1 == 0) goto L_0x0016
            int r0 = r0 + 1
            r1 = 63
            if (r0 >= r1) goto L_0x0016
            long r2 = r8 >> r0
            r4 = 1
            long r2 = r2 & r4
            int r1 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r1 == 0) goto L_0x0003
        L_0x0016:
            if (r0 < 0) goto L_0x0027
            com.badlogic.gdx.utils.Array<java.lang.String> r1 = com.badlogic.gdx.graphics.g3d.Attribute.types
            int r1 = r1.size
            if (r0 >= r1) goto L_0x0027
            com.badlogic.gdx.utils.Array<java.lang.String> r1 = com.badlogic.gdx.graphics.g3d.Attribute.types
            java.lang.Object r1 = r1.get(r0)
            java.lang.String r1 = (java.lang.String) r1
        L_0x0026:
            return r1
        L_0x0027:
            r1 = 0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g3d.Attribute.getAttributeAlias(long):java.lang.String");
    }

    protected static final long register(String alias) {
        long result = getAttributeType(alias);
        if (result > 0) {
            return result;
        }
        types.add(alias);
        return 1 << (types.size - 1);
    }

    protected Attribute(long type2) {
        this.type = type2;
        this.typeBit = Long.numberOfTrailingZeros(type2);
    }

    /* access modifiers changed from: protected */
    public boolean equals(Attribute other) {
        return other.hashCode() == hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Attribute)) {
            return false;
        }
        Attribute other = (Attribute) obj;
        if (this.type == other.type) {
            return equals(other);
        }
        return false;
    }

    public String toString() {
        return getAttributeAlias(this.type);
    }

    public int hashCode() {
        return this.typeBit * 7489;
    }
}
