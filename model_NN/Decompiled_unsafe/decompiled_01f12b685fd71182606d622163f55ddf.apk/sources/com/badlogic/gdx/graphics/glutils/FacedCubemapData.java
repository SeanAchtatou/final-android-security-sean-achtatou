package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Cubemap;
import com.badlogic.gdx.graphics.CubemapData;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class FacedCubemapData implements CubemapData {
    protected final TextureData[] data;

    public FacedCubemapData() {
        this((TextureData) null, (TextureData) null, (TextureData) null, (TextureData) null, (TextureData) null, (TextureData) null);
    }

    public FacedCubemapData(FileHandle positiveX, FileHandle negativeX, FileHandle positiveY, FileHandle negativeY, FileHandle positiveZ, FileHandle negativeZ) {
        this(TextureData.Factory.loadFromFile(positiveX, false), TextureData.Factory.loadFromFile(negativeX, false), TextureData.Factory.loadFromFile(positiveY, false), TextureData.Factory.loadFromFile(negativeY, false), TextureData.Factory.loadFromFile(positiveZ, false), TextureData.Factory.loadFromFile(negativeZ, false));
    }

    public FacedCubemapData(FileHandle positiveX, FileHandle negativeX, FileHandle positiveY, FileHandle negativeY, FileHandle positiveZ, FileHandle negativeZ, boolean useMipMaps) {
        this(TextureData.Factory.loadFromFile(positiveX, useMipMaps), TextureData.Factory.loadFromFile(negativeX, useMipMaps), TextureData.Factory.loadFromFile(positiveY, useMipMaps), TextureData.Factory.loadFromFile(negativeY, useMipMaps), TextureData.Factory.loadFromFile(positiveZ, useMipMaps), TextureData.Factory.loadFromFile(negativeZ, useMipMaps));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.glutils.FacedCubemapData.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void
     arg types: [com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, int]
     candidates:
      com.badlogic.gdx.graphics.glutils.FacedCubemapData.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.glutils.FacedCubemapData.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void */
    public FacedCubemapData(Pixmap positiveX, Pixmap negativeX, Pixmap positiveY, Pixmap negativeY, Pixmap positiveZ, Pixmap negativeZ) {
        this(positiveX, negativeX, positiveY, negativeY, positiveZ, negativeZ, false);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FacedCubemapData(com.badlogic.gdx.graphics.Pixmap r9, com.badlogic.gdx.graphics.Pixmap r10, com.badlogic.gdx.graphics.Pixmap r11, com.badlogic.gdx.graphics.Pixmap r12, com.badlogic.gdx.graphics.Pixmap r13, com.badlogic.gdx.graphics.Pixmap r14, boolean r15) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            if (r9 != 0) goto L_0x0018
            r1 = r6
        L_0x0005:
            if (r10 != 0) goto L_0x001e
            r2 = r6
        L_0x0008:
            if (r11 != 0) goto L_0x0024
            r3 = r6
        L_0x000b:
            if (r12 != 0) goto L_0x002a
            r4 = r6
        L_0x000e:
            if (r13 != 0) goto L_0x0030
            r5 = r6
        L_0x0011:
            if (r14 != 0) goto L_0x0036
        L_0x0013:
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        L_0x0018:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r1 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r1.<init>(r9, r6, r15, r7)
            goto L_0x0005
        L_0x001e:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r2 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r2.<init>(r10, r6, r15, r7)
            goto L_0x0008
        L_0x0024:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r3 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r3.<init>(r11, r6, r15, r7)
            goto L_0x000b
        L_0x002a:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r4 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r4.<init>(r12, r6, r15, r7)
            goto L_0x000e
        L_0x0030:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r5 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r5.<init>(r13, r6, r15, r7)
            goto L_0x0011
        L_0x0036:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r0 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r0.<init>(r14, r6, r15, r7)
            r6 = r0
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.glutils.FacedCubemapData.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void");
    }

    public FacedCubemapData(int width, int height, int depth, Pixmap.Format format) {
        this(new PixmapTextureData(new Pixmap(depth, height, format), null, false, true), new PixmapTextureData(new Pixmap(depth, height, format), null, false, true), new PixmapTextureData(new Pixmap(width, depth, format), null, false, true), new PixmapTextureData(new Pixmap(width, depth, format), null, false, true), new PixmapTextureData(new Pixmap(width, height, format), null, false, true), new PixmapTextureData(new Pixmap(width, height, format), null, false, true));
    }

    public FacedCubemapData(TextureData positiveX, TextureData negativeX, TextureData positiveY, TextureData negativeY, TextureData positiveZ, TextureData negativeZ) {
        this.data = new TextureData[6];
        this.data[0] = positiveX;
        this.data[1] = negativeX;
        this.data[2] = positiveY;
        this.data[3] = negativeY;
        this.data[4] = positiveZ;
        this.data[5] = negativeZ;
    }

    public boolean isManaged() {
        for (TextureData data2 : this.data) {
            if (!data2.isManaged()) {
                return false;
            }
        }
        return true;
    }

    public void load(Cubemap.CubemapSide side, FileHandle file) {
        this.data[side.index] = TextureData.Factory.loadFromFile(file, false);
    }

    public void load(Cubemap.CubemapSide side, Pixmap pixmap) {
        PixmapTextureData pixmapTextureData = null;
        TextureData[] textureDataArr = this.data;
        int i = side.index;
        if (pixmap != null) {
            pixmapTextureData = new PixmapTextureData(pixmap, null, false, false);
        }
        textureDataArr[i] = pixmapTextureData;
    }

    public boolean isComplete() {
        for (TextureData textureData : this.data) {
            if (textureData == null) {
                return false;
            }
        }
        return true;
    }

    public TextureData getTextureData(Cubemap.CubemapSide side) {
        return this.data[side.index];
    }

    public int getWidth() {
        int tmp;
        int tmp2;
        int tmp3;
        int tmp4;
        int width = 0;
        if (this.data[Cubemap.CubemapSide.PositiveZ.index] != null && (tmp4 = this.data[Cubemap.CubemapSide.PositiveZ.index].getWidth()) > 0) {
            width = tmp4;
        }
        if (this.data[Cubemap.CubemapSide.NegativeZ.index] != null && (tmp3 = this.data[Cubemap.CubemapSide.NegativeZ.index].getWidth()) > width) {
            width = tmp3;
        }
        if (this.data[Cubemap.CubemapSide.PositiveY.index] != null && (tmp2 = this.data[Cubemap.CubemapSide.PositiveY.index].getWidth()) > width) {
            width = tmp2;
        }
        if (this.data[Cubemap.CubemapSide.NegativeY.index] == null || (tmp = this.data[Cubemap.CubemapSide.NegativeY.index].getWidth()) <= width) {
            return width;
        }
        return tmp;
    }

    public int getHeight() {
        int tmp;
        int tmp2;
        int tmp3;
        int tmp4;
        int height = 0;
        if (this.data[Cubemap.CubemapSide.PositiveZ.index] != null && (tmp4 = this.data[Cubemap.CubemapSide.PositiveZ.index].getHeight()) > 0) {
            height = tmp4;
        }
        if (this.data[Cubemap.CubemapSide.NegativeZ.index] != null && (tmp3 = this.data[Cubemap.CubemapSide.NegativeZ.index].getHeight()) > height) {
            height = tmp3;
        }
        if (this.data[Cubemap.CubemapSide.PositiveX.index] != null && (tmp2 = this.data[Cubemap.CubemapSide.PositiveX.index].getHeight()) > height) {
            height = tmp2;
        }
        if (this.data[Cubemap.CubemapSide.NegativeX.index] == null || (tmp = this.data[Cubemap.CubemapSide.NegativeX.index].getHeight()) <= height) {
            return height;
        }
        return tmp;
    }

    public boolean isPrepared() {
        return false;
    }

    public void prepare() {
        if (!isComplete()) {
            throw new GdxRuntimeException("You need to complete your cubemap data before using it");
        }
        for (int i = 0; i < this.data.length; i++) {
            if (!this.data[i].isPrepared()) {
                this.data[i].prepare();
            }
        }
    }

    public void consumeCubemapData() {
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i].getType() == TextureData.TextureDataType.Custom) {
                this.data[i].consumeCustomData(GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
            } else {
                Pixmap pixmap = this.data[i].consumePixmap();
                boolean disposePixmap = this.data[i].disposePixmap();
                if (this.data[i].getFormat() != pixmap.getFormat()) {
                    Pixmap tmp = new Pixmap(pixmap.getWidth(), pixmap.getHeight(), this.data[i].getFormat());
                    Pixmap.Blending blend = Pixmap.getBlending();
                    Pixmap.setBlending(Pixmap.Blending.None);
                    tmp.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                    Pixmap.setBlending(blend);
                    if (this.data[i].disposePixmap()) {
                        pixmap.dispose();
                    }
                    pixmap = tmp;
                }
                Gdx.gl.glPixelStorei(GL20.GL_UNPACK_ALIGNMENT, 1);
                Gdx.gl.glTexImage2D(GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, pixmap.getGLInternalFormat(), pixmap.getWidth(), pixmap.getHeight(), 0, pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
            }
        }
    }
}
