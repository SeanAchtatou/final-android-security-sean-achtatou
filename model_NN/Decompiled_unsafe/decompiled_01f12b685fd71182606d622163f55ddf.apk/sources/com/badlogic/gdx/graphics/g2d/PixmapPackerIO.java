package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.math.Rectangle;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

public class PixmapPackerIO {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$PixmapPackerIO$ImageFormat;

    public static class SaveParameters {
        public ImageFormat format = ImageFormat.PNG;
        public Texture.TextureFilter magFilter = Texture.TextureFilter.Nearest;
        public Texture.TextureFilter minFilter = Texture.TextureFilter.Nearest;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$PixmapPackerIO$ImageFormat() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$PixmapPackerIO$ImageFormat;
        if (iArr == null) {
            iArr = new int[ImageFormat.values().length];
            try {
                iArr[ImageFormat.CIM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ImageFormat.PNG.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$PixmapPackerIO$ImageFormat = iArr;
        }
        return iArr;
    }

    public enum ImageFormat {
        CIM(".cim"),
        PNG(".png");
        
        private final String extension;

        public String getExtension() {
            return this.extension;
        }

        private ImageFormat(String extension2) {
            this.extension = extension2;
        }
    }

    public void save(FileHandle file, PixmapPacker packer) throws IOException {
        save(file, packer, new SaveParameters());
    }

    public void save(FileHandle file, PixmapPacker packer, SaveParameters parameters) throws IOException {
        Writer writer = file.writer(false);
        int index = 0;
        Iterator<PixmapPacker.Page> it = packer.pages.iterator();
        while (it.hasNext()) {
            PixmapPacker.Page page = it.next();
            if (page.rects.size > 0) {
                index++;
                FileHandle pageFile = file.sibling(String.valueOf(file.nameWithoutExtension()) + "_" + index + parameters.format.getExtension());
                switch ($SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$PixmapPackerIO$ImageFormat()[parameters.format.ordinal()]) {
                    case 1:
                        PixmapIO.writeCIM(pageFile, page.image);
                        break;
                    case 2:
                        PixmapIO.writePNG(pageFile, page.image);
                        break;
                }
                writer.write("\n");
                writer.write(String.valueOf(pageFile.name()) + "\n");
                writer.write("size: " + page.image.getWidth() + "," + page.image.getHeight() + "\n");
                writer.write("format: " + packer.pageFormat.name() + "\n");
                writer.write("filter: " + parameters.minFilter.name() + "," + parameters.magFilter.name() + "\n");
                writer.write("repeat: none\n");
                Iterator it2 = page.rects.keys().iterator();
                while (it2.hasNext()) {
                    String name = (String) it2.next();
                    writer.write(String.valueOf(name) + "\n");
                    Rectangle rect = page.rects.get(name);
                    writer.write("rotate: false\n");
                    writer.write("xy: " + ((int) rect.x) + "," + ((int) rect.y) + "\n");
                    writer.write("size: " + ((int) rect.width) + "," + ((int) rect.height) + "\n");
                    writer.write("orig: " + ((int) rect.width) + "," + ((int) rect.height) + "\n");
                    writer.write("offset: 0, 0\n");
                    writer.write("index: -1\n");
                }
            }
        }
        writer.close();
    }
}
