package com.badlogic.gdx.graphics.g3d;

import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodeAnimation;
import com.badlogic.gdx.graphics.g3d.model.NodeKeyframe;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class ModelInstance implements RenderableProvider {
    public static boolean defaultShareKeyframes = true;
    public final Array<Animation> animations;
    public final Array<Material> materials;
    public final Model model;
    private ObjectMap<NodePart, ArrayMap<Node, Matrix4>> nodePartBones;
    public final Array<Node> nodes;
    public Matrix4 transform;
    public Object userData;

    public ModelInstance(Model model2) {
        this(model2, (String[]) null);
    }

    public ModelInstance(Model model2, String nodeId, boolean mergeTransform) {
        this(model2, null, nodeId, false, false, mergeTransform);
    }

    public ModelInstance(Model model2, Matrix4 transform2, String nodeId, boolean mergeTransform) {
        this(model2, transform2, nodeId, false, false, mergeTransform);
    }

    public ModelInstance(Model model2, String nodeId, boolean parentTransform, boolean mergeTransform) {
        this(model2, null, nodeId, true, parentTransform, mergeTransform);
    }

    public ModelInstance(Model model2, Matrix4 transform2, String nodeId, boolean parentTransform, boolean mergeTransform) {
        this(model2, transform2, nodeId, true, parentTransform, mergeTransform);
    }

    public ModelInstance(Model model2, String nodeId, boolean recursive, boolean parentTransform, boolean mergeTransform) {
        this(model2, null, nodeId, recursive, parentTransform, mergeTransform);
    }

    public ModelInstance(Model model2, Matrix4 transform2, String nodeId, boolean recursive, boolean parentTransform, boolean mergeTransform) {
        this(model2, transform2, nodeId, recursive, parentTransform, mergeTransform, defaultShareKeyframes);
    }

    public ModelInstance(Model model2, Matrix4 transform2, String nodeId, boolean recursive, boolean parentTransform, boolean mergeTransform, boolean shareKeyframes) {
        this.materials = new Array<>();
        this.nodes = new Array<>();
        this.animations = new Array<>();
        this.nodePartBones = new ObjectMap<>();
        this.model = model2;
        this.transform = transform2 == null ? new Matrix4() : transform2;
        this.nodePartBones.clear();
        Node node = model2.getNode(nodeId, recursive);
        Array<Node> array = this.nodes;
        Node copy = copyNode(node);
        array.add(copy);
        if (mergeTransform) {
            this.transform.mul(parentTransform ? node.globalTransform : node.localTransform);
            copy.translation.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            copy.rotation.idt();
            copy.scale.set(1.0f, 1.0f, 1.0f);
        } else if (parentTransform && copy.hasParent()) {
            this.transform.mul(node.getParent().globalTransform);
        }
        setBones();
        copyAnimations(model2.animations, shareKeyframes);
        calculateTransforms();
    }

    public ModelInstance(Model model2, String... rootNodeIds) {
        this(model2, (Matrix4) null, rootNodeIds);
    }

    public ModelInstance(Model model2, Matrix4 transform2, String... rootNodeIds) {
        this.materials = new Array<>();
        this.nodes = new Array<>();
        this.animations = new Array<>();
        this.nodePartBones = new ObjectMap<>();
        this.model = model2;
        this.transform = transform2 == null ? new Matrix4() : transform2;
        if (rootNodeIds == null) {
            copyNodes(model2.nodes);
        } else {
            copyNodes(model2.nodes, rootNodeIds);
        }
        copyAnimations(model2.animations, defaultShareKeyframes);
        calculateTransforms();
    }

    public ModelInstance(Model model2, Array<String> rootNodeIds) {
        this(model2, (Matrix4) null, rootNodeIds);
    }

    public ModelInstance(Model model2, Matrix4 transform2, Array<String> rootNodeIds) {
        this(model2, transform2, rootNodeIds, defaultShareKeyframes);
    }

    public ModelInstance(Model model2, Matrix4 transform2, Array<String> rootNodeIds, boolean shareKeyframes) {
        this.materials = new Array<>();
        this.nodes = new Array<>();
        this.animations = new Array<>();
        this.nodePartBones = new ObjectMap<>();
        this.model = model2;
        this.transform = transform2 == null ? new Matrix4() : transform2;
        copyNodes(model2.nodes, rootNodeIds);
        copyAnimations(model2.animations, shareKeyframes);
        calculateTransforms();
    }

    public ModelInstance(Model model2, Vector3 position) {
        this(model2);
        this.transform.setToTranslation(position);
    }

    public ModelInstance(Model model2, float x, float y, float z) {
        this(model2);
        this.transform.setToTranslation(x, y, z);
    }

    public ModelInstance(Model model2, Matrix4 transform2) {
        this(model2, transform2, (String[]) null);
    }

    public ModelInstance(ModelInstance copyFrom) {
        this(copyFrom, copyFrom.transform.cpy());
    }

    public ModelInstance(ModelInstance copyFrom, Matrix4 transform2) {
        this(copyFrom, transform2, defaultShareKeyframes);
    }

    public ModelInstance(ModelInstance copyFrom, Matrix4 transform2, boolean shareKeyframes) {
        this.materials = new Array<>();
        this.nodes = new Array<>();
        this.animations = new Array<>();
        this.nodePartBones = new ObjectMap<>();
        this.model = copyFrom.model;
        this.transform = transform2 == null ? new Matrix4() : transform2;
        copyNodes(copyFrom.nodes);
        copyAnimations(copyFrom.animations, shareKeyframes);
        calculateTransforms();
    }

    public ModelInstance copy() {
        return new ModelInstance(this);
    }

    private void copyNodes(Array<Node> nodes2) {
        this.nodePartBones.clear();
        int n = nodes2.size;
        for (int i = 0; i < n; i++) {
            this.nodes.add(copyNode(nodes2.get(i)));
        }
        setBones();
    }

    private void copyNodes(Array<Node> nodes2, String... nodeIds) {
        this.nodePartBones.clear();
        int n = nodes2.size;
        for (int i = 0; i < n; i++) {
            Node node = nodes2.get(i);
            int length = nodeIds.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (nodeIds[i2].equals(node.id)) {
                    this.nodes.add(copyNode(node));
                    break;
                } else {
                    i2++;
                }
            }
        }
        setBones();
    }

    private void copyNodes(Array<Node> nodes2, Array<String> nodeIds) {
        this.nodePartBones.clear();
        int n = nodes2.size;
        for (int i = 0; i < n; i++) {
            Node node = nodes2.get(i);
            Iterator<String> it = nodeIds.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().equals(node.id)) {
                        this.nodes.add(copyNode(node));
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        setBones();
    }

    private void setBones() {
        Iterator it = this.nodePartBones.entries().iterator();
        while (it.hasNext()) {
            ObjectMap.Entry<NodePart, ArrayMap<Node, Matrix4>> e = (ObjectMap.Entry) it.next();
            if (((NodePart) e.key).invBoneBindTransforms == null) {
                ((NodePart) e.key).invBoneBindTransforms = new ArrayMap<>(true, ((ArrayMap) e.value).size, Node.class, Matrix4.class);
            }
            ((NodePart) e.key).invBoneBindTransforms.clear();
            Iterator it2 = ((ArrayMap) e.value).entries().iterator();
            while (it2.hasNext()) {
                ObjectMap.Entry<Node, Matrix4> b = (ObjectMap.Entry) it2.next();
                ((NodePart) e.key).invBoneBindTransforms.put(getNode(((Node) b.key).id), (Matrix4) b.value);
            }
            ((NodePart) e.key).bones = new Matrix4[((ArrayMap) e.value).size];
            for (int i = 0; i < ((NodePart) e.key).bones.length; i++) {
                ((NodePart) e.key).bones[i] = new Matrix4();
            }
        }
    }

    private Node copyNode(Node node) {
        Node copy = new Node();
        copy.id = node.id;
        copy.inheritTransform = node.inheritTransform;
        copy.translation.set(node.translation);
        copy.rotation.set(node.rotation);
        copy.scale.set(node.scale);
        copy.localTransform.set(node.localTransform);
        copy.globalTransform.set(node.globalTransform);
        Iterator<NodePart> it = node.parts.iterator();
        while (it.hasNext()) {
            copy.parts.add(copyNodePart(it.next()));
        }
        for (Node child : node.getChildren()) {
            copy.addChild(copyNode(child));
        }
        return copy;
    }

    private NodePart copyNodePart(NodePart nodePart) {
        NodePart copy = new NodePart();
        copy.meshPart = new MeshPart();
        copy.meshPart.id = nodePart.meshPart.id;
        copy.meshPart.indexOffset = nodePart.meshPart.indexOffset;
        copy.meshPart.numVertices = nodePart.meshPart.numVertices;
        copy.meshPart.primitiveType = nodePart.meshPart.primitiveType;
        copy.meshPart.mesh = nodePart.meshPart.mesh;
        if (nodePart.invBoneBindTransforms != null) {
            this.nodePartBones.put(copy, nodePart.invBoneBindTransforms);
        }
        int index = this.materials.indexOf(nodePart.material, false);
        if (index < 0) {
            Array<Material> array = this.materials;
            Material copy2 = nodePart.material.copy();
            copy.material = copy2;
            array.add(copy2);
        } else {
            copy.material = this.materials.get(index);
        }
        return copy;
    }

    private void copyAnimations(Iterable<com.badlogic.gdx.graphics.g3d.model.Animation> source, boolean shareKeyframes) {
        for (com.badlogic.gdx.graphics.g3d.model.Animation anim : source) {
            com.badlogic.gdx.graphics.g3d.model.Animation animation = new com.badlogic.gdx.graphics.g3d.model.Animation();
            animation.id = anim.id;
            animation.duration = anim.duration;
            Iterator<NodeAnimation> it = anim.nodeAnimations.iterator();
            while (it.hasNext()) {
                NodeAnimation nanim = it.next();
                Node node = getNode(nanim.node.id);
                if (node != null) {
                    NodeAnimation nodeAnim = new NodeAnimation();
                    nodeAnim.node = node;
                    if (shareKeyframes) {
                        nodeAnim.translation = nanim.translation;
                        nodeAnim.rotation = nanim.rotation;
                        nodeAnim.scaling = nanim.scaling;
                    } else {
                        if (nanim.translation != null) {
                            nodeAnim.translation = new Array<>();
                            Iterator<NodeKeyframe<Vector3>> it2 = nanim.translation.iterator();
                            while (it2.hasNext()) {
                                NodeKeyframe<Vector3> kf = it2.next();
                                nodeAnim.translation.add(new NodeKeyframe(kf.keytime, (Vector3) kf.value));
                            }
                        }
                        if (nanim.rotation != null) {
                            nodeAnim.rotation = new Array<>();
                            Iterator<NodeKeyframe<Quaternion>> it3 = nanim.rotation.iterator();
                            while (it3.hasNext()) {
                                NodeKeyframe<Quaternion> kf2 = it3.next();
                                nodeAnim.rotation.add(new NodeKeyframe(kf2.keytime, (Quaternion) kf2.value));
                            }
                        }
                        if (nanim.scaling != null) {
                            nodeAnim.scaling = new Array<>();
                            Iterator<NodeKeyframe<Vector3>> it4 = nanim.scaling.iterator();
                            while (it4.hasNext()) {
                                NodeKeyframe<Vector3> kf3 = it4.next();
                                nodeAnim.scaling.add(new NodeKeyframe(kf3.keytime, (Vector3) kf3.value));
                            }
                        }
                    }
                    if (nodeAnim.translation != null || nodeAnim.rotation != null || nodeAnim.scaling != null) {
                        animation.nodeAnimations.add(nodeAnim);
                    }
                }
            }
            if (animation.nodeAnimations.size > 0) {
                this.animations.add(animation);
            }
        }
    }

    public void getRenderables(Array<Renderable> renderables, Pool<Renderable> pool) {
        Iterator<Node> it = this.nodes.iterator();
        while (it.hasNext()) {
            getRenderables(it.next(), renderables, pool);
        }
    }

    public Renderable getRenderable(Renderable out) {
        return getRenderable(out, this.nodes.get(0));
    }

    public Renderable getRenderable(Renderable out, Node node) {
        return getRenderable(out, node, node.parts.get(0));
    }

    public Renderable getRenderable(Renderable out, Node node, NodePart nodePart) {
        nodePart.setRenderable(out);
        if (nodePart.bones == null && this.transform != null) {
            out.worldTransform.set(this.transform).mul(node.globalTransform);
        } else if (this.transform != null) {
            out.worldTransform.set(this.transform);
        } else {
            out.worldTransform.idt();
        }
        out.userData = this.userData;
        return out;
    }

    /* access modifiers changed from: protected */
    public void getRenderables(Node node, Array<Renderable> renderables, Pool<Renderable> pool) {
        if (node.parts.size > 0) {
            Iterator<NodePart> it = node.parts.iterator();
            while (it.hasNext()) {
                NodePart nodePart = it.next();
                if (nodePart.enabled) {
                    renderables.add(getRenderable(pool.obtain(), node, nodePart));
                }
            }
        }
        for (Node child : node.getChildren()) {
            getRenderables(child, renderables, pool);
        }
    }

    public void calculateTransforms() {
        int n = this.nodes.size;
        for (int i = 0; i < n; i++) {
            this.nodes.get(i).calculateTransforms(true);
        }
        for (int i2 = 0; i2 < n; i2++) {
            this.nodes.get(i2).calculateBoneTransforms(true);
        }
    }

    public BoundingBox calculateBoundingBox(BoundingBox out) {
        out.inf();
        return extendBoundingBox(out);
    }

    public BoundingBox extendBoundingBox(BoundingBox out) {
        int n = this.nodes.size;
        for (int i = 0; i < n; i++) {
            this.nodes.get(i).extendBoundingBox(out);
        }
        return out;
    }

    public com.badlogic.gdx.graphics.g3d.model.Animation getAnimation(String id) {
        return getAnimation(id, true);
    }

    public com.badlogic.gdx.graphics.g3d.model.Animation getAnimation(String id, boolean ignoreCase) {
        int n = this.animations.size;
        if (ignoreCase) {
            for (int i = 0; i < n; i++) {
                com.badlogic.gdx.graphics.g3d.model.Animation animation = this.animations.get(i);
                if (animation.id.equalsIgnoreCase(id)) {
                    return animation;
                }
            }
        } else {
            for (int i2 = 0; i2 < n; i2++) {
                com.badlogic.gdx.graphics.g3d.model.Animation animation2 = this.animations.get(i2);
                if (animation2.id.equals(id)) {
                    return animation2;
                }
            }
        }
        return null;
    }

    public Material getMaterial(String id) {
        return getMaterial(id, true);
    }

    public Material getMaterial(String id, boolean ignoreCase) {
        int n = this.materials.size;
        if (ignoreCase) {
            for (int i = 0; i < n; i++) {
                Material material = this.materials.get(i);
                if (material.id.equalsIgnoreCase(id)) {
                    return material;
                }
            }
        } else {
            for (int i2 = 0; i2 < n; i2++) {
                Material material2 = this.materials.get(i2);
                if (material2.id.equals(id)) {
                    return material2;
                }
            }
        }
        return null;
    }

    public Node getNode(String id) {
        return getNode(id, true);
    }

    public Node getNode(String id, boolean recursive) {
        return getNode(id, recursive, false);
    }

    public Node getNode(String id, boolean recursive, boolean ignoreCase) {
        return Node.getNode(this.nodes, id, recursive, ignoreCase);
    }
}
