package com.badlogic.gdx.graphics.g3d.environment;

import com.badlogic.gdx.graphics.Color;
import com.kbz.esotericsoftware.spine.Animation;

public abstract class BaseLight {
    public final Color color = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
}
