package com.badlogic.gdx.graphics.g3d.environment;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.kbz.esotericsoftware.spine.Animation;

public class AmbientCubemap {
    public final float[] data;

    private static final float clamp(float v) {
        if (v < Animation.CurveTimeline.LINEAR) {
            return Animation.CurveTimeline.LINEAR;
        }
        if (v > 1.0f) {
            return 1.0f;
        }
        return v;
    }

    public AmbientCubemap() {
        this.data = new float[18];
    }

    public AmbientCubemap(float[] copyFrom) {
        if (copyFrom.length != 18) {
            throw new GdxRuntimeException("Incorrect array size");
        }
        this.data = new float[copyFrom.length];
        System.arraycopy(copyFrom, 0, this.data, 0, this.data.length);
    }

    public AmbientCubemap(AmbientCubemap copyFrom) {
        this(copyFrom.data);
    }

    public AmbientCubemap set(float[] values) {
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = values[i];
        }
        return this;
    }

    public AmbientCubemap set(AmbientCubemap other) {
        return set(other.data);
    }

    public AmbientCubemap set(Color color) {
        return set(color.r, color.g, color.b);
    }

    public AmbientCubemap set(float r, float g, float b) {
        int idx = 0;
        while (idx < this.data.length) {
            int idx2 = idx + 1;
            this.data[idx] = r;
            int idx3 = idx2 + 1;
            this.data[idx2] = g;
            this.data[idx3] = b;
            idx = idx3 + 1;
        }
        return this;
    }

    public Color getColor(Color out, int side) {
        int side2 = side * 3;
        return out.set(this.data[side2], this.data[side2 + 1], this.data[side2 + 2], 1.0f);
    }

    public AmbientCubemap clear() {
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = 0.0f;
        }
        return this;
    }

    public AmbientCubemap clamp() {
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = clamp(this.data[i]);
        }
        return this;
    }

    public AmbientCubemap add(float r, float g, float b) {
        int idx = 0;
        while (idx < this.data.length) {
            float[] fArr = this.data;
            int idx2 = idx + 1;
            fArr[idx] = fArr[idx] + r;
            float[] fArr2 = this.data;
            int idx3 = idx2 + 1;
            fArr2[idx2] = fArr2[idx2] + g;
            float[] fArr3 = this.data;
            fArr3[idx3] = fArr3[idx3] + b;
            idx = idx3 + 1;
        }
        return this;
    }

    public AmbientCubemap add(Color color) {
        return add(color.r, color.g, color.b);
    }

    public AmbientCubemap add(float r, float g, float b, float x, float y, float z) {
        float x2 = x * x;
        float y2 = y * y;
        float z2 = z * z;
        float d = x2 + y2 + z2;
        if (d != Animation.CurveTimeline.LINEAR) {
            float d2 = (1.0f / d) * (1.0f + d);
            float rd = r * d2;
            float gd = g * d2;
            float bd = b * d2;
            int idx = x > Animation.CurveTimeline.LINEAR ? 0 : 3;
            float[] fArr = this.data;
            fArr[idx] = fArr[idx] + (x2 * rd);
            float[] fArr2 = this.data;
            int i = idx + 1;
            fArr2[i] = fArr2[i] + (x2 * gd);
            float[] fArr3 = this.data;
            int i2 = idx + 2;
            fArr3[i2] = fArr3[i2] + (x2 * bd);
            int idx2 = y > Animation.CurveTimeline.LINEAR ? 6 : 9;
            float[] fArr4 = this.data;
            fArr4[idx2] = fArr4[idx2] + (y2 * rd);
            float[] fArr5 = this.data;
            int i3 = idx2 + 1;
            fArr5[i3] = fArr5[i3] + (y2 * gd);
            float[] fArr6 = this.data;
            int i4 = idx2 + 2;
            fArr6[i4] = fArr6[i4] + (y2 * bd);
            int idx3 = z > Animation.CurveTimeline.LINEAR ? 12 : 15;
            float[] fArr7 = this.data;
            fArr7[idx3] = fArr7[idx3] + (z2 * rd);
            float[] fArr8 = this.data;
            int i5 = idx3 + 1;
            fArr8[i5] = fArr8[i5] + (z2 * gd);
            float[] fArr9 = this.data;
            int i6 = idx3 + 2;
            fArr9[i6] = fArr9[i6] + (z2 * bd);
        }
        return this;
    }

    public AmbientCubemap add(Color color, Vector3 direction) {
        return add(color.r, color.g, color.b, direction.x, direction.y, direction.z);
    }

    public AmbientCubemap add(float r, float g, float b, Vector3 direction) {
        return add(r, g, b, direction.x, direction.y, direction.z);
    }

    public AmbientCubemap add(Color color, float x, float y, float z) {
        return add(color.r, color.g, color.b, x, y, z);
    }

    public AmbientCubemap add(Color color, Vector3 point, Vector3 target) {
        return add(color.r, color.g, color.b, target.x - point.x, target.y - point.y, target.z - point.z);
    }

    public AmbientCubemap add(Color color, Vector3 point, Vector3 target, float intensity) {
        float t = intensity / (1.0f + target.dst(point));
        return add(color.r * t, color.g * t, color.b * t, target.x - point.x, target.y - point.y, target.z - point.z);
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < this.data.length; i += 3) {
            result = String.valueOf(result) + Float.toString(this.data[i]) + ", " + Float.toString(this.data[i + 1]) + ", " + Float.toString(this.data[i + 2]) + "\n";
        }
        return result;
    }
}
