package com.badlogic.gdx.graphics;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.ByteArray;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.StreamUtils;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class PixmapIO {
    public static void writeCIM(FileHandle file, Pixmap pixmap) {
        CIM.write(file, pixmap);
    }

    public static Pixmap readCIM(FileHandle file) {
        return CIM.read(file);
    }

    public static void writePNG(FileHandle file, Pixmap pixmap) {
        PNG writer;
        try {
            writer = new PNG((int) (((float) (pixmap.getWidth() * pixmap.getHeight())) * 1.5f));
            writer.setFlipY(false);
            writer.write(file, pixmap);
            writer.dispose();
        } catch (IOException ex) {
            throw new GdxRuntimeException("Error writing PNG: " + file, ex);
        } catch (Throwable th) {
            writer.dispose();
            throw th;
        }
    }

    private static class CIM {
        private static final int BUFFER_SIZE = 32000;
        private static final byte[] readBuffer = new byte[BUFFER_SIZE];
        private static final byte[] writeBuffer = new byte[BUFFER_SIZE];

        private CIM() {
        }

        public static void write(FileHandle file, Pixmap pixmap) {
            DataOutputStream out = null;
            try {
                DataOutputStream out2 = new DataOutputStream(new DeflaterOutputStream(file.write(false)));
                try {
                    out2.writeInt(pixmap.getWidth());
                    out2.writeInt(pixmap.getHeight());
                    out2.writeInt(Pixmap.Format.toGdx2DPixmapFormat(pixmap.getFormat()));
                    ByteBuffer pixelBuf = pixmap.getPixels();
                    pixelBuf.position(0);
                    pixelBuf.limit(pixelBuf.capacity());
                    int remainingBytes = pixelBuf.capacity() % BUFFER_SIZE;
                    int iterations = pixelBuf.capacity() / BUFFER_SIZE;
                    synchronized (writeBuffer) {
                        for (int i = 0; i < iterations; i++) {
                            pixelBuf.get(writeBuffer);
                            out2.write(writeBuffer);
                        }
                        pixelBuf.get(writeBuffer, 0, remainingBytes);
                        out2.write(writeBuffer, 0, remainingBytes);
                    }
                    pixelBuf.position(0);
                    pixelBuf.limit(pixelBuf.capacity());
                    StreamUtils.closeQuietly(out2);
                } catch (Exception e) {
                    e = e;
                    out = out2;
                } catch (Throwable th) {
                    th = th;
                    out = out2;
                    StreamUtils.closeQuietly(out);
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    throw new GdxRuntimeException("Couldn't write Pixmap to file '" + file + "'", e);
                } catch (Throwable th2) {
                    th = th2;
                    StreamUtils.closeQuietly(out);
                    throw th;
                }
            }
        }

        public static Pixmap read(FileHandle file) {
            DataInputStream in = null;
            try {
                DataInputStream in2 = new DataInputStream(new InflaterInputStream(new BufferedInputStream(file.read())));
                try {
                    Pixmap pixmap = new Pixmap(in2.readInt(), in2.readInt(), Pixmap.Format.fromGdx2DPixmapFormat(in2.readInt()));
                    ByteBuffer pixelBuf = pixmap.getPixels();
                    pixelBuf.position(0);
                    pixelBuf.limit(pixelBuf.capacity());
                    synchronized (readBuffer) {
                        while (true) {
                            int readBytes = in2.read(readBuffer);
                            if (readBytes > 0) {
                                pixelBuf.put(readBuffer, 0, readBytes);
                            }
                        }
                    }
                    pixelBuf.position(0);
                    pixelBuf.limit(pixelBuf.capacity());
                    StreamUtils.closeQuietly(in2);
                    return pixmap;
                } catch (Exception e) {
                    e = e;
                    in = in2;
                } catch (Throwable th) {
                    th = th;
                    in = in2;
                    StreamUtils.closeQuietly(in);
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    throw new GdxRuntimeException("Couldn't read Pixmap from file '" + file + "'", e);
                } catch (Throwable th2) {
                    th = th2;
                    StreamUtils.closeQuietly(in);
                    throw th;
                }
            }
        }
    }

    public static class PNG implements Disposable {
        private static final byte COLOR_ARGB = 6;
        private static final byte COMPRESSION_DEFLATE = 0;
        private static final byte FILTER_NONE = 0;
        private static final int IDAT = 1229209940;
        private static final int IEND = 1229278788;
        private static final int IHDR = 1229472850;
        private static final byte INTERLACE_NONE = 0;
        private static final byte PAETH = 4;
        private static final byte[] SIGNATURE = {-119, 80, 78, 71, 13, 10, 26, 10};
        private final ChunkBuffer buffer;
        private ByteArray curLineBytes;
        private final Deflater deflater;
        private final DeflaterOutputStream deflaterOutput;
        private boolean flipY;
        private int lastLineLen;
        private ByteArray lineOutBytes;
        private ByteArray prevLineBytes;

        public PNG() {
            this(16384);
        }

        public PNG(int initialBufferSize) {
            this.flipY = true;
            this.buffer = new ChunkBuffer(initialBufferSize);
            this.deflater = new Deflater();
            this.deflaterOutput = new DeflaterOutputStream(this.buffer, this.deflater);
        }

        public void setFlipY(boolean flipY2) {
            this.flipY = flipY2;
        }

        public void setCompression(int level) {
            this.deflater.setLevel(level);
        }

        public void write(FileHandle file, Pixmap pixmap) throws IOException {
            OutputStream output = file.write(false);
            try {
                write(output, pixmap);
            } finally {
                StreamUtils.closeQuietly(output);
            }
        }

        public void write(OutputStream output, Pixmap pixmap) throws IOException {
            byte[] lineOut;
            byte[] curLine;
            byte[] prevLine;
            int py;
            DataOutputStream dataOutput = new DataOutputStream(output);
            dataOutput.write(SIGNATURE);
            this.buffer.writeInt(IHDR);
            this.buffer.writeInt(pixmap.getWidth());
            this.buffer.writeInt(pixmap.getHeight());
            this.buffer.writeByte(8);
            this.buffer.writeByte(6);
            this.buffer.writeByte(0);
            this.buffer.writeByte(0);
            this.buffer.writeByte(0);
            this.buffer.endChunk(dataOutput);
            this.buffer.writeInt(IDAT);
            this.deflater.reset();
            int lineLen = pixmap.getWidth() * 4;
            if (this.lineOutBytes == null) {
                ByteArray byteArray = new ByteArray(lineLen);
                this.lineOutBytes = byteArray;
                lineOut = byteArray.items;
                ByteArray byteArray2 = new ByteArray(lineLen);
                this.curLineBytes = byteArray2;
                curLine = byteArray2.items;
                ByteArray byteArray3 = new ByteArray(lineLen);
                this.prevLineBytes = byteArray3;
                prevLine = byteArray3.items;
            } else {
                lineOut = this.lineOutBytes.ensureCapacity(lineLen);
                curLine = this.curLineBytes.ensureCapacity(lineLen);
                prevLine = this.prevLineBytes.ensureCapacity(lineLen);
                int n = this.lastLineLen;
                for (int i = 0; i < n; i++) {
                    prevLine[i] = 0;
                }
            }
            this.lastLineLen = lineLen;
            ByteBuffer pixels = pixmap.getPixels();
            int oldPosition = pixels.position();
            boolean rgba8888 = pixmap.getFormat() == Pixmap.Format.RGBA8888;
            int h = pixmap.getHeight();
            for (int y = 0; y < h; y++) {
                if (this.flipY) {
                    py = (h - y) - 1;
                } else {
                    py = y;
                }
                if (rgba8888) {
                    pixels.position(py * lineLen);
                    pixels.get(curLine, 0, lineLen);
                } else {
                    int x = 0;
                    for (int px = 0; px < pixmap.getWidth(); px++) {
                        int pixel = pixmap.getPixel(px, py);
                        int x2 = x + 1;
                        curLine[x] = (byte) ((pixel >> 24) & 255);
                        int x3 = x2 + 1;
                        curLine[x2] = (byte) ((pixel >> 16) & 255);
                        int x4 = x3 + 1;
                        curLine[x3] = (byte) ((pixel >> 8) & 255);
                        x = x4 + 1;
                        curLine[x4] = (byte) (pixel & 255);
                    }
                }
                lineOut[0] = (byte) (curLine[0] - prevLine[0]);
                lineOut[1] = (byte) (curLine[1] - prevLine[1]);
                lineOut[2] = (byte) (curLine[2] - prevLine[2]);
                lineOut[3] = (byte) (curLine[3] - prevLine[3]);
                for (int x5 = 4; x5 < lineLen; x5++) {
                    int a = curLine[x5 - 4] & 255;
                    int b = prevLine[x5] & 255;
                    int c = prevLine[x5 - 4] & 255;
                    int p = (a + b) - c;
                    int pa = p - a;
                    if (pa < 0) {
                        pa = -pa;
                    }
                    int pb = p - b;
                    if (pb < 0) {
                        pb = -pb;
                    }
                    int pc = p - c;
                    if (pc < 0) {
                        pc = -pc;
                    }
                    if (pa <= pb && pa <= pc) {
                        c = a;
                    } else if (pb <= pc) {
                        c = b;
                    }
                    lineOut[x5] = (byte) (curLine[x5] - c);
                }
                this.deflaterOutput.write(4);
                this.deflaterOutput.write(lineOut, 0, lineLen);
                byte[] temp = curLine;
                curLine = prevLine;
                prevLine = temp;
            }
            pixels.position(oldPosition);
            this.deflaterOutput.finish();
            this.buffer.endChunk(dataOutput);
            this.buffer.writeInt(IEND);
            this.buffer.endChunk(dataOutput);
            output.flush();
        }

        public void dispose() {
            this.deflater.end();
        }

        static class ChunkBuffer extends DataOutputStream {
            final ByteArrayOutputStream buffer;
            final CRC32 crc;

            ChunkBuffer(int initialSize) {
                this(new ByteArrayOutputStream(initialSize), new CRC32());
            }

            private ChunkBuffer(ByteArrayOutputStream buffer2, CRC32 crc2) {
                super(new CheckedOutputStream(buffer2, crc2));
                this.buffer = buffer2;
                this.crc = crc2;
            }

            public void endChunk(DataOutputStream target) throws IOException {
                flush();
                target.writeInt(this.buffer.size() - 4);
                this.buffer.writeTo(target);
                target.writeInt((int) this.crc.getValue());
                this.buffer.reset();
                this.crc.reset();
            }
        }
    }
}
