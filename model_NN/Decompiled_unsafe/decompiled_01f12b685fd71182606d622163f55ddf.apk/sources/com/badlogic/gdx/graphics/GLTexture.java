package com.badlogic.gdx.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.glutils.MipMapGenerator;
import com.badlogic.gdx.utils.Disposable;

public abstract class GLTexture implements Disposable {
    protected int glHandle;
    public final int glTarget;
    protected Texture.TextureFilter magFilter;
    protected Texture.TextureFilter minFilter;
    protected Texture.TextureWrap uWrap;
    protected Texture.TextureWrap vWrap;

    public abstract int getDepth();

    public abstract int getHeight();

    public abstract int getWidth();

    public abstract boolean isManaged();

    /* access modifiers changed from: protected */
    public abstract void reload();

    public GLTexture(int glTarget2) {
        this(glTarget2, Gdx.gl.glGenTexture());
    }

    public GLTexture(int glTarget2, int glHandle2) {
        this.minFilter = Texture.TextureFilter.Nearest;
        this.magFilter = Texture.TextureFilter.Nearest;
        this.uWrap = Texture.TextureWrap.ClampToEdge;
        this.vWrap = Texture.TextureWrap.ClampToEdge;
        this.glTarget = glTarget2;
        this.glHandle = glHandle2;
    }

    public void bind() {
        Gdx.gl.glBindTexture(this.glTarget, this.glHandle);
    }

    public void bind(int unit) {
        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0 + unit);
        Gdx.gl.glBindTexture(this.glTarget, this.glHandle);
    }

    public Texture.TextureFilter getMinFilter() {
        return this.minFilter;
    }

    public Texture.TextureFilter getMagFilter() {
        return this.magFilter;
    }

    public Texture.TextureWrap getUWrap() {
        return this.uWrap;
    }

    public Texture.TextureWrap getVWrap() {
        return this.vWrap;
    }

    public int getTextureObjectHandle() {
        return this.glHandle;
    }

    public void unsafeSetWrap(Texture.TextureWrap u, Texture.TextureWrap v) {
        unsafeSetWrap(u, v, false);
    }

    public void unsafeSetWrap(Texture.TextureWrap u, Texture.TextureWrap v, boolean force) {
        if (u != null && (force || this.uWrap != u)) {
            Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_WRAP_S, (float) u.getGLEnum());
            this.uWrap = u;
        }
        if (v == null) {
            return;
        }
        if (force || this.vWrap != v) {
            Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_WRAP_T, (float) v.getGLEnum());
            this.vWrap = v;
        }
    }

    public void setWrap(Texture.TextureWrap u, Texture.TextureWrap v) {
        this.uWrap = u;
        this.vWrap = v;
        bind();
        Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_WRAP_S, (float) u.getGLEnum());
        Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_WRAP_T, (float) v.getGLEnum());
    }

    public void unsafeSetFilter(Texture.TextureFilter minFilter2, Texture.TextureFilter magFilter2) {
        unsafeSetFilter(minFilter2, magFilter2, false);
    }

    public void unsafeSetFilter(Texture.TextureFilter minFilter2, Texture.TextureFilter magFilter2, boolean force) {
        if (minFilter2 != null && (force || this.minFilter != minFilter2)) {
            Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_MIN_FILTER, (float) minFilter2.getGLEnum());
            this.minFilter = minFilter2;
        }
        if (magFilter2 == null) {
            return;
        }
        if (force || this.magFilter != magFilter2) {
            Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_MAG_FILTER, (float) magFilter2.getGLEnum());
            this.magFilter = magFilter2;
        }
    }

    public void setFilter(Texture.TextureFilter minFilter2, Texture.TextureFilter magFilter2) {
        this.minFilter = minFilter2;
        this.magFilter = magFilter2;
        bind();
        Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_MIN_FILTER, (float) minFilter2.getGLEnum());
        Gdx.gl.glTexParameterf(this.glTarget, GL20.GL_TEXTURE_MAG_FILTER, (float) magFilter2.getGLEnum());
    }

    /* access modifiers changed from: protected */
    public void delete() {
        if (this.glHandle != 0) {
            Gdx.gl.glDeleteTexture(this.glHandle);
            this.glHandle = 0;
        }
    }

    public void dispose() {
        delete();
    }

    protected static void uploadImageData(int target, TextureData data) {
        uploadImageData(target, data, 0);
    }

    public static void uploadImageData(int target, TextureData data, int miplevel) {
        if (data != null) {
            if (!data.isPrepared()) {
                data.prepare();
            }
            if (data.getType() == TextureData.TextureDataType.Custom) {
                data.consumeCustomData(target);
                return;
            }
            Pixmap pixmap = data.consumePixmap();
            boolean disposePixmap = data.disposePixmap();
            if (data.getFormat() != pixmap.getFormat()) {
                Pixmap tmp = new Pixmap(pixmap.getWidth(), pixmap.getHeight(), data.getFormat());
                Pixmap.Blending blend = Pixmap.getBlending();
                Pixmap.setBlending(Pixmap.Blending.None);
                tmp.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
                Pixmap.setBlending(blend);
                if (data.disposePixmap()) {
                    pixmap.dispose();
                }
                pixmap = tmp;
                disposePixmap = true;
            }
            Gdx.gl.glPixelStorei(GL20.GL_UNPACK_ALIGNMENT, 1);
            if (data.useMipMaps()) {
                MipMapGenerator.generateMipMap(target, pixmap, pixmap.getWidth(), pixmap.getHeight());
            } else {
                Gdx.gl.glTexImage2D(target, miplevel, pixmap.getGLInternalFormat(), pixmap.getWidth(), pixmap.getHeight(), 0, pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
            }
            if (disposePixmap) {
                pixmap.dispose();
            }
        }
    }
}
