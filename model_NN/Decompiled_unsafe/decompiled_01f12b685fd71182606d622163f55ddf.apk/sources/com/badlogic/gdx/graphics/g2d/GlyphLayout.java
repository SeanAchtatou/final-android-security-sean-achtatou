package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Colors;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;

public class GlyphLayout implements Pool.Poolable {
    private static final Array<Color> colorStack = new Array<>(4);
    public float height;
    public final Array<GlyphRun> runs = new Array<>();
    public float width;

    public GlyphLayout() {
    }

    public GlyphLayout(BitmapFont font, CharSequence str) {
        setText(font, str);
    }

    public GlyphLayout(BitmapFont font, CharSequence str, Color color, float targetWidth, int halign, boolean wrap) {
        setText(font, str, color, targetWidth, halign, wrap);
    }

    public GlyphLayout(BitmapFont font, CharSequence str, int start, int end, Color color, float targetWidth, int halign, boolean wrap, String truncate) {
        setText(font, str, start, end, color, targetWidth, halign, wrap, truncate);
    }

    public void setText(BitmapFont font, CharSequence str) {
        setText(font, str, 0, str.length(), font.getColor(), Animation.CurveTimeline.LINEAR, 8, false, null);
    }

    public void setText(BitmapFont font, CharSequence str, Color color, float targetWidth, int halign, boolean wrap) {
        setText(font, str, 0, str.length(), color, targetWidth, halign, wrap, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x023b  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0251  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setText(com.badlogic.gdx.graphics.g2d.BitmapFont r38, java.lang.CharSequence r39, int r40, int r41, com.badlogic.gdx.graphics.Color r42, float r43, int r44, boolean r45, java.lang.String r46) {
        /*
            r37 = this;
            if (r46 == 0) goto L_0x009d
            r45 = 1
        L_0x0004:
            r0 = r38
            com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData r5 = r0.data
            boolean r0 = r5.markupEnabled
            r20 = r0
            java.lang.Class<com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun> r4 = com.badlogic.gdx.graphics.g2d.GlyphLayout.GlyphRun.class
            com.badlogic.gdx.utils.Pool r10 = com.badlogic.gdx.utils.Pools.get(r4)
            r0 = r37
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun> r0 = r0.runs
            r27 = r0
            r0 = r27
            r10.freeAll(r0)
            r27.clear()
            r31 = 0
            r34 = 0
            r30 = 0
            r19 = 0
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.Color> r13 = com.badlogic.gdx.graphics.g2d.GlyphLayout.colorStack
            r24 = r42
            r0 = r42
            r13.add(r0)
            java.lang.Class<com.badlogic.gdx.graphics.Color> r4 = com.badlogic.gdx.graphics.Color.class
            com.badlogic.gdx.utils.Pool r12 = com.badlogic.gdx.utils.Pools.get(r4)
            r26 = r40
            r29 = r40
        L_0x003b:
            r25 = -1
            r22 = 0
            r0 = r29
            r1 = r41
            if (r0 != r1) goto L_0x0104
            r0 = r26
            r1 = r41
            if (r0 != r1) goto L_0x00ab
            r40 = r29
        L_0x004d:
            float r30 = java.lang.Math.max(r30, r31)
            r9 = 1
            int r0 = r13.size
            r21 = r0
        L_0x0056:
            r0 = r21
            if (r9 < r0) goto L_0x01ea
            r13.clear()
            r4 = r44 & 8
            if (r4 != 0) goto L_0x0086
            r4 = r44 & 1
            if (r4 == 0) goto L_0x01f7
            r11 = 1
        L_0x0066:
            r17 = 0
            r18 = -822083584(0xffffffffcf000000, float:-2.14748365E9)
            r15 = 0
            r0 = r27
            int r0 = r0.size
            r21 = r0
            r9 = 0
        L_0x0072:
            r0 = r21
            if (r9 < r0) goto L_0x01fa
            float r28 = r43 - r17
            if (r11 == 0) goto L_0x024d
            r4 = 1073741824(0x40000000, float:2.0)
            float r28 = r28 / r4
            r16 = r15
        L_0x0080:
            r0 = r16
            r1 = r21
            if (r0 < r1) goto L_0x023b
        L_0x0086:
            r0 = r30
            r1 = r37
            r1.width = r0
            float r4 = r5.capHeight
            r0 = r19
            float r7 = (float) r0
            float r0 = r5.lineHeight
            r35 = r0
            float r7 = r7 * r35
            float r4 = r4 + r7
            r0 = r37
            r0.height = r4
            return
        L_0x009d:
            r0 = r38
            com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData r4 = r0.data
            float r4 = r4.spaceWidth
            int r4 = (r43 > r4 ? 1 : (r43 == r4 ? 0 : -1))
            if (r4 > 0) goto L_0x0004
            r45 = 0
            goto L_0x0004
        L_0x00ab:
            r25 = r41
            r40 = r29
        L_0x00af:
            r4 = -1
            r0 = r25
            if (r0 == r4) goto L_0x0251
            r0 = r25
            r1 = r26
            if (r0 == r1) goto L_0x00fc
            java.lang.Object r6 = r10.obtain()
            com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun r6 = (com.badlogic.gdx.graphics.g2d.GlyphLayout.GlyphRun) r6
            r0 = r27
            r0.add(r6)
            com.badlogic.gdx.graphics.Color r4 = r6.color
            r0 = r42
            r4.set(r0)
            r0 = r31
            r6.x = r0
            r0 = r34
            r6.y = r0
            r0 = r39
            r1 = r26
            r2 = r25
            r5.getGlyphs(r6, r0, r1, r2)
            com.badlogic.gdx.utils.FloatArray r4 = r6.xAdvances
            float[] r0 = r4.items
            r33 = r0
            r9 = 0
            com.badlogic.gdx.utils.FloatArray r4 = r6.xAdvances
            int r0 = r4.size
            r21 = r0
        L_0x00ea:
            r0 = r21
            if (r9 < r0) goto L_0x0135
            if (r22 == 0) goto L_0x00fc
            float r30 = java.lang.Math.max(r30, r31)
            r31 = 0
            float r4 = r5.down
            float r34 = r34 + r4
            int r19 = r19 + 1
        L_0x00fc:
            r26 = r40
            r42 = r24
            r29 = r40
            goto L_0x003b
        L_0x0104:
            int r40 = r29 + 1
            r0 = r39
            r1 = r29
            char r4 = r0.charAt(r1)
            switch(r4) {
                case 10: goto L_0x0112;
                case 91: goto L_0x0117;
                default: goto L_0x0111;
            }
        L_0x0111:
            goto L_0x00af
        L_0x0112:
            int r25 = r40 + -1
            r22 = 1
            goto L_0x00af
        L_0x0117:
            if (r20 == 0) goto L_0x00af
            r0 = r37
            r1 = r39
            r2 = r40
            r3 = r41
            int r14 = r0.parseColorMarkup(r1, r2, r3, r12)
            if (r14 < 0) goto L_0x00af
            int r25 = r40 + -1
            int r4 = r14 + 1
            int r40 = r40 + r4
            java.lang.Object r24 = r13.peek()
            com.badlogic.gdx.graphics.Color r24 = (com.badlogic.gdx.graphics.Color) r24
            goto L_0x00af
        L_0x0135:
            r32 = r33[r9]
            float r31 = r31 + r32
            if (r45 == 0) goto L_0x01e3
            int r4 = (r31 > r43 ? 1 : (r31 == r43 ? 0 : -1))
            if (r4 <= 0) goto L_0x01e3
            r4 = 1
            if (r9 <= r4) goto L_0x01e3
            float r7 = r31 - r32
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.BitmapFont$Glyph> r4 = r6.glyphs
            int r35 = r9 + -1
            r0 = r35
            java.lang.Object r4 = r4.get(r0)
            com.badlogic.gdx.graphics.g2d.BitmapFont$Glyph r4 = (com.badlogic.gdx.graphics.g2d.BitmapFont.Glyph) r4
            int r0 = r4.xoffset
            r35 = r0
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.BitmapFont$Glyph> r4 = r6.glyphs
            int r36 = r9 + -1
            r0 = r36
            java.lang.Object r4 = r4.get(r0)
            com.badlogic.gdx.graphics.g2d.BitmapFont$Glyph r4 = (com.badlogic.gdx.graphics.g2d.BitmapFont.Glyph) r4
            int r4 = r4.width
            int r4 = r4 + r35
            float r4 = (float) r4
            float r0 = r5.scaleX
            r35 = r0
            float r4 = r4 * r35
            float r4 = r4 + r7
            r7 = 925353388(0x3727c5ac, float:1.0E-5)
            float r4 = r4 - r7
            int r4 = (r4 > r43 ? 1 : (r4 == r43 ? 0 : -1))
            if (r4 <= 0) goto L_0x01e3
            if (r46 == 0) goto L_0x0187
            r4 = r37
            r7 = r43
            r8 = r46
            r4.truncate(r5, r6, r7, r8, r9, r10)
            float r4 = r6.x
            float r7 = r6.width
            float r31 = r4 + r7
            goto L_0x004d
        L_0x0187:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.BitmapFont$Glyph> r4 = r6.glyphs
            int r7 = r9 + -1
            int r8 = r5.getWrapIndex(r4, r7)
            float r4 = r6.x
            r7 = 0
            int r4 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r4 != 0) goto L_0x0198
            if (r8 == 0) goto L_0x019e
        L_0x0198:
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.BitmapFont$Glyph> r4 = r6.glyphs
            int r4 = r4.size
            if (r8 < r4) goto L_0x01a0
        L_0x019e:
            int r8 = r9 + -1
        L_0x01a0:
            r4 = r37
            r7 = r10
            com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun r23 = r4.wrap(r5, r6, r7, r8, r9)
            r0 = r27
            r1 = r23
            r0.add(r1)
            float r4 = r6.x
            float r7 = r6.width
            float r4 = r4 + r7
            r0 = r30
            float r30 = java.lang.Math.max(r0, r4)
            r31 = 0
            float r4 = r5.down
            float r34 = r34 + r4
            int r19 = r19 + 1
            r4 = 0
            r0 = r23
            r0.x = r4
            r0 = r34
            r1 = r23
            r1.y = r0
            r9 = -1
            r0 = r23
            com.badlogic.gdx.utils.FloatArray r4 = r0.xAdvances
            int r0 = r4.size
            r21 = r0
            r0 = r23
            com.badlogic.gdx.utils.FloatArray r4 = r0.xAdvances
            float[] r0 = r4.items
            r33 = r0
            r6 = r23
        L_0x01df:
            int r9 = r9 + 1
            goto L_0x00ea
        L_0x01e3:
            float r4 = r6.width
            float r4 = r4 + r32
            r6.width = r4
            goto L_0x01df
        L_0x01ea:
            java.lang.Object r4 = r13.get(r9)
            com.badlogic.gdx.graphics.Color r4 = (com.badlogic.gdx.graphics.Color) r4
            r12.free(r4)
            int r9 = r9 + 1
            goto L_0x0056
        L_0x01f7:
            r11 = 0
            goto L_0x0066
        L_0x01fa:
            r0 = r27
            java.lang.Object r6 = r0.get(r9)
            com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun r6 = (com.badlogic.gdx.graphics.g2d.GlyphLayout.GlyphRun) r6
            float r4 = r6.y
            int r4 = (r4 > r18 ? 1 : (r4 == r18 ? 0 : -1))
            if (r4 == 0) goto L_0x021e
            float r0 = r6.y
            r18 = r0
            float r28 = r43 - r17
            if (r11 == 0) goto L_0x0238
            r4 = 1073741824(0x40000000, float:2.0)
            float r28 = r28 / r4
            r16 = r15
        L_0x0216:
            r0 = r16
            if (r0 < r9) goto L_0x0226
            r17 = 0
            r15 = r16
        L_0x021e:
            float r4 = r6.width
            float r17 = r17 + r4
            int r9 = r9 + 1
            goto L_0x0072
        L_0x0226:
            int r15 = r16 + 1
            r0 = r27
            r1 = r16
            java.lang.Object r4 = r0.get(r1)
            com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun r4 = (com.badlogic.gdx.graphics.g2d.GlyphLayout.GlyphRun) r4
            float r7 = r4.x
            float r7 = r7 + r28
            r4.x = r7
        L_0x0238:
            r16 = r15
            goto L_0x0216
        L_0x023b:
            int r15 = r16 + 1
            r0 = r27
            r1 = r16
            java.lang.Object r4 = r0.get(r1)
            com.badlogic.gdx.graphics.g2d.GlyphLayout$GlyphRun r4 = (com.badlogic.gdx.graphics.g2d.GlyphLayout.GlyphRun) r4
            float r7 = r4.x
            float r7 = r7 + r28
            r4.x = r7
        L_0x024d:
            r16 = r15
            goto L_0x0080
        L_0x0251:
            r29 = r40
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.GlyphLayout.setText(com.badlogic.gdx.graphics.g2d.BitmapFont, java.lang.CharSequence, int, int, com.badlogic.gdx.graphics.Color, float, int, boolean, java.lang.String):void");
    }

    private void truncate(BitmapFont.BitmapFontData fontData, GlyphRun run, float targetWidth, String truncate, int widthIndex, Pool<GlyphRun> glyphRunPool) {
        GlyphRun truncateRun = glyphRunPool.obtain();
        fontData.getGlyphs(truncateRun, truncate, 0, truncate.length());
        float truncateWidth = Animation.CurveTimeline.LINEAR;
        int n = truncateRun.xAdvances.size;
        for (int i = 1; i < n; i++) {
            truncateWidth += truncateRun.xAdvances.get(i);
        }
        float targetWidth2 = targetWidth - truncateWidth;
        int count = 0;
        float width2 = run.x;
        while (true) {
            if (count >= run.xAdvances.size) {
                break;
            }
            float xAdvance = run.xAdvances.get(count);
            width2 += xAdvance;
            if (width2 > targetWidth2) {
                run.width = (width2 - run.x) - xAdvance;
                break;
            }
            count++;
        }
        if (count > 1) {
            run.glyphs.truncate(count - 1);
            run.xAdvances.truncate(count);
            adjustLastGlyph(fontData, run);
            run.xAdvances.addAll(truncateRun.xAdvances, 1, truncateRun.xAdvances.size - 1);
        } else {
            run.glyphs.clear();
            run.xAdvances.clear();
            run.xAdvances.addAll(truncateRun.xAdvances);
            run.width += truncateRun.xAdvances.get(0);
        }
        run.glyphs.addAll(truncateRun.glyphs);
        run.width += truncateWidth;
        glyphRunPool.free(truncateRun);
    }

    private GlyphRun wrap(BitmapFont.BitmapFontData fontData, GlyphRun first, Pool<GlyphRun> glyphRunPool, int wrapIndex, int widthIndex) {
        GlyphRun second = glyphRunPool.obtain();
        second.color.set(first.color);
        second.glyphs.addAll(first.glyphs, wrapIndex, first.glyphs.size - wrapIndex);
        second.xAdvances.add((((float) (-second.glyphs.first().xoffset)) * fontData.scaleX) - ((float) fontData.padLeft));
        second.xAdvances.addAll(first.xAdvances, wrapIndex + 1, first.xAdvances.size - (wrapIndex + 1));
        int widthIndex2 = widthIndex;
        while (widthIndex2 < wrapIndex) {
            first.width += first.xAdvances.get(widthIndex2);
            widthIndex2++;
        }
        int widthIndex3 = widthIndex2;
        while (widthIndex3 > wrapIndex + 1) {
            widthIndex3--;
            first.width -= first.xAdvances.get(widthIndex3);
        }
        while (wrapIndex > 0 && fontData.isWhitespace((char) first.glyphs.get(wrapIndex - 1).id)) {
            if (wrapIndex < widthIndex3) {
                first.width -= first.xAdvances.get(wrapIndex);
            }
            wrapIndex--;
        }
        if (wrapIndex == 0) {
            glyphRunPool.free(first);
            this.runs.pop();
        } else {
            first.glyphs.truncate(wrapIndex);
            first.xAdvances.truncate(wrapIndex + 1);
            adjustLastGlyph(fontData, first);
        }
        return second;
    }

    private void adjustLastGlyph(BitmapFont.BitmapFontData fontData, GlyphRun run) {
        BitmapFont.Glyph last = run.glyphs.peek();
        if (!fontData.isWhitespace((char) last.id)) {
            float width2 = (((float) (last.xoffset + last.width)) * fontData.scaleX) - ((float) fontData.padRight);
            run.width += width2 - run.xAdvances.peek();
            run.xAdvances.set(run.xAdvances.size - 1, width2);
        }
    }

    private int parseColorMarkup(CharSequence str, int start, int end, Pool<Color> colorPool) {
        int i;
        int i2;
        if (start == end) {
            return -1;
        }
        switch (str.charAt(start)) {
            case '#':
                int colorInt = 0;
                int i3 = start + 1;
                while (i3 < end) {
                    char ch = str.charAt(i3);
                    if (ch != ']') {
                        if (ch >= '0' && ch <= '9') {
                            i = colorInt * 16;
                            i2 = ch - '0';
                        } else if (ch >= 'a' && ch <= 'f') {
                            i = colorInt * 16;
                            i2 = ch - 'W';
                        } else if (ch < 'A' || ch > 'F') {
                            return -1;
                        } else {
                            i = colorInt * 16;
                            i2 = ch - '7';
                        }
                        colorInt = i + i2;
                        i3++;
                    } else if (i3 < start + 2 || i3 > start + 9) {
                        return -1;
                    } else {
                        Color color = colorPool.obtain();
                        colorStack.add(color);
                        Color.rgba8888ToColor(color, colorInt);
                        if (i3 <= start + 7) {
                            color.a = 1.0f;
                        }
                        return i3 - start;
                    }
                }
                return -1;
            case '[':
                return -1;
            case ']':
                if (colorStack.size > 1) {
                    colorPool.free(colorStack.pop());
                }
                return 0;
            default:
                int colorStart = start;
                int i4 = start + 1;
                while (i4 < end) {
                    if (str.charAt(i4) != ']') {
                        i4++;
                    } else {
                        Color namedColor = Colors.get(str.subSequence(colorStart, i4).toString());
                        if (namedColor == null) {
                            return -1;
                        }
                        Color color2 = colorPool.obtain();
                        colorStack.add(color2);
                        color2.set(namedColor);
                        return i4 - start;
                    }
                }
                return -1;
        }
    }

    public void reset() {
        Pools.get(GlyphRun.class).freeAll(this.runs);
        this.runs.clear();
        this.width = Animation.CurveTimeline.LINEAR;
        this.height = Animation.CurveTimeline.LINEAR;
    }

    public String toString() {
        if (this.runs.size == 0) {
            return "";
        }
        StringBuilder buffer = new StringBuilder(128);
        int n = this.runs.size;
        for (int i = 0; i < n; i++) {
            buffer.append(this.runs.get(i).toString());
            buffer.append(10);
        }
        buffer.setLength(buffer.length() - 1);
        return buffer.toString();
    }

    public static class GlyphRun implements Pool.Poolable {
        public final Color color = new Color();
        public final Array<BitmapFont.Glyph> glyphs = new Array<>();
        public float width;
        public float x;
        public final FloatArray xAdvances = new FloatArray();
        public float y;

        public void reset() {
            this.glyphs.clear();
            this.xAdvances.clear();
            this.width = Animation.CurveTimeline.LINEAR;
        }

        public String toString() {
            StringBuilder buffer = new StringBuilder(this.glyphs.size);
            Array<BitmapFont.Glyph> glyphs2 = this.glyphs;
            int n = glyphs2.size;
            for (int i = 0; i < n; i++) {
                buffer.append((char) glyphs2.get(i).id);
            }
            buffer.append(", #");
            buffer.append(this.color);
            buffer.append(", ");
            buffer.append(this.x);
            buffer.append(", ");
            buffer.append(this.y);
            buffer.append(", ");
            buffer.append(this.width);
            return buffer.toString();
        }
    }
}
