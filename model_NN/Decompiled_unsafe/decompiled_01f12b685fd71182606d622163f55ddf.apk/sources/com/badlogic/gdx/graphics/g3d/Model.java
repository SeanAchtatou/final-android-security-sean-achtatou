package com.badlogic.gdx.graphics.g3d;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodeAnimation;
import com.badlogic.gdx.graphics.g3d.model.NodeKeyframe;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.model.data.ModelAnimation;
import com.badlogic.gdx.graphics.g3d.model.data.ModelData;
import com.badlogic.gdx.graphics.g3d.model.data.ModelMaterial;
import com.badlogic.gdx.graphics.g3d.model.data.ModelMesh;
import com.badlogic.gdx.graphics.g3d.model.data.ModelMeshPart;
import com.badlogic.gdx.graphics.g3d.model.data.ModelNode;
import com.badlogic.gdx.graphics.g3d.model.data.ModelNodeAnimation;
import com.badlogic.gdx.graphics.g3d.model.data.ModelNodeKeyframe;
import com.badlogic.gdx.graphics.g3d.model.data.ModelNodePart;
import com.badlogic.gdx.graphics.g3d.model.data.ModelTexture;
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor;
import com.badlogic.gdx.graphics.g3d.utils.TextureProvider;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class Model implements Disposable {
    public final Array<Animation> animations;
    protected final Array<Disposable> disposables;
    public final Array<Material> materials;
    public final Array<MeshPart> meshParts;
    public final Array<Mesh> meshes;
    private ObjectMap<NodePart, ArrayMap<String, Matrix4>> nodePartBones;
    public final Array<Node> nodes;

    public Model() {
        this.materials = new Array<>();
        this.nodes = new Array<>();
        this.animations = new Array<>();
        this.meshes = new Array<>();
        this.meshParts = new Array<>();
        this.disposables = new Array<>();
        this.nodePartBones = new ObjectMap<>();
    }

    public Model(ModelData modelData) {
        this(modelData, new TextureProvider.FileTextureProvider());
    }

    public Model(ModelData modelData, TextureProvider textureProvider) {
        this.materials = new Array<>();
        this.nodes = new Array<>();
        this.animations = new Array<>();
        this.meshes = new Array<>();
        this.meshParts = new Array<>();
        this.disposables = new Array<>();
        this.nodePartBones = new ObjectMap<>();
        load(modelData, textureProvider);
    }

    private void load(ModelData modelData, TextureProvider textureProvider) {
        loadMeshes(modelData.meshes);
        loadMaterials(modelData.materials, textureProvider);
        loadNodes(modelData.nodes);
        loadAnimations(modelData.animations);
        calculateTransforms();
    }

    private void loadAnimations(Iterable<ModelAnimation> modelAnimations) {
        Quaternion quaternion;
        Vector3 vector3;
        for (ModelAnimation anim : modelAnimations) {
            Animation animation = new Animation();
            animation.id = anim.id;
            Iterator<ModelNodeAnimation> it = anim.nodeAnimations.iterator();
            while (it.hasNext()) {
                ModelNodeAnimation nanim = it.next();
                Node node = getNode(nanim.nodeId);
                if (node != null) {
                    NodeAnimation nodeAnim = new NodeAnimation();
                    nodeAnim.node = node;
                    if (nanim.translation != null) {
                        nodeAnim.translation = new Array<>();
                        nodeAnim.translation.ensureCapacity(nanim.translation.size);
                        Iterator<ModelNodeKeyframe<Vector3>> it2 = nanim.translation.iterator();
                        while (it2.hasNext()) {
                            ModelNodeKeyframe<Vector3> kf = it2.next();
                            if (kf.keytime > animation.duration) {
                                animation.duration = kf.keytime;
                            }
                            Array<NodeKeyframe<Vector3>> array = nodeAnim.translation;
                            float f = kf.keytime;
                            if (kf.value == null) {
                                vector3 = node.translation;
                            } else {
                                vector3 = (Vector3) kf.value;
                            }
                            array.add(new NodeKeyframe(f, new Vector3(vector3)));
                        }
                    }
                    if (nanim.rotation != null) {
                        nodeAnim.rotation = new Array<>();
                        nodeAnim.rotation.ensureCapacity(nanim.rotation.size);
                        Iterator<ModelNodeKeyframe<Quaternion>> it3 = nanim.rotation.iterator();
                        while (it3.hasNext()) {
                            ModelNodeKeyframe<Quaternion> kf2 = it3.next();
                            if (kf2.keytime > animation.duration) {
                                animation.duration = kf2.keytime;
                            }
                            Array<NodeKeyframe<Quaternion>> array2 = nodeAnim.rotation;
                            float f2 = kf2.keytime;
                            if (kf2.value == null) {
                                quaternion = node.rotation;
                            } else {
                                quaternion = (Quaternion) kf2.value;
                            }
                            array2.add(new NodeKeyframe(f2, new Quaternion(quaternion)));
                        }
                    }
                    if (nanim.scaling != null) {
                        nodeAnim.scaling = new Array<>();
                        nodeAnim.scaling.ensureCapacity(nanim.scaling.size);
                        Iterator<ModelNodeKeyframe<Vector3>> it4 = nanim.scaling.iterator();
                        while (it4.hasNext()) {
                            ModelNodeKeyframe<Vector3> kf3 = it4.next();
                            if (kf3.keytime > animation.duration) {
                                animation.duration = kf3.keytime;
                            }
                            nodeAnim.scaling.add(new NodeKeyframe(kf3.keytime, new Vector3(kf3.value == null ? node.scale : (Vector3) kf3.value)));
                        }
                    }
                    if ((nodeAnim.translation != null && nodeAnim.translation.size > 0) || ((nodeAnim.rotation != null && nodeAnim.rotation.size > 0) || (nodeAnim.scaling != null && nodeAnim.scaling.size > 0))) {
                        animation.nodeAnimations.add(nodeAnim);
                    }
                }
            }
            if (animation.nodeAnimations.size > 0) {
                this.animations.add(animation);
            }
        }
    }

    private void loadNodes(Iterable<ModelNode> modelNodes) {
        this.nodePartBones.clear();
        for (ModelNode node : modelNodes) {
            this.nodes.add(loadNode(node));
        }
        Iterator it = this.nodePartBones.entries().iterator();
        while (it.hasNext()) {
            ObjectMap.Entry<NodePart, ArrayMap<String, Matrix4>> e = (ObjectMap.Entry) it.next();
            if (((NodePart) e.key).invBoneBindTransforms == null) {
                ((NodePart) e.key).invBoneBindTransforms = new ArrayMap<>(Node.class, Matrix4.class);
            }
            ((NodePart) e.key).invBoneBindTransforms.clear();
            Iterator it2 = ((ArrayMap) e.value).entries().iterator();
            while (it2.hasNext()) {
                ObjectMap.Entry<String, Matrix4> b = (ObjectMap.Entry) it2.next();
                ((NodePart) e.key).invBoneBindTransforms.put(getNode((String) b.key), new Matrix4((Matrix4) b.value).inv());
            }
        }
    }

    private Node loadNode(ModelNode modelNode) {
        Node node = new Node();
        node.id = modelNode.id;
        if (modelNode.translation != null) {
            node.translation.set(modelNode.translation);
        }
        if (modelNode.rotation != null) {
            node.rotation.set(modelNode.rotation);
        }
        if (modelNode.scale != null) {
            node.scale.set(modelNode.scale);
        }
        if (modelNode.parts != null) {
            for (ModelNodePart modelNodePart : modelNode.parts) {
                MeshPart meshPart = null;
                Material meshMaterial = null;
                if (modelNodePart.meshPartId != null) {
                    Iterator<MeshPart> it = this.meshParts.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            MeshPart part = it.next();
                            if (modelNodePart.meshPartId.equals(part.id)) {
                                meshPart = part;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (modelNodePart.materialId != null) {
                    Iterator<Material> it2 = this.materials.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            Material material = it2.next();
                            if (modelNodePart.materialId.equals(material.id)) {
                                meshMaterial = material;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (meshPart == null || meshMaterial == null) {
                    throw new GdxRuntimeException("Invalid node: " + node.id);
                }
                if (!(meshPart == null || meshMaterial == null)) {
                    NodePart nodePart = new NodePart();
                    nodePart.meshPart = meshPart;
                    nodePart.material = meshMaterial;
                    node.parts.add(nodePart);
                    if (modelNodePart.bones != null) {
                        this.nodePartBones.put(nodePart, modelNodePart.bones);
                    }
                }
            }
        }
        if (modelNode.children != null) {
            for (ModelNode child : modelNode.children) {
                node.addChild(loadNode(child));
            }
        }
        return node;
    }

    private void loadMeshes(Iterable<ModelMesh> meshes2) {
        for (ModelMesh mesh : meshes2) {
            convertMesh(mesh);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
     arg types: [int, int, int, com.badlogic.gdx.graphics.VertexAttributes]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void */
    private void convertMesh(ModelMesh modelMesh) {
        int numIndices = 0;
        for (ModelMeshPart part : modelMesh.parts) {
            numIndices += part.indices.length;
        }
        VertexAttributes attributes = new VertexAttributes(modelMesh.attributes);
        Mesh mesh = new Mesh(true, modelMesh.vertices.length / (attributes.vertexSize / 4), numIndices, attributes);
        this.meshes.add(mesh);
        this.disposables.add(mesh);
        BufferUtils.copy(modelMesh.vertices, mesh.getVerticesBuffer(), modelMesh.vertices.length, 0);
        int offset = 0;
        mesh.getIndicesBuffer().clear();
        for (ModelMeshPart part2 : modelMesh.parts) {
            MeshPart meshPart = new MeshPart();
            meshPart.id = part2.id;
            meshPart.primitiveType = part2.primitiveType;
            meshPart.indexOffset = offset;
            meshPart.numVertices = part2.indices.length;
            meshPart.mesh = mesh;
            mesh.getIndicesBuffer().put(part2.indices);
            offset += meshPart.numVertices;
            this.meshParts.add(meshPart);
        }
        mesh.getIndicesBuffer().position(0);
    }

    private void loadMaterials(Iterable<ModelMaterial> modelMaterials, TextureProvider textureProvider) {
        for (ModelMaterial mtl : modelMaterials) {
            this.materials.add(convertMaterial(mtl, textureProvider));
        }
    }

    private Material convertMaterial(ModelMaterial mtl, TextureProvider textureProvider) {
        Texture texture;
        Material result = new Material();
        result.id = mtl.id;
        if (mtl.ambient != null) {
            result.set(new ColorAttribute(ColorAttribute.Ambient, mtl.ambient));
        }
        if (mtl.diffuse != null) {
            result.set(new ColorAttribute(ColorAttribute.Diffuse, mtl.diffuse));
        }
        if (mtl.specular != null) {
            result.set(new ColorAttribute(ColorAttribute.Specular, mtl.specular));
        }
        if (mtl.emissive != null) {
            result.set(new ColorAttribute(ColorAttribute.Emissive, mtl.emissive));
        }
        if (mtl.reflection != null) {
            result.set(new ColorAttribute(ColorAttribute.Reflection, mtl.reflection));
        }
        if (mtl.shininess > Animation.CurveTimeline.LINEAR) {
            result.set(new FloatAttribute(FloatAttribute.Shininess, mtl.shininess));
        }
        if (mtl.opacity != 1.0f) {
            result.set(new BlendingAttribute(770, 771, mtl.opacity));
        }
        ObjectMap<String, Texture> textures = new ObjectMap<>();
        if (mtl.textures != null) {
            Iterator<ModelTexture> it = mtl.textures.iterator();
            while (it.hasNext()) {
                ModelTexture tex = it.next();
                if (textures.containsKey(tex.fileName)) {
                    texture = (Texture) textures.get(tex.fileName);
                } else {
                    texture = textureProvider.load(tex.fileName);
                    textures.put(tex.fileName, texture);
                    this.disposables.add(texture);
                }
                TextureDescriptor descriptor = new TextureDescriptor(texture);
                descriptor.minFilter = texture.getMinFilter();
                descriptor.magFilter = texture.getMagFilter();
                descriptor.uWrap = texture.getUWrap();
                descriptor.vWrap = texture.getVWrap();
                float offsetU = tex.uvTranslation == null ? Animation.CurveTimeline.LINEAR : tex.uvTranslation.x;
                float offsetV = tex.uvTranslation == null ? Animation.CurveTimeline.LINEAR : tex.uvTranslation.y;
                float scaleU = tex.uvScaling == null ? 1.0f : tex.uvScaling.x;
                float scaleV = tex.uvScaling == null ? 1.0f : tex.uvScaling.y;
                switch (tex.usage) {
                    case 2:
                        result.set(new TextureAttribute(TextureAttribute.Diffuse, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                    case 3:
                        result.set(new TextureAttribute(TextureAttribute.Emissive, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                    case 4:
                        result.set(new TextureAttribute(TextureAttribute.Ambient, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                    case 5:
                        result.set(new TextureAttribute(TextureAttribute.Specular, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                    case 7:
                        result.set(new TextureAttribute(TextureAttribute.Normal, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                    case 8:
                        result.set(new TextureAttribute(TextureAttribute.Bump, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                    case 10:
                        result.set(new TextureAttribute(TextureAttribute.Reflection, descriptor, offsetU, offsetV, scaleU, scaleV));
                        break;
                }
            }
        }
        return result;
    }

    public void manageDisposable(Disposable disposable) {
        if (!this.disposables.contains(disposable, true)) {
            this.disposables.add(disposable);
        }
    }

    public Iterable<Disposable> getManagedDisposables() {
        return this.disposables;
    }

    public void dispose() {
        Iterator<Disposable> it = this.disposables.iterator();
        while (it.hasNext()) {
            it.next().dispose();
        }
    }

    public void calculateTransforms() {
        int n = this.nodes.size;
        for (int i = 0; i < n; i++) {
            this.nodes.get(i).calculateTransforms(true);
        }
        for (int i2 = 0; i2 < n; i2++) {
            this.nodes.get(i2).calculateBoneTransforms(true);
        }
    }

    public BoundingBox calculateBoundingBox(BoundingBox out) {
        out.inf();
        return extendBoundingBox(out);
    }

    public BoundingBox extendBoundingBox(BoundingBox out) {
        int n = this.nodes.size;
        for (int i = 0; i < n; i++) {
            this.nodes.get(i).extendBoundingBox(out);
        }
        return out;
    }

    public com.badlogic.gdx.graphics.g3d.model.Animation getAnimation(String id) {
        return getAnimation(id, true);
    }

    public com.badlogic.gdx.graphics.g3d.model.Animation getAnimation(String id, boolean ignoreCase) {
        int n = this.animations.size;
        if (ignoreCase) {
            for (int i = 0; i < n; i++) {
                com.badlogic.gdx.graphics.g3d.model.Animation animation = this.animations.get(i);
                if (animation.id.equalsIgnoreCase(id)) {
                    return animation;
                }
            }
        } else {
            for (int i2 = 0; i2 < n; i2++) {
                com.badlogic.gdx.graphics.g3d.model.Animation animation2 = this.animations.get(i2);
                if (animation2.id.equals(id)) {
                    return animation2;
                }
            }
        }
        return null;
    }

    public Material getMaterial(String id) {
        return getMaterial(id, true);
    }

    public Material getMaterial(String id, boolean ignoreCase) {
        int n = this.materials.size;
        if (ignoreCase) {
            for (int i = 0; i < n; i++) {
                Material material = this.materials.get(i);
                if (material.id.equalsIgnoreCase(id)) {
                    return material;
                }
            }
        } else {
            for (int i2 = 0; i2 < n; i2++) {
                Material material2 = this.materials.get(i2);
                if (material2.id.equals(id)) {
                    return material2;
                }
            }
        }
        return null;
    }

    public Node getNode(String id) {
        return getNode(id, true);
    }

    public Node getNode(String id, boolean recursive) {
        return getNode(id, recursive, false);
    }

    public Node getNode(String id, boolean recursive, boolean ignoreCase) {
        return Node.getNode(this.nodes, id, recursive, ignoreCase);
    }
}
