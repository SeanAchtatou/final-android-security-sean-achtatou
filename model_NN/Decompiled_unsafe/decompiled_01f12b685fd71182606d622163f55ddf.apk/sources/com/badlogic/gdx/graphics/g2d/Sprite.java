package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class Sprite extends TextureRegion {
    static final int SPRITE_SIZE = 20;
    static final int VERTEX_SIZE = 5;
    private Rectangle bounds;
    private final Color color;
    private boolean dirty;
    float height;
    private float originX;
    private float originY;
    private float rotation;
    private float scaleX;
    private float scaleY;
    final float[] vertices;
    float width;
    private float x;
    private float y;

    public Sprite() {
        this.vertices = new float[20];
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.dirty = true;
        setColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public Sprite(Texture texture) {
        this(texture, 0, 0, texture.getWidth(), texture.getHeight());
    }

    public Sprite(Texture texture, int srcWidth, int srcHeight) {
        this(texture, 0, 0, srcWidth, srcHeight);
    }

    public Sprite(Texture texture, int srcX, int srcY, int srcWidth, int srcHeight) {
        this.vertices = new float[20];
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.dirty = true;
        if (texture == null) {
            throw new IllegalArgumentException("texture cannot be null.");
        }
        this.texture = texture;
        setRegion(srcX, srcY, srcWidth, srcHeight);
        setColor(1.0f, 1.0f, 1.0f, 1.0f);
        setSize((float) Math.abs(srcWidth), (float) Math.abs(srcHeight));
        setOrigin(this.width / 2.0f, this.height / 2.0f);
    }

    public Sprite(TextureRegion region) {
        this.vertices = new float[20];
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.dirty = true;
        setRegion(region);
        setColor(1.0f, 1.0f, 1.0f, 1.0f);
        setSize((float) region.getRegionWidth(), (float) region.getRegionHeight());
        setOrigin(this.width / 2.0f, this.height / 2.0f);
    }

    public Sprite(TextureRegion region, int srcX, int srcY, int srcWidth, int srcHeight) {
        this.vertices = new float[20];
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.dirty = true;
        setRegion(region, srcX, srcY, srcWidth, srcHeight);
        setColor(1.0f, 1.0f, 1.0f, 1.0f);
        setSize((float) Math.abs(srcWidth), (float) Math.abs(srcHeight));
        setOrigin(this.width / 2.0f, this.height / 2.0f);
    }

    public Sprite(Sprite sprite) {
        this.vertices = new float[20];
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.dirty = true;
        set(sprite);
    }

    public void set(Sprite sprite) {
        if (sprite == null) {
            throw new IllegalArgumentException("sprite cannot be null.");
        }
        System.arraycopy(sprite.vertices, 0, this.vertices, 0, 20);
        this.texture = sprite.texture;
        this.u = sprite.u;
        this.v = sprite.v;
        this.u2 = sprite.u2;
        this.v2 = sprite.v2;
        this.x = sprite.x;
        this.y = sprite.y;
        this.width = sprite.width;
        this.height = sprite.height;
        this.regionWidth = sprite.regionWidth;
        this.regionHeight = sprite.regionHeight;
        this.originX = sprite.originX;
        this.originY = sprite.originY;
        this.rotation = sprite.rotation;
        this.scaleX = sprite.scaleX;
        this.scaleY = sprite.scaleY;
        this.color.set(sprite.color);
        this.dirty = sprite.dirty;
    }

    public void setBounds(float x2, float y2, float width2, float height2) {
        this.x = x2;
        this.y = y2;
        this.width = width2;
        this.height = height2;
        if (!this.dirty) {
            float x22 = x2 + width2;
            float y22 = y2 + height2;
            float[] vertices2 = this.vertices;
            vertices2[0] = x2;
            vertices2[1] = y2;
            vertices2[5] = x2;
            vertices2[6] = y22;
            vertices2[10] = x22;
            vertices2[11] = y22;
            vertices2[15] = x22;
            vertices2[16] = y2;
            if (this.rotation != Animation.CurveTimeline.LINEAR || this.scaleX != 1.0f || this.scaleY != 1.0f) {
                this.dirty = true;
            }
        }
    }

    public void setSize(float width2, float height2) {
        this.width = width2;
        this.height = height2;
        if (!this.dirty) {
            float x2 = this.x + width2;
            float y2 = this.y + height2;
            float[] vertices2 = this.vertices;
            vertices2[0] = this.x;
            vertices2[1] = this.y;
            vertices2[5] = this.x;
            vertices2[6] = y2;
            vertices2[10] = x2;
            vertices2[11] = y2;
            vertices2[15] = x2;
            vertices2[16] = this.y;
            if (this.rotation != Animation.CurveTimeline.LINEAR || this.scaleX != 1.0f || this.scaleY != 1.0f) {
                this.dirty = true;
            }
        }
    }

    public void setPosition(float x2, float y2) {
        translate(x2 - this.x, y2 - this.y);
    }

    public void setX(float x2) {
        translateX(x2 - this.x);
    }

    public void setY(float y2) {
        translateY(y2 - this.y);
    }

    public void setCenterX(float x2) {
        setX(x2 - (this.width / 2.0f));
    }

    public void setCenterY(float y2) {
        setY(y2 - (this.height / 2.0f));
    }

    public void setCenter(float x2, float y2) {
        setCenterX(x2);
        setCenterY(y2);
    }

    public void translateX(float xAmount) {
        this.x += xAmount;
        if (!this.dirty) {
            float[] vertices2 = this.vertices;
            vertices2[0] = vertices2[0] + xAmount;
            vertices2[5] = vertices2[5] + xAmount;
            vertices2[10] = vertices2[10] + xAmount;
            vertices2[15] = vertices2[15] + xAmount;
        }
    }

    public void translateY(float yAmount) {
        this.y += yAmount;
        if (!this.dirty) {
            float[] vertices2 = this.vertices;
            vertices2[1] = vertices2[1] + yAmount;
            vertices2[6] = vertices2[6] + yAmount;
            vertices2[11] = vertices2[11] + yAmount;
            vertices2[16] = vertices2[16] + yAmount;
        }
    }

    public void translate(float xAmount, float yAmount) {
        this.x += xAmount;
        this.y += yAmount;
        if (!this.dirty) {
            float[] vertices2 = this.vertices;
            vertices2[0] = vertices2[0] + xAmount;
            vertices2[1] = vertices2[1] + yAmount;
            vertices2[5] = vertices2[5] + xAmount;
            vertices2[6] = vertices2[6] + yAmount;
            vertices2[10] = vertices2[10] + xAmount;
            vertices2[11] = vertices2[11] + yAmount;
            vertices2[15] = vertices2[15] + xAmount;
            vertices2[16] = vertices2[16] + yAmount;
        }
    }

    public void setColor(Color tint) {
        float color2 = tint.toFloatBits();
        float[] vertices2 = this.vertices;
        vertices2[2] = color2;
        vertices2[7] = color2;
        vertices2[12] = color2;
        vertices2[17] = color2;
    }

    public void setAlpha(float a) {
        float color2 = NumberUtils.intToFloatColor((NumberUtils.floatToIntColor(this.vertices[2]) & 16777215) | (((int) (255.0f * a)) << 24));
        this.vertices[2] = color2;
        this.vertices[7] = color2;
        this.vertices[12] = color2;
        this.vertices[17] = color2;
    }

    public void setColor(float r, float g, float b, float a) {
        float color2 = NumberUtils.intToFloatColor((((int) (255.0f * a)) << 24) | (((int) (255.0f * b)) << 16) | (((int) (255.0f * g)) << 8) | ((int) (255.0f * r)));
        float[] vertices2 = this.vertices;
        vertices2[2] = color2;
        vertices2[7] = color2;
        vertices2[12] = color2;
        vertices2[17] = color2;
    }

    public void setColor(float color2) {
        float[] vertices2 = this.vertices;
        vertices2[2] = color2;
        vertices2[7] = color2;
        vertices2[12] = color2;
        vertices2[17] = color2;
    }

    public void setOrigin(float originX2, float originY2) {
        this.originX = originX2;
        this.originY = originY2;
        this.dirty = true;
    }

    public void setOriginCenter() {
        this.originX = this.width / 2.0f;
        this.originY = this.height / 2.0f;
        this.dirty = true;
    }

    public void setRotation(float degrees) {
        this.rotation = degrees;
        this.dirty = true;
    }

    public float getRotation() {
        return this.rotation;
    }

    public void rotate(float degrees) {
        if (degrees != Animation.CurveTimeline.LINEAR) {
            this.rotation += degrees;
            this.dirty = true;
        }
    }

    public void rotate90(boolean clockwise) {
        float[] vertices2 = this.vertices;
        if (clockwise) {
            float temp = vertices2[4];
            vertices2[4] = vertices2[19];
            vertices2[19] = vertices2[14];
            vertices2[14] = vertices2[9];
            vertices2[9] = temp;
            float temp2 = vertices2[3];
            vertices2[3] = vertices2[18];
            vertices2[18] = vertices2[13];
            vertices2[13] = vertices2[8];
            vertices2[8] = temp2;
            return;
        }
        float temp3 = vertices2[4];
        vertices2[4] = vertices2[9];
        vertices2[9] = vertices2[14];
        vertices2[14] = vertices2[19];
        vertices2[19] = temp3;
        float temp4 = vertices2[3];
        vertices2[3] = vertices2[8];
        vertices2[8] = vertices2[13];
        vertices2[13] = vertices2[18];
        vertices2[18] = temp4;
    }

    public void setScale(float scaleXY) {
        this.scaleX = scaleXY;
        this.scaleY = scaleXY;
        this.dirty = true;
    }

    public void setScale(float scaleX2, float scaleY2) {
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
        this.dirty = true;
    }

    public void scale(float amount) {
        this.scaleX += amount;
        this.scaleY += amount;
        this.dirty = true;
    }

    public float[] getVertices() {
        if (this.dirty) {
            this.dirty = false;
            float[] vertices2 = this.vertices;
            float localX = -this.originX;
            float localY = -this.originY;
            float localX2 = localX + this.width;
            float localY2 = localY + this.height;
            float worldOriginX = this.x - localX;
            float worldOriginY = this.y - localY;
            if (!(this.scaleX == 1.0f && this.scaleY == 1.0f)) {
                localX *= this.scaleX;
                localY *= this.scaleY;
                localX2 *= this.scaleX;
                localY2 *= this.scaleY;
            }
            if (this.rotation != Animation.CurveTimeline.LINEAR) {
                float cos = MathUtils.cosDeg(this.rotation);
                float sin = MathUtils.sinDeg(this.rotation);
                float localXCos = localX * cos;
                float localXSin = localX * sin;
                float localY2Cos = localY2 * cos;
                float localY2Sin = localY2 * sin;
                float x1 = (localXCos - (localY * sin)) + worldOriginX;
                float y1 = (localY * cos) + localXSin + worldOriginY;
                vertices2[0] = x1;
                vertices2[1] = y1;
                float x2 = (localXCos - localY2Sin) + worldOriginX;
                float y2 = localY2Cos + localXSin + worldOriginY;
                vertices2[5] = x2;
                vertices2[6] = y2;
                float x3 = ((localX2 * cos) - localY2Sin) + worldOriginX;
                float y3 = localY2Cos + (localX2 * sin) + worldOriginY;
                vertices2[10] = x3;
                vertices2[11] = y3;
                vertices2[15] = (x3 - x2) + x1;
                vertices2[16] = y3 - (y2 - y1);
            } else {
                float x12 = localX + worldOriginX;
                float y12 = localY + worldOriginY;
                float x22 = localX2 + worldOriginX;
                float y22 = localY2 + worldOriginY;
                vertices2[0] = x12;
                vertices2[1] = y12;
                vertices2[5] = x12;
                vertices2[6] = y22;
                vertices2[10] = x22;
                vertices2[11] = y22;
                vertices2[15] = x22;
                vertices2[16] = y12;
            }
        }
        return this.vertices;
    }

    public Rectangle getBoundingRectangle() {
        float[] vertices2 = getVertices();
        float minx = vertices2[0];
        float miny = vertices2[1];
        float maxx = vertices2[0];
        float maxy = vertices2[1];
        if (minx > vertices2[5]) {
            minx = vertices2[5];
        }
        if (minx > vertices2[10]) {
            minx = vertices2[10];
        }
        if (minx > vertices2[15]) {
            minx = vertices2[15];
        }
        if (maxx < vertices2[5]) {
            maxx = vertices2[5];
        }
        if (maxx < vertices2[10]) {
            maxx = vertices2[10];
        }
        if (maxx < vertices2[15]) {
            maxx = vertices2[15];
        }
        if (miny > vertices2[6]) {
            miny = vertices2[6];
        }
        if (miny > vertices2[11]) {
            miny = vertices2[11];
        }
        if (miny > vertices2[16]) {
            miny = vertices2[16];
        }
        if (maxy < vertices2[6]) {
            maxy = vertices2[6];
        }
        if (maxy < vertices2[11]) {
            maxy = vertices2[11];
        }
        if (maxy < vertices2[16]) {
            maxy = vertices2[16];
        }
        if (this.bounds == null) {
            this.bounds = new Rectangle();
        }
        this.bounds.x = minx;
        this.bounds.y = miny;
        this.bounds.width = maxx - minx;
        this.bounds.height = maxy - miny;
        return this.bounds;
    }

    public void draw(Batch batch) {
        batch.draw(this.texture, getVertices(), 0, 20);
    }

    public void draw(Batch batch, float alphaModulation) {
        float oldAlpha = getColor().a;
        setAlpha(oldAlpha * alphaModulation);
        draw(batch);
        setAlpha(oldAlpha);
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public float getOriginX() {
        return this.originX;
    }

    public float getOriginY() {
        return this.originY;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public Color getColor() {
        int intBits = NumberUtils.floatToIntColor(this.vertices[2]);
        Color color2 = this.color;
        color2.r = ((float) (intBits & 255)) / 255.0f;
        color2.g = ((float) ((intBits >>> 8) & 255)) / 255.0f;
        color2.b = ((float) ((intBits >>> 16) & 255)) / 255.0f;
        color2.a = ((float) ((intBits >>> 24) & 255)) / 255.0f;
        return color2;
    }

    public void setRegion(float u, float v, float u2, float v2) {
        super.setRegion(u, v, u2, v2);
        float[] vertices2 = this.vertices;
        vertices2[3] = u;
        vertices2[4] = v2;
        vertices2[8] = u;
        vertices2[9] = v;
        vertices2[13] = u2;
        vertices2[14] = v;
        vertices2[18] = u2;
        vertices2[19] = v2;
    }

    public void setU(float u) {
        super.setU(u);
        this.vertices[3] = u;
        this.vertices[8] = u;
    }

    public void setV(float v) {
        super.setV(v);
        this.vertices[9] = v;
        this.vertices[14] = v;
    }

    public void setU2(float u2) {
        super.setU2(u2);
        this.vertices[13] = u2;
        this.vertices[18] = u2;
    }

    public void setV2(float v2) {
        super.setV2(v2);
        this.vertices[4] = v2;
        this.vertices[19] = v2;
    }

    public void setFlip(boolean x2, boolean y2) {
        boolean performX = false;
        boolean performY = false;
        if (isFlipX() != x2) {
            performX = true;
        }
        if (isFlipY() != y2) {
            performY = true;
        }
        flip(performX, performY);
    }

    public void flip(boolean x2, boolean y2) {
        super.flip(x2, y2);
        float[] vertices2 = this.vertices;
        if (x2) {
            float temp = vertices2[3];
            vertices2[3] = vertices2[13];
            vertices2[13] = temp;
            float temp2 = vertices2[8];
            vertices2[8] = vertices2[18];
            vertices2[18] = temp2;
        }
        if (y2) {
            float temp3 = vertices2[4];
            vertices2[4] = vertices2[14];
            vertices2[14] = temp3;
            float temp4 = vertices2[9];
            vertices2[9] = vertices2[19];
            vertices2[19] = temp4;
        }
    }

    public void scroll(float xAmount, float yAmount) {
        float[] vertices2 = this.vertices;
        if (xAmount != Animation.CurveTimeline.LINEAR) {
            float u = (vertices2[3] + xAmount) % 1.0f;
            float u2 = u + (this.width / ((float) this.texture.getWidth()));
            this.u = u;
            this.u2 = u2;
            vertices2[3] = u;
            vertices2[8] = u;
            vertices2[13] = u2;
            vertices2[18] = u2;
        }
        if (yAmount != Animation.CurveTimeline.LINEAR) {
            float v = (vertices2[9] + yAmount) % 1.0f;
            float v2 = v + (this.height / ((float) this.texture.getHeight()));
            this.v = v;
            this.v2 = v2;
            vertices2[4] = v2;
            vertices2[9] = v;
            vertices2[14] = v;
            vertices2[19] = v2;
        }
    }
}
