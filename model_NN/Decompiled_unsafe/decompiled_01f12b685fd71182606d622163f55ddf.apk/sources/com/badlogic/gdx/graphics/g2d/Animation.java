package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class Animation {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$Animation$PlayMode;
    private float animationDuration;
    private float frameDuration;
    final TextureRegion[] keyFrames;
    private int lastFrameNumber;
    private float lastStateTime;
    private PlayMode playMode = PlayMode.NORMAL;

    public enum PlayMode {
        NORMAL,
        REVERSED,
        LOOP,
        LOOP_REVERSED,
        LOOP_PINGPONG,
        LOOP_RANDOM
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$Animation$PlayMode() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$Animation$PlayMode;
        if (iArr == null) {
            iArr = new int[PlayMode.values().length];
            try {
                iArr[PlayMode.LOOP.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PlayMode.LOOP_PINGPONG.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PlayMode.LOOP_RANDOM.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PlayMode.LOOP_REVERSED.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[PlayMode.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[PlayMode.REVERSED.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$Animation$PlayMode = iArr;
        }
        return iArr;
    }

    public Animation(float frameDuration2, Array<? extends TextureRegion> keyFrames2) {
        this.frameDuration = frameDuration2;
        this.animationDuration = ((float) keyFrames2.size) * frameDuration2;
        this.keyFrames = new TextureRegion[keyFrames2.size];
        int n = keyFrames2.size;
        for (int i = 0; i < n; i++) {
            this.keyFrames[i] = (TextureRegion) keyFrames2.get(i);
        }
        this.playMode = PlayMode.NORMAL;
    }

    public Animation(float frameDuration2, Array<? extends TextureRegion> keyFrames2, PlayMode playMode2) {
        this.frameDuration = frameDuration2;
        this.animationDuration = ((float) keyFrames2.size) * frameDuration2;
        this.keyFrames = new TextureRegion[keyFrames2.size];
        int n = keyFrames2.size;
        for (int i = 0; i < n; i++) {
            this.keyFrames[i] = (TextureRegion) keyFrames2.get(i);
        }
        this.playMode = playMode2;
    }

    public Animation(float frameDuration2, TextureRegion... keyFrames2) {
        this.frameDuration = frameDuration2;
        this.animationDuration = ((float) keyFrames2.length) * frameDuration2;
        this.keyFrames = keyFrames2;
        this.playMode = PlayMode.NORMAL;
    }

    public TextureRegion getKeyFrame(float stateTime, boolean looping) {
        PlayMode oldPlayMode = this.playMode;
        if (!looping || !(this.playMode == PlayMode.NORMAL || this.playMode == PlayMode.REVERSED)) {
            if (!(looping || this.playMode == PlayMode.NORMAL || this.playMode == PlayMode.REVERSED)) {
                if (this.playMode == PlayMode.LOOP_REVERSED) {
                    this.playMode = PlayMode.REVERSED;
                } else {
                    this.playMode = PlayMode.LOOP;
                }
            }
        } else if (this.playMode == PlayMode.NORMAL) {
            this.playMode = PlayMode.LOOP;
        } else {
            this.playMode = PlayMode.LOOP_REVERSED;
        }
        TextureRegion frame = getKeyFrame(stateTime);
        this.playMode = oldPlayMode;
        return frame;
    }

    public TextureRegion getKeyFrame(float stateTime) {
        return this.keyFrames[getKeyFrameIndex(stateTime)];
    }

    public int getKeyFrameIndex(float stateTime) {
        if (this.keyFrames.length == 1) {
            return 0;
        }
        int frameNumber = (int) (stateTime / this.frameDuration);
        switch ($SWITCH_TABLE$com$badlogic$gdx$graphics$g2d$Animation$PlayMode()[this.playMode.ordinal()]) {
            case 1:
                frameNumber = Math.min(this.keyFrames.length - 1, frameNumber);
                break;
            case 2:
                frameNumber = Math.max((this.keyFrames.length - frameNumber) - 1, 0);
                break;
            case 3:
                frameNumber %= this.keyFrames.length;
                break;
            case 4:
                frameNumber = (this.keyFrames.length - (frameNumber % this.keyFrames.length)) - 1;
                break;
            case 5:
                frameNumber %= (this.keyFrames.length * 2) - 2;
                if (frameNumber >= this.keyFrames.length) {
                    frameNumber = (this.keyFrames.length - 2) - (frameNumber - this.keyFrames.length);
                    break;
                }
                break;
            case 6:
                if (((int) (this.lastStateTime / this.frameDuration)) == frameNumber) {
                    frameNumber = this.lastFrameNumber;
                    break;
                } else {
                    frameNumber = MathUtils.random(this.keyFrames.length - 1);
                    break;
                }
        }
        this.lastFrameNumber = frameNumber;
        this.lastStateTime = stateTime;
        return frameNumber;
    }

    public TextureRegion[] getKeyFrames() {
        return this.keyFrames;
    }

    public PlayMode getPlayMode() {
        return this.playMode;
    }

    public void setPlayMode(PlayMode playMode2) {
        this.playMode = playMode2;
    }

    public boolean isAnimationFinished(float stateTime) {
        return this.keyFrames.length + -1 < ((int) (stateTime / this.frameDuration));
    }

    public void setFrameDuration(float frameDuration2) {
        this.frameDuration = frameDuration2;
        this.animationDuration = ((float) this.keyFrames.length) * frameDuration2;
    }

    public float getFrameDuration() {
        return this.frameDuration;
    }

    public float getAnimationDuration() {
        return this.animationDuration;
    }
}
