package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Affine2;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class PolygonSpriteBatch implements Batch {
    private int blendDstFunc;
    private int blendSrcFunc;
    private boolean blendingDisabled;
    float color;
    private final Matrix4 combinedMatrix;
    private ShaderProgram customShader;
    private boolean drawing;
    private float invTexHeight;
    private float invTexWidth;
    private Texture lastTexture;
    public int maxTrianglesInBatch;
    private Mesh mesh;
    private boolean ownsShader;
    private final Matrix4 projectionMatrix;
    public int renderCalls;
    private final ShaderProgram shader;
    private Color tempColor;
    public int totalRenderCalls;
    private final Matrix4 transformMatrix;
    private int triangleIndex;
    private final short[] triangles;
    private int vertexIndex;
    private final float[] vertices;

    public PolygonSpriteBatch() {
        this(2000, null);
    }

    public PolygonSpriteBatch(int size) {
        this(size, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(com.badlogic.gdx.graphics.Mesh$VertexDataType, boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
     arg types: [com.badlogic.gdx.graphics.Mesh$VertexDataType, int, int, int, com.badlogic.gdx.graphics.VertexAttribute[]]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
      com.badlogic.gdx.graphics.Mesh.<init>(com.badlogic.gdx.graphics.Mesh$VertexDataType, boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void */
    public PolygonSpriteBatch(int size, ShaderProgram defaultShader) {
        this.invTexWidth = Animation.CurveTimeline.LINEAR;
        this.invTexHeight = Animation.CurveTimeline.LINEAR;
        this.transformMatrix = new Matrix4();
        this.projectionMatrix = new Matrix4();
        this.combinedMatrix = new Matrix4();
        this.blendSrcFunc = 770;
        this.blendDstFunc = 771;
        this.color = Color.WHITE.toFloatBits();
        this.tempColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.renderCalls = 0;
        this.totalRenderCalls = 0;
        this.maxTrianglesInBatch = 0;
        if (size > 10920) {
            throw new IllegalArgumentException("Can't have more than 10920 triangles per batch: " + size);
        }
        this.mesh = new Mesh(Gdx.gl30 != null ? Mesh.VertexDataType.VertexBufferObjectWithVAO : Mesh.VertexDataType.VertexArray, false, size, size * 3, new VertexAttribute(1, 2, ShaderProgram.POSITION_ATTRIBUTE), new VertexAttribute(4, 4, ShaderProgram.COLOR_ATTRIBUTE), new VertexAttribute(16, 2, "a_texCoord0"));
        this.vertices = new float[(size * 5)];
        this.triangles = new short[(size * 3)];
        if (defaultShader == null) {
            this.shader = SpriteBatch.createDefaultShader();
            this.ownsShader = true;
        } else {
            this.shader = defaultShader;
        }
        this.projectionMatrix.setToOrtho2D(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) Gdx.graphics.getWidth(), (float) Gdx.graphics.getHeight());
    }

    public void begin() {
        if (this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.end must be called before begin.");
        }
        this.renderCalls = 0;
        Gdx.gl.glDepthMask(false);
        if (this.customShader != null) {
            this.customShader.begin();
        } else {
            this.shader.begin();
        }
        setupMatrices();
        this.drawing = true;
    }

    public void end() {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before end.");
        }
        if (this.vertexIndex > 0) {
            flush();
        }
        this.lastTexture = null;
        this.drawing = false;
        GL20 gl = Gdx.gl;
        gl.glDepthMask(true);
        if (isBlendingEnabled()) {
            gl.glDisable(GL20.GL_BLEND);
        }
        if (this.customShader != null) {
            this.customShader.end();
        } else {
            this.shader.end();
        }
    }

    public void setColor(Color tint) {
        this.color = tint.toFloatBits();
    }

    public void setColor(float r, float g, float b, float a) {
        this.color = NumberUtils.intToFloatColor((((int) (255.0f * a)) << 24) | (((int) (255.0f * b)) << 16) | (((int) (255.0f * g)) << 8) | ((int) (255.0f * r)));
    }

    public void setColor(float color2) {
        this.color = color2;
    }

    public Color getColor() {
        int intBits = NumberUtils.floatToIntColor(this.color);
        Color color2 = this.tempColor;
        color2.r = ((float) (intBits & 255)) / 255.0f;
        color2.g = ((float) ((intBits >>> 8) & 255)) / 255.0f;
        color2.b = ((float) ((intBits >>> 16) & 255)) / 255.0f;
        color2.a = ((float) ((intBits >>> 24) & 255)) / 255.0f;
        return color2;
    }

    public float getPackedColor() {
        return this.color;
    }

    public void draw(PolygonRegion region, float x, float y) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        short[] regionTriangles = region.triangles;
        int regionTrianglesLength = regionTriangles.length;
        float[] regionVertices = region.vertices;
        int regionVerticesLength = regionVertices.length;
        Texture texture = region.region.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + regionTrianglesLength > triangles2.length || this.vertexIndex + regionVerticesLength > this.vertices.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int vertexIndex2 = this.vertexIndex;
        int startVertex = vertexIndex2 / 5;
        int i = 0;
        int triangleIndex3 = triangleIndex2;
        while (i < regionTrianglesLength) {
            triangles2[triangleIndex3] = (short) (regionTriangles[i] + startVertex);
            i++;
            triangleIndex3++;
        }
        this.triangleIndex = triangleIndex3;
        float[] vertices2 = this.vertices;
        float color2 = this.color;
        float[] textureCoords = region.textureCoords;
        int i2 = 0;
        int vertexIndex3 = vertexIndex2;
        while (i2 < regionVerticesLength) {
            int vertexIndex4 = vertexIndex3 + 1;
            vertices2[vertexIndex3] = regionVertices[i2] + x;
            int vertexIndex5 = vertexIndex4 + 1;
            vertices2[vertexIndex4] = regionVertices[i2 + 1] + y;
            int vertexIndex6 = vertexIndex5 + 1;
            vertices2[vertexIndex5] = color2;
            int vertexIndex7 = vertexIndex6 + 1;
            vertices2[vertexIndex6] = textureCoords[i2];
            vertices2[vertexIndex7] = textureCoords[i2 + 1];
            i2 += 2;
            vertexIndex3 = vertexIndex7 + 1;
        }
        this.vertexIndex = vertexIndex3;
    }

    public void draw(PolygonRegion region, float x, float y, float width, float height) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        short[] regionTriangles = region.triangles;
        int regionTrianglesLength = regionTriangles.length;
        float[] regionVertices = region.vertices;
        int regionVerticesLength = regionVertices.length;
        TextureRegion textureRegion = region.region;
        Texture texture = textureRegion.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + regionTrianglesLength > triangles2.length || this.vertexIndex + regionVerticesLength > this.vertices.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int vertexIndex2 = this.vertexIndex;
        int startVertex = vertexIndex2 / 5;
        int i = 0;
        int n = regionTriangles.length;
        int triangleIndex3 = triangleIndex2;
        while (i < n) {
            triangles2[triangleIndex3] = (short) (regionTriangles[i] + startVertex);
            i++;
            triangleIndex3++;
        }
        this.triangleIndex = triangleIndex3;
        float[] vertices2 = this.vertices;
        float color2 = this.color;
        float[] textureCoords = region.textureCoords;
        float sX = width / ((float) textureRegion.regionWidth);
        float sY = height / ((float) textureRegion.regionHeight);
        int i2 = 0;
        int vertexIndex3 = vertexIndex2;
        while (i2 < regionVerticesLength) {
            int vertexIndex4 = vertexIndex3 + 1;
            vertices2[vertexIndex3] = (regionVertices[i2] * sX) + x;
            int vertexIndex5 = vertexIndex4 + 1;
            vertices2[vertexIndex4] = (regionVertices[i2 + 1] * sY) + y;
            int vertexIndex6 = vertexIndex5 + 1;
            vertices2[vertexIndex5] = color2;
            int vertexIndex7 = vertexIndex6 + 1;
            vertices2[vertexIndex6] = textureCoords[i2];
            vertices2[vertexIndex7] = textureCoords[i2 + 1];
            i2 += 2;
            vertexIndex3 = vertexIndex7 + 1;
        }
        this.vertexIndex = vertexIndex3;
    }

    public void draw(PolygonRegion region, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        short[] regionTriangles = region.triangles;
        int regionTrianglesLength = regionTriangles.length;
        float[] regionVertices = region.vertices;
        int regionVerticesLength = regionVertices.length;
        TextureRegion textureRegion = region.region;
        Texture texture = textureRegion.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + regionTrianglesLength > triangles2.length || this.vertexIndex + regionVerticesLength > this.vertices.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int vertexIndex2 = this.vertexIndex;
        int startVertex = vertexIndex2 / 5;
        int i = 0;
        int triangleIndex3 = triangleIndex2;
        while (i < regionTrianglesLength) {
            triangles2[triangleIndex3] = (short) (regionTriangles[i] + startVertex);
            i++;
            triangleIndex3++;
        }
        this.triangleIndex = triangleIndex3;
        float[] vertices2 = this.vertices;
        float color2 = this.color;
        float[] textureCoords = region.textureCoords;
        float worldOriginX = x + originX;
        float worldOriginY = y + originY;
        float sX = width / ((float) textureRegion.regionWidth);
        float sY = height / ((float) textureRegion.regionHeight);
        float cos = MathUtils.cosDeg(rotation);
        float sin = MathUtils.sinDeg(rotation);
        int i2 = 0;
        int vertexIndex3 = vertexIndex2;
        while (i2 < regionVerticesLength) {
            float fx = ((regionVertices[i2] * sX) - originX) * scaleX;
            float fy = ((regionVertices[i2 + 1] * sY) - originY) * scaleY;
            int vertexIndex4 = vertexIndex3 + 1;
            vertices2[vertexIndex3] = ((cos * fx) - (sin * fy)) + worldOriginX;
            int vertexIndex5 = vertexIndex4 + 1;
            vertices2[vertexIndex4] = (sin * fx) + (cos * fy) + worldOriginY;
            int vertexIndex6 = vertexIndex5 + 1;
            vertices2[vertexIndex5] = color2;
            int vertexIndex7 = vertexIndex6 + 1;
            vertices2[vertexIndex6] = textureCoords[i2];
            vertices2[vertexIndex7] = textureCoords[i2 + 1];
            i2 += 2;
            vertexIndex3 = vertexIndex7 + 1;
        }
        this.vertexIndex = vertexIndex3;
    }

    public void draw(Texture texture, float[] polygonVertices, int verticesOffset, int verticesCount, short[] polygonTriangles, int trianglesOffset, int trianglesCount) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else if (this.triangleIndex + trianglesCount > triangles2.length || this.vertexIndex + verticesCount > vertices2.length) {
            flush();
        }
        int triangleIndex2 = this.triangleIndex;
        int vertexIndex2 = this.vertexIndex;
        int startVertex = vertexIndex2 / 5;
        int i = trianglesOffset;
        int n = i + trianglesCount;
        int triangleIndex3 = triangleIndex2;
        while (i < n) {
            triangles2[triangleIndex3] = (short) (polygonTriangles[i] + startVertex);
            i++;
            triangleIndex3++;
        }
        this.triangleIndex = triangleIndex3;
        System.arraycopy(polygonVertices, verticesOffset, vertices2, vertexIndex2, verticesCount);
        this.vertexIndex += verticesCount;
    }

    public void draw(Texture texture, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation, int srcX, int srcY, int srcWidth, int srcHeight, boolean flipX, boolean flipY) {
        float x1;
        float y1;
        float x2;
        float y2;
        float x3;
        float y3;
        float x4;
        float y4;
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float worldOriginX = x + originX;
        float worldOriginY = y + originY;
        float fx = -originX;
        float fy = -originY;
        float fx2 = width - originX;
        float fy2 = height - originY;
        if (!(scaleX == 1.0f && scaleY == 1.0f)) {
            fx *= scaleX;
            fy *= scaleY;
            fx2 *= scaleX;
            fy2 *= scaleY;
        }
        float p1x = fx;
        float p1y = fy;
        float p2x = fx;
        float p2y = fy2;
        float p3x = fx2;
        float p3y = fy2;
        float p4x = fx2;
        float p4y = fy;
        if (rotation != Animation.CurveTimeline.LINEAR) {
            float cos = MathUtils.cosDeg(rotation);
            float sin = MathUtils.sinDeg(rotation);
            x1 = (cos * p1x) - (sin * p1y);
            y1 = (sin * p1x) + (cos * p1y);
            x2 = (cos * p2x) - (sin * p2y);
            y2 = (sin * p2x) + (cos * p2y);
            x3 = (cos * p3x) - (sin * p3y);
            y3 = (sin * p3x) + (cos * p3y);
            x4 = x1 + (x3 - x2);
            y4 = y3 - (y2 - y1);
        } else {
            x1 = p1x;
            y1 = p1y;
            x2 = p2x;
            y2 = p2y;
            x3 = p3x;
            y3 = p3y;
            x4 = p4x;
            y4 = p4y;
        }
        float x12 = x1 + worldOriginX;
        float y12 = y1 + worldOriginY;
        float x22 = x2 + worldOriginX;
        float y22 = y2 + worldOriginY;
        float x32 = x3 + worldOriginX;
        float y32 = y3 + worldOriginY;
        float x42 = x4 + worldOriginX;
        float y42 = y4 + worldOriginY;
        float u = ((float) srcX) * this.invTexWidth;
        float v = ((float) (srcY + srcHeight)) * this.invTexHeight;
        float u2 = ((float) (srcX + srcWidth)) * this.invTexWidth;
        float v2 = ((float) srcY) * this.invTexHeight;
        if (flipX) {
            float tmp = u;
            u = u2;
            u2 = tmp;
        }
        if (flipY) {
            float tmp2 = v;
            v = v2;
            v2 = tmp2;
        }
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x12;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y12;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x22;
        int idx8 = idx7 + 1;
        vertices2[idx7] = y22;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = x32;
        int idx13 = idx12 + 1;
        vertices2[idx12] = y32;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = x42;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y42;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(Texture texture, float x, float y, float width, float height, int srcX, int srcY, int srcWidth, int srcHeight, boolean flipX, boolean flipY) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float u = ((float) srcX) * this.invTexWidth;
        float v = ((float) (srcY + srcHeight)) * this.invTexHeight;
        float u2 = ((float) (srcX + srcWidth)) * this.invTexWidth;
        float v2 = ((float) srcY) * this.invTexHeight;
        float fx2 = x + width;
        float fy2 = y + height;
        if (flipX) {
            float tmp = u;
            u = u2;
            u2 = tmp;
        }
        if (flipY) {
            float tmp2 = v;
            v = v2;
            v2 = tmp2;
        }
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x;
        int idx8 = idx7 + 1;
        vertices2[idx7] = fy2;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = fx2;
        int idx13 = idx12 + 1;
        vertices2[idx12] = fy2;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = fx2;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(Texture texture, float x, float y, int srcX, int srcY, int srcWidth, int srcHeight) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
            flush();
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float u = ((float) srcX) * this.invTexWidth;
        float v = ((float) (srcY + srcHeight)) * this.invTexHeight;
        float u2 = ((float) (srcX + srcWidth)) * this.invTexWidth;
        float v2 = ((float) srcY) * this.invTexHeight;
        float fx2 = x + ((float) srcWidth);
        float fy2 = y + ((float) srcHeight);
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x;
        int idx8 = idx7 + 1;
        vertices2[idx7] = fy2;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = fx2;
        int idx13 = idx12 + 1;
        vertices2[idx12] = fy2;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = fx2;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(Texture texture, float x, float y, float width, float height, float u, float v, float u2, float v2) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
            flush();
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float fx2 = x + width;
        float fy2 = y + height;
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x;
        int idx8 = idx7 + 1;
        vertices2[idx7] = fy2;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = fx2;
        int idx13 = idx12 + 1;
        vertices2[idx12] = fy2;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = fx2;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(Texture texture, float x, float y) {
        draw(texture, x, y, (float) texture.getWidth(), (float) texture.getHeight());
    }

    public void draw(Texture texture, float x, float y, float width, float height) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
            flush();
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float fx2 = x + width;
        float fy2 = y + height;
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = 0.0f;
        int idx6 = idx5 + 1;
        vertices2[idx5] = 1.0f;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x;
        int idx8 = idx7 + 1;
        vertices2[idx7] = fy2;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = 0.0f;
        int idx11 = idx10 + 1;
        vertices2[idx10] = 0.0f;
        int idx12 = idx11 + 1;
        vertices2[idx11] = fx2;
        int idx13 = idx12 + 1;
        vertices2[idx12] = fy2;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = 1.0f;
        int idx16 = idx15 + 1;
        vertices2[idx15] = 0.0f;
        int idx17 = idx16 + 1;
        vertices2[idx16] = fx2;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = 1.0f;
        vertices2[idx20] = 1.0f;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(Texture texture, float[] spriteVertices, int offset, int count) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        int triangleCount = (count / 20) * 6;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else if (this.triangleIndex + triangleCount > triangles2.length || this.vertexIndex + count > vertices2.length) {
            flush();
        }
        int vertexIndex2 = this.vertexIndex;
        int triangleIndex2 = this.triangleIndex;
        short vertex = (short) (vertexIndex2 / 5);
        int n = triangleIndex2 + triangleCount;
        while (triangleIndex2 < n) {
            triangles2[triangleIndex2] = vertex;
            triangles2[triangleIndex2 + 1] = (short) (vertex + 1);
            triangles2[triangleIndex2 + 2] = (short) (vertex + 2);
            triangles2[triangleIndex2 + 3] = (short) (vertex + 2);
            triangles2[triangleIndex2 + 4] = (short) (vertex + 3);
            triangles2[triangleIndex2 + 5] = vertex;
            triangleIndex2 += 6;
            vertex = (short) (vertex + 4);
        }
        this.triangleIndex = triangleIndex2;
        System.arraycopy(spriteVertices, offset, vertices2, vertexIndex2, count);
        this.vertexIndex += count;
    }

    public void draw(TextureRegion region, float x, float y) {
        draw(region, x, y, (float) region.getRegionWidth(), (float) region.getRegionHeight());
    }

    public void draw(TextureRegion region, float x, float y, float width, float height) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        Texture texture = region.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float fx2 = x + width;
        float fy2 = y + height;
        float u = region.u;
        float v = region.v2;
        float u2 = region.u2;
        float v2 = region.v;
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x;
        int idx8 = idx7 + 1;
        vertices2[idx7] = fy2;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = fx2;
        int idx13 = idx12 + 1;
        vertices2[idx12] = fy2;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = fx2;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(TextureRegion region, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
        float x1;
        float y1;
        float x2;
        float y2;
        float x3;
        float y3;
        float x4;
        float y4;
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        Texture texture = region.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float worldOriginX = x + originX;
        float worldOriginY = y + originY;
        float fx = -originX;
        float fy = -originY;
        float fx2 = width - originX;
        float fy2 = height - originY;
        if (!(scaleX == 1.0f && scaleY == 1.0f)) {
            fx *= scaleX;
            fy *= scaleY;
            fx2 *= scaleX;
            fy2 *= scaleY;
        }
        float p1x = fx;
        float p1y = fy;
        float p2x = fx;
        float p2y = fy2;
        float p3x = fx2;
        float p3y = fy2;
        float p4x = fx2;
        float p4y = fy;
        if (rotation != Animation.CurveTimeline.LINEAR) {
            float cos = MathUtils.cosDeg(rotation);
            float sin = MathUtils.sinDeg(rotation);
            x1 = (cos * p1x) - (sin * p1y);
            y1 = (sin * p1x) + (cos * p1y);
            x2 = (cos * p2x) - (sin * p2y);
            y2 = (sin * p2x) + (cos * p2y);
            x3 = (cos * p3x) - (sin * p3y);
            y3 = (sin * p3x) + (cos * p3y);
            x4 = x1 + (x3 - x2);
            y4 = y3 - (y2 - y1);
        } else {
            x1 = p1x;
            y1 = p1y;
            x2 = p2x;
            y2 = p2y;
            x3 = p3x;
            y3 = p3y;
            x4 = p4x;
            y4 = p4y;
        }
        float u = region.u;
        float v = region.v2;
        float u2 = region.u2;
        float v2 = region.v;
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x1 + worldOriginX;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y1 + worldOriginY;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x2 + worldOriginX;
        int idx8 = idx7 + 1;
        vertices2[idx7] = y2 + worldOriginY;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = x3 + worldOriginX;
        int idx13 = idx12 + 1;
        vertices2[idx12] = y3 + worldOriginY;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = x4 + worldOriginX;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y4 + worldOriginY;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(TextureRegion region, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation, boolean clockwise) {
        float x1;
        float y1;
        float x2;
        float y2;
        float x3;
        float y3;
        float x4;
        float y4;
        float u1;
        float v1;
        float u2;
        float v2;
        float u3;
        float v3;
        float u4;
        float v4;
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        Texture texture = region.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float worldOriginX = x + originX;
        float worldOriginY = y + originY;
        float fx = -originX;
        float fy = -originY;
        float fx2 = width - originX;
        float fy2 = height - originY;
        if (!(scaleX == 1.0f && scaleY == 1.0f)) {
            fx *= scaleX;
            fy *= scaleY;
            fx2 *= scaleX;
            fy2 *= scaleY;
        }
        float p1x = fx;
        float p1y = fy;
        float p2x = fx;
        float p2y = fy2;
        float p3x = fx2;
        float p3y = fy2;
        float p4x = fx2;
        float p4y = fy;
        if (rotation != Animation.CurveTimeline.LINEAR) {
            float cos = MathUtils.cosDeg(rotation);
            float sin = MathUtils.sinDeg(rotation);
            x1 = (cos * p1x) - (sin * p1y);
            y1 = (sin * p1x) + (cos * p1y);
            x2 = (cos * p2x) - (sin * p2y);
            y2 = (sin * p2x) + (cos * p2y);
            x3 = (cos * p3x) - (sin * p3y);
            y3 = (sin * p3x) + (cos * p3y);
            x4 = x1 + (x3 - x2);
            y4 = y3 - (y2 - y1);
        } else {
            x1 = p1x;
            y1 = p1y;
            x2 = p2x;
            y2 = p2y;
            x3 = p3x;
            y3 = p3y;
            x4 = p4x;
            y4 = p4y;
        }
        float x12 = x1 + worldOriginX;
        float y12 = y1 + worldOriginY;
        float x22 = x2 + worldOriginX;
        float y22 = y2 + worldOriginY;
        float x32 = x3 + worldOriginX;
        float y32 = y3 + worldOriginY;
        float x42 = x4 + worldOriginX;
        float y42 = y4 + worldOriginY;
        if (clockwise) {
            u1 = region.u2;
            v1 = region.v2;
            u2 = region.u;
            v2 = region.v2;
            u3 = region.u;
            v3 = region.v;
            u4 = region.u2;
            v4 = region.v;
        } else {
            u1 = region.u;
            v1 = region.v;
            u2 = region.u2;
            v2 = region.v;
            u3 = region.u2;
            v3 = region.v2;
            u4 = region.u;
            v4 = region.v2;
        }
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x12;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y12;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u1;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v1;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x22;
        int idx8 = idx7 + 1;
        vertices2[idx7] = y22;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u2;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = x32;
        int idx13 = idx12 + 1;
        vertices2[idx12] = y32;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u3;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v3;
        int idx17 = idx16 + 1;
        vertices2[idx16] = x42;
        int idx18 = idx17 + 1;
        vertices2[idx17] = y42;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u4;
        vertices2[idx20] = v4;
        this.vertexIndex = idx20 + 1;
    }

    public void draw(TextureRegion region, float width, float height, Affine2 transform) {
        if (!this.drawing) {
            throw new IllegalStateException("PolygonSpriteBatch.begin must be called before draw.");
        }
        short[] triangles2 = this.triangles;
        float[] vertices2 = this.vertices;
        Texture texture = region.texture;
        if (texture != this.lastTexture) {
            switchTexture(texture);
        } else {
            if (this.triangleIndex + 6 > triangles2.length || this.vertexIndex + 20 > vertices2.length) {
                flush();
            }
        }
        int triangleIndex2 = this.triangleIndex;
        int startVertex = this.vertexIndex / 5;
        int triangleIndex3 = triangleIndex2 + 1;
        triangles2[triangleIndex2] = (short) startVertex;
        int triangleIndex4 = triangleIndex3 + 1;
        triangles2[triangleIndex3] = (short) (startVertex + 1);
        int triangleIndex5 = triangleIndex4 + 1;
        triangles2[triangleIndex4] = (short) (startVertex + 2);
        int triangleIndex6 = triangleIndex5 + 1;
        triangles2[triangleIndex5] = (short) (startVertex + 2);
        int triangleIndex7 = triangleIndex6 + 1;
        triangles2[triangleIndex6] = (short) (startVertex + 3);
        triangles2[triangleIndex7] = (short) startVertex;
        this.triangleIndex = triangleIndex7 + 1;
        float x1 = transform.m02;
        float y1 = transform.m12;
        float x2 = (transform.m01 * height) + transform.m02;
        float y2 = (transform.m11 * height) + transform.m12;
        float x3 = (transform.m00 * width) + (transform.m01 * height) + transform.m02;
        float y3 = (transform.m10 * width) + (transform.m11 * height) + transform.m12;
        float x4 = (transform.m00 * width) + transform.m02;
        float u = region.u;
        float v = region.v2;
        float u2 = region.u2;
        float v2 = region.v;
        float color2 = this.color;
        int idx = this.vertexIndex;
        int idx2 = idx + 1;
        vertices2[idx] = x1;
        int idx3 = idx2 + 1;
        vertices2[idx2] = y1;
        int idx4 = idx3 + 1;
        vertices2[idx3] = color2;
        int idx5 = idx4 + 1;
        vertices2[idx4] = u;
        int idx6 = idx5 + 1;
        vertices2[idx5] = v;
        int idx7 = idx6 + 1;
        vertices2[idx6] = x2;
        int idx8 = idx7 + 1;
        vertices2[idx7] = y2;
        int idx9 = idx8 + 1;
        vertices2[idx8] = color2;
        int idx10 = idx9 + 1;
        vertices2[idx9] = u;
        int idx11 = idx10 + 1;
        vertices2[idx10] = v2;
        int idx12 = idx11 + 1;
        vertices2[idx11] = x3;
        int idx13 = idx12 + 1;
        vertices2[idx12] = y3;
        int idx14 = idx13 + 1;
        vertices2[idx13] = color2;
        int idx15 = idx14 + 1;
        vertices2[idx14] = u2;
        int idx16 = idx15 + 1;
        vertices2[idx15] = v2;
        int idx17 = idx16 + 1;
        vertices2[idx16] = x4;
        int idx18 = idx17 + 1;
        vertices2[idx17] = (transform.m10 * width) + transform.m12;
        int idx19 = idx18 + 1;
        vertices2[idx18] = color2;
        int idx20 = idx19 + 1;
        vertices2[idx19] = u2;
        vertices2[idx20] = v;
        this.vertexIndex = idx20 + 1;
    }

    public void flush() {
        if (this.vertexIndex != 0) {
            this.renderCalls++;
            this.totalRenderCalls++;
            int trianglesInBatch = this.triangleIndex;
            if (trianglesInBatch > this.maxTrianglesInBatch) {
                this.maxTrianglesInBatch = trianglesInBatch;
            }
            this.lastTexture.bind();
            Mesh mesh2 = this.mesh;
            mesh2.setVertices(this.vertices, 0, this.vertexIndex);
            mesh2.setIndices(this.triangles, 0, this.triangleIndex);
            if (this.blendingDisabled) {
                Gdx.gl.glDisable(GL20.GL_BLEND);
            } else {
                Gdx.gl.glEnable(GL20.GL_BLEND);
                if (this.blendSrcFunc != -1) {
                    Gdx.gl.glBlendFunc(this.blendSrcFunc, this.blendDstFunc);
                }
            }
            mesh2.render(this.customShader != null ? this.customShader : this.shader, 4, 0, trianglesInBatch);
            this.vertexIndex = 0;
            this.triangleIndex = 0;
        }
    }

    public void disableBlending() {
        flush();
        this.blendingDisabled = true;
    }

    public void enableBlending() {
        flush();
        this.blendingDisabled = false;
    }

    public void setBlendFunction(int srcFunc, int dstFunc) {
        if (this.blendSrcFunc != srcFunc || this.blendDstFunc != dstFunc) {
            flush();
            this.blendSrcFunc = srcFunc;
            this.blendDstFunc = dstFunc;
        }
    }

    public int getBlendSrcFunc() {
        return this.blendSrcFunc;
    }

    public int getBlendDstFunc() {
        return this.blendDstFunc;
    }

    public void dispose() {
        this.mesh.dispose();
        if (this.ownsShader && this.shader != null) {
            this.shader.dispose();
        }
    }

    public Matrix4 getProjectionMatrix() {
        return this.projectionMatrix;
    }

    public Matrix4 getTransformMatrix() {
        return this.transformMatrix;
    }

    public void setProjectionMatrix(Matrix4 projection) {
        if (this.drawing) {
            flush();
        }
        this.projectionMatrix.set(projection);
        if (this.drawing) {
            setupMatrices();
        }
    }

    public void setTransformMatrix(Matrix4 transform) {
        if (this.drawing) {
            flush();
        }
        this.transformMatrix.set(transform);
        if (this.drawing) {
            setupMatrices();
        }
    }

    private void setupMatrices() {
        this.combinedMatrix.set(this.projectionMatrix).mul(this.transformMatrix);
        if (this.customShader != null) {
            this.customShader.setUniformMatrix("u_projTrans", this.combinedMatrix);
            this.customShader.setUniformi("u_texture", 0);
            return;
        }
        this.shader.setUniformMatrix("u_projTrans", this.combinedMatrix);
        this.shader.setUniformi("u_texture", 0);
    }

    private void switchTexture(Texture texture) {
        flush();
        this.lastTexture = texture;
        this.invTexWidth = 1.0f / ((float) texture.getWidth());
        this.invTexHeight = 1.0f / ((float) texture.getHeight());
    }

    public void setShader(ShaderProgram shader2) {
        if (this.drawing) {
            flush();
            if (this.customShader != null) {
                this.customShader.end();
            } else {
                this.shader.end();
            }
        }
        this.customShader = shader2;
        if (this.drawing) {
            if (this.customShader != null) {
                this.customShader.begin();
            } else {
                this.shader.begin();
            }
            setupMatrices();
        }
    }

    public boolean isBlendingEnabled() {
        return !this.blendingDisabled;
    }

    public boolean isDrawing() {
        return this.drawing;
    }
}
