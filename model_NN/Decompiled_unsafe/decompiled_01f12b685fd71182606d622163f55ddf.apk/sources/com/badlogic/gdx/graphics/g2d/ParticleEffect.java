package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.kbz.KBZ_Tools;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.StreamUtils;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.Iterator;

public class ParticleEffect implements Disposable {
    private BoundingBox bounds;
    private final Array<ParticleEmitter> emitters;
    private boolean ownsTexture;

    public ParticleEffect() {
        this.emitters = new Array<>(8);
    }

    public ParticleEffect(ParticleEffect effect) {
        this.emitters = new Array<>(true, effect.emitters.size);
        int n = effect.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.add(new ParticleEmitter(effect.emitters.get(i)));
        }
    }

    public void start() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).start();
        }
    }

    public void reset() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).reset();
        }
    }

    public void draw(SpriteBatch batch, float delta, float parentAlpha) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).draw(batch, delta, parentAlpha);
        }
    }

    public void draw(SpriteBatch batch, float delta) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).draw(batch, delta, 1.0f);
        }
    }

    public void allowCompletion() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).allowCompletion();
        }
    }

    public boolean isComplete() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            if (!this.emitters.get(i).isComplete()) {
                return false;
            }
        }
        return true;
    }

    public void setDuration(int duration) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            emitter.setContinuous(false);
            emitter.duration = (float) duration;
            emitter.durationTimer = Animation.CurveTimeline.LINEAR;
        }
    }

    public void setPosition(float x, float y) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).setPosition(x, y);
        }
    }

    public void setFlip(boolean flipX, boolean flipY) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).setFlip(flipX, flipY);
        }
    }

    public void flipY() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.get(i).flipY();
        }
    }

    public Array<ParticleEmitter> getEmitters() {
        return this.emitters;
    }

    public ParticleEmitter findEmitter(String name) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            if (emitter.getName().equals(name)) {
                return emitter;
            }
        }
        return null;
    }

    public void save(File file) {
        Writer output = null;
        try {
            Writer output2 = new FileWriter(file);
            int i = 0;
            try {
                int n = this.emitters.size;
                int index = 0;
                while (i < n) {
                    ParticleEmitter emitter = this.emitters.get(i);
                    int index2 = index + 1;
                    if (index > 0) {
                        output2.write("\n\n");
                    }
                    emitter.save(output2);
                    output2.write("- Image Path -\n");
                    output2.write(String.valueOf(emitter.getImagePath()) + "\n");
                    i++;
                    index = index2;
                }
                StreamUtils.closeQuietly(output2);
            } catch (IOException e) {
                ex = e;
                output = output2;
                try {
                    throw new GdxRuntimeException("Error saving effect: " + file, ex);
                } catch (Throwable th) {
                    th = th;
                    StreamUtils.closeQuietly(output);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                output = output2;
                StreamUtils.closeQuietly(output);
                throw th;
            }
        } catch (IOException e2) {
            ex = e2;
            throw new GdxRuntimeException("Error saving effect: " + file, ex);
        }
    }

    public void save_bin(File file) {
        DataOutputStream dos = null;
        try {
            DataOutputStream dos2 = new DataOutputStream(new FileOutputStream(file));
            try {
                dos2.writeShort(this.emitters.size);
                int n = this.emitters.size;
                for (int i = 0; i < n; i++) {
                    this.emitters.get(i).save_bin(dos2);
                }
                StreamUtils.closeQuietly(dos2);
            } catch (IOException e) {
                ex = e;
                dos = dos2;
                try {
                    throw new GdxRuntimeException("Error saving effect: " + file, ex);
                } catch (Throwable th) {
                    th = th;
                    StreamUtils.closeQuietly(dos);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                dos = dos2;
                StreamUtils.closeQuietly(dos);
                throw th;
            }
        } catch (IOException e2) {
            ex = e2;
            throw new GdxRuntimeException("Error saving effect: " + file, ex);
        }
    }

    public void load(FileHandle effectFile, FileHandle imagesDir) {
        loadEmitters(effectFile);
        loadEmitterImages(imagesDir);
    }

    public void load(FileHandle effectFile, TextureAtlas atlas) {
        load(effectFile, atlas, null);
    }

    public void load(InputStream input, boolean pxFormat) throws Exception {
        if (pxFormat) {
            loadEmitters_2(input);
        } else {
            loadEmitters_1(input);
        }
    }

    public void load(FileHandle effectFile, TextureAtlas atlas, String atlasPrefix) {
        loadEmitters(effectFile);
        loadEmitterImages(atlas, atlasPrefix);
    }

    public void loadEmitters(FileHandle effectFile) {
        InputStream input = effectFile.read();
        try {
            if (effectFile.name().endsWith(".px")) {
                loadEmitters_2(input);
            } else {
                loadEmitters_1(input);
            }
            StreamUtils.closeQuietly(input);
        } catch (Exception e) {
            throw new GdxRuntimeException("Error loading effect: " + effectFile, e);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(input);
            throw th;
        }
    }

    private void loadEmitters_2(InputStream inputStream) throws Exception {
        DataInputStream dis = new DataInputStream(inputStream);
        this.emitters.clear();
        short len = dis.readShort();
        for (int i = 0; i < len; i++) {
            this.emitters.add(new ParticleEmitter(dis));
        }
    }

    private void loadEmitters_1(InputStream input) throws Exception {
        this.emitters.clear();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input), 512);
        do {
            ParticleEmitter emitter = new ParticleEmitter(reader);
            reader.readLine();
            emitter.setImagePath(reader.readLine());
            this.emitters.add(emitter);
            if (reader.readLine() == null) {
                return;
            }
        } while (reader.readLine() != null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void loadEmitterImages(TextureAtlas atlas, String atlasPrefix) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            String imagePath = emitter.getImagePath();
            if (imagePath != null) {
                String imageName = new File(imagePath.replace('\\', '/')).getName();
                int lastDotIndex = imageName.lastIndexOf(46);
                if (lastDotIndex != -1) {
                    imageName = imageName.substring(0, lastDotIndex);
                }
                if (atlasPrefix != null) {
                    imageName = String.valueOf(atlasPrefix) + imageName;
                }
                Sprite sprite = atlas.createSprite(imageName);
                if (sprite == null) {
                    throw new IllegalArgumentException("SpriteSheet missing image: " + imageName);
                }
                emitter.setSprite(sprite);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void loadEmitterImages(FileHandle imagesDir) {
        this.ownsTexture = true;
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = this.emitters.get(i);
            String imagePath = emitter.getImagePath();
            if (imagePath != null) {
                String imageName = new File(imagePath.replace('\\', '/')).getName();
                emitter.setSprite(new Sprite(new Texture("imageAll/" + KBZ_Tools.getPackName(KBZ_Tools.getImageId(imageName)) + "/" + imageName)));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    /* access modifiers changed from: protected */
    public Texture loadTexture(FileHandle file) {
        return new Texture(file, false);
    }

    public void dispose() {
        if (this.ownsTexture) {
            int n = this.emitters.size;
            for (int i = 0; i < n; i++) {
                this.emitters.get(i).getSprite().getTexture().dispose();
            }
        }
    }

    public BoundingBox getBoundingBox() {
        if (this.bounds == null) {
            this.bounds = new BoundingBox();
        }
        BoundingBox bounds2 = this.bounds;
        bounds2.inf();
        Iterator<ParticleEmitter> it = this.emitters.iterator();
        while (it.hasNext()) {
            bounds2.ext(it.next().getBoundingBox());
        }
        return bounds2;
    }

    public void scaleEffect(float scaleFactor) {
        scaleEffect(scaleFactor, scaleFactor);
    }

    public void scaleEffect(float xScaleFactor, float yScaleFactor) {
        Iterator<ParticleEmitter> it = this.emitters.iterator();
        while (it.hasNext()) {
            it.next().setScaleFactor(xScaleFactor, yScaleFactor);
        }
    }
}
