package com.badlogic.gdx.graphics.g3d.model;

import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.ArrayMap;

public class NodePart {
    public Matrix4[] bones;
    public boolean enabled = true;
    public ArrayMap<Node, Matrix4> invBoneBindTransforms;
    public Material material;
    public MeshPart meshPart;

    public NodePart() {
    }

    public NodePart(MeshPart meshPart2, Material material2) {
        this.meshPart = meshPart2;
        this.material = material2;
    }

    public Renderable setRenderable(Renderable out) {
        out.material = this.material;
        out.mesh = this.meshPart.mesh;
        out.meshPartOffset = this.meshPart.indexOffset;
        out.meshPartSize = this.meshPart.numVertices;
        out.primitiveType = this.meshPart.primitiveType;
        out.bones = this.bones;
        return out;
    }
}
