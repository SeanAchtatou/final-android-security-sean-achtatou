package com.badlogic.gdx.graphics.g2d.freetype;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.kbz.esotericsoftware.spine.Animation;
import java.nio.ByteBuffer;

public class FreeTypeFontGenerator implements Disposable {
    public static final String DEFAULT_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890\"!`?'.,;:()[]{}<>|/@\\^$-%+=#_&~* ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";
    public static final int NO_MAXIMUM = -1;
    private static int maxTextureSize = 1024;
    boolean bitmapped = false;
    final FreeType.Face face;
    final String filePath;
    final FreeType.Library library;

    public static class FreeTypeFontParameter {
        public String characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        public boolean flip = false;
        public boolean genMipMaps = false;
        public boolean kerning = true;
        public Texture.TextureFilter magFilter = Texture.TextureFilter.Nearest;
        public Texture.TextureFilter minFilter = Texture.TextureFilter.Nearest;
        public PixmapPacker packer = null;
        public int size = 16;
    }

    public static void setMaxTextureSize(int texSize) {
        maxTextureSize = texSize;
    }

    public static int getMaxTextureSize() {
        return maxTextureSize;
    }

    public FreeTypeFontGenerator(FileHandle font) {
        this.filePath = font.pathWithoutExtension();
        this.library = FreeType.initFreeType();
        if (this.library == null) {
            throw new GdxRuntimeException("Couldn't initialize FreeType");
        }
        this.face = FreeType.newFace(this.library, font, 0);
        if (this.face == null) {
            throw new GdxRuntimeException("Couldn't create face for font '" + font + "'");
        } else if (!checkForBitmapFont() && !FreeType.setPixelSizes(this.face, 0, 15)) {
            throw new GdxRuntimeException("Couldn't set size for font '" + font + "'");
        }
    }

    private boolean checkForBitmapFont() {
        if ((this.face.getFaceFlags() & FreeType.FT_FACE_FLAG_FIXED_SIZES) == FreeType.FT_FACE_FLAG_FIXED_SIZES && (this.face.getFaceFlags() & FreeType.FT_FACE_FLAG_HORIZONTAL) == FreeType.FT_FACE_FLAG_HORIZONTAL && FreeType.loadChar(this.face, 32, FreeType.FT_LOAD_DEFAULT) && this.face.getGlyph().getFormat() == 1651078259) {
            this.bitmapped = true;
        }
        return this.bitmapped;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion[], boolean):void}
     arg types: [com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator$FreeTypeBitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion[], int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      SimpleMethodDetails{com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion[], boolean):void} */
    public BitmapFont generateFont(int size, String characters, boolean flip) {
        FreeTypeBitmapFontData data = generateData(size, characters, flip, null);
        BitmapFont font = new BitmapFont((BitmapFont.BitmapFontData) data, data.getTextureRegions(), false);
        font.setOwnsTexture(true);
        return font;
    }

    public BitmapFont generateFont(int size) {
        return generateFont(size, DEFAULT_CHARS, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion[], boolean):void}
     arg types: [com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator$FreeTypeBitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion[], int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      SimpleMethodDetails{com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion[], boolean):void} */
    public BitmapFont generateFont(FreeTypeFontParameter parameter) {
        FreeTypeBitmapFontData data = generateData(parameter);
        BitmapFont font = new BitmapFont((BitmapFont.BitmapFontData) data, data.getTextureRegions(), false);
        font.setOwnsTexture(true);
        return font;
    }

    public int scaleForPixelHeight(int height) {
        if (this.bitmapped || FreeType.setPixelSizes(this.face, 0, height)) {
            FreeType.SizeMetrics fontMetrics = this.face.getSize().getMetrics();
            return (height * height) / (FreeType.toInt(fontMetrics.getAscender()) - FreeType.toInt(fontMetrics.getDescender()));
        }
        throw new GdxRuntimeException("Couldn't set size for font");
    }

    public int scaleForPixelWidth(int width, int numChars) {
        FreeType.SizeMetrics fontMetrics = this.face.getSize().getMetrics();
        int height = ((FreeType.toInt(fontMetrics.getAscender()) - FreeType.toInt(fontMetrics.getDescender())) * width) / (FreeType.toInt(fontMetrics.getMaxAdvance()) * numChars);
        if (this.bitmapped || FreeType.setPixelSizes(this.face, 0, height)) {
            return height;
        }
        throw new GdxRuntimeException("Couldn't set size for font");
    }

    public int scaleToFitSquare(int width, int height, int numChars) {
        return Math.min(scaleForPixelHeight(height), scaleForPixelWidth(width, numChars));
    }

    public class GlyphAndBitmap {
        public FreeType.Bitmap bitmap;
        public BitmapFont.Glyph glyph;

        public GlyphAndBitmap() {
        }
    }

    public GlyphAndBitmap generateGlyphAndBitmap(int c, int size, boolean flip) {
        FreeType.Bitmap bitmap;
        if (this.bitmapped || FreeType.setPixelSizes(this.face, 0, size)) {
            int baseline = FreeType.toInt(this.face.getSize().getMetrics().getAscender());
            if (FreeType.getCharIndex(this.face, c) == 0) {
                return null;
            }
            if (!FreeType.loadChar(this.face, c, FreeType.FT_LOAD_DEFAULT)) {
                throw new GdxRuntimeException("Unable to load character!");
            }
            FreeType.GlyphSlot slot = this.face.getGlyph();
            if (this.bitmapped) {
                bitmap = slot.getBitmap();
            } else if (!FreeType.renderGlyph(slot, FreeType.FT_RENDER_MODE_LIGHT)) {
                bitmap = null;
            } else {
                bitmap = slot.getBitmap();
            }
            FreeType.GlyphMetrics metrics = slot.getMetrics();
            BitmapFont.Glyph glyph = new BitmapFont.Glyph();
            if (bitmap != null) {
                glyph.width = bitmap.getWidth();
                glyph.height = bitmap.getRows();
            } else {
                glyph.width = 0;
                glyph.height = 0;
            }
            glyph.xoffset = slot.getBitmapLeft();
            glyph.yoffset = flip ? (-slot.getBitmapTop()) + baseline : (-(glyph.height - slot.getBitmapTop())) - baseline;
            glyph.xadvance = FreeType.toInt(metrics.getHoriAdvance());
            glyph.srcX = 0;
            glyph.srcY = 0;
            glyph.id = c;
            GlyphAndBitmap result = new GlyphAndBitmap();
            result.glyph = glyph;
            result.bitmap = bitmap;
            return result;
        }
        throw new GdxRuntimeException("Couldn't set size for font");
    }

    public FreeTypeBitmapFontData generateData(int size) {
        return generateData(size, DEFAULT_CHARS, false, null);
    }

    public FreeTypeBitmapFontData generateData(int size, String characters, boolean flip) {
        return generateData(size, characters, flip, null);
    }

    public FreeTypeBitmapFontData generateData(int size, String characters, boolean flip, PixmapPacker packer) {
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = size;
        parameter.characters = characters;
        parameter.flip = flip;
        parameter.packer = packer;
        return generateData(parameter);
    }

    public FreeTypeBitmapFontData generateData(FreeTypeFontParameter parameter) {
        String packPrefix;
        int kerning;
        int i;
        if (parameter == null) {
            parameter = new FreeTypeFontParameter();
        }
        FreeTypeBitmapFontData data = new FreeTypeBitmapFontData();
        if (this.bitmapped || FreeType.setPixelSizes(this.face, 0, parameter.size)) {
            FreeType.SizeMetrics fontMetrics = this.face.getSize().getMetrics();
            data.flipped = parameter.flip;
            data.ascent = (float) FreeType.toInt(fontMetrics.getAscender());
            data.descent = (float) FreeType.toInt(fontMetrics.getDescender());
            data.lineHeight = (float) FreeType.toInt(fontMetrics.getHeight());
            float baseLine = data.ascent;
            if (this.bitmapped && data.lineHeight == Animation.CurveTimeline.LINEAR) {
                for (int c = 32; c < this.face.getNumGlyphs() + 32; c++) {
                    if (FreeType.loadChar(this.face, c, FreeType.FT_LOAD_DEFAULT)) {
                        int lh = FreeType.toInt(this.face.getGlyph().getMetrics().getHeight());
                        data.lineHeight = ((float) lh) > data.lineHeight ? (float) lh : data.lineHeight;
                    }
                }
            }
            if (FreeType.loadChar(this.face, 32, FreeType.FT_LOAD_DEFAULT)) {
                data.spaceWidth = (float) FreeType.toInt(this.face.getGlyph().getMetrics().getHoriAdvance());
            } else {
                data.spaceWidth = (float) this.face.getMaxAdvanceWidth();
            }
            BitmapFont.Glyph spaceGlyph = new BitmapFont.Glyph();
            spaceGlyph.xadvance = (int) data.spaceWidth;
            spaceGlyph.id = 32;
            data.setGlyph(32, spaceGlyph);
            char[] arr$ = BitmapFont.xChars;
            int len$ = arr$.length;
            int i$ = 0;
            while (true) {
                if (i$ < len$) {
                    if (FreeType.loadChar(this.face, arr$[i$], FreeType.FT_LOAD_DEFAULT)) {
                        data.xHeight = (float) FreeType.toInt(this.face.getGlyph().getMetrics().getHeight());
                        break;
                    }
                    i$++;
                } else {
                    break;
                }
            }
            if (data.xHeight == Animation.CurveTimeline.LINEAR) {
                throw new GdxRuntimeException("No x-height character found in font");
            }
            char[] arr$2 = BitmapFont.capChars;
            int len$2 = arr$2.length;
            int i$2 = 0;
            while (true) {
                if (i$2 < len$2) {
                    if (FreeType.loadChar(this.face, arr$2[i$2], FreeType.FT_LOAD_DEFAULT)) {
                        data.capHeight = (float) FreeType.toInt(this.face.getGlyph().getMetrics().getHeight());
                        break;
                    }
                    i$2++;
                } else {
                    break;
                }
            }
            if (this.bitmapped || data.capHeight != 1.0f) {
                data.ascent -= data.capHeight;
                data.down = -data.lineHeight;
                if (parameter.flip) {
                    data.ascent = -data.ascent;
                    data.down = -data.down;
                }
                boolean ownsAtlas = false;
                PixmapPacker packer = parameter.packer;
                if (packer == null) {
                    int maxGlyphHeight = (int) Math.ceil((double) data.lineHeight);
                    int pageWidth = MathUtils.nextPowerOfTwo((int) Math.sqrt((double) (maxGlyphHeight * maxGlyphHeight * parameter.characters.length())));
                    if (maxTextureSize > 0) {
                        pageWidth = Math.min(pageWidth, maxTextureSize);
                    }
                    ownsAtlas = true;
                    packer = new PixmapPacker(pageWidth, pageWidth, Pixmap.Format.RGBA8888, 2, false);
                }
                if (ownsAtlas) {
                    packPrefix = "";
                } else {
                    packPrefix = this.filePath + '_' + parameter.size + (parameter.flip ? "_flip_" : '_');
                }
                for (int i2 = 0; i2 < parameter.characters.length(); i2++) {
                    char c2 = parameter.characters.charAt(i2);
                    if (!FreeType.loadChar(this.face, c2, FreeType.FT_LOAD_DEFAULT)) {
                        Gdx.app.log("FreeTypeFontGenerator", "Couldn't load char '" + c2 + "'");
                    } else if (!FreeType.renderGlyph(this.face.getGlyph(), FreeType.FT_RENDER_MODE_NORMAL)) {
                        Gdx.app.log("FreeTypeFontGenerator", "Couldn't render char '" + c2 + "'");
                    } else {
                        FreeType.GlyphSlot slot = this.face.getGlyph();
                        FreeType.GlyphMetrics metrics = slot.getMetrics();
                        FreeType.Bitmap bitmap = slot.getBitmap();
                        Pixmap pixmap = bitmap.getPixmap(Pixmap.Format.RGBA8888);
                        BitmapFont.Glyph glyph = new BitmapFont.Glyph();
                        glyph.id = c2;
                        glyph.width = pixmap.getWidth();
                        glyph.height = pixmap.getHeight();
                        glyph.xoffset = slot.getBitmapLeft();
                        if (parameter.flip) {
                            i = (-slot.getBitmapTop()) + ((int) baseLine);
                        } else {
                            i = (-(glyph.height - slot.getBitmapTop())) - ((int) baseLine);
                        }
                        glyph.yoffset = i;
                        glyph.xadvance = FreeType.toInt(metrics.getHoriAdvance());
                        if (this.bitmapped) {
                            pixmap.setColor(Color.CLEAR);
                            pixmap.fill();
                            ByteBuffer buf = bitmap.getBuffer();
                            for (int h = 0; h < glyph.height; h++) {
                                int idx = h * bitmap.getPitch();
                                for (int w = 0; w < glyph.width + glyph.xoffset; w++) {
                                    pixmap.drawPixel(w, h, ((buf.get((w / 8) + idx) >>> (7 - (w % 8))) & 1) == 1 ? Color.WHITE.toIntBits() : Color.CLEAR.toIntBits());
                                }
                            }
                        }
                        String name = packPrefix + c2;
                        Rectangle rect = packer.pack(name, pixmap);
                        int pIndex = packer.getPageIndex(name);
                        if (pIndex == -1) {
                            throw new IllegalStateException("packer was not able to insert '" + name + "' into a page");
                        }
                        glyph.page = pIndex;
                        glyph.srcX = (int) rect.x;
                        glyph.srcY = (int) rect.y;
                        data.setGlyph(c2, glyph);
                        pixmap.dispose();
                    }
                }
                if (parameter.kerning) {
                    for (int i3 = 0; i3 < parameter.characters.length(); i3++) {
                        for (int j = 0; j < parameter.characters.length(); j++) {
                            char firstChar = parameter.characters.charAt(i3);
                            BitmapFont.Glyph first = data.getGlyph(firstChar);
                            if (first != null) {
                                char secondChar = parameter.characters.charAt(j);
                                if (!(data.getGlyph(secondChar) == null || (kerning = FreeType.getKerning(this.face, FreeType.getCharIndex(this.face, firstChar), FreeType.getCharIndex(this.face, secondChar), 0)) == 0)) {
                                    first.setKerning(secondChar, FreeType.toInt(kerning));
                                }
                            }
                        }
                    }
                }
                if (ownsAtlas) {
                    Array<PixmapPacker.Page> pages = packer.getPages();
                    data.regions = new TextureRegion[pages.size];
                    for (int i4 = 0; i4 < pages.size; i4++) {
                        PixmapPacker.Page p = pages.get(i4);
                        AnonymousClass1 r0 = new Texture(new PixmapTextureData(p.getPixmap(), p.getPixmap().getFormat(), parameter.genMipMaps, false, true)) {
                            public void dispose() {
                                super.dispose();
                                getTextureData().consumePixmap().dispose();
                            }
                        };
                        r0.setFilter(parameter.minFilter, parameter.magFilter);
                        data.regions[i4] = new TextureRegion(r0);
                    }
                }
                return data;
            }
            throw new GdxRuntimeException("No cap character found in font");
        }
        throw new GdxRuntimeException("Couldn't set size for font");
    }

    public void dispose() {
        FreeType.doneFace(this.face);
        FreeType.doneFreeType(this.library);
    }

    public static class FreeTypeBitmapFontData extends BitmapFont.BitmapFontData {
        TextureRegion[] regions;

        @Deprecated
        public TextureRegion getTextureRegion() {
            return this.regions[0];
        }

        public TextureRegion[] getTextureRegions() {
            return this.regions;
        }
    }
}
