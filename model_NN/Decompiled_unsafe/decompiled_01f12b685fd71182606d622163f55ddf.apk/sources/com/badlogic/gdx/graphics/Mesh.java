package com.badlogic.gdx.graphics;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.IndexArray;
import com.badlogic.gdx.graphics.glutils.IndexBufferObject;
import com.badlogic.gdx.graphics.glutils.IndexBufferObjectSubData;
import com.badlogic.gdx.graphics.glutils.IndexData;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexArray;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.badlogic.gdx.graphics.glutils.VertexBufferObjectSubData;
import com.badlogic.gdx.graphics.glutils.VertexBufferObjectWithVAO;
import com.badlogic.gdx.graphics.glutils.VertexData;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.kbz.esotericsoftware.spine.Animation;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.HashMap;
import java.util.Map;

public class Mesh implements Disposable {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$Mesh$VertexDataType;
    static final Map<Application, Array<Mesh>> meshes = new HashMap();
    boolean autoBind = true;
    final IndexData indices;
    final boolean isVertexArray;
    private final Vector3 tmpV = new Vector3();
    final VertexData vertices;

    public enum VertexDataType {
        VertexArray,
        VertexBufferObject,
        VertexBufferObjectSubData,
        VertexBufferObjectWithVAO
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$graphics$Mesh$VertexDataType() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$graphics$Mesh$VertexDataType;
        if (iArr == null) {
            iArr = new int[VertexDataType.values().length];
            try {
                iArr[VertexDataType.VertexArray.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VertexDataType.VertexBufferObject.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VertexDataType.VertexBufferObjectSubData.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[VertexDataType.VertexBufferObjectWithVAO.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$graphics$Mesh$VertexDataType = iArr;
        }
        return iArr;
    }

    protected Mesh(VertexData vertices2, IndexData indices2, boolean isVertexArray2) {
        this.vertices = vertices2;
        this.indices = indices2;
        this.isVertexArray = isVertexArray2;
        addManagedMesh(Gdx.app, this);
    }

    public Mesh(boolean isStatic, int maxVertices, int maxIndices, VertexAttribute... attributes) {
        this.vertices = makeVertexBuffer(isStatic, maxVertices, new VertexAttributes(attributes));
        this.indices = new IndexBufferObject(isStatic, maxIndices);
        this.isVertexArray = false;
        addManagedMesh(Gdx.app, this);
    }

    public Mesh(boolean isStatic, int maxVertices, int maxIndices, VertexAttributes attributes) {
        this.vertices = makeVertexBuffer(isStatic, maxVertices, attributes);
        this.indices = new IndexBufferObject(isStatic, maxIndices);
        this.isVertexArray = false;
        addManagedMesh(Gdx.app, this);
    }

    public Mesh(boolean staticVertices, boolean staticIndices, int maxVertices, int maxIndices, VertexAttributes attributes) {
        this.vertices = makeVertexBuffer(staticVertices, maxVertices, attributes);
        this.indices = new IndexBufferObject(staticIndices, maxIndices);
        this.isVertexArray = false;
        addManagedMesh(Gdx.app, this);
    }

    private VertexData makeVertexBuffer(boolean isStatic, int maxVertices, VertexAttributes vertexAttributes) {
        if (Gdx.gl30 != null) {
            return new VertexBufferObjectWithVAO(isStatic, maxVertices, vertexAttributes);
        }
        return new VertexBufferObject(isStatic, maxVertices, vertexAttributes);
    }

    public Mesh(VertexDataType type, boolean isStatic, int maxVertices, int maxIndices, VertexAttribute... attributes) {
        switch ($SWITCH_TABLE$com$badlogic$gdx$graphics$Mesh$VertexDataType()[type.ordinal()]) {
            case 2:
                this.vertices = new VertexBufferObject(isStatic, maxVertices, attributes);
                this.indices = new IndexBufferObject(isStatic, maxIndices);
                this.isVertexArray = false;
                break;
            case 3:
                this.vertices = new VertexBufferObjectSubData(isStatic, maxVertices, attributes);
                this.indices = new IndexBufferObjectSubData(isStatic, maxIndices);
                this.isVertexArray = false;
                break;
            case 4:
                this.vertices = new VertexBufferObjectWithVAO(isStatic, maxVertices, attributes);
                this.indices = new IndexBufferObjectSubData(isStatic, maxIndices);
                this.isVertexArray = false;
                break;
            default:
                this.vertices = new VertexArray(maxVertices, attributes);
                this.indices = new IndexArray(maxIndices);
                this.isVertexArray = true;
                break;
        }
        addManagedMesh(Gdx.app, this);
    }

    public Mesh setVertices(float[] vertices2) {
        this.vertices.setVertices(vertices2, 0, vertices2.length);
        return this;
    }

    public Mesh setVertices(float[] vertices2, int offset, int count) {
        this.vertices.setVertices(vertices2, offset, count);
        return this;
    }

    public Mesh updateVertices(int targetOffset, float[] source) {
        return updateVertices(targetOffset, source, 0, source.length);
    }

    public Mesh updateVertices(int targetOffset, float[] source, int sourceOffset, int count) {
        this.vertices.updateVertices(targetOffset, source, sourceOffset, count);
        return this;
    }

    public float[] getVertices(float[] vertices2) {
        return getVertices(0, -1, vertices2);
    }

    public float[] getVertices(int srcOffset, float[] vertices2) {
        return getVertices(srcOffset, -1, vertices2);
    }

    public float[] getVertices(int srcOffset, int count, float[] vertices2) {
        return getVertices(srcOffset, count, vertices2, 0);
    }

    public float[] getVertices(int srcOffset, int count, float[] vertices2, int destOffset) {
        int max = (getNumVertices() * getVertexSize()) / 4;
        if (count == -1 && (count = max - srcOffset) > vertices2.length - destOffset) {
            count = vertices2.length - destOffset;
        }
        if (srcOffset < 0 || count <= 0 || srcOffset + count > max || destOffset < 0 || destOffset >= vertices2.length) {
            throw new IndexOutOfBoundsException();
        } else if (vertices2.length - destOffset < count) {
            throw new IllegalArgumentException("not enough room in vertices array, has " + vertices2.length + " floats, needs " + count);
        } else {
            int pos = getVerticesBuffer().position();
            getVerticesBuffer().position(srcOffset);
            getVerticesBuffer().get(vertices2, destOffset, count);
            getVerticesBuffer().position(pos);
            return vertices2;
        }
    }

    public Mesh setIndices(short[] indices2) {
        this.indices.setIndices(indices2, 0, indices2.length);
        return this;
    }

    public Mesh setIndices(short[] indices2, int offset, int count) {
        this.indices.setIndices(indices2, offset, count);
        return this;
    }

    public void getIndices(short[] indices2) {
        getIndices(indices2, 0);
    }

    public void getIndices(short[] indices2, int destOffset) {
        getIndices(0, indices2, destOffset);
    }

    public void getIndices(int srcOffset, short[] indices2, int destOffset) {
        getIndices(srcOffset, -1, indices2, destOffset);
    }

    public void getIndices(int srcOffset, int count, short[] indices2, int destOffset) {
        int max = getNumIndices();
        if (count < 0) {
            count = max - srcOffset;
        }
        if (srcOffset < 0 || srcOffset >= max || srcOffset + count > max) {
            throw new IllegalArgumentException("Invalid range specified, offset: " + srcOffset + ", count: " + count + ", max: " + max);
        } else if (indices2.length - destOffset < count) {
            throw new IllegalArgumentException("not enough room in indices array, has " + indices2.length + " shorts, needs " + count);
        } else {
            int pos = getIndicesBuffer().position();
            getIndicesBuffer().position(srcOffset);
            getIndicesBuffer().get(indices2, destOffset, count);
            getIndicesBuffer().position(pos);
        }
    }

    public int getNumIndices() {
        return this.indices.getNumIndices();
    }

    public int getNumVertices() {
        return this.vertices.getNumVertices();
    }

    public int getMaxVertices() {
        return this.vertices.getNumMaxVertices();
    }

    public int getMaxIndices() {
        return this.indices.getNumMaxIndices();
    }

    public int getVertexSize() {
        return this.vertices.getAttributes().vertexSize;
    }

    public void setAutoBind(boolean autoBind2) {
        this.autoBind = autoBind2;
    }

    public void bind(ShaderProgram shader) {
        bind(shader, null);
    }

    public void bind(ShaderProgram shader, int[] locations) {
        this.vertices.bind(shader, locations);
        if (this.indices.getNumIndices() > 0) {
            this.indices.bind();
        }
    }

    public void unbind(ShaderProgram shader) {
        unbind(shader, null);
    }

    public void unbind(ShaderProgram shader, int[] locations) {
        this.vertices.unbind(shader, locations);
        if (this.indices.getNumIndices() > 0) {
            this.indices.unbind();
        }
    }

    public void render(ShaderProgram shader, int primitiveType) {
        render(shader, primitiveType, 0, this.indices.getNumMaxIndices() > 0 ? getNumIndices() : getNumVertices(), this.autoBind);
    }

    public void render(ShaderProgram shader, int primitiveType, int offset, int count) {
        render(shader, primitiveType, offset, count, this.autoBind);
    }

    public void render(ShaderProgram shader, int primitiveType, int offset, int count, boolean autoBind2) {
        if (count != 0) {
            if (autoBind2) {
                bind(shader);
            }
            if (this.isVertexArray) {
                if (this.indices.getNumIndices() > 0) {
                    ShortBuffer buffer = this.indices.getBuffer();
                    int oldPosition = buffer.position();
                    int oldLimit = buffer.limit();
                    buffer.position(offset);
                    buffer.limit(offset + count);
                    Gdx.gl20.glDrawElements(primitiveType, count, (int) GL20.GL_UNSIGNED_SHORT, buffer);
                    buffer.position(oldPosition);
                    buffer.limit(oldLimit);
                } else {
                    Gdx.gl20.glDrawArrays(primitiveType, offset, count);
                }
            } else if (this.indices.getNumIndices() > 0) {
                Gdx.gl20.glDrawElements(primitiveType, count, (int) GL20.GL_UNSIGNED_SHORT, offset * 2);
            } else {
                Gdx.gl20.glDrawArrays(primitiveType, offset, count);
            }
            if (autoBind2) {
                unbind(shader);
            }
        }
    }

    public void dispose() {
        if (meshes.get(Gdx.app) != null) {
            meshes.get(Gdx.app).removeValue(this, true);
        }
        this.vertices.dispose();
        this.indices.dispose();
    }

    public VertexAttribute getVertexAttribute(int usage) {
        VertexAttributes attributes = this.vertices.getAttributes();
        int len = attributes.size();
        for (int i = 0; i < len; i++) {
            if (attributes.get(i).usage == usage) {
                return attributes.get(i);
            }
        }
        return null;
    }

    public VertexAttributes getVertexAttributes() {
        return this.vertices.getAttributes();
    }

    public FloatBuffer getVerticesBuffer() {
        return this.vertices.getBuffer();
    }

    public BoundingBox calculateBoundingBox() {
        BoundingBox bbox = new BoundingBox();
        calculateBoundingBox(bbox);
        return bbox;
    }

    public void calculateBoundingBox(BoundingBox bbox) {
        int numVertices = getNumVertices();
        if (numVertices == 0) {
            throw new GdxRuntimeException("No vertices defined");
        }
        FloatBuffer verts = this.vertices.getBuffer();
        bbox.inf();
        VertexAttribute posAttrib = getVertexAttribute(1);
        int offset = posAttrib.offset / 4;
        int vertexSize = this.vertices.getAttributes().vertexSize / 4;
        int idx = offset;
        switch (posAttrib.numComponents) {
            case 1:
                for (int i = 0; i < numVertices; i++) {
                    bbox.ext(verts.get(idx), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    idx += vertexSize;
                }
                return;
            case 2:
                for (int i2 = 0; i2 < numVertices; i2++) {
                    bbox.ext(verts.get(idx), verts.get(idx + 1), Animation.CurveTimeline.LINEAR);
                    idx += vertexSize;
                }
                return;
            case 3:
                for (int i3 = 0; i3 < numVertices; i3++) {
                    bbox.ext(verts.get(idx), verts.get(idx + 1), verts.get(idx + 2));
                    idx += vertexSize;
                }
                return;
            default:
                return;
        }
    }

    public BoundingBox calculateBoundingBox(BoundingBox out, int offset, int count) {
        return extendBoundingBox(out.inf(), offset, count);
    }

    public BoundingBox calculateBoundingBox(BoundingBox out, int offset, int count, Matrix4 transform) {
        return extendBoundingBox(out.inf(), offset, count, transform);
    }

    public BoundingBox extendBoundingBox(BoundingBox out, int offset, int count) {
        return extendBoundingBox(out, offset, count, null);
    }

    public BoundingBox extendBoundingBox(BoundingBox out, int offset, int count, Matrix4 transform) {
        int numIndices = getNumIndices();
        if (offset < 0 || count < 1 || offset + count > numIndices) {
            throw new GdxRuntimeException("Not enough indices ( offset=" + offset + ", count=" + count + ", max=" + numIndices + " )");
        }
        FloatBuffer verts = this.vertices.getBuffer();
        ShortBuffer index = this.indices.getBuffer();
        VertexAttribute posAttrib = getVertexAttribute(1);
        int posoff = posAttrib.offset / 4;
        int vertexSize = this.vertices.getAttributes().vertexSize / 4;
        int end = offset + count;
        switch (posAttrib.numComponents) {
            case 1:
                for (int i = offset; i < end; i++) {
                    this.tmpV.set(verts.get((index.get(i) * vertexSize) + posoff), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    if (transform != null) {
                        this.tmpV.mul(transform);
                    }
                    out.ext(this.tmpV);
                }
                break;
            case 2:
                for (int i2 = offset; i2 < end; i2++) {
                    int idx = (index.get(i2) * vertexSize) + posoff;
                    this.tmpV.set(verts.get(idx), verts.get(idx + 1), Animation.CurveTimeline.LINEAR);
                    if (transform != null) {
                        this.tmpV.mul(transform);
                    }
                    out.ext(this.tmpV);
                }
                break;
            case 3:
                for (int i3 = offset; i3 < end; i3++) {
                    int idx2 = (index.get(i3) * vertexSize) + posoff;
                    this.tmpV.set(verts.get(idx2), verts.get(idx2 + 1), verts.get(idx2 + 2));
                    if (transform != null) {
                        this.tmpV.mul(transform);
                    }
                    out.ext(this.tmpV);
                }
                break;
        }
        return out;
    }

    public float calculateRadiusSquared(float centerX, float centerY, float centerZ, int offset, int count, Matrix4 transform) {
        int numIndices = getNumIndices();
        if (offset < 0 || count < 1 || offset + count > numIndices) {
            throw new GdxRuntimeException("Not enough indices");
        }
        FloatBuffer verts = this.vertices.getBuffer();
        ShortBuffer index = this.indices.getBuffer();
        VertexAttribute posAttrib = getVertexAttribute(1);
        int posoff = posAttrib.offset / 4;
        int vertexSize = this.vertices.getAttributes().vertexSize / 4;
        int end = offset + count;
        float result = Animation.CurveTimeline.LINEAR;
        switch (posAttrib.numComponents) {
            case 1:
                for (int i = offset; i < end; i++) {
                    this.tmpV.set(verts.get((index.get(i) * vertexSize) + posoff), Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    if (transform != null) {
                        this.tmpV.mul(transform);
                    }
                    float r = this.tmpV.sub(centerX, centerY, centerZ).len2();
                    if (r > result) {
                        result = r;
                    }
                }
                break;
            case 2:
                for (int i2 = offset; i2 < end; i2++) {
                    int idx = (index.get(i2) * vertexSize) + posoff;
                    this.tmpV.set(verts.get(idx), verts.get(idx + 1), Animation.CurveTimeline.LINEAR);
                    if (transform != null) {
                        this.tmpV.mul(transform);
                    }
                    float r2 = this.tmpV.sub(centerX, centerY, centerZ).len2();
                    if (r2 > result) {
                        result = r2;
                    }
                }
                break;
            case 3:
                for (int i3 = offset; i3 < end; i3++) {
                    int idx2 = (index.get(i3) * vertexSize) + posoff;
                    this.tmpV.set(verts.get(idx2), verts.get(idx2 + 1), verts.get(idx2 + 2));
                    if (transform != null) {
                        this.tmpV.mul(transform);
                    }
                    float r3 = this.tmpV.sub(centerX, centerY, centerZ).len2();
                    if (r3 > result) {
                        result = r3;
                    }
                }
                break;
        }
        return result;
    }

    public float calculateRadius(float centerX, float centerY, float centerZ, int offset, int count, Matrix4 transform) {
        return (float) Math.sqrt((double) calculateRadiusSquared(centerX, centerY, centerZ, offset, count, transform));
    }

    public float calculateRadius(Vector3 center, int offset, int count, Matrix4 transform) {
        return calculateRadius(center.x, center.y, center.z, offset, count, transform);
    }

    public float calculateRadius(float centerX, float centerY, float centerZ, int offset, int count) {
        return calculateRadius(centerX, centerY, centerZ, offset, count, null);
    }

    public float calculateRadius(Vector3 center, int offset, int count) {
        return calculateRadius(center.x, center.y, center.z, offset, count, null);
    }

    public float calculateRadius(float centerX, float centerY, float centerZ) {
        return calculateRadius(centerX, centerY, centerZ, 0, getNumIndices(), null);
    }

    public float calculateRadius(Vector3 center) {
        return calculateRadius(center.x, center.y, center.z, 0, getNumIndices(), null);
    }

    public ShortBuffer getIndicesBuffer() {
        return this.indices.getBuffer();
    }

    private static void addManagedMesh(Application app, Mesh mesh) {
        Array<Mesh> managedResources = meshes.get(app);
        if (managedResources == null) {
            managedResources = new Array<>();
        }
        managedResources.add(mesh);
        meshes.put(app, managedResources);
    }

    public static void invalidateAllMeshes(Application app) {
        Array<Mesh> meshesArray = meshes.get(app);
        if (meshesArray != null) {
            for (int i = 0; i < meshesArray.size; i++) {
                ((Mesh) meshesArray.get(i)).vertices.invalidate();
                ((Mesh) meshesArray.get(i)).indices.invalidate();
            }
        }
    }

    public static void clearAllMeshes(Application app) {
        meshes.remove(app);
    }

    public static String getManagedStatus() {
        StringBuilder builder = new StringBuilder();
        builder.append("Managed meshes/app: { ");
        for (Application app : meshes.keySet()) {
            builder.append(meshes.get(app).size);
            builder.append(" ");
        }
        builder.append("}");
        return builder.toString();
    }

    public void scale(float scaleX, float scaleY, float scaleZ) {
        VertexAttribute posAttr = getVertexAttribute(1);
        int offset = posAttr.offset / 4;
        int numComponents = posAttr.numComponents;
        int numVertices = getNumVertices();
        int vertexSize = getVertexSize() / 4;
        float[] vertices2 = new float[(numVertices * vertexSize)];
        getVertices(vertices2);
        int idx = offset;
        switch (numComponents) {
            case 1:
                for (int i = 0; i < numVertices; i++) {
                    vertices2[idx] = vertices2[idx] * scaleX;
                    idx += vertexSize;
                }
                break;
            case 2:
                for (int i2 = 0; i2 < numVertices; i2++) {
                    vertices2[idx] = vertices2[idx] * scaleX;
                    int i3 = idx + 1;
                    vertices2[i3] = vertices2[i3] * scaleY;
                    idx += vertexSize;
                }
                break;
            case 3:
                for (int i4 = 0; i4 < numVertices; i4++) {
                    vertices2[idx] = vertices2[idx] * scaleX;
                    int i5 = idx + 1;
                    vertices2[i5] = vertices2[i5] * scaleY;
                    int i6 = idx + 2;
                    vertices2[i6] = vertices2[i6] * scaleZ;
                    idx += vertexSize;
                }
                break;
        }
        setVertices(vertices2);
    }

    public void transform(Matrix4 matrix) {
        transform(matrix, 0, getNumVertices());
    }

    public void transform(Matrix4 matrix, int start, int count) {
        VertexAttribute posAttr = getVertexAttribute(1);
        int posOffset = posAttr.offset / 4;
        int stride = getVertexSize() / 4;
        int numComponents = posAttr.numComponents;
        int numVertices = getNumVertices();
        float[] vertices2 = new float[(count * stride)];
        getVertices(start * stride, count * stride, vertices2);
        transform(matrix, vertices2, stride, posOffset, numComponents, 0, count);
        updateVertices(start * stride, vertices2);
    }

    public static void transform(Matrix4 matrix, float[] vertices2, int vertexSize, int offset, int dimensions, int start, int count) {
        if (offset < 0 || dimensions < 1 || offset + dimensions > vertexSize) {
            throw new IndexOutOfBoundsException();
        } else if (start < 0 || count < 1 || (start + count) * vertexSize > vertices2.length) {
            throw new IndexOutOfBoundsException("start = " + start + ", count = " + count + ", vertexSize = " + vertexSize + ", length = " + vertices2.length);
        } else {
            Vector3 tmp = new Vector3();
            int idx = offset + (start * vertexSize);
            switch (dimensions) {
                case 1:
                    for (int i = 0; i < count; i++) {
                        tmp.set(vertices2[idx], Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR).mul(matrix);
                        vertices2[idx] = tmp.x;
                        idx += vertexSize;
                    }
                    return;
                case 2:
                    for (int i2 = 0; i2 < count; i2++) {
                        tmp.set(vertices2[idx], vertices2[idx + 1], Animation.CurveTimeline.LINEAR).mul(matrix);
                        vertices2[idx] = tmp.x;
                        vertices2[idx + 1] = tmp.y;
                        idx += vertexSize;
                    }
                    return;
                case 3:
                    for (int i3 = 0; i3 < count; i3++) {
                        tmp.set(vertices2[idx], vertices2[idx + 1], vertices2[idx + 2]).mul(matrix);
                        vertices2[idx] = tmp.x;
                        vertices2[idx + 1] = tmp.y;
                        vertices2[idx + 2] = tmp.z;
                        idx += vertexSize;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void transformUV(Matrix3 matrix) {
        transformUV(matrix, 0, getNumVertices());
    }

    /* access modifiers changed from: protected */
    public void transformUV(Matrix3 matrix, int start, int count) {
        int offset = getVertexAttribute(16).offset / 4;
        int vertexSize = getVertexSize() / 4;
        float[] vertices2 = new float[(getNumVertices() * vertexSize)];
        getVertices(0, vertices2.length, vertices2);
        transformUV(matrix, vertices2, vertexSize, offset, start, count);
        setVertices(vertices2, 0, vertices2.length);
    }

    public static void transformUV(Matrix3 matrix, float[] vertices2, int vertexSize, int offset, int start, int count) {
        if (start < 0 || count < 1 || (start + count) * vertexSize > vertices2.length) {
            throw new IndexOutOfBoundsException("start = " + start + ", count = " + count + ", vertexSize = " + vertexSize + ", length = " + vertices2.length);
        }
        Vector2 tmp = new Vector2();
        int idx = offset + (start * vertexSize);
        for (int i = 0; i < count; i++) {
            tmp.set(vertices2[idx], vertices2[idx + 1]).mul(matrix);
            vertices2[idx] = tmp.x;
            vertices2[idx + 1] = tmp.y;
            idx += vertexSize;
        }
    }

    public Mesh copy(boolean isStatic, boolean removeDuplicates, int[] usage) {
        Mesh result;
        int vertexSize = getVertexSize() / 4;
        int numVertices = getNumVertices();
        float[] vertices2 = new float[(numVertices * vertexSize)];
        getVertices(0, vertices2.length, vertices2);
        short[] checks = null;
        VertexAttribute[] attrs = null;
        short newVertexSize = 0;
        if (usage != null) {
            int size = 0;
            int as = 0;
            for (int i = 0; i < usage.length; i++) {
                if (getVertexAttribute(usage[i]) != null) {
                    size += getVertexAttribute(usage[i]).numComponents;
                    as++;
                }
            }
            if (size > 0) {
                attrs = new VertexAttribute[as];
                checks = new short[size];
                int idx = -1;
                int ai = -1;
                for (int vertexAttribute : usage) {
                    VertexAttribute a = getVertexAttribute(vertexAttribute);
                    if (a != null) {
                        for (int j = 0; j < a.numComponents; j++) {
                            idx++;
                            checks[idx] = (short) (a.offset + j);
                        }
                        ai++;
                        attrs[ai] = new VertexAttribute(a.usage, a.numComponents, a.alias);
                        newVertexSize += a.numComponents;
                    }
                }
            }
        }
        if (checks == null) {
            checks = new short[vertexSize];
            for (short i2 = 0; i2 < vertexSize; i2 = (short) (i2 + 1)) {
                checks[i2] = i2;
            }
            newVertexSize = vertexSize;
        }
        int numIndices = getNumIndices();
        short[] indices2 = null;
        if (numIndices > 0) {
            indices2 = new short[numIndices];
            getIndices(indices2);
            if (removeDuplicates || newVertexSize != vertexSize) {
                float[] tmp = new float[vertices2.length];
                int size2 = 0;
                for (int i3 = 0; i3 < numIndices; i3++) {
                    int idx1 = indices2[i3] * vertexSize;
                    short newIndex = -1;
                    if (removeDuplicates) {
                        for (short j2 = 0; j2 < size2 && newIndex < 0; j2 = (short) (j2 + 1)) {
                            int idx2 = j2 * newVertexSize;
                            boolean found = true;
                            for (int k = 0; k < checks.length && found; k++) {
                                if (tmp[idx2 + k] != vertices2[checks[k] + idx1]) {
                                    found = false;
                                }
                            }
                            if (found) {
                                newIndex = j2;
                            }
                        }
                    }
                    if (newIndex > 0) {
                        indices2[i3] = newIndex;
                    } else {
                        int idx3 = size2 * newVertexSize;
                        for (int j3 = 0; j3 < checks.length; j3++) {
                            tmp[idx3 + j3] = vertices2[checks[j3] + idx1];
                        }
                        indices2[i3] = (short) size2;
                        size2++;
                    }
                }
                vertices2 = tmp;
                numVertices = size2;
            }
        }
        if (attrs == null) {
            result = new Mesh(isStatic, numVertices, indices2 == null ? 0 : indices2.length, getVertexAttributes());
        } else {
            result = new Mesh(isStatic, numVertices, indices2 == null ? 0 : indices2.length, attrs);
        }
        result.setVertices(vertices2, 0, numVertices * newVertexSize);
        result.setIndices(indices2);
        return result;
    }

    public Mesh copy(boolean isStatic) {
        return copy(isStatic, false, null);
    }
}
