package com.badlogic.gdx.graphics.g3d.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

public interface TextureProvider {
    Texture load(String str);

    public static class FileTextureProvider implements TextureProvider {
        public Texture load(String fileName) {
            Texture result = new Texture(Gdx.files.internal(fileName));
            result.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            result.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
            return result;
        }
    }

    public static class AssetTextureProvider implements TextureProvider {
        public final AssetManager assetManager;

        public AssetTextureProvider(AssetManager assetManager2) {
            this.assetManager = assetManager2;
        }

        public Texture load(String fileName) {
            return (Texture) this.assetManager.get(fileName, Texture.class);
        }
    }
}
