package com.badlogic.gdx.graphics;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.CubemapLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.glutils.FacedCubemapData;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Cubemap extends GLTexture {
    private static AssetManager assetManager;
    static final Map<Application, Array<Cubemap>> managedCubemaps = new HashMap();
    protected CubemapData data;

    public enum CubemapSide {
        PositiveX(0, GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_X),
        NegativeX(1, GL20.GL_TEXTURE_CUBE_MAP_NEGATIVE_X),
        PositiveY(2, GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_Y),
        NegativeY(3, GL20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y),
        PositiveZ(4, GL20.GL_TEXTURE_CUBE_MAP_POSITIVE_Z),
        NegativeZ(5, GL20.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
        
        public final int glEnum;
        public final int index;

        private CubemapSide(int index2, int glEnum2) {
            this.index = index2;
            this.glEnum = glEnum2;
        }

        public int getGLEnum() {
            return this.glEnum;
        }
    }

    public Cubemap(CubemapData data2) {
        super(GL20.GL_TEXTURE_CUBE_MAP);
        this.data = data2;
        load(data2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    public Cubemap(FileHandle positiveX, FileHandle negativeX, FileHandle positiveY, FileHandle negativeY, FileHandle positiveZ, FileHandle negativeZ) {
        this(positiveX, negativeX, positiveY, negativeY, positiveZ, negativeZ, false);
    }

    public Cubemap(FileHandle positiveX, FileHandle negativeX, FileHandle positiveY, FileHandle negativeY, FileHandle positiveZ, FileHandle negativeZ, boolean useMipMaps) {
        this(TextureData.Factory.loadFromFile(positiveX, useMipMaps), TextureData.Factory.loadFromFile(negativeX, useMipMaps), TextureData.Factory.loadFromFile(positiveY, useMipMaps), TextureData.Factory.loadFromFile(negativeY, useMipMaps), TextureData.Factory.loadFromFile(positiveZ, useMipMaps), TextureData.Factory.loadFromFile(negativeZ, useMipMaps));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void
     arg types: [com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, int]
     candidates:
      com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void */
    public Cubemap(Pixmap positiveX, Pixmap negativeX, Pixmap positiveY, Pixmap negativeY, Pixmap positiveZ, Pixmap negativeZ) {
        this(positiveX, negativeX, positiveY, negativeY, positiveZ, negativeZ, false);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Cubemap(com.badlogic.gdx.graphics.Pixmap r9, com.badlogic.gdx.graphics.Pixmap r10, com.badlogic.gdx.graphics.Pixmap r11, com.badlogic.gdx.graphics.Pixmap r12, com.badlogic.gdx.graphics.Pixmap r13, com.badlogic.gdx.graphics.Pixmap r14, boolean r15) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            if (r9 != 0) goto L_0x0018
            r1 = r6
        L_0x0005:
            if (r10 != 0) goto L_0x001e
            r2 = r6
        L_0x0008:
            if (r11 != 0) goto L_0x0024
            r3 = r6
        L_0x000b:
            if (r12 != 0) goto L_0x002a
            r4 = r6
        L_0x000e:
            if (r13 != 0) goto L_0x0030
            r5 = r6
        L_0x0011:
            if (r14 != 0) goto L_0x0036
        L_0x0013:
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        L_0x0018:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r1 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r1.<init>(r9, r6, r15, r7)
            goto L_0x0005
        L_0x001e:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r2 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r2.<init>(r10, r6, r15, r7)
            goto L_0x0008
        L_0x0024:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r3 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r3.<init>(r11, r6, r15, r7)
            goto L_0x000b
        L_0x002a:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r4 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r4.<init>(r12, r6, r15, r7)
            goto L_0x000e
        L_0x0030:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r5 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r5.<init>(r13, r6, r15, r7)
            goto L_0x0011
        L_0x0036:
            com.badlogic.gdx.graphics.glutils.PixmapTextureData r0 = new com.badlogic.gdx.graphics.glutils.PixmapTextureData
            r0.<init>(r14, r6, r15, r7)
            r6 = r0
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.Cubemap.<init>(com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, com.badlogic.gdx.graphics.Pixmap, boolean):void");
    }

    public Cubemap(int width, int height, int depth, Pixmap.Format format) {
        this(new PixmapTextureData(new Pixmap(depth, height, format), null, false, true), new PixmapTextureData(new Pixmap(depth, height, format), null, false, true), new PixmapTextureData(new Pixmap(width, depth, format), null, false, true), new PixmapTextureData(new Pixmap(width, depth, format), null, false, true), new PixmapTextureData(new Pixmap(width, height, format), null, false, true), new PixmapTextureData(new Pixmap(width, height, format), null, false, true));
    }

    public Cubemap(TextureData positiveX, TextureData negativeX, TextureData positiveY, TextureData negativeY, TextureData positiveZ, TextureData negativeZ) {
        super(GL20.GL_TEXTURE_CUBE_MAP);
        this.minFilter = Texture.TextureFilter.Nearest;
        this.magFilter = Texture.TextureFilter.Nearest;
        this.uWrap = Texture.TextureWrap.ClampToEdge;
        this.vWrap = Texture.TextureWrap.ClampToEdge;
        this.data = new FacedCubemapData(positiveX, negativeX, positiveY, negativeY, positiveZ, negativeZ);
        load(this.data);
    }

    public void load(CubemapData data2) {
        if (!data2.isPrepared()) {
            data2.prepare();
        }
        bind();
        unsafeSetFilter(this.minFilter, this.magFilter, true);
        unsafeSetWrap(this.uWrap, this.vWrap, true);
        data2.consumeCubemapData();
        Gdx.gl.glBindTexture(this.glTarget, 0);
    }

    public CubemapData getCubemapData() {
        return this.data;
    }

    public boolean isManaged() {
        return this.data.isManaged();
    }

    /* access modifiers changed from: protected */
    public void reload() {
        if (!isManaged()) {
            throw new GdxRuntimeException("Tried to reload an unmanaged Cubemap");
        }
        this.glHandle = Gdx.gl.glGenTexture();
        load(this.data);
    }

    public int getWidth() {
        return this.data.getWidth();
    }

    public int getHeight() {
        return this.data.getHeight();
    }

    public int getDepth() {
        return 0;
    }

    public void dispose() {
        if (this.glHandle != 0) {
            delete();
            if (this.data.isManaged() && managedCubemaps.get(Gdx.app) != null) {
                managedCubemaps.get(Gdx.app).removeValue(this, true);
            }
        }
    }

    private static void addManagedCubemap(Application app, Cubemap cubemap) {
        Array<Cubemap> managedCubemapArray = managedCubemaps.get(app);
        if (managedCubemapArray == null) {
            managedCubemapArray = new Array<>();
        }
        managedCubemapArray.add(cubemap);
        managedCubemaps.put(app, managedCubemapArray);
    }

    public static void clearAllCubemaps(Application app) {
        managedCubemaps.remove(app);
    }

    public static void invalidateAllCubemaps(Application app) {
        Array<Cubemap> managedCubemapArray = managedCubemaps.get(app);
        if (managedCubemapArray != null) {
            if (assetManager == null) {
                for (int i = 0; i < managedCubemapArray.size; i++) {
                    ((Cubemap) managedCubemapArray.get(i)).reload();
                }
                return;
            }
            assetManager.finishLoading();
            Array<Cubemap> cubemaps = new Array<>(managedCubemapArray);
            Iterator it = cubemaps.iterator();
            while (it.hasNext()) {
                Cubemap cubemap = (Cubemap) it.next();
                String fileName = assetManager.getAssetFileName(cubemap);
                if (fileName == null) {
                    cubemap.reload();
                } else {
                    final int refCount = assetManager.getReferenceCount(fileName);
                    assetManager.setReferenceCount(fileName, 0);
                    cubemap.glHandle = 0;
                    CubemapLoader.CubemapParameter params = new CubemapLoader.CubemapParameter();
                    params.cubemapData = cubemap.getCubemapData();
                    params.minFilter = cubemap.getMinFilter();
                    params.magFilter = cubemap.getMagFilter();
                    params.wrapU = cubemap.getUWrap();
                    params.wrapV = cubemap.getVWrap();
                    params.cubemap = cubemap;
                    params.loadedCallback = new AssetLoaderParameters.LoadedCallback() {
                        public void finishedLoading(AssetManager assetManager, String fileName, Class type) {
                            assetManager.setReferenceCount(fileName, refCount);
                        }
                    };
                    assetManager.unload(fileName);
                    cubemap.glHandle = Gdx.gl.glGenTexture();
                    assetManager.load(fileName, Cubemap.class, params);
                }
            }
            managedCubemapArray.clear();
            managedCubemapArray.addAll(cubemaps);
        }
    }

    public static void setAssetManager(AssetManager manager) {
        assetManager = manager;
    }

    public static String getManagedStatus() {
        StringBuilder builder = new StringBuilder();
        builder.append("Managed cubemap/app: { ");
        for (Application app : managedCubemaps.keySet()) {
            builder.append(managedCubemaps.get(app).size);
            builder.append(" ");
        }
        builder.append("}");
        return builder.toString();
    }

    public static int getNumManagedCubemaps() {
        return managedCubemaps.get(Gdx.app).size;
    }
}
