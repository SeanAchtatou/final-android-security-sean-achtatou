package com.badlogic.gdx.graphics.g2d;

public class PolygonRegion {
    final TextureRegion region;
    final float[] textureCoords;
    final short[] triangles;
    final float[] vertices;

    public PolygonRegion(TextureRegion region2, float[] vertices2, short[] triangles2) {
        this.region = region2;
        this.vertices = vertices2;
        this.triangles = triangles2;
        float[] textureCoords2 = new float[vertices2.length];
        this.textureCoords = textureCoords2;
        float u = region2.u;
        float v = region2.v;
        float uvWidth = region2.u2 - u;
        float uvHeight = region2.v2 - v;
        int width = region2.regionWidth;
        int height = region2.regionHeight;
        int i = 0;
        int n = vertices2.length;
        while (i < n) {
            textureCoords2[i] = ((vertices2[i] / ((float) width)) * uvWidth) + u;
            int i2 = i + 1;
            textureCoords2[i2] = ((1.0f - (vertices2[i2] / ((float) height))) * uvHeight) + v;
            i = i2 + 1;
        }
    }

    public float[] getVertices() {
        return this.vertices;
    }

    public short[] getTriangles() {
        return this.triangles;
    }

    public float[] getTextureCoords() {
        return this.textureCoords;
    }

    public TextureRegion getRegion() {
        return this.region;
    }
}
