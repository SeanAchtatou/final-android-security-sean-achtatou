package com.badlogic.gdx.graphics.g3d.particles.values;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.kbz.esotericsoftware.spine.Animation;

public final class CylinderSpawnShapeValue extends PrimitiveSpawnShapeValue {
    public CylinderSpawnShapeValue(CylinderSpawnShapeValue cylinderSpawnShapeValue) {
        super(cylinderSpawnShapeValue);
        load(cylinderSpawnShapeValue);
    }

    public CylinderSpawnShapeValue() {
    }

    public void spawnAux(Vector3 vector, float percent) {
        float radiusX;
        float radiusZ;
        float width = this.spawnWidth + (this.spawnWidthDiff * this.spawnWidthValue.getScale(percent));
        float height = this.spawnHeight + (this.spawnHeightDiff * this.spawnHeightValue.getScale(percent));
        float depth = this.spawnDepth + (this.spawnDepthDiff * this.spawnDepthValue.getScale(percent));
        float ty = MathUtils.random(height) - (height / 2.0f);
        if (this.edges) {
            radiusX = width / 2.0f;
            radiusZ = depth / 2.0f;
        } else {
            radiusX = MathUtils.random(width) / 2.0f;
            radiusZ = MathUtils.random(depth) / 2.0f;
        }
        float spawnTheta = Animation.CurveTimeline.LINEAR;
        boolean isRadiusXZero = radiusX == Animation.CurveTimeline.LINEAR;
        boolean isRadiusZZero = radiusZ == Animation.CurveTimeline.LINEAR;
        if (!isRadiusXZero && !isRadiusZZero) {
            spawnTheta = MathUtils.random(360.0f);
        } else if (isRadiusXZero) {
            spawnTheta = (float) (MathUtils.random(1) == 0 ? -90 : 90);
        } else if (isRadiusZZero) {
            spawnTheta = (float) (MathUtils.random(1) == 0 ? 0 : 180);
        }
        vector.set(MathUtils.cosDeg(spawnTheta) * radiusX, ty, MathUtils.sinDeg(spawnTheta) * radiusZ);
    }

    public SpawnShapeValue copy() {
        return new CylinderSpawnShapeValue(this);
    }
}
