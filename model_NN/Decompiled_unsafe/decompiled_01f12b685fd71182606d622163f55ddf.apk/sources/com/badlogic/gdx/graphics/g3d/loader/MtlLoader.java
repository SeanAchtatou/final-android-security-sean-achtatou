package com.badlogic.gdx.graphics.g3d.loader;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.model.data.ModelMaterial;
import com.badlogic.gdx.graphics.g3d.model.data.ModelTexture;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

/* compiled from: ObjLoader */
class MtlLoader {
    public Array<ModelMaterial> materials = new Array<>();

    MtlLoader() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void load(FileHandle file) {
        String curMatName = "default";
        Color difcolor = Color.WHITE;
        Color speccolor = Color.WHITE;
        float opacity = 1.0f;
        float shininess = Animation.CurveTimeline.LINEAR;
        String texFilename = null;
        if (file != null && file.exists()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(file.read()), 4096);
            Color speccolor2 = speccolor;
            Color difcolor2 = difcolor;
            while (true) {
                try {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    if (line.length() > 0 && line.charAt(0) == 9) {
                        line = line.substring(1).trim();
                    }
                    String[] tokens = line.split("\\s+");
                    if (!(tokens[0].length() == 0 || tokens[0].charAt(0) == '#')) {
                        String key = tokens[0].toLowerCase();
                        if (key.equals("newmtl")) {
                            ModelMaterial mat = new ModelMaterial();
                            mat.id = curMatName;
                            mat.diffuse = new Color(difcolor2);
                            mat.specular = new Color(speccolor2);
                            mat.opacity = opacity;
                            mat.shininess = shininess;
                            if (texFilename != null) {
                                ModelTexture tex = new ModelTexture();
                                tex.usage = 2;
                                tex.fileName = new String(texFilename);
                                if (mat.textures == null) {
                                    mat.textures = new Array<>(1);
                                }
                                mat.textures.add(tex);
                            }
                            this.materials.add(mat);
                            if (tokens.length > 1) {
                                curMatName = tokens[1].replace('.', '_');
                            } else {
                                curMatName = "default";
                            }
                            Color difcolor3 = Color.WHITE;
                            try {
                                Color speccolor3 = Color.WHITE;
                                opacity = 1.0f;
                                shininess = Animation.CurveTimeline.LINEAR;
                                speccolor2 = speccolor3;
                                difcolor2 = difcolor3;
                            } catch (IOException e) {
                                return;
                            }
                        } else if (key.equals("kd") || key.equals("ks")) {
                            float r = Float.parseFloat(tokens[1]);
                            float g = Float.parseFloat(tokens[2]);
                            float b = Float.parseFloat(tokens[3]);
                            float a = 1.0f;
                            if (tokens.length > 4) {
                                a = Float.parseFloat(tokens[4]);
                            }
                            if (tokens[0].toLowerCase().equals("kd")) {
                                Color difcolor4 = new Color();
                                difcolor4.set(r, g, b, a);
                                difcolor2 = difcolor4;
                            } else {
                                Color speccolor4 = new Color();
                                try {
                                    speccolor4.set(r, g, b, a);
                                    speccolor2 = speccolor4;
                                } catch (IOException e2) {
                                    return;
                                }
                            }
                        } else if (key.equals("tr") || key.equals("d")) {
                            opacity = Float.parseFloat(tokens[1]);
                        } else if (key.equals("ns")) {
                            shininess = Float.parseFloat(tokens[1]);
                        } else if (key.equals("map_kd")) {
                            texFilename = file.parent().child(tokens[1]).path();
                        }
                    }
                } catch (IOException e3) {
                    return;
                }
            }
            reader.close();
            ModelMaterial mat2 = new ModelMaterial();
            mat2.id = curMatName;
            mat2.diffuse = new Color(difcolor2);
            mat2.specular = new Color(speccolor2);
            mat2.opacity = opacity;
            mat2.shininess = shininess;
            if (texFilename != null) {
                ModelTexture tex2 = new ModelTexture();
                tex2.usage = 2;
                tex2.fileName = new String(texFilename);
                if (mat2.textures == null) {
                    mat2.textures = new Array<>(1);
                }
                mat2.textures.add(tex2);
            }
            this.materials.add(mat2);
        }
    }

    public ModelMaterial getMaterial(String name) {
        Iterator<ModelMaterial> it = this.materials.iterator();
        while (it.hasNext()) {
            ModelMaterial m = it.next();
            if (m.id.equals(name)) {
                return m;
            }
        }
        ModelMaterial mat = new ModelMaterial();
        mat.id = name;
        mat.diffuse = new Color(Color.WHITE);
        this.materials.add(mat);
        return mat;
    }
}
