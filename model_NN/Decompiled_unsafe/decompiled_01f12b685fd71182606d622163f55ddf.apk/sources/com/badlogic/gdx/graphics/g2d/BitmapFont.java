package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.StreamUtils;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BitmapFont implements Disposable {
    private static final int LOG2_PAGE_SIZE = 9;
    private static final int PAGES = 128;
    private static final int PAGE_SIZE = 512;
    private final BitmapFontCache cache;
    final BitmapFontData data;
    private boolean flipped;
    private boolean integer;
    private boolean ownsTexture;
    Array<TextureRegion> regions;

    public BitmapFont() {
        this(Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.fnt"), Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.png"), false, true);
    }

    public BitmapFont(boolean flip) {
        this(Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.fnt"), Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.png"), flip, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    public BitmapFont(FileHandle fontFile, TextureRegion region) {
        this(fontFile, region, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    public BitmapFont(FileHandle fontFile, TextureRegion region, boolean flip) {
        this(new BitmapFontData(fontFile, flip), region, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public BitmapFont(FileHandle fontFile) {
        this(fontFile, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, ?[OBJECT, ARRAY], int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    public BitmapFont(FileHandle fontFile, boolean flip) {
        this(new BitmapFontData(fontFile, flip), (TextureRegion) null, true);
    }

    public BitmapFont(FileHandle fontFile, FileHandle imageFile, boolean flip) {
        this(fontFile, imageFile, flip, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public BitmapFont(FileHandle fontFile, FileHandle imageFile, boolean flip, boolean integer2) {
        this(new BitmapFontData(fontFile, flip), new TextureRegion(new Texture(imageFile, false)), integer2);
        this.ownsTexture = true;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BitmapFont(com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData r3, com.badlogic.gdx.graphics.g2d.TextureRegion r4, boolean r5) {
        /*
            r2 = this;
            if (r4 == 0) goto L_0x0010
            r0 = 1
            com.badlogic.gdx.graphics.g2d.TextureRegion[] r0 = new com.badlogic.gdx.graphics.g2d.TextureRegion[r0]
            r1 = 0
            r0[r1] = r4
            com.badlogic.gdx.utils.Array r0 = com.badlogic.gdx.utils.Array.with(r0)
        L_0x000c:
            r2.<init>(r3, r0, r5)
            return
        L_0x0010:
            r0 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public BitmapFont(BitmapFontData data2, Array<TextureRegion> pageRegions, boolean integer2) {
        FileHandle file;
        if (pageRegions == null || pageRegions.size == 0) {
            int n = data2.imagePaths.length;
            this.regions = new Array<>(n);
            for (int i = 0; i < n; i++) {
                if (data2.fontFile == null) {
                    file = Gdx.files.internal(data2.imagePaths[i]);
                } else {
                    file = Gdx.files.getFileHandle(data2.imagePaths[i], data2.fontFile.type());
                }
                this.regions.add(new TextureRegion(new Texture(file, false)));
            }
            this.ownsTexture = true;
        } else {
            this.regions = pageRegions;
            this.ownsTexture = false;
        }
        this.cache = newFontCache();
        this.flipped = data2.flipped;
        this.data = data2;
        this.integer = integer2;
        load(data2);
    }

    private void load(BitmapFontData data2) {
        for (Glyph[] page : data2.glyphs) {
            if (page != null) {
                for (Glyph glyph : page) {
                    if (glyph != null) {
                        TextureRegion region = this.regions.get(glyph.page);
                        if (region == null) {
                            throw new IllegalArgumentException("BitmapFont texture region array cannot contain null elements.");
                        }
                        data2.setGlyphRegion(glyph, region);
                    }
                }
                continue;
            }
        }
    }

    public GlyphLayout draw(Batch batch, CharSequence str, float x, float y) {
        this.cache.clear();
        GlyphLayout layout = this.cache.addText(str, x, y);
        this.cache.draw(batch);
        return layout;
    }

    public GlyphLayout draw(Batch batch, CharSequence str, float x, float y, float targetWidth, int halign, boolean wrap) {
        this.cache.clear();
        GlyphLayout layout = this.cache.addText(str, x, y, targetWidth, halign, wrap);
        this.cache.draw(batch);
        return layout;
    }

    public GlyphLayout draw(Batch batch, CharSequence str, float x, float y, int start, int end, float targetWidth, int halign, boolean wrap) {
        this.cache.clear();
        GlyphLayout layout = this.cache.addText(str, x, y, start, end, targetWidth, halign, wrap);
        this.cache.draw(batch);
        return layout;
    }

    public void draw(Batch batch, GlyphLayout layout, float x, float y) {
        this.cache.clear();
        this.cache.addText(layout, x, y);
        this.cache.draw(batch);
    }

    public Color getColor() {
        return this.cache.getColor();
    }

    public void setColor(Color color) {
        this.cache.getColor().set(color);
    }

    public void setColor(float r, float g, float b, float a) {
        this.cache.getColor().set(r, g, b, a);
    }

    public float getScaleX() {
        return this.data.scaleX;
    }

    public float getScaleY() {
        return this.data.scaleY;
    }

    public TextureRegion getRegion() {
        return this.regions.first();
    }

    public Array<TextureRegion> getRegions() {
        return this.regions;
    }

    public TextureRegion getRegion(int index) {
        return this.regions.get(index);
    }

    public float getLineHeight() {
        return this.data.lineHeight;
    }

    public float getSpaceWidth() {
        return this.data.spaceWidth;
    }

    public float getXHeight() {
        return this.data.xHeight;
    }

    public float getCapHeight() {
        return this.data.capHeight;
    }

    public float getAscent() {
        return this.data.ascent;
    }

    public float getDescent() {
        return this.data.descent;
    }

    public boolean isFlipped() {
        return this.flipped;
    }

    public void dispose() {
        if (this.ownsTexture) {
            for (int i = 0; i < this.regions.size; i++) {
                this.regions.get(i).getTexture().dispose();
            }
        }
    }

    public void setFixedWidthGlyphs(CharSequence glyphs) {
        BitmapFontData data2 = this.data;
        int maxAdvance = 0;
        int end = glyphs.length();
        for (int index = 0; index < end; index++) {
            Glyph g = data2.getGlyph(glyphs.charAt(index));
            if (g != null && g.xadvance > maxAdvance) {
                maxAdvance = g.xadvance;
            }
        }
        int end2 = glyphs.length();
        for (int index2 = 0; index2 < end2; index2++) {
            Glyph g2 = data2.getGlyph(glyphs.charAt(index2));
            if (g2 != null) {
                g2.xoffset += (maxAdvance - g2.xadvance) / 2;
                g2.xadvance = maxAdvance;
                g2.kerning = null;
            }
        }
    }

    public void setUseIntegerPositions(boolean integer2) {
        this.integer = integer2;
        this.cache.setUseIntegerPositions(integer2);
    }

    public boolean usesIntegerPositions() {
        return this.integer;
    }

    public BitmapFontCache getCache() {
        return this.cache;
    }

    public BitmapFontData getData() {
        return this.data;
    }

    public boolean ownsTexture() {
        return this.ownsTexture;
    }

    public void setOwnsTexture(boolean ownsTexture2) {
        this.ownsTexture = ownsTexture2;
    }

    public BitmapFontCache newFontCache() {
        return new BitmapFontCache(this, this.integer);
    }

    public String toString() {
        if (this.data.fontFile != null) {
            return this.data.fontFile.nameWithoutExtension();
        }
        return super.toString();
    }

    public static class Glyph {
        public int height;
        public int id;
        public byte[][] kerning;
        public int page = 0;
        public int srcX;
        public int srcY;
        public float u;
        public float u2;
        public float v;
        public float v2;
        public int width;
        public int xadvance;
        public int xoffset;
        public int yoffset;

        public int getKerning(char ch) {
            byte[] page2;
            if (this.kerning == null || (page2 = this.kerning[ch >>> 9]) == null) {
                return 0;
            }
            return page2[ch & 511];
        }

        public void setKerning(int ch, int value) {
            if (this.kerning == null) {
                this.kerning = new byte[128][];
            }
            byte[] page2 = this.kerning[ch >>> 9];
            if (page2 == null) {
                page2 = new byte[512];
                this.kerning[ch >>> 9] = page2;
            }
            page2[ch & PAK_ASSETS.IMG_MORE004] = (byte) value;
        }

        public String toString() {
            return Character.toString((char) this.id);
        }
    }

    static int indexOf(CharSequence text, char ch, int start) {
        int n = text.length();
        while (start < n) {
            if (text.charAt(start) == ch) {
                return start;
            }
            start++;
        }
        return n;
    }

    public static class BitmapFontData {
        public float ascent;
        public char[] breakChars;
        public char[] capChars = {'M', 'N', 'B', 'D', 'C', 'E', 'F', 'K', 'A', 'G', 'H', 'I', 'J', 'L', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        public float capHeight = 1.0f;
        public float descent;
        public float down;
        public boolean flipped;
        public FileHandle fontFile;
        public final Glyph[][] glyphs = new Glyph[128][];
        public String[] imagePaths;
        public float lineHeight;
        public boolean markupEnabled;
        public int padBottom;
        public int padLeft;
        public int padRight;
        public int padTop;
        public float scaleX = 1.0f;
        public float scaleY = 1.0f;
        public float spaceWidth;
        public char[] xChars = {'x', 'e', 'a', 'o', 'n', 's', 'r', 'c', 'u', 'm', 'v', 'w', 'z'};
        public float xHeight = 1.0f;

        public BitmapFontData() {
        }

        public BitmapFontData(FileHandle fontFile2, boolean flip) {
            this.fontFile = fontFile2;
            this.flipped = flip;
            load(fontFile2, flip);
        }

        public void load(FileHandle fontFile2, boolean flip) {
            String[] pageLine;
            String fileName;
            if (this.imagePaths != null) {
                throw new IllegalStateException("Already loaded.");
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fontFile2.read()), 512);
            try {
                String line = bufferedReader.readLine();
                if (line == null) {
                    throw new GdxRuntimeException("File is empty.");
                }
                String line2 = line.substring(line.indexOf("padding=") + 8);
                String[] padding = line2.substring(0, line2.indexOf(32)).split(",", 4);
                if (padding.length != 4) {
                    throw new GdxRuntimeException("Invalid padding.");
                }
                this.padTop = Integer.parseInt(padding[0]);
                this.padLeft = Integer.parseInt(padding[1]);
                this.padBottom = Integer.parseInt(padding[2]);
                this.padRight = Integer.parseInt(padding[3]);
                int padY = this.padTop + this.padBottom;
                String line3 = bufferedReader.readLine();
                if (line3 == null) {
                    throw new GdxRuntimeException("Missing common header.");
                }
                String[] common = line3.split(" ", 7);
                if (common.length < 3) {
                    throw new GdxRuntimeException("Invalid common header.");
                } else if (!common[1].startsWith("lineHeight=")) {
                    throw new GdxRuntimeException("Missing: lineHeight");
                } else {
                    this.lineHeight = (float) Integer.parseInt(common[1].substring(11));
                    if (!common[2].startsWith("base=")) {
                        throw new GdxRuntimeException("Missing: base");
                    }
                    float baseLine = (float) Integer.parseInt(common[2].substring(5));
                    int pageCount = 1;
                    if (common.length >= 6 && common[5] != null && common[5].startsWith("pages=")) {
                        try {
                            pageCount = Math.max(1, Integer.parseInt(common[5].substring(6)));
                        } catch (NumberFormatException e) {
                        }
                    }
                    this.imagePaths = new String[pageCount];
                    for (int p = 0; p < pageCount; p++) {
                        String line4 = bufferedReader.readLine();
                        if (line4 == null) {
                            throw new GdxRuntimeException("Missing additional page definitions.");
                        }
                        pageLine = line4.split(" ", 4);
                        if (!pageLine[2].startsWith("file=")) {
                            throw new GdxRuntimeException("Missing: file");
                        }
                        if (pageLine[1].startsWith("id=")) {
                            if (Integer.parseInt(pageLine[1].substring(3)) != p) {
                                throw new GdxRuntimeException("Page IDs must be indices starting at 0: " + pageLine[1].substring(3));
                            }
                        }
                        if (pageLine[2].endsWith("\"")) {
                            fileName = pageLine[2].substring(6, pageLine[2].length() - 1);
                        } else {
                            fileName = pageLine[2].substring(5, pageLine[2].length());
                        }
                        this.imagePaths[p] = fontFile2.parent().child(fileName).path().replaceAll("\\\\", "/");
                    }
                    this.descent = Animation.CurveTimeline.LINEAR;
                    while (true) {
                        String line5 = bufferedReader.readLine();
                        if (line5 != null && !line5.startsWith("kernings ")) {
                            if (line5.startsWith("char ")) {
                                Glyph glyph = new Glyph();
                                StringTokenizer stringTokenizer = new StringTokenizer(line5, " =");
                                stringTokenizer.nextToken();
                                stringTokenizer.nextToken();
                                int ch = Integer.parseInt(stringTokenizer.nextToken());
                                if (ch <= 65535) {
                                    setGlyph(ch, glyph);
                                    glyph.id = ch;
                                    stringTokenizer.nextToken();
                                    glyph.srcX = Integer.parseInt(stringTokenizer.nextToken());
                                    stringTokenizer.nextToken();
                                    glyph.srcY = Integer.parseInt(stringTokenizer.nextToken());
                                    stringTokenizer.nextToken();
                                    glyph.width = Integer.parseInt(stringTokenizer.nextToken());
                                    stringTokenizer.nextToken();
                                    glyph.height = Integer.parseInt(stringTokenizer.nextToken());
                                    stringTokenizer.nextToken();
                                    glyph.xoffset = Integer.parseInt(stringTokenizer.nextToken());
                                    stringTokenizer.nextToken();
                                    if (flip) {
                                        glyph.yoffset = Integer.parseInt(stringTokenizer.nextToken());
                                    } else {
                                        glyph.yoffset = -(glyph.height + Integer.parseInt(stringTokenizer.nextToken()));
                                    }
                                    stringTokenizer.nextToken();
                                    glyph.xadvance = Integer.parseInt(stringTokenizer.nextToken());
                                    if (stringTokenizer.hasMoreTokens()) {
                                        stringTokenizer.nextToken();
                                    }
                                    if (stringTokenizer.hasMoreTokens()) {
                                        try {
                                            glyph.page = Integer.parseInt(stringTokenizer.nextToken());
                                        } catch (NumberFormatException e2) {
                                        }
                                    }
                                    if (glyph.width > 0 && glyph.height > 0) {
                                        this.descent = Math.min(((float) glyph.yoffset) + baseLine, this.descent);
                                    }
                                }
                            }
                        }
                    }
                    this.descent = this.descent + ((float) this.padBottom);
                    while (true) {
                        String line6 = bufferedReader.readLine();
                        if (line6 != null && line6.startsWith("kerning ")) {
                            StringTokenizer stringTokenizer2 = new StringTokenizer(line6, " =");
                            stringTokenizer2.nextToken();
                            stringTokenizer2.nextToken();
                            int first = Integer.parseInt(stringTokenizer2.nextToken());
                            stringTokenizer2.nextToken();
                            int second = Integer.parseInt(stringTokenizer2.nextToken());
                            if (first >= 0 && first <= 65535 && second >= 0 && second <= 65535) {
                                Glyph glyph2 = getGlyph((char) first);
                                stringTokenizer2.nextToken();
                                int amount = Integer.parseInt(stringTokenizer2.nextToken());
                                if (glyph2 != null) {
                                    glyph2.setKerning(second, amount);
                                }
                            }
                        }
                    }
                    Glyph spaceGlyph = getGlyph(' ');
                    if (spaceGlyph == null) {
                        spaceGlyph = new Glyph();
                        spaceGlyph.id = 32;
                        Glyph xadvanceGlyph = getGlyph('l');
                        if (xadvanceGlyph == null) {
                            xadvanceGlyph = getFirstGlyph();
                        }
                        spaceGlyph.xadvance = xadvanceGlyph.xadvance;
                        setGlyph(32, spaceGlyph);
                    }
                    if (spaceGlyph.width == 0) {
                        spaceGlyph.width = spaceGlyph.xadvance + this.padRight;
                    }
                    this.spaceWidth = (float) spaceGlyph.width;
                    Glyph xGlyph = null;
                    int i = 0;
                    while (i < this.xChars.length && (xGlyph = getGlyph(this.xChars[i])) == null) {
                        i++;
                    }
                    if (xGlyph == null) {
                        xGlyph = getFirstGlyph();
                    }
                    this.xHeight = (float) (xGlyph.height - padY);
                    Glyph capGlyph = null;
                    int i2 = 0;
                    while (i2 < this.capChars.length && (capGlyph = getGlyph(this.capChars[i2])) == null) {
                        i2++;
                    }
                    if (capGlyph == null) {
                        Glyph[][] glyphArr = this.glyphs;
                        int length = glyphArr.length;
                        for (int i3 = 0; i3 < length; i3++) {
                            Glyph[] page = glyphArr[i3];
                            if (page != null) {
                                int length2 = page.length;
                                for (int i4 = 0; i4 < length2; i4++) {
                                    Glyph glyph3 = page[i4];
                                    if (!(glyph3 == null || glyph3.height == 0 || glyph3.width == 0)) {
                                        this.capHeight = Math.max(this.capHeight, (float) glyph3.height);
                                    }
                                }
                            }
                        }
                    } else {
                        this.capHeight = (float) capGlyph.height;
                    }
                    this.capHeight = this.capHeight - ((float) padY);
                    this.ascent = baseLine - this.capHeight;
                    this.down = -this.lineHeight;
                    if (flip) {
                        this.ascent = -this.ascent;
                        this.down = -this.down;
                    }
                    StreamUtils.closeQuietly(bufferedReader);
                }
            } catch (NumberFormatException ex) {
                throw new GdxRuntimeException("Invalid page id: " + pageLine[1].substring(3), ex);
            } catch (Exception ex2) {
                throw new GdxRuntimeException("Error loading font file: " + fontFile2, ex2);
            } catch (Throwable th) {
                StreamUtils.closeQuietly(bufferedReader);
                throw th;
            }
        }

        public void setGlyphRegion(Glyph glyph, TextureRegion region) {
            Texture texture = region.getTexture();
            float invTexWidth = 1.0f / ((float) texture.getWidth());
            float invTexHeight = 1.0f / ((float) texture.getHeight());
            float offsetX = Animation.CurveTimeline.LINEAR;
            float offsetY = Animation.CurveTimeline.LINEAR;
            float u = region.u;
            float v = region.v;
            float regionWidth = (float) region.getRegionWidth();
            float regionHeight = (float) region.getRegionHeight();
            if (region instanceof TextureAtlas.AtlasRegion) {
                TextureAtlas.AtlasRegion atlasRegion = (TextureAtlas.AtlasRegion) region;
                offsetX = atlasRegion.offsetX;
                offsetY = ((float) (atlasRegion.originalHeight - atlasRegion.packedHeight)) - atlasRegion.offsetY;
            }
            float x = (float) glyph.srcX;
            float x2 = (float) (glyph.srcX + glyph.width);
            float y = (float) glyph.srcY;
            float y2 = (float) (glyph.srcY + glyph.height);
            if (offsetX > Animation.CurveTimeline.LINEAR) {
                x -= offsetX;
                if (x < Animation.CurveTimeline.LINEAR) {
                    glyph.width = (int) (((float) glyph.width) + x);
                    glyph.xoffset = (int) (((float) glyph.xoffset) - x);
                    x = Animation.CurveTimeline.LINEAR;
                }
                x2 -= offsetX;
                if (x2 > regionWidth) {
                    glyph.width = (int) (((float) glyph.width) - (x2 - regionWidth));
                    x2 = regionWidth;
                }
            }
            if (offsetY > Animation.CurveTimeline.LINEAR) {
                y -= offsetY;
                if (y < Animation.CurveTimeline.LINEAR) {
                    glyph.height = (int) (((float) glyph.height) + y);
                    y = Animation.CurveTimeline.LINEAR;
                }
                y2 -= offsetY;
                if (y2 > regionHeight) {
                    float amount = y2 - regionHeight;
                    glyph.height = (int) (((float) glyph.height) - amount);
                    glyph.yoffset = (int) (((float) glyph.yoffset) + amount);
                    y2 = regionHeight;
                }
            }
            glyph.u = (x * invTexWidth) + u;
            glyph.u2 = (x2 * invTexWidth) + u;
            if (this.flipped) {
                glyph.v = (y * invTexHeight) + v;
                glyph.v2 = (y2 * invTexHeight) + v;
                return;
            }
            glyph.v2 = (y * invTexHeight) + v;
            glyph.v = (y2 * invTexHeight) + v;
        }

        public void setLineHeight(float height) {
            this.lineHeight = this.scaleY * height;
            this.down = this.flipped ? this.lineHeight : -this.lineHeight;
        }

        public void setGlyph(int ch, Glyph glyph) {
            Glyph[] page = this.glyphs[ch / 512];
            if (page == null) {
                page = new Glyph[512];
                this.glyphs[ch / 512] = page;
            }
            page[ch & PAK_ASSETS.IMG_MORE004] = glyph;
        }

        public Glyph getFirstGlyph() {
            for (Glyph[] page : this.glyphs) {
                if (page != null) {
                    for (Glyph glyph : page) {
                        if (glyph != null && glyph.height != 0 && glyph.width != 0) {
                            return glyph;
                        }
                    }
                    continue;
                }
            }
            throw new GdxRuntimeException("No glyphs found.");
        }

        public boolean hasGlyph(char ch) {
            return getGlyph(ch) != null;
        }

        public Glyph getGlyph(char ch) {
            Glyph[] page = this.glyphs[ch / 512];
            if (page != null) {
                return page[ch & 511];
            }
            return null;
        }

        public void getGlyphs(GlyphLayout.GlyphRun run, CharSequence str, int start, int end) {
            boolean markupEnabled2 = this.markupEnabled;
            float scaleX2 = this.scaleX;
            Array<Glyph> glyphs2 = run.glyphs;
            FloatArray xAdvances = run.xAdvances;
            Glyph lastGlyph = null;
            int start2 = start;
            while (start2 < end) {
                int start3 = start2 + 1;
                char ch = str.charAt(start2);
                Glyph glyph = getGlyph(ch);
                if (glyph == null) {
                    start2 = start3;
                } else {
                    glyphs2.add(glyph);
                    if (lastGlyph == null) {
                        xAdvances.add((((float) (-glyph.xoffset)) * scaleX2) - ((float) this.padLeft));
                    } else {
                        xAdvances.add(((float) (lastGlyph.xadvance + lastGlyph.getKerning(ch))) * scaleX2);
                    }
                    lastGlyph = glyph;
                    if (!markupEnabled2 || ch != '[' || start3 >= end || str.charAt(start3) != '[') {
                        start2 = start3;
                    } else {
                        start2 = start3 + 1;
                    }
                }
            }
            if (lastGlyph != null) {
                xAdvances.add((((float) (lastGlyph.xoffset + lastGlyph.width)) * scaleX2) - ((float) this.padRight));
            }
        }

        public int getWrapIndex(Array<Glyph> glyphs2, int start) {
            if (isWhitespace((char) glyphs2.get(start).id)) {
                return start + 1;
            }
            for (int i = start - 1; i >= 1; i--) {
                char ch = (char) glyphs2.get(i).id;
                if (isWhitespace(ch)) {
                    return i + 1;
                }
                if (isBreakChar(ch)) {
                    return i;
                }
            }
            return 0;
        }

        public boolean isBreakChar(char c) {
            if (this.breakChars == null) {
                return false;
            }
            for (char br : this.breakChars) {
                if (c == br) {
                    return true;
                }
            }
            return false;
        }

        public boolean isWhitespace(char c) {
            switch (c) {
                case 9:
                case 10:
                case 13:
                case ' ':
                    return true;
                default:
                    return false;
            }
        }

        public String getImagePath(int index) {
            return this.imagePaths[index];
        }

        public String[] getImagePaths() {
            return this.imagePaths;
        }

        public FileHandle getFontFile() {
            return this.fontFile;
        }

        public void setScale(float scaleX2, float scaleY2) {
            if (scaleX2 == Animation.CurveTimeline.LINEAR) {
                throw new IllegalArgumentException("scaleX cannot be 0.");
            } else if (scaleY2 == Animation.CurveTimeline.LINEAR) {
                throw new IllegalArgumentException("scaleY cannot be 0.");
            } else {
                float x = scaleX2 / this.scaleX;
                float y = scaleY2 / this.scaleY;
                this.lineHeight *= y;
                this.spaceWidth *= x;
                this.xHeight *= y;
                this.capHeight *= y;
                this.ascent *= y;
                this.descent *= y;
                this.down *= y;
                this.padTop = (int) (((float) this.padTop) * y);
                this.padLeft = (int) (((float) this.padLeft) * y);
                this.padBottom = (int) (((float) this.padBottom) * y);
                this.padRight = (int) (((float) this.padRight) * y);
                this.scaleX = scaleX2;
                this.scaleY = scaleY2;
            }
        }

        public void setScale(float scaleXY) {
            setScale(scaleXY, scaleXY);
        }

        public void scale(float amount) {
            setScale(this.scaleX + amount, this.scaleY + amount);
        }
    }
}
