package com.badlogic.gdx.graphics.g3d.particles.influencers;

import com.badlogic.gdx.graphics.g3d.particles.ParticleChannels;
import com.badlogic.gdx.graphics.g3d.particles.ParticleControllerComponent;
import com.kbz.esotericsoftware.spine.Animation;

public class ScaleInfluencer extends SimpleInfluencer {
    public ScaleInfluencer() {
        this.valueChannelDescriptor = ParticleChannels.Scale;
    }

    public void activateParticles(int startIndex, int count) {
        if (this.value.isRelative()) {
            int i = startIndex * this.valueChannel.strideSize;
            int a = startIndex * this.interpolationChannel.strideSize;
            int c = i + (this.valueChannel.strideSize * count);
            while (i < c) {
                float start = this.value.newLowValue() * this.controller.scale.x;
                float diff = this.value.newHighValue() * this.controller.scale.x;
                this.interpolationChannel.data[a + 0] = start;
                this.interpolationChannel.data[a + 1] = diff;
                this.valueChannel.data[i] = (this.value.getScale(Animation.CurveTimeline.LINEAR) * diff) + start;
                i += this.valueChannel.strideSize;
                a += this.interpolationChannel.strideSize;
            }
            return;
        }
        int i2 = startIndex * this.valueChannel.strideSize;
        int a2 = startIndex * this.interpolationChannel.strideSize;
        int c2 = i2 + (this.valueChannel.strideSize * count);
        while (i2 < c2) {
            float start2 = this.value.newLowValue() * this.controller.scale.x;
            float diff2 = (this.value.newHighValue() * this.controller.scale.x) - start2;
            this.interpolationChannel.data[a2 + 0] = start2;
            this.interpolationChannel.data[a2 + 1] = diff2;
            this.valueChannel.data[i2] = (this.value.getScale(Animation.CurveTimeline.LINEAR) * diff2) + start2;
            i2 += this.valueChannel.strideSize;
            a2 += this.interpolationChannel.strideSize;
        }
    }

    public ScaleInfluencer(ScaleInfluencer scaleInfluencer) {
        super(scaleInfluencer);
    }

    public ParticleControllerComponent copy() {
        return new ScaleInfluencer(this);
    }
}
