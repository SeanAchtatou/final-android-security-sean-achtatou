package com.badlogic.gdx.graphics.g3d.particles.batches;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.DepthTestAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.particles.ParallelArray;
import com.badlogic.gdx.graphics.g3d.particles.ParticleShader;
import com.badlogic.gdx.graphics.g3d.particles.ResourceData;
import com.badlogic.gdx.graphics.g3d.particles.renderers.BillboardControllerRenderData;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import java.util.Iterator;

public class BillboardParticleBatch extends BufferedParticleBatch<BillboardControllerRenderData> {
    private static final VertexAttributes CPU_ATTRIBUTES = new VertexAttributes(new VertexAttribute(1, 3, ShaderProgram.POSITION_ATTRIBUTE), new VertexAttribute(16, 2, "a_texCoord0"), new VertexAttribute(2, 4, ShaderProgram.COLOR_ATTRIBUTE));
    private static final int CPU_COLOR_OFFSET = ((short) (CPU_ATTRIBUTES.findByUsage(2).offset / 4));
    private static final int CPU_POSITION_OFFSET = ((short) (CPU_ATTRIBUTES.findByUsage(1).offset / 4));
    private static final int CPU_UV_OFFSET = ((short) (CPU_ATTRIBUTES.findByUsage(16).offset / 4));
    private static final int CPU_VERTEX_SIZE = (CPU_ATTRIBUTES.vertexSize / 4);
    private static final VertexAttributes GPU_ATTRIBUTES = new VertexAttributes(new VertexAttribute(1, 3, ShaderProgram.POSITION_ATTRIBUTE), new VertexAttribute(16, 2, "a_texCoord0"), new VertexAttribute(2, 4, ShaderProgram.COLOR_ATTRIBUTE), new VertexAttribute(512, 4, "a_sizeAndRotation"));
    private static final int GPU_COLOR_OFFSET = ((short) (GPU_ATTRIBUTES.findByUsage(2).offset / 4));
    private static final int GPU_POSITION_OFFSET = ((short) (GPU_ATTRIBUTES.findByUsage(1).offset / 4));
    private static final int GPU_SIZE_ROTATION_OFFSET = ((short) (GPU_ATTRIBUTES.findByUsage(512).offset / 4));
    private static final int GPU_UV_OFFSET = ((short) (GPU_ATTRIBUTES.findByUsage(16).offset / 4));
    private static final int GPU_VERTEX_SIZE = (GPU_ATTRIBUTES.vertexSize / 4);
    private static final int MAX_PARTICLES_PER_MESH = 8191;
    private static final int MAX_VERTICES_PER_MESH = 32764;
    protected static final Matrix3 TMP_M3 = new Matrix3();
    protected static final Vector3 TMP_V1 = new Vector3();
    protected static final Vector3 TMP_V2 = new Vector3();
    protected static final Vector3 TMP_V3 = new Vector3();
    protected static final Vector3 TMP_V4 = new Vector3();
    protected static final Vector3 TMP_V5 = new Vector3();
    protected static final Vector3 TMP_V6 = new Vector3();
    protected static final int directionUsage = 1024;
    protected static final int sizeAndRotationUsage = 512;
    private VertexAttributes currentAttributes;
    private int currentVertexSize;
    private short[] indices;
    protected ParticleShader.AlignMode mode;
    private RenderablePool renderablePool;
    private Array<Renderable> renderables;
    Shader shader;
    protected Texture texture;
    protected boolean useGPU;
    private float[] vertices;

    private class RenderablePool extends Pool<Renderable> {
        public RenderablePool() {
        }

        public Renderable newObject() {
            return BillboardParticleBatch.this.allocRenderable();
        }
    }

    public static class Config {
        ParticleShader.AlignMode mode;
        boolean useGPU;

        public Config() {
        }

        public Config(boolean useGPU2, ParticleShader.AlignMode mode2) {
            this.useGPU = useGPU2;
            this.mode = mode2;
        }
    }

    public BillboardParticleBatch(ParticleShader.AlignMode mode2, boolean useGPU2, int capacity) {
        super(BillboardControllerRenderData.class);
        this.currentVertexSize = 0;
        this.useGPU = false;
        this.mode = ParticleShader.AlignMode.Screen;
        this.renderables = new Array<>();
        this.renderablePool = new RenderablePool();
        allocIndices();
        initRenderData();
        ensureCapacity(capacity);
        setUseGpu(useGPU2);
        setAlignMode(mode2);
    }

    public BillboardParticleBatch() {
        this(ParticleShader.AlignMode.Screen, false, 100);
    }

    public BillboardParticleBatch(int capacity) {
        this(ParticleShader.AlignMode.Screen, false, capacity);
    }

    public void allocParticlesData(int capacity) {
        this.vertices = new float[(this.currentVertexSize * 4 * capacity)];
        allocRenderables(capacity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
     arg types: [int, ?, int, com.badlogic.gdx.graphics.VertexAttributes]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void */
    /* access modifiers changed from: protected */
    public Renderable allocRenderable() {
        Renderable renderable = new Renderable();
        renderable.primitiveType = 4;
        renderable.meshPartOffset = 0;
        renderable.material = new Material(new BlendingAttribute(1, 771, 1.0f), new DepthTestAttribute(515, false), TextureAttribute.createDiffuse(this.texture));
        renderable.mesh = new Mesh(false, (int) MAX_VERTICES_PER_MESH, 49146, this.currentAttributes);
        renderable.mesh.setIndices(this.indices);
        renderable.shader = this.shader;
        return renderable;
    }

    private void allocIndices() {
        this.indices = new short[49146];
        int i = 0;
        int vertex = 0;
        while (i < 49146) {
            this.indices[i] = (short) vertex;
            this.indices[i + 1] = (short) (vertex + 1);
            this.indices[i + 2] = (short) (vertex + 2);
            this.indices[i + 3] = (short) (vertex + 2);
            this.indices[i + 4] = (short) (vertex + 3);
            this.indices[i + 5] = (short) vertex;
            i += 6;
            vertex += 4;
        }
    }

    private void allocRenderables(int capacity) {
        int meshCount = MathUtils.ceil((float) (capacity / MAX_PARTICLES_PER_MESH));
        int free = this.renderablePool.getFree();
        if (free < meshCount) {
            int left = meshCount - free;
            for (int i = 0; i < left; i++) {
                this.renderablePool.free(this.renderablePool.newObject());
            }
        }
    }

    private Shader getShader(Renderable renderable) {
        Shader shader2;
        if (this.useGPU) {
            shader2 = new ParticleShader(renderable, new ParticleShader.Config(this.mode));
        } else {
            shader2 = new DefaultShader(renderable);
        }
        shader2.init();
        return shader2;
    }

    private void allocShader() {
        Renderable newRenderable = allocRenderable();
        Shader shader2 = getShader(newRenderable);
        newRenderable.shader = shader2;
        this.shader = shader2;
        this.renderablePool.free(newRenderable);
    }

    private void clearRenderablesPool() {
        this.renderablePool.freeAll(this.renderables);
        int free = this.renderablePool.getFree();
        for (int i = 0; i < free; i++) {
            ((Renderable) this.renderablePool.obtain()).mesh.dispose();
        }
        this.renderables.clear();
    }

    public void setVertexData() {
        if (this.useGPU) {
            this.currentAttributes = GPU_ATTRIBUTES;
            this.currentVertexSize = GPU_VERTEX_SIZE;
            return;
        }
        this.currentAttributes = CPU_ATTRIBUTES;
        this.currentVertexSize = CPU_VERTEX_SIZE;
    }

    private void initRenderData() {
        setVertexData();
        clearRenderablesPool();
        allocShader();
        resetCapacity();
    }

    public void setAlignMode(ParticleShader.AlignMode mode2) {
        if (mode2 != this.mode) {
            this.mode = mode2;
            if (this.useGPU) {
                initRenderData();
                allocRenderables(this.bufferedParticlesCount);
            }
        }
    }

    public ParticleShader.AlignMode getAlignMode() {
        return this.mode;
    }

    public void setUseGpu(boolean useGPU2) {
        if (this.useGPU != useGPU2) {
            this.useGPU = useGPU2;
            initRenderData();
            allocRenderables(this.bufferedParticlesCount);
        }
    }

    public boolean isUseGPU() {
        return this.useGPU;
    }

    public void setTexture(Texture texture2) {
        this.renderablePool.freeAll(this.renderables);
        this.renderables.clear();
        int free = this.renderablePool.getFree();
        for (int i = 0; i < free; i++) {
            ((TextureAttribute) ((Renderable) this.renderablePool.obtain()).material.get(TextureAttribute.Diffuse)).textureDescription.texture = texture2;
        }
        this.texture = texture2;
    }

    public Texture getTexture() {
        return this.texture;
    }

    public void begin() {
        super.begin();
        this.renderablePool.freeAll(this.renderables);
        this.renderables.clear();
    }

    private static void putVertex(float[] vertices2, int offset, float x, float y, float z, float u, float v, float scaleX, float scaleY, float cosRotation, float sinRotation, float r, float g, float b, float a) {
        vertices2[GPU_POSITION_OFFSET + offset] = x;
        vertices2[GPU_POSITION_OFFSET + offset + 1] = y;
        vertices2[GPU_POSITION_OFFSET + offset + 2] = z;
        vertices2[GPU_UV_OFFSET + offset] = u;
        vertices2[GPU_UV_OFFSET + offset + 1] = v;
        vertices2[GPU_SIZE_ROTATION_OFFSET + offset] = scaleX;
        vertices2[GPU_SIZE_ROTATION_OFFSET + offset + 1] = scaleY;
        vertices2[GPU_SIZE_ROTATION_OFFSET + offset + 2] = cosRotation;
        vertices2[GPU_SIZE_ROTATION_OFFSET + offset + 3] = sinRotation;
        vertices2[GPU_COLOR_OFFSET + offset] = r;
        vertices2[GPU_COLOR_OFFSET + offset + 1] = g;
        vertices2[GPU_COLOR_OFFSET + offset + 2] = b;
        vertices2[GPU_COLOR_OFFSET + offset + 3] = a;
    }

    private static void putVertex(float[] vertices2, int offset, Vector3 p, float u, float v, float r, float g, float b, float a) {
        vertices2[CPU_POSITION_OFFSET + offset] = p.x;
        vertices2[CPU_POSITION_OFFSET + offset + 1] = p.y;
        vertices2[CPU_POSITION_OFFSET + offset + 2] = p.z;
        vertices2[CPU_UV_OFFSET + offset] = u;
        vertices2[CPU_UV_OFFSET + offset + 1] = v;
        vertices2[CPU_COLOR_OFFSET + offset] = r;
        vertices2[CPU_COLOR_OFFSET + offset + 1] = g;
        vertices2[CPU_COLOR_OFFSET + offset + 2] = b;
        vertices2[CPU_COLOR_OFFSET + offset + 3] = a;
    }

    private void fillVerticesGPU(int[] particlesOffset) {
        int tp = 0;
        Iterator it = this.renderData.iterator();
        while (it.hasNext()) {
            BillboardControllerRenderData data = (BillboardControllerRenderData) it.next();
            ParallelArray.FloatChannel scaleChannel = data.scaleChannel;
            ParallelArray.FloatChannel regionChannel = data.regionChannel;
            ParallelArray.FloatChannel positionChannel = data.positionChannel;
            ParallelArray.FloatChannel colorChannel = data.colorChannel;
            ParallelArray.FloatChannel rotationChannel = data.rotationChannel;
            int p = 0;
            int c = data.controller.particles.size;
            while (p < c) {
                int baseOffset = particlesOffset[tp] * this.currentVertexSize * 4;
                float scale = scaleChannel.data[scaleChannel.strideSize * p];
                int regionOffset = p * regionChannel.strideSize;
                int positionOffset = p * positionChannel.strideSize;
                int colorOffset = p * colorChannel.strideSize;
                int rotationOffset = p * rotationChannel.strideSize;
                float px = positionChannel.data[positionOffset + 0];
                float py = positionChannel.data[positionOffset + 1];
                float pz = positionChannel.data[positionOffset + 2];
                float u = regionChannel.data[regionOffset + 0];
                float v = regionChannel.data[regionOffset + 1];
                float u2 = regionChannel.data[regionOffset + 2];
                float v2 = regionChannel.data[regionOffset + 3];
                float sx = regionChannel.data[regionOffset + 4] * scale;
                float sy = regionChannel.data[regionOffset + 5] * scale;
                float r = colorChannel.data[colorOffset + 0];
                float g = colorChannel.data[colorOffset + 1];
                float b = colorChannel.data[colorOffset + 2];
                float a = colorChannel.data[colorOffset + 3];
                float cosRotation = rotationChannel.data[rotationOffset + 0];
                float sinRotation = rotationChannel.data[rotationOffset + 1];
                putVertex(this.vertices, baseOffset, px, py, pz, u, v2, -sx, -sy, cosRotation, sinRotation, r, g, b, a);
                int baseOffset2 = baseOffset + this.currentVertexSize;
                putVertex(this.vertices, baseOffset2, px, py, pz, u2, v2, sx, -sy, cosRotation, sinRotation, r, g, b, a);
                int baseOffset3 = baseOffset2 + this.currentVertexSize;
                putVertex(this.vertices, baseOffset3, px, py, pz, u2, v, sx, sy, cosRotation, sinRotation, r, g, b, a);
                int baseOffset4 = baseOffset3 + this.currentVertexSize;
                putVertex(this.vertices, baseOffset4, px, py, pz, u, v, -sx, sy, cosRotation, sinRotation, r, g, b, a);
                int i = this.currentVertexSize + baseOffset4;
                p++;
                tp++;
            }
        }
    }

    private void fillVerticesToViewPointCPU(int[] particlesOffset) {
        int tp = 0;
        Iterator it = this.renderData.iterator();
        while (it.hasNext()) {
            BillboardControllerRenderData data = (BillboardControllerRenderData) it.next();
            ParallelArray.FloatChannel scaleChannel = data.scaleChannel;
            ParallelArray.FloatChannel regionChannel = data.regionChannel;
            ParallelArray.FloatChannel positionChannel = data.positionChannel;
            ParallelArray.FloatChannel colorChannel = data.colorChannel;
            ParallelArray.FloatChannel rotationChannel = data.rotationChannel;
            int p = 0;
            int c = data.controller.particles.size;
            while (p < c) {
                int baseOffset = particlesOffset[tp] * this.currentVertexSize * 4;
                float scale = scaleChannel.data[scaleChannel.strideSize * p];
                int regionOffset = p * regionChannel.strideSize;
                int positionOffset = p * positionChannel.strideSize;
                int colorOffset = p * colorChannel.strideSize;
                int rotationOffset = p * rotationChannel.strideSize;
                float px = positionChannel.data[positionOffset + 0];
                float py = positionChannel.data[positionOffset + 1];
                float pz = positionChannel.data[positionOffset + 2];
                float u = regionChannel.data[regionOffset + 0];
                float v = regionChannel.data[regionOffset + 1];
                float u2 = regionChannel.data[regionOffset + 2];
                float v2 = regionChannel.data[regionOffset + 3];
                float sx = regionChannel.data[regionOffset + 4] * scale;
                float sy = regionChannel.data[regionOffset + 5] * scale;
                float r = colorChannel.data[colorOffset + 0];
                float g = colorChannel.data[colorOffset + 1];
                float b = colorChannel.data[colorOffset + 2];
                float a = colorChannel.data[colorOffset + 3];
                float cosRotation = rotationChannel.data[rotationOffset + 0];
                float sinRotation = rotationChannel.data[rotationOffset + 1];
                Vector3 look = TMP_V3.set(this.camera.position).sub(px, py, pz).nor();
                Vector3 right = TMP_V1.set(this.camera.up).crs(look).nor();
                Vector3 up = TMP_V2.set(look).crs(right);
                right.scl(sx);
                up.scl(sy);
                if (cosRotation != 1.0f) {
                    TMP_M3.setToRotation(look, cosRotation, sinRotation);
                    putVertex(this.vertices, baseOffset, TMP_V6.set((-TMP_V1.x) - TMP_V2.x, (-TMP_V1.y) - TMP_V2.y, (-TMP_V1.z) - TMP_V2.z).mul(TMP_M3).add(px, py, pz), u, v2, r, g, b, a);
                    int baseOffset2 = baseOffset + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset2, TMP_V6.set(TMP_V1.x - TMP_V2.x, TMP_V1.y - TMP_V2.y, TMP_V1.z - TMP_V2.z).mul(TMP_M3).add(px, py, pz), u2, v2, r, g, b, a);
                    int baseOffset3 = baseOffset2 + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset3, TMP_V6.set(TMP_V1.x + TMP_V2.x, TMP_V1.y + TMP_V2.y, TMP_V1.z + TMP_V2.z).mul(TMP_M3).add(px, py, pz), u2, v, r, g, b, a);
                    putVertex(this.vertices, baseOffset3 + this.currentVertexSize, TMP_V6.set((-TMP_V1.x) + TMP_V2.x, (-TMP_V1.y) + TMP_V2.y, (-TMP_V1.z) + TMP_V2.z).mul(TMP_M3).add(px, py, pz), u, v, r, g, b, a);
                } else {
                    putVertex(this.vertices, baseOffset, TMP_V6.set(((-TMP_V1.x) - TMP_V2.x) + px, ((-TMP_V1.y) - TMP_V2.y) + py, ((-TMP_V1.z) - TMP_V2.z) + pz), u, v2, r, g, b, a);
                    int baseOffset4 = baseOffset + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset4, TMP_V6.set((TMP_V1.x - TMP_V2.x) + px, (TMP_V1.y - TMP_V2.y) + py, (TMP_V1.z - TMP_V2.z) + pz), u2, v2, r, g, b, a);
                    int baseOffset5 = baseOffset4 + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset5, TMP_V6.set(TMP_V1.x + TMP_V2.x + px, TMP_V1.y + TMP_V2.y + py, TMP_V1.z + TMP_V2.z + pz), u2, v, r, g, b, a);
                    putVertex(this.vertices, baseOffset5 + this.currentVertexSize, TMP_V6.set((-TMP_V1.x) + TMP_V2.x + px, (-TMP_V1.y) + TMP_V2.y + py, (-TMP_V1.z) + TMP_V2.z + pz), u, v, r, g, b, a);
                }
                p++;
                tp++;
            }
        }
    }

    private void fillVerticesToScreenCPU(int[] particlesOffset) {
        Vector3 look = TMP_V3.set(this.camera.direction).scl(-1.0f);
        Vector3 right = TMP_V4.set(this.camera.up).crs(look).nor();
        Vector3 up = this.camera.up;
        int tp = 0;
        Iterator it = this.renderData.iterator();
        while (it.hasNext()) {
            BillboardControllerRenderData data = (BillboardControllerRenderData) it.next();
            ParallelArray.FloatChannel scaleChannel = data.scaleChannel;
            ParallelArray.FloatChannel regionChannel = data.regionChannel;
            ParallelArray.FloatChannel positionChannel = data.positionChannel;
            ParallelArray.FloatChannel colorChannel = data.colorChannel;
            ParallelArray.FloatChannel rotationChannel = data.rotationChannel;
            int p = 0;
            int c = data.controller.particles.size;
            while (p < c) {
                int baseOffset = particlesOffset[tp] * this.currentVertexSize * 4;
                float scale = scaleChannel.data[scaleChannel.strideSize * p];
                int regionOffset = p * regionChannel.strideSize;
                int positionOffset = p * positionChannel.strideSize;
                int colorOffset = p * colorChannel.strideSize;
                int rotationOffset = p * rotationChannel.strideSize;
                float px = positionChannel.data[positionOffset + 0];
                float py = positionChannel.data[positionOffset + 1];
                float pz = positionChannel.data[positionOffset + 2];
                float u = regionChannel.data[regionOffset + 0];
                float v = regionChannel.data[regionOffset + 1];
                float u2 = regionChannel.data[regionOffset + 2];
                float v2 = regionChannel.data[regionOffset + 3];
                float sx = regionChannel.data[regionOffset + 4] * scale;
                float sy = regionChannel.data[regionOffset + 5] * scale;
                float r = colorChannel.data[colorOffset + 0];
                float g = colorChannel.data[colorOffset + 1];
                float b = colorChannel.data[colorOffset + 2];
                float a = colorChannel.data[colorOffset + 3];
                float cosRotation = rotationChannel.data[rotationOffset + 0];
                float sinRotation = rotationChannel.data[rotationOffset + 1];
                TMP_V1.set(right).scl(sx);
                TMP_V2.set(up).scl(sy);
                if (cosRotation != 1.0f) {
                    TMP_M3.setToRotation(look, cosRotation, sinRotation);
                    putVertex(this.vertices, baseOffset, TMP_V6.set((-TMP_V1.x) - TMP_V2.x, (-TMP_V1.y) - TMP_V2.y, (-TMP_V1.z) - TMP_V2.z).mul(TMP_M3).add(px, py, pz), u, v2, r, g, b, a);
                    int baseOffset2 = baseOffset + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset2, TMP_V6.set(TMP_V1.x - TMP_V2.x, TMP_V1.y - TMP_V2.y, TMP_V1.z - TMP_V2.z).mul(TMP_M3).add(px, py, pz), u2, v2, r, g, b, a);
                    int baseOffset3 = baseOffset2 + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset3, TMP_V6.set(TMP_V1.x + TMP_V2.x, TMP_V1.y + TMP_V2.y, TMP_V1.z + TMP_V2.z).mul(TMP_M3).add(px, py, pz), u2, v, r, g, b, a);
                    putVertex(this.vertices, baseOffset3 + this.currentVertexSize, TMP_V6.set((-TMP_V1.x) + TMP_V2.x, (-TMP_V1.y) + TMP_V2.y, (-TMP_V1.z) + TMP_V2.z).mul(TMP_M3).add(px, py, pz), u, v, r, g, b, a);
                } else {
                    putVertex(this.vertices, baseOffset, TMP_V6.set(((-TMP_V1.x) - TMP_V2.x) + px, ((-TMP_V1.y) - TMP_V2.y) + py, ((-TMP_V1.z) - TMP_V2.z) + pz), u, v2, r, g, b, a);
                    int baseOffset4 = baseOffset + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset4, TMP_V6.set((TMP_V1.x - TMP_V2.x) + px, (TMP_V1.y - TMP_V2.y) + py, (TMP_V1.z - TMP_V2.z) + pz), u2, v2, r, g, b, a);
                    int baseOffset5 = baseOffset4 + this.currentVertexSize;
                    putVertex(this.vertices, baseOffset5, TMP_V6.set(TMP_V1.x + TMP_V2.x + px, TMP_V1.y + TMP_V2.y + py, TMP_V1.z + TMP_V2.z + pz), u2, v, r, g, b, a);
                    putVertex(this.vertices, baseOffset5 + this.currentVertexSize, TMP_V6.set((-TMP_V1.x) + TMP_V2.x + px, (-TMP_V1.y) + TMP_V2.y + py, (-TMP_V1.z) + TMP_V2.z + pz), u, v, r, g, b, a);
                }
                p++;
                tp++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void flush(int[] offsets) {
        if (this.useGPU) {
            fillVerticesGPU(offsets);
        } else if (this.mode == ParticleShader.AlignMode.Screen) {
            fillVerticesToScreenCPU(offsets);
        } else if (this.mode == ParticleShader.AlignMode.ViewPoint) {
            fillVerticesToViewPointCPU(offsets);
        }
        int vCount = this.bufferedParticlesCount * 4;
        int v = 0;
        while (v < vCount) {
            int addedVertexCount = Math.min(vCount - v, (int) MAX_VERTICES_PER_MESH);
            Renderable renderable = (Renderable) this.renderablePool.obtain();
            renderable.meshPartSize = (addedVertexCount / 4) * 6;
            renderable.mesh.setVertices(this.vertices, this.currentVertexSize * v, this.currentVertexSize * addedVertexCount);
            this.renderables.add(renderable);
            v += addedVertexCount;
        }
    }

    public void getRenderables(Array<Renderable> renderables2, Pool<Renderable> pool) {
        Iterator<Renderable> it = this.renderables.iterator();
        while (it.hasNext()) {
            renderables2.add(pool.obtain().set(it.next()));
        }
    }

    public void save(AssetManager manager, ResourceData resources) {
        ResourceData.SaveData data = resources.createSaveData("billboardBatch");
        data.save("cfg", new Config(this.useGPU, this.mode));
        data.saveAsset(manager.getAssetFileName(this.texture), Texture.class);
    }

    public void load(AssetManager manager, ResourceData resources) {
        ResourceData.SaveData data = resources.getSaveData("billboardBatch");
        if (data != null) {
            setTexture((Texture) manager.get(data.loadAsset()));
            Config cfg = (Config) data.load("cfg");
            setUseGpu(cfg.useGPU);
            setAlignMode(cfg.mode);
        }
    }
}
