package com.badlogic.gdx.backends.android;

import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.SurfaceHolder;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18;
import com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy;
import com.kbz.esotericsoftware.spine.Animation;
import javax.microedition.khronos.opengles.GL10;

public final class AndroidGraphicsLiveWallpaper extends AndroidGraphics {
    public AndroidGraphicsLiveWallpaper(AndroidLiveWallpaper lwp, AndroidApplicationConfiguration config, ResolutionStrategy resolutionStrategy) {
        super(lwp, config, resolutionStrategy, false);
    }

    /* access modifiers changed from: package-private */
    public SurfaceHolder getSurfaceHolder() {
        SurfaceHolder surfaceHolder;
        synchronized (((AndroidLiveWallpaper) this.app).service.sync) {
            surfaceHolder = ((AndroidLiveWallpaper) this.app).service.getSurfaceHolder();
        }
        return surfaceHolder;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v2, types: [com.badlogic.gdx.backends.android.AndroidGraphicsLiveWallpaper$1, com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20API18] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    protected android.view.View createGLSurfaceView(com.badlogic.gdx.backends.android.AndroidApplicationBase r10, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy r11) {
        /*
            r9 = this;
            boolean r1 = r9.checkGL20()
            if (r1 != 0) goto L_0x000e
            com.badlogic.gdx.utils.GdxRuntimeException r1 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r2 = "Libgdx requires OpenGL ES 2.0"
            r1.<init>(r2)
            throw r1
        L_0x000e:
            android.opengl.GLSurfaceView$EGLConfigChooser r7 = r9.getEglConfigChooser()
            int r8 = android.os.Build.VERSION.SDK_INT
            r1 = 10
            if (r8 > r1) goto L_0x004c
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r9.config
            boolean r1 = r1.useGLSurfaceView20API18
            if (r1 == 0) goto L_0x004c
            com.badlogic.gdx.backends.android.AndroidGraphicsLiveWallpaper$1 r0 = new com.badlogic.gdx.backends.android.AndroidGraphicsLiveWallpaper$1
            android.content.Context r1 = r10.getContext()
            r0.<init>(r1, r11)
            if (r7 == 0) goto L_0x0030
            r0.setEGLConfigChooser(r7)
        L_0x002c:
            r0.setRenderer(r9)
        L_0x002f:
            return r0
        L_0x0030:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r9.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r9.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r9.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r9.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r9.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r9.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x002c
        L_0x004c:
            com.badlogic.gdx.backends.android.AndroidGraphicsLiveWallpaper$2 r0 = new com.badlogic.gdx.backends.android.AndroidGraphicsLiveWallpaper$2
            android.content.Context r1 = r10.getContext()
            r0.<init>(r1, r11)
            if (r7 == 0) goto L_0x005e
            r0.setEGLConfigChooser(r7)
        L_0x005a:
            r0.setRenderer(r9)
            goto L_0x002f
        L_0x005e:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r9.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r9.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r9.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r9.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r9.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r9.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.AndroidGraphicsLiveWallpaper.createGLSurfaceView(com.badlogic.gdx.backends.android.AndroidApplicationBase, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy):android.view.View");
    }

    public void onDestroyGLSurfaceView() {
        if (this.view == null) {
            return;
        }
        if ((this.view instanceof GLSurfaceView) || (this.view instanceof GLSurfaceViewAPI18)) {
            try {
                this.view.getClass().getMethod("onDestroy", new Class[0]).invoke(this.view, new Object[0]);
                if (AndroidLiveWallpaperService.DEBUG) {
                    Log.d("WallpaperService", " > AndroidLiveWallpaper - onDestroy() stopped GLThread managed by GLSurfaceView");
                }
            } catch (Throwable t) {
                Log.e("WallpaperService", "failed to destroy GLSurfaceView's thread! GLSurfaceView.onDetachedFromWindow impl changed since API lvl 16!");
                t.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void resume() {
        synchronized (this.synch) {
            this.running = true;
            this.resume = true;
            while (this.resume) {
                try {
                    this.synch.wait();
                } catch (InterruptedException e) {
                    Gdx.app.log("AndroidGraphics", "waiting for resume synchronization failed!");
                }
            }
        }
    }

    public void onDrawFrame(GL10 gl) {
        boolean lrunning;
        boolean lpause;
        boolean ldestroy;
        boolean lresume;
        long time = System.nanoTime();
        this.deltaTime = ((float) (time - this.lastFrameTime)) / 1.0E9f;
        this.lastFrameTime = time;
        if (!this.resume) {
            this.mean.addValue(this.deltaTime);
        } else {
            this.deltaTime = Animation.CurveTimeline.LINEAR;
        }
        synchronized (this.synch) {
            lrunning = this.running;
            lpause = this.pause;
            ldestroy = this.destroy;
            lresume = this.resume;
            if (this.resume) {
                this.resume = false;
                this.synch.notifyAll();
            }
            if (this.pause) {
                this.pause = false;
                this.synch.notifyAll();
            }
            if (this.destroy) {
                this.destroy = false;
                this.synch.notifyAll();
            }
        }
        if (lresume) {
            this.app.getApplicationListener().resume();
            Gdx.app.log("AndroidGraphics", "resumed");
        }
        if (lrunning) {
            synchronized (this.app.getRunnables()) {
                this.app.getExecutedRunnables().clear();
                this.app.getExecutedRunnables().addAll(this.app.getRunnables());
                this.app.getRunnables().clear();
                for (int i = 0; i < this.app.getExecutedRunnables().size; i++) {
                    try {
                        this.app.getExecutedRunnables().get(i).run();
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
            this.app.getInput().processEvents();
            this.frameId++;
            this.app.getApplicationListener().render();
        }
        if (lpause) {
            this.app.getApplicationListener().pause();
            Gdx.app.log("AndroidGraphics", "paused");
        }
        if (ldestroy) {
            this.app.getApplicationListener().dispose();
            Gdx.app.log("AndroidGraphics", "destroyed");
        }
        if (time - this.frameStart > 1000000000) {
            this.fps = this.frames;
            this.frames = 0;
            this.frameStart = time;
        }
        this.frames++;
    }

    /* access modifiers changed from: protected */
    public void logManagedCachesStatus() {
        if (AndroidLiveWallpaperService.DEBUG) {
            super.logManagedCachesStatus();
        }
    }
}
