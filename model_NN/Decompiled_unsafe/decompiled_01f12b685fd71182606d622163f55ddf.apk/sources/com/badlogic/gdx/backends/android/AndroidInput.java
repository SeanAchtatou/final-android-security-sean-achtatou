package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import java.util.ArrayList;
import java.util.Arrays;

public class AndroidInput implements View.OnKeyListener, View.OnTouchListener, Input {
    public static final int NUM_TOUCHES = 20;
    public static final int SUPPORTED_KEYS = 260;
    final float[] R = new float[9];
    public boolean accelerometerAvailable = false;
    private SensorEventListener accelerometerListener;
    private final float[] accelerometerValues = new float[3];
    final Application app;
    private float azimuth = Animation.CurveTimeline.LINEAR;
    int[] button = new int[20];
    private boolean catchBack = false;
    private boolean catchMenu = false;
    private boolean compassAvailable = false;
    private SensorEventListener compassListener;
    private final AndroidApplicationConfiguration config;
    final Context context;
    private long currentEventTimeStamp = System.nanoTime();
    int[] deltaX = new int[20];
    int[] deltaY = new int[20];
    private Handler handle;
    final boolean hasMultitouch;
    private float inclination = Animation.CurveTimeline.LINEAR;
    private boolean[] justPressedKeys = new boolean[260];
    private boolean justTouched = false;
    private int keyCount = 0;
    ArrayList<KeyEvent> keyEvents = new ArrayList<>();
    private boolean keyJustPressed = false;
    ArrayList<View.OnKeyListener> keyListeners = new ArrayList<>();
    boolean keyboardAvailable;
    private boolean[] keys = new boolean[260];
    private final float[] magneticFieldValues = new float[3];
    private SensorManager manager;
    private final Input.Orientation nativeOrientation;
    private final AndroidOnscreenKeyboard onscreenKeyboard;
    final float[] orientation = new float[3];
    private float pitch = Animation.CurveTimeline.LINEAR;
    private InputProcessor processor;
    int[] realId = new int[20];
    boolean requestFocus = true;
    private float roll = Animation.CurveTimeline.LINEAR;
    private int sleepTime = 0;
    private String text = null;
    private Input.TextInputListener textListener = null;
    ArrayList<TouchEvent> touchEvents = new ArrayList<>();
    private final AndroidTouchHandler touchHandler;
    int[] touchX = new int[20];
    int[] touchY = new int[20];
    boolean[] touched = new boolean[20];
    Pool<KeyEvent> usedKeyEvents = new Pool<KeyEvent>(16, 1000) {
        /* access modifiers changed from: protected */
        public KeyEvent newObject() {
            return new KeyEvent();
        }
    };
    Pool<TouchEvent> usedTouchEvents = new Pool<TouchEvent>(16, 1000) {
        /* access modifiers changed from: protected */
        public TouchEvent newObject() {
            return new TouchEvent();
        }
    };
    protected final Vibrator vibrator;

    static class KeyEvent {
        static final int KEY_DOWN = 0;
        static final int KEY_TYPED = 2;
        static final int KEY_UP = 1;
        char keyChar;
        int keyCode;
        long timeStamp;
        int type;

        KeyEvent() {
        }
    }

    static class TouchEvent {
        static final int TOUCH_DOWN = 0;
        static final int TOUCH_DRAGGED = 2;
        static final int TOUCH_MOVED = 4;
        static final int TOUCH_SCROLLED = 3;
        static final int TOUCH_UP = 1;
        int button;
        int pointer;
        int scrollAmount;
        long timeStamp;
        int type;
        int x;
        int y;

        TouchEvent() {
        }
    }

    public AndroidInput(Application activity, Context context2, Object view, AndroidApplicationConfiguration config2) {
        if (view instanceof View) {
            View v = (View) view;
            v.setOnKeyListener(this);
            v.setOnTouchListener(this);
            v.setFocusable(true);
            v.setFocusableInTouchMode(true);
            v.requestFocus();
        }
        this.config = config2;
        this.onscreenKeyboard = new AndroidOnscreenKeyboard(context2, new Handler(), this);
        for (int i = 0; i < this.realId.length; i++) {
            this.realId[i] = -1;
        }
        this.handle = new Handler();
        this.app = activity;
        this.context = context2;
        this.sleepTime = config2.touchSleepTime;
        this.touchHandler = new AndroidMultiTouchHandler();
        this.hasMultitouch = this.touchHandler.supportsMultitouch(context2);
        this.vibrator = (Vibrator) context2.getSystemService("vibrator");
        int rotation = getRotation();
        Graphics.DisplayMode mode = this.app.getGraphics().getDesktopDisplayMode();
        if (((rotation == 0 || rotation == 180) && mode.width >= mode.height) || ((rotation == 90 || rotation == 270) && mode.width <= mode.height)) {
            this.nativeOrientation = Input.Orientation.Landscape;
        } else {
            this.nativeOrientation = Input.Orientation.Portrait;
        }
    }

    public float getAccelerometerX() {
        return this.accelerometerValues[0];
    }

    public float getAccelerometerY() {
        return this.accelerometerValues[1];
    }

    public float getAccelerometerZ() {
        return this.accelerometerValues[2];
    }

    public void getTextInput(Input.TextInputListener listener, String title, String text2, String hint) {
        final String str = title;
        final String str2 = hint;
        final String str3 = text2;
        final Input.TextInputListener textInputListener = listener;
        this.handle.post(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(AndroidInput.this.context);
                alert.setTitle(str);
                final EditText input = new EditText(AndroidInput.this.context);
                input.setHint(str2);
                input.setText(str3);
                input.setSingleLine();
                alert.setView(input);
                alert.setPositiveButton(AndroidInput.this.context.getString(17039370), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Gdx.app.postRunnable(new Runnable() {
                            public void run() {
                                textInputListener.input(input.getText().toString());
                            }
                        });
                    }
                });
                alert.setNegativeButton(AndroidInput.this.context.getString(17039360), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Gdx.app.postRunnable(new Runnable() {
                            public void run() {
                                textInputListener.canceled();
                            }
                        });
                    }
                });
                alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface arg0) {
                        Gdx.app.postRunnable(new Runnable() {
                            public void run() {
                                textInputListener.canceled();
                            }
                        });
                    }
                });
                alert.show();
            }
        });
    }

    public int getX() {
        int i;
        synchronized (this) {
            i = this.touchX[0];
        }
        return i;
    }

    public int getY() {
        int i;
        synchronized (this) {
            i = this.touchY[0];
        }
        return i;
    }

    public int getX(int pointer) {
        int i;
        synchronized (this) {
            i = this.touchX[pointer];
        }
        return i;
    }

    public int getY(int pointer) {
        int i;
        synchronized (this) {
            i = this.touchY[pointer];
        }
        return i;
    }

    public boolean isTouched(int pointer) {
        boolean z;
        synchronized (this) {
            z = this.touched[pointer];
        }
        return z;
    }

    public synchronized boolean isKeyPressed(int key) {
        boolean z = false;
        synchronized (this) {
            if (key == -1) {
                if (this.keyCount > 0) {
                    z = true;
                }
            } else if (key >= 0 && key < 260) {
                z = this.keys[key];
            }
        }
        return z;
    }

    public synchronized boolean isKeyJustPressed(int key) {
        boolean z;
        if (key == -1) {
            z = this.keyJustPressed;
        } else if (key < 0 || key >= 260) {
            z = false;
        } else {
            z = this.justPressedKeys[key];
        }
        return z;
    }

    public boolean isTouched() {
        boolean z;
        synchronized (this) {
            if (this.hasMultitouch) {
                int pointer = 0;
                while (true) {
                    if (pointer >= 20) {
                        break;
                    } else if (this.touched[pointer]) {
                        z = true;
                        break;
                    } else {
                        pointer++;
                    }
                }
            }
            z = this.touched[0];
        }
        return z;
    }

    public void setInputProcessor(InputProcessor processor2) {
        synchronized (this) {
            this.processor = processor2;
        }
    }

    /* access modifiers changed from: package-private */
    public void processEvents() {
        synchronized (this) {
            this.justTouched = false;
            if (this.keyJustPressed) {
                this.keyJustPressed = false;
                for (int i = 0; i < this.justPressedKeys.length; i++) {
                    this.justPressedKeys[i] = false;
                }
            }
            if (this.processor != null) {
                InputProcessor processor2 = this.processor;
                int len = this.keyEvents.size();
                for (int i2 = 0; i2 < len; i2++) {
                    KeyEvent e = this.keyEvents.get(i2);
                    this.currentEventTimeStamp = e.timeStamp;
                    switch (e.type) {
                        case 0:
                            processor2.keyDown(e.keyCode);
                            this.keyJustPressed = true;
                            this.justPressedKeys[e.keyCode] = true;
                            break;
                        case 1:
                            processor2.keyUp(e.keyCode);
                            break;
                        case 2:
                            processor2.keyTyped(e.keyChar);
                            break;
                    }
                    this.usedKeyEvents.free(e);
                }
                int len2 = this.touchEvents.size();
                for (int i3 = 0; i3 < len2; i3++) {
                    TouchEvent e2 = this.touchEvents.get(i3);
                    this.currentEventTimeStamp = e2.timeStamp;
                    switch (e2.type) {
                        case 0:
                            processor2.touchDown(e2.x, e2.y, e2.pointer, e2.button);
                            this.justTouched = true;
                            break;
                        case 1:
                            processor2.touchUp(e2.x, e2.y, e2.pointer, e2.button);
                            break;
                        case 2:
                            processor2.touchDragged(e2.x, e2.y, e2.pointer);
                            break;
                        case 3:
                            processor2.scrolled(e2.scrollAmount);
                            break;
                        case 4:
                            processor2.mouseMoved(e2.x, e2.y);
                            break;
                    }
                    this.usedTouchEvents.free(e2);
                }
            } else {
                int len3 = this.touchEvents.size();
                for (int i4 = 0; i4 < len3; i4++) {
                    TouchEvent e3 = this.touchEvents.get(i4);
                    if (e3.type == 0) {
                        this.justTouched = true;
                    }
                    this.usedTouchEvents.free(e3);
                }
                int len4 = this.keyEvents.size();
                for (int i5 = 0; i5 < len4; i5++) {
                    this.usedKeyEvents.free(this.keyEvents.get(i5));
                }
            }
            if (this.touchEvents.size() == 0) {
                for (int i6 = 0; i6 < this.deltaX.length; i6++) {
                    this.deltaX[0] = 0;
                    this.deltaY[0] = 0;
                }
            }
            this.keyEvents.clear();
            this.touchEvents.clear();
        }
    }

    public boolean onTouch(View view, MotionEvent event) {
        if (this.requestFocus && view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            this.requestFocus = false;
        }
        this.touchHandler.onTouch(event, this);
        if (this.sleepTime != 0) {
            try {
                Thread.sleep((long) this.sleepTime);
            } catch (InterruptedException e) {
            }
        }
        return true;
    }

    public void onTap(int x, int y) {
        postTap(x, y);
    }

    public void onDrop(int x, int y) {
        postTap(x, y);
    }

    /* access modifiers changed from: protected */
    public void postTap(int x, int y) {
        synchronized (this) {
            TouchEvent event = this.usedTouchEvents.obtain();
            event.timeStamp = System.nanoTime();
            event.pointer = 0;
            event.x = x;
            event.y = y;
            event.type = 0;
            this.touchEvents.add(event);
            TouchEvent event2 = this.usedTouchEvents.obtain();
            event2.timeStamp = System.nanoTime();
            event2.pointer = 0;
            event2.x = x;
            event2.y = y;
            event2.type = 1;
            this.touchEvents.add(event2);
        }
        Gdx.app.getGraphics().requestRendering();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0089, code lost:
        if (r14 != 255) goto L_0x014e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0150, code lost:
        if (r12.catchBack == false) goto L_0x0158;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0153, code lost:
        if (r14 != 4) goto L_0x0158;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0155, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x015a, code lost:
        if (r12.catchMenu == false) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x015e, code lost:
        if (r14 != 82) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0160, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0163, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onKey(android.view.View r13, int r14, android.view.KeyEvent r15) {
        /*
            r12 = this;
            r5 = 0
            java.util.ArrayList<android.view.View$OnKeyListener> r7 = r12.keyListeners
            int r6 = r7.size()
        L_0x0007:
            if (r5 >= r6) goto L_0x001c
            java.util.ArrayList<android.view.View$OnKeyListener> r7 = r12.keyListeners
            java.lang.Object r7 = r7.get(r5)
            android.view.View$OnKeyListener r7 = (android.view.View.OnKeyListener) r7
            boolean r7 = r7.onKey(r13, r14, r15)
            if (r7 == 0) goto L_0x0019
            r7 = 1
        L_0x0018:
            return r7
        L_0x0019:
            int r5 = r5 + 1
            goto L_0x0007
        L_0x001c:
            monitor-enter(r12)
            r4 = 0
            int r7 = r15.getKeyCode()     // Catch:{ all -> 0x005d }
            if (r7 != 0) goto L_0x0060
            int r7 = r15.getAction()     // Catch:{ all -> 0x005d }
            r10 = 2
            if (r7 != r10) goto L_0x0060
            java.lang.String r3 = r15.getCharacters()     // Catch:{ all -> 0x005d }
            r5 = 0
        L_0x0030:
            int r7 = r3.length()     // Catch:{ all -> 0x005d }
            if (r5 >= r7) goto L_0x005a
            com.badlogic.gdx.utils.Pool<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.usedKeyEvents     // Catch:{ all -> 0x005d }
            java.lang.Object r7 = r7.obtain()     // Catch:{ all -> 0x005d }
            r0 = r7
            com.badlogic.gdx.backends.android.AndroidInput$KeyEvent r0 = (com.badlogic.gdx.backends.android.AndroidInput.KeyEvent) r0     // Catch:{ all -> 0x005d }
            r4 = r0
            long r10 = java.lang.System.nanoTime()     // Catch:{ all -> 0x005d }
            r4.timeStamp = r10     // Catch:{ all -> 0x005d }
            r7 = 0
            r4.keyCode = r7     // Catch:{ all -> 0x005d }
            char r7 = r3.charAt(r5)     // Catch:{ all -> 0x005d }
            r4.keyChar = r7     // Catch:{ all -> 0x005d }
            r7 = 2
            r4.type = r7     // Catch:{ all -> 0x005d }
            java.util.ArrayList<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.keyEvents     // Catch:{ all -> 0x005d }
            r7.add(r4)     // Catch:{ all -> 0x005d }
            int r5 = r5 + 1
            goto L_0x0030
        L_0x005a:
            r7 = 0
            monitor-exit(r12)     // Catch:{ all -> 0x005d }
            goto L_0x0018
        L_0x005d:
            r7 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x005d }
            throw r7
        L_0x0060:
            int r7 = r15.getUnicodeChar()     // Catch:{ all -> 0x005d }
            char r2 = (char) r7     // Catch:{ all -> 0x005d }
            r7 = 67
            if (r14 != r7) goto L_0x006b
            r2 = 8
        L_0x006b:
            int r7 = r15.getKeyCode()     // Catch:{ all -> 0x005d }
            r10 = 260(0x104, float:3.64E-43)
            if (r7 < r10) goto L_0x0076
            r7 = 0
            monitor-exit(r12)     // Catch:{ all -> 0x005d }
            goto L_0x0018
        L_0x0076:
            int r7 = r15.getAction()     // Catch:{ all -> 0x005d }
            switch(r7) {
                case 0: goto L_0x008d;
                case 1: goto L_0x00d1;
                default: goto L_0x007d;
            }     // Catch:{ all -> 0x005d }
        L_0x007d:
            com.badlogic.gdx.Application r7 = r12.app     // Catch:{ all -> 0x005d }
            com.badlogic.gdx.Graphics r7 = r7.getGraphics()     // Catch:{ all -> 0x005d }
            r7.requestRendering()     // Catch:{ all -> 0x005d }
            monitor-exit(r12)     // Catch:{ all -> 0x005d }
            r7 = 255(0xff, float:3.57E-43)
            if (r14 != r7) goto L_0x014e
            r7 = 1
            goto L_0x0018
        L_0x008d:
            com.badlogic.gdx.utils.Pool<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.usedKeyEvents     // Catch:{ all -> 0x005d }
            java.lang.Object r7 = r7.obtain()     // Catch:{ all -> 0x005d }
            r0 = r7
            com.badlogic.gdx.backends.android.AndroidInput$KeyEvent r0 = (com.badlogic.gdx.backends.android.AndroidInput.KeyEvent) r0     // Catch:{ all -> 0x005d }
            r4 = r0
            long r10 = java.lang.System.nanoTime()     // Catch:{ all -> 0x005d }
            r4.timeStamp = r10     // Catch:{ all -> 0x005d }
            r7 = 0
            r4.keyChar = r7     // Catch:{ all -> 0x005d }
            int r7 = r15.getKeyCode()     // Catch:{ all -> 0x005d }
            r4.keyCode = r7     // Catch:{ all -> 0x005d }
            r7 = 0
            r4.type = r7     // Catch:{ all -> 0x005d }
            r7 = 4
            if (r14 != r7) goto L_0x00b6
            boolean r7 = r15.isAltPressed()     // Catch:{ all -> 0x005d }
            if (r7 == 0) goto L_0x00b6
            r14 = 255(0xff, float:3.57E-43)
            r4.keyCode = r14     // Catch:{ all -> 0x005d }
        L_0x00b6:
            java.util.ArrayList<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.keyEvents     // Catch:{ all -> 0x005d }
            r7.add(r4)     // Catch:{ all -> 0x005d }
            boolean[] r7 = r12.keys     // Catch:{ all -> 0x005d }
            int r10 = r4.keyCode     // Catch:{ all -> 0x005d }
            boolean r7 = r7[r10]     // Catch:{ all -> 0x005d }
            if (r7 != 0) goto L_0x007d
            int r7 = r12.keyCount     // Catch:{ all -> 0x005d }
            int r7 = r7 + 1
            r12.keyCount = r7     // Catch:{ all -> 0x005d }
            boolean[] r7 = r12.keys     // Catch:{ all -> 0x005d }
            int r10 = r4.keyCode     // Catch:{ all -> 0x005d }
            r11 = 1
            r7[r10] = r11     // Catch:{ all -> 0x005d }
            goto L_0x007d
        L_0x00d1:
            long r8 = java.lang.System.nanoTime()     // Catch:{ all -> 0x005d }
            com.badlogic.gdx.utils.Pool<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.usedKeyEvents     // Catch:{ all -> 0x005d }
            java.lang.Object r7 = r7.obtain()     // Catch:{ all -> 0x005d }
            r0 = r7
            com.badlogic.gdx.backends.android.AndroidInput$KeyEvent r0 = (com.badlogic.gdx.backends.android.AndroidInput.KeyEvent) r0     // Catch:{ all -> 0x005d }
            r4 = r0
            r4.timeStamp = r8     // Catch:{ all -> 0x005d }
            r7 = 0
            r4.keyChar = r7     // Catch:{ all -> 0x005d }
            int r7 = r15.getKeyCode()     // Catch:{ all -> 0x005d }
            r4.keyCode = r7     // Catch:{ all -> 0x005d }
            r7 = 1
            r4.type = r7     // Catch:{ all -> 0x005d }
            r7 = 4
            if (r14 != r7) goto L_0x00fa
            boolean r7 = r15.isAltPressed()     // Catch:{ all -> 0x005d }
            if (r7 == 0) goto L_0x00fa
            r14 = 255(0xff, float:3.57E-43)
            r4.keyCode = r14     // Catch:{ all -> 0x005d }
        L_0x00fa:
            java.util.ArrayList<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.keyEvents     // Catch:{ all -> 0x005d }
            r7.add(r4)     // Catch:{ all -> 0x005d }
            com.badlogic.gdx.utils.Pool<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.usedKeyEvents     // Catch:{ all -> 0x005d }
            java.lang.Object r7 = r7.obtain()     // Catch:{ all -> 0x005d }
            r0 = r7
            com.badlogic.gdx.backends.android.AndroidInput$KeyEvent r0 = (com.badlogic.gdx.backends.android.AndroidInput.KeyEvent) r0     // Catch:{ all -> 0x005d }
            r4 = r0
            r4.timeStamp = r8     // Catch:{ all -> 0x005d }
            r4.keyChar = r2     // Catch:{ all -> 0x005d }
            r7 = 0
            r4.keyCode = r7     // Catch:{ all -> 0x005d }
            r7 = 2
            r4.type = r7     // Catch:{ all -> 0x005d }
            java.util.ArrayList<com.badlogic.gdx.backends.android.AndroidInput$KeyEvent> r7 = r12.keyEvents     // Catch:{ all -> 0x005d }
            r7.add(r4)     // Catch:{ all -> 0x005d }
            r7 = 255(0xff, float:3.57E-43)
            if (r14 != r7) goto L_0x0133
            boolean[] r7 = r12.keys     // Catch:{ all -> 0x005d }
            r10 = 255(0xff, float:3.57E-43)
            boolean r7 = r7[r10]     // Catch:{ all -> 0x005d }
            if (r7 == 0) goto L_0x007d
            int r7 = r12.keyCount     // Catch:{ all -> 0x005d }
            int r7 = r7 + -1
            r12.keyCount = r7     // Catch:{ all -> 0x005d }
            boolean[] r7 = r12.keys     // Catch:{ all -> 0x005d }
            r10 = 255(0xff, float:3.57E-43)
            r11 = 0
            r7[r10] = r11     // Catch:{ all -> 0x005d }
            goto L_0x007d
        L_0x0133:
            boolean[] r7 = r12.keys     // Catch:{ all -> 0x005d }
            int r10 = r15.getKeyCode()     // Catch:{ all -> 0x005d }
            boolean r7 = r7[r10]     // Catch:{ all -> 0x005d }
            if (r7 == 0) goto L_0x007d
            int r7 = r12.keyCount     // Catch:{ all -> 0x005d }
            int r7 = r7 + -1
            r12.keyCount = r7     // Catch:{ all -> 0x005d }
            boolean[] r7 = r12.keys     // Catch:{ all -> 0x005d }
            int r10 = r15.getKeyCode()     // Catch:{ all -> 0x005d }
            r11 = 0
            r7[r10] = r11     // Catch:{ all -> 0x005d }
            goto L_0x007d
        L_0x014e:
            boolean r7 = r12.catchBack
            if (r7 == 0) goto L_0x0158
            r7 = 4
            if (r14 != r7) goto L_0x0158
            r7 = 1
            goto L_0x0018
        L_0x0158:
            boolean r7 = r12.catchMenu
            if (r7 == 0) goto L_0x0163
            r7 = 82
            if (r14 != r7) goto L_0x0163
            r7 = 1
            goto L_0x0018
        L_0x0163:
            r7 = 0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.AndroidInput.onKey(android.view.View, int, android.view.KeyEvent):boolean");
    }

    public void setOnscreenKeyboardVisible(final boolean visible) {
        this.handle.post(new Runnable() {
            public void run() {
                InputMethodManager manager = (InputMethodManager) AndroidInput.this.context.getSystemService("input_method");
                if (visible) {
                    View view = ((AndroidGraphics) AndroidInput.this.app.getGraphics()).getView();
                    view.setFocusable(true);
                    view.setFocusableInTouchMode(true);
                    manager.showSoftInput(((AndroidGraphics) AndroidInput.this.app.getGraphics()).getView(), 0);
                    return;
                }
                manager.hideSoftInputFromWindow(((AndroidGraphics) AndroidInput.this.app.getGraphics()).getView().getWindowToken(), 0);
            }
        });
    }

    public void setCatchBackKey(boolean catchBack2) {
        this.catchBack = catchBack2;
    }

    public boolean isCatchBackKey() {
        return this.catchBack;
    }

    public void setCatchMenuKey(boolean catchMenu2) {
        this.catchMenu = catchMenu2;
    }

    public void vibrate(int milliseconds) {
        this.vibrator.vibrate((long) milliseconds);
    }

    public void vibrate(long[] pattern, int repeat) {
        this.vibrator.vibrate(pattern, repeat);
    }

    public void cancelVibrate() {
        this.vibrator.cancel();
    }

    public boolean justTouched() {
        return this.justTouched;
    }

    public boolean isButtonPressed(int button2) {
        boolean z = true;
        synchronized (this) {
            if (this.hasMultitouch) {
                int pointer = 0;
                while (true) {
                    if (pointer < 20) {
                        if (this.touched[pointer] && this.button[pointer] == button2) {
                            break;
                        }
                        pointer++;
                    } else {
                        break;
                    }
                }
            }
            if (!this.touched[0] || this.button[0] != button2) {
                z = false;
            }
        }
        return z;
    }

    private void updateOrientation() {
        if (SensorManager.getRotationMatrix(this.R, null, this.accelerometerValues, this.magneticFieldValues)) {
            SensorManager.getOrientation(this.R, this.orientation);
            this.azimuth = (float) Math.toDegrees((double) this.orientation[0]);
            this.pitch = (float) Math.toDegrees((double) this.orientation[1]);
            this.roll = (float) Math.toDegrees((double) this.orientation[2]);
        }
    }

    public void getRotationMatrix(float[] matrix) {
        SensorManager.getRotationMatrix(matrix, null, this.accelerometerValues, this.magneticFieldValues);
    }

    public float getAzimuth() {
        if (!this.compassAvailable) {
            return Animation.CurveTimeline.LINEAR;
        }
        updateOrientation();
        return this.azimuth;
    }

    public float getPitch() {
        if (!this.compassAvailable) {
            return Animation.CurveTimeline.LINEAR;
        }
        updateOrientation();
        return this.pitch;
    }

    public float getRoll() {
        if (!this.compassAvailable) {
            return Animation.CurveTimeline.LINEAR;
        }
        updateOrientation();
        return this.roll;
    }

    /* access modifiers changed from: package-private */
    public void registerSensorListeners() {
        if (this.config.useAccelerometer) {
            this.manager = (SensorManager) this.context.getSystemService("sensor");
            if (this.manager.getSensorList(1).size() == 0) {
                this.accelerometerAvailable = false;
            } else {
                this.accelerometerListener = new SensorListener(this.nativeOrientation, this.accelerometerValues, this.magneticFieldValues);
                this.accelerometerAvailable = this.manager.registerListener(this.accelerometerListener, this.manager.getSensorList(1).get(0), 1);
            }
        } else {
            this.accelerometerAvailable = false;
        }
        if (this.config.useCompass) {
            if (this.manager == null) {
                this.manager = (SensorManager) this.context.getSystemService("sensor");
            }
            Sensor sensor = this.manager.getDefaultSensor(2);
            if (sensor != null) {
                this.compassAvailable = this.accelerometerAvailable;
                if (this.compassAvailable) {
                    this.compassListener = new SensorListener(this.nativeOrientation, this.accelerometerValues, this.magneticFieldValues);
                    this.compassAvailable = this.manager.registerListener(this.compassListener, sensor, 1);
                }
            } else {
                this.compassAvailable = false;
            }
        } else {
            this.compassAvailable = false;
        }
        Gdx.app.log("AndroidInput", "sensor listener setup");
    }

    /* access modifiers changed from: package-private */
    public void unregisterSensorListeners() {
        if (this.manager != null) {
            if (this.accelerometerListener != null) {
                this.manager.unregisterListener(this.accelerometerListener);
                this.accelerometerListener = null;
            }
            if (this.compassListener != null) {
                this.manager.unregisterListener(this.compassListener);
                this.compassListener = null;
            }
            this.manager = null;
        }
        Gdx.app.log("AndroidInput", "sensor listener tear down");
    }

    public InputProcessor getInputProcessor() {
        return this.processor;
    }

    public boolean isPeripheralAvailable(Input.Peripheral peripheral) {
        if (peripheral == Input.Peripheral.Accelerometer) {
            return this.accelerometerAvailable;
        }
        if (peripheral == Input.Peripheral.Compass) {
            return this.compassAvailable;
        }
        if (peripheral == Input.Peripheral.HardwareKeyboard) {
            return this.keyboardAvailable;
        }
        if (peripheral == Input.Peripheral.OnscreenKeyboard) {
            return true;
        }
        if (peripheral == Input.Peripheral.Vibrator) {
            if (this.vibrator == null) {
                return false;
            }
            return true;
        } else if (peripheral == Input.Peripheral.MultitouchScreen) {
            return this.hasMultitouch;
        } else {
            return false;
        }
    }

    public int getFreePointerIndex() {
        int len = this.realId.length;
        for (int i = 0; i < len; i++) {
            if (this.realId[i] == -1) {
                return i;
            }
        }
        this.realId = resize(this.realId);
        this.touchX = resize(this.touchX);
        this.touchY = resize(this.touchY);
        this.deltaX = resize(this.deltaX);
        this.deltaY = resize(this.deltaY);
        this.touched = resize(this.touched);
        this.button = resize(this.button);
        return len;
    }

    private int[] resize(int[] orig) {
        int[] tmp = new int[(orig.length + 2)];
        System.arraycopy(orig, 0, tmp, 0, orig.length);
        return tmp;
    }

    private boolean[] resize(boolean[] orig) {
        boolean[] tmp = new boolean[(orig.length + 2)];
        System.arraycopy(orig, 0, tmp, 0, orig.length);
        return tmp;
    }

    public int lookUpPointerIndex(int pointerId) {
        int len = this.realId.length;
        for (int i = 0; i < len; i++) {
            if (this.realId[i] == pointerId) {
                return i;
            }
        }
        StringBuffer buf = new StringBuffer();
        for (int i2 = 0; i2 < len; i2++) {
            buf.append(i2 + ":" + this.realId[i2] + " ");
        }
        Gdx.app.log("AndroidInput", "Pointer ID lookup failed: " + pointerId + ", " + buf.toString());
        return -1;
    }

    public int getRotation() {
        int orientation2;
        if (this.context instanceof Activity) {
            orientation2 = ((Activity) this.context).getWindowManager().getDefaultDisplay().getRotation();
        } else {
            orientation2 = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay().getRotation();
        }
        switch (orientation2) {
            case 0:
                return 0;
            case 1:
                return 90;
            case 2:
                return 180;
            case 3:
                return PAK_ASSETS.IMG_BISHATIPSBLUE;
            default:
                return 0;
        }
    }

    public Input.Orientation getNativeOrientation() {
        return this.nativeOrientation;
    }

    public void setCursorCatched(boolean catched) {
    }

    public boolean isCursorCatched() {
        return false;
    }

    public int getDeltaX() {
        return this.deltaX[0];
    }

    public int getDeltaX(int pointer) {
        return this.deltaX[pointer];
    }

    public int getDeltaY() {
        return this.deltaY[0];
    }

    public int getDeltaY(int pointer) {
        return this.deltaY[pointer];
    }

    public void setCursorPosition(int x, int y) {
    }

    public void setCursorImage(Pixmap pixmap, int xHotspot, int yHotspot) {
    }

    public long getCurrentEventTime() {
        return this.currentEventTimeStamp;
    }

    public void addKeyListener(View.OnKeyListener listener) {
        this.keyListeners.add(listener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
     arg types: [boolean[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
    public void onPause() {
        unregisterSensorListeners();
        Arrays.fill(this.realId, -1);
        Arrays.fill(this.touched, false);
    }

    public void onResume() {
        registerSensorListeners();
    }

    private class SensorListener implements SensorEventListener {
        final float[] accelerometerValues;
        final float[] magneticFieldValues;
        final Input.Orientation nativeOrientation;

        SensorListener(Input.Orientation nativeOrientation2, float[] accelerometerValues2, float[] magneticFieldValues2) {
            this.accelerometerValues = accelerometerValues2;
            this.magneticFieldValues = magneticFieldValues2;
            this.nativeOrientation = nativeOrientation2;
        }

        public void onAccuracyChanged(Sensor arg0, int arg1) {
        }

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == 1) {
                if (this.nativeOrientation == Input.Orientation.Portrait) {
                    System.arraycopy(event.values, 0, this.accelerometerValues, 0, this.accelerometerValues.length);
                } else {
                    this.accelerometerValues[0] = event.values[1];
                    this.accelerometerValues[1] = -event.values[0];
                    this.accelerometerValues[2] = event.values[2];
                }
            }
            if (event.sensor.getType() == 2) {
                System.arraycopy(event.values, 0, this.magneticFieldValues, 0, this.magneticFieldValues.length);
            }
        }
    }
}
