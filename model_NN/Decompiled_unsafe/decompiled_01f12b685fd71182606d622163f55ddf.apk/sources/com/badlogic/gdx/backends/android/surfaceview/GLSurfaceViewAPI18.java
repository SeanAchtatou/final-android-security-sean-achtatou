package com.badlogic.gdx.backends.android.surfaceview;

import android.content.Context;
import android.opengl.GLDebugHelper;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.badlogic.gdx.graphics.GL20;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLSurfaceViewAPI18 extends SurfaceView implements SurfaceHolder.Callback {
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    private static final boolean LOG_ATTACH_DETACH = false;
    private static final boolean LOG_EGL = false;
    private static final boolean LOG_PAUSE_RESUME = false;
    private static final boolean LOG_RENDERER = false;
    private static final boolean LOG_RENDERER_DRAW_FRAME = false;
    private static final boolean LOG_SURFACE = false;
    private static final boolean LOG_THREADS = false;
    public static final int RENDERMODE_CONTINUOUSLY = 1;
    public static final int RENDERMODE_WHEN_DIRTY = 0;
    private static final String TAG = "GLSurfaceViewAPI18";
    /* access modifiers changed from: private */
    public static final GLThreadManager sGLThreadManager = new GLThreadManager();
    /* access modifiers changed from: private */
    public int mDebugFlags;
    private boolean mDetached;
    /* access modifiers changed from: private */
    public GLSurfaceView.EGLConfigChooser mEGLConfigChooser;
    /* access modifiers changed from: private */
    public int mEGLContextClientVersion;
    /* access modifiers changed from: private */
    public EGLContextFactory mEGLContextFactory;
    /* access modifiers changed from: private */
    public EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private GLThread mGLThread;
    /* access modifiers changed from: private */
    public GLWrapper mGLWrapper;
    /* access modifiers changed from: private */
    public boolean mPreserveEGLContextOnPause;
    /* access modifiers changed from: private */
    public GLSurfaceView.Renderer mRenderer;
    private final WeakReference<GLSurfaceViewAPI18> mThisWeakRef = new WeakReference<>(this);

    public interface EGLContextFactory {
        EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig);

        void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext);
    }

    public interface EGLWindowSurfaceFactory {
        EGLSurface createWindowSurface(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj);

        void destroySurface(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface);
    }

    public interface GLWrapper {
        GL wrap(GL gl);
    }

    public GLSurfaceViewAPI18(Context context) {
        super(context);
        init();
    }

    public GLSurfaceViewAPI18(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            if (this.mGLThread != null) {
                this.mGLThread.requestExitAndWait();
            }
        } finally {
            super.finalize();
        }
    }

    private void init() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        if (Build.VERSION.SDK_INT <= 8) {
            holder.setFormat(4);
        }
    }

    public void setGLWrapper(GLWrapper glWrapper) {
        this.mGLWrapper = glWrapper;
    }

    public void setDebugFlags(int debugFlags) {
        this.mDebugFlags = debugFlags;
    }

    public int getDebugFlags() {
        return this.mDebugFlags;
    }

    public void setPreserveEGLContextOnPause(boolean preserveOnPause) {
        this.mPreserveEGLContextOnPause = preserveOnPause;
    }

    public boolean getPreserveEGLContextOnPause() {
        return this.mPreserveEGLContextOnPause;
    }

    public void setRenderer(GLSurfaceView.Renderer renderer) {
        checkRenderThreadState();
        if (this.mEGLConfigChooser == null) {
            this.mEGLConfigChooser = new SimpleEGLConfigChooser(true);
        }
        if (this.mEGLContextFactory == null) {
            this.mEGLContextFactory = new DefaultContextFactory();
        }
        if (this.mEGLWindowSurfaceFactory == null) {
            this.mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory();
        }
        this.mRenderer = renderer;
        this.mGLThread = new GLThread(this.mThisWeakRef);
        this.mGLThread.start();
    }

    public void setEGLContextFactory(EGLContextFactory factory) {
        checkRenderThreadState();
        this.mEGLContextFactory = factory;
    }

    public void setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory factory) {
        checkRenderThreadState();
        this.mEGLWindowSurfaceFactory = factory;
    }

    public void setEGLConfigChooser(GLSurfaceView.EGLConfigChooser configChooser) {
        checkRenderThreadState();
        this.mEGLConfigChooser = configChooser;
    }

    public void setEGLConfigChooser(boolean needDepth) {
        setEGLConfigChooser(new SimpleEGLConfigChooser(needDepth));
    }

    public void setEGLConfigChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize, int stencilSize) {
        setEGLConfigChooser(new ComponentSizeChooser(redSize, greenSize, blueSize, alphaSize, depthSize, stencilSize));
    }

    public void setEGLContextClientVersion(int version) {
        checkRenderThreadState();
        this.mEGLContextClientVersion = version;
    }

    public void setRenderMode(int renderMode) {
        this.mGLThread.setRenderMode(renderMode);
    }

    public int getRenderMode() {
        return this.mGLThread.getRenderMode();
    }

    public void requestRender() {
        this.mGLThread.requestRender();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.mGLThread.surfaceDestroyed();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        this.mGLThread.onWindowResize(w, h);
    }

    public void onPause() {
        this.mGLThread.onPause();
    }

    public void onResume() {
        this.mGLThread.onResume();
    }

    public void queueEvent(Runnable r) {
        this.mGLThread.queueEvent(r);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mDetached && this.mRenderer != null) {
            int renderMode = 1;
            if (this.mGLThread != null) {
                renderMode = this.mGLThread.getRenderMode();
            }
            this.mGLThread = new GLThread(this.mThisWeakRef);
            if (renderMode != 1) {
                this.mGLThread.setRenderMode(renderMode);
            }
            this.mGLThread.start();
        }
        this.mDetached = false;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.mGLThread != null) {
            this.mGLThread.requestExitAndWait();
        }
        this.mDetached = true;
        super.onDetachedFromWindow();
    }

    private class DefaultContextFactory implements EGLContextFactory {
        private int EGL_CONTEXT_CLIENT_VERSION;

        private DefaultContextFactory() {
            this.EGL_CONTEXT_CLIENT_VERSION = 12440;
        }

        public EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig config) {
            int[] attrib_list = {this.EGL_CONTEXT_CLIENT_VERSION, GLSurfaceViewAPI18.this.mEGLContextClientVersion, 12344};
            EGLContext eGLContext = EGL10.EGL_NO_CONTEXT;
            if (GLSurfaceViewAPI18.this.mEGLContextClientVersion == 0) {
                attrib_list = null;
            }
            return egl.eglCreateContext(display, config, eGLContext, attrib_list);
        }

        public void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context) {
            if (!egl.eglDestroyContext(display, context)) {
                Log.e("DefaultContextFactory", "display:" + display + " context: " + context);
                EglHelper.throwEglException("eglDestroyContex", egl.eglGetError());
            }
        }
    }

    private static class DefaultWindowSurfaceFactory implements EGLWindowSurfaceFactory {
        private DefaultWindowSurfaceFactory() {
        }

        public EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config, Object nativeWindow) {
            try {
                return egl.eglCreateWindowSurface(display, config, nativeWindow, null);
            } catch (IllegalArgumentException e) {
                Log.e(GLSurfaceViewAPI18.TAG, "eglCreateWindowSurface", e);
                return null;
            }
        }

        public void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface) {
            egl.eglDestroySurface(display, surface);
        }
    }

    private abstract class BaseConfigChooser implements GLSurfaceView.EGLConfigChooser {
        protected int[] mConfigSpec;

        /* access modifiers changed from: package-private */
        public abstract EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);

        public BaseConfigChooser(int[] configSpec) {
            this.mConfigSpec = filterConfigSpec(configSpec);
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
            int[] num_config = new int[1];
            if (!egl.eglChooseConfig(display, this.mConfigSpec, null, 0, num_config)) {
                throw new IllegalArgumentException("eglChooseConfig failed");
            }
            int numConfigs = num_config[0];
            if (numConfigs <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] configs = new EGLConfig[numConfigs];
            if (!egl.eglChooseConfig(display, this.mConfigSpec, configs, numConfigs, num_config)) {
                throw new IllegalArgumentException("eglChooseConfig#2 failed");
            }
            EGLConfig config = chooseConfig(egl, display, configs);
            if (config != null) {
                return config;
            }
            throw new IllegalArgumentException("No config chosen");
        }

        private int[] filterConfigSpec(int[] configSpec) {
            if (GLSurfaceViewAPI18.this.mEGLContextClientVersion != 2) {
                return configSpec;
            }
            int len = configSpec.length;
            int[] newConfigSpec = new int[(len + 2)];
            System.arraycopy(configSpec, 0, newConfigSpec, 0, len - 1);
            newConfigSpec[len - 1] = 12352;
            newConfigSpec[len] = 4;
            newConfigSpec[len + 1] = 12344;
            return newConfigSpec;
        }
    }

    private class ComponentSizeChooser extends BaseConfigChooser {
        protected int mAlphaSize;
        protected int mBlueSize;
        protected int mDepthSize;
        protected int mGreenSize;
        protected int mRedSize;
        protected int mStencilSize;
        private int[] mValue = new int[1];

        public ComponentSizeChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize, int stencilSize) {
            super(new int[]{12324, redSize, 12323, greenSize, 12322, blueSize, 12321, alphaSize, 12325, depthSize, 12326, stencilSize, 12344});
            this.mRedSize = redSize;
            this.mGreenSize = greenSize;
            this.mBlueSize = blueSize;
            this.mAlphaSize = alphaSize;
            this.mDepthSize = depthSize;
            this.mStencilSize = stencilSize;
        }

        public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs) {
            for (EGLConfig config : configs) {
                int d = findConfigAttrib(egl, display, config, 12325, 0);
                int s = findConfigAttrib(egl, display, config, 12326, 0);
                if (d >= this.mDepthSize && s >= this.mStencilSize) {
                    int r = findConfigAttrib(egl, display, config, 12324, 0);
                    int g = findConfigAttrib(egl, display, config, 12323, 0);
                    int b = findConfigAttrib(egl, display, config, 12322, 0);
                    int a = findConfigAttrib(egl, display, config, 12321, 0);
                    if (r == this.mRedSize && g == this.mGreenSize && b == this.mBlueSize && a == this.mAlphaSize) {
                        return config;
                    }
                }
            }
            return null;
        }

        private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue) {
            if (egl.eglGetConfigAttrib(display, config, attribute, this.mValue)) {
                return this.mValue[0];
            }
            return defaultValue;
        }
    }

    private class SimpleEGLConfigChooser extends ComponentSizeChooser {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SimpleEGLConfigChooser(boolean r10) {
            /*
                r8 = this;
                r2 = 8
                r5 = 0
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.this = r9
                if (r10 == 0) goto L_0x0012
                r6 = 16
            L_0x0009:
                r0 = r8
                r1 = r9
                r3 = r2
                r4 = r2
                r7 = r5
                r0.<init>(r2, r3, r4, r5, r6, r7)
                return
            L_0x0012:
                r6 = r5
                goto L_0x0009
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.SimpleEGLConfigChooser.<init>(com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18, boolean):void");
        }
    }

    private static class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;
        private WeakReference<GLSurfaceViewAPI18> mGLSurfaceViewWeakRef;

        public EglHelper(WeakReference<GLSurfaceViewAPI18> glSurfaceViewWeakRef) {
            this.mGLSurfaceViewWeakRef = glSurfaceViewWeakRef;
        }

        public void start() {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            if (this.mEglDisplay == EGL10.EGL_NO_DISPLAY) {
                throw new RuntimeException("eglGetDisplay failed");
            }
            if (!this.mEgl.eglInitialize(this.mEglDisplay, new int[2])) {
                throw new RuntimeException("eglInitialize failed");
            }
            GLSurfaceViewAPI18 view = this.mGLSurfaceViewWeakRef.get();
            if (view == null) {
                this.mEglConfig = null;
                this.mEglContext = null;
            } else {
                this.mEglConfig = view.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
                this.mEglContext = view.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig);
            }
            if (this.mEglContext == null || this.mEglContext == EGL10.EGL_NO_CONTEXT) {
                this.mEglContext = null;
                throwEglException("createContext");
            }
            this.mEglSurface = null;
        }

        public boolean createSurface() {
            if (this.mEgl == null) {
                throw new RuntimeException("egl not initialized");
            } else if (this.mEglDisplay == null) {
                throw new RuntimeException("eglDisplay not initialized");
            } else if (this.mEglConfig == null) {
                throw new RuntimeException("mEglConfig not initialized");
            } else {
                destroySurfaceImp();
                GLSurfaceViewAPI18 view = this.mGLSurfaceViewWeakRef.get();
                if (view != null) {
                    this.mEglSurface = view.mEGLWindowSurfaceFactory.createWindowSurface(this.mEgl, this.mEglDisplay, this.mEglConfig, view.getHolder());
                } else {
                    this.mEglSurface = null;
                }
                if (this.mEglSurface == null || this.mEglSurface == EGL10.EGL_NO_SURFACE) {
                    if (this.mEgl.eglGetError() != 12299) {
                        return false;
                    }
                    Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
                    return false;
                } else if (this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext)) {
                    return true;
                } else {
                    logEglErrorAsWarning("EGLHelper", "eglMakeCurrent", this.mEgl.eglGetError());
                    return false;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public GL createGL() {
            GL gl = this.mEglContext.getGL();
            GLSurfaceViewAPI18 view = this.mGLSurfaceViewWeakRef.get();
            if (view == null) {
                return gl;
            }
            if (view.mGLWrapper != null) {
                gl = view.mGLWrapper.wrap(gl);
            }
            if ((view.mDebugFlags & 3) == 0) {
                return gl;
            }
            int configFlags = 0;
            Writer log = null;
            if ((view.mDebugFlags & 1) != 0) {
                configFlags = 0 | 1;
            }
            if ((view.mDebugFlags & 2) != 0) {
                log = new LogWriter();
            }
            return GLDebugHelper.wrap(gl, configFlags, log);
        }

        public int swap() {
            if (!this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface)) {
                return this.mEgl.eglGetError();
            }
            return 12288;
        }

        public void destroySurface() {
            destroySurfaceImp();
        }

        private void destroySurfaceImp() {
            if (this.mEglSurface != null && this.mEglSurface != EGL10.EGL_NO_SURFACE) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                GLSurfaceViewAPI18 view = this.mGLSurfaceViewWeakRef.get();
                if (view != null) {
                    view.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface);
                }
                this.mEglSurface = null;
            }
        }

        public void finish() {
            if (this.mEglContext != null) {
                GLSurfaceViewAPI18 view = this.mGLSurfaceViewWeakRef.get();
                if (view != null) {
                    view.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext);
                }
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }

        private void throwEglException(String function) {
            throwEglException(function, this.mEgl.eglGetError());
        }

        public static void throwEglException(String function, int error) {
            throw new RuntimeException(formatEglError(function, error));
        }

        public static void logEglErrorAsWarning(String tag, String function, int error) {
            Log.w(tag, formatEglError(function, error));
        }

        private static String getErrorString(int error) {
            switch (error) {
                case 12288:
                    return "EGL_SUCCESS";
                case 12289:
                    return "EGL_NOT_INITIALIZED";
                case 12290:
                    return "EGL_BAD_ACCESS";
                case 12291:
                    return "EGL_BAD_ALLOC";
                case 12292:
                    return "EGL_BAD_ATTRIBUTE";
                case 12293:
                    return "EGL_BAD_CONFIG";
                case 12294:
                    return "EGL_BAD_CONTEXT";
                case 12295:
                    return "EGL_BAD_CURRENT_SURFACE";
                case 12296:
                    return "EGL_BAD_DISPLAY";
                case 12297:
                    return "EGL_BAD_MATCH";
                case 12298:
                    return "EGL_BAD_NATIVE_PIXMAP";
                case 12299:
                    return "EGL_BAD_NATIVE_WINDOW";
                case 12300:
                    return "EGL_BAD_PARAMETER";
                case 12301:
                    return "EGL_BAD_SURFACE";
                case 12302:
                    return "EGL_CONTEXT_LOST";
                default:
                    return "0x" + Integer.toHexString(error);
            }
        }

        public static String formatEglError(String function, int error) {
            return function + " failed: " + getErrorString(error);
        }
    }

    static class GLThread extends Thread {
        private EglHelper mEglHelper;
        private ArrayList<Runnable> mEventQueue = new ArrayList<>();
        /* access modifiers changed from: private */
        public boolean mExited;
        private boolean mFinishedCreatingEglSurface;
        private WeakReference<GLSurfaceViewAPI18> mGLSurfaceViewWeakRef;
        private boolean mHasSurface;
        private boolean mHaveEglContext;
        private boolean mHaveEglSurface;
        private int mHeight = 0;
        private boolean mPaused;
        private boolean mRenderComplete;
        private int mRenderMode = 1;
        private boolean mRequestPaused;
        private boolean mRequestRender = true;
        private boolean mShouldExit;
        private boolean mShouldReleaseEglContext;
        private boolean mSizeChanged = true;
        private boolean mSurfaceIsBad;
        private boolean mWaitingForSurface;
        private int mWidth = 0;

        GLThread(WeakReference<GLSurfaceViewAPI18> glSurfaceViewWeakRef) {
            this.mGLSurfaceViewWeakRef = glSurfaceViewWeakRef;
        }

        public void run() {
            setName("GLThread " + getId());
            try {
                guardedRun();
            } catch (InterruptedException e) {
            } finally {
                GLSurfaceViewAPI18.sGLThreadManager.threadExiting(this);
            }
        }

        private void stopEglSurfaceLocked() {
            if (this.mHaveEglSurface) {
                this.mHaveEglSurface = false;
                this.mEglHelper.destroySurface();
            }
        }

        private void stopEglContextLocked() {
            if (this.mHaveEglContext) {
                this.mEglHelper.finish();
                this.mHaveEglContext = false;
                GLSurfaceViewAPI18.sGLThreadManager.releaseEglContextLocked(this);
            }
        }

        /* JADX WARN: Type inference failed for: r19v66, types: [javax.microedition.khronos.opengles.GL] */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x0228, code lost:
            if (r4 == false) goto L_0x024c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x0234, code lost:
            if (r0.mEglHelper.createSurface() == false) goto L_0x02f7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x0236, code lost:
            r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x023a, code lost:
            monitor-enter(r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:?, code lost:
            r1.mFinishedCreatingEglSurface = true;
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x024a, code lost:
            monitor-exit(r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x024b, code lost:
            r4 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x024c, code lost:
            if (r5 == false) goto L_0x0267;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:?, code lost:
            r8 = r0.mEglHelper.createGL();
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800().checkGLDriver(r8);
            r5 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x0267, code lost:
            if (r3 == false) goto L_0x028f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x0269, code lost:
            r16 = r0.mGLSurfaceViewWeakRef.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x0275, code lost:
            if (r16 == null) goto L_0x028e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x0277, code lost:
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$1000(r16).onSurfaceCreated(r8, r0.mEglHelper.mEglConfig);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x028e, code lost:
            r3 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:134:0x028f, code lost:
            if (r13 == false) goto L_0x02ab;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:135:0x0291, code lost:
            r16 = r0.mGLSurfaceViewWeakRef.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x029d, code lost:
            if (r16 == null) goto L_0x02aa;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x029f, code lost:
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$1000(r16).onSurfaceChanged(r8, r17, r9);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:138:0x02aa, code lost:
            r13 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:139:0x02ab, code lost:
            r16 = r0.mGLSurfaceViewWeakRef.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:140:0x02b7, code lost:
            if (r16 == null) goto L_0x02c2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:141:0x02b9, code lost:
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$1000(r16).onDrawFrame(r8);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:142:0x02c2, code lost:
            r14 = r0.mEglHelper.swap();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:143:0x02cc, code lost:
            switch(r14) {
                case 12288: goto L_0x02ef;
                case 12302: goto L_0x0319;
                default: goto L_0x02cf;
            };
         */
        /* JADX WARNING: Code restructure failed: missing block: B:144:0x02cf, code lost:
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.EglHelper.logEglErrorAsWarning("GLThread", "eglSwapBuffers", r14);
            r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:145:0x02de, code lost:
            monitor-enter(r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:148:?, code lost:
            r1.mSurfaceIsBad = true;
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:149:0x02ee, code lost:
            monitor-exit(r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:150:0x02ef, code lost:
            if (r18 == false) goto L_0x002f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:151:0x02f1, code lost:
            r6 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:157:0x02f7, code lost:
            r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:158:0x02fb, code lost:
            monitor-enter(r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
            r1.mFinishedCreatingEglSurface = true;
            r1.mSurfaceIsBad = true;
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.access$800().notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:162:0x0313, code lost:
            monitor-exit(r20);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:168:0x0319, code lost:
            r10 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x006f, code lost:
            if (r7 == null) goto L_0x0228;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            r7.run();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0074, code lost:
            r7 = null;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void guardedRun() throws java.lang.InterruptedException {
            /*
                r22 = this;
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r19 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper
                r0 = r22
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18> r0 = r0.mGLSurfaceViewWeakRef
                r20 = r0
                r19.<init>(r20)
                r0 = r19
                r1 = r22
                r1.mEglHelper = r0
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mHaveEglContext = r0
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mHaveEglSurface = r0
                r8 = 0
                r3 = 0
                r4 = 0
                r5 = 0
                r10 = 0
                r13 = 0
                r18 = 0
                r6 = 0
                r2 = 0
                r17 = 0
                r9 = 0
                r7 = 0
            L_0x002f:
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d5 }
                monitor-enter(r20)     // Catch:{ all -> 0x01d5 }
            L_0x0034:
                r0 = r22
                boolean r0 = r0.mShouldExit     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x004d
                monitor-exit(r20)     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager
                monitor-enter(r20)
                r22.stopEglSurfaceLocked()     // Catch:{ all -> 0x004a }
                r22.stopEglContextLocked()     // Catch:{ all -> 0x004a }
                monitor-exit(r20)     // Catch:{ all -> 0x004a }
                return
            L_0x004a:
                r19 = move-exception
                monitor-exit(r20)     // Catch:{ all -> 0x004a }
                throw r19
            L_0x004d:
                r0 = r22
                java.util.ArrayList<java.lang.Runnable> r0 = r0.mEventQueue     // Catch:{ all -> 0x01d2 }
                r19 = r0
                boolean r19 = r19.isEmpty()     // Catch:{ all -> 0x01d2 }
                if (r19 != 0) goto L_0x0076
                r0 = r22
                java.util.ArrayList<java.lang.Runnable> r0 = r0.mEventQueue     // Catch:{ all -> 0x01d2 }
                r19 = r0
                r21 = 0
                r0 = r19
                r1 = r21
                java.lang.Object r19 = r0.remove(r1)     // Catch:{ all -> 0x01d2 }
                r0 = r19
                java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ all -> 0x01d2 }
                r7 = r0
            L_0x006e:
                monitor-exit(r20)     // Catch:{ all -> 0x01d2 }
                if (r7 == 0) goto L_0x0228
                r7.run()     // Catch:{ all -> 0x01d5 }
                r7 = 0
                goto L_0x002f
            L_0x0076:
                r11 = 0
                r0 = r22
                boolean r0 = r0.mPaused     // Catch:{ all -> 0x01d2 }
                r19 = r0
                r0 = r22
                boolean r0 = r0.mRequestPaused     // Catch:{ all -> 0x01d2 }
                r21 = r0
                r0 = r19
                r1 = r21
                if (r0 == r1) goto L_0x00a0
                r0 = r22
                boolean r11 = r0.mRequestPaused     // Catch:{ all -> 0x01d2 }
                r0 = r22
                boolean r0 = r0.mRequestPaused     // Catch:{ all -> 0x01d2 }
                r19 = r0
                r0 = r19
                r1 = r22
                r1.mPaused = r0     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.notifyAll()     // Catch:{ all -> 0x01d2 }
            L_0x00a0:
                r0 = r22
                boolean r0 = r0.mShouldReleaseEglContext     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x00b7
                r22.stopEglSurfaceLocked()     // Catch:{ all -> 0x01d2 }
                r22.stopEglContextLocked()     // Catch:{ all -> 0x01d2 }
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mShouldReleaseEglContext = r0     // Catch:{ all -> 0x01d2 }
                r2 = 1
            L_0x00b7:
                if (r10 == 0) goto L_0x00c0
                r22.stopEglSurfaceLocked()     // Catch:{ all -> 0x01d2 }
                r22.stopEglContextLocked()     // Catch:{ all -> 0x01d2 }
                r10 = 0
            L_0x00c0:
                if (r11 == 0) goto L_0x00cd
                r0 = r22
                boolean r0 = r0.mHaveEglSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x00cd
                r22.stopEglSurfaceLocked()     // Catch:{ all -> 0x01d2 }
            L_0x00cd:
                if (r11 == 0) goto L_0x00f5
                r0 = r22
                boolean r0 = r0.mHaveEglContext     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x00f5
                r0 = r22
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18> r0 = r0.mGLSurfaceViewWeakRef     // Catch:{ all -> 0x01d2 }
                r19 = r0
                java.lang.Object r16 = r19.get()     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18 r16 = (com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18) r16     // Catch:{ all -> 0x01d2 }
                if (r16 != 0) goto L_0x01e3
                r12 = 0
            L_0x00e6:
                if (r12 == 0) goto L_0x00f2
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                boolean r19 = r19.shouldReleaseEGLContextWhenPausing()     // Catch:{ all -> 0x01d2 }
                if (r19 == 0) goto L_0x00f5
            L_0x00f2:
                r22.stopEglContextLocked()     // Catch:{ all -> 0x01d2 }
            L_0x00f5:
                if (r11 == 0) goto L_0x010a
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                boolean r19 = r19.shouldTerminateEGLWhenPausing()     // Catch:{ all -> 0x01d2 }
                if (r19 == 0) goto L_0x010a
                r0 = r22
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01d2 }
                r19 = r0
                r19.finish()     // Catch:{ all -> 0x01d2 }
            L_0x010a:
                r0 = r22
                boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 != 0) goto L_0x013c
                r0 = r22
                boolean r0 = r0.mWaitingForSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 != 0) goto L_0x013c
                r0 = r22
                boolean r0 = r0.mHaveEglSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x0125
                r22.stopEglSurfaceLocked()     // Catch:{ all -> 0x01d2 }
            L_0x0125:
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mWaitingForSurface = r0     // Catch:{ all -> 0x01d2 }
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mSurfaceIsBad = r0     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.notifyAll()     // Catch:{ all -> 0x01d2 }
            L_0x013c:
                r0 = r22
                boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x015b
                r0 = r22
                boolean r0 = r0.mWaitingForSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x015b
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mWaitingForSurface = r0     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.notifyAll()     // Catch:{ all -> 0x01d2 }
            L_0x015b:
                if (r6 == 0) goto L_0x016f
                r18 = 0
                r6 = 0
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mRenderComplete = r0     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.notifyAll()     // Catch:{ all -> 0x01d2 }
            L_0x016f:
                boolean r19 = r22.readyToDraw()     // Catch:{ all -> 0x01d2 }
                if (r19 == 0) goto L_0x021f
                r0 = r22
                boolean r0 = r0.mHaveEglContext     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 != 0) goto L_0x0180
                if (r2 == 0) goto L_0x01e9
                r2 = 0
            L_0x0180:
                r0 = r22
                boolean r0 = r0.mHaveEglContext     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x019b
                r0 = r22
                boolean r0 = r0.mHaveEglSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 != 0) goto L_0x019b
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mHaveEglSurface = r0     // Catch:{ all -> 0x01d2 }
                r4 = 1
                r5 = 1
                r13 = 1
            L_0x019b:
                r0 = r22
                boolean r0 = r0.mHaveEglSurface     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x021f
                r0 = r22
                boolean r0 = r0.mSizeChanged     // Catch:{ all -> 0x01d2 }
                r19 = r0
                if (r19 == 0) goto L_0x01c1
                r13 = 1
                r0 = r22
                int r0 = r0.mWidth     // Catch:{ all -> 0x01d2 }
                r17 = r0
                r0 = r22
                int r9 = r0.mHeight     // Catch:{ all -> 0x01d2 }
                r18 = 1
                r4 = 1
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mSizeChanged = r0     // Catch:{ all -> 0x01d2 }
            L_0x01c1:
                r19 = 0
                r0 = r19
                r1 = r22
                r1.mRequestRender = r0     // Catch:{ all -> 0x01d2 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.notifyAll()     // Catch:{ all -> 0x01d2 }
                goto L_0x006e
            L_0x01d2:
                r19 = move-exception
                monitor-exit(r20)     // Catch:{ all -> 0x01d2 }
                throw r19     // Catch:{ all -> 0x01d5 }
            L_0x01d5:
                r19 = move-exception
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager
                monitor-enter(r20)
                r22.stopEglSurfaceLocked()     // Catch:{ all -> 0x031e }
                r22.stopEglContextLocked()     // Catch:{ all -> 0x031e }
                monitor-exit(r20)     // Catch:{ all -> 0x031e }
                throw r19
            L_0x01e3:
                boolean r12 = r16.mPreserveEGLContextOnPause     // Catch:{ all -> 0x01d2 }
                goto L_0x00e6
            L_0x01e9:
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r0 = r19
                r1 = r22
                boolean r19 = r0.tryAcquireEglContextLocked(r1)     // Catch:{ all -> 0x01d2 }
                if (r19 == 0) goto L_0x0180
                r0 = r22
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r0 = r0.mEglHelper     // Catch:{ RuntimeException -> 0x0212 }
                r19 = r0
                r19.start()     // Catch:{ RuntimeException -> 0x0212 }
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mHaveEglContext = r0     // Catch:{ all -> 0x01d2 }
                r3 = 1
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.notifyAll()     // Catch:{ all -> 0x01d2 }
                goto L_0x0180
            L_0x0212:
                r15 = move-exception
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r0 = r19
                r1 = r22
                r0.releaseEglContextLocked(r1)     // Catch:{ all -> 0x01d2 }
                throw r15     // Catch:{ all -> 0x01d2 }
            L_0x021f:
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d2 }
                r19.wait()     // Catch:{ all -> 0x01d2 }
                goto L_0x0034
            L_0x0228:
                if (r4 == 0) goto L_0x024c
                r0 = r22
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01d5 }
                r19 = r0
                boolean r19 = r19.createSurface()     // Catch:{ all -> 0x01d5 }
                if (r19 == 0) goto L_0x02f7
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d5 }
                monitor-enter(r20)     // Catch:{ all -> 0x01d5 }
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mFinishedCreatingEglSurface = r0     // Catch:{ all -> 0x02f4 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x02f4 }
                r19.notifyAll()     // Catch:{ all -> 0x02f4 }
                monitor-exit(r20)     // Catch:{ all -> 0x02f4 }
                r4 = 0
            L_0x024c:
                if (r5 == 0) goto L_0x0267
                r0 = r22
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01d5 }
                r19 = r0
                javax.microedition.khronos.opengles.GL r19 = r19.createGL()     // Catch:{ all -> 0x01d5 }
                r0 = r19
                javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x01d5 }
                r8 = r0
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d5 }
                r0 = r19
                r0.checkGLDriver(r8)     // Catch:{ all -> 0x01d5 }
                r5 = 0
            L_0x0267:
                if (r3 == 0) goto L_0x028f
                r0 = r22
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18> r0 = r0.mGLSurfaceViewWeakRef     // Catch:{ all -> 0x01d5 }
                r19 = r0
                java.lang.Object r16 = r19.get()     // Catch:{ all -> 0x01d5 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18 r16 = (com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18) r16     // Catch:{ all -> 0x01d5 }
                if (r16 == 0) goto L_0x028e
                android.opengl.GLSurfaceView$Renderer r19 = r16.mRenderer     // Catch:{ all -> 0x01d5 }
                r0 = r22
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01d5 }
                r20 = r0
                r0 = r20
                javax.microedition.khronos.egl.EGLConfig r0 = r0.mEglConfig     // Catch:{ all -> 0x01d5 }
                r20 = r0
                r0 = r19
                r1 = r20
                r0.onSurfaceCreated(r8, r1)     // Catch:{ all -> 0x01d5 }
            L_0x028e:
                r3 = 0
            L_0x028f:
                if (r13 == 0) goto L_0x02ab
                r0 = r22
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18> r0 = r0.mGLSurfaceViewWeakRef     // Catch:{ all -> 0x01d5 }
                r19 = r0
                java.lang.Object r16 = r19.get()     // Catch:{ all -> 0x01d5 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18 r16 = (com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18) r16     // Catch:{ all -> 0x01d5 }
                if (r16 == 0) goto L_0x02aa
                android.opengl.GLSurfaceView$Renderer r19 = r16.mRenderer     // Catch:{ all -> 0x01d5 }
                r0 = r19
                r1 = r17
                r0.onSurfaceChanged(r8, r1, r9)     // Catch:{ all -> 0x01d5 }
            L_0x02aa:
                r13 = 0
            L_0x02ab:
                r0 = r22
                java.lang.ref.WeakReference<com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18> r0 = r0.mGLSurfaceViewWeakRef     // Catch:{ all -> 0x01d5 }
                r19 = r0
                java.lang.Object r16 = r19.get()     // Catch:{ all -> 0x01d5 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18 r16 = (com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18) r16     // Catch:{ all -> 0x01d5 }
                if (r16 == 0) goto L_0x02c2
                android.opengl.GLSurfaceView$Renderer r19 = r16.mRenderer     // Catch:{ all -> 0x01d5 }
                r0 = r19
                r0.onDrawFrame(r8)     // Catch:{ all -> 0x01d5 }
            L_0x02c2:
                r0 = r22
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01d5 }
                r19 = r0
                int r14 = r19.swap()     // Catch:{ all -> 0x01d5 }
                switch(r14) {
                    case 12288: goto L_0x02ef;
                    case 12302: goto L_0x0319;
                    default: goto L_0x02cf;
                }     // Catch:{ all -> 0x01d5 }
            L_0x02cf:
                java.lang.String r19 = "GLThread"
                java.lang.String r20 = "eglSwapBuffers"
                r0 = r19
                r1 = r20
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.EglHelper.logEglErrorAsWarning(r0, r1, r14)     // Catch:{ all -> 0x01d5 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d5 }
                monitor-enter(r20)     // Catch:{ all -> 0x01d5 }
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mSurfaceIsBad = r0     // Catch:{ all -> 0x031b }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x031b }
                r19.notifyAll()     // Catch:{ all -> 0x031b }
                monitor-exit(r20)     // Catch:{ all -> 0x031b }
            L_0x02ef:
                if (r18 == 0) goto L_0x002f
                r6 = 1
                goto L_0x002f
            L_0x02f4:
                r19 = move-exception
                monitor-exit(r20)     // Catch:{ all -> 0x02f4 }
                throw r19     // Catch:{ all -> 0x01d5 }
            L_0x02f7:
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r20 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x01d5 }
                monitor-enter(r20)     // Catch:{ all -> 0x01d5 }
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mFinishedCreatingEglSurface = r0     // Catch:{ all -> 0x0316 }
                r19 = 1
                r0 = r19
                r1 = r22
                r1.mSurfaceIsBad = r0     // Catch:{ all -> 0x0316 }
                com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18$GLThreadManager r19 = com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.sGLThreadManager     // Catch:{ all -> 0x0316 }
                r19.notifyAll()     // Catch:{ all -> 0x0316 }
                monitor-exit(r20)     // Catch:{ all -> 0x0316 }
                goto L_0x002f
            L_0x0316:
                r19 = move-exception
                monitor-exit(r20)     // Catch:{ all -> 0x0316 }
                throw r19     // Catch:{ all -> 0x01d5 }
            L_0x0319:
                r10 = 1
                goto L_0x02ef
            L_0x031b:
                r19 = move-exception
                monitor-exit(r20)     // Catch:{ all -> 0x031b }
                throw r19     // Catch:{ all -> 0x01d5 }
            L_0x031e:
                r19 = move-exception
                monitor-exit(r20)     // Catch:{ all -> 0x031e }
                throw r19
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18.GLThread.guardedRun():void");
        }

        public boolean ableToDraw() {
            return this.mHaveEglContext && this.mHaveEglSurface && readyToDraw();
        }

        private boolean readyToDraw() {
            return !this.mPaused && this.mHasSurface && !this.mSurfaceIsBad && this.mWidth > 0 && this.mHeight > 0 && (this.mRequestRender || this.mRenderMode == 1);
        }

        public void setRenderMode(int renderMode) {
            if (renderMode < 0 || renderMode > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mRenderMode = renderMode;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
            }
        }

        public int getRenderMode() {
            int i;
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                i = this.mRenderMode;
            }
            return i;
        }

        public void requestRender() {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mRequestRender = true;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
            }
        }

        public void surfaceCreated() {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mHasSurface = true;
                this.mFinishedCreatingEglSurface = false;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
                while (this.mWaitingForSurface && !this.mFinishedCreatingEglSurface && !this.mExited) {
                    try {
                        GLSurfaceViewAPI18.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void surfaceDestroyed() {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mHasSurface = false;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
                while (!this.mWaitingForSurface && !this.mExited) {
                    try {
                        GLSurfaceViewAPI18.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void onPause() {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mRequestPaused = true;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
                while (!this.mExited && !this.mPaused) {
                    try {
                        GLSurfaceViewAPI18.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void onResume() {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mRequestPaused = false;
                this.mRequestRender = true;
                this.mRenderComplete = false;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
                while (!this.mExited && this.mPaused && !this.mRenderComplete) {
                    try {
                        GLSurfaceViewAPI18.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void onWindowResize(int w, int h) {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mWidth = w;
                this.mHeight = h;
                this.mSizeChanged = true;
                this.mRequestRender = true;
                this.mRenderComplete = false;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
                while (!this.mExited && !this.mPaused && !this.mRenderComplete && ableToDraw()) {
                    try {
                        GLSurfaceViewAPI18.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void requestExitAndWait() {
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mShouldExit = true;
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
                while (!this.mExited) {
                    try {
                        GLSurfaceViewAPI18.sGLThreadManager.wait();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void requestReleaseEglContextLocked() {
            this.mShouldReleaseEglContext = true;
            GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
        }

        public void queueEvent(Runnable r) {
            if (r == null) {
                throw new IllegalArgumentException("r must not be null");
            }
            synchronized (GLSurfaceViewAPI18.sGLThreadManager) {
                this.mEventQueue.add(r);
                GLSurfaceViewAPI18.sGLThreadManager.notifyAll();
            }
        }
    }

    static class LogWriter extends Writer {
        private StringBuilder mBuilder = new StringBuilder();

        LogWriter() {
        }

        public void close() {
            flushBuilder();
        }

        public void flush() {
            flushBuilder();
        }

        public void write(char[] buf, int offset, int count) {
            for (int i = 0; i < count; i++) {
                char c = buf[offset + i];
                if (c == 10) {
                    flushBuilder();
                } else {
                    this.mBuilder.append(c);
                }
            }
        }

        private void flushBuilder() {
            if (this.mBuilder.length() > 0) {
                Log.v("GLSurfaceView", this.mBuilder.toString());
                this.mBuilder.delete(0, this.mBuilder.length());
            }
        }
    }

    private void checkRenderThreadState() {
        if (this.mGLThread != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
    }

    private static class GLThreadManager {
        private static String TAG = "GLThreadManager";
        private static final int kGLES_20 = 131072;
        private static final String kMSM7K_RENDERER_PREFIX = "Q3Dimension MSM7500 ";
        private GLThread mEglOwner;
        private boolean mGLESDriverCheckComplete;
        private int mGLESVersion;
        private boolean mGLESVersionCheckComplete;
        private boolean mLimitedGLESContexts;
        private boolean mMultipleGLESContextsAllowed;

        private GLThreadManager() {
        }

        public synchronized void threadExiting(GLThread thread) {
            boolean unused = thread.mExited = true;
            if (this.mEglOwner == thread) {
                this.mEglOwner = null;
            }
            notifyAll();
        }

        public boolean tryAcquireEglContextLocked(GLThread thread) {
            if (this.mEglOwner == thread || this.mEglOwner == null) {
                this.mEglOwner = thread;
                notifyAll();
                return true;
            }
            checkGLESVersion();
            if (this.mMultipleGLESContextsAllowed) {
                return true;
            }
            if (this.mEglOwner != null) {
                this.mEglOwner.requestReleaseEglContextLocked();
            }
            return false;
        }

        public void releaseEglContextLocked(GLThread thread) {
            if (this.mEglOwner == thread) {
                this.mEglOwner = null;
            }
            notifyAll();
        }

        public synchronized boolean shouldReleaseEGLContextWhenPausing() {
            return this.mLimitedGLESContexts;
        }

        public synchronized boolean shouldTerminateEGLWhenPausing() {
            checkGLESVersion();
            return !this.mMultipleGLESContextsAllowed;
        }

        public synchronized void checkGLDriver(GL10 gl) {
            boolean z;
            boolean z2 = true;
            synchronized (this) {
                if (!this.mGLESDriverCheckComplete) {
                    checkGLESVersion();
                    String renderer = gl.glGetString(GL20.GL_RENDERER);
                    if (this.mGLESVersion < 131072) {
                        if (!renderer.startsWith(kMSM7K_RENDERER_PREFIX)) {
                            z = true;
                        } else {
                            z = false;
                        }
                        this.mMultipleGLESContextsAllowed = z;
                        notifyAll();
                    }
                    if (this.mMultipleGLESContextsAllowed) {
                        z2 = false;
                    }
                    this.mLimitedGLESContexts = z2;
                    this.mGLESDriverCheckComplete = true;
                }
            }
        }

        private void checkGLESVersion() {
            if (!this.mGLESVersionCheckComplete) {
                this.mGLESVersion = 131072;
                if (this.mGLESVersion >= 131072) {
                    this.mMultipleGLESContextsAllowed = true;
                }
                this.mGLESVersionCheckComplete = true;
            }
        }
    }
}
