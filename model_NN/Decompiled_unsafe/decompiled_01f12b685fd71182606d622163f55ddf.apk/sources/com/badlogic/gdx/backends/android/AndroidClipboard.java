package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.os.Build;
import android.text.ClipboardManager;
import com.badlogic.gdx.utils.Clipboard;

public class AndroidClipboard implements Clipboard {
    Context context;

    public AndroidClipboard(Context context2) {
        this.context = context2;
    }

    public String getContents() {
        CharSequence text;
        if (Build.VERSION.SDK_INT < 11) {
            ClipboardManager clipboard = (ClipboardManager) this.context.getSystemService("clipboard");
            if (clipboard.getText() == null) {
                return null;
            }
            return clipboard.getText().toString();
        }
        ClipData clip = ((android.content.ClipboardManager) this.context.getSystemService("clipboard")).getPrimaryClip();
        if (clip == null || (text = clip.getItemAt(0).getText()) == null) {
            return null;
        }
        return text.toString();
    }

    public void setContents(final String contents) {
        try {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    if (Build.VERSION.SDK_INT < 11) {
                        ((ClipboardManager) AndroidClipboard.this.context.getSystemService("clipboard")).setText(contents);
                    } else {
                        ((android.content.ClipboardManager) AndroidClipboard.this.context.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(contents, contents));
                    }
                }
            });
        } catch (Exception e) {
        }
    }
}
