package com.badlogic.gdx.backends.android;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20API18;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewAPI18;
import com.badlogic.gdx.backends.android.surfaceview.GdxEglConfigChooser;
import com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy;
import com.badlogic.gdx.graphics.Cubemap;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.WindowedMean;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public class AndroidGraphics implements GLSurfaceView.Renderer, Graphics {
    private static final String LOG_TAG = "AndroidGraphics";
    static volatile boolean enforceContinuousRendering = false;
    AndroidApplicationBase app;
    private Graphics.BufferFormat bufferFormat;
    protected final AndroidApplicationConfiguration config;
    volatile boolean created;
    protected float deltaTime;
    private float density;
    volatile boolean destroy;
    EGLContext eglContext;
    String extensions;
    protected int fps;
    protected long frameId;
    protected long frameStart;
    protected int frames;
    GL20 gl20;
    GL30 gl30;
    int height;
    private boolean isContinuous;
    protected long lastFrameTime;
    protected WindowedMean mean;
    volatile boolean pause;
    private float ppcX;
    private float ppcY;
    private float ppiX;
    private float ppiY;
    volatile boolean resume;
    volatile boolean running;
    Object synch;
    int[] value;
    final View view;
    int width;

    public AndroidGraphics(AndroidApplicationBase application, AndroidApplicationConfiguration config2, ResolutionStrategy resolutionStrategy) {
        this(application, config2, resolutionStrategy, true);
    }

    public AndroidGraphics(AndroidApplicationBase application, AndroidApplicationConfiguration config2, ResolutionStrategy resolutionStrategy, boolean focusableView) {
        this.lastFrameTime = System.nanoTime();
        this.deltaTime = Animation.CurveTimeline.LINEAR;
        this.frameStart = System.nanoTime();
        this.frameId = -1;
        this.frames = 0;
        this.mean = new WindowedMean(5);
        this.created = false;
        this.running = false;
        this.pause = false;
        this.resume = false;
        this.destroy = false;
        this.ppiX = Animation.CurveTimeline.LINEAR;
        this.ppiY = Animation.CurveTimeline.LINEAR;
        this.ppcX = Animation.CurveTimeline.LINEAR;
        this.ppcY = Animation.CurveTimeline.LINEAR;
        this.density = 1.0f;
        this.bufferFormat = new Graphics.BufferFormat(5, 6, 5, 0, 16, 0, 0, false);
        this.isContinuous = true;
        this.value = new int[1];
        this.synch = new Object();
        this.config = config2;
        this.app = application;
        this.view = createGLSurfaceView(application, resolutionStrategy);
        preserveEGLContextOnPause();
        if (focusableView) {
            this.view.setFocusable(true);
            this.view.setFocusableInTouchMode(true);
        }
    }

    /* access modifiers changed from: protected */
    public void preserveEGLContextOnPause() {
        if ((Build.VERSION.SDK_INT >= 11 && (this.view instanceof GLSurfaceView20)) || (this.view instanceof GLSurfaceView20API18)) {
            try {
                this.view.getClass().getMethod("setPreserveEGLContextOnPause", Boolean.TYPE).invoke(this.view, true);
            } catch (Exception e) {
                Gdx.app.log(LOG_TAG, "Method GLSurfaceView.setPreserveEGLContextOnPause not found");
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20API18} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View createGLSurfaceView(com.badlogic.gdx.backends.android.AndroidApplicationBase r10, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy r11) {
        /*
            r9 = this;
            boolean r1 = r9.checkGL20()
            if (r1 != 0) goto L_0x000e
            com.badlogic.gdx.utils.GdxRuntimeException r1 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r2 = "Libgdx requires OpenGL ES 2.0"
            r1.<init>(r2)
            throw r1
        L_0x000e:
            android.opengl.GLSurfaceView$EGLConfigChooser r7 = r9.getEglConfigChooser()
            int r8 = android.os.Build.VERSION.SDK_INT
            r1 = 10
            if (r8 > r1) goto L_0x004c
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r9.config
            boolean r1 = r1.useGLSurfaceView20API18
            if (r1 == 0) goto L_0x004c
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20API18 r0 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20API18
            android.content.Context r1 = r10.getContext()
            r0.<init>(r1, r11)
            if (r7 == 0) goto L_0x0030
            r0.setEGLConfigChooser(r7)
        L_0x002c:
            r0.setRenderer(r9)
        L_0x002f:
            return r0
        L_0x0030:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r9.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r9.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r9.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r9.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r9.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r9.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x002c
        L_0x004c:
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20 r0 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20
            android.content.Context r1 = r10.getContext()
            r0.<init>(r1, r11)
            if (r7 == 0) goto L_0x005e
            r0.setEGLConfigChooser(r7)
        L_0x005a:
            r0.setRenderer(r9)
            goto L_0x002f
        L_0x005e:
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r1 = r9.config
            int r1 = r1.r
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r2 = r9.config
            int r2 = r2.g
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r3 = r9.config
            int r3 = r3.b
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r4 = r9.config
            int r4 = r4.a
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r5 = r9.config
            int r5 = r5.depth
            com.badlogic.gdx.backends.android.AndroidApplicationConfiguration r6 = r9.config
            int r6 = r6.stencil
            r0.setEGLConfigChooser(r1, r2, r3, r4, r5, r6)
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.AndroidGraphics.createGLSurfaceView(com.badlogic.gdx.backends.android.AndroidApplicationBase, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy):android.view.View");
    }

    public void onPauseGLSurfaceView() {
        if (this.view != null) {
            if (this.view instanceof GLSurfaceViewAPI18) {
                ((GLSurfaceViewAPI18) this.view).onPause();
            }
            if (this.view instanceof GLSurfaceView) {
                ((GLSurfaceView) this.view).onPause();
            }
        }
    }

    public void onResumeGLSurfaceView() {
        if (this.view != null) {
            if (this.view instanceof GLSurfaceViewAPI18) {
                ((GLSurfaceViewAPI18) this.view).onResume();
            }
            if (this.view instanceof GLSurfaceView) {
                ((GLSurfaceView) this.view).onResume();
            }
        }
    }

    /* access modifiers changed from: protected */
    public GLSurfaceView.EGLConfigChooser getEglConfigChooser() {
        return new GdxEglConfigChooser(this.config.r, this.config.g, this.config.b, this.config.a, this.config.depth, this.config.stencil, this.config.numSamples);
    }

    private void updatePpi() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.app.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.ppiX = metrics.xdpi;
        this.ppiY = metrics.ydpi;
        this.ppcX = metrics.xdpi / 2.54f;
        this.ppcY = metrics.ydpi / 2.54f;
        this.density = metrics.density;
    }

    /* access modifiers changed from: protected */
    public boolean checkGL20() {
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl.eglInitialize(display, new int[2]);
        int[] num_config = new int[1];
        egl.eglChooseConfig(display, new int[]{12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344}, new EGLConfig[10], 10, num_config);
        egl.eglTerminate(display);
        return num_config[0] > 0;
    }

    public GL20 getGL20() {
        return this.gl20;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    private void setupGL(GL10 gl) {
        if (this.gl20 == null) {
            this.gl20 = new AndroidGL20();
            Gdx.gl = this.gl20;
            Gdx.gl20 = this.gl20;
            Gdx.app.log(LOG_TAG, "OGL renderer: " + gl.glGetString(GL20.GL_RENDERER));
            Gdx.app.log(LOG_TAG, "OGL vendor: " + gl.glGetString(GL20.GL_VENDOR));
            Gdx.app.log(LOG_TAG, "OGL version: " + gl.glGetString(GL20.GL_VERSION));
            Gdx.app.log(LOG_TAG, "OGL extensions: " + gl.glGetString(GL20.GL_EXTENSIONS));
        }
    }

    public void onSurfaceChanged(GL10 gl, int width2, int height2) {
        this.width = width2;
        this.height = height2;
        updatePpi();
        gl.glViewport(0, 0, this.width, this.height);
        if (!this.created) {
            this.app.getApplicationListener().create();
            this.created = true;
            synchronized (this) {
                this.running = true;
            }
        }
        this.app.getApplicationListener().resize(width2, height2);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config2) {
        this.eglContext = ((EGL10) EGLContext.getEGL()).eglGetCurrentContext();
        setupGL(gl);
        logConfig(config2);
        updatePpi();
        Mesh.invalidateAllMeshes(this.app);
        Texture.invalidateAllTextures(this.app);
        Cubemap.invalidateAllCubemaps(this.app);
        ShaderProgram.invalidateAllShaderPrograms(this.app);
        FrameBuffer.invalidateAllFrameBuffers(this.app);
        logManagedCachesStatus();
        Display display = this.app.getWindowManager().getDefaultDisplay();
        this.width = display.getWidth();
        this.height = display.getHeight();
        this.mean = new WindowedMean(5);
        this.lastFrameTime = System.nanoTime();
        gl.glViewport(0, 0, this.width, this.height);
    }

    private void logConfig(EGLConfig config2) {
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        int r = getAttrib(egl, display, config2, 12324, 0);
        int g = getAttrib(egl, display, config2, 12323, 0);
        int b = getAttrib(egl, display, config2, 12322, 0);
        int a = getAttrib(egl, display, config2, 12321, 0);
        int d = getAttrib(egl, display, config2, 12325, 0);
        int s = getAttrib(egl, display, config2, 12326, 0);
        int samples = Math.max(getAttrib(egl, display, config2, 12337, 0), getAttrib(egl, display, config2, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0));
        boolean coverageSample = getAttrib(egl, display, config2, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0) != 0;
        Gdx.app.log(LOG_TAG, "framebuffer: (" + r + ", " + g + ", " + b + ", " + a + ")");
        Gdx.app.log(LOG_TAG, "depthbuffer: (" + d + ")");
        Gdx.app.log(LOG_TAG, "stencilbuffer: (" + s + ")");
        Gdx.app.log(LOG_TAG, "samples: (" + samples + ")");
        Gdx.app.log(LOG_TAG, "coverage sampling: (" + coverageSample + ")");
        this.bufferFormat = new Graphics.BufferFormat(r, g, b, a, d, s, samples, coverageSample);
    }

    private int getAttrib(EGL10 egl, EGLDisplay display, EGLConfig config2, int attrib, int defValue) {
        if (egl.eglGetConfigAttrib(display, config2, attrib, this.value)) {
            return this.value[0];
        }
        return defValue;
    }

    /* access modifiers changed from: package-private */
    public void resume() {
        synchronized (this.synch) {
            this.running = true;
            this.resume = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void pause() {
        /*
            r6 = this;
            java.lang.Object r2 = r6.synch
            monitor-enter(r2)
            boolean r1 = r6.running     // Catch:{ all -> 0x003a }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
        L_0x0008:
            return
        L_0x0009:
            r1 = 0
            r6.running = r1     // Catch:{ all -> 0x003a }
            r1 = 1
            r6.pause = r1     // Catch:{ all -> 0x003a }
        L_0x000f:
            boolean r1 = r6.pause     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x003d
            java.lang.Object r1 = r6.synch     // Catch:{ InterruptedException -> 0x002f }
            r4 = 4000(0xfa0, double:1.9763E-320)
            r1.wait(r4)     // Catch:{ InterruptedException -> 0x002f }
            boolean r1 = r6.pause     // Catch:{ InterruptedException -> 0x002f }
            if (r1 == 0) goto L_0x000f
            com.badlogic.gdx.Application r1 = com.badlogic.gdx.Gdx.app     // Catch:{ InterruptedException -> 0x002f }
            java.lang.String r3 = "AndroidGraphics"
            java.lang.String r4 = "waiting for pause synchronization took too long; assuming deadlock and killing"
            r1.error(r3, r4)     // Catch:{ InterruptedException -> 0x002f }
            int r1 = android.os.Process.myPid()     // Catch:{ InterruptedException -> 0x002f }
            android.os.Process.killProcess(r1)     // Catch:{ InterruptedException -> 0x002f }
            goto L_0x000f
        L_0x002f:
            r0 = move-exception
            com.badlogic.gdx.Application r1 = com.badlogic.gdx.Gdx.app     // Catch:{ all -> 0x003a }
            java.lang.String r3 = "AndroidGraphics"
            java.lang.String r4 = "waiting for pause synchronization failed!"
            r1.log(r3, r4)     // Catch:{ all -> 0x003a }
            goto L_0x000f
        L_0x003a:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
            throw r1
        L_0x003d:
            monitor-exit(r2)     // Catch:{ all -> 0x003a }
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.AndroidGraphics.pause():void");
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        synchronized (this.synch) {
            this.running = false;
            this.destroy = true;
            while (this.destroy) {
                try {
                    this.synch.wait();
                } catch (InterruptedException e) {
                    Gdx.app.log(LOG_TAG, "waiting for destroy synchronization failed!");
                }
            }
        }
    }

    public void onDrawFrame(GL10 gl) {
        boolean lrunning;
        boolean lpause;
        boolean ldestroy;
        boolean lresume;
        long time = System.nanoTime();
        this.deltaTime = ((float) (time - this.lastFrameTime)) / 1.0E9f;
        this.lastFrameTime = time;
        if (!this.resume) {
            this.mean.addValue(this.deltaTime);
        } else {
            this.deltaTime = Animation.CurveTimeline.LINEAR;
        }
        synchronized (this.synch) {
            lrunning = this.running;
            lpause = this.pause;
            ldestroy = this.destroy;
            lresume = this.resume;
            if (this.resume) {
                this.resume = false;
            }
            if (this.pause) {
                this.pause = false;
                this.synch.notifyAll();
            }
            if (this.destroy) {
                this.destroy = false;
                this.synch.notifyAll();
            }
        }
        if (lresume) {
            Array<LifecycleListener> listeners = this.app.getLifecycleListeners();
            synchronized (listeners) {
                Iterator i$ = listeners.iterator();
                while (i$.hasNext()) {
                    i$.next().resume();
                }
            }
            this.app.getApplicationListener().resume();
            Gdx.app.log(LOG_TAG, "resumed");
        }
        if (lrunning) {
            synchronized (this.app.getRunnables()) {
                this.app.getExecutedRunnables().clear();
                this.app.getExecutedRunnables().addAll(this.app.getRunnables());
                this.app.getRunnables().clear();
            }
            for (int i = 0; i < this.app.getExecutedRunnables().size; i++) {
                try {
                    this.app.getExecutedRunnables().get(i).run();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
            this.app.getInput().processEvents();
            this.frameId = this.frameId + 1;
            this.app.getApplicationListener().render();
        }
        if (lpause) {
            Array<LifecycleListener> listeners2 = this.app.getLifecycleListeners();
            synchronized (listeners2) {
                Iterator i$2 = listeners2.iterator();
                while (i$2.hasNext()) {
                    i$2.next().pause();
                }
            }
            this.app.getApplicationListener().pause();
            Gdx.app.log(LOG_TAG, "paused");
        }
        if (ldestroy) {
            Array<LifecycleListener> listeners3 = this.app.getLifecycleListeners();
            synchronized (listeners3) {
                Iterator i$3 = listeners3.iterator();
                while (i$3.hasNext()) {
                    i$3.next().dispose();
                }
            }
            this.app.getApplicationListener().dispose();
            Gdx.app.log(LOG_TAG, "destroyed");
        }
        if (time - this.frameStart > 1000000000) {
            this.fps = this.frames;
            this.frames = 0;
            this.frameStart = time;
        }
        this.frames = this.frames + 1;
    }

    public long getFrameId() {
        return this.frameId;
    }

    public float getDeltaTime() {
        return this.mean.getMean() == Animation.CurveTimeline.LINEAR ? this.deltaTime : this.mean.getMean();
    }

    public float getRawDeltaTime() {
        return this.deltaTime;
    }

    public Graphics.GraphicsType getType() {
        return Graphics.GraphicsType.AndroidGL;
    }

    public int getFramesPerSecond() {
        return this.fps;
    }

    public void clearManagedCaches() {
        Mesh.clearAllMeshes(this.app);
        Texture.clearAllTextures(this.app);
        Cubemap.clearAllCubemaps(this.app);
        ShaderProgram.clearAllShaderPrograms(this.app);
        FrameBuffer.clearAllFrameBuffers(this.app);
        logManagedCachesStatus();
    }

    /* access modifiers changed from: protected */
    public void logManagedCachesStatus() {
        Gdx.app.log(LOG_TAG, Mesh.getManagedStatus());
        Gdx.app.log(LOG_TAG, Texture.getManagedStatus());
        Gdx.app.log(LOG_TAG, Cubemap.getManagedStatus());
        Gdx.app.log(LOG_TAG, ShaderProgram.getManagedStatus());
        Gdx.app.log(LOG_TAG, FrameBuffer.getManagedStatus());
    }

    public View getView() {
        return this.view;
    }

    public float getPpiX() {
        return this.ppiX;
    }

    public float getPpiY() {
        return this.ppiY;
    }

    public float getPpcX() {
        return this.ppcX;
    }

    public float getPpcY() {
        return this.ppcY;
    }

    public float getDensity() {
        return this.density;
    }

    public boolean supportsDisplayModeChange() {
        return false;
    }

    public boolean setDisplayMode(Graphics.DisplayMode displayMode) {
        return false;
    }

    public Graphics.DisplayMode[] getDisplayModes() {
        return new Graphics.DisplayMode[]{getDesktopDisplayMode()};
    }

    public boolean setDisplayMode(int width2, int height2, boolean fullscreen) {
        return false;
    }

    public void setTitle(String title) {
    }

    private class AndroidDisplayMode extends Graphics.DisplayMode {
        protected AndroidDisplayMode(int width, int height, int refreshRate, int bitsPerPixel) {
            super(width, height, refreshRate, bitsPerPixel);
        }
    }

    public Graphics.DisplayMode getDesktopDisplayMode() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.app.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return new AndroidDisplayMode(metrics.widthPixels, metrics.heightPixels, 0, 0);
    }

    public Graphics.BufferFormat getBufferFormat() {
        return this.bufferFormat;
    }

    public void setVSync(boolean vsync) {
    }

    public boolean supportsExtension(String extension) {
        if (this.extensions == null) {
            this.extensions = Gdx.gl.glGetString(GL20.GL_EXTENSIONS);
        }
        return this.extensions.contains(extension);
    }

    public void setContinuousRendering(boolean isContinuous2) {
        int renderMode;
        if (this.view != null) {
            this.isContinuous = enforceContinuousRendering || isContinuous2;
            if (this.isContinuous) {
                renderMode = 1;
            } else {
                renderMode = 0;
            }
            if (this.view instanceof GLSurfaceViewAPI18) {
                ((GLSurfaceViewAPI18) this.view).setRenderMode(renderMode);
            }
            if (this.view instanceof GLSurfaceView) {
                ((GLSurfaceView) this.view).setRenderMode(renderMode);
            }
            this.mean.clear();
        }
    }

    public boolean isContinuousRendering() {
        return this.isContinuous;
    }

    public void requestRendering() {
        if (this.view != null) {
            if (this.view instanceof GLSurfaceViewAPI18) {
                ((GLSurfaceViewAPI18) this.view).requestRender();
            }
            if (this.view instanceof GLSurfaceView) {
                ((GLSurfaceView) this.view).requestRender();
            }
        }
    }

    public boolean isFullscreen() {
        return true;
    }

    public boolean isGL30Available() {
        return this.gl30 != null;
    }

    public GL30 getGL30() {
        return this.gl30;
    }
}
