package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.kbz.esotericsoftware.spine.Animation;

public class RopeJointDef extends JointDef {
    public final Vector2 localAnchorA = new Vector2(-1.0f, Animation.CurveTimeline.LINEAR);
    public final Vector2 localAnchorB = new Vector2(1.0f, Animation.CurveTimeline.LINEAR);
    public float maxLength = Animation.CurveTimeline.LINEAR;

    public RopeJointDef() {
        this.type = JointDef.JointType.RopeJoint;
    }
}
