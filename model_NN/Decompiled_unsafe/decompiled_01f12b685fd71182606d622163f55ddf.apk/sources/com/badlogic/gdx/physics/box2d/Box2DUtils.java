package com.badlogic.gdx.physics.box2d;

public class Box2DUtils {
    public static long getAddr(Body body) {
        return body.addr;
    }

    public static long getAddr(Fixture fixture) {
        return fixture.addr;
    }

    public static long getAddr(Joint joint) {
        return joint.addr;
    }

    public static int hashCode(long n) {
        return (((int) ((n >>> 32) ^ n)) * 37) + 17;
    }

    public static int hashCode(Body body) {
        return (body.hashCode() * 31) + hashCode(getAddr(body));
    }

    public static int hashCode(Fixture fixture) {
        return (((fixture.hashCode() * 31) + hashCode(getAddr(fixture))) * 31) + hashCode(fixture.getBody());
    }

    public static int hashCode(Joint joint) {
        return (((((joint.hashCode() * 31) + hashCode(getAddr(joint))) * 31) + hashCode(joint.getBodyA())) * 31) + hashCode(joint.getBodyB());
    }
}
