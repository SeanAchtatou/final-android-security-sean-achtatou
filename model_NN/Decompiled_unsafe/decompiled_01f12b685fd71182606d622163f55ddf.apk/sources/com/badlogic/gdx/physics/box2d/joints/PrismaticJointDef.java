package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.kbz.esotericsoftware.spine.Animation;

public class PrismaticJointDef extends JointDef {
    public boolean enableLimit = false;
    public boolean enableMotor = false;
    public final Vector2 localAnchorA = new Vector2();
    public final Vector2 localAnchorB = new Vector2();
    public final Vector2 localAxisA = new Vector2(1.0f, Animation.CurveTimeline.LINEAR);
    public float lowerTranslation = Animation.CurveTimeline.LINEAR;
    public float maxMotorForce = Animation.CurveTimeline.LINEAR;
    public float motorSpeed = Animation.CurveTimeline.LINEAR;
    public float referenceAngle = Animation.CurveTimeline.LINEAR;
    public float upperTranslation = Animation.CurveTimeline.LINEAR;

    public PrismaticJointDef() {
        this.type = JointDef.JointType.PrismaticJoint;
    }

    public void initialize(Body bodyA, Body bodyB, Vector2 anchor, Vector2 axis) {
        this.bodyA = bodyA;
        this.bodyB = bodyB;
        this.localAnchorA.set(bodyA.getLocalPoint(anchor));
        this.localAnchorB.set(bodyB.getLocalPoint(anchor));
        this.localAxisA.set(bodyA.getLocalVector(axis));
        this.referenceAngle = bodyB.getAngle() - bodyA.getAngle();
    }
}
