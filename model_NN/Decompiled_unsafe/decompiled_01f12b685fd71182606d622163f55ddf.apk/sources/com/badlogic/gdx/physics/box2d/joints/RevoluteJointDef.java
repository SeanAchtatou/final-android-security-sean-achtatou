package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.kbz.esotericsoftware.spine.Animation;

public class RevoluteJointDef extends JointDef {
    public boolean enableLimit = false;
    public boolean enableMotor = false;
    public final Vector2 localAnchorA = new Vector2();
    public final Vector2 localAnchorB = new Vector2();
    public float lowerAngle = Animation.CurveTimeline.LINEAR;
    public float maxMotorTorque = Animation.CurveTimeline.LINEAR;
    public float motorSpeed = Animation.CurveTimeline.LINEAR;
    public float referenceAngle = Animation.CurveTimeline.LINEAR;
    public float upperAngle = Animation.CurveTimeline.LINEAR;

    public RevoluteJointDef() {
        this.type = JointDef.JointType.RevoluteJoint;
    }

    public void initialize(Body bodyA, Body bodyB, Vector2 anchor) {
        this.bodyA = bodyA;
        this.bodyB = bodyB;
        this.localAnchorA.set(bodyA.getLocalPoint(anchor));
        this.localAnchorB.set(bodyB.getLocalPoint(anchor));
        this.referenceAngle = bodyB.getAngle() - bodyA.getAngle();
    }
}
