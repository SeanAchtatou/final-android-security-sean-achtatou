package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.kbz.esotericsoftware.spine.Animation;

public class MouseJointDef extends JointDef {
    public float dampingRatio = 0.7f;
    public float frequencyHz = 5.0f;
    public float maxForce = Animation.CurveTimeline.LINEAR;
    public final Vector2 target = new Vector2();

    public MouseJointDef() {
        this.type = JointDef.JointType.MouseJoint;
    }
}
