package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.kbz.esotericsoftware.spine.Animation;

public class DistanceJointDef extends JointDef {
    public float dampingRatio = Animation.CurveTimeline.LINEAR;
    public float frequencyHz = Animation.CurveTimeline.LINEAR;
    public float length = 1.0f;
    public final Vector2 localAnchorA = new Vector2();
    public final Vector2 localAnchorB = new Vector2();

    public DistanceJointDef() {
        this.type = JointDef.JointType.DistanceJoint;
    }

    public void initialize(Body bodyA, Body bodyB, Vector2 anchorA, Vector2 anchorB) {
        this.bodyA = bodyA;
        this.bodyB = bodyB;
        this.localAnchorA.set(bodyA.getLocalPoint(anchorA));
        this.localAnchorB.set(bodyB.getLocalPoint(anchorB));
        this.length = anchorA.dst(anchorB);
    }
}
