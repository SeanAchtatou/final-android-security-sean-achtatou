package com.badlogic.gdx.physics.box2d;

import com.kbz.esotericsoftware.spine.Animation;

public class FixtureDef {
    public float density = Animation.CurveTimeline.LINEAR;
    public final Filter filter = new Filter();
    public float friction = 0.2f;
    public boolean isSensor = false;
    public float restitution = Animation.CurveTimeline.LINEAR;
    public Shape shape;
}
