package com.badlogic.gdx.maps;

public class MapLayer {
    private String name = "";
    private MapObjects objects = new MapObjects();
    private float opacity = 1.0f;
    private MapProperties properties = new MapProperties();
    private boolean visible = true;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public float getOpacity() {
        return this.opacity;
    }

    public void setOpacity(float opacity2) {
        this.opacity = opacity2;
    }

    public MapObjects getObjects() {
        return this.objects;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible2) {
        this.visible = visible2;
    }

    public MapProperties getProperties() {
        return this.properties;
    }
}
