package com.badlogic.gdx.maps.tiled;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;

public class TiledMapImageLayer extends MapLayer {
    private TextureRegion region;
    private int x;
    private int y;

    public TiledMapImageLayer(TextureRegion region2, int x2, int y2) {
        this.region = region2;
        this.x = x2;
        this.y = y2;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public TextureRegion getTextureRegion() {
        return this.region;
    }
}
