package com.badlogic.gdx.maps.tiled;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.ImageResolver;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.BaseTmxMapLoader;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.tendcloud.tenddata.dc;
import java.io.IOException;
import java.util.Iterator;

public class TmxMapLoader extends BaseTmxMapLoader<Parameters> {

    public static class Parameters extends BaseTmxMapLoader.Parameters {
    }

    public TmxMapLoader() {
        super(new InternalFileHandleResolver());
    }

    public TmxMapLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    public TiledMap load(String fileName) {
        return load(fileName, new Parameters());
    }

    public TiledMap load(String fileName, Parameters parameters) {
        try {
            this.convertObjectToTileSpace = parameters.convertObjectToTileSpace;
            this.flipY = parameters.flipY;
            FileHandle tmxFile = resolve(fileName);
            this.root = this.xml.parse(tmxFile);
            ObjectMap<String, Texture> textures = new ObjectMap<>();
            Array<FileHandle> textureFiles = loadTilesets(this.root, tmxFile);
            textureFiles.addAll(loadImages(this.root, tmxFile));
            Iterator<FileHandle> it = textureFiles.iterator();
            while (it.hasNext()) {
                FileHandle textureFile = it.next();
                Texture texture = new Texture(textureFile, parameters.generateMipMaps);
                texture.setFilter(parameters.textureMinFilter, parameters.textureMagFilter);
                textures.put(textureFile.path(), texture);
            }
            TiledMap map = loadTilemap(this.root, tmxFile, new ImageResolver.DirectImageResolver(textures));
            map.setOwnedResources(textures.values().toArray());
            return map;
        } catch (IOException e) {
            throw new GdxRuntimeException("Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    public void loadAsync(AssetManager manager, String fileName, FileHandle tmxFile, Parameters parameter) {
        this.map = null;
        if (parameter != null) {
            this.convertObjectToTileSpace = parameter.convertObjectToTileSpace;
            this.flipY = parameter.flipY;
        } else {
            this.convertObjectToTileSpace = false;
            this.flipY = true;
        }
        try {
            this.map = loadTilemap(this.root, tmxFile, new ImageResolver.AssetManagerImageResolver(manager));
        } catch (Exception e) {
            throw new GdxRuntimeException("Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    public TiledMap loadSync(AssetManager manager, String fileName, FileHandle file, Parameters parameter) {
        return this.map;
    }

    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle tmxFile, Parameters parameter) {
        Array<AssetDescriptor> dependencies = new Array<>();
        try {
            this.root = this.xml.parse(tmxFile);
            boolean generateMipMaps = parameter != null ? parameter.generateMipMaps : false;
            TextureLoader.TextureParameter texParams = new TextureLoader.TextureParameter();
            texParams.genMipMaps = generateMipMaps;
            if (parameter != null) {
                texParams.minFilter = parameter.textureMinFilter;
                texParams.magFilter = parameter.textureMagFilter;
            }
            Iterator<FileHandle> it = loadTilesets(this.root, tmxFile).iterator();
            while (it.hasNext()) {
                dependencies.add(new AssetDescriptor(it.next(), Texture.class, texParams));
            }
            Iterator<FileHandle> it2 = loadImages(this.root, tmxFile).iterator();
            while (it2.hasNext()) {
                dependencies.add(new AssetDescriptor(it2.next(), Texture.class, texParams));
            }
            return dependencies;
        } catch (IOException e) {
            throw new GdxRuntimeException("Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    /* access modifiers changed from: protected */
    public TiledMap loadTilemap(XmlReader.Element root, FileHandle tmxFile, ImageResolver imageResolver) {
        TiledMap map = new TiledMap();
        String mapOrientation = root.getAttribute("orientation", null);
        int mapWidth = root.getIntAttribute("width", 0);
        int mapHeight = root.getIntAttribute("height", 0);
        int tileWidth = root.getIntAttribute("tilewidth", 0);
        int tileHeight = root.getIntAttribute("tileheight", 0);
        String mapBackgroundColor = root.getAttribute("backgroundcolor", null);
        MapProperties mapProperties = map.getProperties();
        if (mapOrientation != null) {
            mapProperties.put("orientation", mapOrientation);
        }
        mapProperties.put("width", Integer.valueOf(mapWidth));
        mapProperties.put("height", Integer.valueOf(mapHeight));
        mapProperties.put("tilewidth", Integer.valueOf(tileWidth));
        mapProperties.put("tileheight", Integer.valueOf(tileHeight));
        if (mapBackgroundColor != null) {
            mapProperties.put("backgroundcolor", mapBackgroundColor);
        }
        this.mapTileWidth = tileWidth;
        this.mapTileHeight = tileHeight;
        this.mapWidthInPixels = mapWidth * tileWidth;
        this.mapHeightInPixels = mapHeight * tileHeight;
        if (mapOrientation != null && "staggered".equals(mapOrientation) && mapHeight > 1) {
            this.mapWidthInPixels = this.mapWidthInPixels + (tileWidth / 2);
            this.mapHeightInPixels = (this.mapHeightInPixels / 2) + (tileHeight / 2);
        }
        XmlReader.Element properties = root.getChildByName("properties");
        if (properties != null) {
            loadProperties(map.getProperties(), properties);
        }
        Iterator<XmlReader.Element> it = root.getChildrenByName("tileset").iterator();
        while (it.hasNext()) {
            XmlReader.Element element = it.next();
            loadTileSet(map, element, tmxFile, imageResolver);
            root.removeChild(element);
        }
        int j = root.getChildCount();
        for (int i = 0; i < j; i++) {
            XmlReader.Element element2 = root.getChild(i);
            String name = element2.getName();
            if (name.equals("layer")) {
                loadTileLayer(map, element2);
            } else if (name.equals("objectgroup")) {
                loadObjectGroup(map, element2);
            } else if (name.equals("imagelayer")) {
                loadImageLayer(map, element2, tmxFile, imageResolver);
            }
        }
        return map;
    }

    /* access modifiers changed from: protected */
    public Array<FileHandle> loadTilesets(XmlReader.Element root, FileHandle tmxFile) throws IOException {
        Array<FileHandle> images = new Array<>();
        Iterator<XmlReader.Element> it = root.getChildrenByName("tileset").iterator();
        while (it.hasNext()) {
            XmlReader.Element tileset = it.next();
            String source = tileset.getAttribute("source", null);
            if (source != null) {
                FileHandle tsxFile = getRelativeFileHandle(tmxFile, source);
                XmlReader.Element tileset2 = this.xml.parse(tsxFile);
                if (tileset2.getChildByName("image") != null) {
                    images.add(getRelativeFileHandle(tsxFile, tileset2.getChildByName("image").getAttribute("source")));
                } else {
                    Iterator<XmlReader.Element> it2 = tileset2.getChildrenByName("tile").iterator();
                    while (it2.hasNext()) {
                        images.add(getRelativeFileHandle(tsxFile, it2.next().getChildByName("image").getAttribute("source")));
                    }
                }
            } else if (tileset.getChildByName("image") != null) {
                images.add(getRelativeFileHandle(tmxFile, tileset.getChildByName("image").getAttribute("source")));
            } else {
                Iterator<XmlReader.Element> it3 = tileset.getChildrenByName("tile").iterator();
                while (it3.hasNext()) {
                    images.add(getRelativeFileHandle(tmxFile, it3.next().getChildByName("image").getAttribute("source")));
                }
            }
        }
        return images;
    }

    /* access modifiers changed from: protected */
    public Array<FileHandle> loadImages(XmlReader.Element root, FileHandle tmxFile) throws IOException {
        Array<FileHandle> images = new Array<>();
        Iterator<XmlReader.Element> it = root.getChildrenByName("imagelayer").iterator();
        while (it.hasNext()) {
            String source = it.next().getChildByName("image").getAttribute("source", null);
            if (source != null) {
                FileHandle handle = getRelativeFileHandle(tmxFile, source);
                if (!images.contains(handle, false)) {
                    images.add(handle);
                }
            }
        }
        return images;
    }

    /* access modifiers changed from: protected */
    public void loadTileSet(TiledMap map, XmlReader.Element element, FileHandle tmxFile, ImageResolver imageResolver) {
        int i;
        int id;
        int i2;
        if (element.getName().equals("tileset")) {
            String name = element.get("name", null);
            int firstgid = element.getIntAttribute("firstgid", 1);
            int tilewidth = element.getIntAttribute("tilewidth", 0);
            int tileheight = element.getIntAttribute("tileheight", 0);
            int spacing = element.getIntAttribute("spacing", 0);
            int margin = element.getIntAttribute("margin", 0);
            String source = element.getAttribute("source", null);
            int offsetX = 0;
            int offsetY = 0;
            String imageSource = "";
            int imageWidth = 0;
            int imageHeight = 0;
            FileHandle image = null;
            if (source != null) {
                FileHandle tsx = getRelativeFileHandle(tmxFile, source);
                try {
                    element = this.xml.parse(tsx);
                    name = element.get("name", null);
                    tilewidth = element.getIntAttribute("tilewidth", 0);
                    tileheight = element.getIntAttribute("tileheight", 0);
                    spacing = element.getIntAttribute("spacing", 0);
                    margin = element.getIntAttribute("margin", 0);
                    XmlReader.Element offset = element.getChildByName("tileoffset");
                    if (offset != null) {
                        offsetX = offset.getIntAttribute("x", 0);
                        offsetY = offset.getIntAttribute("y", 0);
                    }
                    XmlReader.Element imageElement = element.getChildByName("image");
                    if (imageElement != null) {
                        imageSource = imageElement.getAttribute("source");
                        imageWidth = imageElement.getIntAttribute("width", 0);
                        imageHeight = imageElement.getIntAttribute("height", 0);
                        image = getRelativeFileHandle(tsx, imageSource);
                    }
                } catch (IOException e) {
                    throw new GdxRuntimeException("Error parsing external tileset.");
                }
            } else {
                XmlReader.Element offset2 = element.getChildByName("tileoffset");
                if (offset2 != null) {
                    offsetX = offset2.getIntAttribute("x", 0);
                    offsetY = offset2.getIntAttribute("y", 0);
                }
                XmlReader.Element imageElement2 = element.getChildByName("image");
                if (imageElement2 != null) {
                    imageSource = imageElement2.getAttribute("source");
                    imageWidth = imageElement2.getIntAttribute("width", 0);
                    imageHeight = imageElement2.getIntAttribute("height", 0);
                    image = getRelativeFileHandle(tmxFile, imageSource);
                }
            }
            TiledMapTileSet tileset = new TiledMapTileSet();
            tileset.setName(name);
            tileset.getProperties().put("firstgid", Integer.valueOf(firstgid));
            if (image != null) {
                TextureRegion texture = imageResolver.getImage(image.path());
                MapProperties props = tileset.getProperties();
                props.put("imagesource", imageSource);
                props.put("imagewidth", Integer.valueOf(imageWidth));
                props.put("imageheight", Integer.valueOf(imageHeight));
                props.put("tilewidth", Integer.valueOf(tilewidth));
                props.put("tileheight", Integer.valueOf(tileheight));
                props.put("margin", Integer.valueOf(margin));
                props.put("spacing", Integer.valueOf(spacing));
                int stopWidth = texture.getRegionWidth() - tilewidth;
                int stopHeight = texture.getRegionHeight() - tileheight;
                int id2 = firstgid;
                int y = margin;
                while (y <= stopHeight) {
                    int x = margin;
                    while (true) {
                        id = id2;
                        if (x > stopWidth) {
                            break;
                        }
                        StaticTiledMapTile staticTiledMapTile = new StaticTiledMapTile(new TextureRegion(texture, x, y, tilewidth, tileheight));
                        staticTiledMapTile.setId(id);
                        staticTiledMapTile.setOffsetX((float) offsetX);
                        if (this.flipY) {
                            i2 = -offsetY;
                        } else {
                            i2 = offsetY;
                        }
                        staticTiledMapTile.setOffsetY((float) i2);
                        id2 = id + 1;
                        tileset.putTile(id, staticTiledMapTile);
                        x += tilewidth + spacing;
                    }
                    y += tileheight + spacing;
                    id2 = id;
                }
            } else {
                Iterator<XmlReader.Element> it = element.getChildrenByName("tile").iterator();
                while (it.hasNext()) {
                    XmlReader.Element tileElement = it.next();
                    XmlReader.Element imageElement3 = tileElement.getChildByName("image");
                    if (imageElement3 != null) {
                        String imageSource2 = imageElement3.getAttribute("source");
                        int imageWidth2 = imageElement3.getIntAttribute("width", 0);
                        int imageHeight2 = imageElement3.getIntAttribute("height", 0);
                        image = getRelativeFileHandle(tmxFile, imageSource2);
                    }
                    StaticTiledMapTile staticTiledMapTile2 = new StaticTiledMapTile(imageResolver.getImage(image.path()));
                    staticTiledMapTile2.setId(tileElement.getIntAttribute(dc.V) + firstgid);
                    staticTiledMapTile2.setOffsetX((float) offsetX);
                    if (this.flipY) {
                        i = -offsetY;
                    } else {
                        i = offsetY;
                    }
                    staticTiledMapTile2.setOffsetY((float) i);
                    tileset.putTile(staticTiledMapTile2.getId(), staticTiledMapTile2);
                }
            }
            Array<XmlReader.Element> tileElements = element.getChildrenByName("tile");
            Array<AnimatedTiledMapTile> animatedTiles = new Array<>();
            Iterator<XmlReader.Element> it2 = tileElements.iterator();
            while (it2.hasNext()) {
                XmlReader.Element tileElement2 = it2.next();
                TiledMapTile tile = tileset.getTile(firstgid + tileElement2.getIntAttribute(dc.V, 0));
                if (tile != null) {
                    XmlReader.Element animationElement = tileElement2.getChildByName("animation");
                    if (animationElement != null) {
                        Array<StaticTiledMapTile> staticTiles = new Array<>();
                        IntArray intervals = new IntArray();
                        Iterator<XmlReader.Element> it3 = animationElement.getChildrenByName("frame").iterator();
                        while (it3.hasNext()) {
                            XmlReader.Element frameElement = it3.next();
                            staticTiles.add((StaticTiledMapTile) tileset.getTile(frameElement.getIntAttribute("tileid") + firstgid));
                            intervals.add(frameElement.getIntAttribute("duration"));
                        }
                        AnimatedTiledMapTile animatedTile = new AnimatedTiledMapTile(intervals, staticTiles);
                        animatedTile.setId(tile.getId());
                        animatedTiles.add(animatedTile);
                        tile = animatedTile;
                    }
                    String terrain = tileElement2.getAttribute("terrain", null);
                    if (terrain != null) {
                        tile.getProperties().put("terrain", terrain);
                    }
                    String probability = tileElement2.getAttribute("probability", null);
                    if (probability != null) {
                        tile.getProperties().put("probability", probability);
                    }
                    XmlReader.Element properties = tileElement2.getChildByName("properties");
                    if (properties != null) {
                        loadProperties(tile.getProperties(), properties);
                    }
                }
            }
            Iterator it4 = animatedTiles.iterator();
            while (it4.hasNext()) {
                AnimatedTiledMapTile tile2 = (AnimatedTiledMapTile) it4.next();
                tileset.putTile(tile2.getId(), tile2);
            }
            XmlReader.Element properties2 = element.getChildByName("properties");
            if (properties2 != null) {
                loadProperties(tileset.getProperties(), properties2);
            }
            map.getTileSets().addTileSet(tileset);
        }
    }
}
