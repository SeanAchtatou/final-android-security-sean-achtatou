package com.badlogic.gdx.maps.tiled;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.BaseTmxMapLoader;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import java.io.IOException;
import java.util.Iterator;

public class AtlasTmxMapLoader extends BaseTmxMapLoader<AtlasTiledMapLoaderParameters> {
    protected Array<Texture> trackedTextures = new Array<>();

    public static class AtlasTiledMapLoaderParameters extends BaseTmxMapLoader.Parameters {
        public boolean forceTextureFilters = false;
    }

    private interface AtlasResolver {
        TextureAtlas getAtlas(String str);

        public static class DirectAtlasResolver implements AtlasResolver {
            private final ObjectMap<String, TextureAtlas> atlases;

            public DirectAtlasResolver(ObjectMap<String, TextureAtlas> atlases2) {
                this.atlases = atlases2;
            }

            public TextureAtlas getAtlas(String name) {
                return this.atlases.get(name);
            }
        }

        public static class AssetManagerAtlasResolver implements AtlasResolver {
            private final AssetManager assetManager;

            public AssetManagerAtlasResolver(AssetManager assetManager2) {
                this.assetManager = assetManager2;
            }

            public TextureAtlas getAtlas(String name) {
                return (TextureAtlas) this.assetManager.get(name, TextureAtlas.class);
            }
        }
    }

    public AtlasTmxMapLoader() {
        super(new InternalFileHandleResolver());
    }

    public AtlasTmxMapLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    public TiledMap load(String fileName) {
        return load(fileName, new AtlasTiledMapLoaderParameters());
    }

    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle tmxFile, AtlasTiledMapLoaderParameters parameter) {
        Array<AssetDescriptor> dependencies = new Array<>();
        try {
            this.root = this.xml.parse(tmxFile);
            XmlReader.Element properties = this.root.getChildByName("properties");
            if (properties != null) {
                Iterator<XmlReader.Element> it = properties.getChildrenByName("property").iterator();
                while (it.hasNext()) {
                    XmlReader.Element property = it.next();
                    String name = property.getAttribute("name");
                    String value = property.getAttribute("value");
                    if (name.startsWith("atlas")) {
                        dependencies.add(new AssetDescriptor(getRelativeFileHandle(tmxFile, value), TextureAtlas.class));
                    }
                }
            }
            return dependencies;
        } catch (IOException e) {
            throw new GdxRuntimeException("Unable to parse .tmx file.");
        }
    }

    public TiledMap load(String fileName, AtlasTiledMapLoaderParameters parameter) {
        if (parameter != null) {
            try {
                this.convertObjectToTileSpace = parameter.convertObjectToTileSpace;
                this.flipY = parameter.flipY;
            } catch (IOException e) {
                throw new GdxRuntimeException("Couldn't load tilemap '" + fileName + "'", e);
            }
        } else {
            this.convertObjectToTileSpace = false;
            this.flipY = true;
        }
        FileHandle tmxFile = resolve(fileName);
        this.root = this.xml.parse(tmxFile);
        ObjectMap<String, TextureAtlas> atlases = new ObjectMap<>();
        FileHandle atlasFile = loadAtlas(this.root, tmxFile);
        if (atlasFile == null) {
            throw new GdxRuntimeException("Couldn't load atlas");
        }
        atlases.put(atlasFile.path(), new TextureAtlas(atlasFile));
        TiledMap map = loadMap(this.root, tmxFile, new AtlasResolver.DirectAtlasResolver(atlases));
        map.setOwnedResources(atlases.values().toArray());
        setTextureFilters(parameter.textureMinFilter, parameter.textureMagFilter);
        return map;
    }

    /* access modifiers changed from: protected */
    public FileHandle loadAtlas(XmlReader.Element root, FileHandle tmxFile) throws IOException {
        XmlReader.Element e = root.getChildByName("properties");
        if (e != null) {
            Iterator<XmlReader.Element> it = e.getChildrenByName("property").iterator();
            while (it.hasNext()) {
                XmlReader.Element property = it.next();
                String name = property.getAttribute("name", null);
                String value = property.getAttribute("value", null);
                if (name.equals("atlas")) {
                    if (value == null) {
                        value = property.getText();
                    }
                    if (!(value == null || value.length() == 0)) {
                        return getRelativeFileHandle(tmxFile, value);
                    }
                }
            }
        }
        FileHandle atlasFile = tmxFile.sibling(String.valueOf(tmxFile.nameWithoutExtension()) + ".atlas");
        if (atlasFile.exists()) {
            return atlasFile;
        }
        return null;
    }

    private void setTextureFilters(Texture.TextureFilter min, Texture.TextureFilter mag) {
        Iterator<Texture> it = this.trackedTextures.iterator();
        while (it.hasNext()) {
            it.next().setFilter(min, mag);
        }
        this.trackedTextures.clear();
    }

    public void loadAsync(AssetManager manager, String fileName, FileHandle tmxFile, AtlasTiledMapLoaderParameters parameter) {
        this.map = null;
        if (parameter != null) {
            this.convertObjectToTileSpace = parameter.convertObjectToTileSpace;
            this.flipY = parameter.flipY;
        } else {
            this.convertObjectToTileSpace = false;
            this.flipY = true;
        }
        try {
            this.map = loadMap(this.root, tmxFile, new AtlasResolver.AssetManagerAtlasResolver(manager));
        } catch (Exception e) {
            throw new GdxRuntimeException("Couldn't load tilemap '" + fileName + "'", e);
        }
    }

    public TiledMap loadSync(AssetManager manager, String fileName, FileHandle file, AtlasTiledMapLoaderParameters parameter) {
        if (parameter != null) {
            setTextureFilters(parameter.textureMinFilter, parameter.textureMagFilter);
        }
        return this.map;
    }

    /* access modifiers changed from: protected */
    public TiledMap loadMap(XmlReader.Element root, FileHandle tmxFile, AtlasResolver resolver) {
        TiledMap map = new TiledMap();
        String mapOrientation = root.getAttribute("orientation", null);
        int mapWidth = root.getIntAttribute("width", 0);
        int mapHeight = root.getIntAttribute("height", 0);
        int tileWidth = root.getIntAttribute("tilewidth", 0);
        int tileHeight = root.getIntAttribute("tileheight", 0);
        String mapBackgroundColor = root.getAttribute("backgroundcolor", null);
        MapProperties mapProperties = map.getProperties();
        if (mapOrientation != null) {
            mapProperties.put("orientation", mapOrientation);
        }
        mapProperties.put("width", Integer.valueOf(mapWidth));
        mapProperties.put("height", Integer.valueOf(mapHeight));
        mapProperties.put("tilewidth", Integer.valueOf(tileWidth));
        mapProperties.put("tileheight", Integer.valueOf(tileHeight));
        if (mapBackgroundColor != null) {
            mapProperties.put("backgroundcolor", mapBackgroundColor);
        }
        this.mapTileWidth = tileWidth;
        this.mapTileHeight = tileHeight;
        this.mapWidthInPixels = mapWidth * tileWidth;
        this.mapHeightInPixels = mapHeight * tileHeight;
        if (mapOrientation != null && "staggered".equals(mapOrientation) && mapHeight > 1) {
            this.mapWidthInPixels = this.mapWidthInPixels + (tileWidth / 2);
            this.mapHeightInPixels = (this.mapHeightInPixels / 2) + (tileHeight / 2);
        }
        int j = root.getChildCount();
        for (int i = 0; i < j; i++) {
            XmlReader.Element element = root.getChild(i);
            String elementName = element.getName();
            if (elementName.equals("properties")) {
                loadProperties(map.getProperties(), element);
            } else if (elementName.equals("tileset")) {
                loadTileset(map, element, tmxFile, resolver);
            } else if (elementName.equals("layer")) {
                loadTileLayer(map, element);
            } else if (elementName.equals("objectgroup")) {
                loadObjectGroup(map, element);
            }
        }
        return map;
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r36v5, resolved type: com.badlogic.gdx.maps.tiled.TiledMapTile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r36v6, resolved type: com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r36v7, resolved type: com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r36v8, resolved type: com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v114, resolved type: com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadTileset(com.badlogic.gdx.maps.tiled.TiledMap r48, com.badlogic.gdx.utils.XmlReader.Element r49, com.badlogic.gdx.files.FileHandle r50, com.badlogic.gdx.maps.tiled.AtlasTmxMapLoader.AtlasResolver r51) {
        /*
            r47 = this;
            java.lang.String r44 = r49.getName()
            java.lang.String r45 = "tileset"
            boolean r44 = r44.equals(r45)
            if (r44 == 0) goto L_0x02fb
            java.lang.String r44 = "name"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            java.lang.String r21 = r0.get(r1, r2)
            java.lang.String r44 = "firstgid"
            r45 = 1
            r0 = r49
            r1 = r44
            r2 = r45
            int r11 = r0.getIntAttribute(r1, r2)
            java.lang.String r44 = "tilewidth"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r42 = r0.getIntAttribute(r1, r2)
            java.lang.String r44 = "tileheight"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r39 = r0.getIntAttribute(r1, r2)
            java.lang.String r44 = "spacing"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r32 = r0.getIntAttribute(r1, r2)
            java.lang.String r44 = "margin"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r20 = r0.getIntAttribute(r1, r2)
            java.lang.String r44 = "source"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            java.lang.String r31 = r0.getAttribute(r1, r2)
            r23 = 0
            r24 = 0
            java.lang.String r16 = ""
            r17 = 0
            r15 = 0
            r13 = 0
            if (r31 == 0) goto L_0x017e
            r0 = r50
            r1 = r31
            com.badlogic.gdx.files.FileHandle r43 = getRelativeFileHandle(r0, r1)
            r0 = r47
            com.badlogic.gdx.utils.XmlReader r0 = r0.xml     // Catch:{ IOException -> 0x0175 }
            r44 = r0
            r0 = r44
            r1 = r43
            com.badlogic.gdx.utils.XmlReader$Element r49 = r0.parse(r1)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "name"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            java.lang.String r21 = r0.get(r1, r2)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "tilewidth"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r42 = r0.getIntAttribute(r1, r2)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "tileheight"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r39 = r0.getIntAttribute(r1, r2)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "spacing"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r32 = r0.getIntAttribute(r1, r2)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "margin"
            r45 = 0
            r0 = r49
            r1 = r44
            r2 = r45
            int r20 = r0.getIntAttribute(r1, r2)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "tileoffset"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r22 = r0.getChildByName(r1)     // Catch:{ IOException -> 0x0175 }
            if (r22 == 0) goto L_0x00fe
            java.lang.String r44 = "x"
            r45 = 0
            r0 = r22
            r1 = r44
            r2 = r45
            int r23 = r0.getIntAttribute(r1, r2)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "y"
            r45 = 0
            r0 = r22
            r1 = r44
            r2 = r45
            int r24 = r0.getIntAttribute(r1, r2)     // Catch:{ IOException -> 0x0175 }
        L_0x00fe:
            java.lang.String r44 = "image"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r14 = r0.getChildByName(r1)     // Catch:{ IOException -> 0x0175 }
            if (r14 == 0) goto L_0x0132
            java.lang.String r44 = "source"
            r0 = r44
            java.lang.String r16 = r14.getAttribute(r0)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "width"
            r45 = 0
            r0 = r44
            r1 = r45
            int r17 = r14.getIntAttribute(r0, r1)     // Catch:{ IOException -> 0x0175 }
            java.lang.String r44 = "height"
            r45 = 0
            r0 = r44
            r1 = r45
            int r15 = r14.getIntAttribute(r0, r1)     // Catch:{ IOException -> 0x0175 }
            r0 = r43
            r1 = r16
            com.badlogic.gdx.files.FileHandle r13 = getRelativeFileHandle(r0, r1)     // Catch:{ IOException -> 0x0175 }
        L_0x0132:
            com.badlogic.gdx.maps.MapProperties r44 = r48.getProperties()
            java.lang.String r45 = "atlas"
            java.lang.Class<java.lang.String> r46 = java.lang.String.class
            java.lang.Object r8 = r44.get(r45, r46)
            java.lang.String r8 = (java.lang.String) r8
            if (r8 != 0) goto L_0x016b
            java.lang.StringBuilder r44 = new java.lang.StringBuilder
            java.lang.String r45 = r50.nameWithoutExtension()
            java.lang.String r45 = java.lang.String.valueOf(r45)
            r44.<init>(r45)
            java.lang.String r45 = ".atlas"
            java.lang.StringBuilder r44 = r44.append(r45)
            java.lang.String r44 = r44.toString()
            r0 = r50
            r1 = r44
            com.badlogic.gdx.files.FileHandle r7 = r0.sibling(r1)
            boolean r44 = r7.exists()
            if (r44 == 0) goto L_0x016b
            java.lang.String r8 = r7.name()
        L_0x016b:
            if (r8 != 0) goto L_0x01dc
            com.badlogic.gdx.utils.GdxRuntimeException r44 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r45 = "The map is missing the 'atlas' property"
            r44.<init>(r45)
            throw r44
        L_0x0175:
            r10 = move-exception
            com.badlogic.gdx.utils.GdxRuntimeException r44 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.String r45 = "Error parsing external tileset."
            r44.<init>(r45)
            throw r44
        L_0x017e:
            java.lang.String r44 = "tileoffset"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r22 = r0.getChildByName(r1)
            if (r22 == 0) goto L_0x01a6
            java.lang.String r44 = "x"
            r45 = 0
            r0 = r22
            r1 = r44
            r2 = r45
            int r23 = r0.getIntAttribute(r1, r2)
            java.lang.String r44 = "y"
            r45 = 0
            r0 = r22
            r1 = r44
            r2 = r45
            int r24 = r0.getIntAttribute(r1, r2)
        L_0x01a6:
            java.lang.String r44 = "image"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r14 = r0.getChildByName(r1)
            if (r14 == 0) goto L_0x0132
            java.lang.String r44 = "source"
            r0 = r44
            java.lang.String r16 = r14.getAttribute(r0)
            java.lang.String r44 = "width"
            r45 = 0
            r0 = r44
            r1 = r45
            int r17 = r14.getIntAttribute(r0, r1)
            java.lang.String r44 = "height"
            r45 = 0
            r0 = r44
            r1 = r45
            int r15 = r14.getIntAttribute(r0, r1)
            r0 = r50
            r1 = r16
            com.badlogic.gdx.files.FileHandle r13 = getRelativeFileHandle(r0, r1)
            goto L_0x0132
        L_0x01dc:
            r0 = r50
            com.badlogic.gdx.files.FileHandle r9 = getRelativeFileHandle(r0, r8)
            java.lang.String r44 = r9.path()
            r0 = r47
            r1 = r44
            com.badlogic.gdx.files.FileHandle r9 = r0.resolve(r1)
            java.lang.String r44 = r9.path()
            r0 = r51
            r1 = r44
            com.badlogic.gdx.graphics.g2d.TextureAtlas r6 = r0.getAtlas(r1)
            java.lang.String r30 = r9.nameWithoutExtension()
            com.badlogic.gdx.utils.ObjectSet r44 = r6.getTextures()
            java.util.Iterator r44 = r44.iterator()
        L_0x0206:
            boolean r45 = r44.hasNext()
            if (r45 != 0) goto L_0x02fc
            com.badlogic.gdx.maps.tiled.TiledMapTileSet r41 = new com.badlogic.gdx.maps.tiled.TiledMapTileSet
            r41.<init>()
            com.badlogic.gdx.maps.MapProperties r27 = r41.getProperties()
            r0 = r41
            r1 = r21
            r0.setName(r1)
            java.lang.String r44 = "firstgid"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r11)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            java.lang.String r44 = "imagesource"
            r0 = r27
            r1 = r44
            r2 = r16
            r0.put(r1, r2)
            java.lang.String r44 = "imagewidth"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r17)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            java.lang.String r44 = "imageheight"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r15)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            java.lang.String r44 = "tilewidth"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r42)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            java.lang.String r44 = "tileheight"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r39)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            java.lang.String r44 = "margin"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r20)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            java.lang.String r44 = "spacing"
            java.lang.Integer r45 = java.lang.Integer.valueOf(r32)
            r0 = r27
            r1 = r44
            r2 = r45
            r0.put(r1, r2)
            r0 = r30
            com.badlogic.gdx.utils.Array r44 = r6.findRegions(r0)
            java.util.Iterator r45 = r44.iterator()
        L_0x029a:
            boolean r44 = r45.hasNext()
            if (r44 != 0) goto L_0x0311
            java.lang.String r44 = "tile"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.Array r44 = r0.getChildrenByName(r1)
            java.util.Iterator r45 = r44.iterator()
        L_0x02ae:
            boolean r44 = r45.hasNext()
            if (r44 != 0) goto L_0x0364
            java.lang.String r44 = "tile"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.Array r38 = r0.getChildrenByName(r1)
            com.badlogic.gdx.utils.Array r4 = new com.badlogic.gdx.utils.Array
            r4.<init>()
            java.util.Iterator r45 = r38.iterator()
        L_0x02c7:
            boolean r44 = r45.hasNext()
            if (r44 != 0) goto L_0x046b
            java.util.Iterator r44 = r4.iterator()
        L_0x02d1:
            boolean r45 = r44.hasNext()
            if (r45 != 0) goto L_0x0555
            java.lang.String r44 = "properties"
            r0 = r49
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r26 = r0.getChildByName(r1)
            if (r26 == 0) goto L_0x02f0
            com.badlogic.gdx.maps.MapProperties r44 = r41.getProperties()
            r0 = r47
            r1 = r44
            r2 = r26
            r0.loadProperties(r1, r2)
        L_0x02f0:
            com.badlogic.gdx.maps.tiled.TiledMapTileSets r44 = r48.getTileSets()
            r0 = r44
            r1 = r41
            r0.addTileSet(r1)
        L_0x02fb:
            return
        L_0x02fc:
            java.lang.Object r35 = r44.next()
            com.badlogic.gdx.graphics.Texture r35 = (com.badlogic.gdx.graphics.Texture) r35
            r0 = r47
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.Texture> r0 = r0.trackedTextures
            r45 = r0
            r0 = r45
            r1 = r35
            r0.add(r1)
            goto L_0x0206
        L_0x0311:
            java.lang.Object r28 = r45.next()
            com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasRegion r28 = (com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion) r28
            if (r28 == 0) goto L_0x029a
            com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile r36 = new com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
            r0 = r36
            r1 = r28
            r0.<init>(r1)
            r0 = r28
            int r0 = r0.index
            r44 = r0
            int r40 = r11 + r44
            r0 = r36
            r1 = r40
            r0.setId(r1)
            r0 = r23
            float r0 = (float) r0
            r44 = r0
            r0 = r36
            r1 = r44
            r0.setOffsetX(r1)
            r0 = r47
            boolean r0 = r0.flipY
            r44 = r0
            if (r44 == 0) goto L_0x0361
            r0 = r24
            int r0 = -r0
            r44 = r0
        L_0x034a:
            r0 = r44
            float r0 = (float) r0
            r44 = r0
            r0 = r36
            r1 = r44
            r0.setOffsetY(r1)
            r0 = r41
            r1 = r40
            r2 = r36
            r0.putTile(r1, r2)
            goto L_0x029a
        L_0x0361:
            r44 = r24
            goto L_0x034a
        L_0x0364:
            java.lang.Object r37 = r45.next()
            com.badlogic.gdx.utils.XmlReader$Element r37 = (com.badlogic.gdx.utils.XmlReader.Element) r37
            java.lang.String r44 = "id"
            r46 = 0
            r0 = r37
            r1 = r44
            r2 = r46
            int r44 = r0.getIntAttribute(r1, r2)
            int r40 = r11 + r44
            r0 = r41
            r1 = r40
            com.badlogic.gdx.maps.tiled.TiledMapTile r36 = r0.getTile(r1)
            if (r36 != 0) goto L_0x040d
            java.lang.String r44 = "image"
            r0 = r37
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r14 = r0.getChildByName(r1)
            if (r14 == 0) goto L_0x040d
            java.lang.String r44 = "source"
            r0 = r44
            java.lang.String r29 = r14.getAttribute(r0)
            r44 = 0
            r46 = 46
            r0 = r29
            r1 = r46
            int r46 = r0.lastIndexOf(r1)
            r0 = r29
            r1 = r44
            r2 = r46
            java.lang.String r29 = r0.substring(r1, r2)
            r0 = r29
            com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasRegion r28 = r6.findRegion(r0)
            if (r28 != 0) goto L_0x03cf
            com.badlogic.gdx.utils.GdxRuntimeException r44 = new com.badlogic.gdx.utils.GdxRuntimeException
            java.lang.StringBuilder r45 = new java.lang.StringBuilder
            java.lang.String r46 = "Tileset region not found: "
            r45.<init>(r46)
            r0 = r45
            r1 = r29
            java.lang.StringBuilder r45 = r0.append(r1)
            java.lang.String r45 = r45.toString()
            r44.<init>(r45)
            throw r44
        L_0x03cf:
            com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile r36 = new com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
            r0 = r36
            r1 = r28
            r0.<init>(r1)
            r0 = r36
            r1 = r40
            r0.setId(r1)
            r0 = r23
            float r0 = (float) r0
            r44 = r0
            r0 = r36
            r1 = r44
            r0.setOffsetX(r1)
            r0 = r47
            boolean r0 = r0.flipY
            r44 = r0
            if (r44 == 0) goto L_0x0468
            r0 = r24
            int r0 = -r0
            r44 = r0
        L_0x03f8:
            r0 = r44
            float r0 = (float) r0
            r44 = r0
            r0 = r36
            r1 = r44
            r0.setOffsetY(r1)
            r0 = r41
            r1 = r40
            r2 = r36
            r0.putTile(r1, r2)
        L_0x040d:
            if (r36 == 0) goto L_0x02ae
            java.lang.String r44 = "terrain"
            r46 = 0
            r0 = r37
            r1 = r44
            r2 = r46
            java.lang.String r34 = r0.getAttribute(r1, r2)
            if (r34 == 0) goto L_0x042e
            com.badlogic.gdx.maps.MapProperties r44 = r36.getProperties()
            java.lang.String r46 = "terrain"
            r0 = r44
            r1 = r46
            r2 = r34
            r0.put(r1, r2)
        L_0x042e:
            java.lang.String r44 = "probability"
            r46 = 0
            r0 = r37
            r1 = r44
            r2 = r46
            java.lang.String r25 = r0.getAttribute(r1, r2)
            if (r25 == 0) goto L_0x044d
            com.badlogic.gdx.maps.MapProperties r44 = r36.getProperties()
            java.lang.String r46 = "probability"
            r0 = r44
            r1 = r46
            r2 = r25
            r0.put(r1, r2)
        L_0x044d:
            java.lang.String r44 = "properties"
            r0 = r37
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r26 = r0.getChildByName(r1)
            if (r26 == 0) goto L_0x02ae
            com.badlogic.gdx.maps.MapProperties r44 = r36.getProperties()
            r0 = r47
            r1 = r44
            r2 = r26
            r0.loadProperties(r1, r2)
            goto L_0x02ae
        L_0x0468:
            r44 = r24
            goto L_0x03f8
        L_0x046b:
            java.lang.Object r37 = r45.next()
            com.badlogic.gdx.utils.XmlReader$Element r37 = (com.badlogic.gdx.utils.XmlReader.Element) r37
            java.lang.String r44 = "id"
            r46 = 0
            r0 = r37
            r1 = r44
            r2 = r46
            int r19 = r0.getIntAttribute(r1, r2)
            int r44 = r11 + r19
            r0 = r41
            r1 = r44
            com.badlogic.gdx.maps.tiled.TiledMapTile r36 = r0.getTile(r1)
            if (r36 == 0) goto L_0x02c7
            java.lang.String r44 = "animation"
            r0 = r37
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r5 = r0.getChildByName(r1)
            if (r5 == 0) goto L_0x04ca
            com.badlogic.gdx.utils.Array r33 = new com.badlogic.gdx.utils.Array
            r33.<init>()
            com.badlogic.gdx.utils.IntArray r18 = new com.badlogic.gdx.utils.IntArray
            r18.<init>()
            java.lang.String r44 = "frame"
            r0 = r44
            com.badlogic.gdx.utils.Array r44 = r5.getChildrenByName(r0)
            java.util.Iterator r46 = r44.iterator()
        L_0x04ad:
            boolean r44 = r46.hasNext()
            if (r44 != 0) goto L_0x0523
            com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile r3 = new com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile
            r0 = r18
            r1 = r33
            r3.<init>(r0, r1)
            int r44 = r36.getId()
            r0 = r44
            r3.setId(r0)
            r4.add(r3)
            r36 = r3
        L_0x04ca:
            java.lang.String r44 = "terrain"
            r46 = 0
            r0 = r37
            r1 = r44
            r2 = r46
            java.lang.String r34 = r0.getAttribute(r1, r2)
            if (r34 == 0) goto L_0x04e9
            com.badlogic.gdx.maps.MapProperties r44 = r36.getProperties()
            java.lang.String r46 = "terrain"
            r0 = r44
            r1 = r46
            r2 = r34
            r0.put(r1, r2)
        L_0x04e9:
            java.lang.String r44 = "probability"
            r46 = 0
            r0 = r37
            r1 = r44
            r2 = r46
            java.lang.String r25 = r0.getAttribute(r1, r2)
            if (r25 == 0) goto L_0x0508
            com.badlogic.gdx.maps.MapProperties r44 = r36.getProperties()
            java.lang.String r46 = "probability"
            r0 = r44
            r1 = r46
            r2 = r25
            r0.put(r1, r2)
        L_0x0508:
            java.lang.String r44 = "properties"
            r0 = r37
            r1 = r44
            com.badlogic.gdx.utils.XmlReader$Element r26 = r0.getChildByName(r1)
            if (r26 == 0) goto L_0x02c7
            com.badlogic.gdx.maps.MapProperties r44 = r36.getProperties()
            r0 = r47
            r1 = r44
            r2 = r26
            r0.loadProperties(r1, r2)
            goto L_0x02c7
        L_0x0523:
            java.lang.Object r12 = r46.next()
            com.badlogic.gdx.utils.XmlReader$Element r12 = (com.badlogic.gdx.utils.XmlReader.Element) r12
            java.lang.String r44 = "tileid"
            r0 = r44
            int r44 = r12.getIntAttribute(r0)
            int r44 = r44 + r11
            r0 = r41
            r1 = r44
            com.badlogic.gdx.maps.tiled.TiledMapTile r44 = r0.getTile(r1)
            com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile r44 = (com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile) r44
            r0 = r33
            r1 = r44
            r0.add(r1)
            java.lang.String r44 = "duration"
            r0 = r44
            int r44 = r12.getIntAttribute(r0)
            r0 = r18
            r1 = r44
            r0.add(r1)
            goto L_0x04ad
        L_0x0555:
            java.lang.Object r36 = r44.next()
            com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile r36 = (com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile) r36
            int r45 = r36.getId()
            r0 = r41
            r1 = r45
            r2 = r36
            r0.putTile(r1, r2)
            goto L_0x02d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.maps.tiled.AtlasTmxMapLoader.loadTileset(com.badlogic.gdx.maps.tiled.TiledMap, com.badlogic.gdx.utils.XmlReader$Element, com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.maps.tiled.AtlasTmxMapLoader$AtlasResolver):void");
    }
}
