package com.badlogic.gdx.maps.tiled;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.ImageResolver;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.StreamUtils;
import com.badlogic.gdx.utils.XmlReader;
import com.kbz.esotericsoftware.spine.Animation;
import com.tendcloud.tenddata.dc;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public abstract class BaseTmxMapLoader<P extends AssetLoaderParameters<TiledMap>> extends AsynchronousAssetLoader<TiledMap, P> {
    protected static final int FLAG_FLIP_DIAGONALLY = 536870912;
    protected static final int FLAG_FLIP_HORIZONTALLY = Integer.MIN_VALUE;
    protected static final int FLAG_FLIP_VERTICALLY = 1073741824;
    protected static final int MASK_CLEAR = -536870912;
    protected static String pathString;
    protected boolean convertObjectToTileSpace;
    protected boolean flipY = true;
    protected TiledMap map;
    protected int mapHeightInPixels;
    protected int mapTileHeight;
    protected int mapTileWidth;
    protected int mapWidthInPixels;
    protected XmlReader.Element root;
    protected XmlReader xml = new XmlReader();

    public static class Parameters extends AssetLoaderParameters<TiledMap> {
        public boolean convertObjectToTileSpace = false;
        public boolean flipY = true;
        public boolean generateMipMaps = false;
        public Texture.TextureFilter textureMagFilter = Texture.TextureFilter.Nearest;
        public Texture.TextureFilter textureMinFilter = Texture.TextureFilter.Nearest;
    }

    public BaseTmxMapLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    /* access modifiers changed from: protected */
    public void loadTileLayer(TiledMap map2, XmlReader.Element element) {
        int i;
        if (element.getName().equals("layer")) {
            int width = element.getIntAttribute("width", 0);
            int height = element.getIntAttribute("height", 0);
            TiledMapTileLayer layer = new TiledMapTileLayer(width, height, element.getParent().getIntAttribute("tilewidth", 0), element.getParent().getIntAttribute("tileheight", 0));
            loadBasicLayerInfo(layer, element);
            int[] ids = getTileIds(element, width, height);
            TiledMapTileSets tilesets = map2.getTileSets();
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int id = ids[(y * width) + x];
                    boolean flipHorizontally = (FLAG_FLIP_HORIZONTALLY & id) != 0;
                    boolean flipVertically = (FLAG_FLIP_VERTICALLY & id) != 0;
                    boolean flipDiagonally = (FLAG_FLIP_DIAGONALLY & id) != 0;
                    TiledMapTile tile = tilesets.getTile(536870911 & id);
                    if (tile != null) {
                        TiledMapTileLayer.Cell cell = createTileLayerCell(flipHorizontally, flipVertically, flipDiagonally);
                        cell.setTile(tile);
                        if (this.flipY) {
                            i = (height - 1) - y;
                        } else {
                            i = y;
                        }
                        layer.setCell(x, i, cell);
                    }
                }
            }
            XmlReader.Element properties = element.getChildByName("properties");
            if (properties != null) {
                loadProperties(layer.getProperties(), properties);
            }
            map2.getLayers().add(layer);
        }
    }

    /* access modifiers changed from: protected */
    public void loadObjectGroup(TiledMap map2, XmlReader.Element element) {
        if (element.getName().equals("objectgroup")) {
            String name = element.getAttribute("name", null);
            MapLayer layer = new MapLayer();
            layer.setName(name);
            XmlReader.Element properties = element.getChildByName("properties");
            if (properties != null) {
                loadProperties(layer.getProperties(), properties);
            }
            Iterator<XmlReader.Element> it = element.getChildrenByName("object").iterator();
            while (it.hasNext()) {
                loadObject(map2, layer, it.next());
            }
            map2.getLayers().add(layer);
        }
    }

    /* access modifiers changed from: protected */
    public void loadImageLayer(TiledMap map2, XmlReader.Element element, FileHandle tmxFile, ImageResolver imageResolver) {
        if (element.getName().equals("imagelayer")) {
            int x = Integer.parseInt(element.getAttribute("x", "0"));
            int y = Integer.parseInt(element.getAttribute("y", "0"));
            if (this.flipY) {
                y = this.mapHeightInPixels - y;
            }
            TextureRegion texture = null;
            XmlReader.Element image = element.getChildByName("image");
            if (image != null) {
                texture = imageResolver.getImage(getRelativeFileHandle(tmxFile, image.getAttribute("source")).path());
                y -= texture.getRegionHeight();
            }
            TiledMapImageLayer layer = new TiledMapImageLayer(texture, x, y);
            loadBasicLayerInfo(layer, element);
            XmlReader.Element properties = element.getChildByName("properties");
            if (properties != null) {
                loadProperties(layer.getProperties(), properties);
            }
            map2.getLayers().add(layer);
        }
    }

    /* access modifiers changed from: protected */
    public void loadBasicLayerInfo(MapLayer layer, XmlReader.Element element) {
        boolean visible = true;
        String name = element.getAttribute("name", null);
        float opacity = Float.parseFloat(element.getAttribute("opacity", "1.0"));
        if (element.getIntAttribute("visible", 1) != 1) {
            visible = false;
        }
        layer.setName(name);
        layer.setOpacity(opacity);
        layer.setVisible(visible);
    }

    /* access modifiers changed from: protected */
    public void loadObject(TiledMap map2, MapLayer layer, XmlReader.Element element) {
        float floatAttribute;
        float f;
        if (element.getName().equals("object")) {
            MapObject object = null;
            float scaleX = this.convertObjectToTileSpace ? 1.0f / ((float) this.mapTileWidth) : 1.0f;
            float scaleY = this.convertObjectToTileSpace ? 1.0f / ((float) this.mapTileHeight) : 1.0f;
            float x = element.getFloatAttribute("x", Animation.CurveTimeline.LINEAR) * scaleX;
            if (this.flipY) {
                floatAttribute = ((float) this.mapHeightInPixels) - element.getFloatAttribute("y", Animation.CurveTimeline.LINEAR);
            } else {
                floatAttribute = element.getFloatAttribute("y", Animation.CurveTimeline.LINEAR);
            }
            float y = floatAttribute * scaleY;
            float width = element.getFloatAttribute("width", Animation.CurveTimeline.LINEAR) * scaleX;
            float height = element.getFloatAttribute("height", Animation.CurveTimeline.LINEAR) * scaleY;
            if (element.getChildCount() > 0) {
                XmlReader.Element child = element.getChildByName("polygon");
                if (child != null) {
                    String[] points = child.getAttribute("points").split(" ");
                    float[] vertices = new float[(points.length * 2)];
                    for (int i = 0; i < points.length; i++) {
                        String[] point = points[i].split(",");
                        vertices[i * 2] = Float.parseFloat(point[0]) * scaleX;
                        vertices[(i * 2) + 1] = ((float) (this.flipY ? -1 : 1)) * Float.parseFloat(point[1]) * scaleY;
                    }
                    Polygon polygon = new Polygon(vertices);
                    polygon.setPosition(x, y);
                    object = new PolygonMapObject(polygon);
                } else {
                    XmlReader.Element child2 = element.getChildByName("polyline");
                    if (child2 != null) {
                        String[] points2 = child2.getAttribute("points").split(" ");
                        float[] vertices2 = new float[(points2.length * 2)];
                        for (int i2 = 0; i2 < points2.length; i2++) {
                            String[] point2 = points2[i2].split(",");
                            vertices2[i2 * 2] = Float.parseFloat(point2[0]) * scaleX;
                            vertices2[(i2 * 2) + 1] = ((float) (this.flipY ? -1 : 1)) * Float.parseFloat(point2[1]) * scaleY;
                        }
                        Polyline polyline = new Polyline(vertices2);
                        polyline.setPosition(x, y);
                        object = new PolylineMapObject(polyline);
                    } else if (element.getChildByName("ellipse") != null) {
                        object = new EllipseMapObject(x, this.flipY ? y - height : y, width, height);
                    }
                }
            }
            if (object == null) {
                String gid = element.getAttribute("gid", null);
                if (gid != null) {
                    int id = (int) Long.parseLong(gid);
                    boolean flipHorizontally = (FLAG_FLIP_HORIZONTALLY & id) != 0;
                    boolean flipVertically = (FLAG_FLIP_VERTICALLY & id) != 0;
                    TextureRegion textureRegion = new TextureRegion(map2.getTileSets().getTile(536870911 & id).getTextureRegion());
                    textureRegion.flip(flipHorizontally, flipVertically);
                    TextureMapObject textureMapObject = new TextureMapObject(textureRegion);
                    textureMapObject.getProperties().put("gid", Integer.valueOf(id));
                    textureMapObject.setX(x);
                    if (this.flipY) {
                        f = y - height;
                    } else {
                        f = y;
                    }
                    textureMapObject.setY(f);
                    textureMapObject.setScaleX(scaleX);
                    textureMapObject.setScaleY(scaleY);
                    textureMapObject.setRotation(element.getFloatAttribute("rotation", Animation.CurveTimeline.LINEAR));
                    object = textureMapObject;
                } else {
                    object = new RectangleMapObject(x, this.flipY ? y - height : y, width, height);
                }
            }
            object.setName(element.getAttribute("name", null));
            String rotation = element.getAttribute("rotation", null);
            if (rotation != null) {
                object.getProperties().put("rotation", Float.valueOf(Float.parseFloat(rotation)));
            }
            String type = element.getAttribute("type", null);
            if (type != null) {
                object.getProperties().put("type", type);
            }
            int id2 = element.getIntAttribute(dc.V, 0);
            if (id2 != 0) {
                object.getProperties().put(dc.V, Integer.valueOf(id2));
            }
            object.getProperties().put("x", Float.valueOf(x * scaleX));
            MapProperties properties = object.getProperties();
            if (this.flipY) {
                y -= height;
            }
            properties.put("y", Float.valueOf(y * scaleY));
            object.getProperties().put("width", Float.valueOf(width));
            object.getProperties().put("height", Float.valueOf(height));
            object.setVisible(element.getIntAttribute("visible", 1) == 1);
            XmlReader.Element properties2 = element.getChildByName("properties");
            if (properties2 != null) {
                loadProperties(object.getProperties(), properties2);
            }
            layer.getObjects().add(object);
        }
    }

    /* access modifiers changed from: protected */
    public void loadProperties(MapProperties properties, XmlReader.Element element) {
        if (element != null && element.getName().equals("properties")) {
            Iterator<XmlReader.Element> it = element.getChildrenByName("property").iterator();
            while (it.hasNext()) {
                XmlReader.Element property = it.next();
                String name = property.getAttribute("name", null);
                String value = property.getAttribute("value", null);
                if (value == null) {
                    value = property.getText();
                }
                properties.put(name, value);
            }
        }
    }

    /* access modifiers changed from: protected */
    public TiledMapTileLayer.Cell createTileLayerCell(boolean flipHorizontally, boolean flipVertically, boolean flipDiagonally) {
        TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();
        if (!flipDiagonally) {
            cell.setFlipHorizontally(flipHorizontally);
            cell.setFlipVertically(flipVertically);
        } else if (flipHorizontally && flipVertically) {
            cell.setFlipHorizontally(true);
            cell.setRotation(3);
        } else if (flipHorizontally) {
            cell.setRotation(3);
        } else if (flipVertically) {
            cell.setRotation(1);
        } else {
            cell.setFlipVertically(true);
            cell.setRotation(3);
        }
        return cell;
    }

    public static int[] getTileIds(XmlReader.Element element, int width, int height) {
        int curr;
        XmlReader.Element data = element.getChildByName("data");
        String encoding = data.getAttribute("encoding", null);
        if (encoding == null) {
            throw new GdxRuntimeException("Unsupported encoding (XML) for TMX Layer Data");
        }
        int[] ids = new int[(width * height)];
        if (encoding.equals("csv")) {
            String[] array = data.getText().split(",");
            for (int i = 0; i < array.length; i++) {
                ids[i] = (int) Long.parseLong(array[i].trim());
            }
        } else if (encoding.equals("base64")) {
            InputStream is = null;
            try {
                String compression = data.getAttribute("compression", null);
                byte[] bytes = Base64Coder.decode(data.getText());
                if (compression == null) {
                    is = new ByteArrayInputStream(bytes);
                } else if (compression.equals("gzip")) {
                    is = new GZIPInputStream(new ByteArrayInputStream(bytes), bytes.length);
                } else if (compression.equals("zlib")) {
                    is = new InflaterInputStream(new ByteArrayInputStream(bytes));
                } else {
                    throw new GdxRuntimeException("Unrecognised compression (" + compression + ") for TMX Layer Data");
                }
                byte[] temp = new byte[4];
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        int read = is.read(temp);
                        while (read < temp.length && (curr = is.read(temp, read, temp.length - read)) != -1) {
                            read += curr;
                        }
                        if (read != temp.length) {
                            throw new GdxRuntimeException("Error Reading TMX Layer Data: Premature end of tile data");
                        }
                        ids[(y * width) + x] = unsignedByteToInt(temp[0]) | (unsignedByteToInt(temp[1]) << 8) | (unsignedByteToInt(temp[2]) << 16) | (unsignedByteToInt(temp[3]) << 24);
                    }
                }
                StreamUtils.closeQuietly(is);
            } catch (IOException e) {
                throw new GdxRuntimeException("Error Reading TMX Layer Data - IOException: " + e.getMessage());
            } catch (Throwable th) {
                StreamUtils.closeQuietly(is);
                throw th;
            }
        } else {
            throw new GdxRuntimeException("Unrecognised encoding (" + encoding + ") for TMX Layer Data");
        }
        return ids;
    }

    protected static int unsignedByteToInt(byte b) {
        return b & 255;
    }

    public static void setMapImagePath(String str) {
        pathString = str;
    }

    protected static FileHandle getRelativeFileHandle(FileHandle file, String path) {
        return Gdx.files.internal(String.valueOf(pathString) + path.substring(path.lastIndexOf("/")));
    }
}
