package com.badlogic.gdx.maps.objects;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.kbz.esotericsoftware.spine.Animation;

public class RectangleMapObject extends MapObject {
    private Rectangle rectangle;

    public Rectangle getRectangle() {
        return this.rectangle;
    }

    public RectangleMapObject() {
        this(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 1.0f);
    }

    public RectangleMapObject(float x, float y, float width, float height) {
        this.rectangle = new Rectangle(x, y, width, height);
    }
}
