package com.badlogic.gdx.maps.objects;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Ellipse;
import com.kbz.esotericsoftware.spine.Animation;

public class EllipseMapObject extends MapObject {
    private Ellipse ellipse;

    public Ellipse getEllipse() {
        return this.ellipse;
    }

    public EllipseMapObject() {
        this(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 1.0f);
    }

    public EllipseMapObject(float x, float y, float width, float height) {
        this.ellipse = new Ellipse(x, y, width, height);
    }
}
