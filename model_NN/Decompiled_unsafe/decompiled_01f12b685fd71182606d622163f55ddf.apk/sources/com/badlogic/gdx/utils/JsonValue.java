package com.badlogic.gdx.utils;

import com.badlogic.gdx.utils.JsonWriter;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class JsonValue implements Iterable<JsonValue> {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType;
    public JsonValue child;
    private double doubleValue;
    private long longValue;
    public String name;
    public JsonValue next;
    public JsonValue prev;
    public int size;
    private String stringValue;
    private ValueType type;

    public static class PrettyPrintSettings {
        public JsonWriter.OutputType outputType;
        public int singleLineColumns;
        public boolean wrapNumericArrays;
    }

    public enum ValueType {
        object,
        array,
        stringValue,
        doubleValue,
        longValue,
        booleanValue,
        nullValue
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType() {
        int[] iArr = $SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType;
        if (iArr == null) {
            iArr = new int[ValueType.values().length];
            try {
                iArr[ValueType.array.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ValueType.booleanValue.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ValueType.doubleValue.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ValueType.longValue.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ValueType.nullValue.ordinal()] = 7;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ValueType.object.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ValueType.stringValue.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            $SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType = iArr;
        }
        return iArr;
    }

    public JsonValue(ValueType type2) {
        this.type = type2;
    }

    public JsonValue(String value) {
        set(value);
    }

    public JsonValue(double value) {
        set(value, (String) null);
    }

    public JsonValue(long value) {
        set(value, (String) null);
    }

    public JsonValue(double value, String stringValue2) {
        set(value, stringValue2);
    }

    public JsonValue(long value, String stringValue2) {
        set(value, stringValue2);
    }

    public JsonValue(boolean value) {
        set(value);
    }

    public JsonValue get(int index) {
        JsonValue current = this.child;
        while (current != null && index > 0) {
            index--;
            current = current.next;
        }
        return current;
    }

    public JsonValue get(String name2) {
        JsonValue current = this.child;
        while (current != null && !current.name.equalsIgnoreCase(name2)) {
            current = current.next;
        }
        return current;
    }

    public boolean has(String name2) {
        return get(name2) != null;
    }

    public JsonValue require(int index) {
        JsonValue current = this.child;
        while (current != null && index > 0) {
            index--;
            current = current.next;
        }
        if (current != null) {
            return current;
        }
        throw new IllegalArgumentException("Child not found with index: " + index);
    }

    public JsonValue require(String name2) {
        JsonValue current = this.child;
        while (current != null && !current.name.equalsIgnoreCase(name2)) {
            current = current.next;
        }
        if (current != null) {
            return current;
        }
        throw new IllegalArgumentException("Child not found with name: " + name2);
    }

    public JsonValue remove(int index) {
        JsonValue child2 = get(index);
        if (child2 == null) {
            return null;
        }
        if (child2.prev == null) {
            this.child = child2.next;
            if (this.child != null) {
                this.child.prev = null;
            }
        } else {
            child2.prev.next = child2.next;
            if (child2.next != null) {
                child2.next.prev = child2.prev;
            }
        }
        this.size--;
        return child2;
    }

    public JsonValue remove(String name2) {
        JsonValue child2 = get(name2);
        if (child2 == null) {
            return null;
        }
        if (child2.prev == null) {
            this.child = child2.next;
            if (this.child != null) {
                this.child.prev = null;
            }
        } else {
            child2.prev.next = child2.next;
            if (child2.next != null) {
                child2.next.prev = child2.prev;
            }
        }
        this.size--;
        return child2;
    }

    @Deprecated
    public int size() {
        return this.size;
    }

    public String asString() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return this.stringValue;
            case 4:
                return this.stringValue != null ? this.stringValue : Double.toString(this.doubleValue);
            case 5:
                return this.stringValue != null ? this.stringValue : Long.toString(this.longValue);
            case 6:
                return this.longValue != 0 ? "true" : "false";
            case 7:
                return null;
            default:
                throw new IllegalStateException("Value cannot be converted to string: " + this.type);
        }
    }

    public float asFloat() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return Float.parseFloat(this.stringValue);
            case 4:
                return (float) this.doubleValue;
            case 5:
                return (float) this.longValue;
            case 6:
                return (float) (this.longValue != 0 ? 1 : 0);
            default:
                throw new IllegalStateException("Value cannot be converted to float: " + this.type);
        }
    }

    public double asDouble() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return Double.parseDouble(this.stringValue);
            case 4:
                return this.doubleValue;
            case 5:
                return (double) this.longValue;
            case 6:
                return (double) (this.longValue != 0 ? 1 : 0);
            default:
                throw new IllegalStateException("Value cannot be converted to double: " + this.type);
        }
    }

    public long asLong() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return Long.parseLong(this.stringValue);
            case 4:
                return (long) this.doubleValue;
            case 5:
                return this.longValue;
            case 6:
                return (long) (this.longValue != 0 ? 1 : 0);
            default:
                throw new IllegalStateException("Value cannot be converted to long: " + this.type);
        }
    }

    public int asInt() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return Integer.parseInt(this.stringValue);
            case 4:
                return (int) this.doubleValue;
            case 5:
                return (int) this.longValue;
            case 6:
                return this.longValue != 0 ? 1 : 0;
            default:
                throw new IllegalStateException("Value cannot be converted to int: " + this.type);
        }
    }

    public boolean asBoolean() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return this.stringValue.equalsIgnoreCase("true");
            case 4:
                if (this.doubleValue == 0.0d) {
                    return false;
                }
                return true;
            case 5:
                if (this.longValue == 0) {
                    return false;
                }
                return true;
            case 6:
                return this.longValue != 0;
            default:
                throw new IllegalStateException("Value cannot be converted to boolean: " + this.type);
        }
    }

    public byte asByte() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return Byte.parseByte(this.stringValue);
            case 4:
                return (byte) ((int) this.doubleValue);
            case 5:
                return (byte) ((int) this.longValue);
            case 6:
                return this.longValue != 0 ? (byte) 1 : 0;
            default:
                throw new IllegalStateException("Value cannot be converted to byte: " + this.type);
        }
    }

    public short asShort() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                return Short.parseShort(this.stringValue);
            case 4:
                return (short) ((int) this.doubleValue);
            case 5:
                return (short) ((int) this.longValue);
            case 6:
                return this.longValue != 0 ? (short) 1 : 0;
            default:
                throw new IllegalStateException("Value cannot be converted to short: " + this.type);
        }
    }

    public char asChar() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
                if (this.stringValue.length() == 0) {
                    return 0;
                }
                return this.stringValue.charAt(0);
            case 4:
                return (char) ((int) this.doubleValue);
            case 5:
                return (char) ((int) this.longValue);
            case 6:
                return this.longValue != 0 ? (char) 1 : 0;
            default:
                throw new IllegalStateException("Value cannot be converted to char: " + this.type);
        }
    }

    public String[] asStringArray() {
        String v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        String[] array = new String[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = value.stringValue;
                    break;
                case 4:
                    if (this.stringValue == null) {
                        v = Double.toString(value.doubleValue);
                        break;
                    } else {
                        v = this.stringValue;
                        break;
                    }
                case 5:
                    if (this.stringValue == null) {
                        v = Long.toString(value.longValue);
                        break;
                    } else {
                        v = this.stringValue;
                        break;
                    }
                case 6:
                    if (value.longValue == 0) {
                        v = "false";
                        break;
                    } else {
                        v = "true";
                        break;
                    }
                case 7:
                    v = null;
                    break;
                default:
                    throw new IllegalStateException("Value cannot be converted to string: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public float[] asFloatArray() {
        float v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        float[] array = new float[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Float.parseFloat(value.stringValue);
                    break;
                case 4:
                    v = (float) value.doubleValue;
                    break;
                case 5:
                    v = (float) value.longValue;
                    break;
                case 6:
                    v = (float) (value.longValue != 0 ? 1 : 0);
                    break;
                default:
                    throw new IllegalStateException("Value cannot be converted to float: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public double[] asDoubleArray() {
        double v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        double[] array = new double[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Double.parseDouble(value.stringValue);
                    break;
                case 4:
                    v = value.doubleValue;
                    break;
                case 5:
                    v = (double) value.longValue;
                    break;
                case 6:
                    v = (double) (value.longValue != 0 ? 1 : 0);
                    break;
                default:
                    throw new IllegalStateException("Value cannot be converted to double: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public long[] asLongArray() {
        long v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        long[] array = new long[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Long.parseLong(value.stringValue);
                    break;
                case 4:
                    v = (long) value.doubleValue;
                    break;
                case 5:
                    v = value.longValue;
                    break;
                case 6:
                    v = (long) (value.longValue != 0 ? 1 : 0);
                    break;
                default:
                    throw new IllegalStateException("Value cannot be converted to long: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public int[] asIntArray() {
        int v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        int[] array = new int[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Integer.parseInt(value.stringValue);
                    break;
                case 4:
                    v = (int) value.doubleValue;
                    break;
                case 5:
                    v = (int) value.longValue;
                    break;
                case 6:
                    if (value.longValue == 0) {
                        v = 0;
                        break;
                    } else {
                        v = 1;
                        break;
                    }
                default:
                    throw new IllegalStateException("Value cannot be converted to int: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public boolean[] asBooleanArray() {
        boolean v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        boolean[] array = new boolean[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Boolean.parseBoolean(value.stringValue);
                    break;
                case 4:
                    if (value.doubleValue != 0.0d) {
                        v = false;
                        break;
                    } else {
                        v = true;
                        break;
                    }
                case 5:
                    if (value.longValue != 0) {
                        v = false;
                        break;
                    } else {
                        v = true;
                        break;
                    }
                case 6:
                    if (value.longValue == 0) {
                        v = false;
                        break;
                    } else {
                        v = true;
                        break;
                    }
                default:
                    throw new IllegalStateException("Value cannot be converted to boolean: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public byte[] asByteArray() {
        byte v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        byte[] array = new byte[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Byte.parseByte(value.stringValue);
                    break;
                case 4:
                    v = (byte) ((int) value.doubleValue);
                    break;
                case 5:
                    v = (byte) ((int) value.longValue);
                    break;
                case 6:
                    if (value.longValue == 0) {
                        v = 0;
                        break;
                    } else {
                        v = 1;
                        break;
                    }
                default:
                    throw new IllegalStateException("Value cannot be converted to byte: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public short[] asShortArray() {
        short v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        short[] array = new short[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    v = Short.parseShort(value.stringValue);
                    break;
                case 4:
                    v = (short) ((int) value.doubleValue);
                    break;
                case 5:
                    v = (short) ((int) value.longValue);
                    break;
                case 6:
                    if (value.longValue == 0) {
                        v = 0;
                        break;
                    } else {
                        v = 1;
                        break;
                    }
                default:
                    throw new IllegalStateException("Value cannot be converted to short: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public char[] asCharArray() {
        char v;
        if (this.type != ValueType.array) {
            throw new IllegalStateException("Value is not an array: " + this.type);
        }
        char[] array = new char[this.size];
        int i = 0;
        JsonValue value = this.child;
        while (value != null) {
            switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[value.type.ordinal()]) {
                case 3:
                    if (value.stringValue.length() != 0) {
                        v = value.stringValue.charAt(0);
                        break;
                    } else {
                        v = 0;
                        break;
                    }
                case 4:
                    v = (char) ((int) value.doubleValue);
                    break;
                case 5:
                    v = (char) ((int) value.longValue);
                    break;
                case 6:
                    if (value.longValue == 0) {
                        v = 0;
                        break;
                    } else {
                        v = 1;
                        break;
                    }
                default:
                    throw new IllegalStateException("Value cannot be converted to char: " + value.type);
            }
            array[i] = v;
            value = value.next;
            i++;
        }
        return array;
    }

    public boolean hasChild(String name2) {
        return getChild(name2) != null;
    }

    public JsonValue getChild(String name2) {
        JsonValue child2 = get(name2);
        if (child2 == null) {
            return null;
        }
        return child2.child;
    }

    public String getString(String name2, String defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue() || child2.isNull()) ? defaultValue : child2.asString();
    }

    public float getFloat(String name2, float defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asFloat();
    }

    public double getDouble(String name2, double defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asDouble();
    }

    public long getLong(String name2, long defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asLong();
    }

    public int getInt(String name2, int defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asInt();
    }

    public boolean getBoolean(String name2, boolean defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asBoolean();
    }

    public byte getByte(String name2, byte defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asByte();
    }

    public short getShort(String name2, short defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asShort();
    }

    public char getChar(String name2, char defaultValue) {
        JsonValue child2 = get(name2);
        return (child2 == null || !child2.isValue()) ? defaultValue : child2.asChar();
    }

    public String getString(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asString();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public float getFloat(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asFloat();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public double getDouble(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asDouble();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public long getLong(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asLong();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public int getInt(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asInt();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public boolean getBoolean(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asBoolean();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public byte getByte(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asByte();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public short getShort(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asShort();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public char getChar(String name2) {
        JsonValue child2 = get(name2);
        if (child2 != null) {
            return child2.asChar();
        }
        throw new IllegalArgumentException("Named value not found: " + name2);
    }

    public String getString(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asString();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public float getFloat(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asFloat();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public double getDouble(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asDouble();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public long getLong(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asLong();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public int getInt(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asInt();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public boolean getBoolean(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asBoolean();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public byte getByte(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asByte();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public short getShort(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asShort();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public char getChar(int index) {
        JsonValue child2 = get(index);
        if (child2 != null) {
            return child2.asChar();
        }
        throw new IllegalArgumentException("Indexed value not found: " + this.name);
    }

    public ValueType type() {
        return this.type;
    }

    public void setType(ValueType type2) {
        if (type2 == null) {
            throw new IllegalArgumentException("type cannot be null.");
        }
        this.type = type2;
    }

    public boolean isArray() {
        return this.type == ValueType.array;
    }

    public boolean isObject() {
        return this.type == ValueType.object;
    }

    public boolean isString() {
        return this.type == ValueType.stringValue;
    }

    public boolean isNumber() {
        return this.type == ValueType.doubleValue || this.type == ValueType.longValue;
    }

    public boolean isDouble() {
        return this.type == ValueType.doubleValue;
    }

    public boolean isLong() {
        return this.type == ValueType.longValue;
    }

    public boolean isBoolean() {
        return this.type == ValueType.booleanValue;
    }

    public boolean isNull() {
        return this.type == ValueType.nullValue;
    }

    public boolean isValue() {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$JsonValue$ValueType()[this.type.ordinal()]) {
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return true;
            default:
                return false;
        }
    }

    public String name() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public JsonValue child() {
        return this.child;
    }

    public JsonValue next() {
        return this.next;
    }

    public void setNext(JsonValue next2) {
        this.next = next2;
    }

    public JsonValue prev() {
        return this.prev;
    }

    public void setPrev(JsonValue prev2) {
        this.prev = prev2;
    }

    public void set(String value) {
        this.stringValue = value;
        this.type = value == null ? ValueType.nullValue : ValueType.stringValue;
    }

    public void set(double value, String stringValue2) {
        this.doubleValue = value;
        this.longValue = (long) value;
        this.stringValue = stringValue2;
        this.type = ValueType.doubleValue;
    }

    public void set(long value, String stringValue2) {
        this.longValue = value;
        this.doubleValue = (double) value;
        this.stringValue = stringValue2;
        this.type = ValueType.longValue;
    }

    public void set(boolean value) {
        this.longValue = (long) (value ? 1 : 0);
        this.type = ValueType.booleanValue;
    }

    public String toString() {
        if (isValue()) {
            return this.name == null ? asString() : String.valueOf(this.name) + ": " + asString();
        }
        return String.valueOf(this.name == null ? "" : String.valueOf(this.name) + ": ") + prettyPrint(JsonWriter.OutputType.minimal, 0);
    }

    public String prettyPrint(JsonWriter.OutputType outputType, int singleLineColumns) {
        PrettyPrintSettings settings = new PrettyPrintSettings();
        settings.outputType = outputType;
        settings.singleLineColumns = singleLineColumns;
        return prettyPrint(settings);
    }

    public String prettyPrint(PrettyPrintSettings settings) {
        StringBuilder buffer = new StringBuilder(512);
        prettyPrint(this, buffer, 0, settings);
        return buffer.toString();
    }

    private void prettyPrint(JsonValue object, StringBuilder buffer, int indent, PrettyPrintSettings settings) {
        JsonWriter.OutputType outputType = settings.outputType;
        if (object.isObject()) {
            if (object.child == null) {
                buffer.append("{}");
                return;
            }
            boolean newLines = !isFlat(object);
            int start = buffer.length();
            loop0:
            while (true) {
                buffer.append(newLines ? "{\n" : "{ ");
                JsonValue child2 = object.child;
                while (child2 != null) {
                    if (newLines) {
                        indent(indent, buffer);
                    }
                    buffer.append(outputType.quoteName(child2.name));
                    buffer.append(": ");
                    prettyPrint(child2, buffer, indent + 1, settings);
                    if ((!newLines || outputType != JsonWriter.OutputType.minimal) && child2.next != null) {
                        buffer.append(',');
                    }
                    buffer.append(newLines ? 10 : ' ');
                    if (newLines || buffer.length() - start <= settings.singleLineColumns) {
                        child2 = child2.next;
                    } else {
                        buffer.setLength(start);
                        newLines = true;
                    }
                }
                break loop0;
            }
            if (newLines) {
                indent(indent - 1, buffer);
            }
            buffer.append('}');
        } else if (object.isArray()) {
            if (object.child == null) {
                buffer.append("[]");
                return;
            }
            boolean newLines2 = !isFlat(object);
            boolean wrap = settings.wrapNumericArrays || !isNumeric(object);
            int start2 = buffer.length();
            loop2:
            while (true) {
                buffer.append(newLines2 ? "[\n" : "[ ");
                JsonValue child3 = object.child;
                while (child3 != null) {
                    if (newLines2) {
                        indent(indent, buffer);
                    }
                    prettyPrint(child3, buffer, indent + 1, settings);
                    if ((!newLines2 || outputType != JsonWriter.OutputType.minimal) && child3.next != null) {
                        buffer.append(',');
                    }
                    buffer.append(newLines2 ? 10 : ' ');
                    if (!wrap || newLines2 || buffer.length() - start2 <= settings.singleLineColumns) {
                        child3 = child3.next;
                    } else {
                        buffer.setLength(start2);
                        newLines2 = true;
                    }
                }
                break loop2;
            }
            if (newLines2) {
                indent(indent - 1, buffer);
            }
            buffer.append(']');
        } else if (object.isString()) {
            buffer.append(outputType.quoteValue(object.asString()));
        } else if (object.isDouble()) {
            double doubleValue2 = object.asDouble();
            long longValue2 = object.asLong();
            if (doubleValue2 == ((double) longValue2)) {
                doubleValue2 = (double) longValue2;
            }
            buffer.append(doubleValue2);
        } else if (object.isLong()) {
            buffer.append(object.asLong());
        } else if (object.isBoolean()) {
            buffer.append(object.asBoolean());
        } else if (object.isNull()) {
            buffer.append("null");
        } else {
            throw new SerializationException("Unknown object type: " + object);
        }
    }

    private static boolean isFlat(JsonValue object) {
        for (JsonValue child2 = object.child; child2 != null; child2 = child2.next) {
            if (child2.isObject() || child2.isArray()) {
                return false;
            }
        }
        return true;
    }

    private static boolean isNumeric(JsonValue object) {
        for (JsonValue child2 = object.child; child2 != null; child2 = child2.next) {
            if (!child2.isNumber()) {
                return false;
            }
        }
        return true;
    }

    private static void indent(int count, StringBuilder buffer) {
        for (int i = 0; i < count; i++) {
            buffer.append(9);
        }
    }

    public JsonIterator iterator() {
        return new JsonIterator();
    }

    public class JsonIterator implements Iterator<JsonValue>, Iterable<JsonValue> {
        JsonValue current;
        JsonValue entry;

        public JsonIterator() {
            this.entry = JsonValue.this.child;
        }

        public boolean hasNext() {
            return this.entry != null;
        }

        public JsonValue next() {
            this.current = this.entry;
            if (this.current == null) {
                throw new NoSuchElementException();
            }
            this.entry = this.current.next;
            return this.current;
        }

        public void remove() {
            if (this.current.prev == null) {
                JsonValue.this.child = this.current.next;
                if (JsonValue.this.child != null) {
                    JsonValue.this.child.prev = null;
                }
            } else {
                this.current.prev.next = this.current.next;
                if (this.current.next != null) {
                    this.current.next.prev = this.current.prev;
                }
            }
            JsonValue jsonValue = JsonValue.this;
            jsonValue.size--;
        }

        public Iterator<JsonValue> iterator() {
            return this;
        }
    }
}
