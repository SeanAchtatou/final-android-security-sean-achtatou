package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import java.util.Arrays;

public class ByteArray {
    public byte[] items;
    public boolean ordered;
    public int size;

    public ByteArray() {
        this(true, 16);
    }

    public ByteArray(int capacity) {
        this(true, capacity);
    }

    public ByteArray(boolean ordered2, int capacity) {
        this.ordered = ordered2;
        this.items = new byte[capacity];
    }

    public ByteArray(ByteArray array) {
        this.ordered = array.ordered;
        this.size = array.size;
        this.items = new byte[this.size];
        System.arraycopy(array.items, 0, this.items, 0, this.size);
    }

    public ByteArray(byte[] array) {
        this(true, array, 0, array.length);
    }

    public ByteArray(boolean ordered2, byte[] array, int startIndex, int count) {
        this(ordered2, count);
        this.size = count;
        System.arraycopy(array, startIndex, this.items, 0, count);
    }

    public void add(byte value) {
        byte[] items2 = this.items;
        if (this.size == items2.length) {
            items2 = resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        int i = this.size;
        this.size = i + 1;
        items2[i] = value;
    }

    public void addAll(ByteArray array) {
        addAll(array, 0, array.size);
    }

    public void addAll(ByteArray array, int offset, int length) {
        if (offset + length > array.size) {
            throw new IllegalArgumentException("offset + length must be <= size: " + offset + " + " + length + " <= " + array.size);
        }
        addAll(array.items, offset, length);
    }

    public void addAll(byte... array) {
        addAll(array, 0, array.length);
    }

    public void addAll(byte[] array, int offset, int length) {
        byte[] items2 = this.items;
        int sizeNeeded = this.size + length;
        if (sizeNeeded > items2.length) {
            items2 = resize(Math.max(8, (int) (((float) sizeNeeded) * 1.75f)));
        }
        System.arraycopy(array, offset, items2, this.size, length);
        this.size += length;
    }

    public byte get(int index) {
        if (index < this.size) {
            return this.items[index];
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
    }

    public void set(int index, byte value) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
        }
        this.items[index] = value;
    }

    public void incr(int index, byte value) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
        }
        byte[] bArr = this.items;
        bArr[index] = (byte) (bArr[index] + value);
    }

    public void mul(int index, byte value) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
        }
        byte[] bArr = this.items;
        bArr[index] = (byte) (bArr[index] * value);
    }

    public void insert(int index, byte value) {
        if (index > this.size) {
            throw new IndexOutOfBoundsException("index can't be > size: " + index + " > " + this.size);
        }
        byte[] items2 = this.items;
        if (this.size == items2.length) {
            items2 = resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        if (this.ordered) {
            System.arraycopy(items2, index, items2, index + 1, this.size - index);
        } else {
            items2[this.size] = items2[index];
        }
        this.size++;
        items2[index] = value;
    }

    public void swap(int first, int second) {
        if (first >= this.size) {
            throw new IndexOutOfBoundsException("first can't be >= size: " + first + " >= " + this.size);
        } else if (second >= this.size) {
            throw new IndexOutOfBoundsException("second can't be >= size: " + second + " >= " + this.size);
        } else {
            byte[] items2 = this.items;
            byte firstValue = items2[first];
            items2[first] = items2[second];
            items2[second] = firstValue;
        }
    }

    public boolean contains(byte value) {
        byte[] items2 = this.items;
        int i = this.size - 1;
        while (i >= 0) {
            int i2 = i - 1;
            if (items2[i] == value) {
                return true;
            }
            i = i2;
        }
        return false;
    }

    public int indexOf(byte value) {
        byte[] items2 = this.items;
        int n = this.size;
        for (int i = 0; i < n; i++) {
            if (items2[i] == value) {
                return i;
            }
        }
        return -1;
    }

    public int lastIndexOf(byte value) {
        byte[] items2 = this.items;
        for (int i = this.size - 1; i >= 0; i--) {
            if (items2[i] == value) {
                return i;
            }
        }
        return -1;
    }

    public boolean removeValue(byte value) {
        byte[] items2 = this.items;
        int n = this.size;
        for (int i = 0; i < n; i++) {
            if (items2[i] == value) {
                removeIndex(i);
                return true;
            }
        }
        return false;
    }

    public int removeIndex(int index) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
        }
        byte[] items2 = this.items;
        byte b = items2[index];
        this.size--;
        if (this.ordered) {
            System.arraycopy(items2, index + 1, items2, index, this.size - index);
        } else {
            items2[index] = items2[this.size];
        }
        return b;
    }

    public void removeRange(int start, int end) {
        if (end >= this.size) {
            throw new IndexOutOfBoundsException("end can't be >= size: " + end + " >= " + this.size);
        } else if (start > end) {
            throw new IndexOutOfBoundsException("start can't be > end: " + start + " > " + end);
        } else {
            byte[] items2 = this.items;
            int count = (end - start) + 1;
            if (this.ordered) {
                System.arraycopy(items2, start + count, items2, start, this.size - (start + count));
            } else {
                int lastIndex = this.size - 1;
                for (int i = 0; i < count; i++) {
                    items2[start + i] = items2[lastIndex - i];
                }
            }
            this.size -= count;
        }
    }

    public boolean removeAll(ByteArray array) {
        int size2 = this.size;
        int startSize = size2;
        byte[] items2 = this.items;
        int n = array.size;
        for (int i = 0; i < n; i++) {
            int item = array.get(i);
            int ii = 0;
            while (true) {
                if (ii >= size2) {
                    break;
                } else if (item == items2[ii]) {
                    removeIndex(ii);
                    size2--;
                    break;
                } else {
                    ii++;
                }
            }
        }
        if (size2 != startSize) {
            return true;
        }
        return false;
    }

    public byte pop() {
        byte[] bArr = this.items;
        int i = this.size - 1;
        this.size = i;
        return bArr[i];
    }

    public byte peek() {
        return this.items[this.size - 1];
    }

    public byte first() {
        if (this.size != 0) {
            return this.items[0];
        }
        throw new IllegalStateException("Array is empty.");
    }

    public void clear() {
        this.size = 0;
    }

    public byte[] shrink() {
        if (this.items.length != this.size) {
            resize(this.size);
        }
        return this.items;
    }

    public byte[] ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded > this.items.length) {
            resize(Math.max(8, sizeNeeded));
        }
        return this.items;
    }

    /* access modifiers changed from: protected */
    public byte[] resize(int newSize) {
        byte[] newItems = new byte[newSize];
        System.arraycopy(this.items, 0, newItems, 0, Math.min(this.size, newItems.length));
        this.items = newItems;
        return newItems;
    }

    public void sort() {
        Arrays.sort(this.items, 0, this.size);
    }

    public void reverse() {
        byte[] items2 = this.items;
        int lastIndex = this.size - 1;
        int n = this.size / 2;
        for (int i = 0; i < n; i++) {
            int ii = lastIndex - i;
            byte temp = items2[i];
            items2[i] = items2[ii];
            items2[ii] = temp;
        }
    }

    public void shuffle() {
        byte[] items2 = this.items;
        for (int i = this.size - 1; i >= 0; i--) {
            int ii = MathUtils.random(i);
            byte temp = items2[i];
            items2[i] = items2[ii];
            items2[ii] = temp;
        }
    }

    public void truncate(int newSize) {
        if (this.size > newSize) {
            this.size = newSize;
        }
    }

    public byte random() {
        if (this.size == 0) {
            return 0;
        }
        return this.items[MathUtils.random(0, this.size - 1)];
    }

    public byte[] toArray() {
        byte[] array = new byte[this.size];
        System.arraycopy(this.items, 0, array, 0, this.size);
        return array;
    }

    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof ByteArray)) {
            return false;
        }
        ByteArray array = (ByteArray) object;
        int n = this.size;
        if (n != array.size) {
            return false;
        }
        for (int i = 0; i < n; i++) {
            if (this.items[i] != array.items[i]) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        byte[] items2 = this.items;
        StringBuilder buffer = new StringBuilder(32);
        buffer.append('[');
        buffer.append((int) items2[0]);
        for (int i = 1; i < this.size; i++) {
            buffer.append(", ");
            buffer.append((int) items2[i]);
        }
        buffer.append(']');
        return buffer.toString();
    }

    public String toString(String separator) {
        if (this.size == 0) {
            return "";
        }
        byte[] items2 = this.items;
        StringBuilder buffer = new StringBuilder(32);
        buffer.append((int) items2[0]);
        for (int i = 1; i < this.size; i++) {
            buffer.append(separator);
            buffer.append((int) items2[i]);
        }
        return buffer.toString();
    }

    public static ByteArray with(byte... array) {
        return new ByteArray(array);
    }
}
