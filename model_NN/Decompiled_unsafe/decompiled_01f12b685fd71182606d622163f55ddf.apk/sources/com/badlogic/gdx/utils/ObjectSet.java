package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ObjectSet<T> implements Iterable<T> {
    private static final int PRIME1 = -1105259343;
    private static final int PRIME2 = -1262997959;
    private static final int PRIME3 = -825114047;
    int capacity;
    private int hashShift;
    private ObjectSetIterator iterator1;
    private ObjectSetIterator iterator2;
    T[] keyTable;
    private float loadFactor;
    private int mask;
    private int pushIterations;
    public int size;
    private int stashCapacity;
    int stashSize;
    private int threshold;

    public ObjectSet() {
        this(32, 0.8f);
    }

    public ObjectSet(int initialCapacity) {
        this(initialCapacity, 0.8f);
    }

    public ObjectSet(int initialCapacity, float loadFactor2) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + initialCapacity);
        } else if (initialCapacity > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + initialCapacity);
        } else {
            this.capacity = MathUtils.nextPowerOfTwo(initialCapacity);
            if (loadFactor2 <= Animation.CurveTimeline.LINEAR) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + loadFactor2);
            }
            this.loadFactor = loadFactor2;
            this.threshold = (int) (((float) this.capacity) * loadFactor2);
            this.mask = this.capacity - 1;
            this.hashShift = 31 - Integer.numberOfTrailingZeros(this.capacity);
            this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) this.capacity))) * 2);
            this.pushIterations = Math.max(Math.min(this.capacity, 8), ((int) Math.sqrt((double) this.capacity)) / 8);
            this.keyTable = new Object[(this.capacity + this.stashCapacity)];
        }
    }

    public ObjectSet(ObjectSet set) {
        this(set.capacity, set.loadFactor);
        this.stashSize = set.stashSize;
        System.arraycopy(set.keyTable, 0, this.keyTable, 0, set.keyTable.length);
        this.size = set.size;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean add(T r14) {
        /*
            r13 = this;
            r12 = 1
            r0 = 0
            if (r14 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "key cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            T[] r10 = r13.keyTable
            int r8 = r14.hashCode()
            int r1 = r13.mask
            r2 = r8 & r1
            r3 = r10[r2]
            boolean r1 = r14.equals(r3)
            if (r1 == 0) goto L_0x001f
        L_0x001e:
            return r0
        L_0x001f:
            int r4 = r13.hash2(r8)
            r5 = r10[r4]
            boolean r1 = r14.equals(r5)
            if (r1 != 0) goto L_0x001e
            int r6 = r13.hash3(r8)
            r7 = r10[r6]
            boolean r1 = r14.equals(r7)
            if (r1 != 0) goto L_0x001e
            int r9 = r13.capacity
            int r1 = r13.stashSize
            int r11 = r9 + r1
        L_0x003d:
            if (r9 < r11) goto L_0x0056
            if (r3 != 0) goto L_0x0061
            r10[r2] = r14
            int r0 = r13.size
            int r1 = r0 + 1
            r13.size = r1
            int r1 = r13.threshold
            if (r0 < r1) goto L_0x0054
            int r0 = r13.capacity
            int r0 = r0 << 1
            r13.resize(r0)
        L_0x0054:
            r0 = r12
            goto L_0x001e
        L_0x0056:
            r1 = r10[r9]
            boolean r1 = r14.equals(r1)
            if (r1 != 0) goto L_0x001e
            int r9 = r9 + 1
            goto L_0x003d
        L_0x0061:
            if (r5 != 0) goto L_0x0078
            r10[r4] = r14
            int r0 = r13.size
            int r1 = r0 + 1
            r13.size = r1
            int r1 = r13.threshold
            if (r0 < r1) goto L_0x0076
            int r0 = r13.capacity
            int r0 = r0 << 1
            r13.resize(r0)
        L_0x0076:
            r0 = r12
            goto L_0x001e
        L_0x0078:
            if (r7 != 0) goto L_0x008f
            r10[r6] = r14
            int r0 = r13.size
            int r1 = r0 + 1
            r13.size = r1
            int r1 = r13.threshold
            if (r0 < r1) goto L_0x008d
            int r0 = r13.capacity
            int r0 = r0 << 1
            r13.resize(r0)
        L_0x008d:
            r0 = r12
            goto L_0x001e
        L_0x008f:
            r0 = r13
            r1 = r14
            r0.push(r1, r2, r3, r4, r5, r6, r7)
            r0 = r12
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectSet.add(java.lang.Object):boolean");
    }

    public void addAll(Array<? extends String> array) {
        addAll(array, 0, array.size);
    }

    public void addAll(Array<? extends T> array, int offset, int length) {
        if (offset + length > array.size) {
            throw new IllegalArgumentException("offset + length must be <= size: " + offset + " + " + length + " <= " + array.size);
        }
        addAll(array.items, offset, length);
    }

    public void addAll(Object... array) {
        addAll(array, 0, array.length);
    }

    public void addAll(T[] array, int offset, int length) {
        ensureCapacity(length);
        int i = offset;
        int n = i + length;
        while (i < n) {
            add(array[i]);
            i++;
        }
    }

    public void addAll(ObjectSet<String> objectSet) {
        ensureCapacity(objectSet.size);
        Iterator it = objectSet.iterator();
        while (it.hasNext()) {
            add(it.next());
        }
    }

    private void addResize(T key) {
        int hashCode = key.hashCode();
        int index1 = hashCode & this.mask;
        T key1 = this.keyTable[index1];
        if (key1 == null) {
            this.keyTable[index1] = key;
            int i = this.size;
            this.size = i + 1;
            if (i >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int index2 = hash2(hashCode);
        T key2 = this.keyTable[index2];
        if (key2 == null) {
            this.keyTable[index2] = key;
            int i2 = this.size;
            this.size = i2 + 1;
            if (i2 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int index3 = hash3(hashCode);
        T key3 = this.keyTable[index3];
        if (key3 == null) {
            this.keyTable[index3] = key;
            int i3 = this.size;
            this.size = i3 + 1;
            if (i3 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        push(key, index1, key1, index2, key2, index3, key3);
    }

    private void push(T insertKey, int index1, T key1, int index2, T key2, int index3, T key3) {
        T evictedKey;
        T[] tArr = this.keyTable;
        int mask2 = this.mask;
        int i = 0;
        int pushIterations2 = this.pushIterations;
        while (true) {
            switch (MathUtils.random(2)) {
                case 0:
                    evictedKey = key1;
                    tArr[index1] = insertKey;
                    break;
                case 1:
                    evictedKey = key2;
                    tArr[index2] = insertKey;
                    break;
                default:
                    evictedKey = key3;
                    tArr[index3] = insertKey;
                    break;
            }
            int hashCode = evictedKey.hashCode();
            index1 = hashCode & mask2;
            key1 = tArr[index1];
            if (key1 == null) {
                tArr[index1] = evictedKey;
                int i2 = this.size;
                this.size = i2 + 1;
                if (i2 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            index2 = hash2(hashCode);
            key2 = tArr[index2];
            if (key2 == null) {
                tArr[index2] = evictedKey;
                int i3 = this.size;
                this.size = i3 + 1;
                if (i3 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            index3 = hash3(hashCode);
            key3 = tArr[index3];
            if (key3 == null) {
                tArr[index3] = evictedKey;
                int i4 = this.size;
                this.size = i4 + 1;
                if (i4 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i++;
            if (i == pushIterations2) {
                addStash(evictedKey);
                return;
            }
            insertKey = evictedKey;
        }
    }

    private void addStash(T key) {
        if (this.stashSize == this.stashCapacity) {
            resize(this.capacity << 1);
            add(key);
            return;
        }
        this.keyTable[this.capacity + this.stashSize] = key;
        this.stashSize++;
        this.size++;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean remove(T r6) {
        /*
            r5 = this;
            r4 = 0
            r2 = 1
            int r0 = r6.hashCode()
            int r3 = r5.mask
            r1 = r0 & r3
            T[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x001f
            T[] r3 = r5.keyTable
            r3[r1] = r4
            int r3 = r5.size
            int r3 = r3 + -1
            r5.size = r3
        L_0x001e:
            return r2
        L_0x001f:
            int r1 = r5.hash2(r0)
            T[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x0038
            T[] r3 = r5.keyTable
            r3[r1] = r4
            int r3 = r5.size
            int r3 = r3 + -1
            r5.size = r3
            goto L_0x001e
        L_0x0038:
            int r1 = r5.hash3(r0)
            T[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x0051
            T[] r3 = r5.keyTable
            r3[r1] = r4
            int r3 = r5.size
            int r3 = r3 + -1
            r5.size = r3
            goto L_0x001e
        L_0x0051:
            boolean r2 = r5.removeStash(r6)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectSet.remove(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    boolean removeStash(T r5) {
        /*
            r4 = this;
            T[] r1 = r4.keyTable
            int r0 = r4.capacity
            int r3 = r4.stashSize
            int r2 = r0 + r3
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r3 = 0
        L_0x000b:
            return r3
        L_0x000c:
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x001f
            r4.removeStashIndex(r0)
            int r3 = r4.size
            int r3 = r3 + -1
            r4.size = r3
            r3 = 1
            goto L_0x000b
        L_0x001f:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectSet.removeStash(java.lang.Object):boolean");
    }

    /* access modifiers changed from: package-private */
    public void removeStashIndex(int index) {
        this.stashSize--;
        int lastIndex = this.capacity + this.stashSize;
        if (index < lastIndex) {
            this.keyTable[index] = this.keyTable[lastIndex];
        }
    }

    public void shrink(int maximumCapacity) {
        if (maximumCapacity < 0) {
            throw new IllegalArgumentException("maximumCapacity must be >= 0: " + maximumCapacity);
        }
        if (this.size > maximumCapacity) {
            maximumCapacity = this.size;
        }
        if (this.capacity > maximumCapacity) {
            resize(MathUtils.nextPowerOfTwo(maximumCapacity));
        }
    }

    public void clear(int maximumCapacity) {
        if (this.capacity <= maximumCapacity) {
            clear();
            return;
        }
        this.size = 0;
        resize(maximumCapacity);
    }

    public void clear() {
        if (this.size != 0) {
            Object[] keyTable2 = this.keyTable;
            int i = this.capacity + this.stashSize;
            while (true) {
                int i2 = i;
                i = i2 - 1;
                if (i2 <= 0) {
                    this.size = 0;
                    this.stashSize = 0;
                    return;
                }
                keyTable2[i] = null;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean contains(T r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r2 = r3.mask
            r1 = r0 & r2
            T[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.hash2(r0)
            T[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.hash3(r0)
            T[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            boolean r2 = r3.containsKeyStash(r4)
        L_0x0032:
            return r2
        L_0x0033:
            r2 = 1
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectSet.contains(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean containsKeyStash(T r5) {
        /*
            r4 = this;
            T[] r1 = r4.keyTable
            int r0 = r4.capacity
            int r3 = r4.stashSize
            int r2 = r0 + r3
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r3 = 0
        L_0x000b:
            return r3
        L_0x000c:
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0016
            r3 = 1
            goto L_0x000b
        L_0x0016:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectSet.containsKeyStash(java.lang.Object):boolean");
    }

    public T first() {
        Object[] keyTable2 = this.keyTable;
        int n = this.capacity + this.stashSize;
        for (int i = 0; i < n; i++) {
            if (keyTable2[i] != null) {
                return keyTable2[i];
            }
        }
        throw new IllegalStateException("ObjectSet is empty.");
    }

    public void ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded >= this.threshold) {
            resize(MathUtils.nextPowerOfTwo((int) (((float) sizeNeeded) / this.loadFactor)));
        }
    }

    private void resize(int newSize) {
        int oldEndIndex = this.capacity + this.stashSize;
        this.capacity = newSize;
        this.threshold = (int) (((float) newSize) * this.loadFactor);
        this.mask = newSize - 1;
        this.hashShift = 31 - Integer.numberOfTrailingZeros(newSize);
        this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) newSize))) * 2);
        this.pushIterations = Math.max(Math.min(newSize, 8), ((int) Math.sqrt((double) newSize)) / 8);
        T[] oldKeyTable = this.keyTable;
        this.keyTable = new Object[(this.stashCapacity + newSize)];
        int oldSize = this.size;
        this.size = 0;
        this.stashSize = 0;
        if (oldSize > 0) {
            for (int i = 0; i < oldEndIndex; i++) {
                T key = oldKeyTable[i];
                if (key != null) {
                    addResize(key);
                }
            }
        }
    }

    private int hash2(int h) {
        int h2 = h * PRIME2;
        return ((h2 >>> this.hashShift) ^ h2) & this.mask;
    }

    private int hash3(int h) {
        int h2 = h * PRIME3;
        return ((h2 >>> this.hashShift) ^ h2) & this.mask;
    }

    public String toString() {
        return String.valueOf('{') + toString(", ") + '}';
    }

    public String toString(String separator) {
        if (this.size == 0) {
            return "";
        }
        StringBuilder buffer = new StringBuilder(32);
        T[] keyTable2 = this.keyTable;
        int i = keyTable2.length;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            T key = keyTable2[i];
            if (key != null) {
                buffer.append((Object) key);
                break;
            }
        }
        while (true) {
            int i3 = i;
            i = i3 - 1;
            if (i3 <= 0) {
                return buffer.toString();
            }
            T key2 = keyTable2[i];
            if (key2 != null) {
                buffer.append(separator);
                buffer.append((Object) key2);
            }
        }
    }

    public ObjectSetIterator<T> iterator() {
        if (this.iterator1 == null) {
            this.iterator1 = new ObjectSetIterator(this);
            this.iterator2 = new ObjectSetIterator(this);
        }
        if (!this.iterator1.valid) {
            this.iterator1.reset();
            this.iterator1.valid = true;
            this.iterator2.valid = false;
            return this.iterator1;
        }
        this.iterator2.reset();
        this.iterator2.valid = true;
        this.iterator1.valid = false;
        return this.iterator2;
    }

    public static <T> ObjectSet<T> with(T... array) {
        ObjectSet set = new ObjectSet();
        set.addAll(array);
        return set;
    }

    public static class ObjectSetIterator<K> implements Iterable<K>, Iterator<K> {
        int currentIndex;
        public boolean hasNext;
        int nextIndex;
        final ObjectSet<K> set;
        boolean valid = true;

        public ObjectSetIterator(ObjectSet<K> set2) {
            this.set = set2;
            reset();
        }

        public void reset() {
            this.currentIndex = -1;
            this.nextIndex = -1;
            findNextIndex();
        }

        /* access modifiers changed from: package-private */
        public void findNextIndex() {
            this.hasNext = false;
            Object[] keyTable = this.set.keyTable;
            int n = this.set.capacity + this.set.stashSize;
            do {
                int i = this.nextIndex + 1;
                this.nextIndex = i;
                if (i >= n) {
                    return;
                }
            } while (keyTable[this.nextIndex] == null);
            this.hasNext = true;
        }

        public void remove() {
            if (this.currentIndex < 0) {
                throw new IllegalStateException("next must be called before remove.");
            }
            if (this.currentIndex >= this.set.capacity) {
                this.set.removeStashIndex(this.currentIndex);
                this.nextIndex = this.currentIndex - 1;
                findNextIndex();
            } else {
                this.set.keyTable[this.currentIndex] = null;
            }
            this.currentIndex = -1;
            ObjectSet<K> objectSet = this.set;
            objectSet.size--;
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.hasNext;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public K next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                Object obj = this.set.keyTable[this.nextIndex];
                this.currentIndex = this.nextIndex;
                findNextIndex();
                return obj;
            }
        }

        public ObjectSetIterator<K> iterator() {
            return this;
        }

        public Array<K> toArray(Array<K> array) {
            while (this.hasNext) {
                array.add(next());
            }
            return array;
        }

        public Array<K> toArray() {
            return toArray(new Array(true, this.set.size));
        }
    }
}
