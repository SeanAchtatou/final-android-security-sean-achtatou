package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.reflect.ArrayReflection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.objectweb.asm.signature.SignatureVisitor;

public class ArrayMap<K, V> implements Iterable<ObjectMap.Entry<K, V>> {
    private Entries entries1;
    private Entries entries2;
    public K[] keys;
    private Keys keysIter1;
    private Keys keysIter2;
    public boolean ordered;
    public int size;
    public V[] values;
    private Values valuesIter1;
    private Values valuesIter2;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.ArrayMap.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.badlogic.gdx.utils.ArrayMap.<init>(java.lang.Class, java.lang.Class):void
      com.badlogic.gdx.utils.ArrayMap.<init>(boolean, int):void */
    public ArrayMap() {
        this(true, 16);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.ArrayMap.<init>(boolean, int):void
     arg types: [int, int]
     candidates:
      com.badlogic.gdx.utils.ArrayMap.<init>(java.lang.Class, java.lang.Class):void
      com.badlogic.gdx.utils.ArrayMap.<init>(boolean, int):void */
    public ArrayMap(int capacity) {
        this(true, capacity);
    }

    public ArrayMap(boolean ordered2, int capacity) {
        this.ordered = ordered2;
        this.keys = new Object[capacity];
        this.values = new Object[capacity];
    }

    public ArrayMap(boolean ordered2, int capacity, Class keyArrayType, Class valueArrayType) {
        this.ordered = ordered2;
        this.keys = (Object[]) ArrayReflection.newInstance(keyArrayType, capacity);
        this.values = (Object[]) ArrayReflection.newInstance(valueArrayType, capacity);
    }

    public ArrayMap(Class keyArrayType, Class valueArrayType) {
        this(false, 16, keyArrayType, valueArrayType);
    }

    public ArrayMap(ArrayMap array) {
        this(array.ordered, array.size, array.keys.getClass().getComponentType(), array.values.getClass().getComponentType());
        this.size = array.size;
        System.arraycopy(array.keys, 0, this.keys, 0, this.size);
        System.arraycopy(array.values, 0, this.values, 0, this.size);
    }

    public int put(K key, V value) {
        int index = indexOfKey(key);
        if (index == -1) {
            if (this.size == this.keys.length) {
                resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
            }
            index = this.size;
            this.size = index + 1;
        }
        this.keys[index] = key;
        this.values[index] = value;
        return index;
    }

    public int put(K key, V value, int index) {
        int existingIndex = indexOfKey(key);
        if (existingIndex != -1) {
            removeIndex(existingIndex);
        } else if (this.size == this.keys.length) {
            resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        System.arraycopy(this.keys, index, this.keys, index + 1, this.size - index);
        System.arraycopy(this.values, index, this.values, index + 1, this.size - index);
        this.keys[index] = key;
        this.values[index] = value;
        this.size++;
        return index;
    }

    public void putAll(ArrayMap map) {
        putAll(map, 0, map.size);
    }

    public void putAll(ArrayMap map, int offset, int length) {
        if (offset + length > map.size) {
            throw new IllegalArgumentException("offset + length must be <= size: " + offset + " + " + length + " <= " + map.size);
        }
        int sizeNeeded = (this.size + length) - offset;
        if (sizeNeeded >= this.keys.length) {
            resize(Math.max(8, (int) (((float) sizeNeeded) * 1.75f)));
        }
        System.arraycopy(map.keys, offset, this.keys, this.size, length);
        System.arraycopy(map.values, offset, this.values, this.size, length);
        this.size += length;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V get(K r4) {
        /*
            r3 = this;
            K[] r1 = r3.keys
            int r2 = r3.size
            int r0 = r2 + -1
            if (r4 != 0) goto L_0x0027
        L_0x0008:
            if (r0 >= 0) goto L_0x000c
        L_0x000a:
            r2 = 0
        L_0x000b:
            return r2
        L_0x000c:
            r2 = r1[r0]
            if (r2 != r4) goto L_0x0015
            V[] r2 = r3.values
            r2 = r2[r0]
            goto L_0x000b
        L_0x0015:
            int r0 = r0 + -1
            goto L_0x0008
        L_0x0018:
            r2 = r1[r0]
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0025
            V[] r2 = r3.values
            r2 = r2[r0]
            goto L_0x000b
        L_0x0025:
            int r0 = r0 + -1
        L_0x0027:
            if (r0 >= 0) goto L_0x0018
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.get(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public K getKey(V r4, boolean r5) {
        /*
            r3 = this;
            V[] r1 = r3.values
            int r2 = r3.size
            int r0 = r2 + -1
            if (r5 != 0) goto L_0x000a
            if (r4 != 0) goto L_0x0029
        L_0x000a:
            if (r0 >= 0) goto L_0x000e
        L_0x000c:
            r2 = 0
        L_0x000d:
            return r2
        L_0x000e:
            r2 = r1[r0]
            if (r2 != r4) goto L_0x0017
            K[] r2 = r3.keys
            r2 = r2[r0]
            goto L_0x000d
        L_0x0017:
            int r0 = r0 + -1
            goto L_0x000a
        L_0x001a:
            r2 = r1[r0]
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0027
            K[] r2 = r3.keys
            r2 = r2[r0]
            goto L_0x000d
        L_0x0027:
            int r0 = r0 + -1
        L_0x0029:
            if (r0 >= 0) goto L_0x001a
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.getKey(java.lang.Object, boolean):java.lang.Object");
    }

    public K getKeyAt(int index) {
        if (index < this.size) {
            return this.keys[index];
        }
        throw new IndexOutOfBoundsException(String.valueOf(index));
    }

    public V getValueAt(int index) {
        if (index < this.size) {
            return this.values[index];
        }
        throw new IndexOutOfBoundsException(String.valueOf(index));
    }

    public K firstKey() {
        if (this.size != 0) {
            return this.keys[0];
        }
        throw new IllegalStateException("Map is empty.");
    }

    public V firstValue() {
        if (this.size != 0) {
            return this.values[0];
        }
        throw new IllegalStateException("Map is empty.");
    }

    public void setKey(int index, K key) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        this.keys[index] = key;
    }

    public void setValue(int index, V value) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        this.values[index] = value;
    }

    public void insert(int index, K key, V value) {
        if (index > this.size) {
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        if (this.size == this.keys.length) {
            resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        if (this.ordered) {
            System.arraycopy(this.keys, index, this.keys, index + 1, this.size - index);
            System.arraycopy(this.values, index, this.values, index + 1, this.size - index);
        } else {
            this.keys[this.size] = this.keys[index];
            this.values[this.size] = this.values[index];
        }
        this.size++;
        this.keys[index] = key;
        this.values[index] = value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean containsKey(K r6) {
        /*
            r5 = this;
            r3 = 1
            K[] r2 = r5.keys
            int r4 = r5.size
            int r0 = r4 + -1
            if (r6 != 0) goto L_0x0021
            r1 = r0
        L_0x000a:
            if (r1 >= 0) goto L_0x000f
            r0 = r1
        L_0x000d:
            r3 = 0
        L_0x000e:
            return r3
        L_0x000f:
            int r0 = r1 + -1
            r4 = r2[r1]
            if (r4 == r6) goto L_0x000e
            r1 = r0
            goto L_0x000a
        L_0x0017:
            int r0 = r1 + -1
            r4 = r2[r1]
            boolean r4 = r6.equals(r4)
            if (r4 != 0) goto L_0x000e
        L_0x0021:
            r1 = r0
            if (r1 >= 0) goto L_0x0017
            r0 = r1
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.containsKey(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0011  */
    public boolean containsValue(V r6, boolean r7) {
        /*
            r5 = this;
            r3 = 1
            V[] r2 = r5.values
            int r4 = r5.size
            int r0 = r4 + -1
            if (r7 != 0) goto L_0x0017
            if (r6 != 0) goto L_0x0023
            r1 = r0
        L_0x000c:
            if (r1 >= 0) goto L_0x0011
            r0 = r1
        L_0x000f:
            r3 = 0
        L_0x0010:
            return r3
        L_0x0011:
            int r0 = r1 + -1
            r4 = r2[r1]
            if (r4 == r6) goto L_0x0010
        L_0x0017:
            r1 = r0
            goto L_0x000c
        L_0x0019:
            int r0 = r1 + -1
            r4 = r2[r1]
            boolean r4 = r6.equals(r4)
            if (r4 != 0) goto L_0x0010
        L_0x0023:
            r1 = r0
            if (r1 >= 0) goto L_0x0019
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.containsValue(java.lang.Object, boolean):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int indexOfKey(K r5) {
        /*
            r4 = this;
            K[] r1 = r4.keys
            if (r5 != 0) goto L_0x0014
            r0 = 0
            int r2 = r4.size
        L_0x0007:
            if (r0 < r2) goto L_0x000b
        L_0x0009:
            r3 = -1
        L_0x000a:
            return r3
        L_0x000b:
            r3 = r1[r0]
            if (r3 != r5) goto L_0x0011
            r3 = r0
            goto L_0x000a
        L_0x0011:
            int r0 = r0 + 1
            goto L_0x0007
        L_0x0014:
            r0 = 0
            int r2 = r4.size
        L_0x0017:
            if (r0 >= r2) goto L_0x0009
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0023
            r3 = r0
            goto L_0x000a
        L_0x0023:
            int r0 = r0 + 1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.indexOfKey(java.lang.Object):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int indexOfValue(V r5, boolean r6) {
        /*
            r4 = this;
            V[] r2 = r4.values
            if (r6 != 0) goto L_0x0006
            if (r5 != 0) goto L_0x0016
        L_0x0006:
            r0 = 0
            int r1 = r4.size
        L_0x0009:
            if (r0 < r1) goto L_0x000d
        L_0x000b:
            r3 = -1
        L_0x000c:
            return r3
        L_0x000d:
            r3 = r2[r0]
            if (r3 != r5) goto L_0x0013
            r3 = r0
            goto L_0x000c
        L_0x0013:
            int r0 = r0 + 1
            goto L_0x0009
        L_0x0016:
            r0 = 0
            int r1 = r4.size
        L_0x0019:
            if (r0 >= r1) goto L_0x000b
            r3 = r2[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0025
            r3 = r0
            goto L_0x000c
        L_0x0025:
            int r0 = r0 + 1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.indexOfValue(java.lang.Object, boolean):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public V removeKey(K r6) {
        /*
            r5 = this;
            K[] r1 = r5.keys
            if (r6 != 0) goto L_0x001a
            r0 = 0
            int r2 = r5.size
        L_0x0007:
            if (r0 < r2) goto L_0x000b
        L_0x0009:
            r3 = 0
        L_0x000a:
            return r3
        L_0x000b:
            r4 = r1[r0]
            if (r4 != r6) goto L_0x0017
            V[] r4 = r5.values
            r3 = r4[r0]
            r5.removeIndex(r0)
            goto L_0x000a
        L_0x0017:
            int r0 = r0 + 1
            goto L_0x0007
        L_0x001a:
            r0 = 0
            int r2 = r5.size
        L_0x001d:
            if (r0 >= r2) goto L_0x0009
            r4 = r1[r0]
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x002f
            V[] r4 = r5.values
            r3 = r4[r0]
            r5.removeIndex(r0)
            goto L_0x000a
        L_0x002f:
            int r0 = r0 + 1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.removeKey(java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean removeValue(V r6, boolean r7) {
        /*
            r5 = this;
            r3 = 1
            V[] r2 = r5.values
            if (r7 != 0) goto L_0x0007
            if (r6 != 0) goto L_0x0019
        L_0x0007:
            r0 = 0
            int r1 = r5.size
        L_0x000a:
            if (r0 < r1) goto L_0x000e
        L_0x000c:
            r3 = 0
        L_0x000d:
            return r3
        L_0x000e:
            r4 = r2[r0]
            if (r4 != r6) goto L_0x0016
            r5.removeIndex(r0)
            goto L_0x000d
        L_0x0016:
            int r0 = r0 + 1
            goto L_0x000a
        L_0x0019:
            r0 = 0
            int r1 = r5.size
        L_0x001c:
            if (r0 >= r1) goto L_0x000c
            r4 = r2[r0]
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x002a
            r5.removeIndex(r0)
            goto L_0x000d
        L_0x002a:
            int r0 = r0 + 1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ArrayMap.removeValue(java.lang.Object, boolean):boolean");
    }

    public void removeIndex(int index) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        Object[] keys2 = this.keys;
        this.size--;
        if (this.ordered) {
            System.arraycopy(keys2, index + 1, keys2, index, this.size - index);
            System.arraycopy(this.values, index + 1, this.values, index, this.size - index);
        } else {
            keys2[index] = keys2[this.size];
            this.values[index] = this.values[this.size];
        }
        keys2[this.size] = null;
        this.values[this.size] = null;
    }

    public K peekKey() {
        return this.keys[this.size - 1];
    }

    public V peekValue() {
        return this.values[this.size - 1];
    }

    public void clear(int maximumCapacity) {
        if (this.keys.length <= maximumCapacity) {
            clear();
            return;
        }
        this.size = 0;
        resize(maximumCapacity);
    }

    public void clear() {
        Object[] keys2 = this.keys;
        Object[] values2 = this.values;
        int n = this.size;
        for (int i = 0; i < n; i++) {
            keys2[i] = null;
            values2[i] = null;
        }
        this.size = 0;
    }

    public void shrink() {
        if (this.keys.length != this.size) {
            resize(this.size);
        }
    }

    public void ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded >= this.keys.length) {
            resize(Math.max(8, sizeNeeded));
        }
    }

    /* access modifiers changed from: protected */
    public void resize(int newSize) {
        Object[] newKeys = (Object[]) ArrayReflection.newInstance(this.keys.getClass().getComponentType(), newSize);
        System.arraycopy(this.keys, 0, newKeys, 0, Math.min(this.size, newKeys.length));
        this.keys = newKeys;
        Object[] newValues = (Object[]) ArrayReflection.newInstance(this.values.getClass().getComponentType(), newSize);
        System.arraycopy(this.values, 0, newValues, 0, Math.min(this.size, newValues.length));
        this.values = newValues;
    }

    public void reverse() {
        int lastIndex = this.size - 1;
        int n = this.size / 2;
        for (int i = 0; i < n; i++) {
            int ii = lastIndex - i;
            K tempKey = this.keys[i];
            this.keys[i] = this.keys[ii];
            this.keys[ii] = tempKey;
            V tempValue = this.values[i];
            this.values[i] = this.values[ii];
            this.values[ii] = tempValue;
        }
    }

    public void shuffle() {
        for (int i = this.size - 1; i >= 0; i--) {
            int ii = MathUtils.random(i);
            K tempKey = this.keys[i];
            this.keys[i] = this.keys[ii];
            this.keys[ii] = tempKey;
            V tempValue = this.values[i];
            this.values[i] = this.values[ii];
            this.values[ii] = tempValue;
        }
    }

    public void truncate(int newSize) {
        if (this.size > newSize) {
            for (int i = newSize; i < this.size; i++) {
                this.keys[i] = null;
                this.values[i] = null;
            }
            this.size = newSize;
        }
    }

    public String toString() {
        if (this.size == 0) {
            return "{}";
        }
        Object[] keys2 = this.keys;
        Object[] values2 = this.values;
        StringBuilder buffer = new StringBuilder(32);
        buffer.append('{');
        buffer.append(keys2[0]);
        buffer.append((char) SignatureVisitor.INSTANCEOF);
        buffer.append(values2[0]);
        for (int i = 1; i < this.size; i++) {
            buffer.append(", ");
            buffer.append(keys2[i]);
            buffer.append((char) SignatureVisitor.INSTANCEOF);
            buffer.append(values2[i]);
        }
        buffer.append('}');
        return buffer.toString();
    }

    public Iterator<ObjectMap.Entry<K, V>> iterator() {
        return entries();
    }

    public Entries<K, V> entries() {
        if (this.entries1 == null) {
            this.entries1 = new Entries(this);
            this.entries2 = new Entries(this);
        }
        if (!this.entries1.valid) {
            this.entries1.index = 0;
            this.entries1.valid = true;
            this.entries2.valid = false;
            return this.entries1;
        }
        this.entries2.index = 0;
        this.entries2.valid = true;
        this.entries1.valid = false;
        return this.entries2;
    }

    public Values<V> values() {
        if (this.valuesIter1 == null) {
            this.valuesIter1 = new Values(this);
            this.valuesIter2 = new Values(this);
        }
        if (!this.valuesIter1.valid) {
            this.valuesIter1.index = 0;
            this.valuesIter1.valid = true;
            this.valuesIter2.valid = false;
            return this.valuesIter1;
        }
        this.valuesIter2.index = 0;
        this.valuesIter2.valid = true;
        this.valuesIter1.valid = false;
        return this.valuesIter2;
    }

    public Keys<K> keys() {
        if (this.keysIter1 == null) {
            this.keysIter1 = new Keys(this);
            this.keysIter2 = new Keys(this);
        }
        if (!this.keysIter1.valid) {
            this.keysIter1.index = 0;
            this.keysIter1.valid = true;
            this.keysIter2.valid = false;
            return this.keysIter1;
        }
        this.keysIter2.index = 0;
        this.keysIter2.valid = true;
        this.keysIter1.valid = false;
        return this.keysIter2;
    }

    public static class Entries<K, V> implements Iterable<ObjectMap.Entry<K, V>>, Iterator<ObjectMap.Entry<K, V>> {
        ObjectMap.Entry<K, V> entry = new ObjectMap.Entry<>();
        int index;
        private final ArrayMap<K, V> map;
        boolean valid = true;

        public Entries(ArrayMap<K, V> map2) {
            this.map = map2;
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.index < this.map.size;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public Iterator<ObjectMap.Entry<K, V>> iterator() {
            return this;
        }

        public ObjectMap.Entry<K, V> next() {
            if (this.index >= this.map.size) {
                throw new NoSuchElementException(String.valueOf(this.index));
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                this.entry.key = this.map.keys[this.index];
                ObjectMap.Entry<K, V> entry2 = this.entry;
                V[] vArr = this.map.values;
                int i = this.index;
                this.index = i + 1;
                entry2.value = vArr[i];
                return this.entry;
            }
        }

        public void remove() {
            this.index--;
            this.map.removeIndex(this.index);
        }

        public void reset() {
            this.index = 0;
        }
    }

    public static class Values<V> implements Iterable<V>, Iterator<V> {
        int index;
        private final ArrayMap<Object, V> map;
        boolean valid = true;

        public Values(ArrayMap<Object, V> map2) {
            this.map = map2;
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.index < this.map.size;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public Iterator<V> iterator() {
            return this;
        }

        public V next() {
            if (this.index >= this.map.size) {
                throw new NoSuchElementException(String.valueOf(this.index));
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                V[] vArr = this.map.values;
                int i = this.index;
                this.index = i + 1;
                return vArr[i];
            }
        }

        public void remove() {
            this.index--;
            this.map.removeIndex(this.index);
        }

        public void reset() {
            this.index = 0;
        }

        public Array<V> toArray() {
            return new Array<>(true, this.map.values, this.index, this.map.size - this.index);
        }

        public Array<V> toArray(Array array) {
            array.addAll(this.map.values, this.index, this.map.size - this.index);
            return array;
        }
    }

    public static class Keys<K> implements Iterable<K>, Iterator<K> {
        int index;
        private final ArrayMap<K, Object> map;
        boolean valid = true;

        public Keys(ArrayMap<K, Object> map2) {
            this.map = map2;
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.index < this.map.size;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public Iterator<K> iterator() {
            return this;
        }

        public K next() {
            if (this.index >= this.map.size) {
                throw new NoSuchElementException(String.valueOf(this.index));
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                K[] kArr = this.map.keys;
                int i = this.index;
                this.index = i + 1;
                return kArr[i];
            }
        }

        public void remove() {
            this.index--;
            this.map.removeIndex(this.index);
        }

        public void reset() {
            this.index = 0;
        }

        public Array<K> toArray() {
            return new Array<>(true, this.map.keys, this.index, this.map.size - this.index);
        }

        public Array<K> toArray(Array array) {
            array.addAll(this.map.keys, this.index, this.map.size - this.index);
            return array;
        }
    }
}
