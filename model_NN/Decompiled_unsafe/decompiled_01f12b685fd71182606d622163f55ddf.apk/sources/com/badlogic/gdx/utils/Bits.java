package com.badlogic.gdx.utils;

public class Bits {
    long[] bits = new long[1];

    public Bits() {
    }

    public Bits(int nbits) {
        checkCapacity(nbits >>> 6);
    }

    public boolean get(int index) {
        int word = index >>> 6;
        if (word < this.bits.length && (this.bits[word] & (1 << (index & 63))) != 0) {
            return true;
        }
        return false;
    }

    public boolean getAndClear(int index) {
        int word = index >>> 6;
        if (word >= this.bits.length) {
            return false;
        }
        long oldBits = this.bits[word];
        long[] jArr = this.bits;
        jArr[word] = jArr[word] & ((1 << (index & 63)) ^ -1);
        if (this.bits[word] != oldBits) {
            return true;
        }
        return false;
    }

    public boolean getAndSet(int index) {
        int word = index >>> 6;
        checkCapacity(word);
        long oldBits = this.bits[word];
        long[] jArr = this.bits;
        jArr[word] = jArr[word] | (1 << (index & 63));
        return this.bits[word] == oldBits;
    }

    public void set(int index) {
        int word = index >>> 6;
        checkCapacity(word);
        long[] jArr = this.bits;
        jArr[word] = jArr[word] | (1 << (index & 63));
    }

    public void flip(int index) {
        int word = index >>> 6;
        checkCapacity(word);
        long[] jArr = this.bits;
        jArr[word] = jArr[word] ^ (1 << (index & 63));
    }

    private void checkCapacity(int len) {
        if (len >= this.bits.length) {
            long[] newBits = new long[(len + 1)];
            System.arraycopy(this.bits, 0, newBits, 0, this.bits.length);
            this.bits = newBits;
        }
    }

    public void clear(int index) {
        int word = index >>> 6;
        if (word < this.bits.length) {
            long[] jArr = this.bits;
            jArr[word] = jArr[word] & ((1 << (index & 63)) ^ -1);
        }
    }

    public void clear() {
        long[] bits2 = this.bits;
        int length = bits2.length;
        for (int i = 0; i < length; i++) {
            bits2[i] = 0;
        }
    }

    public int numBits() {
        return this.bits.length << 6;
    }

    public int length() {
        long[] bits2 = this.bits;
        for (int word = bits2.length - 1; word >= 0; word--) {
            long bitsAtWord = bits2[word];
            if (bitsAtWord != 0) {
                for (int bit = 63; bit >= 0; bit--) {
                    if (((1 << (bit & 63)) & bitsAtWord) != 0) {
                        return (word << 6) + bit;
                    }
                }
                continue;
            }
        }
        return 0;
    }

    public boolean isEmpty() {
        for (long j : this.bits) {
            if (j != 0) {
                return false;
            }
        }
        return true;
    }

    public int nextSetBit(int fromIndex) {
        long[] bits2 = this.bits;
        int word = fromIndex >>> 6;
        int bitsLength = bits2.length;
        if (word >= bitsLength) {
            return -1;
        }
        long bitsAtWord = bits2[word];
        if (bitsAtWord != 0) {
            for (int i = fromIndex & 63; i < 64; i++) {
                if (((1 << (i & 63)) & bitsAtWord) != 0) {
                    return (word << 6) + i;
                }
            }
        }
        for (int word2 = word + 1; word2 < bitsLength; word2++) {
            if (word2 != 0) {
                long bitsAtWord2 = bits2[word2];
                if (bitsAtWord2 != 0) {
                    for (int i2 = 0; i2 < 64; i2++) {
                        if (((1 << (i2 & 63)) & bitsAtWord2) != 0) {
                            return (word2 << 6) + i2;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            }
        }
        return -1;
    }

    public int nextClearBit(int fromIndex) {
        long[] bits2 = this.bits;
        int word = fromIndex >>> 6;
        int bitsLength = bits2.length;
        if (word >= bitsLength) {
            return -1;
        }
        long bitsAtWord = bits2[word];
        for (int i = fromIndex & 63; i < 64; i++) {
            if (((1 << (i & 63)) & bitsAtWord) == 0) {
                return (word << 6) + i;
            }
        }
        for (int word2 = word + 1; word2 < bitsLength; word2++) {
            if (word2 == 0) {
                return word2 << 6;
            }
            long bitsAtWord2 = bits2[word2];
            for (int i2 = 0; i2 < 64; i2++) {
                if (((1 << (i2 & 63)) & bitsAtWord2) == 0) {
                    return (word2 << 6) + i2;
                }
            }
        }
        return -1;
    }

    public void and(Bits other) {
        int commonWords = Math.min(this.bits.length, other.bits.length);
        for (int i = 0; commonWords > i; i++) {
            long[] jArr = this.bits;
            jArr[i] = jArr[i] & other.bits[i];
        }
        if (this.bits.length > commonWords) {
            int s = this.bits.length;
            for (int i2 = commonWords; s > i2; i2++) {
                this.bits[i2] = 0;
            }
        }
    }

    public void andNot(Bits other) {
        int i = 0;
        int j = this.bits.length;
        int k = other.bits.length;
        while (i < j && i < k) {
            long[] jArr = this.bits;
            jArr[i] = jArr[i] & (other.bits[i] ^ -1);
            i++;
        }
    }

    public void or(Bits other) {
        int commonWords = Math.min(this.bits.length, other.bits.length);
        for (int i = 0; commonWords > i; i++) {
            long[] jArr = this.bits;
            jArr[i] = jArr[i] | other.bits[i];
        }
        if (commonWords < other.bits.length) {
            checkCapacity(other.bits.length);
            int s = other.bits.length;
            for (int i2 = commonWords; s > i2; i2++) {
                this.bits[i2] = other.bits[i2];
            }
        }
    }

    public void xor(Bits other) {
        int commonWords = Math.min(this.bits.length, other.bits.length);
        for (int i = 0; commonWords > i; i++) {
            long[] jArr = this.bits;
            jArr[i] = jArr[i] ^ other.bits[i];
        }
        if (this.bits.length > commonWords) {
            int s = this.bits.length;
            for (int i2 = other.bits.length; s > i2; i2++) {
                this.bits[i2] = 0;
            }
        } else if (commonWords < other.bits.length) {
            checkCapacity(other.bits.length);
            int s2 = other.bits.length;
            for (int i3 = commonWords; s2 > i3; i3++) {
                this.bits[i3] = other.bits[i3];
            }
        }
    }

    public boolean intersects(Bits other) {
        long[] bits2 = this.bits;
        long[] otherBits = other.bits;
        for (int i = Math.min(bits2.length, otherBits.length) - 1; i >= 0; i--) {
            if ((bits2[i] & otherBits[i]) != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean containsAll(Bits other) {
        long[] bits2 = this.bits;
        long[] otherBits = other.bits;
        int otherBitsLength = otherBits.length;
        int bitsLength = bits2.length;
        for (int i = bitsLength; i < otherBitsLength; i++) {
            if (otherBits[i] != 0) {
                return false;
            }
        }
        for (int i2 = Math.min(bitsLength, otherBitsLength) - 1; i2 >= 0; i2--) {
            if ((bits2[i2] & otherBits[i2]) != otherBits[i2]) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int word = length() >>> 6;
        int hash = 0;
        for (int i = 0; word >= i; i++) {
            hash = (hash * 127) + ((int) (this.bits[i] ^ (this.bits[i] >>> 32)));
        }
        return hash;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Bits other = (Bits) obj;
        long[] otherBits = other.bits;
        int commonWords = Math.min(this.bits.length, otherBits.length);
        for (int i = 0; commonWords > i; i++) {
            if (this.bits[i] != otherBits[i]) {
                return false;
            }
        }
        if (this.bits.length == otherBits.length || length() == other.length()) {
            return true;
        }
        return false;
    }
}
