package com.badlogic.gdx.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.kbz.esotericsoftware.spine.Animation;

public class Timer {
    private static final int CANCELLED = -1;
    private static final int FOREVER = -2;
    static Timer instance = new Timer();
    static final Array<Timer> instances = new Array<>(1);
    static TimerThread thread;
    private final Array<Task> tasks = new Array<>(false, 8);

    public static Timer instance() {
        if (instance == null) {
            instance = new Timer();
        }
        return instance;
    }

    public Timer() {
        start();
    }

    public Task postTask(Task task) {
        return scheduleTask(task, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0);
    }

    public Task scheduleTask(Task task, float delaySeconds) {
        return scheduleTask(task, delaySeconds, Animation.CurveTimeline.LINEAR, 0);
    }

    public Task scheduleTask(Task task, float delaySeconds, float intervalSeconds) {
        return scheduleTask(task, delaySeconds, intervalSeconds, -2);
    }

    public Task scheduleTask(Task task, float delaySeconds, float intervalSeconds, int repeatCount) {
        if (task.repeatCount != -1) {
            throw new IllegalArgumentException("The same task may not be scheduled twice.");
        }
        task.executeTimeMillis = (System.nanoTime() / 1000000) + ((long) (delaySeconds * 1000.0f));
        task.intervalMillis = (long) (intervalSeconds * 1000.0f);
        task.repeatCount = repeatCount;
        synchronized (this.tasks) {
            this.tasks.add(task);
        }
        wake();
        return task;
    }

    public void stop() {
        synchronized (instances) {
            instances.removeValue(this, true);
        }
    }

    public void start() {
        synchronized (instances) {
            if (!instances.contains(this, true)) {
                instances.add(this);
                if (thread == null) {
                    thread = new TimerThread();
                }
                wake();
            }
        }
    }

    public void clear() {
        synchronized (this.tasks) {
            int n = this.tasks.size;
            for (int i = 0; i < n; i++) {
                this.tasks.get(i).cancel();
            }
            this.tasks.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public long update(long timeMillis, long waitMillis) {
        synchronized (this.tasks) {
            int i = 0;
            int n = this.tasks.size;
            while (i < n) {
                Task task = this.tasks.get(i);
                if (task.executeTimeMillis > timeMillis) {
                    waitMillis = Math.min(waitMillis, task.executeTimeMillis - timeMillis);
                } else {
                    if (task.repeatCount != -1) {
                        if (task.repeatCount == 0) {
                            task.repeatCount = -1;
                        }
                        Gdx.app.postRunnable(task);
                    }
                    if (task.repeatCount == -1) {
                        this.tasks.removeIndex(i);
                        i--;
                        n--;
                    } else {
                        task.executeTimeMillis = task.intervalMillis + timeMillis;
                        waitMillis = Math.min(waitMillis, task.intervalMillis);
                        if (task.repeatCount > 0) {
                            task.repeatCount--;
                        }
                    }
                }
                i++;
            }
        }
        return waitMillis;
    }

    public void delay(long delayMillis) {
        synchronized (this.tasks) {
            int n = this.tasks.size;
            for (int i = 0; i < n; i++) {
                this.tasks.get(i).executeTimeMillis += delayMillis;
            }
        }
    }

    static void wake() {
        synchronized (instances) {
            instances.notifyAll();
        }
    }

    public static Task post(Task task) {
        return instance().postTask(task);
    }

    public static Task schedule(Task task, float delaySeconds) {
        return instance().scheduleTask(task, delaySeconds);
    }

    public static Task schedule(Task task, float delaySeconds, float intervalSeconds) {
        return instance().scheduleTask(task, delaySeconds, intervalSeconds);
    }

    public static Task schedule(Task task, float delaySeconds, float intervalSeconds, int repeatCount) {
        return instance().scheduleTask(task, delaySeconds, intervalSeconds, repeatCount);
    }

    public static abstract class Task implements Runnable {
        long executeTimeMillis;
        long intervalMillis;
        int repeatCount = -1;

        public abstract void run();

        public void cancel() {
            this.executeTimeMillis = 0;
            this.repeatCount = -1;
        }

        public boolean isScheduled() {
            return this.repeatCount != -1;
        }

        public long getExecuteTimeMillis() {
            return this.executeTimeMillis;
        }
    }

    static class TimerThread implements LifecycleListener, Runnable {
        Application app;
        private long pauseMillis;

        public TimerThread() {
            Gdx.app.addLifecycleListener(this);
            resume();
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r14 = this;
            L_0x0000:
                com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Timer> r8 = com.badlogic.gdx.utils.Timer.instances
                monitor-enter(r8)
                com.badlogic.gdx.Application r3 = r14.app     // Catch:{ all -> 0x0025 }
                com.badlogic.gdx.Application r9 = com.badlogic.gdx.Gdx.app     // Catch:{ all -> 0x0025 }
                if (r3 == r9) goto L_0x000b
                monitor-exit(r8)     // Catch:{ all -> 0x0025 }
            L_0x000a:
                return
            L_0x000b:
                long r10 = java.lang.System.nanoTime()     // Catch:{ all -> 0x0025 }
                r12 = 1000000(0xf4240, double:4.940656E-318)
                long r4 = r10 / r12
                r6 = 5000(0x1388, double:2.4703E-320)
                r1 = 0
                com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Timer> r3 = com.badlogic.gdx.utils.Timer.instances     // Catch:{ all -> 0x0025 }
                int r2 = r3.size     // Catch:{ all -> 0x0025 }
            L_0x001b:
                if (r1 < r2) goto L_0x0028
                com.badlogic.gdx.Application r3 = r14.app     // Catch:{ all -> 0x0025 }
                com.badlogic.gdx.Application r9 = com.badlogic.gdx.Gdx.app     // Catch:{ all -> 0x0025 }
                if (r3 == r9) goto L_0x005d
                monitor-exit(r8)     // Catch:{ all -> 0x0025 }
                goto L_0x000a
            L_0x0025:
                r3 = move-exception
                monitor-exit(r8)     // Catch:{ all -> 0x0025 }
                throw r3
            L_0x0028:
                com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Timer> r3 = com.badlogic.gdx.utils.Timer.instances     // Catch:{ Throwable -> 0x0037 }
                java.lang.Object r3 = r3.get(r1)     // Catch:{ Throwable -> 0x0037 }
                com.badlogic.gdx.utils.Timer r3 = (com.badlogic.gdx.utils.Timer) r3     // Catch:{ Throwable -> 0x0037 }
                long r6 = r3.update(r4, r6)     // Catch:{ Throwable -> 0x0037 }
                int r1 = r1 + 1
                goto L_0x001b
            L_0x0037:
                r0 = move-exception
                com.badlogic.gdx.utils.GdxRuntimeException r9 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ all -> 0x0025 }
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0025 }
                java.lang.String r3 = "Task failed: "
                r10.<init>(r3)     // Catch:{ all -> 0x0025 }
                com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Timer> r3 = com.badlogic.gdx.utils.Timer.instances     // Catch:{ all -> 0x0025 }
                java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x0025 }
                com.badlogic.gdx.utils.Timer r3 = (com.badlogic.gdx.utils.Timer) r3     // Catch:{ all -> 0x0025 }
                java.lang.Class r3 = r3.getClass()     // Catch:{ all -> 0x0025 }
                java.lang.String r3 = r3.getName()     // Catch:{ all -> 0x0025 }
                java.lang.StringBuilder r3 = r10.append(r3)     // Catch:{ all -> 0x0025 }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0025 }
                r9.<init>(r3, r0)     // Catch:{ all -> 0x0025 }
                throw r9     // Catch:{ all -> 0x0025 }
            L_0x005d:
                r10 = 0
                int r3 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
                if (r3 <= 0) goto L_0x0068
                com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Timer> r3 = com.badlogic.gdx.utils.Timer.instances     // Catch:{ InterruptedException -> 0x006a }
                r3.wait(r6)     // Catch:{ InterruptedException -> 0x006a }
            L_0x0068:
                monitor-exit(r8)     // Catch:{ all -> 0x0025 }
                goto L_0x0000
            L_0x006a:
                r3 = move-exception
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Timer.TimerThread.run():void");
        }

        public void resume() {
            long delayMillis = (System.nanoTime() / 1000000) - this.pauseMillis;
            synchronized (Timer.instances) {
                int n = Timer.instances.size;
                for (int i = 0; i < n; i++) {
                    Timer.instances.get(i).delay(delayMillis);
                }
            }
            this.app = Gdx.app;
            new Thread(this, "Timer").start();
        }

        public void pause() {
            this.pauseMillis = System.nanoTime() / 1000000;
            synchronized (Timer.instances) {
                this.app = null;
                Timer.wake();
            }
            Timer.thread = null;
        }

        public void dispose() {
            pause();
            Gdx.app.removeLifecycleListener(this);
            Timer.instances.clear();
            Timer.instance = null;
        }
    }
}
