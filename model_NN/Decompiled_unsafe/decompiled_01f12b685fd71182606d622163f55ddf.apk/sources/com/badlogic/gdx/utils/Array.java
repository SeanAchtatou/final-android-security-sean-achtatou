package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Predicate;
import com.badlogic.gdx.utils.reflect.ArrayReflection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Array<T> implements Iterable<T> {
    public T[] items;
    private ArrayIterable iterable;
    public boolean ordered;
    private Predicate.PredicateIterable<T> predicateIterable;
    public int size;

    public Array() {
        this(true, 16);
    }

    public Array(int capacity) {
        this(true, capacity);
    }

    public Array(boolean ordered2, int capacity) {
        this.ordered = ordered2;
        this.items = new Object[capacity];
    }

    public Array(boolean ordered2, int capacity, Class arrayType) {
        this.ordered = ordered2;
        this.items = (Object[]) ArrayReflection.newInstance(arrayType, capacity);
    }

    public Array(Class arrayType) {
        this(true, 16, arrayType);
    }

    public Array(Array array) {
        this(array.ordered, array.size, array.items.getClass().getComponentType());
        this.size = array.size;
        System.arraycopy(array.items, 0, this.items, 0, this.size);
    }

    public Array(Object[] array) {
        this(true, array, 0, array.length);
    }

    public Array(boolean ordered2, T[] array, int start, int count) {
        this(ordered2, count, array.getClass().getComponentType());
        this.size = count;
        System.arraycopy(array, start, this.items, 0, this.size);
    }

    public void add(T value) {
        Object[] items2 = this.items;
        if (this.size == items2.length) {
            items2 = resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        int i = this.size;
        this.size = i + 1;
        items2[i] = value;
    }

    public void addAll(Array<? extends T> array) {
        addAll(array, 0, array.size);
    }

    public void addAll(Array<? extends T> array, int start, int count) {
        if (start + count > array.size) {
            throw new IllegalArgumentException("start + count must be <= size: " + start + " + " + count + " <= " + array.size);
        }
        addAll(array.items, start, count);
    }

    public void addAll(T... array) {
        addAll(array, 0, array.length);
    }

    public void addAll(T[] array, int start, int count) {
        Object[] items2 = this.items;
        int sizeNeeded = this.size + count;
        if (sizeNeeded > items2.length) {
            items2 = resize(Math.max(8, (int) (((float) sizeNeeded) * 1.75f)));
        }
        System.arraycopy(array, start, items2, this.size, count);
        this.size += count;
    }

    public T get(int index) {
        if (index < this.size) {
            return this.items[index];
        }
        throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
    }

    public void set(int index, T value) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
        }
        this.items[index] = value;
    }

    public void insert(int index, T value) {
        if (index > this.size) {
            throw new IndexOutOfBoundsException("index can't be > size: " + index + " > " + this.size);
        }
        Object[] items2 = this.items;
        if (this.size == items2.length) {
            items2 = resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        if (this.ordered) {
            System.arraycopy(items2, index, items2, index + 1, this.size - index);
        } else {
            items2[this.size] = items2[index];
        }
        this.size++;
        items2[index] = value;
    }

    public void swap(int first, int second) {
        if (first >= this.size) {
            throw new IndexOutOfBoundsException("first can't be >= size: " + first + " >= " + this.size);
        } else if (second >= this.size) {
            throw new IndexOutOfBoundsException("second can't be >= size: " + second + " >= " + this.size);
        } else {
            T[] items2 = this.items;
            T firstValue = items2[first];
            items2[first] = items2[second];
            items2[second] = firstValue;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0011  */
    public boolean contains(T r6, boolean r7) {
        /*
            r5 = this;
            r3 = 1
            T[] r2 = r5.items
            int r4 = r5.size
            int r0 = r4 + -1
            if (r7 != 0) goto L_0x0017
            if (r6 != 0) goto L_0x0023
            r1 = r0
        L_0x000c:
            if (r1 >= 0) goto L_0x0011
            r0 = r1
        L_0x000f:
            r3 = 0
        L_0x0010:
            return r3
        L_0x0011:
            int r0 = r1 + -1
            r4 = r2[r1]
            if (r4 == r6) goto L_0x0010
        L_0x0017:
            r1 = r0
            goto L_0x000c
        L_0x0019:
            int r0 = r1 + -1
            r4 = r2[r1]
            boolean r4 = r6.equals(r4)
            if (r4 != 0) goto L_0x0010
        L_0x0023:
            r1 = r0
            if (r1 >= 0) goto L_0x0019
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.contains(java.lang.Object, boolean):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int indexOf(T r5, boolean r6) {
        /*
            r4 = this;
            T[] r1 = r4.items
            if (r6 != 0) goto L_0x0006
            if (r5 != 0) goto L_0x0016
        L_0x0006:
            r0 = 0
            int r2 = r4.size
        L_0x0009:
            if (r0 < r2) goto L_0x000d
        L_0x000b:
            r3 = -1
        L_0x000c:
            return r3
        L_0x000d:
            r3 = r1[r0]
            if (r3 != r5) goto L_0x0013
            r3 = r0
            goto L_0x000c
        L_0x0013:
            int r0 = r0 + 1
            goto L_0x0009
        L_0x0016:
            r0 = 0
            int r2 = r4.size
        L_0x0019:
            if (r0 >= r2) goto L_0x000b
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0025
            r3 = r0
            goto L_0x000c
        L_0x0025:
            int r0 = r0 + 1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.indexOf(java.lang.Object, boolean):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int lastIndexOf(T r4, boolean r5) {
        /*
            r3 = this;
            T[] r1 = r3.items
            if (r5 != 0) goto L_0x0006
            if (r4 != 0) goto L_0x0017
        L_0x0006:
            int r2 = r3.size
            int r0 = r2 + -1
        L_0x000a:
            if (r0 >= 0) goto L_0x000e
        L_0x000c:
            r2 = -1
        L_0x000d:
            return r2
        L_0x000e:
            r2 = r1[r0]
            if (r2 != r4) goto L_0x0014
            r2 = r0
            goto L_0x000d
        L_0x0014:
            int r0 = r0 + -1
            goto L_0x000a
        L_0x0017:
            int r2 = r3.size
            int r0 = r2 + -1
        L_0x001b:
            if (r0 < 0) goto L_0x000c
            r2 = r1[r0]
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0027
            r2 = r0
            goto L_0x000d
        L_0x0027:
            int r0 = r0 + -1
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.lastIndexOf(java.lang.Object, boolean):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean removeValue(T r6, boolean r7) {
        /*
            r5 = this;
            r3 = 1
            T[] r1 = r5.items
            if (r7 != 0) goto L_0x0007
            if (r6 != 0) goto L_0x0019
        L_0x0007:
            r0 = 0
            int r2 = r5.size
        L_0x000a:
            if (r0 < r2) goto L_0x000e
        L_0x000c:
            r3 = 0
        L_0x000d:
            return r3
        L_0x000e:
            r4 = r1[r0]
            if (r4 != r6) goto L_0x0016
            r5.removeIndex(r0)
            goto L_0x000d
        L_0x0016:
            int r0 = r0 + 1
            goto L_0x000a
        L_0x0019:
            r0 = 0
            int r2 = r5.size
        L_0x001c:
            if (r0 >= r2) goto L_0x000c
            r4 = r1[r0]
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x002a
            r5.removeIndex(r0)
            goto L_0x000d
        L_0x002a:
            int r0 = r0 + 1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.removeValue(java.lang.Object, boolean):boolean");
    }

    public T removeIndex(int index) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + this.size);
        }
        T[] items2 = this.items;
        T value = items2[index];
        this.size--;
        if (this.ordered) {
            System.arraycopy(items2, index + 1, items2, index, this.size - index);
        } else {
            items2[index] = items2[this.size];
        }
        items2[this.size] = null;
        return value;
    }

    public void removeRange(int start, int end) {
        if (end >= this.size) {
            throw new IndexOutOfBoundsException("end can't be >= size: " + end + " >= " + this.size);
        } else if (start > end) {
            throw new IndexOutOfBoundsException("start can't be > end: " + start + " > " + end);
        } else {
            Object[] items2 = this.items;
            int count = (end - start) + 1;
            if (this.ordered) {
                System.arraycopy(items2, start + count, items2, start, this.size - (start + count));
            } else {
                int lastIndex = this.size - 1;
                for (int i = 0; i < count; i++) {
                    items2[start + i] = items2[lastIndex - i];
                }
            }
            this.size -= count;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean removeAll(com.badlogic.gdx.utils.Array<? extends T> r9, boolean r10) {
        /*
            r8 = this;
            int r5 = r8.size
            r6 = r5
            T[] r3 = r8.items
            if (r10 == 0) goto L_0x0027
            r0 = 0
            int r4 = r9.size
        L_0x000a:
            if (r0 < r4) goto L_0x0010
        L_0x000c:
            if (r5 == r6) goto L_0x0047
            r7 = 1
        L_0x000f:
            return r7
        L_0x0010:
            java.lang.Object r2 = r9.get(r0)
            r1 = 0
        L_0x0015:
            if (r1 < r5) goto L_0x001a
        L_0x0017:
            int r0 = r0 + 1
            goto L_0x000a
        L_0x001a:
            r7 = r3[r1]
            if (r2 != r7) goto L_0x0024
            r8.removeIndex(r1)
            int r5 = r5 + -1
            goto L_0x0017
        L_0x0024:
            int r1 = r1 + 1
            goto L_0x0015
        L_0x0027:
            r0 = 0
            int r4 = r9.size
        L_0x002a:
            if (r0 >= r4) goto L_0x000c
            java.lang.Object r2 = r9.get(r0)
            r1 = 0
        L_0x0031:
            if (r1 < r5) goto L_0x0036
        L_0x0033:
            int r0 = r0 + 1
            goto L_0x002a
        L_0x0036:
            r7 = r3[r1]
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x0044
            r8.removeIndex(r1)
            int r5 = r5 + -1
            goto L_0x0033
        L_0x0044:
            int r1 = r1 + 1
            goto L_0x0031
        L_0x0047:
            r7 = 0
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.removeAll(com.badlogic.gdx.utils.Array, boolean):boolean");
    }

    public T pop() {
        if (this.size == 0) {
            throw new IllegalStateException("Array is empty.");
        }
        this.size--;
        T item = this.items[this.size];
        this.items[this.size] = null;
        return item;
    }

    public T peek() {
        if (this.size != 0) {
            return this.items[this.size - 1];
        }
        throw new IllegalStateException("Array is empty.");
    }

    public T first() {
        if (this.size != 0) {
            return this.items[0];
        }
        throw new IllegalStateException("Array is empty.");
    }

    public void clear() {
        Object[] items2 = this.items;
        int n = this.size;
        for (int i = 0; i < n; i++) {
            items2[i] = null;
        }
        this.size = 0;
    }

    public T[] shrink() {
        if (this.items.length != this.size) {
            resize(this.size);
        }
        return this.items;
    }

    public T[] ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded > this.items.length) {
            resize(Math.max(8, sizeNeeded));
        }
        return this.items;
    }

    /* access modifiers changed from: protected */
    public T[] resize(int newSize) {
        Object[] items2 = this.items;
        Object[] newItems = (Object[]) ArrayReflection.newInstance(items2.getClass().getComponentType(), newSize);
        System.arraycopy(items2, 0, newItems, 0, Math.min(this.size, newItems.length));
        this.items = newItems;
        return newItems;
    }

    public void sort() {
        Sort.instance().sort(this.items, 0, this.size);
    }

    public void sort(Comparator<? super T> comparator) {
        Sort.instance().sort(this.items, comparator, 0, this.size);
    }

    public T selectRanked(Comparator<T> comparator, int kthLowest) {
        if (kthLowest >= 1) {
            return Select.instance().select(this.items, comparator, kthLowest, this.size);
        }
        throw new GdxRuntimeException("nth_lowest must be greater than 0, 1 = first, 2 = second...");
    }

    public int selectRankedIndex(Comparator<T> comparator, int kthLowest) {
        if (kthLowest >= 1) {
            return Select.instance().selectIndex(this.items, comparator, kthLowest, this.size);
        }
        throw new GdxRuntimeException("nth_lowest must be greater than 0, 1 = first, 2 = second...");
    }

    public void reverse() {
        T[] items2 = this.items;
        int lastIndex = this.size - 1;
        int n = this.size / 2;
        for (int i = 0; i < n; i++) {
            int ii = lastIndex - i;
            T temp = items2[i];
            items2[i] = items2[ii];
            items2[ii] = temp;
        }
    }

    public void shuffle() {
        T[] items2 = this.items;
        for (int i = this.size - 1; i >= 0; i--) {
            int ii = MathUtils.random(i);
            T temp = items2[i];
            items2[i] = items2[ii];
            items2[ii] = temp;
        }
    }

    public Iterator<T> iterator() {
        if (this.iterable == null) {
            this.iterable = new ArrayIterable(this);
        }
        return this.iterable.iterator();
    }

    public Iterable<T> select(Predicate<T> predicate) {
        if (this.predicateIterable == null) {
            this.predicateIterable = new Predicate.PredicateIterable<>(this, predicate);
        } else {
            this.predicateIterable.set(this, predicate);
        }
        return this.predicateIterable;
    }

    public void truncate(int newSize) {
        if (this.size > newSize) {
            for (int i = newSize; i < this.size; i++) {
                this.items[i] = null;
            }
            this.size = newSize;
        }
    }

    public T random() {
        if (this.size == 0) {
            return null;
        }
        return this.items[MathUtils.random(0, this.size - 1)];
    }

    public T[] toArray() {
        return toArray(this.items.getClass().getComponentType());
    }

    public <V> V[] toArray(Class type) {
        Object[] result = (Object[]) ArrayReflection.newInstance(type, this.size);
        System.arraycopy(this.items, 0, result, 0, this.size);
        return result;
    }

    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Array)) {
            return false;
        }
        Array array = (Array) object;
        int n = this.size;
        if (n != array.size) {
            return false;
        }
        Object[] items1 = this.items;
        Object[] items2 = array.items;
        int i = 0;
        while (i < n) {
            Object o1 = items1[i];
            Object o2 = items2[i];
            if (o1 == null) {
                if (o2 == null) {
                    i++;
                }
            } else if (o1.equals(o2)) {
                i++;
            }
            return false;
        }
        return true;
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        Object[] items2 = this.items;
        StringBuilder buffer = new StringBuilder(32);
        buffer.append('[');
        buffer.append(items2[0]);
        for (int i = 1; i < this.size; i++) {
            buffer.append(", ");
            buffer.append(items2[i]);
        }
        buffer.append(']');
        return buffer.toString();
    }

    public String toString(String separator) {
        if (this.size == 0) {
            return "";
        }
        Object[] items2 = this.items;
        StringBuilder buffer = new StringBuilder(32);
        buffer.append(items2[0]);
        for (int i = 1; i < this.size; i++) {
            buffer.append(separator);
            buffer.append(items2[i]);
        }
        return buffer.toString();
    }

    public static <T> Array<T> of(Class<T> arrayType) {
        return new Array<>(arrayType);
    }

    public static <T> Array<T> of(boolean ordered2, int capacity, Class<T> arrayType) {
        return new Array<>(ordered2, capacity, arrayType);
    }

    public static <T> Array<T> with(T... array) {
        return new Array<>(array);
    }

    public static class ArrayIterator<T> implements Iterator<T>, Iterable<T> {
        private final boolean allowRemove;
        private final Array<T> array;
        int index;
        boolean valid;

        public ArrayIterator(Array<T> array2) {
            this(array2, true);
        }

        public ArrayIterator(Array<T> array2, boolean allowRemove2) {
            this.valid = true;
            this.array = array2;
            this.allowRemove = allowRemove2;
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.index < this.array.size;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public T next() {
            if (this.index >= this.array.size) {
                throw new NoSuchElementException(String.valueOf(this.index));
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                T[] tArr = this.array.items;
                int i = this.index;
                this.index = i + 1;
                return tArr[i];
            }
        }

        public void remove() {
            if (!this.allowRemove) {
                throw new GdxRuntimeException("Remove not allowed.");
            }
            this.index--;
            this.array.removeIndex(this.index);
        }

        public void reset() {
            this.index = 0;
        }

        public Iterator<T> iterator() {
            return this;
        }
    }

    public static class ArrayIterable<T> implements Iterable<T> {
        private final boolean allowRemove;
        private final Array<T> array;
        private ArrayIterator iterator1;
        private ArrayIterator iterator2;

        public ArrayIterable(Array<T> array2) {
            this(array2, true);
        }

        public ArrayIterable(Array<T> array2, boolean allowRemove2) {
            this.array = array2;
            this.allowRemove = allowRemove2;
        }

        public Iterator<T> iterator() {
            if (this.iterator1 == null) {
                this.iterator1 = new ArrayIterator(this.array, this.allowRemove);
                this.iterator2 = new ArrayIterator(this.array, this.allowRemove);
            }
            if (!this.iterator1.valid) {
                this.iterator1.index = 0;
                this.iterator1.valid = true;
                this.iterator2.valid = false;
                return this.iterator1;
            }
            this.iterator2.index = 0;
            this.iterator2.valid = true;
            this.iterator1.valid = false;
            return this.iterator2;
        }
    }
}
