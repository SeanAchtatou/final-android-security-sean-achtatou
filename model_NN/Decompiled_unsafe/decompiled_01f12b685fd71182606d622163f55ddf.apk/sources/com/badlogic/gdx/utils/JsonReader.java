package com.badlogic.gdx.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonValue;
import com.sg.GameSprites.GameSpriteType;
import com.sg.pak.PAK_ASSETS;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class JsonReader implements BaseJsonReader {
    private static final byte[] _json_actions = init__json_actions_0();
    private static final byte[] _json_eof_actions = init__json_eof_actions_0();
    private static final short[] _json_index_offsets = init__json_index_offsets_0();
    private static final byte[] _json_indicies = init__json_indicies_0();
    private static final short[] _json_key_offsets = init__json_key_offsets_0();
    private static final byte[] _json_range_lengths = init__json_range_lengths_0();
    private static final byte[] _json_single_lengths = init__json_single_lengths_0();
    private static final byte[] _json_trans_actions = init__json_trans_actions_0();
    private static final char[] _json_trans_keys = init__json_trans_keys_0();
    private static final byte[] _json_trans_targs = init__json_trans_targs_0();
    static final int json_en_array = 23;
    static final int json_en_main = 1;
    static final int json_en_object = 5;
    static final int json_error = 0;
    static final int json_first_final = 35;
    static final int json_start = 1;
    private JsonValue current;
    private final Array<JsonValue> elements = new Array<>(8);
    private final Array<JsonValue> lastChild = new Array<>(8);
    private JsonValue root;

    public JsonValue parse(String json) {
        char[] data = json.toCharArray();
        return parse(data, 0, data.length);
    }

    public JsonValue parse(Reader reader) {
        try {
            char[] data = new char[1024];
            int offset = 0;
            while (true) {
                int length = reader.read(data, offset, data.length - offset);
                if (length == -1) {
                    JsonValue parse = parse(data, 0, offset);
                    StreamUtils.closeQuietly(reader);
                    return parse;
                } else if (length == 0) {
                    char[] newData = new char[(data.length * 2)];
                    System.arraycopy(data, 0, newData, 0, data.length);
                    data = newData;
                } else {
                    offset += length;
                }
            }
        } catch (IOException ex) {
            throw new SerializationException(ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(reader);
            throw th;
        }
    }

    public JsonValue parse(InputStream input) {
        try {
            JsonValue parse = parse(new InputStreamReader(input, "UTF-8"));
            StreamUtils.closeQuietly(input);
            return parse;
        } catch (IOException ex) {
            throw new SerializationException(ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(input);
            throw th;
        }
    }

    public JsonValue parse(FileHandle file) {
        try {
            return parse(file.reader("UTF-8"));
        } catch (Exception ex) {
            throw new SerializationException("Error parsing file: " + file, ex);
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:4:0x0037 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:320:0x0037 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:323:0x059c */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:330:0x0037 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:328:0x0037 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:327:0x0037 */
    /* JADX WARN: Type inference failed for: r41v2 */
    /* JADX WARN: Type inference failed for: r48v34, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r24v5, types: [byte] */
    /* JADX WARN: Type inference failed for: r41v5 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0560, code lost:
        if (r21 != '*') goto L_0x051c;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x04be A[Catch:{ RuntimeException -> 0x0648 }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x051e A[Catch:{ RuntimeException -> 0x0648 }] */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x0775  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f0 A[Catch:{ RuntimeException -> 0x0648 }] */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x04e4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:373:0x0542 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0061  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.JsonValue parse(char[] r54, int r55, int r56) {
        /*
            r53 = this;
            r35 = r55
            r38 = r56
            r27 = r38
            r45 = 0
            r48 = 4
            r0 = r48
            int[] r0 = new int[r0]
            r41 = r0
            r40 = 0
            com.badlogic.gdx.utils.Array r32 = new com.badlogic.gdx.utils.Array
            r48 = 8
            r0 = r32
            r1 = r48
            r0.<init>(r1)
            r33 = 0
            r43 = 0
            r44 = 0
            r37 = 0
            r25 = 0
            if (r25 == 0) goto L_0x002e
            java.io.PrintStream r48 = java.lang.System.out
            r48.println()
        L_0x002e:
            r24 = 1
            r45 = 0
            r19 = 0
            r12 = 0
            r46 = r45
        L_0x0037:
            switch(r12) {
                case 0: goto L_0x00a9;
                case 1: goto L_0x00b5;
                case 2: goto L_0x0104;
                case 3: goto L_0x003a;
                case 4: goto L_0x059c;
                default: goto L_0x003a;
            }
        L_0x003a:
            r45 = r46
        L_0x003c:
            r0 = r53
            com.badlogic.gdx.utils.JsonValue r0 = r0.root
            r39 = r0
            r48 = 0
            r0 = r48
            r1 = r53
            r1.root = r0
            r48 = 0
            r0 = r48
            r1 = r53
            r1.current = r0
            r0 = r53
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.JsonValue> r0 = r0.lastChild
            r48 = r0
            r48.clear()
            r0 = r35
            r1 = r38
            if (r0 >= r1) goto L_0x0775
            r30 = 1
            r29 = 0
        L_0x0065:
            r0 = r29
            r1 = r35
            if (r0 < r1) goto L_0x0765
            com.badlogic.gdx.utils.SerializationException r48 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r49 = new java.lang.StringBuilder
            java.lang.String r50 = "Error parsing JSON on line "
            r49.<init>(r50)
            r0 = r49
            r1 = r30
            java.lang.StringBuilder r49 = r0.append(r1)
            java.lang.String r50 = " near: "
            java.lang.StringBuilder r49 = r49.append(r50)
            java.lang.String r50 = new java.lang.String
            r51 = 256(0x100, float:3.59E-43)
            int r52 = r38 - r35
            int r51 = java.lang.Math.min(r51, r52)
            r0 = r50
            r1 = r54
            r2 = r35
            r3 = r51
            r0.<init>(r1, r2, r3)
            java.lang.StringBuilder r49 = r49.append(r50)
            java.lang.String r49 = r49.toString()
            r0 = r48
            r1 = r49
            r2 = r37
            r0.<init>(r1, r2)
            throw r48
        L_0x00a9:
            r0 = r35
            r1 = r38
            if (r0 != r1) goto L_0x00b1
            r12 = 4
            goto L_0x0037
        L_0x00b1:
            if (r24 != 0) goto L_0x00b5
            r12 = 5
            goto L_0x0037
        L_0x00b5:
            short[] r48 = com.badlogic.gdx.utils.JsonReader._json_key_offsets     // Catch:{ RuntimeException -> 0x0648 }
            short r13 = r48[r24]     // Catch:{ RuntimeException -> 0x0648 }
            short[] r48 = com.badlogic.gdx.utils.JsonReader._json_index_offsets     // Catch:{ RuntimeException -> 0x0648 }
            short r19 = r48[r24]     // Catch:{ RuntimeException -> 0x0648 }
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_single_lengths     // Catch:{ RuntimeException -> 0x0648 }
            byte r14 = r48[r24]     // Catch:{ RuntimeException -> 0x0648 }
            if (r14 <= 0) goto L_0x00cf
            r15 = r13
            int r48 = r13 + r14
            int r20 = r48 + -1
        L_0x00c8:
            r0 = r20
            if (r0 >= r15) goto L_0x0109
            int r13 = r13 + r14
            int r19 = r19 + r14
        L_0x00cf:
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_range_lengths     // Catch:{ RuntimeException -> 0x0648 }
            byte r14 = r48[r24]     // Catch:{ RuntimeException -> 0x0648 }
            if (r14 <= 0) goto L_0x00e2
            r15 = r13
            int r48 = r14 << 1
            int r48 = r48 + r13
            int r20 = r48 + -2
        L_0x00dc:
            r0 = r20
            if (r0 >= r15) goto L_0x0132
            int r19 = r19 + r14
        L_0x00e2:
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_indicies     // Catch:{ RuntimeException -> 0x0648 }
            byte r19 = r48[r19]     // Catch:{ RuntimeException -> 0x0648 }
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_trans_targs     // Catch:{ RuntimeException -> 0x0648 }
            byte r24 = r48[r19]     // Catch:{ RuntimeException -> 0x0648 }
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_trans_actions     // Catch:{ RuntimeException -> 0x0648 }
            byte r48 = r48[r19]     // Catch:{ RuntimeException -> 0x0648 }
            if (r48 == 0) goto L_0x0104
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_trans_actions     // Catch:{ RuntimeException -> 0x0648 }
            byte r10 = r48[r19]     // Catch:{ RuntimeException -> 0x0648 }
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_actions     // Catch:{ RuntimeException -> 0x0648 }
            int r11 = r10 + 1
            byte r17 = r48[r10]     // Catch:{ RuntimeException -> 0x0648 }
            r18 = r17
            r36 = r35
        L_0x00fe:
            int r17 = r18 + -1
            if (r18 > 0) goto L_0x0161
            r35 = r36
        L_0x0104:
            if (r24 != 0) goto L_0x0591
            r12 = 5
            goto L_0x0037
        L_0x0109:
            int r48 = r20 - r15
            int r48 = r48 >> 1
            int r16 = r15 + r48
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            char[] r49 = com.badlogic.gdx.utils.JsonReader._json_trans_keys     // Catch:{ RuntimeException -> 0x0648 }
            char r49 = r49[r16]     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r48
            r1 = r49
            if (r0 >= r1) goto L_0x011e
            int r20 = r16 + -1
            goto L_0x00c8
        L_0x011e:
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            char[] r49 = com.badlogic.gdx.utils.JsonReader._json_trans_keys     // Catch:{ RuntimeException -> 0x0648 }
            char r49 = r49[r16]     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r48
            r1 = r49
            if (r0 <= r1) goto L_0x012d
            int r15 = r16 + 1
            goto L_0x00c8
        L_0x012d:
            int r48 = r16 - r13
            int r19 = r19 + r48
            goto L_0x00e2
        L_0x0132:
            int r48 = r20 - r15
            int r48 = r48 >> 1
            r48 = r48 & -2
            int r16 = r15 + r48
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            char[] r49 = com.badlogic.gdx.utils.JsonReader._json_trans_keys     // Catch:{ RuntimeException -> 0x0648 }
            char r49 = r49[r16]     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r48
            r1 = r49
            if (r0 >= r1) goto L_0x0149
            int r20 = r16 + -2
            goto L_0x00dc
        L_0x0149:
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            char[] r49 = com.badlogic.gdx.utils.JsonReader._json_trans_keys     // Catch:{ RuntimeException -> 0x0648 }
            int r50 = r16 + 1
            char r49 = r49[r50]     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r48
            r1 = r49
            if (r0 <= r1) goto L_0x015a
            int r15 = r16 + 2
            goto L_0x00dc
        L_0x015a:
            int r48 = r16 - r13
            int r48 = r48 >> 1
            int r19 = r19 + r48
            goto L_0x00e2
        L_0x0161:
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_actions     // Catch:{ RuntimeException -> 0x0201 }
            int r10 = r11 + 1
            byte r48 = r48[r11]     // Catch:{ RuntimeException -> 0x0201 }
            switch(r48) {
                case 0: goto L_0x016e;
                case 1: goto L_0x0174;
                case 2: goto L_0x0322;
                case 3: goto L_0x0393;
                case 4: goto L_0x03aa;
                case 5: goto L_0x041b;
                case 6: goto L_0x0432;
                case 7: goto L_0x04a4;
                case 8: goto L_0x0567;
                default: goto L_0x016a;
            }     // Catch:{ RuntimeException -> 0x0201 }
        L_0x016a:
            r18 = r17
            r11 = r10
            goto L_0x00fe
        L_0x016e:
            r43 = 1
            r18 = r17
            r11 = r10
            goto L_0x00fe
        L_0x0174:
            java.lang.String r47 = new java.lang.String     // Catch:{ RuntimeException -> 0x0201 }
            int r48 = r36 - r40
            r0 = r47
            r1 = r54
            r2 = r40
            r3 = r48
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0201 }
            if (r33 == 0) goto L_0x018d
            r0 = r53
            r1 = r47
            java.lang.String r47 = r0.unescape(r1)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x018d:
            if (r43 == 0) goto L_0x01bb
            r43 = 0
            if (r25 == 0) goto L_0x01ab
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "name: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r47
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x01ab:
            r0 = r32
            r1 = r47
            r0.add(r1)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x01b2:
            r44 = 0
            r40 = r36
            r18 = r17
            r11 = r10
            goto L_0x00fe
        L_0x01bb:
            r0 = r32
            int r0 = r0.size     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            if (r48 <= 0) goto L_0x020a
            java.lang.Object r48 = r32.pop()     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r48 = (java.lang.String) r48     // Catch:{ RuntimeException -> 0x0201 }
            r31 = r48
        L_0x01cb:
            if (r44 == 0) goto L_0x029f
            java.lang.String r48 = "true"
            boolean r48 = r47.equals(r48)     // Catch:{ RuntimeException -> 0x0201 }
            if (r48 == 0) goto L_0x020d
            if (r25 == 0) goto L_0x01f5
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "boolean: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "=true"
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x01f5:
            r48 = 1
            r0 = r53
            r1 = r31
            r2 = r48
            r0.bool(r1, r2)     // Catch:{ RuntimeException -> 0x0201 }
            goto L_0x01b2
        L_0x0201:
            r28 = move-exception
            r45 = r46
            r35 = r36
        L_0x0206:
            r37 = r28
            goto L_0x003c
        L_0x020a:
            r31 = 0
            goto L_0x01cb
        L_0x020d:
            java.lang.String r48 = "false"
            boolean r48 = r47.equals(r48)     // Catch:{ RuntimeException -> 0x0201 }
            if (r48 == 0) goto L_0x0242
            if (r25 == 0) goto L_0x0235
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "boolean: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "=false"
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x0235:
            r48 = 0
            r0 = r53
            r1 = r31
            r2 = r48
            r0.bool(r1, r2)     // Catch:{ RuntimeException -> 0x0201 }
            goto L_0x01b2
        L_0x0242:
            java.lang.String r48 = "null"
            boolean r48 = r47.equals(r48)     // Catch:{ RuntimeException -> 0x0201 }
            if (r48 == 0) goto L_0x0257
            r48 = 0
            r0 = r53
            r1 = r31
            r2 = r48
            r0.string(r1, r2)     // Catch:{ RuntimeException -> 0x0201 }
            goto L_0x01b2
        L_0x0257:
            r22 = 0
            r23 = 1
            r29 = r40
        L_0x025d:
            r0 = r29
            r1 = r36
            if (r0 < r1) goto L_0x02d2
        L_0x0263:
            if (r22 == 0) goto L_0x02e4
            if (r25 == 0) goto L_0x028d
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ NumberFormatException -> 0x029e }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x029e }
            java.lang.String r50 = "double: "
            r49.<init>(r50)     // Catch:{ NumberFormatException -> 0x029e }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x029e }
            java.lang.String r50 = "="
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ NumberFormatException -> 0x029e }
            double r50 = java.lang.Double.parseDouble(r47)     // Catch:{ NumberFormatException -> 0x029e }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ NumberFormatException -> 0x029e }
            java.lang.String r49 = r49.toString()     // Catch:{ NumberFormatException -> 0x029e }
            r48.println(r49)     // Catch:{ NumberFormatException -> 0x029e }
        L_0x028d:
            double r48 = java.lang.Double.parseDouble(r47)     // Catch:{ NumberFormatException -> 0x029e }
            r0 = r53
            r1 = r31
            r2 = r48
            r4 = r47
            r0.number(r1, r2, r4)     // Catch:{ NumberFormatException -> 0x029e }
            goto L_0x01b2
        L_0x029e:
            r48 = move-exception
        L_0x029f:
            if (r25 == 0) goto L_0x02c7
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "string: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "="
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r47
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x02c7:
            r0 = r53
            r1 = r31
            r2 = r47
            r0.string(r1, r2)     // Catch:{ RuntimeException -> 0x0201 }
            goto L_0x01b2
        L_0x02d2:
            char r48 = r54[r29]     // Catch:{ RuntimeException -> 0x0201 }
            switch(r48) {
                case 43: goto L_0x02e0;
                case 45: goto L_0x02e0;
                case 46: goto L_0x02dc;
                case 48: goto L_0x02e0;
                case 49: goto L_0x02e0;
                case 50: goto L_0x02e0;
                case 51: goto L_0x02e0;
                case 52: goto L_0x02e0;
                case 53: goto L_0x02e0;
                case 54: goto L_0x02e0;
                case 55: goto L_0x02e0;
                case 56: goto L_0x02e0;
                case 57: goto L_0x02e0;
                case 69: goto L_0x02dc;
                case 101: goto L_0x02dc;
                default: goto L_0x02d7;
            }     // Catch:{ RuntimeException -> 0x0201 }
        L_0x02d7:
            r22 = 0
            r23 = 0
            goto L_0x0263
        L_0x02dc:
            r22 = 1
            r23 = 0
        L_0x02e0:
            int r29 = r29 + 1
            goto L_0x025d
        L_0x02e4:
            if (r23 == 0) goto L_0x029f
            if (r25 == 0) goto L_0x030e
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "double: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "="
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0201 }
            double r50 = java.lang.Double.parseDouble(r47)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x030e:
            long r48 = java.lang.Long.parseLong(r47)     // Catch:{ NumberFormatException -> 0x031f }
            r0 = r53
            r1 = r31
            r2 = r48
            r4 = r47
            r0.number(r1, r2, r4)     // Catch:{ NumberFormatException -> 0x031f }
            goto L_0x01b2
        L_0x031f:
            r48 = move-exception
            goto L_0x029f
        L_0x0322:
            r0 = r32
            int r0 = r0.size     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            if (r48 <= 0) goto L_0x0390
            java.lang.Object r48 = r32.pop()     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r48 = (java.lang.String) r48     // Catch:{ RuntimeException -> 0x0201 }
            r31 = r48
        L_0x0332:
            if (r25 == 0) goto L_0x034c
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "startObject: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x034c:
            r0 = r53
            r1 = r31
            r0.startObject(r1)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r41
            int r0 = r0.length     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            r0 = r46
            r1 = r48
            if (r0 != r1) goto L_0x0383
            r0 = r41
            int r0 = r0.length     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            int r48 = r48 * 2
            r0 = r48
            int[] r0 = new int[r0]     // Catch:{ RuntimeException -> 0x0201 }
            r34 = r0
            r48 = 0
            r49 = 0
            r0 = r41
            int r0 = r0.length     // Catch:{ RuntimeException -> 0x0201 }
            r50 = r0
            r0 = r41
            r1 = r48
            r2 = r34
            r3 = r49
            r4 = r50
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ RuntimeException -> 0x0201 }
            r41 = r34
        L_0x0383:
            int r45 = r46 + 1
            r41[r46] = r24     // Catch:{ RuntimeException -> 0x07d7 }
            r24 = 5
            r12 = 2
            r46 = r45
            r35 = r36
            goto L_0x0037
        L_0x0390:
            r31 = 0
            goto L_0x0332
        L_0x0393:
            if (r25 == 0) goto L_0x039c
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = "endObject"
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x039c:
            r53.pop()     // Catch:{ RuntimeException -> 0x0201 }
            int r45 = r46 + -1
            r24 = r41[r45]     // Catch:{ RuntimeException -> 0x07d7 }
            r12 = 2
            r46 = r45
            r35 = r36
            goto L_0x0037
        L_0x03aa:
            r0 = r32
            int r0 = r0.size     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            if (r48 <= 0) goto L_0x0418
            java.lang.Object r48 = r32.pop()     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r48 = (java.lang.String) r48     // Catch:{ RuntimeException -> 0x0201 }
            r31 = r48
        L_0x03ba:
            if (r25 == 0) goto L_0x03d4
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r50 = "startArray: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0201 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x03d4:
            r0 = r53
            r1 = r31
            r0.startArray(r1)     // Catch:{ RuntimeException -> 0x0201 }
            r0 = r41
            int r0 = r0.length     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            r0 = r46
            r1 = r48
            if (r0 != r1) goto L_0x040b
            r0 = r41
            int r0 = r0.length     // Catch:{ RuntimeException -> 0x0201 }
            r48 = r0
            int r48 = r48 * 2
            r0 = r48
            int[] r0 = new int[r0]     // Catch:{ RuntimeException -> 0x0201 }
            r34 = r0
            r48 = 0
            r49 = 0
            r0 = r41
            int r0 = r0.length     // Catch:{ RuntimeException -> 0x0201 }
            r50 = r0
            r0 = r41
            r1 = r48
            r2 = r34
            r3 = r49
            r4 = r50
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ RuntimeException -> 0x0201 }
            r41 = r34
        L_0x040b:
            int r45 = r46 + 1
            r41[r46] = r24     // Catch:{ RuntimeException -> 0x07d7 }
            r24 = 23
            r12 = 2
            r46 = r45
            r35 = r36
            goto L_0x0037
        L_0x0418:
            r31 = 0
            goto L_0x03ba
        L_0x041b:
            if (r25 == 0) goto L_0x0424
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = "endArray"
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x0424:
            r53.pop()     // Catch:{ RuntimeException -> 0x0201 }
            int r45 = r46 + -1
            r24 = r41[r45]     // Catch:{ RuntimeException -> 0x07d7 }
            r12 = 2
            r46 = r45
            r35 = r36
            goto L_0x0037
        L_0x0432:
            int r42 = r36 + -1
            int r35 = r36 + 1
            char r48 = r54[r36]     // Catch:{ RuntimeException -> 0x0648 }
            r49 = 47
            r0 = r48
            r1 = r49
            if (r0 != r1) goto L_0x0483
        L_0x0440:
            r0 = r35
            r1 = r27
            if (r0 == r1) goto L_0x0450
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            r49 = 10
            r0 = r48
            r1 = r49
            if (r0 != r1) goto L_0x047e
        L_0x0450:
            int r35 = r35 + -1
        L_0x0452:
            if (r25 == 0) goto L_0x07e0
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "comment "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = new java.lang.String     // Catch:{ RuntimeException -> 0x0648 }
            int r51 = r35 - r42
            r0 = r50
            r1 = r54
            r2 = r42
            r3 = r51
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
            r18 = r17
            r11 = r10
            r36 = r35
            goto L_0x00fe
        L_0x047e:
            int r35 = r35 + 1
            goto L_0x0440
        L_0x0481:
            int r35 = r35 + 1
        L_0x0483:
            int r48 = r35 + 1
            r0 = r48
            r1 = r27
            if (r0 >= r1) goto L_0x0495
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            r49 = 42
            r0 = r48
            r1 = r49
            if (r0 != r1) goto L_0x0481
        L_0x0495:
            int r48 = r35 + 1
            char r48 = r54[r48]     // Catch:{ RuntimeException -> 0x0648 }
            r49 = 47
            r0 = r48
            r1 = r49
            if (r0 != r1) goto L_0x0481
            int r35 = r35 + 1
            goto L_0x0452
        L_0x04a4:
            if (r25 == 0) goto L_0x04ad
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = "unquotedChars"
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x04ad:
            r40 = r36
            r33 = 0
            r44 = 1
            if (r43 == 0) goto L_0x0515
            r35 = r36
        L_0x04b7:
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            switch(r48) {
                case 10: goto L_0x04e2;
                case 13: goto L_0x04e2;
                case 47: goto L_0x04f8;
                case 58: goto L_0x04e2;
                case 92: goto L_0x04f5;
                default: goto L_0x04bc;
            }     // Catch:{ RuntimeException -> 0x0648 }
        L_0x04bc:
            if (r25 == 0) goto L_0x04da
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "unquotedChar (name): '"
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            char r50 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "'"
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x04da:
            int r35 = r35 + 1
            r0 = r35
            r1 = r27
            if (r0 != r1) goto L_0x04b7
        L_0x04e2:
            int r35 = r35 + -1
        L_0x04e4:
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            r49 = 32
            r0 = r48
            r1 = r49
            if (r0 == r1) goto L_0x0563
            r18 = r17
            r11 = r10
            r36 = r35
            goto L_0x00fe
        L_0x04f5:
            r33 = 1
            goto L_0x04bc
        L_0x04f8:
            int r48 = r35 + 1
            r0 = r48
            r1 = r27
            if (r0 == r1) goto L_0x04bc
            int r48 = r35 + 1
            char r21 = r54[r48]     // Catch:{ RuntimeException -> 0x0648 }
            r48 = 47
            r0 = r21
            r1 = r48
            if (r0 == r1) goto L_0x04e2
            r48 = 42
            r0 = r21
            r1 = r48
            if (r0 != r1) goto L_0x04bc
            goto L_0x04e2
        L_0x0515:
            r35 = r36
        L_0x0517:
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            switch(r48) {
                case 10: goto L_0x04e2;
                case 13: goto L_0x04e2;
                case 44: goto L_0x04e2;
                case 47: goto L_0x0546;
                case 92: goto L_0x0543;
                case 93: goto L_0x04e2;
                case 125: goto L_0x04e2;
                default: goto L_0x051c;
            }     // Catch:{ RuntimeException -> 0x0648 }
        L_0x051c:
            if (r25 == 0) goto L_0x053a
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "unquotedChar (value): '"
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            char r50 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "'"
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x053a:
            int r35 = r35 + 1
            r0 = r35
            r1 = r27
            if (r0 != r1) goto L_0x0517
            goto L_0x04e2
        L_0x0543:
            r33 = 1
            goto L_0x051c
        L_0x0546:
            int r48 = r35 + 1
            r0 = r48
            r1 = r27
            if (r0 == r1) goto L_0x051c
            int r48 = r35 + 1
            char r21 = r54[r48]     // Catch:{ RuntimeException -> 0x0648 }
            r48 = 47
            r0 = r21
            r1 = r48
            if (r0 == r1) goto L_0x04e2
            r48 = 42
            r0 = r21
            r1 = r48
            if (r0 != r1) goto L_0x051c
            goto L_0x04e2
        L_0x0563:
            int r35 = r35 + -1
            goto L_0x04e4
        L_0x0567:
            if (r25 == 0) goto L_0x0570
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0201 }
            java.lang.String r49 = "quotedChars"
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0201 }
        L_0x0570:
            int r35 = r36 + 1
            r40 = r35
            r33 = 0
        L_0x0576:
            char r48 = r54[r35]     // Catch:{ RuntimeException -> 0x0648 }
            switch(r48) {
                case 34: goto L_0x0583;
                case 92: goto L_0x058c;
                default: goto L_0x057b;
            }     // Catch:{ RuntimeException -> 0x0648 }
        L_0x057b:
            int r35 = r35 + 1
            r0 = r35
            r1 = r27
            if (r0 != r1) goto L_0x0576
        L_0x0583:
            int r35 = r35 + -1
            r18 = r17
            r11 = r10
            r36 = r35
            goto L_0x00fe
        L_0x058c:
            r33 = 1
            int r35 = r35 + 1
            goto L_0x057b
        L_0x0591:
            int r35 = r35 + 1
            r0 = r35
            r1 = r38
            if (r0 == r1) goto L_0x059c
            r12 = 1
            goto L_0x0037
        L_0x059c:
            r0 = r35
            r1 = r27
            if (r0 != r1) goto L_0x07dc
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_eof_actions     // Catch:{ RuntimeException -> 0x0648 }
            byte r6 = r48[r24]     // Catch:{ RuntimeException -> 0x0648 }
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_actions     // Catch:{ RuntimeException -> 0x0648 }
            int r7 = r6 + 1
            byte r8 = r48[r6]     // Catch:{ RuntimeException -> 0x0648 }
            r9 = r8
        L_0x05ad:
            int r8 = r9 + -1
            if (r9 <= 0) goto L_0x003a
            byte[] r48 = com.badlogic.gdx.utils.JsonReader._json_actions     // Catch:{ RuntimeException -> 0x0648 }
            int r6 = r7 + 1
            byte r48 = r48[r7]     // Catch:{ RuntimeException -> 0x0648 }
            switch(r48) {
                case 1: goto L_0x05bd;
                default: goto L_0x05ba;
            }     // Catch:{ RuntimeException -> 0x0648 }
        L_0x05ba:
            r9 = r8
            r7 = r6
            goto L_0x05ad
        L_0x05bd:
            java.lang.String r47 = new java.lang.String     // Catch:{ RuntimeException -> 0x0648 }
            int r48 = r35 - r40
            r0 = r47
            r1 = r54
            r2 = r40
            r3 = r48
            r0.<init>(r1, r2, r3)     // Catch:{ RuntimeException -> 0x0648 }
            if (r33 == 0) goto L_0x05d6
            r0 = r53
            r1 = r47
            java.lang.String r47 = r0.unescape(r1)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x05d6:
            if (r43 == 0) goto L_0x0602
            r43 = 0
            if (r25 == 0) goto L_0x05f4
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "name: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r49
            r1 = r47
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x05f4:
            r0 = r32
            r1 = r47
            r0.add(r1)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x05fb:
            r44 = 0
            r40 = r35
            r9 = r8
            r7 = r6
            goto L_0x05ad
        L_0x0602:
            r0 = r32
            int r0 = r0.size     // Catch:{ RuntimeException -> 0x0648 }
            r48 = r0
            if (r48 <= 0) goto L_0x064d
            java.lang.Object r48 = r32.pop()     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r48 = (java.lang.String) r48     // Catch:{ RuntimeException -> 0x0648 }
            r31 = r48
        L_0x0612:
            if (r44 == 0) goto L_0x06e2
            java.lang.String r48 = "true"
            boolean r48 = r47.equals(r48)     // Catch:{ RuntimeException -> 0x0648 }
            if (r48 == 0) goto L_0x0650
            if (r25 == 0) goto L_0x063c
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "boolean: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "=true"
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x063c:
            r48 = 1
            r0 = r53
            r1 = r31
            r2 = r48
            r0.bool(r1, r2)     // Catch:{ RuntimeException -> 0x0648 }
            goto L_0x05fb
        L_0x0648:
            r28 = move-exception
            r45 = r46
            goto L_0x0206
        L_0x064d:
            r31 = 0
            goto L_0x0612
        L_0x0650:
            java.lang.String r48 = "false"
            boolean r48 = r47.equals(r48)     // Catch:{ RuntimeException -> 0x0648 }
            if (r48 == 0) goto L_0x0685
            if (r25 == 0) goto L_0x0678
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "boolean: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "=false"
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x0678:
            r48 = 0
            r0 = r53
            r1 = r31
            r2 = r48
            r0.bool(r1, r2)     // Catch:{ RuntimeException -> 0x0648 }
            goto L_0x05fb
        L_0x0685:
            java.lang.String r48 = "null"
            boolean r48 = r47.equals(r48)     // Catch:{ RuntimeException -> 0x0648 }
            if (r48 == 0) goto L_0x069a
            r48 = 0
            r0 = r53
            r1 = r31
            r2 = r48
            r0.string(r1, r2)     // Catch:{ RuntimeException -> 0x0648 }
            goto L_0x05fb
        L_0x069a:
            r22 = 0
            r23 = 1
            r29 = r40
        L_0x06a0:
            r0 = r29
            r1 = r35
            if (r0 < r1) goto L_0x0715
        L_0x06a6:
            if (r22 == 0) goto L_0x0727
            if (r25 == 0) goto L_0x06d0
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ NumberFormatException -> 0x06e1 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x06e1 }
            java.lang.String r50 = "double: "
            r49.<init>(r50)     // Catch:{ NumberFormatException -> 0x06e1 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ NumberFormatException -> 0x06e1 }
            java.lang.String r50 = "="
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ NumberFormatException -> 0x06e1 }
            double r50 = java.lang.Double.parseDouble(r47)     // Catch:{ NumberFormatException -> 0x06e1 }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ NumberFormatException -> 0x06e1 }
            java.lang.String r49 = r49.toString()     // Catch:{ NumberFormatException -> 0x06e1 }
            r48.println(r49)     // Catch:{ NumberFormatException -> 0x06e1 }
        L_0x06d0:
            double r48 = java.lang.Double.parseDouble(r47)     // Catch:{ NumberFormatException -> 0x06e1 }
            r0 = r53
            r1 = r31
            r2 = r48
            r4 = r47
            r0.number(r1, r2, r4)     // Catch:{ NumberFormatException -> 0x06e1 }
            goto L_0x05fb
        L_0x06e1:
            r48 = move-exception
        L_0x06e2:
            if (r25 == 0) goto L_0x070a
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "string: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "="
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r49
            r1 = r47
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x070a:
            r0 = r53
            r1 = r31
            r2 = r47
            r0.string(r1, r2)     // Catch:{ RuntimeException -> 0x0648 }
            goto L_0x05fb
        L_0x0715:
            char r48 = r54[r29]     // Catch:{ RuntimeException -> 0x0648 }
            switch(r48) {
                case 43: goto L_0x0723;
                case 45: goto L_0x0723;
                case 46: goto L_0x071f;
                case 48: goto L_0x0723;
                case 49: goto L_0x0723;
                case 50: goto L_0x0723;
                case 51: goto L_0x0723;
                case 52: goto L_0x0723;
                case 53: goto L_0x0723;
                case 54: goto L_0x0723;
                case 55: goto L_0x0723;
                case 56: goto L_0x0723;
                case 57: goto L_0x0723;
                case 69: goto L_0x071f;
                case 101: goto L_0x071f;
                default: goto L_0x071a;
            }     // Catch:{ RuntimeException -> 0x0648 }
        L_0x071a:
            r22 = 0
            r23 = 0
            goto L_0x06a6
        L_0x071f:
            r22 = 1
            r23 = 0
        L_0x0723:
            int r29 = r29 + 1
            goto L_0x06a0
        L_0x0727:
            if (r23 == 0) goto L_0x06e2
            if (r25 == 0) goto L_0x0751
            java.io.PrintStream r48 = java.lang.System.out     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "double: "
            r49.<init>(r50)     // Catch:{ RuntimeException -> 0x0648 }
            r0 = r49
            r1 = r31
            java.lang.StringBuilder r49 = r0.append(r1)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r50 = "="
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            double r50 = java.lang.Double.parseDouble(r47)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.StringBuilder r49 = r49.append(r50)     // Catch:{ RuntimeException -> 0x0648 }
            java.lang.String r49 = r49.toString()     // Catch:{ RuntimeException -> 0x0648 }
            r48.println(r49)     // Catch:{ RuntimeException -> 0x0648 }
        L_0x0751:
            long r48 = java.lang.Long.parseLong(r47)     // Catch:{ NumberFormatException -> 0x0762 }
            r0 = r53
            r1 = r31
            r2 = r48
            r4 = r47
            r0.number(r1, r2, r4)     // Catch:{ NumberFormatException -> 0x0762 }
            goto L_0x05fb
        L_0x0762:
            r48 = move-exception
            goto L_0x06e2
        L_0x0765:
            char r48 = r54[r29]
            r49 = 10
            r0 = r48
            r1 = r49
            if (r0 != r1) goto L_0x0771
            int r30 = r30 + 1
        L_0x0771:
            int r29 = r29 + 1
            goto L_0x0065
        L_0x0775:
            r0 = r53
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.JsonValue> r0 = r0.elements
            r48 = r0
            r0 = r48
            int r0 = r0.size
            r48 = r0
            if (r48 == 0) goto L_0x07b0
            r0 = r53
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.JsonValue> r0 = r0.elements
            r48 = r0
            java.lang.Object r26 = r48.peek()
            com.badlogic.gdx.utils.JsonValue r26 = (com.badlogic.gdx.utils.JsonValue) r26
            r0 = r53
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.JsonValue> r0 = r0.elements
            r48 = r0
            r48.clear()
            if (r26 == 0) goto L_0x07a8
            boolean r48 = r26.isObject()
            if (r48 == 0) goto L_0x07a8
            com.badlogic.gdx.utils.SerializationException r48 = new com.badlogic.gdx.utils.SerializationException
            java.lang.String r49 = "Error parsing JSON, unmatched brace."
            r48.<init>(r49)
            throw r48
        L_0x07a8:
            com.badlogic.gdx.utils.SerializationException r48 = new com.badlogic.gdx.utils.SerializationException
            java.lang.String r49 = "Error parsing JSON, unmatched bracket."
            r48.<init>(r49)
            throw r48
        L_0x07b0:
            if (r37 == 0) goto L_0x07d6
            com.badlogic.gdx.utils.SerializationException r48 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r49 = new java.lang.StringBuilder
            java.lang.String r50 = "Error parsing JSON: "
            r49.<init>(r50)
            java.lang.String r50 = new java.lang.String
            r0 = r50
            r1 = r54
            r0.<init>(r1)
            java.lang.StringBuilder r49 = r49.append(r50)
            java.lang.String r49 = r49.toString()
            r0 = r48
            r1 = r49
            r2 = r37
            r0.<init>(r1, r2)
            throw r48
        L_0x07d6:
            return r39
        L_0x07d7:
            r28 = move-exception
            r35 = r36
            goto L_0x0206
        L_0x07dc:
            r45 = r46
            goto L_0x003c
        L_0x07e0:
            r18 = r17
            r11 = r10
            r36 = r35
            goto L_0x00fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.JsonReader.parse(char[], int, int):com.badlogic.gdx.utils.JsonValue");
    }

    private static byte[] init__json_actions_0() {
        byte[] bArr = new byte[29];
        bArr[1] = 1;
        bArr[2] = 1;
        bArr[3] = 1;
        bArr[4] = 2;
        bArr[5] = 1;
        bArr[6] = 3;
        bArr[7] = 1;
        bArr[8] = 4;
        bArr[9] = 1;
        bArr[10] = 5;
        bArr[11] = 1;
        bArr[12] = 6;
        bArr[13] = 1;
        bArr[14] = 7;
        bArr[15] = 1;
        bArr[16] = 8;
        bArr[17] = 2;
        bArr[19] = 7;
        bArr[20] = 2;
        bArr[22] = 8;
        bArr[23] = 2;
        bArr[24] = 1;
        bArr[25] = 3;
        bArr[26] = 2;
        bArr[27] = 1;
        bArr[28] = 5;
        return bArr;
    }

    private static short[] init__json_key_offsets_0() {
        short[] sArr = new short[39];
        sArr[2] = 11;
        sArr[3] = 13;
        sArr[4] = 14;
        sArr[5] = 16;
        sArr[6] = 25;
        sArr[7] = 31;
        sArr[8] = 37;
        sArr[9] = 39;
        sArr[10] = 50;
        sArr[11] = 57;
        sArr[12] = 64;
        sArr[13] = 73;
        sArr[14] = 74;
        sArr[15] = 83;
        sArr[16] = 85;
        sArr[17] = 87;
        sArr[18] = 96;
        sArr[19] = 98;
        sArr[20] = 100;
        sArr[21] = 101;
        sArr[22] = 103;
        sArr[23] = 105;
        sArr[24] = 116;
        sArr[25] = 123;
        sArr[26] = 130;
        sArr[27] = 141;
        sArr[28] = 142;
        sArr[29] = 153;
        sArr[30] = 155;
        sArr[31] = 157;
        sArr[32] = 168;
        sArr[33] = 170;
        sArr[34] = 172;
        sArr[35] = 174;
        sArr[36] = 179;
        sArr[37] = 184;
        sArr[38] = 184;
        return sArr;
    }

    private static char[] init__json_trans_keys_0() {
        char[] cArr = new char[185];
        cArr[0] = 13;
        cArr[1] = ' ';
        cArr[2] = '\"';
        cArr[3] = ',';
        cArr[4] = '/';
        cArr[5] = ':';
        cArr[6] = '[';
        cArr[7] = ']';
        cArr[8] = '{';
        cArr[9] = 9;
        cArr[10] = 10;
        cArr[11] = '*';
        cArr[12] = '/';
        cArr[13] = '\"';
        cArr[14] = '*';
        cArr[15] = '/';
        cArr[16] = 13;
        cArr[17] = ' ';
        cArr[18] = '\"';
        cArr[19] = ',';
        cArr[20] = '/';
        cArr[21] = ':';
        cArr[22] = '}';
        cArr[23] = 9;
        cArr[24] = 10;
        cArr[25] = 13;
        cArr[26] = ' ';
        cArr[27] = '/';
        cArr[28] = ':';
        cArr[29] = 9;
        cArr[30] = 10;
        cArr[31] = 13;
        cArr[32] = ' ';
        cArr[33] = '/';
        cArr[34] = ':';
        cArr[35] = 9;
        cArr[36] = 10;
        cArr[37] = '*';
        cArr[38] = '/';
        cArr[39] = 13;
        cArr[40] = ' ';
        cArr[41] = '\"';
        cArr[42] = ',';
        cArr[43] = '/';
        cArr[44] = ':';
        cArr[45] = '[';
        cArr[46] = ']';
        cArr[47] = '{';
        cArr[48] = 9;
        cArr[49] = 10;
        cArr[50] = 9;
        cArr[51] = 10;
        cArr[52] = 13;
        cArr[53] = ' ';
        cArr[54] = ',';
        cArr[55] = '/';
        cArr[56] = '}';
        cArr[57] = 9;
        cArr[58] = 10;
        cArr[59] = 13;
        cArr[60] = ' ';
        cArr[61] = ',';
        cArr[62] = '/';
        cArr[63] = '}';
        cArr[64] = 13;
        cArr[65] = ' ';
        cArr[66] = '\"';
        cArr[67] = ',';
        cArr[68] = '/';
        cArr[69] = ':';
        cArr[70] = '}';
        cArr[71] = 9;
        cArr[72] = 10;
        cArr[73] = '\"';
        cArr[74] = 13;
        cArr[75] = ' ';
        cArr[76] = '\"';
        cArr[77] = ',';
        cArr[78] = '/';
        cArr[79] = ':';
        cArr[80] = '}';
        cArr[81] = 9;
        cArr[82] = 10;
        cArr[83] = '*';
        cArr[84] = '/';
        cArr[85] = '*';
        cArr[86] = '/';
        cArr[87] = 13;
        cArr[88] = ' ';
        cArr[89] = '\"';
        cArr[90] = ',';
        cArr[91] = '/';
        cArr[92] = ':';
        cArr[93] = '}';
        cArr[94] = 9;
        cArr[95] = 10;
        cArr[96] = '*';
        cArr[97] = '/';
        cArr[98] = '*';
        cArr[99] = '/';
        cArr[100] = '\"';
        cArr[101] = '*';
        cArr[102] = '/';
        cArr[103] = '*';
        cArr[104] = '/';
        cArr[105] = 13;
        cArr[106] = ' ';
        cArr[107] = '\"';
        cArr[108] = ',';
        cArr[109] = '/';
        cArr[110] = ':';
        cArr[111] = '[';
        cArr[112] = ']';
        cArr[113] = '{';
        cArr[114] = 9;
        cArr[115] = 10;
        cArr[116] = 9;
        cArr[117] = 10;
        cArr[118] = 13;
        cArr[119] = ' ';
        cArr[120] = ',';
        cArr[121] = '/';
        cArr[122] = ']';
        cArr[123] = 9;
        cArr[124] = 10;
        cArr[125] = 13;
        cArr[126] = ' ';
        cArr[127] = ',';
        cArr[128] = '/';
        cArr[129] = ']';
        cArr[130] = 13;
        cArr[131] = ' ';
        cArr[132] = '\"';
        cArr[133] = ',';
        cArr[134] = '/';
        cArr[135] = ':';
        cArr[136] = '[';
        cArr[137] = ']';
        cArr[138] = '{';
        cArr[139] = 9;
        cArr[140] = 10;
        cArr[141] = '\"';
        cArr[142] = 13;
        cArr[143] = ' ';
        cArr[144] = '\"';
        cArr[145] = ',';
        cArr[146] = '/';
        cArr[147] = ':';
        cArr[148] = '[';
        cArr[149] = ']';
        cArr[150] = '{';
        cArr[151] = 9;
        cArr[152] = 10;
        cArr[153] = '*';
        cArr[154] = '/';
        cArr[155] = '*';
        cArr[156] = '/';
        cArr[157] = 13;
        cArr[158] = ' ';
        cArr[159] = '\"';
        cArr[160] = ',';
        cArr[161] = '/';
        cArr[162] = ':';
        cArr[163] = '[';
        cArr[164] = ']';
        cArr[165] = '{';
        cArr[166] = 9;
        cArr[167] = 10;
        cArr[168] = '*';
        cArr[169] = '/';
        cArr[170] = '*';
        cArr[171] = '/';
        cArr[172] = '*';
        cArr[173] = '/';
        cArr[174] = 13;
        cArr[175] = ' ';
        cArr[176] = '/';
        cArr[177] = 9;
        cArr[178] = 10;
        cArr[179] = 13;
        cArr[180] = ' ';
        cArr[181] = '/';
        cArr[182] = 9;
        cArr[183] = 10;
        return cArr;
    }

    private static byte[] init__json_single_lengths_0() {
        byte[] bArr = new byte[39];
        bArr[1] = 9;
        bArr[2] = 2;
        bArr[3] = 1;
        bArr[4] = 2;
        bArr[5] = 7;
        bArr[6] = 4;
        bArr[7] = 4;
        bArr[8] = 2;
        bArr[9] = 9;
        bArr[10] = 7;
        bArr[11] = 7;
        bArr[12] = 7;
        bArr[13] = 1;
        bArr[14] = 7;
        bArr[15] = 2;
        bArr[16] = 2;
        bArr[17] = 7;
        bArr[18] = 2;
        bArr[19] = 2;
        bArr[20] = 1;
        bArr[21] = 2;
        bArr[22] = 2;
        bArr[23] = 9;
        bArr[24] = 7;
        bArr[25] = 7;
        bArr[26] = 9;
        bArr[27] = 1;
        bArr[28] = 9;
        bArr[29] = 2;
        bArr[30] = 2;
        bArr[31] = 9;
        bArr[32] = 2;
        bArr[33] = 2;
        bArr[34] = 2;
        bArr[35] = 3;
        bArr[36] = 3;
        return bArr;
    }

    private static byte[] init__json_range_lengths_0() {
        byte[] bArr = new byte[39];
        bArr[1] = 1;
        bArr[5] = 1;
        bArr[6] = 1;
        bArr[7] = 1;
        bArr[9] = 1;
        bArr[12] = 1;
        bArr[14] = 1;
        bArr[17] = 1;
        bArr[23] = 1;
        bArr[26] = 1;
        bArr[28] = 1;
        bArr[31] = 1;
        bArr[35] = 1;
        bArr[36] = 1;
        return bArr;
    }

    private static short[] init__json_index_offsets_0() {
        short[] sArr = new short[39];
        sArr[2] = 11;
        sArr[3] = 14;
        sArr[4] = 16;
        sArr[5] = 19;
        sArr[6] = 28;
        sArr[7] = 34;
        sArr[8] = 40;
        sArr[9] = 43;
        sArr[10] = 54;
        sArr[11] = 62;
        sArr[12] = 70;
        sArr[13] = 79;
        sArr[14] = 81;
        sArr[15] = 90;
        sArr[16] = 93;
        sArr[17] = 96;
        sArr[18] = 105;
        sArr[19] = 108;
        sArr[20] = 111;
        sArr[21] = 113;
        sArr[22] = 116;
        sArr[23] = 119;
        sArr[24] = 130;
        sArr[25] = 138;
        sArr[26] = 146;
        sArr[27] = 157;
        sArr[28] = 159;
        sArr[29] = 170;
        sArr[30] = 173;
        sArr[31] = 176;
        sArr[32] = 187;
        sArr[33] = 190;
        sArr[34] = 193;
        sArr[35] = 196;
        sArr[36] = 201;
        sArr[37] = 206;
        sArr[38] = 207;
        return sArr;
    }

    private static byte[] init__json_indicies_0() {
        byte[] bArr = new byte[PAK_ASSETS.IMG_HUOGUO_SHUOMING];
        bArr[0] = 1;
        bArr[1] = 1;
        bArr[2] = 2;
        bArr[3] = 3;
        bArr[4] = 4;
        bArr[5] = 3;
        bArr[6] = 5;
        bArr[7] = 3;
        bArr[8] = 6;
        bArr[9] = 1;
        bArr[11] = 7;
        bArr[12] = 7;
        bArr[13] = 3;
        bArr[14] = 8;
        bArr[15] = 3;
        bArr[16] = 9;
        bArr[17] = 9;
        bArr[18] = 3;
        bArr[19] = 11;
        bArr[20] = 11;
        bArr[21] = 12;
        bArr[22] = 13;
        bArr[23] = 14;
        bArr[24] = 3;
        bArr[25] = 15;
        bArr[26] = 11;
        bArr[27] = 10;
        bArr[28] = 16;
        bArr[29] = 16;
        bArr[30] = 17;
        bArr[31] = 18;
        bArr[32] = 16;
        bArr[33] = 3;
        bArr[34] = 19;
        bArr[35] = 19;
        bArr[36] = 20;
        bArr[37] = 21;
        bArr[38] = 19;
        bArr[39] = 3;
        bArr[40] = 22;
        bArr[41] = 22;
        bArr[42] = 3;
        bArr[43] = 21;
        bArr[44] = 21;
        bArr[45] = 24;
        bArr[46] = 3;
        bArr[47] = 25;
        bArr[48] = 3;
        bArr[49] = 26;
        bArr[50] = 3;
        bArr[51] = 27;
        bArr[52] = 21;
        bArr[53] = 23;
        bArr[54] = 28;
        bArr[55] = 29;
        bArr[56] = 28;
        bArr[57] = 28;
        bArr[58] = 30;
        bArr[59] = 31;
        bArr[60] = 32;
        bArr[61] = 3;
        bArr[62] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[63] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[64] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[65] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[66] = 13;
        bArr[67] = 35;
        bArr[68] = 15;
        bArr[69] = 3;
        bArr[70] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[71] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[72] = 12;
        bArr[73] = 36;
        bArr[74] = 37;
        bArr[75] = 3;
        bArr[76] = 15;
        bArr[77] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[78] = 10;
        bArr[79] = 16;
        bArr[80] = 3;
        bArr[81] = 36;
        bArr[82] = 36;
        bArr[83] = 12;
        bArr[84] = 3;
        bArr[85] = 38;
        bArr[86] = 3;
        bArr[87] = 3;
        bArr[88] = 36;
        bArr[89] = 10;
        bArr[90] = 39;
        bArr[91] = 39;
        bArr[92] = 3;
        bArr[93] = 40;
        bArr[94] = 40;
        bArr[95] = 3;
        bArr[96] = 13;
        bArr[97] = 13;
        bArr[98] = 12;
        bArr[99] = 3;
        bArr[100] = GameSpriteType.f28TYPE_ENEMY_2;
        bArr[101] = 3;
        bArr[102] = 15;
        bArr[103] = 13;
        bArr[104] = 10;
        bArr[105] = 42;
        bArr[106] = 42;
        bArr[107] = 3;
        bArr[108] = 43;
        bArr[109] = 43;
        bArr[110] = 3;
        bArr[111] = 28;
        bArr[112] = 3;
        bArr[113] = 44;
        bArr[114] = 44;
        bArr[115] = 3;
        bArr[116] = 45;
        bArr[117] = 45;
        bArr[118] = 3;
        bArr[119] = GameSpriteType.f2TYPE_BOSS_1;
        bArr[120] = GameSpriteType.f2TYPE_BOSS_1;
        bArr[121] = GameSpriteType.f1TYPE_BOSS_;
        bArr[122] = GameSpriteType.f0TYPE_BOSS_;
        bArr[123] = GameSpriteType.f3TYPE_BOSS_2;
        bArr[124] = 3;
        bArr[125] = 51;
        bArr[126] = GameSpriteType.f56TYPE_ENEMY_;
        bArr[127] = 53;
        bArr[128] = GameSpriteType.f2TYPE_BOSS_1;
        bArr[129] = 46;
        bArr[130] = 54;
        bArr[131] = 55;
        bArr[132] = 54;
        bArr[133] = 54;
        bArr[134] = 56;
        bArr[135] = 57;
        bArr[136] = 58;
        bArr[137] = 3;
        bArr[138] = 59;
        bArr[139] = 60;
        bArr[140] = 59;
        bArr[141] = 59;
        bArr[142] = GameSpriteType.f0TYPE_BOSS_;
        bArr[143] = 61;
        bArr[144] = GameSpriteType.f56TYPE_ENEMY_;
        bArr[145] = 3;
        bArr[146] = 60;
        bArr[147] = 60;
        bArr[148] = GameSpriteType.f1TYPE_BOSS_;
        bArr[149] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[150] = GameSpriteType.f29TYPE_ENEMY_;
        bArr[151] = 3;
        bArr[152] = 51;
        bArr[153] = GameSpriteType.f56TYPE_ENEMY_;
        bArr[154] = 53;
        bArr[155] = 60;
        bArr[156] = 46;
        bArr[157] = 54;
        bArr[158] = 3;
        bArr[159] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[160] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[161] = GameSpriteType.f1TYPE_BOSS_;
        bArr[162] = 3;
        bArr[163] = GameSpriteType.f4TYPE_ENEMY_;
        bArr[164] = 3;
        bArr[165] = 51;
        bArr[166] = 3;
        bArr[167] = 53;
        bArr[168] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[169] = 46;
        bArr[170] = 65;
        bArr[171] = 65;
        bArr[172] = 3;
        bArr[173] = 66;
        bArr[174] = 66;
        bArr[175] = 3;
        bArr[176] = GameSpriteType.f0TYPE_BOSS_;
        bArr[177] = GameSpriteType.f0TYPE_BOSS_;
        bArr[178] = GameSpriteType.f1TYPE_BOSS_;
        bArr[179] = 3;
        bArr[180] = 67;
        bArr[181] = 3;
        bArr[182] = 51;
        bArr[183] = GameSpriteType.f56TYPE_ENEMY_;
        bArr[184] = 53;
        bArr[185] = GameSpriteType.f0TYPE_BOSS_;
        bArr[186] = 46;
        bArr[187] = 68;
        bArr[188] = 68;
        bArr[189] = 3;
        bArr[190] = 69;
        bArr[191] = 69;
        bArr[192] = 3;
        bArr[193] = 70;
        bArr[194] = 70;
        bArr[195] = 3;
        bArr[196] = 8;
        bArr[197] = 8;
        bArr[198] = 71;
        bArr[199] = 8;
        bArr[200] = 3;
        bArr[201] = 72;
        bArr[202] = 72;
        bArr[203] = 73;
        bArr[204] = 72;
        bArr[205] = 3;
        bArr[206] = 3;
        bArr[207] = 3;
        return bArr;
    }

    private static byte[] init__json_trans_targs_0() {
        byte[] bArr = new byte[74];
        bArr[0] = 35;
        bArr[1] = 1;
        bArr[2] = 3;
        bArr[4] = 4;
        bArr[5] = 36;
        bArr[6] = 36;
        bArr[7] = 36;
        bArr[8] = 36;
        bArr[9] = 1;
        bArr[10] = 6;
        bArr[11] = 5;
        bArr[12] = 13;
        bArr[13] = 17;
        bArr[14] = 22;
        bArr[15] = 37;
        bArr[16] = 7;
        bArr[17] = 8;
        bArr[18] = 9;
        bArr[19] = 7;
        bArr[20] = 8;
        bArr[21] = 9;
        bArr[22] = 7;
        bArr[23] = 10;
        bArr[24] = 20;
        bArr[25] = 21;
        bArr[26] = 11;
        bArr[27] = 11;
        bArr[28] = 11;
        bArr[29] = 12;
        bArr[30] = 17;
        bArr[31] = 19;
        bArr[32] = 37;
        bArr[33] = 11;
        bArr[34] = 12;
        bArr[35] = 19;
        bArr[36] = 14;
        bArr[37] = 16;
        bArr[38] = 15;
        bArr[39] = 14;
        bArr[40] = 12;
        bArr[41] = 18;
        bArr[42] = 17;
        bArr[43] = 11;
        bArr[44] = 9;
        bArr[45] = 5;
        bArr[46] = 24;
        bArr[47] = 23;
        bArr[48] = 27;
        bArr[49] = 31;
        bArr[50] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[51] = 25;
        bArr[52] = 38;
        bArr[53] = 25;
        bArr[54] = 25;
        bArr[55] = 26;
        bArr[56] = 31;
        bArr[57] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[58] = 38;
        bArr[59] = 25;
        bArr[60] = 26;
        bArr[61] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[62] = 28;
        bArr[63] = 30;
        bArr[64] = 29;
        bArr[65] = 28;
        bArr[66] = 26;
        bArr[67] = 32;
        bArr[68] = 31;
        bArr[69] = 25;
        bArr[70] = 23;
        bArr[71] = 2;
        bArr[72] = 36;
        bArr[73] = 2;
        return bArr;
    }

    private static byte[] init__json_trans_actions_0() {
        byte[] bArr = new byte[74];
        bArr[0] = 13;
        bArr[2] = 15;
        bArr[5] = 7;
        bArr[6] = 3;
        bArr[7] = 11;
        bArr[8] = 1;
        bArr[9] = 11;
        bArr[10] = 17;
        bArr[12] = 20;
        bArr[15] = 5;
        bArr[16] = 1;
        bArr[17] = 1;
        bArr[18] = 1;
        bArr[22] = 11;
        bArr[23] = 13;
        bArr[24] = 15;
        bArr[26] = 7;
        bArr[27] = 3;
        bArr[28] = 1;
        bArr[29] = 1;
        bArr[30] = 1;
        bArr[31] = 1;
        bArr[32] = 23;
        bArr[39] = 11;
        bArr[40] = 11;
        bArr[42] = 11;
        bArr[43] = 11;
        bArr[44] = 11;
        bArr[45] = 11;
        bArr[46] = 13;
        bArr[48] = 15;
        bArr[51] = 7;
        bArr[52] = 9;
        bArr[53] = 3;
        bArr[54] = 1;
        bArr[55] = 1;
        bArr[56] = 1;
        bArr[57] = 1;
        bArr[58] = 26;
        bArr[65] = 11;
        bArr[66] = 11;
        bArr[68] = 11;
        bArr[69] = 11;
        bArr[70] = 11;
        bArr[71] = 1;
        return bArr;
    }

    private static byte[] init__json_eof_actions_0() {
        byte[] bArr = new byte[39];
        bArr[35] = 1;
        return bArr;
    }

    private void addChild(String name, JsonValue child) {
        child.setName(name);
        if (this.current == null) {
            this.current = child;
            this.root = child;
        } else if (this.current.isArray() || this.current.isObject()) {
            if (this.current.size == 0) {
                this.current.child = child;
            } else {
                JsonValue last = this.lastChild.pop();
                last.next = child;
                child.prev = last;
            }
            this.lastChild.add(child);
            this.current.size++;
        } else {
            this.root = this.current;
        }
    }

    /* access modifiers changed from: protected */
    public void startObject(String name) {
        JsonValue value = new JsonValue(JsonValue.ValueType.object);
        if (this.current != null) {
            addChild(name, value);
        }
        this.elements.add(value);
        this.current = value;
    }

    /* access modifiers changed from: protected */
    public void startArray(String name) {
        JsonValue value = new JsonValue(JsonValue.ValueType.array);
        if (this.current != null) {
            addChild(name, value);
        }
        this.elements.add(value);
        this.current = value;
    }

    /* access modifiers changed from: protected */
    public void pop() {
        this.root = this.elements.pop();
        if (this.current.size > 0) {
            this.lastChild.pop();
        }
        this.current = this.elements.size > 0 ? this.elements.peek() : null;
    }

    /* access modifiers changed from: protected */
    public void string(String name, String value) {
        addChild(name, new JsonValue(value));
    }

    /* access modifiers changed from: protected */
    public void number(String name, double value, String stringValue) {
        addChild(name, new JsonValue(value, stringValue));
    }

    /* access modifiers changed from: protected */
    public void number(String name, long value, String stringValue) {
        addChild(name, new JsonValue(value, stringValue));
    }

    /* access modifiers changed from: protected */
    public void bool(String name, boolean value) {
        addChild(name, new JsonValue(value));
    }

    private String unescape(String value) {
        int length = value.length();
        StringBuilder buffer = new StringBuilder(length + 16);
        int i = 0;
        while (true) {
            if (i < length) {
                int i2 = i + 1;
                char c = value.charAt(i);
                if (c != '\\') {
                    buffer.append(c);
                    i = i2;
                } else if (i2 != length) {
                    i = i2 + 1;
                    char c2 = value.charAt(i2);
                    if (c2 == 'u') {
                        buffer.append(Character.toChars(Integer.parseInt(value.substring(i, i + 4), 16)));
                        i += 4;
                    } else {
                        switch (c2) {
                            case '\"':
                            case '/':
                            case '\\':
                                break;
                            case 'b':
                                c2 = 8;
                                break;
                            case 'f':
                                c2 = 12;
                                break;
                            case 'n':
                                c2 = 10;
                                break;
                            case 'r':
                                c2 = 13;
                                break;
                            case 't':
                                c2 = 9;
                                break;
                            default:
                                throw new SerializationException("Illegal escaped character: \\" + c2);
                        }
                        buffer.append(c2);
                    }
                }
            }
        }
        return buffer.toString();
    }
}
