package com.badlogic.gdx.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.OrderedMap;
import com.badlogic.gdx.utils.reflect.ArrayReflection;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Constructor;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Json {
    private static final boolean debug = false;
    private final ObjectMap<Class, Object[]> classToDefaultValues;
    private final ObjectMap<Class, Serializer> classToSerializer;
    private final ObjectMap<Class, String> classToTag;
    private Serializer defaultSerializer;
    private boolean enumNames;
    private final Object[] equals1;
    private final Object[] equals2;
    private boolean ignoreUnknownFields;
    private JsonWriter.OutputType outputType;
    private boolean quoteLongValues;
    private final ObjectMap<String, Class> tagToClass;
    private String typeName;
    private final ObjectMap<Class, OrderedMap<String, FieldMetadata>> typeToFields;
    private boolean usePrototypes;
    private JsonWriter writer;

    public interface Serializable {
        void read(Json json, JsonValue jsonValue);

        void write(Json json);
    }

    public interface Serializer<T> {
        T read(Json json, JsonValue jsonValue, Class cls);

        void write(Json json, T t, Class cls);
    }

    public Json() {
        this.typeName = "class";
        this.usePrototypes = true;
        this.enumNames = true;
        this.typeToFields = new ObjectMap<>();
        this.tagToClass = new ObjectMap<>();
        this.classToTag = new ObjectMap<>();
        this.classToSerializer = new ObjectMap<>();
        this.classToDefaultValues = new ObjectMap<>();
        this.equals1 = new Object[1];
        this.equals2 = new Object[1];
        this.outputType = JsonWriter.OutputType.minimal;
    }

    public Json(JsonWriter.OutputType outputType2) {
        this.typeName = "class";
        this.usePrototypes = true;
        this.enumNames = true;
        this.typeToFields = new ObjectMap<>();
        this.tagToClass = new ObjectMap<>();
        this.classToTag = new ObjectMap<>();
        this.classToSerializer = new ObjectMap<>();
        this.classToDefaultValues = new ObjectMap<>();
        this.equals1 = new Object[1];
        this.equals2 = new Object[1];
        this.outputType = outputType2;
    }

    public void setIgnoreUnknownFields(boolean ignoreUnknownFields2) {
        this.ignoreUnknownFields = ignoreUnknownFields2;
    }

    public void setOutputType(JsonWriter.OutputType outputType2) {
        this.outputType = outputType2;
    }

    public void setQuoteLongValues(boolean quoteLongValues2) {
        this.quoteLongValues = quoteLongValues2;
    }

    public void setEnumNames(boolean enumNames2) {
        this.enumNames = enumNames2;
    }

    public void addClassTag(String tag, Class type) {
        this.tagToClass.put(tag, type);
        this.classToTag.put(type, tag);
    }

    public Class getClass(String tag) {
        return this.tagToClass.get(tag);
    }

    public String getTag(Class type) {
        return this.classToTag.get(type);
    }

    public void setTypeName(String typeName2) {
        this.typeName = typeName2;
    }

    public void setDefaultSerializer(Serializer defaultSerializer2) {
        this.defaultSerializer = defaultSerializer2;
    }

    public <T> void setSerializer(Class<T> type, Serializer<T> serializer) {
        this.classToSerializer.put(type, serializer);
    }

    public <T> Serializer<T> getSerializer(Class<T> type) {
        return this.classToSerializer.get(type);
    }

    public void setUsePrototypes(boolean usePrototypes2) {
        this.usePrototypes = usePrototypes2;
    }

    public void setElementType(Class type, String fieldName, Class elementType) {
        FieldMetadata metadata = getFields(type).get(fieldName);
        if (metadata == null) {
            throw new SerializationException("Field not found: " + fieldName + " (" + type.getName() + ")");
        }
        metadata.elementType = elementType;
    }

    private OrderedMap<String, FieldMetadata> getFields(Class type) {
        OrderedMap<String, FieldMetadata> fields = this.typeToFields.get(type);
        if (fields != null) {
            return fields;
        }
        Array<Class> classHierarchy = new Array<>();
        for (Class nextClass = type; nextClass != Object.class; nextClass = nextClass.getSuperclass()) {
            classHierarchy.add(nextClass);
        }
        ArrayList<Field> allFields = new ArrayList<>();
        for (int i = classHierarchy.size - 1; i >= 0; i--) {
            Collections.addAll(allFields, ClassReflection.getDeclaredFields((Class) classHierarchy.get(i)));
        }
        OrderedMap<String, FieldMetadata> nameToField = new OrderedMap<>();
        int n = allFields.size();
        for (int i2 = 0; i2 < n; i2++) {
            Field field = (Field) allFields.get(i2);
            if (!field.isTransient() && !field.isStatic() && !field.isSynthetic()) {
                if (!field.isAccessible()) {
                    try {
                        field.setAccessible(true);
                    } catch (AccessControlException e) {
                    }
                }
                nameToField.put(field.getName(), new FieldMetadata(field));
            }
        }
        this.typeToFields.put(type, nameToField);
        return nameToField;
    }

    public String toJson(Object object) {
        return toJson(object, object == null ? null : object.getClass(), (Class) null);
    }

    public String toJson(Object object, Class knownType) {
        return toJson(object, knownType, (Class) null);
    }

    public String toJson(Object object, Class knownType, Class elementType) {
        StringWriter buffer = new StringWriter();
        toJson(object, knownType, elementType, buffer);
        return buffer.toString();
    }

    public void toJson(Object object, FileHandle file) {
        toJson(object, object == null ? null : object.getClass(), (Class) null, file);
    }

    public void toJson(Object object, Class knownType, FileHandle file) {
        toJson(object, knownType, (Class) null, file);
    }

    public void toJson(Object object, Class knownType, Class elementType, FileHandle file) {
        Writer writer2 = null;
        try {
            writer2 = file.writer(false, "UTF-8");
            toJson(object, knownType, elementType, writer2);
            StreamUtils.closeQuietly(writer2);
        } catch (Exception ex) {
            throw new SerializationException("Error writing file: " + file, ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(writer2);
            throw th;
        }
    }

    public void toJson(Object object, Writer writer2) {
        toJson(object, object == null ? null : object.getClass(), (Class) null, writer2);
    }

    public void toJson(Object object, Class knownType, Writer writer2) {
        toJson(object, knownType, (Class) null, writer2);
    }

    public void toJson(Object object, Class knownType, Class elementType, Writer writer2) {
        setWriter(writer2);
        try {
            writeValue(object, knownType, elementType);
        } finally {
            StreamUtils.closeQuietly(this.writer);
            this.writer = null;
        }
    }

    public void setWriter(Writer writer2) {
        if (!(writer2 instanceof JsonWriter)) {
            writer2 = new JsonWriter(writer2);
        }
        this.writer = (JsonWriter) writer2;
        this.writer.setOutputType(this.outputType);
        this.writer.setQuoteLongValues(this.quoteLongValues);
    }

    public JsonWriter getWriter() {
        return this.writer;
    }

    public void writeFields(Object object) {
        Class type = object.getClass();
        Object[] defaultValues = getDefaultValues(type);
        int i = 0;
        Iterator it = new OrderedMap.OrderedMapValues(getFields(type)).iterator();
        while (it.hasNext()) {
            FieldMetadata metadata = (FieldMetadata) it.next();
            Field field = metadata.field;
            try {
                Object value = field.get(object);
                if (defaultValues != null) {
                    int i2 = i + 1;
                    try {
                        Object defaultValue = defaultValues[i];
                        if (value == null && defaultValue == null) {
                            i = i2;
                        } else {
                            if (!(value == null || defaultValue == null)) {
                                if (value.equals(defaultValue)) {
                                    i = i2;
                                } else if (value.getClass().isArray() && defaultValue.getClass().isArray()) {
                                    this.equals1[0] = value;
                                    this.equals2[0] = defaultValue;
                                    if (Arrays.deepEquals(this.equals1, this.equals2)) {
                                        i = i2;
                                    }
                                }
                            }
                            i = i2;
                        }
                    } catch (ReflectionException e) {
                        ex = e;
                        throw new SerializationException("Error accessing field: " + field.getName() + " (" + type.getName() + ")", ex);
                    } catch (SerializationException e2) {
                        ex = e2;
                        ex.addTrace(field + " (" + type.getName() + ")");
                        throw ex;
                    } catch (Exception e3) {
                        runtimeEx = e3;
                        SerializationException ex = new SerializationException(runtimeEx);
                        ex.addTrace(field + " (" + type.getName() + ")");
                        throw ex;
                    }
                }
                this.writer.name(field.getName());
                writeValue(value, field.getType(), metadata.elementType);
            } catch (ReflectionException e4) {
                ex = e4;
            } catch (SerializationException e5) {
                ex = e5;
                ex.addTrace(field + " (" + type.getName() + ")");
                throw ex;
            } catch (Exception e6) {
                runtimeEx = e6;
                SerializationException ex2 = new SerializationException(runtimeEx);
                ex2.addTrace(field + " (" + type.getName() + ")");
                throw ex2;
            }
        }
    }

    private Object[] getDefaultValues(Class type) {
        if (!this.usePrototypes) {
            return null;
        }
        if (this.classToDefaultValues.containsKey(type)) {
            return this.classToDefaultValues.get(type);
        }
        try {
            Object object = newInstance(type);
            ObjectMap<String, FieldMetadata> fields = getFields(type);
            Object[] values = new Object[fields.size];
            this.classToDefaultValues.put(type, values);
            int i = 0;
            Iterator it = fields.values().iterator();
            while (it.hasNext()) {
                Field field = ((FieldMetadata) it.next()).field;
                int i2 = i + 1;
                try {
                    values[i] = field.get(object);
                    i = i2;
                } catch (ReflectionException ex) {
                    throw new SerializationException("Error accessing field: " + field.getName() + " (" + type.getName() + ")", ex);
                } catch (SerializationException ex2) {
                    ex2.addTrace(field + " (" + type.getName() + ")");
                    throw ex2;
                } catch (RuntimeException runtimeEx) {
                    SerializationException ex3 = new SerializationException(runtimeEx);
                    ex3.addTrace(field + " (" + type.getName() + ")");
                    throw ex3;
                }
            }
            return values;
        } catch (Exception e) {
            this.classToDefaultValues.put(type, null);
            return null;
        }
    }

    public void writeField(Object object, String name) {
        writeField(object, name, name, null);
    }

    public void writeField(Object object, String name, Class elementType) {
        writeField(object, name, name, elementType);
    }

    public void writeField(Object object, String fieldName, String jsonName) {
        writeField(object, fieldName, jsonName, null);
    }

    public void writeField(Object object, String fieldName, String jsonName, Class elementType) {
        Class type = object.getClass();
        FieldMetadata metadata = getFields(type).get(fieldName);
        if (metadata == null) {
            throw new SerializationException("Field not found: " + fieldName + " (" + type.getName() + ")");
        }
        Field field = metadata.field;
        if (elementType == null) {
            elementType = metadata.elementType;
        }
        try {
            this.writer.name(jsonName);
            writeValue(field.get(object), field.getType(), elementType);
        } catch (ReflectionException ex) {
            throw new SerializationException("Error accessing field: " + field.getName() + " (" + type.getName() + ")", ex);
        } catch (SerializationException ex2) {
            ex2.addTrace(field + " (" + type.getName() + ")");
            throw ex2;
        } catch (Exception runtimeEx) {
            SerializationException ex3 = new SerializationException(runtimeEx);
            ex3.addTrace(field + " (" + type.getName() + ")");
            throw ex3;
        }
    }

    public void writeValue(String name, Object value) {
        try {
            this.writer.name(name);
            if (value == null) {
                writeValue(value, (Class) null, (Class) null);
            } else {
                writeValue(value, value.getClass(), (Class) null);
            }
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeValue(String name, Object value, Class knownType) {
        try {
            this.writer.name(name);
            writeValue(value, knownType, (Class) null);
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeValue(String name, Object value, Class knownType, Class elementType) {
        try {
            this.writer.name(name);
            writeValue(value, knownType, elementType);
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeValue(Object value) {
        if (value == null) {
            writeValue(value, (Class) null, (Class) null);
        } else {
            writeValue(value, value.getClass(), (Class) null);
        }
    }

    public void writeValue(Object value, Class knownType) {
        writeValue(value, knownType, (Class) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.Json.writeValue(java.lang.Object, java.lang.Class, java.lang.Class):void
     arg types: [V, java.lang.Class, ?[OBJECT, ARRAY]]
     candidates:
      com.badlogic.gdx.utils.Json.writeValue(java.lang.String, java.lang.Object, java.lang.Class):void
      com.badlogic.gdx.utils.Json.writeValue(java.lang.Object, java.lang.Class, java.lang.Class):void */
    public void writeValue(Object value, Class knownType, Class elementType) {
        if (value == null) {
            try {
                this.writer.value(null);
            } catch (IOException ex) {
                throw new SerializationException(ex);
            }
        } else if ((knownType != null && knownType.isPrimitive()) || knownType == String.class || knownType == Integer.class || knownType == Boolean.class || knownType == Float.class || knownType == Long.class || knownType == Double.class || knownType == Short.class || knownType == Byte.class || knownType == Character.class) {
            this.writer.value(value);
        } else {
            Class actualType = value.getClass();
            if (actualType.isPrimitive() || actualType == String.class || actualType == Integer.class || actualType == Boolean.class || actualType == Float.class || actualType == Long.class || actualType == Double.class || actualType == Short.class || actualType == Byte.class || actualType == Character.class) {
                writeObjectStart(actualType, null);
                writeValue("value", value);
                writeObjectEnd();
            } else if (value instanceof Serializable) {
                writeObjectStart(actualType, knownType);
                ((Serializable) value).write(this);
                writeObjectEnd();
            } else {
                Serializer serializer = this.classToSerializer.get(actualType);
                if (serializer != null) {
                    serializer.write(this, value, knownType);
                } else if (value instanceof Array) {
                    if (knownType == null || actualType == knownType || actualType == Array.class) {
                        writeArrayStart();
                        Array array = (Array) value;
                        int n = array.size;
                        for (int i = 0; i < n; i++) {
                            writeValue(array.get(i), elementType, (Class) null);
                        }
                        writeArrayEnd();
                        return;
                    }
                    throw new SerializationException("Serialization of an Array other than the known type is not supported.\nKnown type: " + knownType + "\nActual type: " + actualType);
                } else if (value instanceof Collection) {
                    if (this.typeName == null || actualType == ArrayList.class || (knownType != null && knownType == actualType)) {
                        writeArrayStart();
                        for (Object item : (Collection) value) {
                            writeValue(item, elementType, (Class) null);
                        }
                        writeArrayEnd();
                        return;
                    }
                    writeObjectStart(actualType, knownType);
                    writeArrayStart("items");
                    for (Object item2 : (Collection) value) {
                        writeValue(item2, elementType, (Class) null);
                    }
                    writeArrayEnd();
                    writeObjectEnd();
                } else if (actualType.isArray()) {
                    if (elementType == null) {
                        elementType = actualType.getComponentType();
                    }
                    int length = ArrayReflection.getLength(value);
                    writeArrayStart();
                    for (int i2 = 0; i2 < length; i2++) {
                        writeValue(ArrayReflection.get(value, i2), elementType, (Class) null);
                    }
                    writeArrayEnd();
                } else if (value instanceof ObjectMap) {
                    if (knownType == null) {
                        knownType = ObjectMap.class;
                    }
                    writeObjectStart(actualType, knownType);
                    Iterator it = ((ObjectMap) value).entries().iterator();
                    while (it.hasNext()) {
                        ObjectMap.Entry entry = (ObjectMap.Entry) it.next();
                        this.writer.name(convertToString((Object) entry.key));
                        writeValue((Object) entry.value, elementType, (Class) null);
                    }
                    writeObjectEnd();
                } else if (value instanceof ArrayMap) {
                    if (knownType == null) {
                        knownType = ArrayMap.class;
                    }
                    writeObjectStart(actualType, knownType);
                    ArrayMap map = (ArrayMap) value;
                    int n2 = map.size;
                    for (int i3 = 0; i3 < n2; i3++) {
                        this.writer.name(convertToString((Object) map.keys[i3]));
                        writeValue((Object) map.values[i3], elementType, (Class) null);
                    }
                    writeObjectEnd();
                } else if (value instanceof Map) {
                    if (knownType == null) {
                        knownType = HashMap.class;
                    }
                    writeObjectStart(actualType, knownType);
                    for (Map.Entry entry2 : ((Map) value).entrySet()) {
                        this.writer.name(convertToString(entry2.getKey()));
                        writeValue(entry2.getValue(), elementType, (Class) null);
                    }
                    writeObjectEnd();
                } else if (!ClassReflection.isAssignableFrom(Enum.class, actualType)) {
                    writeObjectStart(actualType, knownType);
                    writeFields(value);
                    writeObjectEnd();
                } else if (this.typeName == null || (knownType != null && knownType == actualType)) {
                    this.writer.value(convertToString((Enum) value));
                } else {
                    if (actualType.getEnumConstants() == null) {
                        actualType = actualType.getSuperclass();
                    }
                    writeObjectStart(actualType, null);
                    this.writer.name("value");
                    this.writer.value(convertToString((Enum) value));
                    writeObjectEnd();
                }
            }
        }
    }

    public void writeObjectStart(String name) {
        try {
            this.writer.name(name);
            writeObjectStart();
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeObjectStart(String name, Class actualType, Class knownType) {
        try {
            this.writer.name(name);
            writeObjectStart(actualType, knownType);
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeObjectStart() {
        try {
            this.writer.object();
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeObjectStart(Class actualType, Class knownType) {
        try {
            this.writer.object();
            if (knownType == null || knownType != actualType) {
                writeType(actualType);
            }
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeObjectEnd() {
        try {
            this.writer.pop();
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeArrayStart(String name) {
        try {
            this.writer.name(name);
            this.writer.array();
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeArrayStart() {
        try {
            this.writer.array();
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeArrayEnd() {
        try {
            this.writer.pop();
        } catch (IOException ex) {
            throw new SerializationException(ex);
        }
    }

    public void writeType(Class type) {
        if (this.typeName != null) {
            String className = getTag(type);
            if (className == null) {
                className = type.getName();
            }
            try {
                this.writer.set(this.typeName, className);
            } catch (IOException ex) {
                throw new SerializationException(ex);
            }
        }
    }

    public <T> T fromJson(Class cls, Reader reader) {
        return readValue(cls, (Class) null, new JsonReader().parse(reader));
    }

    public <T> T fromJson(Class<T> type, Class elementType, Reader reader) {
        return readValue(type, elementType, new JsonReader().parse(reader));
    }

    public <T> T fromJson(Class cls, InputStream input) {
        return readValue(cls, (Class) null, new JsonReader().parse(input));
    }

    public <T> T fromJson(Class<T> type, Class elementType, InputStream input) {
        return readValue(type, elementType, new JsonReader().parse(input));
    }

    public <T> T fromJson(Class cls, FileHandle file) {
        try {
            return readValue(cls, (Class) null, new JsonReader().parse(file));
        } catch (Exception ex) {
            throw new SerializationException("Error reading file: " + file, ex);
        }
    }

    public <T> T fromJson(Class<T> type, Class elementType, FileHandle file) {
        try {
            return readValue(type, elementType, new JsonReader().parse(file));
        } catch (Exception ex) {
            throw new SerializationException("Error reading file: " + file, ex);
        }
    }

    public <T> T fromJson(Class<T> type, char[] data, int offset, int length) {
        return readValue(type, (Class) null, new JsonReader().parse(data, offset, length));
    }

    public <T> T fromJson(Class<T> type, Class elementType, char[] data, int offset, int length) {
        return readValue(type, elementType, new JsonReader().parse(data, offset, length));
    }

    public <T> T fromJson(Class cls, String json) {
        return readValue(cls, (Class) null, new JsonReader().parse(json));
    }

    public <T> T fromJson(Class<T> type, Class elementType, String json) {
        return readValue(type, elementType, new JsonReader().parse(json));
    }

    public void readField(Object object, String name, JsonValue jsonData) {
        readField(object, name, name, (Class) null, jsonData);
    }

    public void readField(Object object, String name, Class elementType, JsonValue jsonData) {
        readField(object, name, name, elementType, jsonData);
    }

    public void readField(Object object, String fieldName, String jsonName, JsonValue jsonData) {
        readField(object, fieldName, jsonName, (Class) null, jsonData);
    }

    public void readField(Object object, String fieldName, String jsonName, Class elementType, JsonValue jsonMap) {
        Class type = object.getClass();
        FieldMetadata metadata = getFields(type).get(fieldName);
        if (metadata == null) {
            throw new SerializationException("Field not found: " + fieldName + " (" + type.getName() + ")");
        }
        Field field = metadata.field;
        if (elementType == null) {
            elementType = metadata.elementType;
        }
        readField(object, field, jsonName, elementType, jsonMap);
    }

    public void readField(Object object, Field field, String jsonName, Class elementType, JsonValue jsonMap) {
        JsonValue jsonValue = jsonMap.get(jsonName);
        if (jsonValue != null) {
            try {
                field.set(object, readValue(field.getType(), elementType, jsonValue));
            } catch (ReflectionException ex) {
                throw new SerializationException("Error accessing field: " + field.getName() + " (" + field.getDeclaringClass().getName() + ")", ex);
            } catch (SerializationException ex2) {
                ex2.addTrace(String.valueOf(field.getName()) + " (" + field.getDeclaringClass().getName() + ")");
                throw ex2;
            } catch (RuntimeException runtimeEx) {
                SerializationException ex3 = new SerializationException(runtimeEx);
                ex3.addTrace(String.valueOf(field.getName()) + " (" + field.getDeclaringClass().getName() + ")");
                throw ex3;
            }
        }
    }

    public void readFields(Object object, JsonValue jsonMap) {
        Class type = object.getClass();
        ObjectMap<String, FieldMetadata> fields = getFields(type);
        for (JsonValue child = jsonMap.child; child != null; child = child.next) {
            FieldMetadata metadata = fields.get(child.name());
            if (metadata != null) {
                Field field = metadata.field;
                try {
                    field.set(object, readValue(field.getType(), metadata.elementType, child));
                } catch (ReflectionException ex) {
                    throw new SerializationException("Error accessing field: " + field.getName() + " (" + type.getName() + ")", ex);
                } catch (SerializationException ex2) {
                    ex2.addTrace(String.valueOf(field.getName()) + " (" + type.getName() + ")");
                    throw ex2;
                } catch (RuntimeException runtimeEx) {
                    SerializationException ex3 = new SerializationException(runtimeEx);
                    ex3.addTrace(String.valueOf(field.getName()) + " (" + type.getName() + ")");
                    throw ex3;
                }
            } else if (!this.ignoreUnknownFields) {
                throw new SerializationException("Field not found: " + child.name() + " (" + type.getName() + ")");
            }
        }
    }

    public <T> T readValue(String name, Class cls, JsonValue jsonMap) {
        return readValue(cls, (Class) null, jsonMap.get(name));
    }

    public <T> T readValue(String name, Class cls, Object obj, JsonValue jsonMap) {
        JsonValue jsonValue = jsonMap.get(name);
        return jsonValue == null ? obj : readValue(cls, (Class) null, jsonValue);
    }

    public <T> T readValue(String name, Class cls, Class elementType, JsonValue jsonMap) {
        return readValue(cls, elementType, jsonMap.get(name));
    }

    public <T> T readValue(String name, Class<T> type, Class elementType, T defaultValue, JsonValue jsonMap) {
        JsonValue jsonValue = jsonMap.get(name);
        return jsonValue == null ? defaultValue : readValue(type, elementType, jsonValue);
    }

    public <T> T readValue(Class cls, Class elementType, Object obj, JsonValue jsonData) {
        return readValue(cls, elementType, jsonData);
    }

    public <T> T readValue(Class<T> type, JsonValue jsonData) {
        return readValue(type, (Class) null, jsonData);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:157:0x03a3, code lost:
        if (r22 != java.lang.Boolean.class) goto L_0x03b0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x03c3  */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x04ef A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T readValue(java.lang.Class<com.badlogic.gdx.utils.Array> r22, java.lang.Class r23, com.badlogic.gdx.utils.JsonValue r24) {
        /*
            r21 = this;
            if (r24 != 0) goto L_0x0004
            r15 = 0
        L_0x0003:
            return r15
        L_0x0004:
            boolean r18 = r24.isObject()
            if (r18 == 0) goto L_0x04f6
            r0 = r21
            java.lang.String r0 = r0.typeName
            r18 = r0
            if (r18 != 0) goto L_0x004b
            r5 = 0
        L_0x0013:
            if (r5 == 0) goto L_0x002e
            r0 = r21
            java.lang.String r0 = r0.typeName
            r18 = r0
            r0 = r24
            r1 = r18
            r0.remove(r1)
            r0 = r21
            java.lang.Class r22 = r0.getClass(r5)
            if (r22 != 0) goto L_0x002e
            java.lang.Class r22 = com.badlogic.gdx.utils.reflect.ClassReflection.forName(r5)     // Catch:{ ReflectionException -> 0x005e }
        L_0x002e:
            if (r22 != 0) goto L_0x006a
            r0 = r21
            com.badlogic.gdx.utils.Json$Serializer r0 = r0.defaultSerializer
            r18 = r0
            if (r18 == 0) goto L_0x0067
            r0 = r21
            com.badlogic.gdx.utils.Json$Serializer r0 = r0.defaultSerializer
            r18 = r0
            r0 = r18
            r1 = r21
            r2 = r24
            r3 = r22
            java.lang.Object r15 = r0.read(r1, r2, r3)
            goto L_0x0003
        L_0x004b:
            r0 = r21
            java.lang.String r0 = r0.typeName
            r18 = r0
            r19 = 0
            r0 = r24
            r1 = r18
            r2 = r19
            java.lang.String r5 = r0.getString(r1, r2)
            goto L_0x0013
        L_0x005e:
            r9 = move-exception
            com.badlogic.gdx.utils.SerializationException r18 = new com.badlogic.gdx.utils.SerializationException
            r0 = r18
            r0.<init>(r9)
            throw r18
        L_0x0067:
            r15 = r24
            goto L_0x0003
        L_0x006a:
            java.lang.Class<java.lang.String> r18 = java.lang.String.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Integer> r18 = java.lang.Integer.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Boolean> r18 = java.lang.Boolean.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Float> r18 = java.lang.Float.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Long> r18 = java.lang.Long.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Double> r18 = java.lang.Double.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Short> r18 = java.lang.Short.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Byte> r18 = java.lang.Byte.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Character> r18 = java.lang.Character.class
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x00be
            java.lang.Class<java.lang.Enum> r18 = java.lang.Enum.class
            r0 = r18
            r1 = r22
            boolean r18 = com.badlogic.gdx.utils.reflect.ClassReflection.isAssignableFrom(r0, r1)
            if (r18 == 0) goto L_0x00ce
        L_0x00be:
            java.lang.String r18 = "value"
            r0 = r21
            r1 = r18
            r2 = r22
            r3 = r24
            java.lang.Object r15 = r0.readValue(r1, r2, r3)
            goto L_0x0003
        L_0x00ce:
            r0 = r21
            java.lang.String r0 = r0.typeName
            r18 = r0
            if (r18 == 0) goto L_0x0110
            java.lang.Class<java.util.Collection> r18 = java.util.Collection.class
            r0 = r18
            r1 = r22
            boolean r18 = com.badlogic.gdx.utils.reflect.ClassReflection.isAssignableFrom(r0, r1)
            if (r18 == 0) goto L_0x0110
            java.lang.String r18 = "items"
            r0 = r24
            r1 = r18
            com.badlogic.gdx.utils.JsonValue r24 = r0.get(r1)
            r12 = r24
        L_0x00ee:
            if (r22 == 0) goto L_0x01cf
            r0 = r21
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.Json$Serializer> r0 = r0.classToSerializer
            r18 = r0
            r0 = r18
            r1 = r22
            java.lang.Object r16 = r0.get(r1)
            com.badlogic.gdx.utils.Json$Serializer r16 = (com.badlogic.gdx.utils.Json.Serializer) r16
            if (r16 == 0) goto L_0x01cf
            r0 = r16
            r1 = r21
            r2 = r22
            java.lang.Object r15 = r0.read(r1, r12, r2)
            r24 = r12
            goto L_0x0003
        L_0x0110:
            r0 = r21
            com.badlogic.gdx.utils.ObjectMap<java.lang.Class, com.badlogic.gdx.utils.Json$Serializer> r0 = r0.classToSerializer
            r18 = r0
            r0 = r18
            r1 = r22
            java.lang.Object r16 = r0.get(r1)
            com.badlogic.gdx.utils.Json$Serializer r16 = (com.badlogic.gdx.utils.Json.Serializer) r16
            if (r16 == 0) goto L_0x0130
            r0 = r16
            r1 = r21
            r2 = r24
            r3 = r22
            java.lang.Object r15 = r0.read(r1, r2, r3)
            goto L_0x0003
        L_0x0130:
            java.lang.Object r14 = r21.newInstance(r22)
            boolean r0 = r14 instanceof com.badlogic.gdx.utils.Json.Serializable
            r18 = r0
            if (r18 == 0) goto L_0x014a
            r18 = r14
            com.badlogic.gdx.utils.Json$Serializable r18 = (com.badlogic.gdx.utils.Json.Serializable) r18
            r0 = r18
            r1 = r21
            r2 = r24
            r0.read(r1, r2)
            r15 = r14
            goto L_0x0003
        L_0x014a:
            boolean r0 = r14 instanceof com.badlogic.gdx.utils.ObjectMap
            r18 = r0
            if (r18 == 0) goto L_0x0173
            r15 = r14
            com.badlogic.gdx.utils.ObjectMap r15 = (com.badlogic.gdx.utils.ObjectMap) r15
            r0 = r24
            com.badlogic.gdx.utils.JsonValue r4 = r0.child
        L_0x0157:
            if (r4 == 0) goto L_0x0003
            java.lang.String r18 = r4.name()
            r19 = 0
            r0 = r21
            r1 = r23
            r2 = r19
            java.lang.Object r19 = r0.readValue(r1, r2, r4)
            r0 = r18
            r1 = r19
            r15.put(r0, r1)
            com.badlogic.gdx.utils.JsonValue r4 = r4.next
            goto L_0x0157
        L_0x0173:
            boolean r0 = r14 instanceof com.badlogic.gdx.utils.ArrayMap
            r18 = r0
            if (r18 == 0) goto L_0x019c
            r15 = r14
            com.badlogic.gdx.utils.ArrayMap r15 = (com.badlogic.gdx.utils.ArrayMap) r15
            r0 = r24
            com.badlogic.gdx.utils.JsonValue r4 = r0.child
        L_0x0180:
            if (r4 == 0) goto L_0x0003
            java.lang.String r18 = r4.name()
            r19 = 0
            r0 = r21
            r1 = r23
            r2 = r19
            java.lang.Object r19 = r0.readValue(r1, r2, r4)
            r0 = r18
            r1 = r19
            r15.put(r0, r1)
            com.badlogic.gdx.utils.JsonValue r4 = r4.next
            goto L_0x0180
        L_0x019c:
            boolean r0 = r14 instanceof java.util.Map
            r18 = r0
            if (r18 == 0) goto L_0x01c5
            r15 = r14
            java.util.Map r15 = (java.util.Map) r15
            r0 = r24
            com.badlogic.gdx.utils.JsonValue r4 = r0.child
        L_0x01a9:
            if (r4 == 0) goto L_0x0003
            java.lang.String r18 = r4.name()
            r19 = 0
            r0 = r21
            r1 = r23
            r2 = r19
            java.lang.Object r19 = r0.readValue(r1, r2, r4)
            r0 = r18
            r1 = r19
            r15.put(r0, r1)
            com.badlogic.gdx.utils.JsonValue r4 = r4.next
            goto L_0x01a9
        L_0x01c5:
            r0 = r21
            r1 = r24
            r0.readFields(r14, r1)
            r15 = r14
            goto L_0x0003
        L_0x01cf:
            boolean r18 = r12.isArray()
            if (r18 == 0) goto L_0x02bf
            if (r22 == 0) goto L_0x01df
            java.lang.Class<java.lang.Object> r18 = java.lang.Object.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x01e1
        L_0x01df:
            java.lang.Class<com.badlogic.gdx.utils.Array> r22 = com.badlogic.gdx.utils.Array.class
        L_0x01e1:
            java.lang.Class<com.badlogic.gdx.utils.Array> r18 = com.badlogic.gdx.utils.Array.class
            r0 = r18
            r1 = r22
            boolean r18 = com.badlogic.gdx.utils.reflect.ClassReflection.isAssignableFrom(r0, r1)
            if (r18 == 0) goto L_0x021f
            java.lang.Class<com.badlogic.gdx.utils.Array> r18 = com.badlogic.gdx.utils.Array.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0202
            com.badlogic.gdx.utils.Array r15 = new com.badlogic.gdx.utils.Array
            r15.<init>()
        L_0x01fa:
            com.badlogic.gdx.utils.JsonValue r4 = r12.child
        L_0x01fc:
            if (r4 != 0) goto L_0x020b
            r24 = r12
            goto L_0x0003
        L_0x0202:
            java.lang.Object r18 = r21.newInstance(r22)
            com.badlogic.gdx.utils.Array r18 = (com.badlogic.gdx.utils.Array) r18
            r15 = r18
            goto L_0x01fa
        L_0x020b:
            r18 = 0
            r0 = r21
            r1 = r23
            r2 = r18
            java.lang.Object r18 = r0.readValue(r1, r2, r4)
            r0 = r18
            r15.add(r0)
            com.badlogic.gdx.utils.JsonValue r4 = r4.next
            goto L_0x01fc
        L_0x021f:
            java.lang.Class<java.util.Collection> r18 = java.util.Collection.class
            r0 = r18
            r1 = r22
            boolean r18 = com.badlogic.gdx.utils.reflect.ClassReflection.isAssignableFrom(r0, r1)
            if (r18 == 0) goto L_0x025b
            boolean r18 = r22.isInterface()
            if (r18 == 0) goto L_0x023e
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
        L_0x0236:
            com.badlogic.gdx.utils.JsonValue r4 = r12.child
        L_0x0238:
            if (r4 != 0) goto L_0x0247
            r24 = r12
            goto L_0x0003
        L_0x023e:
            java.lang.Object r18 = r21.newInstance(r22)
            java.util.Collection r18 = (java.util.Collection) r18
            r15 = r18
            goto L_0x0236
        L_0x0247:
            r18 = 0
            r0 = r21
            r1 = r23
            r2 = r18
            java.lang.Object r18 = r0.readValue(r1, r2, r4)
            r0 = r18
            r15.add(r0)
            com.badlogic.gdx.utils.JsonValue r4 = r4.next
            goto L_0x0238
        L_0x025b:
            boolean r18 = r22.isArray()
            if (r18 == 0) goto L_0x0294
            java.lang.Class r6 = r22.getComponentType()
            if (r23 != 0) goto L_0x0269
            r23 = r6
        L_0x0269:
            int r0 = r12.size
            r18 = r0
            r0 = r18
            java.lang.Object r15 = com.badlogic.gdx.utils.reflect.ArrayReflection.newInstance(r6, r0)
            r10 = 0
            com.badlogic.gdx.utils.JsonValue r4 = r12.child
            r11 = r10
        L_0x0277:
            if (r4 != 0) goto L_0x027d
            r24 = r12
            goto L_0x0003
        L_0x027d:
            int r10 = r11 + 1
            r18 = 0
            r0 = r21
            r1 = r23
            r2 = r18
            java.lang.Object r18 = r0.readValue(r1, r2, r4)
            r0 = r18
            com.badlogic.gdx.utils.reflect.ArrayReflection.set(r15, r11, r0)
            com.badlogic.gdx.utils.JsonValue r4 = r4.next
            r11 = r10
            goto L_0x0277
        L_0x0294:
            com.badlogic.gdx.utils.SerializationException r18 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            java.lang.String r20 = "Unable to convert value to required type: "
            r19.<init>(r20)
            r0 = r19
            java.lang.StringBuilder r19 = r0.append(r12)
            java.lang.String r20 = " ("
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r20 = r22.getName()
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r20 = ")"
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r19 = r19.toString()
            r18.<init>(r19)
            throw r18
        L_0x02bf:
            boolean r18 = r12.isNumber()
            if (r18 == 0) goto L_0x04f2
            if (r22 == 0) goto L_0x02d7
            java.lang.Class r18 = java.lang.Float.TYPE     // Catch:{ NumberFormatException -> 0x037f }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x02d7
            java.lang.Class<java.lang.Float> r18 = java.lang.Float.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x02e3
        L_0x02d7:
            float r18 = r12.asFloat()     // Catch:{ NumberFormatException -> 0x037f }
            java.lang.Float r15 = java.lang.Float.valueOf(r18)     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x02e3:
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ NumberFormatException -> 0x037f }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x02f3
            java.lang.Class<java.lang.Integer> r18 = java.lang.Integer.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x02ff
        L_0x02f3:
            int r18 = r12.asInt()     // Catch:{ NumberFormatException -> 0x037f }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r18)     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x02ff:
            java.lang.Class r18 = java.lang.Long.TYPE     // Catch:{ NumberFormatException -> 0x037f }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x030f
            java.lang.Class<java.lang.Long> r18 = java.lang.Long.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x031b
        L_0x030f:
            long r18 = r12.asLong()     // Catch:{ NumberFormatException -> 0x037f }
            java.lang.Long r15 = java.lang.Long.valueOf(r18)     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x031b:
            java.lang.Class r18 = java.lang.Double.TYPE     // Catch:{ NumberFormatException -> 0x037f }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x032b
            java.lang.Class<java.lang.Double> r18 = java.lang.Double.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0337
        L_0x032b:
            double r18 = r12.asDouble()     // Catch:{ NumberFormatException -> 0x037f }
            java.lang.Double r15 = java.lang.Double.valueOf(r18)     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x0337:
            java.lang.Class<java.lang.String> r18 = java.lang.String.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0347
            java.lang.String r15 = r12.asString()     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x0347:
            java.lang.Class r18 = java.lang.Short.TYPE     // Catch:{ NumberFormatException -> 0x037f }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x0357
            java.lang.Class<java.lang.Short> r18 = java.lang.Short.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0363
        L_0x0357:
            short r18 = r12.asShort()     // Catch:{ NumberFormatException -> 0x037f }
            java.lang.Short r15 = java.lang.Short.valueOf(r18)     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x0363:
            java.lang.Class r18 = java.lang.Byte.TYPE     // Catch:{ NumberFormatException -> 0x037f }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x0373
            java.lang.Class<java.lang.Byte> r18 = java.lang.Byte.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0380
        L_0x0373:
            byte r18 = r12.asByte()     // Catch:{ NumberFormatException -> 0x037f }
            java.lang.Byte r15 = java.lang.Byte.valueOf(r18)     // Catch:{ NumberFormatException -> 0x037f }
            r24 = r12
            goto L_0x0003
        L_0x037f:
            r18 = move-exception
        L_0x0380:
            com.badlogic.gdx.utils.JsonValue r24 = new com.badlogic.gdx.utils.JsonValue
            java.lang.String r18 = r12.asString()
            r0 = r24
            r1 = r18
            r0.<init>(r1)
        L_0x038d:
            boolean r18 = r24.isBoolean()
            if (r18 == 0) goto L_0x03bd
            if (r22 == 0) goto L_0x03a5
            java.lang.Class r18 = java.lang.Boolean.TYPE     // Catch:{ NumberFormatException -> 0x03af }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x03a5
            java.lang.Class<java.lang.Boolean> r18 = java.lang.Boolean.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x03b0
        L_0x03a5:
            boolean r18 = r24.asBoolean()     // Catch:{ NumberFormatException -> 0x03af }
            java.lang.Boolean r15 = java.lang.Boolean.valueOf(r18)     // Catch:{ NumberFormatException -> 0x03af }
            goto L_0x0003
        L_0x03af:
            r18 = move-exception
        L_0x03b0:
            com.badlogic.gdx.utils.JsonValue r12 = new com.badlogic.gdx.utils.JsonValue
            java.lang.String r18 = r24.asString()
            r0 = r18
            r12.<init>(r0)
            r24 = r12
        L_0x03bd:
            boolean r18 = r24.isString()
            if (r18 == 0) goto L_0x04ef
            java.lang.String r17 = r24.asString()
            if (r22 == 0) goto L_0x03d1
            java.lang.Class<java.lang.String> r18 = java.lang.String.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x03d5
        L_0x03d1:
            r15 = r17
            goto L_0x0003
        L_0x03d5:
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ NumberFormatException -> 0x0459 }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x03e5
            java.lang.Class<java.lang.Integer> r18 = java.lang.Integer.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x03eb
        L_0x03e5:
            java.lang.Integer r15 = java.lang.Integer.valueOf(r17)     // Catch:{ NumberFormatException -> 0x0459 }
            goto L_0x0003
        L_0x03eb:
            java.lang.Class r18 = java.lang.Float.TYPE     // Catch:{ NumberFormatException -> 0x0459 }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x03fb
            java.lang.Class<java.lang.Float> r18 = java.lang.Float.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0401
        L_0x03fb:
            java.lang.Float r15 = java.lang.Float.valueOf(r17)     // Catch:{ NumberFormatException -> 0x0459 }
            goto L_0x0003
        L_0x0401:
            java.lang.Class r18 = java.lang.Long.TYPE     // Catch:{ NumberFormatException -> 0x0459 }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x0411
            java.lang.Class<java.lang.Long> r18 = java.lang.Long.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0417
        L_0x0411:
            java.lang.Long r15 = java.lang.Long.valueOf(r17)     // Catch:{ NumberFormatException -> 0x0459 }
            goto L_0x0003
        L_0x0417:
            java.lang.Class r18 = java.lang.Double.TYPE     // Catch:{ NumberFormatException -> 0x0459 }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x0427
            java.lang.Class<java.lang.Double> r18 = java.lang.Double.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x042d
        L_0x0427:
            java.lang.Double r15 = java.lang.Double.valueOf(r17)     // Catch:{ NumberFormatException -> 0x0459 }
            goto L_0x0003
        L_0x042d:
            java.lang.Class r18 = java.lang.Short.TYPE     // Catch:{ NumberFormatException -> 0x0459 }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x043d
            java.lang.Class<java.lang.Short> r18 = java.lang.Short.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0443
        L_0x043d:
            java.lang.Short r15 = java.lang.Short.valueOf(r17)     // Catch:{ NumberFormatException -> 0x0459 }
            goto L_0x0003
        L_0x0443:
            java.lang.Class r18 = java.lang.Byte.TYPE     // Catch:{ NumberFormatException -> 0x0459 }
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x0453
            java.lang.Class<java.lang.Byte> r18 = java.lang.Byte.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x045a
        L_0x0453:
            java.lang.Byte r15 = java.lang.Byte.valueOf(r17)     // Catch:{ NumberFormatException -> 0x0459 }
            goto L_0x0003
        L_0x0459:
            r18 = move-exception
        L_0x045a:
            java.lang.Class r18 = java.lang.Boolean.TYPE
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x046a
            java.lang.Class<java.lang.Boolean> r18 = java.lang.Boolean.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x0470
        L_0x046a:
            java.lang.Boolean r15 = java.lang.Boolean.valueOf(r17)
            goto L_0x0003
        L_0x0470:
            java.lang.Class r18 = java.lang.Character.TYPE
            r0 = r22
            r1 = r18
            if (r0 == r1) goto L_0x0480
            java.lang.Class<java.lang.Character> r18 = java.lang.Character.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x048c
        L_0x0480:
            r18 = 0
            char r18 = r17.charAt(r18)
            java.lang.Character r15 = java.lang.Character.valueOf(r18)
            goto L_0x0003
        L_0x048c:
            java.lang.Class<java.lang.Enum> r18 = java.lang.Enum.class
            r0 = r18
            r1 = r22
            boolean r18 = com.badlogic.gdx.utils.reflect.ClassReflection.isAssignableFrom(r0, r1)
            if (r18 == 0) goto L_0x04a2
            java.lang.Object[] r7 = r22.getEnumConstants()
            java.lang.Enum[] r7 = (java.lang.Enum[]) r7
            r10 = 0
            int r13 = r7.length
        L_0x04a0:
            if (r10 < r13) goto L_0x04ae
        L_0x04a2:
            java.lang.Class<java.lang.CharSequence> r18 = java.lang.CharSequence.class
            r0 = r22
            r1 = r18
            if (r0 != r1) goto L_0x04c2
            r15 = r17
            goto L_0x0003
        L_0x04ae:
            r8 = r7[r10]
            r0 = r21
            java.lang.String r18 = r0.convertToString(r8)
            boolean r18 = r17.equals(r18)
            if (r18 == 0) goto L_0x04bf
            r15 = r8
            goto L_0x0003
        L_0x04bf:
            int r10 = r10 + 1
            goto L_0x04a0
        L_0x04c2:
            com.badlogic.gdx.utils.SerializationException r18 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            java.lang.String r20 = "Unable to convert value to required type: "
            r19.<init>(r20)
            r0 = r19
            r1 = r24
            java.lang.StringBuilder r19 = r0.append(r1)
            java.lang.String r20 = " ("
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r20 = r22.getName()
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r20 = ")"
            java.lang.StringBuilder r19 = r19.append(r20)
            java.lang.String r19 = r19.toString()
            r18.<init>(r19)
            throw r18
        L_0x04ef:
            r15 = 0
            goto L_0x0003
        L_0x04f2:
            r24 = r12
            goto L_0x038d
        L_0x04f6:
            r12 = r24
            goto L_0x00ee
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Json.readValue(java.lang.Class, java.lang.Class, com.badlogic.gdx.utils.JsonValue):java.lang.Object");
    }

    private String convertToString(Enum e) {
        return this.enumNames ? e.name() : e.toString();
    }

    private String convertToString(Object object) {
        if (object instanceof Enum) {
            return convertToString((Enum) object);
        }
        if (object instanceof Class) {
            return ((Class) object).getName();
        }
        return String.valueOf(object);
    }

    /* access modifiers changed from: protected */
    public Object newInstance(Class type) {
        try {
            return ClassReflection.newInstance(type);
        } catch (Exception e) {
            ex = e;
            try {
                Constructor constructor = ClassReflection.getDeclaredConstructor(type, new Class[0]);
                constructor.setAccessible(true);
                return constructor.newInstance(new Object[0]);
            } catch (SecurityException e2) {
            } catch (ReflectionException e3) {
                if (ClassReflection.isAssignableFrom(Enum.class, type)) {
                    if (type.getEnumConstants() == null) {
                        type = type.getSuperclass();
                    }
                    return type.getEnumConstants()[0];
                } else if (type.isArray()) {
                    throw new SerializationException("Encountered JSON object when expected array of type: " + type.getName(), ex);
                } else if (!ClassReflection.isMemberClass(type) || ClassReflection.isStaticClass(type)) {
                    throw new SerializationException("Class cannot be created (missing no-arg constructor): " + type.getName(), ex);
                } else {
                    throw new SerializationException("Class cannot be created (non-static member class): " + type.getName(), ex);
                }
            } catch (Exception privateConstructorException) {
                ex = privateConstructorException;
            }
        }
        throw new SerializationException("Error constructing instance of class: " + type.getName(), ex);
    }

    public String prettyPrint(Object object) {
        return prettyPrint(object, 0);
    }

    public String prettyPrint(String json) {
        return prettyPrint(json, 0);
    }

    public String prettyPrint(Object object, int singleLineColumns) {
        return prettyPrint(toJson(object), singleLineColumns);
    }

    public String prettyPrint(String json, int singleLineColumns) {
        return new JsonReader().parse(json).prettyPrint(this.outputType, singleLineColumns);
    }

    public String prettyPrint(Object object, JsonValue.PrettyPrintSettings settings) {
        return prettyPrint(toJson(object), settings);
    }

    public String prettyPrint(String json, JsonValue.PrettyPrintSettings settings) {
        return new JsonReader().parse(json).prettyPrint(settings);
    }

    private static class FieldMetadata {
        Class elementType;
        Field field;

        public FieldMetadata(Field field2) {
            this.field = field2;
            this.elementType = field2.getElementType((ClassReflection.isAssignableFrom(ObjectMap.class, field2.getType()) || ClassReflection.isAssignableFrom(Map.class, field2.getType())) ? 1 : 0);
        }
    }

    public static abstract class ReadOnlySerializer<T> implements Serializer<T> {
        public abstract T read(Json json, JsonValue jsonValue, Class cls);

        public void write(Json json, T t, Class knownType) {
        }
    }
}
