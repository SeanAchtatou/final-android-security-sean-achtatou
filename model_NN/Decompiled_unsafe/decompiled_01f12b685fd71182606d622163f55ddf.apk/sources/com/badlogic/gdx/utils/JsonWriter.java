package com.badlogic.gdx.utils;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Pattern;

public class JsonWriter extends Writer {
    private JsonObject current;
    private boolean named;
    private OutputType outputType = OutputType.json;
    private boolean quoteLongValues = false;
    private final Array<JsonObject> stack = new Array<>();
    final Writer writer;

    public JsonWriter(Writer writer2) {
        this.writer = writer2;
    }

    public Writer getWriter() {
        return this.writer;
    }

    public void setOutputType(OutputType outputType2) {
        this.outputType = outputType2;
    }

    public void setQuoteLongValues(boolean quoteLongValues2) {
        this.quoteLongValues = quoteLongValues2;
    }

    public JsonWriter name(String name) throws IOException {
        if (this.current == null || this.current.array) {
            throw new IllegalStateException("Current item must be an object.");
        }
        if (!this.current.needsComma) {
            this.current.needsComma = true;
        } else {
            this.writer.write(44);
        }
        this.writer.write(this.outputType.quoteName(name));
        this.writer.write(58);
        this.named = true;
        return this;
    }

    public JsonWriter object() throws IOException {
        requireCommaOrName();
        Array<JsonObject> array = this.stack;
        JsonObject jsonObject = new JsonObject(false);
        this.current = jsonObject;
        array.add(jsonObject);
        return this;
    }

    public JsonWriter array() throws IOException {
        requireCommaOrName();
        Array<JsonObject> array = this.stack;
        JsonObject jsonObject = new JsonObject(true);
        this.current = jsonObject;
        array.add(jsonObject);
        return this;
    }

    public JsonWriter value(Object value) throws IOException {
        if (this.quoteLongValues && ((value instanceof Long) || (value instanceof Double) || (value instanceof BigDecimal) || (value instanceof BigInteger))) {
            value = value.toString();
        } else if (value instanceof Number) {
            Number number = (Number) value;
            long longValue = number.longValue();
            if (number.doubleValue() == ((double) longValue)) {
                value = Long.valueOf(longValue);
            }
        }
        requireCommaOrName();
        this.writer.write(this.outputType.quoteValue(value));
        return this;
    }

    public JsonWriter json(String json) throws IOException {
        requireCommaOrName();
        this.writer.write(json);
        return this;
    }

    private void requireCommaOrName() throws IOException {
        if (this.current != null) {
            if (this.current.array) {
                if (!this.current.needsComma) {
                    this.current.needsComma = true;
                } else {
                    this.writer.write(44);
                }
            } else if (!this.named) {
                throw new IllegalStateException("Name must be set.");
            } else {
                this.named = false;
            }
        }
    }

    public JsonWriter object(String name) throws IOException {
        return name(name).object();
    }

    public JsonWriter array(String name) throws IOException {
        return name(name).array();
    }

    public JsonWriter set(String name, Object value) throws IOException {
        return name(name).value(value);
    }

    public JsonWriter json(String name, String json) throws IOException {
        return name(name).json(json);
    }

    public JsonWriter pop() throws IOException {
        if (this.named) {
            throw new IllegalStateException("Expected an object, array, or value since a name was set.");
        }
        this.stack.pop().close();
        this.current = this.stack.size == 0 ? null : this.stack.peek();
        return this;
    }

    public void write(char[] cbuf, int off, int len) throws IOException {
        this.writer.write(cbuf, off, len);
    }

    public void flush() throws IOException {
        this.writer.flush();
    }

    public void close() throws IOException {
        while (this.stack.size > 0) {
            pop();
        }
        this.writer.close();
    }

    private class JsonObject {
        final boolean array;
        boolean needsComma;

        JsonObject(boolean array2) throws IOException {
            this.array = array2;
            JsonWriter.this.writer.write(array2 ? 91 : 123);
        }

        /* access modifiers changed from: package-private */
        public void close() throws IOException {
            JsonWriter.this.writer.write(this.array ? 93 : 125);
        }
    }

    public enum OutputType {
        json,
        javascript,
        minimal;
        
        private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$utils$JsonWriter$OutputType;
        private static Pattern javascriptPattern = Pattern.compile("^[a-zA-Z_$][a-zA-Z_$0-9]*$");
        private static Pattern minimalNamePattern = Pattern.compile("^[^\":,}/ ][^:]*$");
        private static Pattern minimalValuePattern = Pattern.compile("^[^\":,{\\[\\]/ ][^}\\],]*$");

        public String quoteValue(Object value) {
            int length;
            if (value == null) {
                return "null";
            }
            String string = value.toString();
            if ((value instanceof Number) || (value instanceof Boolean)) {
                return string;
            }
            String string2 = string.replace("\\", "\\\\").replace("\r", "\\r").replace("\n", "\\n").replace("\t", "\\t");
            return (this != minimal || string2.equals("true") || string2.equals("false") || string2.equals("null") || string2.contains("//") || string2.contains("/*") || (length = string2.length()) <= 0 || string2.charAt(length + -1) == ' ' || !minimalValuePattern.matcher(string2).matches()) ? String.valueOf('\"') + string2.replace("\"", "\\\"") + '\"' : string2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String quoteName(java.lang.String r5) {
            /*
                r4 = this;
                r3 = 34
                java.lang.String r0 = "\\"
                java.lang.String r1 = "\\\\"
                java.lang.String r0 = r5.replace(r0, r1)
                java.lang.String r1 = "\r"
                java.lang.String r2 = "\\r"
                java.lang.String r0 = r0.replace(r1, r2)
                java.lang.String r1 = "\n"
                java.lang.String r2 = "\\n"
                java.lang.String r0 = r0.replace(r1, r2)
                java.lang.String r1 = "\t"
                java.lang.String r2 = "\\t"
                java.lang.String r5 = r0.replace(r1, r2)
                int[] r0 = $SWITCH_TABLE$com$badlogic$gdx$utils$JsonWriter$OutputType()
                int r1 = r4.ordinal()
                r0 = r0[r1]
                switch(r0) {
                    case 2: goto L_0x0069;
                    case 3: goto L_0x004d;
                    default: goto L_0x002f;
                }
            L_0x002f:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                java.lang.String r1 = java.lang.String.valueOf(r3)
                r0.<init>(r1)
                java.lang.String r1 = "\""
                java.lang.String r2 = "\\\""
                java.lang.String r1 = r5.replace(r1, r2)
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.StringBuilder r0 = r0.append(r3)
                java.lang.String r5 = r0.toString()
            L_0x004c:
                return r5
            L_0x004d:
                java.lang.String r0 = "//"
                boolean r0 = r5.contains(r0)
                if (r0 != 0) goto L_0x0069
                java.lang.String r0 = "/*"
                boolean r0 = r5.contains(r0)
                if (r0 != 0) goto L_0x0069
                java.util.regex.Pattern r0 = com.badlogic.gdx.utils.JsonWriter.OutputType.minimalNamePattern
                java.util.regex.Matcher r0 = r0.matcher(r5)
                boolean r0 = r0.matches()
                if (r0 != 0) goto L_0x004c
            L_0x0069:
                java.util.regex.Pattern r0 = com.badlogic.gdx.utils.JsonWriter.OutputType.javascriptPattern
                java.util.regex.Matcher r0 = r0.matcher(r5)
                boolean r0 = r0.matches()
                if (r0 == 0) goto L_0x002f
                goto L_0x004c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.JsonWriter.OutputType.quoteName(java.lang.String):java.lang.String");
        }
    }
}
