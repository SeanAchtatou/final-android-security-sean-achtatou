package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.Vector2;

public enum Scaling {
    fit,
    fill,
    fillX,
    fillY,
    stretch,
    stretchX,
    stretchY,
    none;
    
    private static /* synthetic */ int[] $SWITCH_TABLE$com$badlogic$gdx$utils$Scaling;
    private static final Vector2 temp = new Vector2();

    public Vector2 apply(float sourceWidth, float sourceHeight, float targetWidth, float targetHeight) {
        switch ($SWITCH_TABLE$com$badlogic$gdx$utils$Scaling()[ordinal()]) {
            case 1:
                float scale = targetHeight / targetWidth > sourceHeight / sourceWidth ? targetWidth / sourceWidth : targetHeight / sourceHeight;
                temp.x = sourceWidth * scale;
                temp.y = sourceHeight * scale;
                break;
            case 2:
                float scale2 = targetHeight / targetWidth < sourceHeight / sourceWidth ? targetWidth / sourceWidth : targetHeight / sourceHeight;
                temp.x = sourceWidth * scale2;
                temp.y = sourceHeight * scale2;
                break;
            case 3:
                float scale3 = targetWidth / sourceWidth;
                temp.x = sourceWidth * scale3;
                temp.y = sourceHeight * scale3;
                break;
            case 4:
                float scale4 = targetHeight / sourceHeight;
                temp.x = sourceWidth * scale4;
                temp.y = sourceHeight * scale4;
                break;
            case 5:
                temp.x = targetWidth;
                temp.y = targetHeight;
                break;
            case 6:
                temp.x = targetWidth;
                temp.y = sourceHeight;
                break;
            case 7:
                temp.x = sourceWidth;
                temp.y = targetHeight;
                break;
            case 8:
                temp.x = sourceWidth;
                temp.y = sourceHeight;
                break;
        }
        return temp;
    }
}
