package com.badlogic.gdx.utils;

import com.badlogic.gdx.files.FileHandle;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

public class I18NBundle {
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final Locale ROOT_LOCALE = new Locale("", "", "");
    private static boolean exceptionOnMissingKey = true;
    private static boolean simpleFormatter = false;
    private TextFormatter formatter;
    private Locale locale;
    private I18NBundle parent;
    private ObjectMap<String, String> properties;

    public static boolean getSimpleFormatter() {
        return simpleFormatter;
    }

    public static void setSimpleFormatter(boolean enabled) {
        simpleFormatter = enabled;
    }

    public static boolean getExceptionOnMissingKey() {
        return exceptionOnMissingKey;
    }

    public static void setExceptionOnMissingKey(boolean enabled) {
        exceptionOnMissingKey = enabled;
    }

    public static I18NBundle createBundle(FileHandle baseFileHandle) {
        return createBundleImpl(baseFileHandle, Locale.getDefault(), DEFAULT_ENCODING);
    }

    public static I18NBundle createBundle(FileHandle baseFileHandle, Locale locale2) {
        return createBundleImpl(baseFileHandle, locale2, DEFAULT_ENCODING);
    }

    public static I18NBundle createBundle(FileHandle baseFileHandle, String encoding) {
        return createBundleImpl(baseFileHandle, Locale.getDefault(), encoding);
    }

    public static I18NBundle createBundle(FileHandle baseFileHandle, Locale locale2, String encoding) {
        return createBundleImpl(baseFileHandle, locale2, encoding);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.badlogic.gdx.utils.I18NBundle createBundleImpl(com.badlogic.gdx.files.FileHandle r10, java.util.Locale r11, java.lang.String r12) {
        /*
            r8 = 0
            if (r10 == 0) goto L_0x0007
            if (r11 == 0) goto L_0x0007
            if (r12 != 0) goto L_0x000d
        L_0x0007:
            java.lang.NullPointerException r6 = new java.lang.NullPointerException
            r6.<init>()
            throw r6
        L_0x000d:
            r1 = 0
            r0 = 0
            r5 = r11
        L_0x0010:
            java.util.List r3 = getCandidateLocales(r5)
            com.badlogic.gdx.utils.I18NBundle r1 = loadBundleChain(r10, r12, r3, r8, r0)
            if (r1 == 0) goto L_0x0082
            java.util.Locale r2 = r1.getLocale()
            java.util.Locale r6 = com.badlogic.gdx.utils.I18NBundle.ROOT_LOCALE
            boolean r4 = r2.equals(r6)
            if (r4 == 0) goto L_0x002c
            boolean r6 = r2.equals(r11)
            if (r6 == 0) goto L_0x006c
        L_0x002c:
            if (r1 != 0) goto L_0x008a
            if (r0 != 0) goto L_0x0089
            java.util.MissingResourceException r6 = new java.util.MissingResourceException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Can't find bundle for base file handle "
            r7.<init>(r8)
            java.lang.String r8 = r10.path()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = ", locale "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r7 = r7.toString()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.StringBuilder r8 = r8.append(r10)
            java.lang.String r9 = "_"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r11)
            java.lang.String r8 = r8.toString()
            java.lang.String r9 = ""
            r6.<init>(r7, r8, r9)
            throw r6
        L_0x006c:
            int r6 = r3.size()
            r7 = 1
            if (r6 != r7) goto L_0x007d
            java.lang.Object r6 = r3.get(r8)
            boolean r6 = r2.equals(r6)
            if (r6 != 0) goto L_0x002c
        L_0x007d:
            if (r4 == 0) goto L_0x0082
            if (r0 != 0) goto L_0x0082
            r0 = r1
        L_0x0082:
            java.util.Locale r5 = getFallbackLocale(r5)
            if (r5 != 0) goto L_0x0010
            goto L_0x002c
        L_0x0089:
            r1 = r0
        L_0x008a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.I18NBundle.createBundleImpl(com.badlogic.gdx.files.FileHandle, java.util.Locale, java.lang.String):com.badlogic.gdx.utils.I18NBundle");
    }

    private static List<Locale> getCandidateLocales(Locale locale2) {
        String language = locale2.getLanguage();
        String country = locale2.getCountry();
        String variant = locale2.getVariant();
        List<Locale> locales = new ArrayList<>(4);
        if (variant.length() > 0) {
            locales.add(locale2);
        }
        if (country.length() > 0) {
            locales.add(locales.size() == 0 ? locale2 : new Locale(language, country));
        }
        if (language.length() > 0) {
            if (locales.size() != 0) {
                locale2 = new Locale(language);
            }
            locales.add(locale2);
        }
        locales.add(ROOT_LOCALE);
        return locales;
    }

    private static Locale getFallbackLocale(Locale locale2) {
        Locale defaultLocale = Locale.getDefault();
        if (locale2.equals(defaultLocale)) {
            return null;
        }
        return defaultLocale;
    }

    private static I18NBundle loadBundleChain(FileHandle baseFileHandle, String encoding, List<Locale> candidateLocales, int candidateIndex, I18NBundle baseBundle) {
        Locale targetLocale = candidateLocales.get(candidateIndex);
        I18NBundle parent2 = null;
        if (candidateIndex != candidateLocales.size() - 1) {
            parent2 = loadBundleChain(baseFileHandle, encoding, candidateLocales, candidateIndex + 1, baseBundle);
        } else if (baseBundle != null && targetLocale.equals(ROOT_LOCALE)) {
            return baseBundle;
        }
        I18NBundle bundle = loadBundle(baseFileHandle, encoding, targetLocale);
        if (bundle == null) {
            return parent2;
        }
        bundle.parent = parent2;
        return bundle;
    }

    private static I18NBundle loadBundle(FileHandle baseFileHandle, String encoding, Locale targetLocale) {
        I18NBundle bundle = null;
        Reader reader = null;
        try {
            FileHandle fileHandle = toFileHandle(baseFileHandle, targetLocale);
            if (checkFileExistence(fileHandle)) {
                I18NBundle bundle2 = new I18NBundle();
                try {
                    reader = fileHandle.reader(encoding);
                    bundle2.load(reader);
                    bundle = bundle2;
                } catch (IOException e) {
                    e = e;
                    try {
                        throw new GdxRuntimeException(e);
                    } catch (Throwable th) {
                        th = th;
                        StreamUtils.closeQuietly(reader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    StreamUtils.closeQuietly(reader);
                    throw th;
                }
            }
            StreamUtils.closeQuietly(reader);
            if (bundle != null) {
                bundle.setLocale(targetLocale);
            }
            return bundle;
        } catch (IOException e2) {
            e = e2;
            throw new GdxRuntimeException(e);
        }
    }

    private static boolean checkFileExistence(FileHandle fh) {
        try {
            fh.read().close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void load(Reader reader) throws IOException {
        this.properties = new ObjectMap<>();
        PropertiesUtils.load(this.properties, reader);
    }

    private static FileHandle toFileHandle(FileHandle baseFileHandle, Locale locale2) {
        StringBuilder sb = new StringBuilder(baseFileHandle.name());
        if (!locale2.equals(ROOT_LOCALE)) {
            String language = locale2.getLanguage();
            String country = locale2.getCountry();
            String variant = locale2.getVariant();
            boolean emptyLanguage = "".equals(language);
            boolean emptyCountry = "".equals(country);
            boolean emptyVariant = "".equals(variant);
            if (!emptyLanguage || !emptyCountry || !emptyVariant) {
                sb.append('_');
                if (!emptyVariant) {
                    sb.append(language).append('_').append(country).append('_').append(variant);
                } else if (!emptyCountry) {
                    sb.append(language).append('_').append(country);
                } else {
                    sb.append(language);
                }
            }
        }
        return baseFileHandle.sibling(sb.append(".properties").toString());
    }

    public Locale getLocale() {
        return this.locale;
    }

    private void setLocale(Locale locale2) {
        this.locale = locale2;
        this.formatter = new TextFormatter(locale2, !simpleFormatter);
    }

    public final String get(String key) {
        String result = this.properties.get(key);
        if (result == null) {
            if (this.parent != null) {
                result = this.parent.get(key);
            }
            if (result == null) {
                if (!exceptionOnMissingKey) {
                    return "???" + key + "???";
                }
                throw new MissingResourceException("Can't find bundle key " + key, getClass().getName(), key);
            }
        }
        return result;
    }

    public String format(String key, Object... args) {
        return this.formatter.format(get(key), args);
    }
}
