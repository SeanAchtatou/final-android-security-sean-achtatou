package com.badlogic.gdx.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;
import com.sg.GameSprites.GameSpriteType;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import org.objectweb.asm.signature.SignatureVisitor;

public class XmlReader {
    private static final byte[] _xml_actions = init__xml_actions_0();
    private static final short[] _xml_index_offsets = init__xml_index_offsets_0();
    private static final byte[] _xml_indicies = init__xml_indicies_0();
    private static final byte[] _xml_key_offsets = init__xml_key_offsets_0();
    private static final byte[] _xml_range_lengths = init__xml_range_lengths_0();
    private static final byte[] _xml_single_lengths = init__xml_single_lengths_0();
    private static final byte[] _xml_trans_actions = init__xml_trans_actions_0();
    private static final char[] _xml_trans_keys = init__xml_trans_keys_0();
    private static final byte[] _xml_trans_targs = init__xml_trans_targs_0();
    static final int xml_en_elementBody = 15;
    static final int xml_en_main = 1;
    static final int xml_error = 0;
    static final int xml_first_final = 34;
    static final int xml_start = 1;
    private Element current;
    private final Array<Element> elements = new Array<>(8);
    private Element root;
    private final StringBuilder textBuffer = new StringBuilder(64);

    public Element parse(String xml) {
        char[] data = xml.toCharArray();
        return parse(data, 0, data.length);
    }

    public Element parse(Reader reader) throws IOException {
        try {
            char[] data = new char[1024];
            int offset = 0;
            while (true) {
                int length = reader.read(data, offset, data.length - offset);
                if (length == -1) {
                    Element parse = parse(data, 0, offset);
                    StreamUtils.closeQuietly(reader);
                    return parse;
                } else if (length == 0) {
                    char[] newData = new char[(data.length * 2)];
                    System.arraycopy(data, 0, newData, 0, data.length);
                    data = newData;
                } else {
                    offset += length;
                }
            }
        } catch (IOException ex) {
            throw new SerializationException(ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(reader);
            throw th;
        }
    }

    public Element parse(InputStream input) throws IOException {
        try {
            Element parse = parse(new InputStreamReader(input, "UTF-8"));
            StreamUtils.closeQuietly(input);
            return parse;
        } catch (IOException ex) {
            throw new SerializationException(ex);
        } catch (Throwable th) {
            StreamUtils.closeQuietly(input);
            throw th;
        }
    }

    public Element parse(FileHandle file) throws IOException {
        try {
            return parse(file.reader("UTF-8"));
        } catch (Exception ex) {
            throw new SerializationException("Error parsing file: " + file, ex);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x000d, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0132, code lost:
        if (r39[r31 + 1] != '[') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x013e, code lost:
        if (r39[r31 + 2] != 'C') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x014a, code lost:
        if (r39[r31 + 3] != 'D') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0156, code lost:
        if (r39[r31 + 4] != 'A') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0162, code lost:
        if (r39[r31 + 5] != 'T') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x016e, code lost:
        if (r39[r31 + 6] != 'A') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x017a, code lost:
        if (r39[r31 + 7] != '[') goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x017c, code lost:
        r31 = r31 + 8;
        r28 = r31 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x018a, code lost:
        if (r39[r28 - 2] != ']') goto L_0x01bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0196, code lost:
        if (r39[r28 - 1] != ']') goto L_0x01bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01a0, code lost:
        if (r39[r28] != '>') goto L_0x01bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01a2, code lost:
        text(new java.lang.String(r39, r31, (r28 - r31) - 2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01ba, code lost:
        r17 = 15;
        r6 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01bf, code lost:
        r28 = r28 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01c8, code lost:
        if (r16 != '!') goto L_0x020e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01d4, code lost:
        if (r39[r31 + 1] != '-') goto L_0x020e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01e0, code lost:
        if (r39[r31 + 2] != '-') goto L_0x020e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01e2, code lost:
        r28 = r31 + 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01ec, code lost:
        if (r39[r28] != '-') goto L_0x0209;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01f8, code lost:
        if (r39[r28 + 1] != '-') goto L_0x0209;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0204, code lost:
        if (r39[r28 + 2] != '>') goto L_0x0209;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0206, code lost:
        r28 = r28 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0209, code lost:
        r28 = r28 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x020c, code lost:
        r28 = r28 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0216, code lost:
        if (r39[r28] != '>') goto L_0x020c;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x028f  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x02d3  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0343  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.XmlReader.Element parse(char[] r39, int r40, int r41) {
        /*
            r38 = this;
            r28 = r40
            r29 = r41
            r31 = 0
            r15 = 0
            r24 = 0
            r17 = 1
            r13 = 0
            r6 = 0
        L_0x000d:
            switch(r6) {
                case 0: goto L_0x0058;
                case 1: goto L_0x0064;
                case 2: goto L_0x00a8;
                default: goto L_0x0010;
            }
        L_0x0010:
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x0376
            r26 = 1
            r25 = 0
        L_0x001a:
            r0 = r25
            r1 = r28
            if (r0 < r1) goto L_0x0366
            com.badlogic.gdx.utils.SerializationException r33 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            java.lang.String r35 = "Error parsing XML on line "
            r34.<init>(r35)
            r0 = r34
            r1 = r26
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r35 = " near: "
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r35 = new java.lang.String
            r36 = 32
            int r37 = r29 - r28
            int r36 = java.lang.Math.min(r36, r37)
            r0 = r35
            r1 = r39
            r2 = r28
            r3 = r36
            r0.<init>(r1, r2, r3)
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r34 = r34.toString()
            r33.<init>(r34)
            throw r33
        L_0x0058:
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x0060
            r6 = 4
            goto L_0x000d
        L_0x0060:
            if (r17 != 0) goto L_0x0064
            r6 = 5
            goto L_0x000d
        L_0x0064:
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_key_offsets
            byte r7 = r33[r17]
            short[] r33 = com.badlogic.gdx.utils.XmlReader._xml_index_offsets
            short r13 = r33[r17]
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_single_lengths
            byte r8 = r33[r17]
            if (r8 <= 0) goto L_0x007b
            r9 = r7
            int r33 = r7 + r8
            int r14 = r33 + -1
        L_0x0077:
            if (r14 >= r9) goto L_0x00ad
            int r7 = r7 + r8
            int r13 = r13 + r8
        L_0x007b:
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_range_lengths
            byte r8 = r33[r17]
            if (r8 <= 0) goto L_0x008b
            r9 = r7
            int r33 = r8 << 1
            int r33 = r33 + r7
            int r14 = r33 + -2
        L_0x0088:
            if (r14 >= r9) goto L_0x00d6
            int r13 = r13 + r8
        L_0x008b:
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_indicies
            byte r13 = r33[r13]
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_trans_targs
            byte r17 = r33[r13]
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_trans_actions
            byte r33 = r33[r13]
            if (r33 == 0) goto L_0x00a8
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_trans_actions
            byte r4 = r33[r13]
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_actions
            int r5 = r4 + 1
            byte r11 = r33[r4]
            r12 = r11
        L_0x00a4:
            int r11 = r12 + -1
            if (r12 > 0) goto L_0x0105
        L_0x00a8:
            if (r17 != 0) goto L_0x035b
            r6 = 5
            goto L_0x000d
        L_0x00ad:
            int r33 = r14 - r9
            int r33 = r33 >> 1
            int r10 = r9 + r33
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.XmlReader._xml_trans_keys
            char r34 = r34[r10]
            r0 = r33
            r1 = r34
            if (r0 >= r1) goto L_0x00c2
            int r14 = r10 + -1
            goto L_0x0077
        L_0x00c2:
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.XmlReader._xml_trans_keys
            char r34 = r34[r10]
            r0 = r33
            r1 = r34
            if (r0 <= r1) goto L_0x00d1
            int r9 = r10 + 1
            goto L_0x0077
        L_0x00d1:
            int r33 = r10 - r7
            int r13 = r13 + r33
            goto L_0x008b
        L_0x00d6:
            int r33 = r14 - r9
            int r33 = r33 >> 1
            r33 = r33 & -2
            int r10 = r9 + r33
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.XmlReader._xml_trans_keys
            char r34 = r34[r10]
            r0 = r33
            r1 = r34
            if (r0 >= r1) goto L_0x00ed
            int r14 = r10 + -2
            goto L_0x0088
        L_0x00ed:
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.XmlReader._xml_trans_keys
            int r35 = r10 + 1
            char r34 = r34[r35]
            r0 = r33
            r1 = r34
            if (r0 <= r1) goto L_0x00fe
            int r9 = r10 + 2
            goto L_0x0088
        L_0x00fe:
            int r33 = r10 - r7
            int r33 = r33 >> 1
            int r13 = r13 + r33
            goto L_0x008b
        L_0x0105:
            byte[] r33 = com.badlogic.gdx.utils.XmlReader._xml_actions
            int r4 = r5 + 1
            byte r33 = r33[r5]
            switch(r33) {
                case 0: goto L_0x0111;
                case 1: goto L_0x0116;
                case 2: goto L_0x0235;
                case 3: goto L_0x023f;
                case 4: goto L_0x0247;
                case 5: goto L_0x024e;
                case 6: goto L_0x025f;
                case 7: goto L_0x0279;
                default: goto L_0x010e;
            }
        L_0x010e:
            r12 = r11
            r5 = r4
            goto L_0x00a4
        L_0x0111:
            r31 = r28
            r12 = r11
            r5 = r4
            goto L_0x00a4
        L_0x0116:
            char r16 = r39[r31]
            r33 = 63
            r0 = r16
            r1 = r33
            if (r0 == r1) goto L_0x0128
            r33 = 33
            r0 = r16
            r1 = r33
            if (r0 != r1) goto L_0x0219
        L_0x0128:
            int r33 = r31 + 1
            char r33 = r39[r33]
            r34 = 91
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r33 = r31 + 2
            char r33 = r39[r33]
            r34 = 67
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r33 = r31 + 3
            char r33 = r39[r33]
            r34 = 68
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r33 = r31 + 4
            char r33 = r39[r33]
            r34 = 65
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r33 = r31 + 5
            char r33 = r39[r33]
            r34 = 84
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r33 = r31 + 6
            char r33 = r39[r33]
            r34 = 65
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r33 = r31 + 7
            char r33 = r39[r33]
            r34 = 91
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01c2
            int r31 = r31 + 8
            int r28 = r31 + 2
        L_0x0180:
            int r33 = r28 + -2
            char r33 = r39[r33]
            r34 = 93
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01bf
            int r33 = r28 + -1
            char r33 = r39[r33]
            r34 = 93
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01bf
            char r33 = r39[r28]
            r34 = 62
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01bf
            java.lang.String r33 = new java.lang.String
            int r34 = r28 - r31
            int r34 = r34 + -2
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.text(r1)
        L_0x01ba:
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x01bf:
            int r28 = r28 + 1
            goto L_0x0180
        L_0x01c2:
            r33 = 33
            r0 = r16
            r1 = r33
            if (r0 != r1) goto L_0x020e
            int r33 = r31 + 1
            char r33 = r39[r33]
            r34 = 45
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x020e
            int r33 = r31 + 2
            char r33 = r39[r33]
            r34 = 45
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x020e
            int r28 = r31 + 3
        L_0x01e4:
            char r33 = r39[r28]
            r34 = 45
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x0209
            int r33 = r28 + 1
            char r33 = r39[r33]
            r34 = 45
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x0209
            int r33 = r28 + 2
            char r33 = r39[r33]
            r34 = 62
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x0209
            int r28 = r28 + 2
            goto L_0x01ba
        L_0x0209:
            int r28 = r28 + 1
            goto L_0x01e4
        L_0x020c:
            int r28 = r28 + 1
        L_0x020e:
            char r33 = r39[r28]
            r34 = 62
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x020c
            goto L_0x01ba
        L_0x0219:
            r24 = 1
            java.lang.String r33 = new java.lang.String
            int r34 = r28 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.open(r1)
            r12 = r11
            r5 = r4
            goto L_0x00a4
        L_0x0235:
            r24 = 0
            r38.close()
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x023f:
            r38.close()
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x0247:
            if (r24 == 0) goto L_0x010e
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x024e:
            java.lang.String r15 = new java.lang.String
            int r33 = r28 - r31
            r0 = r39
            r1 = r31
            r2 = r33
            r15.<init>(r0, r1, r2)
            r12 = r11
            r5 = r4
            goto L_0x00a4
        L_0x025f:
            java.lang.String r33 = new java.lang.String
            int r34 = r28 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.attribute(r15, r1)
            r12 = r11
            r5 = r4
            goto L_0x00a4
        L_0x0279:
            r21 = r28
        L_0x027b:
            r0 = r21
            r1 = r31
            if (r0 != r1) goto L_0x02c8
        L_0x0281:
            r18 = r31
            r22 = 0
            r19 = r18
        L_0x0287:
            r0 = r19
            r1 = r21
            if (r0 != r1) goto L_0x02d3
            if (r22 == 0) goto L_0x0343
            r0 = r31
            r1 = r21
            if (r0 >= r1) goto L_0x02a8
            r0 = r38
            com.badlogic.gdx.utils.StringBuilder r0 = r0.textBuffer
            r33 = r0
            int r34 = r21 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.append(r1, r2, r3)
        L_0x02a8:
            r0 = r38
            com.badlogic.gdx.utils.StringBuilder r0 = r0.textBuffer
            r33 = r0
            java.lang.String r33 = r33.toString()
            r0 = r38
            r1 = r33
            r0.text(r1)
            r0 = r38
            com.badlogic.gdx.utils.StringBuilder r0 = r0.textBuffer
            r33 = r0
            r34 = 0
            r33.setLength(r34)
            r12 = r11
            r5 = r4
            goto L_0x00a4
        L_0x02c8:
            int r33 = r21 + -1
            char r33 = r39[r33]
            switch(r33) {
                case 9: goto L_0x02d0;
                case 10: goto L_0x02d0;
                case 13: goto L_0x02d0;
                case 32: goto L_0x02d0;
                default: goto L_0x02cf;
            }
        L_0x02cf:
            goto L_0x0281
        L_0x02d0:
            int r21 = r21 + -1
            goto L_0x027b
        L_0x02d3:
            int r18 = r19 + 1
            char r33 = r39[r19]
            r34 = 38
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x02e2
            r19 = r18
            goto L_0x0287
        L_0x02e2:
            r23 = r18
            r19 = r18
        L_0x02e6:
            r0 = r19
            r1 = r21
            if (r0 == r1) goto L_0x0287
            int r18 = r19 + 1
            char r33 = r39[r19]
            r34 = 59
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x02fb
            r19 = r18
            goto L_0x02e6
        L_0x02fb:
            r0 = r38
            com.badlogic.gdx.utils.StringBuilder r0 = r0.textBuffer
            r33 = r0
            int r34 = r23 - r31
            int r34 = r34 + -1
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.append(r1, r2, r3)
            java.lang.String r27 = new java.lang.String
            int r33 = r18 - r23
            int r33 = r33 + -1
            r0 = r27
            r1 = r39
            r2 = r23
            r3 = r33
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r27
            java.lang.String r32 = r0.entity(r1)
            r0 = r38
            com.badlogic.gdx.utils.StringBuilder r0 = r0.textBuffer
            r33 = r0
            if (r32 == 0) goto L_0x0340
        L_0x0331:
            r0 = r33
            r1 = r32
            r0.append(r1)
            r31 = r18
            r22 = 1
            r19 = r18
            goto L_0x0287
        L_0x0340:
            r32 = r27
            goto L_0x0331
        L_0x0343:
            java.lang.String r33 = new java.lang.String
            int r34 = r21 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.text(r1)
            goto L_0x010e
        L_0x035b:
            int r28 = r28 + 1
            r0 = r28
            r1 = r29
            if (r0 == r1) goto L_0x0010
            r6 = 1
            goto L_0x000d
        L_0x0366:
            char r33 = r39[r25]
            r34 = 10
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x0372
            int r26 = r26 + 1
        L_0x0372:
            int r25 = r25 + 1
            goto L_0x001a
        L_0x0376:
            r0 = r38
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.XmlReader$Element> r0 = r0.elements
            r33 = r0
            r0 = r33
            int r0 = r0.size
            r33 = r0
            if (r33 == 0) goto L_0x03b2
            r0 = r38
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.XmlReader$Element> r0 = r0.elements
            r33 = r0
            java.lang.Object r20 = r33.peek()
            com.badlogic.gdx.utils.XmlReader$Element r20 = (com.badlogic.gdx.utils.XmlReader.Element) r20
            r0 = r38
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.XmlReader$Element> r0 = r0.elements
            r33 = r0
            r33.clear()
            com.badlogic.gdx.utils.SerializationException r33 = new com.badlogic.gdx.utils.SerializationException
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            java.lang.String r35 = "Error parsing XML, unclosed element: "
            r34.<init>(r35)
            java.lang.String r35 = r20.getName()
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r34 = r34.toString()
            r33.<init>(r34)
            throw r33
        L_0x03b2:
            r0 = r38
            com.badlogic.gdx.utils.XmlReader$Element r0 = r0.root
            r30 = r0
            r33 = 0
            r0 = r33
            r1 = r38
            r1.root = r0
            return r30
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.XmlReader.parse(char[], int, int):com.badlogic.gdx.utils.XmlReader$Element");
    }

    private static byte[] init__xml_actions_0() {
        byte[] bArr = new byte[26];
        bArr[1] = 1;
        bArr[3] = 1;
        bArr[4] = 1;
        bArr[5] = 1;
        bArr[6] = 2;
        bArr[7] = 1;
        bArr[8] = 3;
        bArr[9] = 1;
        bArr[10] = 4;
        bArr[11] = 1;
        bArr[12] = 5;
        bArr[13] = 1;
        bArr[14] = 6;
        bArr[15] = 1;
        bArr[16] = 7;
        bArr[17] = 2;
        bArr[19] = 6;
        bArr[20] = 2;
        bArr[21] = 1;
        bArr[22] = 4;
        bArr[23] = 2;
        bArr[24] = 2;
        bArr[25] = 4;
        return bArr;
    }

    private static byte[] init__xml_key_offsets_0() {
        byte[] bArr = new byte[36];
        bArr[2] = 4;
        bArr[3] = 9;
        bArr[4] = 14;
        bArr[5] = 20;
        bArr[6] = 26;
        bArr[7] = 30;
        bArr[8] = 35;
        bArr[9] = 36;
        bArr[10] = 37;
        bArr[11] = 42;
        bArr[12] = 46;
        bArr[13] = GameSpriteType.f3TYPE_BOSS_2;
        bArr[14] = 51;
        bArr[15] = GameSpriteType.f56TYPE_ENEMY_;
        bArr[16] = 56;
        bArr[17] = 57;
        bArr[18] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[19] = 67;
        bArr[20] = 73;
        bArr[21] = 79;
        bArr[22] = 83;
        bArr[23] = 88;
        bArr[24] = 89;
        bArr[25] = 90;
        bArr[26] = 95;
        bArr[27] = 99;
        bArr[28] = 103;
        bArr[29] = 104;
        bArr[30] = 108;
        bArr[31] = 109;
        bArr[32] = 110;
        bArr[33] = 111;
        bArr[34] = 112;
        bArr[35] = 115;
        return bArr;
    }

    private static char[] init__xml_trans_keys_0() {
        char[] cArr = new char[116];
        cArr[0] = ' ';
        cArr[1] = '<';
        cArr[2] = 9;
        cArr[3] = 13;
        cArr[4] = ' ';
        cArr[5] = '/';
        cArr[6] = '>';
        cArr[7] = 9;
        cArr[8] = 13;
        cArr[9] = ' ';
        cArr[10] = '/';
        cArr[11] = '>';
        cArr[12] = 9;
        cArr[13] = 13;
        cArr[14] = ' ';
        cArr[15] = '/';
        cArr[16] = SignatureVisitor.INSTANCEOF;
        cArr[17] = '>';
        cArr[18] = 9;
        cArr[19] = 13;
        cArr[20] = ' ';
        cArr[21] = '/';
        cArr[22] = SignatureVisitor.INSTANCEOF;
        cArr[23] = '>';
        cArr[24] = 9;
        cArr[25] = 13;
        cArr[26] = ' ';
        cArr[27] = SignatureVisitor.INSTANCEOF;
        cArr[28] = 9;
        cArr[29] = 13;
        cArr[30] = ' ';
        cArr[31] = '\"';
        cArr[32] = '\'';
        cArr[33] = 9;
        cArr[34] = 13;
        cArr[35] = '\"';
        cArr[36] = '\"';
        cArr[37] = ' ';
        cArr[38] = '/';
        cArr[39] = '>';
        cArr[40] = 9;
        cArr[41] = 13;
        cArr[42] = ' ';
        cArr[43] = '>';
        cArr[44] = 9;
        cArr[45] = 13;
        cArr[46] = ' ';
        cArr[47] = '>';
        cArr[48] = 9;
        cArr[49] = 13;
        cArr[50] = '\'';
        cArr[51] = '\'';
        cArr[52] = ' ';
        cArr[53] = '<';
        cArr[54] = 9;
        cArr[55] = 13;
        cArr[56] = '<';
        cArr[57] = ' ';
        cArr[58] = '/';
        cArr[59] = '>';
        cArr[60] = 9;
        cArr[61] = 13;
        cArr[62] = ' ';
        cArr[63] = '/';
        cArr[64] = '>';
        cArr[65] = 9;
        cArr[66] = 13;
        cArr[67] = ' ';
        cArr[68] = '/';
        cArr[69] = SignatureVisitor.INSTANCEOF;
        cArr[70] = '>';
        cArr[71] = 9;
        cArr[72] = 13;
        cArr[73] = ' ';
        cArr[74] = '/';
        cArr[75] = SignatureVisitor.INSTANCEOF;
        cArr[76] = '>';
        cArr[77] = 9;
        cArr[78] = 13;
        cArr[79] = ' ';
        cArr[80] = SignatureVisitor.INSTANCEOF;
        cArr[81] = 9;
        cArr[82] = 13;
        cArr[83] = ' ';
        cArr[84] = '\"';
        cArr[85] = '\'';
        cArr[86] = 9;
        cArr[87] = 13;
        cArr[88] = '\"';
        cArr[89] = '\"';
        cArr[90] = ' ';
        cArr[91] = '/';
        cArr[92] = '>';
        cArr[93] = 9;
        cArr[94] = 13;
        cArr[95] = ' ';
        cArr[96] = '>';
        cArr[97] = 9;
        cArr[98] = 13;
        cArr[99] = ' ';
        cArr[100] = '>';
        cArr[101] = 9;
        cArr[102] = 13;
        cArr[103] = '<';
        cArr[104] = ' ';
        cArr[105] = '/';
        cArr[106] = 9;
        cArr[107] = 13;
        cArr[108] = '>';
        cArr[109] = '>';
        cArr[110] = '\'';
        cArr[111] = '\'';
        cArr[112] = ' ';
        cArr[113] = 9;
        cArr[114] = 13;
        return cArr;
    }

    private static byte[] init__xml_single_lengths_0() {
        byte[] bArr = new byte[36];
        bArr[1] = 2;
        bArr[2] = 3;
        bArr[3] = 3;
        bArr[4] = 4;
        bArr[5] = 4;
        bArr[6] = 2;
        bArr[7] = 3;
        bArr[8] = 1;
        bArr[9] = 1;
        bArr[10] = 3;
        bArr[11] = 2;
        bArr[12] = 2;
        bArr[13] = 1;
        bArr[14] = 1;
        bArr[15] = 2;
        bArr[16] = 1;
        bArr[17] = 3;
        bArr[18] = 3;
        bArr[19] = 4;
        bArr[20] = 4;
        bArr[21] = 2;
        bArr[22] = 3;
        bArr[23] = 1;
        bArr[24] = 1;
        bArr[25] = 3;
        bArr[26] = 2;
        bArr[27] = 2;
        bArr[28] = 1;
        bArr[29] = 2;
        bArr[30] = 1;
        bArr[31] = 1;
        bArr[32] = 1;
        bArr[33] = 1;
        bArr[34] = 1;
        return bArr;
    }

    private static byte[] init__xml_range_lengths_0() {
        byte[] bArr = new byte[36];
        bArr[1] = 1;
        bArr[2] = 1;
        bArr[3] = 1;
        bArr[4] = 1;
        bArr[5] = 1;
        bArr[6] = 1;
        bArr[7] = 1;
        bArr[10] = 1;
        bArr[11] = 1;
        bArr[12] = 1;
        bArr[15] = 1;
        bArr[17] = 1;
        bArr[18] = 1;
        bArr[19] = 1;
        bArr[20] = 1;
        bArr[21] = 1;
        bArr[22] = 1;
        bArr[25] = 1;
        bArr[26] = 1;
        bArr[27] = 1;
        bArr[29] = 1;
        bArr[34] = 1;
        return bArr;
    }

    private static short[] init__xml_index_offsets_0() {
        short[] sArr = new short[36];
        sArr[2] = 4;
        sArr[3] = 9;
        sArr[4] = 14;
        sArr[5] = 20;
        sArr[6] = 26;
        sArr[7] = 30;
        sArr[8] = 35;
        sArr[9] = 37;
        sArr[10] = 39;
        sArr[11] = 44;
        sArr[12] = 48;
        sArr[13] = 52;
        sArr[14] = 54;
        sArr[15] = 56;
        sArr[16] = 60;
        sArr[17] = 62;
        sArr[18] = 67;
        sArr[19] = 72;
        sArr[20] = 78;
        sArr[21] = 84;
        sArr[22] = 88;
        sArr[23] = 93;
        sArr[24] = 95;
        sArr[25] = 97;
        sArr[26] = 102;
        sArr[27] = 106;
        sArr[28] = 110;
        sArr[29] = 112;
        sArr[30] = 116;
        sArr[31] = 118;
        sArr[32] = 120;
        sArr[33] = 122;
        sArr[34] = 124;
        sArr[35] = 127;
        return sArr;
    }

    private static byte[] init__xml_indicies_0() {
        byte[] bArr = new byte[129];
        bArr[1] = 2;
        bArr[3] = 1;
        bArr[4] = 2;
        bArr[5] = 1;
        bArr[6] = 1;
        bArr[7] = 2;
        bArr[8] = 3;
        bArr[9] = 5;
        bArr[10] = 6;
        bArr[11] = 7;
        bArr[12] = 5;
        bArr[13] = 4;
        bArr[14] = 9;
        bArr[15] = 10;
        bArr[16] = 1;
        bArr[17] = 11;
        bArr[18] = 9;
        bArr[19] = 8;
        bArr[20] = 13;
        bArr[21] = 1;
        bArr[22] = 14;
        bArr[23] = 1;
        bArr[24] = 13;
        bArr[25] = 12;
        bArr[26] = 15;
        bArr[27] = 16;
        bArr[28] = 15;
        bArr[29] = 1;
        bArr[30] = 16;
        bArr[31] = 17;
        bArr[32] = 18;
        bArr[33] = 16;
        bArr[34] = 1;
        bArr[35] = 20;
        bArr[36] = 19;
        bArr[37] = 22;
        bArr[38] = 21;
        bArr[39] = 9;
        bArr[40] = 10;
        bArr[41] = 11;
        bArr[42] = 9;
        bArr[43] = 1;
        bArr[44] = 23;
        bArr[45] = 24;
        bArr[46] = 23;
        bArr[47] = 1;
        bArr[48] = 25;
        bArr[49] = 11;
        bArr[50] = 25;
        bArr[51] = 1;
        bArr[52] = 20;
        bArr[53] = 26;
        bArr[54] = 22;
        bArr[55] = 27;
        bArr[56] = 29;
        bArr[57] = 30;
        bArr[58] = 29;
        bArr[59] = 28;
        bArr[60] = 32;
        bArr[61] = 31;
        bArr[62] = 30;
        bArr[63] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[64] = 1;
        bArr[65] = 30;
        bArr[66] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[67] = 36;
        bArr[68] = 37;
        bArr[69] = 38;
        bArr[70] = 36;
        bArr[71] = 35;
        bArr[72] = 40;
        bArr[73] = GameSpriteType.f28TYPE_ENEMY_2;
        bArr[74] = 1;
        bArr[75] = 42;
        bArr[76] = 40;
        bArr[77] = 39;
        bArr[78] = 44;
        bArr[79] = 1;
        bArr[80] = 45;
        bArr[81] = 1;
        bArr[82] = 44;
        bArr[83] = 43;
        bArr[84] = 46;
        bArr[85] = GameSpriteType.f2TYPE_BOSS_1;
        bArr[86] = 46;
        bArr[87] = 1;
        bArr[88] = GameSpriteType.f2TYPE_BOSS_1;
        bArr[89] = GameSpriteType.f1TYPE_BOSS_;
        bArr[90] = GameSpriteType.f0TYPE_BOSS_;
        bArr[91] = GameSpriteType.f2TYPE_BOSS_1;
        bArr[92] = 1;
        bArr[93] = 51;
        bArr[94] = GameSpriteType.f3TYPE_BOSS_2;
        bArr[95] = 53;
        bArr[96] = GameSpriteType.f56TYPE_ENEMY_;
        bArr[97] = 40;
        bArr[98] = GameSpriteType.f28TYPE_ENEMY_2;
        bArr[99] = 42;
        bArr[100] = 40;
        bArr[101] = 1;
        bArr[102] = 54;
        bArr[103] = 55;
        bArr[104] = 54;
        bArr[105] = 1;
        bArr[106] = 56;
        bArr[107] = 42;
        bArr[108] = 56;
        bArr[109] = 1;
        bArr[110] = 57;
        bArr[111] = 1;
        bArr[112] = 57;
        bArr[113] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[114] = 57;
        bArr[115] = 1;
        bArr[116] = 1;
        bArr[117] = 58;
        bArr[118] = 59;
        bArr[119] = 58;
        bArr[120] = 51;
        bArr[121] = 60;
        bArr[122] = 53;
        bArr[123] = 61;
        bArr[124] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[125] = GameSpriteType.f48TYPE_ENEMY_;
        bArr[126] = 1;
        bArr[127] = 1;
        return bArr;
    }

    private static byte[] init__xml_trans_targs_0() {
        byte[] bArr = new byte[63];
        bArr[0] = 1;
        bArr[2] = 2;
        bArr[3] = 3;
        bArr[4] = 3;
        bArr[5] = 4;
        bArr[6] = 11;
        bArr[7] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[8] = 5;
        bArr[9] = 4;
        bArr[10] = 11;
        bArr[11] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[12] = 5;
        bArr[13] = 6;
        bArr[14] = 7;
        bArr[15] = 6;
        bArr[16] = 7;
        bArr[17] = 8;
        bArr[18] = 13;
        bArr[19] = 9;
        bArr[20] = 10;
        bArr[21] = 9;
        bArr[22] = 10;
        bArr[23] = 12;
        bArr[24] = GameSpriteType.f42TYPE_ENEMY_1;
        bArr[25] = 12;
        bArr[26] = 14;
        bArr[27] = 14;
        bArr[28] = 16;
        bArr[29] = 15;
        bArr[30] = 17;
        bArr[31] = 16;
        bArr[32] = 17;
        bArr[33] = 18;
        bArr[34] = 30;
        bArr[35] = 18;
        bArr[36] = 19;
        bArr[37] = 26;
        bArr[38] = 28;
        bArr[39] = 20;
        bArr[40] = 19;
        bArr[41] = 26;
        bArr[42] = 28;
        bArr[43] = 20;
        bArr[44] = 21;
        bArr[45] = 22;
        bArr[46] = 21;
        bArr[47] = 22;
        bArr[48] = 23;
        bArr[49] = 32;
        bArr[50] = 24;
        bArr[51] = 25;
        bArr[52] = 24;
        bArr[53] = 25;
        bArr[54] = 27;
        bArr[55] = 28;
        bArr[56] = 27;
        bArr[57] = 29;
        bArr[58] = 31;
        bArr[59] = 35;
        bArr[60] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[61] = GameSpriteType.f41TYPE_ENEMY_;
        bArr[62] = GameSpriteType.f42TYPE_ENEMY_1;
        return bArr;
    }

    private static byte[] init__xml_trans_actions_0() {
        byte[] bArr = new byte[63];
        bArr[3] = 1;
        bArr[5] = 3;
        bArr[6] = 3;
        bArr[7] = 20;
        bArr[8] = 1;
        bArr[11] = 9;
        bArr[13] = 11;
        bArr[14] = 11;
        bArr[19] = 1;
        bArr[20] = 17;
        bArr[22] = 13;
        bArr[23] = 5;
        bArr[24] = 23;
        bArr[26] = 1;
        bArr[28] = 1;
        bArr[32] = 15;
        bArr[33] = 1;
        bArr[36] = 3;
        bArr[37] = 3;
        bArr[38] = 20;
        bArr[39] = 1;
        bArr[42] = 9;
        bArr[44] = 11;
        bArr[45] = 11;
        bArr[50] = 1;
        bArr[51] = 17;
        bArr[53] = 13;
        bArr[54] = 5;
        bArr[55] = 23;
        bArr[59] = 7;
        bArr[60] = 1;
        return bArr;
    }

    /* access modifiers changed from: protected */
    public void open(String name) {
        Element child = new Element(name, this.current);
        Element parent = this.current;
        if (parent != null) {
            parent.addChild(child);
        }
        this.elements.add(child);
        this.current = child;
    }

    /* access modifiers changed from: protected */
    public void attribute(String name, String value) {
        this.current.setAttribute(name, value);
    }

    /* access modifiers changed from: protected */
    public String entity(String name) {
        if (name.equals("lt")) {
            return "<";
        }
        if (name.equals("gt")) {
            return ">";
        }
        if (name.equals("amp")) {
            return "&";
        }
        if (name.equals("apos")) {
            return "'";
        }
        if (name.equals("quot")) {
            return "\"";
        }
        if (name.startsWith("#x")) {
            return Character.toString((char) Integer.parseInt(name.substring(2), 16));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void text(String text) {
        String existing = this.current.getText();
        Element element = this.current;
        if (existing != null) {
            text = String.valueOf(existing) + text;
        }
        element.setText(text);
    }

    /* access modifiers changed from: protected */
    public void close() {
        this.root = this.elements.pop();
        this.current = this.elements.size > 0 ? this.elements.peek() : null;
    }

    public static class Element {
        private ObjectMap<String, String> attributes;
        private Array<Element> children;
        private final String name;
        private Element parent;
        private String text;

        public Element(String name2, Element parent2) {
            this.name = name2;
            this.parent = parent2;
        }

        public String getName() {
            return this.name;
        }

        public ObjectMap<String, String> getAttributes() {
            return this.attributes;
        }

        public String getAttribute(String name2) {
            if (this.attributes == null) {
                throw new GdxRuntimeException("Element " + name2 + " doesn't have attribute: " + name2);
            }
            String value = this.attributes.get(name2);
            if (value != null) {
                return value;
            }
            throw new GdxRuntimeException("Element " + name2 + " doesn't have attribute: " + name2);
        }

        public String getAttribute(String name2, String defaultValue) {
            String value;
            if (this.attributes == null || (value = this.attributes.get(name2)) == null) {
                return defaultValue;
            }
            return value;
        }

        public void setAttribute(String name2, String value) {
            if (this.attributes == null) {
                this.attributes = new ObjectMap<>(8);
            }
            this.attributes.put(name2, value);
        }

        public int getChildCount() {
            if (this.children == null) {
                return 0;
            }
            return this.children.size;
        }

        public Element getChild(int index) {
            if (this.children != null) {
                return this.children.get(index);
            }
            throw new GdxRuntimeException("Element has no children: " + this.name);
        }

        public void addChild(Element element) {
            if (this.children == null) {
                this.children = new Array<>(8);
            }
            this.children.add(element);
        }

        public String getText() {
            return this.text;
        }

        public void setText(String text2) {
            this.text = text2;
        }

        public void removeChild(int index) {
            if (this.children != null) {
                this.children.removeIndex(index);
            }
        }

        public void removeChild(Element child) {
            if (this.children != null) {
                this.children.removeValue(child, true);
            }
        }

        public void remove() {
            this.parent.removeChild(this);
        }

        public Element getParent() {
            return this.parent;
        }

        public String toString() {
            return toString("");
        }

        public String toString(String indent) {
            StringBuilder buffer = new StringBuilder(128);
            buffer.append(indent);
            buffer.append('<');
            buffer.append(this.name);
            if (this.attributes != null) {
                Iterator it = this.attributes.entries().iterator();
                while (it.hasNext()) {
                    ObjectMap.Entry<String, String> entry = (ObjectMap.Entry) it.next();
                    buffer.append(' ');
                    buffer.append((String) entry.key);
                    buffer.append("=\"");
                    buffer.append((String) entry.value);
                    buffer.append('\"');
                }
            }
            if (this.children == null && (this.text == null || this.text.length() == 0)) {
                buffer.append("/>");
            } else {
                buffer.append(">\n");
                String childIndent = String.valueOf(indent) + 9;
                if (this.text != null && this.text.length() > 0) {
                    buffer.append(childIndent);
                    buffer.append(this.text);
                    buffer.append(10);
                }
                if (this.children != null) {
                    Iterator<Element> it2 = this.children.iterator();
                    while (it2.hasNext()) {
                        buffer.append(it2.next().toString(childIndent));
                        buffer.append(10);
                    }
                }
                buffer.append(indent);
                buffer.append("</");
                buffer.append(this.name);
                buffer.append('>');
            }
            return buffer.toString();
        }

        public Element getChildByName(String name2) {
            if (this.children == null) {
                return null;
            }
            for (int i = 0; i < this.children.size; i++) {
                Element element = this.children.get(i);
                if (element.name.equals(name2)) {
                    return element;
                }
            }
            return null;
        }

        public Element getChildByNameRecursive(String name2) {
            if (this.children == null) {
                return null;
            }
            for (int i = 0; i < this.children.size; i++) {
                Element element = this.children.get(i);
                if (element.name.equals(name2)) {
                    return element;
                }
                Element found = element.getChildByNameRecursive(name2);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }

        public Array<Element> getChildrenByName(String name2) {
            Array<Element> result = new Array<>();
            if (this.children != null) {
                for (int i = 0; i < this.children.size; i++) {
                    Element child = this.children.get(i);
                    if (child.name.equals(name2)) {
                        result.add(child);
                    }
                }
            }
            return result;
        }

        public Array<Element> getChildrenByNameRecursively(String name2) {
            Array<Element> result = new Array<>();
            getChildrenByNameRecursively(name2, result);
            return result;
        }

        private void getChildrenByNameRecursively(String name2, Array<Element> result) {
            if (this.children != null) {
                for (int i = 0; i < this.children.size; i++) {
                    Element child = this.children.get(i);
                    if (child.name.equals(name2)) {
                        result.add(child);
                    }
                    child.getChildrenByNameRecursively(name2, result);
                }
            }
        }

        public float getFloatAttribute(String name2) {
            return Float.parseFloat(getAttribute(name2));
        }

        public float getFloatAttribute(String name2, float defaultValue) {
            String value = getAttribute(name2, null);
            return value == null ? defaultValue : Float.parseFloat(value);
        }

        public int getIntAttribute(String name2) {
            return Integer.parseInt(getAttribute(name2));
        }

        public int getIntAttribute(String name2, int defaultValue) {
            String value = getAttribute(name2, null);
            return value == null ? defaultValue : Integer.parseInt(value);
        }

        public boolean getBooleanAttribute(String name2) {
            return Boolean.parseBoolean(getAttribute(name2));
        }

        public boolean getBooleanAttribute(String name2, boolean defaultValue) {
            String value = getAttribute(name2, null);
            return value == null ? defaultValue : Boolean.parseBoolean(value);
        }

        public String get(String name2) {
            String value = get(name2, null);
            if (value != null) {
                return value;
            }
            throw new GdxRuntimeException("Element " + this.name + " doesn't have attribute or child: " + name2);
        }

        public String get(String name2, String defaultValue) {
            String value;
            if (this.attributes != null && (value = this.attributes.get(name2)) != null) {
                return value;
            }
            Element child = getChildByName(name2);
            if (child == null) {
                return defaultValue;
            }
            String value2 = child.getText();
            if (value2 == null) {
                return defaultValue;
            }
            return value2;
        }

        public int getInt(String name2) {
            String value = get(name2, null);
            if (value != null) {
                return Integer.parseInt(value);
            }
            throw new GdxRuntimeException("Element " + this.name + " doesn't have attribute or child: " + name2);
        }

        public int getInt(String name2, int defaultValue) {
            String value = get(name2, null);
            return value == null ? defaultValue : Integer.parseInt(value);
        }

        public float getFloat(String name2) {
            String value = get(name2, null);
            if (value != null) {
                return Float.parseFloat(value);
            }
            throw new GdxRuntimeException("Element " + this.name + " doesn't have attribute or child: " + name2);
        }

        public float getFloat(String name2, float defaultValue) {
            String value = get(name2, null);
            return value == null ? defaultValue : Float.parseFloat(value);
        }

        public boolean getBoolean(String name2) {
            String value = get(name2, null);
            if (value != null) {
                return Boolean.parseBoolean(value);
            }
            throw new GdxRuntimeException("Element " + this.name + " doesn't have attribute or child: " + name2);
        }

        public boolean getBoolean(String name2, boolean defaultValue) {
            String value = get(name2, null);
            return value == null ? defaultValue : Boolean.parseBoolean(value);
        }
    }
}
