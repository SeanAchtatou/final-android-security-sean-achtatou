package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.objectweb.asm.signature.SignatureVisitor;

public class ObjectFloatMap<K> implements Iterable<Entry<K>> {
    private static final int PRIME1 = -1105259343;
    private static final int PRIME2 = -1262997959;
    private static final int PRIME3 = -825114047;
    int capacity;
    private Entries entries1;
    private Entries entries2;
    private int hashShift;
    K[] keyTable;
    private Keys keys1;
    private Keys keys2;
    private float loadFactor;
    private int mask;
    private int pushIterations;
    public int size;
    private int stashCapacity;
    int stashSize;
    private int threshold;
    float[] valueTable;
    private Values values1;
    private Values values2;

    public ObjectFloatMap() {
        this(32, 0.8f);
    }

    public ObjectFloatMap(int initialCapacity) {
        this(initialCapacity, 0.8f);
    }

    public ObjectFloatMap(int initialCapacity, float loadFactor2) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("initialCapacity must be >= 0: " + initialCapacity);
        } else if (initialCapacity > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + initialCapacity);
        } else {
            this.capacity = MathUtils.nextPowerOfTwo(initialCapacity);
            if (loadFactor2 <= Animation.CurveTimeline.LINEAR) {
                throw new IllegalArgumentException("loadFactor must be > 0: " + loadFactor2);
            }
            this.loadFactor = loadFactor2;
            this.threshold = (int) (((float) this.capacity) * loadFactor2);
            this.mask = this.capacity - 1;
            this.hashShift = 31 - Integer.numberOfTrailingZeros(this.capacity);
            this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) this.capacity))) * 2);
            this.pushIterations = Math.max(Math.min(this.capacity, 8), ((int) Math.sqrt((double) this.capacity)) / 8);
            this.keyTable = new Object[(this.capacity + this.stashCapacity)];
            this.valueTable = new float[this.keyTable.length];
        }
    }

    public ObjectFloatMap(ObjectFloatMap<? extends K> map) {
        this(map.capacity, map.loadFactor);
        this.stashSize = map.stashSize;
        System.arraycopy(map.keyTable, 0, this.keyTable, 0, map.keyTable.length);
        System.arraycopy(map.valueTable, 0, this.valueTable, 0, map.valueTable.length);
        this.size = map.size;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void put(K r14, float r15) {
        /*
            r13 = this;
            if (r14 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "key cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000a:
            K[] r11 = r13.keyTable
            int r9 = r14.hashCode()
            int r0 = r13.mask
            r3 = r9 & r0
            r4 = r11[r3]
            boolean r0 = r14.equals(r4)
            if (r0 == 0) goto L_0x0021
            float[] r0 = r13.valueTable
            r0[r3] = r15
        L_0x0020:
            return
        L_0x0021:
            int r5 = r13.hash2(r9)
            r6 = r11[r5]
            boolean r0 = r14.equals(r6)
            if (r0 == 0) goto L_0x0032
            float[] r0 = r13.valueTable
            r0[r5] = r15
            goto L_0x0020
        L_0x0032:
            int r7 = r13.hash3(r9)
            r8 = r11[r7]
            boolean r0 = r14.equals(r8)
            if (r0 == 0) goto L_0x0043
            float[] r0 = r13.valueTable
            r0[r7] = r15
            goto L_0x0020
        L_0x0043:
            int r10 = r13.capacity
            int r0 = r13.stashSize
            int r12 = r10 + r0
        L_0x0049:
            if (r10 < r12) goto L_0x0065
            if (r4 != 0) goto L_0x0075
            r11[r3] = r14
            float[] r0 = r13.valueTable
            r0[r3] = r15
            int r0 = r13.size
            int r1 = r0 + 1
            r13.size = r1
            int r1 = r13.threshold
            if (r0 < r1) goto L_0x0020
            int r0 = r13.capacity
            int r0 = r0 << 1
            r13.resize(r0)
            goto L_0x0020
        L_0x0065:
            r0 = r11[r10]
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x0072
            float[] r0 = r13.valueTable
            r0[r10] = r15
            goto L_0x0020
        L_0x0072:
            int r10 = r10 + 1
            goto L_0x0049
        L_0x0075:
            if (r6 != 0) goto L_0x008f
            r11[r5] = r14
            float[] r0 = r13.valueTable
            r0[r5] = r15
            int r0 = r13.size
            int r1 = r0 + 1
            r13.size = r1
            int r1 = r13.threshold
            if (r0 < r1) goto L_0x0020
            int r0 = r13.capacity
            int r0 = r0 << 1
            r13.resize(r0)
            goto L_0x0020
        L_0x008f:
            if (r8 != 0) goto L_0x00aa
            r11[r7] = r14
            float[] r0 = r13.valueTable
            r0[r7] = r15
            int r0 = r13.size
            int r1 = r0 + 1
            r13.size = r1
            int r1 = r13.threshold
            if (r0 < r1) goto L_0x0020
            int r0 = r13.capacity
            int r0 = r0 << 1
            r13.resize(r0)
            goto L_0x0020
        L_0x00aa:
            r0 = r13
            r1 = r14
            r2 = r15
            r0.push(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.put(java.lang.Object, float):void");
    }

    public void putAll(ObjectFloatMap<K> map) {
        Iterator it = map.entries().iterator();
        while (it.hasNext()) {
            Entry<K> entry = (Entry) it.next();
            put(entry.key, entry.value);
        }
    }

    private void putResize(K key, float value) {
        int hashCode = key.hashCode();
        int index1 = hashCode & this.mask;
        K key1 = this.keyTable[index1];
        if (key1 == null) {
            this.keyTable[index1] = key;
            this.valueTable[index1] = value;
            int i = this.size;
            this.size = i + 1;
            if (i >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int index2 = hash2(hashCode);
        K key2 = this.keyTable[index2];
        if (key2 == null) {
            this.keyTable[index2] = key;
            this.valueTable[index2] = value;
            int i2 = this.size;
            this.size = i2 + 1;
            if (i2 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        int index3 = hash3(hashCode);
        K key3 = this.keyTable[index3];
        if (key3 == null) {
            this.keyTable[index3] = key;
            this.valueTable[index3] = value;
            int i3 = this.size;
            this.size = i3 + 1;
            if (i3 >= this.threshold) {
                resize(this.capacity << 1);
                return;
            }
            return;
        }
        push(key, value, index1, key1, index2, key2, index3, key3);
    }

    private void push(K insertKey, float insertValue, int index1, K key1, int index2, K key2, int index3, K key3) {
        K evictedKey;
        float evictedValue;
        K[] kArr = this.keyTable;
        float[] valueTable2 = this.valueTable;
        int mask2 = this.mask;
        int i = 0;
        int pushIterations2 = this.pushIterations;
        while (true) {
            switch (MathUtils.random(2)) {
                case 0:
                    evictedKey = key1;
                    evictedValue = valueTable2[index1];
                    kArr[index1] = insertKey;
                    valueTable2[index1] = insertValue;
                    break;
                case 1:
                    evictedKey = key2;
                    evictedValue = valueTable2[index2];
                    kArr[index2] = insertKey;
                    valueTable2[index2] = insertValue;
                    break;
                default:
                    evictedKey = key3;
                    evictedValue = valueTable2[index3];
                    kArr[index3] = insertKey;
                    valueTable2[index3] = insertValue;
                    break;
            }
            int hashCode = evictedKey.hashCode();
            index1 = hashCode & mask2;
            key1 = kArr[index1];
            if (key1 == null) {
                kArr[index1] = evictedKey;
                valueTable2[index1] = evictedValue;
                int i2 = this.size;
                this.size = i2 + 1;
                if (i2 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            index2 = hash2(hashCode);
            key2 = kArr[index2];
            if (key2 == null) {
                kArr[index2] = evictedKey;
                valueTable2[index2] = evictedValue;
                int i3 = this.size;
                this.size = i3 + 1;
                if (i3 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            index3 = hash3(hashCode);
            key3 = kArr[index3];
            if (key3 == null) {
                kArr[index3] = evictedKey;
                valueTable2[index3] = evictedValue;
                int i4 = this.size;
                this.size = i4 + 1;
                if (i4 >= this.threshold) {
                    resize(this.capacity << 1);
                    return;
                }
                return;
            }
            i++;
            if (i == pushIterations2) {
                putStash(evictedKey, evictedValue);
                return;
            } else {
                insertKey = evictedKey;
                insertValue = evictedValue;
            }
        }
    }

    private void putStash(K key, float value) {
        if (this.stashSize == this.stashCapacity) {
            resize(this.capacity << 1);
            put(key, value);
            return;
        }
        int index = this.capacity + this.stashSize;
        this.keyTable[index] = key;
        this.valueTable[index] = value;
        this.stashSize++;
        this.size++;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float get(K r4, float r5) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r2 = r3.mask
            r1 = r0 & r2
            K[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.hash2(r0)
            K[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.hash3(r0)
            K[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            float r2 = r3.getStash(r4, r5)
        L_0x0032:
            return r2
        L_0x0033:
            float[] r2 = r3.valueTable
            r2 = r2[r1]
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.get(java.lang.Object, float):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private float getStash(K r5, float r6) {
        /*
            r4 = this;
            K[] r1 = r4.keyTable
            int r0 = r4.capacity
            int r3 = r4.stashSize
            int r2 = r0 + r3
        L_0x0008:
            if (r0 < r2) goto L_0x000b
        L_0x000a:
            return r6
        L_0x000b:
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0018
            float[] r3 = r4.valueTable
            r6 = r3[r0]
            goto L_0x000a
        L_0x0018:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.getStash(java.lang.Object, float):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float getAndIncrement(K r6, float r7, float r8) {
        /*
            r5 = this;
            int r0 = r6.hashCode()
            int r3 = r5.mask
            r1 = r0 & r3
            K[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 != 0) goto L_0x0033
            int r1 = r5.hash2(r0)
            K[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 != 0) goto L_0x0033
            int r1 = r5.hash3(r0)
            K[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 != 0) goto L_0x0033
            float r2 = r5.getAndIncrementStash(r6, r7, r8)
        L_0x0032:
            return r2
        L_0x0033:
            float[] r3 = r5.valueTable
            r2 = r3[r1]
            float[] r3 = r5.valueTable
            float r4 = r2 + r8
            r3[r1] = r4
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.getAndIncrement(java.lang.Object, float, float):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private float getAndIncrementStash(K r7, float r8, float r9) {
        /*
            r6 = this;
            K[] r1 = r6.keyTable
            int r0 = r6.capacity
            int r4 = r6.stashSize
            int r2 = r0 + r4
        L_0x0008:
            if (r0 < r2) goto L_0x0011
            float r4 = r8 + r9
            r6.put(r7, r4)
            r3 = r8
        L_0x0010:
            return r3
        L_0x0011:
            r4 = r1[r0]
            boolean r4 = r7.equals(r4)
            if (r4 == 0) goto L_0x0024
            float[] r4 = r6.valueTable
            r3 = r4[r0]
            float[] r4 = r6.valueTable
            float r5 = r3 + r9
            r4[r0] = r5
            goto L_0x0010
        L_0x0024:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.getAndIncrementStash(java.lang.Object, float, float):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public float remove(K r6, float r7) {
        /*
            r5 = this;
            r4 = 0
            int r0 = r6.hashCode()
            int r3 = r5.mask
            r1 = r0 & r3
            K[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x0022
            K[] r3 = r5.keyTable
            r3[r1] = r4
            float[] r3 = r5.valueTable
            r2 = r3[r1]
            int r3 = r5.size
            int r3 = r3 + -1
            r5.size = r3
        L_0x0021:
            return r2
        L_0x0022:
            int r1 = r5.hash2(r0)
            K[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x003f
            K[] r3 = r5.keyTable
            r3[r1] = r4
            float[] r3 = r5.valueTable
            r2 = r3[r1]
            int r3 = r5.size
            int r3 = r3 + -1
            r5.size = r3
            goto L_0x0021
        L_0x003f:
            int r1 = r5.hash3(r0)
            K[] r3 = r5.keyTable
            r3 = r3[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x005c
            K[] r3 = r5.keyTable
            r3[r1] = r4
            float[] r3 = r5.valueTable
            r2 = r3[r1]
            int r3 = r5.size
            int r3 = r3 + -1
            r5.size = r3
            goto L_0x0021
        L_0x005c:
            float r2 = r5.removeStash(r6, r7)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.remove(java.lang.Object, float):float");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    float removeStash(K r6, float r7) {
        /*
            r5 = this;
            K[] r1 = r5.keyTable
            int r0 = r5.capacity
            int r4 = r5.stashSize
            int r2 = r0 + r4
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r3 = r7
        L_0x000b:
            return r3
        L_0x000c:
            r4 = r1[r0]
            boolean r4 = r6.equals(r4)
            if (r4 == 0) goto L_0x0022
            float[] r4 = r5.valueTable
            r3 = r4[r0]
            r5.removeStashIndex(r0)
            int r4 = r5.size
            int r4 = r4 + -1
            r5.size = r4
            goto L_0x000b
        L_0x0022:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.removeStash(java.lang.Object, float):float");
    }

    /* access modifiers changed from: package-private */
    public void removeStashIndex(int index) {
        this.stashSize--;
        int lastIndex = this.capacity + this.stashSize;
        if (index < lastIndex) {
            this.keyTable[index] = this.keyTable[lastIndex];
            this.valueTable[index] = this.valueTable[lastIndex];
        }
    }

    public void shrink(int maximumCapacity) {
        if (maximumCapacity < 0) {
            throw new IllegalArgumentException("maximumCapacity must be >= 0: " + maximumCapacity);
        }
        if (this.size > maximumCapacity) {
            maximumCapacity = this.size;
        }
        if (this.capacity > maximumCapacity) {
            resize(MathUtils.nextPowerOfTwo(maximumCapacity));
        }
    }

    public void clear(int maximumCapacity) {
        if (this.capacity <= maximumCapacity) {
            clear();
            return;
        }
        this.size = 0;
        resize(maximumCapacity);
    }

    public void clear() {
        if (this.size != 0) {
            Object[] keyTable2 = this.keyTable;
            int i = this.capacity + this.stashSize;
            while (true) {
                int i2 = i;
                i = i2 - 1;
                if (i2 <= 0) {
                    this.size = 0;
                    this.stashSize = 0;
                    return;
                }
                keyTable2[i] = null;
            }
        }
    }

    public boolean containsValue(float value) {
        float[] valueTable2 = this.valueTable;
        int i = this.capacity + this.stashSize;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 <= 0) {
                return false;
            }
            if (valueTable2[i] == value) {
                return true;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean containsKey(K r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            int r2 = r3.mask
            r1 = r0 & r2
            K[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.hash2(r0)
            K[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            int r1 = r3.hash3(r0)
            K[] r2 = r3.keyTable
            r2 = r2[r1]
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0033
            boolean r2 = r3.containsKeyStash(r4)
        L_0x0032:
            return r2
        L_0x0033:
            r2 = 1
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.containsKey(java.lang.Object):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean containsKeyStash(K r5) {
        /*
            r4 = this;
            K[] r1 = r4.keyTable
            int r0 = r4.capacity
            int r3 = r4.stashSize
            int r2 = r0 + r3
        L_0x0008:
            if (r0 < r2) goto L_0x000c
            r3 = 0
        L_0x000b:
            return r3
        L_0x000c:
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0016
            r3 = 1
            goto L_0x000b
        L_0x0016:
            int r0 = r0 + 1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ObjectFloatMap.containsKeyStash(java.lang.Object):boolean");
    }

    public K findKey(float value) {
        float[] valueTable2 = this.valueTable;
        int i = this.capacity + this.stashSize;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 <= 0) {
                return null;
            }
            if (valueTable2[i] == value) {
                return this.keyTable[i];
            }
        }
    }

    public void ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded >= this.threshold) {
            resize(MathUtils.nextPowerOfTwo((int) (((float) sizeNeeded) / this.loadFactor)));
        }
    }

    private void resize(int newSize) {
        int oldEndIndex = this.capacity + this.stashSize;
        this.capacity = newSize;
        this.threshold = (int) (((float) newSize) * this.loadFactor);
        this.mask = newSize - 1;
        this.hashShift = 31 - Integer.numberOfTrailingZeros(newSize);
        this.stashCapacity = Math.max(3, ((int) Math.ceil(Math.log((double) newSize))) * 2);
        this.pushIterations = Math.max(Math.min(newSize, 8), ((int) Math.sqrt((double) newSize)) / 8);
        K[] oldKeyTable = this.keyTable;
        float[] oldValueTable = this.valueTable;
        this.keyTable = new Object[(this.stashCapacity + newSize)];
        this.valueTable = new float[(this.stashCapacity + newSize)];
        int oldSize = this.size;
        this.size = 0;
        this.stashSize = 0;
        if (oldSize > 0) {
            for (int i = 0; i < oldEndIndex; i++) {
                K key = oldKeyTable[i];
                if (key != null) {
                    putResize(key, oldValueTable[i]);
                }
            }
        }
    }

    private int hash2(int h) {
        int h2 = h * PRIME2;
        return ((h2 >>> this.hashShift) ^ h2) & this.mask;
    }

    private int hash3(int h) {
        int h2 = h * PRIME3;
        return ((h2 >>> this.hashShift) ^ h2) & this.mask;
    }

    public String toString() {
        if (this.size == 0) {
            return "{}";
        }
        StringBuilder buffer = new StringBuilder(32);
        buffer.append('{');
        K[] keyTable2 = this.keyTable;
        float[] valueTable2 = this.valueTable;
        int i = keyTable2.length;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            K key = keyTable2[i];
            if (key != null) {
                buffer.append((Object) key);
                buffer.append((char) SignatureVisitor.INSTANCEOF);
                buffer.append(valueTable2[i]);
                break;
            }
        }
        while (true) {
            int i3 = i;
            i = i3 - 1;
            if (i3 <= 0) {
                buffer.append('}');
                return buffer.toString();
            }
            K key2 = keyTable2[i];
            if (key2 != null) {
                buffer.append(", ");
                buffer.append((Object) key2);
                buffer.append((char) SignatureVisitor.INSTANCEOF);
                buffer.append(valueTable2[i]);
            }
        }
    }

    public Entries<K> iterator() {
        return entries();
    }

    public Entries<K> entries() {
        if (this.entries1 == null) {
            this.entries1 = new Entries(this);
            this.entries2 = new Entries(this);
        }
        if (!this.entries1.valid) {
            this.entries1.reset();
            this.entries1.valid = true;
            this.entries2.valid = false;
            return this.entries1;
        }
        this.entries2.reset();
        this.entries2.valid = true;
        this.entries1.valid = false;
        return this.entries2;
    }

    public Values values() {
        if (this.values1 == null) {
            this.values1 = new Values(this);
            this.values2 = new Values(this);
        }
        if (!this.values1.valid) {
            this.values1.reset();
            this.values1.valid = true;
            this.values2.valid = false;
            return this.values1;
        }
        this.values2.reset();
        this.values2.valid = true;
        this.values1.valid = false;
        return this.values2;
    }

    public Keys<K> keys() {
        if (this.keys1 == null) {
            this.keys1 = new Keys(this);
            this.keys2 = new Keys(this);
        }
        if (!this.keys1.valid) {
            this.keys1.reset();
            this.keys1.valid = true;
            this.keys2.valid = false;
            return this.keys1;
        }
        this.keys2.reset();
        this.keys2.valid = true;
        this.keys1.valid = false;
        return this.keys2;
    }

    public static class Entry<K> {
        public K key;
        public float value;

        public String toString() {
            return ((Object) this.key) + "=" + this.value;
        }
    }

    private static class MapIterator<K> {
        int currentIndex;
        public boolean hasNext;
        final ObjectFloatMap<K> map;
        int nextIndex;
        boolean valid = true;

        public MapIterator(ObjectFloatMap<K> map2) {
            this.map = map2;
            reset();
        }

        public void reset() {
            this.currentIndex = -1;
            this.nextIndex = -1;
            findNextIndex();
        }

        /* access modifiers changed from: package-private */
        public void findNextIndex() {
            this.hasNext = false;
            Object[] keyTable = this.map.keyTable;
            int n = this.map.capacity + this.map.stashSize;
            do {
                int i = this.nextIndex + 1;
                this.nextIndex = i;
                if (i >= n) {
                    return;
                }
            } while (keyTable[this.nextIndex] == null);
            this.hasNext = true;
        }

        public void remove() {
            if (this.currentIndex < 0) {
                throw new IllegalStateException("next must be called before remove.");
            }
            if (this.currentIndex >= this.map.capacity) {
                this.map.removeStashIndex(this.currentIndex);
                this.nextIndex = this.currentIndex - 1;
                findNextIndex();
            } else {
                this.map.keyTable[this.currentIndex] = null;
            }
            this.currentIndex = -1;
            ObjectFloatMap<K> objectFloatMap = this.map;
            objectFloatMap.size--;
        }
    }

    public static class Entries<K> extends MapIterator<K> implements Iterable<Entry<K>>, Iterator<Entry<K>> {
        private Entry<K> entry = new Entry<>();

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public Entries(ObjectFloatMap<K> map) {
            super(map);
        }

        public Entry<K> next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                Object[] keyTable = this.map.keyTable;
                this.entry.key = keyTable[this.nextIndex];
                this.entry.value = this.map.valueTable[this.nextIndex];
                this.currentIndex = this.nextIndex;
                findNextIndex();
                return this.entry;
            }
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.hasNext;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public Entries<K> iterator() {
            return this;
        }

        public void remove() {
            super.remove();
        }
    }

    public static class Values extends MapIterator<Object> {
        public /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public Values(ObjectFloatMap<?> map) {
            super(map);
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.hasNext;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public float next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                float value = this.map.valueTable[this.nextIndex];
                this.currentIndex = this.nextIndex;
                findNextIndex();
                return value;
            }
        }

        public FloatArray toArray() {
            FloatArray array = new FloatArray(true, this.map.size);
            while (this.hasNext) {
                array.add(next());
            }
            return array;
        }
    }

    public static class Keys<K> extends MapIterator<K> implements Iterable<K>, Iterator<K> {
        public /* bridge */ /* synthetic */ void reset() {
            super.reset();
        }

        public Keys(ObjectFloatMap<K> map) {
            super(map);
        }

        public boolean hasNext() {
            if (this.valid) {
                return this.hasNext;
            }
            throw new GdxRuntimeException("#iterator() cannot be used nested.");
        }

        public K next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            } else if (!this.valid) {
                throw new GdxRuntimeException("#iterator() cannot be used nested.");
            } else {
                K key = this.map.keyTable[this.nextIndex];
                this.currentIndex = this.nextIndex;
                findNextIndex();
                return key;
            }
        }

        public Keys<K> iterator() {
            return this;
        }

        public Array<K> toArray() {
            Array array = new Array(true, this.map.size);
            while (this.hasNext) {
                array.add(next());
            }
            return array;
        }

        public Array<K> toArray(Array<K> array) {
            while (this.hasNext) {
                array.add(next());
            }
            return array;
        }

        public void remove() {
            super.remove();
        }
    }
}
