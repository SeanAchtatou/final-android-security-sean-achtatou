package com.badlogic.gdx.utils;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.Buffer;
import java.nio.ByteBuffer;

public final class StreamUtils {
    public static final int DEFAULT_BUFFER_SIZE = 4096;
    public static final byte[] EMPTY_BYTES = new byte[0];

    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        copyStream(input, output, new byte[4096]);
    }

    public static void copyStream(InputStream input, OutputStream output, int bufferSize) throws IOException {
        copyStream(input, output, new byte[bufferSize]);
    }

    public static void copyStream(InputStream input, OutputStream output, byte[] buffer) throws IOException {
        while (true) {
            int bytesRead = input.read(buffer);
            if (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);
            } else {
                return;
            }
        }
    }

    public static void copyStream(InputStream input, ByteBuffer output) throws IOException {
        copyStream(input, output, new byte[4096]);
    }

    public static void copyStream(InputStream input, ByteBuffer output, int bufferSize) throws IOException {
        copyStream(input, output, new byte[bufferSize]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.BufferUtils.copy(byte[], int, java.nio.Buffer, int):void
     arg types: [byte[], int, java.nio.ByteBuffer, int]
     candidates:
      com.badlogic.gdx.utils.BufferUtils.copy(char[], int, int, java.nio.Buffer):void
      com.badlogic.gdx.utils.BufferUtils.copy(char[], int, java.nio.Buffer, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(double[], int, int, java.nio.Buffer):void
      com.badlogic.gdx.utils.BufferUtils.copy(double[], int, java.nio.Buffer, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(float[], int, int, java.nio.Buffer):void
      com.badlogic.gdx.utils.BufferUtils.copy(float[], int, java.nio.Buffer, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(float[], java.nio.Buffer, int, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(int[], int, int, java.nio.Buffer):void
      com.badlogic.gdx.utils.BufferUtils.copy(int[], int, java.nio.Buffer, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(long[], int, int, java.nio.Buffer):void
      com.badlogic.gdx.utils.BufferUtils.copy(long[], int, java.nio.Buffer, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(short[], int, java.nio.Buffer, int):void
      com.badlogic.gdx.utils.BufferUtils.copy(byte[], int, java.nio.Buffer, int):void */
    public static int copyStream(InputStream input, ByteBuffer output, byte[] buffer) throws IOException {
        int startPosition = output.position();
        int total = 0;
        while (true) {
            int bytesRead = input.read(buffer);
            if (bytesRead == -1) {
                output.position(startPosition);
                return total;
            }
            BufferUtils.copy(buffer, 0, (Buffer) output, bytesRead);
            total += bytesRead;
            output.position(startPosition + total);
        }
    }

    public static byte[] copyStreamToByteArray(InputStream input) throws IOException {
        return copyStreamToByteArray(input, input.available());
    }

    public static byte[] copyStreamToByteArray(InputStream input, int estimatedSize) throws IOException {
        ByteArrayOutputStream baos = new OptimizedByteArrayOutputStream(Math.max(0, estimatedSize));
        copyStream(input, baos);
        return baos.toByteArray();
    }

    public static String copyStreamToString(InputStream input) throws IOException {
        return copyStreamToString(input, input.available(), null);
    }

    public static String copyStreamToString(InputStream input, int estimatedSize) throws IOException {
        return copyStreamToString(input, estimatedSize, null);
    }

    public static String copyStreamToString(InputStream input, int estimatedSize, String charset) throws IOException {
        InputStreamReader reader = charset == null ? new InputStreamReader(input) : new InputStreamReader(input, charset);
        StringWriter writer = new StringWriter(Math.max(0, estimatedSize));
        char[] buffer = new char[4096];
        while (true) {
            int charsRead = reader.read(buffer);
            if (charsRead == -1) {
                return writer.toString();
            }
            writer.write(buffer, 0, charsRead);
        }
    }

    public static void closeQuietly(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (Exception e) {
            }
        }
    }

    public static class OptimizedByteArrayOutputStream extends ByteArrayOutputStream {
        public OptimizedByteArrayOutputStream(int initialSize) {
            super(initialSize);
        }

        public synchronized byte[] toByteArray() {
            byte[] byteArray;
            if (this.count == this.buf.length) {
                byteArray = this.buf;
            } else {
                byteArray = super.toByteArray();
            }
            return byteArray;
        }

        public byte[] getBuffer() {
            return this.buf;
        }
    }
}
