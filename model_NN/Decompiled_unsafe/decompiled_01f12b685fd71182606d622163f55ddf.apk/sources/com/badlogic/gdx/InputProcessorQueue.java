package com.badlogic.gdx;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.TimeUtils;

public class InputProcessorQueue implements InputProcessor {
    private static final int KEY_DOWN = 0;
    private static final int KEY_TYPED = 2;
    private static final int KEY_UP = 1;
    private static final int MOUSE_MOVED = 6;
    private static final int SCROLLED = 7;
    private static final int TOUCH_DOWN = 3;
    private static final int TOUCH_DRAGGED = 5;
    private static final int TOUCH_UP = 4;
    private long currentEventTime;
    private final IntArray processingQueue = new IntArray();
    private InputProcessor processor;
    private final IntArray queue = new IntArray();

    public InputProcessorQueue() {
    }

    public InputProcessorQueue(InputProcessor processor2) {
        this.processor = processor2;
    }

    public void setProcessor(InputProcessor processor2) {
        this.processor = processor2;
    }

    public InputProcessor getProcessor() {
        return this.processor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (r1 < r2) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        r3.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        r0 = r1 + 1;
        r1 = r0 + 1;
        r10.currentEventTime = (((long) r3.get(r1)) << 32) | (((long) r3.get(r0)) & 4294967295L);
        r0 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        switch(r3.get(r1)) {
            case 0: goto L_0x004b;
            case 1: goto L_0x0057;
            case 2: goto L_0x0063;
            case 3: goto L_0x0070;
            case 4: goto L_0x008f;
            case 5: goto L_0x00af;
            case 6: goto L_0x00c8;
            case 7: goto L_0x00dc;
            default: goto L_0x0049;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        r1 = r0 + 1;
        r10.processor.keyDown(r3.get(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        r1 = r0 + 1;
        r10.processor.keyUp(r3.get(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0063, code lost:
        r1 = r0 + 1;
        r10.processor.keyTyped((char) r3.get(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0070, code lost:
        r4 = r10.processor;
        r1 = r0 + 1;
        r5 = r3.get(r0);
        r0 = r1 + 1;
        r6 = r3.get(r1);
        r1 = r0 + 1;
        r4.touchDown(r5, r6, r3.get(r0), r3.get(r1));
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008f, code lost:
        r4 = r10.processor;
        r1 = r0 + 1;
        r5 = r3.get(r0);
        r0 = r1 + 1;
        r6 = r3.get(r1);
        r1 = r0 + 1;
        r4.touchUp(r5, r6, r3.get(r0), r3.get(r1));
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00af, code lost:
        r4 = r10.processor;
        r1 = r0 + 1;
        r5 = r3.get(r0);
        r0 = r1 + 1;
        r6 = r3.get(r1);
        r1 = r0 + 1;
        r4.touchDragged(r5, r6, r3.get(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c8, code lost:
        r1 = r0 + 1;
        r10.processor.mouseMoved(r3.get(r0), r3.get(r1));
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00dc, code lost:
        r1 = r0 + 1;
        r10.processor.scrolled(r3.get(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r2 = r3.size;
        r1 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void drain() {
        /*
            r10 = this;
            com.badlogic.gdx.utils.IntArray r3 = r10.processingQueue
            monitor-enter(r10)
            com.badlogic.gdx.InputProcessor r4 = r10.processor     // Catch:{ all -> 0x0023 }
            if (r4 != 0) goto L_0x000e
            com.badlogic.gdx.utils.IntArray r4 = r10.queue     // Catch:{ all -> 0x0023 }
            r4.clear()     // Catch:{ all -> 0x0023 }
            monitor-exit(r10)     // Catch:{ all -> 0x0023 }
        L_0x000d:
            return
        L_0x000e:
            com.badlogic.gdx.utils.IntArray r4 = r10.queue     // Catch:{ all -> 0x0023 }
            r3.addAll(r4)     // Catch:{ all -> 0x0023 }
            com.badlogic.gdx.utils.IntArray r4 = r10.queue     // Catch:{ all -> 0x0023 }
            r4.clear()     // Catch:{ all -> 0x0023 }
            monitor-exit(r10)     // Catch:{ all -> 0x0023 }
            r0 = 0
            int r2 = r3.size
            r1 = r0
        L_0x001d:
            if (r1 < r2) goto L_0x0026
            r3.clear()
            goto L_0x000d
        L_0x0023:
            r4 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x0023 }
            throw r4
        L_0x0026:
            int r0 = r1 + 1
            int r4 = r3.get(r1)
            long r4 = (long) r4
            r6 = 32
            long r4 = r4 << r6
            int r1 = r0 + 1
            int r6 = r3.get(r0)
            long r6 = (long) r6
            r8 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r6 = r6 & r8
            long r4 = r4 | r6
            r10.currentEventTime = r4
            int r0 = r1 + 1
            int r4 = r3.get(r1)
            switch(r4) {
                case 0: goto L_0x004b;
                case 1: goto L_0x0057;
                case 2: goto L_0x0063;
                case 3: goto L_0x0070;
                case 4: goto L_0x008f;
                case 5: goto L_0x00af;
                case 6: goto L_0x00c8;
                case 7: goto L_0x00dc;
                default: goto L_0x0049;
            }
        L_0x0049:
            r1 = r0
            goto L_0x001d
        L_0x004b:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            r4.keyDown(r5)
            goto L_0x001d
        L_0x0057:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            r4.keyUp(r5)
            goto L_0x001d
        L_0x0063:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            char r5 = (char) r5
            r4.keyTyped(r5)
            goto L_0x001d
        L_0x0070:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            int r0 = r1 + 1
            int r6 = r3.get(r1)
            int r1 = r0 + 1
            int r7 = r3.get(r0)
            int r0 = r1 + 1
            int r8 = r3.get(r1)
            r4.touchDown(r5, r6, r7, r8)
            r1 = r0
            goto L_0x001d
        L_0x008f:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            int r0 = r1 + 1
            int r6 = r3.get(r1)
            int r1 = r0 + 1
            int r7 = r3.get(r0)
            int r0 = r1 + 1
            int r8 = r3.get(r1)
            r4.touchUp(r5, r6, r7, r8)
            r1 = r0
            goto L_0x001d
        L_0x00af:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            int r0 = r1 + 1
            int r6 = r3.get(r1)
            int r1 = r0 + 1
            int r7 = r3.get(r0)
            r4.touchDragged(r5, r6, r7)
            goto L_0x001d
        L_0x00c8:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            int r0 = r1 + 1
            int r6 = r3.get(r1)
            r4.mouseMoved(r5, r6)
            r1 = r0
            goto L_0x001d
        L_0x00dc:
            com.badlogic.gdx.InputProcessor r4 = r10.processor
            int r1 = r0 + 1
            int r5 = r3.get(r0)
            r4.scrolled(r5)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.InputProcessorQueue.drain():void");
    }

    private void queueTime() {
        long time = TimeUtils.nanoTime();
        this.queue.add((int) (time >> 32));
        this.queue.add((int) time);
    }

    public synchronized boolean keyDown(int keycode) {
        queueTime();
        this.queue.add(0);
        this.queue.add(keycode);
        return false;
    }

    public synchronized boolean keyUp(int keycode) {
        queueTime();
        this.queue.add(1);
        this.queue.add(keycode);
        return false;
    }

    public synchronized boolean keyTyped(char character) {
        queueTime();
        this.queue.add(2);
        this.queue.add(character);
        return false;
    }

    public synchronized boolean touchDown(int screenX, int screenY, int pointer, int button) {
        queueTime();
        this.queue.add(3);
        this.queue.add(screenX);
        this.queue.add(screenY);
        this.queue.add(pointer);
        this.queue.add(button);
        return false;
    }

    public synchronized boolean touchUp(int screenX, int screenY, int pointer, int button) {
        queueTime();
        this.queue.add(4);
        this.queue.add(screenX);
        this.queue.add(screenY);
        this.queue.add(pointer);
        this.queue.add(button);
        return false;
    }

    public synchronized boolean touchDragged(int screenX, int screenY, int pointer) {
        queueTime();
        this.queue.add(5);
        this.queue.add(screenX);
        this.queue.add(screenY);
        this.queue.add(pointer);
        return false;
    }

    public synchronized boolean mouseMoved(int screenX, int screenY) {
        queueTime();
        this.queue.add(6);
        this.queue.add(screenX);
        this.queue.add(screenY);
        return false;
    }

    public synchronized boolean scrolled(int amount) {
        queueTime();
        this.queue.add(7);
        this.queue.add(amount);
        return false;
    }

    public long getCurrentEventTime() {
        return this.currentEventTime;
    }
}
