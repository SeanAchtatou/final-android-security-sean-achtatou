package com.kotcrab.vis.ui.util.form;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisValidableTextField;
import java.io.File;

public class FormValidator extends SimpleFormValidator {
    public FormValidator(Button buttonToDisable, Label errorMsgLabel) {
        super(buttonToDisable, errorMsgLabel);
    }

    public void fileExists(VisValidableTextField field, String errorMsg) {
        field.addValidator(new FileExistsValidator(errorMsg));
        add(field);
    }

    public void fileExists(VisValidableTextField field, VisTextField relativeTo, String errorMsg) {
        field.addValidator(new FileExistsValidator(relativeTo, errorMsg));
        add(field);
    }

    public void fileExists(VisValidableTextField field, VisTextField relativeTo, String errorMsg, boolean errorIfRelativeEmpty) {
        field.addValidator(new FileExistsValidator(relativeTo, errorMsg, false, errorIfRelativeEmpty));
        add(field);
    }

    public void fileExists(VisValidableTextField field, File relativeTo, String errorMsg) {
        field.addValidator(new FileExistsValidator(relativeTo, errorMsg));
        add(field);
    }

    public void fileExists(VisValidableTextField field, FileHandle relativeTo, String errorMsg) {
        field.addValidator(new FileExistsValidator(relativeTo.file(), errorMsg));
        add(field);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String):void
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String):void
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.lang.String, boolean):void */
    public void fileNotExists(VisValidableTextField field, String errorMsg) {
        field.addValidator(new FileExistsValidator(errorMsg, true));
        add(field);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void
     arg types: [com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, int]
     candidates:
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void */
    public void fileNotExists(VisValidableTextField field, VisTextField relativeTo, String errorMsg) {
        field.addValidator(new FileExistsValidator(relativeTo, errorMsg, true));
        add(field);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void
     arg types: [java.io.File, java.lang.String, int]
     candidates:
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void */
    public void fileNotExists(VisValidableTextField field, File relativeTo, String errorMsg) {
        field.addValidator(new FileExistsValidator(relativeTo, errorMsg, true));
        add(field);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void
     arg types: [java.io.File, java.lang.String, int]
     candidates:
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void
      com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void */
    public void fileNotExists(VisValidableTextField field, FileHandle relativeTo, String errorMsg) {
        field.addValidator(new FileExistsValidator(relativeTo.file(), errorMsg, true));
        add(field);
    }

    public static class FileExistsValidator extends FormInputValidator {
        boolean errorIfRelativeEmpty;
        boolean existNot;
        VisTextField relativeTo;
        File relativeToFile;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String):void
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String):void
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.lang.String, boolean):void */
        public FileExistsValidator(String errorMsg) {
            this(errorMsg, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void
         arg types: [com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, int]
         candidates:
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void */
        public FileExistsValidator(VisTextField relativeTo2, String errorMsg) {
            this(relativeTo2, errorMsg, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void
         arg types: [java.io.File, java.lang.String, int]
         candidates:
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(com.kotcrab.vis.ui.widget.VisTextField, java.lang.String, boolean):void
          com.kotcrab.vis.ui.util.form.FormValidator.FileExistsValidator.<init>(java.io.File, java.lang.String, boolean):void */
        public FileExistsValidator(File relativeTo2, String errorMsg) {
            this(relativeTo2, errorMsg, false);
        }

        public FileExistsValidator(String errorMsg, boolean existNot2) {
            super(errorMsg);
            this.existNot = existNot2;
        }

        public FileExistsValidator(VisTextField relativeTo2, String errorMsg, boolean existNot2) {
            super(errorMsg);
            this.relativeTo = relativeTo2;
            this.existNot = existNot2;
        }

        public FileExistsValidator(File relativeTo2, String errorMsg, boolean existNot2) {
            super(errorMsg);
            this.relativeToFile = relativeTo2;
            this.existNot = existNot2;
        }

        public FileExistsValidator(VisTextField relativeTo2, String errorMsg, boolean existNot2, boolean errorIfRelativeEmpty2) {
            super(errorMsg);
            this.relativeTo = relativeTo2;
            this.existNot = existNot2;
            this.errorIfRelativeEmpty = errorIfRelativeEmpty2;
        }

        public boolean validate(String input) {
            File file;
            if (this.relativeTo != null) {
                if (this.relativeTo.getText().length() == 0 && !this.errorIfRelativeEmpty) {
                    return true;
                }
                file = new File(this.relativeTo.getText(), input);
            } else if (this.relativeToFile != null) {
                file = new File(this.relativeToFile, input);
            } else {
                file = new File(input);
            }
            if (!this.existNot) {
                return file.exists();
            }
            if (file.exists()) {
                return false;
            }
            return true;
        }
    }
}
