package com.kotcrab.vis.ui.util.dialog;

public interface InputDialogListener {
    void canceled();

    void finished(String str);
}
