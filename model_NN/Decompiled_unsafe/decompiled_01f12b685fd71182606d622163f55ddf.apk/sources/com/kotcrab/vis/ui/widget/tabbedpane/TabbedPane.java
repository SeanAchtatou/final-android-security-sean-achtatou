package com.kotcrab.vis.ui.widget.tabbedpane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Scaling;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.i18n.BundleText;
import com.kotcrab.vis.ui.util.dialog.DialogUtils;
import com.kotcrab.vis.ui.util.dialog.OptionDialogAdapter;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import java.util.Iterator;

public class TabbedPane {
    /* access modifiers changed from: private */
    public Tab activeTab;
    private boolean allowTabDeselect;
    /* access modifiers changed from: private */
    public ButtonGroup<Button> group;
    private Array<TabbedPaneListener> listeners;
    private VisTable mainTable;
    /* access modifiers changed from: private */
    public TabbedPaneStyle style;
    private Array<Tab> tabs;
    /* access modifiers changed from: private */
    public ObjectMap<Tab, TabButtonTable> tabsButtonMap;
    private VisTable tabsTable;

    public TabbedPane() {
        this((TabbedPaneStyle) VisUI.getSkin().get(TabbedPaneStyle.class));
    }

    public TabbedPane(String styleName) {
        this((TabbedPaneStyle) VisUI.getSkin().get(styleName, TabbedPaneStyle.class));
    }

    public TabbedPane(TabbedPaneStyle style2) {
        this.style = style2;
        this.listeners = new Array<>();
        this.group = new ButtonGroup<>();
        this.mainTable = new VisTable();
        this.tabsTable = new VisTable();
        this.mainTable.setBackground(style2.background);
        this.tabs = new Array<>();
        this.tabsButtonMap = new ObjectMap<>();
        this.mainTable.add(this.tabsTable).left().expand();
        this.mainTable.row();
        if (style2.bottomBar != null) {
            this.mainTable.add(new Image(style2.bottomBar)).expand().fill().height(style2.bottomBar.getMinHeight());
        }
    }

    public void setAllowTabDeselect(boolean allowTabDeselect2) {
        this.allowTabDeselect = allowTabDeselect2;
        if (allowTabDeselect2) {
            this.group.setMinCheckCount(0);
        } else {
            this.group.setMinCheckCount(1);
        }
    }

    public boolean isAllowTabDeselect() {
        return this.allowTabDeselect;
    }

    public void add(Tab tab) {
        tab.setPane(this);
        this.tabs.add(tab);
        rebuildTabsTable();
        switchTab(tab);
    }

    public void insert(int index, Tab tab) {
        tab.setPane(this);
        this.tabs.insert(index, tab);
        rebuildTabsTable();
    }

    public boolean remove(Tab tab) {
        return remove(tab, true);
    }

    public boolean remove(final Tab tab, boolean ignoreTabDirty) {
        if (ignoreTabDirty) {
            return removeTab(tab);
        }
        if (!tab.isDirty() || this.mainTable.getStage() == null) {
            return removeTab(tab);
        }
        DialogUtils.showOptionDialog(this.mainTable.getStage(), get(Text.UNSAVED_DIALOG_TITLE), get(Text.UNSAVED_DIALOG_TEXT), DialogUtils.OptionDialogType.YES_NO_CANCEL, new OptionDialogAdapter() {
            public void yes() {
                tab.save();
                boolean unused = TabbedPane.this.removeTab(tab);
            }

            public void no() {
                boolean unused = TabbedPane.this.removeTab(tab);
            }
        });
        return false;
    }

    /* access modifiers changed from: private */
    public boolean removeTab(Tab tab) {
        int index = this.tabs.indexOf(tab, true);
        boolean success = this.tabs.removeValue(tab, true);
        if (success) {
            tab.setPane(null);
            tab.dispose();
            notifyListenersRemoved(tab);
            if (this.tabs.size == 0) {
                notifyListenersRemovedAll();
            } else if (this.activeTab == tab && index != 0) {
                switchTab(index - 1);
            }
            rebuildTabsTable();
        }
        return success;
    }

    public void removeAll() {
        Iterator<Tab> it = this.tabs.iterator();
        while (it.hasNext()) {
            Tab tab = it.next();
            tab.setPane(null);
            tab.dispose();
        }
        this.tabs.clear();
        rebuildTabsTable();
        notifyListenersRemovedAll();
    }

    public void switchTab(int index) {
        this.tabsButtonMap.get(this.tabs.get(index)).select();
    }

    public void switchTab(Tab tab) {
        this.tabsButtonMap.get(tab).select();
    }

    public void updateTabTitle(Tab tab) {
        this.tabsButtonMap.get(tab).button.setText(getTabTitle(tab));
    }

    /* access modifiers changed from: private */
    public String getTabTitle(Tab tab) {
        String title = tab.getTabTitle();
        if (tab.isDirty()) {
            return "*" + title;
        }
        return title;
    }

    private void rebuildTabsTable() {
        Tab lastSelectedTab = this.activeTab;
        this.tabsTable.clear();
        this.group.clear();
        this.tabsButtonMap.clear();
        Iterator<Tab> it = this.tabs.iterator();
        while (it.hasNext()) {
            Tab tab = it.next();
            TabButtonTable buttonTable = new TabButtonTable(tab);
            this.tabsTable.add(buttonTable);
            this.tabsButtonMap.put(tab, buttonTable);
            this.group.add(buttonTable.button);
            if (this.tabs.size == 1 && lastSelectedTab != null) {
                buttonTable.select();
                notifyListenersSwitched(tab);
            }
            if (tab == lastSelectedTab) {
                buttonTable.select();
            }
        }
    }

    public Table getTable() {
        return this.mainTable;
    }

    public void addListener(TabbedPaneListener listener) {
        this.listeners.add(listener);
    }

    public boolean removeListener(TabbedPaneListener listener) {
        return this.listeners.removeValue(listener, true);
    }

    /* access modifiers changed from: private */
    public void notifyListenersSwitched(Tab tab) {
        Iterator<TabbedPaneListener> it = this.listeners.iterator();
        while (it.hasNext()) {
            it.next().switchedTab(tab);
        }
    }

    private void notifyListenersRemoved(Tab tab) {
        Iterator<TabbedPaneListener> it = this.listeners.iterator();
        while (it.hasNext()) {
            it.next().removedTab(tab);
        }
    }

    private void notifyListenersRemovedAll() {
        Iterator<TabbedPaneListener> it = this.listeners.iterator();
        while (it.hasNext()) {
            it.next().removedAllTabs();
        }
    }

    public Array<Tab> getTabs() {
        return this.tabs;
    }

    private String get(Text text) {
        return VisUI.getTabbedPaneBundle().get(text.getName());
    }

    public static class TabbedPaneStyle {
        public Drawable background;
        public Drawable bottomBar;
        public VisTextButton.VisTextButtonStyle buttonStyle;

        public TabbedPaneStyle() {
        }

        public TabbedPaneStyle(TabbedPaneStyle other) {
            this.bottomBar = other.bottomBar;
            this.background = other.background;
            this.buttonStyle = other.buttonStyle;
        }

        public TabbedPaneStyle(Drawable background2, Drawable bottomBar2, VisTextButton.VisTextButtonStyle buttonStyle2) {
            this.background = background2;
            this.bottomBar = bottomBar2;
            this.buttonStyle = buttonStyle2;
        }
    }

    private class TabButtonTable extends VisTable {
        public VisTextButton button;
        /* access modifiers changed from: private */
        public VisTextButton.VisTextButtonStyle buttonStyle;
        public VisImageButton closeButton = new VisImageButton("close");
        /* access modifiers changed from: private */
        public VisImageButton.VisImageButtonStyle closeButtonStyle;
        /* access modifiers changed from: private */
        public Tab tab;

        public TabButtonTable(Tab tab2) {
            this.tab = tab2;
            this.button = new VisTextButton(TabbedPane.this.getTabTitle(tab2), TabbedPane.this.style.buttonStyle);
            this.button.setFocusBorderEnabled(false);
            this.closeButton.getImage().setScaling(Scaling.fill);
            this.closeButton.getImage().setColor(Color.RED);
            addListeners();
            this.buttonStyle = (VisTextButton.VisTextButtonStyle) this.button.getStyle();
            this.closeButtonStyle = this.closeButton.getStyle();
            add(this.button);
            if (tab2.isCloseableByUser()) {
                add(this.closeButton).size(14.0f, this.button.getHeight());
            }
        }

        private void addListeners() {
            this.closeButton.addListener(new ChangeListener() {
                public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                    TabButtonTable.this.closeTab();
                }
            });
            this.button.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int mouseButton) {
                    TabButtonTable.this.closeButtonStyle.up = TabButtonTable.this.buttonStyle.down;
                    if (mouseButton != 2) {
                        return false;
                    }
                    TabButtonTable.this.closeTab();
                    return false;
                }

                public boolean mouseMoved(InputEvent event, float x, float y) {
                    if (TabbedPane.this.activeTab == TabButtonTable.this.tab) {
                        return false;
                    }
                    TabButtonTable.this.closeButtonStyle.up = TabButtonTable.this.buttonStyle.over;
                    return false;
                }

                public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                    if (TabbedPane.this.activeTab != TabButtonTable.this.tab) {
                        TabButtonTable.this.closeButtonStyle.up = TabButtonTable.this.buttonStyle.up;
                    }
                }

                public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                    if (TabbedPane.this.activeTab != TabButtonTable.this.tab && !Gdx.input.justTouched()) {
                        TabButtonTable.this.closeButtonStyle.up = TabButtonTable.this.buttonStyle.over;
                    }
                }
            });
            this.button.addListener(new ChangeListener() {
                public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                    if (TabButtonTable.this.button.isChecked()) {
                        if (TabbedPane.this.activeTab != null) {
                            TabButtonTable table = (TabButtonTable) TabbedPane.this.tabsButtonMap.get(TabbedPane.this.activeTab);
                            if (table != null) {
                                table.deselect();
                            }
                            TabbedPane.this.activeTab.onHide();
                        }
                        Tab unused = TabbedPane.this.activeTab = TabButtonTable.this.tab;
                        TabbedPane.this.notifyListenersSwitched(TabButtonTable.this.tab);
                        TabButtonTable.this.tab.onShow();
                        TabButtonTable.this.closeButtonStyle.up = TabButtonTable.this.buttonStyle.down;
                        TabButtonTable.this.closeButtonStyle.over = null;
                    } else if (TabbedPane.this.group.getChecked() == null) {
                        if (TabbedPane.this.activeTab != null) {
                            TabButtonTable table2 = (TabButtonTable) TabbedPane.this.tabsButtonMap.get(TabbedPane.this.activeTab);
                            if (table2 != null) {
                                table2.deselect();
                            }
                            TabbedPane.this.activeTab.onHide();
                        }
                        Tab unused2 = TabbedPane.this.activeTab = null;
                        TabbedPane.this.notifyListenersSwitched(null);
                    }
                }
            });
        }

        /* access modifiers changed from: private */
        public void closeTab() {
            if (this.tab.isCloseableByUser()) {
                TabbedPane.this.remove(this.tab, false);
            }
        }

        public void select() {
            this.button.setChecked(true);
        }

        public void deselect() {
            this.closeButtonStyle.up = this.buttonStyle.up;
            this.closeButtonStyle.over = this.buttonStyle.over;
        }
    }

    private enum Text implements BundleText {
        UNSAVED_DIALOG_TITLE {
            public String getName() {
                return "unsavedDialogTitle";
            }
        },
        UNSAVED_DIALOG_TEXT {
            public String getName() {
                return "unsavedDialogText";
            }
        };

        public String get() {
            throw new UnsupportedOperationException();
        }

        public String format() {
            throw new UnsupportedOperationException();
        }

        public String format(Object... arguments) {
            throw new UnsupportedOperationException();
        }
    }
}
