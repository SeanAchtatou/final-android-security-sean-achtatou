package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;

public class Palette extends VisImage {
    private static final Drawable CROSS = VisUI.getSkin().getDrawable("color-picker-cross");
    private static final Drawable HORIZONTAL_SELECTOR = VisUI.getSkin().getDrawable("color-picker-selector-vertical");
    private static final Drawable VERTICAL_SELECTOR = VisUI.getSkin().getDrawable("color-picker-selector-horizontal");
    private int maxValue;
    private float selectorX;
    private float selectorY;
    private int x;
    private int y;

    public Palette(Texture texture, int x2, int y2, int maxValue2, ChangeListener listener) {
        super(texture);
        this.maxValue = maxValue2;
        setValue(x2, y2);
        addListener(listener);
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Palette.this.updateValueFromTouch(x, y);
                return true;
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                Palette.this.updateValueFromTouch(x, y);
            }
        });
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        HORIZONTAL_SELECTOR.draw(batch, getX(), ((getY() + this.selectorY) - (HORIZONTAL_SELECTOR.getMinHeight() / 2.0f)) + 0.1f, getImageWidth(), HORIZONTAL_SELECTOR.getMinHeight());
        VERTICAL_SELECTOR.draw(batch, ((getX() + this.selectorX) - (VERTICAL_SELECTOR.getMinWidth() / 2.0f)) + 0.1f, getY(), VERTICAL_SELECTOR.getMinWidth(), getImageHeight());
        CROSS.draw(batch, ((getX() + this.selectorX) - (CROSS.getMinWidth() / 2.0f)) + 0.1f, ((getY() + this.selectorY) - (CROSS.getMinHeight() / 2.0f)) + 0.1f, CROSS.getMinWidth(), CROSS.getMinHeight());
    }

    public void setValue(int v, int s) {
        this.x = v;
        this.y = s;
        if (this.x < 0) {
            this.x = 0;
        }
        if (this.x > this.maxValue) {
            this.x = this.maxValue;
        }
        if (this.y < 0) {
            this.y = 0;
        }
        if (this.y > this.maxValue) {
            this.y = this.maxValue;
        }
        this.selectorX = (((float) this.x) / ((float) this.maxValue)) * 160.0f;
        this.selectorY = (((float) this.y) / ((float) this.maxValue)) * 160.0f;
    }

    /* access modifiers changed from: private */
    public void updateValueFromTouch(float touchX, float touchY) {
        setValue((int) ((touchX / 160.0f) * ((float) this.maxValue)), (int) ((touchY / 160.0f) * ((float) this.maxValue)));
        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
        fire(changeEvent);
        Pools.free(changeEvent);
    }

    public int getS() {
        return this.x;
    }

    public int getV() {
        return this.y;
    }
}
