package com.kotcrab.vis.ui.building.utilities.layouts;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.building.OneColumnTableBuilder;
import com.kotcrab.vis.ui.building.OneRowTableBuilder;
import com.kotcrab.vis.ui.building.TableBuilder;
import com.kotcrab.vis.ui.building.utilities.CellWidget;

public enum TableLayout implements ActorLayout {
    VERTICAL {
        public Actor convertToActor(CellWidget<?>... widgets) {
            return convertToTable(new OneColumnTableBuilder(), widgets);
        }
    },
    HORIZONTAL {
        public Actor convertToActor(CellWidget<?>... widgets) {
            return convertToTable(new OneRowTableBuilder(), widgets);
        }
    };

    public Actor convertToActor(Actor... widgets) {
        return convertToActor(CellWidget.wrap(widgets));
    }

    public static Actor convertToTable(TableBuilder usingBuilder, CellWidget<?>... widgets) {
        for (CellWidget<?> widget : widgets) {
            usingBuilder.append((CellWidget<? extends Actor>) widget);
        }
        return usingBuilder.build();
    }

    public static GridTableLayout grid(int rowSize) {
        return GridTableLayout.withRowSize(rowSize);
    }
}
