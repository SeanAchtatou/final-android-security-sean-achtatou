package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.kotcrab.vis.ui.FocusManager;
import com.kotcrab.vis.ui.VisUI;

public class VisTree extends Tree {
    public VisTree(String styleName) {
        super(VisUI.getSkin(), styleName);
        init();
    }

    public VisTree() {
        super(VisUI.getSkin());
        init();
    }

    public VisTree(Tree.TreeStyle style) {
        super(style);
        init();
    }

    private void init() {
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                FocusManager.getFocus();
                return false;
            }
        });
    }
}
