package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.VisUI;

public class Separator extends Widget {
    private SeparatorStyle style;

    public Separator() {
        this.style = (SeparatorStyle) VisUI.getSkin().get(SeparatorStyle.class);
    }

    public Separator(boolean vertical) {
        this.style = (SeparatorStyle) VisUI.getSkin().get(vertical ? "vertical" : "default", SeparatorStyle.class);
    }

    public Separator(String styleName) {
        this.style = (SeparatorStyle) VisUI.getSkin().get(styleName, SeparatorStyle.class);
    }

    public Separator(SeparatorStyle style2) {
        this.style = style2;
    }

    public float getPrefHeight() {
        return (float) this.style.thickness;
    }

    public float getPrefWidth() {
        return (float) this.style.thickness;
    }

    public void draw(Batch batch, float parentAlpha) {
        Color c = getColor();
        batch.setColor(c.r, c.g, c.b, c.a * parentAlpha);
        this.style.background.draw(batch, getX(), getY(), getWidth(), getHeight());
    }

    public SeparatorStyle getStyle() {
        return this.style;
    }

    public static class SeparatorStyle {
        public Drawable background;
        public int thickness;
        public boolean vertical;

        public SeparatorStyle() {
        }

        public SeparatorStyle(Drawable bg, int thickness2) {
            this(bg, thickness2, false);
        }

        public SeparatorStyle(Drawable bg, int thickness2, boolean vertical2) {
            this.background = bg;
            this.thickness = thickness2;
            this.vertical = vertical2;
        }
    }
}
