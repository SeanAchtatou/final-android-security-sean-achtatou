package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.VisUI;

public class VisWindow extends Window {
    public static float FADE_TIME = 0.3f;
    private boolean centerOnAdd;

    public VisWindow(String title) {
        this(title, true);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VisWindow(String title, boolean showWindowBorder) {
        super(title, VisUI.getSkin(), showWindowBorder ? "default" : "noborder");
        getTitleLabel().setAlignment(VisUI.getDefaultTitleAlign());
    }

    public void setPosition(float x, float y) {
        super.setPosition((float) ((int) x), (float) ((int) y));
    }

    public boolean centerWindow() {
        if (getParent() == null) {
            this.centerOnAdd = true;
            return false;
        }
        moveToCenter();
        return true;
    }

    /* access modifiers changed from: protected */
    public void setStage(Stage stage) {
        super.setStage(stage);
        if (this.centerOnAdd) {
            this.centerOnAdd = false;
            moveToCenter();
        }
    }

    private void moveToCenter() {
        Stage parent = getStage();
        if (parent != null) {
            setPosition((parent.getWidth() - getWidth()) / 2.0f, (parent.getHeight() - getHeight()) / 2.0f);
        }
    }

    public void fadeOut(float time) {
        addAction(Actions.sequence(Actions.fadeOut(time, Interpolation.fade), Actions.removeActor()));
    }

    public VisWindow fadeIn(float time) {
        setColor(1.0f, 1.0f, 1.0f, Animation.CurveTimeline.LINEAR);
        addAction(Actions.fadeIn(time, Interpolation.fade));
        return this;
    }

    public void fadeOut() {
        fadeOut(FADE_TIME);
    }

    public VisWindow fadeIn() {
        return fadeIn(FADE_TIME);
    }

    /* access modifiers changed from: protected */
    public void close() {
        fadeOut();
    }

    public void addCloseButton() {
        VisImageButton closeButton = new VisImageButton("close-window");
        getTitleTable().add(closeButton).padRight((-getPadRight()) + 0.7f);
        closeButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                VisWindow.this.close();
            }
        });
        if (getTitleTable().getChildren().size == 2) {
            getTitleTable().getCell(getTitleLabel()).padLeft(closeButton.getWidth() * 2.0f);
        }
    }

    public void closeOnEscape() {
        addListener(new InputListener() {
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode != 131) {
                    return false;
                }
                VisWindow.this.close();
                return true;
            }
        });
    }
}
