package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.kotcrab.vis.ui.InputValidator;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.Validators;
import com.kotcrab.vis.ui.widget.VisTextField;
import java.util.Iterator;

public class NumberSelector extends VisTable {
    /* access modifiers changed from: private */
    public float buttonRepeatInitialTime;
    /* access modifiers changed from: private */
    public ButtonRepeatTask buttonRepeatTask;
    /* access modifiers changed from: private */
    public float buttonRepeatTime;
    private int current;
    private Array<NumberSelectorListener> listeners;
    private int max;
    private int min;
    private int step;
    /* access modifiers changed from: private */
    public VisValidableTextField text;

    public interface NumberSelectorListener {
        void changed(int i);
    }

    public NumberSelector(String name, int initialValue, int min2, int max2) {
        this(name, initialValue, min2, max2, 1);
    }

    public NumberSelector(String name, int initialValue, int min2, int max2, int step2) {
        this("default", name, initialValue, min2, max2, step2);
    }

    public NumberSelector(String styleName, String name, int initialValue, int min2, int max2, int step2) {
        this((NumberSelectorStyle) VisUI.getSkin().get(styleName, NumberSelectorStyle.class), name, initialValue, min2, max2, step2);
    }

    public NumberSelector(NumberSelectorStyle style, String name, int initialValue, int min2, int max2, int step2) {
        this.buttonRepeatTask = new ButtonRepeatTask();
        this.buttonRepeatInitialTime = 0.4f;
        this.buttonRepeatTime = 0.1f;
        this.listeners = new Array<>();
        this.current = initialValue;
        this.max = max2;
        this.min = min2;
        this.step = step2;
        VisTable buttonsTable = new VisTable();
        VisImageButton up = new VisImageButton(style.up);
        VisImageButton down = new VisImageButton(style.down);
        this.text = new VisValidableTextField(Validators.INTEGERS) {
            public float getPrefWidth() {
                return 40.0f;
            }
        };
        this.text.setProgrammaticChangeEvents(false);
        this.text.setTextFieldFilter(new VisTextField.TextFieldFilter.DigitsOnlyFilter());
        this.text.setText(String.valueOf(this.current));
        this.text.addValidator(new InputValidator() {
            public boolean validateInput(String input) {
                return NumberSelector.this.checkInput(input);
            }
        });
        buttonsTable.add(up).height(12.0f).row();
        buttonsTable.add(down).height(12.0f);
        add(name);
        add(this.text).fillX().expandX().height(24.0f).padLeft(6.0f).padRight(1.0f);
        add(buttonsTable).width(12.0f);
        up.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                NumberSelector.this.getStage().setScrollFocus(NumberSelector.this.text);
                NumberSelector.this.increment();
            }
        });
        down.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                NumberSelector.this.getStage().setScrollFocus(NumberSelector.this.text);
                NumberSelector.this.decrement();
            }
        });
        up.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!NumberSelector.this.buttonRepeatTask.isScheduled()) {
                    NumberSelector.this.buttonRepeatTask.increment = true;
                    NumberSelector.this.buttonRepeatTask.cancel();
                    Timer.schedule(NumberSelector.this.buttonRepeatTask, NumberSelector.this.buttonRepeatInitialTime, NumberSelector.this.buttonRepeatTime);
                }
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                NumberSelector.this.buttonRepeatTask.cancel();
            }
        });
        down.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (NumberSelector.this.buttonRepeatTask.isScheduled()) {
                    return true;
                }
                NumberSelector.this.buttonRepeatTask.increment = false;
                NumberSelector.this.buttonRepeatTask.cancel();
                Timer.schedule(NumberSelector.this.buttonRepeatTask, NumberSelector.this.buttonRepeatInitialTime, NumberSelector.this.buttonRepeatTime);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                NumberSelector.this.buttonRepeatTask.cancel();
            }
        });
        this.text.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                NumberSelector.this.textChanged();
            }
        });
        this.text.addListener(new FocusListener() {
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                NumberSelector.this.valueChanged();
                NumberSelector.this.getStage().setScrollFocus(null);
            }
        });
        this.text.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                NumberSelector.this.getStage().setScrollFocus(NumberSelector.this.text);
                return true;
            }

            public boolean scrolled(InputEvent event, float x, float y, int amount) {
                if (amount == 1) {
                    NumberSelector.this.decrement();
                } else {
                    NumberSelector.this.increment();
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void textChanged() {
        if (!this.text.getText().equals("")) {
            if (checkInput(this.text.getText())) {
                this.current = Integer.parseInt(this.text.getText());
            } else {
                valueChanged();
            }
        }
    }

    public void increment() {
        if (this.current + this.step > this.max) {
            this.current = this.max;
        } else {
            this.current += this.step;
        }
        valueChanged();
    }

    public void decrement() {
        if (this.current - this.step < this.min) {
            this.current = this.min;
        } else {
            this.current -= this.step;
        }
        valueChanged();
    }

    public void setValue(int newValue) {
        if (newValue > this.max) {
            this.current = this.max;
        } else if (newValue < this.min) {
            this.current = this.min;
        } else {
            this.current = newValue;
        }
        valueChanged();
    }

    public int getValue() {
        return this.current;
    }

    /* access modifiers changed from: private */
    public boolean checkInput(String input) {
        try {
            int x = Integer.parseInt(input);
            if (x < this.min || x > this.max) {
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void valueChanged() {
        int pos = this.text.getCursorPosition();
        this.text.setText(String.valueOf(this.current));
        this.text.setCursorPosition(pos);
        Iterator<NumberSelectorListener> it = this.listeners.iterator();
        while (it.hasNext()) {
            it.next().changed(this.current);
        }
    }

    public void addChangeListener(NumberSelectorListener listener) {
        if (listener != null && !this.listeners.contains(listener, true)) {
            this.listeners.add(listener);
        }
    }

    public static class NumberSelectorStyle {
        public Drawable down;
        public Drawable up;

        public NumberSelectorStyle() {
        }

        public NumberSelectorStyle(Drawable up2, Drawable down2) {
            this.up = up2;
            this.down = down2;
        }
    }

    private class ButtonRepeatTask extends Timer.Task {
        boolean increment;

        private ButtonRepeatTask() {
        }

        public void run() {
            if (this.increment) {
                NumberSelector.this.increment();
            } else {
                NumberSelector.this.decrement();
            }
        }
    }
}
