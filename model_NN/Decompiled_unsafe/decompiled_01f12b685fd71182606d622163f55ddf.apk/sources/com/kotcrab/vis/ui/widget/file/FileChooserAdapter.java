package com.kotcrab.vis.ui.widget.file;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

public class FileChooserAdapter implements FileChooserListener {
    public void canceled() {
    }

    public void selected(Array<FileHandle> array) {
    }

    public void selected(FileHandle file) {
    }
}
