package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.kbz.esotericsoftware.spine.Animation;

public class CollapsibleWidget extends WidgetGroup {
    /* access modifiers changed from: private */
    public boolean actionRunning;
    /* access modifiers changed from: private */
    public boolean collapsed;
    /* access modifiers changed from: private */
    public float currentHeight;
    /* access modifiers changed from: private */
    public Table table;

    public CollapsibleWidget(Table table2) {
        this(table2, false);
    }

    public CollapsibleWidget(Table table2, boolean collapsed2) {
        this.collapsed = collapsed2;
        this.table = table2;
        updateTouchable();
        addActor(table2);
    }

    public void setCollapsed(boolean collapse, boolean withAnimation) {
        this.collapsed = collapse;
        this.actionRunning = true;
        updateTouchable();
        if (withAnimation) {
            addAction(new Action() {
                public boolean act(float delta) {
                    if (CollapsibleWidget.this.collapsed) {
                        float unused = CollapsibleWidget.this.currentHeight = CollapsibleWidget.this.currentHeight - (1000.0f * delta);
                        if (CollapsibleWidget.this.currentHeight <= Animation.CurveTimeline.LINEAR) {
                            float unused2 = CollapsibleWidget.this.currentHeight = Animation.CurveTimeline.LINEAR;
                            boolean unused3 = CollapsibleWidget.this.collapsed = true;
                            boolean unused4 = CollapsibleWidget.this.actionRunning = false;
                        }
                    } else {
                        float unused5 = CollapsibleWidget.this.currentHeight = CollapsibleWidget.this.currentHeight + (1000.0f * delta);
                        if (CollapsibleWidget.this.currentHeight > CollapsibleWidget.this.table.getPrefHeight()) {
                            float unused6 = CollapsibleWidget.this.currentHeight = CollapsibleWidget.this.table.getPrefHeight();
                            boolean unused7 = CollapsibleWidget.this.collapsed = false;
                            boolean unused8 = CollapsibleWidget.this.actionRunning = false;
                        }
                    }
                    CollapsibleWidget.this.invalidateHierarchy();
                    if (!CollapsibleWidget.this.actionRunning) {
                        return true;
                    }
                    return false;
                }
            });
            return;
        }
        if (collapse) {
            this.currentHeight = Animation.CurveTimeline.LINEAR;
            this.collapsed = true;
        } else {
            this.currentHeight = this.table.getPrefHeight();
            this.collapsed = false;
        }
        this.actionRunning = false;
        invalidateHierarchy();
    }

    public void setCollapsed(boolean collapse) {
        setCollapsed(collapse, true);
    }

    private void updateTouchable() {
        if (this.collapsed) {
            setTouchable(Touchable.disabled);
        } else {
            setTouchable(Touchable.enabled);
        }
    }

    public boolean isCollapsed() {
        return this.collapsed;
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.currentHeight > 1.0f) {
            batch.flush();
            boolean clipEnabled = clipBegin(getX(), getY(), getWidth(), this.currentHeight);
            super.draw(batch, parentAlpha);
            batch.flush();
            if (clipEnabled) {
                clipEnd();
            }
        }
    }

    public void layout() {
        super.layout();
        this.table.setBounds(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, this.table.getPrefWidth(), this.table.getPrefHeight());
        if (this.actionRunning) {
            return;
        }
        if (this.collapsed) {
            this.currentHeight = Animation.CurveTimeline.LINEAR;
        } else {
            this.currentHeight = this.table.getPrefHeight();
        }
    }

    public float getPrefWidth() {
        return this.table.getPrefWidth();
    }

    public float getPrefHeight() {
        if (this.actionRunning) {
            return this.currentHeight;
        }
        if (this.collapsed) {
            return Animation.CurveTimeline.LINEAR;
        }
        return this.table.getPrefHeight();
    }

    /* access modifiers changed from: protected */
    public void setStage(Stage stage) {
        super.setStage(stage);
    }
}
