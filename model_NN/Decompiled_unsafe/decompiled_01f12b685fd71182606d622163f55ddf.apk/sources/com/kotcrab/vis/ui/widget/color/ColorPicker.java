package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Disposable;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.util.ColorUtils;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisValidableTextField;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.kotcrab.vis.ui.widget.color.ColorChannelWidget;
import com.sg.pak.PAK_ASSETS;

public class ColorPicker extends VisWindow implements Disposable {
    static final int BAR_HEIGHT = 11;
    static final int BAR_WIDTH = 130;
    static final int FIELD_WIDTH = 50;
    static final int HEX_FIELD_WIDTH = 95;
    static final int PALETTE_SIZE = 160;
    static final float VERTICAL_BAR_WIDTH = 15.0f;
    private static final Drawable WHITE = VisUI.getSkin().getDrawable("white");
    /* access modifiers changed from: private */
    public ColorChannelWidget aBar;
    private ColorChannelWidget bBar;
    private Pixmap barPixmap;
    private Texture barTexture;
    private VisTextButton cancelButton;
    /* access modifiers changed from: private */
    public Color color;
    private Image currentColor;
    private ColorChannelWidget gBar;
    /* access modifiers changed from: private */
    public ColorChannelWidget hBar;
    /* access modifiers changed from: private */
    public VisValidableTextField hexField;
    /* access modifiers changed from: private */
    public ColorPickerListener listener;
    private Image newColor;
    private VisTextButton okButton;
    /* access modifiers changed from: private */
    public Color oldColor;
    /* access modifiers changed from: private */
    public Palette palette;
    private Pixmap palettePixmap;
    private Texture paletteTexture;
    private ColorChannelWidget rBar;
    private VisTextButton restoreButton;
    /* access modifiers changed from: private */
    public ColorChannelWidget sBar;
    /* access modifiers changed from: private */
    public Color tmpColor;
    /* access modifiers changed from: private */
    public ColorChannelWidget vBar;
    /* access modifiers changed from: private */
    public VerticalChannelBar verticalBar;

    public ColorPicker() {
        this("Color Picker");
    }

    public ColorPicker(String title) {
        this(title, null);
    }

    public ColorPicker(ColorPickerListener listener2) {
        this("Color Picker", listener2);
    }

    public ColorPicker(String title, ColorPickerListener listener2) {
        super(title);
        this.listener = listener2;
        setModal(true);
        setMovable(true);
        addCloseButton();
        closeOnEscape();
        this.oldColor = new Color(Color.BLACK);
        this.color = new Color(Color.BLACK);
        this.tmpColor = new Color(Color.BLACK);
        createColorWidgets();
        createUI();
        createListeners();
        updatePixmaps();
        pack();
        centerWindow();
    }

    private void createUI() {
        VisTable rightTable = new VisTable(true);
        rightTable.add(this.hBar).row();
        rightTable.add(this.sBar).row();
        rightTable.add(this.vBar).row();
        rightTable.add();
        rightTable.row();
        rightTable.add(this.rBar).row();
        rightTable.add(this.gBar).row();
        rightTable.add(this.bBar).row();
        rightTable.add();
        rightTable.row();
        rightTable.add(this.aBar).row();
        VisTable leftTable = new VisTable(true);
        leftTable.add(this.palette).size(160.0f);
        leftTable.row();
        leftTable.add(createColorsPreviewTable()).expandX().fillX();
        leftTable.row();
        leftTable.add(createHexTable()).expandX().left();
        add(leftTable).top().padRight(5.0f);
        add(this.verticalBar).size((float) VERTICAL_BAR_WIDTH, 160.0f).top();
        add(rightTable).expand().left().top().pad(4.0f);
        row();
        add(createButtons()).pad(3.0f).right().expandX().colspan(3);
    }

    private VisTable createColorsPreviewTable() {
        VisTable table = new VisTable(false);
        table.add(new VisLabel("Old")).spaceRight(3.0f);
        AlphaImage alphaImage = new AlphaImage(WHITE);
        this.currentColor = alphaImage;
        table.add(alphaImage).height(25.0f).expandX().fillX();
        table.row();
        table.add(new VisLabel("New")).spaceRight(3.0f);
        AlphaImage alphaImage2 = new AlphaImage(WHITE, true);
        this.newColor = alphaImage2;
        table.add(alphaImage2).height(25.0f).expandX().fillX();
        this.currentColor.setColor(this.color);
        this.newColor.setColor(this.color);
        return table;
    }

    private VisTable createHexTable() {
        VisTable table = new VisTable(true);
        table.add(new VisLabel("Hex"));
        VisValidableTextField visValidableTextField = new VisValidableTextField("00000000");
        this.hexField = visValidableTextField;
        table.add(visValidableTextField).width(95.0f);
        table.row();
        this.hexField.setMaxLength(8);
        this.hexField.setProgrammaticChangeEvents(false);
        this.hexField.setTextFieldFilter(new VisTextField.TextFieldFilter() {
            public boolean acceptChar(VisTextField textField, char c) {
                return Character.isDigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
            }
        });
        this.hexField.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                if (ColorPicker.this.hexField.getText().length() == 8) {
                    ColorPicker.this.setColor(Color.valueOf(ColorPicker.this.hexField.getText()));
                }
            }
        });
        return table;
    }

    private VisTable createButtons() {
        VisTable table = new VisTable(true);
        table.defaults().right();
        VisTextButton visTextButton = new VisTextButton("Restore");
        this.restoreButton = visTextButton;
        table.add(visTextButton);
        VisTextButton visTextButton2 = new VisTextButton("OK");
        this.okButton = visTextButton2;
        table.add(visTextButton2);
        VisTextButton visTextButton3 = new VisTextButton("Cancel");
        this.cancelButton = visTextButton3;
        table.add(visTextButton3);
        return table;
    }

    private void createColorWidgets() {
        this.palettePixmap = new Pixmap(100, 100, Pixmap.Format.RGB888);
        this.paletteTexture = new Texture(this.palettePixmap);
        this.barPixmap = new Pixmap(1, (int) PAK_ASSETS.IMG_XINJILU, Pixmap.Format.RGB888);
        for (int h = 0; h < 360; h++) {
            ColorUtils.HSVtoRGB((float) (360 - h), 100.0f, 100.0f, this.tmpColor);
            this.barPixmap.drawPixel(0, h, Color.rgba8888(this.tmpColor));
        }
        this.barTexture = new Texture(this.barPixmap);
        this.palette = new Palette(this.paletteTexture, 0, 0, 100, new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                ColorPicker.this.sBar.setValue(ColorPicker.this.palette.getV());
                ColorPicker.this.vBar.setValue(ColorPicker.this.palette.getS());
                ColorPicker.this.updateHSVValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }
        });
        this.verticalBar = new VerticalChannelBar(this.barTexture, 0, PAK_ASSETS.IMG_XINJILU, new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                ColorPicker.this.hBar.setValue(ColorPicker.this.verticalBar.getValue());
                ColorPicker.this.updateHSVValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }
        });
        this.hBar = new ColorChannelWidget("H", PAK_ASSETS.IMG_XINJILU, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                ColorPicker.this.verticalBar.setValue(ColorPicker.this.hBar.getValue());
                ColorPicker.this.updateHSVValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                for (int h = 0; h < 360; h++) {
                    ColorUtils.HSVtoRGB((float) h, (float) ColorPicker.this.sBar.getValue(), (float) ColorPicker.this.vBar.getValue(), ColorPicker.this.tmpColor);
                    pixmap.drawPixel(h, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
        this.sBar = new ColorChannelWidget("S", 100, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                ColorPicker.this.palette.setValue(ColorPicker.this.vBar.getValue(), ColorPicker.this.sBar.getValue());
                ColorPicker.this.updateHSVValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                for (int s = 0; s < 100; s++) {
                    ColorUtils.HSVtoRGB((float) ColorPicker.this.hBar.getValue(), (float) s, (float) ColorPicker.this.vBar.getValue(), ColorPicker.this.tmpColor);
                    pixmap.drawPixel(s, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
        this.vBar = new ColorChannelWidget("V", 100, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                ColorPicker.this.palette.setValue(ColorPicker.this.vBar.getValue(), ColorPicker.this.sBar.getValue());
                ColorPicker.this.updateHSVValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                for (int v = 0; v < 100; v++) {
                    ColorUtils.HSVtoRGB((float) ColorPicker.this.hBar.getValue(), (float) ColorPicker.this.sBar.getValue(), (float) v, ColorPicker.this.tmpColor);
                    pixmap.drawPixel(v, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
        this.rBar = new ColorChannelWidget("R", 255, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                ColorPicker.this.updateRGBValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                for (int r = 0; r < 255; r++) {
                    ColorPicker.this.tmpColor.set(((float) r) / 255.0f, ColorPicker.this.color.g, ColorPicker.this.color.b, 1.0f);
                    pixmap.drawPixel(r, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
        this.gBar = new ColorChannelWidget("G", 255, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                ColorPicker.this.updateRGBValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                for (int g = 0; g < 255; g++) {
                    ColorPicker.this.tmpColor.set(ColorPicker.this.color.r, ((float) g) / 255.0f, ColorPicker.this.color.b, 1.0f);
                    pixmap.drawPixel(g, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
        this.bBar = new ColorChannelWidget("B", 255, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                ColorPicker.this.updateRGBValuesFromFields();
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                for (int b = 0; b < 255; b++) {
                    ColorPicker.this.tmpColor.set(ColorPicker.this.color.r, ColorPicker.this.color.g, ((float) b) / 255.0f, 1.0f);
                    pixmap.drawPixel(b, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
        this.aBar = new ColorChannelWidget("A", 255, true, new ColorChannelWidget.ColorChannelWidgetListener() {
            public void updateFields() {
                if (ColorPicker.this.aBar.isInputValid()) {
                    ColorPicker.this.color.a = ((float) ColorPicker.this.aBar.getValue()) / 255.0f;
                }
                ColorPicker.this.updatePixmaps();
            }

            public void draw(Pixmap pixmap) {
                pixmap.fill();
                for (int i = 0; i < 255; i++) {
                    ColorPicker.this.tmpColor.set(ColorPicker.this.color.r, ColorPicker.this.color.g, ColorPicker.this.color.b, ((float) i) / 255.0f);
                    pixmap.drawPixel(i, 0, Color.rgba8888(ColorPicker.this.tmpColor));
                }
            }
        });
    }

    private void createListeners() {
        this.restoreButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                ColorPicker.this.setColor(ColorPicker.this.oldColor);
            }
        });
        this.okButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                if (ColorPicker.this.listener != null) {
                    ColorPicker.this.listener.finished(new Color(ColorPicker.this.color));
                }
                ColorPicker.this.setColor(ColorPicker.this.color);
                ColorPicker.this.fadeOut();
            }
        });
        this.cancelButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                ColorPicker.this.setColor(ColorPicker.this.oldColor);
                ColorPicker.this.close();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void close() {
        if (this.listener != null) {
            this.listener.canceled();
        }
        super.close();
    }

    public ColorPickerListener getListener() {
        return this.listener;
    }

    public void setListener(ColorPickerListener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: private */
    public void updatePixmaps() {
        for (int v = 0; v <= 100; v++) {
            for (int s = 0; s <= 100; s++) {
                ColorUtils.HSVtoRGB((float) this.hBar.getValue(), (float) s, (float) v, this.tmpColor);
                this.palettePixmap.drawPixel(v, 100 - s, Color.rgba8888(this.tmpColor));
            }
        }
        this.paletteTexture.draw(this.palettePixmap, 0, 0);
        this.newColor.setColor(this.color);
        this.hBar.redraw();
        this.sBar.redraw();
        this.vBar.redraw();
        this.rBar.redraw();
        this.gBar.redraw();
        this.bBar.redraw();
        this.aBar.redraw();
        this.hexField.setText(this.color.toString().toUpperCase());
        this.hexField.setCursorPosition(this.hexField.getMaxLength());
    }

    public void setColor(Color c) {
        this.currentColor.setColor(new Color(c));
        this.oldColor = new Color(c);
        this.color = new Color(c);
        updateFieldsFromColor();
        updatePixmaps();
    }

    public void dispose() {
        this.paletteTexture.dispose();
        this.barTexture.dispose();
        this.palettePixmap.dispose();
        this.barPixmap.dispose();
        this.hBar.dispose();
        this.sBar.dispose();
        this.vBar.dispose();
        this.rBar.dispose();
        this.gBar.dispose();
        this.bBar.dispose();
        this.aBar.dispose();
    }

    private void updateFieldsFromColor() {
        int[] hsv = ColorUtils.RGBtoHSV(this.color);
        int ch = hsv[0];
        int cs = hsv[1];
        int cv = hsv[2];
        int cr = MathUtils.round(this.color.r * 255.0f);
        int cg = MathUtils.round(this.color.g * 255.0f);
        int cb = MathUtils.round(this.color.b * 255.0f);
        int ca = MathUtils.round(this.color.a * 255.0f);
        this.hBar.setValue(ch);
        this.sBar.setValue(cs);
        this.vBar.setValue(cv);
        this.rBar.setValue(cr);
        this.gBar.setValue(cg);
        this.bBar.setValue(cb);
        this.aBar.setValue(ca);
        this.verticalBar.setValue(this.hBar.getValue());
        this.palette.setValue(this.vBar.getValue(), this.sBar.getValue());
    }

    /* access modifiers changed from: private */
    public void updateHSVValuesFromFields() {
        int[] hsv = ColorUtils.RGBtoHSV(this.color);
        int h = hsv[0];
        int s = hsv[1];
        int v = hsv[2];
        if (this.hBar.isInputValid()) {
            h = this.hBar.getValue();
        }
        if (this.sBar.isInputValid()) {
            s = this.sBar.getValue();
        }
        if (this.vBar.isInputValid()) {
            v = this.vBar.getValue();
        }
        this.color = ColorUtils.HSVtoRGB((float) h, (float) s, (float) v, this.color.a);
        int cr = MathUtils.round(this.color.r * 255.0f);
        int cg = MathUtils.round(this.color.g * 255.0f);
        int cb = MathUtils.round(this.color.b * 255.0f);
        this.rBar.setValue(cr);
        this.gBar.setValue(cg);
        this.bBar.setValue(cb);
    }

    /* access modifiers changed from: private */
    public void updateRGBValuesFromFields() {
        int r = MathUtils.round(this.color.r * 255.0f);
        int g = MathUtils.round(this.color.g * 255.0f);
        int b = MathUtils.round(this.color.b * 255.0f);
        if (this.rBar.isInputValid()) {
            r = this.rBar.getValue();
        }
        if (this.gBar.isInputValid()) {
            g = this.gBar.getValue();
        }
        if (this.bBar.isInputValid()) {
            b = this.bBar.getValue();
        }
        this.color.set(((float) r) / 255.0f, ((float) g) / 255.0f, ((float) b) / 255.0f, this.color.a);
        int[] hsv = ColorUtils.RGBtoHSV(this.color);
        int ch = hsv[0];
        int cs = hsv[1];
        int cv = hsv[2];
        this.hBar.setValue(ch);
        this.sBar.setValue(cs);
        this.vBar.setValue(cv);
        this.verticalBar.setValue(this.hBar.getValue());
        this.palette.setValue(this.vBar.getValue(), this.sBar.getValue());
    }
}
