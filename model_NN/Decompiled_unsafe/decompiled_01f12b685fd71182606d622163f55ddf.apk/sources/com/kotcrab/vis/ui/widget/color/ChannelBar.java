package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;

public class ChannelBar extends VisImage {
    private static final Drawable BAR_SELECTOR = VisUI.getSkin().getDrawable("color-picker-bar-selector");
    private int maxValue;
    private float selectorX;
    private int value;

    public ChannelBar(Texture texture, int value2, int maxValue2, ChangeListener listener) {
        super(texture);
        this.maxValue = maxValue2;
        setValue(value2);
        addListener(listener);
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ChannelBar.this.updateValueFromTouch(x);
                return true;
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                ChannelBar.this.updateValueFromTouch(x);
            }
        });
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        BAR_SELECTOR.draw(batch, (getX() + this.selectorX) - (BAR_SELECTOR.getMinWidth() / 2.0f), getY() - 2.0f, BAR_SELECTOR.getMinWidth(), BAR_SELECTOR.getMinHeight());
    }

    public void setValue(int newValue) {
        this.value = newValue;
        if (this.value < 0) {
            this.value = 0;
        }
        if (this.value > this.maxValue) {
            this.value = this.maxValue;
        }
        this.selectorX = (((float) this.value) / ((float) this.maxValue)) * 130.0f;
    }

    public int getValue() {
        return this.value;
    }

    /* access modifiers changed from: private */
    public void updateValueFromTouch(float x) {
        setValue((int) ((x / 130.0f) * ((float) this.maxValue)));
        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
        fire(changeEvent);
        Pools.free(changeEvent);
    }
}
