package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.scenes.scene2d.utils.UIUtils;
import com.badlogic.gdx.utils.Clipboard;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.FocusManager;
import com.kotcrab.vis.ui.Focusable;
import com.kotcrab.vis.ui.VisUI;

public class VisTextField extends Widget implements Disableable, Focusable {
    private static final char BACKSPACE = '\b';
    private static final char BULLET = '•';
    private static final char DELETE = '';
    protected static final char ENTER_ANDROID = '\n';
    protected static final char ENTER_DESKTOP = '\r';
    private static final char TAB = '\t';
    public static float keyRepeatInitialTime = 0.4f;
    public static float keyRepeatTime = 0.05f;
    private static final Vector2 tmp1 = new Vector2();
    private static final Vector2 tmp2 = new Vector2();
    private static final Vector2 tmp3 = new Vector2();
    private float blinkTime;
    private ClickListener clickListener;
    private Clipboard clipboard;
    protected int cursor;
    boolean cursorOn;
    boolean disabled;
    protected CharSequence displayText;
    /* access modifiers changed from: private */
    public boolean drawBorder;
    TextFieldFilter filter;
    boolean focusTraversal;
    protected final FloatArray glyphPositions;
    protected boolean hasSelection;
    InputListener inputListener;
    private boolean inputValid;
    KeyRepeatTask keyRepeatTask;
    KeyTypedRepeatTask keyTypedRepeatTask;
    TextField.OnscreenKeyboard keyboard;
    long lastBlink;
    protected final GlyphLayout layout;
    TextFieldListener listener;
    private int maxLength;
    private String messageText;
    boolean onlyFontChars;
    private StringBuilder passwordBuffer;
    private char passwordCharacter;
    boolean passwordMode;
    float renderOffset;
    protected int selectionStart;
    private float selectionWidth;
    private float selectionX;
    VisTextFieldStyle style;
    protected String text;
    private int textHAlign;
    protected float textHeight;
    protected float textOffset;
    private int visibleTextEnd;
    private int visibleTextStart;
    protected boolean writeEnters;

    public interface TextFieldListener {
        void keyTyped(VisTextField visTextField, char c);
    }

    public VisTextField() {
        this("", (VisTextFieldStyle) VisUI.getSkin().get(VisTextFieldStyle.class));
    }

    public VisTextField(String text2) {
        this(text2, (VisTextFieldStyle) VisUI.getSkin().get(VisTextFieldStyle.class));
    }

    public VisTextField(String text2, String styleName) {
        this(text2, (VisTextFieldStyle) VisUI.getSkin().get(styleName, VisTextFieldStyle.class));
    }

    public VisTextField(String text2, VisTextFieldStyle style2) {
        this.layout = new GlyphLayout();
        this.glyphPositions = new FloatArray();
        this.keyboard = new TextField.DefaultOnscreenKeyboard();
        this.focusTraversal = true;
        this.onlyFontChars = true;
        this.textHAlign = 8;
        this.passwordCharacter = BULLET;
        this.maxLength = 0;
        this.blinkTime = 0.32f;
        this.cursorOn = true;
        this.keyRepeatTask = new KeyRepeatTask();
        this.keyTypedRepeatTask = new KeyTypedRepeatTask();
        this.inputValid = true;
        setStyle(style2);
        this.clipboard = Gdx.app.getClipboard();
        initialize();
        setText(text2);
        setSize(getPrefWidth(), getPrefHeight());
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        this.writeEnters = false;
        InputListener createInputListener = createInputListener();
        this.inputListener = createInputListener;
        addListener(createInputListener);
        ClickListener clickListener2 = new ClickListener();
        this.clickListener = clickListener2;
        addListener(clickListener2);
        addListener(new FocusListener() {
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused) {
                    VisTextField.this.keyTypedRepeatTask.cancel();
                    VisTextField.this.keyRepeatTask.cancel();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public InputListener createInputListener() {
        return new TextFieldClickListener();
    }

    /* access modifiers changed from: protected */
    public int letterUnderCursor(float x) {
        float x2 = x - (this.renderOffset + this.textOffset);
        int index = this.glyphPositions.size - 1;
        float[] glyphPositions2 = this.glyphPositions.items;
        int i = 0;
        int n = this.glyphPositions.size;
        while (true) {
            if (i >= n) {
                break;
            } else if (glyphPositions2[i] > x2) {
                index = i - 1;
                break;
            } else {
                i++;
            }
        }
        return Math.max(0, index);
    }

    /* access modifiers changed from: protected */
    public boolean isWordCharacter(char c) {
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9');
    }

    /* access modifiers changed from: protected */
    public int[] wordUnderCursor(int at) {
        String text2 = this.text;
        int start = at;
        int right = text2.length();
        int left = 0;
        int index = start;
        while (true) {
            if (index >= right) {
                break;
            } else if (!isWordCharacter(text2.charAt(index))) {
                right = index;
                break;
            } else {
                index++;
            }
        }
        int index2 = start - 1;
        while (true) {
            if (index2 <= -1) {
                break;
            } else if (!isWordCharacter(text2.charAt(index2))) {
                left = index2 + 1;
                break;
            } else {
                index2--;
            }
        }
        return new int[]{left, right};
    }

    /* access modifiers changed from: package-private */
    public int[] wordUnderCursor(float x) {
        return wordUnderCursor(letterUnderCursor(x));
    }

    /* access modifiers changed from: package-private */
    public boolean withinMaxLength(int size) {
        return this.maxLength <= 0 || size < this.maxLength;
    }

    public int getMaxLength() {
        return this.maxLength;
    }

    public void setMaxLength(int maxLength2) {
        this.maxLength = maxLength2;
    }

    public void setOnlyFontChars(boolean onlyFontChars2) {
        this.onlyFontChars = onlyFontChars2;
    }

    public VisTextFieldStyle getStyle() {
        return this.style;
    }

    public void setStyle(VisTextFieldStyle style2) {
        if (style2 == null) {
            throw new IllegalArgumentException("style cannot be null.");
        }
        this.style = style2;
        this.textHeight = style2.font.getCapHeight() - (style2.font.getDescent() * 2.0f);
        invalidateHierarchy();
    }

    /* access modifiers changed from: protected */
    public void calculateOffsets() {
        float visibleWidth = getWidth();
        if (this.style.background != null) {
            visibleWidth -= this.style.background.getLeftWidth() + this.style.background.getRightWidth();
        }
        float distance = this.glyphPositions.get(this.cursor) - Math.abs(this.renderOffset);
        if (distance <= Animation.CurveTimeline.LINEAR) {
            if (this.cursor > 0) {
                this.renderOffset = -this.glyphPositions.get(this.cursor - 1);
            } else {
                this.renderOffset = Animation.CurveTimeline.LINEAR;
            }
        } else if (distance > visibleWidth) {
            this.renderOffset -= distance - visibleWidth;
        }
        this.visibleTextStart = 0;
        this.textOffset = Animation.CurveTimeline.LINEAR;
        float start = Math.abs(this.renderOffset);
        int glyphCount = this.glyphPositions.size;
        float[] glyphPositions2 = this.glyphPositions.items;
        float startPos = Animation.CurveTimeline.LINEAR;
        int i = 0;
        while (true) {
            if (i >= glyphCount) {
                break;
            } else if (glyphPositions2[i] >= start) {
                this.visibleTextStart = i;
                startPos = glyphPositions2[i];
                this.textOffset = startPos - start;
                break;
            } else {
                i++;
            }
        }
        this.visibleTextEnd = Math.min(this.displayText.length(), this.cursor + 1);
        while (this.visibleTextEnd <= this.displayText.length() && glyphPositions2[this.visibleTextEnd] - startPos <= visibleWidth) {
            this.visibleTextEnd++;
        }
        this.visibleTextEnd = Math.max(0, this.visibleTextEnd - 1);
        if (this.hasSelection) {
            int minIndex = Math.min(this.cursor, this.selectionStart);
            int maxIndex = Math.max(this.cursor, this.selectionStart);
            float minX = Math.max(glyphPositions2[minIndex], startPos);
            float maxX = Math.min(glyphPositions2[maxIndex], glyphPositions2[this.visibleTextEnd]);
            this.selectionX = minX;
            this.selectionWidth = maxX - minX;
        }
        if (this.textHAlign == 1 || this.textHAlign == 16) {
            this.textOffset = visibleWidth - (glyphPositions2[this.visibleTextEnd] - startPos);
            if (this.textHAlign == 1) {
                this.textOffset = (float) Math.round(this.textOffset * 0.5f);
            }
            if (this.hasSelection) {
                this.selectionX += this.textOffset;
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        Drawable background;
        BitmapFont messageFont;
        Stage stage = getStage();
        boolean focused = stage != null && stage.getKeyboardFocus() == this;
        BitmapFont font = this.style.font;
        Color fontColor = (!this.disabled || this.style.disabledFontColor == null) ? (!focused || this.style.focusedFontColor == null) ? this.style.fontColor : this.style.focusedFontColor : this.style.disabledFontColor;
        Drawable selection = this.style.selection;
        Drawable cursorPatch = this.style.cursor;
        if (!this.disabled || this.style.disabledBackground == null) {
            background = (!focused || this.style.focusedBackground == null) ? this.style.background : this.style.focusedBackground;
        } else {
            background = this.style.disabledBackground;
        }
        if (!this.disabled && this.clickListener.isOver() && this.style.backgroundOver != null) {
            background = this.style.backgroundOver;
        }
        Color color = getColor();
        float x = getX();
        float y = getY();
        float width = getWidth();
        float height = getHeight();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        float bgLeftWidth = Animation.CurveTimeline.LINEAR;
        if (background != null) {
            background.draw(batch, x, y, width, height);
            bgLeftWidth = background.getLeftWidth();
        }
        float textY = getTextY(font, background);
        calculateOffsets();
        if (focused && this.hasSelection && selection != null) {
            drawSelection(selection, batch, font, x + bgLeftWidth, y + textY);
        }
        float yOffset = font.isFlipped() ? -this.textHeight : Animation.CurveTimeline.LINEAR;
        if (this.displayText.length() != 0) {
            font.setColor(fontColor.r, fontColor.g, fontColor.b, fontColor.a * parentAlpha);
            drawText(batch, font, x + bgLeftWidth, y + textY + yOffset);
        } else if (!focused && this.messageText != null) {
            if (this.style.messageFontColor != null) {
                font.setColor(this.style.messageFontColor.r, this.style.messageFontColor.g, this.style.messageFontColor.b, this.style.messageFontColor.a * parentAlpha);
            } else {
                font.setColor(0.7f, 0.7f, 0.7f, parentAlpha);
            }
            if (this.style.messageFont != null) {
                messageFont = this.style.messageFont;
            } else {
                messageFont = font;
            }
            messageFont.draw(batch, this.messageText, x + bgLeftWidth, y + textY + yOffset);
        }
        if (this.drawBorder && focused && !this.disabled) {
            blink();
            if (this.cursorOn && cursorPatch != null) {
                drawCursor(cursorPatch, batch, font, x + bgLeftWidth, y + textY);
            }
        }
        if (!this.inputValid) {
            this.style.errorBorder.draw(batch, getX(), getY(), getWidth(), getHeight());
        } else if (this.drawBorder && this.style.focusBorder != null) {
            this.style.focusBorder.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
    }

    /* access modifiers changed from: protected */
    public float getTextY(BitmapFont font, Drawable background) {
        float height = getHeight();
        float textY = (this.textHeight / 2.0f) + font.getDescent();
        if (background == null) {
            return (float) ((int) ((height / 2.0f) + textY));
        }
        float bottom = background.getBottomHeight();
        return (float) ((int) ((((height - background.getTopHeight()) - bottom) / 2.0f) + textY + bottom));
    }

    /* access modifiers changed from: protected */
    public void drawSelection(Drawable selection, Batch batch, BitmapFont font, float x, float y) {
        selection.draw(batch, this.selectionX + x + this.renderOffset, (y - this.textHeight) - font.getDescent(), this.selectionWidth, this.textHeight + (font.getDescent() / 2.0f));
    }

    /* access modifiers changed from: protected */
    public void drawText(Batch batch, BitmapFont font, float x, float y) {
        font.draw(batch, this.displayText, x + this.textOffset, y, this.visibleTextStart, this.visibleTextEnd, Animation.CurveTimeline.LINEAR, 8, false);
    }

    /* access modifiers changed from: protected */
    public void drawCursor(Drawable cursorPatch, Batch batch, BitmapFont font, float x, float y) {
        cursorPatch.draw(batch, (((this.textOffset + x) + this.glyphPositions.get(this.cursor)) - this.glyphPositions.items[this.visibleTextStart]) - 1.0f, (y - this.textHeight) - font.getDescent(), cursorPatch.getMinWidth(), this.textHeight + (font.getDescent() / 2.0f));
    }

    /* access modifiers changed from: package-private */
    public void updateDisplayText() {
        BitmapFont font = this.style.font;
        BitmapFont.BitmapFontData data = font.getData();
        String text2 = this.text;
        int textLength = text2.length();
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < textLength; i++) {
            char c = text2.charAt(i);
            if (!data.hasGlyph(c)) {
                c = ' ';
            }
            buffer.append(c);
        }
        String newDisplayText = buffer.toString();
        if (!this.passwordMode || !data.hasGlyph(this.passwordCharacter)) {
            this.displayText = newDisplayText;
        } else {
            if (this.passwordBuffer == null) {
                this.passwordBuffer = new StringBuilder(newDisplayText.length());
            }
            if (this.passwordBuffer.length() > textLength) {
                this.passwordBuffer.setLength(textLength);
            } else {
                for (int i2 = this.passwordBuffer.length(); i2 < textLength; i2++) {
                    this.passwordBuffer.append(this.passwordCharacter);
                }
            }
            this.displayText = this.passwordBuffer;
        }
        this.layout.setText(font, this.displayText);
        this.glyphPositions.clear();
        float x = Animation.CurveTimeline.LINEAR;
        if (this.layout.runs.size > 0) {
            FloatArray xAdvances = this.layout.runs.first().xAdvances;
            int n = xAdvances.size;
            for (int i3 = 0; i3 < n; i3++) {
                this.glyphPositions.add(x);
                x += xAdvances.get(i3);
            }
        }
        this.glyphPositions.add(x);
        if (this.selectionStart > newDisplayText.length()) {
            this.selectionStart = textLength;
        }
    }

    private void blink() {
        long time = TimeUtils.nanoTime();
        if (((float) (time - this.lastBlink)) / 1.0E9f > this.blinkTime) {
            this.cursorOn = !this.cursorOn;
            this.lastBlink = time;
        }
    }

    public void copy() {
        if (this.hasSelection && !this.passwordMode) {
            this.clipboard.setContents(this.text.substring(Math.min(this.cursor, this.selectionStart), Math.max(this.cursor, this.selectionStart)));
        }
    }

    public void cut() {
        if (this.hasSelection && !this.passwordMode) {
            copy();
            this.cursor = delete();
        }
    }

    /* access modifiers changed from: package-private */
    public void paste() {
        paste(this.clipboard.getContents());
    }

    /* access modifiers changed from: package-private */
    public void paste(String content) {
        if (content != null) {
            StringBuilder buffer = new StringBuilder();
            int textLength = this.text.length();
            BitmapFont.BitmapFontData data = this.style.font.getData();
            int n = content.length();
            for (int i = 0; i < n && withinMaxLength(buffer.length() + textLength); i++) {
                char c = content.charAt(i);
                if ((this.writeEnters && (c == 10 || c == 13)) || ((!this.onlyFontChars || data.hasGlyph(c)) && (this.filter == null || this.filter.acceptChar(this, c)))) {
                    buffer.append(c);
                }
            }
            String content2 = buffer.toString();
            if (this.hasSelection) {
                this.cursor = delete(false);
            }
            this.text = insert(this.cursor, content2, this.text);
            updateDisplayText();
            this.cursor += content2.length();
        }
    }

    /* access modifiers changed from: package-private */
    public String insert(int position, CharSequence text2, String to) {
        if (to.length() == 0) {
            return text2.toString();
        }
        return to.substring(0, position) + ((Object) text2) + to.substring(position, to.length());
    }

    /* access modifiers changed from: package-private */
    public int delete() {
        return delete(true);
    }

    /* access modifiers changed from: package-private */
    public int delete(boolean updateText) {
        return delete(this.selectionStart, this.cursor, updateText);
    }

    /* access modifiers changed from: package-private */
    public int delete(int from, int to, boolean updateText) {
        int minIndex = Math.min(from, to);
        int maxIndex = Math.max(from, to);
        this.text = (minIndex > 0 ? this.text.substring(0, minIndex) : "") + (maxIndex < this.text.length() ? this.text.substring(maxIndex, this.text.length()) : "");
        if (updateText) {
            updateDisplayText();
        }
        clearSelection();
        return minIndex;
    }

    public void next(boolean up) {
        Stage stage = getStage();
        if (stage != null) {
            getParent().localToStageCoordinates(tmp1.set(getX(), getY()));
            VisTextField textField = findNextTextField(stage.getActors(), null, tmp2, tmp1, up);
            if (textField == null) {
                if (up) {
                    tmp1.set(Float.MIN_VALUE, Float.MIN_VALUE);
                } else {
                    tmp1.set(Float.MAX_VALUE, Float.MAX_VALUE);
                }
                textField = findNextTextField(getStage().getActors(), null, tmp2, tmp1, up);
            }
            if (textField != null) {
                textField.focusField();
                textField.setCursorPosition(textField.getText().length());
                return;
            }
            Gdx.input.setOnscreenKeyboardVisible(false);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: com.badlogic.gdx.scenes.scene2d.Actor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v0, resolved type: com.kotcrab.vis.ui.widget.VisTextField} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v3, resolved type: com.kotcrab.vis.ui.widget.VisTextField} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.kotcrab.vis.ui.widget.VisTextField findNextTextField(com.badlogic.gdx.utils.Array<com.badlogic.gdx.scenes.scene2d.Actor> r15, com.kotcrab.vis.ui.widget.VisTextField r16, com.badlogic.gdx.math.Vector2 r17, com.badlogic.gdx.math.Vector2 r18, boolean r19) {
        /*
            r14 = this;
            com.badlogic.gdx.scenes.scene2d.ui.Window r10 = r14.findModalWindow(r14)
            r9 = 0
            int r11 = r15.size
        L_0x0007:
            if (r9 >= r11) goto L_0x00b9
            java.lang.Object r7 = r15.get(r9)
            com.badlogic.gdx.scenes.scene2d.Actor r7 = (com.badlogic.gdx.scenes.scene2d.Actor) r7
            if (r7 != r14) goto L_0x0014
        L_0x0011:
            int r9 = r9 + 1
            goto L_0x0007
        L_0x0014:
            boolean r1 = r7 instanceof com.kotcrab.vis.ui.widget.VisTextField
            if (r1 == 0) goto L_0x00a0
            r13 = r7
            com.kotcrab.vis.ui.widget.VisTextField r13 = (com.kotcrab.vis.ui.widget.VisTextField) r13
            if (r10 == 0) goto L_0x0023
            com.badlogic.gdx.scenes.scene2d.ui.Window r12 = r14.findModalWindow(r13)
            if (r12 != r10) goto L_0x0011
        L_0x0023:
            boolean r1 = r13.isDisabled()
            if (r1 != 0) goto L_0x0011
            boolean r1 = r13.focusTraversal
            if (r1 == 0) goto L_0x0011
            boolean r1 = r14.isActorVisibleInStage(r13)
            if (r1 == 0) goto L_0x0011
            com.badlogic.gdx.scenes.scene2d.Group r1 = r7.getParent()
            com.badlogic.gdx.math.Vector2 r2 = com.kotcrab.vis.ui.widget.VisTextField.tmp3
            float r3 = r7.getX()
            float r4 = r7.getY()
            com.badlogic.gdx.math.Vector2 r2 = r2.set(r3, r4)
            com.badlogic.gdx.math.Vector2 r8 = r1.localToStageCoordinates(r2)
            float r1 = r8.y
            r0 = r18
            float r2 = r0.y
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 < 0) goto L_0x0067
            float r1 = r8.y
            r0 = r18
            float r2 = r0.y
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x009c
            float r1 = r8.x
            r0 = r18
            float r2 = r0.x
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x009c
        L_0x0067:
            r1 = 1
        L_0x0068:
            r1 = r1 ^ r19
            if (r1 == 0) goto L_0x0011
            if (r16 == 0) goto L_0x0091
            float r1 = r8.y
            r0 = r17
            float r2 = r0.y
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 > 0) goto L_0x008c
            float r1 = r8.y
            r0 = r17
            float r2 = r0.y
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x009e
            float r1 = r8.x
            r0 = r17
            float r2 = r0.x
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x009e
        L_0x008c:
            r1 = 1
        L_0x008d:
            r1 = r1 ^ r19
            if (r1 == 0) goto L_0x0011
        L_0x0091:
            r16 = r7
            com.kotcrab.vis.ui.widget.VisTextField r16 = (com.kotcrab.vis.ui.widget.VisTextField) r16
            r0 = r17
            r0.set(r8)
            goto L_0x0011
        L_0x009c:
            r1 = 0
            goto L_0x0068
        L_0x009e:
            r1 = 0
            goto L_0x008d
        L_0x00a0:
            boolean r1 = r7 instanceof com.badlogic.gdx.scenes.scene2d.Group
            if (r1 == 0) goto L_0x0011
            com.badlogic.gdx.scenes.scene2d.Group r7 = (com.badlogic.gdx.scenes.scene2d.Group) r7
            com.badlogic.gdx.utils.SnapshotArray r2 = r7.getChildren()
            r1 = r14
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r19
            com.kotcrab.vis.ui.widget.VisTextField r16 = r1.findNextTextField(r2, r3, r4, r5, r6)
            goto L_0x0011
        L_0x00b9:
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kotcrab.vis.ui.widget.VisTextField.findNextTextField(com.badlogic.gdx.utils.Array, com.kotcrab.vis.ui.widget.VisTextField, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.math.Vector2, boolean):com.kotcrab.vis.ui.widget.VisTextField");
    }

    private boolean isActorVisibleInStage(Actor actor) {
        if (actor == null) {
            return true;
        }
        if (!actor.isVisible()) {
            return false;
        }
        return isActorVisibleInStage(actor.getParent());
    }

    private Window findModalWindow(Actor actor) {
        if (actor == null) {
            return null;
        }
        if (!(actor instanceof Window) || !((Window) actor).isModal()) {
            return findModalWindow(actor.getParent());
        }
        return (Window) actor;
    }

    public InputListener getDefaultInputListener() {
        return this.inputListener;
    }

    public void setTextFieldListener(TextFieldListener listener2) {
        this.listener = listener2;
    }

    public TextFieldFilter getTextFieldFilter() {
        return this.filter;
    }

    public void setTextFieldFilter(TextFieldFilter filter2) {
        this.filter = filter2;
    }

    public void setFocusTraversal(boolean focusTraversal2) {
        this.focusTraversal = focusTraversal2;
    }

    public String getMessageText() {
        return this.messageText;
    }

    public void setMessageText(String messageText2) {
        this.messageText = messageText2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String str) {
        if (str == null) {
            throw new IllegalArgumentException("text cannot be null.");
        } else if (!str.equals(this.text)) {
            clearSelection();
            this.text = "";
            paste(str);
            this.cursor = 0;
        }
    }

    public int getSelectionStart() {
        return this.selectionStart;
    }

    public String getSelection() {
        return this.hasSelection ? this.text.substring(Math.min(this.selectionStart, this.cursor), Math.max(this.selectionStart, this.cursor)) : "";
    }

    public void setSelection(int selectionStart2, int selectionEnd) {
        if (selectionStart2 < 0) {
            throw new IllegalArgumentException("selectionStart must be >= 0");
        } else if (selectionEnd < 0) {
            throw new IllegalArgumentException("selectionEnd must be >= 0");
        } else {
            int selectionStart3 = Math.min(this.text.length(), selectionStart2);
            int selectionEnd2 = Math.min(this.text.length(), selectionEnd);
            if (selectionEnd2 == selectionStart3) {
                clearSelection();
                return;
            }
            if (selectionEnd2 < selectionStart3) {
                int temp = selectionEnd2;
                selectionEnd2 = selectionStart3;
                selectionStart3 = temp;
            }
            this.hasSelection = true;
            this.selectionStart = selectionStart3;
            this.cursor = selectionEnd2;
        }
    }

    public void selectAll() {
        setSelection(0, this.text.length());
    }

    public void clearSelection() {
        this.hasSelection = false;
    }

    public int getCursorPosition() {
        return this.cursor;
    }

    public void setCursorPosition(int cursorPosition) {
        if (cursorPosition < 0) {
            throw new IllegalArgumentException("cursorPosition must be >= 0");
        }
        clearSelection();
        this.cursor = Math.min(cursorPosition, this.text.length());
    }

    public TextField.OnscreenKeyboard getOnscreenKeyboard() {
        return this.keyboard;
    }

    public void setOnscreenKeyboard(TextField.OnscreenKeyboard keyboard2) {
        this.keyboard = keyboard2;
    }

    public void setClipboard(Clipboard clipboard2) {
        this.clipboard = clipboard2;
    }

    public float getPrefWidth() {
        return 150.0f;
    }

    public float getPrefHeight() {
        float prefHeight = this.textHeight;
        if (this.style.background != null) {
            return Math.max(this.style.background.getBottomHeight() + prefHeight + this.style.background.getTopHeight(), this.style.background.getMinHeight());
        }
        return prefHeight;
    }

    public boolean isPasswordMode() {
        return this.passwordMode;
    }

    public void setPasswordMode(boolean passwordMode2) {
        this.passwordMode = passwordMode2;
        updateDisplayText();
    }

    public void setPasswordCharacter(char passwordCharacter2) {
        this.passwordCharacter = passwordCharacter2;
        if (this.passwordMode) {
            updateDisplayText();
        }
    }

    public void setBlinkTime(float blinkTime2) {
        this.blinkTime = blinkTime2;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean disabled2) {
        this.disabled = disabled2;
        if (disabled2) {
            FocusManager.getFocus();
            this.keyRepeatTask.cancel();
            this.keyTypedRepeatTask.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void moveCursor(boolean forward, boolean jump) {
        int limit;
        int charOffset = 0;
        if (forward) {
            limit = this.text.length();
        } else {
            limit = 0;
        }
        if (!forward) {
            charOffset = -1;
        }
        do {
            if (forward) {
                int i = this.cursor + 1;
                this.cursor = i;
                if (i >= limit) {
                    return;
                }
            } else {
                int i2 = this.cursor - 1;
                this.cursor = i2;
                if (i2 <= limit) {
                    return;
                }
            }
            if (!jump) {
                return;
            }
        } while (continueCursor(this.cursor, charOffset));
    }

    /* access modifiers changed from: protected */
    public boolean continueCursor(int index, int offset) {
        return isWordCharacter(this.text.charAt(index + offset));
    }

    public void focusField() {
        if (!this.disabled) {
            FocusManager.getFocus(this);
            setCursorPosition(0);
            this.selectionStart = this.cursor;
            Stage stage = getStage();
            if (stage != null) {
                stage.setKeyboardFocus(this);
            }
            this.keyboard.show(true);
            this.hasSelection = true;
        }
    }

    public void focusLost() {
        this.drawBorder = false;
    }

    public void focusGained() {
        this.drawBorder = true;
    }

    public boolean isEmpty() {
        return this.text.length() == 0;
    }

    public boolean isInputValid() {
        return this.inputValid;
    }

    public void setInputValid(boolean inputValid2) {
        this.inputValid = inputValid2;
    }

    public static class VisTextFieldStyle extends TextField.TextFieldStyle {
        public Drawable backgroundOver;
        public Drawable errorBorder;
        public Drawable focusBorder;

        public VisTextFieldStyle() {
        }

        public VisTextFieldStyle(BitmapFont font, Color fontColor, Drawable cursor, Drawable selection, Drawable background) {
            super(font, fontColor, cursor, selection, background);
        }

        public VisTextFieldStyle(VisTextFieldStyle style) {
            super(style);
            this.focusBorder = style.focusBorder;
            this.errorBorder = style.errorBorder;
            this.backgroundOver = style.backgroundOver;
        }
    }

    public interface TextFieldFilter {
        boolean acceptChar(VisTextField visTextField, char c);

        public static class DigitsOnlyFilter implements TextFieldFilter {
            public boolean acceptChar(VisTextField textField, char c) {
                return Character.isDigit(c);
            }
        }
    }

    class KeyRepeatTask extends Timer.Task {
        int keycode;

        KeyRepeatTask() {
        }

        public void run() {
            VisTextField.this.inputListener.keyDown(null, this.keycode);
        }
    }

    class KeyTypedRepeatTask extends Timer.Task {
        char character;
        int keycode;

        KeyTypedRepeatTask() {
        }

        public void run() {
            VisTextField.this.inputListener.keyTyped(null, this.character);
        }
    }

    public class TextFieldClickListener extends ClickListener {
        public TextFieldClickListener() {
        }

        public void clicked(InputEvent event, float x, float y) {
            int count = getTapCount() % 4;
            if (count == 0) {
                VisTextField.this.clearSelection();
            }
            if (count == 2) {
                int[] array = VisTextField.this.wordUnderCursor(x);
                VisTextField.this.setSelection(array[0], array[1]);
            }
            if (count == 3) {
                VisTextField.this.selectAll();
            }
        }

        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if (!super.touchDown(event, x, y, pointer, button)) {
                return false;
            }
            if (pointer == 0 && button != 0) {
                return false;
            }
            if (VisTextField.this.disabled) {
                return true;
            }
            FocusManager.getFocus(VisTextField.this);
            setCursorPosition(x, y);
            VisTextField.this.selectionStart = VisTextField.this.cursor;
            Stage stage = VisTextField.this.getStage();
            if (stage != null) {
                stage.setKeyboardFocus(VisTextField.this);
            }
            VisTextField.this.keyboard.show(true);
            VisTextField.this.hasSelection = true;
            return true;
        }

        public void touchDragged(InputEvent event, float x, float y, int pointer) {
            super.touchDragged(event, x, y, pointer);
            setCursorPosition(x, y);
        }

        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            if (VisTextField.this.selectionStart == VisTextField.this.cursor) {
                VisTextField.this.hasSelection = false;
            }
            super.touchUp(event, x, y, pointer, button);
        }

        /* access modifiers changed from: protected */
        public void setCursorPosition(float x, float y) {
            VisTextField.this.lastBlink = 0;
            VisTextField.this.cursorOn = false;
            VisTextField.this.cursor = VisTextField.this.letterUnderCursor(x);
        }

        /* access modifiers changed from: protected */
        public void goHome(boolean jump) {
            VisTextField.this.cursor = 0;
        }

        /* access modifiers changed from: protected */
        public void goEnd(boolean jump) {
            VisTextField.this.cursor = VisTextField.this.text.length();
        }

        public boolean keyDown(InputEvent event, int keycode) {
            boolean jump;
            if (VisTextField.this.disabled) {
                return false;
            }
            VisTextField.this.lastBlink = 0;
            VisTextField.this.cursorOn = false;
            Stage stage = VisTextField.this.getStage();
            if (stage == null || stage.getKeyboardFocus() != VisTextField.this || !VisTextField.this.drawBorder) {
                return false;
            }
            boolean repeat = false;
            boolean ctrl = UIUtils.ctrl();
            if (!ctrl || VisTextField.this.passwordMode) {
                jump = false;
            } else {
                jump = true;
            }
            if (ctrl) {
                if (keycode == 50) {
                    VisTextField.this.paste();
                    return true;
                } else if (keycode == 31 || keycode == 133) {
                    VisTextField.this.copy();
                    return true;
                } else if (keycode == 52 || keycode == 67) {
                    VisTextField.this.cut();
                    return true;
                } else if (keycode == 29) {
                    VisTextField.this.selectAll();
                    return true;
                }
            }
            if (UIUtils.shift()) {
                if (keycode == 133) {
                    VisTextField.this.paste();
                }
                if (keycode == 112 && VisTextField.this.hasSelection) {
                    VisTextField.this.copy();
                    VisTextField.this.delete();
                }
                int temp = VisTextField.this.cursor;
                if (keycode == 21) {
                    VisTextField.this.moveCursor(false, jump);
                    repeat = true;
                } else if (keycode == 22) {
                    VisTextField.this.moveCursor(true, jump);
                    repeat = true;
                } else if (keycode == 3) {
                    goHome(jump);
                } else if (keycode == 132) {
                    goEnd(jump);
                }
                if (!VisTextField.this.hasSelection) {
                    VisTextField.this.selectionStart = temp;
                    VisTextField.this.hasSelection = true;
                }
            } else {
                if (keycode == 21) {
                    VisTextField.this.moveCursor(false, jump);
                    VisTextField.this.clearSelection();
                    repeat = true;
                }
                if (keycode == 22) {
                    VisTextField.this.moveCursor(true, jump);
                    VisTextField.this.clearSelection();
                    repeat = true;
                }
                if (keycode == 3) {
                    goHome(jump);
                    VisTextField.this.clearSelection();
                }
                if (keycode == 132) {
                    goEnd(jump);
                    VisTextField.this.clearSelection();
                }
            }
            VisTextField.this.cursor = MathUtils.clamp(VisTextField.this.cursor, 0, VisTextField.this.text.length());
            if (repeat) {
                scheduleKeyRepeatTask(keycode);
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void scheduleKeyRepeatTask(int keycode) {
            if (!VisTextField.this.keyRepeatTask.isScheduled() || VisTextField.this.keyRepeatTask.keycode != keycode) {
                VisTextField.this.keyRepeatTask.keycode = keycode;
                VisTextField.this.keyRepeatTask.cancel();
                Timer.schedule(VisTextField.this.keyRepeatTask, VisTextField.keyRepeatInitialTime, VisTextField.keyRepeatTime);
            }
        }

        /* access modifiers changed from: protected */
        public void scheduleKeyTypedRepeatTask(int keycode, char character) {
            if (!VisTextField.this.keyTypedRepeatTask.isScheduled() || VisTextField.this.keyTypedRepeatTask.character != character) {
                VisTextField.this.keyTypedRepeatTask.character = character;
                VisTextField.this.keyTypedRepeatTask.keycode = keycode;
                VisTextField.this.keyTypedRepeatTask.cancel();
                Timer.schedule(VisTextField.this.keyTypedRepeatTask, VisTextField.keyRepeatInitialTime, VisTextField.keyRepeatTime);
            }
        }

        public boolean keyUp(InputEvent event, int keycode) {
            if (VisTextField.this.disabled) {
                return false;
            }
            VisTextField.this.keyRepeatTask.cancel();
            if (VisTextField.this.keyTypedRepeatTask.keycode == keycode) {
                VisTextField.this.keyTypedRepeatTask.cancel();
            }
            return true;
        }

        public boolean keyTyped(InputEvent event, char character) {
            Stage stage;
            if (VisTextField.this.disabled || (stage = VisTextField.this.getStage()) == null || stage.getKeyboardFocus() != VisTextField.this || !VisTextField.this.drawBorder) {
                return false;
            }
            if ((character == 9 || character == 10) && VisTextField.this.focusTraversal) {
                VisTextField.this.next(UIUtils.shift());
            } else {
                boolean delete = character == 127;
                boolean backspace = character == 8;
                boolean add = !VisTextField.this.onlyFontChars || VisTextField.this.style.font.getData().hasGlyph(character) || (VisTextField.this.writeEnters && (character == 10 || character == 13));
                boolean remove = backspace || delete;
                if (add || remove) {
                    if (VisTextField.this.hasSelection) {
                        VisTextField.this.cursor = VisTextField.this.delete(false);
                    } else {
                        if (backspace && VisTextField.this.cursor > 0) {
                            VisTextField visTextField = VisTextField.this;
                            StringBuilder append = new StringBuilder().append(VisTextField.this.text.substring(0, VisTextField.this.cursor - 1));
                            String str = VisTextField.this.text;
                            VisTextField visTextField2 = VisTextField.this;
                            int i = visTextField2.cursor;
                            visTextField2.cursor = i - 1;
                            visTextField.text = append.append(str.substring(i)).toString();
                            VisTextField.this.renderOffset = Animation.CurveTimeline.LINEAR;
                            scheduleKeyTypedRepeatTask(event != null ? event.getKeyCode() : VisTextField.this.keyTypedRepeatTask.keycode, character);
                        }
                        if (delete && VisTextField.this.cursor < VisTextField.this.text.length()) {
                            VisTextField.this.text = VisTextField.this.text.substring(0, VisTextField.this.cursor) + VisTextField.this.text.substring(VisTextField.this.cursor + 1);
                        }
                    }
                    if (add && !remove) {
                        boolean isEnter = character == 13 || character == 10;
                        if ((!isEnter && VisTextField.this.filter != null && !VisTextField.this.filter.acceptChar(VisTextField.this, character)) || !VisTextField.this.withinMaxLength(VisTextField.this.text.length())) {
                            return true;
                        }
                        String insertion = isEnter ? "\n" : String.valueOf(character);
                        VisTextField visTextField3 = VisTextField.this;
                        VisTextField visTextField4 = VisTextField.this;
                        VisTextField visTextField5 = VisTextField.this;
                        int i2 = visTextField5.cursor;
                        visTextField5.cursor = i2 + 1;
                        visTextField3.text = visTextField4.insert(i2, insertion, VisTextField.this.text);
                        scheduleKeyTypedRepeatTask(event != null ? event.getKeyCode() : VisTextField.this.keyTypedRepeatTask.keycode, character);
                    }
                    VisTextField.this.updateDisplayText();
                }
            }
            if (VisTextField.this.listener != null) {
                VisTextField.this.listener.keyTyped(VisTextField.this, character);
            }
            return true;
        }
    }
}
