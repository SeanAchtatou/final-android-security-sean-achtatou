package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.VisUI;

public class LinkLabel extends VisLabel {
    private static final String DEFAULT_COLOR_NAME = "link-label";
    /* access modifiers changed from: private */
    public LinkLabelListener listener;
    /* access modifiers changed from: private */
    public CharSequence url;

    public interface LinkLabelListener {
        void clicked(String str);
    }

    public LinkLabel(CharSequence text) {
        super(text);
        setColor(VisUI.getSkin().getColor(DEFAULT_COLOR_NAME));
        init(text);
    }

    public LinkLabel(CharSequence text, CharSequence url2) {
        super(text);
        setColor(VisUI.getSkin().getColor(DEFAULT_COLOR_NAME));
        init(url2);
    }

    public LinkLabel(CharSequence text, int alignment) {
        super(text, alignment);
        setColor(VisUI.getSkin().getColor(DEFAULT_COLOR_NAME));
        init(text);
    }

    public LinkLabel(CharSequence text, Color textColor) {
        super(text, textColor);
        init(text);
    }

    public LinkLabel(CharSequence text, Label.LabelStyle style) {
        super(text, style);
        init(text);
    }

    public LinkLabel(CharSequence text, CharSequence url2, String styleName) {
        super(text, styleName);
        init(url2);
    }

    public LinkLabel(CharSequence text, String fontName, Color color) {
        super(text, fontName, color);
        init(text);
    }

    public LinkLabel(CharSequence text, String fontName, String colorName) {
        super(text, fontName, colorName);
        init(text);
    }

    private void init(CharSequence linkUrl) {
        this.url = linkUrl;
        addListener(new ClickListener(0) {
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (LinkLabel.this.listener == null) {
                    Gdx.f87net.openURI(LinkLabel.this.url.toString());
                } else {
                    LinkLabel.this.listener.clicked(LinkLabel.this.url.toString());
                }
            }
        });
    }

    public CharSequence getUrl() {
        return this.url;
    }

    public void setUrl(CharSequence url2) {
        this.url = url2;
    }

    public LinkLabelListener getListener() {
        return this.listener;
    }

    public void setListener(LinkLabelListener listener2) {
        this.listener = listener2;
    }
}
