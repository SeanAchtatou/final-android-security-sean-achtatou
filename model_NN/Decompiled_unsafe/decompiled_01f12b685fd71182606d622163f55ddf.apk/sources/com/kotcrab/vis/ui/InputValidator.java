package com.kotcrab.vis.ui;

public interface InputValidator {
    boolean validateInput(String str);
}
