package com.kotcrab.vis.ui.util.form;

import com.kotcrab.vis.ui.InputValidator;

public class ValidatorWrapper extends FormInputValidator {
    private InputValidator validator;

    public ValidatorWrapper(String errorMsg, InputValidator validator2) {
        super(errorMsg);
        this.validator = validator2;
    }

    /* access modifiers changed from: protected */
    public boolean validate(String input) {
        return this.validator.validateInput(input);
    }
}
