package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Timer;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.VisUI;
import java.util.Iterator;

public class Tooltip extends VisTable {
    private static final Drawable BACKGROUND = VisUI.getSkin().getDrawable("tooltip-bg");
    public static final float DEFAULT_APPEAR_DELAY_TIME = 0.6f;
    public static final float DEFAULT_FADE_TIME = 0.3f;
    /* access modifiers changed from: private */
    public float appearDelayTime = 0.6f;
    private Actor content;
    private Cell<Actor> contentCell;
    /* access modifiers changed from: private */
    public DisplayTask displayTask;
    /* access modifiers changed from: private */
    public float fadeTime = 0.3f;
    private TooltipInputListener listener;
    /* access modifiers changed from: private */
    public Actor target;

    public Tooltip(Actor target2, String text) {
        super(true);
        VisLabel label = new VisLabel(text);
        label.setAlignment(1);
        init(target2, label);
    }

    public Tooltip(Actor target2, Actor content2) {
        super(true);
        init(target2, content2);
    }

    public Tooltip(String text) {
        super(true);
        VisLabel label = new VisLabel(text);
        label.setAlignment(1);
        init(null, label);
    }

    public Tooltip(Actor content2) {
        super(true);
        init(null, content2);
    }

    public static void removeTooltip(Actor target2) {
        Iterator<EventListener> it = target2.getListeners().iterator();
        while (it.hasNext()) {
            EventListener listener2 = it.next();
            if (listener2 instanceof TooltipInputListener) {
                target2.removeListener(listener2);
            }
        }
    }

    private void init(Actor target2, Actor content2) {
        this.target = target2;
        this.content = content2;
        this.listener = new TooltipInputListener();
        this.displayTask = new DisplayTask();
        setBackground(BACKGROUND);
        this.contentCell = add(content2).padLeft(3.0f).padRight(3.0f).padBottom(2.0f);
        pack();
        if (target2 != null) {
            attach();
        }
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Tooltip.this.toFront();
                return true;
            }

            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (pointer == -1) {
                    Tooltip.this.clearActions();
                    Tooltip.this.addAction(Actions.sequence(Actions.fadeIn(Tooltip.this.fadeTime, Interpolation.fade)));
                }
            }

            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (pointer == -1) {
                    Tooltip.this.fadeOut();
                }
            }
        });
    }

    public void attach() {
        if (this.target != null) {
            Iterator<EventListener> it = this.target.getListeners().iterator();
            while (it.hasNext()) {
                if (it.next() instanceof TooltipInputListener) {
                    throw new IllegalStateException("More than one tooltip cannot be added to the same target!");
                }
            }
            this.target.addListener(this.listener);
        }
    }

    public void detach() {
        if (this.target != null) {
            this.target.removeListener(this.listener);
        }
    }

    public void setTarget(Actor newTarget) {
        detach();
        this.target = newTarget;
        attach();
    }

    /* access modifiers changed from: private */
    public void fadeOut() {
        clearActions();
        addAction(Actions.sequence(Actions.fadeOut(this.fadeTime, Interpolation.fade), Actions.removeActor()));
    }

    /* access modifiers changed from: private */
    public VisTable fadeIn() {
        clearActions();
        setColor(1.0f, 1.0f, 1.0f, Animation.CurveTimeline.LINEAR);
        addAction(Actions.sequence(Actions.fadeIn(this.fadeTime, Interpolation.fade)));
        return this;
    }

    public Actor getContent() {
        return this.content;
    }

    public void setContent(Actor content2) {
        this.content = content2;
        this.contentCell.setActor(content2);
    }

    public void setPosition(float x, float y) {
        super.setPosition((float) ((int) x), (float) ((int) y));
    }

    public float getAppearDelayTime() {
        return this.appearDelayTime;
    }

    public void setAppearDelayTime(float appearDelayTime2) {
        this.appearDelayTime = appearDelayTime2;
    }

    public float getFadeTime() {
        return this.fadeTime;
    }

    public void setFadeTime(float fadeTime2) {
        this.fadeTime = fadeTime2;
    }

    /* access modifiers changed from: private */
    public void keepWithinStage() {
        Stage stage = getStage();
        Camera camera = stage.getCamera();
        if (camera instanceof OrthographicCamera) {
            OrthographicCamera orthographicCamera = (OrthographicCamera) camera;
            float parentWidth = stage.getWidth();
            float parentHeight = stage.getHeight();
            if (getX(16) - camera.position.x > (parentWidth / 2.0f) / orthographicCamera.zoom) {
                setPosition(camera.position.x + ((parentWidth / 2.0f) / orthographicCamera.zoom), getY(16), 16);
            }
            if (getX(8) - camera.position.x < ((-parentWidth) / 2.0f) / orthographicCamera.zoom) {
                setPosition(camera.position.x - ((parentWidth / 2.0f) / orthographicCamera.zoom), getY(8), 8);
            }
            if (getY(2) - camera.position.y > (parentHeight / 2.0f) / orthographicCamera.zoom) {
                setPosition(getX(2), camera.position.y + ((parentHeight / 2.0f) / orthographicCamera.zoom), 2);
            }
            if (getY(4) - camera.position.y < ((-parentHeight) / 2.0f) / orthographicCamera.zoom) {
                setPosition(getX(4), camera.position.y - ((parentHeight / 2.0f) / orthographicCamera.zoom), 4);
            }
        } else if (getParent() == stage.getRoot()) {
            float parentWidth2 = stage.getWidth();
            float parentHeight2 = stage.getHeight();
            if (getX() < Animation.CurveTimeline.LINEAR) {
                setX(Animation.CurveTimeline.LINEAR);
            }
            if (getRight() > parentWidth2) {
                setX(parentWidth2 - getWidth());
            }
            if (getY() < Animation.CurveTimeline.LINEAR) {
                setY(Animation.CurveTimeline.LINEAR);
            }
            if (getTop() > parentHeight2) {
                setY(parentHeight2 - getHeight());
            }
        }
    }

    private class DisplayTask extends Timer.Task {
        private DisplayTask() {
        }

        public void run() {
            Tooltip.this.target.getStage().addActor(Tooltip.this.fadeIn());
            Tooltip.this.keepWithinStage();
        }
    }

    private class TooltipInputListener extends InputListener {
        private TooltipInputListener() {
        }

        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            Tooltip.this.toFront();
            Tooltip.this.fadeOut();
            return true;
        }

        public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
            if (pointer == -1) {
                Vector2 targetPos = Tooltip.this.target.localToStageCoordinates(new Vector2());
                Tooltip.this.setX(targetPos.x + ((Tooltip.this.target.getWidth() - Tooltip.this.getWidth()) / 2.0f));
                float tooltipY = (targetPos.y - Tooltip.this.getHeight()) - 6.0f;
                float stageHeight = Tooltip.this.target.getStage().getHeight();
                if (stageHeight - tooltipY > stageHeight) {
                    Tooltip.this.setY(targetPos.y + Tooltip.this.target.getHeight() + 6.0f);
                } else {
                    Tooltip.this.setY(tooltipY);
                }
                Tooltip.this.displayTask.cancel();
                Timer.schedule(Tooltip.this.displayTask, Tooltip.this.appearDelayTime);
            }
        }

        public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
            if (pointer == -1) {
                Tooltip.this.displayTask.cancel();
                Tooltip.this.fadeOut();
            }
        }

        public boolean mouseMoved(InputEvent event, float x, float y) {
            if (!Tooltip.this.isVisible() || Tooltip.this.getActions().size != 0) {
                return false;
            }
            Tooltip.this.fadeOut();
            return false;
        }
    }
}
