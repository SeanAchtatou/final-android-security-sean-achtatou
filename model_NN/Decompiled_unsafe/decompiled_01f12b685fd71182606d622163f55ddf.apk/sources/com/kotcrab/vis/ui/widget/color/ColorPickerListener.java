package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Color;

public interface ColorPickerListener {
    void canceled();

    void finished(Color color);
}
