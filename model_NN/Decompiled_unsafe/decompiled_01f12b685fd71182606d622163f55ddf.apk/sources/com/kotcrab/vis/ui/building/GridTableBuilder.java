package com.kotcrab.vis.ui.building;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.building.utilities.CellWidget;
import com.kotcrab.vis.ui.building.utilities.Padding;
import java.util.Iterator;

public class GridTableBuilder extends TableBuilder {
    private final int rowSize;

    public GridTableBuilder(int rowSize2) {
        this.rowSize = rowSize2;
    }

    public GridTableBuilder(Padding defaultWidgetPadding, int rowSize2) {
        super(defaultWidgetPadding);
        this.rowSize = rowSize2;
    }

    public GridTableBuilder(int rowSize2, int estimatedWidgetsAmount, int estimatedRowsAmount) {
        super(estimatedWidgetsAmount, estimatedRowsAmount);
        this.rowSize = rowSize2;
    }

    public GridTableBuilder(int rowSize2, int estimatedWidgetsAmount, int estimatedRowsAmount, Padding defaultWidgetPadding) {
        super(estimatedWidgetsAmount, estimatedRowsAmount, defaultWidgetPadding);
        this.rowSize = rowSize2;
    }

    /* access modifiers changed from: protected */
    public void fillTable(Table table) {
        int widgetsCounter = 0;
        Iterator<CellWidget<? extends Actor>> it = getWidgets().iterator();
        while (it.hasNext()) {
            it.next().buildCell(table, getDefaultWidgetPadding());
            widgetsCounter++;
            if (widgetsCounter == this.rowSize) {
                widgetsCounter -= this.rowSize;
                table.row();
            }
        }
    }
}
