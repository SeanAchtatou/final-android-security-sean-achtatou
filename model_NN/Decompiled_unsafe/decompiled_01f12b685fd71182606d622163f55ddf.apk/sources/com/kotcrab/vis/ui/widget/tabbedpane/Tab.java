package com.kotcrab.vis.ui.widget.tabbedpane;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;

public abstract class Tab implements Disposable {
    private boolean activeTab;
    private boolean closeableByUser = true;
    private boolean dirty = false;
    private TabbedPane pane;
    private boolean savable = false;

    public abstract Table getContentTable();

    public abstract String getTabTitle();

    public Tab() {
    }

    public Tab(boolean savable2) {
        this.savable = savable2;
    }

    public Tab(boolean savable2, boolean closeableByUser2) {
        this.savable = savable2;
        this.closeableByUser = closeableByUser2;
    }

    public void onShow() {
        this.activeTab = true;
    }

    public void onHide() {
        this.activeTab = false;
    }

    public boolean isActiveTab() {
        return this.activeTab;
    }

    public TabbedPane getPane() {
        return this.pane;
    }

    public void setPane(TabbedPane pane2) {
        this.pane = pane2;
    }

    public boolean isSavable() {
        return this.savable;
    }

    public boolean isCloseableByUser() {
        return this.closeableByUser;
    }

    public boolean isDirty() {
        return this.dirty;
    }

    public void setDirty(boolean dirty2) {
        checkSavable();
        if (dirty2 != this.dirty) {
            this.dirty = dirty2;
            if (this.pane != null) {
                getPane().updateTabTitle(this);
            }
        }
    }

    public void dirty() {
        setDirty(true);
    }

    public boolean save() {
        checkSavable();
        return false;
    }

    private void checkSavable() {
        if (!this.savable) {
            throw new IllegalStateException("Tab " + getTabTitle() + " is not savable!");
        }
    }

    public void removeFromTabPane() {
        if (this.pane != null) {
            this.pane.remove(this);
        }
    }

    public void dispose() {
    }
}
