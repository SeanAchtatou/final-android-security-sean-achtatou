package com.kotcrab.vis.ui.widget.file;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;
import com.kotcrab.vis.ui.widget.MenuItem;
import com.kotcrab.vis.ui.widget.PopupMenu;
import com.kotcrab.vis.ui.widget.VisDialog;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

public class FilePopupMenu extends PopupMenu {
    private MenuItem addToFavorites = new MenuItem(getText(FileChooserText.CONTEXT_MENU_ADD_TO_FAVORITES));
    private I18NBundle bundle;
    /* access modifiers changed from: private */
    public FileChooser chooser;
    private MenuItem delete = new MenuItem(getText(FileChooserText.CONTEXT_MENU_DELETE));
    /* access modifiers changed from: private */
    public FileHandle file;
    private MenuItem removeFromFavorites = new MenuItem(getText(FileChooserText.CONTEXT_MENU_REMOVE_FROM_FAVORITES));
    private MenuItem showInExplorer = new MenuItem(getText(FileChooserText.CONTEXT_MENU_SHOW_IN_EXPLORER));

    public FilePopupMenu(FileChooser fileChooser, I18NBundle bundle2) {
        this.chooser = fileChooser;
        this.bundle = bundle2;
        this.delete.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                FilePopupMenu.this.showDeleteDialog();
            }
        });
        this.showInExplorer.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                try {
                    if (FilePopupMenu.this.file.isDirectory()) {
                        Desktop.getDesktop().open(FilePopupMenu.this.file.file());
                    } else {
                        Desktop.getDesktop().open(FilePopupMenu.this.file.parent().file());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.addToFavorites.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                FilePopupMenu.this.chooser.addFavorite(FilePopupMenu.this.file);
            }
        });
        this.removeFromFavorites.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                FilePopupMenu.this.chooser.removeFavorite(FilePopupMenu.this.file);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kotcrab.vis.ui.widget.VisDialog.button(java.lang.String, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog
     arg types: [java.lang.String, boolean]
     candidates:
      com.kotcrab.vis.ui.widget.VisDialog.button(com.badlogic.gdx.scenes.scene2d.ui.Button, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog
      com.kotcrab.vis.ui.widget.VisDialog.button(java.lang.String, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog */
    /* access modifiers changed from: private */
    public void showDeleteDialog() {
        VisDialog dialog = new VisDialog(getText(FileChooserText.POPUP_TITLE)) {
            /* access modifiers changed from: protected */
            public void result(Object object) {
                if (Boolean.parseBoolean(object.toString())) {
                    FilePopupMenu.this.file.delete();
                    FilePopupMenu.this.chooser.refresh();
                }
            }
        };
        dialog.text(getText(FileChooserText.CONTEXT_MENU_DELETE_WARNING));
        dialog.button(getText(FileChooserText.POPUP_NO), (Object) false);
        dialog.button(getText(FileChooserText.POPUP_YES), (Object) true);
        dialog.pack();
        dialog.centerWindow();
        this.chooser.getStage().addActor(dialog.fadeIn());
    }

    public void build(Array<FileHandle> favorites, FileHandle file2) {
        this.file = file2;
        clear();
        if (file2.type() == Files.FileType.Absolute || file2.type() == Files.FileType.External) {
            addItem(this.delete);
        }
        if (file2.type() == Files.FileType.Absolute) {
            addItem(this.showInExplorer);
            if (!file2.isDirectory()) {
                return;
            }
            if (favorites.contains(file2, false)) {
                addItem(this.removeFromFavorites);
            } else {
                addItem(this.addToFavorites);
            }
        }
    }

    public void buildForFavorite(Array<FileHandle> favorites, File file2) {
        this.file = Gdx.files.absolute(file2.getAbsolutePath());
        clear();
        addItem(this.showInExplorer);
        if (favorites.contains(this.file, false)) {
            addItem(this.removeFromFavorites);
        }
    }

    private String getText(FileChooserText text) {
        return this.bundle.get(text.getName());
    }
}
