package com.kotcrab.vis.ui.util.dialog;

public interface ConfirmDialogListener<T> {
    void result(T t);
}
