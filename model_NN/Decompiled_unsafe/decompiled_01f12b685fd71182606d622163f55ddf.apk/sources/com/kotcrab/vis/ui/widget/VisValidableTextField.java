package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.InputValidator;
import java.util.Iterator;

public class VisValidableTextField extends VisTextField {
    /* access modifiers changed from: private */
    public boolean changeListenerAdded;
    /* access modifiers changed from: private */
    public String lastValid;
    private boolean programmaticChangeEvents = true;
    private LastValidFocusListener restoreFocusListener;
    /* access modifiers changed from: private */
    public boolean restoreLastValid = false;
    private boolean validationEnabled = true;
    private Array<InputValidator> validators = new Array<>();

    public VisValidableTextField() {
        init();
    }

    public VisValidableTextField(String text) {
        super(text);
        init();
    }

    public VisValidableTextField(InputValidator validator) {
        addValidator(validator);
        init();
    }

    public VisValidableTextField(InputValidator... validators2) {
        for (InputValidator validator : validators2) {
            addValidator(validator);
        }
        init();
    }

    public VisValidableTextField(boolean restoreLastValid2, InputValidator validator) {
        addValidator(validator);
        init();
        setRestoreLastValid(restoreLastValid2);
    }

    public VisValidableTextField(boolean restoreLastValid2, InputValidator... validators2) {
        for (InputValidator validator : validators2) {
            addValidator(validator);
        }
        init();
        setRestoreLastValid(restoreLastValid2);
    }

    private void init() {
        addListener(new InputListener() {
            public boolean keyTyped(InputEvent event, char character) {
                VisValidableTextField.this.validateInput();
                if (!VisValidableTextField.this.changeListenerAdded) {
                    return false;
                }
                VisValidableTextField.this.fire(new ChangeListener.ChangeEvent());
                return false;
            }
        });
    }

    public void setText(String str) {
        super.setText(str);
        validateInput();
        if (this.programmaticChangeEvents) {
            fire(new ChangeListener.ChangeEvent());
        }
    }

    public void validateInput() {
        if (this.validationEnabled) {
            Iterator<InputValidator> it = this.validators.iterator();
            while (it.hasNext()) {
                if (!it.next().validateInput(getText())) {
                    setInputValid(false);
                    return;
                }
            }
        }
        setInputValid(true);
    }

    public boolean addListener(EventListener listener) {
        if (listener instanceof ChangeListener) {
            this.changeListenerAdded = true;
        }
        return super.addListener(listener);
    }

    public boolean removeListener(EventListener listener) {
        boolean result = super.removeListener(listener);
        Iterator<EventListener> it = getListeners().iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next() instanceof ChangeListener) {
                    break;
                }
            } else {
                this.changeListenerAdded = false;
                break;
            }
        }
        return result;
    }

    public void addValidator(InputValidator validator) {
        this.validators.add(validator);
        validateInput();
    }

    public Array<InputValidator> getValidators() {
        return this.validators;
    }

    public InputValidator getValidator() {
        if (this.validators.size == 0) {
            return null;
        }
        return this.validators.get(0);
    }

    public boolean isValidationEnabled() {
        return this.validationEnabled;
    }

    public void setValidationEnabled(boolean validationEnabled2) {
        this.validationEnabled = validationEnabled2;
        validateInput();
    }

    public void setProgrammaticChangeEvents(boolean programmaticChangeEvents2) {
        this.programmaticChangeEvents = programmaticChangeEvents2;
    }

    public boolean isRestoreLastValid() {
        return this.restoreLastValid;
    }

    public void setRestoreLastValid(boolean restoreLastValid2) {
        if (this.hasSelection) {
            throw new IllegalStateException("Last valid text restore can't be changed while filed has selection");
        }
        this.restoreLastValid = restoreLastValid2;
        if (restoreLastValid2) {
            if (this.restoreFocusListener == null) {
                this.restoreFocusListener = new LastValidFocusListener();
            }
            addListener(this.restoreFocusListener);
            return;
        }
        removeListener(this.restoreFocusListener);
    }

    private class LastValidFocusListener extends FocusListener {
        private LastValidFocusListener() {
        }

        public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
            if (focused && VisValidableTextField.this.restoreLastValid) {
                String unused = VisValidableTextField.this.lastValid = VisValidableTextField.this.getText();
            }
            if (!focused && !VisValidableTextField.this.isInputValid() && VisValidableTextField.this.restoreLastValid) {
                VisValidableTextField.super.setText(VisValidableTextField.this.lastValid);
                VisValidableTextField.this.setInputValid(true);
            }
        }
    }
}
