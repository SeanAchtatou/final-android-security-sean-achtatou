package com.kotcrab.vis.ui.util;

import com.kotcrab.vis.ui.InputValidator;

public class Validators {
    public static final FloatValidator FLOATS = new FloatValidator();
    public static final IntegerValidator INTEGERS = new IntegerValidator();

    public static class IntegerValidator implements InputValidator {
        public boolean validateInput(String input) {
            try {
                Integer.parseInt(input);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    }

    public static class FloatValidator implements InputValidator {
        public boolean validateInput(String input) {
            try {
                Float.parseFloat(input);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    }

    public static class LesserThanValidator implements InputValidator {
        private boolean equals;
        private float lesserThan;

        public LesserThanValidator(float lesserThan2) {
            this.lesserThan = lesserThan2;
        }

        public LesserThanValidator(float lesserThan2, boolean inputCanBeEqual) {
            this.lesserThan = lesserThan2;
            this.equals = inputCanBeEqual;
        }

        public boolean validateInput(String input) {
            try {
                float value = Float.valueOf(input).floatValue();
                if (this.equals) {
                    if (value <= this.lesserThan) {
                        return true;
                    }
                    return false;
                } else if (value >= this.lesserThan) {
                    return false;
                } else {
                    return true;
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }
    }

    public static class GreaterThanValidator implements InputValidator {
        private boolean equals;
        private float greaterThan;

        public GreaterThanValidator(float greaterThan2) {
            this.greaterThan = greaterThan2;
        }

        public GreaterThanValidator(float greaterThan2, boolean inputCanBeEqual) {
            this.greaterThan = greaterThan2;
            this.equals = inputCanBeEqual;
        }

        public boolean validateInput(String input) {
            try {
                float value = Float.valueOf(input).floatValue();
                if (this.equals) {
                    if (value >= this.greaterThan) {
                        return true;
                    }
                    return false;
                } else if (value <= this.greaterThan) {
                    return false;
                } else {
                    return true;
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }
    }
}
