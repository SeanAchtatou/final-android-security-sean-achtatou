package com.kotcrab.vis.ui;

public interface Focusable {
    void focusGained();

    void focusLost();
}
