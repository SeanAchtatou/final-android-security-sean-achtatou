package com.kotcrab.vis.ui.building.utilities.layouts;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.building.GridTableBuilder;
import com.kotcrab.vis.ui.building.utilities.CellWidget;

public class GridTableLayout implements ActorLayout {
    private final int rowSize;

    public GridTableLayout(int rowSize2) {
        this.rowSize = rowSize2;
    }

    public static GridTableLayout withRowSize(int rowSize2) {
        return new GridTableLayout(rowSize2);
    }

    public Actor convertToActor(Actor... widgets) {
        return convertToActor(CellWidget.wrap(widgets));
    }

    public Actor convertToActor(CellWidget<?>... widgets) {
        return TableLayout.convertToTable(new GridTableBuilder(this.rowSize), widgets);
    }
}
