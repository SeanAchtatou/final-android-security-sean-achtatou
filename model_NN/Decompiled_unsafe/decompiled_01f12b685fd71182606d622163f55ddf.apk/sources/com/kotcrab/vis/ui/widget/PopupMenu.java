package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.VisUI;

public class PopupMenu extends Table {
    private InputListener defaultInputListener;
    private ChangeListener sharedMenuItemListener;
    private InputListener stageListener;
    private PopupMenuStyle style;
    /* access modifiers changed from: private */
    public PopupMenu subMenu;

    public PopupMenu() {
        this("default");
    }

    public PopupMenu(String styleName) {
        super(VisUI.getSkin());
        this.style = (PopupMenuStyle) VisUI.getSkin().get(styleName, PopupMenuStyle.class);
        createListeners();
    }

    private void createListeners() {
        this.stageListener = new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!PopupMenu.this.menuStructureContains(x, y)) {
                    PopupMenu.this.remove();
                }
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (PopupMenu.this.subMenu != null) {
                    removeIfNeeded(x, y);
                }
            }

            private boolean removeIfNeeded(float x, float y) {
                if (PopupMenu.this.contains(x, y)) {
                    return false;
                }
                PopupMenu.this.remove();
                return true;
            }
        };
        this.sharedMenuItemListener = new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                if (!event.isStopped()) {
                    PopupMenu.this.remove();
                }
            }
        };
    }

    public void addItem(MenuItem item) {
        add(item).fillX().row();
        pack();
        item.addListener(this.sharedMenuItemListener);
    }

    public void addSeparator() {
        add(new Separator("menu")).padTop(2.0f).padBottom(2.0f).fill().expand().row();
    }

    public InputListener getDefaultInputListener() {
        if (this.defaultInputListener == null) {
            this.defaultInputListener = new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if (event.getButton() == 1) {
                        PopupMenu.this.showMenu(event.getStage(), event.getStageX(), event.getStageY());
                    }
                }
            };
        }
        return this.defaultInputListener;
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.style.background != null) {
            this.style.background.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
        super.draw(batch, parentAlpha);
        if (this.style.border != null) {
            this.style.border.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
    }

    public void showMenu(Stage stage, float x, float y) {
        setPosition(x, y - getHeight());
        if (stage.getHeight() - getY() > stage.getHeight()) {
            setY(getY() + getHeight());
        }
        stage.addActor(this);
    }

    public boolean contains(float x, float y) {
        return getX() <= x && getX() + getWidth() >= x && getY() <= y && getY() + getHeight() >= y;
    }

    public boolean menuStructureContains(float x, float y) {
        if (contains(x, y)) {
            return true;
        }
        if (this.subMenu != null) {
            return this.subMenu.menuStructureContains(x, y);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setSubMenu(PopupMenu subMenu2) {
        if (this.subMenu != subMenu2) {
            if (this.subMenu != null) {
                this.subMenu.remove();
            }
            this.subMenu = subMenu2;
        }
    }

    /* access modifiers changed from: protected */
    public void setStage(Stage stage) {
        super.setStage(stage);
        if (stage != null) {
            stage.addListener(this.stageListener);
        }
    }

    public boolean remove() {
        if (getStage() != null) {
            getStage().removeListener(this.stageListener);
        }
        if (this.subMenu != null) {
            this.subMenu.remove();
        }
        return super.remove();
    }

    public static class PopupMenuStyle {
        public Drawable background;
        public Drawable border;

        public PopupMenuStyle() {
        }

        public PopupMenuStyle(Drawable background2, Drawable border2) {
            this.background = background2;
            this.border = border2;
        }

        public PopupMenuStyle(PopupMenuStyle style) {
            this.background = style.background;
            this.border = style.border;
        }
    }
}
