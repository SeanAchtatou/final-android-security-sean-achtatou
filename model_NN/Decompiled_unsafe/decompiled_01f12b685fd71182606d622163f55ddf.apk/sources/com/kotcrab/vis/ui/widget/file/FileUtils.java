package com.kotcrab.vis.ui.widget.file;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;

public class FileUtils {
    private static final Comparator<FileHandle> FILE_COMPARATOR = new Comparator<FileHandle>() {
        public int compare(FileHandle f1, FileHandle f2) {
            return f1.name().toLowerCase().compareTo(f2.name().toLowerCase());
        }
    };
    public static final String OS = System.getProperty("os.name").toLowerCase();
    private static final String[] UNITS = {"B", "KB", "MB", "GB", "TB", "EB"};

    public static String readableFileSize(long size) {
        if (size <= 0) {
            return "0 B";
        }
        int digitGroups = (int) (Math.log10((double) size) / Math.log10(1024.0d));
        return new DecimalFormat("#,##0.#").format(((double) size) / Math.pow(1024.0d, (double) digitGroups)).replace(",", ".") + " " + UNITS[digitGroups];
    }

    public static Array<FileHandle> sortFiles(FileHandle[] files) {
        Array<FileHandle> directoriesList = new Array<>();
        Array<FileHandle> filesList = new Array<>();
        Arrays.sort(files, FILE_COMPARATOR);
        for (FileHandle f : files) {
            if (f.isDirectory()) {
                directoriesList.add(f);
            } else {
                filesList.add(f);
            }
        }
        directoriesList.addAll(filesList);
        return directoriesList;
    }

    public static boolean isValidFileName(String name) {
        try {
            if (!isWindows() || (!name.contains(">") && !name.contains("<"))) {
                return new File(name).getCanonicalFile().getName().equals(name);
            }
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean isWindows() {
        return OS.contains("win");
    }

    public static boolean isMac() {
        return OS.contains("mac");
    }

    public static boolean isUnix() {
        return OS.contains("nix") || OS.contains("nux") || OS.contains("aix");
    }

    public static FileHandle toFileHandle(File file) {
        return Gdx.files.absolute(file.getAbsolutePath());
    }
}
