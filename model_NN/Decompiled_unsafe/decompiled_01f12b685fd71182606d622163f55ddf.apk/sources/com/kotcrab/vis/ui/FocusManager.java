package com.kotcrab.vis.ui;

public class FocusManager {
    private static Focusable focusedWidget;

    public static void getFocus(Focusable widget) {
        if (focusedWidget != null) {
            focusedWidget.focusLost();
        }
        focusedWidget = widget;
        focusedWidget.focusGained();
    }

    public static void getFocus() {
        if (focusedWidget != null) {
            focusedWidget.focusLost();
        }
        focusedWidget = null;
    }
}
