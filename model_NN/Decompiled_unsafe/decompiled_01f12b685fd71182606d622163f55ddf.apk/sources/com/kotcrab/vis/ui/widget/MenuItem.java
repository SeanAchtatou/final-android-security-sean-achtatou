package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.VisUI;

public class MenuItem extends Button {
    private boolean generateDisabledImage;
    private Image image;
    private Label label;
    private Cell<VisLabel> shortcutLabelCell;
    private MenuItemStyle style;
    /* access modifiers changed from: private */
    public PopupMenu subMenu;
    private Cell<Image> subMenuIconCell;
    private Image subMenuImage;
    private InputListener subMenuListener;

    public MenuItem(String text) {
        this(text, (Image) null, (MenuItemStyle) VisUI.getSkin().get(MenuItemStyle.class));
    }

    public MenuItem(String text, ChangeListener changeListener) {
        this(text, (Image) null, (MenuItemStyle) VisUI.getSkin().get(MenuItemStyle.class));
        addListener(changeListener);
    }

    public MenuItem(String text, Drawable drawable) {
        this(text, drawable, (MenuItemStyle) VisUI.getSkin().get(MenuItemStyle.class));
    }

    public MenuItem(String text, Drawable drawable, ChangeListener changeListener) {
        this(text, drawable, (MenuItemStyle) VisUI.getSkin().get(MenuItemStyle.class));
        addListener(changeListener);
    }

    public MenuItem(String text, Image image2) {
        this(text, image2, (MenuItemStyle) VisUI.getSkin().get(MenuItemStyle.class));
    }

    public MenuItem(String text, Image image2, ChangeListener changeListener) {
        this(text, image2, (MenuItemStyle) VisUI.getSkin().get(MenuItemStyle.class));
        addListener(changeListener);
    }

    public MenuItem(String text, Image image2, MenuItemStyle style2) {
        super(style2);
        this.generateDisabledImage = true;
        init(text, image2, style2);
    }

    public MenuItem(String text, Drawable drawable, MenuItemStyle style2) {
        super(style2);
        this.generateDisabledImage = true;
        init(text, new Image(drawable), style2);
    }

    private void init(String text, Image image2, MenuItemStyle style2) {
        this.style = style2;
        this.image = image2;
        setSkin(VisUI.getSkin());
        defaults().space(3.0f);
        if (image2 != null) {
            image2.setScaling(Scaling.fit);
        }
        add(image2).size(22.0f);
        this.label = new Label(text, new Label.LabelStyle(style2.font, style2.fontColor));
        this.label.setAlignment(8);
        add(this.label).expand().fill();
        this.shortcutLabelCell = add(new VisLabel("", "menuitem-shortcut")).padLeft(10.0f).right();
        Image image3 = new Image(style2.subMenu);
        this.subMenuImage = image3;
        this.subMenuIconCell = add(image3).padLeft(3.0f).padRight(3.0f).size(style2.subMenu.getMinWidth(), style2.subMenu.getMinHeight());
        this.subMenuIconCell.setActor(null);
        addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                if (MenuItem.this.subMenu != null) {
                    event.stop();
                }
            }
        });
        addListener(new InputListener() {
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (MenuItem.this.subMenu == null || MenuItem.this.isDisabled()) {
                    ((PopupMenu) MenuItem.this.getParent()).setSubMenu(null);
                    return;
                }
                Stage stage = MenuItem.this.getStage();
                Vector2 pos = MenuItem.this.localToStageCoordinates(new Vector2(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
                MenuItem.this.subMenu.setPosition((pos.x + MenuItem.this.getWidth()) - 1.0f, (pos.y - MenuItem.this.subMenu.getHeight()) + MenuItem.this.getHeight());
                if (MenuItem.this.subMenu.getY() < Animation.CurveTimeline.LINEAR) {
                    MenuItem.this.subMenu.setY((MenuItem.this.subMenu.getY() + MenuItem.this.subMenu.getHeight()) - MenuItem.this.getHeight());
                }
                stage.addActor(MenuItem.this.subMenu);
                ((PopupMenu) MenuItem.this.getParent()).setSubMenu(MenuItem.this.subMenu);
            }
        });
    }

    public void setSubMenu(PopupMenu subMenu2) {
        this.subMenu = subMenu2;
        if (subMenu2 == null) {
            this.subMenuIconCell.setActor(null);
        } else {
            this.subMenuIconCell.setActor(this.subMenuImage);
        }
    }

    public MenuItemStyle getStyle() {
        return this.style;
    }

    public void setStyle(Button.ButtonStyle style2) {
        if (!(style2 instanceof MenuItemStyle)) {
            throw new IllegalArgumentException("style must be a MenuItemStyle.");
        }
        super.setStyle(style2);
        this.style = (MenuItemStyle) style2;
        if (this.label != null) {
            TextButton.TextButtonStyle textButtonStyle = (TextButton.TextButtonStyle) style2;
            Label.LabelStyle labelStyle = this.label.getStyle();
            labelStyle.font = textButtonStyle.font;
            labelStyle.fontColor = textButtonStyle.fontColor;
            this.label.setStyle(labelStyle);
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        Color fontColor;
        if (isDisabled() && this.style.disabledFontColor != null) {
            fontColor = this.style.disabledFontColor;
        } else if (isPressed() && this.style.downFontColor != null) {
            fontColor = this.style.downFontColor;
        } else if (!isChecked() || this.style.checkedFontColor == null) {
            fontColor = (!isOver() || this.style.overFontColor == null) ? this.style.fontColor : this.style.overFontColor;
        } else {
            fontColor = (!isOver() || this.style.checkedOverFontColor == null) ? this.style.checkedFontColor : this.style.checkedOverFontColor;
        }
        if (fontColor != null) {
            this.label.getStyle().fontColor = fontColor;
        }
        if (this.image != null && this.generateDisabledImage) {
            if (isDisabled()) {
                this.image.setColor(Color.GRAY);
            } else {
                this.image.setColor(Color.WHITE);
            }
        }
        super.draw(batch, parentAlpha);
    }

    public boolean isGenerateDisabledImage() {
        return this.generateDisabledImage;
    }

    public void setGenerateDisabledImage(boolean generateDisabledImage2) {
        this.generateDisabledImage = generateDisabledImage2;
    }

    public MenuItem setShortcut(int keycode) {
        return setShortcut(Input.Keys.toString(keycode));
    }

    public MenuItem setShortcut(int modifier, int keycode) {
        return setShortcut(Input.Keys.toString(modifier) + "+" + Input.Keys.toString(keycode));
    }

    public String getShortcut() {
        return this.shortcutLabelCell.getActor().getText().toString();
    }

    public MenuItem setShortcut(String text) {
        this.shortcutLabelCell.getActor().setText(text);
        return this;
    }

    public Image getImage() {
        return this.image;
    }

    public Cell<?> getImageCell() {
        return getCell(this.image);
    }

    public Label getLabel() {
        return this.label;
    }

    public Cell<?> getLabelCell() {
        return getCell(this.label);
    }

    public CharSequence getText() {
        return this.label.getText();
    }

    public void setText(CharSequence text) {
        this.label.setText(text);
    }

    public static class MenuItemStyle extends TextButton.TextButtonStyle {
        public Drawable subMenu;

        public MenuItemStyle() {
        }

        public MenuItemStyle(Drawable subMenu2) {
            this.subMenu = subMenu2;
        }

        public MenuItemStyle(MenuItemStyle other) {
            super(other);
            this.subMenu = other.subMenu;
        }
    }
}
