package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Disposable;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.color.ColorInputField;

public class ColorChannelWidget extends VisTable implements Disposable {
    /* access modifiers changed from: private */
    public ChannelBar bar;
    private ChangeListener barListener;
    private ColorChannelWidgetListener drawer;
    /* access modifiers changed from: private */
    public ColorInputField inputField;
    private int maxValue;
    private Pixmap pixmap;
    private Texture texture;
    private boolean useAlpha;
    /* access modifiers changed from: private */
    public int value;

    interface ColorChannelWidgetListener {
        void draw(Pixmap pixmap);

        void updateFields();
    }

    public ColorChannelWidget(String label, int maxValue2, ColorChannelWidgetListener drawer2) {
        this(label, maxValue2, false, drawer2);
    }

    public ColorChannelWidget(String label, int maxValue2, boolean useAlpha2, final ColorChannelWidgetListener drawer2) {
        super(true);
        this.value = 0;
        this.maxValue = maxValue2;
        this.drawer = drawer2;
        this.useAlpha = useAlpha2;
        this.barListener = new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                int unused = ColorChannelWidget.this.value = ColorChannelWidget.this.bar.getValue();
                drawer2.updateFields();
                ColorChannelWidget.this.inputField.setValue(ColorChannelWidget.this.value);
            }
        };
        if (useAlpha2) {
            this.pixmap = new Pixmap(maxValue2, 1, Pixmap.Format.RGBA8888);
        } else {
            this.pixmap = new Pixmap(maxValue2, 1, Pixmap.Format.RGB888);
        }
        this.texture = new Texture(this.pixmap);
        add(new VisLabel(label)).width(10.0f).center();
        ColorInputField colorInputField = new ColorInputField(maxValue2, new ColorInputField.ColorInputFieldListener() {
            public void changed(int newValue) {
                int unused = ColorChannelWidget.this.value = newValue;
                drawer2.updateFields();
                ColorChannelWidget.this.bar.setValue(newValue);
            }
        });
        this.inputField = colorInputField;
        add(colorInputField).width(50.0f);
        ChannelBar createBarImage = createBarImage();
        this.bar = createBarImage;
        add(createBarImage).size(130.0f, 11.0f);
        this.inputField.setValue(0);
    }

    public void dispose() {
        this.pixmap.dispose();
        this.texture.dispose();
    }

    public void redraw() {
        this.drawer.draw(this.pixmap);
        this.texture.draw(this.pixmap, 0, 0);
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value2) {
        this.value = value2;
        this.inputField.setValue(value2);
        this.bar.setValue(value2);
    }

    private ChannelBar createBarImage() {
        if (this.useAlpha) {
            return new AlphaChannelBar(this.texture, this.value, this.maxValue, this.barListener);
        }
        return new ChannelBar(this.texture, this.value, this.maxValue, this.barListener);
    }

    public boolean isInputValid() {
        return this.inputField.isInputValid();
    }
}
