package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.kotcrab.vis.ui.InputValidator;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisValidableTextField;

public class ColorInputField extends VisValidableTextField {
    private int maxValue;
    /* access modifiers changed from: private */
    public int value = 0;

    interface ColorInputFieldListener {
        void changed(int i);
    }

    public ColorInputField(final int maxValue2, final ColorInputFieldListener listener) {
        super(new ColorFieldValidator(maxValue2));
        this.maxValue = maxValue2;
        setProgrammaticChangeEvents(false);
        setMaxLength(3);
        setTextFieldFilter(new NumberFilter());
        addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                if (ColorInputField.this.getText().length() > 0) {
                    int unused = ColorInputField.this.value = Integer.valueOf(ColorInputField.this.getText()).intValue();
                }
            }
        });
        addListener(new InputListener() {
            public boolean keyTyped(InputEvent event, char character) {
                ColorInputField field = (ColorInputField) event.getListenerActor();
                if (character == '+') {
                    field.changeValue(Gdx.input.isKeyPressed(59) ? 10 : 1);
                }
                if (character == '-') {
                    field.changeValue(Gdx.input.isKeyPressed(59) ? -10 : -1);
                }
                if (character != 0) {
                    listener.changed(ColorInputField.this.getValue());
                }
                return true;
            }
        });
        addListener(new FocusListener() {
            public void keyboardFocusChanged(FocusListener.FocusEvent event, Actor actor, boolean focused) {
                if (!focused && !ColorInputField.this.isInputValid()) {
                    ColorInputField.this.setValue(maxValue2);
                }
            }
        });
    }

    public void changeValue(int byValue) {
        this.value += byValue;
        if (this.value > this.maxValue) {
            this.value = this.maxValue;
        }
        if (this.value < 0) {
            this.value = 0;
        }
        updateUI();
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value2) {
        this.value = value2;
        updateUI();
    }

    private void updateUI() {
        setText(String.valueOf(this.value));
        setCursorPosition(getMaxLength());
    }

    private static class NumberFilter implements VisTextField.TextFieldFilter {
        private NumberFilter() {
        }

        public boolean acceptChar(VisTextField textField, char c) {
            return Character.isDigit(c);
        }
    }

    private static class ColorFieldValidator implements InputValidator {
        private int maxValue;

        public ColorFieldValidator(int maxValue2) {
            this.maxValue = maxValue2;
        }

        public boolean validateInput(String input) {
            if (!input.equals("") && Integer.valueOf(Integer.parseInt(input)).intValue() <= this.maxValue) {
                return true;
            }
            return false;
        }
    }
}
