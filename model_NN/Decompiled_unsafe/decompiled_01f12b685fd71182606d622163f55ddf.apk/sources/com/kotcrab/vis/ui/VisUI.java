package com.kotcrab.vis.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.I18NBundle;
import java.util.Locale;

public class VisUI {
    public static final String VERSION = "0.7.4";
    private static int defaultSpacingBottom = 8;
    private static int defaultSpacingLeft = 0;
    private static int defaultSpacingRight = 6;
    private static int defaultSpacingTop = 0;
    private static int defaultTitleAlign = 8;
    private static I18NBundle dialogUtilsBundle;
    private static I18NBundle fileChooserBundle;
    private static Skin skin;
    private static I18NBundle tabbedPaneBundle;

    public static void load() {
        load(Gdx.files.classpath("com/kotcrab/vis/ui/uiskin.json"));
    }

    public static void load(FileHandle visSkinFile) {
        skin = new Skin(visSkinFile);
    }

    public static void load(Skin skin2) {
        skin = skin2;
    }

    public static void dispose() {
        if (skin != null) {
            skin.dispose();
            skin = null;
        }
    }

    public static Skin getSkin() {
        return skin;
    }

    public static int getDefaultTitleAlign() {
        return defaultTitleAlign;
    }

    public static void setDefaultTitleAlign(int defaultTitleAlign2) {
        defaultTitleAlign = defaultTitleAlign2;
    }

    public static I18NBundle getFileChooserBundle() {
        if (fileChooserBundle == null) {
            fileChooserBundle = I18NBundle.createBundle(Gdx.files.classpath("com/kotcrab/vis/ui/i18n/FileChooser"), new Locale("en"));
        }
        return fileChooserBundle;
    }

    public static void setFileChooserBundle(I18NBundle fileChooserBundle2) {
        fileChooserBundle = fileChooserBundle2;
    }

    public static I18NBundle getDialogUtilsBundle() {
        if (dialogUtilsBundle == null) {
            dialogUtilsBundle = I18NBundle.createBundle(Gdx.files.classpath("com/kotcrab/vis/ui/i18n/DialogUtils"), new Locale("en"));
        }
        return dialogUtilsBundle;
    }

    public static void setDialogUtilsBundle(I18NBundle dialogUtilsBundle2) {
        dialogUtilsBundle = dialogUtilsBundle2;
    }

    public static I18NBundle getTabbedPaneBundle() {
        if (tabbedPaneBundle == null) {
            tabbedPaneBundle = I18NBundle.createBundle(Gdx.files.classpath("com/kotcrab/vis/ui/i18n/TabbedPane"), new Locale("en"));
        }
        return tabbedPaneBundle;
    }

    public static void setTabbedPaneBundle(I18NBundle tabbedPaneBundle2) {
        tabbedPaneBundle = tabbedPaneBundle2;
    }

    public static int getDefaultSpacingTop() {
        return defaultSpacingTop;
    }

    public static void setDefaultSpacingTop(int defaultSpacingTop2) {
        defaultSpacingTop = defaultSpacingTop2;
    }

    public static int getDefaultSpacingBottom() {
        return defaultSpacingBottom;
    }

    public static void setDefaultSpacingBottom(int defaultSpacingBottom2) {
        defaultSpacingBottom = defaultSpacingBottom2;
    }

    public static int getDefaultSpacingRight() {
        return defaultSpacingRight;
    }

    public static void setDefaultSpacingRight(int defaultSpacingRight2) {
        defaultSpacingRight = defaultSpacingRight2;
    }

    public static int getDefaultSpacingLeft() {
        return defaultSpacingLeft;
    }

    public static void setDefaultSpacingLeft(int defaultSpacingLeft2) {
        defaultSpacingLeft = defaultSpacingLeft2;
    }
}
