package com.kotcrab.vis.ui.util.form;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.InputValidator;
import com.kotcrab.vis.ui.util.Validators;
import com.kotcrab.vis.ui.widget.VisValidableTextField;
import java.util.Iterator;

public class SimpleFormValidator {
    private Button button;
    private ChangeSharedListener changeListener = new ChangeSharedListener();
    private Label errorMsgLabel;
    private Array<VisValidableTextField> fields = new Array<>();

    public SimpleFormValidator(Button buttonToDisable, Label errorMsgLabel2) {
        this.button = buttonToDisable;
        this.errorMsgLabel = errorMsgLabel2;
    }

    public void notEmpty(VisValidableTextField field, String errorMsg) {
        field.addValidator(new EmptyInputValidator(errorMsg));
        add(field);
    }

    public void integerNumber(VisValidableTextField field, String errorMsg) {
        field.addValidator(new ValidatorWrapper(errorMsg, Validators.INTEGERS));
        add(field);
    }

    public void floatNumber(VisValidableTextField field, String errorMsg) {
        field.addValidator(new ValidatorWrapper(errorMsg, Validators.FLOATS));
        add(field);
    }

    public void valueGreaterThan(VisValidableTextField field, String errorMsg, float value) {
        valueGreaterThan(field, errorMsg, value, false);
    }

    public void valueLesserThan(VisValidableTextField field, String errorMsg, float value) {
        valueLesserThan(field, errorMsg, value, false);
    }

    public void valueGreaterThan(VisValidableTextField field, String errorMsg, float value, boolean equals) {
        field.addValidator(new ValidatorWrapper(errorMsg, new Validators.GreaterThanValidator(value, equals)));
        add(field);
    }

    public void valueLesserThan(VisValidableTextField field, String errorMsg, float value, boolean equals) {
        field.addValidator(new ValidatorWrapper(errorMsg, new Validators.LesserThanValidator(value, equals)));
        add(field);
    }

    public void custom(VisValidableTextField field, FormInputValidator customValidator) {
        field.addValidator(customValidator);
        add(field);
    }

    /* access modifiers changed from: protected */
    public void add(VisValidableTextField field) {
        this.fields.add(field);
        field.addListener(this.changeListener);
        checkAll();
    }

    /* access modifiers changed from: private */
    public void checkAll() {
        this.button.setDisabled(false);
        this.errorMsgLabel.setText("");
        Iterator<VisValidableTextField> it = this.fields.iterator();
        while (it.hasNext()) {
            it.next().validateInput();
        }
        Iterator<VisValidableTextField> it2 = this.fields.iterator();
        while (it2.hasNext()) {
            VisValidableTextField field = it2.next();
            if (!field.isInputValid()) {
                Iterator<InputValidator> it3 = field.getValidators().iterator();
                while (it3.hasNext()) {
                    FormInputValidator validator = (FormInputValidator) it3.next();
                    if (!validator.getLastResult()) {
                        this.errorMsgLabel.setText(validator.getErrorMsg());
                        this.button.setDisabled(true);
                        return;
                    }
                }
                return;
            }
        }
    }

    public static class EmptyInputValidator extends FormInputValidator {
        public EmptyInputValidator(String errorMsg) {
            super(errorMsg);
        }

        public boolean validate(String input) {
            return !input.isEmpty();
        }
    }

    private class ChangeSharedListener extends ChangeListener {
        private ChangeSharedListener() {
        }

        public void changed(ChangeListener.ChangeEvent event, Actor actor) {
            SimpleFormValidator.this.checkAll();
        }
    }
}
