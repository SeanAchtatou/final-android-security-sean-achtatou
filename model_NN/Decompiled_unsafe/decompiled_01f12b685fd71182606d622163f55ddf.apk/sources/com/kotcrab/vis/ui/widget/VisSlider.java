package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.FocusManager;
import com.kotcrab.vis.ui.VisUI;

public class VisSlider extends VisProgressBar {
    private ClickListener clickListener;
    int draggingPointer;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public VisSlider(float min, float max, float stepSize, boolean vertical) {
        this(min, max, stepSize, vertical, (VisSliderStyle) VisUI.getSkin().get("default-" + (vertical ? "vertical" : "horizontal"), VisSliderStyle.class));
    }

    public VisSlider(float min, float max, float stepSize, boolean vertical, String styleName) {
        this(min, max, stepSize, vertical, (VisSliderStyle) VisUI.getSkin().get(styleName, VisSliderStyle.class));
    }

    public VisSlider(float min, float max, float stepSize, boolean vertical, VisSliderStyle style) {
        super(min, max, stepSize, vertical, style);
        this.draggingPointer = -1;
        this.shiftIgnoresSnap = true;
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (VisSlider.this.disabled || VisSlider.this.draggingPointer != -1) {
                    return false;
                }
                VisSlider.this.draggingPointer = pointer;
                VisSlider.this.calculatePositionAndValue(x, y);
                FocusManager.getFocus();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (pointer == VisSlider.this.draggingPointer) {
                    VisSlider.this.draggingPointer = -1;
                    if (!VisSlider.this.calculatePositionAndValue(x, y)) {
                        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
                        VisSlider.this.fire(changeEvent);
                        Pools.free(changeEvent);
                    }
                }
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                VisSlider.this.calculatePositionAndValue(x, y);
            }
        });
        this.clickListener = new ClickListener();
        addListener(this.clickListener);
    }

    public void draw(Batch batch, float parentAlpha) {
        Drawable knob;
        VisSliderStyle style = getStyle();
        boolean disabled = this.disabled;
        if (!disabled || style.disabledKnob == null) {
            knob = isDragging() ? style.knobDown : this.clickListener.isOver() ? style.knobOver : style.knob;
        } else {
            knob = style.disabledKnob;
        }
        Drawable bg = (!disabled || style.disabledBackground == null) ? style.background : style.disabledBackground;
        Drawable knobBefore = (!disabled || style.disabledKnobBefore == null) ? style.knobBefore : style.disabledKnobBefore;
        Drawable knobAfter = (!disabled || style.disabledKnobAfter == null) ? style.knobAfter : style.disabledKnobAfter;
        Color color = getColor();
        float x = getX();
        float y = getY();
        float width = getWidth();
        float height = getHeight();
        float knobHeight = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinHeight();
        float knobWidth = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinWidth();
        float value = getVisualValue();
        float min = getMinValue();
        float max = getMaxValue();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        if (this.vertical) {
            bg.draw(batch, x + ((float) ((int) ((width - bg.getMinWidth()) * 0.5f))), y, bg.getMinWidth(), height);
            float positionHeight = height - (bg.getTopHeight() + bg.getBottomHeight());
            float knobHeightHalf = Animation.CurveTimeline.LINEAR;
            if (min != max) {
                if (knob == null) {
                    knobHeightHalf = knobBefore == null ? Animation.CurveTimeline.LINEAR : knobBefore.getMinHeight() * 0.5f;
                    this.position = ((value - min) / (max - min)) * (positionHeight - knobHeightHalf);
                    this.position = Math.min(positionHeight - knobHeightHalf, this.position);
                } else {
                    knobHeightHalf = knobHeight * 0.5f;
                    this.position = ((value - min) / (max - min)) * (positionHeight - knobHeight);
                    this.position = Math.min(positionHeight - knobHeight, this.position) + bg.getBottomHeight();
                }
                this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
            }
            if (knobBefore != null) {
                float offset = Animation.CurveTimeline.LINEAR;
                if (bg != null) {
                    offset = bg.getTopHeight();
                }
                knobBefore.draw(batch, x + ((float) ((int) ((width - knobBefore.getMinWidth()) * 0.5f))), y + offset, knobBefore.getMinWidth(), (float) ((int) (this.position + knobHeightHalf)));
            }
            if (knobAfter != null) {
                knobAfter.draw(batch, x + ((float) ((int) ((width - knobAfter.getMinWidth()) * 0.5f))), y + ((float) ((int) (this.position + knobHeightHalf))), knobAfter.getMinWidth(), height - ((float) ((int) (this.position + knobHeightHalf))));
            }
            if (knob != null) {
                knob.draw(batch, x + ((float) ((int) ((width - knobWidth) * 0.5f))), (float) ((int) (this.position + y)), knobWidth, knobHeight);
                return;
            }
            return;
        }
        bg.draw(batch, x, y + ((float) ((int) ((height - bg.getMinHeight()) * 0.5f))), width, bg.getMinHeight());
        float positionWidth = width - (bg.getLeftWidth() + bg.getRightWidth());
        float knobWidthHalf = Animation.CurveTimeline.LINEAR;
        if (min != max) {
            if (knob == null) {
                knobWidthHalf = knobBefore == null ? Animation.CurveTimeline.LINEAR : knobBefore.getMinWidth() * 0.5f;
                this.position = ((value - min) / (max - min)) * (positionWidth - knobWidthHalf);
                this.position = Math.min(positionWidth - knobWidthHalf, this.position);
            } else {
                knobWidthHalf = knobWidth * 0.5f;
                this.position = ((value - min) / (max - min)) * (positionWidth - knobWidth);
                this.position = Math.min(positionWidth - knobWidth, this.position) + bg.getLeftWidth();
            }
            this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
        }
        if (knobBefore != null) {
            float offset2 = Animation.CurveTimeline.LINEAR;
            if (bg != null) {
                offset2 = bg.getLeftWidth();
            }
            knobBefore.draw(batch, x + offset2, y + ((float) ((int) ((height - knobBefore.getMinHeight()) * 0.5f))), (float) ((int) (this.position + knobWidthHalf)), knobBefore.getMinHeight());
        }
        if (knobAfter != null) {
            knobAfter.draw(batch, x + ((float) ((int) (this.position + knobWidthHalf))), y + ((float) ((int) ((height - knobAfter.getMinHeight()) * 0.5f))), width - ((float) ((int) (this.position + knobWidthHalf))), knobAfter.getMinHeight());
        }
        if (knob != null) {
            knob.draw(batch, (float) ((int) (this.position + x)), (float) ((int) (((height - knobHeight) * 0.5f) + y)), knobWidth, knobHeight);
        }
    }

    public void setStyle(VisSliderStyle style) {
        if (style == null) {
            throw new NullPointerException("style cannot be null");
        } else if (!(style instanceof VisSliderStyle)) {
            throw new IllegalArgumentException("style must be a SliderStyle.");
        } else {
            super.setStyle((ProgressBar.ProgressBarStyle) style);
        }
    }

    public VisSliderStyle getStyle() {
        return (VisSliderStyle) super.getStyle();
    }

    /* access modifiers changed from: package-private */
    public boolean calculatePositionAndValue(float x, float y) {
        float value;
        VisSliderStyle style = getStyle();
        Drawable knob = (!this.disabled || style.disabledKnob == null) ? style.knob : style.disabledKnob;
        Drawable bg = (!this.disabled || style.disabledBackground == null) ? style.background : style.disabledBackground;
        float oldPosition = this.position;
        float min = getMinValue();
        float max = getMaxValue();
        if (this.vertical) {
            float height = (getHeight() - bg.getTopHeight()) - bg.getBottomHeight();
            float knobHeight = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinHeight();
            this.position = (y - bg.getBottomHeight()) - (0.5f * knobHeight);
            value = min + ((max - min) * (this.position / (height - knobHeight)));
            this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
            this.position = Math.min(height - knobHeight, this.position);
        } else {
            float width = (getWidth() - bg.getLeftWidth()) - bg.getRightWidth();
            float knobWidth = knob == null ? Animation.CurveTimeline.LINEAR : knob.getMinWidth();
            this.position = (x - bg.getLeftWidth()) - (0.5f * knobWidth);
            value = min + ((max - min) * (this.position / (width - knobWidth)));
            this.position = Math.max((float) Animation.CurveTimeline.LINEAR, this.position);
            this.position = Math.min(width - knobWidth, this.position);
        }
        boolean valueSet = setValue(value);
        if (value == value) {
            this.position = oldPosition;
        }
        return valueSet;
    }

    public boolean isDragging() {
        return this.draggingPointer != -1;
    }

    public static class VisSliderStyle extends ProgressBar.ProgressBarStyle {
        public Drawable knobDown;
        public Drawable knobOver;

        public VisSliderStyle() {
        }

        public VisSliderStyle(Drawable background, Drawable knob) {
            super(background, knob);
        }

        public VisSliderStyle(VisSliderStyle style) {
            super(style);
            this.knobOver = style.knobOver;
            this.knobDown = style.knobDown;
        }
    }
}
