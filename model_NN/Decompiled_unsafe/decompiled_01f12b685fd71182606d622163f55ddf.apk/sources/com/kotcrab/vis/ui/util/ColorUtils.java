package com.kotcrab.vis.ui.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class ColorUtils {
    public static Color HSVtoRGB(float h, float s, float v, float alpha) {
        Color c = HSVtoRGB(h, s, v);
        c.a = alpha;
        return c;
    }

    public static Color HSVtoRGB(float h, float s, float v) {
        Color c = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        HSVtoRGB(h, s, v, c);
        return c;
    }

    public static Color HSVtoRGB(float h, float s, float v, Color targetColor) {
        int r;
        int g;
        int b;
        if (h == 360.0f) {
            h = 359.0f;
        }
        float h2 = (float) Math.max(0.0d, Math.min(360.0d, (double) h));
        float s2 = ((float) Math.max(0.0d, Math.min(100.0d, (double) s))) / 100.0f;
        float v2 = ((float) Math.max(0.0d, Math.min(100.0d, (double) v))) / 100.0f;
        float h3 = h2 / 60.0f;
        int i = MathUtils.floor(h3);
        float f = h3 - ((float) i);
        float p = v2 * (1.0f - s2);
        float q = v2 * (1.0f - (s2 * f));
        float t = v2 * (1.0f - ((1.0f - f) * s2));
        switch (i) {
            case 0:
                r = MathUtils.round(255.0f * v2);
                g = MathUtils.round(255.0f * t);
                b = MathUtils.round(255.0f * p);
                break;
            case 1:
                r = MathUtils.round(255.0f * q);
                g = MathUtils.round(255.0f * v2);
                b = MathUtils.round(255.0f * p);
                break;
            case 2:
                r = MathUtils.round(255.0f * p);
                g = MathUtils.round(255.0f * v2);
                b = MathUtils.round(255.0f * t);
                break;
            case 3:
                r = MathUtils.round(255.0f * p);
                g = MathUtils.round(255.0f * q);
                b = MathUtils.round(255.0f * v2);
                break;
            case 4:
                r = MathUtils.round(255.0f * t);
                g = MathUtils.round(255.0f * p);
                b = MathUtils.round(255.0f * v2);
                break;
            default:
                r = MathUtils.round(255.0f * v2);
                g = MathUtils.round(255.0f * p);
                b = MathUtils.round(255.0f * q);
                break;
        }
        Color color = targetColor;
        color.set(((float) r) / 255.0f, ((float) g) / 255.0f, ((float) b) / 255.0f, targetColor.a);
        return targetColor;
    }

    public static int[] RGBtoHSV(Color c) {
        return RGBtoHSV(c.r, c.g, c.b);
    }

    public static int[] RGBtoHSV(float r, float g, float b) {
        float h;
        float min = Math.min(Math.min(r, g), b);
        float max = Math.max(Math.max(r, g), b);
        float v = max;
        float delta = max - min;
        if (max != Animation.CurveTimeline.LINEAR) {
            float s = delta / max;
            if (delta == Animation.CurveTimeline.LINEAR) {
                h = Animation.CurveTimeline.LINEAR;
            } else if (r == max) {
                h = (g - b) / delta;
            } else if (g == max) {
                h = 2.0f + ((b - r) / delta);
            } else {
                h = 4.0f + ((r - g) / delta);
            }
            float h2 = h * 60.0f;
            if (h2 < Animation.CurveTimeline.LINEAR) {
                h2 += 360.0f;
            }
            return new int[]{MathUtils.round(h2), MathUtils.round(s * 100.0f), MathUtils.round(v * 100.0f)};
        }
        return new int[]{MathUtils.round(Animation.CurveTimeline.LINEAR), MathUtils.round(Animation.CurveTimeline.LINEAR), MathUtils.round(v)};
    }
}
