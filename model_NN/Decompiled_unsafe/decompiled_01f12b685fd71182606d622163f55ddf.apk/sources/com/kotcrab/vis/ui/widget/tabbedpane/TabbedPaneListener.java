package com.kotcrab.vis.ui.widget.tabbedpane;

public interface TabbedPaneListener {
    void removedAllTabs();

    void removedTab(Tab tab);

    void switchedTab(Tab tab);
}
