package com.kotcrab.vis.ui.building;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.building.utilities.CellWidget;
import com.kotcrab.vis.ui.building.utilities.Padding;
import java.util.Iterator;

public class OneRowTableBuilder extends TableBuilder {
    public OneRowTableBuilder() {
    }

    public OneRowTableBuilder(Padding defaultWidgetPadding) {
        super(defaultWidgetPadding);
    }

    public OneRowTableBuilder(int estimatedWidgetsAmount, int estimatedRowsAmount) {
        super(estimatedWidgetsAmount, estimatedRowsAmount);
    }

    public OneRowTableBuilder(int estimatedWidgetsAmount, int estimatedRowsAmount, Padding defaultWidgetPadding) {
        super(estimatedWidgetsAmount, estimatedRowsAmount, defaultWidgetPadding);
    }

    /* access modifiers changed from: protected */
    public void fillTable(Table table) {
        Iterator<CellWidget<? extends Actor>> it = getWidgets().iterator();
        while (it.hasNext()) {
            it.next().buildCell(table, getDefaultWidgetPadding());
        }
    }
}
