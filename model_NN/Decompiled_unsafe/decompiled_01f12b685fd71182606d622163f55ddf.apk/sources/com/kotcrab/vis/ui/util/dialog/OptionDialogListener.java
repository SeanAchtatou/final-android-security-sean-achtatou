package com.kotcrab.vis.ui.util.dialog;

public interface OptionDialogListener {
    void cancel();

    void no();

    void yes();
}
