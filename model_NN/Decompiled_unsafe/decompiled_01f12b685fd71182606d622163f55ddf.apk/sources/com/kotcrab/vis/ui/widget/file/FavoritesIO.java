package com.kotcrab.vis.ui.widget.file;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import java.util.Iterator;

public class FavoritesIO {
    private static final String DEFAULT_FAVORITES_PREFS_NAME = "com.kotcrab.vis.ui.widget.file.filechooser_favorites";
    private static String favoritesPrefsName = DEFAULT_FAVORITES_PREFS_NAME;
    private static String keyName = "favorites";
    private Json json = new Json();
    private Preferences prefs = Gdx.app.getPreferences(favoritesPrefsName);

    public static String getFavoritesPrefsName() {
        return favoritesPrefsName;
    }

    public static void setFavoritesPrefsName(String favoritesPrefsName2) {
        favoritesPrefsName = favoritesPrefsName2;
    }

    public Array<FileHandle> loadFavorites() {
        String data = this.prefs.getString(keyName, null);
        if (data == null) {
            return new Array<>();
        }
        return ((FavouriteData) this.json.fromJson(FavouriteData.class, data)).toFileHadnleArray();
    }

    public void saveFavorites(Array<FileHandle> favorites) {
        this.prefs.putString(keyName, this.json.toJson(new FavouriteData(favorites)));
        this.prefs.flush();
    }

    public void checkIfUsingDefaultName() {
        if (favoritesPrefsName.equals(DEFAULT_FAVORITES_PREFS_NAME)) {
            Gdx.app.log("VisUI", "Warning, using default favorites preference name for file chooser! (see FileChooser.setFavoritesPrefsName(String))");
        }
    }

    private static class FavouriteData {
        public Array<FileHandleData> data;

        public FavouriteData() {
        }

        public FavouriteData(Array<FileHandle> favourites) {
            this.data = new Array<>();
            Iterator<FileHandle> it = favourites.iterator();
            while (it.hasNext()) {
                this.data.add(new FileHandleData(it.next()));
            }
        }

        public Array<FileHandle> toFileHadnleArray() {
            Array<FileHandle> files = new Array<>();
            Iterator<FileHandleData> it = this.data.iterator();
            while (it.hasNext()) {
                files.add(it.next().toFileHandle());
            }
            return files;
        }
    }

    private static class FileHandleData {
        public String path;
        public Files.FileType type;

        public FileHandleData() {
        }

        public FileHandleData(FileHandle file) {
            this.type = file.type();
            this.path = file.path();
        }

        public FileHandle toFileHandle() {
            switch (this.type) {
                case Absolute:
                    return Gdx.files.absolute(this.path);
                case Classpath:
                    return Gdx.files.classpath(this.path);
                case External:
                    return Gdx.files.external(this.path);
                case Internal:
                    return Gdx.files.internal(this.path);
                case Local:
                    return Gdx.files.local(this.path);
                default:
                    throw new IllegalStateException("Unknown file type!");
            }
        }
    }
}
