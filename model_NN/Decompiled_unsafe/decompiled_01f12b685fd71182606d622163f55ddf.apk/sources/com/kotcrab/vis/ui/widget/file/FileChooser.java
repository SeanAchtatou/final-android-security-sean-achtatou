package com.kotcrab.vis.ui.widget.file;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisDialog;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisSplitPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisWindow;
import java.io.File;
import java.io.FileFilter;
import java.util.Iterator;
import javax.swing.filechooser.FileSystemView;

public class FileChooser extends VisWindow {
    /* access modifiers changed from: private */
    public static final Drawable HIGHLIGHT = VisUI.getSkin().getDrawable("list-selection");
    private VisImageButton backButton;
    private I18NBundle bundle;
    /* access modifiers changed from: private */
    public FileHandle currentDirectory;
    /* access modifiers changed from: private */
    public VisTextField currentPath;
    /* access modifiers changed from: private */
    public Array<FileHandle> favorites;
    private FavoritesIO favoritesIO;
    private FileFilter fileFilter;
    /* access modifiers changed from: private */
    public FilePopupMenu fileMenu;
    private Array<ShortcutItem> fileRootsCache;
    /* access modifiers changed from: private */
    public VisScrollPane fileScrollPane;
    /* access modifiers changed from: private */
    public VisTable fileScrollPaneTable;
    private FileSystemView fileSystemView;
    /* access modifiers changed from: private */
    public VisTable fileTable;
    private VisImageButton forwardButton;
    /* access modifiers changed from: private */
    public int groupMultiselectKey;
    private Array<FileHandle> history;
    private int historyIndex;
    /* access modifiers changed from: private */
    public FileChooserListener listener;
    /* access modifiers changed from: private */
    public Mode mode;
    /* access modifiers changed from: private */
    public int multiselectKey;
    /* access modifiers changed from: private */
    public boolean multiselectionEnabled;
    private VisTextField selectedFileTextField;
    /* access modifiers changed from: private */
    public Array<FileItem> selectedItems;
    /* access modifiers changed from: private */
    public ShortcutItem selectedShortcut;
    private SelectionMode selectionMode;
    private VisScrollPane shortcutsScrollPane;
    /* access modifiers changed from: private */
    public VisTable shortcutsScrollPaneTable;
    private VisTable shortcutsTable;
    /* access modifiers changed from: private */
    public FileChooserStyle style;

    public static class FileChooserStyle {
        public Drawable iconArrowLeft = VisUI.getSkin().getDrawable("icon-arrow-left");
        public Drawable iconArrowRight = VisUI.getSkin().getDrawable("icon-arrow-right");
        public Drawable iconDrive = VisUI.getSkin().getDrawable("icon-drive");
        public Drawable iconFolder = VisUI.getSkin().getDrawable("icon-folder");
    }

    public enum Mode {
        OPEN,
        SAVE
    }

    public enum SelectionMode {
        FILES,
        DIRECTORIES,
        FILES_AND_DIRECTORIES
    }

    public FileChooser(Mode mode2) {
        super("");
        this.selectionMode = SelectionMode.FILES;
        this.multiselectionEnabled = false;
        this.groupMultiselectKey = 59;
        this.multiselectKey = 129;
        this.fileFilter = new DefaultFileFilter();
        this.fileSystemView = FileSystemView.getFileSystemView();
        this.selectedItems = new Array<>();
        this.historyIndex = 0;
        this.fileRootsCache = new Array<>();
        this.bundle = VisUI.getFileChooserBundle();
        this.mode = mode2;
        getTitleLabel().setText(getText(FileChooserText.TITLE_CHOOSE_FILES));
        init();
    }

    public FileChooser(String title, Mode mode2) {
        super(title);
        this.selectionMode = SelectionMode.FILES;
        this.multiselectionEnabled = false;
        this.groupMultiselectKey = 59;
        this.multiselectKey = 129;
        this.fileFilter = new DefaultFileFilter();
        this.fileSystemView = FileSystemView.getFileSystemView();
        this.selectedItems = new Array<>();
        this.historyIndex = 0;
        this.fileRootsCache = new Array<>();
        this.mode = mode2;
        this.bundle = VisUI.getFileChooserBundle();
        init();
    }

    public FileChooser(I18NBundle bundle2, Mode mode2) {
        super("");
        this.selectionMode = SelectionMode.FILES;
        this.multiselectionEnabled = false;
        this.groupMultiselectKey = 59;
        this.multiselectKey = 129;
        this.fileFilter = new DefaultFileFilter();
        this.fileSystemView = FileSystemView.getFileSystemView();
        this.selectedItems = new Array<>();
        this.historyIndex = 0;
        this.fileRootsCache = new Array<>();
        this.mode = mode2;
        this.bundle = bundle2;
        getTitleLabel().setText(getText(FileChooserText.TITLE_CHOOSE_FILES));
        init();
    }

    public FileChooser(I18NBundle bundle2, String title, Mode mode2) {
        super(title);
        this.selectionMode = SelectionMode.FILES;
        this.multiselectionEnabled = false;
        this.groupMultiselectKey = 59;
        this.multiselectKey = 129;
        this.fileFilter = new DefaultFileFilter();
        this.fileSystemView = FileSystemView.getFileSystemView();
        this.selectedItems = new Array<>();
        this.historyIndex = 0;
        this.fileRootsCache = new Array<>();
        this.mode = mode2;
        this.bundle = bundle2;
        init();
    }

    public static void setFavoritesPrefsName(String name) {
        FavoritesIO.setFavoritesPrefsName(name);
    }

    private void init() {
        this.style = new FileChooserStyle();
        setModal(true);
        setResizable(true);
        setMovable(true);
        addCloseButton();
        closeOnEscape();
        this.favoritesIO = new FavoritesIO();
        this.favoritesIO.checkIfUsingDefaultName();
        this.favorites = this.favoritesIO.loadFavorites();
        createToolbar();
        createCenterContentPanel();
        createFileTextBox();
        createBottomButtons();
        this.fileMenu = new FilePopupMenu(this, this.bundle);
        rebuildShortcutsList();
        setDirectory(System.getProperty("user.home"));
        setSize(500.0f, 600.0f);
        centerWindow();
        validateSettings();
        addListener(new InputListener() {
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode != 29 || !Gdx.input.isKeyPressed(129)) {
                    return false;
                }
                FileChooser.this.selectAll();
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public String getText(FileChooserText text) {
        return this.bundle.get(text.getName());
    }

    private void createToolbar() {
        VisTable toolbarTable = new VisTable(true);
        toolbarTable.defaults().minWidth(30.0f).right();
        add(toolbarTable).fillX().expandX().pad(3.0f).padRight(2.0f);
        this.backButton = new VisImageButton(this.style.iconArrowLeft);
        this.forwardButton = new VisImageButton(this.style.iconArrowRight);
        this.forwardButton.setDisabled(true);
        this.forwardButton.setGenerateDisabledImage(true);
        this.backButton.setGenerateDisabledImage(true);
        this.currentPath = new VisTextField();
        this.currentPath.addListener(new InputListener() {
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode != 66) {
                    return false;
                }
                FileHandle file = Gdx.files.absolute(FileChooser.this.currentPath.getText());
                if (file.exists()) {
                    FileChooser.this.setDirectory(file);
                    return false;
                }
                FileChooser.this.showDialog(FileChooser.this.getText(FileChooserText.POPUP_DIRECTORY_DOES_NOT_EXIST));
                FileChooser.this.currentPath.setText(FileChooser.this.currentDirectory.path());
                return false;
            }
        });
        toolbarTable.add(this.backButton);
        toolbarTable.add(this.forwardButton);
        toolbarTable.add(this.currentPath).expand().fill();
        this.backButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                FileChooser.this.historyBack();
            }
        });
        this.forwardButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                FileChooser.this.historyForward();
            }
        });
    }

    private void createCenterContentPanel() {
        this.fileTable = new VisTable();
        this.fileScrollPane = createScrollPane(this.fileTable);
        this.fileScrollPaneTable = new VisTable();
        this.fileScrollPaneTable.add(this.fileScrollPane).pad(2.0f).top().expand().fillX();
        this.shortcutsTable = new VisTable();
        this.shortcutsScrollPane = createScrollPane(this.shortcutsTable);
        this.shortcutsScrollPaneTable = new VisTable();
        this.shortcutsScrollPaneTable.add(this.shortcutsScrollPane).pad(2.0f).top().expand().fillX();
        VisSplitPane splitPane = new VisSplitPane(this.shortcutsScrollPaneTable, this.fileScrollPaneTable, false);
        splitPane.setSplitAmount(0.3f);
        splitPane.setMinSplitAmount(0.05f);
        splitPane.setMaxSplitAmount(0.8913f);
        row();
        add(splitPane).expand().fill();
        row();
    }

    private void createFileTextBox() {
        VisTable table = new VisTable(true);
        VisLabel nameLabel = new VisLabel(getText(FileChooserText.FILE_NAME));
        this.selectedFileTextField = new VisTextField();
        table.add(nameLabel);
        table.add(this.selectedFileTextField).expand().fill();
        this.selectedFileTextField.addListener(new InputListener() {
            public boolean keyTyped(InputEvent event, char character) {
                FileChooser.this.deselectAll(false);
                return false;
            }
        });
        add(table).expandX().fillX().pad(3.0f).padRight(2.0f).padBottom(2.0f);
        row();
    }

    private void createBottomButtons() {
        VisTextButton cancelButton = new VisTextButton(getText(FileChooserText.CANCEL));
        VisTextButton confirmButton = new VisTextButton(this.mode == Mode.OPEN ? getText(FileChooserText.OPEN) : getText(FileChooserText.SAVE));
        VisTable buttonTable = new VisTable(true);
        buttonTable.defaults().minWidth(70.0f).right();
        add(buttonTable).padTop(3.0f).padBottom(3.0f).padRight(2.0f).fillX().expandX();
        buttonTable.add(cancelButton).expand().right();
        buttonTable.add(confirmButton);
        cancelButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                FileChooser.this.fadeOut();
                FileChooser.this.listener.canceled();
            }
        });
        confirmButton.addListener(new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                FileChooser.this.selectionFinished();
            }
        });
    }

    /* access modifiers changed from: private */
    public void selectionFinished() {
        if (this.selectedItems.size == 1) {
            if (this.selectionMode == SelectionMode.FILES) {
                FileHandle selected = this.selectedItems.get(0).file;
                if (selected.isDirectory()) {
                    setDirectory(selected);
                    return;
                }
            }
            if (this.selectionMode == SelectionMode.DIRECTORIES && !this.selectedItems.get(0).file.isDirectory()) {
                showDialog(getText(FileChooserText.POPUP_ONLY_DIRECTORIES));
                return;
            }
        }
        if (this.selectedItems.size > 0 || this.mode == Mode.SAVE) {
            notifyListnerAndCloseDialog(getFileListFromSelected());
        } else if (this.selectionMode == SelectionMode.FILES) {
            showDialog(getText(FileChooserText.POPUP_CHOOSE_FILE));
        } else {
            Array<FileHandle> files = new Array<>();
            files.add(this.currentDirectory);
            notifyListnerAndCloseDialog(files);
        }
    }

    /* access modifiers changed from: protected */
    public void close() {
        this.listener.canceled();
        super.close();
    }

    /* access modifiers changed from: private */
    public void notifyListnerAndCloseDialog(Array<FileHandle> files) {
        if (files != null) {
            this.listener.selected(files);
            this.listener.selected(files.get(0));
            fadeOut();
        }
    }

    private VisScrollPane createScrollPane(VisTable table) {
        VisScrollPane scrollPane = new VisScrollPane(table);
        scrollPane.setOverscroll(false, true);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setScrollingDisabled(true, false);
        return scrollPane;
    }

    private Array<FileHandle> getFileListFromSelected() {
        Array<FileHandle> list = new Array<>();
        if (this.mode == Mode.OPEN) {
            Iterator<FileItem> it = this.selectedItems.iterator();
            while (it.hasNext()) {
                list.add(it.next().file);
            }
            return list;
        } else if (this.selectedItems.size > 0) {
            Iterator<FileItem> it2 = this.selectedItems.iterator();
            while (it2.hasNext()) {
                list.add(it2.next().file);
            }
            showOverwriteQuestion(list);
            return null;
        } else {
            String fileName = this.selectedFileTextField.getText();
            FileHandle file = this.currentDirectory.child(fileName);
            if (!FileUtils.isValidFileName(fileName)) {
                showDialog(getText(FileChooserText.POPUP_FILENAME_INVALID));
                return null;
            } else if (file.exists()) {
                list.add(file);
                showOverwriteQuestion(list);
                return null;
            } else {
                list.add(file);
                return list;
            }
        }
    }

    /* access modifiers changed from: private */
    public void showDialog(String text) {
        VisDialog dialog = new VisDialog(getText(FileChooserText.POPUP_TITLE));
        dialog.text(text);
        dialog.button(getText(FileChooserText.POPUP_OK));
        dialog.pack();
        dialog.centerWindow();
        getStage().addActor(dialog.fadeIn());
    }

    private void showOverwriteQuestion(Array<FileHandle> filesList) {
        VisDialog dialog = new VisDialog(getText(FileChooserText.POPUP_TITLE)) {
            /* access modifiers changed from: protected */
            public void result(Object object) {
                FileChooser.this.notifyListnerAndCloseDialog((Array) object);
            }
        };
        dialog.text(filesList.size == 1 ? getText(FileChooserText.POPUP_FILE_EXIST_OVERWRITE) : getText(FileChooserText.POPUP_MULTIPLE_FILE_EXIST_OVERWRITE));
        dialog.button(getText(FileChooserText.POPUP_NO), (Object) null);
        dialog.button(getText(FileChooserText.POPUP_YES), filesList);
        dialog.pack();
        dialog.centerWindow();
        getStage().addActor(dialog.fadeIn());
    }

    private void rebuildShortcutsList(boolean rebuildRootCache) {
        this.shortcutsTable.clear();
        String userHome = System.getProperty("user.home");
        String userName = System.getProperty("user.name");
        this.shortcutsTable.add(new ShortcutItem(this.fileSystemView.getHomeDirectory(), getText(FileChooserText.DESKTOP), this.style.iconFolder)).expand().fill().row();
        this.shortcutsTable.add(new ShortcutItem(new File(userHome), userName, this.style.iconFolder)).expand().fill().row();
        this.shortcutsTable.addSeparator();
        if (rebuildRootCache) {
            rebuildFileRootsCache();
        }
        Iterator<ShortcutItem> it = this.fileRootsCache.iterator();
        while (it.hasNext()) {
            this.shortcutsTable.add(it.next()).expandX().fillX().row();
        }
        Array<FileHandle> favourites = this.favoritesIO.loadFavorites();
        if (favourites.size > 0) {
            this.shortcutsTable.addSeparator();
            Iterator<FileHandle> it2 = favourites.iterator();
            while (it2.hasNext()) {
                FileHandle f = it2.next();
                this.shortcutsTable.add(new ShortcutItem(f.file(), f.name(), this.style.iconFolder)).expand().fill().row();
            }
        }
    }

    private void rebuildShortcutsList() {
        rebuildShortcutsList(true);
    }

    private void rebuildFileRootsCache() {
        ShortcutItem item;
        this.fileRootsCache.clear();
        File[] roots = File.listRoots();
        for (File root : roots) {
            if (this.mode == Mode.OPEN) {
                if (!root.canRead()) {
                }
            } else if (!root.canWrite()) {
            }
            String displayName = this.fileSystemView.getSystemDisplayName(root);
            if (displayName == null || displayName.equals("")) {
                item = new ShortcutItem(root, root.toString(), this.style.iconDrive);
            } else {
                item = new ShortcutItem(root, displayName, this.style.iconDrive);
            }
            this.fileRootsCache.add(item);
        }
    }

    private void rebuildFileList() {
        deselectAll();
        this.fileTable.clear();
        FileHandle[] files = this.currentDirectory.list(this.fileFilter);
        this.currentPath.setText(this.currentDirectory.path());
        if (files.length != 0) {
            Iterator<FileHandle> it = FileUtils.sortFiles(files).iterator();
            while (it.hasNext()) {
                FileHandle f = it.next();
                if (f.file() == null || !f.file().isHidden()) {
                    this.fileTable.add(new FileItem(f, null)).expand().fill().row();
                }
            }
            this.fileScrollPane.setScrollX(Animation.CurveTimeline.LINEAR);
            this.fileScrollPane.setScrollY(Animation.CurveTimeline.LINEAR);
        }
    }

    public void refresh() {
        rebuildShortcutsList();
        rebuildFileList();
    }

    public void addFavorite(FileHandle favourite) {
        this.favorites.add(favourite);
        this.favoritesIO.saveFavorites(this.favorites);
        rebuildShortcutsList(false);
    }

    public boolean removeFavorite(FileHandle favorite) {
        boolean removed = this.favorites.removeValue(favorite, false);
        this.favoritesIO.saveFavorites(this.favorites);
        rebuildShortcutsList(false);
        return removed;
    }

    public void setVisble(boolean visible) {
        if (!isVisible() && visible) {
            deselectAll();
        }
        super.setVisible(visible);
    }

    /* access modifiers changed from: private */
    public void deselectAll() {
        deselectAll(true);
    }

    /* access modifiers changed from: private */
    public void deselectAll(boolean updateTextField) {
        Iterator<FileItem> it = this.selectedItems.iterator();
        while (it.hasNext()) {
            it.next().deselect(false);
        }
        this.selectedItems.clear();
        if (updateTextField) {
            setSelectedFileFieldText();
        }
    }

    /* access modifiers changed from: private */
    public void selectAll() {
        Iterator<Cell> it = this.fileTable.getCells().iterator();
        while (it.hasNext()) {
            boolean unused = ((FileItem) it.next().getActor()).select(false);
        }
        removeInvalidSelections();
        setSelectedFileFieldText();
    }

    /* access modifiers changed from: private */
    public void setSelectedFileFieldText() {
        if (this.selectedItems.size == 0) {
            this.selectedFileTextField.setText("");
        } else if (this.selectedItems.size == 1) {
            this.selectedFileTextField.setText(this.selectedItems.get(0).file.name());
        } else {
            StringBuilder b = new StringBuilder();
            Iterator<FileItem> it = this.selectedItems.iterator();
            while (it.hasNext()) {
                b.append('\"');
                b.append(it.next().file.name());
                b.append("\" ");
            }
            this.selectedFileTextField.setText(b.toString());
        }
    }

    /* access modifiers changed from: private */
    public void removeInvalidSelections() {
        if (this.selectionMode == SelectionMode.FILES) {
            Iterator<FileItem> it = this.selectedItems.iterator();
            while (it.hasNext()) {
                FileItem item = it.next();
                if (item.file.isDirectory()) {
                    item.deselect();
                }
            }
        }
        if (this.selectionMode == SelectionMode.DIRECTORIES) {
            Iterator<FileItem> it2 = this.selectedItems.iterator();
            while (it2.hasNext()) {
                FileItem item2 = it2.next();
                if (!item2.file.isDirectory()) {
                    item2.deselect();
                }
            }
        }
    }

    private void historyBuild() {
        Array<FileHandle> fileTree = new Array<>();
        fileTree.add(this.currentDirectory);
        FileHandle next = this.currentDirectory;
        while (true) {
            FileHandle parent = next.parent();
            if (next.file().getParent() == null) {
                break;
            }
            next = parent;
            fileTree.add(parent);
        }
        fileTree.reverse();
        this.history = fileTree;
        this.historyIndex = fileTree.size - 1;
        if (this.historyIndex == 0) {
            this.backButton.setDisabled(true);
        } else {
            this.backButton.setDisabled(false);
        }
        this.forwardButton.setDisabled(true);
    }

    /* access modifiers changed from: private */
    public void historyAdd(FileHandle file) {
        this.history.add(file);
        this.historyIndex++;
        this.backButton.setDisabled(false);
    }

    /* access modifiers changed from: private */
    public void historyBack() {
        if (this.historyIndex > 0) {
            this.historyIndex--;
            setDirectory(this.history.get(this.historyIndex), false);
            this.forwardButton.setDisabled(false);
        }
        if (this.historyIndex == 0) {
            this.backButton.setDisabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void historyForward() {
        this.historyIndex++;
        setDirectory(this.history.get(this.historyIndex), false);
        if (this.historyIndex == this.history.size - 1) {
            this.forwardButton.setDisabled(true);
        }
        this.backButton.setDisabled(false);
    }

    public Mode getMode() {
        return this.mode;
    }

    public void setMode(Mode mode2) {
        this.mode = mode2;
    }

    public void setDirectory(String directory) {
        setDirectory(Gdx.files.absolute(directory));
    }

    public void setDirectory(File directory) {
        setDirectory(Gdx.files.absolute(directory.getAbsolutePath()));
    }

    public void setDirectory(FileHandle directory) {
        setDirectory(directory, true);
    }

    private void setDirectory(FileHandle directory, boolean rebuildHistory) {
        if (!directory.exists()) {
            throw new IllegalStateException("Provided directory does not exist!");
        } else if (!directory.isDirectory()) {
            throw new IllegalStateException("Provided directory path is a file, not directory!");
        } else {
            this.currentDirectory = directory;
            rebuildFileList();
            if (rebuildHistory) {
                historyBuild();
            }
        }
    }

    public FileFilter getFileFilter() {
        return this.fileFilter;
    }

    public void setFileFilter(FileFilter fileFilter2) {
        this.fileFilter = fileFilter2;
    }

    public SelectionMode getSelectionMode() {
        return this.selectionMode;
    }

    public void setSelectionMode(SelectionMode selectionMode2) {
        this.selectionMode = selectionMode2;
        switch (selectionMode2) {
            case FILES:
                getTitleLabel().setText(getText(FileChooserText.TITLE_CHOOSE_FILES));
                return;
            case DIRECTORIES:
                getTitleLabel().setText(getText(FileChooserText.TITLE_CHOOSE_DIRECTORIES));
                return;
            case FILES_AND_DIRECTORIES:
                getTitleLabel().setText(getText(FileChooserText.TITLE_CHOOSE_FILES_AND_DIRECTORIES));
                return;
            default:
                return;
        }
    }

    public boolean isMultiselectionEnabled() {
        return this.multiselectionEnabled;
    }

    public void setMultiselectionEnabled(boolean multiselectionEnabled2) {
        this.multiselectionEnabled = multiselectionEnabled2;
    }

    public void setListener(FileChooserListener listener2) {
        this.listener = listener2;
        validateSettings();
    }

    public int getMultiselectKey() {
        return this.multiselectKey;
    }

    public void setMultiselectKey(int multiselectKey2) {
        this.multiselectKey = multiselectKey2;
    }

    public int getGroupMultiselectKey() {
        return this.groupMultiselectKey;
    }

    public void setGroupMultiselectKey(int groupMultiselectKey2) {
        this.groupMultiselectKey = groupMultiselectKey2;
    }

    private void validateSettings() {
        if (this.listener == null) {
            this.listener = new FileChooserAdapter();
        }
    }

    /* access modifiers changed from: protected */
    public void setStage(Stage stage) {
        super.setStage(stage);
        deselectAll();
    }

    private class DefaultFileFilter implements FileFilter {
        private DefaultFileFilter() {
        }

        public boolean accept(File f) {
            if (f.isHidden()) {
                return false;
            }
            if (FileChooser.this.mode == Mode.OPEN) {
                if (f.canRead()) {
                    return true;
                }
                return false;
            } else if (!f.canWrite()) {
                return false;
            }
            return true;
        }
    }

    private class FileItem extends Table {
        public FileHandle file;
        private VisLabel name;
        private VisLabel size;

        public FileItem(final FileHandle file2, Drawable icon) {
            this.file = file2;
            setTouchable(Touchable.enabled);
            this.name = new VisLabel(file2.name());
            this.name.setEllipsis(true);
            if (file2.isDirectory()) {
                this.size = new VisLabel("");
            } else {
                this.size = new VisLabel(FileUtils.readableFileSize(file2.length()));
            }
            if (icon == null && file2.isDirectory()) {
                icon = FileChooser.this.style.iconFolder;
            }
            if (icon != null) {
                add(new Image(icon)).padTop(3.0f);
            }
            add(this.name).padLeft(icon == null ? 22.0f : Animation.CurveTimeline.LINEAR).width(new Value(FileChooser.this) {
                public float get(Actor context) {
                    return (FileChooser.this.fileScrollPaneTable.getWidth() - ((float) FileItem.this.getUsedWidth())) - ((float) (file2.isDirectory() ? 35 : 60));
                }
            });
            add(this.size).expandX().right().padRight(6.0f);
            addListener();
        }

        /* access modifiers changed from: private */
        public int getUsedWidth() {
            int width = 0;
            Iterator<Cell> it = getCells().iterator();
            while (it.hasNext()) {
                Cell<?> cell = it.next();
                if (cell.getActor() != this.name) {
                    width = (int) (((float) width) + cell.getActor().getWidth());
                }
            }
            return width;
        }

        private void addListener() {
            addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if (event.getButton() == 1) {
                        FileChooser.this.fileMenu.build(FileChooser.this.favorites, FileItem.this.file);
                        FileChooser.this.fileMenu.showMenu(FileItem.this.getStage(), event.getStageX(), event.getStageY());
                    }
                }
            });
            addListener(new ClickListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (FileChooser.this.selectedShortcut != null) {
                        FileChooser.this.selectedShortcut.deselect();
                    }
                    if (!FileChooser.this.multiselectionEnabled || (!Gdx.input.isKeyPressed(FileChooser.this.multiselectKey) && !Gdx.input.isKeyPressed(FileChooser.this.groupMultiselectKey))) {
                        FileChooser.this.deselectAll();
                    }
                    boolean itemSelected = FileItem.this.select();
                    if (FileChooser.this.selectedItems.size > 1 && FileChooser.this.multiselectionEnabled && Gdx.input.isKeyPressed(FileChooser.this.groupMultiselectKey)) {
                        selectGroup();
                    }
                    if (FileChooser.this.selectedItems.size > 1) {
                        FileChooser.this.removeInvalidSelections();
                    }
                    FileChooser.this.setSelectedFileFieldText();
                    if (!itemSelected) {
                        return false;
                    }
                    return super.touchDown(event, x, y, pointer, button);
                }

                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    if (getTapCount() == 2 && FileChooser.this.selectedItems.contains(FileItem.this, true)) {
                        FileHandle file = FileItem.this.file;
                        if (file.isDirectory()) {
                            FileChooser.this.historyAdd(file);
                            FileChooser.this.setDirectory(file);
                            return;
                        }
                        FileChooser.this.selectionFinished();
                    }
                }

                private void selectGroup() {
                    int start;
                    int end;
                    Array<Cell> cells = FileChooser.this.fileTable.getCells();
                    int thisSelectionIndex = getItemId(cells, FileItem.this);
                    int lastSelectionIndex = getItemId(cells, (FileItem) FileChooser.this.selectedItems.get(FileChooser.this.selectedItems.size - 2));
                    if (thisSelectionIndex > lastSelectionIndex) {
                        start = lastSelectionIndex;
                        end = thisSelectionIndex;
                    } else {
                        start = thisSelectionIndex;
                        end = lastSelectionIndex;
                    }
                    for (int i = start; i < end; i++) {
                        boolean unused = ((FileItem) cells.get(i).getActor()).select(false);
                    }
                }

                private int getItemId(Array<Cell> cells, FileItem item) {
                    for (int i = 0; i < cells.size; i++) {
                        if (cells.get(i).getActor() == item) {
                            return i;
                        }
                    }
                    throw new IllegalStateException("Item not found in cells");
                }
            });
        }

        /* access modifiers changed from: private */
        public boolean select() {
            return select(true);
        }

        /* access modifiers changed from: private */
        public boolean select(boolean deselectIfAlreadySelected) {
            if (!deselectIfAlreadySelected || !FileChooser.this.selectedItems.contains(this, true)) {
                setBackground(FileChooser.HIGHLIGHT);
                if (FileChooser.this.selectedItems.contains(this, true)) {
                    return true;
                }
                FileChooser.this.selectedItems.add(this);
                return true;
            }
            deselect();
            return false;
        }

        /* access modifiers changed from: private */
        public void deselect() {
            deselect(true);
        }

        /* access modifiers changed from: private */
        public void deselect(boolean removeFromList) {
            setBackground((Drawable) null);
            if (removeFromList) {
                FileChooser.this.selectedItems.removeValue(this, true);
            }
        }
    }

    private class ShortcutItem extends Table {
        public File file;
        private VisLabel name;

        public ShortcutItem(File file2, String customName, Drawable icon) {
            this.file = file2;
            this.name = new VisLabel(customName);
            this.name.setEllipsis(true);
            add(new Image(icon)).padTop(3.0f);
            add(this.name).expand().fill().padRight(6.0f).width(new Value(FileChooser.this) {
                public float get(Actor context) {
                    return (FileChooser.this.shortcutsScrollPaneTable.getWidth() - ((float) ShortcutItem.this.getUsedWidth())) - 10.0f;
                }
            });
            addListener();
        }

        /* access modifiers changed from: private */
        public int getUsedWidth() {
            int width = 0;
            Iterator<Cell> it = getCells().iterator();
            while (it.hasNext()) {
                Cell<?> cell = it.next();
                if (cell.getActor() != this.name) {
                    width = (int) (((float) width) + cell.getActor().getWidth());
                }
            }
            return width;
        }

        private void addListener() {
            addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if (event.getButton() == 1) {
                        FileChooser.this.fileMenu.buildForFavorite(FileChooser.this.favorites, ShortcutItem.this.file);
                        FileChooser.this.fileMenu.showMenu(ShortcutItem.this.getStage(), event.getStageX(), event.getStageY());
                    }
                }
            });
            addListener(new ClickListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    FileChooser.this.deselectAll();
                    FileChooser.this.setSelectedFileFieldText();
                    ShortcutItem.this.select();
                    return super.touchDown(event, x, y, pointer, button);
                }

                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    if (getTapCount() == 1) {
                        File file = ShortcutItem.this.file;
                        if (!file.exists()) {
                            FileChooser.this.showDialog(FileChooser.this.getText(FileChooserText.POPUP_DIRECTORY_DOES_NOT_EXIST));
                        } else if (file.isDirectory()) {
                            FileChooser.this.setDirectory(file.getAbsolutePath());
                            ShortcutItem.this.getStage().setScrollFocus(FileChooser.this.fileScrollPane);
                        }
                    }
                }
            });
        }

        /* access modifiers changed from: private */
        public void select() {
            if (FileChooser.this.selectedShortcut != null) {
                FileChooser.this.selectedShortcut.deselect();
            }
            ShortcutItem unused = FileChooser.this.selectedShortcut = this;
            setBackground(FileChooser.HIGHLIGHT);
        }

        /* access modifiers changed from: private */
        public void deselect() {
            setBackground((Drawable) null);
        }
    }
}
