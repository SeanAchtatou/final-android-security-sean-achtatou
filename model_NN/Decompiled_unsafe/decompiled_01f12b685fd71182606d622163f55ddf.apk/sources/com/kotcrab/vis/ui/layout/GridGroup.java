package com.kotcrab.vis.ui.layout;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.SnapshotArray;
import com.kbz.esotericsoftware.spine.Animation;

public class GridGroup extends WidgetGroup {
    private int itemSize = 256;
    private float lastPrefHeight;
    private float prefHeight;
    private float prefWidth;
    private boolean sizeInvalid = true;
    private float spacing = 8.0f;

    public GridGroup() {
        setTouchable(Touchable.childrenOnly);
    }

    public GridGroup(int itemSize2) {
        this.itemSize = itemSize2;
    }

    public GridGroup(float spacing2) {
        this.spacing = spacing2;
    }

    public GridGroup(int itemSize2, float spacing2) {
        this.spacing = spacing2;
        this.itemSize = itemSize2;
    }

    private void computeSize() {
        float maxHeight;
        this.prefWidth = getWidth();
        this.prefHeight = Animation.CurveTimeline.LINEAR;
        this.sizeInvalid = false;
        if (getChildren().size == 0) {
            this.prefWidth = Animation.CurveTimeline.LINEAR;
            this.prefHeight = Animation.CurveTimeline.LINEAR;
            return;
        }
        float width = getWidth();
        float maxHeight2 = Animation.CurveTimeline.LINEAR;
        float tempX = this.spacing;
        int n = getChildren().size;
        for (int i = 0; i < n; i++) {
            if (((float) this.itemSize) + tempX + this.spacing >= width) {
                tempX = this.spacing;
                maxHeight2 += ((float) this.itemSize) + this.spacing;
            }
            tempX += ((float) this.itemSize) - this.spacing;
        }
        if (((float) this.itemSize) + (this.spacing * 2.0f) >= this.prefWidth) {
            maxHeight = maxHeight2 + this.spacing;
        } else {
            maxHeight = maxHeight2 + ((float) this.itemSize) + (this.spacing * 2.0f);
        }
        this.prefHeight = maxHeight;
    }

    public void layout() {
        float y;
        if (this.sizeInvalid) {
            computeSize();
            if (this.lastPrefHeight != this.prefHeight) {
                this.lastPrefHeight = this.prefHeight;
                invalidateHierarchy();
            }
        }
        SnapshotArray<Actor> children = getChildren();
        float width = getWidth();
        boolean notEnoughSpace = ((float) this.itemSize) + (this.spacing * 2.0f) > width;
        float x = this.spacing;
        if (notEnoughSpace) {
            y = getHeight();
        } else {
            y = (getHeight() - ((float) this.itemSize)) - this.spacing;
        }
        int n = children.size;
        for (int i = 0; i < n; i++) {
            Actor child = children.get(i);
            if (((float) this.itemSize) + x + this.spacing > width) {
                x = this.spacing;
                y -= ((float) this.itemSize) + this.spacing;
            }
            child.setBounds(x, y, (float) this.itemSize, (float) this.itemSize);
            x += ((float) this.itemSize) + this.spacing;
        }
    }

    public float getSpacing() {
        return this.spacing;
    }

    public void setSpacing(float spacing2) {
        this.spacing = spacing2;
        invalidateHierarchy();
    }

    public int getItemSize() {
        return this.itemSize;
    }

    public void setItemSize(int itemSize2) {
        this.itemSize = itemSize2;
        invalidateHierarchy();
    }

    public void invalidate() {
        super.invalidate();
        this.sizeInvalid = true;
    }

    public float getPrefWidth() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.prefWidth;
    }

    public float getPrefHeight() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.prefHeight;
    }
}
