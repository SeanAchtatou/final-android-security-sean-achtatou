package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.VisUI;

public class AlphaChannelBar extends ChannelBar {
    private static Drawable ALPHA_BAR = VisUI.getSkin().getDrawable("alpha-bar-10px");

    public AlphaChannelBar(Texture texture, int value, int maxValue, ChangeListener listener) {
        super(texture, value, maxValue, listener);
    }

    public void draw(Batch batch, float parentAlpha) {
        ALPHA_BAR.draw(batch, getImageX() + getX(), getImageY() + getY(), getScaleX() * getImageWidth(), getScaleY() * getImageHeight());
        super.draw(batch, parentAlpha);
    }
}
