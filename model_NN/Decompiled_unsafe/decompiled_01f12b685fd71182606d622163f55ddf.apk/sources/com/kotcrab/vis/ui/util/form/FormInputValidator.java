package com.kotcrab.vis.ui.util.form;

import com.kotcrab.vis.ui.InputValidator;

public abstract class FormInputValidator implements InputValidator {
    private String errorMsg;
    private boolean result;

    /* access modifiers changed from: protected */
    public abstract boolean validate(String str);

    public FormInputValidator(String errorMsg2) {
        this.errorMsg = errorMsg2;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    /* access modifiers changed from: protected */
    public boolean getLastResult() {
        return this.result;
    }

    public boolean validateInput(String input) {
        this.result = validate(input);
        return this.result;
    }
}
