package com.kotcrab.vis.ui.util.dialog;

import cn.egame.terminal.paysdk.EgamePay;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.InputValidator;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.i18n.BundleText;
import com.kotcrab.vis.ui.util.TableUtils;
import com.kotcrab.vis.ui.widget.VisDialog;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.kotcrab.vis.ui.widget.VisValidableTextField;
import com.kotcrab.vis.ui.widget.VisWindow;

public class DialogUtils {
    private static final int BUTTON_CANCEL = 0;
    private static final int BUTTON_DETAILS = 4;
    private static final int BUTTON_NO = 2;
    private static final int BUTTON_OK = 3;
    private static final int BUTTON_YES = 1;

    public enum OptionDialogType {
        YES_NO,
        YES_NO_CANCEL,
        YES_CANCEL
    }

    public static void showOKDialog(Stage stage, String title, String text) {
        VisDialog dialog = new VisDialog(title);
        dialog.text(text);
        dialog.button(get(Text.OK)).padBottom(3.0f);
        dialog.pack();
        dialog.centerWindow();
        stage.addActor(dialog.fadeIn());
    }

    public static OptionDialog showOptionDialog(Stage stage, String title, String text, OptionDialogType type, OptionDialogListener listener) {
        OptionDialog dialog = new OptionDialog(title, text, type, listener);
        stage.addActor(dialog.fadeIn());
        return dialog;
    }

    public static <T> ConfirmDialog<T> showConfirmDialog(Stage stage, String title, String text, String[] buttons, T[] returns, ConfirmDialogListener<T> listener) {
        ConfirmDialog<T> dialog = new ConfirmDialog<>(title, text, buttons, returns, listener);
        stage.addActor(dialog.fadeIn());
        return dialog;
    }

    public static void showInputDialog(Stage stage, String title, String fieldTitle, InputDialogListener listener) {
        stage.addActor(new InputDialog(title, fieldTitle, true, null, listener).fadeIn());
    }

    public static void showInputDialog(Stage stage, String title, String fieldTitle, InputValidator validator, InputDialogListener listener) {
        stage.addActor(new InputDialog(title, fieldTitle, true, validator, listener).fadeIn());
    }

    public static void showInputDialog(Stage stage, String title, String fieldTitle, boolean cancelable, InputDialogListener listener) {
        stage.addActor(new InputDialog(title, fieldTitle, cancelable, null, listener).fadeIn());
    }

    public static void showInputDialog(Stage stage, String title, String fieldTitle, boolean cancelable, InputValidator validator, InputDialogListener listener) {
        stage.addActor(new InputDialog(title, fieldTitle, cancelable, validator, listener).fadeIn());
    }

    public static void showErrorDialog(Stage stage, String text) {
        showErrorDialog(stage, text, (String) null);
    }

    public static void showErrorDialog(Stage stage, String text, Exception exception) {
        if (exception == null) {
            showErrorDialog(stage, text, (String) null);
        } else {
            showErrorDialog(stage, text, getStackTrace(exception));
        }
    }

    public static void showErrorDialog(Stage stage, String text, String details) {
        stage.addActor(new ErrorDialog(text, details).fadeIn());
    }

    /* access modifiers changed from: private */
    public static VisScrollPane createScrollPane(Actor widget) {
        VisScrollPane scrollPane = new VisScrollPane(widget);
        scrollPane.setOverscroll(false, true);
        scrollPane.setFadeScrollBars(false);
        return scrollPane;
    }

    private static String getStackTrace(Throwable throwable) {
        StringBuilder builder = new StringBuilder();
        String msg = throwable.getMessage();
        if (msg != null) {
            builder.append(msg);
            builder.append("\n\n");
        }
        for (StackTraceElement element : throwable.getStackTrace()) {
            builder.append(element);
            builder.append("\n");
        }
        return builder.toString();
    }

    public static class InputDialog extends VisWindow {
        private VisTextButton cancelButton;
        /* access modifiers changed from: private */
        public VisTextField field;
        /* access modifiers changed from: private */
        public InputDialogListener listener;
        /* access modifiers changed from: private */
        public VisTextButton okButton;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public InputDialog(String title, String fieldTitle, boolean cancelable, InputValidator validator, InputDialogListener listener2) {
            super(title);
            boolean z = true;
            this.listener = listener2;
            TableUtils.setSpacingDefaults(this);
            setModal(true);
            if (cancelable) {
                addCloseButton();
                closeOnEscape();
            }
            VisTable buttonsTable = new VisTable(true);
            VisTextButton visTextButton = new VisTextButton(DialogUtils.get(Text.CANCEL));
            this.cancelButton = visTextButton;
            buttonsTable.add(visTextButton);
            VisTextButton visTextButton2 = new VisTextButton(DialogUtils.get(Text.OK));
            this.okButton = visTextButton2;
            buttonsTable.add(visTextButton2);
            VisTable fieldTable = new VisTable(true);
            if (validator == null) {
                this.field = new VisTextField();
            } else {
                this.field = new VisValidableTextField(validator);
            }
            if (fieldTitle != null) {
                fieldTable.add(new VisLabel(fieldTitle));
            }
            fieldTable.add(this.field).expand().fill();
            add(fieldTable).padTop(3.0f).spaceBottom(4.0f);
            row();
            add(buttonsTable).padBottom(3.0f);
            addListeners();
            if (validator != null) {
                addValidableFieldListener(this.field);
                this.okButton.setDisabled(this.field.isInputValid() ? false : z);
            }
            pack();
            centerWindow();
            this.field.focusField();
        }

        /* access modifiers changed from: protected */
        public void close() {
            super.close();
            this.listener.canceled();
        }

        private void addValidableFieldListener(final VisTextField field2) {
            field2.addListener(new ChangeListener() {
                public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                    if (field2.isInputValid()) {
                        InputDialog.this.okButton.setDisabled(false);
                    } else {
                        InputDialog.this.okButton.setDisabled(true);
                    }
                }
            });
        }

        private void addListeners() {
            this.okButton.addListener(new ChangeListener() {
                public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                    InputDialog.this.listener.finished(InputDialog.this.field.getText());
                    InputDialog.this.fadeOut();
                }
            });
            this.cancelButton.addListener(new ChangeListener() {
                public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                    InputDialog.this.close();
                }
            });
            this.field.addListener(new InputListener() {
                public boolean keyDown(InputEvent event, int keycode) {
                    if (keycode == 66 && !InputDialog.this.okButton.isDisabled()) {
                        InputDialog.this.listener.finished(InputDialog.this.field.getText());
                        InputDialog.this.fadeOut();
                    }
                    return super.keyDown(event, keycode);
                }
            });
        }
    }

    public static class OptionDialog extends VisDialog {
        private VisTextButton cancelButton = new VisTextButton(DialogUtils.get(Text.CANCEL));
        private OptionDialogListener listener;
        private VisTextButton noButton = new VisTextButton(DialogUtils.get(Text.NO));
        private VisTextButton yesButton = new VisTextButton(DialogUtils.get(Text.YES));

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kotcrab.vis.ui.widget.VisDialog.button(com.badlogic.gdx.scenes.scene2d.ui.Button, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog
         arg types: [com.kotcrab.vis.ui.widget.VisTextButton, int]
         candidates:
          com.kotcrab.vis.ui.widget.VisDialog.button(java.lang.String, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog
          com.kotcrab.vis.ui.widget.VisDialog.button(com.badlogic.gdx.scenes.scene2d.ui.Button, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog */
        public OptionDialog(String title, String text, OptionDialogType type, OptionDialogListener listener2) {
            super(title);
            this.listener = listener2;
            text(text);
            defaults().padBottom(3.0f);
            switch (type) {
                case YES_NO:
                    button((Button) this.yesButton, (Object) 1);
                    button((Button) this.noButton, (Object) 2);
                    break;
                case YES_CANCEL:
                    button((Button) this.yesButton, (Object) 1);
                    button((Button) this.cancelButton, (Object) 0);
                    break;
                case YES_NO_CANCEL:
                    button((Button) this.yesButton, (Object) 1);
                    button((Button) this.noButton, (Object) 2);
                    button((Button) this.cancelButton, (Object) 0);
                    break;
            }
            pack();
            centerWindow();
        }

        /* access modifiers changed from: protected */
        public void result(Object object) {
            int result = ((Integer) object).intValue();
            if (result == 1) {
                this.listener.yes();
            }
            if (result == 2) {
                this.listener.no();
            }
            if (result == 0) {
                this.listener.cancel();
            }
        }

        public void setNoButtonText(String text) {
            this.noButton.setText(text);
        }

        public void setYesButtonText(String text) {
            this.yesButton.setText(text);
        }

        public void setCancelButtonText(String text) {
            this.cancelButton.setText(text);
        }
    }

    public static class ErrorDialog extends VisDialog {
        private Cell<?> detailsCell;
        private VisTable detailsTable = new VisTable(true);

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kotcrab.vis.ui.widget.VisDialog.button(java.lang.String, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog
         arg types: [java.lang.String, int]
         candidates:
          com.kotcrab.vis.ui.widget.VisDialog.button(com.badlogic.gdx.scenes.scene2d.ui.Button, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog
          com.kotcrab.vis.ui.widget.VisDialog.button(java.lang.String, java.lang.Object):com.kotcrab.vis.ui.widget.VisDialog */
        public ErrorDialog(String text, String stackTrace) {
            super(DialogUtils.get(Text.ERROR));
            text(text);
            if (stackTrace != null) {
                final VisTextButton copyButton = new VisTextButton(DialogUtils.get(Text.COPY));
                final VisLabel errorLabel = new VisLabel(stackTrace);
                copyButton.addListener(new ChangeListener() {
                    public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                        Gdx.app.getClipboard().setContents(errorLabel.getText().toString());
                        copyButton.setText(DialogUtils.get(Text.COPIED));
                    }
                });
                this.detailsTable.add(new VisLabel(DialogUtils.get(Text.DETAILS_COLON))).left().expand().padTop(6.0f);
                this.detailsTable.add(copyButton);
                this.detailsTable.row();
                VisTable errorTable = new VisTable();
                errorTable.add(errorLabel).top().expand().fillX();
                this.detailsTable.add(DialogUtils.createScrollPane(errorTable)).colspan(2).width(600.0f).height(300.0f);
                getContentTable().row();
                this.detailsCell = getContentTable().add(this.detailsTable);
                this.detailsCell.setActor(null);
                button(DialogUtils.get(Text.DETAILS), (Object) 4);
            }
            button(DialogUtils.get(Text.OK), (Object) 3).padBottom(3.0f);
            pack();
            centerWindow();
        }

        /* access modifiers changed from: protected */
        public void result(Object object) {
            if (((Integer) object).intValue() == 4) {
                this.detailsCell.setActor(this.detailsCell.hasActor() ? null : this.detailsTable);
                pack();
                centerWindow();
                cancel();
            }
        }
    }

    /* access modifiers changed from: private */
    public static String get(Text text) {
        return VisUI.getDialogUtilsBundle().get(text.getName());
    }

    public static class ConfirmDialog<T> extends VisDialog {
        private ConfirmDialogListener<T> listener;

        public ConfirmDialog(String title, String text, String[] buttons, T[] returns, ConfirmDialogListener<T> listener2) {
            super(title);
            this.listener = listener2;
            text(text);
            for (int i = 0; i < buttons.length; i++) {
                button(buttons[i], returns[i]);
            }
            padBottom(3.0f);
            pack();
            centerWindow();
        }

        /* access modifiers changed from: protected */
        public void result(Object object) {
            this.listener.result(object);
        }
    }

    private enum Text implements BundleText {
        YES {
            public String getName() {
                return "yes";
            }
        },
        NO {
            public String getName() {
                return "no";
            }
        },
        CANCEL {
            public String getName() {
                return EgamePay.PAY_PARAMS_KEY_MONTHLY_UNSUBSCRIBE_EVENT;
            }
        },
        OK {
            public String getName() {
                return "ok";
            }
        },
        ERROR {
            public String getName() {
                return "error";
            }
        },
        DETAILS {
            public String getName() {
                return "details";
            }
        },
        DETAILS_COLON {
            public String getName() {
                return "detailsColon";
            }
        },
        COPY {
            public String getName() {
                return "copy";
            }
        },
        COPIED {
            public String getName() {
                return "copied";
            }
        };

        public String get() {
            throw new UnsupportedOperationException();
        }

        public String format() {
            throw new UnsupportedOperationException();
        }

        public String format(Object... arguments) {
            throw new UnsupportedOperationException();
        }
    }
}
