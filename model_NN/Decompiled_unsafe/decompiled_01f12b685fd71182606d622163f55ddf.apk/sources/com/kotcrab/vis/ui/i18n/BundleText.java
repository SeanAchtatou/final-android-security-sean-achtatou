package com.kotcrab.vis.ui.i18n;

public interface BundleText {
    String format();

    String format(Object... objArr);

    String get();

    String getName();
}
