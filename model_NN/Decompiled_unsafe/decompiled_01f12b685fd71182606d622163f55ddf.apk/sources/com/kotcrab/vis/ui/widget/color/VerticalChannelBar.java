package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisImage;

public class VerticalChannelBar extends VisImage {
    private static final Drawable BAR_SELECTOR = VisUI.getSkin().getDrawable("color-picker-selector-vertical");
    private int maxValue;
    private float selectorY;
    private int value;

    public VerticalChannelBar(Texture texture, int value2, int maxValue2, ChangeListener listener) {
        super(texture);
        this.maxValue = maxValue2;
        setValue(value2);
        addListener(listener);
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                VerticalChannelBar.this.updateValueFromTouch(y);
                return true;
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                VerticalChannelBar.this.updateValueFromTouch(y);
            }
        });
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        BAR_SELECTOR.draw(batch, getX(), ((getY() + getImageY()) + this.selectorY) - 2.5f, getImageWidth(), BAR_SELECTOR.getMinHeight());
    }

    public void setValue(int newValue) {
        this.value = newValue;
        if (this.value < 0) {
            this.value = 0;
        }
        if (this.value > this.maxValue) {
            this.value = this.maxValue;
        }
        this.selectorY = (((float) this.value) / ((float) this.maxValue)) * 160.0f;
    }

    /* access modifiers changed from: private */
    public void updateValueFromTouch(float y) {
        setValue((int) ((y / 160.0f) * ((float) this.maxValue)));
        ChangeListener.ChangeEvent changeEvent = (ChangeListener.ChangeEvent) Pools.obtain(ChangeListener.ChangeEvent.class);
        fire(changeEvent);
        Pools.free(changeEvent);
    }

    public int getValue() {
        return this.value;
    }
}
