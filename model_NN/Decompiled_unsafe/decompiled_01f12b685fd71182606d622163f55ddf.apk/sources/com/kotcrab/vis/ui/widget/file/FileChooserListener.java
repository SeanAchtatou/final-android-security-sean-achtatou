package com.kotcrab.vis.ui.widget.file;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

public interface FileChooserListener {
    void canceled();

    void selected(FileHandle fileHandle);

    void selected(Array<FileHandle> array);
}
