package com.kotcrab.vis.ui.building.utilities;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class CellWidget<Widget extends Actor> {
    public static final CellWidget<?> EMPTY = empty();
    private static final int IGNORED_SIZE = 0;
    /* access modifiers changed from: private */
    public final Alignment alignment;
    /* access modifiers changed from: private */
    public final boolean expandX;
    /* access modifiers changed from: private */
    public final boolean expandY;
    /* access modifiers changed from: private */
    public final boolean fillX;
    /* access modifiers changed from: private */
    public final boolean fillY;
    /* access modifiers changed from: private */
    public final int height;
    /* access modifiers changed from: private */
    public final int minHeight;
    /* access modifiers changed from: private */
    public final int minWidth;
    /* access modifiers changed from: private */
    public final Padding padding;
    /* access modifiers changed from: private */
    public final boolean useSpacing;
    /* access modifiers changed from: private */
    public final Widget widget;
    /* access modifiers changed from: private */
    public final int width;

    private CellWidget(CellWidgetBuilder<Widget> cellWidgetBuilder) {
        this.widget = cellWidgetBuilder.widget;
        this.padding = cellWidgetBuilder.padding;
        this.expandX = cellWidgetBuilder.expandX;
        this.expandY = cellWidgetBuilder.expandY;
        this.fillX = cellWidgetBuilder.fillX;
        this.fillY = cellWidgetBuilder.fillY;
        this.useSpacing = cellWidgetBuilder.useSpacing;
        this.alignment = cellWidgetBuilder.alignment;
        this.width = cellWidgetBuilder.width;
        this.height = cellWidgetBuilder.height;
        this.minWidth = cellWidgetBuilder.minWidth;
        this.minHeight = cellWidgetBuilder.minHeight;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kotcrab.vis.ui.building.utilities.CellWidget.CellWidgetBuilder.<init>(com.badlogic.gdx.scenes.scene2d.Actor, com.kotcrab.vis.ui.building.utilities.CellWidget$1):void
     arg types: [Widget, ?[OBJECT, ARRAY]]
     candidates:
      com.kotcrab.vis.ui.building.utilities.CellWidget.CellWidgetBuilder.<init>(com.kotcrab.vis.ui.building.utilities.CellWidget, com.kotcrab.vis.ui.building.utilities.CellWidget$1):void
      com.kotcrab.vis.ui.building.utilities.CellWidget.CellWidgetBuilder.<init>(com.badlogic.gdx.scenes.scene2d.Actor, com.kotcrab.vis.ui.building.utilities.CellWidget$1):void */
    public static <Widget extends Actor> CellWidgetBuilder<Widget> of(Widget widget2) {
        return new CellWidgetBuilder<>((Actor) widget2);
    }

    public static <Widget extends Actor> CellWidgetBuilder<Widget> using(CellWidget<Widget> widget2) {
        return new CellWidgetBuilder<>();
    }

    public static <Widget extends Actor> CellWidget<Widget> wrap(Widget widget2) {
        return of(widget2).wrap();
    }

    public static CellWidget<?>[] wrap(Actor... widgets) {
        CellWidget<?>[] wrappedWidgets = new CellWidget[widgets.length];
        for (int index = 0; index < widgets.length; index++) {
            wrappedWidgets[index] = of(widgets[index]).wrap();
        }
        return wrappedWidgets;
    }

    public static CellWidget<?> empty() {
        return builder().wrap();
    }

    public static CellWidgetBuilder<Actor> builder() {
        return of(null);
    }

    public Widget getWidget() {
        return this.widget;
    }

    public Cell<?> buildCell(Table table) {
        return buildCell(table, null);
    }

    public Cell<?> buildCell(Table table, Padding defaultWidgetPadding) {
        Cell<?> cell = table.add((Actor) this.widget);
        applyPadding(cell, defaultWidgetPadding);
        applySizeData(cell);
        applyFillingData(cell);
        return cell;
    }

    private void applyPadding(Cell<?> cell, Padding defaultWidgetPadding) {
        Padding appliedPadding = (Padding) Nullables.getOrElse(this.padding, defaultWidgetPadding);
        if (appliedPadding == null) {
            return;
        }
        if (this.useSpacing) {
            appliedPadding.applySpacing(cell);
        } else {
            appliedPadding.applyPadding(cell);
        }
    }

    private void applySizeData(Cell<?> cell) {
        if (this.width > 0) {
            cell.width((float) this.width);
        }
        if (this.height > 0) {
            cell.height((float) this.height);
        }
        if (this.minWidth > 0) {
            cell.minWidth((float) this.minWidth);
        }
        if (this.minHeight > 0) {
            cell.minHeight((float) this.minHeight);
        }
    }

    private void applyFillingData(Cell<?> cell) {
        if (this.alignment != null) {
            this.alignment.apply(cell);
        }
        cell.expand(this.expandX, this.expandY);
        cell.fill(this.fillX, this.fillY);
    }

    public static class CellWidgetBuilder<Widget extends Actor> {
        /* access modifiers changed from: private */
        public Alignment alignment;
        /* access modifiers changed from: private */
        public boolean expandX;
        /* access modifiers changed from: private */
        public boolean expandY;
        /* access modifiers changed from: private */
        public boolean fillX;
        /* access modifiers changed from: private */
        public boolean fillY;
        /* access modifiers changed from: private */
        public int height;
        /* access modifiers changed from: private */
        public int minHeight;
        /* access modifiers changed from: private */
        public int minWidth;
        /* access modifiers changed from: private */
        public Padding padding;
        /* access modifiers changed from: private */
        public boolean useSpacing;
        /* access modifiers changed from: private */
        public Actor widget;
        /* access modifiers changed from: private */
        public int width;

        private CellWidgetBuilder(Actor widget2) {
            this.width = 0;
            this.height = 0;
            this.minWidth = 0;
            this.minHeight = 0;
            this.widget = widget2;
        }

        private CellWidgetBuilder(CellWidget<Widget> widget2) {
            this.width = 0;
            this.height = 0;
            this.minWidth = 0;
            this.minHeight = 0;
            this.widget = widget2.widget;
            this.padding = widget2.padding;
            this.expandX = widget2.expandX;
            this.expandY = widget2.expandY;
            this.fillX = widget2.fillX;
            this.fillY = widget2.fillY;
            this.useSpacing = widget2.useSpacing;
            this.alignment = widget2.alignment;
            this.width = widget2.width;
            this.height = widget2.height;
            this.minWidth = widget2.minWidth;
            this.minHeight = widget2.minHeight;
        }

        public CellWidget<Widget> wrap() {
            return new CellWidget<>(this);
        }

        public CellWidgetBuilder<Widget> widget(Widget widget2) {
            this.widget = widget2;
            return this;
        }

        public CellWidgetBuilder<Widget> padding(Padding padding2) {
            this.padding = padding2;
            return this;
        }

        public CellWidgetBuilder<Widget> useSpacing() {
            this.useSpacing = true;
            return this;
        }

        public CellWidgetBuilder<Widget> expandX() {
            this.expandX = true;
            return this;
        }

        public CellWidgetBuilder<Widget> expandY() {
            this.expandY = true;
            return this;
        }

        public CellWidgetBuilder<Widget> fillX() {
            this.fillX = true;
            return this;
        }

        public CellWidgetBuilder<Widget> fillY() {
            this.fillY = true;
            return this;
        }

        public CellWidgetBuilder<Widget> align(Alignment alignment2) {
            this.alignment = alignment2;
            return this;
        }

        public CellWidgetBuilder<Widget> width(int width2) {
            this.width = width2;
            return this;
        }

        public CellWidgetBuilder<Widget> height(int height2) {
            this.height = height2;
            return this;
        }

        public CellWidgetBuilder<Widget> minWidth(int minWidth2) {
            this.minWidth = minWidth2;
            return this;
        }

        public CellWidgetBuilder<Widget> minHeight(int minHeight2) {
            this.minHeight = minHeight2;
            return this;
        }
    }
}
