package com.kotcrab.vis.ui.util;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.VisUI;

public class TableUtils {
    public static void setSpacingDefaults(Table table) {
        table.defaults().spaceBottom((float) VisUI.getDefaultSpacingBottom());
        table.defaults().spaceRight((float) VisUI.getDefaultSpacingRight());
    }
}
