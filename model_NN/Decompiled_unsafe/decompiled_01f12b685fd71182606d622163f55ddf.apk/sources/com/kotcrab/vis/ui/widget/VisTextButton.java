package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.FocusManager;
import com.kotcrab.vis.ui.Focusable;
import com.kotcrab.vis.ui.VisUI;

public class VisTextButton extends TextButton implements Focusable {
    private boolean drawBorder;
    private boolean focusBorderEnabled = true;
    private VisTextButtonStyle style;

    public VisTextButton(String text, String styleName) {
        super(text, (TextButton.TextButtonStyle) VisUI.getSkin().get(styleName, VisTextButtonStyle.class));
        init();
    }

    public VisTextButton(String text) {
        super(text, (TextButton.TextButtonStyle) VisUI.getSkin().get(VisTextButtonStyle.class));
        init();
    }

    public VisTextButton(String text, ChangeListener listener) {
        super(text, (TextButton.TextButtonStyle) VisUI.getSkin().get(VisTextButtonStyle.class));
        init();
        addListener(listener);
    }

    public VisTextButton(String text, String styleName, ChangeListener listener) {
        super(text, (TextButton.TextButtonStyle) VisUI.getSkin().get(styleName, VisTextButtonStyle.class));
        init();
        addListener(listener);
    }

    public VisTextButton(String text, VisTextButtonStyle buttonStyle) {
        super(text, buttonStyle);
        init();
    }

    private void init() {
        this.style = (VisTextButtonStyle) getStyle();
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (VisTextButton.this.isDisabled()) {
                    return false;
                }
                FocusManager.getFocus(VisTextButton.this);
                return false;
            }
        });
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (this.focusBorderEnabled && this.drawBorder && this.style.focusBorder != null) {
            this.style.focusBorder.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
    }

    public static class VisTextButtonStyle extends TextButton.TextButtonStyle {
        public Drawable focusBorder;

        public VisTextButtonStyle() {
        }

        public VisTextButtonStyle(Drawable up, Drawable down, Drawable checked, BitmapFont font) {
            super(up, down, checked, font);
        }

        public VisTextButtonStyle(VisTextButtonStyle style) {
            super(style);
            this.focusBorder = style.focusBorder;
        }
    }

    public boolean isFocusBorderEnabled() {
        return this.focusBorderEnabled;
    }

    public void setFocusBorderEnabled(boolean focusBorderEnabled2) {
        this.focusBorderEnabled = focusBorderEnabled2;
    }

    public void focusLost() {
        this.drawBorder = false;
    }

    public void focusGained() {
        this.drawBorder = true;
    }
}
