package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.kbz.esotericsoftware.spine.Animation;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTextButton;

public class Menu extends PopupMenu {
    /* access modifiers changed from: private */
    public MenuBar menuBar;
    public VisTextButton openButton;
    private String title;

    public Menu(String title2) {
        this.title = title2;
        this.openButton = new VisTextButton(title2, new VisTextButton.VisTextButtonStyle((VisTextButton.VisTextButtonStyle) VisUI.getSkin().get("menu-bar", VisTextButton.VisTextButtonStyle.class)));
        this.openButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (Menu.this.menuBar.getCurrentMenu() == Menu.this) {
                    Menu.this.menuBar.closeMenu();
                } else {
                    Menu.this.switchMenu();
                    event.stop();
                }
                return true;
            }

            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (Menu.this.menuBar.getCurrentMenu() != null) {
                    Menu.this.switchMenu();
                }
            }
        });
    }

    public String getTitle() {
        return this.title;
    }

    /* access modifiers changed from: private */
    public void switchMenu() {
        this.menuBar.closeMenu();
        showMenu();
    }

    private void showMenu() {
        Vector2 pos = this.openButton.localToStageCoordinates(new Vector2(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
        setPosition(pos.x, pos.y - getHeight());
        this.openButton.getStage().addActor(this);
        this.menuBar.setCurrentMenu(this);
    }

    public boolean remove() {
        boolean result = super.remove();
        this.menuBar.setCurrentMenu(null);
        return result;
    }

    /* access modifiers changed from: package-private */
    public void setMenuBar(MenuBar menuBar2) {
        if (this.menuBar != null) {
            throw new IllegalStateException("Menu was already added to MenuBar");
        }
        this.menuBar = menuBar2;
    }

    /* access modifiers changed from: package-private */
    public TextButton getOpenButton() {
        return this.openButton;
    }
}
