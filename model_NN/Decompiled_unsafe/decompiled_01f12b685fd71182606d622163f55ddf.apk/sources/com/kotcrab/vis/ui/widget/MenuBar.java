package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.VisUI;
import java.util.Iterator;

public class MenuBar {
    private static final Drawable BUTTTON_DEFAULT = VisUI.getSkin().getDrawable("button");
    private Menu currentMenu;
    private Table mainTable;
    private Table menuItems;
    private Array<Menu> menus = new Array<>();

    public MenuBar() {
        Skin skin = VisUI.getSkin();
        this.menuItems = new VisTable();
        this.mainTable = new VisTable() {
            /* access modifiers changed from: protected */
            public void sizeChanged() {
                super.sizeChanged();
                MenuBar.this.closeMenu();
            }
        };
        this.mainTable.left();
        this.mainTable.add(this.menuItems);
        this.mainTable.setBackground(skin.getDrawable("menu-bg"));
    }

    public void addMenu(Menu menu) {
        this.menus.add(menu);
        menu.setMenuBar(this);
        this.menuItems.add(menu.getOpenButton());
    }

    public boolean removeMenu(Menu menu) {
        boolean removed = this.menus.removeValue(menu, true);
        if (removed) {
            menu.setMenuBar(null);
            this.menuItems.removeActor(menu.getOpenButton());
        }
        return removed;
    }

    public void insertMenu(int index, Menu menu) {
        this.menus.insert(index, menu);
        menu.setMenuBar(this);
        rebuild();
    }

    private void rebuild() {
        this.menuItems.clear();
        Iterator<Menu> it = this.menus.iterator();
        while (it.hasNext()) {
            this.menuItems.add(it.next().getOpenButton());
        }
    }

    public void closeMenu() {
        if (this.currentMenu != null) {
            deselectButton(this.currentMenu.getOpenButton());
            this.currentMenu.remove();
            this.currentMenu = null;
        }
    }

    /* access modifiers changed from: package-private */
    public Menu getCurrentMenu() {
        return this.currentMenu;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentMenu(Menu newMenu) {
        if (newMenu != null) {
            selectButton(newMenu.getOpenButton());
        }
        if (this.currentMenu != null) {
            deselectButton(this.currentMenu.getOpenButton());
        }
        this.currentMenu = newMenu;
    }

    public Table getTable() {
        return this.mainTable;
    }

    public void selectButton(TextButton button) {
        button.getStyle().up = button.getStyle().over;
    }

    public void deselectButton(TextButton button) {
        button.getStyle().up = BUTTTON_DEFAULT;
    }
}
