package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.FocusManager;
import com.kotcrab.vis.ui.Focusable;
import com.kotcrab.vis.ui.VisUI;

public class VisCheckBox extends TextButton implements Focusable {
    private Drawable checkboxImage;
    private boolean drawBorder;
    private Image image;
    private Cell imageCell;
    private VisCheckBoxStyle style;

    public VisCheckBox(String text) {
        this(text, (VisCheckBoxStyle) VisUI.getSkin().get(VisCheckBoxStyle.class));
    }

    public VisCheckBox(String text, boolean checked) {
        this(text, (VisCheckBoxStyle) VisUI.getSkin().get(VisCheckBoxStyle.class));
        setChecked(checked);
    }

    public VisCheckBox(String text, String styleName) {
        this(text, (VisCheckBoxStyle) VisUI.getSkin().get(styleName, VisCheckBoxStyle.class));
    }

    public VisCheckBox(String text, VisCheckBoxStyle style2) {
        super(text, style2);
        clearChildren();
        Image image2 = new Image(style2.checkboxOff);
        this.image = image2;
        this.imageCell = add(image2);
        Label label = getLabel();
        add(label).padLeft(5.0f);
        label.setAlignment(8);
        setSize(getPrefWidth(), getPrefHeight());
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (VisCheckBox.this.isDisabled()) {
                    return false;
                }
                FocusManager.getFocus(VisCheckBox.this);
                return false;
            }
        });
    }

    public CheckBox.CheckBoxStyle getStyle() {
        return this.style;
    }

    public void setStyle(Button.ButtonStyle style2) {
        if (!(style2 instanceof VisCheckBoxStyle)) {
            throw new IllegalArgumentException("style must be a VisCheckBoxStyle.");
        }
        super.setStyle(style2);
        this.style = (VisCheckBoxStyle) style2;
    }

    public void draw(Batch batch, float parentAlpha) {
        this.image.setDrawable(getCheckboxImage());
        super.draw(batch, parentAlpha);
        if (this.drawBorder && this.style.focusBorder != null) {
            this.style.focusBorder.draw(batch, getX(), this.image.getY() + getY(), this.image.getWidth(), this.image.getHeight());
        }
    }

    public Image getImage() {
        return this.image;
    }

    public Cell getImageCell() {
        return this.imageCell;
    }

    public void focusLost() {
        this.drawBorder = false;
    }

    public void focusGained() {
        this.drawBorder = true;
    }

    /* access modifiers changed from: protected */
    public Drawable getCheckboxImage() {
        if (isDisabled()) {
            if (isChecked()) {
                return this.style.checkboxOnDisabled;
            }
            return this.style.checkboxOffDisabled;
        } else if (isPressed()) {
            if (isChecked()) {
                return this.style.checkboxOnDown;
            }
            return this.style.checkboxOffDown;
        } else if (isChecked()) {
            if (isOver()) {
                return this.style.checkboxOnOver;
            }
            return this.style.checkboxOn;
        } else if (isOver()) {
            return this.style.checkboxOver;
        } else {
            return this.style.checkboxOff;
        }
    }

    public static class VisCheckBoxStyle extends CheckBox.CheckBoxStyle {
        public Drawable checkboxOffDown;
        public Drawable checkboxOnDown;
        public Drawable checkboxOnOver;
        public Drawable focusBorder;

        public VisCheckBoxStyle() {
        }

        public VisCheckBoxStyle(Drawable checkboxOff, Drawable checkboxOn, BitmapFont font, Color fontColor) {
            super(checkboxOff, checkboxOn, font, fontColor);
        }

        public VisCheckBoxStyle(VisCheckBoxStyle style) {
            super(style);
            this.focusBorder = style.focusBorder;
            this.checkboxOnOver = style.checkboxOnOver;
            this.checkboxOnDown = style.checkboxOnDown;
            this.checkboxOffDown = style.checkboxOffDown;
        }
    }
}
