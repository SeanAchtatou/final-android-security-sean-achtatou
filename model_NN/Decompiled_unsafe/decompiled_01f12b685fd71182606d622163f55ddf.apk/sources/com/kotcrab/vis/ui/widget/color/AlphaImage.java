package com.kotcrab.vis.ui.widget.color;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.VisUI;

public class AlphaImage extends Image {
    private static final Drawable ALPHA_BAR = VisUI.getSkin().getDrawable("alpha-bar-25px");
    private static final Drawable ALPHA_BAR_SHIFTED = VisUI.getSkin().getDrawable("alpha-bar-25px-shifted");
    private Drawable alphaDrawable;
    private boolean shiftAlpha;

    public AlphaImage(Drawable imageUp) {
        super(imageUp);
        setAlphaDrawable();
    }

    public AlphaImage(Drawable imageUp, boolean shiftAlpha2) {
        super(imageUp);
        this.shiftAlpha = shiftAlpha2;
        setAlphaDrawable();
    }

    private void setAlphaDrawable() {
        this.alphaDrawable = this.shiftAlpha ? ALPHA_BAR_SHIFTED : ALPHA_BAR;
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(1.0f, 1.0f, 1.0f, parentAlpha);
        this.alphaDrawable.draw(batch, getImageX() + getX(), getImageY() + getY(), getScaleX() * getImageWidth(), getScaleY() * getImageHeight());
        super.draw(batch, parentAlpha);
    }
}
