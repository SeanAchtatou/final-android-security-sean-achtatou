package com.kotcrab.vis.ui.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;
import com.kotcrab.vis.ui.FocusManager;
import com.kotcrab.vis.ui.Focusable;
import com.kotcrab.vis.ui.VisUI;

public class VisImageButton extends Button implements Focusable {
    private boolean drawBorder;
    private boolean generateDisabledImage;
    private Image image;
    private VisImageButtonStyle style;

    public VisImageButton(Drawable imageUp) {
        this(imageUp, null, null);
    }

    public VisImageButton(Drawable imageUp, String tooltipText) {
        this(imageUp, null, null);
        new Tooltip(this, tooltipText);
    }

    public VisImageButton(Drawable imageUp, Drawable imageDown) {
        this(imageUp, imageDown, null);
    }

    public VisImageButton(Drawable imageUp, Drawable imageDown, Drawable imageChecked) {
        super(new VisImageButtonStyle((VisImageButtonStyle) VisUI.getSkin().get(VisImageButtonStyle.class)));
        this.generateDisabledImage = false;
        this.style.imageUp = imageUp;
        this.style.imageDown = imageDown;
        this.style.imageChecked = imageChecked;
        init();
    }

    public VisImageButton(String styleName) {
        super(new VisImageButtonStyle((VisImageButtonStyle) VisUI.getSkin().get(styleName, VisImageButtonStyle.class)));
        this.generateDisabledImage = false;
        init();
    }

    private void init() {
        this.image = new Image();
        this.image.setScaling(Scaling.fit);
        add(this.image);
        setSize(getPrefWidth(), getPrefHeight());
        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (VisImageButton.this.isDisabled()) {
                    return false;
                }
                FocusManager.getFocus(VisImageButton.this);
                return false;
            }
        });
        updateImage();
    }

    public void setGenerateDisabledImage(boolean generate) {
        this.generateDisabledImage = generate;
    }

    public VisImageButtonStyle getStyle() {
        return this.style;
    }

    public void setStyle(Button.ButtonStyle style2) {
        if (!(style2 instanceof VisImageButtonStyle)) {
            throw new IllegalArgumentException("style must be an ImageButtonStyle.");
        }
        super.setStyle(style2);
        this.style = (VisImageButtonStyle) style2;
        if (this.image != null) {
            updateImage();
        }
    }

    private void updateImage() {
        Drawable drawable = null;
        if (isDisabled() && this.style.imageDisabled != null) {
            drawable = this.style.imageDisabled;
        } else if (isPressed() && this.style.imageDown != null) {
            drawable = this.style.imageDown;
        } else if (isChecked() && this.style.imageChecked != null) {
            drawable = (this.style.imageCheckedOver == null || !isOver()) ? this.style.imageChecked : this.style.imageCheckedOver;
        } else if (isOver() && this.style.imageOver != null) {
            drawable = this.style.imageOver;
        } else if (this.style.imageUp != null) {
            drawable = this.style.imageUp;
        }
        this.image.setDrawable(drawable);
        if (!this.generateDisabledImage || this.style.imageDisabled != null || !isDisabled()) {
            this.image.setColor(Color.WHITE);
        } else {
            this.image.setColor(Color.GRAY);
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        updateImage();
        super.draw(batch, parentAlpha);
        if (this.drawBorder && this.style.focusBorder != null) {
            this.style.focusBorder.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
    }

    public Image getImage() {
        return this.image;
    }

    public Cell<?> getImageCell() {
        return getCell(this.image);
    }

    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        if (disabled) {
            FocusManager.getFocus();
        }
    }

    public void focusLost() {
        this.drawBorder = false;
    }

    public void focusGained() {
        this.drawBorder = true;
    }

    public static class VisImageButtonStyle extends Button.ButtonStyle {
        public Drawable focusBorder;
        public Drawable imageChecked;
        public Drawable imageCheckedOver;
        public Drawable imageDisabled;
        public Drawable imageDown;
        public Drawable imageOver;
        public Drawable imageUp;

        public VisImageButtonStyle() {
        }

        public VisImageButtonStyle(Drawable up, Drawable down, Drawable checked, Drawable imageUp2, Drawable imageDown2, Drawable imageChecked2) {
            super(up, down, checked);
            this.imageUp = imageUp2;
            this.imageDown = imageDown2;
            this.imageChecked = imageChecked2;
        }

        public VisImageButtonStyle(VisImageButtonStyle style) {
            super(style);
            this.imageUp = style.imageUp;
            this.imageDown = style.imageDown;
            this.imageOver = style.imageOver;
            this.imageChecked = style.imageChecked;
            this.imageCheckedOver = style.imageCheckedOver;
            this.imageDisabled = style.imageDisabled;
            this.focusBorder = style.focusBorder;
        }
    }
}
