package com.xiaomi.game.plugin.stat.b;

import android.os.Build;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import com.xiaomi.game.plugin.stat.MiGamePluginStatConfig;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class a {
    private static a c = null;
    private ExecutorService a = Executors.newSingleThreadExecutor();
    private b b;
    private String d;
    /* access modifiers changed from: private */
    public MiGamePluginStatConfig e;

    private a() {
    }

    public static a a() {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        }
        return c;
    }

    private boolean b() {
        if (com.xiaomi.game.plugin.stat.c.a.a(this.e.a())) {
            return false;
        }
        if (d.d(this.e.a())) {
            com.xiaomi.game.plugin.stat.c.a.a("Today has uploaded the init. Return don't upload init again! ");
            return true;
        }
        this.a.execute(new Runnable() {
            public void run() {
                try {
                    TreeMap treeMap = new TreeMap();
                    treeMap.put("app_id", a.this.e.b());
                    treeMap.put("app_key", a.this.e.c());
                    treeMap.put("device_id", d.a(a.this.e.a()));
                    treeMap.put("channel", a.this.e.e());
                    treeMap.put("version", a.this.e.d());
                    treeMap.put("stat_value", d.a());
                    boolean b = c.b(a.this.e.a(), com.xiaomi.game.plugin.stat.c.a.a(treeMap));
                    if (b) {
                        d.e(a.this.e.a());
                    }
                    com.xiaomi.game.plugin.stat.c.a.a("upload the init: " + b);
                } catch (Throwable th) {
                    com.xiaomi.game.plugin.stat.c.a.a("Upload MiStat data failed:" + th.getMessage());
                    if (com.xiaomi.game.plugin.stat.c.a.a()) {
                        th.printStackTrace();
                    }
                }
            }
        });
        return true;
    }

    public void a(MiGamePluginStatConfig miGamePluginStatConfig) {
        this.d = String.valueOf(System.currentTimeMillis());
        this.e = miGamePluginStatConfig;
        this.a.execute(new Runnable() {
            public void run() {
                d.a(a.this.e.a());
            }
        });
        this.b = new b(this.e.a(), this.e.f());
        b();
        final String c2 = d.c(this.e.a());
        this.a.execute(new Runnable() {
            public void run() {
                a.this.a(c2);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        if (com.xiaomi.game.plugin.stat.c.a.a(this.e.a())) {
            return false;
        }
        if (!d.f(this.e.a())) {
            com.xiaomi.game.plugin.stat.c.a.a("has uploaded crash in 1 minute. Don't upload crash in 1 min. ");
            return false;
        }
        try {
            if (!TextUtils.isEmpty(str)) {
                com.xiaomi.game.plugin.stat.c.a.a("upload  crash: " + str);
                if (c.a(this.e.a(), str)) {
                    com.xiaomi.game.plugin.stat.c.a.a("upload  crash success.");
                    d.a(this.e.a(), null);
                } else {
                    com.xiaomi.game.plugin.stat.c.a.a("upload  crash failed.");
                }
                d.g(this.e.a());
            } else {
                com.xiaomi.game.plugin.stat.c.a.a("no  crash .");
            }
        } catch (Throwable th) {
            if (com.xiaomi.game.plugin.stat.c.a.a()) {
                th.printStackTrace();
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Throwable th) {
        if (th == null) {
            return false;
        }
        com.xiaomi.game.plugin.stat.c.a.a("begin uploadCrash: " + th);
        String stackTraceString = Log.getStackTraceString(th);
        if (TextUtils.isEmpty(stackTraceString)) {
            com.xiaomi.game.plugin.stat.c.a.a("callstack cannot be empty!");
            return false;
        }
        try {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                com.xiaomi.game.plugin.stat.c.a.a("目前是主线程...,需要做些额外处理才能及时搜集crash");
                if (Build.VERSION.SDK_INT >= 9) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().build());
                }
            } else {
                com.xiaomi.game.plugin.stat.c.a.a("目前是非UI线程...,不需要做些额外处理也能及时搜集crash");
            }
            TreeMap treeMap = new TreeMap();
            treeMap.put("app_id", this.e.b());
            treeMap.put("app_key", this.e.c());
            treeMap.put("device_uuid", d.a(this.e.a()));
            treeMap.put("device_os", "Android " + Build.VERSION.SDK_INT);
            treeMap.put("device_model", Build.MODEL);
            treeMap.put("app_version", this.e.d());
            treeMap.put("app_channel", this.e.e());
            treeMap.put("app_start_time", this.d);
            treeMap.put("app_crash_time", String.valueOf(System.currentTimeMillis()));
            treeMap.put("crash_exception_type", th.getClass().getName() + ":" + th.getMessage());
            treeMap.put("crash_exception_desc", th instanceof OutOfMemoryError ? "OutOfMemoryError" : stackTraceString);
            treeMap.put("crash_callstack", stackTraceString);
            String a2 = com.xiaomi.game.plugin.stat.c.a.a(treeMap);
            d.a(this.e.a(), a2);
            a(a2);
        } catch (Throwable th2) {
            com.xiaomi.game.plugin.stat.c.a.a("Error to upload the exception " + th2.getMessage());
            if (com.xiaomi.game.plugin.stat.c.a.a()) {
                th2.printStackTrace();
            }
        }
        return true;
    }
}
