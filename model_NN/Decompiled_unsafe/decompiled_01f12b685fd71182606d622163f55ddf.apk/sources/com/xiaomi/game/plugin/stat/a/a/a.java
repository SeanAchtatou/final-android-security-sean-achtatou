package com.xiaomi.game.plugin.stat.a.a;

import android.accessibilityservice.AccessibilityService;
import android.view.accessibility.AccessibilityEvent;

public class a extends AccessibilityService {
    private static String a;
    private static a b = null;

    public static a a() {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    b = new a();
                }
            }
        }
        return b;
    }

    public String b() {
        return a;
    }

    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32) {
            try {
                a = accessibilityEvent.getPackageName().toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onInterrupt() {
    }
}
