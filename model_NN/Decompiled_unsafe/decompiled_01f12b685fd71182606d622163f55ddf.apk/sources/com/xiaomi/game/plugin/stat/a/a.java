package com.xiaomi.game.plugin.stat.a;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import com.xiaomi.a.a.a.a;
import com.xiaomi.game.plugin.stat.MiGamePluginStatConfig;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;

public class a {
    private ExecutorService a = Executors.newSingleThreadExecutor();
    private Timer b = null;
    /* access modifiers changed from: private */
    public long c;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public com.xiaomi.a.a.a.a e;
    /* access modifiers changed from: private */
    public MiGamePluginStatConfig f;
    /* access modifiers changed from: private */
    public Object g = new Object();
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public ServiceConnection i = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.xiaomi.a.a.a.a unused = a.this.e = a.C0001a.a(iBinder);
            com.xiaomi.game.plugin.stat.c.a.a("aidl onServiceConnected");
            synchronized (a.this.g) {
                a.this.g.notifyAll();
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            com.xiaomi.a.a.a.a unused = a.this.e = (com.xiaomi.a.a.a.a) null;
            com.xiaomi.game.plugin.stat.c.a.a("aidl onServiceDisconnected");
        }
    };

    /* renamed from: com.xiaomi.game.plugin.stat.a.a$a  reason: collision with other inner class name */
    class C0003a extends TimerTask {
        C0003a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.xiaomi.game.plugin.stat.a.a.a(com.xiaomi.game.plugin.stat.a.a, boolean):boolean
         arg types: [com.xiaomi.game.plugin.stat.a.a, int]
         candidates:
          com.xiaomi.game.plugin.stat.a.a.a(com.xiaomi.game.plugin.stat.a.a, long):long
          com.xiaomi.game.plugin.stat.a.a.a(com.xiaomi.game.plugin.stat.a.a, com.xiaomi.a.a.a.a):com.xiaomi.a.a.a.a
          com.xiaomi.game.plugin.stat.a.a.a(com.xiaomi.game.plugin.stat.a.a, java.lang.String):java.lang.String
          com.xiaomi.game.plugin.stat.a.a.a(com.xiaomi.game.plugin.stat.a.a, boolean):boolean */
        public void run() {
            if (TextUtils.isEmpty(a.this.h)) {
                String unused = a.this.h = a.this.e();
            }
            String a2 = a.this.a();
            if (TextUtils.isEmpty(a2)) {
                return;
            }
            if (!a.this.d && a.this.h.equals(a2)) {
                long unused2 = a.this.c = System.currentTimeMillis();
                boolean unused3 = a.this.d = true;
            } else if (a.this.d && !a.this.h.equals(a2)) {
                long currentTimeMillis = System.currentTimeMillis() - a.this.c;
                long unused4 = a.this.c = System.currentTimeMillis();
                boolean unused5 = a.this.d = false;
                a.this.a(currentTimeMillis);
            }
        }
    }

    public a(MiGamePluginStatConfig miGamePluginStatConfig) {
        this.f = miGamePluginStatConfig;
        if (this.f.g()) {
            b();
        }
        c();
        d();
    }

    private void b() {
        if (this.f != null) {
            String packageName = this.f.a().getPackageName();
            if (!TextUtils.isEmpty(packageName) && !TextUtils.equals(packageName, "com.xiaomi.gamecenter") && !com.xiaomi.game.plugin.stat.c.a.a(this.f.a())) {
                this.a.execute(new Runnable() {
                    public void run() {
                        StringBuilder sb;
                        try {
                            HashMap hashMap = new HashMap();
                            hashMap.put("ac", "bid522");
                            hashMap.put("ver", b.e(a.this.f.a()));
                            hashMap.put("appid", a.this.f.b());
                            hashMap.put("imei", b.a(a.this.f.a()));
                            hashMap.put("channelId", a.this.f.e());
                            hashMap.put("ua", b.a());
                            hashMap.put("jarver", "2.1");
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("packageName", a.this.f.a().getPackageName());
                            jSONObject.put("from", a.this.f.e());
                            jSONObject.put("fromver", a.this.f.d());
                            jSONObject.put("minijar", "minijar");
                            jSONObject.put("minijarver", "2.1");
                            hashMap.put("ext", jSONObject.toString());
                            sb = new StringBuilder();
                            for (Map.Entry entry : hashMap.entrySet()) {
                                if (sb.length() > 0) {
                                    sb.append("&");
                                }
                                if (!TextUtils.isEmpty((CharSequence) entry.getValue())) {
                                    sb.append((String) entry.getKey());
                                    sb.append("=");
                                    sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                                }
                            }
                            com.xiaomi.game.plugin.stat.c.a.a("sendMiGameDAU statusCode:" + ((HttpURLConnection) new URL("https://data.game.xiaomi.com/1px.gif?" + sb.toString()).openConnection()).getResponseCode());
                        } catch (Exception e) {
                            sb.append("");
                            e.printStackTrace();
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private synchronized void c() {
        this.a.execute(new Runnable() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r0 = r0.f     // Catch:{ Throwable -> 0x00e3 }
                    android.content.Context r0 = r0.a()     // Catch:{ Throwable -> 0x00e3 }
                    java.lang.String r1 = r0.getPackageName()     // Catch:{ Throwable -> 0x00e3 }
                    boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x00e3 }
                    if (r0 != 0) goto L_0x001c
                    java.lang.String r0 = "com.xiaomi.gamecenter"
                    boolean r0 = android.text.TextUtils.equals(r1, r0)     // Catch:{ Throwable -> 0x00e3 }
                    if (r0 == 0) goto L_0x001d
                L_0x001c:
                    return
                L_0x001d:
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.a.a.a.a r0 = r0.e     // Catch:{ Throwable -> 0x00e3 }
                    if (r0 != 0) goto L_0x001c
                    android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x00e3 }
                    java.lang.String r2 = "com.xiaomi.gamecenter.for3thd.migame.IMiGamePluginStat"
                    r0.<init>(r2)     // Catch:{ Throwable -> 0x00e3 }
                    java.lang.String r2 = "com.xiaomi.gamecenter"
                    r0.setPackage(r2)     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.a.a r2 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r2 = r2.f     // Catch:{ Throwable -> 0x00e3 }
                    android.content.Context r2 = r2.a()     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.a.a r3 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    android.content.ServiceConnection r3 = r3.i     // Catch:{ Throwable -> 0x00e3 }
                    r4 = 1
                    r2.bindService(r0, r3, r4)     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    java.lang.Object r2 = r0.g     // Catch:{ Throwable -> 0x00e3 }
                    monitor-enter(r2)     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ InterruptedException -> 0x00e9 }
                    java.lang.Object r0 = r0.g     // Catch:{ InterruptedException -> 0x00e9 }
                    r4 = 5000(0x1388, double:2.4703E-320)
                    r0.wait(r4)     // Catch:{ InterruptedException -> 0x00e9 }
                L_0x0057:
                    monitor-exit(r2)     // Catch:{ all -> 0x00ef }
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.a.a.a.a r0 = r0.e     // Catch:{ Throwable -> 0x00e3 }
                    if (r0 == 0) goto L_0x001c
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.a.a.a.a r0 = r0.e     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.a.a r2 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r2 = r2.f     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r2 = r2.b()     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r3 = "migame_init"
                    com.xiaomi.game.plugin.stat.a.a r4 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r4 = r4.f     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r4 = r4.e()     // Catch:{ Throwable -> 0x00f2 }
                    boolean r0 = r0.a(r2, r1, r3, r4)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f2 }
                    r2.<init>()     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r3 = "AIDL_TEST:res:"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r2 = "  "
                    java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r1 = "   "
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.a.a r1 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r1 = r1.f     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r1 = r1.b()     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r1 = "   "
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.a.a r1 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r1 = r1.f     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r1 = r1.e()     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00f2 }
                    java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00f2 }
                    com.xiaomi.game.plugin.stat.c.a.a(r0)     // Catch:{ Throwable -> 0x00f2 }
                L_0x00c8:
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.MiGamePluginStatConfig r0 = r0.f     // Catch:{ Throwable -> 0x00e3 }
                    android.content.Context r0 = r0.a()     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.a.a r1 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    android.content.ServiceConnection r1 = r1.i     // Catch:{ Throwable -> 0x00e3 }
                    r0.unbindService(r1)     // Catch:{ Throwable -> 0x00e3 }
                    com.xiaomi.game.plugin.stat.a.a r0 = com.xiaomi.game.plugin.stat.a.a.this     // Catch:{ Throwable -> 0x00e3 }
                    r1 = 0
                    com.xiaomi.a.a.a.a unused = r0.e = r1     // Catch:{ Throwable -> 0x00e3 }
                    goto L_0x001c
                L_0x00e3:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x001c
                L_0x00e9:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x00ef }
                    goto L_0x0057
                L_0x00ef:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch:{ all -> 0x00ef }
                    throw r0     // Catch:{ Throwable -> 0x00e3 }
                L_0x00f2:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ Throwable -> 0x00e3 }
                    goto L_0x00c8
                */
                throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.game.plugin.stat.a.a.AnonymousClass3.run():void");
            }
        });
    }

    private void d() {
        String packageName = this.f.a().getPackageName();
        if (!TextUtils.isEmpty(packageName) && !TextUtils.equals(packageName, "com.xiaomi.gamecenter") && this.b == null) {
            this.c = System.currentTimeMillis();
            this.b = new Timer();
            this.b.schedule(new C0003a(), 0, 1000);
            this.b.purge();
        }
    }

    /* access modifiers changed from: private */
    public String e() {
        return this.f.a().getPackageName();
    }

    private String f() {
        List<ActivityManager.RunningTaskInfo> runningTasks;
        try {
            if (!(this.f.a() == null || (runningTasks = ((ActivityManager) this.f.a().getSystemService("activity")).getRunningTasks(1)) == null || runningTasks.size() <= 0)) {
                return runningTasks.get(0).topActivity.getPackageName();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private static String g() {
        return com.xiaomi.game.plugin.stat.a.a.a.a().b();
    }

    public String a() {
        return (com.xiaomi.game.plugin.stat.c.a.b(this.f.a().getApplicationContext()) || Build.VERSION.SDK_INT < 21) ? f() : g();
    }

    public void a(final long j) {
        if (this.f != null && !com.xiaomi.game.plugin.stat.c.a.a(this.f.a())) {
            this.a.execute(new Runnable() {
                public void run() {
                    StringBuilder sb;
                    try {
                        HashMap hashMap = new HashMap();
                        hashMap.put("ac", "xmsdk");
                        hashMap.put("type", "minijargameforeground");
                        hashMap.put("ver", b.e(a.this.f.a()));
                        hashMap.put("appid", a.this.f.b());
                        String a2 = b.a(a.this.f.a());
                        hashMap.put("imei", a2);
                        b.a(hashMap, a2, a.this.f.b());
                        hashMap.put("CID", a.this.f.e());
                        hashMap.put("ua", b.a());
                        hashMap.put("time", String.valueOf(j));
                        hashMap.put("jarver", "2.1");
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("packageName", a.this.f.a().getPackageName());
                        jSONObject.put("from", "miadgamesdk");
                        hashMap.put("ext", jSONObject.toString());
                        sb = new StringBuilder();
                        for (Map.Entry entry : hashMap.entrySet()) {
                            if (sb.length() > 0) {
                                sb.append("&");
                            }
                            if (!TextUtils.isEmpty((CharSequence) entry.getValue())) {
                                sb.append((String) entry.getKey());
                                sb.append("=");
                                sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                            }
                        }
                        com.xiaomi.game.plugin.stat.c.a.a("sendGameForegroundTime statusCode:" + ((HttpURLConnection) new URL("https://data.game.xiaomi.com/1px.gif?" + sb.toString()).openConnection()).getResponseCode() + ",time:" + j);
                    } catch (Exception e) {
                        sb.append("");
                        e.printStackTrace();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            });
        }
    }
}
