package com.xiaomi.game.plugin.stat.c;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Random;

public class a {
    private static boolean a;

    public static int a(int i) {
        return new Random().nextInt(i) + 1;
    }

    public static String a(Map<String, String> map) {
        if (map == null || map.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (Map.Entry next : map.entrySet()) {
            if (!(next.getKey() == null || next.getValue() == null)) {
                try {
                    stringBuffer.append(URLEncoder.encode((String) next.getKey(), "UTF-8"));
                    stringBuffer.append("=");
                    stringBuffer.append(URLEncoder.encode((String) next.getValue(), "UTF-8"));
                    stringBuffer.append("&");
                } catch (UnsupportedEncodingException e) {
                    return null;
                }
            }
        }
        return (stringBuffer.length() > 0 ? stringBuffer.deleteCharAt(stringBuffer.length() - 1) : stringBuffer).toString();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(byte[] r5) {
        /*
            if (r5 == 0) goto L_0x003c
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0.update(r5)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            byte[] r3 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0 = 0
            r1 = r0
        L_0x0016:
            int r0 = r3.length     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r1 >= r0) goto L_0x0033
            byte r0 = r3[r1]     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r0 >= 0) goto L_0x001f
            int r0 = r0 + 256
        L_0x001f:
            r4 = 16
            if (r0 >= r4) goto L_0x0028
            java.lang.String r4 = "0"
            r2.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x0028:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0016
        L_0x0033:
            java.lang.String r0 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x0037:
            return r0
        L_0x0038:
            r0 = move-exception
            r0.printStackTrace()
        L_0x003c:
            r0 = 0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.game.plugin.stat.c.a.a(byte[]):java.lang.String");
    }

    public static void a(String str) {
        if (a && !TextUtils.isEmpty(str)) {
            Log.e("MiGamePluginStat", str);
        }
    }

    public static void a(boolean z) {
        a = z;
    }

    public static boolean a() {
        return a;
    }

    public static boolean a(Context context) {
        try {
            if (b(context)) {
                return ((Boolean) Class.forName("miui.os.Build").getField("IS_CTA_BUILD").get(null)).booleanValue();
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean b(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.xiaomi.xmsf", 0);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
