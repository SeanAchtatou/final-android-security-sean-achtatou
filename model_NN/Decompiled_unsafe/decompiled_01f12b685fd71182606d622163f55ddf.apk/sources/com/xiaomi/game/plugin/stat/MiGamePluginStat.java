package com.xiaomi.game.plugin.stat;

import android.app.Application;
import android.text.TextUtils;
import com.xiaomi.game.plugin.stat.c.a;

public final class MiGamePluginStat {
    private static MiGamePluginStat a;

    private MiGamePluginStat() {
    }

    public static MiGamePluginStat getsInstance() {
        if (a == null) {
            synchronized (MiGamePluginStat.class) {
                if (a == null) {
                    a = new MiGamePluginStat();
                }
            }
        }
        return a;
    }

    public static void setDebug(boolean z) {
        a.a(z);
    }

    public static void verifyEntry() {
        boolean z;
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        if (stackTrace != null) {
            int length = stackTrace.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = false;
                    break;
                }
                StackTraceElement stackTraceElement = stackTrace[i];
                if (stackTraceElement != null && !TextUtils.isEmpty(stackTraceElement.getMethodName()) && "onCreate".equals(stackTraceElement.getMethodName())) {
                    try {
                        if (Application.class.isAssignableFrom(Class.forName(stackTraceElement.getClassName()))) {
                            z = true;
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                i++;
            }
            if (!z) {
                throw new IllegalStateException("MUST CALL FROM APPLICATION ONCREATE METHOD!!!");
            }
        }
    }

    public void initWithChannelAndPkgNameList(MiGamePluginStatConfig miGamePluginStatConfig) {
        verifyEntry();
        com.xiaomi.game.plugin.stat.b.a.a().a(miGamePluginStatConfig);
        new com.xiaomi.game.plugin.stat.a.a(miGamePluginStatConfig);
    }
}
