package com.xiaomi.game.plugin.stat.a;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import com.xiaomi.game.plugin.stat.c.a;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

class b {
    static String a() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a("ro.product.manufacturer"));
        stringBuffer.append("|");
        stringBuffer.append(Build.MODEL);
        stringBuffer.append("|");
        stringBuffer.append(a("ro.build.version.release"));
        stringBuffer.append("|");
        stringBuffer.append(a("ro.build.display.id"));
        stringBuffer.append("|");
        stringBuffer.append(a("ro.build.version.sdk"));
        stringBuffer.append("|");
        stringBuffer.append(a("ro.product.device"));
        return stringBuffer.toString();
    }

    static String a(Context context) {
        try {
            if (!b()) {
                return c(context);
            }
            String b = b(context);
            return !TextUtils.isEmpty(b) ? b(b) : "";
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = "";
            th2.printStackTrace();
            return str;
        }
    }

    static String a(String str) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class).invoke(null, str);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    static void a(HashMap<String, String> hashMap, String str, String str2) {
        if (hashMap != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            String a = a.a((str + "_" + str2).getBytes());
            int a2 = a.a(20);
            int a3 = a.a(a.length() - a2);
            String substring = a.substring(a2, a2 + a3);
            hashMap.put("p1", substring);
            hashMap.put("p2", a2 + "." + a3);
        }
    }

    static String b(Context context) {
        WifiInfo connectionInfo;
        String macAddress;
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null || (macAddress = connectionInfo.getMacAddress()) == null) {
            return null;
        }
        return macAddress;
    }

    static String b(String str) {
        try {
            return Base64.encodeToString(MessageDigest.getInstance("SHA1").digest(str.getBytes()), 8).substring(0, 16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    static boolean b() {
        try {
            Class<?> cls = Class.forName("miui.os.Build");
            return ((Boolean) cls.getField("IS_TABLET").get(cls)).booleanValue();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    static String c(Context context) {
        return b(d(context));
    }

    static String d(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    static String e(Context context) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(context.getApplicationInfo().packageName, 0).versionCode);
        } catch (Exception e) {
            return "unknown";
        }
    }
}
