package com.xiaomi.game.plugin.stat;

import android.content.Context;
import android.text.TextUtils;
import com.sg.game.pay.Version;
import java.util.ArrayList;

public final class MiGamePluginStatConfig {
    private final Context a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private final ArrayList<String> f;
    private final boolean g;

    public static class Builder {
        /* access modifiers changed from: private */
        public Context a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public String e;
        /* access modifiers changed from: private */
        public ArrayList<String> f;
        /* access modifiers changed from: private */
        public boolean g = false;

        public MiGamePluginStatConfig build() {
            return new MiGamePluginStatConfig(this);
        }

        public Builder setAppId(String str) {
            this.b = str;
            return this;
        }

        public Builder setAppKey(String str) {
            this.c = str;
            return this;
        }

        public Builder setChannel(String str) {
            this.e = str;
            return this;
        }

        public Builder setContext(Context context) {
            this.a = context;
            return this;
        }

        public Builder setJarVersion(String str) {
            this.d = str;
            return this;
        }

        public Builder setPkgNameList(ArrayList<String> arrayList) {
            this.f = arrayList;
            return this;
        }

        public Builder setReportDAU(boolean z) {
            this.g = z;
            return this;
        }
    }

    private MiGamePluginStatConfig(Builder builder) {
        Builder a2 = a(builder);
        this.b = a2.b;
        this.c = a2.c;
        this.e = a2.e;
        this.a = a2.a;
        this.d = a2.d;
        this.f = a2.f;
        this.g = a2.g;
    }

    public Context a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public Builder a(Builder builder) {
        if (builder.a == null || TextUtils.isEmpty(builder.b) || TextUtils.isEmpty(builder.c) || builder.f == null || builder.f.size() == 0) {
            throw new IllegalStateException("非法的MiGamePluginStatParam 参数设置");
        }
        if (TextUtils.isEmpty(builder.e)) {
            String unused = builder.e = "default";
        }
        if (TextUtils.isEmpty(builder.d)) {
            String unused2 = builder.d = Version.version;
        }
        if (!builder.f.contains("com.xiaomi.game.plugin.stat")) {
            builder.f.add("com.xiaomi.game.plugin.stat");
        }
        return builder;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public ArrayList<String> f() {
        return this.f;
    }

    public boolean g() {
        return this.g;
    }
}
