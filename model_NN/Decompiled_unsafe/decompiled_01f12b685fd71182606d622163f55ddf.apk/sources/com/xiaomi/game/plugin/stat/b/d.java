package com.xiaomi.game.plugin.stat.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.wali.gamecenter.report.ReportOrigin;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

class d {
    private static String a;

    public static String a() {
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject = new JSONObject();
        jSONArray.put(jSONObject);
        jSONObject.put("startTS", System.currentTimeMillis());
        jSONObject.put("endTS", System.currentTimeMillis() + 20);
        JSONArray jSONArray2 = new JSONArray();
        jSONObject.put("content", jSONArray2);
        JSONObject jSONObject2 = new JSONObject();
        jSONArray2.put(jSONObject2);
        jSONObject2.put(ReportOrigin.ORIGIN_CATEGORY, "mistat_basic");
        JSONArray jSONArray3 = new JSONArray();
        jSONObject2.put("values", jSONArray3);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("key", "mistat_dau");
        jSONObject3.put("type", "event");
        jSONObject3.put("value", 1);
        jSONArray3.put(jSONObject3);
        return jSONArray.toString();
    }

    static String a(Context context) {
        String str;
        String str2 = null;
        try {
            if (a == null) {
                a = b(context, "device_id");
                if (TextUtils.isEmpty(a)) {
                    String b = b(context);
                    try {
                        str = Settings.Secure.getString(context.getContentResolver(), "android_id");
                    } catch (Throwable th) {
                        str = null;
                    }
                    if (Build.VERSION.SDK_INT > 8) {
                        str2 = Build.SERIAL;
                    }
                    a = a(b + str + str2);
                    if (!TextUtils.isEmpty(a) && !TextUtils.isEmpty(b)) {
                        a(context, "device_id", a);
                    }
                }
            }
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
        return a;
    }

    static String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            instance.update(b(str));
            return String.format("%1$032X", new BigInteger(1, instance.digest()));
        } catch (NoSuchAlgorithmException e) {
            return str;
        }
    }

    static boolean a(Context context, String str) {
        a(context, "crash_content", str);
        return TextUtils.isEmpty(str) ? a(context, "crash_create_time", null) : a(context, "crash_create_time", String.valueOf(System.currentTimeMillis()));
    }

    private static boolean a(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str)) {
            return false;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("_device_holder_gamesdkplugin", 0).edit();
        if (TextUtils.isEmpty(str2)) {
            edit.remove(str);
        } else {
            edit.putString(str, str2);
        }
        return edit.commit();
    }

    public static String b() {
        return new SimpleDateFormat("yyyyMMdd", Locale.CHINA).format(new Date());
    }

    public static String b(Context context) {
        try {
            if (context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                String deviceId = telephonyManager.getDeviceId();
                int i = 10;
                while (deviceId == null) {
                    int i2 = i - 1;
                    if (i <= 0) {
                        break;
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                    }
                    deviceId = telephonyManager.getDeviceId();
                    i = i2;
                }
                return deviceId;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return "";
    }

    private static String b(Context context, String str) {
        return (context == null || TextUtils.isEmpty(str)) ? "" : context.getSharedPreferences("_device_holder_gamesdkplugin", 0).getString(str, "");
    }

    private static byte[] b(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str.getBytes();
        }
    }

    static String c(Context context) {
        try {
            String b = b(context, "crash_create_time");
            if (TextUtils.isEmpty(b)) {
                return null;
            }
            if (Math.abs(System.currentTimeMillis() - Long.parseLong(b)) < fm.b) {
                return b(context, "crash_content");
            }
            a(context, null);
            return null;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    static boolean d(Context context) {
        return TextUtils.equals(b(), b(context, "report_init_date"));
    }

    static boolean e(Context context) {
        return a(context, "report_init_date", b());
    }

    static boolean f(Context context) {
        try {
            String b = b(context, "report_crash_time");
            if (!TextUtils.isEmpty(b)) {
                if (Math.abs(System.currentTimeMillis() - Long.parseLong(b)) < 60000) {
                    return false;
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return true;
    }

    static boolean g(Context context) {
        return a(context, "report_crash_time", String.valueOf(System.currentTimeMillis()));
    }
}
