package com.xiaomi.game.plugin.stat.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.json.JSONObject;

class c {

    public static final class a extends FilterInputStream {
        private boolean a;

        public a(InputStream inputStream) {
            super(inputStream);
        }

        public int read(byte[] bArr, int i, int i2) {
            int read;
            if (!this.a && (read = super.read(bArr, i, i2)) != -1) {
                return read;
            }
            this.a = true;
            return -1;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v12, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r1v16 */
    /* JADX WARN: Type inference failed for: r1v19 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007f A[SYNTHETIC, Splitter:B:28:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0084 A[Catch:{ IOException -> 0x0096 }] */
    private static java.lang.String a(android.content.Context r7, java.lang.String r8, java.lang.String r9) {
        /*
            r2 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r0.<init>(r8)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.net.HttpURLConnection r0 = a(r7, r0)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r1 = 10000(0x2710, float:1.4013E-41)
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            byte[] r3 = r9.getBytes()     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r4 = 0
            int r5 = r3.length     // Catch:{ IOException -> 0x00a7, Throwable -> 0x00a0 }
            r1.write(r3, r4, r5)     // Catch:{ IOException -> 0x00a7, Throwable -> 0x00a0 }
            r1.flush()     // Catch:{ IOException -> 0x00a7, Throwable -> 0x00a0 }
            r1.close()     // Catch:{ IOException -> 0x00a7, Throwable -> 0x00a0 }
            r3 = 0
            r0.getResponseCode()     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            com.xiaomi.game.plugin.stat.b.c$a r5 = new com.xiaomi.game.plugin.stat.b.c$a     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r5.<init>(r0)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0076, Throwable -> 0x0088, all -> 0x0098 }
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            r4.<init>()     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            java.lang.String r5 = "line.separator"
            java.lang.String r5 = java.lang.System.getProperty(r5)     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
        L_0x0056:
            if (r0 == 0) goto L_0x0063
            r4.append(r0)     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            r4.append(r5)     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            goto L_0x0056
        L_0x0063:
            java.lang.String r0 = r4.toString()     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            r1.close()     // Catch:{ IOException -> 0x00ac, Throwable -> 0x00a2, all -> 0x009b }
            r1 = 0
            if (r2 == 0) goto L_0x0070
            r3.close()     // Catch:{ IOException -> 0x00ae }
        L_0x0070:
            if (r2 == 0) goto L_0x0075
            r1.close()     // Catch:{ IOException -> 0x00ae }
        L_0x0075:
            return r0
        L_0x0076:
            r0 = move-exception
            r1 = r2
        L_0x0078:
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r0 = move-exception
            r6 = r1
            r1 = r2
            r2 = r6
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0087:
            throw r0
        L_0x0088:
            r0 = move-exception
            r1 = r2
        L_0x008a:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0094 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0094 }
            r3.<init>(r0)     // Catch:{ all -> 0x0094 }
            throw r3     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r0 = move-exception
            goto L_0x007d
        L_0x0096:
            r1 = move-exception
            goto L_0x0087
        L_0x0098:
            r0 = move-exception
            r1 = r2
            goto L_0x007d
        L_0x009b:
            r0 = move-exception
            r6 = r1
            r1 = r2
            r2 = r6
            goto L_0x007d
        L_0x00a0:
            r0 = move-exception
            goto L_0x008a
        L_0x00a2:
            r0 = move-exception
            r6 = r1
            r1 = r2
            r2 = r6
            goto L_0x008a
        L_0x00a7:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0078
        L_0x00ac:
            r0 = move-exception
            goto L_0x0078
        L_0x00ae:
            r1 = move-exception
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.game.plugin.stat.b.c.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String");
    }

    public static String a(URL url) {
        StringBuilder sb = new StringBuilder();
        sb.append(url.getProtocol()).append("://").append("10.0.0.172").append(url.getPath());
        if (!TextUtils.isEmpty(url.getQuery())) {
            sb.append("?").append(url.getQuery());
        }
        return sb.toString();
    }

    public static HttpURLConnection a(Context context, URL url) {
        if (!"http".equals(url.getProtocol())) {
            return (HttpURLConnection) url.openConnection();
        }
        if (b(context)) {
            return (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80)));
        }
        if (!a(context)) {
            return (HttpURLConnection) url.openConnection();
        }
        String host = url.getHost();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a(url)).openConnection();
        httpURLConnection.addRequestProperty("X-Online-Host", host);
        return httpURLConnection;
    }

    public static boolean a(Context context) {
        if (!"CN".equalsIgnoreCase(((TelephonyManager) context.getSystemService("phone")).getSimCountryIso())) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return false;
            }
            try {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo == null) {
                    return false;
                }
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (TextUtils.isEmpty(extraInfo) || extraInfo.length() < 3 || extraInfo.contains("ctwap")) {
                    return false;
                }
                return extraInfo.regionMatches(true, extraInfo.length() - 3, "wap", 0, 3);
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean a(Context context, String str) {
        return a(a(context, "https://data.mistat.xiaomi.com/micrash", str));
    }

    private static boolean a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                return jSONObject != null && jSONObject.getInt("code") == 0;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static boolean b(Context context) {
        if (!"CN".equalsIgnoreCase(((TelephonyManager) context.getSystemService("phone")).getSimCountryIso())) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return false;
            }
            try {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo == null) {
                    return false;
                }
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (TextUtils.isEmpty(extraInfo) || extraInfo.length() < 3) {
                    return false;
                }
                return extraInfo.contains("ctwap");
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean b(Context context, String str) {
        return a(a(context, "https://data.mistat.xiaomi.com/mistats", str));
    }
}
