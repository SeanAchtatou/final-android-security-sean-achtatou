package com.xiaomi.game.plugin.stat.b;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Iterator;

class b implements Thread.UncaughtExceptionHandler {
    private Context a;
    private Thread.UncaughtExceptionHandler b = Thread.getDefaultUncaughtExceptionHandler();
    private ArrayList<String> c;

    b(Context context, ArrayList<String> arrayList) {
        this.a = context.getApplicationContext();
        Thread.setDefaultUncaughtExceptionHandler(this);
        this.c = arrayList;
    }

    private void a(Thread thread, Context context, Throwable th) {
        try {
            String stackTraceString = Log.getStackTraceString(th);
            if (!TextUtils.isEmpty(stackTraceString) && this.c != null) {
                Iterator<String> it = this.c.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    String next = it.next();
                    if (!TextUtils.isEmpty(next) && stackTraceString.contains(next)) {
                        a.a().a(th);
                        break;
                    }
                }
            }
            if (this.b != null) {
                this.b.uncaughtException(thread, th);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        if (th != null) {
            a(thread, this.a, th);
        }
    }
}
