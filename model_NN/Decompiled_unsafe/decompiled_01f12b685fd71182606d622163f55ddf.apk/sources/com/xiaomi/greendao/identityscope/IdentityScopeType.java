package com.xiaomi.greendao.identityscope;

public enum IdentityScopeType {
    Session,
    None
}
