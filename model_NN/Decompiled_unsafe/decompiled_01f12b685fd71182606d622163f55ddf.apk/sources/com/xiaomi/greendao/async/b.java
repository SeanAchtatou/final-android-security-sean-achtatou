package com.xiaomi.greendao.async;

import com.xiaomi.greendao.async.AsyncOperation;

/* synthetic */ class b {
    static final /* synthetic */ int[] a = new int[AsyncOperation.OperationType.values().length];

    static {
        try {
            a[AsyncOperation.OperationType.Delete.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            a[AsyncOperation.OperationType.DeleteInTxIterable.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            a[AsyncOperation.OperationType.DeleteInTxArray.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            a[AsyncOperation.OperationType.Insert.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            a[AsyncOperation.OperationType.InsertInTxIterable.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            a[AsyncOperation.OperationType.InsertInTxArray.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            a[AsyncOperation.OperationType.InsertOrReplace.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            a[AsyncOperation.OperationType.InsertOrReplaceInTxIterable.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            a[AsyncOperation.OperationType.InsertOrReplaceInTxArray.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            a[AsyncOperation.OperationType.Update.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            a[AsyncOperation.OperationType.UpdateInTxIterable.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            a[AsyncOperation.OperationType.UpdateInTxArray.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            a[AsyncOperation.OperationType.TransactionRunnable.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            a[AsyncOperation.OperationType.TransactionCallable.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            a[AsyncOperation.OperationType.QueryList.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            a[AsyncOperation.OperationType.QueryUnique.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            a[AsyncOperation.OperationType.DeleteByKey.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
        try {
            a[AsyncOperation.OperationType.DeleteAll.ordinal()] = 18;
        } catch (NoSuchFieldError e18) {
        }
        try {
            a[AsyncOperation.OperationType.Load.ordinal()] = 19;
        } catch (NoSuchFieldError e19) {
        }
        try {
            a[AsyncOperation.OperationType.LoadAll.ordinal()] = 20;
        } catch (NoSuchFieldError e20) {
        }
        try {
            a[AsyncOperation.OperationType.Count.ordinal()] = 21;
        } catch (NoSuchFieldError e21) {
        }
        try {
            a[AsyncOperation.OperationType.Refresh.ordinal()] = 22;
        } catch (NoSuchFieldError e22) {
        }
    }
}
