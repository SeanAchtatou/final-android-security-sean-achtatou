package com.xiaomi.greendao.async;

import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.xiaomi.greendao.DaoException;
import com.xiaomi.greendao.DaoLog;
import com.xiaomi.greendao.query.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

class a implements Handler.Callback, Runnable {
    private static ExecutorService a = Executors.newCachedThreadPool();
    private final BlockingQueue<AsyncOperation> b = new LinkedBlockingQueue();
    private volatile boolean c;
    private volatile int d = 50;
    private volatile AsyncOperationListener e;
    private volatile AsyncOperationListener f;
    private volatile int g = 50;
    private int h;
    private int i;
    private Handler j;
    private int k;

    a() {
    }

    private void a(AsyncOperation asyncOperation, AsyncOperation asyncOperation2) {
        boolean z;
        ArrayList arrayList = new ArrayList();
        arrayList.add(asyncOperation);
        arrayList.add(asyncOperation2);
        SQLiteDatabase database = asyncOperation.getDatabase();
        database.beginTransaction();
        int i2 = 0;
        while (true) {
            try {
                if (i2 >= arrayList.size()) {
                    z = false;
                    break;
                }
                AsyncOperation asyncOperation3 = (AsyncOperation) arrayList.get(i2);
                d(asyncOperation3);
                if (asyncOperation3.isFailed()) {
                    z = false;
                    break;
                }
                if (i2 == arrayList.size() - 1) {
                    AsyncOperation peek = this.b.peek();
                    if (i2 >= this.d || !asyncOperation3.isMergeableWith(peek)) {
                        database.setTransactionSuccessful();
                    } else {
                        AsyncOperation remove = this.b.remove();
                        if (remove != peek) {
                            throw new DaoException("Internal error: peeked op did not match removed op");
                        }
                        arrayList.add(remove);
                    }
                }
                i2++;
            } finally {
                try {
                    database.endTransaction();
                } catch (RuntimeException e2) {
                    DaoLog.i("Async transaction could not be ended, success so far was: " + false, e2);
                }
            }
        }
        database.setTransactionSuccessful();
        z = true;
        try {
        } catch (RuntimeException e3) {
            DaoLog.i("Async transaction could not be ended, success so far was: " + z, e3);
            z = false;
        }
        if (z) {
            int size = arrayList.size();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AsyncOperation asyncOperation4 = (AsyncOperation) it.next();
                asyncOperation4.mergedOperationsCount = size;
                b(asyncOperation4);
            }
            return;
        }
        DaoLog.i("Reverted merged transaction because one of the operations failed. Executing operations one by one instead...");
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            AsyncOperation asyncOperation5 = (AsyncOperation) it2.next();
            asyncOperation5.reset();
            c(asyncOperation5);
        }
    }

    private void b(AsyncOperation asyncOperation) {
        asyncOperation.setCompleted();
        AsyncOperationListener asyncOperationListener = this.e;
        if (asyncOperationListener != null) {
            asyncOperationListener.onAsyncOperationCompleted(asyncOperation);
        }
        if (this.f != null) {
            if (this.j == null) {
                this.j = new Handler(Looper.getMainLooper(), this);
            }
            this.j.sendMessage(this.j.obtainMessage(1, asyncOperation));
        }
        synchronized (this) {
            this.i++;
            if (this.i == this.h) {
                notifyAll();
            }
        }
    }

    private void c(AsyncOperation asyncOperation) {
        d(asyncOperation);
        b(asyncOperation);
    }

    private void d(AsyncOperation asyncOperation) {
        asyncOperation.timeStarted = System.currentTimeMillis();
        try {
            switch (b.a[asyncOperation.type.ordinal()]) {
                case 1:
                    asyncOperation.dao.delete(asyncOperation.parameter);
                    break;
                case 2:
                    asyncOperation.dao.deleteInTx((Iterable) asyncOperation.parameter);
                    break;
                case 3:
                    asyncOperation.dao.deleteInTx((Object[]) asyncOperation.parameter);
                    break;
                case 4:
                    asyncOperation.dao.insert(asyncOperation.parameter);
                    break;
                case 5:
                    asyncOperation.dao.insertInTx((Iterable) asyncOperation.parameter);
                    break;
                case 6:
                    asyncOperation.dao.insertInTx((Object[]) asyncOperation.parameter);
                    break;
                case 7:
                    asyncOperation.dao.insertOrReplace(asyncOperation.parameter);
                    break;
                case 8:
                    asyncOperation.dao.insertOrReplaceInTx((Iterable) asyncOperation.parameter);
                    break;
                case 9:
                    asyncOperation.dao.insertOrReplaceInTx((Object[]) asyncOperation.parameter);
                    break;
                case 10:
                    asyncOperation.dao.update(asyncOperation.parameter);
                    break;
                case 11:
                    asyncOperation.dao.updateInTx((Iterable) asyncOperation.parameter);
                    break;
                case 12:
                    asyncOperation.dao.updateInTx((Object[]) asyncOperation.parameter);
                    break;
                case 13:
                    e(asyncOperation);
                    break;
                case 14:
                    f(asyncOperation);
                    break;
                case 15:
                    asyncOperation.result = ((Query) asyncOperation.parameter).forCurrentThread().list();
                    break;
                case 16:
                    asyncOperation.result = ((Query) asyncOperation.parameter).forCurrentThread().unique();
                    break;
                case 17:
                    asyncOperation.dao.deleteByKey(asyncOperation.parameter);
                    break;
                case 18:
                    asyncOperation.dao.deleteAll();
                    break;
                case 19:
                    asyncOperation.result = asyncOperation.dao.load(asyncOperation.parameter);
                    break;
                case 20:
                    asyncOperation.result = asyncOperation.dao.loadAll();
                    break;
                case 21:
                    asyncOperation.result = Long.valueOf(asyncOperation.dao.count());
                    break;
                case 22:
                    asyncOperation.dao.refresh(asyncOperation.parameter);
                    break;
                default:
                    throw new DaoException("Unsupported operation: " + asyncOperation.type);
            }
        } catch (Throwable th) {
            asyncOperation.throwable = th;
        }
        asyncOperation.timeCompleted = System.currentTimeMillis();
    }

    private void e(AsyncOperation asyncOperation) {
        SQLiteDatabase database = asyncOperation.getDatabase();
        database.beginTransaction();
        try {
            ((Runnable) asyncOperation.parameter).run();
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    private void f(AsyncOperation asyncOperation) {
        SQLiteDatabase database = asyncOperation.getDatabase();
        database.beginTransaction();
        try {
            asyncOperation.result = ((Callable) asyncOperation.parameter).call();
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    public int a() {
        return this.d;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void a(AsyncOperation asyncOperation) {
        synchronized (this) {
            int i2 = this.k + 1;
            this.k = i2;
            asyncOperation.sequenceNumber = i2;
            this.b.add(asyncOperation);
            this.h++;
            if (!this.c) {
                this.c = true;
                a.execute(this);
            }
        }
    }

    public void a(AsyncOperationListener asyncOperationListener) {
        this.e = asyncOperationListener;
    }

    public int b() {
        return this.g;
    }

    public void b(int i2) {
        this.g = i2;
    }

    public void b(AsyncOperationListener asyncOperationListener) {
        this.f = asyncOperationListener;
    }

    public AsyncOperationListener c() {
        return this.e;
    }

    public synchronized boolean c(int i2) {
        if (!e()) {
            try {
                wait((long) i2);
            } catch (InterruptedException e2) {
                throw new DaoException("Interrupted while waiting for all operations to complete", e2);
            }
        }
        return e();
    }

    public AsyncOperationListener d() {
        return this.f;
    }

    public synchronized boolean e() {
        return this.h == this.i;
    }

    public synchronized void f() {
        while (!e()) {
            try {
                wait();
            } catch (InterruptedException e2) {
                throw new DaoException("Interrupted while waiting for all operations to complete", e2);
            }
        }
    }

    public boolean handleMessage(Message message) {
        AsyncOperationListener asyncOperationListener = this.f;
        if (asyncOperationListener == null) {
            return false;
        }
        asyncOperationListener.onAsyncOperationCompleted((AsyncOperation) message.obj);
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        r1 = r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            r5 = 0
        L_0x0001:
            java.util.concurrent.BlockingQueue<com.xiaomi.greendao.async.AsyncOperation> r0 = r6.b     // Catch:{ InterruptedException -> 0x0042 }
            r2 = 1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x0042 }
            java.lang.Object r0 = r0.poll(r2, r1)     // Catch:{ InterruptedException -> 0x0042 }
            com.xiaomi.greendao.async.AsyncOperation r0 = (com.xiaomi.greendao.async.AsyncOperation) r0     // Catch:{ InterruptedException -> 0x0042 }
            if (r0 != 0) goto L_0x0076
            monitor-enter(r6)     // Catch:{ InterruptedException -> 0x0042 }
            java.util.concurrent.BlockingQueue<com.xiaomi.greendao.async.AsyncOperation> r0 = r6.b     // Catch:{ all -> 0x0064 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0064 }
            com.xiaomi.greendao.async.AsyncOperation r0 = (com.xiaomi.greendao.async.AsyncOperation) r0     // Catch:{ all -> 0x0064 }
            if (r0 != 0) goto L_0x0021
            r0 = 0
            r6.c = r0     // Catch:{ all -> 0x0064 }
            monitor-exit(r6)     // Catch:{ all -> 0x0064 }
            r6.c = r5
        L_0x0020:
            return
        L_0x0021:
            monitor-exit(r6)     // Catch:{ all -> 0x0064 }
            r1 = r0
        L_0x0023:
            boolean r0 = r1.isMergeTx()     // Catch:{ InterruptedException -> 0x0042 }
            if (r0 == 0) goto L_0x0072
            java.util.concurrent.BlockingQueue<com.xiaomi.greendao.async.AsyncOperation> r0 = r6.b     // Catch:{ InterruptedException -> 0x0042 }
            int r2 = r6.g     // Catch:{ InterruptedException -> 0x0042 }
            long r2 = (long) r2     // Catch:{ InterruptedException -> 0x0042 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0042 }
            java.lang.Object r0 = r0.poll(r2, r4)     // Catch:{ InterruptedException -> 0x0042 }
            com.xiaomi.greendao.async.AsyncOperation r0 = (com.xiaomi.greendao.async.AsyncOperation) r0     // Catch:{ InterruptedException -> 0x0042 }
            if (r0 == 0) goto L_0x0072
            boolean r2 = r1.isMergeableWith(r0)     // Catch:{ InterruptedException -> 0x0042 }
            if (r2 == 0) goto L_0x006b
            r6.a(r1, r0)     // Catch:{ InterruptedException -> 0x0042 }
            goto L_0x0001
        L_0x0042:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            r1.<init>()     // Catch:{ all -> 0x0067 }
            java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0067 }
            java.lang.String r2 = r2.getName()     // Catch:{ all -> 0x0067 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0067 }
            java.lang.String r2 = " was interruppted"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0067 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0067 }
            com.xiaomi.greendao.DaoLog.w(r1, r0)     // Catch:{ all -> 0x0067 }
            r6.c = r5
            goto L_0x0020
        L_0x0064:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0064 }
            throw r0     // Catch:{ InterruptedException -> 0x0042 }
        L_0x0067:
            r0 = move-exception
            r6.c = r5
            throw r0
        L_0x006b:
            r6.c(r1)     // Catch:{ InterruptedException -> 0x0042 }
            r6.c(r0)     // Catch:{ InterruptedException -> 0x0042 }
            goto L_0x0001
        L_0x0072:
            r6.c(r1)     // Catch:{ InterruptedException -> 0x0042 }
            goto L_0x0001
        L_0x0076:
            r1 = r0
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.greendao.async.a.run():void");
    }
}
