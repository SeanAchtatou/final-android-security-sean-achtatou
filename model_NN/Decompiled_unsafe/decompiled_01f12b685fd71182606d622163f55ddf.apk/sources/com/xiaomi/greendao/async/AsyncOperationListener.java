package com.xiaomi.greendao.async;

public interface AsyncOperationListener {
    void onAsyncOperationCompleted(AsyncOperation asyncOperation);
}
