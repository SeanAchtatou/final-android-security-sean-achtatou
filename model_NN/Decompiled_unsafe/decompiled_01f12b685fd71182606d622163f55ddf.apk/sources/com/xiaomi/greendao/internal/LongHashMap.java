package com.xiaomi.greendao.internal;

import com.xiaomi.greendao.DaoLog;
import java.util.Arrays;

public final class LongHashMap<T> {
    private int capacity;
    private int size;
    private a<T>[] table;
    private int threshold;

    public LongHashMap() {
        this(16);
    }

    public LongHashMap(int i) {
        this.capacity = i;
        this.threshold = (i * 4) / 3;
        this.table = new a[i];
    }

    public void clear() {
        this.size = 0;
        Arrays.fill(this.table, (Object) null);
    }

    public boolean containsKey(long j) {
        for (a<T> aVar = this.table[((((int) (j >>> 32)) ^ ((int) j)) & Integer.MAX_VALUE) % this.capacity]; aVar != null; aVar = aVar.c) {
            if (aVar.a == j) {
                return true;
            }
        }
        return false;
    }

    public T get(long j) {
        for (a<T> aVar = this.table[((((int) (j >>> 32)) ^ ((int) j)) & Integer.MAX_VALUE) % this.capacity]; aVar != null; aVar = aVar.c) {
            if (aVar.a == j) {
                return aVar.b;
            }
        }
        return null;
    }

    public void logStats() {
        int i = 0;
        for (a<T> aVar : this.table) {
            while (aVar != null && aVar.c != null) {
                i++;
                aVar = aVar.c;
            }
        }
        DaoLog.d("load: " + (((float) this.size) / ((float) this.capacity)) + ", size: " + this.size + ", capa: " + this.capacity + ", collisions: " + i + ", collision ratio: " + (((float) i) / ((float) this.size)));
    }

    public T put(long j, T t) {
        int i = ((((int) (j >>> 32)) ^ ((int) j)) & Integer.MAX_VALUE) % this.capacity;
        a<T> aVar = this.table[i];
        for (a<T> aVar2 = aVar; aVar2 != null; aVar2 = aVar2.c) {
            if (aVar2.a == j) {
                T t2 = aVar2.b;
                aVar2.b = t;
                return t2;
            }
        }
        this.table[i] = new a<>(j, t, aVar);
        this.size++;
        if (this.size > this.threshold) {
            setCapacity(this.capacity * 2);
        }
        return null;
    }

    public T remove(long j) {
        int i = ((((int) (j >>> 32)) ^ ((int) j)) & Integer.MAX_VALUE) % this.capacity;
        a<T> aVar = this.table[i];
        a<T> aVar2 = null;
        while (aVar != null) {
            a<T> aVar3 = aVar.c;
            if (aVar.a == j) {
                if (aVar2 == null) {
                    this.table[i] = aVar3;
                } else {
                    aVar2.c = aVar3;
                }
                this.size--;
                return aVar.b;
            }
            aVar2 = aVar;
            aVar = aVar3;
        }
        return null;
    }

    public void reserveRoom(int i) {
        setCapacity((i * 5) / 3);
    }

    public void setCapacity(int i) {
        a<T>[] aVarArr = new a[i];
        for (a<T> aVar : this.table) {
            while (aVar != null) {
                long j = aVar.a;
                int i2 = ((((int) (j >>> 32)) ^ ((int) j)) & Integer.MAX_VALUE) % i;
                a<T> aVar2 = aVar.c;
                aVar.c = aVarArr[i2];
                aVarArr[i2] = aVar;
                aVar = aVar2;
            }
        }
        this.table = aVarArr;
        this.capacity = i;
        this.threshold = (i * 4) / 3;
    }

    public int size() {
        return this.size;
    }
}
