package com.xiaomi.greendao.query;

import android.database.Cursor;
import com.xiaomi.greendao.AbstractDao;
import java.util.Date;

public class CursorQuery<T> extends c<T> {
    private final g<T> queryData;

    private CursorQuery(g<T> gVar, AbstractDao<T, ?> abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr, i, i2);
        this.queryData = gVar;
    }

    static <T2> CursorQuery<T2> create(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr, int i, int i2) {
        return (CursorQuery) new g(abstractDao, str, toStringArray(objArr), i, i2).a();
    }

    public static <T2> CursorQuery<T2> internalCreate(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr) {
        return create(abstractDao, str, objArr, -1, -1);
    }

    public CursorQuery forCurrentThread() {
        return (CursorQuery) this.queryData.a(this);
    }

    public Cursor query() {
        checkThread();
        return this.dao.getDatabase().rawQuery(this.sql, this.parameters);
    }

    public /* bridge */ /* synthetic */ void setLimit(int i) {
        super.setLimit(i);
    }

    public /* bridge */ /* synthetic */ void setOffset(int i) {
        super.setOffset(i);
    }

    public /* bridge */ /* synthetic */ c setParameter(int i, Boolean bool) {
        return super.setParameter(i, bool);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
     arg types: [int, java.lang.Object]
     candidates:
      com.xiaomi.greendao.query.CursorQuery.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.CursorQuery.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T> */
    public /* bridge */ /* synthetic */ c setParameter(int i, Object obj) {
        return super.setParameter(i, obj);
    }

    public /* bridge */ /* synthetic */ c setParameter(int i, Date date) {
        return super.setParameter(i, date);
    }
}
