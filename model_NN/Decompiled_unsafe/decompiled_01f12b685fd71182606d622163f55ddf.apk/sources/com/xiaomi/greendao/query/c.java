package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;
import java.util.Date;

abstract class c<T> extends a<T> {
    protected final int limitPosition;
    protected final int offsetPosition;

    protected c(AbstractDao<T, ?> abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr);
        this.limitPosition = i;
        this.offsetPosition = i2;
    }

    public void setLimit(int i) {
        checkThread();
        if (this.limitPosition == -1) {
            throw new IllegalStateException("Limit must be set with QueryBuilder before it can be used here");
        }
        this.parameters[this.limitPosition] = Integer.toString(i);
    }

    public void setOffset(int i) {
        checkThread();
        if (this.offsetPosition == -1) {
            throw new IllegalStateException("Offset must be set with QueryBuilder before it can be used here");
        }
        this.parameters[this.offsetPosition] = Integer.toString(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
     arg types: [int, java.lang.Integer]
     candidates:
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T> */
    public c<T> setParameter(int i, Boolean bool) {
        Integer num;
        if (bool != null) {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        } else {
            num = null;
        }
        return setParameter(i, (Object) num);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
     arg types: [int, java.lang.Object]
     candidates:
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T> */
    public c<T> setParameter(int i, Object obj) {
        if (i < 0 || (i != this.limitPosition && i != this.offsetPosition)) {
            return (c) super.setParameter(i, obj);
        }
        throw new IllegalArgumentException("Illegal parameter index: " + i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
     arg types: [int, java.lang.Long]
     candidates:
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T> */
    public c<T> setParameter(int i, Date date) {
        return setParameter(i, (Object) (date != null ? Long.valueOf(date.getTime()) : null));
    }
}
