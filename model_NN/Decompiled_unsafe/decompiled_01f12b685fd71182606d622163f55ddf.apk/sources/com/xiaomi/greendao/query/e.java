package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;

final class e<T2> extends b<T2, CountQuery<T2>> {
    private e(AbstractDao<T2, ?> abstractDao, String str, String[] strArr) {
        super(abstractDao, str, strArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public CountQuery<T2> b() {
        return new CountQuery<>(this, this.b, this.a, (String[]) this.c.clone());
    }
}
