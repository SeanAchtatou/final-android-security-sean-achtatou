package com.xiaomi.greendao.query;

import android.os.Process;
import android.util.SparseArray;
import com.xiaomi.greendao.AbstractDao;
import com.xiaomi.greendao.query.a;
import java.lang.ref.WeakReference;

abstract class b<T, Q extends a<T>> {
    final String a;
    final AbstractDao<T, ?> b;
    final String[] c;
    final SparseArray<WeakReference<Q>> d = new SparseArray<>();

    b(AbstractDao<T, ?> abstractDao, String str, String[] strArr) {
        this.b = abstractDao;
        this.a = str;
        this.c = strArr;
    }

    /* access modifiers changed from: package-private */
    public Q a() {
        int i;
        Q q;
        int myTid = Process.myTid();
        if (myTid == 0) {
            long id = Thread.currentThread().getId();
            if (id < 0 || id > 2147483647L) {
                throw new RuntimeException("Cannot handle thread ID: " + id);
            }
            i = (int) id;
        } else {
            i = myTid;
        }
        synchronized (this.d) {
            WeakReference weakReference = this.d.get(i);
            q = weakReference != null ? (a) weakReference.get() : null;
            if (q == null) {
                c();
                q = b();
                this.d.put(i, new WeakReference(q));
            } else {
                System.arraycopy(this.c, 0, q.parameters, 0, this.c.length);
            }
        }
        return q;
    }

    /* access modifiers changed from: package-private */
    public Q a(Q q) {
        if (Thread.currentThread() != q.ownerThread) {
            return a();
        }
        System.arraycopy(this.c, 0, q.parameters, 0, this.c.length);
        return q;
    }

    /* access modifiers changed from: protected */
    public abstract Q b();

    /* access modifiers changed from: package-private */
    public void c() {
        synchronized (this.d) {
            for (int size = this.d.size() - 1; size >= 0; size--) {
                if (this.d.valueAt(size).get() == null) {
                    this.d.remove(this.d.keyAt(size));
                }
            }
        }
    }
}
