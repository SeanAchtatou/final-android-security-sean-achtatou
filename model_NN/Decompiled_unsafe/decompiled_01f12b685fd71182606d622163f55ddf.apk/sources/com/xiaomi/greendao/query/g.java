package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;

final class g<T2> extends b<T2, CursorQuery<T2>> {
    private final int e;
    private final int f;

    g(AbstractDao abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr);
        this.e = i;
        this.f = i2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public CursorQuery<T2> b() {
        return new CursorQuery<>(this, this.b, this.a, (String[]) this.c.clone(), this.e, this.f);
    }
}
