package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;
import com.xiaomi.greendao.DaoException;
import com.xiaomi.greendao.Property;
import com.xiaomi.greendao.query.WhereCondition;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

class l<T> {
    private final AbstractDao<T, ?> a;
    private final List<WhereCondition> b = new ArrayList();
    private final String c;

    l(AbstractDao<T, ?> abstractDao, String str) {
        this.a = abstractDao;
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public WhereCondition a(String str, WhereCondition whereCondition, WhereCondition whereCondition2, WhereCondition... whereConditionArr) {
        StringBuilder sb = new StringBuilder("(");
        ArrayList arrayList = new ArrayList();
        a(sb, arrayList, whereCondition);
        sb.append(str);
        a(sb, arrayList, whereCondition2);
        for (WhereCondition a2 : whereConditionArr) {
            sb.append(str);
            a(sb, arrayList, a2);
        }
        sb.append(')');
        return new WhereCondition.StringCondition(sb.toString(), arrayList.toArray());
    }

    /* access modifiers changed from: package-private */
    public void a(Property property) {
        boolean z = false;
        if (this.a != null) {
            Property[] properties = this.a.getProperties();
            int length = properties.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (property == properties[i]) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                throw new DaoException("Property '" + property.name + "' is not part of " + this.a);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(WhereCondition whereCondition) {
        if (whereCondition instanceof WhereCondition.PropertyCondition) {
            a(((WhereCondition.PropertyCondition) whereCondition).property);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(WhereCondition whereCondition, WhereCondition... whereConditionArr) {
        a(whereCondition);
        this.b.add(whereCondition);
        for (WhereCondition whereCondition2 : whereConditionArr) {
            a(whereCondition2);
            this.b.add(whereCondition2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(StringBuilder sb, String str, List<Object> list) {
        ListIterator<WhereCondition> listIterator = this.b.listIterator();
        while (listIterator.hasNext()) {
            if (listIterator.hasPrevious()) {
                sb.append(" AND ");
            }
            WhereCondition next = listIterator.next();
            next.appendTo(sb, str);
            next.appendValuesTo(list);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(StringBuilder sb, List<Object> list, WhereCondition whereCondition) {
        a(whereCondition);
        whereCondition.appendTo(sb, this.c);
        whereCondition.appendValuesTo(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.b.isEmpty();
    }
}
