package com.xiaomi.greendao.query;

import android.database.Cursor;
import com.xiaomi.greendao.AbstractDao;
import com.xiaomi.greendao.DaoException;

public class CountQuery<T> extends a<T> {
    private final e<T> queryData;

    private CountQuery(e<T> eVar, AbstractDao<T, ?> abstractDao, String str, String[] strArr) {
        super(abstractDao, str, strArr);
        this.queryData = eVar;
    }

    static <T2> CountQuery<T2> create(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr) {
        return (CountQuery) new e(abstractDao, str, toStringArray(objArr)).a();
    }

    public long count() {
        checkThread();
        Cursor rawQuery = this.dao.getDatabase().rawQuery(this.sql, this.parameters);
        try {
            if (!rawQuery.moveToNext()) {
                throw new DaoException("No result for count");
            } else if (!rawQuery.isLast()) {
                throw new DaoException("Unexpected row count: " + rawQuery.getCount());
            } else if (rawQuery.getColumnCount() == 1) {
                return rawQuery.getLong(0);
            } else {
                throw new DaoException("Unexpected column count: " + rawQuery.getColumnCount());
            }
        } finally {
            rawQuery.close();
        }
    }

    public CountQuery<T> forCurrentThread() {
        return (CountQuery) this.queryData.a(this);
    }

    public /* bridge */ /* synthetic */ a setParameter(int i, Object obj) {
        return super.setParameter(i, obj);
    }
}
