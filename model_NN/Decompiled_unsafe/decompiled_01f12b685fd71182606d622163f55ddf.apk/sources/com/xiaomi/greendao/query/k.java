package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;

final class k<T2> extends b<T2, Query<T2>> {
    private final int e;
    private final int f;

    k(AbstractDao<T2, ?> abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr);
        this.e = i;
        this.f = i2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Query<T2> b() {
        return new Query<>(this, this.b, this.a, (String[]) this.c.clone(), this.e, this.f);
    }
}
