package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;

final class i<T2> extends b<T2, DeleteQuery<T2>> {
    private i(AbstractDao<T2, ?> abstractDao, String str, String[] strArr) {
        super(abstractDao, str, strArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public DeleteQuery<T2> b() {
        return new DeleteQuery<>(this, this.b, this.a, (String[]) this.c.clone());
    }
}
