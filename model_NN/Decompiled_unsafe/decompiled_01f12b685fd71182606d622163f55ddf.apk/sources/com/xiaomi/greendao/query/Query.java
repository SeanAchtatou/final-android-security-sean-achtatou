package com.xiaomi.greendao.query;

import com.xiaomi.greendao.AbstractDao;
import com.xiaomi.greendao.DaoException;
import java.util.Date;
import java.util.List;

public class Query<T> extends c<T> {
    private final k<T> queryData;

    private Query(k<T> kVar, AbstractDao<T, ?> abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr, i, i2);
        this.queryData = kVar;
    }

    static <T2> Query<T2> create(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr, int i, int i2) {
        return (Query) new k(abstractDao, str, toStringArray(objArr), i, i2).a();
    }

    public static <T2> Query<T2> internalCreate(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr) {
        return create(abstractDao, str, objArr, -1, -1);
    }

    public Query<T> forCurrentThread() {
        return (Query) this.queryData.a(this);
    }

    public List<T> list() {
        checkThread();
        return this.daoAccess.loadAllAndCloseCursor(this.dao.getDatabase().rawQuery(this.sql, this.parameters));
    }

    public CloseableListIterator<T> listIterator() {
        return listLazyUncached().listIteratorAutoClose();
    }

    public LazyList<T> listLazy() {
        checkThread();
        return new LazyList<>(this.daoAccess, this.dao.getDatabase().rawQuery(this.sql, this.parameters), true);
    }

    public LazyList<T> listLazyUncached() {
        checkThread();
        return new LazyList<>(this.daoAccess, this.dao.getDatabase().rawQuery(this.sql, this.parameters), false);
    }

    public /* bridge */ /* synthetic */ void setLimit(int i) {
        super.setLimit(i);
    }

    public /* bridge */ /* synthetic */ void setOffset(int i) {
        super.setOffset(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
     arg types: [int, java.lang.Boolean]
     candidates:
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.util.Date):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.Query.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T> */
    public Query<T> setParameter(int i, Boolean bool) {
        return (Query) super.setParameter(i, bool);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
     arg types: [int, java.lang.Object]
     candidates:
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.util.Date):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.Query.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T> */
    public Query<T> setParameter(int i, Object obj) {
        return (Query) super.setParameter(i, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T>
     arg types: [int, java.util.Date]
     candidates:
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.util.Date):com.xiaomi.greendao.query.Query<T>
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.Query.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Boolean):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.c<T>
      com.xiaomi.greendao.query.a.setParameter(int, java.lang.Object):com.xiaomi.greendao.query.a<T>
      com.xiaomi.greendao.query.c.setParameter(int, java.util.Date):com.xiaomi.greendao.query.c<T> */
    public Query<T> setParameter(int i, Date date) {
        return (Query) super.setParameter(i, date);
    }

    public T unique() {
        checkThread();
        return this.daoAccess.loadUniqueAndCloseCursor(this.dao.getDatabase().rawQuery(this.sql, this.parameters));
    }

    public T uniqueOrThrow() {
        T unique = unique();
        if (unique != null) {
            return unique;
        }
        throw new DaoException("No entity found for query");
    }
}
