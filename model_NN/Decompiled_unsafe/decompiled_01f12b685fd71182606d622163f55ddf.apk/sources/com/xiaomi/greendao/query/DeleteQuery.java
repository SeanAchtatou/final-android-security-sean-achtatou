package com.xiaomi.greendao.query;

import android.database.sqlite.SQLiteDatabase;
import com.xiaomi.greendao.AbstractDao;

public class DeleteQuery<T> extends a<T> {
    private final i<T> queryData;

    private DeleteQuery(i<T> iVar, AbstractDao<T, ?> abstractDao, String str, String[] strArr) {
        super(abstractDao, str, strArr);
        this.queryData = iVar;
    }

    static <T2> DeleteQuery<T2> create(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr) {
        return (DeleteQuery) new i(abstractDao, str, toStringArray(objArr)).a();
    }

    public void executeDeleteWithoutDetachingEntities() {
        checkThread();
        SQLiteDatabase database = this.dao.getDatabase();
        if (database.isDbLockedByCurrentThread()) {
            this.dao.getDatabase().execSQL(this.sql, this.parameters);
            return;
        }
        database.beginTransaction();
        try {
            this.dao.getDatabase().execSQL(this.sql, this.parameters);
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    public DeleteQuery<T> forCurrentThread() {
        return (DeleteQuery) this.queryData.a(this);
    }

    public /* bridge */ /* synthetic */ a setParameter(int i, Object obj) {
        return super.setParameter(i, obj);
    }
}
