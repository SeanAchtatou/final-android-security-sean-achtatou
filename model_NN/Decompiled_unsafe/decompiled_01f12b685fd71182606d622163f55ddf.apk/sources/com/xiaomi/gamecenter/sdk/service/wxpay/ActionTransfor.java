package com.xiaomi.gamecenter.sdk.service.wxpay;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.UUID;

public class ActionTransfor {

    public enum ActionResult {
        ACTION_FAIL,
        ACTION_OK,
        ACTION_CANCEL,
        ACTION_NONE
    }

    public static class DataAction implements Parcelable {
        public static final Parcelable.Creator<DataAction> CREATOR = new Parcelable.Creator<DataAction>() {
            public DataAction createFromParcel(Parcel parcel) {
                DataAction dataAction = new DataAction();
                dataAction.action_id = parcel.readString();
                dataAction.result = ActionResult.valueOf(parcel.readString());
                dataAction.data = parcel.readBundle();
                dataAction.errcode = parcel.readInt();
                return dataAction;
            }

            public DataAction[] newArray(int i) {
                return new DataAction[i];
            }
        };
        public String action_id;
        public Bundle data;
        public int errcode;
        public ActionResult result;

        public DataAction() {
            this.result = ActionResult.ACTION_NONE;
            this.data = new Bundle();
            this.action_id = UUID.randomUUID().toString();
            this.result = ActionResult.ACTION_NONE;
            this.errcode = 0;
        }

        public void assign(DataAction dataAction) {
            if (dataAction != null) {
                this.action_id = dataAction.action_id;
                this.result = dataAction.result;
                this.errcode = dataAction.errcode;
                this.data = new Bundle(dataAction.data);
                return;
            }
            this.action_id = null;
            this.data = null;
            this.errcode = -1;
        }

        public DataAction(Bundle bundle) {
            this.result = ActionResult.ACTION_NONE;
            this.action_id = UUID.randomUUID().toString();
            this.result = ActionResult.ACTION_NONE;
            this.errcode = 0;
            this.data = bundle;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.action_id);
            parcel.writeString(this.result.toString());
            parcel.writeBundle(this.data);
            parcel.writeInt(this.errcode);
        }
    }
}
