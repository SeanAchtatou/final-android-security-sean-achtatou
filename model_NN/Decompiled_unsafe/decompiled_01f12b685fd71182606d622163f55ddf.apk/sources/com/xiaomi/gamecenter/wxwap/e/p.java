package com.xiaomi.gamecenter.wxwap.e;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: ZSIMInfo */
public class p {
    public static String a(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static String b(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = k(context);
        }
        if (TextUtils.isEmpty(deviceId)) {
            return "";
        }
        return deviceId;
    }

    public static int c(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimState();
    }

    public static String d(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
    }

    public static String e(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimOperatorName();
    }

    public static String f(Context context) {
        int phoneType = ((TelephonyManager) context.getSystemService("phone")).getPhoneType();
        if (phoneType == 1) {
            return "GSM";
        }
        if (phoneType == 2) {
            return "CDMA";
        }
        return "未知";
    }

    public static String g(Context context) {
        String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
        if ("46000".equals(simOperator) || "46002".equals(simOperator) || "45412".equals(simOperator) || "46007".equals(simOperator)) {
            return "CMCC";
        }
        if ("46001".equals(simOperator)) {
            return "UNICOM";
        }
        if ("46003".equals(simOperator)) {
            return "TELECOM";
        }
        return "UNKNOWN";
    }

    public static String h(Context context) {
        String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
        if ("46000".equals(simOperator) || "46002".equals(simOperator) || "45412".equals(simOperator) || "46007".equals(simOperator)) {
            return "CMCC";
        }
        if ("46001".equals(simOperator)) {
            return "UNICOM";
        }
        if ("46003".equals(simOperator)) {
            return "TELECOM";
        }
        return "UNKNOW";
    }

    public static String i(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimCountryIso();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005c, code lost:
        if (r0 == null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ba, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00bb, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x010d, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x010e, code lost:
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0116, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067 A[SYNTHETIC, Splitter:B:23:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00e2 A[SYNTHETIC, Splitter:B:60:0x00e2] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00f8 A[SYNTHETIC, Splitter:B:73:0x00f8] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0104 A[SYNTHETIC, Splitter:B:80:0x0104] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x010d A[ExcHandler: all (th java.lang.Throwable), Splitter:B:25:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x011e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String j(android.content.Context r9) {
        /*
            r3 = 0
            r4 = -1
            r1 = 0
            java.lang.String r0 = "wifi"
            java.lang.Object r0 = r9.getSystemService(r0)
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0
            if (r0 == 0) goto L_0x0016
            boolean r0 = r0.isWifiEnabled()
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = ""
        L_0x0015:
            return r1
        L_0x0016:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00bf }
            java.lang.String r2 = "/sys/class/net/"
            r0.<init>(r2)     // Catch:{ Exception -> 0x00bf }
            java.io.File[] r5 = r0.listFiles()     // Catch:{ Exception -> 0x00bf }
            r2 = r1
        L_0x0022:
            int r0 = r5.length     // Catch:{ Exception -> 0x00bf }
            if (r3 >= r0) goto L_0x0122
            r0 = r5[r3]     // Catch:{ Exception -> 0x00bf }
            boolean r0 = r0.isDirectory()     // Catch:{ Exception -> 0x00bf }
            if (r0 == 0) goto L_0x0120
            r0 = r5[r3]     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r6 = "lo"
            boolean r0 = r0.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x00bf }
            if (r0 != 0) goto L_0x0120
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            r6.<init>()     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            r7 = r5[r3]     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            java.lang.String r7 = r7.getPath()     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            java.lang.String r7 = "/carrier"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x00c5, all -> 0x0118 }
            r0.read()     // Catch:{ Exception -> 0x011a }
            if (r0 == 0) goto L_0x0062
            r0.close()     // Catch:{ IOException -> 0x00ba }
        L_0x0061:
            r0 = r1
        L_0x0062:
            r8 = r3
            r3 = r0
            r0 = r8
        L_0x0065:
            if (r4 == r0) goto L_0x011e
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            r6.<init>()     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            java.lang.String r7 = "/sys/class/net/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            r0 = r5[r0]     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            java.lang.String r5 = "/address"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00f0, all -> 0x0101 }
            int r0 = r2.available()     // Catch:{ Exception -> 0x0113, all -> 0x010d }
            if (r0 <= 0) goto L_0x011c
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0113, all -> 0x010d }
            r2.read(r0)     // Catch:{ Exception -> 0x0113, all -> 0x010d }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0113, all -> 0x010d }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0113, all -> 0x010d }
            java.lang.String r0 = "\n"
            int r0 = r3.indexOf(r0)     // Catch:{ Exception -> 0x0116, all -> 0x010d }
            if (r0 == r4) goto L_0x00af
            r4 = 0
            java.lang.String r3 = r3.substring(r4, r0)     // Catch:{ Exception -> 0x0116, all -> 0x010d }
            int r0 = r3.length()     // Catch:{ Exception -> 0x0116, all -> 0x010d }
            if (r0 != 0) goto L_0x00af
            r3 = r1
        L_0x00af:
            if (r2 == 0) goto L_0x00b4
            r2.close()     // Catch:{ IOException -> 0x00eb }
        L_0x00b4:
            java.lang.String r1 = b(r3)     // Catch:{ Exception -> 0x00bf }
            goto L_0x0015
        L_0x00ba:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00bf }
            goto L_0x0061
        L_0x00bf:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0015
        L_0x00c5:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r8
        L_0x00c9:
            r2.printStackTrace()     // Catch:{ all -> 0x00dc }
            if (r0 == 0) goto L_0x00d2
            r0.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00d1:
            r0 = r1
        L_0x00d2:
            int r3 = r3 + 1
            r2 = r0
            goto L_0x0022
        L_0x00d7:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00bf }
            goto L_0x00d1
        L_0x00dc:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
        L_0x00e0:
            if (r2 == 0) goto L_0x00e5
            r2.close()     // Catch:{ IOException -> 0x00e6 }
        L_0x00e5:
            throw r0     // Catch:{ Exception -> 0x00bf }
        L_0x00e6:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x00bf }
            goto L_0x00e5
        L_0x00eb:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00bf }
            goto L_0x00b4
        L_0x00f0:
            r0 = move-exception
            r2 = r3
            r3 = r1
        L_0x00f3:
            r0.printStackTrace()     // Catch:{ all -> 0x0110 }
            if (r2 == 0) goto L_0x00b4
            r2.close()     // Catch:{ IOException -> 0x00fc }
            goto L_0x00b4
        L_0x00fc:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x00bf }
            goto L_0x00b4
        L_0x0101:
            r0 = move-exception
        L_0x0102:
            if (r3 == 0) goto L_0x0107
            r3.close()     // Catch:{ IOException -> 0x0108 }
        L_0x0107:
            throw r0     // Catch:{ Exception -> 0x00bf }
        L_0x0108:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ Exception -> 0x00bf }
            goto L_0x0107
        L_0x010d:
            r0 = move-exception
            r3 = r2
            goto L_0x0102
        L_0x0110:
            r0 = move-exception
            r3 = r2
            goto L_0x0102
        L_0x0113:
            r0 = move-exception
            r3 = r1
            goto L_0x00f3
        L_0x0116:
            r0 = move-exception
            goto L_0x00f3
        L_0x0118:
            r0 = move-exception
            goto L_0x00e0
        L_0x011a:
            r2 = move-exception
            goto L_0x00c9
        L_0x011c:
            r3 = r1
            goto L_0x00af
        L_0x011e:
            r3 = r1
            goto L_0x00b4
        L_0x0120:
            r0 = r2
            goto L_0x00d2
        L_0x0122:
            r0 = r4
            r3 = r2
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.gamecenter.wxwap.e.p.j(android.content.Context):java.lang.String");
    }

    private static String b(String str) {
        return str.replace(":", "").toUpperCase();
    }

    public static String k(Context context) {
        WifiInfo connectionInfo;
        String macAddress;
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null || (macAddress = connectionInfo.getMacAddress()) == null) {
            return null;
        }
        return b(macAddress);
    }

    public static String l(Context context) {
        return a(b(context));
    }

    public static String a(String str) {
        try {
            return Base64.encodeToString(MessageDigest.getInstance("SHA1").digest(str.getBytes()), 8).substring(0, 16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }
}
