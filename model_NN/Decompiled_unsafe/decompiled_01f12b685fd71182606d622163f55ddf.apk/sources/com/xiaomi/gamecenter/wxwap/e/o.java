package com.xiaomi.gamecenter.wxwap.e;

import android.util.Base64;

/* compiled from: URLBase64 */
public final class o {
    public static String a(byte[] bArr) {
        return Base64.encodeToString(bArr, 10).replaceAll("=", ".");
    }

    public static byte[] a(String str) {
        return Base64.decode(str, 8);
    }
}
