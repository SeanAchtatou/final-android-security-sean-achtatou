package com.xiaomi.gamecenter.wxwap.e;

import android.os.Handler;

/* compiled from: CountDownTimer */
class g extends Handler {
    final /* synthetic */ f a;

    g(f fVar) {
        this.a = fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r9) {
        /*
            r8 = this;
            r6 = 0
            com.xiaomi.gamecenter.wxwap.e.f r2 = r8.a
            monitor-enter(r2)
            com.xiaomi.gamecenter.wxwap.e.f r0 = r8.a     // Catch:{ all -> 0x0025 }
            boolean r0 = r0.d     // Catch:{ all -> 0x0025 }
            if (r0 == 0) goto L_0x000f
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
        L_0x000e:
            return
        L_0x000f:
            com.xiaomi.gamecenter.wxwap.e.f r0 = r8.a     // Catch:{ all -> 0x0025 }
            long r0 = r0.c     // Catch:{ all -> 0x0025 }
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0025 }
            long r0 = r0 - r4
            int r3 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r3 > 0) goto L_0x0028
            com.xiaomi.gamecenter.wxwap.e.f r0 = r8.a     // Catch:{ all -> 0x0025 }
            r0.c()     // Catch:{ all -> 0x0025 }
        L_0x0023:
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            goto L_0x000e
        L_0x0025:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            throw r0
        L_0x0028:
            com.xiaomi.gamecenter.wxwap.e.f r3 = r8.a     // Catch:{ all -> 0x0025 }
            long r4 = r3.b     // Catch:{ all -> 0x0025 }
            int r3 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x003b
            r3 = 1
            android.os.Message r3 = r8.obtainMessage(r3)     // Catch:{ all -> 0x0025 }
            r8.sendMessageDelayed(r3, r0)     // Catch:{ all -> 0x0025 }
            goto L_0x0023
        L_0x003b:
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0025 }
            com.xiaomi.gamecenter.wxwap.e.f r3 = r8.a     // Catch:{ all -> 0x0025 }
            r3.a(r0)     // Catch:{ all -> 0x0025 }
            com.xiaomi.gamecenter.wxwap.e.f r0 = r8.a     // Catch:{ all -> 0x0025 }
            long r0 = r0.b     // Catch:{ all -> 0x0025 }
            long r0 = r0 + r4
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0025 }
            long r0 = r0 - r4
        L_0x0050:
            int r3 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r3 >= 0) goto L_0x005c
            com.xiaomi.gamecenter.wxwap.e.f r3 = r8.a     // Catch:{ all -> 0x0025 }
            long r4 = r3.b     // Catch:{ all -> 0x0025 }
            long r0 = r0 + r4
            goto L_0x0050
        L_0x005c:
            r3 = 1
            android.os.Message r3 = r8.obtainMessage(r3)     // Catch:{ all -> 0x0025 }
            r8.sendMessageDelayed(r3, r0)     // Catch:{ all -> 0x0025 }
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.gamecenter.wxwap.e.g.handleMessage(android.os.Message):void");
    }
}
