package com.xiaomi.gamecenter.wxwap.fragment;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sg.pak.PAK_ASSETS;
import com.xiaomi.gamecenter.wxwap.d.b;
import com.xiaomi.gamecenter.wxwap.e.n;

public class HyWxScanFragment extends BaseFragment {
    public static final String d = "HyWxScanFragment";
    private RelativeLayout e;
    private TextView f;
    private ImageView g;
    private String h;

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.a().a(126);
        this.e = new RelativeLayout(getActivity());
        this.e.setBackgroundColor(-1579033);
        this.e.setGravity(17);
        return this.e;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setGravity(1);
        linearLayout.setOrientation(1);
        this.e.addView(linearLayout, new RelativeLayout.LayoutParams(-1, -2));
        this.f = new TextView(getActivity());
        this.f.setGravity(17);
        this.f.setTextColor(-16777216);
        this.f.setTextSize((float) b());
        linearLayout.addView(this.f, new LinearLayout.LayoutParams(-1, -2));
        this.g = new ImageView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(c((int) PAK_ASSETS.IMG_UP011), c((int) PAK_ASSETS.IMG_UP011));
        layoutParams.topMargin = c(100);
        linearLayout.addView(this.g, layoutParams);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new f(this));
    }

    public void onStop() {
        super.onStop();
        if (this.c != null) {
            this.c.cancel();
            this.c = null;
        }
    }

    public void a(Bundle bundle, String str) {
        this.h = bundle.getString("feeValue");
        this.a.a("WXNATIVE");
    }

    public void a(String str, String str2, String str3) {
        this.b.dismiss();
        d(str2);
        a(300000, 3000);
    }

    private void d(String str) {
        if (this.f != null) {
            if (TextUtils.isEmpty(this.h) || "-1".equals(this.h)) {
                this.f.setText(Html.fromHtml("请使用<font color=\"#fda13c\"> 微信 </font>扫码支付"));
            } else {
                this.f.setText(Html.fromHtml("请使用<font color=\"#fda13c\"> 微信 </font>扫码支付<font color=\"#fda13c\">" + String.format("%.02f", Float.valueOf(((float) Long.parseLong(this.h)) / 100.0f)) + " 元</font>"));
            }
        }
        if (this.g != null) {
            this.g.setImageBitmap(n.a(str, 300));
        }
    }

    public int b() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        if (i >= i2) {
            i = i2;
        }
        return (int) ((((float) i) / 1536.0f) * 32.0f);
    }

    public int c(int i) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i2 = displayMetrics.widthPixels;
        int i3 = displayMetrics.heightPixels;
        if (i2 >= i3) {
            i2 = i3;
        }
        return (int) ((((float) i2) / 1536.0f) * ((float) i));
    }
}
