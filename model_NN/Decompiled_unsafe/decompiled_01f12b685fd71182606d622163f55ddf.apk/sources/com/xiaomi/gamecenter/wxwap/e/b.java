package com.xiaomi.gamecenter.wxwap.e;

import java.util.Date;
import java.util.Random;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: AESUtils */
public class b {
    public static String a(String str, String str2) {
        if (str2 == null || str2.equals("")) {
            return "";
        }
        byte[] bArr = new byte[0];
        try {
            SecretKeySpec c = c(str);
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, c, new IvParameterSpec("0102030405060708".getBytes()));
            bArr = instance.doFinal(str2.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        new e();
        return e.a(bArr);
    }

    public static String b(String str, String str2) throws Exception {
        SecretKeySpec c = c(str);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, c, new IvParameterSpec("0102030405060708".getBytes()));
        new e();
        return new String(instance.doFinal(e.a(str2)));
    }

    private static SecretKeySpec c(String str) throws Exception {
        byte[] bytes = str.getBytes("UTF-8");
        byte[] bArr = new byte[16];
        int i = 0;
        while (i < bytes.length && i < bArr.length) {
            bArr[i] = bytes[i];
            i++;
        }
        return new SecretKeySpec(bArr, "AES");
    }

    public static String a(String str) {
        Exception exc;
        String str2;
        try {
            int nextInt = new Random(new Date().getTime()).nextInt(10);
            String str3 = nextInt + UUID.randomUUID().toString().replace("-", "").substring(0, nextInt);
            String a = a(str3, str);
            try {
                return str3 + a;
            } catch (Exception e) {
                Exception exc2 = e;
                str2 = a;
                exc = exc2;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            str2 = null;
            exc = exc3;
        }
        exc.printStackTrace();
        return str2;
    }

    public static String b(String str) {
        try {
            int intValue = Integer.valueOf(str.substring(0, 1)).intValue();
            return b(intValue + str.substring(1, intValue + 1), str.substring(intValue + 1));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void a(String... strArr) throws Exception {
        System.out.println(a("clientkey", "2882303761517402635"));
        System.out.println(b("clientkey", "oYeE3b1RVTL6tSkO0iK/NXE02FxbRG9YHTlPK6129nQ="));
    }
}
