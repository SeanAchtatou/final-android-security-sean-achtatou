package com.xiaomi.gamecenter.wxwap.fragment;

import android.content.Intent;
import android.net.Uri;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;

/* compiled from: HyWxWapH5Fragment */
class k implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ j b;

    k(j jVar, String str) {
        this.b = jVar;
        this.a = str;
    }

    public void run() {
        try {
            this.b.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a)));
            b.a().a(ResultCode.REP_H5_PAY_CALLEX);
        } catch (Exception e) {
            this.b.a.b((int) ResultCode.WXPAY_ERROR);
            e.printStackTrace();
        }
        synchronized (this.b.a.j) {
            try {
                this.b.a.j.wait();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        this.b.a.getActivity().runOnUiThread(new l(this));
    }
}
