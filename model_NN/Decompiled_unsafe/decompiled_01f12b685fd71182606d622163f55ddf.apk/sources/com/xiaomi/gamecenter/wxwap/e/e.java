package com.xiaomi.gamecenter.wxwap.e;

import com.sg.GameSprites.GameSpriteType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/* compiled from: Base64Utils */
public class e {
    private static final char[] a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    public static String a(byte[] bArr) {
        int length = bArr.length;
        StringBuffer stringBuffer = new StringBuffer((bArr.length * 3) / 2);
        int i = length - 3;
        int i2 = 0;
        int i3 = 0;
        while (i3 <= i) {
            byte b = ((bArr[i3] & 255) << 16) | ((bArr[i3 + 1] & 255) << 8) | (bArr[i3 + 2] & 255);
            stringBuffer.append(a[(b >> 18) & 63]);
            stringBuffer.append(a[(b >> 12) & 63]);
            stringBuffer.append(a[(b >> 6) & 63]);
            stringBuffer.append(a[b & GameSpriteType.f29TYPE_ENEMY_]);
            int i4 = i3 + 3;
            int i5 = i2 + 1;
            if (i2 >= 14) {
                stringBuffer.append(" ");
                i5 = 0;
            }
            i2 = i5;
            i3 = i4;
        }
        if (i3 == (0 + length) - 2) {
            int i6 = ((bArr[i3 + 1] & 255) << 8) | ((bArr[i3] & 255) << 16);
            stringBuffer.append(a[(i6 >> 18) & 63]);
            stringBuffer.append(a[(i6 >> 12) & 63]);
            stringBuffer.append(a[(i6 >> 6) & 63]);
            stringBuffer.append("=");
        } else if (i3 == (0 + length) - 1) {
            int i7 = (bArr[i3] & 255) << 16;
            stringBuffer.append(a[(i7 >> 18) & 63]);
            stringBuffer.append(a[(i7 >> 12) & 63]);
            stringBuffer.append("==");
        }
        return stringBuffer.toString();
    }

    private static int a(char c) {
        if (c >= 'A' && c <= 'Z') {
            return c - 'A';
        }
        if (c >= 'a' && c <= 'z') {
            return (c - 'a') + 26;
        }
        if (c >= '0' && c <= '9') {
            return (c - '0') + 26 + 26;
        }
        switch (c) {
            case '+':
                return 62;
            case '/':
                return 63;
            case '=':
                return 0;
            default:
                throw new RuntimeException("unexpected code: " + c);
        }
    }

    public static byte[] a(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(str, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                System.err.println("Error while decoding BASE64: " + e.toString());
            }
            return byteArray;
        } catch (IOException e2) {
            throw new RuntimeException();
        }
    }

    private static void a(String str, OutputStream outputStream) throws IOException {
        int i = 0;
        int length = str.length();
        while (true) {
            if (i < length && str.charAt(i) <= ' ') {
                i++;
            } else if (i != length) {
                int a2 = (a(str.charAt(i)) << 18) + (a(str.charAt(i + 1)) << 12) + (a(str.charAt(i + 2)) << 6) + a(str.charAt(i + 3));
                outputStream.write((a2 >> 16) & 255);
                if (str.charAt(i + 2) != '=') {
                    outputStream.write((a2 >> 8) & 255);
                    if (str.charAt(i + 3) != '=') {
                        outputStream.write(a2 & 255);
                        i += 4;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }
}
