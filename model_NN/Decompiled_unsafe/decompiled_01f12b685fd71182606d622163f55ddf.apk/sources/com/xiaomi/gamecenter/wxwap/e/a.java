package com.xiaomi.gamecenter.wxwap.e;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: AESEncryption */
public final class a {
    public static byte[] a(byte[] bArr, byte[] bArr2) throws Exception {
        if (bArr2 == null) {
            try {
                System.out.print("Key为空null");
                throw new Exception("Key为空null");
            } catch (Exception e) {
                throw new Exception("AES加密错误", e);
            }
        } else if (bArr2.length != 16) {
            throw new Exception("Key长度不是16位");
        } else {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(1, secretKeySpec);
            return instance.doFinal(bArr);
        }
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) throws Exception {
        if (bArr2 == null) {
            try {
                System.out.print("Key为空null");
                throw new Exception("Key为空null");
            } catch (Exception e) {
                throw new Exception("AES加密错误", e);
            }
        } else if (bArr2.length != 16) {
            throw new Exception("Key长度不是16位");
        } else {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(2, secretKeySpec);
            return instance.doFinal(bArr);
        }
    }

    public static byte[] a(String str, byte[] bArr) throws Exception {
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("Key is null");
        } else if (bArr.length != 16) {
            throw new IllegalArgumentException("Key length != 16");
        } else {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, new IvParameterSpec(l.b(bArr)));
            return instance.doFinal(str.getBytes());
        }
    }

    public static byte[] c(byte[] bArr, byte[] bArr2) throws Exception {
        if (bArr2 == null || bArr2.length == 0) {
            throw new IllegalArgumentException("Key is null");
        } else if (bArr2.length != 16) {
            throw new IllegalArgumentException("Key length != 16位");
        } else {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(2, secretKeySpec, new IvParameterSpec(l.b(bArr2)));
            return instance.doFinal(bArr);
        }
    }

    public static byte[] a(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length % 2 == 1) {
            return null;
        }
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i != length / 2; i++) {
            bArr[i] = (byte) Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16);
        }
        return bArr;
    }

    public static String a(byte[] bArr) {
        String str = "";
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() == 1) {
                str = str + "0" + hexString;
            } else {
                str = str + hexString;
            }
        }
        return str.toUpperCase();
    }

    public static byte[] a(String str, String str2) throws Exception {
        if (str2 == null) {
            try {
                throw new Exception("Key为空null");
            } catch (Exception e) {
                throw new Exception("AES加密错误", e);
            }
        } else if (str2.length() != 16) {
            throw new Exception("Key长度不是16位");
        } else {
            SecretKeySpec secretKeySpec = new SecretKeySpec(str2.getBytes(), "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, new IvParameterSpec(l.b(str2.getBytes())));
            return instance.doFinal(str.getBytes());
        }
    }

    public static byte[] a(byte[] bArr, String str) throws Exception {
        if (str == null) {
            try {
                throw new Exception("Key为空null");
            } catch (Exception e) {
                throw new Exception("AES加密错误", e);
            }
        } else if (str.length() != 16) {
            throw new Exception("Key长度不是16位");
        } else {
            SecretKeySpec secretKeySpec = new SecretKeySpec(str.getBytes(), "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(2, secretKeySpec, new IvParameterSpec(l.b(str.getBytes())));
            return instance.doFinal(bArr);
        }
    }
}
