package com.xiaomi.gamecenter.wxwap.e;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.json.JSONObject;

/* compiled from: FileUtil */
public class i {
    public static boolean a(Context context, String str) {
        if (new File(context.getCacheDir(), str).exists()) {
            return true;
        }
        return false;
    }

    public static boolean a(Context context, String str, String str2) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 0);
            openFileOutput.write(str2.getBytes());
            openFileOutput.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context, String str) {
        File file = new File(context.getFilesDir(), str);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static String c(Context context, String str) {
        Exception e;
        String str2;
        try {
            FileInputStream openFileInput = context.openFileInput(str);
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            str2 = new String(bArr, "utf-8");
            try {
                openFileInput.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return str2;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str2 = "";
            e = exc;
            e.printStackTrace();
            return str2;
        }
        return str2;
    }

    public static String d(Context context, String str) {
        File file = new File(context.getFilesDir(), str);
        if (!file.exists()) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                InputStream open = context.getAssets().open(str);
                byte[] bArr = new byte[1024];
                for (int read = open.read(bArr); read > 0; read = open.read(bArr)) {
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.flush();
                open.close();
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file.getAbsolutePath();
    }

    public static String a(Context context) {
        try {
            InputStream open = context.getAssets().open("meng.sdk.dat");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return new JSONObject(new String(bArr, "utf-8")).getString("cid");
        } catch (Exception e) {
            return "meng_100_1_android";
        }
    }
}
