package com.xiaomi.gamecenter.wxwap.e;

import android.content.Context;

/* compiled from: DensityUtils */
public class h {
    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static int b(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().density) + 0.5f);
    }
}
