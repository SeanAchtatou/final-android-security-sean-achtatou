package com.xiaomi.gamecenter.wxwap.e;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: MD5 */
public final class l {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(byte[] r5) {
        /*
            if (r5 == 0) goto L_0x0044
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            r0.update(r5)     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            byte[] r3 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            r0 = 0
            r1 = r0
        L_0x0016:
            int r0 = r3.length     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            if (r1 >= r0) goto L_0x0033
            byte r0 = r3[r1]     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            if (r0 >= 0) goto L_0x001f
            int r0 = r0 + 256
        L_0x001f:
            r4 = 16
            if (r0 >= r4) goto L_0x0028
            java.lang.String r4 = "0"
            r2.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
        L_0x0028:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0016
        L_0x0033:
            java.lang.String r0 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
            r1 = 8
            r2 = 24
            java.lang.String r0 = r0.substring(r1, r2)     // Catch:{ NoSuchAlgorithmException -> 0x0040 }
        L_0x003f:
            return r0
        L_0x0040:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0044:
            r0 = 0
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.gamecenter.wxwap.e.l.a(byte[]):java.lang.String");
    }

    public static byte[] b(byte[] bArr) {
        if (bArr != null) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(bArr);
                return instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(byte[] r5) {
        /*
            if (r5 == 0) goto L_0x003c
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0.update(r5)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            byte[] r3 = r0.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r0 = 0
            r1 = r0
        L_0x0016:
            int r0 = r3.length     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r1 >= r0) goto L_0x0033
            byte r0 = r3[r1]     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            if (r0 >= 0) goto L_0x001f
            int r0 = r0 + 256
        L_0x001f:
            r4 = 16
            if (r0 >= r4) goto L_0x0028
            java.lang.String r4 = "0"
            r2.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x0028:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0016
        L_0x0033:
            java.lang.String r0 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0038 }
        L_0x0037:
            return r0
        L_0x0038:
            r0 = move-exception
            r0.printStackTrace()
        L_0x003c:
            r0 = 0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.gamecenter.wxwap.e.l.c(byte[]):java.lang.String");
    }

    public static boolean a(byte[] bArr, byte[] bArr2) {
        int length;
        if (bArr == null || bArr2 == null || (length = bArr.length) != bArr.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }
}
