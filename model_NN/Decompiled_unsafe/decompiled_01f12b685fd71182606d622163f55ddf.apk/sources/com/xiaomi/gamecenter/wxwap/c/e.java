package com.xiaomi.gamecenter.wxwap.c;

import android.os.Bundle;
import com.android.volley.VolleyError;
import com.datalab.tools.Constant;
import com.xiaomi.gamecenter.wxwap.a.a;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.config.b;
import com.xiaomi.gamecenter.wxwap.e.j;
import com.xiaomi.gamecenter.wxwap.e.k;
import com.xiaomi.gamecenter.wxwap.e.o;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: PayProtocol */
class e extends a {
    final /* synthetic */ b a;

    e(b bVar) {
        this.a = bVar;
    }

    public void a(String str) {
        try {
            String str2 = new String(o.a(str), "UTF-8");
            com.xiaomi.gamecenter.wxwap.b.a.e("createUndefineOrder--->", str2);
            JSONObject jSONObject = new JSONObject(str2);
            String optString = jSONObject.optString(Constant.SIGN);
            String optString2 = jSONObject.optString("data");
            String optString3 = jSONObject.optString("errorMsg");
            String optString4 = jSONObject.optString("errcode");
            HashMap hashMap = new HashMap();
            hashMap.put("errcode", optString4);
            hashMap.put("errorMsg", optString3);
            hashMap.put("data", optString2);
            com.xiaomi.gamecenter.wxwap.b.a.e("createUndefineOrder", optString2);
            if (!j.a(k.a(hashMap) + "&uri=" + b.c, this.a.d + "&key").equals(optString) || !optString4.equals("200")) {
                com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_CREATE_ORDER_FAIL);
                this.a.j.a((int) ResultCode.CREATE_UNDEFINEORDER_ERROR);
                return;
            }
            String str3 = new String(com.xiaomi.gamecenter.wxwap.e.a.c(o.a(optString2), com.xiaomi.gamecenter.wxwap.e.a.a(com.xiaomi.gamecenter.wxwap.config.a.c)), "UTF-8");
            com.xiaomi.gamecenter.wxwap.b.a.e("onSuccess", str3);
            JSONObject jSONObject2 = new JSONObject(str3);
            String unused = this.a.h = jSONObject2.optString("orderId");
            String unused2 = this.a.g = jSONObject2.optString("feeValue");
            String unused3 = this.a.i = jSONObject2.getString("displayName");
            String optString5 = jSONObject2.optString("paymentList");
            com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_CREATE_ORDER_SUCCESS);
            Bundle bundle = new Bundle();
            bundle.putString("miOrderId", this.a.h);
            bundle.putString("displayName", this.a.i);
            bundle.putString("feeValue", this.a.g);
            bundle.putString("appId", this.a.c);
            bundle.putString("appKey", this.a.d);
            this.a.j.a(bundle, optString5);
        } catch (Exception e) {
            com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_CREATE_UNDEFINEORDER_FAIL);
            e.printStackTrace();
            this.a.j.a((int) ResultCode.CREATE_UNDEFINEORDER_ERROR);
        }
    }

    public void a(VolleyError volleyError) {
        this.a.j.a();
    }
}
