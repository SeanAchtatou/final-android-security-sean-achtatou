package com.xiaomi.gamecenter.wxwap.c;

import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.datalab.tools.Constant;
import com.xiaomi.gamecenter.wxwap.a.a;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.config.b;
import com.xiaomi.gamecenter.wxwap.e.j;
import com.xiaomi.gamecenter.wxwap.e.k;
import com.xiaomi.gamecenter.wxwap.e.o;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: PayProtocol */
class f extends a {
    final /* synthetic */ String a;
    final /* synthetic */ b b;

    f(b bVar, String str) {
        this.b = bVar;
        this.a = str;
    }

    public void a(String str) {
        try {
            String str2 = new String(o.a(str), "UTF-8");
            com.xiaomi.gamecenter.wxwap.b.a.e("getPayInfo", str2);
            JSONObject jSONObject = new JSONObject(str2);
            String optString = jSONObject.optString(Constant.SIGN);
            String optString2 = jSONObject.optString("data");
            String optString3 = jSONObject.optString("errorMsg");
            String optString4 = jSONObject.optString("errcode");
            HashMap hashMap = new HashMap();
            hashMap.put("errcode", optString4);
            hashMap.put("errorMsg", optString3);
            hashMap.put("data", optString2);
            com.xiaomi.gamecenter.wxwap.b.a.e("data", optString2);
            if (!j.a(k.a(hashMap) + "&uri=" + b.e, this.b.d + "&key").equals(optString) || !optString4.equals("200")) {
                com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.GET_PAYINTO_ERROR);
                this.b.j.a((int) ResultCode.GET_PAYINTO_ERROR);
                return;
            }
            String str3 = new String(com.xiaomi.gamecenter.wxwap.e.a.c(o.a(optString2), com.xiaomi.gamecenter.wxwap.e.a.a(com.xiaomi.gamecenter.wxwap.config.a.c)), "UTF-8");
            com.xiaomi.gamecenter.wxwap.b.a.e("onSuccess", str3);
            JSONObject jSONObject2 = new JSONObject(str3);
            String optString5 = jSONObject2.optString("schemeUrl");
            String optString6 = jSONObject2.optString("codeUrl");
            String optString7 = jSONObject2.optString("mwebUrl");
            String optString8 = jSONObject2.optString("referer");
            com.xiaomi.gamecenter.wxwap.b.a.e("mwebUrl", optString7);
            com.xiaomi.gamecenter.wxwap.b.a.e("referer", optString8);
            if (!TextUtils.isEmpty(optString5)) {
                this.b.j.a(this.a, optString5, null);
                com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_GET_PAYINFO_WX_SUCCESS);
            } else if (!TextUtils.isEmpty(optString7)) {
                com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_GET_PAYINFO_WX_SUCCESS);
                this.b.j.a(this.a, optString7, optString8);
            } else {
                com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_GET_PAYINFO_SCAN_SUCCESS);
                this.b.j.a(this.a, optString6, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.b.j.a((int) ResultCode.GET_PAYINTO_ERROR);
        }
    }

    public void a(VolleyError volleyError) {
        this.b.j.a();
    }
}
