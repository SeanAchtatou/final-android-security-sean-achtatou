package com.xiaomi.gamecenter.wxwap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.WindowManager;
import com.xiaomi.gamecenter.wxwap.fragment.HyWxScanFragment;
import com.xiaomi.gamecenter.wxwap.fragment.HyWxWapH5Fragment;
import com.xiaomi.gamecenter.wxwap.model.AppInfo;
import java.util.Arrays;

public class HyWxWappayActivity extends Activity {
    private Bundle bundle;
    private Fragment mCurrentFragment;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle2) {
        super.onCreate(bundle2);
        this.bundle = getIntent().getBundleExtra("_bundle");
        AppInfo appInfo = (AppInfo) this.bundle.getSerializable("_appinfo");
        if (Arrays.toString(appInfo.getPaymentList()).contains("WXMWEB") || Arrays.toString(appInfo.getPaymentList()).contains("WXAPP")) {
            addWapH5Fragment();
            hideWindow();
            return;
        }
        addNativeFragment();
    }

    private void hideWindow() {
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.height = 0;
        attributes.width = 0;
    }

    private void addNativeFragment() {
        FragmentTransaction beginTransaction = getFragmentManager().beginTransaction();
        this.mCurrentFragment = new HyWxScanFragment();
        this.mCurrentFragment.setArguments(this.bundle);
        beginTransaction.add(16908290, this.mCurrentFragment, HyWxScanFragment.d);
        beginTransaction.commit();
    }

    private void addWapH5Fragment() {
        FragmentTransaction beginTransaction = getFragmentManager().beginTransaction();
        this.mCurrentFragment = new HyWxWapH5Fragment();
        this.mCurrentFragment.setArguments(this.bundle);
        beginTransaction.add(16908290, this.mCurrentFragment, HyWxWapH5Fragment.d);
        beginTransaction.commit();
    }
}
