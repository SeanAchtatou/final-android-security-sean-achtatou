package com.xiaomi.gamecenter.wxwap.model;

import java.io.Serializable;

public class BundleInfo implements Serializable {
    private String appid;
    private String appkey;
    private long callId;

    public String getAppid() {
        return this.appid;
    }

    public void setAppid(String str) {
        this.appid = str;
    }

    public String getAppkey() {
        return this.appkey;
    }

    public void setAppkey(String str) {
        this.appkey = str;
    }

    public long getCallId() {
        return this.callId;
    }

    public void setCallId(long j) {
        this.callId = j;
    }
}
