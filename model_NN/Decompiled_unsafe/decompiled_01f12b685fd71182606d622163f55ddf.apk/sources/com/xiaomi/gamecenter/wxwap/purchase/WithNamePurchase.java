package com.xiaomi.gamecenter.wxwap.purchase;

public class WithNamePurchase extends Purchase {
    private String chargeCode = "0101";
    private String feeValue;
    private String purchaseName;

    public String getChargeCode() {
        return this.chargeCode;
    }

    public void setChargeCode(String str) {
        this.chargeCode = str;
    }

    public String getPurchaseName() {
        return this.purchaseName;
    }

    public void setPurchaseName(String str) {
        this.purchaseName = str;
    }

    public String getFeeValue() {
        return this.feeValue;
    }

    public void setFeeValue(String str) {
        this.feeValue = str;
    }
}
