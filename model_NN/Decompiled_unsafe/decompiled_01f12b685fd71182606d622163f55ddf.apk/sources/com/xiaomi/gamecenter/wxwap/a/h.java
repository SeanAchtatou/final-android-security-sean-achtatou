package com.xiaomi.gamecenter.wxwap.a;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import java.util.Map;

/* compiled from: RestClient */
final class h extends StringRequest {
    final /* synthetic */ Map a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(int i, String str, Response.Listener listener, Response.ErrorListener errorListener, Map map) {
        super(i, str, listener, errorListener);
        this.a = map;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> getParams() throws AuthFailureError {
        return this.a;
    }
}
