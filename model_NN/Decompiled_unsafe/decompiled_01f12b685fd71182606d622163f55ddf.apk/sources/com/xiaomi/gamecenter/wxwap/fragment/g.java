package com.xiaomi.gamecenter.wxwap.fragment;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.xiaomi.gamecenter.sdk.service.wxpay.IWxAppPay;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;

/* compiled from: HyWxWapH5Fragment */
class g implements ServiceConnection {
    final /* synthetic */ HyWxWapH5Fragment a;

    g(HyWxWapH5Fragment hyWxWapH5Fragment) {
        this.a = hyWxWapH5Fragment;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        b.a().a(ResultCode.REP_WXAPP_PAY_CONNECT_SERVICE_SUCCESS);
        IWxAppPay unused = this.a.n = IWxAppPay.Stub.asInterface(iBinder);
        new h(this).start();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        ServiceConnection unused = this.a.m = (ServiceConnection) null;
        b.a().a(ResultCode.REP_WXAPP_PAY_CONNECT_SERVICE_FAIL);
        this.a.b((int) ResultCode.WXPAY_ERROR);
    }
}
