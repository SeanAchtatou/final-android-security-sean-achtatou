package com.xiaomi.gamecenter.wxwap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.xiaomi.gamecenter.wxwap.d.a;
import com.xiaomi.gamecenter.wxwap.d.b;
import com.xiaomi.gamecenter.wxwap.e.k;
import com.xiaomi.gamecenter.wxwap.e.m;
import com.xiaomi.gamecenter.wxwap.model.AppInfo;
import com.xiaomi.gamecenter.wxwap.model.CallModel;
import com.xiaomi.gamecenter.wxwap.purchase.Purchase;

public class HyWxWapPay {
    private static volatile HyWxWapPay mInstance;
    private String appid;
    private String appkey;
    private Context mContext;

    public static void init(Context context, String str, String str2) {
        if (mInstance == null) {
            m.b(context.getApplicationContext());
            mInstance = new HyWxWapPay(context, str, str2);
        }
    }

    public static HyWxWapPay getInstance() {
        if (mInstance != null) {
            return mInstance;
        }
        throw new IllegalStateException("please HyWxWapPay.init() in application before use this method");
    }

    public HyWxWapPay(Context context, String str, String str2) {
        this.appid = str;
        this.appkey = str2;
        this.mContext = context;
        b.a(context, str);
        b.a().c();
        a.a(context);
        com.xiaomi.gamecenter.wxwap.a.b.a(context);
    }

    public void pay(Activity activity, Purchase purchase, PayResultCallback payResultCallback) {
        b.a().b();
        Intent intent = new Intent(activity, HyWxWappayActivity.class);
        AppInfo appInfo = new AppInfo();
        appInfo.setAppid(this.appid);
        appInfo.setAppkey(this.appkey);
        appInfo.setCallId(CallModel.add(payResultCallback));
        if (!k.a(this.mContext) || k.d(activity) <= 570556416) {
            appInfo.setPaymentList(new String[]{"WXNATIVE"});
        } else if (k.c(activity)) {
            appInfo.setPaymentList(new String[]{"WXMWEB", "WXAPP"});
        } else {
            appInfo.setPaymentList(new String[]{"WXMWEB"});
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("_appinfo", appInfo);
        bundle.putSerializable("_purchase", purchase);
        intent.putExtra("_bundle", bundle);
        activity.startActivity(intent);
    }
}
