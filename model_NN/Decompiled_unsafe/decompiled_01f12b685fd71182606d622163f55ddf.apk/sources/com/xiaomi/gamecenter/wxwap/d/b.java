package com.xiaomi.gamecenter.wxwap.d;

import android.content.Context;
import com.wali.gamecenter.report.ReportManager;
import com.wali.gamecenter.report.ReportType;
import com.wali.gamecenter.report.model.Bid522Report;
import com.wali.gamecenter.report.model.XmsdkReport;
import com.wali.gamecenter.report.utils.MD5;
import com.xiaomi.gamecenter.wxwap.config.a;
import com.xiaomi.gamecenter.wxwap.e.i;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/* compiled from: ReporterUtils */
public class b {
    private static b b;
    private static String c;
    private static Map<String, String> e = new HashMap();
    private Context a;
    private String d = MD5.getMD5(UUID.randomUUID().toString().getBytes());

    private b(Context context) {
        this.a = context;
    }

    public static void a(Context context, String str) {
        if (b == null) {
            ReportManager.Init(context.getApplicationContext());
            b = new b(context);
        }
        c = str;
    }

    public static b a() {
        return b;
    }

    public void b() {
        this.d = MD5.getMD5(UUID.randomUUID().toString().getBytes());
    }

    public void a(int i) {
        XmsdkReport xmsdkReport = new XmsdkReport(this.a);
        xmsdkReport.setAppid(c);
        xmsdkReport.setNum(i + "");
        xmsdkReport.setClient(a.a);
        xmsdkReport.setCpChannel(i.a(this.a));
        xmsdkReport.ver = a.b;
        xmsdkReport.index = this.d;
        xmsdkReport.send();
    }

    public void a(String str, int i) {
        XmsdkReport xmsdkReport = new XmsdkReport(this.a);
        xmsdkReport.setAppid(c);
        xmsdkReport.setNum(i + "");
        xmsdkReport.setClient(a.a);
        xmsdkReport.setCpChannel(i.a(this.a));
        xmsdkReport.ver = a.b;
        xmsdkReport.index = str;
        xmsdkReport.send();
    }

    public void a(String str, ReportType reportType, int i) {
        XmsdkReport xmsdkReport = new XmsdkReport(this.a);
        xmsdkReport.setAppid(c);
        xmsdkReport.setNum(i + "");
        xmsdkReport.setType(reportType);
        xmsdkReport.setClient(a.a);
        xmsdkReport.setCpChannel(i.a(this.a));
        xmsdkReport.ver = a.b;
        xmsdkReport.index = str;
        xmsdkReport.send();
    }

    public void c() {
        if (!e.containsKey(this.a.getPackageName())) {
            Bid522Report bid522Report = new Bid522Report(this.a);
            bid522Report.setAppid(c);
            bid522Report.ver = a.b;
            bid522Report.setChannelId(i.a(this.a));
            bid522Report.getExt().from = a.a;
            bid522Report.send();
        }
    }

    static {
        e.put("com.xiaomi.gamecenter.sdk.service", "SDK");
        e.put("com.xiaomi.gamecenter", "游戏中心");
    }
}
