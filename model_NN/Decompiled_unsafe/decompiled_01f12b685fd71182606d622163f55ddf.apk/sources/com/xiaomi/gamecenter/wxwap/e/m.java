package com.xiaomi.gamecenter.wxwap.e;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.regex.Pattern;

/* compiled from: MiUtils */
public class m {
    static String a = null;
    static String b = null;
    static String c = null;
    static String d = null;

    public static boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        if (connectivityManager.getActiveNetworkInfo() == null) {
            return false;
        }
        return true;
    }

    public static PublicKey a(byte[] bArr) throws CertificateException {
        return a(new ByteArrayInputStream(bArr));
    }

    public static PublicKey a(String str) throws CertificateException {
        return a(new ByteArrayInputStream(str.getBytes()));
    }

    private static PublicKey a(InputStream inputStream) throws CertificateException {
        return CertificateFactory.getInstance("X.509").generateCertificate(inputStream).getPublicKey();
    }

    private m() {
    }

    public static void b(Context context) {
        d = d(context);
    }

    public static String a() {
        return d;
    }

    public static String c(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(b("ro.product.manufacturer"));
        stringBuffer.append("|");
        stringBuffer.append(b("ro.product.model"));
        stringBuffer.append("|");
        stringBuffer.append(b("ro.build.version.release"));
        stringBuffer.append("|");
        stringBuffer.append(b("ro.build.display.id"));
        stringBuffer.append("|");
        stringBuffer.append(b("ro.build.version.sdk"));
        stringBuffer.append("|");
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        stringBuffer.append(TextUtils.isEmpty(telephonyManager.getDeviceId()) ? "" : telephonyManager.getDeviceId());
        return stringBuffer.toString();
    }

    public static String d(Context context) {
        if (d == null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(b("ro.product.manufacturer"));
            stringBuffer.append("|");
            stringBuffer.append(b("ro.product.model"));
            stringBuffer.append("|");
            stringBuffer.append(b());
            stringBuffer.append("|");
            stringBuffer.append(b("ro.build.display.id"));
            stringBuffer.append("|");
            stringBuffer.append(b("ro.build.version.sdk"));
            stringBuffer.append("|");
            stringBuffer.append(b("ro.product.device"));
            d = stringBuffer.toString();
        }
        return d;
    }

    private static String b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(b("ro.build.version.release"));
        stringBuffer.append("_");
        if (c()) {
            stringBuffer.append("alpha");
        } else if (d()) {
            stringBuffer.append("develop");
        } else if (e()) {
            stringBuffer.append("stable");
        } else {
            stringBuffer.append("na");
        }
        stringBuffer.append("_");
        stringBuffer.append(Build.VERSION.INCREMENTAL);
        return stringBuffer.toString();
    }

    private static boolean c() {
        try {
            return b("ro.product.mod_device").endsWith("_alpha");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean d() {
        try {
            String str = Build.VERSION.INCREMENTAL;
            if (TextUtils.isEmpty(str) || !str.matches("\\d+.\\d+.\\d+(-internal)?")) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean e() {
        if (!"user".equals(Build.TYPE) || d()) {
            return false;
        }
        return true;
    }

    public static String b(String str) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class).invoke(null, str);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean c(String str) {
        if (str == null) {
            return false;
        }
        return Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,2,5-9]))\\d{8}$").matcher(str).matches();
    }
}
