package com.xiaomi.gamecenter.wxwap.purchase;

public class OrderPurchase extends Purchase {
    private String displayName;
    private String feeValue;
    private String miOrderId;

    public String getMiOrderId() {
        return this.miOrderId;
    }

    public void setMiOrderId(String str) {
        this.miOrderId = str;
    }

    public String getFeeValue() {
        return this.feeValue;
    }

    public void setFeeValue(String str) {
        this.feeValue = str;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String str) {
        this.displayName = str;
    }
}
