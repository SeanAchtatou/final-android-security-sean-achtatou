package com.xiaomi.gamecenter.wxwap.model;

public final class ServiceToken {
    private String akey;
    private String key;
    private String miid;
    private String session;
    private String t;
    private String uid;

    public String getUid() {
        return this.uid;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public String getT() {
        return this.t;
    }

    public void setT(String str) {
        this.t = str;
    }

    public String getSession() {
        return this.session;
    }

    public void setSession(String str) {
        this.session = str;
    }

    public String getAkey() {
        return this.akey;
    }

    public void setAkey(String str) {
        this.akey = str;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public String getMiid() {
        return this.miid;
    }

    public void setMiid(String str) {
        this.miid = str;
    }
}
