package com.xiaomi.gamecenter.wxwap.c;

import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.xiaomi.gamecenter.wxwap.a.a;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.model.TokenManager;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PayProtocol */
class c extends a {
    final /* synthetic */ b a;

    c(b bVar) {
        this.a = bVar;
    }

    public void a(String str) {
        try {
            com.xiaomi.gamecenter.wxwap.b.a.a("getSession" + str);
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("errcode");
            jSONObject.optString("result");
            if (TextUtils.equals(optString, "0")) {
                TokenManager.getInstance().save2File(this.a.b, str);
                this.a.j.b("");
                return;
            }
            this.a.j.a((int) ResultCode.GET_SESSION_ERROR);
        } catch (JSONException e) {
            e.printStackTrace();
            this.a.j.a((int) ResultCode.GET_SESSION_ERROR);
        }
    }

    public void a(VolleyError volleyError) {
        volleyError.printStackTrace();
        this.a.j.a();
    }
}
