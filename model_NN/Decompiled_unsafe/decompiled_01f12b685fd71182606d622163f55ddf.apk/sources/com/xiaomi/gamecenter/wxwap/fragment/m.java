package com.xiaomi.gamecenter.wxwap.fragment;

import android.content.Intent;
import android.net.Uri;

/* compiled from: HyWxWapH5Fragment */
class m implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ HyWxWapH5Fragment b;

    m(HyWxWapH5Fragment hyWxWapH5Fragment, String str) {
        this.b = hyWxWapH5Fragment;
        this.a = str;
    }

    public void run() {
        this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a)));
        synchronized (this.b.j) {
            try {
                this.b.j.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.b.getActivity().runOnUiThread(new n(this));
    }
}
