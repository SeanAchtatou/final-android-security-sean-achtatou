package com.xiaomi.gamecenter.wxwap.fragment;

import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.xiaomi.gamecenter.sdk.service.wxpay.ActionTransfor;
import com.xiaomi.gamecenter.sdk.service.wxpay.IWxAppPay;
import com.xiaomi.gamecenter.sdk.service.wxpay.IWxPayCallback;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;
import com.xiaomi.gamecenter.wxwap.model.TokenManager;
import java.net.URLDecoder;
import org.json.JSONException;
import org.json.JSONObject;

public class HyWxWapH5Fragment extends BaseFragment {
    public static final String d = "HyWxH5WapFragment";
    private static final String e = "com.xiaomi.gamecenter.sdk.service";
    private static final String f = "com.xiaomi.gamecenter.sdk.service.pay.wxapp.new";
    private static final int g = 0;
    private static final int h = -18003;
    private static final int i = -12;
    /* access modifiers changed from: private */
    public byte[] j = new byte[0];
    private RelativeLayout k;
    private WebView l;
    /* access modifiers changed from: private */
    public ServiceConnection m;
    /* access modifiers changed from: private */
    public IWxAppPay n;
    /* access modifiers changed from: private */
    public Bundle o;
    /* access modifiers changed from: private */
    public int p = -1;
    /* access modifiers changed from: private */
    public IWxPayCallback q = new IWxPayCallback.Stub() {
        public void startActivity(String str, String str2, Bundle bundle) throws RemoteException {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            bundle.setClassLoader(ActionTransfor.DataAction.class.getClassLoader());
            intent.putExtras(bundle);
            intent.setClassName(str, str2);
            HyWxWapH5Fragment.this.getActivity().startActivity(intent);
        }
    };

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b.a().a(123);
    }

    public void a(Bundle bundle, String str) {
        if (str.contains("WXAPP")) {
            String uid = TokenManager.getInstance().getToken(getActivity()).getUid();
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("openId", uid);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            bundle.putString("userInfo", jSONObject.toString());
            this.o = new Bundle(bundle);
            Intent intent = new Intent();
            intent.setAction(f);
            intent.setPackage(e);
            getActivity().getApplicationContext().bindService(intent, this.m, 1);
            b.a().a(ResultCode.REP_WXAPP_PAY_CONNECT_SERVICE);
        } else {
            this.a.a("WXMWEB");
        }
        b.a().a(ResultCode.REP_H5_PAY_CREATEORDER);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.k = new RelativeLayout(getActivity());
        this.k.setBackgroundColor(-1579033);
        this.k.setGravity(17);
        this.m = new g(this);
        return this.k;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        b.a().a(ResultCode.REP_H5_PAY_CREATEVIEW);
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setGravity(1);
        linearLayout.setOrientation(1);
        this.k.addView(linearLayout, new RelativeLayout.LayoutParams(-1, -1));
        this.l = new WebView(getActivity());
        j jVar = new j(this);
        this.l.getSettings().setJavaScriptEnabled(true);
        this.l.setWebViewClient(jVar);
        linearLayout.addView(this.l, new LinearLayout.LayoutParams(-1, -1));
    }

    public void a(String str, String str2, String str3) {
        if (str3 == null) {
            new Thread(new m(this, str2)).start();
            return;
        }
        String decode = URLDecoder.decode(str2);
        this.l.loadDataWithBaseURL(URLDecoder.decode(str3), "<script>\n        window.location.href=\"" + decode + "\";\n" + "    </script>", "text/html", "UTF-8", null);
        b.a().a(ResultCode.REP_H5_PAY_ONPAY);
    }

    public void onResume() {
        super.onResume();
        synchronized (this.j) {
            this.j.notify();
        }
    }
}
