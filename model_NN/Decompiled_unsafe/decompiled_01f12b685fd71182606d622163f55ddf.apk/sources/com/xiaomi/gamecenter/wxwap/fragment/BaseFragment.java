package com.xiaomi.gamecenter.wxwap.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.xiaomi.gamecenter.wxwap.PayResultCallback;
import com.xiaomi.gamecenter.wxwap.c.a;
import com.xiaomi.gamecenter.wxwap.c.b;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.model.AppInfo;
import com.xiaomi.gamecenter.wxwap.model.CallModel;
import com.xiaomi.gamecenter.wxwap.model.TokenManager;
import com.xiaomi.gamecenter.wxwap.purchase.OrderPurchase;
import com.xiaomi.gamecenter.wxwap.purchase.Purchase;
import java.util.Arrays;

public class BaseFragment extends Fragment implements a {
    protected b a;
    public ProgressDialog b;
    public CountDownTimer c;
    private String[] d;
    private Purchase e;
    private PayResultCallback f;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle bundleExtra = getActivity().getIntent().getBundleExtra("_bundle");
        AppInfo appInfo = (AppInfo) bundleExtra.getSerializable("_appinfo");
        this.d = appInfo.getPaymentList();
        this.e = (Purchase) bundleExtra.getSerializable("_purchase");
        this.f = CallModel.pop(appInfo.getCallId());
        this.b = new ProgressDialog(getActivity());
        this.b.setMessage("正在获取订单信息...");
        this.b.setCancelable(false);
        this.b.show();
        this.a = new b(getActivity(), appInfo, this.e);
        this.a.a(this);
        if (TokenManager.getInstance().isExist(getActivity())) {
            b();
        } else {
            this.a.a();
        }
    }

    public void a() {
        b((int) ResultCode.NET_ERROR);
    }

    public void a(String str) {
        if (str.equals(Integer.valueOf((int) ResultCode.VERIFY_ERROR)) || str.equals(Integer.valueOf((int) ResultCode.VERIFY_FAIL)) || str.equals("406")) {
            b();
            return;
        }
        this.b.dismiss();
        Bundle bundle = new Bundle();
        String uid = TokenManager.getInstance().getToken(getActivity()).getUid();
        bundle.putString("_url", "https://hysdk.game.xiaomi.com/realname/index.jsp?fuid=" + uid + "&sid=" + TokenManager.getInstance().getToken(getActivity()).getSession());
        bundle.putInt("_code", Integer.parseInt(str));
        HyWebFragment hyWebFragment = new HyWebFragment();
        hyWebFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().add(16908290, hyWebFragment, "WEBVIEW").commit();
        hyWebFragment.a(new a(this));
    }

    public void b(String str) {
        b();
    }

    public void a(int i) {
        b(i);
    }

    public void a(Bundle bundle, String str) {
        this.a.a(str);
    }

    public void a(String str, String str2, String str3) {
    }

    public void c(String str) {
        if (!str.equals("TRADE_SUCCESS")) {
            return;
        }
        if (Arrays.toString(this.d).contains("WXMWEB")) {
            com.xiaomi.gamecenter.wxwap.d.b.a().a(124);
            b((int) ResultCode.WXPAY_SUCCESS);
            return;
        }
        com.xiaomi.gamecenter.wxwap.d.b.a().a(127);
        b((int) ResultCode.SCANPAY_SUCCESS);
    }

    /* access modifiers changed from: protected */
    public void a(long j, long j2) {
        this.b.setMessage("正在查询订单信息...");
        if (this.c == null) {
            this.c = new b(this, j, j2);
            this.c.start();
        }
    }

    public void b(int i) {
        this.a.d();
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
        }
        if (this.c != null) {
            this.c.cancel();
            this.c = null;
        }
        if (i == -701 || i == -704) {
            this.f.onSuccess(this.e.getCpOrderId());
        } else {
            this.f.onError(i, ResultCode.errorMap.get(Integer.valueOf(i)));
        }
        getActivity().finish();
        getActivity().overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.e instanceof OrderPurchase) {
            this.a.a(this.d[0]);
        } else {
            this.a.a(this.d);
        }
    }
}
