package com.xiaomi.gamecenter.wxwap.model;

import java.io.Serializable;

public class AppInfo implements Serializable {
    private String appid;
    private String appkey;
    private long callId;
    private String[] paymentList;

    public String getAppid() {
        return this.appid;
    }

    public void setAppid(String str) {
        this.appid = str;
    }

    public String getAppkey() {
        return this.appkey;
    }

    public void setAppkey(String str) {
        this.appkey = str;
    }

    public long getCallId() {
        return this.callId;
    }

    public void setCallId(long j) {
        this.callId = j;
    }

    public String[] getPaymentList() {
        return this.paymentList;
    }

    public void setPaymentList(String[] strArr) {
        this.paymentList = strArr;
    }
}
