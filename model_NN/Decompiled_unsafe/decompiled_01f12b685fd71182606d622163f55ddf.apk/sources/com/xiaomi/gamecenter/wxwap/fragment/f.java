package com.xiaomi.gamecenter.wxwap.fragment;

import android.view.KeyEvent;
import android.view.View;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;

/* compiled from: HyWxScanFragment */
class f implements View.OnKeyListener {
    final /* synthetic */ HyWxScanFragment a;

    f(HyWxScanFragment hyWxScanFragment) {
        this.a = hyWxScanFragment;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 1) {
            return false;
        }
        this.a.a.d();
        b.a().a(128);
        this.a.b((int) ResultCode.SCANPAY_CANCEL);
        return true;
    }
}
