package com.xiaomi.gamecenter.wxwap.c;

import com.android.volley.VolleyError;
import com.datalab.tools.Constant;
import com.xiaomi.gamecenter.wxwap.a.a;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.config.b;
import com.xiaomi.gamecenter.wxwap.e.j;
import com.xiaomi.gamecenter.wxwap.e.k;
import com.xiaomi.gamecenter.wxwap.e.o;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: PayProtocol */
class g extends a {
    final /* synthetic */ b a;

    g(b bVar) {
        this.a = bVar;
    }

    public void a(String str) {
        try {
            String str2 = new String(o.a(str), "UTF-8");
            com.xiaomi.gamecenter.wxwap.b.a.e("queryOrder", str2);
            JSONObject jSONObject = new JSONObject(str2);
            String optString = jSONObject.optString(Constant.SIGN);
            String optString2 = jSONObject.optString("data");
            String optString3 = jSONObject.optString("errorMsg");
            String optString4 = jSONObject.optString("errcode");
            HashMap hashMap = new HashMap();
            hashMap.put("errcode", optString4);
            hashMap.put("errorMsg", optString3);
            hashMap.put("data", optString2);
            com.xiaomi.gamecenter.wxwap.b.a.e("data", optString2);
            if (!j.a(k.a(hashMap) + "&uri=" + b.g, this.a.d + "&key").equals(optString) || !optString4.equals("200")) {
                com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_QUERY_ORDER_FAIL);
                this.a.j.a((int) ResultCode.QUERY_ORDER_ERROR);
                return;
            }
            String str3 = new String(com.xiaomi.gamecenter.wxwap.e.a.c(o.a(optString2), com.xiaomi.gamecenter.wxwap.e.a.a(com.xiaomi.gamecenter.wxwap.config.a.c)), "UTF-8");
            com.xiaomi.gamecenter.wxwap.b.a.e("onSuccess", str3);
            String optString5 = new JSONObject(str3).optString("status");
            com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_QUERY_ORDER_SUCCESS);
            this.a.j.c(optString5);
        } catch (Exception e) {
            e.printStackTrace();
            this.a.j.a((int) ResultCode.QUERY_ORDER_ERROR);
        }
    }

    public void a(VolleyError volleyError) {
        this.a.j.a();
    }
}
