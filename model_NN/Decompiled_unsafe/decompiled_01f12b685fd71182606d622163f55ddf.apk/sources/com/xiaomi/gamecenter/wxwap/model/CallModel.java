package com.xiaomi.gamecenter.wxwap.model;

import com.xiaomi.gamecenter.wxwap.PayResultCallback;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class CallModel implements Serializable {
    private static Map map = Collections.synchronizedMap(new HashMap());
    private static AtomicLong value = new AtomicLong();

    public static long add(PayResultCallback payResultCallback) {
        if (payResultCallback == null) {
            return 0;
        }
        long incrementAndGet = value.incrementAndGet();
        map.put(Long.valueOf(incrementAndGet), payResultCallback);
        return incrementAndGet;
    }

    public static PayResultCallback pop(long j) {
        return (PayResultCallback) map.remove(Long.valueOf(j));
    }

    public static boolean isExist(long j) {
        return map.containsKey(Long.valueOf(j));
    }
}
