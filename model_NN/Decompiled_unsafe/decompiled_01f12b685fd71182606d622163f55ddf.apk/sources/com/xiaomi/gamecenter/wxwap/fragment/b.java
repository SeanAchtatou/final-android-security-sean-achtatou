package com.xiaomi.gamecenter.wxwap.fragment;

import android.os.CountDownTimer;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;

/* compiled from: BaseFragment */
class b extends CountDownTimer {
    final /* synthetic */ BaseFragment a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    b(BaseFragment baseFragment, long j, long j2) {
        super(j, j2);
        this.a = baseFragment;
    }

    public void onTick(long j) {
        this.a.a.c();
    }

    public void onFinish() {
        com.xiaomi.gamecenter.wxwap.d.b.a().a(125);
        this.a.b((int) ResultCode.WXPAY_CANCEL);
    }
}
