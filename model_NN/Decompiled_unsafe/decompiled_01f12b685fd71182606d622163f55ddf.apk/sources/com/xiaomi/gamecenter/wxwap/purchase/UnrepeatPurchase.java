package com.xiaomi.gamecenter.wxwap.purchase;

public class UnrepeatPurchase extends Purchase {
    private String chargeCode;

    public String getChargeCode() {
        return this.chargeCode;
    }

    public void setChargeCode(String str) {
        this.chargeCode = str;
    }
}
