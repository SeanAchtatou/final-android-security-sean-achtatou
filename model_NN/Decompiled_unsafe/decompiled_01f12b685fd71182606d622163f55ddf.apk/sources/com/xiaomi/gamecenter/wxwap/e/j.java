package com.xiaomi.gamecenter.wxwap.e;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: HmacSHA1Encryption */
public final class j {
    private static final String a = "HmacSHA1";
    private static final String b = "UTF-8";

    public static String a(String str, String str2) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(str2.getBytes(b), a);
        Mac instance = Mac.getInstance(a);
        instance.init(secretKeySpec);
        return a(instance.doFinal(str.getBytes(b))).toString();
    }

    public static String a(byte[] bArr, String str) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(str.getBytes(b), a);
        Mac instance = Mac.getInstance(a);
        instance.init(secretKeySpec);
        return a(instance.doFinal(bArr)).toString();
    }

    public static StringBuilder a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02x", Byte.valueOf(bArr[i])));
        }
        return sb;
    }

    public static void a(String[] strArr) {
        try {
            System.out.print(a("p=evOlL3w2vL_FRgFvAnMed47cOh1woWx9PCnlPWUERVYh_PUAz2J-PTxgIoE74gCMCewf3JRTxqCMq49f5KQdYenjCjUUK-138Bi8U6H-_vfnJxaqBZNIdcO5bKE5yk2Fc2MVtUqhGIp5_2fPgVDTPlLEtIwJXpmQYfds8Bp9GwvgwfNoT51I06kxQ1nZVK66xHY0jKYko-eHRuv3vFz7QMdWZnujC5U8dYwPOFQDdbnWgnPdp5uNFm5NGdfZdMBQiicTzZVYOxu6ojKOzyB0to9j59kkcES4sfnzycMYjr2E9pmKRtm-Y6kfmsDMIQoeMBho3ikTkzqs3Rr7uQiD9ASp_HwaEl_x_5UguTR4A5nsqJG1LWKZc66s1Y2zFwDYqYi7p1vxj1_cPq_V5FyqMznaTah1JwckOUr3mSpg6K1SLBHSOZhNf7PtTAvbyx-9YzqGY-YEBB-MV6wVedToHYGWAgHXnLnIbFXa06oKx_o.&session=niynZix5mWYZc6o22U&uid=118795252&uri=​/order-manager/order/v1/createWeChatOrder", "5151740219635&key"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
