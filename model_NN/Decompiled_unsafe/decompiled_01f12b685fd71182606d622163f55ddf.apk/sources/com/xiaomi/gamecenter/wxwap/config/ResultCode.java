package com.xiaomi.gamecenter.wxwap.config;

import java.util.HashMap;
import java.util.Map;

public class ResultCode {
    public static final int CREATE_UNDEFINEORDER_ERROR = -708;
    public static final int GET_PAYINTO_ERROR = -709;
    public static final int GET_SESSION_ERROR = -707;
    public static final int NET_ERROR = -700;
    public static final int QUERY_ORDER_ERROR = -710;
    public static final int REP_CREATE_ORDER_CALL = 3010;
    public static final int REP_CREATE_ORDER_FAIL = 3013;
    public static final int REP_CREATE_ORDER_SUCCESS = 3011;
    public static final int REP_CREATE_UNDEFINEORDER_FAIL = 3012;
    public static final int REP_GET_PAYINFO_SCAN_SUCCESS = 3023;
    public static final int REP_GET_PAYINFO_WX_CALL = 3015;
    public static final int REP_GET_PAYINFO_WX_SUCCESS = 3016;
    public static final int REP_H5_PAY_CALLEX = 3054;
    public static final int REP_H5_PAY_CANCEL = 3056;
    public static final int REP_H5_PAY_CREATEORDER = 3051;
    public static final int REP_H5_PAY_CREATEVIEW = 3052;
    public static final int REP_H5_PAY_ONPAY = 3053;
    public static final int REP_H5_PAY_QUERYPAY = 3055;
    public static final int REP_QUERY_ORDER_CALL = 3024;
    public static final int REP_QUERY_ORDER_FAIL = 3026;
    public static final int REP_QUERY_ORDER_SUCCESS = 3025;
    public static final int REP_SCANPAY_CALL = 126;
    public static final int REP_SCANPAY_CANCEL = 128;
    public static final int REP_SCANPAY_SUCCESS = 127;
    public static final int REP_VERIFY_CLICK_BACK = 3032;
    public static final int REP_VERIFY_CLICK_CLOSE = 3031;
    public static final int REP_VERIFY_CLICK_DIALOG_GOSIGN = 3034;
    public static final int REP_VERIFY_CLICK_DIALOG_SURE = 3033;
    public static final int REP_VERIFY_SHOWPAGE = 3030;
    public static final int REP_VERIFY_SUCCESS = 3035;
    public static final int REP_WXAPP_PAY_CANCLE = 167;
    public static final int REP_WXAPP_PAY_CONNECT_SERVICE = 3057;
    public static final int REP_WXAPP_PAY_CONNECT_SERVICE_FAIL = 3058;
    public static final int REP_WXAPP_PAY_CONNECT_SERVICE_SUCCESS = 3059;
    public static final int REP_WXAPP_PAY_FAIL = 165;
    public static final int REP_WXAPP_PAY_SUCCESS = 166;
    public static final int REP_WXPAY_CALL = 123;
    public static final int REP_WXPAY_CANCEL = 125;
    public static final int REP_WXPAY_SUCCESS = 124;
    public static final int SCANPAY_CANCEL = -705;
    public static final int SCANPAY_ERROR = -706;
    public static final int SCANPAY_SUCCESS = -704;
    public static final int VERIFY_ERROR = -712;
    public static final int VERIFY_FAIL = -711;
    public static final int WXPAY_CANCEL = -702;
    public static final int WXPAY_ERROR = -703;
    public static final int WXPAY_SUCCESS = -701;
    public static Map<Integer, String> errorMap = new HashMap();

    static {
        errorMap.put(Integer.valueOf((int) WXPAY_SUCCESS), "支付成功");
        errorMap.put(Integer.valueOf((int) SCANPAY_SUCCESS), "支付成功");
        errorMap.put(Integer.valueOf((int) WXPAY_CANCEL), "取消支付");
        errorMap.put(Integer.valueOf((int) REP_H5_PAY_CANCEL), "取消支付");
        errorMap.put(167, "取消支付");
        errorMap.put(Integer.valueOf((int) SCANPAY_CANCEL), "取消支付");
        errorMap.put(Integer.valueOf((int) WXPAY_ERROR), "支付失败");
        errorMap.put(Integer.valueOf((int) SCANPAY_ERROR), "支付失败");
        errorMap.put(Integer.valueOf((int) GET_SESSION_ERROR), "获取session错误");
        errorMap.put(Integer.valueOf((int) CREATE_UNDEFINEORDER_ERROR), "创建预订单错误");
        errorMap.put(Integer.valueOf((int) GET_PAYINTO_ERROR), "获取支付信息错误");
        errorMap.put(Integer.valueOf((int) QUERY_ORDER_ERROR), "查询订单错误");
        errorMap.put(Integer.valueOf((int) NET_ERROR), "网络错误");
    }
}
