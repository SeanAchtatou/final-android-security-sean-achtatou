package com.xiaomi.gamecenter.wxwap;

public interface PayResultCallback {
    void onError(int i, String str);

    void onSuccess(String str);
}
