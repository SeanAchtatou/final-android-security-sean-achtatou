package com.xiaomi.gamecenter.wxwap.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;
import com.xiaomi.gamecenter.wxwap.e.h;

public class HyWebFragment extends Fragment implements View.OnKeyListener {
    /* access modifiers changed from: private */
    public WebView a;
    private String b;
    /* access modifiers changed from: private */
    public o c;
    private int d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getActivity().getWindow().setFlags(1024, 2000);
        this.b = getArguments().getString("_url");
        this.d = getArguments().getInt("_code");
        com.xiaomi.gamecenter.wxwap.b.a.a("loadUrl=" + this.b + ",code=" + this.d);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int a2;
        int a3;
        FrameLayout frameLayout = new FrameLayout(getActivity());
        frameLayout.setBackgroundColor(Color.parseColor("#b0000000"));
        this.a = new WebView(getActivity());
        if (getResources().getConfiguration().orientation == 2) {
            a2 = h.a(getActivity(), 313.0f);
            a3 = h.a(getActivity(), 313.0f);
        } else {
            a2 = h.a(getActivity(), 313.0f);
            a3 = h.a(getActivity(), 590.0f);
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a3);
        layoutParams.gravity = 17;
        this.a.setLayoutParams(layoutParams);
        this.a.getSettings().setDisplayZoomControls(false);
        this.a.getSettings().setSupportZoom(false);
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.setWebViewClient(new a(this, null));
        this.a.setWebChromeClient(new WebChromeClient());
        frameLayout.addView(this.a);
        this.a.loadUrl(this.b);
        b.a().a(ResultCode.REP_VERIFY_SHOWPAGE);
        return frameLayout;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this);
    }

    public void a(o oVar) {
        this.c = oVar;
    }

    private class a extends WebViewClient {
        private a() {
        }

        /* synthetic */ a(HyWebFragment hyWebFragment, c cVar) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            com.xiaomi.gamecenter.wxwap.b.a.a(str);
            if (str.startsWith("migamesdk://")) {
                String queryParameter = Uri.parse(str).getQueryParameter("result");
                if (queryParameter.equals("closed")) {
                    b.a().a(ResultCode.REP_VERIFY_CLICK_CLOSE);
                    HyWebFragment.this.a();
                } else if (queryParameter.equals("success")) {
                    b.a().a(ResultCode.REP_VERIFY_SUCCESS);
                    HyWebFragment.this.c.b();
                    HyWebFragment.this.getFragmentManager().beginTransaction().remove(HyWebFragment.this.getFragmentManager().findFragmentByTag("WEBVIEW")).commit();
                }
            } else {
                HyWebFragment.this.a.loadUrl(str);
                HyWebFragment.this.getView().setFocusableInTouchMode(true);
                HyWebFragment.this.getView().requestFocus();
                HyWebFragment.this.getView().setOnKeyListener(new e(this));
            }
            return true;
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 1) {
            return false;
        }
        b.a().a(ResultCode.REP_VERIFY_CLICK_BACK);
        a();
        return true;
    }

    /* access modifiers changed from: private */
    public void a() {
        switch (this.d) {
            case 404:
                this.a.setVisibility(4);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("您未完成真实身份登记,根据国家规定，无法为游戏充值。");
                builder.setTitle("提示");
                builder.setPositiveButton("去登记", new c(this));
                builder.setNegativeButton("确定", new d(this));
                builder.setCancelable(false);
                builder.show();
                return;
            case 405:
                this.c.b();
                getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag("WEBVIEW")).commit();
                return;
            case 406:
                this.c.b();
                getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag("WEBVIEW")).commit();
                return;
            default:
                this.c.b();
                getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag("WEBVIEW")).commit();
                return;
        }
    }
}
