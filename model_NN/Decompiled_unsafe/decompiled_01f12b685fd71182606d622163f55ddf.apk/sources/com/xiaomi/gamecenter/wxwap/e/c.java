package com.xiaomi.gamecenter.wxwap.e;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import com.xiaomi.gamecenter.wxwap.b.a;

/* compiled from: AccountUtils */
public class c {
    public static String a(Context context) {
        Account[] accountsByType = AccountManager.get(context).getAccountsByType("com.xiaomi");
        if (accountsByType == null || accountsByType.length <= 0) {
            return "";
        }
        int length = accountsByType.length;
        for (int i = 0; i < length; i++) {
            a.a("account=" + accountsByType[i].name);
        }
        return accountsByType[0].name;
    }
}
