package com.xiaomi.gamecenter.wxwap.e;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: HyUtils */
public class k {
    public static final int a = 570556416;

    public static String a(Map<String, String> map) {
        String str;
        ArrayList arrayList = new ArrayList(map.keySet());
        Collections.sort(arrayList);
        String str2 = "";
        int i = 0;
        while (i < arrayList.size()) {
            String str3 = (String) arrayList.get(i);
            String str4 = map.get(str3);
            if (TextUtils.isEmpty(str4)) {
                str = str2;
            } else if (i == arrayList.size() - 1) {
                str = str2 + str3 + "=" + str4;
            } else {
                str = str2 + str3 + "=" + str4 + "&";
            }
            i++;
            str2 = str;
        }
        return str2;
    }

    public static boolean a(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.tencent.mm", 0) != null) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.eg.android.AlipayGphone", 0) != null) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean c(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.xiaomi.gamecenter.sdk.service", 64).versionCode >= 480900) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static JSONArray a(String[] strArr) {
        JSONArray jSONArray = new JSONArray();
        if (strArr == null || strArr.length <= 0) {
            jSONArray.put("ALIPAY");
        } else {
            for (String put : strArr) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static int d(Context context) {
        int i;
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.plugin.provider/sharedpref"), new String[]{"_id", "key", "type", "value"}, "key = ?", new String[]{"_build_info_sdk_int_"}, null);
        if (query == null) {
            return 0;
        }
        int columnIndex = query.getColumnIndex("type");
        int columnIndex2 = query.getColumnIndex("value");
        if (query.moveToFirst()) {
            i = ((Integer) a.a(query.getInt(columnIndex), query.getString(columnIndex2))).intValue();
        } else {
            i = 0;
        }
        query.close();
        return i;
    }

    /* compiled from: HyUtils */
    private static final class a {
        private a() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public static Object a(int i, String str) {
            switch (i) {
                case 1:
                    try {
                        return Integer.valueOf(str);
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                case 2:
                    return Long.valueOf(str);
                case 3:
                    return str;
                case 4:
                    return Boolean.valueOf(str);
                case 5:
                    return Float.valueOf(str);
                case 6:
                    return Double.valueOf(str);
            }
            return null;
        }
    }
}
