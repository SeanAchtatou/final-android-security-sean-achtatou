package com.xiaomi.gamecenter.wxwap.purchase;

import java.io.Serializable;

public abstract class Purchase implements Serializable {
    private String cpOrderId = "";
    private String cpUserInfo = "";

    public String getCpOrderId() {
        return this.cpOrderId;
    }

    public void setCpOrderId(String str) {
        this.cpOrderId = str;
    }

    public String getCpUserInfo() {
        return this.cpUserInfo;
    }

    public void setCpUserInfo(String str) {
        this.cpUserInfo = str;
    }
}
