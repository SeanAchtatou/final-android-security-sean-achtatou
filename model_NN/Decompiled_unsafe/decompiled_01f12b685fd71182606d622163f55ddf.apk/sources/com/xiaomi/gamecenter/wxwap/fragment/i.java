package com.xiaomi.gamecenter.wxwap.fragment;

import cn.egame.terminal.paysdk.FailedCode;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;

/* compiled from: HyWxWapH5Fragment */
class i implements Runnable {
    final /* synthetic */ h a;

    i(h hVar) {
        this.a = hVar;
    }

    public void run() {
        this.a.a.a.getActivity().getApplicationContext().unbindService(this.a.a.a.m);
        switch (this.a.a.a.p) {
            case -18003:
                b.a().a(165);
                this.a.a.a.b((int) ResultCode.WXPAY_ERROR);
                return;
            case FailedCode.ERROR_CODE_MD5_FAILED:
                b.a().a(167);
                this.a.a.a.b((int) ResultCode.WXPAY_CANCEL);
                return;
            case 0:
                b.a().a(166);
                this.a.a.a.b((int) ResultCode.WXPAY_SUCCESS);
                return;
            default:
                b.a().a(165);
                this.a.a.a.b((int) ResultCode.WXPAY_ERROR);
                return;
        }
    }
}
