package com.xiaomi.gamecenter.wxwap.a;

import android.content.Context;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.wali.gamecenter.report.utils.ZSIMInfo;
import com.xiaomi.gamecenter.wxwap.config.a;
import com.xiaomi.gamecenter.wxwap.e.i;
import com.xiaomi.gamecenter.wxwap.e.m;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

/* compiled from: RestClient */
public class b {
    public static RequestQueue a;
    private static Context b;
    private static volatile b c;

    public static void a(Context context) {
        if (c == null) {
            c = new b(context);
        }
    }

    public b(Context context) {
        b = context;
        a = Volley.newRequestQueue(b, new HurlStack(null, b()));
    }

    public b a() {
        return c;
    }

    public static void a(String str, Map<String, String> map, a aVar) {
        aVar.a();
        e eVar = new e(0, str, new c(aVar), new d(aVar), map);
        eVar.setRetryPolicy(new DefaultRetryPolicy(12500, 0, 1.0f));
        eVar.setTag(b.class);
        a.add(eVar);
    }

    public static void b(String str, Map<String, String> map, a aVar) {
        aVar.a();
        h hVar = new h(1, str, new f(aVar), new g(aVar), map);
        hVar.setRetryPolicy(new DefaultRetryPolicy(12500, 0, 1.0f));
        hVar.setTag(b.class);
        a.add(hVar);
    }

    public static Map<String, Object> b(Context context) {
        HashMap hashMap = new HashMap();
        hashMap.put("imei", ZSIMInfo.getSha1DeviceID(context));
        hashMap.put("imsi", ZSIMInfo.getIMSI(context));
        hashMap.put("mac", ZSIMInfo.getMacAddress(context));
        hashMap.put("ua", m.d(context));
        hashMap.put("clientType", "android");
        hashMap.put("carrierInfo", ZSIMInfo.getSIMOperator(context));
        hashMap.put("channelId", i.a(context));
        hashMap.put("sdkVersion", a.b);
        hashMap.put("nonceStr", UUID.randomUUID().toString());
        hashMap.put("timeStamp", System.currentTimeMillis() + "");
        hashMap.put("publishChannel", a.d);
        return hashMap;
    }

    private static SSLSocketFactory b() {
        TrustManager[] trustManagerArr = {new i()};
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, trustManagerArr, new SecureRandom());
            return instance.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (KeyManagementException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
