package com.xiaomi.gamecenter.wxwap.purchase;

public class FeePurchase extends Purchase {
    private String feeValue;

    public String getFeeValue() {
        return this.feeValue;
    }

    public void setFeeValue(String str) {
        this.feeValue = str;
    }
}
