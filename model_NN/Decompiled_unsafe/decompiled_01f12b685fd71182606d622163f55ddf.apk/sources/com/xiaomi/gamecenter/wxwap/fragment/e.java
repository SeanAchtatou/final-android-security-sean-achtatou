package com.xiaomi.gamecenter.wxwap.fragment;

import android.view.KeyEvent;
import android.view.View;
import com.xiaomi.gamecenter.wxwap.fragment.HyWebFragment;

/* compiled from: HyWebFragment */
class e implements View.OnKeyListener {
    final /* synthetic */ HyWebFragment.a a;

    e(HyWebFragment.a aVar) {
        this.a = aVar;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 1) {
            return false;
        }
        HyWebFragment.this.a.goBack();
        return true;
    }
}
