package com.xiaomi.gamecenter.wxwap.e;

import android.os.Handler;
import android.os.SystemClock;

/* compiled from: CountDownTimer */
public abstract class f {
    private static final int e = 1;
    private final long a;
    /* access modifiers changed from: private */
    public final long b;
    /* access modifiers changed from: private */
    public long c;
    /* access modifiers changed from: private */
    public boolean d = false;
    private Handler f = new g(this);

    public abstract void a(long j);

    public abstract void c();

    public f(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    public final synchronized void a() {
        this.d = true;
        this.f.removeMessages(1);
    }

    public final synchronized f b() {
        f fVar;
        this.d = false;
        if (this.a <= 0) {
            c();
            fVar = this;
        } else {
            this.c = SystemClock.elapsedRealtime() + this.a;
            this.f.sendMessage(this.f.obtainMessage(1));
            fVar = this;
        }
        return fVar;
    }
}
