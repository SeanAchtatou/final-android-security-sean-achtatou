package com.xiaomi.gamecenter.wxwap.fragment;

import android.os.RemoteException;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;

/* compiled from: HyWxWapH5Fragment */
class h extends Thread {
    final /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    public void run() {
        super.run();
        try {
            int unused = this.a.a.p = this.a.a.n.pay(this.a.a.o, this.a.a.q);
            this.a.a.getActivity().runOnUiThread(new i(this));
        } catch (RemoteException e) {
            b.a().a(165);
            this.a.a.b((int) ResultCode.WXPAY_ERROR);
        } catch (Exception e2) {
            b.a().a(165);
            this.a.a.b((int) ResultCode.WXPAY_ERROR);
            e2.printStackTrace();
        }
    }
}
