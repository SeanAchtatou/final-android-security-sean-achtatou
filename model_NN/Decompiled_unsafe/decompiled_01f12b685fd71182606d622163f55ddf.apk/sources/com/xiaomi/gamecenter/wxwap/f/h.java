package com.xiaomi.gamecenter.wxwap.f;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.animation.Animation;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

/* compiled from: ScalePopup */
public class h extends b {
    private WebView j;
    private FrameLayout k;
    private String l;

    public h(Activity activity, String str) {
        super(activity);
        this.l = str;
    }

    /* access modifiers changed from: protected */
    public Animation c() {
        return n();
    }

    /* access modifiers changed from: protected */
    public View d() {
        return this.k;
    }

    public View a() {
        this.k = new FrameLayout(this.e);
        this.k.setBackgroundColor(Color.parseColor("#b0000000"));
        this.j = new WebView(this.e);
        this.j.setLayoutParams(new FrameLayout.LayoutParams(com.xiaomi.gamecenter.wxwap.e.h.a(this.e, 313.0f), com.xiaomi.gamecenter.wxwap.e.h.a(this.e, 590.0f)));
        this.j.getSettings().setDisplayZoomControls(false);
        this.j.getSettings().setSupportZoom(false);
        this.j.getSettings().setJavaScriptEnabled(true);
        this.j.setWebViewClient(new a());
        this.j.setWebChromeClient(new WebChromeClient());
        this.k.addView(this.j);
        this.j.loadUrl(this.l);
        return this.k;
    }

    public View b() {
        return this.k;
    }

    /* compiled from: ScalePopup */
    private class a extends WebViewClient {
        private a() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            com.xiaomi.gamecenter.wxwap.b.a.a(str);
            return true;
        }
    }
}
