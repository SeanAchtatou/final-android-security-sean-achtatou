package com.xiaomi.gamecenter.wxwap.f;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.PopupWindow;
import com.kbz.esotericsoftware.spine.Animation;

/* compiled from: BasePopupWindow */
public abstract class b implements a {
    private static final String j = "BasePopupWindow";
    protected PopupWindow a;
    protected View b;
    protected View c;
    protected View d;
    protected Activity e;
    protected Animation f;
    protected Animator g;
    protected Animation h;
    protected Animator i;
    private boolean k = false;
    /* access modifiers changed from: private */
    public a l;
    private Animator.AnimatorListener m = new f(this);
    private Animation.AnimationListener n = new g(this);

    /* compiled from: BasePopupWindow */
    public interface a {
        void a();
    }

    /* access modifiers changed from: protected */
    public abstract Animation c();

    /* access modifiers changed from: protected */
    public abstract View d();

    public b(Activity activity) {
        a(activity, -1, -1);
    }

    public b(Activity activity, int i2, int i3) {
        a(activity, i2, i3);
    }

    private void a(Activity activity, int i2, int i3) {
        this.e = activity;
        this.b = a();
        this.b.setFocusableInTouchMode(true);
        this.a = new PopupWindow(this.b, i2, i3);
        this.a.setBackgroundDrawable(new ColorDrawable());
        this.a.setFocusable(true);
        this.a.setOutsideTouchable(true);
        this.a.setAnimationStyle(0);
        this.c = b();
        this.d = d();
        if (this.d != null) {
            this.d.setOnClickListener(new c(this));
            if (this.c != null) {
                this.c.setOnClickListener(new d(this));
            }
        }
        this.h = c();
        this.i = e();
        this.f = g();
        this.g = h();
    }

    public Animator e() {
        return null;
    }

    public View f() {
        return null;
    }

    public Animation g() {
        return null;
    }

    public Animator h() {
        return null;
    }

    public void i() {
        try {
            a(0, null);
        } catch (Exception e2) {
            Log.e(j, "show error");
            e2.printStackTrace();
        }
    }

    public void a(int i2) {
        try {
            a(i2, null);
        } catch (Exception e2) {
            Log.e(j, "show error");
            e2.printStackTrace();
        }
    }

    public void a(View view) {
        try {
            a(0, view);
        } catch (Exception e2) {
            Log.e(j, "show error");
            e2.printStackTrace();
        }
    }

    private void a(int i2, View view) throws Exception {
        if (i2 == 0 && view != null) {
            this.a.showAtLocation(view, 17, 0, 0);
        }
        if (i2 != 0 && view == null) {
            this.a.showAtLocation(this.e.findViewById(i2), 17, 0, 0);
        }
        if (i2 == 0 && view == null) {
            this.a.showAtLocation(this.e.findViewById(16908290), 17, 0, 0);
        }
        if (!(this.h == null || this.c == null)) {
            this.c.clearAnimation();
            this.c.startAnimation(this.h);
        }
        if (this.h == null && this.i != null && this.c != null) {
            this.i.start();
        }
    }

    public void a(boolean z) {
        if (z) {
            this.a.setSoftInputMode(16);
        } else {
            this.a.setSoftInputMode(48);
        }
    }

    public void b(boolean z) {
        this.k = z;
        if (z) {
            a(true);
        } else {
            a(false);
        }
    }

    public void c(boolean z) {
        if (z) {
            this.a.setBackgroundDrawable(new ColorDrawable());
        } else {
            this.a.setBackgroundDrawable(null);
        }
    }

    public View b(int i2) {
        if (i2 != 0) {
            return LayoutInflater.from(this.e).inflate(i2, (ViewGroup) null);
        }
        return null;
    }

    public boolean j() {
        return this.a.isShowing();
    }

    public a k() {
        return this.l;
    }

    public void a(a aVar) {
        this.l = aVar;
        if (this.l != null) {
            this.a.setOnDismissListener(new e(this));
        }
    }

    public void l() {
        try {
            if (this.f != null) {
                this.f.setAnimationListener(this.n);
                this.c.clearAnimation();
                this.c.startAnimation(this.f);
            } else if (this.g != null) {
                this.g.removeListener(this.m);
                this.g.addListener(this.m);
                this.g.start();
            } else {
                this.a.dismiss();
            }
        } catch (Exception e2) {
            Log.d(j, "dismiss error");
        }
    }

    /* access modifiers changed from: protected */
    public Animation a(int i2, int i3, int i4) {
        TranslateAnimation translateAnimation = new TranslateAnimation(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) i2, (float) i3);
        translateAnimation.setDuration((long) i4);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setFillAfter(true);
        return translateAnimation;
    }

    /* access modifiers changed from: protected */
    public android.view.animation.Animation a(float f2, float f3, float f4, float f5, int i2, float f6, int i3, float f7) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f4, f5, i2, f6, i3, f7);
        scaleAnimation.setDuration(300);
        scaleAnimation.setFillEnabled(true);
        scaleAnimation.setFillAfter(true);
        return scaleAnimation;
    }

    /* access modifiers changed from: protected */
    public android.view.animation.Animation m() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(300);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setFillEnabled(true);
        scaleAnimation.setFillAfter(true);
        return scaleAnimation;
    }

    /* access modifiers changed from: protected */
    public android.view.animation.Animation n() {
        AlphaAnimation alphaAnimation = new AlphaAnimation((float) Animation.CurveTimeline.LINEAR, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setFillAfter(true);
        return alphaAnimation;
    }

    /* access modifiers changed from: protected */
    public AnimatorSet o() {
        AnimatorSet animatorSet = new AnimatorSet();
        if (this.c != null) {
            animatorSet.playTogether(ObjectAnimator.ofFloat(this.c, "translationY", 250.0f, Animation.CurveTimeline.LINEAR).setDuration(400L), ObjectAnimator.ofFloat(this.c, "alpha", 0.4f, 1.0f).setDuration(375L));
        }
        return animatorSet;
    }
}
