package com.xiaomi.gamecenter.wxwap.c;

import com.android.volley.VolleyError;
import com.xiaomi.gamecenter.wxwap.a.a;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PayProtocol */
class d extends a {
    final /* synthetic */ b a;

    d(b bVar) {
        this.a = bVar;
    }

    public void a(String str) {
        try {
            com.xiaomi.gamecenter.wxwap.b.a.e("verifyID", str);
            this.a.j.a(new JSONObject(str).optString("errcode"));
        } catch (JSONException e) {
            e.printStackTrace();
            this.a.j.a("-712");
        }
    }

    public void a(VolleyError volleyError) {
        this.a.j.a("-711");
    }
}
