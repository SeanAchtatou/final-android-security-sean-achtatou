package com.xiaomi.gamecenter.wxwap.c;

import android.content.Context;
import android.text.TextUtils;
import com.datalab.tools.Constant;
import com.wali.gamecenter.report.utils.ZSIMInfo;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.e.a;
import com.xiaomi.gamecenter.wxwap.e.c;
import com.xiaomi.gamecenter.wxwap.e.j;
import com.xiaomi.gamecenter.wxwap.e.k;
import com.xiaomi.gamecenter.wxwap.e.o;
import com.xiaomi.gamecenter.wxwap.model.AppInfo;
import com.xiaomi.gamecenter.wxwap.model.TokenManager;
import com.xiaomi.gamecenter.wxwap.purchase.FeePurchase;
import com.xiaomi.gamecenter.wxwap.purchase.OrderPurchase;
import com.xiaomi.gamecenter.wxwap.purchase.Purchase;
import com.xiaomi.gamecenter.wxwap.purchase.RepeatPurchase;
import com.xiaomi.gamecenter.wxwap.purchase.UnrepeatPurchase;
import com.xiaomi.gamecenter.wxwap.purchase.WithNamePurchase;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PayProtocol */
public class b {
    private Purchase a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    private String e;
    private String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public a j;
    private String k;
    private String l;

    public b(Context context, AppInfo appInfo, Purchase purchase) {
        this.b = context;
        this.a = purchase;
        this.c = appInfo.getAppid();
        this.d = appInfo.getAppkey();
        this.k = Arrays.toString(appInfo.getPaymentList());
        if (purchase instanceof UnrepeatPurchase) {
            this.e = ((UnrepeatPurchase) purchase).getChargeCode();
            if (TextUtils.isEmpty(this.e)) {
                throw new IllegalArgumentException("计费代码不能为空");
            }
            this.f = "1";
        }
        if (purchase instanceof RepeatPurchase) {
            this.e = ((RepeatPurchase) purchase).getChargeCode();
            this.f = ((RepeatPurchase) purchase).getAmout();
            if (TextUtils.isEmpty(this.e)) {
                throw new IllegalArgumentException("计费代码不能为空");
            } else if (TextUtils.isEmpty(this.f)) {
                this.f = "1";
            }
        }
        if (purchase instanceof FeePurchase) {
            this.g = ((FeePurchase) purchase).getFeeValue();
            if (TextUtils.isEmpty(this.g)) {
                throw new IllegalArgumentException("金额不能为空");
            }
            this.e = "-1";
        } else {
            this.g = "-1";
        }
        if (purchase instanceof OrderPurchase) {
            OrderPurchase orderPurchase = (OrderPurchase) purchase;
            this.g = orderPurchase.getFeeValue();
            this.i = orderPurchase.getDisplayName();
            this.h = orderPurchase.getMiOrderId();
        }
        if (purchase instanceof WithNamePurchase) {
            WithNamePurchase withNamePurchase = (WithNamePurchase) purchase;
            this.g = withNamePurchase.getFeeValue();
            this.e = withNamePurchase.getChargeCode();
            this.l = withNamePurchase.getPurchaseName();
        }
    }

    public void a(a aVar) {
        this.j = aVar;
    }

    public void a() {
        String sha1DeviceID = ZSIMInfo.getSha1DeviceID(this.b);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("imei", sha1DeviceID);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("guestInfo", jSONObject.toString());
        com.xiaomi.gamecenter.wxwap.a.b.a(com.xiaomi.gamecenter.wxwap.config.b.b, hashMap, new c(this));
    }

    public void b() {
        HashMap hashMap = new HashMap();
        hashMap.put("devAppId", this.c);
        hashMap.put("fuid", TokenManager.getInstance().getToken(this.b).getUid());
        hashMap.put("sid", TokenManager.getInstance().getToken(this.b).getSession());
        hashMap.put("actionType", "spay");
        com.xiaomi.gamecenter.wxwap.a.b.b(com.xiaomi.gamecenter.wxwap.config.b.i, hashMap, new d(this));
    }

    public void a(String[] strArr) {
        String str;
        com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_CREATE_ORDER_CALL);
        String str2 = "";
        Map<String, Object> b2 = com.xiaomi.gamecenter.wxwap.a.b.b(this.b);
        b2.put("devAppId", this.c);
        b2.put("productCode", this.e);
        b2.put("quantity", this.f);
        b2.put("feeValue", this.g);
        b2.put("goodsName", this.l);
        b2.put("cpOrderId", this.a.getCpOrderId());
        b2.put("cpUserInfo", this.a.getCpUserInfo());
        b2.put("paymentList", k.a(strArr));
        b2.put("openId", TokenManager.getInstance().getToken(this.b).getUid());
        String a2 = c.a(this.b);
        if (!TextUtils.isEmpty(a2)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("xiaomiId", a2);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            b2.put("userMark", jSONObject.toString());
        }
        try {
            str2 = o.a(a.a(new JSONObject(b2).toString(), a.a(com.xiaomi.gamecenter.wxwap.config.a.c)));
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("session", TokenManager.getInstance().getToken(this.b).getSession());
        hashMap.put("uid", TokenManager.getInstance().getToken(this.b).getUid());
        hashMap.put("p", str2);
        try {
            String str3 = k.a(hashMap) + "&uri=" + com.xiaomi.gamecenter.wxwap.config.b.c;
            com.xiaomi.gamecenter.wxwap.b.a.e("signString", str3);
            com.xiaomi.gamecenter.wxwap.b.a.e("appkey", this.d);
            str = j.a(str3, this.d + "&key");
        } catch (Exception e4) {
            e4.printStackTrace();
            str = "";
        }
        hashMap.put(Constant.SIGN, str);
        com.xiaomi.gamecenter.wxwap.a.b.b(com.xiaomi.gamecenter.wxwap.config.b.d, hashMap, new e(this));
    }

    public void a(String str) {
        String str2;
        com.xiaomi.gamecenter.wxwap.d.b.a().a(ResultCode.REP_GET_PAYINFO_WX_CALL);
        String str3 = "";
        Map<String, Object> b2 = com.xiaomi.gamecenter.wxwap.a.b.b(this.b);
        b2.put("devAppId", this.c);
        b2.put("openId", TokenManager.getInstance().getToken(this.b).getUid());
        b2.put("payment", str);
        b2.put("orderId", this.h);
        b2.put("amount", this.g);
        b2.put("displayName", this.i);
        b2.put("tradeType", "WXMWEB");
        try {
            str3 = o.a(a.a(new JSONObject(b2).toString(), a.a(com.xiaomi.gamecenter.wxwap.config.a.c)));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("session", TokenManager.getInstance().getToken(this.b).getSession());
        hashMap.put("uid", TokenManager.getInstance().getToken(this.b).getUid());
        hashMap.put("p", str3);
        try {
            String str4 = k.a(hashMap) + "&uri=" + com.xiaomi.gamecenter.wxwap.config.b.e;
            com.xiaomi.gamecenter.wxwap.b.a.e("signString", str4);
            com.xiaomi.gamecenter.wxwap.b.a.e("appkey", this.d);
            str2 = j.a(str4, this.d + "&key");
        } catch (Exception e3) {
            e3.printStackTrace();
            str2 = "";
        }
        hashMap.put(Constant.SIGN, str2);
        com.xiaomi.gamecenter.wxwap.a.b.b(com.xiaomi.gamecenter.wxwap.config.b.f, hashMap, new f(this, str));
    }

    public void c() {
        String str;
        com.xiaomi.gamecenter.wxwap.d.b.a().a(3024);
        String str2 = "";
        Map<String, Object> b2 = com.xiaomi.gamecenter.wxwap.a.b.b(this.b);
        b2.put("devAppId", this.c);
        b2.put("openId", TokenManager.getInstance().getToken(this.b).getUid());
        b2.put("orderId", this.h);
        try {
            str2 = o.a(a.a(new JSONObject(b2).toString(), a.a(com.xiaomi.gamecenter.wxwap.config.a.c)));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("session", TokenManager.getInstance().getToken(this.b).getSession());
        hashMap.put("uid", TokenManager.getInstance().getToken(this.b).getUid());
        hashMap.put("p", str2);
        try {
            String str3 = k.a(hashMap) + "&uri=" + com.xiaomi.gamecenter.wxwap.config.b.g;
            com.xiaomi.gamecenter.wxwap.b.a.e("signString", str3);
            com.xiaomi.gamecenter.wxwap.b.a.e("appkey", this.d);
            str = j.a(str3, this.d + "&key");
        } catch (Exception e3) {
            e3.printStackTrace();
            str = "";
        }
        hashMap.put(Constant.SIGN, str);
        com.xiaomi.gamecenter.wxwap.a.b.b(com.xiaomi.gamecenter.wxwap.config.b.h, hashMap, new g(this));
    }

    public void d() {
        com.xiaomi.gamecenter.wxwap.a.b.a.cancelAll(com.xiaomi.gamecenter.wxwap.a.b.class);
    }
}
