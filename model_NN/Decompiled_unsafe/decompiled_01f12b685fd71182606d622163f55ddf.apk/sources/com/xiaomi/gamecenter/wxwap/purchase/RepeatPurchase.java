package com.xiaomi.gamecenter.wxwap.purchase;

public class RepeatPurchase extends Purchase {
    private String amout;
    private String chargeCode;

    public String getChargeCode() {
        return this.chargeCode;
    }

    public void setChargeCode(String str) {
        this.chargeCode = str;
    }

    public String getAmout() {
        return this.amout;
    }

    public void setAmout(String str) {
        this.amout = str;
    }
}
