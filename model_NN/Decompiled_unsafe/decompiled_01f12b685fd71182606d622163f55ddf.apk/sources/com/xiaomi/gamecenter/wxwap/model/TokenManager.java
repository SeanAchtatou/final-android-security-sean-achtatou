package com.xiaomi.gamecenter.wxwap.model;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.xiaomi.gamecenter.wxwap.e.b;
import com.xiaomi.gamecenter.wxwap.e.i;
import com.xiaomi.gamecenter.wxwap.e.l;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenManager {
    private static final String FILENAME = ".huyugamepaytoken_security";
    private static volatile TokenManager instance;
    private ServiceToken serviceToken;

    private TokenManager() {
    }

    public static TokenManager getInstance() {
        if (instance == null) {
            synchronized (TokenManager.class) {
                if (instance == null) {
                    instance = new TokenManager();
                }
            }
        }
        return instance;
    }

    public boolean isExist(Context context) {
        if (new File(context.getFilesDir(), FILENAME).exists()) {
            return true;
        }
        return false;
    }

    public boolean save2File(Context context, String str) {
        return i.a(context, FILENAME, b.a(getAESKey(context), str));
    }

    public String readToken(Context context) {
        try {
            return b.b(getAESKey(context), i.c(context, FILENAME));
        } catch (Exception e) {
            i.b(context, FILENAME);
            e.printStackTrace();
            return null;
        }
    }

    public ServiceToken getToken(Context context) {
        if (this.serviceToken == null && isExist(context)) {
            String readToken = readToken(context);
            if (!TextUtils.isEmpty(readToken)) {
                try {
                    JSONObject jSONObject = new JSONObject(readToken);
                    jSONObject.optString("errcode");
                    JSONObject jSONObject2 = new JSONObject(jSONObject.optString("result"));
                    this.serviceToken = new ServiceToken();
                    this.serviceToken.setAkey(jSONObject2.optString("akey"));
                    this.serviceToken.setKey(jSONObject2.optString("key"));
                    this.serviceToken.setMiid(jSONObject2.optString("miid"));
                    this.serviceToken.setUid(jSONObject2.optString("uid"));
                    this.serviceToken.setT(jSONObject2.optString("t"));
                    this.serviceToken.setSession(jSONObject2.optString("session"));
                    return this.serviceToken;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return this.serviceToken;
    }

    public boolean deleteToken(Context context) {
        instance = null;
        this.serviceToken = null;
        return context.deleteFile(FILENAME);
    }

    private String getAESKey(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (TextUtils.isEmpty(string)) {
            string = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return l.c(string.getBytes());
    }
}
