package com.xiaomi.gamecenter.wxwap.fragment;

import android.annotation.TargetApi;
import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.xiaomi.gamecenter.wxwap.config.ResultCode;
import com.xiaomi.gamecenter.wxwap.d.b;

/* compiled from: HyWxWapH5Fragment */
class j extends WebViewClient {
    final /* synthetic */ HyWxWapH5Fragment a;

    j(HyWxWapH5Fragment hyWxWapH5Fragment) {
        this.a = hyWxWapH5Fragment;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith("http:") || str.startsWith("https:")) {
            return false;
        }
        new Thread(new k(this, str)).start();
        return true;
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        this.a.b((int) ResultCode.WXPAY_ERROR);
        b.a().a(ResultCode.WXPAY_ERROR);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (Build.VERSION.SDK_INT < 23) {
            this.a.b((int) ResultCode.WXPAY_ERROR);
            b.a().a(ResultCode.WXPAY_ERROR);
            super.onReceivedError(webView, i, str, str2);
        }
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        this.a.b((int) ResultCode.WXPAY_ERROR);
        b.a().a(ResultCode.WXPAY_ERROR);
    }
}
