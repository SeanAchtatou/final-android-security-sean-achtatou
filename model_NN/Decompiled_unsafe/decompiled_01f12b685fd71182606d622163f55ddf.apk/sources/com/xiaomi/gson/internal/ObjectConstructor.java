package com.xiaomi.gson.internal;

public interface ObjectConstructor<T> {
    T construct();
}
