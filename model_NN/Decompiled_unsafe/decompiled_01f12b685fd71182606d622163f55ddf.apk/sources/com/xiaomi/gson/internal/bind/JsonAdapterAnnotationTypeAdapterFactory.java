package com.xiaomi.gson.internal.bind;

import com.xiaomi.gson.Gson;
import com.xiaomi.gson.TypeAdapter;
import com.xiaomi.gson.TypeAdapterFactory;
import com.xiaomi.gson.annotations.JsonAdapter;
import com.xiaomi.gson.internal.ConstructorConstructor;
import com.xiaomi.gson.reflect.TypeToken;

public final class JsonAdapterAnnotationTypeAdapterFactory implements TypeAdapterFactory {
    private final ConstructorConstructor constructorConstructor;

    public JsonAdapterAnnotationTypeAdapterFactory(ConstructorConstructor constructorConstructor2) {
        this.constructorConstructor = constructorConstructor2;
    }

    static TypeAdapter<?> getTypeAdapter(ConstructorConstructor constructorConstructor2, Gson gson, TypeToken<?> typeToken, JsonAdapter jsonAdapter) {
        TypeAdapter<?> create;
        Class<?> value = jsonAdapter.value();
        if (TypeAdapter.class.isAssignableFrom(value)) {
            create = (TypeAdapter) constructorConstructor2.get(TypeToken.get((Class) value)).construct();
        } else if (TypeAdapterFactory.class.isAssignableFrom(value)) {
            create = ((TypeAdapterFactory) constructorConstructor2.get(TypeToken.get((Class) value)).construct()).create(gson, typeToken);
        } else {
            throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
        }
        return create != null ? create.nullSafe() : create;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.xiaomi.gson.reflect.TypeToken<T>, com.xiaomi.gson.reflect.TypeToken] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.xiaomi.gson.TypeAdapter<T> create(com.xiaomi.gson.Gson r3, com.xiaomi.gson.reflect.TypeToken<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.getRawType()
            java.lang.Class<com.xiaomi.gson.annotations.JsonAdapter> r1 = com.xiaomi.gson.annotations.JsonAdapter.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.xiaomi.gson.annotations.JsonAdapter r0 = (com.xiaomi.gson.annotations.JsonAdapter) r0
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            com.xiaomi.gson.internal.ConstructorConstructor r1 = r2.constructorConstructor
            com.xiaomi.gson.TypeAdapter r0 = getTypeAdapter(r1, r3, r4, r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.gson.internal.bind.JsonAdapterAnnotationTypeAdapterFactory.create(com.xiaomi.gson.Gson, com.xiaomi.gson.reflect.TypeToken):com.xiaomi.gson.TypeAdapter");
    }
}
