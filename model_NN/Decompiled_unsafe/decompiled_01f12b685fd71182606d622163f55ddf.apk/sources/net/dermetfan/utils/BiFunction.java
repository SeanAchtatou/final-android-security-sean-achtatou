package net.dermetfan.utils;

public interface BiFunction<R, A1, A2> {
    R apply(A1 a1, A2 a2);
}
