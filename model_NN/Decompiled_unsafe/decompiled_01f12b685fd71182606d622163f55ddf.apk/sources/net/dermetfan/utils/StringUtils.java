package net.dermetfan.utils;

public class StringUtils {
    public static String toJavaIdentifier(String string) {
        if (string.length() == 0) {
            return string;
        }
        StringBuilder result = new StringBuilder(string.length());
        if (isJavaIdentifierStart(string.charAt(0))) {
            result.append(string.charAt(0));
        }
        boolean nextUpperCase = false;
        int n = string.length();
        for (int i = 1; i < n; i++) {
            char c = string.charAt(i);
            if (isJavaIdentifierPart(c)) {
                if (nextUpperCase) {
                    c = Character.toUpperCase(c);
                    nextUpperCase = false;
                }
                result.append(c);
            } else {
                nextUpperCase = true;
            }
        }
        return result.toString();
    }

    public static boolean isJavaIdentifierStart(char c) {
        return isJavaIdentifierStart((int) c);
    }

    public static boolean isJavaIdentifierStart(int codePoint) {
        return (codePoint >= 97 && codePoint <= 122) || (codePoint >= 65 && codePoint <= 90) || codePoint == 36 || codePoint == 181 || ((codePoint >= 192 && codePoint <= 214) || ((codePoint >= 216 && codePoint <= 246) || (codePoint >= 248 && codePoint <= 255)));
    }

    public static boolean isJavaIdentifierPart(char c) {
        return isJavaIdentifierPart((int) c);
    }

    public static boolean isJavaIdentifierPart(int codePoint) {
        return isJavaIdentifierStart(codePoint) || (codePoint >= 48 && codePoint <= 57);
    }

    public static String replace(int index, String replacement, String string) {
        return string.substring(0, index).concat(replacement).concat(string.substring(index + 1));
    }

    public static String replace(int index, char replacement, String string) {
        return (string.substring(0, index) + replacement).concat(string.substring(index + 1));
    }

    public static String remove(int beginIndex, int endIndex, String string) {
        return string.substring(0, beginIndex).concat(string.substring(endIndex));
    }

    public static String remove(int index, String string) {
        return remove(index, index + 1, string);
    }

    public static int count(String target, String string, int beginIndex, int endIndex, boolean overlap) {
        int count = 0;
        int increment = overlap ? 1 : target.length();
        int i = string.indexOf(target, beginIndex);
        while (i != -1 && i < endIndex) {
            count++;
            i = string.indexOf(target, i + increment);
        }
        return count;
    }

    public static int count(String target, String string, int beginIndex, int endIndex) {
        return count(target, string, beginIndex, endIndex, false);
    }

    public static int count(String target, String string, boolean overlap) {
        return count(target, string, 0, string.length(), overlap);
    }

    public static int count(String target, String string) {
        return count(target, string, false);
    }

    public static int count(int c, String string, int beginIndex, int endIndex) {
        int count = 0;
        int i = string.indexOf(c, beginIndex);
        while (i != -1 && i < endIndex) {
            count++;
            i = string.indexOf(c, i + 1);
        }
        return count;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.StringUtils.count(int, java.lang.String, int, int):int
     arg types: [char, java.lang.String, int, int]
     candidates:
      net.dermetfan.utils.StringUtils.count(char, java.lang.String, int, int):int
      net.dermetfan.utils.StringUtils.count(java.lang.String, java.lang.String, int, int):int
      net.dermetfan.utils.StringUtils.count(int, java.lang.String, int, int):int */
    public static int count(char c, String string, int beginIndex, int endIndex) {
        return count((int) c, string, beginIndex, endIndex);
    }

    public static int count(int c, String string) {
        return count(c, string, 0, string.length());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.StringUtils.count(int, java.lang.String):int
     arg types: [char, java.lang.String]
     candidates:
      net.dermetfan.utils.StringUtils.count(char, java.lang.String):int
      net.dermetfan.utils.StringUtils.count(java.lang.String, java.lang.String):int
      net.dermetfan.utils.StringUtils.count(int, java.lang.String):int */
    public static int count(char c, String string) {
        return count((int) c, string);
    }
}
