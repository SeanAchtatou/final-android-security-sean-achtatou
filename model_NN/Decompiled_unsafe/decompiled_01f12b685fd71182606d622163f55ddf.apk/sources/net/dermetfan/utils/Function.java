package net.dermetfan.utils;

public interface Function<R, A> {
    R apply(Object obj);
}
