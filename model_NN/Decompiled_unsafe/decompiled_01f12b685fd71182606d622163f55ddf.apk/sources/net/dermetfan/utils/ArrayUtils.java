package net.dermetfan.utils;

import java.lang.reflect.Array;

public class ArrayUtils {
    public static <T> T wrapIndex(int index, Object[] objArr) {
        return objArr[wrapIndex(index, objArr.length)];
    }

    public static int wrapIndex(int index, int[] array) {
        return array[wrapIndex(index, array.length)];
    }

    public static float wrapIndex(int index, float[] array) {
        return array[wrapIndex(index, array.length)];
    }

    public static int wrapIndex(int index, int length) {
        return (index + length) % length;
    }

    public static <T> boolean contains(Object[] objArr, Object obj, boolean identity) {
        int i;
        int i2 = objArr.length - 1;
        if (identity) {
            do {
                i = i2;
                if (i >= 0) {
                    i2 = i - 1;
                }
            } while (objArr[i] != obj);
            return true;
        }
        do {
            i = i2;
            if (i >= 0) {
                i2 = i - 1;
            }
        } while (!objArr[i].equals(obj));
        return true;
        return false;
    }

    public static <T, T2 extends T> boolean contains(Object[] objArr, Object[] objArr2, boolean identity) {
        for (Object contains : objArr2) {
            if (!contains(objArr, contains, identity)) {
                return false;
            }
        }
        return true;
    }

    public static <T, T2 extends T> boolean containsAny(T[] array, T2[] other, boolean identity) {
        for (T2 contains : other) {
            if (contains(array, contains, identity)) {
                return true;
            }
        }
        return false;
    }

    public static boolean equalsAny(Object obj, Object[] array, int offset, int length) {
        for (int i = (offset + length) - 1; i >= offset; i--) {
            if (obj.equals(array[i])) {
                return true;
            }
        }
        return false;
    }

    public static boolean equalsAny(Object obj, Object[] array) {
        return equalsAny(obj, array, 0, array.length);
    }

    public static void shuffle(Object[] array) {
        for (int i = array.length - 1; i > 0; i--) {
            int ii = (int) (Math.random() * ((double) (i + 1)));
            Object tmp = array[i];
            array[i] = array[ii];
            array[ii] = tmp;
        }
    }

    public static void shuffle(int[] array) {
        for (int i = array.length - 1; i > 0; i--) {
            int ii = (int) (Math.random() * ((double) (i + 1)));
            int tmp = array[i];
            array[i] = array[ii];
            array[ii] = tmp;
        }
    }

    public static void shuffle(float[] array) {
        for (int i = array.length - 1; i > 0; i--) {
            int ii = (int) (Math.random() * ((double) (i + 1)));
            float tmp = array[i];
            array[i] = array[ii];
            array[ii] = tmp;
        }
    }

    public static float[] unbox(Float[] values) {
        float[] unboxed = new float[values.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = values[i].floatValue();
        }
        return unboxed;
    }

    public static Float[] box(float[] values) {
        Float[] boxed = new Float[values.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = Float.valueOf(values[i]);
        }
        return boxed;
    }

    public static int[] unbox(Integer[] values) {
        int[] unboxed = new int[values.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = values[i].intValue();
        }
        return unboxed;
    }

    public static Integer[] box(int[] values) {
        Integer[] boxed = new Integer[values.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = Integer.valueOf(values[i]);
        }
        return boxed;
    }

    public static boolean[] unbox(Boolean[] values) {
        boolean[] unboxed = new boolean[values.length];
        for (int i = 0; i < unboxed.length; i++) {
            unboxed[i] = values[i].booleanValue();
        }
        return unboxed;
    }

    public static Boolean[] box(boolean[] values) {
        Boolean[] boxed = new Boolean[values.length];
        for (int i = 0; i < boxed.length; i++) {
            boxed[i] = Boolean.valueOf(values[i]);
        }
        return boxed;
    }

    public static void checkRegion(Object[] array, int offset, int length) {
        if (array == null) {
            throw new IllegalArgumentException("array is null");
        } else if (offset < 0) {
            throw new ArrayIndexOutOfBoundsException("negative offset: " + offset);
        } else if (length < 0) {
            throw new ArrayIndexOutOfBoundsException("negative length: " + length);
        } else if (offset + length > array.length) {
            throw new ArrayIndexOutOfBoundsException(offset + length);
        }
    }

    public static void requireCapacity(Object[] source, int offset, int length, Object[] dest, int destOffset) {
        checkRegion(source, offset, length);
        checkRegion(dest, destOffset, length);
    }

    public static int selectCount(int offset, int length, int start, int everyXth) {
        int count = 0;
        int i = start - 1;
        while (i < offset + length) {
            if (i >= offset) {
                count++;
            }
            i += everyXth;
        }
        return count;
    }

    public static <T> T[] select(Object[] objArr, int offset, int length, int start, int everyXth, Object[] objArr2, int destOffset) {
        int outputLength = selectCount(offset, length, start, everyXth);
        checkRegion(objArr2, destOffset, outputLength);
        int di = destOffset;
        int i = start - 1;
        while (di < outputLength) {
            if (i >= offset) {
                objArr2[di] = objArr[i];
                di++;
            }
            i += everyXth;
        }
        return objArr2;
    }

    public static <T> T[] select(Object[] objArr, int offset, int length, int start, int everyXth, Object[] objArr2) {
        return select(objArr, offset, length, start, everyXth, objArr2, 0);
    }

    public static <T> T[] select(Object[] objArr, int start, int everyXth, Object[] objArr2) {
        return select(objArr, 0, objArr.length, start, everyXth, objArr2);
    }

    public static <T> T[] select(Object[] objArr, int everyXth, Object[] objArr2) {
        return select(objArr, 0, everyXth, objArr2);
    }

    public static <T> T[] select(Object[] objArr, int offset, int length, int start, int everyXth) {
        return select(objArr, offset, length, start, everyXth, (Object[]) Array.newInstance(objArr.getClass().getComponentType(), selectCount(offset, length, start, everyXth)), 0);
    }

    public static <T> T[] select(Object[] objArr, int start, int everyXth) {
        return select(objArr, 0, objArr.length, start, everyXth);
    }

    public static <T> T[] select(T[] items, int everyXth) {
        return select(items, 0, everyXth);
    }

    public static <T> T[] select(Object[] objArr, int[] indices, int indicesOffset, int indicesLength, Object[] objArr2, int destOffset) {
        checkRegion(indices, indicesOffset, indicesLength);
        if (objArr2 != null) {
            checkRegion(objArr2, destOffset, indicesLength);
        } else {
            objArr2 = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), destOffset + indicesLength);
        }
        int i = indicesOffset;
        int di = destOffset;
        while (i < indicesOffset + indicesLength) {
            objArr2[di] = objArr[indices[i]];
            i++;
            di++;
        }
        return objArr2;
    }

    public static <T> T[] select(Object[] objArr, int[] indices, Object[] objArr2, int destOffset) {
        return select(objArr, indices, 0, indices.length, objArr2, destOffset);
    }

    public static <T> T[] select(Object[] objArr, int[] indices, Object[] objArr2) {
        return select(objArr, indices, objArr2, 0);
    }

    public static <T> T[] select(T[] items, int[] indices) {
        return select(items, indices, (Object[]) null);
    }

    public static void checkRegion(float[] array, int offset, int length) {
        if (array == null) {
            throw new IllegalArgumentException("array is null");
        } else if (offset < 0) {
            throw new ArrayIndexOutOfBoundsException("negative offset: " + offset);
        } else if (length < 0) {
            throw new ArrayIndexOutOfBoundsException("negative length: " + length);
        } else if (offset + length > array.length) {
            throw new ArrayIndexOutOfBoundsException(offset + length);
        }
    }

    public static void requireCapacity(float[] source, int offset, int length, float[] dest, int destOffset) {
        checkRegion(source, offset, length);
        checkRegion(dest, destOffset, length);
    }

    public static float[] select(float[] items, int offset, int length, int start, int everyXth, float[] dest, int destOffset) {
        int outputLength = selectCount(offset, length, start, everyXth);
        checkRegion(dest, destOffset, outputLength);
        int di = destOffset;
        int i = start - 1;
        while (di < outputLength) {
            if (i >= offset) {
                dest[di] = items[i];
                di++;
            }
            i += everyXth;
        }
        return dest;
    }

    public static float[] select(float[] items, int offset, int length, int start, int everyXth, float[] dest) {
        return select(items, offset, length, start, everyXth, dest, 0);
    }

    public static float[] select(float[] items, int start, int everyXth, float[] dest) {
        return select(items, 0, items.length, start, everyXth, dest);
    }

    public static float[] select(float[] items, int everyXth, float[] dest) {
        return select(items, 0, everyXth, dest);
    }

    public static float[] select(float[] items, int offset, int length, int start, int everyXth) {
        return select(items, offset, length, start, everyXth, new float[selectCount(offset, length, start, everyXth)], 0);
    }

    public static float[] select(float[] items, int start, int everyXth) {
        return select(items, 0, items.length, start, everyXth);
    }

    public static float[] select(float[] items, int everyXth) {
        return select(items, 0, everyXth);
    }

    public static float[] select(float[] items, int[] indices, int indicesOffset, int indicesLength, float[] dest, int destOffset) {
        checkRegion(indices, indicesOffset, indicesLength);
        if (dest != null) {
            checkRegion(dest, destOffset, indicesLength);
        } else {
            dest = new float[(destOffset + indicesLength)];
        }
        int i = indicesOffset;
        int di = destOffset;
        while (i < indicesOffset + indicesLength) {
            dest[di] = items[indices[i]];
            i++;
            di++;
        }
        return dest;
    }

    public static float[] select(float[] items, int[] indices, float[] dest, int destOffset) {
        return select(items, indices, 0, indices.length, dest, destOffset);
    }

    public static float[] select(float[] items, int[] indices, float[] dest) {
        return select(items, indices, dest, 0);
    }

    public static float[] select(float[] items, int[] indices) {
        return select(items, indices, (float[]) null);
    }

    public static void checkRegion(int[] array, int offset, int length) {
        if (array == null) {
            throw new IllegalArgumentException("array is null");
        } else if (offset < 0) {
            throw new ArrayIndexOutOfBoundsException("negative offset: " + offset);
        } else if (length < 0) {
            throw new ArrayIndexOutOfBoundsException("negative length: " + length);
        } else if (offset + length > array.length) {
            throw new ArrayIndexOutOfBoundsException(offset + length);
        }
    }

    public static void requireCapacity(int[] source, int offset, int length, int[] dest, int destOffset) {
        checkRegion(source, offset, length);
        checkRegion(dest, destOffset, length);
    }

    public static int[] select(int[] items, int offset, int length, int start, int everyXth, int[] dest, int destOffset) {
        int outputLength = selectCount(offset, length, start, everyXth);
        checkRegion(dest, destOffset, outputLength);
        int di = destOffset;
        int i = start - 1;
        while (di < outputLength) {
            if (i >= offset) {
                dest[di] = items[i];
                di++;
            }
            i += everyXth;
        }
        return dest;
    }

    public static int[] select(int[] items, int offset, int length, int start, int everyXth, int[] dest) {
        return select(items, offset, length, start, everyXth, dest, 0);
    }

    public static int[] select(int[] items, int start, int everyXth, int[] dest) {
        return select(items, 0, items.length, start, everyXth, dest);
    }

    public static int[] select(int[] items, int everyXth, int[] dest) {
        return select(items, 0, everyXth, dest);
    }

    public static int[] select(int[] items, int offset, int length, int start, int everyXth) {
        return select(items, offset, length, start, everyXth, new int[selectCount(offset, length, start, everyXth)], 0);
    }

    public static int[] select(int[] items, int start, int everyXth) {
        return select(items, 0, items.length, start, everyXth);
    }

    public static int[] select(int[] items, int everyXth) {
        return select(items, 0, everyXth);
    }

    public static int[] select(int[] items, int[] indices, int indicesOffset, int indicesLength, int[] dest, int destOffset) {
        checkRegion(indices, indicesOffset, indicesLength);
        if (dest != null) {
            checkRegion(dest, destOffset, indicesLength);
        } else {
            dest = new int[(destOffset + indicesLength)];
        }
        int i = indicesOffset;
        int di = destOffset;
        while (i < indicesOffset + indicesLength) {
            dest[di] = items[indices[i]];
            i++;
            di++;
        }
        return dest;
    }

    public static int[] select(int[] items, int[] indices, int[] dest, int destOffset) {
        return select(items, indices, 0, indices.length, dest, destOffset);
    }

    public static int[] select(int[] items, int[] indices, int[] dest) {
        return select(items, indices, dest, 0);
    }

    public static int[] select(int[] items, int[] indices) {
        return select(items, indices, (int[]) null);
    }
}
