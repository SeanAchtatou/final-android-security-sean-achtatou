package net.dermetfan.utils.math;

import com.kbz.esotericsoftware.spine.Animation;
import net.dermetfan.utils.ArrayUtils;

public class GeometryUtils {
    private static float[] floats = new float[127];

    public static void setFloats(float[] floats2) {
        floats = floats2;
    }

    public static float[] getFloats() {
        return floats;
    }

    public static boolean between(float x, float y, float aX, float aY, float bX, float bY, boolean inclusive) {
        return MathUtils.det(x, y, aX, aY, bX, bY) == Animation.CurveTimeline.LINEAR && MathUtils.between(x, aX, bX, inclusive) && MathUtils.between(y, aY, bY, inclusive);
    }

    public static boolean between(float x, float y, float aX, float aY, float bX, float bY) {
        return between(x, y, aX, aY, bX, bY, true);
    }

    public static float[] add(float[] items, int offset, int length, float x, float y) {
        for (int i = offset + 1; i < offset + length; i += 2) {
            int i2 = i - 1;
            items[i2] = items[i2] + x;
            items[i] = items[i] + y;
        }
        return items;
    }

    public static float[] add(float[] vertices, float x, float y) {
        return add(vertices, 0, vertices.length, x, y);
    }

    public static float[] sub(float[] items, int offset, int length, float x, float y) {
        return add(items, offset, length, -x, -y);
    }

    public static float[] sub(float[] items, float x, float y) {
        return sub(items, 0, items.length, x, y);
    }

    public static float[] addX(float[] items, int offset, int length, float value) {
        return add(items, offset, length, value, Animation.CurveTimeline.LINEAR);
    }

    public static float[] addX(float[] items, float value) {
        return addX(items, 0, items.length, value);
    }

    public static float[] addY(float[] items, int offset, int length, float value) {
        return add(items, offset, length, Animation.CurveTimeline.LINEAR, value);
    }

    public static float[] addY(float[] items, float value) {
        return addY(items, 0, items.length, value);
    }

    public static float[] subX(float[] items, int offset, int length, float value) {
        return sub(items, offset, length, value, Animation.CurveTimeline.LINEAR);
    }

    public static float[] subX(float[] items, float value) {
        return subX(items, 0, items.length, value);
    }

    public static float[] subY(float[] items, int offset, int length, float value) {
        return sub(items, offset, length, Animation.CurveTimeline.LINEAR, value);
    }

    public static float[] subY(float[] items, float value) {
        return subY(items, 0, items.length, value);
    }

    public static float width(float[] vertices, int offset, int length) {
        return MathUtils.amplitude2(filterX(vertices, offset, length, floats), 0, length / 2);
    }

    public static float width(float[] vertices) {
        return width(vertices, 0, vertices.length);
    }

    public static float height(float[] vertices, int offset, int length) {
        return MathUtils.amplitude2(filterY(vertices, offset, length, floats), 0, length / 2);
    }

    public static float height(float[] vertices) {
        return height(vertices, 0, vertices.length);
    }

    public static float depth(float[] vertices, int offset, int length) {
        return MathUtils.amplitude2(filterZ(vertices, offset, length, floats), 0, length / 3);
    }

    public static float depth(float[] vertices) {
        return depth(vertices, 0, vertices.length);
    }

    public static float[] filterX(float[] vertices, int offset, int length, float[] dest, int destOffset) {
        ArrayUtils.checkRegion(vertices, offset, length);
        return ArrayUtils.select(vertices, offset, length, -1, 2, dest, destOffset);
    }

    public static float[] filterX(float[] vertices, int offset, int length, float[] dest) {
        return filterX(vertices, offset, length, dest, 0);
    }

    public static float[] filterX(float[] vertices, float[] dest) {
        return filterX(vertices, 0, vertices.length, dest);
    }

    public static float[] filterX(float[] vertices, int offset, int length) {
        return filterX(vertices, offset, length, new float[(length / 2)]);
    }

    public static float[] filterX(float[] vertices) {
        return filterX(vertices, new float[(vertices.length / 2)]);
    }

    public static float[] filterY(float[] vertices, int offset, int length, float[] dest, int destOffset) {
        ArrayUtils.checkRegion(vertices, offset, length);
        return ArrayUtils.select(vertices, offset, length, 0, 2, dest, destOffset);
    }

    public static float[] filterY(float[] vertices, int offset, int length, float[] dest) {
        return filterY(vertices, offset, length, dest, 0);
    }

    public static float[] filterY(float[] vertices, float[] dest) {
        return filterY(vertices, 0, vertices.length, dest);
    }

    public static float[] filterY(float[] vertices, int offset, int length) {
        return filterY(vertices, offset, length, new float[(length / 2)]);
    }

    public static float[] filterY(float[] vertices) {
        return filterY(vertices, new float[(vertices.length / 2)]);
    }

    public static float[] filterZ(float[] vertices, int offset, int length, float[] dest, int destOffset) {
        ArrayUtils.checkRegion(vertices, offset, length);
        return ArrayUtils.select(vertices, offset, length, 0, 3, dest, destOffset);
    }

    public static float[] filterZ(float[] vertices, int offset, int length, float[] dest) {
        return filterZ(vertices, offset, length, dest, 0);
    }

    public static float[] filterZ(float[] vertices, float[] dest) {
        return filterZ(vertices, 0, vertices.length, dest);
    }

    public static float[] filterZ(float[] vertices, int offset, int length) {
        return filterZ(vertices, offset, length, new float[(length / 3)]);
    }

    public static float[] filterZ(float[] vertices) {
        return filterZ(vertices, new float[(vertices.length / 3)]);
    }

    public static float[] filterW(float[] vertices, int offset, int length, float[] dest, int destOffset) {
        ArrayUtils.checkRegion(vertices, offset, length);
        return ArrayUtils.select(vertices, offset, length, 0, 4, dest, destOffset);
    }

    public static float[] filterW(float[] vertices, int offset, int length, float[] dest) {
        return filterW(vertices, offset, length, dest, 0);
    }

    public static float[] filterW(float[] vertices, float[] dest) {
        return filterW(vertices, 0, vertices.length, dest);
    }

    public static float[] filterW(float[] vertices, int offset, int length) {
        return filterW(vertices, offset, length, new float[(length / 4)]);
    }

    public static float[] filterW(float[] vertices) {
        return filterW(vertices, new float[(vertices.length / 4)]);
    }

    public static float minX(float[] vertices, int offset, int length) {
        return MathUtils.min(filterX(vertices, offset, length, floats), 0, length / 2);
    }

    public static float minX(float[] vertices) {
        return minX(vertices, 0, vertices.length);
    }

    public static float minY(float[] vertices, int offset, int length) {
        return MathUtils.min(filterY(vertices, offset, length, floats), 0, length / 2);
    }

    public static float minY(float[] vertices) {
        return minY(vertices, 0, vertices.length);
    }

    public static float maxX(float[] vertices, int offset, int length) {
        return MathUtils.max(filterX(vertices, offset, length, floats), 0, length / 2);
    }

    public static float maxX(float[] vertices) {
        return maxX(vertices, 0, vertices.length);
    }

    public static float maxY(float[] vertices, int offset, int length) {
        return MathUtils.max(filterY(vertices, offset, length, floats), 0, length / 2);
    }

    public static float maxY(float[] vertices) {
        return maxY(vertices, 0, vertices.length);
    }

    public static float[] rotate(float x, float y, float width, float height, float radians, float[] output, int offset) {
        if (output == null || offset + 8 > output.length - 1) {
            output = new float[8];
        }
        float rad = (float) (Math.sqrt((double) ((height * height) + (width * width))) / 2.0d);
        float theta = (float) Math.atan2((double) height, (double) width);
        float x0 = (float) (((double) rad) * Math.cos((double) (theta + radians)));
        float y0 = (float) (((double) rad) * Math.sin((double) (theta + radians)));
        float x1 = (float) (((double) rad) * Math.cos((double) ((-theta) + radians)));
        float y1 = (float) (((double) rad) * Math.sin((double) ((-theta) + radians)));
        float offsetX = x + (width / 2.0f);
        float offsetY = y + (height / 2.0f);
        output[offset] = offsetX + x0;
        output[offset + 1] = offsetY + y0;
        output[offset + 2] = offsetX + x1;
        output[offset + 3] = offsetY + y1;
        output[offset + 4] = offsetX - x0;
        output[offset + 5] = offsetY - y0;
        output[offset + 6] = offsetX - x1;
        output[offset + 7] = offsetY - y1;
        return output;
    }

    public static float invertAxis(float coord, float axisSize) {
        return MathUtils.mirror(coord, axisSize / 2.0f);
    }

    public static float[] invertAxes(float[] vertices, int offset, int length, boolean x, boolean y) {
        int i;
        if (x || y) {
            float height = height(vertices, offset, length);
            float width = width(vertices, offset, length);
            int i2 = (x ? 0 : 1) + offset;
            while (i2 < offset + length) {
                vertices[i2] = i2 % 2 == 0 ? invertAxis(vertices[i2], width) : invertAxis(vertices[i2], height);
                if (x ^ y) {
                    i = 2;
                } else {
                    i = 1;
                }
                i2 += i;
            }
        }
        return vertices;
    }

    public static float[] invertAxes(float[] vertices, boolean x, boolean y) {
        return invertAxes(vertices, 0, vertices.length, x, y);
    }

    public static float[] toYDown(float[] vertices, int offset, int length) {
        ArrayUtils.checkRegion(vertices, offset, length);
        invertAxes(vertices, offset, length, false, true);
        return subY(vertices, offset, length, height(vertices, offset, length));
    }

    public static float[] toYDown(float[] vertices) {
        return toYDown(vertices, 0, vertices.length);
    }

    public static float[] toYUp(float[] vertices, int offset, int length) {
        ArrayUtils.checkRegion(vertices, offset, length);
        invertAxes(vertices, offset, length, false, true);
        return subY(vertices, offset, length, height(vertices, offset, length));
    }

    public static float[] toYUp(float[] vertices) {
        return toYUp(vertices, 0, vertices.length);
    }
}
