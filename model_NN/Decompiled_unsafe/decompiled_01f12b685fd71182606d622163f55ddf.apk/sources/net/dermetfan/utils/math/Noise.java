package net.dermetfan.utils.math;

import java.lang.reflect.Array;
import java.util.Random;
import net.dermetfan.utils.ArrayUtils;
import net.dermetfan.utils.Function;
import net.dermetfan.utils.Pair;

public abstract class Noise {
    private static Random random = new Random();
    private static long seed = -1;
    private static boolean seedEnabled;

    public static float[] midpointDisplacement(float[] values, float range, float smoothness) {
        int i = 0;
        while (i < values.length) {
            values[i] = ((ArrayUtils.wrapIndex(i - 1, values) + ArrayUtils.wrapIndex(i + 1, values)) / 2.0f) + random(-range, range);
            i++;
            range /= smoothness;
        }
        return values;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][]
     arg types: [int, float, float, int, ?[OBJECT, ARRAY], int, int]
     candidates:
      net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, float, int, int):float[][]
      net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][] */
    public static float[][] midpointDisplacement(int n, float smoothness, float range, int scaleX, int scaleY) {
        return midpointDisplacement(n, range, smoothness, true, (Function<Float, Pair<Float, Float>>) null, scaleX, scaleY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][]
     arg types: [int, float, float, int, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int]
     candidates:
      net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, float, int, int):float[][]
      net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][] */
    public static float[][] midpointDisplacement(int n, float smoothness, float range, Function<Float, Pair<Float, Float>> init, int scaleX, int scaleY) {
        return midpointDisplacement(n, range, smoothness, false, init, scaleX, scaleY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, float, int, int):float[][]
     arg types: [int, float, float, int, float, int, int]
     candidates:
      net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][]
      net.dermetfan.utils.math.Noise.midpointDisplacement(int, float, float, boolean, float, int, int):float[][] */
    public static float[][] midpointDisplacement(int n, float smoothness, float range, float init, int scaleX, int scaleY) {
        return midpointDisplacement(n, range, smoothness, false, init, scaleX, scaleY);
    }

    public static float[][] midpointDisplacement(int n, float smoothness, float range, boolean initializeRandomly, final float init, int scaleX, int scaleY) {
        return midpointDisplacement(n, range, smoothness, initializeRandomly, initializeRandomly ? null : new Function<Float, Pair<Float, Float>>() {
            public /* bridge */ /* synthetic */ Object apply(Object obj) {
                return apply((Pair<Float, Float>) ((Pair) obj));
            }

            public Float apply(Pair<Float, Float> pair) {
                return Float.valueOf(init);
            }
        }, scaleX, scaleY);
    }

    private static float[][] midpointDisplacement(int n, float smoothness, float range, boolean initializeRandomly, Function<Float, Pair<Float, Float>> init, int scaleX, int scaleY) {
        if (n < 0) {
            throw new IllegalArgumentException("n must be >= 0: " + n);
        }
        float range2 = range / 2.0f;
        int power = (int) Math.pow(2.0d, (double) n);
        int width = (scaleX * power) + 1;
        int height = (scaleY * power) + 1;
        float[][] map = (float[][]) Array.newInstance(Float.TYPE, width, height);
        Pair<Float, Float> coord = new Pair<>();
        for (int x = 0; x < width; x += power) {
            for (int y = 0; y < height; y += power) {
                map[x][y] = initializeRandomly ? random(-range2, range2) : init.apply(coord.set(Float.valueOf((float) x), Float.valueOf((float) y))).floatValue();
            }
        }
        int step = power / 2;
        while (step > 0) {
            boolean sx = false;
            int x2 = 0;
            while (x2 < width) {
                boolean sy = false;
                int y2 = 0;
                while (y2 < height) {
                    if (sx && sy) {
                        map[x2][y2] = ((((map[x2 - step][y2 - step] + map[x2 + step][y2 - step]) + map[x2 - step][y2 + step]) + map[x2 + step][y2 + step]) / 4.0f) + random(-range2, range2);
                    } else if (sx) {
                        map[x2][y2] = ((map[x2 - step][y2] + map[x2 + step][y2]) / 2.0f) + random(-range2, range2);
                    } else if (sy) {
                        map[x2][y2] = ((map[x2][y2 - step] + map[x2][y2 + step]) / 2.0f) + random(-range2, range2);
                    }
                    y2 += step;
                    sy = !sy;
                }
                x2 += step;
                sx = !sx;
            }
            step /= 2;
            range2 /= smoothness;
        }
        return map;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][]
     arg types: [int, float, float, boolean, boolean, int, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int]
     candidates:
      net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, float, int, int):float[][]
      net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][] */
    public static float[][] diamondSquare(int n, float smoothness, float range, boolean wrapX, boolean wrapY, Function<Float, Pair<Float, Float>> init, int scaleX, int scaleY) {
        return diamondSquare(n, smoothness, range, wrapX, wrapY, false, init, scaleX, scaleY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, float, int, int):float[][]
     arg types: [int, float, float, boolean, boolean, int, int, int, int]
     candidates:
      net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][]
      net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, float, int, int):float[][] */
    public static float[][] diamondSquare(int n, float smoothness, float range, boolean wrapX, boolean wrapY, int scaleX, int scaleY) {
        return diamondSquare(n, smoothness, range, wrapX, wrapY, true, Float.NaN, scaleX, scaleY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, float, int, int):float[][]
     arg types: [int, float, float, boolean, boolean, int, float, int, int]
     candidates:
      net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, net.dermetfan.utils.Function<java.lang.Float, net.dermetfan.utils.Pair<java.lang.Float, java.lang.Float>>, int, int):float[][]
      net.dermetfan.utils.math.Noise.diamondSquare(int, float, float, boolean, boolean, boolean, float, int, int):float[][] */
    public static float[][] diamondSquare(int n, float smoothness, float range, boolean wrapX, boolean wrapY, float init, int scaleX, int scaleY) {
        return diamondSquare(n, smoothness, range, wrapX, wrapY, false, init, scaleX, scaleY);
    }

    public static float[][] diamondSquare(int n, float smoothness, float range, boolean wrapX, boolean wrapY, boolean initializeRandomly, final float init, int scaleX, int scaleY) {
        return diamondSquare(n, smoothness, range, wrapX, wrapY, initializeRandomly, initializeRandomly ? null : new Function<Float, Pair<Float, Float>>() {
            public /* bridge */ /* synthetic */ Object apply(Object obj) {
                return apply((Pair<Float, Float>) ((Pair) obj));
            }

            public Float apply(Pair<Float, Float> pair) {
                return Float.valueOf(init);
            }
        }, scaleX, scaleY);
    }

    private static float[][] diamondSquare(int n, float smoothness, float range, boolean wrapX, boolean wrapY, boolean initializeRandomly, Function<Float, Pair<Float, Float>> init, int scaleX, int scaleY) {
        if (n < 0) {
            throw new IllegalArgumentException("n must be >= 0: " + n);
        }
        float range2 = range / 2.0f;
        int power = (int) Math.pow(2.0d, (double) n);
        int width = (scaleX * power) + 1;
        int height = (scaleY * power) + 1;
        float[][] map = (float[][]) Array.newInstance(Float.TYPE, width, height);
        Pair<Float, Float> coord = new Pair<>();
        for (int x = 0; x < width; x += power) {
            for (int y = 0; y < height; y += power) {
                map[x][y] = initializeRandomly ? random(-range2, range2) : init.apply(coord.set(Float.valueOf((float) x), Float.valueOf((float) y))).floatValue();
            }
        }
        int power2 = power / 2;
        while (power2 > 0) {
            for (int x2 = power2; x2 < width; x2 += power2 * 2) {
                for (int y2 = power2; y2 < height; y2 += power2 * 2) {
                    map[x2][y2] = ((((map[x2 - power2][y2 - power2] + map[x2 - power2][y2 + power2]) + map[x2 + power2][y2 + power2]) + map[x2 + power2][y2 - power2]) / 4.0f) + random(-range2, range2);
                }
            }
            int x3 = 0;
            while (true) {
                if (x3 >= width - (wrapX ? 1 : 0)) {
                    break;
                }
                int y3 = power2 * (1 - ((x3 / power2) % 2));
                while (true) {
                    if (y3 >= height - (wrapY ? 1 : 0)) {
                        break;
                    }
                    float[] fArr = map[x3];
                    float avg = (((map[ArrayUtils.wrapIndex(x3 - power2, width)][y3] + map[ArrayUtils.wrapIndex(x3 + power2, width)][y3]) + map[x3][ArrayUtils.wrapIndex(y3 - power2, height)]) + map[x3][ArrayUtils.wrapIndex(y3 + power2, height)]) / 4.0f;
                    fArr[y3] = random(-range2, range2) + avg;
                    if (wrapX && x3 == 0) {
                        map[width - 1][y3] = avg;
                    }
                    if (wrapY && y3 == 0) {
                        map[x3][height - 1] = avg;
                    }
                    y3 += power2 * 2;
                }
                x3 += power2;
            }
            power2 /= 2;
            range2 /= smoothness;
        }
        return map;
    }

    public static float random(float start, float end) {
        return (random.nextFloat() * (end - start)) + start;
    }

    public static void setSeedEnabled(boolean seedEnabled2) {
        seedEnabled = seedEnabled2;
        if (seedEnabled2) {
            random.setSeed(seed);
        } else {
            random = new Random();
        }
    }

    public static boolean isSeedEnabled() {
        return seedEnabled;
    }

    public static long getSeed() {
        return seed;
    }

    public static void setSeed(long seed2) {
        Random random2 = random;
        seed = seed2;
        random2.setSeed(seed2);
    }

    public static Random getRandom() {
        return random;
    }
}
