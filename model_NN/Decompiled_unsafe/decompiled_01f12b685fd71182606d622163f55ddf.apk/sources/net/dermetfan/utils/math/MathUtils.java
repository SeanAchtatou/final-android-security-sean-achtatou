package net.dermetfan.utils.math;

import com.kbz.esotericsoftware.spine.Animation;
import net.dermetfan.utils.ArrayUtils;

public class MathUtils {
    public static int crossSum(int n) {
        int csum = 0;
        while (n > 0) {
            csum += n % 10;
            n /= 10;
        }
        return csum;
    }

    public static int factorial(int n) {
        return (int) factorial((float) n);
    }

    public static float factorial(float n) {
        if (n < Animation.CurveTimeline.LINEAR) {
            throw new IllegalArgumentException("n must be >= 0: " + n);
        } else if (n <= 1.0f) {
            return 1.0f;
        } else {
            return factorial(n - 1.0f) * n;
        }
    }

    public static boolean between(float value, float min, float max) {
        return between(value, min, max, false);
    }

    public static boolean between(float value, float min, float max, boolean inclusive) {
        float min2 = Math.min(min, max);
        float max2 = Math.max(min2, max);
        if (inclusive) {
            if (value < min2 || value > max2) {
                return false;
            }
            return true;
        } else if (value <= min2 || value >= max2) {
            return false;
        } else {
            return true;
        }
    }

    public static float det(float x1, float y1, float x2, float y2, float x3, float y3) {
        return (((((x1 * y2) + (x2 * y3)) + (x3 * y1)) - (y1 * x2)) - (y2 * x3)) - (y3 * x1);
    }

    public static float normalize(float value, float range) {
        return normalize(value, Animation.CurveTimeline.LINEAR, range);
    }

    public static float normalize(float value, float min, float max) {
        float under;
        if (min == max) {
            return min;
        }
        float oldMin = min;
        float oldMax = max;
        float min2 = Math.min(min, max);
        float max2 = Math.max(oldMin, max);
        if (value < min2) {
            under = Math.abs(min2 - value);
        } else {
            under = 0.0f;
        }
        float over = value > max2 ? value - Math.abs(max2) : 0.0f;
        if (under > Animation.CurveTimeline.LINEAR) {
            if (oldMax > oldMin) {
                under = -under;
            }
            return normalize(oldMax + under, min2, max2);
        } else if (over <= Animation.CurveTimeline.LINEAR) {
            return value;
        } else {
            if (oldMin >= oldMax) {
                over = -over;
            }
            return normalize(oldMin + over, min2, max2);
        }
    }

    public static float mirror(float value, float baseline) {
        return (2.0f * baseline) - value;
    }

    public static float replaceNaN(float value, float replacement) {
        return Float.isNaN(value) ? replacement : value;
    }

    public static <T> T elementAtSum(float sum, float[] values, T[] elements, int valuesOffset, int valuesLength, int elementsOffset, int elementsLength) {
        float total = Animation.CurveTimeline.LINEAR;
        for (int i = valuesOffset; i < valuesOffset + valuesLength; i++) {
            total += values[i];
            if (total >= sum) {
                return elements[(elementsOffset + i) - valuesOffset];
            }
        }
        return total <= Animation.CurveTimeline.LINEAR ? elements[elementsOffset] : elements[(elementsOffset + elementsLength) - 1];
    }

    public static <T> T elementAtSum(float sum, float[] values, T[] elements) {
        return elementAtSum(sum, values, elements, 0, values.length, 0, elements.length);
    }

    public static float[] clamp(float[] items, float min, float max, int offset, int length) {
        for (int i = offset; i < offset + length; i++) {
            items[i] = com.badlogic.gdx.math.MathUtils.clamp(items[i], min, max);
        }
        return items;
    }

    public static float[] clamp(float[] items, float min, float max) {
        return clamp(items, min, max, 0, items.length);
    }

    public static float[] abs(float[] items, int offset, int length) {
        for (int i = offset; i < offset + length; i++) {
            items[i] = Math.abs(items[i]);
        }
        return items;
    }

    public static float[] abs(float[] items) {
        return abs(items, 0, items.length);
    }

    public static float[] add(float[] items, float value, int offset, int length) {
        for (int i = offset; i < offset + length; i++) {
            items[i] = items[i] + value;
        }
        return items;
    }

    public static float[] add(float[] items, float value) {
        return add(items, value, 0, items.length);
    }

    public static float[] sub(float[] items, float value, int offset, int length) {
        return add(items, -value, offset, length);
    }

    public static float[] sub(float[] items, float value) {
        return sub(items, value, 0, items.length);
    }

    public static float[] mul(float[] items, float factor, int offset, int length) {
        for (int i = offset; i < offset + length; i++) {
            items[i] = items[i] * factor;
        }
        return items;
    }

    public static float[] mul(float[] items, float factor) {
        return mul(items, factor, 0, items.length);
    }

    public static float[] div(float[] items, float divisor, int offset, int length) {
        return mul(items, 1.0f / divisor, offset, length);
    }

    public static float[] div(float[] items, float divisor) {
        return div(items, divisor, 0, items.length);
    }

    public static float approachZero(float value, float amount) {
        float amount2 = Math.abs(amount);
        if (amount2 > Math.abs(value) || value == Animation.CurveTimeline.LINEAR) {
            return Animation.CurveTimeline.LINEAR;
        }
        if (value <= Animation.CurveTimeline.LINEAR) {
            amount2 = -amount2;
        }
        return value - amount2;
    }

    public static float sum(float[] items, int offset, int length) {
        float sum = Animation.CurveTimeline.LINEAR;
        for (int i = offset; i < offset + length; i++) {
            sum += items[i];
        }
        return sum;
    }

    public static float sum(float[] items) {
        return sum(items, 0, items.length);
    }

    public static float amplitude2(float[] items, int offset, int length) {
        return max(items, offset, length) - min(items, offset, length);
    }

    public static float amplitude2(float[] items) {
        return amplitude2(items, 0, items.length);
    }

    public static float max(float[] items, int offset, int length) {
        ArrayUtils.checkRegion(items, offset, length);
        if (length == 0) {
            return Float.NaN;
        }
        float max = Float.NEGATIVE_INFINITY;
        for (int i = offset; i < offset + length; i++) {
            float f = items[i];
            if (f > max) {
                max = f;
            }
        }
        return max;
    }

    public static float max(float[] items) {
        return max(items, 0, items.length);
    }

    public static float min(float[] items, int offset, int length) {
        ArrayUtils.checkRegion(items, offset, length);
        if (length == 0) {
            return Float.NaN;
        }
        float min = Float.POSITIVE_INFINITY;
        for (int i = offset; i < offset + length; i++) {
            float f = items[i];
            if (f < min) {
                min = f;
            }
        }
        return min;
    }

    public static float min(float[] items) {
        return min(items, 0, items.length);
    }

    public static float nearest(float value, float[] items, float minDiff, int offset, int length) {
        if (value == Float.POSITIVE_INFINITY) {
            return max(items, offset, length);
        }
        if (value == Float.NEGATIVE_INFINITY) {
            return min(items, offset, length);
        }
        float smallestDiff = Float.POSITIVE_INFINITY;
        float nearest = Float.NaN;
        for (int i = offset; i < offset + length; i++) {
            float diff = Math.abs(value - items[i]);
            if (diff >= minDiff && diff < smallestDiff) {
                smallestDiff = diff;
                nearest = items[i];
            }
        }
        return nearest;
    }

    public static float nearest(float value, float[] items, float minDiff) {
        return nearest(value, items, minDiff, 0, items.length);
    }

    public static float nearest(float value, float[] items, int offset, int length) {
        return nearest(value, items, Animation.CurveTimeline.LINEAR, offset, length);
    }

    public static float nearest(float value, float[] items) {
        return nearest(value, items, 0, items.length);
    }

    public static float[] scale(float[] items, float min, float max, int offset, int length) {
        float tmp = amplitude2(items, offset, length) / (max - min);
        for (int i = offset; i < offset + length; i++) {
            items[i] = items[i] / tmp;
        }
        float tmp2 = min - min(items, offset, length);
        for (int i2 = offset; i2 < offset + length; i2++) {
            items[i2] = items[i2] + tmp2;
        }
        return items;
    }

    public static float[] scale(float[] items, float min, float max) {
        return scale(items, min, max, 0, items.length);
    }
}
