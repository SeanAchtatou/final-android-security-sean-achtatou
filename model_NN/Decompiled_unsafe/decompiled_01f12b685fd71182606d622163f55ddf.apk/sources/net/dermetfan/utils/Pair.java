package net.dermetfan.utils;

import com.badlogic.gdx.utils.reflect.ClassReflection;

public class Pair<K, V> {
    private K key;
    private V value;

    public Pair() {
    }

    public Pair(K key2, V value2) {
        this.key = key2;
        this.value = value2;
    }

    public Pair(Pair<K, V> pair) {
        this.key = pair.key;
        this.value = pair.value;
    }

    public Pair<K, V> set(Pair<K, V> pair) {
        this.key = pair.key;
        this.value = pair.value;
        return this;
    }

    public Pair<K, V> set(K key2, V value2) {
        this.key = key2;
        this.value = value2;
        return this;
    }

    public void clear() {
        this.key = null;
        this.value = null;
    }

    public void clearKey() {
        this.key = null;
    }

    public void clearValue() {
        this.value = null;
    }

    public boolean isEmpty() {
        return this.key == null && this.value == null;
    }

    public boolean isFull() {
        return (this.key == null || this.value == null) ? false : true;
    }

    public boolean hasKey() {
        return this.key != null;
    }

    public boolean hasValue() {
        return this.value != null;
    }

    public void swap() throws IllegalStateException {
        if (this.key.getClass() != this.value.getClass()) {
            throw new IllegalStateException("key and value are not of the same type: " + ClassReflection.getSimpleName(this.key.getClass()) + " - " + ClassReflection.getSimpleName(this.value.getClass()));
        }
        Object obj = this.value;
        this.value = this.key;
        this.key = obj;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.value != null) {
            i = this.value.hashCode();
        }
        return i2 + i;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Pair)) {
            return super.equals(obj);
        }
        Pair<?, ?> pair = (Pair) obj;
        return this.key.equals(pair.key) && this.value.equals(pair.value);
    }

    public String toString() {
        return "[" + ((Object) this.key) + " & " + ((Object) this.value) + ']';
    }

    public K getKey() {
        return this.key;
    }

    public void setKey(K key2) {
        this.key = key2;
    }

    public V getValue() {
        return this.value;
    }

    public void setValue(V value2) {
        this.value = value2;
    }
}
