package net.dermetfan.utils;

import net.dermetfan.utils.math.MathUtils;

public class Appender {
    private CharSequence[] appendices;
    private float[] durations;
    private int index;
    private float time;

    public static CharSequence append(CharSequence seq, CharSequence appendix) {
        return seq.toString() + ((Object) appendix);
    }

    public static CharSequence appendixAt(float time2, CharSequence[] appendices2, float[] durations2) {
        return (CharSequence) MathUtils.elementAtSum(time2, durations2, appendices2);
    }

    public Appender(CharSequence appendices2, float durations2) {
        set(new CharSequence[]{appendices2}, new float[]{durations2});
    }

    public Appender(CharSequence[] appendices2, float durations2) {
        this(appendices2, new float[appendices2.length]);
        for (int i = 0; i < appendices2.length; i++) {
            this.durations[i] = durations2;
        }
    }

    public Appender(CharSequence[] appendices2, float[] durations2) {
        set(appendices2, durations2);
    }

    public float update(float delta) {
        float f = this.time + delta;
        this.time = f;
        if (f > this.durations[this.index]) {
            this.time -= this.durations[this.index];
            int i = this.index + 1;
            this.index = i;
            if (i >= this.appendices.length) {
                this.index = 0;
            }
            if (this.time > this.durations[this.index]) {
                this.time -= ((float) ((int) (this.time / this.durations[this.index]))) * this.durations[this.index];
            }
        }
        return (float) this.index;
    }

    public CharSequence appendixAt(float time2) {
        return appendixAt(time2, this.appendices, this.durations);
    }

    public CharSequence append(CharSequence seq) {
        return append(seq, this.appendices[com.badlogic.gdx.math.MathUtils.clamp(this.index, 0, this.appendices.length)]);
    }

    public CharSequence append(CharSequence seq, float time2) {
        return append(seq, appendixAt(time2));
    }

    public CharSequence updateAndAppend(CharSequence seq, float delta) {
        update(delta);
        return append(seq);
    }

    public void set(CharSequence[] appendices2, float[] durations2) {
        if (appendices2.length != durations2.length) {
            throw new IllegalArgumentException("appendices[] and durations[] must have the same length: " + appendices2.length + ", " + durations2.length);
        }
        this.appendices = appendices2;
        this.durations = durations2;
    }

    public void set(CharSequence[] appendices2, float durations2) {
        this.appendices = appendices2;
        this.durations = new float[appendices2.length];
        for (int i = 0; i < appendices2.length; i++) {
            this.durations[i] = durations2;
        }
    }

    public CharSequence[] getAppendices() {
        return this.appendices;
    }

    public void setAppendices(CharSequence[] appendices2) {
        set(appendices2, this.durations);
    }

    public float[] getDurations() {
        return this.durations;
    }

    public void setDurations(float[] durations2) {
        set(this.appendices, durations2);
    }

    public float getTime() {
        return this.time;
    }

    public void setTime(float time2) {
        this.time = time2;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index2) {
        this.index = index2;
    }
}
