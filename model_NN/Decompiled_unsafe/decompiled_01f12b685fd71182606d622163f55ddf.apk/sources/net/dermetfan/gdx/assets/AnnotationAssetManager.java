package net.dermetfan.gdx.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class AnnotationAssetManager extends AssetManager {

    @Documented
    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Asset {
        boolean load() default true;

        String params() default "";

        Class<?> value() default void.class;
    }

    public AnnotationAssetManager() {
    }

    public AnnotationAssetManager(FileHandleResolver resolver) {
        super(resolver);
    }

    public void load(Object container) {
        for (Field field : ClassReflection.getDeclaredFields(container.getClass())) {
            if (field.isAnnotationPresent(Asset.class) && ((Asset) field.getDeclaredAnnotation(Asset.class).getAnnotation(Asset.class)).load()) {
                load(field, container);
            }
        }
    }

    public void load(Class<?> container) {
        for (Field field : ClassReflection.getDeclaredFields(container)) {
            if (field.isAnnotationPresent(Asset.class) && ((Asset) field.getDeclaredAnnotation(Asset.class).getAnnotation(Asset.class)).load()) {
                load(field);
            }
        }
    }

    public void load(Field field, Object container) {
        int assetCount = getAssetCount(field, container);
        int i = 0;
        while (i < assetCount) {
            boolean single = assetCount == 1;
            if (single) {
                i = -1;
            }
            String path = getAssetPath(field, container, i);
            Class<?> type = getAssetType(field, container, i);
            AssetLoaderParameters params = getAssetLoaderParameters(field, container, i);
            if (path == null || type == null) {
                Gdx.app.debug(ClassReflection.getSimpleName(getClass()), '@' + ClassReflection.getSimpleName(Asset.class) + " (" + path + ", " + type + ") " + field.getName());
            }
            load(path, type, params);
            if (!single) {
                i++;
            } else {
                return;
            }
        }
    }

    public void load(Field field) {
        load(field, (Object) null);
    }

    public static int getAssetCount(Field field, Object container) {
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        try {
            if (field.getType().getSimpleName().endsWith("[]")) {
                return ((Object[]) field.get(container)).length;
            }
            return field.get(container) == null ? 0 : 1;
        } catch (ReflectionException e) {
            Gdx.app.error("AnnotationAssetManager", "could not access field \"" + field.getName() + "\" of class " + ClassReflection.getSimpleName(field.getDeclaringClass()) + " and instance " + container, e);
            return 1;
        }
    }

    public static String getAssetPath(Field field, Object container, int index) {
        try {
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            Object content = index < 0 ? field.get(container) : ((Object[]) field.get(container))[index];
            if (content instanceof AssetDescriptor) {
                return ((AssetDescriptor) content).fileName;
            }
            if (content instanceof FileHandle) {
                return ((FileHandle) content).path();
            }
            return content.toString();
        } catch (ReflectionException | IllegalArgumentException e) {
            Gdx.app.error("AnnotationAssetManager", "could not access field \"" + field.getName() + "\"", e);
            return null;
        }
    }

    public static String getAssetPath(Field field, Object container) {
        return getAssetPath(field, container, -1);
    }

    public static Class<?> getAssetType(Field field, Object container, int index) {
        Object content;
        if (field.isAnnotationPresent(Asset.class)) {
            return ((Asset) field.getDeclaredAnnotation(Asset.class).getAnnotation(Asset.class)).value();
        }
        if (!ClassReflection.isAssignableFrom(AssetDescriptor.class, field.getType()) && index < 0) {
            return null;
        }
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        if (index < 0) {
            try {
                content = field.get(container);
            } catch (ReflectionException e) {
                Gdx.app.error("AnnotationAssetManager", "could not access field \"" + field.getName() + "\"", e);
            }
        } else {
            content = ((Object[]) field.get(container))[index];
        }
        if (content instanceof AssetDescriptor) {
            return ((AssetDescriptor) content).type;
        }
        return null;
    }

    public static Class<?> getAssetType(Field field, Object container) {
        return getAssetType(field, container, -1);
    }

    public static <T> AssetLoaderParameters<T> getAssetLoaderParameters(Field field, Object container, int index) {
        Object content;
        if (field.isAnnotationPresent(Asset.class)) {
            String params = ((Asset) field.getDeclaredAnnotation(Asset.class).getAnnotation(Asset.class)).params();
            if (params.length() > 0) {
                Field paramsField = null;
                if (params.contains(".")) {
                    int lastPeriod = params.lastIndexOf(46);
                    String className = params.substring(0, lastPeriod);
                    try {
                        paramsField = ClassReflection.getDeclaredField(ClassReflection.forName(className), params.substring(lastPeriod + 1));
                    } catch (ReflectionException e) {
                        Gdx.app.error("AnnotationAssetManager", "could not access class " + className, e);
                    }
                } else {
                    try {
                        paramsField = ClassReflection.getDeclaredField(field.getDeclaringClass(), params);
                    } catch (ReflectionException e2) {
                        Gdx.app.error("AnnotationAssetManager", "could not access field \"" + field.getName() + "\" of class " + ClassReflection.getSimpleName(field.getDeclaringClass()), e2);
                    }
                }
                if (paramsField != null) {
                    if (ClassReflection.isAssignableFrom(AssetLoaderParameters.class, paramsField.getType())) {
                        try {
                            if (!paramsField.isAccessible()) {
                                paramsField.setAccessible(true);
                            }
                            return (AssetLoaderParameters) paramsField.get(container);
                        } catch (ReflectionException e3) {
                            Gdx.app.error("AnnotationAssetManager", "could not access value of field \"" + paramsField.getName() + "\" of class " + ClassReflection.getSimpleName(paramsField.getDeclaringClass()) + " and instance " + container, e3);
                        }
                    } else {
                        Gdx.app.debug("AnnotationAssetManager", "field \"" + paramsField.getName() + "\" of class " + ClassReflection.getSimpleName(paramsField.getDeclaringClass()) + " and instance " + container + " is not assignable from AssetLoaderParameters");
                    }
                }
            }
        }
        if (!ClassReflection.isAssignableFrom(AssetDescriptor.class, field.getType()) && index < 0) {
            return null;
        }
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        if (index < 0) {
            try {
                content = field.get(container);
            } catch (ReflectionException | IllegalArgumentException e4) {
                Gdx.app.error("AnnotationAssetManager", "could not access field\"" + field.getName() + "\"", e4);
            }
        } else {
            content = ((Object[]) field.get(container))[index];
        }
        if (content instanceof AssetDescriptor) {
            return ((AssetDescriptor) content).params;
        }
        return null;
    }

    public static <T> AssetLoaderParameters<T> getAssetLoaderParameters(Field field, Object container) {
        return getAssetLoaderParameters(field, container, -1);
    }

    public <T> AssetDescriptor<T> createAssetDescriptor(Field field, Object container) {
        Class<?> fieldType = field.getType();
        Class<?> type = getAssetType(field, container, -1);
        if (fieldType == AssetDescriptor.class) {
            try {
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }
                AssetDescriptor<T> assetDescriptor = (AssetDescriptor) field.get(container);
                if (assetDescriptor.type == type) {
                    return assetDescriptor;
                }
                return new AssetDescriptor<>(assetDescriptor.file, type, assetDescriptor.params);
            } catch (ReflectionException | IllegalArgumentException e) {
                Gdx.app.error(ClassReflection.getSimpleName(getClass()), "couldn't access field \"" + field.getName() + "\"", e);
                return null;
            }
        } else {
            try {
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }
                if (fieldType == FileHandle.class) {
                    return new AssetDescriptor<>((FileHandle) field.get(container), type);
                }
                return new AssetDescriptor<>(field.get(container).toString(), type);
            } catch (ReflectionException | IllegalArgumentException e2) {
                Gdx.app.error(ClassReflection.getSimpleName(getClass()), "couldn't access field \"" + field.getName() + "\"", e2);
            }
        }
    }

    public <T> AssetDescriptor<T> createAssetDescriptor(Field field) {
        return createAssetDescriptor(field, null);
    }
}
