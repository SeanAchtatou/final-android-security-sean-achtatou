package net.dermetfan.gdx.math;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;

public abstract class BayazitDecomposer {
    public static final float EPSILON = 1.1920929E-7f;
    public static int maxPolygonVertices = 8;

    public static Vector2 cross(Vector2 a, float s) {
        return new Vector2(a.y * s, (-s) * a.x);
    }

    private static Vector2 at(int i, Array<Vector2> vertices) {
        return vertices.get(i < 0 ? vertices.size - ((-i) % vertices.size) : i % vertices.size);
    }

    private static Array<Vector2> copy(int i, int j, Array<Vector2> vertices) {
        Array<Vector2> p = new Array<>();
        while (j < i) {
            j += vertices.size;
        }
        while (i <= j) {
            p.add(at(i, vertices));
            i++;
        }
        return p;
    }

    public static Array<Array<Vector2>> convexPartition(Array<Vector2> vertices) {
        Array<Vector2> lowerPoly;
        Array<Vector2> upperPoly;
        double score;
        if (GeometryUtils.areVerticesClockwise(vertices)) {
            vertices.reverse();
        }
        Array<Array<Vector2>> list = new Array<>();
        Vector2 lowerInt = new Vector2();
        Vector2 upperInt = new Vector2();
        int lowerIndex = 0;
        int upperIndex = 0;
        int i = 0;
        while (true) {
            if (i >= vertices.size) {
                if (vertices.size > maxPolygonVertices) {
                    Array<Vector2> lowerPoly2 = copy(0, vertices.size / 2, vertices);
                    Array<Vector2> upperPoly2 = copy(vertices.size / 2, 0, vertices);
                    list.addAll(convexPartition(lowerPoly2));
                    list.addAll(convexPartition(upperPoly2));
                } else {
                    list.add(vertices);
                }
                for (int i2 = 0; i2 < list.size; i2++) {
                    list.set(i2, SimplifyTools.collinearSimplify(list.get(i2), Animation.CurveTimeline.LINEAR));
                }
                for (int i3 = list.size - 1; i3 >= 0; i3--) {
                    if (list.get(i3).size == 0) {
                        list.removeIndex(i3);
                    }
                }
            } else if (reflex(i, vertices)) {
                float upperDist = Float.MAX_VALUE;
                float lowerDist = Float.MAX_VALUE;
                for (int j = 0; j < vertices.size; j++) {
                    if (left(at(i - 1, vertices), at(i, vertices), at(j, vertices)) && rightOn(at(i - 1, vertices), at(i, vertices), at(j - 1, vertices))) {
                        Vector2 p = lineIntersect(at(i - 1, vertices), at(i, vertices), at(j, vertices), at(j - 1, vertices));
                        if (right(at(i + 1, vertices), at(i, vertices), p)) {
                            float d = squareDist(at(i, vertices), p);
                            if (d < lowerDist) {
                                lowerDist = d;
                                lowerInt = p;
                                lowerIndex = j;
                            }
                        }
                    }
                    if (left(at(i + 1, vertices), at(i, vertices), at(j + 1, vertices)) && rightOn(at(i + 1, vertices), at(i, vertices), at(j, vertices))) {
                        Vector2 p2 = lineIntersect(at(i + 1, vertices), at(i, vertices), at(j, vertices), at(j + 1, vertices));
                        if (left(at(i - 1, vertices), at(i, vertices), p2)) {
                            float d2 = squareDist(at(i, vertices), p2);
                            if (d2 < upperDist) {
                                upperDist = d2;
                                upperIndex = j;
                                upperInt = p2;
                            }
                        }
                    }
                }
                if (lowerIndex == (upperIndex + 1) % vertices.size) {
                    Vector2 vector2 = new Vector2((lowerInt.x + upperInt.x) / 2.0f, (lowerInt.y + upperInt.y) / 2.0f);
                    lowerPoly = copy(i, upperIndex, vertices);
                    lowerPoly.add(vector2);
                    upperPoly = copy(lowerIndex, i, vertices);
                    upperPoly.add(vector2);
                } else {
                    double highestScore = 0.0d;
                    double bestIndex = (double) lowerIndex;
                    while (upperIndex < lowerIndex) {
                        upperIndex += vertices.size;
                    }
                    for (int j2 = lowerIndex; j2 <= upperIndex; j2++) {
                        if (canSee(i, j2, vertices)) {
                            double score2 = (double) (1.0f / (squareDist(at(i, vertices), at(j2, vertices)) + 1.0f));
                            if (!reflex(j2, vertices)) {
                                score = score2 + 1.0d;
                            } else if (!rightOn(at(j2 - 1, vertices), at(j2, vertices), at(i, vertices)) || !leftOn(at(j2 + 1, vertices), at(j2, vertices), at(i, vertices))) {
                                score = score2 + 2.0d;
                            } else {
                                score = score2 + 3.0d;
                            }
                            if (score > highestScore) {
                                bestIndex = (double) j2;
                                highestScore = score;
                            }
                        }
                    }
                    lowerPoly = copy(i, (int) bestIndex, vertices);
                    upperPoly = copy((int) bestIndex, i, vertices);
                }
                list.addAll(convexPartition(lowerPoly));
                list.addAll(convexPartition(upperPoly));
            } else {
                i++;
            }
        }
        return list;
    }

    private static boolean canSee(int i, int j, Array<Vector2> vertices) {
        if (reflex(i, vertices)) {
            if (leftOn(at(i, vertices), at(i - 1, vertices), at(j, vertices)) && rightOn(at(i, vertices), at(i + 1, vertices), at(j, vertices))) {
                return false;
            }
        } else if (rightOn(at(i, vertices), at(i + 1, vertices), at(j, vertices)) || leftOn(at(i, vertices), at(i - 1, vertices), at(j, vertices))) {
            return false;
        }
        if (reflex(j, vertices)) {
            if (leftOn(at(j, vertices), at(j - 1, vertices), at(i, vertices)) && rightOn(at(j, vertices), at(j + 1, vertices), at(i, vertices))) {
                return false;
            }
        } else if (rightOn(at(j, vertices), at(j + 1, vertices), at(i, vertices)) || leftOn(at(j, vertices), at(j - 1, vertices), at(i, vertices))) {
            return false;
        }
        for (int k = 0; k < vertices.size; k++) {
            if (!((k + 1) % vertices.size == i || k == i || (k + 1) % vertices.size == j || k == j)) {
                if (lineIntersect(at(i, vertices), at(j, vertices), at(k, vertices), at(k + 1, vertices), true, true, new Vector2())) {
                    return false;
                }
            }
        }
        return true;
    }

    public static Vector2 lineIntersect(Vector2 p1, Vector2 p2, Vector2 q1, Vector2 q2) {
        Vector2 i = new Vector2();
        float a1 = p2.y - p1.y;
        float b1 = p1.x - p2.x;
        float c1 = (p1.x * a1) + (p1.y * b1);
        float a2 = q2.y - q1.y;
        float b2 = q1.x - q2.x;
        float c2 = (q1.x * a2) + (q1.y * b2);
        float det = (a1 * b2) - (a2 * b1);
        if (!floatEquals(det, Animation.CurveTimeline.LINEAR)) {
            i.x = ((b2 * c1) - (b1 * c2)) / det;
            i.y = ((a1 * c2) - (a2 * c1)) / det;
        }
        return i;
    }

    public static boolean floatEquals(float value1, float value2) {
        return Math.abs(value1 - value2) <= 1.1920929E-7f;
    }

    public static boolean lineIntersect(Vector2 point1, Vector2 point2, Vector2 point3, Vector2 point4, boolean firstIsSegment, boolean secondIsSegment, Vector2 point) {
        Vector2 point5 = new Vector2();
        float a = point4.y - point3.y;
        float b = point2.x - point1.x;
        float c = point4.x - point3.x;
        float d = point2.y - point1.y;
        float denom = (a * b) - (c * d);
        if (denom < -1.1920929E-7f || denom > 1.1920929E-7f) {
            float e = point1.y - point3.y;
            float f = point1.x - point3.x;
            float oneOverDenom = 1.0f / denom;
            float ua = ((c * e) - (a * f)) * oneOverDenom;
            if (!firstIsSegment || (ua >= Animation.CurveTimeline.LINEAR && ua <= 1.0f)) {
                float ub = ((b * e) - (d * f)) * oneOverDenom;
                if ((!secondIsSegment || (ub >= Animation.CurveTimeline.LINEAR && ub <= 1.0f)) && !(ua == Animation.CurveTimeline.LINEAR && ub == Animation.CurveTimeline.LINEAR)) {
                    point5.x = point1.x + (ua * b);
                    point5.y = point1.y + (ua * d);
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean reflex(int i, Array<Vector2> vertices) {
        return right(i, vertices);
    }

    private static boolean right(int i, Array<Vector2> vertices) {
        return right(at(i - 1, vertices), at(i, vertices), at(i + 1, vertices));
    }

    private static boolean left(Vector2 a, Vector2 b, Vector2 c) {
        return area(a, b, c) > Animation.CurveTimeline.LINEAR;
    }

    private static boolean leftOn(Vector2 a, Vector2 b, Vector2 c) {
        return area(a, b, c) >= Animation.CurveTimeline.LINEAR;
    }

    private static boolean right(Vector2 a, Vector2 b, Vector2 c) {
        return area(a, b, c) < Animation.CurveTimeline.LINEAR;
    }

    private static boolean rightOn(Vector2 a, Vector2 b, Vector2 c) {
        return area(a, b, c) <= Animation.CurveTimeline.LINEAR;
    }

    public static float area(Vector2 a, Vector2 b, Vector2 c) {
        return (a.x * (b.y - c.y)) + (b.x * (c.y - a.y)) + (c.x * (a.y - b.y));
    }

    private static float squareDist(Vector2 a, Vector2 b) {
        float dx = b.x - a.x;
        float dy = b.y - a.y;
        return (dx * dx) + (dy * dy);
    }

    public static class SimplifyTools {
        private static double distanceTolerance;
        private static boolean[] usePt;

        public static Array<Vector2> collinearSimplify(Array<Vector2> vertices, float collinearityTolerance) {
            if (vertices.size < 3) {
                return vertices;
            }
            Array<Vector2> simplified = new Array<>();
            for (int i = 0; i < vertices.size; i++) {
                int prevId = i - 1;
                if (prevId < 0) {
                    prevId = vertices.size - 1;
                }
                int nextId = i + 1;
                if (nextId >= vertices.size) {
                    nextId = 0;
                }
                Vector2 current = vertices.get(i);
                if (!collinear(vertices.get(prevId), current, vertices.get(nextId), collinearityTolerance)) {
                    simplified.add(current);
                }
            }
            return simplified;
        }

        public static boolean collinear(Vector2 a, Vector2 b, Vector2 c, float tolerance) {
            return floatInRange(BayazitDecomposer.area(a, b, c), -tolerance, tolerance);
        }

        public static boolean floatInRange(float value, float min, float max) {
            return value >= min && value <= max;
        }

        public static Array<Vector2> collinearSimplify(Array<Vector2> vertices) {
            return collinearSimplify(vertices, Animation.CurveTimeline.LINEAR);
        }

        public static Array<Vector2> douglasPeuckerSimplify(Array<Vector2> vertices, float distanceTolerance2) {
            distanceTolerance = (double) distanceTolerance2;
            usePt = new boolean[vertices.size];
            for (int i = 0; i < vertices.size; i++) {
                usePt[i] = true;
            }
            simplifySection(vertices, 0, vertices.size - 1);
            Array<Vector2> result = new Array<>();
            for (int i2 = 0; i2 < vertices.size; i2++) {
                if (usePt[i2]) {
                    result.add(vertices.get(i2));
                }
            }
            return result;
        }

        private static void simplifySection(Array<Vector2> vertices, int i, int j) {
            if (i + 1 != j) {
                Vector2 A = vertices.get(i);
                Vector2 B = vertices.get(j);
                double maxDistance = -1.0d;
                int maxIndex = i;
                for (int k = i + 1; k < j; k++) {
                    double distance = distancePointLine(vertices.get(k), A, B);
                    if (distance > maxDistance) {
                        maxDistance = distance;
                        maxIndex = k;
                    }
                }
                if (maxDistance <= distanceTolerance) {
                    for (int k2 = i + 1; k2 < j; k2++) {
                        usePt[k2] = false;
                    }
                    return;
                }
                simplifySection(vertices, i, maxIndex);
                simplifySection(vertices, maxIndex, j);
            }
        }

        private static double distancePointPoint(Vector2 p, Vector2 p2) {
            double dx = (double) (p.x - p2.x);
            double dy = (double) (p.y - p2.x);
            return Math.sqrt((dx * dx) + (dy * dy));
        }

        private static double distancePointLine(Vector2 p, Vector2 A, Vector2 B) {
            if (A.x == B.x && A.y == B.y) {
                return distancePointPoint(p, A);
            }
            double r = (double) ((((p.x - A.x) * (B.x - A.x)) + ((p.y - A.y) * (B.y - A.y))) / (((B.x - A.x) * (B.x - A.x)) + ((B.y - A.y) * (B.y - A.y))));
            if (r <= 0.0d) {
                return distancePointPoint(p, A);
            }
            if (r >= 1.0d) {
                return distancePointPoint(p, B);
            }
            return Math.abs((double) ((((A.y - p.y) * (B.x - A.x)) - ((A.x - p.x) * (B.y - A.y))) / (((B.x - A.x) * (B.x - A.x)) + ((B.y - A.y) * (B.y - A.y))))) * Math.sqrt((double) (((B.x - A.x) * (B.x - A.x)) + ((B.y - A.y) * (B.y - A.y))));
        }

        public static Array<Vector2> reduceByArea(Array<Vector2> vertices, float areaTolerance) {
            Vector2 v3;
            if (vertices.size <= 3) {
                return vertices;
            }
            if (areaTolerance < Animation.CurveTimeline.LINEAR) {
                throw new IllegalArgumentException("areaTolerance: must be equal to or greater then zero.");
            }
            Array<Vector2> result = new Array<>();
            Vector2 v1 = vertices.get(vertices.size - 2);
            Vector2 v2 = vertices.get(vertices.size - 1);
            float areaTolerance2 = areaTolerance * 2.0f;
            int index = 0;
            while (index < vertices.size) {
                if (index != vertices.size - 1) {
                    v3 = vertices.get(index);
                } else if (result.size == 0) {
                    throw new IllegalArgumentException("areaTolerance: The tolerance is too high!");
                } else {
                    v3 = result.get(0);
                }
                if (Math.abs(cross(v1, v3).floatValue() - (cross(v1, v2).floatValue() + cross(v2, v3).floatValue())) > areaTolerance2) {
                    result.add(v2);
                    v1 = v2;
                }
                index++;
                v2 = v3;
            }
            return result;
        }

        public static Float cross(Vector2 a, Vector2 b) {
            return Float.valueOf((a.x * b.y) - (a.y * b.x));
        }

        public static void mergeParallelEdges(Array<Vector2> vertices, float tolerance) {
            if (vertices.size > 3) {
                boolean[] mergeMe = new boolean[vertices.size];
                int newNVertices = vertices.size;
                int i = 0;
                while (i < vertices.size) {
                    int lower = i == 0 ? vertices.size - 1 : i - 1;
                    int middle = i;
                    int upper = i == vertices.size + -1 ? 0 : i + 1;
                    float dx0 = vertices.get(middle).x - vertices.get(lower).x;
                    float dy0 = vertices.get(middle).y - vertices.get(lower).y;
                    float dx1 = vertices.get(upper).y - vertices.get(middle).x;
                    float dy1 = vertices.get(upper).y - vertices.get(middle).y;
                    float norm0 = (float) Math.sqrt((double) ((dx0 * dx0) + (dy0 * dy0)));
                    float norm1 = (float) Math.sqrt((double) ((dx1 * dx1) + (dy1 * dy1)));
                    if ((norm0 <= Animation.CurveTimeline.LINEAR || norm1 <= Animation.CurveTimeline.LINEAR) && newNVertices > 3) {
                        mergeMe[i] = true;
                        newNVertices--;
                    }
                    float dx02 = dx0 / norm0;
                    float dy02 = dy0 / norm0;
                    float dx12 = dx1 / norm1;
                    float dy12 = dy1 / norm1;
                    float dot = (dx02 * dx12) + (dy02 * dy12);
                    if (Math.abs((dx02 * dy12) - (dx12 * dy02)) >= tolerance || dot <= Animation.CurveTimeline.LINEAR || newNVertices <= 3) {
                        mergeMe[i] = false;
                    } else {
                        mergeMe[i] = true;
                        newNVertices--;
                    }
                    i++;
                }
                if (newNVertices != vertices.size && newNVertices != 0) {
                    int currIndex = 0;
                    Array array = new Array(vertices);
                    vertices.clear();
                    for (int i2 = 0; i2 < array.size; i2++) {
                        if (!(mergeMe[i2] || newNVertices == 0 || currIndex == newNVertices)) {
                            vertices.add(array.get(i2));
                            currIndex++;
                        }
                    }
                }
            }
        }

        public static Array<Vector2> mergeIdenticalPoints(Array<Vector2> vertices) {
            Array<Vector2> results = new Array<>();
            for (int i = 0; i < vertices.size; i++) {
                Vector2 vOriginal = vertices.get(i);
                boolean alreadyExists = false;
                int j = 0;
                while (true) {
                    if (j >= results.size) {
                        break;
                    } else if (vOriginal.equals(results.get(j))) {
                        alreadyExists = true;
                        break;
                    } else {
                        j++;
                    }
                }
                if (!alreadyExists) {
                    results.add(vertices.get(i));
                }
            }
            return results;
        }

        public static Array<Vector2> reduceByDistance(Array<Vector2> vertices, float distance) {
            if (vertices.size < 3) {
                return vertices;
            }
            Array<Vector2> simplified = new Array<>();
            for (int i = 0; i < vertices.size; i++) {
                Vector2 current = vertices.get(i);
                int ii = i + 1;
                if (ii >= vertices.size) {
                    ii = 0;
                }
                Vector2 next = vertices.get(ii);
                if (new Vector2(next.x - current.x, next.y - current.y).len2() > distance) {
                    simplified.add(current);
                }
            }
            return simplified;
        }

        public static Array<Vector2> reduceByNth(Array<Vector2> vertices, int nth) {
            if (vertices.size < 3 || nth == 0) {
                return vertices;
            }
            Array<Vector2> result = new Array<>(vertices.size);
            for (int i = 0; i < vertices.size; i++) {
                if (i % nth != 0) {
                    result.add(vertices.get(i));
                }
            }
            return result;
        }
    }
}
