package net.dermetfan.gdx.math;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.ShortArray;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Comparator;
import java.util.Iterator;
import net.dermetfan.gdx.utils.ArrayUtils;
import net.dermetfan.utils.math.MathUtils;

public class GeometryUtils extends net.dermetfan.utils.math.GeometryUtils {
    static final /* synthetic */ boolean $assertionsDisabled = (!GeometryUtils.class.desiredAssertionStatus());
    private static final Comparator<Vector2> arrangeClockwiseComparator = new Comparator<Vector2>() {
        public int compare(Vector2 a, Vector2 b) {
            if (a.x > b.x) {
                return 1;
            }
            if (a.x < b.x) {
                return -1;
            }
            return 0;
        }
    };
    private static final FloatArray tmpFloatArray = new FloatArray();
    private static Array<Vector2> tmpVector2Array = new Array<>();
    private static final Vector2 vec2_0 = new Vector2();
    private static final Vector2 vec2_1 = new Vector2();

    public static boolean between(Vector2 point, Vector2 a, Vector2 b, boolean inclusive) {
        return between(point.x, point.y, a.x, a.y, b.x, b.y, inclusive);
    }

    public static boolean between(Vector2 point, Vector2 a, Vector2 b) {
        return between(point.x, point.y, a.x, a.y, b.x, b.y);
    }

    public static Vector2 abs(Vector2 vector) {
        vector.x = Math.abs(vector.x);
        vector.y = Math.abs(vector.y);
        return vector;
    }

    public static Vector3 abs(Vector3 vector) {
        vector.x = Math.abs(vector.x);
        vector.y = Math.abs(vector.y);
        vector.z = Math.abs(vector.z);
        return vector;
    }

    public static Array<Vector2> add(Array<Vector2> vertices, float x, float y) {
        Iterator<Vector2> it = vertices.iterator();
        while (it.hasNext()) {
            it.next().add(x, y);
        }
        return vertices;
    }

    public static Array<Vector2> sub(Array<Vector2> vertices, float x, float y) {
        return add(vertices, -x, -y);
    }

    public static FloatArray add(FloatArray vertices, float x, float y) {
        add(vertices.items, 0, vertices.size, x, y);
        return vertices;
    }

    public static FloatArray sub(FloatArray vertices, float x, float y) {
        sub(vertices.items, 0, vertices.size, x, y);
        return vertices;
    }

    public static FloatArray addX(FloatArray vertices, float value) {
        addX(vertices.items, 0, vertices.size, value);
        return vertices;
    }

    public static FloatArray subX(FloatArray vertices, float value) {
        subX(vertices.items, 0, vertices.size, value);
        return vertices;
    }

    public static FloatArray addY(FloatArray vertices, float value) {
        addY(vertices.items, 0, vertices.size, value);
        return vertices;
    }

    public static FloatArray subY(FloatArray vertices, float value) {
        subY(vertices.items, 0, vertices.size, value);
        return vertices;
    }

    public static Vector2 size(Array<Vector2> vertices, Vector2 output) {
        return output.set(width(vertices), height(vertices));
    }

    public static Vector2 size(Array<Vector2> vertices) {
        return size(vertices, vec2_0);
    }

    public static float width(Array<Vector2> vertices) {
        return MathUtils.amplitude2(filterX(vertices));
    }

    public static float height(Array<Vector2> vertices) {
        return MathUtils.amplitude2(filterY(vertices));
    }

    public static float width(FloatArray vertices) {
        return MathUtils.amplitude2(filterX(vertices));
    }

    public static float height(FloatArray vertices) {
        return MathUtils.amplitude2(filterY(vertices));
    }

    public static float depth(FloatArray vertices) {
        return MathUtils.amplitude2(filterZ(vertices));
    }

    public static FloatArray filterX(Array<Vector2> vertices, FloatArray output) {
        if (output == null) {
            output = new FloatArray(vertices.size);
        }
        output.clear();
        output.ensureCapacity(vertices.size);
        for (int i = 0; i < vertices.size; i++) {
            output.add(vertices.get(i).x);
        }
        return output;
    }

    public static FloatArray filterX(Array<Vector2> vertices) {
        return filterX(vertices, tmpFloatArray);
    }

    public static FloatArray filterX(FloatArray vertices, FloatArray output) {
        return ArrayUtils.select(vertices, -1, 2, output);
    }

    public static FloatArray filterX(FloatArray vertices) {
        return filterX(vertices, tmpFloatArray);
    }

    public static FloatArray filterX3D(FloatArray vertices, FloatArray output) {
        return ArrayUtils.select(vertices, -2, 3, output);
    }

    public static FloatArray filterX3D(FloatArray vertices) {
        return filterX3D(vertices, tmpFloatArray);
    }

    public static FloatArray filterY(Array<Vector2> vertices, FloatArray output) {
        if (output == null) {
            output = new FloatArray(vertices.size);
        }
        output.clear();
        output.ensureCapacity(vertices.size);
        for (int i = 0; i < vertices.size; i++) {
            output.add(vertices.get(i).y);
        }
        return output;
    }

    public static FloatArray filterY(Array<Vector2> vertices) {
        return filterY(vertices, tmpFloatArray);
    }

    public static FloatArray filterY(FloatArray vertices, FloatArray output) {
        return ArrayUtils.select(vertices, 2, output);
    }

    public static FloatArray filterY(FloatArray vertices) {
        return filterY(vertices, tmpFloatArray);
    }

    public static FloatArray filterY3D(FloatArray vertices, FloatArray output) {
        return ArrayUtils.select(vertices, -4, 3, output);
    }

    public static FloatArray filterY3D(FloatArray vertices) {
        return filterY3D(vertices, tmpFloatArray);
    }

    public static FloatArray filterZ(FloatArray vertices, FloatArray output) {
        return ArrayUtils.select(vertices, 3, output);
    }

    public static FloatArray filterZ(FloatArray vertices) {
        return filterZ(vertices, tmpFloatArray);
    }

    public static FloatArray filterW(FloatArray vertices, FloatArray output) {
        return ArrayUtils.select(vertices, 4, output);
    }

    public static FloatArray filterW(FloatArray vertices) {
        return filterW(vertices, tmpFloatArray);
    }

    public static float minX(Array<Vector2> vertices) {
        return MathUtils.min(filterX(vertices));
    }

    public static float minY(Array<Vector2> vertices) {
        return MathUtils.min(filterY(vertices));
    }

    public static float maxX(Array<Vector2> vertices) {
        return MathUtils.max(filterX(vertices));
    }

    public static float maxY(Array<Vector2> vertices) {
        return MathUtils.max(filterY(vertices));
    }

    public static float minX(FloatArray vertices) {
        return MathUtils.min(filterX(vertices));
    }

    public static float minY(FloatArray vertices) {
        return MathUtils.min(filterY(vertices));
    }

    public static float maxX(FloatArray vertices) {
        return MathUtils.max(filterX(vertices));
    }

    public static float maxY(FloatArray vertices) {
        return MathUtils.max(filterY(vertices));
    }

    public static Vector2 rotate(Vector2 point, Vector2 origin, float radians) {
        return point.equals(origin) ? point : point.sub(origin).rotateRad(radians).add(origin);
    }

    public static void rotateLine(Vector2 a, Vector2 b, float radians) {
        rotate(a, vec2_0.set(a).add(b).scl(0.5f), radians);
        rotate(b, vec2_0, radians);
    }

    public static FloatArray rotate(float x, float y, float width, float height, float radians, FloatArray output) {
        output.clear();
        output.ensureCapacity(8);
        rotate(x, y, width, height, radians, output.items, 0);
        return output;
    }

    public static FloatArray rotate(float x, float y, float width, float height, float radians) {
        return rotate(x, y, width, height, radians, tmpFloatArray);
    }

    public static FloatArray rotate(Rectangle rectangle, float radians, FloatArray output) {
        return rotate(rectangle.x, rectangle.y, rectangle.width, rectangle.height, radians, output);
    }

    public static FloatArray rotate(Rectangle rectangle, float radians) {
        return rotate(rectangle, radians, tmpFloatArray);
    }

    public static FloatArray toFloatArray(Array<Vector2> vector2s, FloatArray output) {
        if (output == null) {
            output = new FloatArray(vector2s.size * 2);
        }
        output.clear();
        output.ensureCapacity(vector2s.size * 2);
        int vi = -1;
        for (int i = 0; i < vector2s.size * 2; i++) {
            if (i % 2 == 0) {
                vi++;
                output.add(vector2s.get(vi).x);
            } else {
                output.add(vector2s.get(vi).y);
            }
        }
        return output;
    }

    public static FloatArray toFloatArray(Array<Vector2> vector2s) {
        return toFloatArray(vector2s, tmpFloatArray);
    }

    public static Array<Vector2> toVector2Array(FloatArray floats, Array<Vector2> output) {
        if (floats.size % 2 != 0) {
            throw new IllegalArgumentException("the float array's length is not dividable by two, so it won't make up a Vector2 array: " + floats.size);
        }
        if (output == null) {
            output = new Array<>(floats.size / 2);
        }
        output.clear();
        int fi = -1;
        for (int i = 0; i < floats.size / 2; i++) {
            int fi2 = fi + 1;
            float f = floats.get(fi2);
            fi = fi2 + 1;
            output.add(new Vector2(f, floats.get(fi)));
        }
        return output;
    }

    public static Array<Vector2> toVector2Array(FloatArray floats) {
        return toVector2Array(floats, tmpVector2Array);
    }

    public static Polygon[] toPolygonArray(Array<Vector2> vertices, int vertexCount) {
        IntArray vertexCounts = (IntArray) Pools.obtain(IntArray.class);
        vertexCounts.clear();
        vertexCounts.ensureCapacity(vertices.size / vertexCount);
        for (int i = 0; i < vertices.size / vertexCount; i++) {
            vertexCounts.add(vertexCount);
        }
        Polygon[] polygons = toPolygonArray(vertices, vertexCounts);
        vertexCounts.clear();
        Pools.free(vertexCounts);
        return polygons;
    }

    public static Polygon[] toPolygonArray(Array<Vector2> vertices, IntArray vertexCounts) {
        Polygon[] polygons = new Polygon[vertexCounts.size];
        int vertice = -1;
        for (int i = 0; i < polygons.length; i++) {
            tmpVector2Array.clear();
            tmpVector2Array.ensureCapacity(vertexCounts.get(i));
            for (int i2 = 0; i2 < vertexCounts.get(i); i2++) {
                vertice++;
                tmpVector2Array.add(vertices.get(vertice));
            }
            polygons[i] = new Polygon(toFloatArray(tmpVector2Array).toArray());
        }
        return polygons;
    }

    public static boolean areVerticesClockwise(Polygon polygon) {
        return polygon.area() < Animation.CurveTimeline.LINEAR;
    }

    public static boolean areVerticesClockwise(FloatArray vertices) {
        return vertices.size <= 4 || polygonArea(vertices) < Animation.CurveTimeline.LINEAR;
    }

    public static boolean areVerticesClockwise(float[] vertices, int offset, int length) {
        return vertices.length <= 8 || com.badlogic.gdx.math.GeometryUtils.polygonArea(vertices, offset, length) < Animation.CurveTimeline.LINEAR;
    }

    public static boolean areVerticesClockwise(float[] vertices) {
        return areVerticesClockwise(vertices, 0, vertices.length);
    }

    public static boolean areVerticesClockwise(Array<Vector2> vertices) {
        return vertices.size <= 2 || areVerticesClockwise(toFloatArray(vertices));
    }

    public static float polygonArea(FloatArray vertices) {
        return com.badlogic.gdx.math.GeometryUtils.polygonArea(vertices.items, 0, vertices.size);
    }

    public static void arrangeClockwise(Array<Vector2> vertices) {
        int n = vertices.size;
        int i1 = 1;
        int i2 = vertices.size - 1;
        if (tmpVector2Array == null) {
            tmpVector2Array = new Array<>(vertices.size);
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices);
        tmpVector2Array.sort(arrangeClockwiseComparator);
        tmpVector2Array.set(0, vertices.first());
        Vector2 C = vertices.first();
        Vector2 D = vertices.get(n - 1);
        int i = 1;
        while (true) {
            int i22 = i2;
            int i12 = i1;
            if (i < n - 1) {
                if (MathUtils.det(C.x, C.y, D.x, D.y, vertices.get(i).x, vertices.get(i).y) < Animation.CurveTimeline.LINEAR) {
                    i1 = i12 + 1;
                    tmpVector2Array.set(i12, vertices.get(i));
                    i2 = i22;
                } else {
                    i2 = i22 - 1;
                    tmpVector2Array.set(i22, vertices.get(i));
                    i1 = i12;
                }
                i++;
            } else {
                tmpVector2Array.set(i12, vertices.get(n - 1));
                vertices.clear();
                vertices.addAll(tmpVector2Array, 0, n);
                return;
            }
        }
    }

    public static FloatArray invertAxes(FloatArray vertices, boolean x, boolean y) {
        invertAxes(vertices.items, 0, vertices.size, x, y);
        return vertices;
    }

    public static FloatArray toYDown(FloatArray vertices) {
        toYDown(vertices.items, 0, vertices.size);
        return vertices;
    }

    public static FloatArray toYUp(FloatArray vertices) {
        toYUp(vertices.items, 0, vertices.size);
        return vertices;
    }

    public static Rectangle setToAABB(Rectangle aabb, FloatArray vertices) {
        return aabb.set(minX(vertices), minY(vertices), width(vertices), height(vertices));
    }

    public static Rectangle setToAABB(Rectangle aabb, Array<Vector2> vertices) {
        return aabb.set(minX(vertices), minY(vertices), width(vertices), height(vertices));
    }

    public static boolean isConvex(FloatArray vertices) {
        return isConvex(toVector2Array(vertices));
    }

    public static boolean isConvex(Polygon polygon) {
        tmpFloatArray.clear();
        tmpFloatArray.addAll(polygon.getVertices());
        return isConvex(tmpFloatArray);
    }

    public static boolean isConvex(Array<Vector2> vertices) {
        Vector2 v = vec2_1;
        float res = Animation.CurveTimeline.LINEAR;
        for (int i = 0; i < vertices.size; i++) {
            Vector2 p = vertices.get(i);
            vec2_0.set(vertices.get((i + 1) % vertices.size));
            v.x = vec2_0.x - p.x;
            v.y = vec2_0.y - p.y;
            Vector2 u = vertices.get((i + 2) % vertices.size);
            if (i == 0) {
                res = (((u.x * v.y) - (u.y * v.x)) + (v.x * p.y)) - (v.y * p.x);
            } else {
                float newres = (((u.x * v.y) - (u.y * v.x)) + (v.x * p.y)) - (v.y * p.x);
                if ((newres > Animation.CurveTimeline.LINEAR && res < Animation.CurveTimeline.LINEAR) || (newres < Animation.CurveTimeline.LINEAR && res > Animation.CurveTimeline.LINEAR)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static Polygon[] triangulate(Polygon concave) {
        Array<Vector2> polygonVertices = (Array) Pools.obtain(Array.class);
        polygonVertices.clear();
        tmpFloatArray.clear();
        tmpFloatArray.addAll(concave.getTransformedVertices());
        polygonVertices.addAll(toVector2Array(tmpFloatArray));
        ShortArray indices = new EarClippingTriangulator().computeTriangles(tmpFloatArray);
        Array<Vector2> vertices = (Array) Pools.obtain(Array.class);
        vertices.clear();
        vertices.ensureCapacity(indices.size);
        for (int i = 0; i < indices.size; i++) {
            vertices.set(i, polygonVertices.get(indices.get(i)));
        }
        Polygon[] polygons = toPolygonArray(vertices, 3);
        polygonVertices.clear();
        vertices.clear();
        Pools.free(polygonVertices);
        Pools.free(vertices);
        return polygons;
    }

    public static Polygon[] decompose(Polygon concave) {
        tmpFloatArray.clear();
        tmpFloatArray.addAll(concave.getTransformedVertices());
        Array<Array<Vector2>> convexPolys = BayazitDecomposer.convexPartition(new Array(toVector2Array(tmpFloatArray)));
        Polygon[] convexPolygons = new Polygon[convexPolys.size];
        for (int i = 0; i < convexPolygons.length; i++) {
            convexPolygons[i] = new Polygon(toFloatArray(convexPolys.get(i)).toArray());
        }
        return convexPolygons;
    }

    public static Vector2 keepWithin(Vector2 position, float width, float height, float x2, float y2, float width2, float height2) {
        if (width2 < width) {
            position.x = ((width2 / 2.0f) + x2) - (width / 2.0f);
        } else if (position.x < x2) {
            position.x = x2;
        } else if (position.x + width > x2 + width2) {
            position.x = (x2 + width2) - width;
        }
        if (height2 < height) {
            position.y = ((height2 / 2.0f) + y2) - (height / 2.0f);
        } else if (position.y < y2) {
            position.y = y2;
        } else if (position.y + height > y2 + height2) {
            position.y = (y2 + height2) - height;
        }
        return position;
    }

    public static Vector2 keepWithin(float x, float y, float width, float height, float rectX, float rectY, float rectWidth, float rectHeight) {
        return keepWithin(vec2_0.set(x, y), width, height, rectX, rectY, rectWidth, rectHeight);
    }

    public static Rectangle keepWithin(Rectangle rect, Rectangle other) {
        return rect.setPosition(keepWithin(rect.x, rect.y, rect.width, rect.height, other.x, other.y, other.width, other.height));
    }

    public static void keepWithin(OrthographicCamera camera, float x, float y, float width, float height) {
        vec2_0.set(keepWithin(camera.position.x - ((camera.viewportWidth / 2.0f) * camera.zoom), camera.position.y - ((camera.viewportHeight / 2.0f) * camera.zoom), camera.viewportWidth * camera.zoom, camera.viewportHeight * camera.zoom, x, y, width, height));
        camera.position.x = vec2_0.x + ((camera.viewportWidth / 2.0f) * camera.zoom);
        camera.position.y = vec2_0.y + ((camera.viewportHeight / 2.0f) * camera.zoom);
    }

    public static int intersectSegments(Vector2 a, Vector2 b, FloatArray polygon, Vector2 intersection1, Vector2 intersection2) {
        FloatArray intersections = (FloatArray) Pools.obtain(FloatArray.class);
        intersectSegments(a.x, a.y, b.x, b.y, polygon, true, intersections);
        int size = intersections.size;
        if (size >= 2) {
            intersection1.set(intersections.get(0), intersections.get(1));
            if (size == 4) {
                intersection2.set(intersections.get(2), intersections.get(3));
            } else if (size > 4 && !$assertionsDisabled) {
                throw new AssertionError("more intersection points with a convex polygon found than possible: " + size);
            }
        }
        Pools.free(intersections);
        return size / 2;
    }

    public static void intersectSegments(Vector2 a, Vector2 b, FloatArray segments, boolean polygon, Array<Vector2> intersections) {
        FloatArray fa = (FloatArray) Pools.obtain(FloatArray.class);
        intersectSegments(a.x, a.y, b.x, b.y, segments, polygon, fa);
        if (fa.size < 1) {
            intersections.clear();
            Pools.free(fa);
            return;
        }
        intersections.ensureCapacity((fa.size / 2) - intersections.size);
        for (int i = 1; i < fa.size; i += 2) {
            if (intersections.size > i / 2) {
                intersections.get(i / 2).set(fa.get(i - 1), fa.get(i));
            } else {
                intersections.add(new Vector2(fa.get(i - 1), fa.get(i)));
            }
        }
        Pools.free(fa);
    }

    public static void intersectSegments(float x1, float y1, float x2, float y2, FloatArray segments, boolean polygon, FloatArray intersections) {
        if (polygon && segments.size < 6) {
            throw new IllegalArgumentException("a polygon consists of at least 3 points: " + segments.size);
        } else if (segments.size < 4) {
            throw new IllegalArgumentException("segments does not contain enough vertices to represent at least one segment: " + segments.size);
        } else if (segments.size % 2 != 0) {
            throw new IllegalArgumentException("malformed segments; the number of vertices is not dividable by 2: " + segments.size);
        } else {
            intersections.clear();
            vec2_0.setZero();
            int n = segments.size - (polygon ? 0 : 2);
            for (int i = 0; i < n; i += 2) {
                if (Intersector.intersectSegments(x1, y1, x2, y2, segments.get(i), segments.get(i + 1), ArrayUtils.wrapIndex(i + 2, segments), ArrayUtils.wrapIndex(i + 3, segments), vec2_0)) {
                    intersections.add(vec2_0.x);
                    intersections.add(vec2_0.y);
                }
            }
            Pools.free(vec2_0);
        }
    }

    public static <T extends Shape2D> T reset(T shape) {
        if (shape instanceof Polygon) {
            return reset((Polygon) ((Polygon) shape));
        }
        if (shape instanceof Polyline) {
            return reset((Polyline) ((Polyline) shape));
        }
        if (shape instanceof Rectangle) {
            return reset((Rectangle) ((Rectangle) shape));
        }
        if (shape instanceof Circle) {
            return reset((Circle) ((Circle) shape));
        }
        if (shape instanceof Ellipse) {
            return reset((Ellipse) ((Ellipse) shape));
        }
        return shape;
    }

    public static Polygon reset(Polygon polygon) {
        polygon.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        polygon.setRotation(Animation.CurveTimeline.LINEAR);
        polygon.setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        polygon.setScale(1.0f, 1.0f);
        float[] vertices = polygon.getVertices();
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = 0.0f;
        }
        polygon.setVertices(vertices);
        return polygon;
    }

    public static Polyline reset(Polyline polyline) {
        polyline.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        polyline.setRotation(Animation.CurveTimeline.LINEAR);
        polyline.setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        polyline.setScale(1.0f, 1.0f);
        float[] vertices = polyline.getVertices();
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = 0.0f;
        }
        polyline.dirty();
        return polyline;
    }

    public static Rectangle reset(Rectangle rectangle) {
        return rectangle.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public static Circle reset(Circle circle) {
        circle.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        return circle;
    }

    public static Ellipse reset(Ellipse ellipse) {
        ellipse.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        return ellipse;
    }
}
