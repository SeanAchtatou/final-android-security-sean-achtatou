package net.dermetfan.gdx.math;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;
import net.dermetfan.utils.StringUtils;

public class InterpolationUtils {
    public static Interpolation get(String name) {
        if (name == null) {
            return null;
        }
        try {
            Object obj = ClassReflection.getField(Interpolation.class, StringUtils.toJavaIdentifier(name)).get(null);
            if (obj instanceof Interpolation) {
                return (Interpolation) obj;
            }
            return null;
        } catch (ReflectionException e) {
            throw new GdxRuntimeException("failed to get Interpolation for name \"" + name + "\"", e);
        }
    }
}
