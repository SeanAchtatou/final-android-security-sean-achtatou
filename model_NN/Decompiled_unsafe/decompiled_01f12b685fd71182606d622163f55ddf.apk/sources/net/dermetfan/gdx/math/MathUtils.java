package net.dermetfan.gdx.math;

import com.badlogic.gdx.utils.FloatArray;

public class MathUtils extends net.dermetfan.utils.math.MathUtils {
    public static FloatArray clamp(FloatArray values, float min, float max) {
        clamp(values.items, min, max, 0, values.size);
        return values;
    }

    public static FloatArray abs(FloatArray values) {
        abs(values.items, 0, values.size);
        return values;
    }

    public static FloatArray add(FloatArray values, float value) {
        add(values.items, value, 0, values.size);
        return values;
    }

    public static FloatArray sub(FloatArray values, float value) {
        sub(values.items, value, 0, values.size);
        return values;
    }

    public static FloatArray mul(FloatArray values, float factor) {
        mul(values.items, factor, 0, values.size);
        return values;
    }

    public static FloatArray div(FloatArray values, float divisor) {
        div(values.items, divisor, 0, values.size);
        return values;
    }

    public static float sum(FloatArray values) {
        return sum(values.items, 0, values.size);
    }

    public static float amplitude2(FloatArray f) {
        return amplitude2(f.items, 0, f.size);
    }

    public static float max(FloatArray floats) {
        return max(floats.items, 0, floats.size);
    }

    public static float min(FloatArray floats) {
        return min(floats.items, 0, floats.size);
    }

    public static float nearest(float value, FloatArray values, float range) {
        return nearest(value, values.items, range, 0, values.size);
    }

    public static float nearest(float value, FloatArray values) {
        return nearest(value, values.items, 0, values.size);
    }

    public static FloatArray scale(FloatArray values, float min, float max) {
        scale(values.items, min, max, 0, values.size);
        return values;
    }
}
