package net.dermetfan.gdx.maps.tiled;

import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.XmlWriter;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Iterator;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;
import net.dermetfan.gdx.maps.MapUtils;
import net.dermetfan.gdx.math.GeometryUtils;

public class TmxMapWriter extends XmlWriter {
    private int layerHeight;

    public enum Format {
        XML,
        CSV,
        Base64,
        Base64Zlib,
        Base64Gzip
    }

    public TmxMapWriter(Writer writer) {
        super(writer);
    }

    public TmxMapWriter tmx(Map map, Format format) throws IOException {
        append((CharSequence) "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        append((CharSequence) "<!DOCTYPE map SYSTEM \"http://mapeditor.org/dtd/1.0/map.dtd\">\n");
        MapProperties props = map.getProperties();
        int height = ((Integer) MapUtils.getProperty(props, "height", 0)).intValue();
        int tileHeight = ((Integer) MapUtils.getProperty(props, "tileheight", 0)).intValue();
        int oldLayerHeight = this.layerHeight;
        this.layerHeight = height * tileHeight;
        element("map");
        attribute("version", "1.0");
        attribute("orientation", MapUtils.getProperty(props, "orientation", "orthogonal"));
        attribute("width", MapUtils.getProperty(props, "width", 0));
        attribute("height", Integer.valueOf(height));
        attribute("tilewidth", MapUtils.getProperty(props, "tilewidth", 0));
        attribute("tileheight", Integer.valueOf(tileHeight));
        Array<String> excludedKeys = (Array) Pools.obtain(Array.class);
        excludedKeys.clear();
        excludedKeys.add("version");
        excludedKeys.add("orientation");
        excludedKeys.add("width");
        excludedKeys.add("height");
        excludedKeys.add("tilewidth");
        excludedKeys.add("tileheight");
        tmx(props, excludedKeys);
        excludedKeys.clear();
        Pools.free(excludedKeys);
        if (map instanceof TiledMap) {
            tmx(((TiledMap) map).getTileSets());
        }
        tmx(map.getLayers(), format);
        pop();
        this.layerHeight = oldLayerHeight;
        return this;
    }

    public TmxMapWriter tmx(MapProperties properties) throws IOException {
        return tmx(properties, (Array<String>) null);
    }

    public TmxMapWriter tmx(MapProperties properties, Array<String> exclude) throws IOException {
        Iterator<String> keys = properties.getKeys();
        if (keys.hasNext()) {
            boolean elementEmitted = false;
            while (keys.hasNext()) {
                String key = keys.next();
                if (exclude == null || !exclude.contains(key, false)) {
                    if (!elementEmitted) {
                        element("properties");
                        elementEmitted = true;
                    }
                    element("property").attribute("name", key).attribute("value", properties.get(key)).pop();
                }
            }
            if (elementEmitted) {
                pop();
            }
        }
        return this;
    }

    public TmxMapWriter tmx(TiledMapTileSets sets) throws IOException {
        Iterator<TiledMapTileSet> it = sets.iterator();
        while (it.hasNext()) {
            tmx(it.next());
        }
        return this;
    }

    public TmxMapWriter tmx(TiledMapTileSet set) throws IOException {
        MapProperties props = set.getProperties();
        element("tileset");
        attribute("firstgid", MapUtils.getProperty(props, "firstgid", 1));
        attribute("name", set.getName());
        attribute("tilewidth", MapUtils.getProperty(props, "tilewidth", 0));
        attribute("tileheight", MapUtils.getProperty(props, "tileheight", 0));
        float spacing = ((Float) MapUtils.getProperty(props, "spacing", Float.valueOf(Float.NaN))).floatValue();
        float margin = ((Float) MapUtils.getProperty(props, "margin", Float.valueOf(Float.NaN))).floatValue();
        if (!Float.isNaN(spacing)) {
            attribute("spacing", Integer.valueOf(MathUtils.round(spacing)));
        }
        if (!Float.isNaN(margin)) {
            attribute("margin", Integer.valueOf(MathUtils.round(margin)));
        }
        Iterator<TiledMapTile> iter = set.iterator();
        if (iter.hasNext()) {
            TiledMapTile tile = iter.next();
            element("tileoffset");
            attribute("x", Integer.valueOf(MathUtils.round(tile.getOffsetX())));
            attribute("y", Integer.valueOf(MathUtils.round(-tile.getOffsetY())));
            pop();
        }
        element("image");
        attribute("source", MapUtils.getProperty(props, "imagesource", ""));
        attribute("imagewidth", MapUtils.getProperty(props, "imagewidth", 0));
        attribute("imageheight", MapUtils.getProperty(props, "imageheight", 0));
        pop();
        Iterator<TiledMapTile> iter2 = set.iterator();
        if (iter2.hasNext()) {
            Array<String> asAttributes = (Array) Pools.obtain(Array.class);
            asAttributes.clear();
            boolean elementEmitted = false;
            TiledMapTile tile2 = iter2.next();
            while (true) {
                TiledMapTile tile3 = tile2;
                if (!iter2.hasNext()) {
                    break;
                }
                MapProperties tileProps = tile3.getProperties();
                Iterator it = asAttributes.iterator();
                while (it.hasNext()) {
                    String attribute = (String) it.next();
                    if (tileProps.containsKey(attribute)) {
                        if (!elementEmitted) {
                            element("tile");
                            elementEmitted = true;
                        }
                        attribute(attribute, tileProps.get(attribute));
                    }
                }
                tmx(tileProps, asAttributes);
                tile2 = iter2.next();
            }
            asAttributes.clear();
            Pools.free(asAttributes);
            if (elementEmitted) {
                pop();
            }
        }
        pop();
        return this;
    }

    public TmxMapWriter tmx(MapLayers layers, Format format) throws IOException {
        Iterator<MapLayer> it = layers.iterator();
        while (it.hasNext()) {
            MapLayer layer = it.next();
            if (layer instanceof TiledMapTileLayer) {
                tmx((TiledMapTileLayer) layer, format);
            } else {
                tmx(layer);
            }
        }
        return this;
    }

    public TmxMapWriter tmx(MapLayer layer) throws IOException {
        element("objectgroup");
        attribute("name", layer.getName());
        tmx(layer.getProperties());
        tmx(layer.getObjects());
        pop();
        return this;
    }

    public TmxMapWriter tmx(TiledMapTileLayer layer, Format format) throws IOException {
        TiledMapTile tile;
        element("layer");
        attribute("name", layer.getName());
        attribute("width", Integer.valueOf(layer.getWidth()));
        attribute("height", Integer.valueOf(layer.getHeight()));
        attribute("visible", Integer.valueOf(layer.isVisible() ? 1 : 0));
        attribute("opacity", Float.valueOf(layer.getOpacity()));
        tmx(layer.getProperties());
        element("data");
        if (format == Format.XML) {
            attribute("encoding", "xml");
            for (int y = layer.getHeight() - 1; y > -1; y--) {
                for (int x = 0; x < layer.getWidth(); x++) {
                    TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                    if (!(cell == null || (tile = cell.getTile()) == null)) {
                        element("tile");
                        attribute("gid", Integer.valueOf(tile.getId()));
                        pop();
                    }
                }
            }
        } else if (format == Format.CSV) {
            attribute("encoding", "csv");
            StringBuilder csv = new StringBuilder();
            for (int y2 = layer.getHeight() - 1; y2 > -1; y2--) {
                for (int x2 = 0; x2 < layer.getWidth(); x2++) {
                    TiledMapTileLayer.Cell cell2 = layer.getCell(x2, y2);
                    TiledMapTile tile2 = cell2 != null ? cell2.getTile() : null;
                    csv.append(tile2 != null ? tile2.getId() : 0);
                    if (x2 + 1 < layer.getWidth() || y2 - 1 > -1) {
                        csv.append(',');
                    }
                }
                csv.append(10);
            }
            append(10).append((CharSequence) csv);
        } else if (format == Format.Base64 || format == Format.Base64Zlib || format == Format.Base64Gzip) {
            attribute("encoding", "base64");
            if (format == Format.Base64Zlib) {
                attribute("compression", "zlib");
            } else if (format == Format.Base64Gzip) {
                attribute("compression", "gzip");
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            OutputStream out = format == Format.Base64Zlib ? new DeflaterOutputStream(baos) : format == Format.Base64Gzip ? new GZIPOutputStream(baos) : baos;
            for (int y3 = layer.getHeight() - 1; y3 > -1; y3--) {
                for (int x3 = 0; x3 < layer.getWidth(); x3++) {
                    TiledMapTileLayer.Cell cell3 = layer.getCell(x3, y3);
                    TiledMapTile tile3 = cell3 != null ? cell3.getTile() : null;
                    int gid = tile3 != null ? tile3.getId() : 0;
                    out.write(gid & 255);
                    out.write((gid >> 8) & 255);
                    out.write((gid >> 16) & 255);
                    out.write((gid >> 24) & 255);
                }
            }
            if (out instanceof DeflaterOutputStream) {
                ((DeflaterOutputStream) out).finish();
            }
            out.close();
            baos.close();
            append(10).append((CharSequence) String.valueOf(Base64Coder.encode(baos.toByteArray()))).append(10);
        }
        pop();
        pop();
        return this;
    }

    public TmxMapWriter tmx(MapObjects objects) throws IOException {
        Iterator<MapObject> it = objects.iterator();
        while (it.hasNext()) {
            tmx(it.next());
        }
        return this;
    }

    public TmxMapWriter tmx(MapObject object) throws IOException {
        MapProperties props = object.getProperties();
        element("object");
        attribute("name", object.getName());
        if (props.containsKey("type")) {
            attribute("type", MapUtils.getProperty(props, "type", ""));
        }
        if (props.containsKey("gid")) {
            attribute("gid", MapUtils.getProperty(props, "gid", 0));
        }
        int objectX = ((Integer) MapUtils.getProperty(props, "x", 0)).intValue();
        int objectY = ((Integer) MapUtils.getProperty(props, "y", 0)).intValue();
        if (object instanceof RectangleMapObject) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            int height = MathUtils.round(rect.height);
            attribute("x", Integer.valueOf(objectX)).attribute("y", Integer.valueOf(toYDown(objectY + height)));
            attribute("width", Integer.valueOf(MathUtils.round(rect.width))).attribute("height", Integer.valueOf(height));
        } else if (object instanceof EllipseMapObject) {
            Ellipse ellipse = ((EllipseMapObject) object).getEllipse();
            int height2 = MathUtils.round(ellipse.height);
            attribute("x", Integer.valueOf(objectX)).attribute("y", Integer.valueOf(toYDown(objectY + height2)));
            attribute("width", Integer.valueOf(MathUtils.round(ellipse.width))).attribute("height", Integer.valueOf(height2));
            element("ellipse").pop();
        } else if (object instanceof CircleMapObject) {
            Circle circle = ((CircleMapObject) object).getCircle();
            attribute("x", Integer.valueOf(objectX)).attribute("y", Integer.valueOf(objectY));
            attribute("width", Integer.valueOf(MathUtils.round(circle.radius * 2.0f))).attribute("height", Integer.valueOf(MathUtils.round(circle.radius * 2.0f)));
            element("ellipse").pop();
        } else if (object instanceof PolygonMapObject) {
            attribute("x", Integer.valueOf(objectX)).attribute("y", Integer.valueOf(toYDown(objectY)));
            Polygon polygon = ((PolygonMapObject) object).getPolygon();
            element("polygon");
            FloatArray tmp = (FloatArray) Pools.obtain(FloatArray.class);
            tmp.clear();
            tmp.addAll(polygon.getVertices());
            attribute("points", points(GeometryUtils.toYDown(tmp)));
            tmp.clear();
            Pools.free(tmp);
            pop();
        } else if (object instanceof PolylineMapObject) {
            attribute("x", Integer.valueOf(objectX)).attribute("y", Integer.valueOf(toYDown(objectY)));
            Polyline polyline = ((PolylineMapObject) object).getPolyline();
            element("polyline");
            FloatArray tmp2 = (FloatArray) Pools.obtain(FloatArray.class);
            tmp2.clear();
            tmp2.addAll(polyline.getVertices());
            attribute("points", points(GeometryUtils.toYDown(tmp2)));
            tmp2.clear();
            Pools.free(tmp2);
            pop();
        }
        if (props.containsKey("rotation")) {
            attribute("rotation", MapUtils.getProperty(props, "rotation", Float.valueOf((float) Animation.CurveTimeline.LINEAR)));
        }
        if (props.containsKey("visible")) {
            attribute("visible", Integer.valueOf(object.isVisible() ? 1 : 0));
        }
        if (object.getOpacity() != 1.0f) {
            attribute("opacity", Float.valueOf(object.getOpacity()));
        }
        Array<String> excludedKeys = (Array) Pools.obtain(Array.class);
        excludedKeys.clear();
        excludedKeys.add("type");
        excludedKeys.add("gid");
        excludedKeys.add("x");
        excludedKeys.add("y");
        excludedKeys.add("width");
        excludedKeys.add("height");
        excludedKeys.add("rotation");
        excludedKeys.add("visible");
        excludedKeys.add("opacity");
        tmx(props, excludedKeys);
        excludedKeys.clear();
        Pools.free(excludedKeys);
        pop();
        return this;
    }

    private String points(FloatArray vertices) {
        StringBuilder points = new StringBuilder();
        for (int i = 0; i < vertices.size; i++) {
            points.append(MathUtils.round(vertices.get(i))).append(i % 2 != 0 ? i + 1 < vertices.size ? " " : "" : ",");
        }
        return points.toString();
    }

    public int toYDown(int y) {
        return MathUtils.round(toYDown((float) y));
    }

    public float toYDown(float y) {
        return net.dermetfan.utils.math.GeometryUtils.invertAxis(y, (float) this.layerHeight);
    }

    public int getLayerHeight() {
        return this.layerHeight;
    }

    public void setLayerHeight(int layerHeight2) {
        this.layerHeight = layerHeight2;
    }
}
