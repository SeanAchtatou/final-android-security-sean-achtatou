package net.dermetfan.gdx.maps;

import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class MapUtils {
    private static final Vector2 vec2 = new Vector2();

    public static <T> T findProperty(String key, T defaultValue, MapProperties... properties) {
        T value = defaultValue;
        for (MapProperties property : properties) {
            value = getProperty(property, key, value);
        }
        return value;
    }

    public static <T> T getProperty(MapProperties properties, String key, T defaultValue) {
        T value;
        if (properties == null || key == null || (value = properties.get(key)) == null) {
            return defaultValue;
        }
        if ((value instanceof String) && ((String) value).length() == 0) {
            return defaultValue;
        }
        if (defaultValue != null) {
            if (defaultValue.getClass() == Boolean.class && !(value instanceof Boolean)) {
                return Boolean.valueOf(value.toString());
            }
            if (defaultValue.getClass() == Integer.class && !(value instanceof Integer)) {
                return Integer.valueOf(Float.valueOf(value.toString()).intValue());
            }
            if (defaultValue.getClass() == Float.class && !(value instanceof Float)) {
                return Float.valueOf(value.toString());
            }
            if (defaultValue.getClass() == Double.class && !(value instanceof Double)) {
                return Double.valueOf(value.toString());
            }
            if (defaultValue.getClass() == Long.class && !(value instanceof Long)) {
                return Long.valueOf(value.toString());
            }
            if (defaultValue.getClass() == Short.class && !(value instanceof Short)) {
                return Short.valueOf(value.toString());
            }
            if (defaultValue.getClass() == Byte.class && !(value instanceof Byte)) {
                return Byte.valueOf(value.toString());
            }
        }
        return value;
    }

    public static String readableHierarchy(Map map) {
        return readableHierarchy(map, 0);
    }

    public static String readableHierarchy(Map map, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            hierarchy.append(9);
        }
        hierarchy.append(ClassReflection.getSimpleName(map.getClass())).append(10);
        hierarchy.append(readableHierarchy(map.getProperties(), indent + 1));
        if (map instanceof TiledMap) {
            hierarchy.append(readableHierarchy(((TiledMap) map).getTileSets(), indent + 1));
        }
        hierarchy.append(readableHierarchy(map.getLayers(), indent + 1));
        return hierarchy.toString();
    }

    public static String readableHierarchy(TiledMapTileSets sets, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        Iterator<TiledMapTileSet> it = sets.iterator();
        while (it.hasNext()) {
            hierarchy.append(readableHierarchy(it.next(), indent));
        }
        return hierarchy.toString();
    }

    public static String readableHierarchy(TiledMapTileSet set, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            hierarchy.append(9);
        }
        hierarchy.append(ClassReflection.getSimpleName(set.getClass())).append(' ').append(set.getName()).append(" (").append(set.size()).append(" tiles)\n");
        hierarchy.append(readableHierarchy(set.getProperties(), indent + 1));
        Iterator<TiledMapTile> it = set.iterator();
        while (it.hasNext()) {
            hierarchy.append(readableHierarchy(it.next(), indent + 1));
        }
        return hierarchy.toString();
    }

    public static String readableHierarchy(TiledMapTile tile, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            hierarchy.append(9);
        }
        hierarchy.append(ClassReflection.getSimpleName(tile.getClass())).append(" (ID: ").append(tile.getId()).append(", offset: ").append(tile.getOffsetX()).append('x').append(tile.getOffsetY()).append(", BlendMode: ").append(tile.getBlendMode()).append(")\n");
        hierarchy.append(readableHierarchy(tile.getProperties(), indent + 1));
        return hierarchy.toString();
    }

    public static String readableHierarchy(MapLayers layers, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        Iterator<MapLayer> it = layers.iterator();
        while (it.hasNext()) {
            hierarchy.append(readableHierarchy(it.next(), indent));
        }
        return hierarchy.toString();
    }

    public static String readableHierarchy(MapLayer layer, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            hierarchy.append(9);
        }
        hierarchy.append(ClassReflection.getSimpleName(layer.getClass()));
        if (layer instanceof TiledMapTileLayer) {
            TiledMapTileLayer tileLayer = (TiledMapTileLayer) layer;
            hierarchy.append(" (size: ").append(tileLayer.getWidth()).append('x').append(tileLayer.getHeight()).append(", tile size: ").append(tileLayer.getTileWidth()).append('x').append(tileLayer.getTileHeight()).append(')');
        } else {
            hierarchy.append(' ').append(layer.getName());
        }
        hierarchy.append(10);
        hierarchy.append(readableHierarchy(layer.getProperties(), indent + 1));
        hierarchy.append(readableHierarchy(layer.getObjects(), indent + 1));
        return hierarchy.toString();
    }

    public static String readableHierarchy(MapObjects objects, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        Iterator<MapObject> it = objects.iterator();
        while (it.hasNext()) {
            hierarchy.append(readableHierarchy(it.next(), indent));
        }
        return hierarchy.toString();
    }

    public static String readableHierarchy(MapObject object, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            hierarchy.append(9);
        }
        hierarchy.append(ClassReflection.getSimpleName(object.getClass())).append(' ').append(object.getName()).append(10);
        hierarchy.append(readableHierarchy(object.getProperties(), indent + 1));
        return hierarchy.toString();
    }

    public static String readableHierarchy(MapProperties properties, int indent) {
        StringBuilder hierarchy = new StringBuilder();
        Iterator<String> keys = properties.getKeys();
        while (keys.hasNext()) {
            String key = keys.next();
            for (int i = 0; i < indent; i++) {
                hierarchy.append(9);
            }
            hierarchy.append(key).append(": ").append(properties.get(key).toString()).append(10);
        }
        return hierarchy.toString();
    }

    public static TiledMapTile[] toTiledMapTileArray(TiledMapTileSet tiles) {
        TiledMapTile[] tileArray = new TiledMapTile[tiles.size()];
        Iterator<TiledMapTile> tileIterator = tiles.iterator();
        int i = 0;
        while (tileIterator.hasNext()) {
            tileArray[i] = tileIterator.next();
            i++;
        }
        return tileArray;
    }

    public static Vector2 toIsometricGridPoint(Vector2 point, float cellWidth, float cellHeight) {
        point.x /= cellWidth;
        point.y = ((point.y - (cellHeight / 2.0f)) / cellHeight) + point.x;
        point.x -= point.y - point.x;
        return point;
    }

    public static Vector2 toIsometricGridPoint(float x, float y, float cellWidth, float cellHeight) {
        return toIsometricGridPoint(vec2.set(x, y), cellWidth, cellHeight);
    }

    public static Vector3 toIsometricGridPoint(Vector3 point, float cellWidth, float cellHeight) {
        Vector2 vec22 = toIsometricGridPoint(point.x, point.y, cellWidth, cellHeight);
        point.x = vec22.x;
        point.y = vec22.y;
        return point;
    }

    public static Vector2 size(TiledMap map, Vector2 output) {
        Array<TiledMapTileLayer> layers = map.getLayers().getByType(TiledMapTileLayer.class);
        float maxWidth = Animation.CurveTimeline.LINEAR;
        float maxTileWidth = Animation.CurveTimeline.LINEAR;
        float maxHeight = Animation.CurveTimeline.LINEAR;
        float maxTileHeight = Animation.CurveTimeline.LINEAR;
        Iterator it = layers.iterator();
        while (it.hasNext()) {
            TiledMapTileLayer layer = (TiledMapTileLayer) it.next();
            int layerWidth = layer.getWidth();
            int layerHeight = layer.getHeight();
            float layerTileWidth = layer.getTileWidth();
            float layerTileHeight = layer.getTileHeight();
            if (((float) layerWidth) > maxWidth) {
                maxWidth = (float) layerWidth;
            }
            if (layerTileWidth > maxTileWidth) {
                maxTileWidth = layerTileWidth;
            }
            if (((float) layerHeight) > maxHeight) {
                maxHeight = (float) layerHeight;
            }
            if (layerTileHeight > maxTileHeight) {
                maxTileHeight = layerTileHeight;
            }
        }
        return output.set(maxWidth * maxTileWidth, maxHeight * maxTileHeight);
    }
}
