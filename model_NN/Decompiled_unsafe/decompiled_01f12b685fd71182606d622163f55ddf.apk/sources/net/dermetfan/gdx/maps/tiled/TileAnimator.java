package net.dermetfan.gdx.maps.tiled;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import java.util.Comparator;
import java.util.Iterator;
import net.dermetfan.gdx.maps.MapUtils;

public abstract class TileAnimator {
    public static void animateLayer(TiledMapTile[] tiles, TiledMapTileLayer layer, String animationKey, String orderedKey, String intervalKey, float defaultInterval) {
        ObjectMap<String, Array<StaticTiledMapTile>> animations = filterFrames(tiles, animationKey);
        sortFrames(animations, orderedKey);
        animateLayer(animations, layer, animationKey, intervalKey, defaultInterval);
    }

    public static void animateLayer(TiledMapTileSet tiles, TiledMapTileLayer target, String animationKey, String orderedKey, String intervalKey, float defaultInterval) {
        animateLayer(MapUtils.toTiledMapTileArray(tiles), target, animationKey, orderedKey, intervalKey, defaultInterval);
    }

    public static void animateLayer(ObjectMap<String, Array<StaticTiledMapTile>> animations, TiledMapTileLayer layer, String animationKey, String intervalKey, float defaultInterval) {
        TiledMapTile tile;
        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                if (!(cell == null || (tile = cell.getTile()) == null)) {
                    MapProperties tileProperties = tile.getProperties();
                    if (tileProperties.containsKey(animationKey)) {
                        AnimatedTiledMapTile animatedTile = new AnimatedTiledMapTile(((Float) MapUtils.getProperty(tileProperties, intervalKey, Float.valueOf(defaultInterval))).floatValue(), animations.get(tileProperties.get(animationKey, String.class)));
                        animatedTile.getProperties().putAll(tile.getProperties());
                        cell.setTile(animatedTile);
                    }
                }
            }
        }
    }

    public static ObjectMap<String, Array<StaticTiledMapTile>> filterFrames(TiledMapTile[] tiles, String animationKey) {
        ObjectMap<String, Array<StaticTiledMapTile>> animations = new ObjectMap<>();
        for (TiledMapTile tile : tiles) {
            if (tile instanceof StaticTiledMapTile) {
                MapProperties tileProperties = tile.getProperties();
                if (tileProperties.containsKey(animationKey)) {
                    String animationName = (String) tileProperties.get(animationKey, String.class);
                    if (!animations.containsKey(animationName)) {
                        animations.put(animationName, new Array(3));
                    }
                    animations.get(animationName).add((StaticTiledMapTile) tile);
                }
            }
        }
        return animations;
    }

    public static ObjectMap<String, Array<StaticTiledMapTile>> sortFrames(ObjectMap<String, Array<StaticTiledMapTile>> animations, String orderedKey) {
        ObjectMap.Entries<String, Array<StaticTiledMapTile>> entries = animations.entries();
        while (entries.hasNext) {
            ObjectMap.Entry<String, Array<StaticTiledMapTile>> entry = entries.next();
            Iterator it = ((Array) entry.value).iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((StaticTiledMapTile) it.next()).getProperties().containsKey(orderedKey)) {
                        sortFrames((Array) entry.value, orderedKey);
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return animations;
    }

    public static void sortFrames(Array<StaticTiledMapTile> frames, final String orderedKey) {
        frames.sort(new Comparator<StaticTiledMapTile>() {
            public int compare(StaticTiledMapTile tile1, StaticTiledMapTile tile2) {
                int tile1Frame = ((Integer) MapUtils.getProperty(tile1.getProperties(), orderedKey, -1)).intValue();
                int tile2Frame = ((Integer) MapUtils.getProperty(tile2.getProperties(), orderedKey, -1)).intValue();
                if (tile1Frame < tile2Frame) {
                    return -1;
                }
                return tile1Frame > tile2Frame ? 1 : 0;
            }
        });
    }
}
