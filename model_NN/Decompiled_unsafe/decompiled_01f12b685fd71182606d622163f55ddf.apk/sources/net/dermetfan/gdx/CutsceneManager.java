package net.dermetfan.gdx;

import com.badlogic.gdx.utils.Array;
import java.util.Iterator;

public class CutsceneManager {
    private final Array<Cutscene> cutscenes = new Array<>();

    public interface Cutscene {
        void end();

        void init();

        boolean update(float f);
    }

    public void start(Cutscene cutscene) {
        this.cutscenes.add(cutscene);
        cutscene.init();
    }

    public void update(float delta) {
        Iterator<Cutscene> it = this.cutscenes.iterator();
        while (it.hasNext()) {
            Cutscene cutscene = it.next();
            if (cutscene.update(delta)) {
                end(cutscene);
            }
        }
    }

    public void end(Cutscene cutscene) {
        this.cutscenes.removeValue(cutscene, true);
        cutscene.end();
    }

    public Array<Cutscene> getCutscenes() {
        return this.cutscenes;
    }
}
