package net.dermetfan.gdx;

import net.dermetfan.utils.Appender;

public class Typewriter {
    private Appender appender;
    private boolean cursorAfterTyping;
    private boolean cursorWhileTyping;
    private CharSequenceInterpolator interpolator;

    public Typewriter() {
        this.cursorWhileTyping = false;
        this.cursorAfterTyping = true;
        this.interpolator = new CharSequenceInterpolator(40.0f);
        this.appender = new Appender(new CharSequence[]{"|", ""}, 0.5f);
    }

    public Typewriter(CharSequence cursor) {
        this.cursorWhileTyping = false;
        this.cursorAfterTyping = true;
        this.interpolator = new CharSequenceInterpolator(40.0f);
        this.appender = new Appender(new CharSequence[]{"|", ""}, 0.5f);
        this.appender.getAppendices()[0] = cursor;
    }

    public Typewriter(boolean cursorWhileTyping2, boolean cursorAfterTyping2) {
        this.cursorWhileTyping = false;
        this.cursorAfterTyping = true;
        this.interpolator = new CharSequenceInterpolator(40.0f);
        this.appender = new Appender(new CharSequence[]{"|", ""}, 0.5f);
        this.cursorWhileTyping = cursorWhileTyping2;
        this.cursorAfterTyping = cursorAfterTyping2;
    }

    public Typewriter(CharSequence cursor, boolean cursorWhileTyping2, boolean cursorAfterTyping2) {
        this(cursorWhileTyping2, cursorAfterTyping2);
        this.appender.getAppendices()[0] = cursor;
    }

    public void update(float delta) {
        this.interpolator.update(delta);
        this.appender.update(delta);
    }

    public CharSequence type(CharSequence seq) {
        CharSequence str = this.interpolator.interpolate(seq);
        if (str.length() == seq.length()) {
            if (this.cursorAfterTyping) {
                return this.appender.append(str);
            }
            return str;
        } else if (this.cursorWhileTyping) {
            return this.appender.append(str);
        } else {
            return str;
        }
    }

    public CharSequence updateAndType(CharSequence seq, float delta) {
        update(delta);
        return type(seq);
    }

    public float getCharsPerSecond() {
        return this.interpolator.getCharsPerSecond();
    }

    public void setCharsPerSecond(float charsPerSecond) {
        this.interpolator.setCharsPerSecond(charsPerSecond);
    }

    public float getTime() {
        return this.interpolator.getTime();
    }

    public void setTime(float time) {
        this.interpolator.setTime(time);
    }

    public boolean isCursorWhileTyping() {
        return this.cursorWhileTyping;
    }

    public void setCursorWhileTyping(boolean cursorWhileTyping2) {
        this.cursorWhileTyping = cursorWhileTyping2;
    }

    public boolean isCursorAfterTyping() {
        return this.cursorAfterTyping;
    }

    public void setCursorAfterTyping(boolean cursorAfterTyping2) {
        this.cursorAfterTyping = cursorAfterTyping2;
    }

    public CharSequenceInterpolator getInterpolator() {
        return this.interpolator;
    }

    public void setInterpolator(CharSequenceInterpolator interpolator2) {
        this.interpolator = interpolator2;
    }

    public Appender getAppender() {
        return this.appender;
    }

    public void setAppender(Appender appender2) {
        this.appender = appender2;
    }
}
