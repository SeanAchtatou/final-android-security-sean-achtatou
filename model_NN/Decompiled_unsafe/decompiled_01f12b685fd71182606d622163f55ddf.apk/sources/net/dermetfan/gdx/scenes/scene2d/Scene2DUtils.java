package net.dermetfan.gdx.scenes.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kbz.esotericsoftware.spine.Animation;

public class Scene2DUtils {
    static final /* synthetic */ boolean $assertionsDisabled = (!Scene2DUtils.class.desiredAssertionStatus());
    private static final Vector2 tmp = new Vector2();

    public static void copy(Event e, Event c) {
        c.setTarget(e.getTarget());
        c.setStage(e.getStage());
        c.setCapture(e.isCapture());
        c.setBubbles(e.getBubbles());
        c.setListenerActor(e.getListenerActor());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.scenes.scene2d.Scene2DUtils.copy(com.badlogic.gdx.scenes.scene2d.Event, com.badlogic.gdx.scenes.scene2d.Event):void
     arg types: [com.badlogic.gdx.scenes.scene2d.InputEvent, com.badlogic.gdx.scenes.scene2d.InputEvent]
     candidates:
      net.dermetfan.gdx.scenes.scene2d.Scene2DUtils.copy(com.badlogic.gdx.scenes.scene2d.InputEvent, com.badlogic.gdx.scenes.scene2d.InputEvent):void
      net.dermetfan.gdx.scenes.scene2d.Scene2DUtils.copy(com.badlogic.gdx.scenes.scene2d.Event, com.badlogic.gdx.scenes.scene2d.Event):void */
    public static void copy(InputEvent e, InputEvent c) {
        copy((Event) e, (Event) c);
        c.setStageX(e.getStageX());
        c.setStageY(e.getStageY());
        c.setButton(e.getButton());
        c.setCharacter(e.getCharacter());
        c.setKeyCode(e.getKeyCode());
        c.setPointer(e.getPointer());
        c.setType(e.getType());
        c.setRelatedActor(e.getRelatedActor());
        c.setScrollAmount(e.getScrollAmount());
    }

    public static Vector2 localToOtherCoordinates(Vector2 pos, Actor actor, Actor other) {
        Group lastParent = lastParent(actor);
        if (lastParent == null || lastParent != lastParent(other)) {
            throw new IllegalArgumentException(actor + " and " + other + " are not in the same hierarchy");
        }
        actor.localToAscendantCoordinates(lastParent, pos);
        lastParent.localToDescendantCoordinates(other, pos);
        return pos;
    }

    @Deprecated
    public static Vector2 stageToLocalCoordinates(Vector2 pos, Actor actor) {
        return actor == actor.getStage().getRoot() ? pos : actor.getStage().getRoot().localToDescendantCoordinates(actor, pos);
    }

    @Deprecated
    public static Vector2 stageToLocalCoordinates(float x, float y, Actor actor) {
        return stageToLocalCoordinates(tmp.set(x, y), actor);
    }

    public static Group lastParent(Actor actor) {
        if (!actor.hasParent()) {
            return null;
        }
        Group parent = actor.getParent();
        while (parent.hasParent()) {
            parent = parent.getParent();
        }
        if ($assertionsDisabled || !parent.hasParent()) {
            return parent;
        }
        throw new AssertionError();
    }

    public static Vector2 positionInStageCoordinates(Actor actor) {
        if (actor.hasParent()) {
            actor.localToStageCoordinates(tmp.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
        } else {
            tmp.set(actor.getX(), actor.getY());
        }
        return tmp;
    }

    public static void addAtStageCoordinates(Actor actor, Group newParent) {
        tmp.set(positionInStageCoordinates(actor));
        newParent.stageToLocalCoordinates(tmp);
        newParent.addActor(actor);
        actor.setPosition(tmp.x, tmp.y);
    }

    public static Vector2 pointerPosition(Stage stage) {
        return pointerPosition(stage, 0);
    }

    public static Vector2 pointerPosition(Stage stage, int pointer) {
        tmp.set((float) Gdx.input.getX(pointer), (float) Gdx.input.getY(pointer));
        stage.screenToStageCoordinates(tmp);
        return tmp;
    }

    public static Vector2 align(float width, float height, int align) {
        tmp.setZero();
        if ((align & 1) == 1) {
            tmp.set(width / 2.0f, height / 2.0f);
        }
        if ((align & 16) == 16) {
            tmp.x = width;
        }
        if ((align & 8) == 8) {
            tmp.x = Animation.CurveTimeline.LINEAR;
        }
        if ((align & 2) == 2) {
            tmp.y = height;
        }
        if ((align & 4) == 4) {
            tmp.y = Animation.CurveTimeline.LINEAR;
        }
        return tmp;
    }
}
