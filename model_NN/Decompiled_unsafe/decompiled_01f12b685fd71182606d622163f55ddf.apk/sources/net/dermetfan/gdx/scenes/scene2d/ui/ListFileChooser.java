package net.dermetfan.gdx.scenes.scene2d.ui;

import cn.egame.terminal.paysdk.EgamePay;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.Selection;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.io.File;
import java.util.Iterator;
import net.dermetfan.gdx.scenes.scene2d.ui.FileChooser;

public class ListFileChooser extends FileChooser {
    private Button backButton;
    public final ClickListener backButtonListener;
    private Button cancelButton;
    public final ClickListener cancelButtonListener;
    /* access modifiers changed from: private */
    public Button chooseButton;
    public final ClickListener chooseButtonListener;
    /* access modifiers changed from: private */
    public List<String> contents;
    public final ChangeListener contentsListener;
    private ScrollPane contentsPane;
    /* access modifiers changed from: private */
    public FileHandle directory;
    /* access modifiers changed from: private */
    public final Array<FileHandle> fileHistory;
    /* access modifiers changed from: private */
    public Files.FileType fileType;
    public final InputListener keyControlsListener;
    /* access modifiers changed from: private */
    public Button openButton;
    public final ClickListener openButtonListener;
    private Button parentButton;
    public final ClickListener parentButtonListener;
    /* access modifiers changed from: private */
    public TextField pathField;
    public final TextField.TextFieldListener pathFieldListener;
    private Style style;

    public ListFileChooser(Skin skin, FileChooser.Listener listener) {
        this((Style) skin.get(Style.class), listener);
        setSkin(skin);
    }

    public ListFileChooser(Skin skin, String styleName, FileChooser.Listener listener) {
        this((Style) skin.get(styleName, Style.class), listener);
        setSkin(skin);
    }

    public ListFileChooser(Style style2, FileChooser.Listener listener) {
        super(listener);
        this.fileType = Files.FileType.Absolute;
        this.fileHistory = new Array<>();
        this.pathFieldListener = new TextField.TextFieldListener() {
            public void keyTyped(TextField textField, char key) {
                if (key == 13 || key == 10) {
                    FileHandle loc = Gdx.files.getFileHandle(textField.getText(), ListFileChooser.this.fileType);
                    if (ListFileChooser.this.isNewFilesChoosable() || loc.exists()) {
                        if (loc.isDirectory()) {
                            ListFileChooser.this.setDirectory(loc);
                        } else {
                            ListFileChooser.this.getListener().choose(loc);
                        }
                        if (ListFileChooser.this.getStage() != null) {
                            ListFileChooser.this.getStage().setKeyboardFocus(ListFileChooser.this);
                        }
                    }
                }
            }
        };
        this.chooseButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Selection<String> selection = ListFileChooser.this.contents.getSelection();
                if (!selection.getMultiple()) {
                    FileHandle selected = ListFileChooser.this.currentlySelected();
                    if (ListFileChooser.this.isDirectoriesChoosable() || !selected.isDirectory()) {
                        ListFileChooser.this.getListener().choose(selected);
                    } else {
                        ListFileChooser.this.setDirectory(selected);
                    }
                } else {
                    Array<FileHandle> files = (Array) Pools.obtain(Array.class);
                    files.clear();
                    Iterator it = selection.iterator();
                    while (it.hasNext()) {
                        files.add(ListFileChooser.this.directory.child((String) it.next()));
                    }
                    ListFileChooser.this.getListener().choose(files);
                    Pools.free(files);
                }
            }
        };
        this.openButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                FileHandle child = ListFileChooser.this.currentlySelected();
                if (child.isDirectory()) {
                    ListFileChooser.this.setDirectory(child);
                }
            }
        };
        this.cancelButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ListFileChooser.this.getListener().cancel();
            }
        };
        this.backButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if (ListFileChooser.this.fileHistory.size > 1) {
                    ListFileChooser.this.fileHistory.removeIndex(ListFileChooser.this.fileHistory.size - 1);
                    ListFileChooser.this.setDirectory(ListFileChooser.this.directory = (FileHandle) ListFileChooser.this.fileHistory.peek(), false);
                }
            }
        };
        this.parentButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ListFileChooser.this.setDirectory(ListFileChooser.this.directory.parent());
            }
        };
        this.contentsListener = new ChangeListener() {
            public void changed(ChangeListener.ChangeEvent event, Actor actor) {
                ListFileChooser.this.openButton.setDisabled(!ListFileChooser.this.currentlySelected().isDirectory());
                ListFileChooser.this.chooseButton.setDisabled(ListFileChooser.this.isDirectoriesChoosable());
            }
        };
        this.keyControlsListener = new InputListener() {
            public boolean keyTyped(InputEvent event, char c) {
                int direction;
                if (event.isHandled()) {
                    return true;
                }
                if ((ListFileChooser.this.getStage() != null && ListFileChooser.this.getStage().getKeyboardFocus() == ListFileChooser.this.pathField) || (c != 13 && c != 10)) {
                    int keyCode = event.getKeyCode();
                    if (keyCode == 67) {
                        ListFileChooser.this.backButtonListener.clicked(null, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                        return true;
                    } else if (keyCode == 21) {
                        ListFileChooser.this.parentButtonListener.clicked(null, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                        return true;
                    } else {
                        if (keyCode == 19) {
                            direction = -1;
                        } else if (keyCode != 20) {
                            return false;
                        } else {
                            direction = 1;
                        }
                        ListFileChooser.this.contents.setSelectedIndex(MathUtils.clamp(ListFileChooser.this.contents.getSelectedIndex() + direction, 0, ListFileChooser.this.contents.getItems().size - 1));
                        return true;
                    }
                } else if (ListFileChooser.this.currentlySelected().isDirectory()) {
                    ListFileChooser.this.openButtonListener.clicked(null, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    return true;
                } else {
                    ListFileChooser.this.chooseButtonListener.clicked(null, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    return true;
                }
            }
        };
        this.style = style2;
        buildWidgets();
        setDirectory(Gdx.files.absolute(Gdx.files.getExternalStoragePath()));
        build();
        refresh();
    }

    public void setFileType(Files.FileType fileType2) {
        this.fileType = fileType2;
        this.fileHistory.clear();
        setDirectory(Gdx.files.getFileHandle("", fileType2));
    }

    /* access modifiers changed from: protected */
    public void buildWidgets() {
        addListener(this.keyControlsListener);
        TextField textField = new TextField("", this.style.pathFieldStyle);
        this.pathField = textField;
        textField.setTextFieldListener(this.pathFieldListener);
        this.contents = new List<>(this.style.contentsStyle);
        this.contents.addListener(this.contentsListener);
        Button newButton = UIUtils.newButton(this.style.chooseButtonStyle, "select");
        this.chooseButton = newButton;
        newButton.addListener(this.chooseButtonListener);
        Button newButton2 = UIUtils.newButton(this.style.openButtonStyle, "open");
        this.openButton = newButton2;
        newButton2.addListener(this.openButtonListener);
        Button newButton3 = UIUtils.newButton(this.style.cancelButtonStyle, EgamePay.PAY_PARAMS_KEY_MONTHLY_UNSUBSCRIBE_EVENT);
        this.cancelButton = newButton3;
        newButton3.addListener(this.cancelButtonListener);
        Button newButton4 = UIUtils.newButton(this.style.backButtonStyle, "back");
        this.backButton = newButton4;
        newButton4.addListener(this.backButtonListener);
        Button newButton5 = UIUtils.newButton(this.style.parentButtonStyle, "up");
        this.parentButton = newButton5;
        newButton5.addListener(this.parentButtonListener);
        this.contentsPane = this.style.contentsPaneStyle == null ? new ScrollPane(this.contents) : new ScrollPane(this.contents, this.style.contentsPaneStyle);
        setBackground(this.style.background);
    }

    /* access modifiers changed from: protected */
    public void build() {
        clearChildren();
        Style style2 = getStyle();
        add(this.backButton).fill().space(style2.space);
        add(this.pathField).fill().space(style2.space);
        add(this.parentButton).fill().space(style2.space).row();
        add(this.contentsPane).colspan(3).expand().fill().space(style2.space).row();
        if (isDirectoriesChoosable()) {
            add(this.openButton).fill().space(style2.space);
        }
        add(this.chooseButton).fill().colspan(isDirectoriesChoosable() ? 1 : 2).space(style2.space);
        add(this.cancelButton).fill().space(style2.space);
    }

    public void refresh() {
        scan(this.directory);
    }

    /* access modifiers changed from: protected */
    public void scan(FileHandle dir) {
        try {
            FileHandle[] files = dir.list(this.handlingFileFilter);
            String[] names = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                String name = files[i].name();
                if (files[i].isDirectory()) {
                    name = name + File.separator;
                }
                names[i] = name;
            }
            this.contents.setItems(names);
        } catch (GdxRuntimeException e) {
            Gdx.app.error("ListFileChooser", " cannot read " + dir);
        }
    }

    public FileHandle currentlySelected() {
        String selected = this.contents.getSelected();
        return selected == null ? this.directory : this.directory.child(selected);
    }

    public void setDirectory(FileHandle dir) {
        setDirectory(dir, true);
    }

    public void setDirectory(FileHandle dir, boolean addToHistory) {
        FileHandle loc = dir.isDirectory() ? dir : dir.parent();
        if (addToHistory) {
            this.fileHistory.add(loc);
        }
        this.directory = loc;
        scan(loc);
        this.pathField.setText(loc.path());
        this.pathField.setCursorPosition(this.pathField.getText().length());
    }

    public Button getBackButton() {
        return this.backButton;
    }

    public void setBackButton(Button backButton2) {
        this.backButton.removeListener(this.backButtonListener);
        backButton2.addListener(this.backButtonListener);
        Cell cell = getCell(this.backButton);
        this.backButton = backButton2;
        cell.setActor(backButton2);
    }

    public Button getCancelButton() {
        return this.cancelButton;
    }

    public void setCancelButton(Button cancelButton2) {
        this.cancelButton.removeListener(this.cancelButtonListener);
        cancelButton2.addListener(this.cancelButtonListener);
        Cell cell = getCell(this.cancelButton);
        this.cancelButton = cancelButton2;
        cell.setActor(cancelButton2);
    }

    public Button getChooseButton() {
        return this.chooseButton;
    }

    public void setChooseButton(Button chooseButton2) {
        this.chooseButton.removeListener(this.chooseButtonListener);
        chooseButton2.addListener(this.chooseButtonListener);
        Cell cell = getCell(this.chooseButton);
        this.chooseButton = chooseButton2;
        cell.setActor(chooseButton2);
    }

    public List<String> getContents() {
        return this.contents;
    }

    public void setContents(List<String> contents2) {
        this.contents.removeListener(this.contentsListener);
        contents2.addListener(this.contentsListener);
        this.contentsPane.setWidget(contents2);
    }

    public ScrollPane getContentsPane() {
        return this.contentsPane;
    }

    public void setContentsPane(ScrollPane contentsPane2) {
        contentsPane2.setWidget(this.contents);
        Cell cell = getCell(this.contentsPane);
        this.contentsPane = contentsPane2;
        cell.setActor(contentsPane2);
    }

    public FileHandle getDirectory() {
        return this.directory;
    }

    public Array<FileHandle> getFileHistory() {
        return this.fileHistory;
    }

    public void setFileHistory(Array<FileHandle> fileHistory2) {
        this.fileHistory.clear();
        this.fileHistory.addAll(fileHistory2);
    }

    public Button getOpenButton() {
        return this.openButton;
    }

    public void setOpenButton(Button openButton2) {
        this.openButton.removeListener(this.openButtonListener);
        openButton2.addListener(this.openButtonListener);
        Cell cell = getCell(this.openButton);
        this.openButton = openButton2;
        cell.setActor(openButton2);
    }

    public Button getParentButton() {
        return this.parentButton;
    }

    public void setParentButton(Button parentButton2) {
        this.parentButton.removeListener(this.parentButtonListener);
        parentButton2.addListener(this.parentButtonListener);
        Cell cell = getCell(this.parentButton);
        this.parentButton = parentButton2;
        cell.setActor(parentButton2);
    }

    public TextField getPathField() {
        return this.pathField;
    }

    public void setPathField(TextField pathField2) {
        this.pathField.setTextFieldListener(null);
        pathField2.setTextFieldListener(this.pathFieldListener);
        Cell cell = getCell(this.pathField);
        this.pathField = pathField2;
        cell.setActor(pathField2);
    }

    public void setDirectoriesChoosable(boolean directoriesChoosable) {
        if (isDirectoriesChoosable() != directoriesChoosable) {
            super.setDirectoriesChoosable(directoriesChoosable);
            build();
        }
    }

    public Style getStyle() {
        return this.style;
    }

    public void setStyle(Style style2) {
        this.style = style2;
        setBackground(style2.background);
        this.backButton.setStyle(style2.backButtonStyle);
        this.cancelButton.setStyle(style2.cancelButtonStyle);
        this.chooseButton.setStyle(style2.chooseButtonStyle);
        this.contents.setStyle(style2.contentsStyle);
        this.contentsPane.setStyle(style2.contentsPaneStyle);
        this.openButton.setStyle(style2.openButtonStyle);
        this.parentButton.setStyle(style2.parentButtonStyle);
        this.pathField.setStyle(style2.pathFieldStyle);
    }

    public static class Style implements Json.Serializable {
        public Button.ButtonStyle backButtonStyle;
        public Drawable background;
        public Button.ButtonStyle cancelButtonStyle;
        public Button.ButtonStyle chooseButtonStyle;
        public ScrollPane.ScrollPaneStyle contentsPaneStyle;
        public List.ListStyle contentsStyle;
        public Button.ButtonStyle openButtonStyle;
        public Button.ButtonStyle parentButtonStyle;
        public TextField.TextFieldStyle pathFieldStyle;
        public float space;

        public Style() {
        }

        public Style(Style style) {
            set(style);
        }

        public Style(TextField.TextFieldStyle textFieldStyle, List.ListStyle listStyle, Button.ButtonStyle buttonStyles, Drawable background2) {
            this(textFieldStyle, listStyle, buttonStyles, buttonStyles, buttonStyles, buttonStyles, buttonStyles, background2);
        }

        public Style(TextField.TextFieldStyle pathFieldStyle2, List.ListStyle contentsStyle2, Button.ButtonStyle chooseButtonStyle2, Button.ButtonStyle openButtonStyle2, Button.ButtonStyle cancelButtonStyle2, Button.ButtonStyle backButtonStyle2, Button.ButtonStyle parentButtonStyle2, Drawable background2) {
            this.pathFieldStyle = pathFieldStyle2;
            this.contentsStyle = contentsStyle2;
            this.chooseButtonStyle = chooseButtonStyle2;
            this.openButtonStyle = openButtonStyle2;
            this.cancelButtonStyle = cancelButtonStyle2;
            this.backButtonStyle = backButtonStyle2;
            this.parentButtonStyle = parentButtonStyle2;
            this.background = background2;
        }

        public void set(Style style) {
            this.pathFieldStyle = style.pathFieldStyle;
            this.contentsStyle = style.contentsStyle;
            this.chooseButtonStyle = style.chooseButtonStyle;
            this.openButtonStyle = style.openButtonStyle;
            this.cancelButtonStyle = style.cancelButtonStyle;
            this.backButtonStyle = style.backButtonStyle;
            this.parentButtonStyle = style.parentButtonStyle;
            this.contentsPaneStyle = style.contentsPaneStyle;
            this.background = style.background;
            this.space = style.space;
        }

        public void setButtonStyles(Button.ButtonStyle style) {
            this.parentButtonStyle = style;
            this.backButtonStyle = style;
            this.cancelButtonStyle = style;
            this.openButtonStyle = style;
            this.chooseButtonStyle = style;
        }

        public void write(Json json) {
            json.writeObjectStart("");
            json.writeFields(this);
            json.writeObjectEnd();
        }

        public void read(Json json, JsonValue jsonData) {
            setButtonStyles(UIUtils.readButtonStyle("buttonStyles", json, jsonData));
            Button.ButtonStyle tmpBS = UIUtils.readButtonStyle("backButtonStyle", json, jsonData);
            if (tmpBS != null) {
                this.backButtonStyle = tmpBS;
            }
            Button.ButtonStyle tmpBS2 = UIUtils.readButtonStyle("cancelButtonStyle", json, jsonData);
            if (tmpBS2 != null) {
                this.cancelButtonStyle = tmpBS2;
            }
            Button.ButtonStyle tmpBS3 = UIUtils.readButtonStyle("chooseButtonStyle", json, jsonData);
            if (tmpBS3 != null) {
                this.chooseButtonStyle = tmpBS3;
            }
            Button.ButtonStyle tmpBS4 = UIUtils.readButtonStyle("openButtonStyle", json, jsonData);
            if (tmpBS4 != null) {
                this.openButtonStyle = tmpBS4;
            }
            Button.ButtonStyle tmpBS5 = UIUtils.readButtonStyle("parentButtonStyle", json, jsonData);
            if (tmpBS5 != null) {
                this.parentButtonStyle = tmpBS5;
            }
            this.contentsStyle = (List.ListStyle) json.readValue("contentsStyle", List.ListStyle.class, jsonData);
            this.pathFieldStyle = (TextField.TextFieldStyle) json.readValue("pathFieldStyle", TextField.TextFieldStyle.class, jsonData);
            if (jsonData.has("contentsPaneStyle")) {
                this.contentsPaneStyle = (ScrollPane.ScrollPaneStyle) json.readValue("contentsPaneStyle", ScrollPane.ScrollPaneStyle.class, jsonData);
            }
            if (jsonData.has("space")) {
                this.space = ((Float) json.readValue("space", Float.TYPE, jsonData)).floatValue();
            }
        }
    }
}
