package net.dermetfan.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import net.dermetfan.utils.math.MathUtils;

public class ScrollPaneSnapAction extends Action {
    private boolean indicateVertical;
    private Indicator indicator;
    private ScrollPane pane;
    private final SlotSearchEvent searchEvent;
    private final FloatArray slots;
    private boolean snapOutCancelled;
    private boolean snapOutFired;
    private boolean snapped;
    private Value targetX;
    private Value targetY;
    private float visualScrollX;
    private float visualScrollY;

    public interface Indicator {
        void indicate(ScrollPaneSnapAction scrollPaneSnapAction, int i, int i2, float f);
    }

    public ScrollPaneSnapAction() {
        this(Value.percentWidth(0.5f), Value.percentHeight(0.5f));
    }

    public ScrollPaneSnapAction(Value targetX2, Value targetY2) {
        this.slots = new FloatArray();
        this.searchEvent = new SlotSearchEvent() {
            {
                setBubbles(false);
            }
        };
        this.snapOutFired = true;
        this.visualScrollX = Float.NaN;
        this.visualScrollY = Float.NaN;
        this.targetX = targetX2;
        this.targetY = targetY2;
    }

    public boolean act(float delta) {
        if (this.pane == null) {
            return false;
        }
        boolean cancelSnapping = false;
        if (this.pane.isDragging() || this.pane.isPanning()) {
            this.snapped = false;
            if (!this.snapOutFired) {
                SnapEvent event = (SnapEvent) Pools.obtain(SnapEvent.class);
                event.init(this, SnapEvent.Type.Out, getSnappedSlotX(), getSnappedSlotY());
                boolean fire = this.pane.fire(event);
                this.snapOutCancelled = fire;
                if (fire) {
                    snap0(getSnappedSlotX(), getSnappedSlotY());
                    this.pane.cancel();
                }
                Pools.free(event);
                this.snapOutFired = true;
            }
            cancelSnapping = true;
        }
        if (!cancelSnapping) {
            boolean z = this.snapped | this.snapOutCancelled;
            this.snapped = z;
            if (z) {
                this.snapOutCancelled = false;
                this.snapOutFired = false;
                cancelSnapping = true;
            }
        }
        boolean slotsUpdated = false;
        if (!cancelSnapping) {
            updateSlots();
            slotsUpdated = true;
            snapClosest();
        }
        if (!(this.indicator == null || (this.pane.getVisualScrollX() == this.visualScrollX && this.pane.getVisualScrollY() == this.visualScrollY))) {
            this.visualScrollX = this.pane.getVisualScrollX();
            this.visualScrollY = this.pane.getVisualScrollY();
            if (!slotsUpdated) {
                updateSlots();
            }
            float currentSlot = this.indicateVertical ? getSnappedSlotY() : getSnappedSlotX();
            int page = 0;
            float closestSmaller = Float.NEGATIVE_INFINITY;
            float closestGreater = Float.POSITIVE_INFINITY;
            for (int i = this.indicateVertical ? 1 : 0; i < this.slots.size; i += 2) {
                float slot = this.slots.get(i);
                float diff = currentSlot - slot;
                if (diff >= Animation.CurveTimeline.LINEAR) {
                    if (diff <= currentSlot - closestSmaller) {
                        closestSmaller = slot;
                    }
                } else if (diff >= currentSlot - closestGreater) {
                    closestGreater = slot;
                }
                if (slot <= currentSlot) {
                    page++;
                }
            }
            this.indicator.indicate(this, page, this.slots.size / 2, MathUtils.replaceNaN((currentSlot - closestSmaller) / (closestGreater - closestSmaller), 1.0f));
        }
        return false;
    }

    public void setTarget(Actor target) {
        super.setTarget(target);
        if (target instanceof ScrollPane) {
            this.pane = (ScrollPane) target;
            dirtyIndicator();
        } else if (target == null) {
            this.pane = null;
            if (this.indicator != null) {
                this.indicator.indicate(this, 0, 0, Animation.CurveTimeline.LINEAR);
            }
        }
    }

    public void reset() {
        super.reset();
        this.targetX = Value.percentWidth(0.5f);
        this.targetY = Value.percentHeight(0.5f);
        this.indicator = null;
        this.indicateVertical = false;
        this.slots.clear();
        this.searchEvent.reset();
        this.pane = null;
        this.snapOutFired = true;
        this.snapOutCancelled = false;
        this.snapped = false;
        this.visualScrollX = Float.NaN;
        this.visualScrollY = Float.NaN;
    }

    public void updateSlots() {
        this.slots.clear();
        findSlots(this.pane.getWidget());
    }

    private void findSlots(Actor root) {
        this.searchEvent.setTarget(this.pane.getWidget());
        root.notify(this.searchEvent, false);
        if (root instanceof Group) {
            Iterator<Actor> it = ((Group) root).getChildren().iterator();
            while (it.hasNext()) {
                findSlots(it.next());
            }
        }
    }

    public void reportSlot(float x, float y) {
        this.slots.ensureCapacity(2);
        this.slots.add(x);
        this.slots.add(y);
        dirtyIndicator();
    }

    public boolean findClosestSlot(Vector2 slot) {
        float targetX2 = this.targetX.get(this.pane) + this.pane.getVisualScrollX();
        float targetY2 = this.targetY.get(this.pane) + this.pane.getVisualScrollY();
        float closestDistance = Float.POSITIVE_INFINITY;
        boolean found = false;
        for (int i = 1; i < this.slots.size; i += 2) {
            float slotX = this.slots.get(i - 1);
            float slotY = this.slots.get(i);
            float distance = Vector2.dst2(targetX2, targetY2, slotX, slotY);
            if (distance <= closestDistance) {
                closestDistance = distance;
                slot.set(slotX, slotY);
                found = true;
            }
        }
        return found;
    }

    private void snapClosest() {
        Vector2 vec2 = (Vector2) Pools.obtain(Vector2.class);
        if (findClosestSlot(vec2)) {
            snap(vec2.x, vec2.y);
        } else {
            snap(getSnappedSlotX(), getSnappedSlotY());
        }
        Pools.free(vec2);
    }

    public void snap(float slotX, float slotY) {
        SnapEvent event = (SnapEvent) Pools.obtain(SnapEvent.class);
        if (!this.snapOutFired) {
            event.init(this, SnapEvent.Type.Out, getSnappedSlotX(), getSnappedSlotY());
            this.snapOutCancelled = this.pane.fire(event);
            this.snapOutFired = true;
            if (this.snapOutCancelled) {
                Pools.free(event);
                return;
            }
        }
        event.init(this, SnapEvent.Type.In, slotX, slotY);
        if (!this.pane.fire(event)) {
            snap0(slotX, slotY);
            this.snapped = true;
            this.snapOutFired = false;
        }
        Pools.free(event);
    }

    private void snap0(float slotX, float slotY) {
        this.pane.fling(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.pane.setScrollX(slotX - this.targetX.get(this.pane));
        this.pane.setScrollY(slotY - this.targetY.get(this.pane));
    }

    public void dirtyIndicator() {
        this.visualScrollY = Float.NaN;
        this.visualScrollX = Float.NaN;
    }

    public float getSnappedSlotX() {
        return this.pane.getScrollX() + this.targetX.get(this.pane);
    }

    public float getSnappedSlotY() {
        return this.pane.getScrollY() + this.targetY.get(this.pane);
    }

    public Value getTargetX() {
        return this.targetX;
    }

    public void setTargetX(Value targetX2) {
        this.targetX = targetX2;
    }

    public Value getTargetY() {
        return this.targetY;
    }

    public void setTargetY(Value targetY2) {
        this.targetY = targetY2;
    }

    public void setTarget(Value targetX2, Value targetY2) {
        this.targetX = targetX2;
        this.targetY = targetY2;
    }

    public Indicator getIndicator() {
        return this.indicator;
    }

    public void setIndicator(Indicator indicator2) {
        this.indicator = indicator2;
    }

    public boolean isIndicateVertical() {
        return this.indicateVertical;
    }

    public void setIndicateVertical(boolean indicateVertical2) {
        this.indicateVertical = indicateVertical2;
    }

    public FloatArray getSlots() {
        return this.slots;
    }

    public boolean isSnapped() {
        return this.snapped;
    }

    public static class SnapEvent extends Event {
        static final /* synthetic */ boolean $assertionsDisabled = (!ScrollPaneSnapAction.class.desiredAssertionStatus());
        private ScrollPaneSnapAction action;
        private float slotX;
        private float slotY;
        private Type type;

        public enum Type {
            In,
            Out
        }

        /* access modifiers changed from: private */
        public void init(ScrollPaneSnapAction action2, Type type2, float slotX2, float slotY2) {
            this.action = action2;
            this.type = type2;
            this.slotX = slotX2;
            this.slotY = slotY2;
        }

        public void reset() {
            super.reset();
            this.action = null;
            this.type = null;
            this.slotX = Animation.CurveTimeline.LINEAR;
            this.slotY = Animation.CurveTimeline.LINEAR;
        }

        public ScrollPane getScrollPane() {
            if ($assertionsDisabled || (getListenerActor() instanceof ScrollPane)) {
                return (ScrollPane) getListenerActor();
            }
            throw new AssertionError();
        }

        public ScrollPaneSnapAction getAction() {
            return this.action;
        }

        public Type getType() {
            return this.type;
        }

        public float getSlotX() {
            return this.slotX;
        }

        public float getSlotY() {
            return this.slotY;
        }
    }

    private class SlotSearchEvent extends Event {
        static final /* synthetic */ boolean $assertionsDisabled = (!ScrollPaneSnapAction.class.desiredAssertionStatus());

        private SlotSearchEvent() {
        }

        public ScrollPaneSnapAction getAction() {
            return ScrollPaneSnapAction.this;
        }

        public ScrollPane getScrollPane() {
            if ($assertionsDisabled || (getTarget().getParent() instanceof ScrollPane)) {
                return (ScrollPane) getTarget().getParent();
            }
            throw new AssertionError("SlotSearchEvent#getTarget() must be ScrollPane#getWidget()");
        }

        public void reportSlot(float x, float y) {
            Vector2 vec2 = (Vector2) Pools.obtain(Vector2.class);
            getListenerActor().localToAscendantCoordinates(getScrollPane().getWidget(), vec2.set(x, y));
            getAction().reportSlot(vec2.x, vec2.y);
            Pools.free(vec2);
        }
    }

    public static abstract class Slot implements EventListener {
        public abstract void getSlot(Actor actor, Vector2 vector2);

        public boolean handle(Event e) {
            if (!(e instanceof SlotSearchEvent)) {
                return false;
            }
            SlotSearchEvent event = (SlotSearchEvent) e;
            Vector2 vec2 = (Vector2) Pools.obtain(Vector2.class);
            getSlot(event.getListenerActor(), vec2);
            event.reportSlot(vec2.x, vec2.y);
            Pools.free(vec2);
            return false;
        }
    }

    public static class AlignSlot extends Slot {
        private int align;

        public AlignSlot(int align2) {
            this.align = align2;
        }

        public void getSlot(Actor actor, Vector2 slot) {
            slot.set(actor.getX(this.align) - actor.getX(), actor.getY(this.align) - actor.getY());
        }

        public int getAlign() {
            return this.align;
        }

        public void setAlign(int align2) {
            this.align = align2;
        }
    }

    public static class ValueSlot extends Slot {
        private Value valueX;
        private Value valueY;

        public ValueSlot(Value valueX2, Value valueY2) {
            this.valueX = valueX2;
            this.valueY = valueY2;
        }

        public void getSlot(Actor actor, Vector2 slot) {
            slot.set(this.valueX.get(actor), this.valueY.get(actor));
        }

        public Value getValueX() {
            return this.valueX;
        }

        public void setValueX(Value valueX2) {
            this.valueX = valueX2;
        }

        public Value getValueY() {
            return this.valueY;
        }

        public void setValueY(Value valueY2) {
            this.valueY = valueY2;
        }
    }
}
