package net.dermetfan.gdx.scenes.scene2d.ui;

import cn.egame.terminal.paysdk.EgamePay;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.Selection;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Pools;
import java.io.File;
import java.io.FileFilter;
import java.util.Iterator;
import net.dermetfan.gdx.scenes.scene2d.ui.FileChooser;
import net.dermetfan.utils.Function;

public class TreeFileChooser extends FileChooser {
    private Button cancelButton;
    public final ClickListener cancelButtonListener;
    /* access modifiers changed from: private */
    public Button chooseButton;
    public final ClickListener chooseButtonListener;
    private Style style;
    /* access modifiers changed from: private */
    public Tree tree;
    public final ClickListener treeListener;
    private ScrollPane treePane;

    public static Tree.Node fileNode(FileHandle file, Label.LabelStyle labelStyle) {
        return fileNode(file, labelStyle, (Function<Void, Tree.Node>) null);
    }

    public static Tree.Node fileNode(FileHandle file, Label.LabelStyle labelStyle, Function<Void, Tree.Node> nodeConsumer) {
        return fileNode(file, (FileFilter) null, labelStyle, nodeConsumer);
    }

    public static Tree.Node fileNode(FileHandle file, FileFilter filter, Label.LabelStyle labelStyle) {
        return fileNode(file, filter, labelStyle, (Function<Void, Tree.Node>) null);
    }

    public static Tree.Node fileNode(FileHandle file, FileFilter filter, final Label.LabelStyle labelStyle, Function<Void, Tree.Node> nodeConsumer) {
        return fileNode(file, filter, new Function<Label, FileHandle>() {
            public Label apply(FileHandle file) {
                String name = file.name();
                if (name.length() == 0) {
                    String name2 = file.path();
                    name = name2.substring(0, name2.lastIndexOf(47));
                }
                if (file.isDirectory()) {
                    name = name + File.separatorChar;
                }
                return new Label(name, labelStyle);
            }
        }, nodeConsumer);
    }

    public static Tree.Node fileNode(FileHandle file, FileFilter filter, Function<Label, FileHandle> labelSupplier) {
        return fileNode(file, filter, labelSupplier, (Function<Void, Tree.Node>) null);
    }

    public static Tree.Node fileNode(FileHandle file, FileFilter filter, Function<Label, FileHandle> labelSupplier, Function<Void, Tree.Node> nodeConsumer) {
        Tree.Node node;
        Label label = labelSupplier.apply(file);
        if (file.isDirectory()) {
            final Tree.Node dummy = new Tree.Node(new Actor());
            final FileFilter fileFilter = filter;
            final FileHandle fileHandle = file;
            final Function<Label, FileHandle> function = labelSupplier;
            final Function<Void, Tree.Node> function2 = nodeConsumer;
            node = new Tree.Node(label) {
                private boolean childrenAdded;

                public void setExpanded(boolean expanded) {
                    int i = 0;
                    if (expanded != isExpanded()) {
                        if (expanded && !this.childrenAdded) {
                            if (fileFilter != null) {
                                FileHandle[] list = fileHandle.list(fileFilter);
                                int length = list.length;
                                while (i < length) {
                                    add(TreeFileChooser.fileNode(fileHandle.child(list[i].name()), fileFilter, function, function2));
                                    i++;
                                }
                            } else {
                                FileHandle[] list2 = fileHandle.list();
                                int length2 = list2.length;
                                while (i < length2) {
                                    add(TreeFileChooser.fileNode(list2[i], fileFilter, function, function2));
                                    i++;
                                }
                            }
                            this.childrenAdded = true;
                            remove(dummy);
                        }
                        super.setExpanded(expanded);
                    }
                }
            };
            node.add(dummy);
            if (nodeConsumer != null) {
                nodeConsumer.apply(dummy);
            }
        } else {
            node = new Tree.Node(label);
        }
        node.setObject(file);
        if (nodeConsumer != null) {
            nodeConsumer.apply(node);
        }
        return node;
    }

    public TreeFileChooser(Skin skin, FileChooser.Listener listener) {
        this((Style) skin.get(Style.class), listener);
        setSkin(skin);
    }

    public TreeFileChooser(Skin skin, String styleName, FileChooser.Listener listener) {
        this((Style) skin.get(styleName, Style.class), listener);
        setSkin(skin);
    }

    public TreeFileChooser(Style style2, FileChooser.Listener listener) {
        super(listener);
        this.treeListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Selection<Tree.Node> selection = TreeFileChooser.this.tree.getSelection();
                if (selection.size() < 1) {
                    TreeFileChooser.this.chooseButton.setDisabled(true);
                    return;
                }
                if (!TreeFileChooser.this.isDirectoriesChoosable()) {
                    Object lastObj = selection.getLastSelected().getObject();
                    if ((lastObj instanceof FileHandle) && ((FileHandle) lastObj).isDirectory()) {
                        TreeFileChooser.this.chooseButton.setDisabled(true);
                        return;
                    }
                }
                TreeFileChooser.this.chooseButton.setDisabled(false);
            }
        };
        this.chooseButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if (!TreeFileChooser.this.chooseButton.isDisabled()) {
                    Selection<Tree.Node> selection = TreeFileChooser.this.tree.getSelection();
                    if (selection.size() < 1) {
                        return;
                    }
                    if (!selection.getMultiple() || selection.size() <= 1) {
                        Object object = selection.getLastSelected().getObject();
                        if (object instanceof FileHandle) {
                            FileHandle file = (FileHandle) object;
                            if (TreeFileChooser.this.isDirectoriesChoosable() || !file.isDirectory()) {
                                TreeFileChooser.this.getListener().choose(file);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    Array<FileHandle> files = (Array) Pools.obtain(Array.class);
                    Iterator<Tree.Node> it = selection.iterator();
                    while (it.hasNext()) {
                        Object object2 = it.next().getObject();
                        if (object2 instanceof FileHandle) {
                            FileHandle file2 = (FileHandle) object2;
                            if (TreeFileChooser.this.isDirectoriesChoosable() || !file2.isDirectory()) {
                                files.add(file2);
                            }
                        }
                    }
                    TreeFileChooser.this.getListener().choose(files);
                    files.clear();
                    Pools.free(files);
                }
            }
        };
        this.cancelButtonListener = new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                TreeFileChooser.this.getListener().cancel();
            }
        };
        this.style = style2;
        buildWidgets();
        build();
    }

    public Tree.Node add(FileHandle file) {
        Tree.Node node = fileNode(file, this.handlingFileFilter, this.style.labelStyle);
        this.tree.add(node);
        return node;
    }

    /* access modifiers changed from: protected */
    public void buildWidgets() {
        Tree tree2 = new Tree(this.style.treeStyle);
        this.tree = tree2;
        tree2.addListener(this.treeListener);
        if (this.style.scrollPaneStyle != null) {
            this.treePane = new ScrollPane(this.tree, this.style.scrollPaneStyle);
        } else {
            this.treePane = new ScrollPane(this.tree);
        }
        Button newButton = UIUtils.newButton(this.style.selectButtonStyle, "select");
        this.chooseButton = newButton;
        newButton.addListener(this.chooseButtonListener);
        this.chooseButton.setDisabled(true);
        Button newButton2 = UIUtils.newButton(this.style.cancelButtonStyle, EgamePay.PAY_PARAMS_KEY_MONTHLY_UNSUBSCRIBE_EVENT);
        this.cancelButton = newButton2;
        newButton2.addListener(this.cancelButtonListener);
    }

    /* access modifiers changed from: protected */
    public void build() {
        clearChildren();
        this.treePane.setWidget(this.tree);
        add(this.treePane).colspan(2).row();
        add(this.chooseButton).fill();
        add(this.cancelButton).fill();
    }

    public Style getStyle() {
        return this.style;
    }

    public void setStyle(Style style2) {
        this.style = style2;
        setBackground(style2.background);
        this.tree.setStyle(style2.treeStyle);
        this.chooseButton.setStyle(style2.selectButtonStyle);
        this.cancelButton.setStyle(style2.cancelButtonStyle);
    }

    public Tree getTree() {
        return this.tree;
    }

    public void setTree(Tree tree2) {
        this.tree.removeListener(this.treeListener);
        this.tree = tree2;
        tree2.addListener(this.treeListener);
        this.treePane.setWidget(tree2);
    }

    public ScrollPane getTreePane() {
        return this.treePane;
    }

    public void setTreePane(ScrollPane treePane2) {
        treePane2.setWidget(this.tree);
        Cell cell = getCell(this.treePane);
        this.treePane = treePane2;
        cell.setActor(treePane2);
    }

    public Button getChooseButton() {
        return this.chooseButton;
    }

    public void setChooseButton(Button chooseButton2) {
        this.chooseButton.removeListener(this.chooseButtonListener);
        chooseButton2.addListener(this.chooseButtonListener);
        Cell cell = getCell(this.chooseButton);
        this.chooseButton = chooseButton2;
        cell.setActor(chooseButton2);
    }

    public Button getCancelButton() {
        return this.cancelButton;
    }

    public void setCancelButton(Button cancelButton2) {
        this.cancelButton.removeListener(this.cancelButtonListener);
        cancelButton2.addListener(this.cancelButtonListener);
        Cell cell = getCell(this.cancelButton);
        this.cancelButton = cancelButton2;
        cell.setActor(cancelButton2);
    }

    public static class Style implements Json.Serializable {
        public Drawable background;
        public Button.ButtonStyle cancelButtonStyle;
        public Label.LabelStyle labelStyle;
        public ScrollPane.ScrollPaneStyle scrollPaneStyle;
        public Button.ButtonStyle selectButtonStyle;
        public Tree.TreeStyle treeStyle;

        public void write(Json json) {
            json.writeObjectStart("");
            json.writeFields(this);
            json.writeObjectEnd();
        }

        public void read(Json json, JsonValue jsonData) {
            this.treeStyle = (Tree.TreeStyle) json.readValue("treeStyle", Tree.TreeStyle.class, jsonData);
            if (jsonData.has("scrollPaneStyle")) {
                this.scrollPaneStyle = (ScrollPane.ScrollPaneStyle) json.readValue("scrollPaneStyle", ScrollPane.ScrollPaneStyle.class, jsonData);
            }
            this.labelStyle = (Label.LabelStyle) json.readValue("labelStyle", Label.LabelStyle.class, jsonData);
            this.selectButtonStyle = UIUtils.readButtonStyle("selectButtonStyle", json, jsonData);
            this.cancelButtonStyle = UIUtils.readButtonStyle("cancelButtonStyle", json, jsonData);
            if (jsonData.has("background")) {
                this.background = (Drawable) json.readValue("background", Drawable.class, jsonData);
            }
        }
    }
}
