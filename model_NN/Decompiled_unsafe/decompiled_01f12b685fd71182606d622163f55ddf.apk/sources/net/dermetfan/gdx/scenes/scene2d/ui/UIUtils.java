package net.dermetfan.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.kbz.esotericsoftware.spine.Animation;

public class UIUtils extends com.badlogic.gdx.scenes.scene2d.utils.UIUtils {
    @Deprecated
    public static void layoutSize(Widget widget) {
        float f = Float.POSITIVE_INFINITY;
        float clamp = MathUtils.clamp(widget.getPrefWidth(), widget.getMinWidth(), widget.getMaxWidth() == Animation.CurveTimeline.LINEAR ? Float.POSITIVE_INFINITY : widget.getMaxWidth());
        float prefHeight = widget.getPrefHeight();
        float minHeight = widget.getMinHeight();
        if (widget.getMaxHeight() != Animation.CurveTimeline.LINEAR) {
            f = widget.getMaxHeight();
        }
        widget.setSize(clamp, MathUtils.clamp(prefHeight, minHeight, f));
    }

    public static Button newButton(Button.ButtonStyle style) {
        return newButton(style, "");
    }

    public static Button newButton(Button.ButtonStyle style, String textIfAny) {
        if (style instanceof ImageTextButton.ImageTextButtonStyle) {
            return new ImageTextButton(textIfAny, (ImageTextButton.ImageTextButtonStyle) style);
        }
        if (style instanceof TextButton.TextButtonStyle) {
            return new TextButton(textIfAny, (TextButton.TextButtonStyle) style);
        }
        if (style instanceof ImageButton.ImageButtonStyle) {
            return new ImageButton((ImageButton.ImageButtonStyle) style);
        }
        return new Button(style);
    }

    public static Button.ButtonStyle readButtonStyle(String name, Json json, JsonValue jsonValue) {
        try {
            return (Button.ButtonStyle) json.readValue(name, TextButton.TextButtonStyle.class, jsonValue);
        } catch (NullPointerException e) {
            try {
                return (Button.ButtonStyle) json.readValue(name, ImageButton.ImageButtonStyle.class, jsonValue);
            } catch (NullPointerException e2) {
                try {
                    return (Button.ButtonStyle) json.readValue(name, ImageTextButton.ImageTextButtonStyle.class, jsonValue);
                } catch (NullPointerException e3) {
                    try {
                        return (Button.ButtonStyle) json.readValue(name, Button.ButtonStyle.class, jsonValue);
                    } catch (NullPointerException e4) {
                        return null;
                    }
                }
            }
        }
    }
}
