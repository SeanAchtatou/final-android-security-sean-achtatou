package net.dermetfan.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import net.dermetfan.gdx.scenes.scene2d.Scene2DUtils;
import net.dermetfan.gdx.scenes.scene2d.utils.PolygonRegionDrawable;

public class JigsawPuzzle {
    private final Array<Piece> pieces;

    public JigsawPuzzle() {
        this.pieces = new Array<>(Piece.class);
    }

    public JigsawPuzzle(int pieces2) {
        this.pieces = new Array<>(pieces2);
    }

    public JigsawPuzzle(Piece... pieces2) {
        this.pieces = new Array<>(pieces2);
    }

    public void solve(Piece relativeTo) {
        if (!this.pieces.contains(relativeTo, true)) {
            throw new IllegalArgumentException("the reference piece is not part of the puzzle");
        }
        Iterator<Piece> it = this.pieces.iterator();
        while (it.hasNext()) {
            Piece piece = it.next();
            if (piece != relativeTo) {
                piece.place(relativeTo);
            }
        }
    }

    public void add(Piece piece) {
        if (!this.pieces.contains(piece, true)) {
            this.pieces.add(piece);
        }
    }

    public boolean remove(Piece piece) {
        return this.pieces.removeValue(piece, true);
    }

    @Deprecated
    public Piece findClosest(Piece piece) {
        float distance = Float.POSITIVE_INFINITY;
        Piece closest = null;
        Iterator<Piece> it = this.pieces.iterator();
        while (it.hasNext()) {
            Piece other = it.next();
            if (other != piece) {
                float dist = Vector2.dst(other.getX() + other.getSlotX(), other.getY() + other.getSlotY(), piece.getX() + piece.getSlotX(), piece.getY() + piece.getSlotY());
                if (dist < distance) {
                    distance = dist;
                    closest = other;
                }
            }
        }
        return closest;
    }

    public boolean isSolved(float tolerance) {
        Piece reference = this.pieces.first();
        Iterator<Piece> it = this.pieces.iterator();
        while (it.hasNext()) {
            if (!it.next().isPlacedCorrectly(reference, tolerance)) {
                return false;
            }
        }
        return true;
    }

    public Array<Piece> getPieces() {
        return this.pieces;
    }

    public static class Piece extends Image {
        private float slotX;
        private float slotY;

        public Piece(Drawable drawable) {
            super(drawable);
        }

        public void setDrawable(Drawable drawable) {
            super.setDrawable(drawable);
            if (drawable instanceof PolygonRegionDrawable) {
                PolygonRegionDrawable pd = (PolygonRegionDrawable) drawable;
                this.slotX = pd.getPolygonX();
                this.slotY = pd.getPolygonY();
                return;
            }
            this.slotY = Animation.CurveTimeline.LINEAR;
            this.slotX = Animation.CurveTimeline.LINEAR;
        }

        public Actor hit(float x, float y, boolean touchable) {
            PolygonRegionDrawable drawable;
            Actor hit = super.hit(x, y, touchable);
            if (getDrawable() instanceof PolygonRegionDrawable) {
                drawable = (PolygonRegionDrawable) getDrawable();
            } else {
                drawable = null;
            }
            if (hit != this || drawable == null) {
                return hit;
            }
            float[] vertices = drawable.getRegion().getVertices();
            if (!Intersector.isPointInPolygon(vertices, 0, vertices.length, ((x / getWidth()) * drawable.getPolygonWidth()) + this.slotX, ((y / getHeight()) * drawable.getPolygonHeight()) + this.slotY)) {
                return null;
            }
            return hit;
        }

        public void place(Piece reference) {
            Vector2 refPuzzlePoint = ((Vector2) Pools.obtain(Vector2.class)).set(reference.getX(), reference.getY()).sub(reference.slotX, reference.slotY);
            setPosition(refPuzzlePoint.x + this.slotX, refPuzzlePoint.y + this.slotY);
            Pools.free(refPuzzlePoint);
        }

        public boolean isPlacedCorrectly(Piece reference, float tolerance) {
            Vector2 puzzlePos = ((Vector2) Pools.obtain(Vector2.class)).set(-this.slotX, -this.slotY);
            Vector2 refPuzzlePoint = ((Vector2) Pools.obtain(Vector2.class)).set(-reference.slotX, -reference.slotY);
            localToStageCoordinates(puzzlePos);
            reference.localToStageCoordinates(refPuzzlePoint);
            boolean rel = puzzlePos.epsilonEquals(refPuzzlePoint, tolerance);
            Pools.free(puzzlePos);
            Pools.free(refPuzzlePoint);
            return rel;
        }

        public void setSlot(float slotX2, float slotY2) {
            this.slotX = slotX2;
            this.slotY = slotY2;
        }

        public float getSlotX() {
            return this.slotX;
        }

        public void setSlotX(float slotX2) {
            this.slotX = slotX2;
        }

        public float getSlotY() {
            return this.slotY;
        }

        public void setSlotY(float slotY2) {
            this.slotY = slotY2;
        }
    }

    public static class Source extends DragAndDrop.Source {
        private DragAndDrop dragAndDrop;
        private float moveBackDuration;
        private final DragAndDrop.Payload payload;
        private JigsawPuzzle puzzle;
        /* access modifiers changed from: private */
        public final Vector2 vec2;

        public Source(Group board, DragAndDrop dragAndDrop2, JigsawPuzzle puzzle2) {
            super(board);
            this.moveBackDuration = 0.5f;
            this.payload = new DragAndDrop.Payload();
            this.vec2 = new Vector2();
            this.dragAndDrop = dragAndDrop2;
            this.puzzle = puzzle2;
        }

        public Source(Group board, DragAndDrop dragAndDrop2, JigsawPuzzle puzzle2, float moveBackDuration2) {
            this(board, dragAndDrop2, puzzle2);
            this.moveBackDuration = moveBackDuration2;
        }

        public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
            Actor actor = getActor().hit(x, y, true);
            if (actor == getActor() || !(actor instanceof Piece) || !this.puzzle.getPieces().contains((Piece) actor, true)) {
                return null;
            }
            this.payload.setDragActor(actor);
            ((Group) getActor()).localToDescendantCoordinates(actor, this.vec2.set(x, y));
            this.dragAndDrop.setDragActorPosition(-this.vec2.x, (-this.vec2.y) + actor.getHeight());
            this.vec2.set(actor.getX(), actor.getY());
            return this.payload;
        }

        public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload2, DragAndDrop.Target target) {
            final Actor actor = payload2.getDragActor();
            if (actor.getParent() == null) {
                getActor().getStage().addActor(actor);
                getActor().localToStageCoordinates(this.vec2);
                actor.addAction(Actions.sequence(Actions.moveTo(this.vec2.x, this.vec2.y, this.moveBackDuration), Actions.run(new Runnable() {
                    public void run() {
                        ((Group) Source.this.getActor()).addActor(actor);
                        Source.this.getActor().stageToLocalCoordinates(Source.this.vec2);
                        actor.setPosition(Source.this.vec2.x, Source.this.vec2.y);
                    }
                })));
            }
        }

        public DragAndDrop getDragAndDrop() {
            return this.dragAndDrop;
        }

        public void setDragAndDrop(DragAndDrop dragAndDrop2) {
            this.dragAndDrop = dragAndDrop2;
        }

        public JigsawPuzzle getPuzzle() {
            return this.puzzle;
        }

        public void setPuzzle(JigsawPuzzle puzzle2) {
            this.puzzle = puzzle2;
        }

        public float getMoveBackDuration() {
            return this.moveBackDuration;
        }

        public void setMoveBackDuration(float moveBackDuration2) {
            this.moveBackDuration = moveBackDuration2;
        }
    }

    public static class Target extends DragAndDrop.Target {
        private JigsawPuzzle puzzle;
        private float tolerance;

        public Target(Group group, JigsawPuzzle puzzle2, float tolerance2) {
            super(group);
            this.puzzle = puzzle2;
            this.tolerance = tolerance2;
        }

        public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            return true;
        }

        public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
            Actor dragged = payload.getDragActor();
            Scene2DUtils.addAtStageCoordinates(dragged, (Group) getActor());
            if (dragged instanceof Piece) {
                Piece piece = (Piece) dragged;
                for (int i = 0; i < this.puzzle.getPieces().size; i++) {
                    Piece ref = this.puzzle.getPieces().get(i);
                    if (ref != piece && piece.isPlacedCorrectly(ref, this.tolerance)) {
                        piece.place(ref);
                        placed(piece);
                        return;
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void placed(Piece piece) {
            if (this.puzzle.isSolved(this.tolerance)) {
                solved();
            }
        }

        /* access modifiers changed from: protected */
        public void solved() {
        }

        public JigsawPuzzle getPuzzle() {
            return this.puzzle;
        }

        public void setPuzzle(JigsawPuzzle puzzle2) {
            this.puzzle = puzzle2;
        }

        public float getTolerance() {
            return this.tolerance;
        }

        public void setTolerance(float tolerance2) {
            this.tolerance = tolerance2;
        }
    }
}
