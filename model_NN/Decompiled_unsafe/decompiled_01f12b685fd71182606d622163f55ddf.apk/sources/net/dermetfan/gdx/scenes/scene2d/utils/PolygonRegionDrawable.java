package net.dermetfan.gdx.scenes.scene2d.utils;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import net.dermetfan.utils.math.GeometryUtils;

public class PolygonRegionDrawable extends BaseDrawable implements TransformDrawable {
    private float polygonHeight;
    private float polygonWidth;
    private float polygonX;
    private float polygonY;
    private PolygonRegion region;

    public PolygonRegionDrawable() {
    }

    public PolygonRegionDrawable(PolygonRegion region2) {
        setRegion(region2);
    }

    public PolygonRegionDrawable(PolygonRegionDrawable drawable) {
        super(drawable);
        this.region = drawable.region;
        this.polygonX = drawable.polygonX;
        this.polygonY = drawable.polygonY;
        this.polygonWidth = drawable.polygonWidth;
        this.polygonHeight = drawable.polygonHeight;
    }

    public void draw(Batch batch, float x, float y, float width, float height) {
        float width2 = width + (((float) this.region.getRegion().getRegionWidth()) - this.polygonWidth);
        float height2 = height + (((float) this.region.getRegion().getRegionHeight()) - this.polygonHeight);
        if (batch instanceof PolygonSpriteBatch) {
            ((PolygonSpriteBatch) batch).draw(this.region, x - this.polygonX, y - this.polygonY, width2, height2);
            return;
        }
        batch.draw(this.region.getRegion(), x, y, width2, height2);
    }

    public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
        float width2 = width + (((float) this.region.getRegion().getRegionWidth()) - this.polygonWidth);
        float height2 = height + (((float) this.region.getRegion().getRegionHeight()) - this.polygonHeight);
        if (batch instanceof PolygonSpriteBatch) {
            ((PolygonSpriteBatch) batch).draw(this.region, x - this.polygonX, y - this.polygonY, originX, originY, width2, height2, scaleX, scaleY, rotation);
            return;
        }
        batch.draw(this.region.getRegion(), x, y, originX, originY, width2, height2, scaleX, scaleY, rotation);
    }

    public void setRegion(PolygonRegion region2) {
        this.region = region2;
        float[] vertices = region2.getVertices();
        this.polygonWidth = GeometryUtils.width(vertices);
        this.polygonHeight = GeometryUtils.height(vertices);
        this.polygonX = GeometryUtils.minX(vertices);
        this.polygonY = GeometryUtils.minY(vertices);
        setMinWidth(this.polygonWidth);
        setMinHeight(this.polygonHeight);
    }

    public PolygonRegion getRegion() {
        return this.region;
    }

    public float getPolygonX() {
        return this.polygonX;
    }

    public float getPolygonY() {
        return this.polygonY;
    }

    public float getPolygonWidth() {
        return this.polygonWidth;
    }

    public float getPolygonHeight() {
        return this.polygonHeight;
    }
}
