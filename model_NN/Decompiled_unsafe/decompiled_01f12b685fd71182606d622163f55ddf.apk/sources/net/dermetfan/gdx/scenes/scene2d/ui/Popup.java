package net.dermetfan.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.Timer;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import net.dermetfan.gdx.Multiplexer;
import net.dermetfan.gdx.scenes.scene2d.Scene2DUtils;

public class Popup<T extends Actor> implements EventListener {
    private Behavior behavior;
    private T popup;

    public Popup(T popup2, Behavior behavior2) {
        this.popup = popup2;
        this.behavior = behavior2;
    }

    public Popup(T popup2, Behavior... behaviors) {
        this(popup2, new BehaviorMultiplexer(behaviors));
    }

    public boolean show() {
        Event dummy = (Event) Pools.obtain(InputEvent.class);
        boolean result = show(dummy);
        Pools.free(dummy);
        return result;
    }

    public boolean show(Event event) {
        return this.behavior.show(event, this);
    }

    public boolean hide() {
        Event dummy = (Event) Pools.obtain(InputEvent.class);
        boolean result = hide(dummy);
        Pools.free(dummy);
        return result;
    }

    public boolean hide(Event event) {
        return this.behavior.hide(event, this);
    }

    public boolean handle(Event event) {
        Reaction reaction = this.behavior.handle(event, this);
        if (reaction == null) {
            reaction = Reaction.None;
        }
        switch (reaction) {
            case ShowHandle:
            case Show:
                show(event);
                break;
            case HideHandle:
            case Hide:
                hide(event);
                break;
        }
        return reaction.handles;
    }

    public boolean isAscendantOf(Actor child) {
        if (this.popup == child) {
            return true;
        }
        Iterator<EventListener> it = this.popup.getListeners().iterator();
        while (it.hasNext()) {
            EventListener listener = it.next();
            if ((listener instanceof Popup) && ((Popup) listener).isAscendantOf(child)) {
                return true;
            }
        }
        return false;
    }

    public T getPopup() {
        return this.popup;
    }

    public void setPopup(T popup2) {
        this.popup = popup2;
    }

    public Behavior getBehavior() {
        return this.behavior;
    }

    public void setBehavior(Behavior behavior2) {
        this.behavior = behavior2;
    }

    public enum Reaction {
        Show(false),
        Hide(false),
        None(false),
        ShowHandle(true),
        HideHandle(true),
        Handle(true);
        
        public final boolean handles;

        private Reaction(boolean handles2) {
            this.handles = handles2;
        }
    }

    public interface Behavior {
        Reaction handle(Event event, Popup popup);

        boolean hide(Event event, Popup popup);

        boolean show(Event event, Popup popup);

        public static class Adapter implements Behavior {
            public boolean show(Event event, Popup popup) {
                return false;
            }

            public boolean hide(Event event, Popup popup) {
                return false;
            }

            public Reaction handle(Event event, Popup popup) {
                return null;
            }
        }
    }

    public static class BehaviorMultiplexer extends Multiplexer<Behavior> implements Behavior {
        static final /* synthetic */ boolean $assertionsDisabled = (!Popup.class.desiredAssertionStatus());

        public BehaviorMultiplexer() {
        }

        public BehaviorMultiplexer(int size) {
            super(size);
        }

        public BehaviorMultiplexer(Behavior... receivers) {
            super(receivers);
        }

        public BehaviorMultiplexer(Array<Behavior> receivers) {
            super(receivers);
        }

        public boolean show(Event event, Popup popup) {
            boolean handled = false;
            Iterator it = this.receivers.iterator();
            while (it.hasNext()) {
                handled |= ((Behavior) it.next()).show(event, popup);
            }
            return handled;
        }

        public boolean hide(Event event, Popup popup) {
            boolean handled = false;
            Iterator it = this.receivers.iterator();
            while (it.hasNext()) {
                handled |= ((Behavior) it.next()).hide(event, popup);
            }
            return handled;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public Reaction handle(Event event, Popup popup) {
            Reaction reaction = null;
            boolean handled = false;
            for (int i = 0; i < this.receivers.size - 1; i++) {
                Reaction itsReaction = ((Behavior) this.receivers.get(i)).handle(event, popup);
                if (reaction == null) {
                    reaction = itsReaction;
                }
                if (!handled && itsReaction != null && itsReaction.handles) {
                    handled = true;
                }
            }
            if (!handled || reaction.handles) {
                return reaction;
            }
            switch (reaction) {
                case Show:
                    return Reaction.ShowHandle;
                case HideHandle:
                default:
                    if (!$assertionsDisabled) {
                        throw new AssertionError();
                    }
                    break;
                case Hide:
                    return Reaction.HideHandle;
                case None:
                    break;
            }
            return Reaction.Handle;
        }
    }

    public static class AddToStageBehavior extends Behavior.Adapter {
        public boolean show(Event event, Popup popup) {
            if (popup.getPopup().getStage() != event.getStage()) {
                event.getStage().addActor(popup.getPopup());
            }
            return super.show(event, popup);
        }
    }

    public static class VisibilityBehavior extends Behavior.Adapter {
        public boolean show(Event event, Popup popup) {
            popup.getPopup().setVisible(true);
            popup.getPopup().toFront();
            return true;
        }

        public boolean hide(Event event, Popup popup) {
            popup.getPopup().setVisible(false);
            return false;
        }
    }

    public static class FadeBehavior extends Behavior.Adapter {
        private float fadeInDuration;
        private Interpolation fadeInInterpolation;
        private float fadeOutDuration;
        private Interpolation fadeOutInterpolation;

        public FadeBehavior() {
            this.fadeInDuration = 0.4f;
            this.fadeOutDuration = 0.4f;
            this.fadeInInterpolation = Interpolation.fade;
            this.fadeOutInterpolation = Interpolation.fade;
        }

        public FadeBehavior(float fadeDuration) {
            this(fadeDuration, fadeDuration);
        }

        public FadeBehavior(Interpolation fadeInterpolation) {
            this(fadeInterpolation, fadeInterpolation);
        }

        public FadeBehavior(float fadeDuration, Interpolation fadeInterpolation) {
            this(fadeDuration, fadeDuration, fadeInterpolation, fadeInterpolation);
        }

        public FadeBehavior(float fadeInDuration2, float fadeOutDuration2) {
            this.fadeInDuration = 0.4f;
            this.fadeOutDuration = 0.4f;
            this.fadeInInterpolation = Interpolation.fade;
            this.fadeOutInterpolation = Interpolation.fade;
            this.fadeInDuration = fadeInDuration2;
            this.fadeOutDuration = fadeOutDuration2;
        }

        public FadeBehavior(Interpolation fadeInInterpolation2, Interpolation fadeOutInterpolation2) {
            this.fadeInDuration = 0.4f;
            this.fadeOutDuration = 0.4f;
            this.fadeInInterpolation = Interpolation.fade;
            this.fadeOutInterpolation = Interpolation.fade;
            this.fadeInInterpolation = fadeInInterpolation2;
            this.fadeOutInterpolation = fadeOutInterpolation2;
        }

        public FadeBehavior(float fadeInDuration2, float fadeOutDuration2, Interpolation fadeInInterpolation2, Interpolation fadeOutInterpolation2) {
            this.fadeInDuration = 0.4f;
            this.fadeOutDuration = 0.4f;
            this.fadeInInterpolation = Interpolation.fade;
            this.fadeOutInterpolation = Interpolation.fade;
            this.fadeInDuration = fadeInDuration2;
            this.fadeOutDuration = fadeOutDuration2;
            this.fadeInInterpolation = fadeInInterpolation2;
            this.fadeOutInterpolation = fadeOutInterpolation2;
        }

        public boolean show(Event event, Popup popup) {
            popup.getPopup().toFront();
            popup.getPopup().addAction(Actions.sequence(Actions.visible(true), Actions.fadeIn(this.fadeInDuration, this.fadeInInterpolation)));
            return super.show(event, popup);
        }

        public boolean hide(Event event, Popup popup) {
            popup.getPopup().addAction(Actions.sequence(Actions.fadeOut(this.fadeOutDuration, this.fadeOutInterpolation), Actions.visible(false)));
            return super.hide(event, popup);
        }

        public float getFadeInDuration() {
            return this.fadeInDuration;
        }

        public void setFadeInDuration(float fadeInDuration2) {
            this.fadeInDuration = fadeInDuration2;
        }

        public float getFadeOutDuration() {
            return this.fadeOutDuration;
        }

        public void setFadeOutDuration(float fadeOutDuration2) {
            this.fadeOutDuration = fadeOutDuration2;
        }

        public Interpolation getFadeInInterpolation() {
            return this.fadeInInterpolation;
        }

        public void setFadeInInterpolation(Interpolation fadeInInterpolation2) {
            this.fadeInInterpolation = fadeInInterpolation2;
        }

        public Interpolation getFadeOutInterpolation() {
            return this.fadeOutInterpolation;
        }

        public void setFadeOutInterpolation(Interpolation fadeOutInterpolation2) {
            this.fadeOutInterpolation = fadeOutInterpolation2;
        }
    }

    public static class MenuBehavior extends Behavior.Adapter {
        private int showButtons = 1;

        public MenuBehavior() {
        }

        public MenuBehavior(int... showButtons2) {
            for (int button : showButtons2) {
                showOn(button);
            }
        }

        public Reaction handle(Event e, Popup popup) {
            if (!(e instanceof InputEvent)) {
                return Reaction.None;
            }
            InputEvent event = (InputEvent) e;
            switch (event.getType()) {
                case touchDown:
                    if (((1 << event.getButton()) & this.showButtons) == this.showButtons && event.getTarget().getListeners().contains(popup, true)) {
                        return Reaction.ShowHandle;
                    }
                    if (!popup.isAscendantOf(event.getTarget())) {
                        return Reaction.Hide;
                    }
                    if (event.getKeyCode() != 82 && event.getTarget().getListeners().contains(popup, true)) {
                        return Reaction.ShowHandle;
                    }
                    if (event.getKeyCode() == 131 || event.getKeyCode() == 4) {
                        return Reaction.HideHandle;
                    }
                    return null;
                case keyDown:
                    if (event.getKeyCode() != 82) {
                        break;
                    }
                    return Reaction.HideHandle;
                default:
                    return null;
            }
        }

        public int showOn(int button) {
            int i = this.showButtons | (1 << button);
            this.showButtons = i;
            return i;
        }

        public int showNotOn(int button) {
            int i = this.showButtons & ((1 << button) ^ -1);
            this.showButtons = i;
            return i;
        }

        public int getShowButtons() {
            return this.showButtons;
        }

        public void setShowButtons(int showButtons2) {
            this.showButtons = showButtons2;
        }
    }

    public static class TooltipBehavior extends Behavior.Adapter {
        private int cancelEvents = ((1 << InputEvent.Type.touchDown.ordinal()) | (1 << InputEvent.Type.exit.ordinal()));
        private float hideDelay;
        private int hideEvents = (((1 << InputEvent.Type.touchDown.ordinal()) | (1 << InputEvent.Type.touchUp.ordinal())) | (1 << InputEvent.Type.exit.ordinal()));
        private final PopupTask hideTask = new PopupTask() {
            public void run() {
                this.popup.hide(this.event);
            }
        };
        private float showDelay = 0.75f;
        private int showEvents = (1 << InputEvent.Type.enter.ordinal());
        private final PopupTask showTask = new PopupTask() {
            public void run() {
                this.popup.show(this.event);
            }
        };
        private int targetPopupCancelEvents;
        private int targetPopupHideEvents = (1 << InputEvent.Type.mouseMoved.ordinal());
        private int targetPopupShowEvents = ((1 << InputEvent.Type.enter.ordinal()) | (1 << InputEvent.Type.exit.ordinal()));

        public TooltipBehavior() {
        }

        public TooltipBehavior(float delay) {
            setDelay(delay);
        }

        public TooltipBehavior(float showDelay2, float hideDelay2) {
            this.showDelay = showDelay2;
            this.hideDelay = hideDelay2;
        }

        public TooltipBehavior(int showEvents2) {
            this.showEvents = showEvents2;
        }

        public TooltipBehavior(int showEvents2, int hideEvents2) {
            this.showEvents = showEvents2;
            this.hideEvents = hideEvents2;
        }

        public TooltipBehavior(int showEvents2, int hideEvents2, int cancelEvents2) {
            this.showEvents = showEvents2;
            this.hideEvents = hideEvents2;
            this.cancelEvents = cancelEvents2;
        }

        public Reaction handle(Event e, Popup popup) {
            if (!(e instanceof InputEvent)) {
                return super.handle(e, popup);
            }
            InputEvent event = (InputEvent) e;
            InputEvent.Type type = event.getType();
            int flag = 1 << type.ordinal();
            if (type == InputEvent.Type.keyDown && event.getKeyCode() == 131 && ((this.targetPopupHideEvents & flag) != flag || event.getTarget().getListeners().contains(popup, true))) {
                return Reaction.Hide;
            }
            if (event.getRelatedActor() == popup.getPopup()) {
                return super.handle(e, popup);
            }
            if ((this.cancelEvents & flag) == flag && ((this.targetPopupCancelEvents & flag) != flag || event.getTarget().getListeners().contains(popup, true))) {
                this.showTask.cancel();
            }
            if ((this.hideEvents & flag) == flag && ((this.targetPopupHideEvents & flag) != flag || event.getTarget().getListeners().contains(popup, true))) {
                if (this.hideDelay <= Animation.CurveTimeline.LINEAR) {
                    return Reaction.Hide;
                }
                this.hideTask.init(event, popup);
                if (!this.hideTask.isScheduled()) {
                    Timer.schedule(this.hideTask, this.hideDelay);
                }
            }
            if ((this.showEvents & flag) == flag && ((this.targetPopupShowEvents & flag) != flag || event.getTarget().getListeners().contains(popup, true))) {
                if (this.showDelay <= Animation.CurveTimeline.LINEAR) {
                    return Reaction.Show;
                }
                this.showTask.init(event, popup);
                if (!this.showTask.isScheduled()) {
                    Timer.schedule(this.showTask, this.showDelay);
                }
            }
            return super.handle(e, popup);
        }

        public int showOn(InputEvent.Type event) {
            int ordinal = this.showEvents | (1 << event.ordinal());
            this.showEvents = ordinal;
            return ordinal;
        }

        public int showNotOn(InputEvent.Type event) {
            int ordinal = this.showEvents & ((1 << event.ordinal()) ^ -1);
            this.showEvents = ordinal;
            return ordinal;
        }

        public int hideOn(InputEvent.Type event) {
            int ordinal = this.hideEvents | (1 << event.ordinal());
            this.hideEvents = ordinal;
            return ordinal;
        }

        public int hideNotOn(InputEvent.Type event) {
            int ordinal = this.hideEvents & ((1 << event.ordinal()) ^ -1);
            this.hideEvents = ordinal;
            return ordinal;
        }

        public int cancelOn(InputEvent.Type event) {
            int ordinal = this.cancelEvents | (1 << event.ordinal());
            this.cancelEvents = ordinal;
            return ordinal;
        }

        public int cancelNotOn(InputEvent.Type event) {
            int ordinal = this.cancelEvents & ((1 << event.ordinal()) ^ -1);
            this.cancelEvents = ordinal;
            return ordinal;
        }

        public void setDelay(float delay) {
            this.hideDelay = delay;
            this.showDelay = delay;
        }

        public float getShowDelay() {
            return this.showDelay;
        }

        public void setShowDelay(float showDelay2) {
            this.showDelay = showDelay2;
        }

        public float getHideDelay() {
            return this.hideDelay;
        }

        public void setHideDelay(float hideDelay2) {
            this.hideDelay = hideDelay2;
        }

        public int getShowEvents() {
            return this.showEvents;
        }

        public void setShowEvents(int showEvents2) {
            this.showEvents = showEvents2;
        }

        public int getHideEvents() {
            return this.hideEvents;
        }

        public void setHideEvents(int hideEvents2) {
            this.hideEvents = hideEvents2;
        }

        public int getCancelEvents() {
            return this.cancelEvents;
        }

        public void setCancelEvents(int cancelEvents2) {
            this.cancelEvents = cancelEvents2;
        }

        public int getTargetPopupShowEvents() {
            return this.targetPopupShowEvents;
        }

        public void setTargetPopupShowEvents(int targetPopupShowEvents2) {
            this.targetPopupShowEvents = targetPopupShowEvents2;
        }

        public int getTargetPopupHideEvents() {
            return this.targetPopupHideEvents;
        }

        public void setTargetPopupHideEvents(int targetPopupHideEvents2) {
            this.targetPopupHideEvents = targetPopupHideEvents2;
        }

        public int getTargetPopupCancelEvents() {
            return this.targetPopupCancelEvents;
        }

        public void setTargetPopupCancelEvents(int targetPopupCancelEvents2) {
            this.targetPopupCancelEvents = targetPopupCancelEvents2;
        }

        private static abstract class PopupTask extends Timer.Task {
            protected final InputEvent event;
            protected Popup popup;

            private PopupTask() {
                this.event = new InputEvent();
            }

            public void init(InputEvent event2, Popup popup2) {
                this.event.reset();
                Scene2DUtils.copy(event2, this.event);
                this.popup = popup2;
            }
        }

        public static class TooltipPositionBehavior extends PositionBehavior {
            private boolean followPointer;

            public TooltipPositionBehavior(PositionBehavior.Position position) {
                super(position);
            }

            public TooltipPositionBehavior(PositionBehavior.Position position, boolean followPointer2) {
                super(position);
                this.followPointer = followPointer2;
            }

            public Reaction handle(Event event, Popup popup) {
                if (this.followPointer && (event instanceof InputEvent) && ((InputEvent) event).getType() == InputEvent.Type.mouseMoved) {
                    getPosition().apply(event, popup.getPopup());
                }
                return super.handle(event, popup);
            }

            public boolean isFollowPointer() {
                return this.followPointer;
            }

            public void setFollowPointer(boolean followPointer2) {
                this.followPointer = followPointer2;
            }
        }
    }

    public static class PositionBehavior extends Behavior.Adapter {
        private Position position;

        public interface Position {
            void apply(Event event, Actor actor);
        }

        public PositionBehavior(Position position2) {
            this.position = position2;
        }

        public PositionBehavior(Position... positions) {
            this(new PositionMultiplexer(positions));
        }

        public boolean show(Event event, Popup popup) {
            this.position.apply(event, popup.getPopup());
            return super.show(event, popup);
        }

        public Position getPosition() {
            return this.position;
        }

        public void setPosition(Position position2) {
            this.position = position2;
        }

        public static class PositionMultiplexer extends Multiplexer<Position> implements Position {
            public PositionMultiplexer() {
            }

            public PositionMultiplexer(int size) {
                super(size);
            }

            public PositionMultiplexer(Position... receivers) {
                super(receivers);
            }

            public PositionMultiplexer(Array<Position> receivers) {
                super(receivers);
            }

            public void apply(Event event, Actor popup) {
                Iterator it = this.receivers.iterator();
                while (it.hasNext()) {
                    ((Position) it.next()).apply(event, popup);
                }
            }
        }

        public static class PointerPosition implements Position {
            private int pointer;

            public PointerPosition() {
            }

            public PointerPosition(int pointer2) {
                this.pointer = pointer2;
            }

            public void apply(Event event, Actor popup) {
                Vector2 pos = Scene2DUtils.pointerPosition(event.getStage(), this.pointer);
                if (popup.hasParent()) {
                    popup.getParent().stageToLocalCoordinates(pos);
                }
                popup.setPosition(pos.x, pos.y);
            }

            public int getPointer() {
                return this.pointer;
            }

            public void setPointer(int pointer2) {
                this.pointer = pointer2;
            }
        }

        public static class PresetPosition implements Position {
            private float x;
            private float y;

            public PresetPosition() {
            }

            public PresetPosition(float x2, float y2) {
                this.x = x2;
                this.y = y2;
            }

            public void apply(Event event, Actor popup) {
                popup.setPosition(this.x, this.y);
            }

            public float getX() {
                return this.x;
            }

            public void setX(float x2) {
                this.x = x2;
            }

            public float getY() {
                return this.y;
            }

            public void setY(float y2) {
                this.y = y2;
            }
        }

        public static class OffsetPosition implements Position {
            private float x;
            private float y;

            public OffsetPosition(float x2, float y2) {
                this.x = x2;
                this.y = y2;
            }

            public void apply(Event event, Actor popup) {
                popup.setPosition(popup.getX() + this.x, popup.getY() + this.y);
            }

            public float getX() {
                return this.x;
            }

            public void setX(float x2) {
                this.x = x2;
            }

            public float getY() {
                return this.y;
            }

            public void setY(float y2) {
                this.y = y2;
            }
        }

        public static class EventPosition implements Position {
            public void apply(Event event, Actor popup) {
                if (event instanceof InputEvent) {
                    InputEvent inputEvent = (InputEvent) event;
                    Vector2 pos = (Vector2) Pools.obtain(Vector2.class);
                    pos.set(inputEvent.getStageX(), inputEvent.getStageY());
                    if (popup.hasParent()) {
                        popup.getParent().stageToLocalCoordinates(pos);
                    }
                    popup.setPosition(pos.x, pos.y);
                    Pools.free(pos);
                    return;
                }
                popup.setPosition(Float.NaN, Float.NaN);
            }
        }

        public static class AlignPosition implements Position {
            private int align;
            private int targetAlign;

            public AlignPosition(int targetAlign2, int align2) {
                this.targetAlign = targetAlign2;
                this.align = align2;
            }

            public void apply(Event event, Actor popup) {
                Actor target = event.getTarget();
                Vector2 pos = ((Vector2) Pools.obtain(Vector2.class)).setZero();
                pos.set(Scene2DUtils.align(target.getWidth(), target.getHeight(), this.targetAlign));
                target.localToStageCoordinates(pos);
                popup.stageToLocalCoordinates(pos);
                popup.localToParentCoordinates(pos);
                popup.setPosition(pos.x, pos.y, this.align);
                Pools.free(pos);
            }

            public int getAlign() {
                return this.align;
            }

            public void setAlign(int align2) {
                this.align = align2;
            }

            public int getTargetAlign() {
                return this.targetAlign;
            }

            public void setTargetAlign(int targetAlign2) {
                this.targetAlign = targetAlign2;
            }
        }

        public static class AlignedOffsetPosition implements Position {
            private int align;

            public AlignedOffsetPosition(int align2) {
                this.align = align2;
            }

            public void apply(Event event, Actor popup) {
                Vector2 offset = Scene2DUtils.align(popup.getWidth(), popup.getHeight(), this.align);
                popup.setPosition(popup.getX() - offset.x, popup.getY() - offset.y);
            }

            public int getAlign() {
                return this.align;
            }

            public void setAlign(int align2) {
                this.align = align2;
            }
        }
    }
}
