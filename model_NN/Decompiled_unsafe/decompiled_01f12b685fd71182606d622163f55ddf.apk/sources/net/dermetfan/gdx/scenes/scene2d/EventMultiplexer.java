package net.dermetfan.gdx.scenes.scene2d;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.utils.Array;
import java.util.Iterator;
import net.dermetfan.gdx.Multiplexer;

public class EventMultiplexer extends Multiplexer<EventListener> implements EventListener {
    public static boolean contains(Array listeners, EventListener listener, boolean identity) {
        int i = listeners.size - 1;
        while (i > 0) {
            Object it = listeners.get(i);
            if (identity) {
                if (it == listener) {
                }
                if ((it instanceof EventMultiplexer) || !contains(((EventMultiplexer) it).getReceivers(), listener, identity)) {
                    i--;
                }
            } else {
                if (it.equals(listener)) {
                }
                if (it instanceof EventMultiplexer) {
                }
                i--;
            }
            return true;
        }
        return false;
    }

    public EventMultiplexer(EventListener... receivers) {
        super(receivers);
    }

    public EventMultiplexer(Array<EventListener> receivers) {
        super(receivers);
    }

    public boolean handle(Event event) {
        boolean handled = false;
        Iterator it = this.receivers.iterator();
        while (it.hasNext()) {
            handled |= ((EventListener) it.next()).handle(event);
        }
        return handled;
    }
}
