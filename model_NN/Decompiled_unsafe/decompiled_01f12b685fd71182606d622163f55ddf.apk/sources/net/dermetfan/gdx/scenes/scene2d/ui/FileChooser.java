package net.dermetfan.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import java.io.File;
import java.io.FileFilter;

public abstract class FileChooser extends Table {
    private boolean directoriesChoosable;
    /* access modifiers changed from: private */
    public FileFilter fileFilter;
    protected final FileFilter handlingFileFilter = new FileFilter() {
        public boolean accept(File file) {
            return (FileChooser.this.showHidden || !file.isHidden()) && (FileChooser.this.fileFilter == null || FileChooser.this.fileFilter.accept(file));
        }
    };
    private Listener listener;
    private boolean newFilesChoosable;
    /* access modifiers changed from: private */
    public boolean showHidden;

    public interface Listener {
        void cancel();

        void choose(FileHandle fileHandle);

        void choose(Array<FileHandle> array);
    }

    /* access modifiers changed from: protected */
    public abstract void build();

    public FileChooser(Listener listener2) {
        this.listener = listener2;
    }

    public Listener getListener() {
        return this.listener;
    }

    public void setListener(Listener listener2) {
        this.listener = listener2;
    }

    public FileFilter getFileFilter() {
        return this.fileFilter;
    }

    public void setFileFilter(FileFilter fileFilter2) {
        this.fileFilter = fileFilter2;
    }

    public FileFilter getHandlingFileFilter() {
        return this.handlingFileFilter;
    }

    public boolean isShowHidden() {
        return this.showHidden;
    }

    public void setShowHidden(boolean showHidden2) {
        this.showHidden = showHidden2;
    }

    public boolean isDirectoriesChoosable() {
        return this.directoriesChoosable;
    }

    public void setDirectoriesChoosable(boolean directoriesChoosable2) {
        this.directoriesChoosable = directoriesChoosable2;
    }

    public boolean isNewFilesChoosable() {
        return this.newFilesChoosable;
    }

    public void setNewFilesChoosable(boolean newFilesChoosable2) {
        this.newFilesChoosable = newFilesChoosable2;
    }
}
