package net.dermetfan.gdx.scenes.scene2d.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.Layout;
import com.badlogic.gdx.utils.SnapshotArray;
import com.kbz.esotericsoftware.spine.Animation;

public class CircularGroup extends WidgetGroup {
    /* access modifiers changed from: private */
    public float angleOffset;
    private float cachedMinHeight;
    private float cachedMinWidth;
    private float cachedPrefHeight;
    private float cachedPrefWidth;
    private final DragManager dragManager;
    private float fullAngle;
    /* access modifiers changed from: private */
    public float maxAngleOffset;
    /* access modifiers changed from: private */
    public float minAngleOffset;
    private Modifier modifier;
    private boolean shrinkChildren;
    private boolean sizeInvalid;
    /* access modifiers changed from: private */
    public final Vector2 tmp;
    private boolean virtualChildEnabled;

    public CircularGroup() {
        this((Modifier) null);
    }

    public CircularGroup(Modifier modifier2) {
        this.fullAngle = 360.0f;
        this.maxAngleOffset = this.fullAngle;
        this.virtualChildEnabled = true;
        this.shrinkChildren = true;
        this.dragManager = new DragManager();
        this.sizeInvalid = true;
        this.tmp = new Vector2();
        this.modifier = modifier2 == null ? new Modifier.Adapter() : modifier2;
    }

    public CircularGroup(boolean draggable) {
        this(null, draggable);
    }

    public CircularGroup(Modifier modifier2, boolean draggable) {
        this(modifier2);
        setDraggable(draggable);
    }

    public void act(float delta) {
        this.dragManager.act(delta);
        super.act(delta);
    }

    public void drawDebug(ShapeRenderer shapes) {
        super.drawDebug(shapes);
        shapes.set(ShapeRenderer.ShapeType.Line);
        shapes.setColor(Color.CYAN);
        shapes.ellipse(getX(), getY(), getWidth() * getScaleX(), getHeight() * getScaleY());
        SnapshotArray<Actor> children = getChildren();
        for (int index = 0; index < children.size; index++) {
            Actor child = children.get(index);
            this.tmp.set(this.modifier.localAnchor(this.tmp.set(child.getWidth(), child.getHeight() / 2.0f), child, index, children.size, this));
            shapes.line(getX() + ((getWidth() / 2.0f) * getScaleX()), getY() + ((getHeight() / 2.0f) * getScaleY()), getX() + ((child.getX() + this.tmp.x) * getScaleX()), getY() + ((child.getY() + this.tmp.y) * getScaleY()));
        }
    }

    /* access modifiers changed from: protected */
    public void computeSize() {
        float prefWidth;
        float minWidth;
        float prefHeight;
        float minHeight;
        float minWidth2;
        float minHeight2;
        float prefWidth2;
        float prefHeight2;
        this.cachedMinHeight = Float.POSITIVE_INFINITY;
        this.cachedMinWidth = Float.POSITIVE_INFINITY;
        this.cachedPrefHeight = Animation.CurveTimeline.LINEAR;
        this.cachedPrefWidth = Animation.CurveTimeline.LINEAR;
        SnapshotArray<Actor> children = getChildren();
        for (int index = 0; index < children.size; index++) {
            Actor child = children.get(index);
            if (child instanceof Layout) {
                Layout layout = (Layout) child;
                minWidth = layout.getMinWidth();
                minHeight = layout.getMinHeight();
                prefWidth = layout.getPrefWidth();
                prefHeight = layout.getPrefHeight();
            } else {
                prefWidth = child.getWidth();
                minWidth = prefWidth;
                prefHeight = child.getHeight();
                minHeight = prefHeight;
            }
            this.tmp.set(this.modifier.anchorOffset(this.tmp.setZero(), child, index, children.size, this));
            float offsetX = this.tmp.x;
            float offsetY = this.tmp.y;
            this.tmp.set(this.modifier.localAnchor(this.tmp.set(minWidth, minHeight / 2.0f), child, index, children.size, this)).sub(offsetX, offsetY);
            if (this.tmp.x < minWidth || this.tmp.x < Animation.CurveTimeline.LINEAR) {
                minWidth2 = minWidth - this.tmp.x;
            } else {
                minWidth2 = minWidth + (this.tmp.x - minWidth);
            }
            if (this.tmp.y < minHeight || this.tmp.y < Animation.CurveTimeline.LINEAR) {
                minHeight2 = minHeight - this.tmp.y;
            } else {
                minHeight2 = minHeight + (this.tmp.y - minHeight);
            }
            this.tmp.set(this.modifier.localAnchor(this.tmp.set(prefWidth, prefHeight / 2.0f), child, index, children.size, this)).sub(offsetX, offsetY);
            if (this.tmp.x < prefWidth || this.tmp.x < Animation.CurveTimeline.LINEAR) {
                prefWidth2 = prefWidth - this.tmp.x;
            } else {
                prefWidth2 = prefWidth + (this.tmp.x - prefWidth);
            }
            if (this.tmp.y < prefHeight || this.tmp.y < Animation.CurveTimeline.LINEAR) {
                prefHeight2 = prefHeight - this.tmp.y;
            } else {
                prefHeight2 = prefHeight + (this.tmp.y - prefHeight);
            }
            if (minWidth2 < this.cachedMinWidth) {
                this.cachedMinWidth = minWidth2;
            }
            if (minHeight2 < this.cachedMinHeight) {
                this.cachedMinHeight = minHeight2;
            }
            if (prefWidth2 > this.cachedPrefWidth) {
                this.cachedPrefWidth = prefWidth2;
            }
            if (prefHeight2 > this.cachedPrefHeight) {
                this.cachedPrefHeight = prefHeight2;
            }
        }
        this.cachedMinWidth *= 2.0f;
        this.cachedMinHeight *= 2.0f;
        this.cachedPrefWidth *= 2.0f;
        this.cachedPrefHeight *= 2.0f;
        float max = Math.max(this.cachedMinWidth, this.cachedMinHeight);
        this.cachedMinHeight = max;
        this.cachedMinWidth = max;
        float max2 = Math.max(this.cachedPrefWidth, this.cachedPrefHeight);
        this.cachedPrefHeight = max2;
        this.cachedPrefWidth = max2;
        this.sizeInvalid = false;
    }

    public float getMinWidth() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.cachedMinWidth;
    }

    public float getMinHeight() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.cachedMinHeight;
    }

    public float getPrefWidth() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.cachedPrefWidth;
    }

    public float getPrefHeight() {
        if (this.sizeInvalid) {
            computeSize();
        }
        return this.cachedPrefHeight;
    }

    public void invalidate() {
        super.invalidate();
        this.sizeInvalid = true;
    }

    public void layout() {
        float width;
        float height;
        float prefWidthUnderflow = this.shrinkChildren ? Math.max((float) Animation.CurveTimeline.LINEAR, getPrefWidth() - getWidth()) / 2.0f : Animation.CurveTimeline.LINEAR;
        float prefHeightUnderflow = this.shrinkChildren ? Math.max((float) Animation.CurveTimeline.LINEAR, getPrefHeight() - getHeight()) / 2.0f : Animation.CurveTimeline.LINEAR;
        SnapshotArray<Actor> children = getChildren();
        for (int index = 0; index < children.size; index++) {
            Actor child = children.get(index);
            if (child instanceof Layout) {
                Layout childLayout = (Layout) child;
                width = Math.max(childLayout.getPrefWidth() - prefWidthUnderflow, childLayout.getMinWidth());
                if (childLayout.getMaxWidth() != Animation.CurveTimeline.LINEAR) {
                    width = Math.min(width, childLayout.getMaxWidth());
                }
                height = Math.max(childLayout.getPrefHeight() - prefHeightUnderflow, childLayout.getMinHeight());
                if (childLayout.getMaxHeight() != Animation.CurveTimeline.LINEAR) {
                    height = Math.min(height, childLayout.getMaxHeight());
                }
                child.setSize(width, height);
                childLayout.validate();
            } else {
                width = child.getWidth();
                height = child.getHeight();
            }
            float angle = this.modifier.angle(((this.fullAngle / ((float) (children.size - (this.virtualChildEnabled ? 0 : 1)))) * ((float) index)) + this.angleOffset, child, index, children.size, this);
            float rotation = this.modifier.rotation(angle, child, index, children.size, this);
            this.tmp.set(this.modifier.anchorOffset(this.tmp.setZero(), child, index, children.size, this));
            this.tmp.rotate(angle);
            float offsetX = this.tmp.x;
            float offsetY = this.tmp.y;
            this.tmp.set(this.modifier.localAnchor(this.tmp.set(width, height / 2.0f), child, index, children.size, this));
            float localAnchorX = this.tmp.x;
            float localAnchorY = this.tmp.y;
            child.setOrigin(localAnchorX, localAnchorY);
            child.setRotation(rotation);
            child.setPosition(((getWidth() / 2.0f) + offsetX) - localAnchorX, ((getHeight() / 2.0f) + offsetY) - localAnchorY);
        }
    }

    public boolean isDraggable() {
        return this.dragManager.isDraggingActivated();
    }

    public void setDraggable(boolean draggable) {
        this.dragManager.setDraggingActivated(draggable);
        if (draggable) {
            addListener(this.dragManager);
        } else {
            removeListener(this.dragManager);
        }
    }

    public void translateAngleOffsetLimits(float amount) {
        setMinAngleOffset(this.minAngleOffset + amount);
        setMaxAngleOffset(this.maxAngleOffset + amount);
    }

    public float getFullAngle() {
        return this.fullAngle;
    }

    public void setFullAngle(float fullAngle2) {
        setFullAngle(fullAngle2, fullAngle2 >= 360.0f);
    }

    public void setFullAngle(float fullAngle2, boolean virtualChildEnabled2) {
        this.fullAngle = fullAngle2;
        this.virtualChildEnabled = virtualChildEnabled2;
        invalidate();
    }

    public float getAngleOffset() {
        return this.angleOffset;
    }

    public void setAngleOffset(float angleOffset2) {
        this.angleOffset = MathUtils.clamp(angleOffset2, this.minAngleOffset, this.maxAngleOffset);
        invalidate();
    }

    public float getMinAngleOffset() {
        return this.minAngleOffset;
    }

    public void setMinAngleOffset(float minAngleOffset2) {
        if (minAngleOffset2 > this.maxAngleOffset) {
            throw new IllegalArgumentException("minAngleOffset must not be > maxAngleOffset");
        }
        this.minAngleOffset = minAngleOffset2;
        this.angleOffset = Math.max(minAngleOffset2, this.angleOffset);
    }

    public float getMaxAngleOffset() {
        return this.maxAngleOffset;
    }

    public void setMaxAngleOffset(float maxAngleOffset2) {
        if (maxAngleOffset2 < this.minAngleOffset) {
            throw new IllegalArgumentException("maxAngleOffset must not be < minAngleOffset");
        }
        this.maxAngleOffset = maxAngleOffset2;
        this.angleOffset = Math.min(this.angleOffset, maxAngleOffset2);
    }

    public boolean isVirtualChildEnabled() {
        return this.virtualChildEnabled;
    }

    public void setVirtualChildEnabled(boolean virtualChildEnabled2) {
        this.virtualChildEnabled = virtualChildEnabled2;
    }

    public Modifier getModifier() {
        return this.modifier;
    }

    public void setModifier(Modifier modifier2) {
        if (modifier2 == null) {
            throw new IllegalArgumentException("modifier must not be null");
        }
        this.modifier = modifier2;
        invalidateHierarchy();
    }

    public boolean isShrinkChildren() {
        return this.shrinkChildren;
    }

    public void setShrinkChildren(boolean shrinkChildren2) {
        this.shrinkChildren = shrinkChildren2;
    }

    public DragManager getDragManager() {
        return this.dragManager;
    }

    public interface Modifier {
        Vector2 anchorOffset(Vector2 vector2, Actor actor, int i, int i2, CircularGroup circularGroup);

        float angle(float f, Actor actor, int i, int i2, CircularGroup circularGroup);

        Vector2 localAnchor(Vector2 vector2, Actor actor, int i, int i2, CircularGroup circularGroup);

        float rotation(float f, Actor actor, int i, int i2, CircularGroup circularGroup);

        public static class Adapter implements Modifier {
            public float angle(float defaultAngle, Actor child, int index, int numChildren, CircularGroup group) {
                return defaultAngle;
            }

            public float rotation(float angle, Actor child, int index, int numChildren, CircularGroup group) {
                return angle;
            }

            public Vector2 anchorOffset(Vector2 anchorOffset, Actor child, int index, int numChildren, CircularGroup group) {
                return anchorOffset;
            }

            public Vector2 localAnchor(Vector2 localAnchor, Actor child, int index, int numChildren, CircularGroup group) {
                return localAnchor;
            }
        }
    }

    public class DragManager extends DragListener {
        private float deceleration;
        private boolean dragging;
        private boolean draggingActivated;
        private float maxAbsDelta;
        private float previousAngle;
        private float velocity;
        private boolean velocityActivated;

        private DragManager() {
            this.velocityActivated = true;
            this.draggingActivated = true;
            this.deceleration = 500.0f;
            this.maxAbsDelta = 350.0f;
        }

        public void dragStart(InputEvent event, float x, float y, int pointer) {
            if (this.draggingActivated) {
                this.velocity = Animation.CurveTimeline.LINEAR;
                this.dragging = true;
                this.previousAngle = angle(x, y);
            }
        }

        public void drag(InputEvent event, float x, float y, int pointer) {
            if (this.draggingActivated) {
                float currentAngle = angle(x, y);
                float delta = currentAngle - this.previousAngle;
                this.previousAngle = currentAngle;
                if (Math.abs(delta) <= this.maxAbsDelta) {
                    this.velocity = ((float) Gdx.graphics.getFramesPerSecond()) * delta;
                    float newAngleOffset = CircularGroup.this.angleOffset + delta;
                    float oldAngleOffset = CircularGroup.this.angleOffset;
                    setAngleOffset(newAngleOffset);
                    if (CircularGroup.this.angleOffset != oldAngleOffset) {
                        CircularGroup.this.invalidate();
                    }
                }
            }
        }

        public void dragStop(InputEvent event, float x, float y, int pointer) {
            if (this.draggingActivated) {
                this.dragging = false;
            }
        }

        public void act(float delta) {
            if (!this.dragging && this.velocity != Animation.CurveTimeline.LINEAR && this.velocityActivated) {
                setAngleOffset(CircularGroup.this.angleOffset + (this.velocity * delta));
                CircularGroup.this.invalidate();
                if (this.deceleration != Animation.CurveTimeline.LINEAR) {
                    this.velocity = net.dermetfan.utils.math.MathUtils.approachZero(this.velocity, this.deceleration * delta);
                }
            }
        }

        private float angle(float x, float y) {
            return CircularGroup.this.tmp.set(x, y).sub(CircularGroup.this.getWidth() / 2.0f, CircularGroup.this.getHeight() / 2.0f).angle();
        }

        private void setAngleOffset(float angleOffset) {
            if (CircularGroup.this.maxAngleOffset - CircularGroup.this.minAngleOffset == 360.0f) {
                float unused = CircularGroup.this.angleOffset = net.dermetfan.utils.math.MathUtils.normalize(angleOffset, CircularGroup.this.minAngleOffset, CircularGroup.this.maxAngleOffset);
            } else {
                CircularGroup.this.setAngleOffset(angleOffset);
            }
        }

        public boolean isVelocityActivated() {
            return this.velocityActivated;
        }

        public void setVelocityActivated(boolean velocityActivated2) {
            this.velocityActivated = velocityActivated2;
        }

        public boolean isDraggingActivated() {
            return this.draggingActivated;
        }

        public void setDraggingActivated(boolean draggingActivated2) {
            this.draggingActivated = draggingActivated2;
        }

        public float getVelocity() {
            return this.velocity;
        }

        public void setVelocity(float velocity2) {
            this.velocity = velocity2;
        }

        public float getDeceleration() {
            return this.deceleration;
        }

        public void setDeceleration(float deceleration2) {
            this.deceleration = deceleration2;
        }

        public float getMaxAbsDelta() {
            return this.maxAbsDelta;
        }

        public void setMaxAbsDelta(float maxAbsDelta2) {
            this.maxAbsDelta = maxAbsDelta2;
        }
    }
}
