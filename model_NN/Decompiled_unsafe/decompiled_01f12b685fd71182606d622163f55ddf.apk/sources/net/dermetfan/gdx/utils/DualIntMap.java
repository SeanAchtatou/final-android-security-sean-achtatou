package net.dermetfan.gdx.utils;

import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;

public class DualIntMap<V> {
    static final /* synthetic */ boolean $assertionsDisabled = (!DualIntMap.class.desiredAssertionStatus());
    private final IntMap<V> keyToValue;
    private final ObjectIntMap<V> valueToKey;

    public DualIntMap() {
        this.keyToValue = new IntMap<>();
        this.valueToKey = new ObjectIntMap<>();
    }

    public DualIntMap(int initialCapacity) {
        this.keyToValue = new IntMap<>(initialCapacity);
        this.valueToKey = new ObjectIntMap<>(initialCapacity);
    }

    public DualIntMap(int initialCapacity, float loadFactor) {
        this.keyToValue = new IntMap<>(initialCapacity, loadFactor);
        this.valueToKey = new ObjectIntMap<>(initialCapacity, loadFactor);
    }

    public DualIntMap(IntMap<V> map) {
        this.keyToValue = new IntMap<>(map);
        this.valueToKey = new ObjectIntMap<>(map.size);
        IntMap.Keys keys = map.keys();
        while (keys.hasNext) {
            int key = keys.next();
            this.valueToKey.put(map.get(key), key);
        }
    }

    public DualIntMap(DualIntMap<V> map) {
        this.keyToValue = new IntMap<>(map.keyToValue);
        this.valueToKey = new ObjectIntMap<>(map.valueToKey);
    }

    public void put(int key, V value) {
        this.keyToValue.put(key, value);
        this.valueToKey.put(value, key);
    }

    public int getKey(V value, int defaultKey) {
        return this.valueToKey.get(value, defaultKey);
    }

    public V getValue(int key) {
        return this.keyToValue.get(key);
    }

    public V removeKey(int key) {
        V value = this.keyToValue.remove(key);
        if (value != null) {
            int removed = this.valueToKey.remove(value, key);
            if (!$assertionsDisabled && removed != key) {
                throw new AssertionError();
            }
        }
        return value;
    }

    public int removeValue(V value, int defaultKey) {
        int key = this.valueToKey.remove(value, defaultKey);
        this.keyToValue.remove(key);
        return key;
    }

    public IntMap<V> getKeyToValue() {
        return this.keyToValue;
    }

    public ObjectIntMap<V> getValueToKey() {
        return this.valueToKey;
    }
}
