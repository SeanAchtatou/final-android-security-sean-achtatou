package net.dermetfan.gdx.utils;

import com.badlogic.gdx.utils.ObjectMap;

public class DualObjectMap<K, V> {
    static final /* synthetic */ boolean $assertionsDisabled = (!DualObjectMap.class.desiredAssertionStatus());
    private final ObjectMap<K, V> keyToValue;
    private final ObjectMap<V, K> valueToKey;

    public DualObjectMap() {
        this.keyToValue = new ObjectMap<>();
        this.valueToKey = new ObjectMap<>();
    }

    public DualObjectMap(int initialCapacity) {
        this.keyToValue = new ObjectMap<>(initialCapacity);
        this.valueToKey = new ObjectMap<>(initialCapacity);
    }

    public DualObjectMap(int initialCapacity, float loadFactor) {
        this.keyToValue = new ObjectMap<>(initialCapacity, loadFactor);
        this.valueToKey = new ObjectMap<>(initialCapacity, loadFactor);
    }

    public DualObjectMap(ObjectMap<K, V> map) {
        this.keyToValue = new ObjectMap<>(map);
        this.valueToKey = new ObjectMap<>(map.size);
        ObjectMap.Keys<K> it = map.keys().iterator();
        while (it.hasNext()) {
            K key = it.next();
            this.valueToKey.put(map.get(key), key);
        }
    }

    public DualObjectMap(DualObjectMap<K, V> map) {
        this.keyToValue = new ObjectMap<>(map.keyToValue);
        this.valueToKey = new ObjectMap<>(map.valueToKey);
    }

    public void put(K key, V value) {
        this.keyToValue.put(key, value);
        this.valueToKey.put(value, key);
    }

    public K getKey(V value) {
        K key = this.valueToKey.get(value);
        if ($assertionsDisabled || key != null) {
            return key;
        }
        throw new AssertionError();
    }

    public V getValue(K key) {
        V value = this.keyToValue.get(key);
        if ($assertionsDisabled || value != null) {
            return value;
        }
        throw new AssertionError();
    }

    public V removeKey(K key) {
        V value = this.keyToValue.remove(key);
        if ($assertionsDisabled || value != null) {
            K removed = this.valueToKey.remove(value);
            if ($assertionsDisabled || removed != null) {
                return value;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public K removeValue(V value) {
        K key = this.valueToKey.remove(value);
        if ($assertionsDisabled || key != null) {
            V oldObject = this.keyToValue.remove(key);
            if ($assertionsDisabled || oldObject != null) {
                return key;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public ObjectMap<K, V> getKeyToValue() {
        return this.keyToValue;
    }

    public ObjectMap<V, K> getValueToKey() {
        return this.valueToKey;
    }
}
