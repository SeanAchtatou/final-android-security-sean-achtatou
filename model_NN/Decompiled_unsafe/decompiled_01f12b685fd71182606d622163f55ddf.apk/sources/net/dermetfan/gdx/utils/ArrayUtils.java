package net.dermetfan.gdx.utils;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;

public class ArrayUtils extends net.dermetfan.utils.ArrayUtils {
    public static <T> T wrapIndex(int index, Array array) {
        return array.get(wrapIndex(index, array.size));
    }

    public static <T> Array<T> select(Array<T> items, int offset, int length, int start, int everyXth, Array<T> dest) {
        int outputLength = selectCount(offset, length, start, everyXth);
        if (dest == null) {
            dest = new Array<>(outputLength);
        }
        dest.clear();
        dest.ensureCapacity(outputLength);
        if (start + length > items.size) {
            throw new ArrayIndexOutOfBoundsException((start + length) - 1);
        }
        dest.size = outputLength;
        select(items.items, offset, length, start, everyXth, dest.items);
        return dest;
    }

    public static <T> Array<T> select(Array array, int start, int everyXth, Array array2) {
        return select(array, 0, array.size, start, everyXth, array2);
    }

    public static <T> Array<T> select(Array array, int everyXth, Array array2) {
        return select(array, 0, everyXth, array2);
    }

    public static <T> Array<T> select(Array<T> items, int offset, int length, int start, int everyXth) {
        return select(items, offset, length, start, everyXth, (Array) null);
    }

    public static <T> Array<T> select(Array array, int start, int everyXth) {
        return select(array, 0, array.size, start, everyXth);
    }

    public static <T> Array<T> select(Array<T> items, int everyXth) {
        return select(items, 0, everyXth);
    }

    public static <T> Array<T> select(Array<T> items, int[] indices, int indicesOffset, int indicesLength, Array<T> dest) {
        if (dest == null) {
            dest = new Array<>(true, indicesLength, items.items.getClass().getComponentType());
        }
        dest.clear();
        dest.ensureCapacity(indicesLength);
        if (indicesLength > items.size) {
            throw new ArrayIndexOutOfBoundsException(indicesLength - 1);
        }
        dest.size = indicesLength;
        select(items.items, indices, indicesOffset, indicesLength, dest.items, 0);
        return dest;
    }

    public static <T> Array<T> select(Array array, int[] indices, Array array2) {
        return select(array, indices, 0, indices.length, array2);
    }

    public static <T> Array<T> select(Array<T> items, int[] indices) {
        return select(items, indices, (Array) null);
    }

    public static <T> Array<T> select(Array array, IntArray indices, Array array2) {
        return select(array, indices.items, 0, indices.size, array2);
    }

    public static <T> Array<T> select(Array<T> items, IntArray indices) {
        return select(items, indices, (Array) null);
    }

    public static <T> Array<T> skipselect(Array<T> elements, IntArray skips, IntArray repeatSkips, Array<T> output) {
        boolean normal;
        int rsi;
        int skip;
        int i;
        boolean repeat = true;
        if (skips == null || skips.size <= 0) {
            normal = false;
        } else {
            normal = true;
        }
        if (repeatSkips == null || repeatSkips.size <= 0) {
            repeat = false;
        }
        if (!normal && !repeat) {
            return elements;
        }
        int span = 0;
        int rsi2 = 0;
        int length = 0;
        while (length < elements.size) {
            if (normal && length < skips.size) {
                skip = skips.get(length);
            } else if (repeat) {
                if (rsi2 >= repeatSkips.size) {
                    rsi2 = 0;
                    i = 0;
                } else {
                    i = rsi2;
                    rsi2++;
                }
                skip = repeatSkips.get(i);
            } else {
                skip = (Integer.MAX_VALUE - span) - 1;
            }
            if (span + skip + 1 > elements.size) {
                break;
            }
            span += skip + 1;
            length++;
        }
        if (length == elements.size) {
            return elements;
        }
        if (output == null) {
            output = new Array<>(length);
        }
        output.clear();
        output.ensureCapacity(length - output.size);
        int si = 0;
        int ei = 0;
        int rsi3 = 0;
        while (true) {
            if (si >= length) {
                break;
            }
            int ei2 = ei + 1;
            output.add(elements.get(ei));
            si++;
            if (si >= skips.size) {
                if (!repeat) {
                    break;
                }
                if (rsi3 >= repeatSkips.size) {
                    rsi = 0;
                    rsi3 = 0;
                } else {
                    rsi = rsi3 + 1;
                }
                ei = ei2 + repeatSkips.get(rsi3);
                rsi3 = rsi;
            } else {
                ei = ei2 + skips.get(si);
            }
        }
        return output;
    }

    public static <T> Array<T> skipselect(Array<T> elements, IntArray skips, IntArray repeatSkips) {
        return skipselect(elements, skips, repeatSkips, (Array) null);
    }

    public static <T> Array<T> skipselect(Array<T> elements, int firstSkip, int skips, Array<T> output) {
        int span = firstSkip;
        int length = 0;
        while (true) {
            if (length < elements.size) {
                if (span + skips + 1 > elements.size) {
                    length++;
                    break;
                }
                span += skips + 1;
                length++;
            } else {
                break;
            }
        }
        if (output == null) {
            output = new Array<>(length);
        }
        output.clear();
        output.ensureCapacity(length - output.size);
        int si = 0;
        int ei = firstSkip;
        while (si < length) {
            output.add(elements.get(ei));
            si++;
            ei += skips + 1;
        }
        return output;
    }

    public static <T> Array<T> skipselect(Array<T> elements, int firstSkip, int skips) {
        return skipselect(elements, firstSkip, skips, (Array) null);
    }

    public static <T> boolean equalsAny(T obj, Array<T> array) {
        return equalsAny(obj, array.items, 0, array.size);
    }

    public static int wrapIndex(int index, IntArray array) {
        return array.get(wrapIndex(index, array.size));
    }

    public static IntArray select(IntArray items, int offset, int length, int start, int everyXth, IntArray dest) {
        int outputLength = selectCount(offset, length, start, everyXth);
        if (dest == null) {
            dest = new IntArray(outputLength);
        }
        dest.clear();
        dest.ensureCapacity(outputLength);
        if (start + length > items.size) {
            throw new ArrayIndexOutOfBoundsException((start + length) - 1);
        }
        dest.size = outputLength;
        select(items.items, offset, length, start, everyXth, dest.items);
        return dest;
    }

    public static IntArray select(IntArray items, int start, int everyXth, IntArray dest) {
        return select(items, 0, items.size, start, everyXth, dest);
    }

    public static IntArray select(IntArray items, int everyXth, IntArray dest) {
        return select(items, 0, everyXth, dest);
    }

    public static IntArray select(IntArray items, int offset, int length, int start, int everyXth) {
        return select(items, offset, length, start, everyXth, (IntArray) null);
    }

    public static IntArray select(IntArray items, int start, int everyXth) {
        return select(items, 0, items.size, start, everyXth);
    }

    public static IntArray select(IntArray items, int everyXth) {
        return select(items, 0, everyXth);
    }

    public static IntArray select(IntArray items, int[] indices, int indicesOffset, int indicesLength, IntArray dest) {
        if (dest == null) {
            dest = new IntArray(true, indicesLength);
        }
        dest.clear();
        dest.ensureCapacity(indicesLength);
        if (indicesLength > items.size) {
            throw new ArrayIndexOutOfBoundsException(indicesLength - 1);
        }
        dest.size = indicesLength;
        select(items.items, indices, indicesOffset, indicesLength, dest.items, 0);
        return dest;
    }

    public static IntArray select(IntArray items, int[] indices, IntArray dest) {
        return select(items, indices, 0, indices.length, dest);
    }

    public static IntArray select(IntArray items, int[] indices) {
        return select(items, indices, (IntArray) null);
    }

    public static IntArray select(IntArray items, IntArray indices, IntArray dest) {
        return select(items, indices.items, 0, indices.size, dest);
    }

    public static IntArray select(IntArray items, IntArray indices) {
        return select(items, indices, (IntArray) null);
    }

    public static IntArray skipselect(IntArray elements, IntArray skips, IntArray repeatSkips, IntArray output) {
        boolean normal;
        int rsi;
        int skip;
        int i;
        boolean repeat = true;
        if (skips == null || skips.size <= 0) {
            normal = false;
        } else {
            normal = true;
        }
        if (repeatSkips == null || repeatSkips.size <= 0) {
            repeat = false;
        }
        if (!normal && !repeat) {
            return elements;
        }
        int span = 0;
        int rsi2 = 0;
        int length = 0;
        while (length < elements.size) {
            if (normal && length < skips.size) {
                skip = skips.get(length);
            } else if (repeat) {
                if (rsi2 >= repeatSkips.size) {
                    rsi2 = 0;
                    i = 0;
                } else {
                    i = rsi2;
                    rsi2++;
                }
                skip = repeatSkips.get(i);
            } else {
                skip = (Integer.MAX_VALUE - span) - 1;
            }
            if (span + skip + 1 > elements.size) {
                break;
            }
            span += skip + 1;
            length++;
        }
        if (length == elements.size) {
            return elements;
        }
        if (output == null) {
            output = new IntArray(length);
        }
        output.clear();
        output.ensureCapacity(length - output.size);
        int si = 0;
        int ei = 0;
        int rsi3 = 0;
        while (true) {
            if (si >= length) {
                break;
            }
            int ei2 = ei + 1;
            output.add(elements.get(ei));
            si++;
            if (si >= skips.size) {
                if (!repeat) {
                    break;
                }
                if (rsi3 >= repeatSkips.size) {
                    rsi = 0;
                    rsi3 = 0;
                } else {
                    rsi = rsi3 + 1;
                }
                ei = ei2 + repeatSkips.get(rsi3);
                rsi3 = rsi;
            } else {
                ei = ei2 + skips.get(si);
            }
        }
        return output;
    }

    public static IntArray skipselect(IntArray elements, IntArray skips, IntArray repeatSkips) {
        return skipselect(elements, skips, repeatSkips, (IntArray) null);
    }

    public static IntArray skipselect(IntArray elements, int firstSkip, int skips, IntArray output) {
        int span = firstSkip;
        int length = 0;
        while (true) {
            if (length < elements.size) {
                if (span + skips + 1 > elements.size) {
                    length++;
                    break;
                }
                span += skips + 1;
                length++;
            } else {
                break;
            }
        }
        if (output == null) {
            output = new IntArray(length);
        }
        output.clear();
        output.ensureCapacity(length - output.size);
        int si = 0;
        int ei = firstSkip;
        while (si < length) {
            output.add(elements.get(ei));
            si++;
            ei += skips + 1;
        }
        return output;
    }

    public static IntArray skipselect(IntArray elements, int firstSkip, int skips) {
        return skipselect(elements, firstSkip, skips, (IntArray) null);
    }

    public static float wrapIndex(int index, FloatArray array) {
        return array.get(wrapIndex(index, array.size));
    }

    public static FloatArray select(FloatArray items, int offset, int length, int start, int everyXth, FloatArray dest) {
        int outputLength = selectCount(offset, length, start, everyXth);
        if (dest == null) {
            dest = new FloatArray(outputLength);
        }
        dest.clear();
        dest.ensureCapacity(outputLength);
        if (start + length > items.size) {
            throw new ArrayIndexOutOfBoundsException((start + length) - 1);
        }
        dest.size = outputLength;
        select(items.items, offset, length, start, everyXth, dest.items);
        return dest;
    }

    public static FloatArray select(FloatArray items, int start, int everyXth, FloatArray dest) {
        return select(items, 0, items.size, start, everyXth, dest);
    }

    public static FloatArray select(FloatArray items, int everyXth, FloatArray dest) {
        return select(items, 0, everyXth, dest);
    }

    public static FloatArray select(FloatArray items, int offset, int length, int start, int everyXth) {
        return select(items, offset, length, start, everyXth, (FloatArray) null);
    }

    public static FloatArray select(FloatArray items, int start, int everyXth) {
        return select(items, 0, items.size, start, everyXth);
    }

    public static FloatArray select(FloatArray items, int everyXth) {
        return select(items, 0, everyXth);
    }

    public static FloatArray select(FloatArray items, int[] indices, int indicesOffset, int indicesLength, FloatArray dest) {
        if (dest == null) {
            dest = new FloatArray(true, indicesLength);
        }
        dest.clear();
        dest.ensureCapacity(indicesLength);
        if (indicesLength > items.size) {
            throw new ArrayIndexOutOfBoundsException(indicesLength - 1);
        }
        dest.size = indicesLength;
        select(items.items, indices, indicesOffset, indicesLength, dest.items, 0);
        return dest;
    }

    public static FloatArray select(FloatArray items, int[] indices, FloatArray dest) {
        return select(items, indices, 0, indices.length, dest);
    }

    public static FloatArray select(FloatArray items, int[] indices) {
        return select(items, indices, (FloatArray) null);
    }

    public static FloatArray select(FloatArray items, IntArray indices, FloatArray dest) {
        return select(items, indices.items, 0, indices.size, dest);
    }

    public static FloatArray select(FloatArray items, IntArray indices) {
        return select(items, indices, (FloatArray) null);
    }

    public static FloatArray skipselect(FloatArray elements, IntArray skips, IntArray repeatSkips, FloatArray output) {
        boolean normal;
        int rsi;
        int skip;
        int i;
        boolean repeat = true;
        if (skips == null || skips.size <= 0) {
            normal = false;
        } else {
            normal = true;
        }
        if (repeatSkips == null || repeatSkips.size <= 0) {
            repeat = false;
        }
        if (!normal && !repeat) {
            return elements;
        }
        int span = 0;
        int rsi2 = 0;
        int length = 0;
        while (length < elements.size) {
            if (normal && length < skips.size) {
                skip = skips.get(length);
            } else if (repeat) {
                if (rsi2 >= repeatSkips.size) {
                    rsi2 = 0;
                    i = 0;
                } else {
                    i = rsi2;
                    rsi2++;
                }
                skip = repeatSkips.get(i);
            } else {
                skip = (Integer.MAX_VALUE - span) - 1;
            }
            if (span + skip + 1 > elements.size) {
                break;
            }
            span += skip + 1;
            length++;
        }
        if (length == elements.size) {
            return elements;
        }
        if (output == null) {
            output = new FloatArray(length);
        }
        output.clear();
        output.ensureCapacity(length - output.size);
        int si = 0;
        int ei = 0;
        int rsi3 = 0;
        while (true) {
            if (si >= length) {
                break;
            }
            int ei2 = ei + 1;
            output.add(elements.get(ei));
            si++;
            if (si >= skips.size) {
                if (!repeat) {
                    break;
                }
                if (rsi3 >= repeatSkips.size) {
                    rsi = 0;
                    rsi3 = 0;
                } else {
                    rsi = rsi3 + 1;
                }
                ei = ei2 + repeatSkips.get(rsi3);
                rsi3 = rsi;
            } else {
                ei = ei2 + skips.get(si);
            }
        }
        return output;
    }

    public static FloatArray skipselect(FloatArray elements, IntArray skips, IntArray repeatSkips) {
        return skipselect(elements, skips, repeatSkips, (FloatArray) null);
    }

    public static FloatArray skipselect(FloatArray elements, int firstSkip, int skips, FloatArray output) {
        int span = firstSkip;
        int length = 0;
        while (true) {
            if (length < elements.size) {
                if (span + skips + 1 > elements.size) {
                    length++;
                    break;
                }
                span += skips + 1;
                length++;
            } else {
                break;
            }
        }
        if (output == null) {
            output = new FloatArray(length);
        }
        output.clear();
        output.ensureCapacity(length - output.size);
        int si = 0;
        int ei = firstSkip;
        while (si < length) {
            output.add(elements.get(ei));
            si++;
            ei += skips + 1;
        }
        return output;
    }

    public static FloatArray skipselect(FloatArray elements, int firstSkip, int skips) {
        return skipselect(elements, firstSkip, skips, (FloatArray) null);
    }
}
