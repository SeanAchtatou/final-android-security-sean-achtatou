package net.dermetfan.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Comparator;
import java.util.Iterator;
import net.dermetfan.gdx.math.GeometryUtils;
import net.dermetfan.gdx.physics.box2d.Box2DUtils;
import net.dermetfan.utils.Function;

public class Box2DPolygonSprite extends PolygonSprite {
    public static final Function<Box2DPolygonSprite, Object> defaultUserDataAccessor = new Function<Box2DPolygonSprite, Object>() {
        public Box2DPolygonSprite apply(Object userData) {
            if (userData instanceof Box2DPolygonSprite) {
                return (Box2DPolygonSprite) userData;
            }
            return null;
        }
    };
    private static Function<Box2DPolygonSprite, Object> userDataAccessor = defaultUserDataAccessor;
    private static final Vector2 vec2 = new Vector2();
    private static Comparator<Box2DPolygonSprite> zComparator = new Comparator<Box2DPolygonSprite>() {
        public int compare(Box2DPolygonSprite s1, Box2DPolygonSprite s2) {
            if (s1.zIndex - s2.zIndex > Animation.CurveTimeline.LINEAR) {
                return 1;
            }
            return s1.zIndex - s2.zIndex < Animation.CurveTimeline.LINEAR ? -1 : 0;
        }
    };
    private boolean adjustHeight = true;
    private boolean adjustToPolygon = true;
    private boolean adjustWidth = true;
    private boolean useOriginX;
    private boolean useOriginY;
    /* access modifiers changed from: private */
    public float zIndex;

    public static void draw(Batch batch, World world) {
        draw(batch, world, false);
    }

    public static void draw(Batch batch, World world, boolean sortByZ) {
        Array<Body> tmpBodies = (Array) Pools.obtain(Array.class);
        world.getBodies(tmpBodies);
        if (sortByZ) {
            ObjectMap<Box2DPolygonSprite, Object> tmpZMap = (ObjectMap) Pools.obtain(ObjectMap.class);
            tmpZMap.clear();
            Iterator it = tmpBodies.iterator();
            while (it.hasNext()) {
                Body body = (Body) it.next();
                Box2DPolygonSprite tmpBox2DPolygonSprite = userDataAccessor.apply(body.getUserData());
                if (tmpBox2DPolygonSprite != null) {
                    tmpZMap.put(tmpBox2DPolygonSprite, body);
                }
                Iterator<Fixture> it2 = body.getFixtureList().iterator();
                while (it2.hasNext()) {
                    Fixture fixture = it2.next();
                    Box2DPolygonSprite tmpBox2DPolygonSprite2 = userDataAccessor.apply(fixture.getUserData());
                    if (tmpBox2DPolygonSprite2 != null) {
                        tmpZMap.put(tmpBox2DPolygonSprite2, fixture);
                    }
                }
            }
            Array<Box2DPolygonSprite> tmpKeys = (Array) Pools.obtain(Array.class);
            Iterator<Box2DPolygonSprite> keys = tmpZMap.keys();
            while (keys.hasNext()) {
                tmpKeys.add(keys.next());
            }
            tmpKeys.sort(zComparator);
            Iterator it3 = tmpKeys.iterator();
            while (it3.hasNext()) {
                Box2DPolygonSprite key = (Box2DPolygonSprite) it3.next();
                Object value = tmpZMap.get(key);
                if (value instanceof Body) {
                    key.draw(batch, (Body) value);
                } else {
                    key.draw(batch, (Fixture) value);
                }
            }
            tmpKeys.clear();
            tmpZMap.clear();
            Pools.free(tmpKeys);
            Pools.free(tmpZMap);
        } else {
            Iterator it4 = tmpBodies.iterator();
            while (it4.hasNext()) {
                Body body2 = (Body) it4.next();
                Box2DPolygonSprite tmpBox2DPolygonSprite3 = userDataAccessor.apply(body2.getUserData());
                if (tmpBox2DPolygonSprite3 != null) {
                    tmpBox2DPolygonSprite3.draw(batch, body2);
                }
                Iterator<Fixture> it5 = body2.getFixtureList().iterator();
                while (it5.hasNext()) {
                    Fixture fixture2 = it5.next();
                    Box2DPolygonSprite tmpBox2DPolygonSprite4 = userDataAccessor.apply(fixture2.getUserData());
                    if (tmpBox2DPolygonSprite4 != null) {
                        tmpBox2DPolygonSprite4.draw(batch, fixture2);
                    }
                }
            }
        }
        tmpBodies.clear();
        Pools.free(tmpBodies);
    }

    public static Comparator<Box2DPolygonSprite> getZComparator() {
        return zComparator;
    }

    public static void setZComparator(Comparator<Box2DPolygonSprite> zComparator2) {
        if (zComparator2 == null) {
            throw new IllegalArgumentException("zComparator must not be null");
        }
        zComparator = zComparator2;
    }

    public static Function<Box2DPolygonSprite, ?> getUserDataAccessor() {
        return userDataAccessor;
    }

    public static void setUserDataAccessor(Function<Box2DPolygonSprite, Object> userDataAccessor2) {
        if (userDataAccessor2 == null) {
            userDataAccessor2 = defaultUserDataAccessor;
        }
        userDataAccessor = userDataAccessor2;
    }

    public Box2DPolygonSprite(PolygonRegion region) {
        super(region);
    }

    public Box2DPolygonSprite(PolygonSprite sprite) {
        super(sprite);
    }

    public void draw(Batch batch, Fixture fixture) {
        vec2.set(Box2DUtils.position(fixture));
        draw(batch, vec2.x, vec2.y, Box2DUtils.width(fixture), Box2DUtils.height(fixture), fixture.getBody().getAngle());
    }

    public void draw(Batch batch, Body body) {
        float width = Box2DUtils.width(body);
        float height = Box2DUtils.height(body);
        vec2.set(Box2DUtils.minX(body) + (width / 2.0f), Box2DUtils.minY(body) + (height / 2.0f));
        vec2.set(body.getWorldPoint(vec2));
        draw(batch, vec2.x, vec2.y, width, height, body.getAngle());
    }

    public void draw(Batch batch, float box2dX, float box2dY, float box2dWidth, float box2dHeight, float box2dRotation) {
        if (batch instanceof PolygonSpriteBatch) {
            draw((PolygonSpriteBatch) batch, box2dX, box2dY, box2dWidth, box2dHeight, box2dRotation);
            return;
        }
        batch.setColor(getColor());
        batch.draw(getRegion().getRegion(), getX() + (box2dX - (box2dWidth / 2.0f)), getY() + (box2dY - (box2dHeight / 2.0f)), this.useOriginX ? getOriginX() : box2dWidth / 2.0f, this.useOriginY ? getOriginY() : box2dHeight / 2.0f, this.adjustWidth ? box2dWidth : getWidth(), this.adjustHeight ? box2dHeight : getHeight(), getScaleX(), getScaleY(), getRotation() + (57.295776f * box2dRotation));
    }

    public void draw(PolygonSpriteBatch batch, float box2dX, float box2dY, float box2dWidth, float box2dHeight, float box2dRotation) {
        float x;
        float y;
        float originX;
        float originY;
        float width;
        float height;
        float f;
        batch.setColor(getColor());
        if (this.adjustToPolygon) {
            FloatArray vertices = (FloatArray) Pools.obtain(FloatArray.class);
            vertices.clear();
            vertices.addAll(getRegion().getVertices());
            float polygonWidth = GeometryUtils.width(vertices);
            float polygonHeight = GeometryUtils.height(vertices);
            float polygonWidthRatio = ((float) getRegion().getRegion().getRegionWidth()) / polygonWidth;
            float polygonHeightRatio = ((float) getRegion().getRegion().getRegionHeight()) / polygonHeight;
            width = box2dWidth * polygonWidthRatio;
            height = box2dHeight * polygonHeightRatio;
            float polygonX = GeometryUtils.minX(vertices);
            float polygonY = GeometryUtils.minY(vertices);
            float offsetX = width / (((float) getRegion().getRegion().getRegionWidth()) / polygonX);
            float offsetY = height / (((float) getRegion().getRegion().getRegionHeight()) / polygonY);
            x = (box2dX - offsetX) - ((width / 2.0f) / polygonWidthRatio);
            y = (box2dY - offsetY) - ((height / 2.0f) / polygonHeightRatio);
            originX = offsetX + (box2dWidth / 2.0f);
            originY = offsetY + (box2dHeight / 2.0f);
        } else {
            x = box2dX - (box2dWidth / 2.0f);
            y = box2dY - (box2dHeight / 2.0f);
            originX = box2dWidth / 2.0f;
            originY = box2dHeight / 2.0f;
            width = box2dWidth;
            height = box2dHeight;
        }
        PolygonRegion region = getRegion();
        float x2 = x + getX();
        float y2 = y + getY();
        if (this.useOriginX) {
            f = getOriginX();
        } else {
            f = originX;
        }
        batch.draw(region, x2, y2, f, this.useOriginY ? getOriginY() : originY, this.adjustWidth ? width : getWidth(), this.adjustHeight ? height : getHeight(), getScaleX(), getScaleY(), getRotation() + (57.295776f * box2dRotation));
    }

    public float getZIndex() {
        return this.zIndex;
    }

    public void setZIndex(float zIndex2) {
        this.zIndex = zIndex2;
    }

    public boolean isAdjustWidth() {
        return this.adjustWidth;
    }

    public void setAdjustWidth(boolean adjustWidth2) {
        this.adjustWidth = adjustWidth2;
    }

    public boolean isAdjustHeight() {
        return this.adjustHeight;
    }

    public void setAdjustHeight(boolean adjustHeight2) {
        this.adjustHeight = adjustHeight2;
    }

    public void setAdjustSize(boolean adjustSize) {
        this.adjustHeight = adjustSize;
        this.adjustWidth = adjustSize;
    }

    public boolean isAdjustToPolygon() {
        return this.adjustToPolygon;
    }

    public void setAdjustToPolygon(boolean adjustToPolygon2) {
        this.adjustToPolygon = adjustToPolygon2;
    }

    public boolean isUseOriginX() {
        return this.useOriginX;
    }

    public void setUseOriginX(boolean useOriginX2) {
        this.useOriginX = useOriginX2;
    }

    public boolean isUseOriginY() {
        return this.useOriginY;
    }

    public void setUseOriginY(boolean useOriginY2) {
        this.useOriginY = useOriginY2;
    }

    public void setUseOrigin(boolean useOrigin) {
        this.useOriginY = useOrigin;
        this.useOriginX = useOrigin;
    }

    public void setWidth(float width) {
        setSize(width, getHeight());
    }

    public void setHeight(float height) {
        setSize(getWidth(), height);
    }
}
