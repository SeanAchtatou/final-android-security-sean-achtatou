package net.dermetfan.gdx.graphics.g2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class AnimatedBox2DSprite extends Box2DSprite {
    private AnimatedSprite animatedSprite;

    public AnimatedBox2DSprite(AnimatedSprite animatedSprite2) {
        super((Sprite) animatedSprite2);
        this.animatedSprite = animatedSprite2;
    }

    public void draw(Batch batch, float box2dX, float box2dY, float box2dWidth, float box2dHeight, float box2dRotation) {
        if (this.animatedSprite.isAutoUpdate()) {
            update();
        }
        super.draw(batch, box2dX, box2dY, box2dWidth, box2dHeight, box2dRotation);
    }

    public void update() {
        update(Gdx.graphics.getDeltaTime());
    }

    public void update(float delta) {
        this.animatedSprite.update(delta);
        setRegion(this.animatedSprite);
        if (this.animatedSprite.isUseFrameRegionSize()) {
            setSize(this.animatedSprite.getWidth(), this.animatedSprite.getHeight());
        }
    }

    public AnimatedSprite getAnimatedSprite() {
        return this.animatedSprite;
    }

    public void setAnimatedSprite(AnimatedSprite animatedSprite2) {
        this.animatedSprite = animatedSprite2;
    }

    public Animation getAnimation() {
        return this.animatedSprite.getAnimation();
    }

    public void setAnimation(Animation animation) {
        this.animatedSprite.setAnimation(animation);
    }

    public boolean isUseFrameRegionSize() {
        return this.animatedSprite.isUseFrameRegionSize();
    }

    public void setUseFrameRegionSize(boolean useFrameRegionSize) {
        this.animatedSprite.setUseFrameRegionSize(useFrameRegionSize);
    }

    public void flipFrames(float startTime, float endTime, boolean flipX, boolean flipY, boolean set) {
        this.animatedSprite.flipFrames(startTime, endTime, flipX, flipY, set);
    }

    public boolean isAnimationFinished() {
        return this.animatedSprite.isAnimationFinished();
    }

    public void stop() {
        this.animatedSprite.stop();
    }

    public boolean isAutoUpdate() {
        return this.animatedSprite.isAutoUpdate();
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.animatedSprite.setAutoUpdate(autoUpdate);
    }

    public void flipFrames(boolean flipX, boolean flipY) {
        this.animatedSprite.flipFrames(flipX, flipY);
    }

    public boolean isCenterFrames() {
        return this.animatedSprite.isCenterFrames();
    }

    public void setCenterFrames(boolean centerFrames) {
        this.animatedSprite.setCenterFrames(centerFrames);
    }

    public void flipFrames(float startTime, float endTime, boolean flipX, boolean flipY) {
        this.animatedSprite.flipFrames(startTime, endTime, flipX, flipY);
    }

    public void play() {
        this.animatedSprite.play();
    }

    public float getTime() {
        return this.animatedSprite.getTime();
    }

    public void setTime(float time) {
        this.animatedSprite.setTime(time);
    }

    public void flipFrames(boolean flipX, boolean flipY, boolean set) {
        this.animatedSprite.flipFrames(flipX, flipY, set);
    }

    public void pause() {
        this.animatedSprite.pause();
    }

    public boolean isPlaying() {
        return this.animatedSprite.isPlaying();
    }

    public void setPlaying(boolean playing) {
        this.animatedSprite.setPlaying(playing);
    }
}
