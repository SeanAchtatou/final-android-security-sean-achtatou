package net.dermetfan.gdx.graphics.g2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kbz.esotericsoftware.spine.Animation;

public class AnimatedSprite extends Sprite {
    private Animation animation;
    private boolean autoUpdate = true;
    private boolean centerFrames;
    private float oldHeight;
    private float oldOriginX;
    private float oldOriginY;
    private float oldWidth;
    private float oldX;
    private float oldY;
    private boolean playing = true;
    private float time;
    private boolean useFrameRegionSize;

    public AnimatedSprite(Animation animation2) {
        super(animation2.getKeyFrame(Animation.CurveTimeline.LINEAR));
        this.animation = animation2;
    }

    public void update() {
        update(Gdx.graphics.getDeltaTime());
    }

    public void update(float delta) {
        this.oldX = getX();
        this.oldY = getY();
        this.oldWidth = getWidth();
        this.oldHeight = getHeight();
        this.oldOriginX = getOriginX();
        this.oldOriginY = getOriginY();
        if (this.playing) {
            com.badlogic.gdx.graphics.g2d.Animation animation2 = this.animation;
            float f = this.time + delta;
            this.time = f;
            setRegion(animation2.getKeyFrame(f));
            if (this.useFrameRegionSize) {
                setSize((float) getRegionWidth(), (float) getRegionHeight());
            }
        }
    }

    public void draw(Batch batch) {
        if (this.autoUpdate) {
            update();
        }
        boolean centerFramesEnabled = this.centerFrames && this.useFrameRegionSize;
        if (centerFramesEnabled) {
            float differenceX = this.oldWidth - ((float) getRegionWidth());
            float differenceY = this.oldHeight - ((float) getRegionHeight());
            setOrigin(this.oldOriginX - (differenceX / 2.0f), this.oldOriginY - (differenceY / 2.0f));
            setBounds(this.oldX + (differenceX / 2.0f), this.oldY + (differenceY / 2.0f), this.oldWidth - differenceX, this.oldHeight - differenceY);
        }
        super.draw(batch);
        if (centerFramesEnabled) {
            setOrigin(this.oldOriginX, this.oldOriginY);
            setBounds(this.oldX, this.oldY, this.oldWidth, this.oldHeight);
        }
    }

    public void flipFrames(boolean flipX, boolean flipY) {
        flipFrames(flipX, flipY, false);
    }

    public void flipFrames(boolean flipX, boolean flipY, boolean set) {
        flipFrames(Animation.CurveTimeline.LINEAR, this.animation.getAnimationDuration(), flipX, flipY, set);
    }

    public void flipFrames(float startTime, float endTime, boolean flipX, boolean flipY) {
        flipFrames(startTime, endTime, flipX, flipY, false);
    }

    public void flipFrames(float startTime, float endTime, boolean flipX, boolean flipY, boolean set) {
        boolean z;
        float t = startTime;
        while (t < endTime) {
            TextureRegion frame = this.animation.getKeyFrame(t);
            if (set) {
                z = flipX && !frame.isFlipX();
            } else {
                z = flipX;
            }
            frame.flip(z, set ? flipY && !frame.isFlipY() : flipY);
            t += this.animation.getFrameDuration();
        }
    }

    public void play() {
        this.playing = true;
    }

    public void pause() {
        this.playing = false;
    }

    public void stop() {
        this.playing = false;
        this.time = Animation.CurveTimeline.LINEAR;
    }

    public void setTime(float time2) {
        this.time = time2;
    }

    public float getTime() {
        return this.time;
    }

    public com.badlogic.gdx.graphics.g2d.Animation getAnimation() {
        return this.animation;
    }

    public void setAnimation(com.badlogic.gdx.graphics.g2d.Animation animation2) {
        this.animation = animation2;
    }

    public boolean isPlaying() {
        return this.playing;
    }

    public void setPlaying(boolean playing2) {
        this.playing = playing2;
    }

    public boolean isAnimationFinished() {
        return this.animation.isAnimationFinished(this.time);
    }

    public boolean isAutoUpdate() {
        return this.autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate2) {
        this.autoUpdate = autoUpdate2;
    }

    public boolean isUseFrameRegionSize() {
        return this.useFrameRegionSize;
    }

    public void setUseFrameRegionSize(boolean useFrameRegionSize2) {
        this.useFrameRegionSize = useFrameRegionSize2;
    }

    public boolean isCenterFrames() {
        return this.centerFrames;
    }

    public void setCenterFrames(boolean centerFrames2) {
        this.centerFrames = centerFrames2;
    }
}
