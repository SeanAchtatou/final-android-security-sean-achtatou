package net.dermetfan.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Comparator;
import java.util.Iterator;
import net.dermetfan.gdx.physics.box2d.Box2DUtils;
import net.dermetfan.utils.Function;

public class Box2DSprite extends Sprite {
    public static final Function<Box2DSprite, Object> defaultUserDataAccessor = new Function<Box2DSprite, Object>() {
        public Box2DSprite apply(Object userData) {
            if (userData instanceof Box2DSprite) {
                return (Box2DSprite) userData;
            }
            return null;
        }
    };
    private static Function<Box2DSprite, Object> userDataAccessor = defaultUserDataAccessor;
    private static final Vector2 vec2 = new Vector2();
    private static Comparator<Box2DSprite> zComparator = new Comparator<Box2DSprite>() {
        public int compare(Box2DSprite s1, Box2DSprite s2) {
            if (s1.zIndex - s2.zIndex > Animation.CurveTimeline.LINEAR) {
                return 1;
            }
            return s1.zIndex - s2.zIndex < Animation.CurveTimeline.LINEAR ? -1 : 0;
        }
    };
    private boolean adjustHeight = true;
    private boolean adjustWidth = true;
    private boolean useOriginX;
    private boolean useOriginY;
    /* access modifiers changed from: private */
    public float zIndex;

    public Box2DSprite() {
    }

    public Box2DSprite(Texture texture, int srcWidth, int srcHeight) {
        super(texture, srcWidth, srcHeight);
    }

    public Box2DSprite(Texture texture, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(texture, srcX, srcY, srcWidth, srcHeight);
    }

    public Box2DSprite(TextureRegion region, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(region, srcX, srcY, srcWidth, srcHeight);
    }

    public Box2DSprite(Texture texture) {
        super(texture);
    }

    public Box2DSprite(TextureRegion region) {
        super(region);
    }

    public Box2DSprite(Sprite sprite) {
        super(sprite);
    }

    public static void draw(Batch batch, World world) {
        draw(batch, world, false);
    }

    public static void draw(Batch batch, World world, boolean sortByZ) {
        Array<Body> tmpBodies = (Array) Pools.obtain(Array.class);
        world.getBodies(tmpBodies);
        if (sortByZ) {
            ObjectMap<Box2DSprite, Object> tmpZMap = (ObjectMap) Pools.obtain(ObjectMap.class);
            tmpZMap.clear();
            Iterator it = tmpBodies.iterator();
            while (it.hasNext()) {
                Body body = (Body) it.next();
                Box2DSprite tmpBox2DSprite = userDataAccessor.apply(body.getUserData());
                if (tmpBox2DSprite != null) {
                    tmpZMap.put(tmpBox2DSprite, body);
                }
                Iterator<Fixture> it2 = body.getFixtureList().iterator();
                while (it2.hasNext()) {
                    Fixture fixture = it2.next();
                    Box2DSprite tmpBox2DSprite2 = userDataAccessor.apply(fixture.getUserData());
                    if (tmpBox2DSprite2 != null) {
                        tmpZMap.put(tmpBox2DSprite2, fixture);
                    }
                }
            }
            Array<Box2DSprite> tmpKeys = (Array) Pools.obtain(Array.class);
            Iterator<Box2DSprite> keys = tmpZMap.keys();
            while (keys.hasNext()) {
                tmpKeys.add(keys.next());
            }
            tmpKeys.sort(zComparator);
            Iterator it3 = tmpKeys.iterator();
            while (it3.hasNext()) {
                Box2DSprite key = (Box2DSprite) it3.next();
                Object value = tmpZMap.get(key);
                if (value instanceof Body) {
                    key.draw(batch, (Body) value);
                } else {
                    key.draw(batch, (Fixture) value);
                }
            }
            tmpKeys.clear();
            tmpZMap.clear();
            Pools.free(tmpKeys);
            Pools.free(tmpZMap);
        } else {
            Iterator it4 = tmpBodies.iterator();
            while (it4.hasNext()) {
                Body body2 = (Body) it4.next();
                Box2DSprite tmpBox2DSprite3 = userDataAccessor.apply(body2.getUserData());
                if (tmpBox2DSprite3 != null) {
                    tmpBox2DSprite3.draw(batch, body2);
                }
                Iterator<Fixture> it5 = body2.getFixtureList().iterator();
                while (it5.hasNext()) {
                    Fixture fixture2 = it5.next();
                    Box2DSprite tmpBox2DSprite4 = userDataAccessor.apply(fixture2.getUserData());
                    if (tmpBox2DSprite4 != null) {
                        tmpBox2DSprite4.draw(batch, fixture2);
                    }
                }
            }
        }
        tmpBodies.clear();
        Pools.free(tmpBodies);
    }

    public void draw(Batch batch, Fixture fixture) {
        vec2.set(Box2DUtils.position(fixture));
        draw(batch, vec2.x, vec2.y, Box2DUtils.width(fixture), Box2DUtils.height(fixture), fixture.getBody().getAngle());
    }

    public void draw(Batch batch, Body body) {
        float width = Box2DUtils.width(body);
        float height = Box2DUtils.height(body);
        vec2.set(Box2DUtils.minX(body) + (width / 2.0f), Box2DUtils.minY(body) + (height / 2.0f));
        vec2.set(body.getWorldPoint(vec2));
        draw(batch, vec2.x, vec2.y, width, height, body.getAngle());
    }

    public void draw(Batch batch, float box2dX, float box2dY, float box2dWidth, float box2dHeight, float box2dRotation) {
        batch.setColor(getColor());
        batch.draw(this, (box2dX - (box2dWidth / 2.0f)) + getX(), (box2dY - (box2dHeight / 2.0f)) + getY(), this.useOriginX ? getOriginX() : box2dWidth / 2.0f, this.useOriginY ? getOriginY() : box2dHeight / 2.0f, this.adjustWidth ? box2dWidth : getWidth(), this.adjustHeight ? box2dHeight : getHeight(), getScaleX(), getScaleY(), (57.295776f * box2dRotation) + getRotation());
    }

    public float getZIndex() {
        return this.zIndex;
    }

    public void setZIndex(float zIndex2) {
        this.zIndex = zIndex2;
    }

    public boolean isAdjustWidth() {
        return this.adjustWidth;
    }

    public void setAdjustWidth(boolean adjustWidth2) {
        this.adjustWidth = adjustWidth2;
    }

    public boolean isAdjustHeight() {
        return this.adjustHeight;
    }

    public void setAdjustHeight(boolean adjustHeight2) {
        this.adjustHeight = adjustHeight2;
    }

    public void setAdjustSize(boolean adjustSize) {
        this.adjustHeight = adjustSize;
        this.adjustWidth = adjustSize;
    }

    public boolean isUseOriginX() {
        return this.useOriginX;
    }

    public void setUseOriginX(boolean useOriginX2) {
        this.useOriginX = useOriginX2;
    }

    public boolean isUseOriginY() {
        return this.useOriginY;
    }

    public void setUseOriginY(boolean useOriginY2) {
        this.useOriginY = useOriginY2;
    }

    public void setUseOrigin(boolean useOrigin) {
        this.useOriginY = useOrigin;
        this.useOriginX = useOrigin;
    }

    public void setWidth(float width) {
        setSize(width, getHeight());
    }

    public void setHeight(float height) {
        setSize(getWidth(), height);
    }

    public static Comparator<Box2DSprite> getZComparator() {
        return zComparator;
    }

    public static void setZComparator(Comparator<Box2DSprite> zComparator2) {
        if (zComparator2 == null) {
            throw new IllegalArgumentException("zComparator must not be null");
        }
        zComparator = zComparator2;
    }

    public static Function<Box2DSprite, ?> getUserDataAccessor() {
        return userDataAccessor;
    }

    public static void setUserDataAccessor(Function<Box2DSprite, Object> userDataAccessor2) {
        if (userDataAccessor2 == null) {
            userDataAccessor2 = defaultUserDataAccessor;
        }
        userDataAccessor = userDataAccessor2;
    }
}
