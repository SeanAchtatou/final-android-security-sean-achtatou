package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import java.util.Iterator;

public class Chain {
    private Builder builder;
    private final Array<Connection> connections;
    private final Array<Body> segments;
    private final Array<Connection> tmpConnections;
    private final Array<Body> tmpSegments;

    public interface Builder {
        Connection createConnection(Body body, int i, Body body2, int i2);

        Body createSegment(int i, int i2, Chain chain);
    }

    public static class DefBuilder implements Builder {
        protected BodyDef bodyDef;
        protected FixtureDef fixtureDef;
        protected JointDef jointDef;
        protected World world;

        public DefBuilder(World world2, BodyDef bodyDef2, FixtureDef fixtureDef2, JointDef jointDef2) {
            this.world = world2;
            this.bodyDef = bodyDef2;
            this.fixtureDef = fixtureDef2;
            this.jointDef = jointDef2;
        }

        public Body createSegment(int index, int length, Chain chain) {
            return this.world.createBody(this.bodyDef).createFixture(this.fixtureDef).getBody();
        }

        public Connection createConnection(Body seg1, int seg1index, Body seg2, int seg2index) {
            this.jointDef.bodyA = seg1;
            this.jointDef.bodyB = seg2;
            return new Connection(this.world.createJoint(this.jointDef));
        }

        public World getWorld() {
            return this.world;
        }

        public void setWorld(World world2) {
            this.world = world2;
        }

        public BodyDef getBodyDef() {
            return this.bodyDef;
        }

        public void setBodyDef(BodyDef bodyDef2) {
            this.bodyDef = bodyDef2;
        }

        public FixtureDef getFixtureDef() {
            return this.fixtureDef;
        }

        public void setFixtureDef(FixtureDef fixtureDef2) {
            this.fixtureDef = fixtureDef2;
        }

        public JointDef getJointDef() {
            return this.jointDef;
        }

        public void setJointDef(JointDef jointDef2) {
            this.jointDef = jointDef2;
        }
    }

    public static class DefShapeBuilder implements Disposable, Builder {
        protected BodyDef bodyDef;
        protected float density;
        protected JointDef jointDef;
        protected Shape shape;
        protected World world;

        public DefShapeBuilder(World world2, BodyDef bodyDef2, Shape shape2, float density2, JointDef jointDef2) {
            this.world = world2;
            this.bodyDef = bodyDef2;
            this.shape = shape2;
            this.density = density2;
            this.jointDef = jointDef2;
        }

        public Body createSegment(int index, int length, Chain chain) {
            return this.world.createBody(this.bodyDef).createFixture(this.shape, this.density).getBody();
        }

        public Connection createConnection(Body seg1, int seg1index, Body seg2, int seg2index) {
            this.jointDef.bodyA = seg1;
            this.jointDef.bodyB = seg2;
            return new Connection(this.world.createJoint(this.jointDef));
        }

        public void dispose() {
            this.shape.dispose();
        }

        public World getWorld() {
            return this.world;
        }

        public void setWorld(World world2) {
            this.world = world2;
        }

        public BodyDef getBodyDef() {
            return this.bodyDef;
        }

        public void setBodyDef(BodyDef bodyDef2) {
            this.bodyDef = bodyDef2;
        }

        public float getDensity() {
            return this.density;
        }

        public void setDensity(float density2) {
            this.density = density2;
        }

        public JointDef getJointDef() {
            return this.jointDef;
        }

        public void setJointDef(JointDef jointDef2) {
            this.jointDef = jointDef2;
        }

        public Shape getShape() {
            return this.shape;
        }
    }

    public static abstract class CopyBuilder implements Builder {
        protected Body template;

        public CopyBuilder(Body template2) {
            this.template = template2;
        }

        public Body createSegment(int index, int length, Chain chain) {
            return Box2DUtils.clone(this.template);
        }

        public Body getTemplate() {
            return this.template;
        }

        public void setTemplate(Body template2) {
            this.template = template2;
        }
    }

    public static class JointDefCopyBuilder extends CopyBuilder {
        protected JointDef jointDef;

        public JointDefCopyBuilder(Body template, JointDef jointDef2) {
            super(template);
            this.jointDef = jointDef2;
        }

        public Connection createConnection(Body seg1, int seg1index, Body seg2, int seg2index) {
            this.jointDef.bodyA = seg1;
            this.jointDef.bodyB = seg2;
            return new Connection(this.template.getWorld().createJoint(this.jointDef));
        }

        public JointDef getJointDef() {
            return this.jointDef;
        }

        public void setJointDef(JointDef jointDef2) {
            this.jointDef = jointDef2;
        }
    }

    public static class Connection {
        public final Array<Joint> joints = new Array<>(2);

        public Connection(Joint joint) {
            this.joints.add(joint);
        }

        public Connection(Joint joint1, Joint joint2) {
            this.joints.add(joint1);
            this.joints.add(joint2);
        }

        public Connection(Joint joint1, Joint joint2, Joint joint3) {
            this.joints.add(joint1);
            this.joints.add(joint2);
            this.joints.add(joint3);
        }

        public Connection(Joint... joints2) {
            this.joints.addAll(joints2);
        }

        public void add(Joint joint) {
            this.joints.add(joint);
        }

        public boolean remove(Joint joint) {
            return this.joints.removeValue(joint, true);
        }

        public Joint remove(int index) {
            return this.joints.removeIndex(index);
        }

        public void destroy() {
            Iterator<Joint> it = this.joints.iterator();
            while (it.hasNext()) {
                Joint joint = it.next();
                joint.getBodyA().getWorld().destroyJoint(joint);
            }
        }
    }

    public Chain(Chain other) {
        this.segments = new Array<>();
        this.connections = new Array<>();
        this.tmpSegments = new Array<>();
        this.tmpConnections = new Array<>();
        this.builder = other.builder;
        this.segments.addAll(other.segments);
        this.connections.addAll(other.connections);
    }

    public Chain(Builder builder2) {
        this.segments = new Array<>();
        this.connections = new Array<>();
        this.tmpSegments = new Array<>();
        this.tmpConnections = new Array<>();
        this.builder = builder2;
    }

    public Chain(int length, Builder builder2) {
        this(length, builder2, true);
    }

    public Chain(int length, Builder builder2, boolean build) {
        this.segments = new Array<>();
        this.connections = new Array<>();
        this.tmpSegments = new Array<>();
        this.tmpConnections = new Array<>();
        this.segments.ensureCapacity(length - this.segments.size);
        this.connections.ensureCapacity(length - this.segments.size);
        this.builder = builder2;
        if (build) {
            extend(length);
        }
    }

    public Chain(Builder builder2, Body... segments2) {
        this.segments = new Array<>();
        this.connections = new Array<>();
        this.tmpSegments = new Array<>();
        this.tmpConnections = new Array<>();
        this.builder = builder2;
        for (Body segment : segments2) {
            add(segment);
        }
    }

    public Chain extend(int length) {
        return extend(length, this.builder);
    }

    public Chain extend(int length, Builder builder2) {
        while (length > 0) {
            extend(builder2);
            length--;
        }
        return this;
    }

    public Body extend() {
        return extend(this.builder);
    }

    public Body extend(Builder builder2) {
        Body segment = createSegment(this.segments.size, builder2);
        add(segment);
        return segment;
    }

    public void shorten() {
        destroy(this.segments.size - 1);
    }

    public void shorten(int length) {
        destroy(this.segments.size - length, this.segments.size - 1);
    }

    public Body createSegment(int index) {
        return createSegment(index, this.builder);
    }

    public Body createSegment(int index, Builder builder2) {
        return builder2.createSegment(index, this.segments.size + 1, this);
    }

    public Connection createConnection(int segmentIndex1, int segmentIndex2) {
        return createConnection(segmentIndex1, segmentIndex2, this.builder);
    }

    public Connection createConnection(int segmentIndex1, int segmentIndex2, Builder builder2) {
        return builder2.createConnection(this.segments.get(segmentIndex1), segmentIndex1, this.segments.get(segmentIndex2), segmentIndex2);
    }

    public void add(Body segment) {
        this.segments.add(segment);
        if (this.segments.size > 1) {
            this.connections.add(createConnection(this.segments.size + -2 < 0 ? 0 : this.segments.size - 2, this.segments.size - 1));
        }
    }

    public void add(Body... segments2) {
        for (Body segment : segments2) {
            add(segment);
        }
    }

    public Body insert(int index) {
        Body segment = createSegment(index);
        insert(index, segment);
        return segment;
    }

    public void insert(int index, Body segment) {
        if (index - 1 >= 0) {
            this.connections.removeIndex(index - 1).destroy();
        }
        this.segments.insert(index, segment);
        if (index - 1 >= 0) {
            this.connections.insert(index - 1, createConnection(index - 1, index));
        }
        if (index + 1 < this.segments.size) {
            this.connections.insert(index, createConnection(index, index + 1));
        }
    }

    public Body replace(int index, Body segment) {
        Body old = remove(index);
        insert(index, segment);
        return old;
    }

    public Body remove(Body segment) {
        if (this.segments.contains(segment, true)) {
            return remove(this.segments.indexOf(segment, true));
        }
        throw new IllegalArgumentException("the given body is not a segment of this Chain");
    }

    public Body remove(int index) {
        Body previous;
        if (index - 1 >= 0) {
            previous = this.segments.get(index - 1);
        } else {
            previous = null;
        }
        Body next = index + 1 < this.segments.size ? this.segments.get(index + 1) : null;
        Body segment = this.segments.removeIndex(index);
        if (index - 1 >= 0) {
            index--;
            this.connections.removeIndex(index).destroy();
        }
        if (index < this.connections.size) {
            this.connections.removeIndex(index).destroy();
        }
        if (!(previous == null || next == null)) {
            this.connections.insert(index, this.builder.createConnection(previous, index, next, index + 1));
        }
        return segment;
    }

    public Array<Body> remove(int beginIndex, int endIndex) {
        this.tmpSegments.clear();
        while (endIndex >= beginIndex) {
            this.tmpSegments.add(remove(endIndex));
            endIndex--;
        }
        return this.tmpSegments;
    }

    public void destroy(Body segment) {
        if (!this.segments.contains(segment, true)) {
            throw new IllegalArgumentException("the given body must be a segment of this Chain");
        }
        destroy(this.segments.indexOf(segment, true));
    }

    public void destroy(int index) {
        Body segment = remove(index);
        segment.getWorld().destroyBody(segment);
    }

    public void destroy(int beginIndex, int endIndex) {
        Array<Body> removed = remove(beginIndex, endIndex);
        Iterator<Body> it = removed.iterator();
        while (it.hasNext()) {
            Body body = it.next();
            body.getWorld().destroyBody(body);
        }
        removed.clear();
    }

    public Chain split(Connection connection) {
        if (this.connections.contains(connection, true)) {
            return split(this.connections.indexOf(connection, true));
        }
        throw new IllegalArgumentException("the joint must be part of this Chain");
    }

    public Chain split(int connectionIndex) {
        Body[] segs = new Body[(connectionIndex + 1)];
        for (int i = 0; i <= connectionIndex; i++) {
            this.connections.removeIndex(0).destroy();
            segs[i] = this.segments.removeIndex(0);
        }
        return new Chain(this.builder, segs);
    }

    public int length() {
        return this.segments.size;
    }

    public Body getSegment(int index) {
        return this.segments.get(index);
    }

    public Connection getConnection(int index) {
        return this.connections.get(index);
    }

    public Array<Body> getSegments() {
        this.tmpSegments.clear();
        this.tmpSegments.addAll(this.segments);
        return this.tmpSegments;
    }

    public Array<Connection> getConnections() {
        this.tmpConnections.clear();
        this.tmpConnections.addAll(this.connections);
        return this.tmpConnections;
    }

    public Builder getBuilder() {
        return this.builder;
    }

    public void setBuilder(Builder builder2) {
        if (builder2 == null) {
            throw new IllegalArgumentException("builder must not be null");
        }
        this.builder = builder2;
    }
}
