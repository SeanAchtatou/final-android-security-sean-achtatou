package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import net.dermetfan.utils.Function;
import net.dermetfan.utils.math.MathUtils;

public class Autopilot {
    public static final Function<Vector2, Body> defaultPositionAccessor = new Function<Vector2, Body>() {
        public Vector2 apply(Body body) {
            return body.getWorldCenter();
        }
    };
    private static final Vector2 vec2_0 = new Vector2();
    private static final Vector2 vec2_1 = new Vector2();
    private boolean adaptForceToMass;
    private float angle;
    public final Vector2 destination;
    private float distanceScalar;
    private Interpolation interpolation;
    private boolean moveRelative;
    private final Vector2 movementForce;
    private Function<Vector2, Body> positionAccessor;
    private boolean rotateRelative;
    private float rotationForce;

    public static Vector2 calculateForce(Vector2 destination2, Vector2 force) {
        return vec2_0.set(destination2).nor().scl(force);
    }

    public static Vector2 calculateForce(Vector2 destination2, Vector2 force, float distanceScalar2, Interpolation interpolation2) {
        return calculateForce(destination2, force).scl(interpolation2.apply(destination2.len() / distanceScalar2));
    }

    public static float calculateTorque(Vector2 target, float rotation, float angularVelocity, float inertia, float force, float delta) {
        return ((((MathUtils.normalize(com.badlogic.gdx.math.MathUtils.atan2(target.y, target.x) - ((angularVelocity * delta) + rotation), -3.1415927f, 3.1415927f) / 6.2831855f) * force) * delta) * inertia) / delta;
    }

    public Autopilot(Vector2 destination2, float angle2, float forces) {
        this(destination2, angle2, vec2_0.set(forces, forces), forces);
    }

    public Autopilot(Vector2 destination2, float angle2, Vector2 movementForce2, float rotationForce2) {
        this(destination2, angle2, movementForce2, rotationForce2, true);
    }

    public Autopilot(Vector2 destination2, float angle2, Vector2 movementForce2, float rotationForce2, boolean adaptForceToMass2) {
        this.destination = new Vector2();
        this.movementForce = new Vector2();
        this.distanceScalar = 1.0f;
        this.interpolation = Interpolation.linear;
        this.positionAccessor = defaultPositionAccessor;
        this.destination.set(destination2);
        this.angle = angle2;
        this.movementForce.set(movementForce2);
        this.rotationForce = rotationForce2;
        this.adaptForceToMass = adaptForceToMass2;
    }

    public void move(Body body, Vector2 destination2, Vector2 force, boolean wake) {
        Vector2 position = this.positionAccessor.apply(body);
        if (!this.moveRelative) {
            destination2 = vec2_0.set(destination2).sub(position);
        }
        if (this.adaptForceToMass) {
            force = vec2_1.set(force).scl(body.getMass());
        }
        body.applyForce(calculateForce(destination2, force), position, wake);
    }

    public void move(Body body, Vector2 destination2, Vector2 force, Interpolation interpolation2, float distanceScalar2, boolean wake) {
        Vector2 position = this.positionAccessor.apply(body);
        if (!this.moveRelative) {
            destination2 = vec2_0.set(destination2).sub(position);
        }
        if (this.adaptForceToMass) {
            force = vec2_1.set(force).scl(body.getMass());
        }
        body.applyForce(calculateForce(destination2, force, distanceScalar2, interpolation2), position, wake);
    }

    public void move(Body body, boolean interpolate, boolean wake) {
        if (interpolate) {
            move(body, this.destination, this.movementForce, this.interpolation, this.distanceScalar, wake);
            return;
        }
        move(body, this.destination, this.movementForce, wake);
    }

    public void rotate(Body body, Vector2 target, float angle2, float force, float delta, boolean wake) {
        body.applyTorque(calculateTorque(this.rotateRelative ? target : vec2_0.set(this.positionAccessor.apply(body)).sub(target), body.getTransform().getRotation() + angle2, body.getAngularVelocity(), body.getInertia(), this.adaptForceToMass ? body.getMass() * force : force, delta), wake);
    }

    public void rotate(Body body, float delta, boolean wake) {
        rotate(body, this.destination, this.angle, this.rotationForce, delta, wake);
    }

    public void rotate(Body body, float delta) {
        rotate(body, delta, true);
    }

    public void navigate(Body body, float delta, boolean interpolate, boolean wake) {
        rotate(body, delta, wake);
        move(body, interpolate, wake);
    }

    public void navigate(Body body, float delta, boolean wake) {
        navigate(body, delta, false, wake);
    }

    public void navigate(Body body, float delta) {
        navigate(body, delta, true);
    }

    public void setDestination(float x, float y) {
        this.destination.set(x, y);
    }

    public void setDestination(Vector2 destination2) {
        this.destination.set(destination2);
    }

    public Vector2 getDestination() {
        return this.destination;
    }

    public float getAngle() {
        return this.angle;
    }

    public void setAngle(float angle2) {
        this.angle = angle2;
    }

    public void setRelative(boolean relative) {
        this.rotateRelative = relative;
        this.moveRelative = relative;
    }

    public void setRelative(boolean moveRelative2, boolean rotateRelative2) {
        this.moveRelative = moveRelative2;
        this.rotateRelative = rotateRelative2;
    }

    public boolean isMoveRelative() {
        return this.moveRelative;
    }

    public void setMoveRelative(boolean moveRelative2) {
        this.moveRelative = moveRelative2;
    }

    public boolean isRotateRelative() {
        return this.rotateRelative;
    }

    public void setRotateRelative(boolean rotateRelative2) {
        this.rotateRelative = rotateRelative2;
    }

    public void setForces(float movementForceX, float movementForceY, float rotationForce2) {
        this.movementForce.set(movementForceX, movementForceY);
        this.rotationForce = rotationForce2;
    }

    public void setForces(Vector2 movementForce2, float rotationForce2) {
        this.movementForce.set(movementForce2);
        this.rotationForce = rotationForce2;
    }

    public Vector2 getMovementForce() {
        return this.movementForce;
    }

    public void setMovementForce(float x, float y) {
        this.movementForce.set(x, y);
    }

    public void setMovementForce(Vector2 movementForce2) {
        this.movementForce.set(movementForce2);
    }

    public float getRotationForce() {
        return this.rotationForce;
    }

    public void setRotationForce(float rotationForce2) {
        this.rotationForce = rotationForce2;
    }

    public boolean isAdaptForceToMass() {
        return this.adaptForceToMass;
    }

    public void setAdaptForceToMass(boolean adaptForceToMass2) {
        this.adaptForceToMass = adaptForceToMass2;
    }

    public float getDistanceScalar() {
        return this.distanceScalar;
    }

    public void setDistanceScalar(float distanceScalar2) {
        this.distanceScalar = distanceScalar2;
    }

    public Interpolation getInterpolation() {
        return this.interpolation;
    }

    public void setInterpolation(Interpolation interpolation2) {
        this.interpolation = interpolation2;
    }

    public Function<Vector2, Body> getPositionAccessor() {
        return this.positionAccessor;
    }

    public void setPositionAccessor(Function<Vector2, Body> positionAccessor2) {
        if (positionAccessor2 == null) {
            positionAccessor2 = defaultPositionAccessor;
        }
        this.positionAccessor = positionAccessor2;
    }
}
