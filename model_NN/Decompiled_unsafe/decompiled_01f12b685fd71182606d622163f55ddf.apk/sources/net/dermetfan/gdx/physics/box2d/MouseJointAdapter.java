package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class MouseJointAdapter extends InputAdapter {
    /* access modifiers changed from: private */
    public static final Vector2 vec2_0 = new Vector2();
    private static final Vector2 vec2_1 = new Vector2();
    /* access modifiers changed from: private */
    public boolean adaptMaxForceToBodyMass;
    private Camera camera;
    /* access modifiers changed from: private */
    public MouseJoint joint;
    /* access modifiers changed from: private */
    public MouseJointDef jointDef;
    /* access modifiers changed from: private */
    public Listener listener;
    private boolean mouseMoved;
    /* access modifiers changed from: private */
    public byte pointer;
    private final QueryCallback queryCallback;
    private final Vector3 vec3;

    public class Manager extends InputAdapter {
        private Array<MouseJointAdapter> adapters = new Array<>(false, 2);
        private byte max = -1;

        public Manager() {
        }

        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if (this.adapters.size <= pointer && (pointer + 1 < this.max || this.max < 0)) {
                this.adapters.add(newMouseJointAdapter((byte) pointer));
            }
            boolean handled = false;
            Iterator<MouseJointAdapter> it = this.adapters.iterator();
            while (it.hasNext()) {
                handled |= it.next().touchDown(screenX, screenY, pointer, button);
            }
            return handled;
        }

        public boolean touchDragged(int screenX, int screenY, int pointer) {
            boolean handled = false;
            Iterator<MouseJointAdapter> it = this.adapters.iterator();
            while (it.hasNext()) {
                handled |= it.next().touchDragged(screenX, screenY, pointer);
            }
            return handled;
        }

        public boolean mouseMoved(int screenX, int screenY) {
            boolean handled = false;
            Iterator<MouseJointAdapter> it = this.adapters.iterator();
            while (it.hasNext()) {
                handled |= it.next().mouseMoved(screenX, screenY);
            }
            return handled;
        }

        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            boolean handled = false;
            Iterator<MouseJointAdapter> it = this.adapters.iterator();
            while (it.hasNext()) {
                handled |= it.next().touchUp(screenX, screenY, pointer, button);
            }
            return handled;
        }

        public MouseJointAdapter newMouseJointAdapter(byte pointer) {
            MouseJointAdapter adapter = new MouseJointAdapter(MouseJointAdapter.this);
            byte unused = adapter.pointer = pointer;
            return adapter;
        }

        public byte getMax() {
            return this.max;
        }

        public void setMax(byte max2) {
            if (max2 < -1) {
                throw new IllegalArgumentException("max must be greater or equal to -1");
            }
            this.max = max2;
        }

        public Array<MouseJointAdapter> getAdapters() {
            return this.adapters;
        }

        public void setAdapters(Array<MouseJointAdapter> adapters2) {
            if (adapters2 == null) {
                throw new IllegalArgumentException("adapters must not be null");
            }
            if (adapters2.size > this.max && this.max > 0) {
                this.max = (byte) adapters2.size;
            }
            this.adapters = adapters2;
        }
    }

    public interface Listener {
        boolean dragged(MouseJoint mouseJoint, Vector2 vector2, Vector2 vector22);

        boolean released(MouseJoint mouseJoint, Vector2 vector2);

        boolean touched(Fixture fixture, Vector2 vector2);

        public static class Adapter implements Listener {
            public boolean touched(Fixture fixture, Vector2 position) {
                return false;
            }

            public boolean dragged(MouseJoint joint, Vector2 oldPosition, Vector2 position) {
                return false;
            }

            public boolean released(MouseJoint joint, Vector2 position) {
                return false;
            }
        }
    }

    public MouseJointAdapter(MouseJointDef jointDef2, boolean adaptMaxForceToBodyMass2, Camera camera2) {
        this(jointDef2, adaptMaxForceToBodyMass2, camera2, (byte) 0);
    }

    public MouseJointAdapter(MouseJointDef jointDef2, boolean adaptMaxForceToBodyMass2, Camera camera2, byte pointer2) {
        this(jointDef2, adaptMaxForceToBodyMass2, camera2, pointer2, null);
    }

    public MouseJointAdapter(MouseJointDef jointDef2, boolean adaptMaxForceToBodyMass2, Camera camera2, Listener listener2) {
        this(jointDef2, adaptMaxForceToBodyMass2, camera2, (byte) 0, listener2);
    }

    public MouseJointAdapter(MouseJointDef jointDef2, boolean adaptMaxForceToBodyMass2, Camera camera2, byte pointer2, Listener listener2) {
        this.vec3 = new Vector3();
        this.queryCallback = new QueryCallback() {
            public boolean reportFixture(Fixture fixture) {
                Body body = fixture.getBody();
                if (body != MouseJointAdapter.this.jointDef.bodyA && fixture.testPoint(MouseJointAdapter.vec2_0) && (MouseJointAdapter.this.listener == null || !MouseJointAdapter.this.listener.touched(fixture, MouseJointAdapter.vec2_0))) {
                    MouseJointAdapter.this.jointDef.bodyB = body;
                    MouseJointAdapter.this.jointDef.target.set(MouseJointAdapter.vec2_0);
                    if (MouseJointAdapter.this.adaptMaxForceToBodyMass) {
                        float maxForce = MouseJointAdapter.this.jointDef.maxForce;
                        MouseJointAdapter.this.jointDef.maxForce *= fixture.getBody().getMass();
                        MouseJoint unused = MouseJointAdapter.this.joint = (MouseJoint) MouseJointAdapter.this.jointDef.bodyA.getWorld().createJoint(MouseJointAdapter.this.jointDef);
                        MouseJointAdapter.this.jointDef.maxForce = maxForce;
                        return false;
                    }
                    MouseJoint unused2 = MouseJointAdapter.this.joint = (MouseJoint) MouseJointAdapter.this.jointDef.bodyA.getWorld().createJoint(MouseJointAdapter.this.jointDef);
                }
                return true;
            }
        };
        this.jointDef = jointDef2;
        this.adaptMaxForceToBodyMass = adaptMaxForceToBodyMass2;
        this.camera = camera2;
        this.pointer = pointer2;
        setListener(listener2);
    }

    public MouseJointAdapter(MouseJointAdapter other) {
        this(other.jointDef, other.adaptMaxForceToBodyMass, other.camera, other.pointer);
        setListener(other.listener);
        this.mouseMoved = other.mouseMoved;
    }

    public boolean touchDown(int screenX, int screenY, int pointer2, int button) {
        if (this.joint != null || !reactsToPointer(pointer2)) {
            return false;
        }
        this.camera.unproject(this.vec3.set((float) screenX, (float) screenY, Animation.CurveTimeline.LINEAR));
        vec2_0.set(this.vec3.x, this.vec3.y);
        this.jointDef.bodyA.getWorld().QueryAABB(this.queryCallback, vec2_0.x, vec2_0.y, vec2_0.x, vec2_0.y);
        return true;
    }

    public boolean touchDragged(int screenX, int screenY, int pointer2) {
        if (this.joint == null || !reactsToPointer(pointer2)) {
            return false;
        }
        this.camera.unproject(this.vec3.set((float) screenX, (float) screenY, Animation.CurveTimeline.LINEAR));
        if (this.listener == null || !this.listener.dragged(this.joint, vec2_1.set(vec2_0), vec2_0.set(this.vec3.x, this.vec3.y))) {
            this.joint.setTarget(vec2_0.set(this.vec3.x, this.vec3.y));
        }
        return true;
    }

    public boolean mouseMoved(int screenX, int screenY) {
        return this.mouseMoved && touchDragged(screenX, screenY, this.pointer);
    }

    public boolean touchUp(int screenX, int screenY, int pointer2, int button) {
        if (this.joint == null || !reactsToPointer(pointer2)) {
            return false;
        }
        this.camera.unproject(this.vec3.set((float) screenX, (float) screenY, Animation.CurveTimeline.LINEAR));
        if (this.listener != null && this.listener.released(this.joint, vec2_0.set(this.vec3.x, this.vec3.y))) {
            return false;
        }
        this.jointDef.bodyA.getWorld().destroyJoint(this.joint);
        this.joint = null;
        return true;
    }

    public boolean reactsToPointer(int pointer2) {
        return this.pointer == pointer2 || this.pointer < 0;
    }

    public byte getPointer() {
        return this.pointer;
    }

    public void setPointer(byte pointer2) {
        this.pointer = pointer2;
    }

    public Listener getListener() {
        return this.listener;
    }

    public void setListener(Listener listener2) {
        this.listener = listener2;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera camera2) {
        if (camera2 == null) {
            throw new IllegalArgumentException("camera must not be null");
        }
        this.camera = camera2;
    }

    public boolean isAdaptMaxForceToBodyMass() {
        return this.adaptMaxForceToBodyMass;
    }

    public void setAdaptMaxForceToBodyMass(boolean adaptMaxForceToBodyMass2) {
        this.adaptMaxForceToBodyMass = adaptMaxForceToBodyMass2;
    }

    public boolean isMouseMoved() {
        return this.mouseMoved;
    }

    public void setMouseMoved(boolean mouseMoved2) {
        this.mouseMoved = mouseMoved2;
    }

    public MouseJointDef getJointDef() {
        return this.jointDef;
    }

    public void setJointDef(MouseJointDef jointDef2) {
        if (jointDef2 == null) {
            throw new IllegalArgumentException("jointDef must not be null");
        }
        this.jointDef = jointDef2;
    }

    public MouseJoint getJoint() {
        return this.joint;
    }

    public void setJoint(MouseJoint joint2) {
        this.joint = joint2;
    }
}
