package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import net.dermetfan.utils.Function;
import net.dermetfan.utils.math.MathUtils;

public class Breakable {
    /* access modifiers changed from: private */
    public boolean breakBody;
    /* access modifiers changed from: private */
    public boolean breakBodyWithoutFixtures;
    /* access modifiers changed from: private */
    public Callback callback;
    /* access modifiers changed from: private */
    public float normalResistance;
    /* access modifiers changed from: private */
    public float reactionForceLength2Resistance;
    /* access modifiers changed from: private */
    public final Vector2 reactionForceResistance;
    /* access modifiers changed from: private */
    public float reactionTorqueResistance;
    /* access modifiers changed from: private */
    public float tangentResistance;

    public static class Manager implements ContactListener {
        public static final Function<Breakable, Object> defaultUserDataAccessor = new Function<Breakable, Object>() {
            public Breakable apply(Object userData) {
                if (userData instanceof Breakable) {
                    return (Breakable) userData;
                }
                return null;
            }
        };
        public final Array<Body> brokenBodies = new Array<>(1);
        public final Array<Fixture> brokenFixtures = new Array<>(1);
        public final Array<Joint> brokenJoints = new Array<>(1);
        private final Array<Joint> tmpJoints = new Array<>(0);
        private Function<Breakable, Object> userDataAccessor = defaultUserDataAccessor;

        public Manager() {
        }

        public Manager(Function<Breakable, Object> userDataAccessor2) {
            setUserDataAccessor(userDataAccessor2);
        }

        public void destroy() {
            Iterator<Fixture> it = this.brokenFixtures.iterator();
            while (it.hasNext()) {
                Fixture fixture = it.next();
                this.brokenFixtures.removeValue(fixture, true);
                fixture.getBody().destroyFixture(fixture);
            }
            Iterator<Body> it2 = this.brokenBodies.iterator();
            while (it2.hasNext()) {
                Body body = it2.next();
                this.brokenBodies.removeValue(body, true);
                body.getWorld().destroyBody(body);
            }
            Iterator<Joint> it3 = this.brokenJoints.iterator();
            while (it3.hasNext()) {
                Joint joint = it3.next();
                this.brokenJoints.removeValue(joint, true);
                joint.getBodyA().getWorld().destroyJoint(joint);
            }
        }

        public void strain(Contact contact, ContactImpulse impulse) {
            float normalImpulse = MathUtils.sum(impulse.getNormalImpulses());
            float tangentImpulse = Math.abs(MathUtils.sum(impulse.getTangentImpulses()));
            Fixture fixtureA = contact.getFixtureA();
            Fixture fixtureB = contact.getFixtureB();
            if (shouldBreak(this.userDataAccessor.apply(fixtureA.getUserData()), normalImpulse, tangentImpulse, contact, impulse, fixtureA)) {
                destroy(fixtureA);
            }
            if (shouldBreak(this.userDataAccessor.apply(fixtureB.getUserData()), normalImpulse, tangentImpulse, contact, impulse, fixtureB)) {
                destroy(fixtureB);
            }
            Body bodyA = fixtureA.getBody();
            Body bodyB = fixtureB.getBody();
            if (shouldBreak(this.userDataAccessor.apply(bodyA.getUserData()), normalImpulse, tangentImpulse, contact, impulse, fixtureA)) {
                destroy(bodyA);
            }
            if (shouldBreak(this.userDataAccessor.apply(bodyB.getUserData()), normalImpulse, tangentImpulse, contact, impulse, fixtureB)) {
                destroy(bodyB);
            }
        }

        public void strain(World world, float delta) {
            world.getJoints(this.tmpJoints);
            Iterator<Joint> it = this.tmpJoints.iterator();
            while (it.hasNext()) {
                strain(it.next(), delta);
            }
        }

        public void strain(Joint joint, float delta) {
            Breakable breakable = this.userDataAccessor.apply(joint.getUserData());
            if (breakable != null && shouldBreak(breakable, joint.getReactionForce(1.0f / delta), Math.abs(joint.getReactionTorque(1.0f / delta)), joint)) {
                destroy(joint);
            }
        }

        public static boolean shouldBreak(Breakable breakable, float normalImpulse, float tangentImpulse, Contact contact, ContactImpulse impulse, Fixture fixture) {
            return breakable != null && (normalImpulse > breakable.normalResistance || tangentImpulse > breakable.tangentResistance) && (breakable.callback == null || !breakable.callback.strained(fixture, breakable, contact, impulse, normalImpulse, tangentImpulse));
        }

        public static boolean shouldBreak(Breakable breakable, Vector2 reactionForce, float reactionTorque, Joint joint) {
            return breakable != null && (Math.abs(reactionForce.x) > breakable.reactionForceResistance.x || Math.abs(reactionForce.y) > breakable.reactionForceResistance.y || reactionForce.len2() > breakable.reactionForceLength2Resistance || reactionTorque > breakable.reactionTorqueResistance) && (breakable.callback == null || !breakable.callback.strained(joint, breakable, reactionForce, reactionTorque));
        }

        public void destroy(Fixture fixture) {
            if (!this.brokenFixtures.contains(fixture, true)) {
                Breakable breakable = this.userDataAccessor.apply(fixture.getUserData());
                if (breakable == null || breakable.callback == null || !breakable.callback.destroyed(fixture, breakable)) {
                    this.brokenFixtures.add(fixture);
                }
                if (breakable != null) {
                    Body body = fixture.getBody();
                    if (breakable.breakBody) {
                        destroy(body);
                    } else if (breakable.breakBodyWithoutFixtures) {
                        Iterator<Fixture> it = body.getFixtureList().iterator();
                        while (it.hasNext()) {
                            if (!this.brokenFixtures.contains(it.next(), true)) {
                                return;
                            }
                        }
                        destroy(body);
                    }
                }
            }
        }

        public void destroy(Body body) {
            if (!this.brokenBodies.contains(body, true)) {
                Breakable breakable = this.userDataAccessor.apply(body.getUserData());
                if (breakable == null || breakable.callback == null || !breakable.callback.destroyed(body, breakable)) {
                    this.brokenBodies.add(body);
                }
            }
        }

        public void destroy(Joint joint) {
            if (!this.brokenJoints.contains(joint, true)) {
                Breakable breakable = this.userDataAccessor.apply(joint.getUserData());
                if (breakable == null || breakable.callback == null || !breakable.callback.destroyed(joint, breakable)) {
                    this.brokenJoints.add(joint);
                }
                if (breakable != null && breakable.breakBody) {
                    destroy(joint.getBodyA());
                    destroy(joint.getBodyB());
                }
            }
        }

        public void beginContact(Contact contact) {
        }

        public void preSolve(Contact contact, Manifold oldManifold) {
        }

        public void postSolve(Contact contact, ContactImpulse impulse) {
            strain(contact, impulse);
        }

        public void endContact(Contact contact) {
        }

        public Array<Fixture> getBrokenFixtures() {
            return this.brokenFixtures;
        }

        public Array<Body> getBrokenBodies() {
            return this.brokenBodies;
        }

        public Function<Breakable, Object> getUserDataAccessor() {
            return this.userDataAccessor;
        }

        public void setUserDataAccessor(Function<Breakable, Object> userDataAccessor2) {
            if (userDataAccessor2 == null) {
                userDataAccessor2 = defaultUserDataAccessor;
            }
            this.userDataAccessor = userDataAccessor2;
        }
    }

    public interface Callback {
        boolean destroyed(Body body, Breakable breakable);

        boolean destroyed(Fixture fixture, Breakable breakable);

        boolean destroyed(Joint joint, Breakable breakable);

        boolean strained(Fixture fixture, Breakable breakable, Contact contact, ContactImpulse contactImpulse, float f, float f2);

        boolean strained(Joint joint, Breakable breakable, Vector2 vector2, float f);

        public static class Adapter implements Callback {
            public boolean strained(Fixture fixture, Breakable breakable, Contact contact, ContactImpulse impulse, float normalImpulse, float tangentImpulse) {
                return false;
            }

            public boolean strained(Joint joint, Breakable breakable, Vector2 reactionForce, float reactionTorque) {
                return false;
            }

            public boolean destroyed(Body body, Breakable breakable) {
                return false;
            }

            public boolean destroyed(Fixture fixture, Breakable breakable) {
                return false;
            }

            public boolean destroyed(Joint joint, Breakable breakable) {
                return false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean):void
     arg types: [float, float, int]
     candidates:
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean):void */
    public Breakable(float normalResistance2, float tangentResistance2) {
        this(normalResistance2, tangentResistance2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, boolean):void
     arg types: [float, float, boolean, int]
     candidates:
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, boolean):void */
    public Breakable(float normalResistance2, float tangentResistance2, boolean breakBody2) {
        this(normalResistance2, tangentResistance2, breakBody2, true);
    }

    public Breakable(float normalResistance2, float tangentResistance2, boolean breakBody2, boolean breakBodyWithoutFixtures2) {
        this(normalResistance2, tangentResistance2, Vector2.Zero, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, breakBody2, breakBodyWithoutFixtures2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
     arg types: [float, float, int, net.dermetfan.gdx.physics.box2d.Breakable$Callback]
     candidates:
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, boolean):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void */
    public Breakable(float normalResistance2, float tangentResistance2, Callback callback2) {
        this(normalResistance2, tangentResistance2, false, callback2);
    }

    public Breakable(float normalResistance2, float tangentResistance2, boolean breakBody2, Callback callback2) {
        this(normalResistance2, tangentResistance2, Vector2.Zero, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, breakBody2, true, callback2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean):void
     arg types: [com.badlogic.gdx.math.Vector2, float, float, int]
     candidates:
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(float, float, boolean, boolean):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean):void */
    public Breakable(Vector2 reactionForceResistance2, float reactionForceLength2Resistance2, float reactionTorqueResistance2) {
        this(reactionForceResistance2, reactionForceLength2Resistance2, reactionTorqueResistance2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean, boolean):void
     arg types: [com.badlogic.gdx.math.Vector2, float, float, boolean, int]
     candidates:
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean, boolean):void */
    public Breakable(Vector2 reactionForceResistance2, float reactionForceLength2Resistance2, float reactionTorqueResistance2, boolean breakBody2) {
        this(reactionForceResistance2, reactionForceLength2Resistance2, reactionTorqueResistance2, breakBody2, true);
    }

    public Breakable(Vector2 reactionForceResistance2, float reactionForceLength2Resistance2, float reactionTorqueResistance2, boolean breakBody2, boolean breakBodyWithoutFixtures2) {
        this(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, reactionForceResistance2, reactionForceLength2Resistance2, reactionTorqueResistance2, breakBody2, breakBodyWithoutFixtures2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void
     arg types: [com.badlogic.gdx.math.Vector2, float, float, int, net.dermetfan.gdx.physics.box2d.Breakable$Callback]
     candidates:
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean, boolean):void
      net.dermetfan.gdx.physics.box2d.Breakable.<init>(com.badlogic.gdx.math.Vector2, float, float, boolean, net.dermetfan.gdx.physics.box2d.Breakable$Callback):void */
    public Breakable(Vector2 reactionForceResistance2, float reactionForceLength2Resistance2, float reactionTorqueResistance2, Callback callback2) {
        this(reactionForceResistance2, reactionForceLength2Resistance2, reactionTorqueResistance2, false, callback2);
    }

    public Breakable(Vector2 reactionForceResistance2, float reactionForceLength2Resistance2, float reactionTorqueResistance2, boolean breakBody2, Callback callback2) {
        this(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, reactionForceResistance2, reactionForceLength2Resistance2, reactionTorqueResistance2, breakBody2, true, callback2);
    }

    public Breakable(float normalResistance2, float tangentResistance2, Vector2 reactionForceResistance2, float reactionForceLength2Resistance2, float reactionTorqueResistance2, boolean breakBody2, boolean breakBodyWithoutFixtures2, Callback callback2) {
        this.reactionForceResistance = new Vector2();
        this.breakBodyWithoutFixtures = true;
        this.normalResistance = normalResistance2;
        this.tangentResistance = tangentResistance2;
        this.reactionForceResistance.set(reactionForceResistance2);
        this.reactionForceLength2Resistance = reactionForceLength2Resistance2;
        this.reactionTorqueResistance = reactionTorqueResistance2;
        this.breakBody = breakBody2;
        this.breakBodyWithoutFixtures = breakBodyWithoutFixtures2;
        this.callback = callback2;
    }

    public Breakable(Breakable other) {
        this(other.normalResistance, other.tangentResistance, other.reactionForceResistance, other.reactionForceLength2Resistance, other.reactionTorqueResistance, other.breakBody, other.breakBodyWithoutFixtures, other.callback);
    }

    public float getNormalResistance() {
        return this.normalResistance;
    }

    public void setNormalResistance(float normalResistance2) {
        this.normalResistance = normalResistance2;
    }

    public float getTangentResistance() {
        return this.tangentResistance;
    }

    public void setTangentResistance(float tangentResistance2) {
        this.tangentResistance = tangentResistance2;
    }

    public Vector2 getReactionForceResistance() {
        return this.reactionForceResistance;
    }

    public void setReactionForceResistance(Vector2 reactionForceResistance2) {
        this.reactionForceResistance.set(reactionForceResistance2);
    }

    public float getReactionForceLength2Resistance() {
        return this.reactionForceLength2Resistance;
    }

    public void setReactionForceLength2Resistance(float reactionForceLength2Resistance2) {
        this.reactionForceLength2Resistance = reactionForceLength2Resistance2;
    }

    public float getReactionTorqueResistance() {
        return this.reactionTorqueResistance;
    }

    public void setReactionTorqueResistance(float reactionTorqueResistance2) {
        this.reactionTorqueResistance = reactionTorqueResistance2;
    }

    public boolean isBreakBody() {
        return this.breakBody;
    }

    public void setBreakBody(boolean breakBody2) {
        this.breakBody = breakBody2;
    }

    public boolean isBreakBodyWithoutFixtures() {
        return this.breakBodyWithoutFixtures;
    }

    public void setBreakBodyWithoutFixtures(boolean breakBodyWithoutFixtures2) {
        this.breakBodyWithoutFixtures = breakBodyWithoutFixtures2;
    }

    public Callback getCallback() {
        return this.callback;
    }

    public void setCallback(Callback callback2) {
        this.callback = callback2;
    }
}
