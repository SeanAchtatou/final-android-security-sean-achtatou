package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DUtils;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.FrictionJoint;
import com.badlogic.gdx.physics.box2d.joints.GearJoint;
import com.badlogic.gdx.physics.box2d.joints.MotorJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RopeJoint;
import com.badlogic.gdx.physics.box2d.joints.WeldJoint;
import com.badlogic.gdx.physics.box2d.joints.WheelJoint;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import java.util.Objects;

public class WorldObserver {
    static final /* synthetic */ boolean $assertionsDisabled = (!WorldObserver.class.desiredAssertionStatus());
    private final IntMap<BodyChange> bodyChanges = new IntMap<>();
    private final IntMap<Body> currentBodies = new IntMap<>();
    private final IntMap<Fixture> currentFixtures = new IntMap<>();
    private final Array<Joint> currentJoints = new Array<>();
    private final IntMap<FixtureChange> fixtureChanges = new IntMap<>();
    private final ObjectMap<Joint, JointChange> jointChanges = new ObjectMap<>();
    private Listener listener;
    private final IntMap<Body> previousBodies = new IntMap<>();
    private final IntMap<Fixture> previousFixtures = new IntMap<>();
    private final Array<Joint> previousJoints = new Array<>();
    private final Array<Body> tmpBodies = new Array<>();
    private final WorldChange worldChange = new WorldChange();

    public interface Change<T> extends Pool.Poolable {
        void apply(T t);

        <C extends Change<T>> boolean newValuesEqual(C c);

        boolean update(T t);
    }

    public WorldObserver() {
    }

    public WorldObserver(Listener listener2) {
        setListener(listener2);
    }

    public void update(World world, float step) {
        if (this.listener != null) {
            this.listener.preUpdate(world, step);
        }
        if (this.worldChange.update(world) && this.listener != null) {
            this.listener.changed(world, this.worldChange);
        }
        world.getBodies(this.tmpBodies);
        this.currentBodies.clear();
        this.currentFixtures.clear();
        Iterator<Body> it = this.tmpBodies.iterator();
        while (it.hasNext()) {
            Body body = it.next();
            this.currentBodies.put(Box2DUtils.hashCode(body), body);
            Iterator<Fixture> it2 = body.getFixtureList().iterator();
            while (it2.hasNext()) {
                Fixture fixture = it2.next();
                this.currentFixtures.put(Box2DUtils.hashCode(fixture), fixture);
            }
        }
        Iterator<IntMap.Entry<Body>> it3 = this.previousBodies.entries().iterator();
        while (it3.hasNext()) {
            IntMap.Entry<Body> entry = it3.next();
            if (!this.currentBodies.containsKey(entry.key)) {
                Pools.free(this.bodyChanges.remove(entry.key));
                if (this.listener != null) {
                    this.listener.destroyed((Body) entry.value);
                }
            }
        }
        this.previousBodies.clear();
        this.previousBodies.putAll(this.currentBodies);
        Iterator<IntMap.Entry<Fixture>> it4 = this.previousFixtures.entries().iterator();
        while (it4.hasNext()) {
            IntMap.Entry<Fixture> entry2 = it4.next();
            if (!this.currentFixtures.containsKey(entry2.key)) {
                Pools.free(this.fixtureChanges.get(entry2.key));
                if (this.listener != null) {
                    this.listener.destroyed((Fixture) entry2.value);
                }
            }
        }
        this.previousFixtures.clear();
        this.previousFixtures.putAll(this.currentFixtures);
        Iterator<IntMap.Entry<Body>> it5 = this.currentBodies.entries().iterator();
        while (it5.hasNext()) {
            IntMap.Entry<Body> entry3 = it5.next();
            BodyChange bodyChange = this.bodyChanges.get(entry3.key);
            if (bodyChange == null) {
                BodyChange bodyChange2 = (BodyChange) Pools.obtain(BodyChange.class);
                bodyChange2.update((Body) entry3.value);
                this.bodyChanges.put(entry3.key, bodyChange2);
                if (this.listener != null) {
                    this.listener.created((Body) entry3.value);
                }
            } else if (bodyChange.update((Body) entry3.value) && this.listener != null) {
                this.listener.changed((Body) entry3.value, bodyChange);
            }
        }
        Iterator<IntMap.Entry<Fixture>> it6 = this.currentFixtures.entries().iterator();
        while (it6.hasNext()) {
            IntMap.Entry<Fixture> entry4 = it6.next();
            FixtureChange fixtureChange = this.fixtureChanges.get(entry4.key);
            if (fixtureChange == null) {
                FixtureChange fixtureChange2 = (FixtureChange) Pools.obtain(FixtureChange.class);
                fixtureChange2.created(((Fixture) entry4.value).getBody());
                fixtureChange2.update((Fixture) entry4.value);
                this.fixtureChanges.put(entry4.key, fixtureChange2);
                if (this.listener != null) {
                    this.listener.created((Fixture) entry4.value);
                }
            } else if (fixtureChange.update((Fixture) entry4.value) && this.listener != null) {
                this.listener.changed((Fixture) entry4.value, fixtureChange);
            }
        }
        world.getJoints(this.currentJoints);
        Iterator<Joint> it7 = this.currentJoints.iterator();
        while (it7.hasNext()) {
            Joint joint = it7.next();
            JointChange<Joint> jointChange = this.jointChanges.get(joint);
            if (jointChange == null) {
                JointChange<Joint> newJointChange = JointChange.obtainFor(joint.getType());
                newJointChange.update(joint);
                this.jointChanges.put(joint, newJointChange);
                if (this.listener != null) {
                    this.listener.created(joint);
                }
            } else if (jointChange.update(joint) && this.listener != null) {
                this.listener.changed(joint, jointChange);
            }
        }
        this.previousJoints.removeAll(this.currentJoints, true);
        Iterator<Joint> it8 = this.previousJoints.iterator();
        while (it8.hasNext()) {
            Joint joint2 = it8.next();
            JointChange change = this.jointChanges.remove(joint2);
            if ($assertionsDisabled || change != null) {
                Pools.free(change);
                if (this.listener != null) {
                    this.listener.destroyed(joint2);
                }
            } else {
                throw new AssertionError();
            }
        }
        this.previousJoints.clear();
        this.previousJoints.addAll(this.currentJoints);
        if (this.listener != null) {
            this.listener.postUpdate(world, step);
        }
    }

    public BodyChange getBodyChange(int hash) {
        return this.bodyChanges.get(hash);
    }

    public FixtureChange getFixtureChange(int hash) {
        return this.fixtureChanges.get(hash);
    }

    public JointChange getJointChange(Joint joint) {
        return this.jointChanges.get(joint);
    }

    public WorldChange getWorldChange() {
        return this.worldChange;
    }

    public Listener getListener() {
        return this.listener;
    }

    public void setListener(Listener listener2) {
        if (this.listener != null) {
            this.listener.removedFrom(this);
        }
        this.listener = listener2;
        if (listener2 != null) {
            listener2.setOn(this);
        }
    }

    public interface Listener {
        void changed(Body body, BodyChange bodyChange);

        void changed(Fixture fixture, FixtureChange fixtureChange);

        void changed(Joint joint, JointChange jointChange);

        void changed(World world, WorldChange worldChange);

        void created(Body body);

        void created(Fixture fixture);

        void created(Joint joint);

        void destroyed(Body body);

        void destroyed(Fixture fixture);

        void destroyed(Joint joint);

        void postUpdate(World world, float f);

        void preUpdate(World world, float f);

        void removedFrom(WorldObserver worldObserver);

        void setOn(WorldObserver worldObserver);

        public static class Adapter implements Listener {
            public void setOn(WorldObserver observer) {
            }

            public void removedFrom(WorldObserver observer) {
            }

            public void preUpdate(World world, float step) {
            }

            public void postUpdate(World world, float step) {
            }

            public void changed(World world, WorldChange change) {
            }

            public void changed(Body body, BodyChange change) {
            }

            public void created(Body body) {
            }

            public void destroyed(Body body) {
            }

            public void changed(Fixture fixture, FixtureChange change) {
            }

            public void created(Fixture fixture) {
            }

            public void destroyed(Fixture fixture) {
            }

            public void changed(Joint joint, JointChange change) {
            }

            public void created(Joint joint) {
            }

            public void destroyed(Joint joint) {
            }
        }
    }

    public static class UnexpectedListener implements Listener {
        private final ObjectMap<Body, ExpectationBase> bases = new ObjectMap<>();
        private Listener listener;
        private final Pool<ExpectationBase> pool = new Pool<ExpectationBase>(5, 75) {
            /* access modifiers changed from: protected */
            public ExpectationBase newObject() {
                return new ExpectationBase();
            }
        };
        private float step;

        public UnexpectedListener(Listener listener2) {
            this.listener = listener2;
        }

        public void changed(Body body, BodyChange change) {
            boolean unexpected;
            if (change.newType == null && change.newAngularDamping == null && change.newGravityScale == null && change.newMassData == null && !change.userDataChanged) {
                unexpected = false;
            } else {
                unexpected = true;
            }
            ExpectationBase base = this.bases.get(body);
            if (!unexpected && change.newLinearVelocity != null && !change.newLinearVelocity.equals(base.linearVelocity.mulAdd(body.getWorld().getGravity(), this.step).scl(1.0f / ((this.step * body.getLinearDamping()) + 1.0f)))) {
                unexpected = true;
            } else if (change.newTransform != null && change.newTransform.vals[0] != base.transform.vals[0] + (base.linearVelocity.x * this.step) && change.newTransform.vals[1] != base.transform.vals[1] + (base.linearVelocity.y * this.step)) {
                unexpected = true;
            } else if (!(change.newAngularVelocity == null || change.newAngularVelocity.floatValue() == base.angularVelocity * (1.0f / ((this.step * body.getAngularDamping()) + 1.0f)))) {
                unexpected = true;
            }
            base.set(body);
            if (unexpected) {
                this.listener.changed(body, change);
            }
        }

        public void setOn(WorldObserver observer) {
            this.listener.setOn(observer);
        }

        public void removedFrom(WorldObserver observer) {
            this.listener.removedFrom(observer);
        }

        public void preUpdate(World world, float step2) {
            Listener listener2 = this.listener;
            this.step = step2;
            listener2.preUpdate(world, step2);
        }

        public void postUpdate(World world, float step2) {
            Listener listener2 = this.listener;
            this.step = step2;
            listener2.postUpdate(world, step2);
        }

        public void changed(World world, WorldChange change) {
            this.listener.changed(world, change);
        }

        public void created(Body body) {
            this.bases.put(body, this.pool.obtain().set(body));
            this.listener.created(body);
        }

        public void destroyed(Body body) {
            this.pool.free(this.bases.remove(body));
            this.listener.destroyed(body);
        }

        public void changed(Fixture fixture, FixtureChange change) {
            this.listener.changed(fixture, change);
        }

        public void created(Fixture fixture) {
            this.listener.created(fixture);
        }

        public void destroyed(Fixture fixture) {
            this.listener.destroyed(fixture);
        }

        public void changed(Joint joint, JointChange change) {
            this.listener.changed(joint, change);
        }

        public void created(Joint joint) {
            this.listener.created(joint);
        }

        public void destroyed(Joint joint) {
            this.listener.destroyed(joint);
        }

        public Listener getListener() {
            return this.listener;
        }

        public void setListener(Listener listener2) {
            this.listener = listener2;
        }

        private static class ExpectationBase implements Pool.Poolable {
            float angularVelocity;
            final Vector2 linearVelocity;
            final Transform transform;

            private ExpectationBase() {
                this.transform = new Transform();
                this.linearVelocity = new Vector2();
            }

            public ExpectationBase set(Body body) {
                Transform bodyTransform = body.getTransform();
                this.transform.vals[0] = bodyTransform.vals[0];
                this.transform.vals[1] = bodyTransform.vals[1];
                this.transform.vals[2] = bodyTransform.vals[2];
                this.transform.vals[3] = bodyTransform.vals[3];
                this.linearVelocity.set(body.getLinearVelocity());
                this.angularVelocity = body.getAngularVelocity();
                return this;
            }

            public void reset() {
                float[] fArr = this.transform.vals;
                float[] fArr2 = this.transform.vals;
                float[] fArr3 = this.transform.vals;
                this.transform.vals[3] = 0.0f;
                fArr3[2] = 0.0f;
                fArr2[1] = 0.0f;
                fArr[0] = 0.0f;
                this.angularVelocity = Animation.CurveTimeline.LINEAR;
            }
        }
    }

    public static class WorldChange implements Change<World> {
        public Boolean newAutoClearForces;
        public Vector2 newGravity;
        private transient Boolean oldAutoClearForces;
        private final transient Vector2 oldGravity = new Vector2();

        public boolean update(World world) {
            Boolean autoClearForces = Boolean.valueOf(world.getAutoClearForces());
            Vector2 gravity = world.getGravity();
            boolean changed = false;
            if (!autoClearForces.equals(this.oldAutoClearForces)) {
                this.newAutoClearForces = autoClearForces;
                this.oldAutoClearForces = autoClearForces;
                changed = true;
            } else {
                this.newAutoClearForces = null;
            }
            if (!gravity.equals(this.oldGravity)) {
                Vector2 vector2 = this.oldGravity;
                this.newGravity = gravity;
                vector2.set(gravity);
                return true;
            }
            this.newAutoClearForces = null;
            return changed;
        }

        public void apply(World world) {
            if (this.newAutoClearForces != null) {
                world.setAutoClearForces(this.newAutoClearForces.booleanValue());
            }
            if (this.newGravity != null) {
                world.setGravity(this.newGravity);
            }
        }

        public <C extends Change<World>> boolean newValuesEqual(C other) {
            boolean diff;
            boolean z = true;
            if (!(other instanceof WorldChange)) {
                return false;
            }
            WorldChange o = (WorldChange) other;
            if (!Objects.equals(this.newAutoClearForces, o.newAutoClearForces)) {
                diff = true;
            } else {
                diff = false;
            }
            if (Objects.equals(this.newGravity, o.newGravity)) {
                z = false;
            }
            return diff | z;
        }

        public void reset() {
            this.oldAutoClearForces = null;
            this.oldGravity.setZero();
            this.newAutoClearForces = null;
            this.newGravity = null;
        }
    }

    public static class BodyChange implements Change<Body> {
        public Boolean newActive;
        public Float newAngularDamping;
        public Float newAngularVelocity;
        public Boolean newAwake;
        public Boolean newBullet;
        public Boolean newFixedRotation;
        public Float newGravityScale;
        public Float newLinearDamping;
        public Vector2 newLinearVelocity;
        public MassData newMassData;
        public Boolean newSleepingAllowed;
        public Transform newTransform;
        public BodyDef.BodyType newType;
        public Object newUserData;
        private transient boolean oldActive;
        private transient float oldAngularDamping;
        private transient float oldAngularVelocity;
        private transient boolean oldAwake;
        private transient boolean oldBullet;
        private transient boolean oldFixedRotation;
        private transient float oldGravityScale;
        private transient float oldLinearDamping;
        private final transient Vector2 oldLinearVelocity = new Vector2();
        private final transient MassData oldMassData = new MassData();
        private transient boolean oldSleepingAllowed;
        private final transient Transform oldTransform = new Transform();
        private transient BodyDef.BodyType oldType;
        private transient Object oldUserData;
        /* access modifiers changed from: private */
        public boolean userDataChanged;

        private void updateOldTransform(Transform transform) {
            this.oldTransform.vals[0] = transform.vals[0];
            this.oldTransform.vals[1] = transform.vals[1];
            this.oldTransform.vals[2] = transform.vals[2];
            this.oldTransform.vals[3] = transform.vals[3];
        }

        private void updateOldMassData(MassData massData) {
            this.oldMassData.center.set(massData.center);
            this.oldMassData.mass = massData.mass;
            this.oldMassData.I = massData.I;
        }

        public boolean update(Body body) {
            Transform transform = body.getTransform();
            BodyDef.BodyType type = body.getType();
            float angularDamping = body.getAngularDamping();
            float angularVelocity = body.getAngularVelocity();
            float linearDamping = body.getLinearDamping();
            float gravityScale = body.getGravityScale();
            Vector2 linearVelocity = body.getLinearVelocity();
            MassData massData = body.getMassData();
            boolean fixedRotation = body.isFixedRotation();
            boolean bullet = body.isBullet();
            boolean awake = body.isAwake();
            boolean active = body.isActive();
            boolean sleepingAllowed = body.isSleepingAllowed();
            Object userData = body.getUserData();
            boolean changed = false;
            if (!Box2DUtils.equals(transform, this.oldTransform)) {
                this.newTransform = transform;
                updateOldTransform(transform);
                changed = true;
            } else {
                this.newTransform = null;
            }
            if (!type.equals(this.oldType)) {
                this.newType = type;
                this.oldType = type;
                changed = true;
            } else {
                this.newType = null;
            }
            if (angularDamping != this.oldAngularDamping) {
                Float valueOf = Float.valueOf(angularDamping);
                this.newAngularDamping = valueOf;
                this.oldAngularDamping = valueOf.floatValue();
                changed = true;
            } else {
                this.newAngularDamping = null;
            }
            if (angularVelocity != this.oldAngularVelocity) {
                Float valueOf2 = Float.valueOf(angularVelocity);
                this.newAngularVelocity = valueOf2;
                this.oldAngularVelocity = valueOf2.floatValue();
                changed = true;
            } else {
                this.newAngularVelocity = null;
            }
            if (linearDamping != this.oldLinearDamping) {
                Float valueOf3 = Float.valueOf(linearDamping);
                this.newLinearDamping = valueOf3;
                this.oldLinearDamping = valueOf3.floatValue();
                changed = true;
            } else {
                this.newLinearDamping = null;
            }
            if (gravityScale != this.oldGravityScale) {
                Float valueOf4 = Float.valueOf(gravityScale);
                this.newGravityScale = valueOf4;
                this.oldGravityScale = valueOf4.floatValue();
                changed = true;
            } else {
                this.newGravityScale = null;
            }
            if (!linearVelocity.equals(this.oldLinearVelocity)) {
                this.newLinearVelocity = linearVelocity;
                this.oldLinearVelocity.set(linearVelocity);
                changed = true;
            } else {
                this.newLinearVelocity = null;
            }
            if (!Box2DUtils.equals(massData, this.oldMassData)) {
                this.newMassData = massData;
                updateOldMassData(massData);
                changed = true;
            } else {
                this.newMassData = null;
            }
            if (fixedRotation != this.oldFixedRotation) {
                this.oldFixedRotation = fixedRotation;
                this.newFixedRotation = Boolean.valueOf(fixedRotation);
                changed = true;
            } else {
                this.newFixedRotation = null;
            }
            if (bullet != this.oldBullet) {
                Boolean valueOf5 = Boolean.valueOf(bullet);
                this.newBullet = valueOf5;
                this.oldBullet = valueOf5.booleanValue();
                changed = true;
            } else {
                this.newBullet = null;
            }
            if (awake != this.oldAwake) {
                Boolean valueOf6 = Boolean.valueOf(awake);
                this.newAwake = valueOf6;
                this.oldAwake = valueOf6.booleanValue();
                changed = true;
            } else {
                this.newAwake = null;
            }
            if (active != this.oldActive) {
                this.oldActive = active;
                this.newActive = Boolean.valueOf(active);
                changed = true;
            } else {
                this.newActive = null;
            }
            if (sleepingAllowed != this.oldSleepingAllowed) {
                this.oldSleepingAllowed = sleepingAllowed;
                this.newSleepingAllowed = Boolean.valueOf(sleepingAllowed);
                changed = true;
            } else {
                this.newSleepingAllowed = null;
            }
            if (userData == null ? this.oldUserData != null : !userData.equals(this.oldUserData)) {
                this.newUserData = userData;
                this.oldUserData = userData;
                this.userDataChanged = true;
                return true;
            }
            this.newUserData = null;
            this.userDataChanged = false;
            return changed;
        }

        public void apply(Body body) {
            if (this.newTransform != null) {
                body.setTransform(this.newTransform.vals[0], this.newTransform.vals[1], this.newTransform.getRotation());
            }
            if (this.newType != null) {
                body.setType(this.newType);
            }
            if (this.newAngularDamping != null) {
                body.setAngularDamping(this.newAngularDamping.floatValue());
            }
            if (this.newAngularVelocity != null) {
                body.setAngularVelocity(this.newAngularVelocity.floatValue());
            }
            if (this.newLinearDamping != null) {
                body.setLinearDamping(this.newLinearDamping.floatValue());
            }
            if (this.newGravityScale != null) {
                body.setGravityScale(this.newGravityScale.floatValue());
            }
            if (this.newLinearVelocity != null) {
                body.setLinearVelocity(this.newLinearVelocity);
            }
            if (this.newMassData != null) {
                body.setMassData(this.newMassData);
            }
            if (this.newFixedRotation != null) {
                body.setFixedRotation(this.newFixedRotation.booleanValue());
            }
            if (this.newBullet != null) {
                body.setBullet(this.newBullet.booleanValue());
            }
            if (this.newAwake != null) {
                body.setAwake(this.newAwake.booleanValue());
            }
            if (this.newActive != null) {
                body.setActive(this.newActive.booleanValue());
            }
            if (this.newSleepingAllowed != null) {
                body.setSleepingAllowed(this.newSleepingAllowed.booleanValue());
            }
            if (this.userDataChanged) {
                body.setUserData(this.newUserData);
            }
        }

        public <C extends Change<Body>> boolean newValuesEqual(C other) {
            if (!(other instanceof BodyChange)) {
                return false;
            }
            BodyChange o = (BodyChange) other;
            if (!Objects.equals(this.newTransform, o.newTransform) || !Objects.equals(this.newType, o.newType) || !Objects.equals(this.newAngularDamping, o.newAngularDamping) || !Objects.equals(this.newAngularVelocity, o.newAngularVelocity) || !Objects.equals(this.newLinearDamping, o.newLinearDamping) || !Objects.equals(this.newGravityScale, o.newGravityScale) || !Objects.equals(this.newLinearVelocity, o.newLinearVelocity) || !Objects.equals(this.newMassData, o.newMassData) || !Objects.equals(this.newFixedRotation, o.newFixedRotation) || !Objects.equals(this.newBullet, o.newBullet) || !Objects.equals(this.newAwake, o.newAwake) || !Objects.equals(this.newActive, o.newActive) || !Objects.equals(this.newSleepingAllowed, o.newSleepingAllowed) || !Objects.equals(this.newUserData, o.newUserData)) {
                return false;
            }
            return true;
        }

        public void reset() {
            float[] fArr = this.oldTransform.vals;
            float[] fArr2 = this.oldTransform.vals;
            float[] fArr3 = this.oldTransform.vals;
            this.oldTransform.vals[3] = 0.0f;
            fArr3[2] = 0.0f;
            fArr2[1] = 0.0f;
            fArr[0] = 0.0f;
            this.oldType = null;
            this.oldAngularDamping = Animation.CurveTimeline.LINEAR;
            this.oldAngularVelocity = Animation.CurveTimeline.LINEAR;
            this.oldLinearDamping = Animation.CurveTimeline.LINEAR;
            this.oldGravityScale = Animation.CurveTimeline.LINEAR;
            this.oldLinearVelocity.setZero();
            this.oldMassData.mass = Animation.CurveTimeline.LINEAR;
            this.oldMassData.I = Animation.CurveTimeline.LINEAR;
            this.oldMassData.center.setZero();
            this.oldFixedRotation = false;
            this.oldBullet = false;
            this.oldAwake = false;
            this.oldActive = false;
            this.oldSleepingAllowed = false;
            this.oldUserData = null;
            this.newTransform = null;
            this.newType = null;
            this.newAngularDamping = null;
            this.newAngularVelocity = null;
            this.newLinearDamping = null;
            this.newGravityScale = null;
            this.newLinearVelocity = null;
            this.newMassData = null;
            this.newFixedRotation = null;
            this.newBullet = null;
            this.newAwake = null;
            this.newActive = null;
            this.newSleepingAllowed = null;
            this.newUserData = null;
            this.userDataChanged = false;
        }
    }

    public static class FixtureChange implements Change<Fixture> {
        private transient boolean destroyed;
        public Float newDensity;
        public Filter newFilter;
        public Float newFriction;
        public Float newRestitution;
        public Boolean newSensor;
        public Object newUserData;
        private transient Body oldBody;
        private transient float oldDensity;
        private final transient Filter oldFilter = new Filter();
        private transient float oldFriction;
        private transient float oldRestitution;
        private transient boolean oldSensor;
        private transient Object oldUserData;
        boolean userDataChanged;

        /* access modifiers changed from: package-private */
        public void created(Body body) {
            this.oldBody = body;
        }

        private void updateOldFilter(Filter newFilter2) {
            this.oldFilter.categoryBits = newFilter2.categoryBits;
            this.oldFilter.groupIndex = newFilter2.groupIndex;
            this.oldFilter.maskBits = newFilter2.maskBits;
        }

        public boolean isDestroyed() {
            return this.destroyed;
        }

        public boolean update(Fixture fixture) {
            Body body = fixture.getBody();
            if (body != this.oldBody) {
                this.destroyed = true;
                this.oldBody = body;
                return false;
            }
            float density = fixture.getDensity();
            float friction = fixture.getFriction();
            float restitution = fixture.getRestitution();
            Filter filter = fixture.getFilterData();
            boolean sensor = fixture.isSensor();
            Object userData = fixture.getUserData();
            boolean changed = false;
            if (density != this.oldDensity) {
                Float valueOf = Float.valueOf(density);
                this.newDensity = valueOf;
                this.oldDensity = valueOf.floatValue();
                changed = true;
            } else {
                this.newDensity = null;
            }
            if (friction != this.oldFriction) {
                Float valueOf2 = Float.valueOf(friction);
                this.newFriction = valueOf2;
                this.oldFriction = valueOf2.floatValue();
                changed = true;
            } else {
                this.newFriction = null;
            }
            if (restitution != this.oldRestitution) {
                Float valueOf3 = Float.valueOf(restitution);
                this.newRestitution = valueOf3;
                this.oldRestitution = valueOf3.floatValue();
                changed = true;
            } else {
                this.newRestitution = null;
            }
            if (!Box2DUtils.equals(filter, this.oldFilter)) {
                this.newFilter = filter;
                updateOldFilter(filter);
                changed = true;
            } else {
                this.newFilter = null;
            }
            if (sensor != this.oldSensor) {
                Boolean valueOf4 = Boolean.valueOf(sensor);
                this.newSensor = valueOf4;
                this.oldSensor = valueOf4.booleanValue();
                changed = true;
            } else {
                this.newSensor = null;
            }
            if (userData == null ? this.oldUserData != null : !userData.equals(this.oldUserData)) {
                this.newUserData = userData;
                this.oldUserData = userData;
                this.userDataChanged = true;
                return true;
            }
            this.newUserData = null;
            this.userDataChanged = false;
            return changed;
        }

        public void apply(Fixture fixture) {
            if (this.destroyed) {
                throw new IllegalStateException("destroyed FixtureChanges may not be applied");
            }
            if (this.newDensity != null) {
                fixture.setDensity(this.newDensity.floatValue());
            }
            if (this.newFriction != null) {
                fixture.setFriction(this.newFriction.floatValue());
            }
            if (this.newRestitution != null) {
                fixture.setRestitution(this.newRestitution.floatValue());
            }
            if (this.newFilter != null) {
                fixture.setFilterData(this.newFilter);
            }
            if (this.newSensor != null) {
                fixture.setSensor(this.newSensor.booleanValue());
            }
            if (this.userDataChanged) {
                fixture.setUserData(this.newUserData);
            }
        }

        public <C extends Change<Fixture>> boolean newValuesEqual(C other) {
            if (!(other instanceof FixtureChange)) {
                return false;
            }
            FixtureChange o = (FixtureChange) other;
            if (!Objects.equals(this.newDensity, o.newDensity) || !Objects.equals(this.newFriction, o.newFriction) || !Objects.equals(this.newRestitution, o.newRestitution) || !Objects.equals(this.newFilter, o.newFilter) || !Objects.equals(this.newSensor, o.newSensor) || !Objects.equals(this.newUserData, o.newUserData)) {
                return false;
            }
            return true;
        }

        public void reset() {
            this.oldBody = null;
            this.destroyed = false;
            this.oldDensity = Animation.CurveTimeline.LINEAR;
            this.oldFriction = Animation.CurveTimeline.LINEAR;
            this.oldRestitution = Animation.CurveTimeline.LINEAR;
            this.oldFilter.categoryBits = 1;
            this.oldFilter.maskBits = -1;
            this.oldFilter.groupIndex = 0;
            this.oldSensor = false;
            this.oldUserData = null;
            this.newDensity = null;
            this.newFriction = null;
            this.newRestitution = null;
            this.newFilter = null;
            this.newSensor = null;
            this.newUserData = null;
            this.userDataChanged = false;
        }
    }

    public static class JointChange<T extends Joint> implements Change<T> {
        public Object newUserData;
        private transient Object oldUserData;
        boolean userDataChanged;

        public static JointChange obtainFor(JointDef.JointType type) {
            Class<? extends JointChange> changeType;
            switch (type) {
                case RevoluteJoint:
                    changeType = RevoluteJointChange.class;
                    break;
                case PrismaticJoint:
                    changeType = PrismaticJointChange.class;
                    break;
                case DistanceJoint:
                    changeType = DistanceJointChange.class;
                    break;
                case PulleyJoint:
                    changeType = JointChange.class;
                    break;
                case MouseJoint:
                    changeType = MouseJointChange.class;
                    break;
                case GearJoint:
                    changeType = GearJointChange.class;
                    break;
                case WheelJoint:
                    changeType = WheelJointChange.class;
                    break;
                case WeldJoint:
                    changeType = WeldJointChange.class;
                    break;
                case FrictionJoint:
                    changeType = FrictionJointChange.class;
                    break;
                case RopeJoint:
                    changeType = RopeJointChange.class;
                    break;
                case MotorJoint:
                    changeType = MotorJointChange.class;
                    break;
                default:
                    changeType = JointChange.class;
                    break;
            }
            return (JointChange) Pools.obtain(changeType);
        }

        public boolean update(T joint) {
            Object userData = joint.getUserData();
            if (userData == null ? this.oldUserData != null : !userData.equals(this.oldUserData)) {
                this.newUserData = userData;
                this.oldUserData = userData;
                this.userDataChanged = true;
                return true;
            }
            this.newUserData = null;
            this.userDataChanged = false;
            return false;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void apply(T r2) {
            /*
                r1 = this;
                boolean r0 = r1.userDataChanged
                if (r0 == 0) goto L_0x0009
                java.lang.Object r0 = r1.newUserData
                r2.setUserData(r0)
            L_0x0009:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: net.dermetfan.gdx.physics.box2d.WorldObserver.JointChange.apply(com.badlogic.gdx.physics.box2d.Joint):void");
        }

        public <C extends Change<T>> boolean newValuesEqual(C other) {
            return (other instanceof JointChange) && Objects.equals(this.newUserData, ((JointChange) other).newUserData);
        }

        public void reset() {
            this.oldUserData = null;
            this.newUserData = null;
            this.userDataChanged = false;
        }
    }

    public static class RevoluteJointChange extends JointChange<RevoluteJoint> {
        public Float newLowerLimit;
        public Float newMaxMotorTorque;
        public Float newMotorSpeed;
        public Float newUpperLimit;
        private transient float oldLowerLimit;
        private transient float oldMaxMotorTorque;
        private transient float oldMotorSpeed;
        private transient float oldUpperLimit;

        public boolean update(RevoluteJoint joint) {
            float lowerLimit = joint.getLowerLimit();
            float upperLimit = joint.getUpperLimit();
            float maxMotorTorque = joint.getMaxMotorTorque();
            float motorSpeed = joint.getMotorSpeed();
            boolean changed = super.update((Joint) joint);
            if (lowerLimit != this.oldLowerLimit) {
                this.oldLowerLimit = lowerLimit;
                this.newLowerLimit = Float.valueOf(lowerLimit);
                changed = true;
            } else {
                this.newLowerLimit = null;
            }
            if (upperLimit != this.oldUpperLimit) {
                this.oldUpperLimit = upperLimit;
                this.newUpperLimit = Float.valueOf(upperLimit);
                changed = true;
            } else {
                this.newUpperLimit = null;
            }
            if (maxMotorTorque != this.oldMaxMotorTorque) {
                this.oldMaxMotorTorque = maxMotorTorque;
                this.newMaxMotorTorque = Float.valueOf(maxMotorTorque);
                changed = true;
            } else {
                this.newMaxMotorTorque = null;
            }
            if (motorSpeed != this.oldMotorSpeed) {
                this.oldMotorSpeed = motorSpeed;
                this.newMotorSpeed = Float.valueOf(motorSpeed);
                return true;
            }
            this.newMotorSpeed = null;
            return changed;
        }

        public void apply(RevoluteJoint joint) {
            super.apply((Joint) joint);
            if (!(this.newLowerLimit == null && this.newUpperLimit == null)) {
                joint.setLimits(this.newLowerLimit != null ? this.newLowerLimit.floatValue() : joint.getLowerLimit(), this.newUpperLimit != null ? this.newUpperLimit.floatValue() : joint.getUpperLimit());
            }
            if (this.newMaxMotorTorque != null) {
                joint.setMaxMotorTorque(this.newMaxMotorTorque.floatValue());
            }
            if (this.newMotorSpeed != null) {
                joint.setMotorSpeed(this.newMotorSpeed.floatValue());
            }
        }

        public <C extends Change<RevoluteJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof RevoluteJointChange)) {
                return false;
            }
            RevoluteJointChange o = (RevoluteJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newLowerLimit, o.newLowerLimit) || !Objects.equals(this.newUpperLimit, o.newUpperLimit) || !Objects.equals(this.newMaxMotorTorque, o.newMaxMotorTorque) || !Objects.equals(this.newMotorSpeed, o.newMotorSpeed)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldLowerLimit = Animation.CurveTimeline.LINEAR;
            this.oldUpperLimit = Animation.CurveTimeline.LINEAR;
            this.oldMaxMotorTorque = Animation.CurveTimeline.LINEAR;
            this.oldMotorSpeed = Animation.CurveTimeline.LINEAR;
            this.newLowerLimit = null;
            this.newUpperLimit = null;
            this.newMaxMotorTorque = null;
            this.newMotorSpeed = null;
        }
    }

    public static class PrismaticJointChange extends JointChange<PrismaticJoint> {
        public Float newLowerLimit;
        public Float newMaxMotorForce;
        public Float newMotorSpeed;
        public Float newUpperLimit;
        private transient float oldLowerLimit;
        private transient float oldMaxMotorTorque;
        private transient float oldMotorSpeed;
        private transient float oldUpperLimit;

        public boolean update(PrismaticJoint joint) {
            float lowerLimit = joint.getLowerLimit();
            float upperLimit = joint.getUpperLimit();
            float maxMotorTorque = joint.getMaxMotorForce();
            float motorSpeed = joint.getMotorSpeed();
            boolean changed = super.update((Joint) joint);
            if (lowerLimit != this.oldLowerLimit) {
                this.oldLowerLimit = lowerLimit;
                this.newLowerLimit = Float.valueOf(lowerLimit);
                changed = true;
            } else {
                this.newLowerLimit = null;
            }
            if (upperLimit != this.oldUpperLimit) {
                this.oldUpperLimit = upperLimit;
                this.newUpperLimit = Float.valueOf(upperLimit);
                changed = true;
            } else {
                this.newUpperLimit = null;
            }
            if (maxMotorTorque != this.oldMaxMotorTorque) {
                this.oldMaxMotorTorque = maxMotorTorque;
                this.newMaxMotorForce = Float.valueOf(maxMotorTorque);
                changed = true;
            } else {
                this.newMaxMotorForce = null;
            }
            if (motorSpeed != this.oldMotorSpeed) {
                this.oldMotorSpeed = motorSpeed;
                this.newMotorSpeed = Float.valueOf(motorSpeed);
                return true;
            }
            this.newMotorSpeed = null;
            return changed;
        }

        public void apply(PrismaticJoint joint) {
            super.apply((Joint) joint);
            if (!(this.newLowerLimit == null && this.newUpperLimit == null)) {
                joint.setLimits(this.newLowerLimit != null ? this.newLowerLimit.floatValue() : joint.getLowerLimit(), this.newUpperLimit != null ? this.newUpperLimit.floatValue() : joint.getUpperLimit());
            }
            if (this.newMaxMotorForce != null) {
                joint.setMaxMotorForce(this.newMaxMotorForce.floatValue());
            }
            if (this.newMotorSpeed != null) {
                joint.setMotorSpeed(this.newMotorSpeed.floatValue());
            }
        }

        public <C extends Change<PrismaticJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof PrismaticJointChange)) {
                return false;
            }
            PrismaticJointChange o = (PrismaticJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newLowerLimit, o.newLowerLimit) || !Objects.equals(this.newUpperLimit, o.newUpperLimit) || !Objects.equals(this.newMaxMotorForce, o.newMaxMotorForce) || !Objects.equals(this.newMotorSpeed, o.newMotorSpeed)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldLowerLimit = Animation.CurveTimeline.LINEAR;
            this.oldUpperLimit = Animation.CurveTimeline.LINEAR;
            this.oldMaxMotorTorque = Animation.CurveTimeline.LINEAR;
            this.oldMotorSpeed = Animation.CurveTimeline.LINEAR;
            this.newLowerLimit = null;
            this.newUpperLimit = null;
            this.newMaxMotorForce = null;
            this.newMotorSpeed = null;
        }
    }

    public static class DistanceJointChange extends JointChange<DistanceJoint> {
        public Float newDampingRatio;
        public Float newFrequency;
        public Float newLength;
        private transient float oldDampingRatio;
        private transient float oldFrequency;
        private transient float oldLength;

        public boolean update(DistanceJoint joint) {
            float dampingRatio = joint.getDampingRatio();
            float frequency = joint.getFrequency();
            float length = joint.getLength();
            boolean changed = super.update((Joint) joint);
            if (dampingRatio != this.oldDampingRatio) {
                this.oldDampingRatio = dampingRatio;
                this.newDampingRatio = Float.valueOf(dampingRatio);
                changed = true;
            } else {
                this.newDampingRatio = null;
            }
            if (frequency != this.oldFrequency) {
                this.oldFrequency = frequency;
                this.newFrequency = Float.valueOf(frequency);
                changed = true;
            } else {
                this.newFrequency = null;
            }
            if (length != this.oldLength) {
                this.oldLength = length;
                this.newLength = Float.valueOf(length);
                return true;
            }
            this.newLength = null;
            return changed;
        }

        public void apply(DistanceJoint joint) {
            super.apply((Joint) joint);
            if (this.newDampingRatio != null) {
                joint.setDampingRatio(this.newDampingRatio.floatValue());
            }
            if (this.newFrequency != null) {
                joint.setFrequency(this.newFrequency.floatValue());
            }
            if (this.newLength != null) {
                joint.setLength(this.newLength.floatValue());
            }
        }

        public <C extends Change<DistanceJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof DistanceJointChange)) {
                return false;
            }
            DistanceJointChange o = (DistanceJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newDampingRatio, o.newDampingRatio) || !Objects.equals(this.newFrequency, o.newFrequency) || !Objects.equals(this.newLength, o.newLength)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldDampingRatio = Animation.CurveTimeline.LINEAR;
            this.oldFrequency = Animation.CurveTimeline.LINEAR;
            this.oldLength = Animation.CurveTimeline.LINEAR;
            this.newDampingRatio = null;
            this.newFrequency = null;
            this.newLength = null;
        }
    }

    public static class MouseJointChange extends JointChange<MouseJoint> {
        public Float newDampingRatio;
        public Float newFrequency;
        public Float newMaxForce;
        public Vector2 newTarget;
        private transient float oldDampingRatio;
        private transient float oldFrequency;
        private transient float oldMaxForce;
        private final transient Vector2 oldTarget = new Vector2();

        public boolean update(MouseJoint joint) {
            float dampingRatio = joint.getDampingRatio();
            float frequency = joint.getFrequency();
            float maxForce = joint.getMaxForce();
            Vector2 target = joint.getTarget();
            boolean changed = super.update((Joint) joint);
            if (dampingRatio != this.oldDampingRatio) {
                this.oldDampingRatio = dampingRatio;
                this.newDampingRatio = Float.valueOf(dampingRatio);
                changed = true;
            } else {
                this.newDampingRatio = null;
            }
            if (frequency != this.oldFrequency) {
                this.oldFrequency = frequency;
                this.newFrequency = Float.valueOf(frequency);
                changed = true;
            } else {
                this.newFrequency = null;
            }
            if (maxForce != this.oldMaxForce) {
                this.oldMaxForce = maxForce;
                this.newMaxForce = Float.valueOf(maxForce);
                changed = true;
            } else {
                this.newMaxForce = null;
            }
            if (!target.equals(this.oldTarget)) {
                Vector2 vector2 = this.oldTarget;
                this.newTarget = target;
                vector2.set(target);
                return true;
            }
            this.newTarget = null;
            return changed;
        }

        public void apply(MouseJoint joint) {
            super.apply((Joint) joint);
            if (this.newDampingRatio != null) {
                joint.setDampingRatio(this.newDampingRatio.floatValue());
            }
            if (this.newFrequency != null) {
                joint.setFrequency(this.newFrequency.floatValue());
            }
            if (this.newMaxForce != null) {
                joint.setMaxForce(this.newMaxForce.floatValue());
            }
            if (this.newTarget != null) {
                joint.setTarget(this.newTarget);
            }
        }

        public <C extends Change<MouseJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof MouseJointChange)) {
                return false;
            }
            MouseJointChange o = (MouseJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newDampingRatio, o.newDampingRatio) || !Objects.equals(this.newFrequency, o.newFrequency) || !Objects.equals(this.newMaxForce, o.newMaxForce)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldDampingRatio = Animation.CurveTimeline.LINEAR;
            this.oldFrequency = Animation.CurveTimeline.LINEAR;
            this.oldMaxForce = Animation.CurveTimeline.LINEAR;
            this.oldTarget.setZero();
            this.newDampingRatio = null;
            this.newFrequency = null;
            this.newMaxForce = null;
            this.newTarget = null;
        }
    }

    public static class GearJointChange extends JointChange<GearJoint> {
        public Float newRatio;
        private transient float oldRatio;

        public boolean update(GearJoint joint) {
            float ratio = joint.getRatio();
            boolean changed = super.update((Joint) joint);
            if (ratio != this.oldRatio) {
                this.oldRatio = ratio;
                this.newRatio = Float.valueOf(ratio);
                return true;
            }
            this.newRatio = null;
            return changed;
        }

        public void apply(GearJoint joint) {
            super.apply((Joint) joint);
            if (this.newRatio != null) {
                joint.setRatio(this.newRatio.floatValue());
            }
        }

        public <C extends Change<GearJoint>> boolean newValuesEqual(C other) {
            return (other instanceof GearJointChange) && super.newValuesEqual(other) && Objects.equals(this.newRatio, ((GearJointChange) other).newRatio);
        }

        public void reset() {
            super.reset();
            this.oldRatio = Animation.CurveTimeline.LINEAR;
            this.newRatio = null;
        }
    }

    public static class WheelJointChange extends JointChange<WheelJoint> {
        public Float newMaxMotorTorque;
        public Float newMotorSpeed;
        public Float newSpringDampingRatio;
        public Float newSpringFrequencyHz;
        private transient float oldMaxMotorTorque;
        private transient float oldMotorSpeed;
        private transient float oldSpringDampingRatio;
        private transient float oldSpringFrequencyHz;

        public boolean update(WheelJoint joint) {
            float sprintDampingRatio = joint.getSpringDampingRatio();
            float springFrequencyHz = joint.getSpringFrequencyHz();
            float maxMotorTorque = joint.getMaxMotorTorque();
            float motorSpeed = joint.getMotorSpeed();
            boolean changed = super.update((Joint) joint);
            if (sprintDampingRatio != this.oldSpringDampingRatio) {
                this.oldSpringDampingRatio = sprintDampingRatio;
                this.newSpringDampingRatio = Float.valueOf(sprintDampingRatio);
                changed = true;
            } else {
                this.newSpringDampingRatio = null;
            }
            if (springFrequencyHz != this.oldSpringFrequencyHz) {
                this.oldSpringFrequencyHz = springFrequencyHz;
                this.newSpringFrequencyHz = Float.valueOf(springFrequencyHz);
                changed = true;
            } else {
                this.newSpringFrequencyHz = null;
            }
            if (maxMotorTorque != this.oldMaxMotorTorque) {
                this.oldMaxMotorTorque = maxMotorTorque;
                this.newMaxMotorTorque = Float.valueOf(maxMotorTorque);
                changed = true;
            } else {
                this.newMaxMotorTorque = null;
            }
            if (motorSpeed != this.oldMotorSpeed) {
                this.oldMotorSpeed = motorSpeed;
                this.newMotorSpeed = Float.valueOf(motorSpeed);
                return true;
            }
            this.newMotorSpeed = null;
            return changed;
        }

        public void apply(WheelJoint joint) {
            super.apply((Joint) joint);
            if (this.newSpringDampingRatio != null) {
                joint.setSpringDampingRatio(this.newSpringDampingRatio.floatValue());
            }
            if (this.newSpringFrequencyHz != null) {
                joint.setSpringFrequencyHz(this.newSpringFrequencyHz.floatValue());
            }
            if (this.newMaxMotorTorque != null) {
                joint.setMaxMotorTorque(this.newMaxMotorTorque.floatValue());
            }
            if (this.newMotorSpeed != null) {
                joint.setMotorSpeed(this.newMotorSpeed.floatValue());
            }
        }

        public <C extends Change<WheelJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof WheelJointChange)) {
                return false;
            }
            WheelJointChange o = (WheelJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newSpringDampingRatio, o.newSpringDampingRatio) || !Objects.equals(this.newSpringFrequencyHz, o.newSpringFrequencyHz) || !Objects.equals(this.newMaxMotorTorque, o.newMaxMotorTorque) || !Objects.equals(this.newMotorSpeed, o.newMotorSpeed)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldSpringDampingRatio = Animation.CurveTimeline.LINEAR;
            this.oldSpringFrequencyHz = Animation.CurveTimeline.LINEAR;
            this.oldMaxMotorTorque = Animation.CurveTimeline.LINEAR;
            this.oldMotorSpeed = Animation.CurveTimeline.LINEAR;
            this.newSpringDampingRatio = null;
            this.newSpringFrequencyHz = null;
            this.newMaxMotorTorque = null;
            this.newMotorSpeed = null;
        }
    }

    public static class WeldJointChange extends JointChange<WeldJoint> {
        public Float newDampingRatio;
        public Float newFrequency;
        private transient float oldDampingRatio;
        private transient float oldFrequency;

        public boolean update(WeldJoint joint) {
            float dampingRatio = joint.getDampingRatio();
            float frequency = joint.getFrequency();
            boolean changed = super.update((Joint) joint);
            if (dampingRatio != this.oldDampingRatio) {
                this.oldDampingRatio = dampingRatio;
                this.newDampingRatio = Float.valueOf(dampingRatio);
                changed = true;
            } else {
                this.newDampingRatio = null;
            }
            if (frequency != this.oldFrequency) {
                this.oldFrequency = frequency;
                this.newFrequency = Float.valueOf(frequency);
                return true;
            }
            this.newFrequency = null;
            return changed;
        }

        public void apply(WeldJoint joint) {
            super.apply((Joint) joint);
            if (this.newDampingRatio != null) {
                joint.setDampingRatio(this.newDampingRatio.floatValue());
            }
            if (this.newFrequency != null) {
                joint.setFrequency(this.newFrequency.floatValue());
            }
        }

        public <C extends Change<WeldJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof WeldJointChange)) {
                return false;
            }
            WeldJointChange o = (WeldJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newDampingRatio, o.newDampingRatio) || !Objects.equals(this.newFrequency, o.newFrequency)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldDampingRatio = Animation.CurveTimeline.LINEAR;
            this.oldFrequency = Animation.CurveTimeline.LINEAR;
            this.newDampingRatio = null;
            this.newFrequency = null;
        }
    }

    public static class FrictionJointChange extends JointChange<FrictionJoint> {
        public Float newMaxForce;
        public Float newMaxTorque;
        private transient float oldMaxForce;
        private transient float oldMaxTorque;

        public boolean update(FrictionJoint joint) {
            float maxForce = joint.getMaxForce();
            float maxTorque = joint.getMaxTorque();
            boolean changed = super.update((Joint) joint);
            if (maxForce != this.oldMaxForce) {
                this.oldMaxForce = maxForce;
                this.newMaxForce = Float.valueOf(maxForce);
                changed = true;
            } else {
                this.newMaxForce = null;
            }
            if (maxTorque != this.oldMaxTorque) {
                this.oldMaxTorque = maxTorque;
                this.newMaxTorque = Float.valueOf(maxTorque);
                return true;
            }
            this.newMaxTorque = null;
            return changed;
        }

        public void apply(FrictionJoint joint) {
            super.apply((Joint) joint);
            if (this.newMaxForce != null) {
                joint.setMaxForce(this.newMaxForce.floatValue());
            }
            if (this.newMaxTorque != null) {
                joint.setMaxTorque(this.newMaxTorque.floatValue());
            }
        }

        public <C extends Change<FrictionJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof FrictionJointChange)) {
                return false;
            }
            FrictionJointChange o = (FrictionJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newMaxForce, o.newMaxForce) || !Objects.equals(this.newMaxTorque, o.newMaxTorque)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldMaxForce = Animation.CurveTimeline.LINEAR;
            this.oldMaxTorque = Animation.CurveTimeline.LINEAR;
            this.newMaxForce = null;
            this.newMaxTorque = null;
        }
    }

    public static class RopeJointChange extends JointChange<RopeJoint> {
        public Float newMaxLength;
        private transient float oldMaxLength;

        public boolean update(RopeJoint joint) {
            float maxLength = joint.getMaxLength();
            boolean changed = super.update((Joint) joint);
            if (maxLength != this.oldMaxLength) {
                this.oldMaxLength = maxLength;
                this.newMaxLength = Float.valueOf(maxLength);
                return true;
            }
            this.newMaxLength = null;
            return changed;
        }

        public void apply(RopeJoint joint) {
            super.apply((Joint) joint);
            if (this.newMaxLength != null) {
                joint.setMaxLength(this.newMaxLength.floatValue());
            }
        }

        public <C extends Change<RopeJoint>> boolean newValuesEqual(C other) {
            return (other instanceof RopeJointChange) && super.newValuesEqual(other) && Objects.equals(this.newMaxLength, ((RopeJointChange) other).newMaxLength);
        }

        public void reset() {
            super.reset();
            this.oldMaxLength = Animation.CurveTimeline.LINEAR;
            this.newMaxLength = null;
        }
    }

    public static class MotorJointChange extends JointChange<MotorJoint> {
        public Float newAngularOffset;
        public Float newCorrectionFactor;
        public Vector2 newLinearOffset;
        public Float newMaxForce;
        public Float newMaxTorque;
        private transient float oldAngularOffset;
        private transient float oldCorrectionFactor;
        private final transient Vector2 oldLinearOffset = new Vector2();
        private transient float oldMaxForce;
        private transient float oldMaxTorque;

        public boolean update(MotorJoint joint) {
            float maxForce = joint.getMaxForce();
            float maxTorque = joint.getMaxTorque();
            float correctionFactor = joint.getCorrectionFactor();
            float angularOffset = joint.getAngularOffset();
            Vector2 linearOffset = joint.getLinearOffset();
            boolean changed = super.update((Joint) joint);
            if (maxForce != this.oldMaxForce) {
                this.oldMaxForce = maxForce;
                this.newMaxForce = Float.valueOf(maxForce);
                changed = true;
            } else {
                this.newMaxForce = null;
            }
            if (maxTorque != this.oldMaxTorque) {
                this.oldMaxTorque = maxTorque;
                this.newMaxTorque = Float.valueOf(maxTorque);
                changed = true;
            } else {
                this.newMaxTorque = null;
            }
            if (correctionFactor != this.oldCorrectionFactor) {
                this.oldCorrectionFactor = correctionFactor;
                this.newCorrectionFactor = Float.valueOf(correctionFactor);
                changed = true;
            } else {
                this.newCorrectionFactor = null;
            }
            if (angularOffset != this.oldAngularOffset) {
                this.oldAngularOffset = angularOffset;
                this.newAngularOffset = Float.valueOf(angularOffset);
                changed = true;
            } else {
                this.newAngularOffset = null;
            }
            if (!linearOffset.equals(this.oldLinearOffset)) {
                Vector2 vector2 = this.oldLinearOffset;
                this.newLinearOffset = linearOffset;
                vector2.set(linearOffset);
                return true;
            }
            this.newLinearOffset = null;
            return changed;
        }

        public void apply(MotorJoint joint) {
            super.apply((Joint) joint);
            if (this.newMaxForce != null) {
                joint.setMaxForce(this.newMaxForce.floatValue());
            }
            if (this.newMaxTorque != null) {
                joint.setMaxForce(this.newMaxTorque.floatValue());
            }
            if (this.newCorrectionFactor != null) {
                joint.setCorrectionFactor(this.newCorrectionFactor.floatValue());
            }
            if (this.newAngularOffset != null) {
                joint.setAngularOffset(this.newAngularOffset.floatValue());
            }
            if (this.newLinearOffset != null) {
                joint.setLinearOffset(this.newLinearOffset);
            }
        }

        public <C extends Change<MotorJoint>> boolean newValuesEqual(C other) {
            if (!(other instanceof MotorJointChange)) {
                return false;
            }
            MotorJointChange o = (MotorJointChange) other;
            if (!super.newValuesEqual(other) || !Objects.equals(this.newAngularOffset, o.newAngularOffset) || !Objects.equals(this.newCorrectionFactor, o.newCorrectionFactor) || !Objects.equals(this.newLinearOffset, o.newLinearOffset) || !Objects.equals(this.newMaxForce, o.newMaxForce)) {
                return false;
            }
            return true;
        }

        public void reset() {
            super.reset();
            this.oldMaxForce = Animation.CurveTimeline.LINEAR;
            this.oldMaxTorque = Animation.CurveTimeline.LINEAR;
            this.oldCorrectionFactor = Animation.CurveTimeline.LINEAR;
            this.oldAngularOffset = Animation.CurveTimeline.LINEAR;
            this.oldLinearOffset.setZero();
            this.newMaxForce = null;
            this.newMaxTorque = null;
            this.newCorrectionFactor = null;
            this.newAngularOffset = null;
            this.newLinearOffset = null;
        }
    }
}
