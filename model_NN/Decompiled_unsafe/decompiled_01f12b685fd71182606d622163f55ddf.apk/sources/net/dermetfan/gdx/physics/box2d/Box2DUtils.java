package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.FrictionJoint;
import com.badlogic.gdx.physics.box2d.joints.FrictionJointDef;
import com.badlogic.gdx.physics.box2d.joints.GearJoint;
import com.badlogic.gdx.physics.box2d.joints.GearJointDef;
import com.badlogic.gdx.physics.box2d.joints.MotorJoint;
import com.badlogic.gdx.physics.box2d.joints.MotorJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.PulleyJoint;
import com.badlogic.gdx.physics.box2d.joints.PulleyJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJoint;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJoint;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.physics.box2d.joints.WheelJoint;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Arrays;
import java.util.Iterator;
import net.dermetfan.gdx.math.GeometryUtils;
import net.dermetfan.gdx.math.MathUtils;
import net.dermetfan.gdx.utils.ArrayUtils;
import net.dermetfan.utils.Pair;

public class Box2DUtils extends com.badlogic.gdx.physics.box2d.Box2DUtils {
    public static boolean autoCache = true;
    public static final ObjectMap<Shape, ShapeCache> cache = new ObjectMap<>();
    public static boolean checkPreconditions = true;
    public static final byte maxPolygonVertices = 8;
    public static final float maxTranslation = 2.0f;
    public static final float minExclusivePolygonArea = 1.1920929E-7f;
    private static final Polygon polygon = new Polygon(new float[8]);
    private static final Array<Vector2> tmpVector2Array = new Array<>(8);
    private static final Vector2 vec2_0 = new Vector2();
    private static final Vector2 vec2_1 = new Vector2();

    public static class ShapeCache {
        public final float height;
        public final float maxX;
        public final float maxY;
        public final float minX;
        public final float minY;
        public final Vector2[] vertices;
        public final float width;

        public ShapeCache(Vector2[] vertices2, float width2, float height2, float minX2, float maxX2, float minY2, float maxY2) {
            this.vertices = vertices2;
            this.width = width2;
            this.height = height2;
            this.minX = minX2;
            this.maxX = maxX2;
            this.minY = minY2;
            this.maxY = maxY2;
        }
    }

    public static ShapeCache cache(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape);
        }
        Vector2[] vertices = vertices0(shape);
        Vector2[] cachedVertices = new Vector2[vertices.length];
        System.arraycopy(vertices, 0, cachedVertices, 0, vertices.length);
        ShapeCache results = new ShapeCache(cachedVertices, width0(shape), height0(shape), minX0(shape), maxX0(shape), minY0(shape), maxY0(shape));
        cache.put(shape, results);
        return results;
    }

    private static Vector2[] vertices0(Shape shape) {
        switch (shape.getType()) {
            case Polygon:
                PolygonShape polygonShape = (PolygonShape) shape;
                Vector2[] vertices = new Vector2[polygonShape.getVertexCount()];
                for (int i = 0; i < vertices.length; i++) {
                    vertices[i] = new Vector2();
                    polygonShape.getVertex(i, vertices[i]);
                }
                return vertices;
            case Edge:
                EdgeShape edgeShape = (EdgeShape) shape;
                edgeShape.getVertex1(vec2_0);
                edgeShape.getVertex2(vec2_1);
                return new Vector2[]{new Vector2(vec2_0), new Vector2(vec2_1)};
            case Chain:
                ChainShape chainShape = (ChainShape) shape;
                Vector2[] vertices2 = new Vector2[chainShape.getVertexCount()];
                for (int i2 = 0; i2 < vertices2.length; i2++) {
                    vertices2[i2] = new Vector2();
                    chainShape.getVertex(i2, vertices2[i2]);
                }
                return vertices2;
            case Circle:
                CircleShape circleShape = (CircleShape) shape;
                Vector2 position = circleShape.getPosition();
                float radius = circleShape.getRadius();
                return new Vector2[]{new Vector2(position.x - radius, position.y - radius), new Vector2(position.x + radius, position.y - radius), new Vector2(position.x + radius, position.y + radius), new Vector2(position.x - radius, position.y + radius)};
            default:
                throw new IllegalArgumentException("shapes of the type '" + shape.getType().name() + "' are not supported");
        }
    }

    private static float minX0(Shape shape) {
        if (shape instanceof CircleShape) {
            return ((CircleShape) shape).getPosition().x - shape.getRadius();
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices0(shape));
        return MathUtils.min(GeometryUtils.filterX(tmpVector2Array));
    }

    private static float minY0(Shape shape) {
        if (shape instanceof CircleShape) {
            return ((CircleShape) shape).getPosition().y - shape.getRadius();
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices0(shape));
        return MathUtils.min(GeometryUtils.filterY(tmpVector2Array));
    }

    private static float maxX0(Shape shape) {
        if (shape instanceof CircleShape) {
            return ((CircleShape) shape).getPosition().x + shape.getRadius();
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices0(shape));
        return MathUtils.max(GeometryUtils.filterX(tmpVector2Array));
    }

    private static float maxY0(Shape shape) {
        if (shape instanceof CircleShape) {
            return ((CircleShape) shape).getPosition().y + shape.getRadius();
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices0(shape));
        return MathUtils.max(GeometryUtils.filterY(tmpVector2Array));
    }

    private static float width0(Shape shape) {
        if (shape.getType() == Shape.Type.Circle) {
            return shape.getRadius() * 2.0f;
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices0(shape));
        return MathUtils.amplitude2(GeometryUtils.filterX(tmpVector2Array));
    }

    private static float height0(Shape shape) {
        if (shape.getType() == Shape.Type.Circle) {
            return shape.getRadius() * 2.0f;
        }
        tmpVector2Array.clear();
        tmpVector2Array.addAll(vertices0(shape));
        return MathUtils.amplitude2(GeometryUtils.filterY(tmpVector2Array));
    }

    private static Vector2 size0(Shape shape) {
        return vec2_0.set(width0(shape), height0(shape));
    }

    public static Vector2[] vertices(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).vertices;
        }
        if (autoCache) {
            return cache(shape).vertices;
        }
        return vertices0(shape);
    }

    public static float minX(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).minX;
        }
        if (autoCache) {
            return cache(shape).minX;
        }
        return minX0(shape);
    }

    public static float minY(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).minY;
        }
        if (autoCache) {
            return cache(shape).minY;
        }
        return minY0(shape);
    }

    public static float maxX(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).maxX;
        }
        if (autoCache) {
            return cache(shape).maxX;
        }
        return maxX0(shape);
    }

    public static float maxY(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).maxY;
        }
        if (autoCache) {
            return cache(shape).maxY;
        }
        return maxY0(shape);
    }

    public static float width(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).width;
        }
        if (autoCache) {
            return cache(shape).width;
        }
        return width0(shape);
    }

    public static float height(Shape shape) {
        if (cache.containsKey(shape)) {
            return cache.get(shape).height;
        }
        if (autoCache) {
            return cache(shape).height;
        }
        return height0(shape);
    }

    public static Vector2 size(Shape shape) {
        ShapeCache results = cache.containsKey(shape) ? cache.get(shape) : autoCache ? cache(shape) : null;
        return results != null ? vec2_0.set(results.width, results.height) : size0(shape);
    }

    public static Vector2[] vertices(Fixture fixture) {
        return vertices(fixture.getShape());
    }

    public static float minX(Fixture fixture) {
        return minX(fixture.getShape());
    }

    public static float minY(Fixture fixture) {
        return minY(fixture.getShape());
    }

    public static float maxX(Fixture fixture) {
        return maxX(fixture.getShape());
    }

    public static float maxY(Fixture fixture) {
        return maxY(fixture.getShape());
    }

    public static float width(Fixture fixture) {
        return width(fixture.getShape());
    }

    public static float height(Fixture fixture) {
        return height(fixture.getShape());
    }

    public static Vector2 size(Fixture fixture) {
        return size(fixture.getShape());
    }

    public static Vector2[][] fixtureVertices(Body body) {
        Array<Fixture> fixtures = body.getFixtureList();
        Vector2[][] vertices = new Vector2[fixtures.size][];
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = vertices(fixtures.get(i));
        }
        return vertices;
    }

    @Deprecated
    public static Vector2[] vertices(Body body) {
        Vector2[][] fixtureVertices = fixtureVertices(body);
        int vertexCount = 0;
        for (Vector2[] fixtureVertice : fixtureVertices) {
            vertexCount += fixtureVertice.length;
        }
        Vector2[] vertices = new Vector2[vertexCount];
        int vi = -1;
        for (Vector2[] verts : fixtureVertices) {
            for (Vector2 vertice : fixtureVertices[r9]) {
                vi++;
                vertices[vi] = vertice;
            }
        }
        return vertices;
    }

    public static float minX(Body body) {
        float x = Float.POSITIVE_INFINITY;
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            float tmp = minX(it.next());
            if (tmp < x) {
                x = tmp;
            }
        }
        return x;
    }

    public static float minY(Body body) {
        float y = Float.POSITIVE_INFINITY;
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            float tmp = minY(it.next());
            if (tmp < y) {
                y = tmp;
            }
        }
        return y;
    }

    public static float maxX(Body body) {
        float x = Float.NEGATIVE_INFINITY;
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            float tmp = maxX(it.next());
            if (tmp > x) {
                x = tmp;
            }
        }
        return x;
    }

    public static float maxY(Body body) {
        float y = Float.NEGATIVE_INFINITY;
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            float tmp = maxY(it.next());
            if (tmp > y) {
                y = tmp;
            }
        }
        return y;
    }

    public static float width(Body body) {
        return Math.abs(maxX(body) - minX(body));
    }

    public static float height(Body body) {
        return Math.abs(maxY(body) - minY(body));
    }

    public static Vector2 size(Body body) {
        return vec2_0.set(width(body), height(body));
    }

    public static Vector2 positionRelative(CircleShape shape) {
        return shape.getPosition();
    }

    public static Vector2 positionRelative(Shape shape, float rotation) {
        if (shape instanceof CircleShape) {
            return positionRelative((CircleShape) shape);
        }
        return vec2_0.set(minX(shape) + (width(shape) / 2.0f), minY(shape) + (height(shape) / 2.0f)).rotate(rotation);
    }

    public static Vector2 position(Shape shape, Body body) {
        return body.getPosition().add(positionRelative(shape, body.getAngle() * 57.295776f));
    }

    public static Vector2 positionRelative(Fixture fixture) {
        return positionRelative(fixture.getShape(), fixture.getBody().getAngle() * 57.295776f);
    }

    public static Vector2 position(Fixture fixture) {
        return position(fixture.getShape(), fixture.getBody());
    }

    public static Rectangle aabb(Shape shape, float rotation, Rectangle aabb) {
        if (com.badlogic.gdx.math.MathUtils.isZero(rotation)) {
            return aabb.set(minX(shape), minY(shape), width(shape), height(shape));
        }
        Vector2[] v2Vertices = vertices(shape);
        GeometryUtils.reset(polygon);
        float[] vertices = polygon.getVertices();
        if (vertices.length < v2Vertices.length * 2) {
            vertices = new float[(v2Vertices.length * 2)];
        }
        for (int i = 0; i < vertices.length; i++) {
            int v2i = Math.min(i / 2, v2Vertices.length - 1);
            vertices[i] = i % 2 == 0 ? v2Vertices[v2i].x : v2Vertices[v2i].y;
        }
        polygon.setVertices(vertices);
        if (shape.getType() == Shape.Type.Circle) {
            polygon.setOrigin(GeometryUtils.minX(vertices) + (GeometryUtils.width(vertices) / 2.0f), GeometryUtils.minY(vertices) + (GeometryUtils.height(vertices) / 2.0f));
            polygon.setRotation((-rotation) * 57.295776f);
            polygon.setVertices(polygon.getTransformedVertices());
            polygon.setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        }
        polygon.setRotation(rotation * 57.295776f);
        return aabb.set(polygon.getBoundingRectangle());
    }

    public static Rectangle aabb(Shape shape, float rotation) {
        return aabb(shape, rotation, polygon.getBoundingRectangle());
    }

    public static Rectangle aabb(Fixture fixture, Rectangle aabb) {
        return aabb(fixture.getShape(), fixture.getBody().getAngle(), aabb).setPosition(fixture.getBody().getPosition().add(aabb.x, aabb.y));
    }

    public static Rectangle aabb(Fixture fixture) {
        return aabb(fixture, polygon.getBoundingRectangle());
    }

    public static Rectangle aabb(Body body, Rectangle aabb) {
        float minX = Float.POSITIVE_INFINITY;
        float minY = Float.POSITIVE_INFINITY;
        float maxX = Float.NEGATIVE_INFINITY;
        float maxY = Float.NEGATIVE_INFINITY;
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            aabb(it.next(), aabb);
            if (aabb.x < minX) {
                minX = aabb.x;
            }
            if (aabb.x + aabb.width > maxX) {
                maxX = aabb.x + aabb.width;
            }
            if (aabb.y < minY) {
                minY = aabb.y;
            }
            if (aabb.y + aabb.height > maxY) {
                maxY = aabb.y + aabb.height;
            }
        }
        return aabb.set(minX, minY, maxX - minX, maxY - minY);
    }

    public static Rectangle aabb(Body body) {
        return aabb(body, polygon.getBoundingRectangle());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.clone(com.badlogic.gdx.physics.box2d.Body, boolean):com.badlogic.gdx.physics.box2d.Body
     arg types: [com.badlogic.gdx.physics.box2d.Body, int]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.clone(com.badlogic.gdx.physics.box2d.Fixture, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.Fixture
      net.dermetfan.gdx.physics.box2d.Box2DUtils.clone(com.badlogic.gdx.physics.box2d.Body, boolean):com.badlogic.gdx.physics.box2d.Body */
    public static Body clone(Body body) {
        return clone(body, false);
    }

    public static Body clone(Body body, boolean shapes) {
        Body clone = body.getWorld().createBody(createDef(body));
        clone.setUserData(body.getUserData());
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            clone(it.next(), clone, shapes);
        }
        return clone;
    }

    public static Fixture clone(Fixture fixture, Body body) {
        return clone(fixture, body, false);
    }

    public static Fixture clone(Fixture fixture, Body body, boolean shape) {
        FixtureDef fixtureDef = createDef(fixture);
        if (shape) {
            fixtureDef.shape = clone(fixture.getShape());
        }
        Fixture clone = body.createFixture(fixtureDef);
        clone.setUserData(clone.getUserData());
        return clone;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static <T extends com.badlogic.gdx.physics.box2d.Shape> T clone(T r13) {
        /*
            int[] r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.AnonymousClass1.$SwitchMap$com$badlogic$gdx$physics$box2d$Shape$Type
            com.badlogic.gdx.physics.box2d.Shape$Type r12 = r13.getType()
            int r12 = r12.ordinal()
            r11 = r11[r12]
            switch(r11) {
                case 1: goto L_0x002e;
                case 2: goto L_0x0060;
                case 3: goto L_0x0080;
                case 4: goto L_0x0011;
                default: goto L_0x000f;
            }
        L_0x000f:
            r3 = 0
        L_0x0010:
            return r3
        L_0x0011:
            com.badlogic.gdx.physics.box2d.CircleShape r3 = new com.badlogic.gdx.physics.box2d.CircleShape
            r3.<init>()
            r11 = r3
            com.badlogic.gdx.physics.box2d.CircleShape r11 = (com.badlogic.gdx.physics.box2d.CircleShape) r11
            r2 = r11
            com.badlogic.gdx.physics.box2d.CircleShape r2 = (com.badlogic.gdx.physics.box2d.CircleShape) r2
            r11 = r13
            com.badlogic.gdx.physics.box2d.CircleShape r11 = (com.badlogic.gdx.physics.box2d.CircleShape) r11
            com.badlogic.gdx.math.Vector2 r11 = r11.getPosition()
            r2.setPosition(r11)
        L_0x0026:
            float r11 = r13.getRadius()
            r3.setRadius(r11)
            goto L_0x0010
        L_0x002e:
            com.badlogic.gdx.physics.box2d.PolygonShape r3 = new com.badlogic.gdx.physics.box2d.PolygonShape
            r3.<init>()
            r11 = r3
            com.badlogic.gdx.physics.box2d.PolygonShape r11 = (com.badlogic.gdx.physics.box2d.PolygonShape) r11
            r9 = r11
            com.badlogic.gdx.physics.box2d.PolygonShape r9 = (com.badlogic.gdx.physics.box2d.PolygonShape) r9
            r8 = r13
            com.badlogic.gdx.physics.box2d.PolygonShape r8 = (com.badlogic.gdx.physics.box2d.PolygonShape) r8
            int r11 = r8.getVertexCount()
            float[] r10 = new float[r11]
            r6 = 0
        L_0x0043:
            int r11 = r10.length
            if (r6 >= r11) goto L_0x005c
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            r8.getVertex(r6, r11)
            int r7 = r6 + 1
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            float r11 = r11.x
            r10[r6] = r11
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            float r11 = r11.y
            r10[r7] = r11
            int r6 = r7 + 1
            goto L_0x0043
        L_0x005c:
            r9.set(r10)
            goto L_0x0026
        L_0x0060:
            com.badlogic.gdx.physics.box2d.EdgeShape r3 = new com.badlogic.gdx.physics.box2d.EdgeShape
            r3.<init>()
            r11 = r3
            com.badlogic.gdx.physics.box2d.EdgeShape r11 = (com.badlogic.gdx.physics.box2d.EdgeShape) r11
            r5 = r11
            com.badlogic.gdx.physics.box2d.EdgeShape r5 = (com.badlogic.gdx.physics.box2d.EdgeShape) r5
            r4 = r13
            com.badlogic.gdx.physics.box2d.EdgeShape r4 = (com.badlogic.gdx.physics.box2d.EdgeShape) r4
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            r4.getVertex1(r11)
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_1
            r4.getVertex2(r11)
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            com.badlogic.gdx.math.Vector2 r12 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_1
            r5.set(r11, r12)
            goto L_0x0026
        L_0x0080:
            com.badlogic.gdx.physics.box2d.ChainShape r3 = new com.badlogic.gdx.physics.box2d.ChainShape
            r3.<init>()
            r11 = r3
            com.badlogic.gdx.physics.box2d.ChainShape r11 = (com.badlogic.gdx.physics.box2d.ChainShape) r11
            r1 = r11
            com.badlogic.gdx.physics.box2d.ChainShape r1 = (com.badlogic.gdx.physics.box2d.ChainShape) r1
            r0 = r13
            com.badlogic.gdx.physics.box2d.ChainShape r0 = (com.badlogic.gdx.physics.box2d.ChainShape) r0
            int r11 = r0.getVertexCount()
            float[] r10 = new float[r11]
            r6 = 0
        L_0x0095:
            int r11 = r10.length
            if (r6 >= r11) goto L_0x00ae
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            r0.getVertex(r6, r11)
            int r7 = r6 + 1
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            float r11 = r11.x
            r10[r6] = r11
            com.badlogic.gdx.math.Vector2 r11 = net.dermetfan.gdx.physics.box2d.Box2DUtils.vec2_0
            float r11 = r11.y
            r10[r7] = r11
            int r6 = r7 + 1
            goto L_0x0095
        L_0x00ae:
            boolean r11 = r0.isLooped()
            if (r11 == 0) goto L_0x00b9
            r1.createLoop(r10)
            goto L_0x0026
        L_0x00b9:
            r1.createChain(r10)
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: net.dermetfan.gdx.physics.box2d.Box2DUtils.clone(com.badlogic.gdx.physics.box2d.Shape):com.badlogic.gdx.physics.box2d.Shape");
    }

    public static <T extends Joint> T clone(T joint) {
        return joint.getBodyA().getWorld().createJoint(createDef((Joint) joint));
    }

    public static BodyDef set(BodyDef bodyDef, Body body) {
        bodyDef.active = body.isActive();
        bodyDef.allowSleep = body.isSleepingAllowed();
        bodyDef.angle = body.getAngle();
        bodyDef.angularDamping = body.getAngularDamping();
        bodyDef.angularVelocity = body.getAngularVelocity();
        bodyDef.awake = body.isAwake();
        bodyDef.bullet = body.isBullet();
        bodyDef.fixedRotation = body.isFixedRotation();
        bodyDef.gravityScale = body.getGravityScale();
        bodyDef.linearDamping = body.getLinearDamping();
        bodyDef.linearVelocity.set(body.getLinearVelocity());
        bodyDef.position.set(body.getPosition());
        bodyDef.type = body.getType();
        return bodyDef;
    }

    public static FixtureDef set(FixtureDef fixtureDef, Fixture fixture) {
        fixtureDef.density = fixture.getDensity();
        Filter filter = fixture.getFilterData();
        fixtureDef.filter.categoryBits = filter.categoryBits;
        fixtureDef.filter.groupIndex = filter.groupIndex;
        fixtureDef.filter.maskBits = filter.maskBits;
        fixtureDef.friction = fixture.getFriction();
        fixtureDef.isSensor = fixture.isSensor();
        fixtureDef.restitution = fixture.getRestitution();
        fixtureDef.shape = fixture.getShape();
        return fixtureDef;
    }

    public static JointDef set(JointDef jointDef, Joint joint) {
        jointDef.type = joint.getType();
        jointDef.collideConnected = joint.getCollideConnected();
        jointDef.bodyA = joint.getBodyA();
        jointDef.bodyB = joint.getBodyB();
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static DistanceJointDef set(DistanceJointDef jointDef, DistanceJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.dampingRatio = joint.getDampingRatio();
        jointDef.frequencyHz = joint.getFrequency();
        jointDef.length = joint.getLength();
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static FrictionJointDef set(FrictionJointDef jointDef, FrictionJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        jointDef.maxForce = joint.getMaxForce();
        jointDef.maxTorque = joint.getMaxTorque();
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static GearJointDef set(GearJointDef jointDef, GearJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.joint1 = joint.getJoint1();
        jointDef.joint2 = joint.getJoint2();
        jointDef.ratio = joint.getRatio();
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static MotorJointDef set(MotorJointDef jointDef, MotorJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.angularOffset = joint.getAngularOffset();
        jointDef.linearOffset.set(joint.getLinearOffset());
        jointDef.correctionFactor = joint.getCorrectionFactor();
        jointDef.maxForce = joint.getMaxForce();
        jointDef.maxTorque = joint.getMaxTorque();
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static MouseJointDef set(MouseJointDef jointDef, MouseJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.dampingRatio = joint.getDampingRatio();
        jointDef.frequencyHz = joint.getFrequency();
        jointDef.maxForce = joint.getMaxForce();
        jointDef.target.set(joint.getTarget());
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static RevoluteJointDef set(RevoluteJointDef jointDef, RevoluteJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.enableLimit = joint.isLimitEnabled();
        jointDef.enableMotor = joint.isMotorEnabled();
        jointDef.maxMotorTorque = joint.getMaxMotorTorque();
        jointDef.motorSpeed = joint.getMotorSpeed();
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        jointDef.lowerAngle = joint.getLowerLimit();
        jointDef.upperAngle = joint.getUpperLimit();
        jointDef.referenceAngle = joint.getReferenceAngle();
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static PrismaticJointDef set(PrismaticJointDef jointDef, PrismaticJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.enableLimit = joint.isLimitEnabled();
        jointDef.enableMotor = joint.isMotorEnabled();
        jointDef.maxMotorForce = joint.getMaxMotorForce();
        jointDef.motorSpeed = joint.getMotorSpeed();
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        jointDef.localAxisA.set(joint.getLocalAxisA());
        jointDef.lowerTranslation = joint.getLowerLimit();
        jointDef.upperTranslation = joint.getUpperLimit();
        jointDef.referenceAngle = joint.getReferenceAngle();
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static PulleyJointDef set(PulleyJointDef jointDef, PulleyJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.groundAnchorA.set(joint.getGroundAnchorA());
        jointDef.groundAnchorB.set(joint.getGroundAnchorB());
        jointDef.lengthA = joint.getLength1();
        jointDef.lengthB = joint.getLength2();
        jointDef.ratio = joint.getRatio();
        jointDef.localAnchorA.set(joint.getBodyA().getLocalPoint(joint.getAnchorA()));
        jointDef.localAnchorB.set(joint.getBodyB().getLocalPoint(joint.getAnchorB()));
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static WheelJointDef set(WheelJointDef jointDef, WheelJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.dampingRatio = joint.getSpringDampingRatio();
        jointDef.frequencyHz = joint.getSpringFrequencyHz();
        jointDef.enableMotor = joint.isMotorEnabled();
        jointDef.maxMotorTorque = joint.getMaxMotorTorque();
        jointDef.motorSpeed = joint.getMotorSpeed();
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        jointDef.localAxisA.set(joint.getLocalAxisA());
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static WeldJointDef set(WeldJointDef jointDef, WeldJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.dampingRatio = joint.getDampingRatio();
        jointDef.frequencyHz = joint.getFrequency();
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        return jointDef;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef
     arg types: [com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint]
     candidates:
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.BodyDef, com.badlogic.gdx.physics.box2d.Body):com.badlogic.gdx.physics.box2d.BodyDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.FixtureDef, com.badlogic.gdx.physics.box2d.Fixture):com.badlogic.gdx.physics.box2d.FixtureDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.DistanceJointDef, com.badlogic.gdx.physics.box2d.joints.DistanceJoint):com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.FrictionJointDef, com.badlogic.gdx.physics.box2d.joints.FrictionJoint):com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.GearJointDef, com.badlogic.gdx.physics.box2d.joints.GearJoint):com.badlogic.gdx.physics.box2d.joints.GearJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MotorJointDef, com.badlogic.gdx.physics.box2d.joints.MotorJoint):com.badlogic.gdx.physics.box2d.joints.MotorJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.MouseJointDef, com.badlogic.gdx.physics.box2d.joints.MouseJoint):com.badlogic.gdx.physics.box2d.joints.MouseJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef, com.badlogic.gdx.physics.box2d.joints.PrismaticJoint):com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.PulleyJointDef, com.badlogic.gdx.physics.box2d.joints.PulleyJoint):com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef, com.badlogic.gdx.physics.box2d.joints.RevoluteJoint):com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.RopeJointDef, com.badlogic.gdx.physics.box2d.joints.RopeJoint):com.badlogic.gdx.physics.box2d.joints.RopeJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WeldJointDef, com.badlogic.gdx.physics.box2d.joints.WeldJoint):com.badlogic.gdx.physics.box2d.joints.WeldJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.joints.WheelJointDef, com.badlogic.gdx.physics.box2d.joints.WheelJoint):com.badlogic.gdx.physics.box2d.joints.WheelJointDef
      net.dermetfan.gdx.physics.box2d.Box2DUtils.set(com.badlogic.gdx.physics.box2d.JointDef, com.badlogic.gdx.physics.box2d.Joint):com.badlogic.gdx.physics.box2d.JointDef */
    public static RopeJointDef set(RopeJointDef jointDef, RopeJoint joint) {
        set((JointDef) jointDef, (Joint) joint);
        jointDef.localAnchorA.set(joint.getLocalAnchorA());
        jointDef.localAnchorB.set(joint.getLocalAnchorB());
        jointDef.maxLength = joint.getMaxLength();
        return jointDef;
    }

    public static BodyDef createDef(Body body) {
        return set(new BodyDef(), body);
    }

    public static FixtureDef createDef(Fixture fixture) {
        return set(new FixtureDef(), fixture);
    }

    public static JointDef createDef(Joint joint) {
        switch (joint.getType()) {
            case RevoluteJoint:
                return createDef((RevoluteJoint) joint);
            case PrismaticJoint:
                return createDef((PrismaticJoint) joint);
            case DistanceJoint:
                return createDef((DistanceJoint) joint);
            case PulleyJoint:
                return createDef((PulleyJoint) joint);
            case MouseJoint:
                return createDef((MouseJoint) joint);
            case GearJoint:
                return createDef((GearJoint) joint);
            case WheelJoint:
                return createDef((WheelJoint) joint);
            case WeldJoint:
                return createDef((WeldJoint) joint);
            case FrictionJoint:
                return createDef((FrictionJoint) joint);
            case RopeJoint:
                return createDef((RopeJoint) joint);
            case MotorJoint:
                return createDef((MotorJoint) joint);
            case Unknown:
            default:
                return null;
        }
    }

    public static DistanceJointDef createDef(DistanceJoint joint) {
        return set(new DistanceJointDef(), joint);
    }

    public static FrictionJointDef createDef(FrictionJoint joint) {
        return set(new FrictionJointDef(), joint);
    }

    public static GearJointDef createDef(GearJoint joint) {
        return set(new GearJointDef(), joint);
    }

    public static MotorJointDef createDef(MotorJoint joint) {
        return set(new MotorJointDef(), joint);
    }

    public static MouseJointDef createDef(MouseJoint joint) {
        return set(new MouseJointDef(), joint);
    }

    public static RevoluteJointDef createDef(RevoluteJoint joint) {
        return set(new RevoluteJointDef(), joint);
    }

    public static PrismaticJointDef createDef(PrismaticJoint joint) {
        return set(new PrismaticJointDef(), joint);
    }

    public static PulleyJointDef createDef(PulleyJoint joint) {
        return set(new PulleyJointDef(), joint);
    }

    public static WheelJointDef createDef(WheelJoint joint) {
        return set(new WheelJointDef(), joint);
    }

    public static WeldJointDef createDef(WeldJoint joint) {
        return set(new WeldJointDef(), joint);
    }

    public static RopeJointDef createDef(RopeJoint joint) {
        return set(new RopeJointDef(), joint);
    }

    public static boolean split(Body body, Vector2 a, Vector2 b, Pair<Body, Body> store) {
        World world = body.getWorld();
        BodyDef bodyDef = createDef(body);
        Body aBody = world.createBody(bodyDef);
        Body bBody = world.createBody(bodyDef);
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            if (!split(it.next(), a, b, aBody, bBody, null)) {
                return false;
            }
        }
        if (store != null) {
            store.set(aBody, bBody);
        }
        return true;
    }

    public static boolean split(Fixture fixture, Vector2 a, Vector2 b, Body aBody, Body bBody, Pair<Fixture, Fixture> store) {
        Pair<FixtureDef, FixtureDef> defs = (Pair) Pools.obtain(Pair.class);
        if (!split(fixture, a, b, defs)) {
            Pools.free(defs);
            return false;
        }
        Fixture aFixture = aBody.createFixture((FixtureDef) defs.getKey());
        Fixture bFixture = bBody.createFixture((FixtureDef) defs.getValue());
        if (store != null) {
            store.set(aFixture, bFixture);
        }
        return true;
    }

    public static boolean split(Fixture fixture, Vector2 a, Vector2 b, Pair<FixtureDef, FixtureDef> store) {
        Body body = fixture.getBody();
        Vector2 bodyPos = body.getPosition();
        Vector2 tmpA = ((Vector2) Pools.obtain(Vector2.class)).set(a).sub(bodyPos);
        Vector2 tmpB = ((Vector2) Pools.obtain(Vector2.class)).set(b).sub(bodyPos);
        GeometryUtils.rotate(tmpA, Vector2.Zero, -body.getAngle());
        GeometryUtils.rotate(tmpB, Vector2.Zero, -body.getAngle());
        Pair<Shape, Shape> shapes = (Pair) Pools.obtain(Pair.class);
        boolean split = split(fixture.getShape(), tmpA, tmpB, shapes);
        Pools.free(tmpA);
        Pools.free(tmpB);
        if (!split) {
            Pools.free(shapes);
            return false;
        }
        FixtureDef aDef = createDef(fixture);
        FixtureDef bDef = createDef(fixture);
        aDef.shape = (Shape) shapes.getKey();
        bDef.shape = (Shape) shapes.getValue();
        Pools.free(shapes);
        store.set(aDef, bDef);
        return true;
    }

    public static <T extends Shape> boolean split(T shape, Vector2 a, Vector2 b, Pair<T, T> store) {
        Shape.Type type = shape.getType();
        if (type == Shape.Type.Circle) {
            throw new IllegalArgumentException("shapes of the type " + Shape.Type.Circle + " cannot be split since Box2D does not support curved shapes other than circles: " + ((Object) shape));
        } else if (type == Shape.Type.Edge) {
            Vector2 vertex1 = (Vector2) Pools.obtain(Vector2.class);
            Vector2 vertex2 = (Vector2) Pools.obtain(Vector2.class);
            Vector2 intersection = (Vector2) Pools.obtain(Vector2.class);
            EdgeShape es = (EdgeShape) shape;
            es.getVertex1(vertex1);
            es.getVertex2(vertex2);
            if (!Intersector.intersectSegments(a, b, vertex1, vertex2, intersection)) {
                Pools.free(vertex1);
                Pools.free(vertex2);
                Pools.free(intersection);
                return false;
            }
            EdgeShape sa = new EdgeShape();
            EdgeShape sb = new EdgeShape();
            sa.set(vertex1, intersection);
            sb.set(intersection, vertex2);
            store.set(sa, sb);
            Pools.free(vertex1);
            Pools.free(vertex2);
            Pools.free(intersection);
            return true;
        } else {
            store.clear();
            Vector2[] vertices = vertices((Shape) shape);
            Vector2 aa = ((Vector2) Pools.obtain(Vector2.class)).set(a);
            Vector2 bb = ((Vector2) Pools.obtain(Vector2.class)).set(b);
            Array<Vector2> aVertices = (Array) Pools.obtain(Array.class);
            Array<Vector2> bVertices = (Array) Pools.obtain(Array.class);
            aVertices.clear();
            bVertices.clear();
            if (type == Shape.Type.Polygon) {
                aVertices.add(aa);
                aVertices.add(bb);
                GeometryUtils.arrangeClockwise(aVertices);
                tmpVector2Array.clear();
                tmpVector2Array.addAll(vertices);
                if (GeometryUtils.intersectSegments(a, b, GeometryUtils.toFloatArray(tmpVector2Array), (Vector2) aVertices.first(), (Vector2) aVertices.peek()) < 2) {
                    Pools.free(aa);
                    Pools.free(bb);
                    Pools.free(aVertices);
                    Pools.free(bVertices);
                    return false;
                }
                bVertices.add(aa);
                bVertices.add(bb);
                int length = vertices.length;
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= length) {
                        break;
                    }
                    Vector2 vertice = vertices[i2];
                    float det = MathUtils.det(aa.x, aa.y, vertice.x, vertice.y, bb.x, bb.y);
                    if (det < Animation.CurveTimeline.LINEAR) {
                        aVertices.add(vertice);
                    } else if (det > Animation.CurveTimeline.LINEAR) {
                        bVertices.add(vertice);
                    } else {
                        aVertices.add(vertice);
                        bVertices.add(vertice);
                    }
                    i = i2 + 1;
                }
                GeometryUtils.arrangeClockwise(aVertices);
                GeometryUtils.arrangeClockwise(bVertices);
                if (!checkPreconditions) {
                    PolygonShape sa2 = new PolygonShape();
                    PolygonShape sb2 = new PolygonShape();
                    sa2.set((Vector2[]) aVertices.toArray(Vector2.class));
                    sb2.set((Vector2[]) bVertices.toArray(Vector2.class));
                    store.set(sa2, sb2);
                } else if (aVertices.size >= 3 && aVertices.size <= 8 && bVertices.size >= 3 && bVertices.size <= 8) {
                    FloatArray aVerticesFloatArray = GeometryUtils.toFloatArray(aVertices, new FloatArray(aVertices.size * 2));
                    FloatArray bVerticesFloatArray = GeometryUtils.toFloatArray(bVertices, new FloatArray(bVertices.size * 2));
                    if (GeometryUtils.polygonArea(aVerticesFloatArray) > 1.1920929E-7f && GeometryUtils.polygonArea(bVerticesFloatArray) > 1.1920929E-7f) {
                        PolygonShape sa3 = new PolygonShape();
                        PolygonShape sb3 = new PolygonShape();
                        sa3.set(aVerticesFloatArray.toArray());
                        sb3.set(bVerticesFloatArray.toArray());
                        store.set(sa3, sb3);
                    }
                }
            } else if (type == Shape.Type.Chain) {
                Vector2 tmp = (Vector2) Pools.obtain(Vector2.class);
                boolean intersected = false;
                for (int i3 = 0; i3 < vertices.length; i3++) {
                    if (!intersected) {
                        aVertices.add(vertices[i3]);
                    } else {
                        bVertices.add(vertices[i3]);
                    }
                    if (!intersected && i3 + 1 < vertices.length && Intersector.intersectSegments(vertices[i3], vertices[i3 + 1], aa, bb, tmp)) {
                        intersected = true;
                        aVertices.add(tmp);
                        bVertices.add(tmp);
                    }
                }
                if (intersected && (!checkPreconditions || (aVertices.size >= 3 && bVertices.size >= 3))) {
                    ChainShape sa4 = new ChainShape();
                    ChainShape sb4 = new ChainShape();
                    sa4.createChain((Vector2[]) aVertices.toArray(Vector2.class));
                    sb4.createChain((Vector2[]) bVertices.toArray(Vector2.class));
                    store.set(sa4, sb4);
                }
                Pools.free(tmp);
            }
            Pools.free(aa);
            Pools.free(bb);
            Pools.free(aVertices);
            Pools.free(bVertices);
            return store.isFull();
        }
    }

    public static boolean equals(Transform a, Transform b) {
        return Arrays.equals(a.vals, b.vals);
    }

    public static boolean equals(MassData a, MassData b) {
        return a.center.equals(b.center) && a.mass == b.mass && a.I == b.I;
    }

    public static boolean equals(Filter a, Filter b) {
        return a.categoryBits == b.categoryBits && a.maskBits == b.maskBits && a.groupIndex == b.groupIndex;
    }

    public static void setSensor(Body body, boolean sensor) {
        Iterator<Fixture> it = body.getFixtureList().iterator();
        while (it.hasNext()) {
            it.next().setSensor(sensor);
        }
    }

    public static void destroyFixtures(Body body) {
        Array<Fixture> fixtures = body.getFixtureList();
        while (fixtures.size > 0) {
            body.destroyFixture(fixtures.peek());
        }
    }

    public static void destroyFixtures(Body body, Array<Fixture> exclude) {
        Array<Fixture> fixtures = body.getFixtureList();
        int preserved = 0;
        while (preserved < fixtures.size) {
            Fixture fixture = fixtures.get((fixtures.size - 1) - preserved);
            if (!exclude.contains(fixture, true)) {
                body.destroyFixture(fixture);
            } else {
                preserved++;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.dermetfan.utils.ArrayUtils.contains(java.lang.Object[], java.lang.Object, boolean):boolean
     arg types: [com.badlogic.gdx.physics.box2d.Fixture[], com.badlogic.gdx.physics.box2d.Fixture, int]
     candidates:
      net.dermetfan.utils.ArrayUtils.contains(java.lang.Object[], java.lang.Object[], boolean):boolean
      net.dermetfan.utils.ArrayUtils.contains(java.lang.Object[], java.lang.Object, boolean):boolean */
    public static void destroyFixtures(Body body, Fixture... exclude) {
        Array<Fixture> fixtures = body.getFixtureList();
        int preserved = 0;
        while (preserved < fixtures.size) {
            Fixture fixture = fixtures.get((fixtures.size - 1) - preserved);
            if (!ArrayUtils.contains((Object[]) exclude, (Object) fixture, true)) {
                body.destroyFixture(fixture);
            } else {
                preserved++;
            }
        }
    }

    public static void destroyFixtures(Body body, Fixture exclude) {
        Array<Fixture> fixtures = body.getFixtureList();
        int preserved = 0;
        while (preserved < fixtures.size) {
            Fixture fixture = fixtures.get((fixtures.size - 1) - preserved);
            if (fixture != exclude) {
                body.destroyFixture(fixture);
            } else {
                preserved++;
            }
        }
    }

    public static BodyDef reset(BodyDef bodyDef) {
        bodyDef.position.setZero();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.angle = Animation.CurveTimeline.LINEAR;
        bodyDef.linearVelocity.setZero();
        bodyDef.angularVelocity = Animation.CurveTimeline.LINEAR;
        bodyDef.linearDamping = Animation.CurveTimeline.LINEAR;
        bodyDef.angularDamping = Animation.CurveTimeline.LINEAR;
        bodyDef.allowSleep = true;
        bodyDef.awake = true;
        bodyDef.fixedRotation = false;
        bodyDef.bullet = false;
        bodyDef.active = true;
        bodyDef.gravityScale = 1.0f;
        return bodyDef;
    }

    public static FixtureDef reset(FixtureDef fixtureDef) {
        fixtureDef.shape = null;
        fixtureDef.friction = Animation.CurveTimeline.LINEAR;
        fixtureDef.restitution = Animation.CurveTimeline.LINEAR;
        fixtureDef.density = Animation.CurveTimeline.LINEAR;
        fixtureDef.isSensor = false;
        fixtureDef.filter.categoryBits = 1;
        fixtureDef.filter.maskBits = -1;
        fixtureDef.filter.groupIndex = 0;
        return fixtureDef;
    }
}
