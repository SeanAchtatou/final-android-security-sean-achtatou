package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;
import java.util.Iterator;
import net.dermetfan.gdx.Multiplexer;

public class ContactMultiplexer extends Multiplexer<ContactListener> implements ContactListener {
    public ContactMultiplexer(ContactListener... receivers) {
        super(receivers);
    }

    public ContactMultiplexer(Array<ContactListener> receivers) {
        super(receivers);
    }

    public void beginContact(Contact contact) {
        Iterator it = this.receivers.iterator();
        while (it.hasNext()) {
            ((ContactListener) it.next()).beginContact(contact);
        }
    }

    public void preSolve(Contact contact, Manifold oldManifold) {
        Iterator it = this.receivers.iterator();
        while (it.hasNext()) {
            ((ContactListener) it.next()).preSolve(contact, oldManifold);
        }
    }

    public void postSolve(Contact contact, ContactImpulse impulse) {
        Iterator it = this.receivers.iterator();
        while (it.hasNext()) {
            ((ContactListener) it.next()).postSolve(contact, impulse);
        }
    }

    public void endContact(Contact contact) {
        Iterator it = this.receivers.iterator();
        while (it.hasNext()) {
            ((ContactListener) it.next()).endContact(contact);
        }
    }
}
