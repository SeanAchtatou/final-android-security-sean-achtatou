package net.dermetfan.gdx.physics.box2d;

import com.badlogic.gdx.maps.Map;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.FrictionJointDef;
import com.badlogic.gdx.physics.box2d.joints.GearJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.PulleyJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import net.dermetfan.gdx.maps.MapUtils;
import net.dermetfan.gdx.math.GeometryUtils;

public class Box2DMapObjectParser {
    static final /* synthetic */ boolean $assertionsDisabled;
    public static final Listener.Adapter defaultListener = new Listener.Adapter();
    private Aliases aliases = new Aliases();
    private ObjectMap<String, Body> bodies = new ObjectMap<>();
    private ObjectMap<String, Fixture> fixtures = new ObjectMap<>();
    private MapProperties heritage;
    private boolean ignoreLayerUnitScale;
    private boolean ignoreMapUnitScale;
    private ObjectMap<String, Joint> joints = new ObjectMap<>();
    private MapProperties layerProperties;
    private Listener listener = defaultListener;
    private MapProperties mapProperties;
    private final Matrix4 mat4 = new Matrix4();
    private float tileHeight = 1.0f;
    private float tileWidth = 1.0f;
    private boolean triangulate;
    private float unitScale = 1.0f;
    private final Vector2 vec2 = new Vector2();
    private final Vector3 vec3 = new Vector3();

    public static class Aliases {
        public String active = "active";
        public String allowSleep = "allowSleep";
        public String angle = "angle";
        public String angularDamping = "angularDamping";
        public String angularVelocity = "angularVelocity";
        public String autoClearForces = "autoClearForces";
        public String awake = "awake";
        public String body = "body";
        public String bodyA = "bodyA";
        public String bodyB = "bodyB";
        public String bodyType = "bodyType";
        public String bullet = "bullet";
        public String categoryBits = "categoryBits";
        public String collideConnected = "collideConnected";
        public String dampingRatio = "dampingRatio";
        public String density = "density";
        public String distanceJoint = "DistanceJoint";
        public String dynamicBody = "DynamicBody";
        public String enableLimit = "enableLimit";
        public String enableMotor = "enableMotor";
        public String fixedRotation = "fixedRotation";
        public String fixture = "fixture";
        public String frequencyHz = "frequencyHz";
        public String friciton = "friction";
        public String frictionJoint = "FrictionJoint";
        public String gearJoint = "GearJoint";
        public String gravityScale = "gravityScale";
        public String gravityX = "gravityX";
        public String gravityY = "gravityY";
        public String groundAnchorAX = "groundAnchorAX";
        public String groundAnchorAY = "groundAnchorAY";
        public String groundAnchorBX = "groundAnchorBX";
        public String groundAnchorBY = "groundAnchorBY";
        public String groupIndex = "groupIndex";
        public String height = "height";
        public String isSensor = "isSensor";
        public String isometric = "isometric";
        public String joint = "joint";
        public String joint1 = "joint1";
        public String joint2 = "joint2";
        public String jointType = "jointType";
        public String kinematicBody = "KinematicBody";
        public String length = "length";
        public String lengthA = "lengthA";
        public String lengthB = "lengthB";
        public String linearDamping = "linearDamping";
        public String linearVelocityX = "linearVelocityX";
        public String linearVelocityY = "linearVelocityY";
        public String localAnchorAX = "localAnchorAX";
        public String localAnchorAY = "localAnchorAY";
        public String localAnchorBX = "localAnchorBX";
        public String localAnchorBY = "localAnchorBY";
        public String localAxisAX = "localAxisAX";
        public String localAxisAY = "localAxisAY";
        public String lowerAngle = "lowerAngle";
        public String lowerTranslation = "lowerTranslation";
        public String maskBits = "maskBits";
        public String maxForce = "maxForce";
        public String maxLength = "maxLength";
        public String maxMotorForce = "maxMotorForce";
        public String maxMotorTorque = "maxMotorTorque";
        public String maxTorque = "maxTorque";
        public String motorSpeed = "motorSpeed";
        public String mouseJoint = "MouseJoint";
        public String object = "object";
        public String orientation = "orientation";
        public String orthogonal = "orthogonal";
        public String prismaticJoint = "PrismaticJoint";
        public String pulleyJoint = "PulleyJoint";
        public String ratio = "ratio";
        public String referenceAngle = "referenceAngle";
        public String restitution = "restitution";
        public String revoluteJoint = "RevoluteJoint";
        public String ropeJoint = "RopeJoint";
        public String staggered = "staggered";
        public String staticBody = "StaticBody";
        public String targetX = "targetX";
        public String targetY = "targetY";
        public String tileHeight = "tileheight";
        public String tileWidth = "tilewidth";
        public String type = "type";
        public String unitScale = "unitScale";
        public String upperAngle = "upperAngle";
        public String upperTranslation = "upperTranslation";
        public String userData = "userData";
        public String weldJoint = "WeldJoint";
        public String wheelJoint = "WheelJoint";
        public String width = "width";
        public String x = "x";
        public String y = "y";
    }

    static {
        boolean z;
        if (!Box2DMapObjectParser.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public interface Listener {
        MapObject createBody(MapObject mapObject);

        MapObject createFixture(MapObject mapObject);

        MapObject createFixtures(MapObject mapObject);

        MapObject createJoint(MapObject mapObject);

        MapObject createObject(MapObject mapObject);

        void created(Body body, MapObject mapObject);

        void created(Fixture fixture, MapObject mapObject);

        void created(Joint joint, MapObject mapObject);

        void init(Box2DMapObjectParser box2DMapObjectParser);

        void load(Map map, Array<MapLayer> array);

        void load(MapLayer mapLayer, Array<MapObject> array);

        public static class Adapter implements Listener {
            public void init(Box2DMapObjectParser parser) {
            }

            public void load(Map map, Array<MapLayer> queue) {
                MapLayers layers = map.getLayers();
                queue.ensureCapacity(layers.getCount());
                Iterator<MapLayer> it = layers.iterator();
                while (it.hasNext()) {
                    queue.add(it.next());
                }
            }

            public void load(MapLayer layer, Array<MapObject> queue) {
                MapObjects objects = layer.getObjects();
                queue.ensureCapacity(objects.getCount());
                Iterator<MapObject> it = objects.iterator();
                while (it.hasNext()) {
                    queue.add(it.next());
                }
            }

            public MapObject createObject(MapObject mapObject) {
                return mapObject;
            }

            public MapObject createBody(MapObject mapObject) {
                return mapObject;
            }

            public MapObject createFixtures(MapObject mapObject) {
                return mapObject;
            }

            public MapObject createFixture(MapObject mapObject) {
                return mapObject;
            }

            public MapObject createJoint(MapObject mapObject) {
                return mapObject;
            }

            public void created(Body body, MapObject mapObject) {
            }

            public void created(Fixture fixture, MapObject mapObject) {
            }

            public void created(Joint joint, MapObject mapObject) {
            }
        }
    }

    public Box2DMapObjectParser() {
    }

    public Box2DMapObjectParser(Aliases aliases2) {
        this.aliases = aliases2;
    }

    public Box2DMapObjectParser(Listener listener2) {
        this.listener = listener2;
    }

    public Box2DMapObjectParser(Aliases aliases2, Listener listener2) {
        this.aliases = aliases2;
        this.listener = listener2;
    }

    public Box2DMapObjectParser(Aliases aliases2, float tileWidth2, float tileHeight2) {
        this.aliases = aliases2;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public Box2DMapObjectParser(Listener listener2, float tileWidth2, float tileHeight2) {
        this.listener = listener2;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public Box2DMapObjectParser(Aliases aliases2, Listener listener2, float tileWidth2, float tileHeight2) {
        this.aliases = aliases2;
        this.listener = listener2;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public Box2DMapObjectParser(float unitScale2) {
        this.unitScale = unitScale2;
    }

    public Box2DMapObjectParser(float unitScale2, float tileWidth2, float tileHeight2) {
        this.unitScale = unitScale2;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public Box2DMapObjectParser(Aliases aliases2, float unitScale2) {
        this.aliases = aliases2;
        this.unitScale = unitScale2;
    }

    public Box2DMapObjectParser(Listener listener2, float unitScale2) {
        this.listener = listener2;
        this.unitScale = unitScale2;
    }

    public Box2DMapObjectParser(Aliases aliases2, Listener listener2, float unitScale2) {
        this.aliases = aliases2;
        this.listener = listener2;
        this.unitScale = unitScale2;
    }

    public Box2DMapObjectParser(Aliases aliases2, float unitScale2, float tileWidth2, float tileHeight2) {
        this.aliases = aliases2;
        this.unitScale = unitScale2;
        this.ignoreMapUnitScale = true;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public Box2DMapObjectParser(Listener listener2, float unitScale2, float tileWidth2, float tileHeight2) {
        this.listener = listener2;
        this.unitScale = unitScale2;
        this.ignoreMapUnitScale = true;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public Box2DMapObjectParser(Listener listener2, Aliases aliases2, float unitScale2, float tileWidth2, float tileHeight2) {
        this.aliases = aliases2;
        this.listener = listener2;
        this.unitScale = unitScale2;
        this.ignoreMapUnitScale = true;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
    }

    public World load(World world, Map map) {
        MapProperties oldMapProperties = this.mapProperties;
        this.mapProperties = map.getProperties();
        world.setGravity(this.vec2.set(((Float) MapUtils.getProperty(this.mapProperties, this.aliases.gravityX, Float.valueOf(world.getGravity().x))).floatValue(), ((Float) MapUtils.getProperty(this.mapProperties, this.aliases.gravityY, Float.valueOf(world.getGravity().y))).floatValue()));
        world.setAutoClearForces(((Boolean) MapUtils.getProperty(this.mapProperties, this.aliases.autoClearForces, Boolean.valueOf(world.getAutoClearForces()))).booleanValue());
        if (!this.ignoreMapUnitScale) {
            this.unitScale = ((Float) MapUtils.getProperty(this.mapProperties, this.aliases.unitScale, Float.valueOf(this.unitScale))).floatValue();
        }
        this.tileWidth = ((Float) MapUtils.getProperty(this.mapProperties, this.aliases.tileWidth, Float.valueOf(this.tileWidth))).floatValue();
        this.tileHeight = ((Float) MapUtils.getProperty(this.mapProperties, this.aliases.tileHeight, Float.valueOf(this.tileHeight))).floatValue();
        this.listener.init(this);
        Array<MapLayer> layers = (Array) Pools.obtain(Array.class);
        layers.clear();
        this.listener.load(map, layers);
        Iterator it = layers.iterator();
        while (it.hasNext()) {
            load(world, (MapLayer) it.next());
        }
        layers.clear();
        Pools.free(layers);
        this.mapProperties = oldMapProperties;
        return world;
    }

    public World load(World world, MapLayer layer) {
        MapProperties oldLayerProperties = this.layerProperties;
        this.layerProperties = layer.getProperties();
        float oldUnitScale = this.unitScale;
        if (!this.ignoreLayerUnitScale) {
            this.unitScale = ((Float) MapUtils.getProperty(layer.getProperties(), this.aliases.unitScale, Float.valueOf(this.unitScale))).floatValue();
        }
        String typeFallback = (String) MapUtils.findProperty(this.aliases.type, "", this.heritage, this.mapProperties, this.layerProperties);
        Array<MapObject> objects = (Array) Pools.obtain(Array.class);
        objects.clear();
        this.listener.load(layer, objects);
        Iterator it = objects.iterator();
        while (it.hasNext()) {
            MapObject object = (MapObject) it.next();
            if (((String) MapUtils.getProperty(object.getProperties(), this.aliases.type, typeFallback)).equals(this.aliases.object)) {
                createObject(world, object);
            }
        }
        Iterator it2 = objects.iterator();
        while (it2.hasNext()) {
            MapObject object2 = (MapObject) it2.next();
            if (((String) MapUtils.getProperty(object2.getProperties(), this.aliases.type, typeFallback)).equals(this.aliases.body)) {
                createBody(world, object2);
            }
        }
        Iterator it3 = objects.iterator();
        while (it3.hasNext()) {
            MapObject object3 = (MapObject) it3.next();
            if (((String) MapUtils.getProperty(object3.getProperties(), this.aliases.type, typeFallback)).equals(this.aliases.fixture)) {
                createFixtures(object3);
            }
        }
        Iterator it4 = objects.iterator();
        while (it4.hasNext()) {
            MapObject object4 = (MapObject) it4.next();
            if (((String) MapUtils.getProperty(object4.getProperties(), this.aliases.type, typeFallback)).equals(this.aliases.joint)) {
                createJoint(object4);
            }
        }
        objects.clear();
        Pools.free(objects);
        this.layerProperties = oldLayerProperties;
        this.unitScale = oldUnitScale;
        return world;
    }

    public Body createObject(World world, MapObject object) {
        MapObject object2 = this.listener.createObject(object);
        if (object2 == null) {
            return null;
        }
        Body body = createBody(world, object2);
        createFixtures(object2, body);
        return body;
    }

    public Body createBody(World world, MapObject mapObject) {
        MapObject mapObject2 = this.listener.createBody(mapObject);
        if (mapObject2 == null) {
            return null;
        }
        MapProperties properties = mapObject2.getProperties();
        BodyDef bodyDef = new BodyDef();
        assignProperties(bodyDef, this.heritage);
        assignProperties(bodyDef, this.mapProperties);
        assignProperties(bodyDef, this.layerProperties);
        assignProperties(bodyDef, properties);
        Body body = world.createBody(bodyDef);
        body.setUserData(MapUtils.findProperty(this.aliases.userData, body.getUserData(), this.heritage, this.mapProperties, this.layerProperties, properties));
        this.bodies.put(findAvailableName(mapObject2.getName(), this.bodies), body);
        this.listener.created(body, mapObject2);
        return body;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public Fixture createFixture(MapObject mapObject, Body body) {
        Shape shape;
        float width;
        float height;
        MapObject mapObject2 = this.listener.createFixture(mapObject);
        if (mapObject2 == null) {
            return null;
        }
        String orientation = (String) MapUtils.findProperty(this.aliases.orientation, this.aliases.orthogonal, this.heritage, this.mapProperties, this.layerProperties, mapObject2.getProperties());
        transform(this.mat4, orientation);
        Shape shape2 = null;
        if (mapObject2 instanceof RectangleMapObject) {
            Rectangle rectangle = ((RectangleMapObject) mapObject2).getRectangle();
            this.vec3.set(rectangle.x, rectangle.y, Animation.CurveTimeline.LINEAR);
            this.vec3.mul(this.mat4);
            float x = this.vec3.x;
            float y = this.vec3.y;
            if (!orientation.equals(this.aliases.staggered)) {
                this.vec3.set(rectangle.width, rectangle.height, Animation.CurveTimeline.LINEAR).mul(this.mat4);
                width = this.vec3.x;
                height = this.vec3.y;
            } else {
                width = rectangle.width * this.unitScale;
                height = rectangle.height * this.unitScale;
            }
            Shape shape3 = new PolygonShape();
            ((PolygonShape) shape3).setAsBox(width / 2.0f, height / 2.0f, this.vec2.set((x - body.getPosition().x) + (width / 2.0f), (y - body.getPosition().y) + (height / 2.0f)), body.getAngle());
            shape2 = shape3;
        } else if ((mapObject2 instanceof PolygonMapObject) || (mapObject2 instanceof PolylineMapObject)) {
            FloatArray vertices = (FloatArray) Pools.obtain(FloatArray.class);
            vertices.clear();
            vertices.addAll(mapObject2 instanceof PolygonMapObject ? ((PolygonMapObject) mapObject2).getPolygon().getTransformedVertices() : ((PolylineMapObject) mapObject2).getPolyline().getTransformedVertices());
            int ix = 0;
            for (int iy = 1; iy < vertices.size; iy += 2) {
                this.vec3.set(vertices.get(ix), vertices.get(iy), Animation.CurveTimeline.LINEAR);
                this.vec3.mul(this.mat4);
                vertices.set(ix, this.vec3.x - body.getPosition().x);
                vertices.set(iy, this.vec3.y - body.getPosition().y);
                ix += 2;
            }
            if (mapObject2 instanceof PolygonMapObject) {
                Shape polygonShape = new PolygonShape();
                ((PolygonShape) polygonShape).set(vertices.items, 0, vertices.size);
                shape = polygonShape;
            } else if (vertices.size == 4) {
                Shape edgeShape = new EdgeShape();
                ((EdgeShape) edgeShape).set(vertices.get(0), vertices.get(1), vertices.get(2), vertices.get(3));
                shape = edgeShape;
            } else {
                vertices.shrink();
                Shape chainShape = new ChainShape();
                ((ChainShape) chainShape).createChain(vertices.items);
                shape = chainShape;
            }
            Pools.free(vertices);
            shape2 = shape;
        } else if ((mapObject2 instanceof CircleMapObject) || (mapObject2 instanceof EllipseMapObject)) {
            if (mapObject2 instanceof CircleMapObject) {
                Circle circle = ((CircleMapObject) mapObject2).getCircle();
                this.vec3.set(circle.x, circle.y, circle.radius);
            } else {
                Ellipse ellipse = ((EllipseMapObject) mapObject2).getEllipse();
                if (ellipse.width != ellipse.height) {
                    throw new IllegalArgumentException("Cannot parse " + mapObject2.getName() + " because " + ClassReflection.getSimpleName(mapObject2.getClass()) + "s that are not circles are not supported");
                }
                this.vec3.set(ellipse.x + (ellipse.width / 2.0f), ellipse.y + (ellipse.height / 2.0f), ellipse.width / 2.0f);
            }
            this.vec3.mul(this.mat4);
            this.vec3.sub(body.getPosition().x, body.getPosition().y, Animation.CurveTimeline.LINEAR);
            Shape shape4 = new CircleShape();
            CircleShape circleShape = (CircleShape) shape4;
            circleShape.setPosition(this.vec2.set(this.vec3.x, this.vec3.y));
            circleShape.setRadius(this.vec3.z);
            shape2 = shape4;
        } else if (mapObject2 instanceof TextureMapObject) {
            throw new IllegalArgumentException("Cannot parse " + mapObject2.getName() + " because " + ClassReflection.getSimpleName(mapObject2.getClass()) + "s are not supported");
        } else if (!$assertionsDisabled) {
            throw new AssertionError(mapObject2 + " is a not known subclass of " + MapObject.class.getName());
        }
        MapProperties properties = mapObject2.getProperties();
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape2;
        assignProperties(fixtureDef, this.heritage);
        assignProperties(fixtureDef, this.mapProperties);
        assignProperties(fixtureDef, this.layerProperties);
        assignProperties(fixtureDef, properties);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(MapUtils.findProperty(this.aliases.userData, fixture.getUserData(), this.heritage, this.mapProperties, this.layerProperties, properties));
        shape2.dispose();
        this.fixtures.put(findAvailableName(mapObject2.getName(), this.fixtures), fixture);
        this.listener.created(fixture, mapObject2);
        return fixture;
    }

    public Fixture[] createFixtures(MapObject mapObject, Body body) {
        MapObject mapObject2 = this.listener.createFixtures(mapObject);
        if (mapObject2 == null) {
            return null;
        }
        if (mapObject2 instanceof PolygonMapObject) {
            Polygon polygon = ((PolygonMapObject) mapObject2).getPolygon();
            if (!GeometryUtils.isConvex(polygon) || (Box2DUtils.checkPreconditions && polygon.getVertices().length / 2 > 8)) {
                Polygon[] convexPolygons = this.triangulate ? GeometryUtils.triangulate(polygon) : GeometryUtils.decompose(polygon);
                Fixture[] fixtures2 = new Fixture[convexPolygons.length];
                for (int i = 0; i < fixtures2.length; i++) {
                    PolygonMapObject convexObject = new PolygonMapObject(convexPolygons[i]);
                    convexObject.setColor(mapObject2.getColor());
                    convexObject.setName(mapObject2.getName());
                    convexObject.setOpacity(mapObject2.getOpacity());
                    convexObject.setVisible(mapObject2.isVisible());
                    convexObject.getProperties().putAll(mapObject2.getProperties());
                    fixtures2[i] = createFixture(convexObject, body);
                }
                return fixtures2;
            }
        }
        return new Fixture[]{createFixture(mapObject2, body)};
    }

    public Fixture createFixture(MapObject mapObject) {
        return createFixture(mapObject, findBody(mapObject, this.heritage, this.mapProperties, this.layerProperties));
    }

    public Fixture[] createFixtures(MapObject mapObject) {
        return createFixtures(mapObject, findBody(mapObject, this.heritage, this.mapProperties, this.layerProperties));
    }

    public void transform(Matrix4 mat, String orientation) {
        mat.idt();
        if (orientation.equals(this.aliases.isometric)) {
            mat.scale((float) (Math.sqrt(2.0d) / 2.0d), (float) (Math.sqrt(2.0d) / 4.0d), 1.0f);
            mat.rotate(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, -45.0f);
            mat.translate(-1.0f, 1.0f, Animation.CurveTimeline.LINEAR);
            mat.scale(this.unitScale * 2.0f, this.unitScale * 2.0f, this.unitScale * 2.0f);
        } else if (orientation.equals(this.aliases.staggered)) {
            mat.scale(this.unitScale, this.unitScale, this.unitScale);
            mat.translate((-this.tileWidth) / 2.0f, ((-this.tileHeight) * ((float) (((Integer) MapUtils.findProperty(this.aliases.height, 0, this.mapProperties, this.layerProperties)).intValue() / 2))) + (this.tileHeight / 2.0f), Animation.CurveTimeline.LINEAR);
        } else {
            mat.scale(this.unitScale, this.unitScale, this.unitScale);
        }
    }

    private Body findBody(MapObject mapObject, MapProperties... heritage2) {
        String name = mapObject.getName();
        Body body = null;
        if (name != null) {
            body = this.bodies.get(name);
        }
        if (body == null) {
            body = this.bodies.get(MapUtils.getProperty(mapObject.getProperties(), this.aliases.body, ""));
        }
        if (body == null) {
            for (MapProperties properties : heritage2) {
                body = this.bodies.get(MapUtils.getProperty(properties, this.aliases.body, ""));
                if (body != null) {
                    break;
                }
            }
        }
        if (body != null) {
            return body;
        }
        StringBuilder append = new StringBuilder().append("the body of ").append(name == null ? "an unnamed " : "the ").append("fixture ");
        if (name == null) {
            name = "";
        }
        throw new IllegalStateException(append.append(name).append("does not exist").toString());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v0, resolved type: com.badlogic.gdx.physics.box2d.joints.WeldJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v0, resolved type: com.badlogic.gdx.physics.box2d.joints.RopeJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v0, resolved type: com.badlogic.gdx.physics.box2d.joints.PulleyJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v0, resolved type: com.badlogic.gdx.physics.box2d.joints.MouseJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: com.badlogic.gdx.physics.box2d.joints.GearJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v7, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: com.badlogic.gdx.physics.box2d.joints.FrictionJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: com.badlogic.gdx.physics.box2d.joints.DistanceJointDef} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v10, resolved type: com.badlogic.gdx.physics.box2d.joints.WheelJointDef} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.physics.box2d.Joint createJoint(com.badlogic.gdx.maps.MapObject r19) {
        /*
            r18 = this;
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Listener r15 = r0.listener
            r0 = r19
            com.badlogic.gdx.maps.MapObject r19 = r15.createJoint(r0)
            if (r19 != 0) goto L_0x000e
            r4 = 0
        L_0x000d:
            return r4
        L_0x000e:
            com.badlogic.gdx.maps.MapProperties r9 = r19.getProperties()
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.jointType
            java.lang.String r16 = ""
            r0 = r16
            java.lang.Object r6 = net.dermetfan.gdx.maps.MapUtils.getProperty(r9, r15, r0)
            java.lang.String r6 = (java.lang.String) r6
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.distanceJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x0098
            com.badlogic.gdx.physics.box2d.joints.DistanceJointDef r1 = new com.badlogic.gdx.physics.box2d.joints.DistanceJointDef
            r1.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r1, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r1, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r1, r15)
            r0 = r18
            r0.assignProperties(r1, r9)
            r5 = r1
        L_0x0054:
            r0 = r18
            r0.assignProperties(r5, r9)
            com.badlogic.gdx.physics.box2d.Body r15 = r5.bodyA
            com.badlogic.gdx.physics.box2d.World r15 = r15.getWorld()
            com.badlogic.gdx.physics.box2d.Joint r4 = r15.createJoint(r5)
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.userData
            java.lang.Object r16 = r4.getUserData()
            r0 = r16
            java.lang.Object r15 = net.dermetfan.gdx.maps.MapUtils.getProperty(r9, r15, r0)
            r4.setUserData(r15)
            r0 = r18
            com.badlogic.gdx.utils.ObjectMap<java.lang.String, com.badlogic.gdx.physics.box2d.Joint> r15 = r0.joints
            java.lang.String r16 = r19.getName()
            r0 = r18
            com.badlogic.gdx.utils.ObjectMap<java.lang.String, com.badlogic.gdx.physics.box2d.Joint> r0 = r0.joints
            r17 = r0
            java.lang.String r16 = findAvailableName(r16, r17)
            r0 = r16
            r15.put(r0, r4)
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Listener r15 = r0.listener
            r0 = r19
            r15.created(r4, r0)
            goto L_0x000d
        L_0x0098:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.frictionJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x00cb
            com.badlogic.gdx.physics.box2d.joints.FrictionJointDef r2 = new com.badlogic.gdx.physics.box2d.joints.FrictionJointDef
            r2.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r2, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r2, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r2, r15)
            r0 = r18
            r0.assignProperties(r2, r9)
            r5 = r2
            goto L_0x0054
        L_0x00cb:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.gearJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x00ff
            com.badlogic.gdx.physics.box2d.joints.GearJointDef r3 = new com.badlogic.gdx.physics.box2d.joints.GearJointDef
            r3.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r3, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r3, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r3, r15)
            r0 = r18
            r0.assignProperties(r3, r9)
            r5 = r3
            goto L_0x0054
        L_0x00ff:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.mouseJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x0133
            com.badlogic.gdx.physics.box2d.joints.MouseJointDef r7 = new com.badlogic.gdx.physics.box2d.joints.MouseJointDef
            r7.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r7, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r7, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r7, r15)
            r0 = r18
            r0.assignProperties(r7, r9)
            r5 = r7
            goto L_0x0054
        L_0x0133:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.prismaticJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x0167
            com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef r8 = new com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef
            r8.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r8, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r8, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r8, r15)
            r0 = r18
            r0.assignProperties(r8, r9)
            r5 = r8
            goto L_0x0054
        L_0x0167:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.pulleyJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x019b
            com.badlogic.gdx.physics.box2d.joints.PulleyJointDef r10 = new com.badlogic.gdx.physics.box2d.joints.PulleyJointDef
            r10.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r10, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r10, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r10, r15)
            r0 = r18
            r0.assignProperties(r10, r9)
            r5 = r10
            goto L_0x0054
        L_0x019b:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.revoluteJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x01cf
            com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef r11 = new com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef
            r11.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r11, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r11, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r11, r15)
            r0 = r18
            r0.assignProperties(r11, r9)
            r5 = r11
            goto L_0x0054
        L_0x01cf:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.ropeJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x0203
            com.badlogic.gdx.physics.box2d.joints.RopeJointDef r12 = new com.badlogic.gdx.physics.box2d.joints.RopeJointDef
            r12.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r12, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r12, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r12, r15)
            r0 = r18
            r0.assignProperties(r12, r9)
            r5 = r12
            goto L_0x0054
        L_0x0203:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.weldJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x0237
            com.badlogic.gdx.physics.box2d.joints.WeldJointDef r13 = new com.badlogic.gdx.physics.box2d.joints.WeldJointDef
            r13.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r13, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r13, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r13, r15)
            r0 = r18
            r0.assignProperties(r13, r9)
            r5 = r13
            goto L_0x0054
        L_0x0237:
            r0 = r18
            net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser$Aliases r15 = r0.aliases
            java.lang.String r15 = r15.wheelJoint
            boolean r15 = r6.equals(r15)
            if (r15 == 0) goto L_0x026b
            com.badlogic.gdx.physics.box2d.joints.WheelJointDef r14 = new com.badlogic.gdx.physics.box2d.joints.WheelJointDef
            r14.<init>()
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.heritage
            r0 = r18
            r0.assignProperties(r14, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.mapProperties
            r0 = r18
            r0.assignProperties(r14, r15)
            r0 = r18
            com.badlogic.gdx.maps.MapProperties r15 = r0.layerProperties
            r0 = r18
            r0.assignProperties(r14, r15)
            r0 = r18
            r0.assignProperties(r14, r9)
            r5 = r14
            goto L_0x0054
        L_0x026b:
            java.lang.IllegalArgumentException r15 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            r16.<init>()
            java.lang.Class<com.badlogic.gdx.physics.box2d.JointDef$JointType> r17 = com.badlogic.gdx.physics.box2d.JointDef.JointType.class
            java.lang.String r17 = com.badlogic.gdx.utils.reflect.ClassReflection.getSimpleName(r17)
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r17 = " "
            java.lang.StringBuilder r16 = r16.append(r17)
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r6)
            java.lang.String r17 = " is unknown"
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r16 = r16.toString()
            r15.<init>(r16)
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: net.dermetfan.gdx.physics.box2d.Box2DMapObjectParser.createJoint(com.badlogic.gdx.maps.MapObject):com.badlogic.gdx.physics.box2d.Joint");
    }

    public void assignProperties(BodyDef bodyDef, MapProperties properties) {
        if (properties != null) {
            bodyDef.type = ((String) MapUtils.getProperty(properties, this.aliases.bodyType, "")).equals(this.aliases.staticBody) ? BodyDef.BodyType.StaticBody : ((String) MapUtils.getProperty(properties, this.aliases.bodyType, "")).equals(this.aliases.dynamicBody) ? BodyDef.BodyType.DynamicBody : ((String) MapUtils.getProperty(properties, this.aliases.bodyType, "")).equals(this.aliases.kinematicBody) ? BodyDef.BodyType.KinematicBody : bodyDef.type;
            bodyDef.active = ((Boolean) MapUtils.getProperty(properties, this.aliases.active, Boolean.valueOf(bodyDef.active))).booleanValue();
            bodyDef.allowSleep = ((Boolean) MapUtils.getProperty(properties, this.aliases.allowSleep, Boolean.valueOf(bodyDef.allowSleep))).booleanValue();
            bodyDef.angle = ((Float) MapUtils.getProperty(properties, this.aliases.angle, Float.valueOf(bodyDef.angle))).floatValue() * 0.017453292f;
            bodyDef.angularDamping = ((Float) MapUtils.getProperty(properties, this.aliases.angularDamping, Float.valueOf(bodyDef.angularDamping))).floatValue();
            bodyDef.angularVelocity = ((Float) MapUtils.getProperty(properties, this.aliases.angularVelocity, Float.valueOf(bodyDef.angularVelocity))).floatValue();
            bodyDef.awake = ((Boolean) MapUtils.getProperty(properties, this.aliases.awake, Boolean.valueOf(bodyDef.awake))).booleanValue();
            bodyDef.bullet = ((Boolean) MapUtils.getProperty(properties, this.aliases.bullet, Boolean.valueOf(bodyDef.bullet))).booleanValue();
            bodyDef.fixedRotation = ((Boolean) MapUtils.getProperty(properties, this.aliases.fixedRotation, Boolean.valueOf(bodyDef.fixedRotation))).booleanValue();
            bodyDef.gravityScale = ((Float) MapUtils.getProperty(properties, this.aliases.gravityScale, Float.valueOf(bodyDef.gravityScale))).floatValue();
            bodyDef.linearDamping = ((Float) MapUtils.getProperty(properties, this.aliases.linearDamping, Float.valueOf(bodyDef.linearDamping))).floatValue();
            bodyDef.linearVelocity.set(((Float) MapUtils.getProperty(properties, this.aliases.linearVelocityX, Float.valueOf(bodyDef.linearVelocity.x))).floatValue(), ((Float) MapUtils.getProperty(properties, this.aliases.linearVelocityY, Float.valueOf(bodyDef.linearVelocity.y))).floatValue());
            bodyDef.position.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.x, Float.valueOf(bodyDef.position.x))).floatValue(), ((Float) MapUtils.getProperty(properties, this.aliases.y, Float.valueOf(bodyDef.position.y))).floatValue() * this.unitScale);
        }
    }

    public void assignProperties(FixtureDef fixtureDef, MapProperties properties) {
        if (properties != null) {
            fixtureDef.density = ((Float) MapUtils.getProperty(properties, this.aliases.density, Float.valueOf(fixtureDef.density))).floatValue();
            fixtureDef.filter.categoryBits = ((Short) MapUtils.getProperty(properties, this.aliases.categoryBits, Short.valueOf(fixtureDef.filter.categoryBits))).shortValue();
            fixtureDef.filter.groupIndex = ((Short) MapUtils.getProperty(properties, this.aliases.groupIndex, Short.valueOf(fixtureDef.filter.groupIndex))).shortValue();
            fixtureDef.filter.maskBits = ((Short) MapUtils.getProperty(properties, this.aliases.maskBits, Short.valueOf(fixtureDef.filter.maskBits))).shortValue();
            fixtureDef.friction = ((Float) MapUtils.getProperty(properties, this.aliases.friciton, Float.valueOf(fixtureDef.friction))).floatValue();
            fixtureDef.isSensor = ((Boolean) MapUtils.getProperty(properties, this.aliases.isSensor, Boolean.valueOf(fixtureDef.isSensor))).booleanValue();
            fixtureDef.restitution = ((Float) MapUtils.getProperty(properties, this.aliases.restitution, Float.valueOf(fixtureDef.restitution))).floatValue();
        }
    }

    public void assignProperties(JointDef jointDef, MapProperties properties) {
        if (properties != null) {
            jointDef.bodyA = this.bodies.get(MapUtils.getProperty(properties, this.aliases.bodyA, ""));
            jointDef.bodyB = this.bodies.get(MapUtils.getProperty(properties, this.aliases.bodyB, ""));
            jointDef.collideConnected = ((Boolean) MapUtils.getProperty(properties, this.aliases.collideConnected, Boolean.valueOf(jointDef.collideConnected))).booleanValue();
        }
    }

    public void assignProperties(DistanceJointDef distanceJointDef, MapProperties properties) {
        if (properties != null) {
            distanceJointDef.dampingRatio = ((Float) MapUtils.getProperty(properties, this.aliases.dampingRatio, Float.valueOf(distanceJointDef.dampingRatio))).floatValue();
            distanceJointDef.frequencyHz = ((Float) MapUtils.getProperty(properties, this.aliases.frequencyHz, Float.valueOf(distanceJointDef.frequencyHz))).floatValue();
            distanceJointDef.length = ((((Float) MapUtils.getProperty(properties, this.aliases.length, Float.valueOf(distanceJointDef.length))).floatValue() * (this.tileWidth + this.tileHeight)) / 2.0f) * this.unitScale;
            distanceJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(distanceJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(distanceJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            distanceJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(distanceJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(distanceJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
        }
    }

    public void assignProperties(FrictionJointDef frictionJointDef, MapProperties properties) {
        if (properties != null) {
            frictionJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(frictionJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(frictionJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            frictionJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(frictionJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(frictionJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            frictionJointDef.maxForce = ((Float) MapUtils.getProperty(properties, this.aliases.maxForce, Float.valueOf(frictionJointDef.maxForce))).floatValue();
            frictionJointDef.maxTorque = ((Float) MapUtils.getProperty(properties, this.aliases.maxTorque, Float.valueOf(frictionJointDef.maxTorque))).floatValue();
        }
    }

    public void assignProperties(GearJointDef gearJointDef, MapProperties properties) {
        if (properties != null) {
            gearJointDef.joint1 = this.joints.get(MapUtils.getProperty(properties, this.aliases.joint1, ""));
            gearJointDef.joint2 = this.joints.get(MapUtils.getProperty(properties, this.aliases.joint2, ""));
            gearJointDef.ratio = ((Float) MapUtils.getProperty(properties, this.aliases.ratio, Float.valueOf(gearJointDef.ratio))).floatValue();
        }
    }

    public void assignProperties(MouseJointDef mouseJointDef, MapProperties properties) {
        if (properties != null) {
            mouseJointDef.dampingRatio = ((Float) MapUtils.getProperty(properties, this.aliases.dampingRatio, Float.valueOf(mouseJointDef.dampingRatio))).floatValue();
            mouseJointDef.frequencyHz = ((Float) MapUtils.getProperty(properties, this.aliases.frequencyHz, Float.valueOf(mouseJointDef.frequencyHz))).floatValue();
            mouseJointDef.maxForce = ((Float) MapUtils.getProperty(properties, this.aliases.maxForce, Float.valueOf(mouseJointDef.maxForce))).floatValue();
            mouseJointDef.target.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.targetX, Float.valueOf(mouseJointDef.target.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.targetY, Float.valueOf(mouseJointDef.target.y))).floatValue() * this.tileHeight * this.unitScale);
        }
    }

    public void assignProperties(PrismaticJointDef prismaticJointDef, MapProperties properties) {
        if (properties != null) {
            prismaticJointDef.enableLimit = ((Boolean) MapUtils.getProperty(properties, this.aliases.enableLimit, Boolean.valueOf(prismaticJointDef.enableLimit))).booleanValue();
            prismaticJointDef.enableMotor = ((Boolean) MapUtils.getProperty(properties, this.aliases.enableMotor, Boolean.valueOf(prismaticJointDef.enableMotor))).booleanValue();
            prismaticJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(prismaticJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(prismaticJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            prismaticJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(prismaticJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(prismaticJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            prismaticJointDef.localAxisA.set(((Float) MapUtils.getProperty(properties, this.aliases.localAxisAX, Float.valueOf(prismaticJointDef.localAxisA.x))).floatValue(), ((Float) MapUtils.getProperty(properties, this.aliases.localAxisAY, Float.valueOf(prismaticJointDef.localAxisA.y))).floatValue());
            prismaticJointDef.lowerTranslation = ((((Float) MapUtils.getProperty(properties, this.aliases.lowerTranslation, Float.valueOf(prismaticJointDef.lowerTranslation))).floatValue() * (this.tileWidth + this.tileHeight)) / 2.0f) * this.unitScale;
            prismaticJointDef.maxMotorForce = ((Float) MapUtils.getProperty(properties, this.aliases.maxMotorForce, Float.valueOf(prismaticJointDef.maxMotorForce))).floatValue();
            prismaticJointDef.motorSpeed = ((Float) MapUtils.getProperty(properties, this.aliases.motorSpeed, Float.valueOf(prismaticJointDef.motorSpeed))).floatValue();
            prismaticJointDef.referenceAngle = ((Float) MapUtils.getProperty(properties, this.aliases.referenceAngle, Float.valueOf(prismaticJointDef.referenceAngle))).floatValue() * 0.017453292f;
            prismaticJointDef.upperTranslation = ((((Float) MapUtils.getProperty(properties, this.aliases.upperTranslation, Float.valueOf(prismaticJointDef.upperTranslation))).floatValue() * (this.tileWidth + this.tileHeight)) / 2.0f) * this.unitScale;
        }
    }

    public void assignProperties(PulleyJointDef pulleyJointDef, MapProperties properties) {
        if (properties != null) {
            pulleyJointDef.groundAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.groundAnchorAX, Float.valueOf(pulleyJointDef.groundAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.groundAnchorAY, Float.valueOf(pulleyJointDef.groundAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            pulleyJointDef.groundAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.groundAnchorBX, Float.valueOf(pulleyJointDef.groundAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.groundAnchorBY, Float.valueOf(pulleyJointDef.groundAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            pulleyJointDef.lengthA = ((((Float) MapUtils.getProperty(properties, this.aliases.lengthA, Float.valueOf(pulleyJointDef.lengthA))).floatValue() * (this.tileWidth + this.tileHeight)) / 2.0f) * this.unitScale;
            pulleyJointDef.lengthB = ((((Float) MapUtils.getProperty(properties, this.aliases.lengthB, Float.valueOf(pulleyJointDef.lengthB))).floatValue() * (this.tileWidth + this.tileHeight)) / 2.0f) * this.unitScale;
            pulleyJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(pulleyJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(pulleyJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            pulleyJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(pulleyJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(pulleyJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            pulleyJointDef.ratio = ((Float) MapUtils.getProperty(properties, this.aliases.ratio, Float.valueOf(pulleyJointDef.ratio))).floatValue();
        }
    }

    public void assignProperties(RevoluteJointDef revoluteJointDef, MapProperties properties) {
        if (properties != null) {
            revoluteJointDef.enableLimit = ((Boolean) MapUtils.getProperty(properties, this.aliases.enableLimit, Boolean.valueOf(revoluteJointDef.enableLimit))).booleanValue();
            revoluteJointDef.enableMotor = ((Boolean) MapUtils.getProperty(properties, this.aliases.enableMotor, Boolean.valueOf(revoluteJointDef.enableMotor))).booleanValue();
            revoluteJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(revoluteJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(revoluteJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            revoluteJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(revoluteJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(revoluteJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            revoluteJointDef.lowerAngle = ((Float) MapUtils.getProperty(properties, this.aliases.lowerAngle, Float.valueOf(revoluteJointDef.lowerAngle))).floatValue() * 0.017453292f;
            revoluteJointDef.maxMotorTorque = ((Float) MapUtils.getProperty(properties, this.aliases.maxMotorTorque, Float.valueOf(revoluteJointDef.maxMotorTorque))).floatValue();
            revoluteJointDef.motorSpeed = ((Float) MapUtils.getProperty(properties, this.aliases.motorSpeed, Float.valueOf(revoluteJointDef.motorSpeed))).floatValue();
            revoluteJointDef.referenceAngle = ((Float) MapUtils.getProperty(properties, this.aliases.referenceAngle, Float.valueOf(revoluteJointDef.referenceAngle))).floatValue() * 0.017453292f;
            revoluteJointDef.upperAngle = ((Float) MapUtils.getProperty(properties, this.aliases.upperAngle, Float.valueOf(revoluteJointDef.upperAngle))).floatValue() * 0.017453292f;
        }
    }

    public void assignProperties(RopeJointDef ropeJointDef, MapProperties properties) {
        if (properties != null) {
            ropeJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(ropeJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(ropeJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            ropeJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(ropeJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(ropeJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            ropeJointDef.maxLength = ((((Float) MapUtils.getProperty(properties, this.aliases.maxLength, Float.valueOf(ropeJointDef.maxLength))).floatValue() * (this.tileWidth + this.tileHeight)) / 2.0f) * this.unitScale;
        }
    }

    public void assignProperties(WeldJointDef weldJointDef, MapProperties properties) {
        if (properties != null) {
            weldJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(weldJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(weldJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            weldJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(weldJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(weldJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            weldJointDef.referenceAngle = ((Float) MapUtils.getProperty(properties, this.aliases.referenceAngle, Float.valueOf(weldJointDef.referenceAngle))).floatValue() * 0.017453292f;
        }
    }

    public void assignProperties(WheelJointDef wheelJointDef, MapProperties properties) {
        if (properties != null) {
            wheelJointDef.dampingRatio = ((Float) MapUtils.getProperty(properties, this.aliases.dampingRatio, Float.valueOf(wheelJointDef.dampingRatio))).floatValue();
            wheelJointDef.enableMotor = ((Boolean) MapUtils.getProperty(properties, this.aliases.enableMotor, Boolean.valueOf(wheelJointDef.enableMotor))).booleanValue();
            wheelJointDef.frequencyHz = ((Float) MapUtils.getProperty(properties, this.aliases.frequencyHz, Float.valueOf(wheelJointDef.frequencyHz))).floatValue();
            wheelJointDef.localAnchorA.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAX, Float.valueOf(wheelJointDef.localAnchorA.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorAY, Float.valueOf(wheelJointDef.localAnchorA.y))).floatValue() * this.tileHeight * this.unitScale);
            wheelJointDef.localAnchorB.set(this.unitScale * ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBX, Float.valueOf(wheelJointDef.localAnchorB.x))).floatValue() * this.tileWidth, ((Float) MapUtils.getProperty(properties, this.aliases.localAnchorBY, Float.valueOf(wheelJointDef.localAnchorB.y))).floatValue() * this.tileHeight * this.unitScale);
            wheelJointDef.localAxisA.set(((Float) MapUtils.getProperty(properties, this.aliases.localAxisAX, Float.valueOf(wheelJointDef.localAxisA.x))).floatValue(), ((Float) MapUtils.getProperty(properties, this.aliases.localAxisAY, Float.valueOf(wheelJointDef.localAxisA.y))).floatValue());
            wheelJointDef.maxMotorTorque = ((Float) MapUtils.getProperty(properties, this.aliases.maxMotorTorque, Float.valueOf(wheelJointDef.maxMotorTorque))).floatValue();
            wheelJointDef.motorSpeed = ((Float) MapUtils.getProperty(properties, this.aliases.motorSpeed, Float.valueOf(wheelJointDef.motorSpeed))).floatValue();
        }
    }

    public static String findAvailableName(String desiredName, ObjectMap<String, ?> map) {
        if (desiredName == null) {
            desiredName = String.valueOf(map.size);
        }
        if (!map.containsKey(desiredName)) {
            return desiredName;
        }
        int duplicate = 1;
        while (map.containsKey(desiredName + duplicate)) {
            duplicate++;
        }
        return desiredName + duplicate;
    }

    public void reset() {
        this.aliases = new Aliases();
        this.listener = defaultListener;
        this.unitScale = 1.0f;
        this.tileWidth = 1.0f;
        this.tileHeight = 1.0f;
        this.triangulate = false;
        this.bodies.clear();
        this.fixtures.clear();
        this.joints.clear();
        this.heritage = null;
        this.mapProperties = null;
        this.layerProperties = null;
    }

    public float getUnitScale() {
        return this.unitScale;
    }

    public void setUnitScale(float unitScale2) {
        this.unitScale = unitScale2;
    }

    public boolean isIgnoreMapUnitScale() {
        return this.ignoreMapUnitScale;
    }

    public void setIgnoreMapUnitScale(boolean ignoreMapUnitScale2) {
        this.ignoreMapUnitScale = ignoreMapUnitScale2;
    }

    public boolean isIgnoreLayerUnitScale() {
        return this.ignoreLayerUnitScale;
    }

    public void setIgnoreLayerUnitScale(boolean ignoreLayerUnitScale2) {
        this.ignoreLayerUnitScale = ignoreLayerUnitScale2;
    }

    public float getTileWidth() {
        return this.tileWidth;
    }

    public void setTileWidth(float tileWidth2) {
        this.tileWidth = tileWidth2;
    }

    public float getTileHeight() {
        return this.tileHeight;
    }

    public void setTileHeight(float tileHeight2) {
        this.tileHeight = tileHeight2;
    }

    public boolean isTriangulate() {
        return this.triangulate;
    }

    public void setTriangulate(boolean triangulate2) {
        this.triangulate = triangulate2;
    }

    public Aliases getAliases() {
        return this.aliases;
    }

    public void setAliases(Aliases aliases2) {
        this.aliases = aliases2;
    }

    public Listener getListener() {
        return this.listener;
    }

    public void setListener(Listener listener2) {
        if (listener2 == null) {
            listener2 = defaultListener;
        }
        this.listener = listener2;
    }

    public ObjectMap<String, Body> getBodies() {
        return this.bodies;
    }

    public ObjectMap<String, Fixture> getFixtures() {
        return this.fixtures;
    }

    public ObjectMap<String, Joint> getJoints() {
        return this.joints;
    }

    public MapProperties getHeritage() {
        return this.heritage;
    }

    public void setHeritage(MapProperties heritage2) {
        this.heritage = heritage2;
    }
}
