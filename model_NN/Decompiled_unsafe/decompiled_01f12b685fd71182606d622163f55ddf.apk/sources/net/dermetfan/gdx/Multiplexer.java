package net.dermetfan.gdx;

import com.badlogic.gdx.utils.Array;

public abstract class Multiplexer<T> {
    protected final Array<T> receivers;

    public Multiplexer() {
        this.receivers = new Array<>();
    }

    public Multiplexer(int size) {
        this.receivers = new Array<>(size);
    }

    @SafeVarargs
    public Multiplexer(T... receivers2) {
        this.receivers = new Array<>(receivers2);
    }

    public Multiplexer(Array<T> receivers2) {
        this.receivers = new Array<>(receivers2);
    }

    public void add(T receiver) {
        this.receivers.add(receiver);
    }

    public boolean remove(T receiver) {
        return this.receivers.removeValue(receiver, true);
    }

    public void clear() {
        this.receivers.clear();
    }

    public int size() {
        return this.receivers.size;
    }

    public void setReceivers(Array<T> receivers2) {
        this.receivers.clear();
        this.receivers.addAll(receivers2);
    }

    public void setReceivers(T... receivers2) {
        this.receivers.clear();
        this.receivers.addAll(receivers2);
    }

    public Array<T> getReceivers() {
        return this.receivers;
    }
}
