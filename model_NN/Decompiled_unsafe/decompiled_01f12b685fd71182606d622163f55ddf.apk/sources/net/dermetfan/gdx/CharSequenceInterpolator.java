package net.dermetfan.gdx;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class CharSequenceInterpolator {
    private float charsPerSecond;
    private Interpolation interpolation = Interpolation.linear;
    private float time;

    public static float duration(int length, float charsPerSecond2) {
        return ((float) length) * (1.0f / charsPerSecond2);
    }

    public static float charsPerSecondFor(float duration, int length) {
        return ((float) length) / duration;
    }

    public static float linear(float time2, float charsPerSecond2, int length) {
        return time2 / duration(length, charsPerSecond2);
    }

    public static CharSequence interpolate(CharSequence seq, float time2, float charsPerSecond2, Interpolation interpolation2) {
        return interpolate(seq, time2, charsPerSecond2, 0, seq.length(), interpolation2);
    }

    public static CharSequence interpolate(CharSequence seq, float time2, float charsPerSecond2, int beginIndex, int endIndex, Interpolation interpolation2) {
        return seq.subSequence(beginIndex, (int) MathUtils.clamp(net.dermetfan.utils.math.MathUtils.replaceNaN(interpolation2.apply((float) beginIndex, (float) endIndex, linear(time2, charsPerSecond2, endIndex - beginIndex)), Animation.CurveTimeline.LINEAR), (float) beginIndex, (float) endIndex));
    }

    public CharSequenceInterpolator(float charsPerSecond2) {
        this.charsPerSecond = charsPerSecond2;
    }

    public void update(float delta) {
        this.time += delta;
    }

    public CharSequence interpolate(CharSequence seq) {
        return interpolate(seq, this.time, this.charsPerSecond, this.interpolation);
    }

    public CharSequence updateAndInterpolate(CharSequence seq, float delta) {
        update(delta);
        return interpolate(seq);
    }

    public Interpolation getInterpolation() {
        return this.interpolation;
    }

    public void setInterpolation(Interpolation interpolation2) {
        this.interpolation = interpolation2;
    }

    public float getCharsPerSecond() {
        return this.charsPerSecond;
    }

    public void setCharsPerSecond(float charsPerSecond2) {
        this.charsPerSecond = charsPerSecond2;
    }

    public float getTime() {
        return this.time;
    }

    public void setTime(float time2) {
        this.time = time2;
    }
}
