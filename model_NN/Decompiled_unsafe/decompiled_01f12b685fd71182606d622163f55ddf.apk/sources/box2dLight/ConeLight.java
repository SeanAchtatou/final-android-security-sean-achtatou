package box2dLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class ConeLight extends PositionalLight {
    float coneDegree;

    public ConeLight(RayHandler rayHandler, int rays, Color color, float distance, float x, float y, float directionDegree, float coneDegree2) {
        super(rayHandler, rays, color, distance, x, y, directionDegree);
        setConeDegree(coneDegree2);
    }

    public void update() {
        updateBody();
        if (this.dirty) {
            setEndPoints();
        }
        if (!cull()) {
            if (!this.staticLight || this.dirty) {
                this.dirty = false;
                updateMesh();
            }
        }
    }

    public void setDirection(float direction) {
        this.direction = direction;
        this.dirty = true;
    }

    public float getConeDegree() {
        return this.coneDegree;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public void setConeDegree(float coneDegree2) {
        this.coneDegree = MathUtils.clamp(coneDegree2, (float) Animation.CurveTimeline.LINEAR, 180.0f);
        this.dirty = true;
    }

    public void setDistance(float dist) {
        float dist2 = dist * RayHandler.gammaCorrectionParameter;
        if (dist2 < 0.01f) {
            dist2 = 0.01f;
        }
        this.distance = dist2;
        this.dirty = true;
    }

    /* access modifiers changed from: protected */
    public void setEndPoints() {
        for (int i = 0; i < this.rayNum; i++) {
            float angle = (this.direction + this.coneDegree) - (((2.0f * this.coneDegree) * ((float) i)) / (((float) this.rayNum) - 1.0f));
            float[] fArr = this.sin;
            float s = MathUtils.sinDeg(angle);
            fArr[i] = s;
            float[] fArr2 = this.cos;
            float c = MathUtils.cosDeg(angle);
            fArr2[i] = c;
            this.endX[i] = this.distance * c;
            this.endY[i] = this.distance * s;
        }
    }
}
