package box2dLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.kbz.esotericsoftware.spine.Animation;
import shaders.DiffuseShader;
import shaders.Gaussian;
import shaders.ShadowShader;
import shaders.WithoutShadowShader;

class LightMap {
    public static final int U1 = 2;
    public static final int U2 = 6;
    public static final int U3 = 10;
    public static final int U4 = 14;
    public static final int V1 = 3;
    public static final int V2 = 7;
    public static final int V3 = 11;
    public static final int V4 = 15;
    public static final int VERT_SIZE = 16;
    public static final int X1 = 0;
    public static final int X2 = 4;
    public static final int X3 = 8;
    public static final int X4 = 12;
    public static final int Y1 = 1;
    public static final int Y2 = 5;
    public static final int Y3 = 9;
    public static final int Y4 = 13;
    private ShaderProgram blurShader;
    private ShaderProgram diffuseShader;
    FrameBuffer frameBuffer;
    boolean lightMapDrawingDisabled;
    private Mesh lightMapMesh;
    private FrameBuffer pingPongBuffer;
    private RayHandler rayHandler;
    private ShaderProgram shadowShader;
    private ShaderProgram withoutShadowShader;

    public void render() {
        boolean needed;
        if (this.rayHandler.lightRenderedLastFrame > 0) {
            needed = true;
        } else {
            needed = false;
        }
        if (needed && this.rayHandler.blur) {
            gaussianBlur();
        }
        if (!this.lightMapDrawingDisabled) {
            this.frameBuffer.getColorBufferTexture().bind(0);
            if (this.rayHandler.shadows) {
                Color c = this.rayHandler.ambientLight;
                ShaderProgram shader = this.shadowShader;
                if (RayHandler.isDiffuse) {
                    shader = this.diffuseShader;
                    shader.begin();
                    this.rayHandler.diffuseBlendFunc.apply();
                    shader.setUniformf("ambient", c.r, c.g, c.b, c.a);
                } else {
                    shader.begin();
                    this.rayHandler.shadowBlendFunc.apply();
                    shader.setUniformf("ambient", c.r * c.a, c.g * c.a, c.b * c.a, 1.0f - c.a);
                }
                this.lightMapMesh.render(shader, 6);
                shader.end();
            } else if (needed) {
                this.rayHandler.simpleBlendFunc.apply();
                this.withoutShadowShader.begin();
                this.lightMapMesh.render(this.withoutShadowShader, 6);
                this.withoutShadowShader.end();
            }
            Gdx.gl20.glDisable(GL20.GL_BLEND);
        }
    }

    public void gaussianBlur() {
        Gdx.gl20.glDisable(GL20.GL_BLEND);
        for (int i = 0; i < this.rayHandler.blurNum; i++) {
            this.frameBuffer.getColorBufferTexture().bind(0);
            this.pingPongBuffer.begin();
            this.blurShader.begin();
            this.blurShader.setUniformf("dir", 1.0f, (float) Animation.CurveTimeline.LINEAR);
            this.lightMapMesh.render(this.blurShader, 6, 0, 4);
            this.blurShader.end();
            this.pingPongBuffer.end();
            this.pingPongBuffer.getColorBufferTexture().bind(0);
            this.frameBuffer.begin();
            this.blurShader.begin();
            this.blurShader.setUniformf("dir", (float) Animation.CurveTimeline.LINEAR, 1.0f);
            this.lightMapMesh.render(this.blurShader, 6, 0, 4);
            this.blurShader.end();
            if (this.rayHandler.customViewport) {
                this.frameBuffer.end(this.rayHandler.viewportX, this.rayHandler.viewportY, this.rayHandler.viewportWidth, this.rayHandler.viewportHeight);
            } else {
                this.frameBuffer.end();
            }
        }
        Gdx.gl20.glEnable(GL20.GL_BLEND);
    }

    public LightMap(RayHandler rayHandler2, int fboWidth, int fboHeight) {
        this.rayHandler = rayHandler2;
        fboWidth = fboWidth <= 0 ? 1 : fboWidth;
        fboHeight = fboHeight <= 0 ? 1 : fboHeight;
        this.frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, fboWidth, fboHeight, false);
        this.pingPongBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, fboWidth, fboHeight, false);
        this.lightMapMesh = createLightMapMesh();
        this.shadowShader = ShadowShader.createShadowShader();
        this.diffuseShader = DiffuseShader.createShadowShader();
        this.withoutShadowShader = WithoutShadowShader.createShadowShader();
        this.blurShader = Gaussian.createBlurShader(fboWidth, fboHeight);
    }

    /* access modifiers changed from: package-private */
    public void dispose() {
        this.shadowShader.dispose();
        this.blurShader.dispose();
        this.lightMapMesh.dispose();
        this.frameBuffer.dispose();
        this.pingPongBuffer.dispose();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
     arg types: [int, int, int, com.badlogic.gdx.graphics.VertexAttribute[]]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void */
    private Mesh createLightMapMesh() {
        float[] verts = new float[16];
        verts[0] = -1.0f;
        verts[1] = -1.0f;
        verts[4] = 1.0f;
        verts[5] = -1.0f;
        verts[8] = 1.0f;
        verts[9] = 1.0f;
        verts[12] = -1.0f;
        verts[13] = 1.0f;
        verts[2] = 0.0f;
        verts[3] = 0.0f;
        verts[6] = 1.0f;
        verts[7] = 0.0f;
        verts[10] = 1.0f;
        verts[11] = 1.0f;
        verts[14] = 0.0f;
        verts[15] = 1.0f;
        Mesh tmpMesh = new Mesh(true, 4, 0, new VertexAttribute(1, 2, ShaderProgram.POSITION_ATTRIBUTE), new VertexAttribute(16, 2, ShaderProgram.TEXCOORD_ATTRIBUTE));
        tmpMesh.setVertices(verts);
        return tmpMesh;
    }
}
