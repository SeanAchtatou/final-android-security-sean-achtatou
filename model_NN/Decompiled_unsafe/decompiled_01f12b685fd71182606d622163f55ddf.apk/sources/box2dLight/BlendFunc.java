package box2dLight;

import com.badlogic.gdx.Gdx;

public class BlendFunc {
    final int default_dfactor;
    final int default_sfactor;
    int dfactor;
    int sfactor;

    public BlendFunc(int sfactor2, int dfactor2) {
        this.default_sfactor = sfactor2;
        this.default_dfactor = dfactor2;
        this.sfactor = sfactor2;
        this.dfactor = dfactor2;
    }

    public void set(int sfactor2, int dfactor2) {
        this.sfactor = sfactor2;
        this.dfactor = dfactor2;
    }

    public void reset() {
        this.sfactor = this.default_sfactor;
        this.dfactor = this.default_dfactor;
    }

    public void apply() {
        Gdx.gl20.glBlendFunc(this.sfactor, this.dfactor);
    }
}
