package box2dLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;

public class PointLight extends PositionalLight {
    public PointLight(RayHandler rayHandler, int rays) {
        this(rayHandler, rays, Light.DefaultColor, 15.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public PointLight(RayHandler rayHandler, int rays, Color color, float distance, float x, float y) {
        super(rayHandler, rays, color, distance, x, y, Animation.CurveTimeline.LINEAR);
    }

    public void update() {
        updateBody();
        if (this.dirty) {
            setEndPoints();
        }
        if (!cull()) {
            if (!this.staticLight || this.dirty) {
                this.dirty = false;
                updateMesh();
            }
        }
    }

    public void setDistance(float dist) {
        float dist2 = dist * RayHandler.gammaCorrectionParameter;
        if (dist2 < 0.01f) {
            dist2 = 0.01f;
        }
        this.distance = dist2;
        this.dirty = true;
    }

    /* access modifiers changed from: package-private */
    public void setEndPoints() {
        float angleNum = 360.0f / ((float) (this.rayNum - 1));
        for (int i = 0; i < this.rayNum; i++) {
            float angle = angleNum * ((float) i);
            this.sin[i] = MathUtils.sinDeg(angle);
            this.cos[i] = MathUtils.cosDeg(angle);
            this.endX[i] = this.distance * this.cos[i];
            this.endY[i] = this.distance * this.sin[i];
        }
    }

    @Deprecated
    public void setDirection(float directionDegree) {
    }
}
