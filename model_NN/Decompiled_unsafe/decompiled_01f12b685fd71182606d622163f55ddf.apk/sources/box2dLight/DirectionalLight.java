package box2dLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.kbz.esotericsoftware.spine.Animation;

public class DirectionalLight extends Light {
    protected float cos;
    protected final Vector2[] end = new Vector2[this.rayNum];
    protected float sin;
    protected final Vector2[] start = new Vector2[this.rayNum];

    public DirectionalLight(RayHandler rayHandler, int rays, Color color, float directionDegree) {
        super(rayHandler, rays, color, Float.POSITIVE_INFINITY, directionDegree);
        this.vertexNum = (this.vertexNum - 1) * 2;
        for (int i = 0; i < this.rayNum; i++) {
            this.start[i] = new Vector2();
            this.end[i] = new Vector2();
        }
        this.lightMesh = new Mesh(Mesh.VertexDataType.VertexArray, this.staticLight, this.vertexNum, 0, new VertexAttribute(1, 2, "vertex_positions"), new VertexAttribute(4, 4, "quad_colors"), new VertexAttribute(32, 1, "s"));
        this.softShadowMesh = new Mesh(Mesh.VertexDataType.VertexArray, this.staticLight, this.vertexNum, 0, new VertexAttribute(1, 2, "vertex_positions"), new VertexAttribute(4, 4, "quad_colors"), new VertexAttribute(32, 1, "s"));
        update();
    }

    public void setDirection(float direction) {
        this.direction = direction;
        this.sin = MathUtils.sinDeg(direction);
        this.cos = MathUtils.cosDeg(direction);
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void update() {
        float sizeOfScreen;
        if (!this.staticLight || this.dirty) {
            this.dirty = false;
            float width = this.rayHandler.x2 - this.rayHandler.x1;
            float height = this.rayHandler.y2 - this.rayHandler.y1;
            if (width > height) {
                sizeOfScreen = width;
            } else {
                sizeOfScreen = height;
            }
            float xAxelOffSet = sizeOfScreen * this.cos;
            float yAxelOffSet = sizeOfScreen * this.sin;
            if (xAxelOffSet * xAxelOffSet < 0.1f && yAxelOffSet * yAxelOffSet < 0.1f) {
                xAxelOffSet = 1.0f;
                yAxelOffSet = 1.0f;
            }
            float widthOffSet = sizeOfScreen * (-this.sin);
            float heightOffSet = sizeOfScreen * this.cos;
            float x = ((this.rayHandler.x1 + this.rayHandler.x2) * 0.5f) - widthOffSet;
            float y = ((this.rayHandler.y1 + this.rayHandler.y2) * 0.5f) - heightOffSet;
            float portionX = (2.0f * widthOffSet) / ((float) (this.rayNum - 1));
            float x2 = ((float) MathUtils.floor(x / (2.0f * portionX))) * portionX * 2.0f;
            float portionY = (2.0f * heightOffSet) / ((float) (this.rayNum - 1));
            float y2 = ((float) MathUtils.ceil(y / (2.0f * portionY))) * portionY * 2.0f;
            for (int i = 0; i < this.rayNum; i++) {
                float steppedX = (((float) i) * portionX) + x2;
                float steppedY = (((float) i) * portionY) + y2;
                this.m_index = i;
                this.start[i].x = steppedX - xAxelOffSet;
                this.start[i].y = steppedY - yAxelOffSet;
                float[] fArr = this.mx;
                float f = steppedX + xAxelOffSet;
                this.end[i].x = f;
                fArr[i] = f;
                float[] fArr2 = this.my;
                float f2 = steppedY + yAxelOffSet;
                this.end[i].y = f2;
                fArr2[i] = f2;
                if (this.rayHandler.world != null && !this.xray) {
                    this.rayHandler.world.rayCast(this.ray, this.start[i], this.end[i]);
                }
            }
            int arraySize = this.rayNum;
            int size = 0;
            for (int i2 = 0; i2 < arraySize; i2++) {
                int size2 = size + 1;
                this.segments[size] = this.start[i2].x;
                int size3 = size2 + 1;
                this.segments[size2] = this.start[i2].y;
                int size4 = size3 + 1;
                this.segments[size3] = this.colorF;
                int size5 = size4 + 1;
                this.segments[size4] = 1.0f;
                int size6 = size5 + 1;
                this.segments[size5] = this.mx[i2];
                int size7 = size6 + 1;
                this.segments[size6] = this.my[i2];
                int size8 = size7 + 1;
                this.segments[size7] = this.colorF;
                size = size8 + 1;
                this.segments[size8] = 1.0f;
            }
            this.lightMesh.setVertices(this.segments, 0, size);
            if (this.soft && !this.xray) {
                int size9 = 0;
                for (int i3 = 0; i3 < arraySize; i3++) {
                    int size10 = size9 + 1;
                    this.segments[size9] = this.mx[i3];
                    int size11 = size10 + 1;
                    this.segments[size10] = this.my[i3];
                    int size12 = size11 + 1;
                    this.segments[size11] = this.colorF;
                    int size13 = size12 + 1;
                    this.segments[size12] = 1.0f;
                    int size14 = size13 + 1;
                    this.segments[size13] = this.mx[i3] + (this.softShadowLength * this.cos);
                    int size15 = size14 + 1;
                    this.segments[size14] = this.my[i3] + (this.softShadowLength * this.sin);
                    int size16 = size15 + 1;
                    this.segments[size15] = zeroColorBits;
                    size9 = size16 + 1;
                    this.segments[size16] = 1.0f;
                }
                this.softShadowMesh.setVertices(this.segments, 0, size9);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void render() {
        this.rayHandler.lightRenderedLastFrame++;
        this.lightMesh.render(this.rayHandler.lightShader, 5, 0, this.vertexNum);
        if (this.soft && !this.xray) {
            this.softShadowMesh.render(this.rayHandler.lightShader, 5, 0, this.vertexNum);
        }
    }

    public boolean contains(float x, float y) {
        boolean oddNodes = false;
        float[] fArr = this.mx;
        int i = this.rayNum;
        float x2 = this.start[0].x;
        fArr[i] = x2;
        float[] fArr2 = this.my;
        int i2 = this.rayNum;
        float y2 = this.start[0].y;
        fArr2[i2] = y2;
        for (int i3 = 0; i3 <= this.rayNum; i3++) {
            float x1 = this.mx[i3];
            float y1 = this.my[i3];
            if (((y1 < y && y2 >= y) || (y1 >= y && y2 < y)) && ((y - y1) / (y2 - y1)) * (x2 - x1) < x - x1) {
                if (!oddNodes) {
                    oddNodes = true;
                } else {
                    oddNodes = false;
                }
            }
            x2 = x1;
            y2 = y1;
        }
        for (int i4 = 0; i4 < this.rayNum; i4++) {
            float x12 = this.start[i4].x;
            float y12 = this.start[i4].y;
            if (((y12 < y && y2 >= y) || (y12 >= y && y2 < y)) && ((y - y12) / (y2 - y12)) * (x2 - x12) < x - x12) {
                if (!oddNodes) {
                    oddNodes = true;
                } else {
                    oddNodes = false;
                }
            }
            x2 = x12;
            y2 = y12;
        }
        return oddNodes;
    }

    @Deprecated
    public void attachToBody(Body body) {
    }

    @Deprecated
    public void setPosition(float x, float y) {
    }

    @Deprecated
    public Body getBody() {
        return null;
    }

    @Deprecated
    public float getX() {
        return Animation.CurveTimeline.LINEAR;
    }

    @Deprecated
    public float getY() {
        return Animation.CurveTimeline.LINEAR;
    }

    @Deprecated
    public void setPosition(Vector2 position) {
    }

    @Deprecated
    public void setDistance(float dist) {
    }
}
