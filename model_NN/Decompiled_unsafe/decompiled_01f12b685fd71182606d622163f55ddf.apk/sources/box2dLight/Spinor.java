package box2dLight;

import com.badlogic.gdx.utils.StringBuilder;
import com.kbz.esotericsoftware.spine.Animation;

public class Spinor {
    private static final float COSINE_THRESHOLD = 0.001f;
    float complex;
    float real;

    public Spinor() {
    }

    public Spinor(float angle) {
        set(angle);
    }

    public Spinor(Spinor copyFrom) {
        set(copyFrom);
    }

    public Spinor(float real2, float complex2) {
        set(real2, complex2);
    }

    public Spinor set(float angle) {
        float angle2 = angle / 2.0f;
        set((float) Math.cos((double) angle2), (float) Math.sin((double) angle2));
        return this;
    }

    public Spinor set(Spinor copyFrom) {
        set(copyFrom.real, copyFrom.complex);
        return this;
    }

    public Spinor set(float real2, float complex2) {
        this.real = real2;
        this.complex = complex2;
        return this;
    }

    public Spinor scale(float t) {
        this.real *= t;
        this.complex *= t;
        return this;
    }

    public Spinor invert() {
        this.complex = -this.complex;
        scale(len2());
        return this;
    }

    public Spinor add(Spinor other) {
        this.real += other.real;
        this.complex += other.complex;
        return this;
    }

    public Spinor add(float angle) {
        float angle2 = angle / 2.0f;
        this.real = (float) (((double) this.real) + Math.cos((double) angle2));
        this.complex = (float) (((double) this.complex) + Math.sin((double) angle2));
        return this;
    }

    public Spinor sub(Spinor other) {
        this.real -= other.real;
        this.complex -= other.complex;
        return this;
    }

    public Spinor sub(float angle) {
        float angle2 = angle / 2.0f;
        this.real = (float) (((double) this.real) - Math.cos((double) angle2));
        this.complex = (float) (((double) this.complex) - Math.sin((double) angle2));
        return this;
    }

    public float len() {
        return (float) Math.sqrt((double) ((this.real * this.real) + (this.complex * this.complex)));
    }

    public float len2() {
        return (this.real * this.real) + (this.complex * this.complex);
    }

    public Spinor mul(Spinor other) {
        set((this.real * other.real) - (this.complex * other.complex), (this.real * other.complex) + (this.complex * other.real));
        return this;
    }

    public Spinor nor() {
        float length = len();
        this.real /= length;
        this.complex /= length;
        return this;
    }

    public float angle() {
        return ((float) Math.atan2((double) this.complex, (double) this.real)) * 2.0f;
    }

    public Spinor lerp(Spinor end, float alpha, Spinor tmp) {
        scale(1.0f - alpha);
        tmp.set(end).scale(alpha);
        add(tmp);
        nor();
        return this;
    }

    public Spinor slerp(Spinor dest, float t) {
        float tc;
        float tr;
        float scale0;
        float scale1;
        float cosom = (this.real * dest.real) + (this.complex * dest.complex);
        if (cosom < Animation.CurveTimeline.LINEAR) {
            cosom = -cosom;
            tc = -dest.complex;
            tr = -dest.real;
        } else {
            tc = dest.complex;
            tr = dest.real;
        }
        if (1.0f - cosom > COSINE_THRESHOLD) {
            float omega = (float) Math.acos((double) cosom);
            float sinom = (float) Math.sin((double) omega);
            scale0 = ((float) Math.sin((double) ((1.0f - t) * omega))) / sinom;
            scale1 = ((float) Math.sin((double) (t * omega))) / sinom;
        } else {
            scale0 = 1.0f - t;
            scale1 = t;
        }
        this.complex = (this.complex * scale0) + (scale1 * tc);
        this.real = (this.real * scale0) + (scale1 * tr);
        return this;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        float radians = angle();
        result.append("radians: ");
        result.append(radians);
        result.append(", degrees: ");
        result.append(57.295776f * radians);
        return result.toString();
    }
}
