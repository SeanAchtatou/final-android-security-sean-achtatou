package box2dLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pools;
import com.kbz.esotericsoftware.spine.Animation;

public class ChainLight extends Light {
    public static float defaultRayStartOffset = 0.001f;
    protected Body body;
    protected float bodyAngle;
    protected float bodyAngleOffset;
    protected final Vector2 bodyPosition;
    public final FloatArray chain;
    protected final Rectangle chainLightBounds;
    protected final float[] endX;
    protected final float[] endY;
    protected int rayDirection;
    protected final Rectangle rayHandlerBounds;
    public float rayStartOffset;
    protected final Matrix3 restorePosition;
    protected final Matrix3 rotateAroundZero;
    protected final FloatArray segmentAngles;
    protected final FloatArray segmentLengths;
    protected final float[] startX;
    protected final float[] startY;
    protected final Vector2 tmpEnd;
    protected final Vector2 tmpPerp;
    protected final Vector2 tmpStart;
    protected final Vector2 tmpVec;
    protected final Matrix3 zeroPosition;

    public ChainLight(RayHandler rayHandler, int rays, Color color, float distance, int rayDirection2) {
        this(rayHandler, rays, color, distance, rayDirection2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(com.badlogic.gdx.graphics.Mesh$VertexDataType, boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
     arg types: [com.badlogic.gdx.graphics.Mesh$VertexDataType, int, int, int, com.badlogic.gdx.graphics.VertexAttribute[]]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
      com.badlogic.gdx.graphics.Mesh.<init>(com.badlogic.gdx.graphics.Mesh$VertexDataType, boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void */
    public ChainLight(RayHandler rayHandler, int rays, Color color, float distance, int rayDirection2, float[] chain2) {
        super(rayHandler, rays, color, distance, Animation.CurveTimeline.LINEAR);
        this.segmentAngles = new FloatArray();
        this.segmentLengths = new FloatArray();
        this.bodyPosition = new Vector2();
        this.tmpEnd = new Vector2();
        this.tmpStart = new Vector2();
        this.tmpPerp = new Vector2();
        this.tmpVec = new Vector2();
        this.zeroPosition = new Matrix3();
        this.rotateAroundZero = new Matrix3();
        this.restorePosition = new Matrix3();
        this.chainLightBounds = new Rectangle();
        this.rayHandlerBounds = new Rectangle();
        this.rayStartOffset = defaultRayStartOffset;
        this.rayDirection = rayDirection2;
        this.vertexNum = (this.vertexNum - 1) * 2;
        this.endX = new float[rays];
        this.endY = new float[rays];
        this.startX = new float[rays];
        this.startY = new float[rays];
        this.chain = chain2 != null ? new FloatArray(chain2) : new FloatArray();
        this.lightMesh = new Mesh(Mesh.VertexDataType.VertexArray, false, this.vertexNum, 0, new VertexAttribute(1, 2, "vertex_positions"), new VertexAttribute(4, 4, "quad_colors"), new VertexAttribute(32, 1, "s"));
        this.softShadowMesh = new Mesh(Mesh.VertexDataType.VertexArray, false, this.vertexNum * 2, 0, new VertexAttribute(1, 2, "vertex_positions"), new VertexAttribute(4, 4, "quad_colors"), new VertexAttribute(32, 1, "s"));
        setMesh();
    }

    /* access modifiers changed from: package-private */
    public void update() {
        if (this.dirty) {
            updateChain();
            applyAttachment();
        } else {
            updateBody();
        }
        if (!cull()) {
            if (!this.staticLight || this.dirty) {
                this.dirty = false;
                updateMesh();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void render() {
        if (!this.rayHandler.culling || !this.culled) {
            this.rayHandler.lightRenderedLastFrame++;
            this.lightMesh.render(this.rayHandler.lightShader, 5, 0, this.vertexNum);
            if (this.soft && !this.xray) {
                this.softShadowMesh.render(this.rayHandler.lightShader, 5, 0, this.vertexNum);
            }
        }
    }

    public void debugRender(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(Color.YELLOW);
        FloatArray vertices = (FloatArray) Pools.obtain(FloatArray.class);
        vertices.clear();
        for (int i = 0; i < this.rayNum; i++) {
            vertices.addAll(this.mx[i], this.my[i]);
        }
        for (int i2 = this.rayNum - 1; i2 > -1; i2--) {
            vertices.addAll(this.startX[i2], this.startY[i2]);
        }
        shapeRenderer.polygon(vertices.shrink());
        Pools.free(vertices);
    }

    public void attachToBody(Body body2) {
        attachToBody(body2, Animation.CurveTimeline.LINEAR);
    }

    public void attachToBody(Body body2, float degrees) {
        this.body = body2;
        this.bodyPosition.set(body2.getPosition());
        this.bodyAngleOffset = 0.017453292f * degrees;
        this.bodyAngle = body2.getAngle();
        applyAttachment();
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public Body getBody() {
        return this.body;
    }

    public float getX() {
        return this.tmpPosition.x;
    }

    public float getY() {
        return this.tmpPosition.y;
    }

    public void setPosition(float x, float y) {
        this.tmpPosition.x = x;
        this.tmpPosition.y = y;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public void setPosition(Vector2 position) {
        this.tmpPosition.x = position.x;
        this.tmpPosition.y = position.y;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public boolean contains(float x, float y) {
        boolean result = true;
        if (!this.chainLightBounds.contains(x, y)) {
            return false;
        }
        FloatArray vertices = (FloatArray) Pools.obtain(FloatArray.class);
        vertices.clear();
        for (int i = 0; i < this.rayNum; i++) {
            vertices.addAll(this.mx[i], this.my[i]);
        }
        for (int i2 = this.rayNum - 1; i2 > -1; i2--) {
            vertices.addAll(this.startX[i2], this.startY[i2]);
        }
        int intersects = 0;
        for (int i3 = 0; i3 < vertices.size; i3 += 2) {
            float x1 = vertices.items[i3];
            float y1 = vertices.items[i3 + 1];
            float x2 = vertices.items[(i3 + 2) % vertices.size];
            float y2 = vertices.items[(i3 + 3) % vertices.size];
            if (((y1 <= y && y < y2) || (y2 <= y && y < y1)) && x < (((x2 - x1) / (y2 - y1)) * (y - y1)) + x1) {
                intersects++;
            }
        }
        if ((intersects & 1) != 1) {
            result = false;
        }
        Pools.free(vertices);
        return result;
    }

    public void setDistance(float dist) {
        float dist2 = dist * RayHandler.gammaCorrectionParameter;
        if (dist2 < 0.01f) {
            dist2 = 0.01f;
        }
        this.distance = dist2;
        this.dirty = true;
    }

    @Deprecated
    public void setDirection(float directionDegree) {
    }

    public void updateChain() {
        float f;
        int segmentRays;
        Vector2 v1 = (Vector2) Pools.obtain(Vector2.class);
        Vector2 v2 = (Vector2) Pools.obtain(Vector2.class);
        Vector2 vSegmentStart = (Vector2) Pools.obtain(Vector2.class);
        Vector2 vDirection = (Vector2) Pools.obtain(Vector2.class);
        Vector2 vRayOffset = (Vector2) Pools.obtain(Vector2.class);
        Spinor tmpAngle = (Spinor) Pools.obtain(Spinor.class);
        Spinor previousAngle = (Spinor) Pools.obtain(Spinor.class);
        Spinor currentAngle = (Spinor) Pools.obtain(Spinor.class);
        Spinor nextAngle = (Spinor) Pools.obtain(Spinor.class);
        Spinor startAngle = (Spinor) Pools.obtain(Spinor.class);
        Spinor endAngle = (Spinor) Pools.obtain(Spinor.class);
        Spinor rayAngle = (Spinor) Pools.obtain(Spinor.class);
        int segmentCount = (this.chain.size / 2) - 1;
        this.segmentAngles.clear();
        this.segmentLengths.clear();
        float remainingLength = Animation.CurveTimeline.LINEAR;
        int i = 0;
        int j = 0;
        while (i < this.chain.size - 2) {
            v1.set(this.chain.items[i + 2], this.chain.items[i + 3]).sub(this.chain.items[i], this.chain.items[i + 1]);
            this.segmentLengths.add(v1.len());
            this.segmentAngles.add(v1.rotate90(this.rayDirection).angle() * 0.017453292f);
            remainingLength += this.segmentLengths.items[j];
            i += 2;
            j++;
        }
        int rayNumber = 0;
        int remainingRays = this.rayNum;
        int i2 = 0;
        while (i2 < segmentCount) {
            previousAngle.set(i2 == 0 ? this.segmentAngles.items[i2] : this.segmentAngles.items[i2 - 1]);
            currentAngle.set(this.segmentAngles.items[i2]);
            if (i2 == this.segmentAngles.size - 1) {
                f = this.segmentAngles.items[i2];
            } else {
                f = this.segmentAngles.items[i2 + 1];
            }
            nextAngle.set(f);
            startAngle.set(previousAngle).slerp(currentAngle, 0.5f);
            endAngle.set(currentAngle).slerp(nextAngle, 0.5f);
            int segmentVertex = i2 * 2;
            vSegmentStart.set(this.chain.items[segmentVertex], this.chain.items[segmentVertex + 1]);
            vDirection.set(this.chain.items[segmentVertex + 2], this.chain.items[segmentVertex + 3]).sub(vSegmentStart).nor();
            float raySpacing = remainingLength / ((float) remainingRays);
            if (i2 == segmentCount - 1) {
                segmentRays = remainingRays;
            } else {
                segmentRays = (int) ((this.segmentLengths.items[i2] / remainingLength) * ((float) remainingRays));
            }
            for (int j2 = 0; j2 < segmentRays; j2++) {
                float position = ((float) j2) * raySpacing;
                rayAngle.set(startAngle).slerp(endAngle, position / this.segmentLengths.items[i2]);
                float angle = rayAngle.angle();
                vRayOffset.set(this.rayStartOffset, Animation.CurveTimeline.LINEAR).rotateRad(angle);
                v1.set(vDirection).scl(position).add(vSegmentStart).add(vRayOffset);
                this.startX[rayNumber] = v1.x;
                this.startY[rayNumber] = v1.y;
                v2.set(this.distance, Animation.CurveTimeline.LINEAR).rotateRad(angle).add(v1);
                this.endX[rayNumber] = v2.x;
                this.endY[rayNumber] = v2.y;
                rayNumber++;
            }
            remainingRays -= segmentRays;
            remainingLength -= this.segmentLengths.items[i2];
            i2++;
        }
        Pools.free(v1);
        Pools.free(v2);
        Pools.free(vSegmentStart);
        Pools.free(vDirection);
        Pools.free(vRayOffset);
        Pools.free(previousAngle);
        Pools.free(currentAngle);
        Pools.free(nextAngle);
        Pools.free(startAngle);
        Pools.free(endAngle);
        Pools.free(rayAngle);
        Pools.free(tmpAngle);
    }

    /* access modifiers changed from: package-private */
    public void applyAttachment() {
        if (this.body != null && !this.staticLight) {
            this.restorePosition.setToTranslation(this.bodyPosition);
            this.rotateAroundZero.setToRotationRad(this.bodyAngle + this.bodyAngleOffset);
            for (int i = 0; i < this.rayNum; i++) {
                this.tmpVec.set(this.startX[i], this.startY[i]).mul(this.rotateAroundZero).mul(this.restorePosition);
                this.startX[i] = this.tmpVec.x;
                this.startY[i] = this.tmpVec.y;
                this.tmpVec.set(this.endX[i], this.endY[i]).mul(this.rotateAroundZero).mul(this.restorePosition);
                this.endX[i] = this.tmpVec.x;
                this.endY[i] = this.tmpVec.y;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean cull() {
        boolean z = false;
        if (!this.rayHandler.culling) {
            this.culled = false;
        } else {
            updateBoundingRects();
            if (this.chainLightBounds.width > Animation.CurveTimeline.LINEAR && this.chainLightBounds.height > Animation.CurveTimeline.LINEAR && !this.chainLightBounds.overlaps(this.rayHandlerBounds)) {
                z = true;
            }
            this.culled = z;
        }
        return this.culled;
    }

    /* access modifiers changed from: package-private */
    public void updateBody() {
        if (this.body != null && !this.staticLight) {
            Vector2 vec = this.body.getPosition();
            this.tmpVec.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR).sub(this.bodyPosition);
            this.bodyPosition.set(vec);
            this.zeroPosition.setToTranslation(this.tmpVec);
            this.restorePosition.setToTranslation(this.bodyPosition);
            this.rotateAroundZero.setToRotationRad(this.bodyAngle).inv().rotateRad(this.body.getAngle());
            this.bodyAngle = this.body.getAngle();
            for (int i = 0; i < this.rayNum; i++) {
                this.tmpVec.set(this.startX[i], this.startY[i]).mul(this.zeroPosition).mul(this.rotateAroundZero).mul(this.restorePosition);
                this.startX[i] = this.tmpVec.x;
                this.startY[i] = this.tmpVec.y;
                this.tmpVec.set(this.endX[i], this.endY[i]).mul(this.zeroPosition).mul(this.rotateAroundZero).mul(this.restorePosition);
                this.endX[i] = this.tmpVec.x;
                this.endY[i] = this.tmpVec.y;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateMesh() {
        for (int i = 0; i < this.rayNum; i++) {
            this.m_index = i;
            this.f[i] = 1.0f;
            this.tmpEnd.x = this.endX[i];
            this.mx[i] = this.tmpEnd.x;
            this.tmpEnd.y = this.endY[i];
            this.my[i] = this.tmpEnd.y;
            this.tmpStart.x = this.startX[i];
            this.tmpStart.y = this.startY[i];
            if (this.rayHandler.world != null && !this.xray) {
                this.rayHandler.world.rayCast(this.ray, this.tmpStart, this.tmpEnd);
            }
        }
        setMesh();
    }

    /* access modifiers changed from: protected */
    public void setMesh() {
        int size = 0;
        for (int i = 0; i < this.rayNum; i++) {
            int size2 = size + 1;
            this.segments[size] = this.startX[i];
            int size3 = size2 + 1;
            this.segments[size2] = this.startY[i];
            int size4 = size3 + 1;
            this.segments[size3] = this.colorF;
            int size5 = size4 + 1;
            this.segments[size4] = 1.0f;
            int size6 = size5 + 1;
            this.segments[size5] = this.mx[i];
            int size7 = size6 + 1;
            this.segments[size6] = this.my[i];
            int size8 = size7 + 1;
            this.segments[size7] = this.colorF;
            size = size8 + 1;
            this.segments[size8] = 1.0f - this.f[i];
        }
        this.lightMesh.setVertices(this.segments, 0, size);
        if (this.soft && !this.xray) {
            int size9 = 0;
            for (int i2 = 0; i2 < this.rayNum; i2++) {
                int size10 = size9 + 1;
                this.segments[size9] = this.mx[i2];
                int size11 = size10 + 1;
                this.segments[size10] = this.my[i2];
                int size12 = size11 + 1;
                this.segments[size11] = this.colorF;
                float s = 1.0f - this.f[i2];
                int size13 = size12 + 1;
                this.segments[size12] = s;
                this.tmpPerp.set(this.mx[i2], this.my[i2]).sub(this.startX[i2], this.startY[i2]).nor().scl(this.softShadowLength * s).add(this.mx[i2], this.my[i2]);
                int size14 = size13 + 1;
                this.segments[size13] = this.tmpPerp.x;
                int size15 = size14 + 1;
                this.segments[size14] = this.tmpPerp.y;
                int size16 = size15 + 1;
                this.segments[size15] = zeroColorBits;
                size9 = size16 + 1;
                this.segments[size16] = 0.0f;
            }
            this.softShadowMesh.setVertices(this.segments, 0, size9);
        }
    }

    /* access modifiers changed from: protected */
    public void updateBoundingRects() {
        float maxX = this.startX[0];
        float minX = this.startX[0];
        float maxY = this.startY[0];
        float minY = this.startY[0];
        for (int i = 0; i < this.rayNum; i++) {
            if (maxX <= this.startX[i]) {
                maxX = this.startX[i];
            }
            if (maxX <= this.mx[i]) {
                maxX = this.mx[i];
            }
            if (minX >= this.startX[i]) {
                minX = this.startX[i];
            }
            if (minX >= this.mx[i]) {
                minX = this.mx[i];
            }
            if (maxY <= this.startY[i]) {
                maxY = this.startY[i];
            }
            if (maxY <= this.my[i]) {
                maxY = this.my[i];
            }
            if (minY >= this.startY[i]) {
                minY = this.startY[i];
            }
            if (minY >= this.my[i]) {
                minY = this.my[i];
            }
        }
        this.chainLightBounds.set(minX, minY, maxX - minX, maxY - minY);
        this.rayHandlerBounds.set(this.rayHandler.x1, this.rayHandler.y1, this.rayHandler.x2 - this.rayHandler.x1, this.rayHandler.y2 - this.rayHandler.y1);
    }
}
