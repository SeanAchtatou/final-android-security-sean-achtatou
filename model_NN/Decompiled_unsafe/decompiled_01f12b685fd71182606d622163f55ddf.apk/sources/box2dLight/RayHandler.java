package box2dLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;
import shaders.LightShader;

public class RayHandler implements Disposable {
    static final float GAMMA_COR = 0.625f;
    static boolean gammaCorrection = false;
    static float gammaCorrectionParameter = 1.0f;
    public static boolean isDiffuse = false;
    final Color ambientLight;
    boolean blur;
    int blurNum;
    final Matrix4 combined;
    boolean culling;
    boolean customViewport;
    public final BlendFunc diffuseBlendFunc;
    final Array<Light> disabledLights;
    final Array<Light> lightList;
    final LightMap lightMap;
    int lightRenderedLastFrame;
    final ShaderProgram lightShader;
    public final BlendFunc shadowBlendFunc;
    boolean shadows;
    public final BlendFunc simpleBlendFunc;
    int viewportHeight;
    int viewportWidth;
    int viewportX;
    int viewportY;
    World world;
    float x1;
    float x2;
    float y1;
    float y2;

    public RayHandler(World world2) {
        this(world2, Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 4);
    }

    public RayHandler(World world2, int fboWidth, int fboHeigth) {
        this.diffuseBlendFunc = new BlendFunc(774, 0);
        this.shadowBlendFunc = new BlendFunc(1, 771);
        this.simpleBlendFunc = new BlendFunc(770, 1);
        this.combined = new Matrix4();
        this.ambientLight = new Color();
        this.lightList = new Array<>(false, 16);
        this.disabledLights = new Array<>(false, 16);
        this.culling = true;
        this.shadows = true;
        this.blur = true;
        this.blurNum = 1;
        this.customViewport = false;
        this.viewportX = 0;
        this.viewportY = 0;
        this.viewportWidth = Gdx.graphics.getWidth();
        this.viewportHeight = Gdx.graphics.getHeight();
        this.lightRenderedLastFrame = 0;
        this.world = world2;
        this.lightMap = new LightMap(this, fboWidth, fboHeigth);
        this.lightShader = LightShader.createLightShader();
    }

    public void setCombinedMatrix(OrthographicCamera camera) {
        setCombinedMatrix(camera.combined, camera.position.x, camera.position.y, camera.zoom * camera.viewportWidth, camera.zoom * camera.viewportHeight);
    }

    public void setCombinedMatrix(Matrix4 combined2) {
        System.arraycopy(combined2.val, 0, this.combined.val, 0, 16);
        float halfViewPortWidth = 1.0f / combined2.val[0];
        float x = (-halfViewPortWidth) * combined2.val[12];
        this.x1 = x - halfViewPortWidth;
        this.x2 = x + halfViewPortWidth;
        float halfViewPortHeight = 1.0f / combined2.val[5];
        float y = (-halfViewPortHeight) * combined2.val[13];
        this.y1 = y - halfViewPortHeight;
        this.y2 = y + halfViewPortHeight;
    }

    public void setCombinedMatrix(Matrix4 combined2, float x, float y, float viewPortWidth, float viewPortHeight) {
        System.arraycopy(combined2.val, 0, this.combined.val, 0, 16);
        float halfViewPortWidth = viewPortWidth * 0.5f;
        this.x1 = x - halfViewPortWidth;
        this.x2 = x + halfViewPortWidth;
        float halfViewPortHeight = viewPortHeight * 0.5f;
        this.y1 = y - halfViewPortHeight;
        this.y2 = y + halfViewPortHeight;
    }

    /* access modifiers changed from: package-private */
    public boolean intersect(float x, float y, float radius) {
        return this.x1 < x + radius && this.x2 > x - radius && this.y1 < y + radius && this.y2 > y - radius;
    }

    public void updateAndRender() {
        update();
        render();
    }

    public void update() {
        Iterator i$ = this.lightList.iterator();
        while (i$.hasNext()) {
            i$.next().update();
        }
    }

    public void render() {
        boolean useLightMap = false;
        this.lightRenderedLastFrame = 0;
        Gdx.gl.glDepthMask(false);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        this.simpleBlendFunc.apply();
        if (this.shadows || this.blur) {
            useLightMap = true;
        }
        if (useLightMap) {
            this.lightMap.frameBuffer.begin();
            Gdx.gl.glClearColor(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            Gdx.gl.glClear(16384);
        }
        this.lightShader.begin();
        this.lightShader.setUniformMatrix("u_projTrans", this.combined);
        Iterator i$ = this.lightList.iterator();
        while (i$.hasNext()) {
            i$.next().render();
        }
        this.lightShader.end();
        if (useLightMap) {
            if (this.customViewport) {
                this.lightMap.frameBuffer.end(this.viewportX, this.viewportY, this.viewportWidth, this.viewportHeight);
            } else {
                this.lightMap.frameBuffer.end();
            }
            this.lightMap.render();
        }
    }

    public boolean pointAtLight(float x, float y) {
        Iterator i$ = this.lightList.iterator();
        while (i$.hasNext()) {
            if (i$.next().contains(x, y)) {
                return true;
            }
        }
        return false;
    }

    public boolean pointAtShadow(float x, float y) {
        Iterator i$ = this.lightList.iterator();
        while (i$.hasNext()) {
            if (i$.next().contains(x, y)) {
                return false;
            }
        }
        return true;
    }

    public void dispose() {
        removeAll();
        if (this.lightMap != null) {
            this.lightMap.dispose();
        }
        if (this.lightShader != null) {
            this.lightShader.dispose();
        }
    }

    public void removeAll() {
        Iterator i$ = this.lightList.iterator();
        while (i$.hasNext()) {
            i$.next().dispose();
        }
        this.lightList.clear();
        Iterator i$2 = this.disabledLights.iterator();
        while (i$2.hasNext()) {
            i$2.next().dispose();
        }
        this.disabledLights.clear();
    }

    public void setCulling(boolean culling2) {
        this.culling = culling2;
    }

    public void setBlur(boolean blur2) {
        this.blur = blur2;
    }

    public void setBlurNum(int blurNum2) {
        this.blurNum = blurNum2;
    }

    public void setShadows(boolean shadows2) {
        this.shadows = shadows2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
     arg types: [float, ?, int]
     candidates:
      com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
      com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
      com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
      com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
      com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
    public void setAmbientLight(float ambientLight2) {
        this.ambientLight.a = MathUtils.clamp(ambientLight2, (float) Animation.CurveTimeline.LINEAR, 1.0f);
    }

    public void setAmbientLight(float r, float g, float b, float a) {
        this.ambientLight.set(r, g, b, a);
    }

    public void setAmbientLight(Color ambientLightColor) {
        this.ambientLight.set(ambientLightColor);
    }

    public void setWorld(World world2) {
        this.world = world2;
    }

    public static boolean getGammaCorrection() {
        return gammaCorrection;
    }

    public static void setGammaCorrection(boolean gammaCorrectionWanted) {
        gammaCorrection = gammaCorrectionWanted;
        gammaCorrectionParameter = gammaCorrection ? GAMMA_COR : 1.0f;
    }

    public static void useDiffuseLight(boolean useDiffuse) {
        isDiffuse = useDiffuse;
    }

    public void useCustomViewport(int x, int y, int width, int height) {
        this.customViewport = true;
        this.viewportX = x;
        this.viewportY = y;
        this.viewportWidth = width;
        this.viewportHeight = height;
    }

    public void useDefaultViewport() {
        this.customViewport = false;
    }

    public void setLightMapRendering(boolean isAutomatic) {
        this.lightMap.lightMapDrawingDisabled = !isAutomatic;
    }

    public Texture getLightMapTexture() {
        return this.lightMap.frameBuffer.getColorBufferTexture();
    }

    public FrameBuffer getLightMapBuffer() {
        return this.lightMap.frameBuffer;
    }
}
