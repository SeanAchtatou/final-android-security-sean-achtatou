package box2dLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.utils.Disposable;
import com.kbz.esotericsoftware.spine.Animation;

public abstract class Light implements Disposable {
    static final Color DefaultColor = new Color(0.75f, 0.75f, 0.5f, 0.75f);
    static final int MIN_RAYS = 3;
    /* access modifiers changed from: private */
    public static Filter filterA = null;
    static final float zeroColorBits = Color.toFloatBits((float) Animation.CurveTimeline.LINEAR, (float) Animation.CurveTimeline.LINEAR, (float) Animation.CurveTimeline.LINEAR, (float) Animation.CurveTimeline.LINEAR);
    protected boolean active = true;
    protected final Color color = new Color();
    protected float colorF;
    protected boolean culled = false;
    protected float direction;
    protected boolean dirty = true;
    protected float distance;
    protected float[] f;
    protected Mesh lightMesh;
    protected int m_index = 0;
    protected float[] mx;
    protected float[] my;
    final RayCastCallback ray = new RayCastCallback() {
        public final float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
            if (Light.filterA != null && !Light.this.contactFilter(fixture)) {
                return -1.0f;
            }
            Light.this.mx[Light.this.m_index] = point.x;
            Light.this.my[Light.this.m_index] = point.y;
            Light.this.f[Light.this.m_index] = fraction;
            return fraction;
        }
    };
    protected RayHandler rayHandler;
    protected int rayNum;
    protected float[] segments;
    protected boolean soft = true;
    protected float softShadowLength = 2.5f;
    protected Mesh softShadowMesh;
    protected boolean staticLight = false;
    protected final Vector2 tmpPosition = new Vector2();
    protected int vertexNum;
    protected boolean xray = false;

    public abstract void attachToBody(Body body);

    public abstract Body getBody();

    public abstract float getX();

    public abstract float getY();

    /* access modifiers changed from: package-private */
    public abstract void render();

    public abstract void setDirection(float f2);

    public abstract void setDistance(float f2);

    public abstract void setPosition(float f2, float f3);

    public abstract void setPosition(Vector2 vector2);

    /* access modifiers changed from: package-private */
    public abstract void update();

    public Light(RayHandler rayHandler2, int rays, Color color2, float distance2, float directionDegree) {
        rayHandler2.lightList.add(this);
        this.rayHandler = rayHandler2;
        setRayNum(rays);
        setColor(color2);
        setDistance(distance2);
        setDirection(directionDegree);
    }

    public Vector2 getPosition() {
        return this.tmpPosition;
    }

    public void setColor(Color newColor) {
        if (newColor != null) {
            this.color.set(newColor);
        } else {
            this.color.set(DefaultColor);
        }
        this.colorF = this.color.toFloatBits();
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public void setColor(float r, float g, float b, float a) {
        this.color.set(r, g, b, a);
        this.colorF = this.color.toFloatBits();
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public void add(RayHandler rayHandler2) {
        this.rayHandler = rayHandler2;
        if (this.active) {
            rayHandler2.lightList.add(this);
        } else {
            rayHandler2.disabledLights.add(this);
        }
    }

    public void remove() {
        if (this.active) {
            this.rayHandler.lightList.removeValue(this, false);
        } else {
            this.rayHandler.disabledLights.removeValue(this, false);
        }
        this.rayHandler = null;
    }

    public void dispose() {
        this.lightMesh.dispose();
        this.softShadowMesh.dispose();
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active2) {
        if (active2 != this.active) {
            this.active = active2;
            if (this.rayHandler == null) {
                return;
            }
            if (active2) {
                this.rayHandler.lightList.add(this);
                this.rayHandler.disabledLights.removeValue(this, true);
                return;
            }
            this.rayHandler.disabledLights.add(this);
            this.rayHandler.lightList.removeValue(this, true);
        }
    }

    public boolean isXray() {
        return this.xray;
    }

    public void setXray(boolean xray2) {
        this.xray = xray2;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public boolean isStaticLight() {
        return this.staticLight;
    }

    public void setStaticLight(boolean staticLight2) {
        this.staticLight = staticLight2;
        if (staticLight2) {
            this.dirty = true;
        }
    }

    public boolean isSoft() {
        return this.soft;
    }

    public void setSoft(boolean soft2) {
        this.soft = soft2;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public float getSoftShadowLength() {
        return this.softShadowLength;
    }

    public void setSoftnessLength(float softShadowLength2) {
        this.softShadowLength = softShadowLength2;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public Color getColor() {
        return this.color;
    }

    public float getDistance() {
        return this.distance / RayHandler.gammaCorrectionParameter;
    }

    public boolean contains(float x, float y) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void setRayNum(int rays) {
        if (rays < 3) {
            rays = 3;
        }
        this.rayNum = rays;
        this.vertexNum = rays + 1;
        this.segments = new float[(this.vertexNum * 8)];
        this.mx = new float[this.vertexNum];
        this.my = new float[this.vertexNum];
        this.f = new float[this.vertexNum];
    }

    /* access modifiers changed from: package-private */
    public boolean contactFilter(Fixture fixtureB) {
        Filter filterB = fixtureB.getFilterData();
        if (filterA.groupIndex == 0 || filterA.groupIndex != filterB.groupIndex) {
            if ((filterA.maskBits & filterB.categoryBits) == 0 || (filterA.categoryBits & filterB.maskBits) == 0) {
                return false;
            }
            return true;
        } else if (filterA.groupIndex > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void setContactFilter(Filter filter) {
        filterA = filter;
    }

    public static void setContactFilter(short categoryBits, short groupIndex, short maskBits) {
        filterA = new Filter();
        filterA.categoryBits = categoryBits;
        filterA.groupIndex = groupIndex;
        filterA.maskBits = maskBits;
    }
}
