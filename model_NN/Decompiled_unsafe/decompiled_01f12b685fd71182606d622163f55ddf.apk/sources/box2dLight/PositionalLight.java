package box2dLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.kbz.esotericsoftware.spine.Animation;

public abstract class PositionalLight extends Light {
    protected Body body;
    protected float bodyAngleOffset;
    protected float bodyOffsetX;
    protected float bodyOffsetY;
    protected float[] cos;
    protected float[] endX;
    protected float[] endY;
    protected float[] sin;
    protected final Vector2 start = new Vector2();
    protected final Vector2 tmpEnd = new Vector2();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(com.badlogic.gdx.graphics.Mesh$VertexDataType, boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
     arg types: [com.badlogic.gdx.graphics.Mesh$VertexDataType, int, int, int, com.badlogic.gdx.graphics.VertexAttribute[]]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
      com.badlogic.gdx.graphics.Mesh.<init>(com.badlogic.gdx.graphics.Mesh$VertexDataType, boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void */
    public PositionalLight(RayHandler rayHandler, int rays, Color color, float distance, float x, float y, float directionDegree) {
        super(rayHandler, rays, color, distance, directionDegree);
        this.start.x = x;
        this.start.y = y;
        this.lightMesh = new Mesh(Mesh.VertexDataType.VertexArray, false, this.vertexNum, 0, new VertexAttribute(1, 2, "vertex_positions"), new VertexAttribute(4, 4, "quad_colors"), new VertexAttribute(32, 1, "s"));
        this.softShadowMesh = new Mesh(Mesh.VertexDataType.VertexArray, false, this.vertexNum * 2, 0, new VertexAttribute(1, 2, "vertex_positions"), new VertexAttribute(4, 4, "quad_colors"), new VertexAttribute(32, 1, "s"));
        setMesh();
    }

    /* access modifiers changed from: package-private */
    public void update() {
        updateBody();
        if (!cull()) {
            if (!this.staticLight || this.dirty) {
                this.dirty = false;
                updateMesh();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void render() {
        if (!this.rayHandler.culling || !this.culled) {
            this.rayHandler.lightRenderedLastFrame++;
            this.lightMesh.render(this.rayHandler.lightShader, 6, 0, this.vertexNum);
            if (this.soft && !this.xray) {
                this.softShadowMesh.render(this.rayHandler.lightShader, 5, 0, (this.vertexNum - 1) * 2);
            }
        }
    }

    public void attachToBody(Body body2) {
        attachToBody(body2, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public void attachToBody(Body body2, float offsetX, float offsetY) {
        attachToBody(body2, offsetX, offsetY, Animation.CurveTimeline.LINEAR);
    }

    public void attachToBody(Body body2, float offsetX, float offSetY, float degrees) {
        this.body = body2;
        this.bodyOffsetX = offsetX;
        this.bodyOffsetY = offSetY;
        this.bodyAngleOffset = degrees;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public Vector2 getPosition() {
        this.tmpPosition.x = this.start.x;
        this.tmpPosition.y = this.start.y;
        return this.tmpPosition;
    }

    public Body getBody() {
        return this.body;
    }

    public float getX() {
        return this.start.x;
    }

    public float getY() {
        return this.start.y;
    }

    public void setPosition(float x, float y) {
        this.start.x = x;
        this.start.y = y;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public void setPosition(Vector2 position) {
        this.start.x = position.x;
        this.start.y = position.y;
        if (this.staticLight) {
            this.dirty = true;
        }
    }

    public boolean contains(float x, float y) {
        float x_d = this.start.x - x;
        float y_d = this.start.y - y;
        if (this.distance * this.distance <= (x_d * x_d) + (y_d * y_d)) {
            return false;
        }
        boolean oddNodes = false;
        float[] fArr = this.mx;
        int i = this.rayNum;
        float x2 = this.start.x;
        fArr[i] = x2;
        float[] fArr2 = this.my;
        int i2 = this.rayNum;
        float y2 = this.start.y;
        fArr2[i2] = y2;
        for (int i3 = 0; i3 <= this.rayNum; i3++) {
            float x1 = this.mx[i3];
            float y1 = this.my[i3];
            if (((y1 < y && y2 >= y) || (y1 >= y && y2 < y)) && ((y - y1) / (y2 - y1)) * (x2 - x1) < x - x1) {
                if (!oddNodes) {
                    oddNodes = true;
                } else {
                    oddNodes = false;
                }
            }
            x2 = x1;
            y2 = y1;
        }
        return oddNodes;
    }

    /* access modifiers changed from: protected */
    public void setRayNum(int rays) {
        super.setRayNum(rays);
        this.sin = new float[rays];
        this.cos = new float[rays];
        this.endX = new float[rays];
        this.endY = new float[rays];
    }

    /* access modifiers changed from: protected */
    public boolean cull() {
        this.culled = this.rayHandler.culling && !this.rayHandler.intersect(this.start.x, this.start.y, this.distance + this.softShadowLength);
        return this.culled;
    }

    /* access modifiers changed from: protected */
    public void updateBody() {
        if (this.body != null && !this.staticLight) {
            Vector2 vec = this.body.getPosition();
            float angle = this.body.getAngle();
            float cos2 = MathUtils.cos(angle);
            float sin2 = MathUtils.sin(angle);
            float dX = (this.bodyOffsetX * cos2) - (this.bodyOffsetY * sin2);
            float dY = (this.bodyOffsetX * sin2) + (this.bodyOffsetY * cos2);
            this.start.x = vec.x + dX;
            this.start.y = vec.y + dY;
            setDirection(this.bodyAngleOffset + (57.295776f * angle));
        }
    }

    /* access modifiers changed from: protected */
    public void updateMesh() {
        for (int i = 0; i < this.rayNum; i++) {
            this.m_index = i;
            this.f[i] = 1.0f;
            this.tmpEnd.x = this.endX[i] + this.start.x;
            this.mx[i] = this.tmpEnd.x;
            this.tmpEnd.y = this.endY[i] + this.start.y;
            this.my[i] = this.tmpEnd.y;
            if (this.rayHandler.world != null && !this.xray) {
                this.rayHandler.world.rayCast(this.ray, this.start, this.tmpEnd);
            }
        }
        setMesh();
    }

    /* access modifiers changed from: protected */
    public void setMesh() {
        int size = 0 + 1;
        this.segments[0] = this.start.x;
        int size2 = size + 1;
        this.segments[size] = this.start.y;
        int size3 = size2 + 1;
        this.segments[size2] = this.colorF;
        int size4 = size3 + 1;
        this.segments[size3] = 1.0f;
        for (int i = 0; i < this.rayNum; i++) {
            int size5 = size4 + 1;
            this.segments[size4] = this.mx[i];
            int size6 = size5 + 1;
            this.segments[size5] = this.my[i];
            int size7 = size6 + 1;
            this.segments[size6] = this.colorF;
            size4 = size7 + 1;
            this.segments[size7] = 1.0f - this.f[i];
        }
        this.lightMesh.setVertices(this.segments, 0, size4);
        if (this.soft && !this.xray) {
            int size8 = 0;
            for (int i2 = 0; i2 < this.rayNum; i2++) {
                int size9 = size8 + 1;
                this.segments[size8] = this.mx[i2];
                int size10 = size9 + 1;
                this.segments[size9] = this.my[i2];
                int size11 = size10 + 1;
                this.segments[size10] = this.colorF;
                float s = 1.0f - this.f[i2];
                int size12 = size11 + 1;
                this.segments[size11] = s;
                int size13 = size12 + 1;
                this.segments[size12] = this.mx[i2] + (this.softShadowLength * s * this.cos[i2]);
                int size14 = size13 + 1;
                this.segments[size13] = this.my[i2] + (this.softShadowLength * s * this.sin[i2]);
                int size15 = size14 + 1;
                this.segments[size14] = zeroColorBits;
                size8 = size15 + 1;
                this.segments[size15] = 0.0f;
            }
            this.softShadowMesh.setVertices(this.segments, 0, size8);
        }
    }
}
