package a.a.a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class a implements InvocationHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ b f0a;
    private /* synthetic */ c b;

    a(c cVar, b bVar) {
        this.b = cVar;
        this.f0a = bVar;
    }

    public final Object invoke(Object obj, Method method, Object[] objArr) {
        c.f1a.b("invoke: " + method.getName() + " listener: " + this.f0a);
        if ("onZoom".equals(method.getName())) {
            this.f0a.a(((Boolean) objArr[0]).booleanValue());
            return null;
        } else if ("onVisibilityChanged".equals(method.getName())) {
            ((Boolean) objArr[0]).booleanValue();
            return null;
        } else {
            c.f1a.b("unhandled listener method: " + method);
            return null;
        }
    }
}
