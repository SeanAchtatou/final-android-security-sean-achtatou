package com.agilebinary.a.a.a.g;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class d extends f implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f96a;

    public d(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Source byte array may not be null");
        }
        this.f96a = bArr;
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        outputStream.write(this.f96a);
        outputStream.flush();
    }

    public final boolean a() {
        return true;
    }

    public final long c() {
        return (long) this.f96a.length;
    }

    public final Object clone() {
        return super.clone();
    }

    public final InputStream f() {
        return new ByteArrayInputStream(this.f96a);
    }

    public final boolean g() {
        return false;
    }
}
