package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.h;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.k.m;
import java.util.StringTokenizer;

public final class g implements k {
    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    private static int[] a(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
        int[] iArr = new int[stringTokenizer.countTokens()];
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            try {
                iArr[i] = Integer.parseInt(stringTokenizer.nextToken().trim());
                if (iArr[i] < 0) {
                    throw new e("Invalid Port attribute.");
                }
                i++;
            } catch (NumberFormatException e) {
                throw new e("Invalid Port attribute: " + e.getMessage());
            }
        }
        return iArr;
    }

    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (bVar instanceof h) {
            h hVar = (h) bVar;
            if (str != null && str.trim().length() > 0) {
                hVar.a(a(str));
            }
        }
    }

    public final void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            int c = fVar.c();
            if ((cVar instanceof m) && ((m) cVar).e("port") && !a(c, cVar.g())) {
                throw new i("Port attribute violates RFC 2965: Request port not found in cookie's port list.");
            }
        }
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            int c = fVar.c();
            if ((cVar instanceof m) && ((m) cVar).e("port")) {
                if (cVar.g() == null) {
                    return false;
                }
                if (!a(c, cVar.g())) {
                    return false;
                }
            }
            return true;
        }
    }
}
