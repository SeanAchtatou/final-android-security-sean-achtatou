package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c.c.g;
import com.agilebinary.a.a.a.c.c.j;
import com.agilebinary.a.a.a.c.c.k;
import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.x;
import java.io.OutputStream;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final a f82a;

    public c(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Content length strategy may not be null");
        }
        this.f82a = aVar;
    }

    public final void a(e eVar, x xVar, com.agilebinary.a.a.a.c cVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        } else {
            long a2 = this.f82a.a(xVar);
            OutputStream gVar = a2 == -2 ? new g(eVar) : a2 == -1 ? new k(eVar) : new j(eVar, a2);
            cVar.a(gVar);
            gVar.close();
        }
    }
}
