package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.c.b.e;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.logging.Log;

public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    protected final Lock f41a = new ReentrantLock();
    protected Set b = new HashSet();
    private final Log c = a.a(getClass());
    private volatile boolean d;
    private e e = new e();

    protected j() {
    }

    public void a() {
        this.f41a.lock();
        try {
            if (this.d) {
                this.f41a.unlock();
                return;
            }
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                it.remove();
                g c2 = ((g) it.next()).c();
                if (c2 != null) {
                    c2.k();
                }
            }
            this.e.a();
            this.d = true;
            this.f41a.unlock();
        } catch (IOException e2) {
            this.c.debug("I/O error closing connection", e2);
        } catch (Throwable th) {
            this.f41a.unlock();
            throw th;
        }
    }

    public void a(long j, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        }
        this.f41a.lock();
        try {
            this.e.a(timeUnit.toMillis(j));
        } finally {
            this.f41a.unlock();
        }
    }
}
