package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import java.net.InetAddress;

public final class f implements b, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final b f108a;
    private final InetAddress b;
    private boolean c;
    private b[] d;
    private g e;
    private e f;
    private boolean g;

    private f(b bVar, InetAddress inetAddress) {
        if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        }
        this.f108a = bVar;
        this.b = inetAddress;
        this.e = g.PLAIN;
        this.f = e.PLAIN;
    }

    public f(c cVar) {
        this(cVar.a(), cVar.b());
    }

    public final b a() {
        return this.f108a;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Hop index must not be negative: " + i);
        }
        int c2 = c();
        if (i < c2) {
            return i < c2 - 1 ? this.d[i] : this.f108a;
        }
        throw new IllegalArgumentException("Hop index " + i + " exceeds tracked route length " + c2 + ".");
    }

    public final void a(b bVar, boolean z) {
        if (bVar == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        } else if (this.c) {
            throw new IllegalStateException("Already connected.");
        } else {
            this.c = true;
            this.d = new b[]{bVar};
            this.g = z;
        }
    }

    public final void a(boolean z) {
        if (this.c) {
            throw new IllegalStateException("Already connected.");
        }
        this.c = true;
        this.g = z;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final void b(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No tunnel unless connected.");
        } else if (this.d == null) {
            throw new IllegalStateException("No tunnel without proxy.");
        } else {
            this.e = g.TUNNELLED;
            this.g = z;
        }
    }

    public final int c() {
        if (!this.c) {
            return 0;
        }
        if (this.d == null) {
            return 1;
        }
        return this.d.length + 1;
    }

    public final void c(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No layered protocol unless connected.");
        }
        this.f = e.LAYERED;
        this.g = z;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.TUNNELLED;
    }

    public final boolean e() {
        return this.f == e.LAYERED;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        boolean equals = this.f108a.equals(fVar.f108a) & (this.b == fVar.b || (this.b != null && this.b.equals(fVar.b))) & (this.d == fVar.d || !(this.d == null || fVar.d == null || this.d.length != fVar.d.length)) & (this.c == fVar.c && this.g == fVar.g && this.e == fVar.e && this.f == fVar.f);
        if (!equals || this.d == null) {
            return equals;
        }
        boolean z = equals;
        int i = 0;
        while (z && i < this.d.length) {
            z = this.d[i].equals(fVar.d[i]);
            i++;
        }
        return z;
    }

    public final boolean f() {
        return this.g;
    }

    public final boolean g() {
        return this.c;
    }

    public final c h() {
        if (!this.c) {
            return null;
        }
        return new c(this.f108a, this.b, this.d, this.g, this.e, this.f);
    }

    public final int hashCode() {
        int hashCode = this.f108a.hashCode();
        if (this.b != null) {
            hashCode ^= this.b.hashCode();
        }
        if (this.d != null) {
            int length = hashCode ^ this.d.length;
            for (b hashCode2 : this.d) {
                length ^= hashCode2.hashCode();
            }
            hashCode = length;
        }
        if (this.c) {
            hashCode ^= 286331153;
        }
        if (this.g) {
            hashCode ^= 572662306;
        }
        return (hashCode ^ this.e.hashCode()) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append("RouteTracker[");
        if (this.b != null) {
            sb.append(this.b);
            sb.append("->");
        }
        sb.append('{');
        if (this.c) {
            sb.append('c');
        }
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == e.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        if (this.d != null) {
            for (b append : this.d) {
                sb.append(append);
                sb.append("->");
            }
        }
        sb.append(this.f108a);
        sb.append(']');
        return sb.toString();
    }
}
