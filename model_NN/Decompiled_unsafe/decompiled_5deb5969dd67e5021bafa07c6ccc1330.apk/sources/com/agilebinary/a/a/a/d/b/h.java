package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.t;
import java.util.Collection;

public final class h implements ab {
    public final void a(f fVar, k kVar) {
        Collection<t> collection;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!fVar.a().a().equalsIgnoreCase("CONNECT") && (collection = (Collection) fVar.g().a("http.default-headers")) != null) {
            for (t a2 : collection) {
                fVar.a(a2);
            }
        }
    }
}
