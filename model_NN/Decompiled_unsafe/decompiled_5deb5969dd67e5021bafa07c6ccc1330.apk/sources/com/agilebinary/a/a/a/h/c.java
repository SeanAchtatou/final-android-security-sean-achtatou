package com.agilebinary.a.a.a.h;

import java.io.IOException;
import java.io.InputStream;

public final class c extends InputStream implements d {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f105a;
    private boolean b;
    private final e c;

    public c(InputStream inputStream, e eVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("Wrapped stream may not be null.");
        }
        this.f105a = inputStream;
        this.b = false;
        this.c = eVar;
    }

    private void a(int i) {
        if (this.f105a != null && i < 0) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.a(this.f105a);
                }
                if (z) {
                    this.f105a.close();
                }
            } finally {
                this.f105a = null;
            }
        }
    }

    private boolean c() {
        if (!this.b) {
            return this.f105a != null;
        }
        throw new IOException("Attempted read on closed stream.");
    }

    private void d() {
        if (this.f105a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.j_();
                }
                if (z) {
                    this.f105a.close();
                }
            } finally {
                this.f105a = null;
            }
        }
    }

    public final int available() {
        if (!c()) {
            return 0;
        }
        try {
            return this.f105a.available();
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final void b() {
        this.b = true;
        d();
    }

    public final void close() {
        this.b = true;
        if (this.f105a != null) {
            try {
                if (this.c != null ? this.c.b(this.f105a) : true) {
                    this.f105a.close();
                }
            } finally {
                this.f105a = null;
            }
        }
    }

    public final void d_() {
        close();
    }

    public final int read() {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.f105a.read();
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final int read(byte[] bArr) {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.f105a.read(bArr);
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.f105a.read(bArr, i, i2);
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }
}
