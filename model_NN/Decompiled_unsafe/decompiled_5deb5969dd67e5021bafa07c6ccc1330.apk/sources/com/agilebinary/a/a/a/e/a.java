package com.agilebinary.a.a.a.e;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class a extends d implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f90a = new HashMap();

    public e a(String str, Object obj) {
        this.f90a.put(str, obj);
        return this;
    }

    public Object a(String str) {
        return this.f90a.get(str);
    }

    public Object clone() {
        a aVar = (a) super.clone();
        for (Map.Entry entry : this.f90a.entrySet()) {
            if (entry.getKey() instanceof String) {
                aVar.a((String) entry.getKey(), entry.getValue());
            }
        }
        return aVar;
    }
}
