package com.agilebinary.a.a.a.c.b.a;

import java.util.Date;
import java.util.concurrent.locks.Condition;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    private final Condition f42a;
    private final c b;
    private Thread c;
    private boolean d;

    public k(Condition condition, c cVar) {
        if (condition == null) {
            throw new IllegalArgumentException("Condition must not be null.");
        }
        this.f42a = condition;
        this.b = cVar;
    }

    public final void a() {
        if (this.c == null) {
            throw new IllegalStateException("Nobody waiting on this object.");
        }
        this.f42a.signalAll();
    }

    public final boolean a(Date date) {
        boolean z;
        if (this.c != null) {
            throw new IllegalStateException("A thread is already waiting on this object.\ncaller: " + Thread.currentThread() + "\nwaiter: " + this.c);
        } else if (this.d) {
            throw new InterruptedException("Operation interrupted");
        } else {
            this.c = Thread.currentThread();
            if (date != null) {
                try {
                    z = this.f42a.awaitUntil(date);
                } catch (Throwable th) {
                    this.c = null;
                    throw th;
                }
            } else {
                this.f42a.await();
                z = true;
            }
            if (this.d) {
                throw new InterruptedException("Operation interrupted");
            }
            this.c = null;
            return z;
        }
    }

    public final void b() {
        this.d = true;
        this.f42a.signalAll();
    }
}
