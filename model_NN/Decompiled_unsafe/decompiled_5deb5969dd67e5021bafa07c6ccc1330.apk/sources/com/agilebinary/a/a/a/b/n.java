package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f;

public final class n extends l implements f {
    private final String c;
    private final String d;
    private d e;

    private n(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Request line may not be null");
        }
        this.e = dVar;
        this.c = dVar.a();
        this.d = dVar.c();
    }

    public n(String str, String str2, a aVar) {
        this(new q(str, str2, aVar));
    }

    public final d a() {
        if (this.e == null) {
            this.e = new q(this.c, this.d, b.b(g()));
        }
        return this.e;
    }

    public final a c() {
        return a().b();
    }

    public final String toString() {
        return this.c + " " + this.d + " " + this.f15a;
    }
}
