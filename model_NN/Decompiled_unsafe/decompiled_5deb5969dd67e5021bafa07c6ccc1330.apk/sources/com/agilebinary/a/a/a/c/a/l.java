package com.agilebinary.a.a.a.c.a;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f29a = {"EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private static final Date b;
    private static TimeZone c = TimeZone.getTimeZone("GMT");

    static {
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(c);
        instance.set(2000, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        b = instance.getTime();
    }

    private l() {
    }

    public static Date a(String str, String[] strArr) {
        return b(str, strArr);
    }

    private static Date b(String str, String[] strArr) {
        if (str == null) {
            throw new IllegalArgumentException("dateValue is null");
        }
        String[] strArr2 = strArr == null ? f29a : strArr;
        Date date = b;
        String substring = (str.length() <= 1 || !str.startsWith("'") || !str.endsWith("'")) ? str : str.substring(1, str.length() - 1);
        int length = strArr2.length;
        int i = 0;
        while (i < length) {
            SimpleDateFormat a2 = ad.a(strArr2[i]);
            a2.set2DigitYearStart(date);
            try {
                return a2.parse(substring);
            } catch (ParseException e) {
                i++;
            }
        }
        throw new k("Unable to parse the date " + substring);
    }
}
