package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.e.e;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

class d implements i {

    /* renamed from: a  reason: collision with root package name */
    private final a f101a;

    d(a aVar) {
        this.f101a = aVar;
    }

    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, e eVar) {
        int i;
        InetAddress inetAddress;
        String hostName = inetSocketAddress.getHostName();
        int port = inetSocketAddress.getPort();
        if (inetSocketAddress2 != null) {
            InetAddress address = inetSocketAddress2.getAddress();
            i = inetSocketAddress2.getPort();
            inetAddress = address;
        } else {
            i = 0;
            inetAddress = null;
        }
        return this.f101a.a(socket, hostName, port, inetAddress, i, eVar);
    }

    public final boolean a(Socket socket) {
        return this.f101a.a(socket);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof d ? this.f101a.equals(((d) obj).f101a) : this.f101a.equals(obj);
    }

    public final Socket f_() {
        return this.f101a.a();
    }

    public int hashCode() {
        return this.f101a.hashCode();
    }
}
