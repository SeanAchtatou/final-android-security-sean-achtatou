package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.p;

public final class e implements ab {
    public final void a(f fVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!(fVar instanceof p)) {
        } else {
            if (fVar.a("Transfer-Encoding")) {
                throw new l("Transfer-encoding header already present");
            } else if (fVar.a("Content-Length")) {
                throw new l("Content-Length header already present");
            } else {
                a b = fVar.a().b();
                c h = ((p) fVar).h();
                if (h == null) {
                    fVar.a("Content-Length", "0");
                    return;
                }
                if (!h.k_() && h.c() >= 0) {
                    fVar.a("Content-Length", Long.toString(h.c()));
                } else if (b.a(aa.c)) {
                    throw new l("Chunked transfer encoding not allowed for " + b);
                } else {
                    fVar.a("Transfer-Encoding", "chunked");
                }
                if (h.d() != null && !fVar.a("Content-Type")) {
                    fVar.a(h.d());
                }
                if (h.e() != null && !fVar.a("Content-Encoding")) {
                    fVar.a(h.e());
                }
            }
        }
    }
}
