package com.agilebinary.mobilemonitor.client.android.a.a;

import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.Serializable;

public class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f146a = b.a();
    private int b;
    private int c;
    private int d;

    public d() {
    }

    public d(int i, int i2, int i3) {
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    public String a() {
        return "{\"base_station_id\":\"" + this.b + "\", \"" + "network_id" + "\":\"" + this.c + "\", \"" + "system_id" + "\":\"" + this.d + "\"}";
    }

    public final void a(int i) {
        this.b = i;
    }

    public final int b() {
        return this.b;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final int c() {
        return this.c;
    }

    public final void c(int i) {
        this.d = i;
    }

    public final int d() {
        return this.d;
    }

    public String toString() {
        return "GeocodeCdmaCell: " + a();
    }
}
