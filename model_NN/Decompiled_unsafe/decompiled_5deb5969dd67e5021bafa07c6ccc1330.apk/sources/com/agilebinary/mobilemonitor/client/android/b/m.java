package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;

public final class m extends g {

    /* renamed from: a  reason: collision with root package name */
    private static m f177a;

    private m(Context context) {
        super(context, "settings");
    }

    public static m a(Context context) {
        if (f177a == null) {
            f177a = new m(context);
        }
        return f177a;
    }
}
