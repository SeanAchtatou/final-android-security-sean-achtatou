package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.j;
import com.agilebinary.mobilemonitor.client.android.a.o;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.h;
import com.agilebinary.mobilemonitor.client.android.ui.EventListActivity_base;
import java.util.HashMap;
import java.util.Map;

public final class b extends AsyncTask implements g, o {

    /* renamed from: a  reason: collision with root package name */
    private static String f209a = com.agilebinary.mobilemonitor.client.android.c.b.a();
    private static Map b = new HashMap();
    private byte c;
    private j d;
    private com.agilebinary.mobilemonitor.client.android.b.j e = this.f.g.e();
    private EventListActivity_base f;
    private Class g;
    private com.agilebinary.a.a.a.d.c.b h;
    private boolean i;

    public b(EventListActivity_base eventListActivity_base, j jVar, byte b2, Class cls) {
        this.c = b2;
        this.f = eventListActivity_base;
        this.d = jVar;
        this.g = cls;
        a(cls, this);
    }

    public static synchronized b a(Class cls) {
        b bVar;
        synchronized (b.class) {
            bVar = (b) b.get(cls);
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public e doInBackground(h... hVarArr) {
        e eVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.f.getSystemService("power")).newWakeLock(6, f209a);
        newWakeLock.acquire();
        try {
            h hVar = new h(this.c, hVarArr[0].a(), hVarArr[0].b());
            if (isCancelled()) {
                b(this.g);
                this.f.g.f();
                try {
                    newWakeLock.release();
                } catch (Exception e2) {
                }
                return null;
            }
            s a2 = this.d.a(this.f.g.a(), hVar, this.e, this, this);
            if (a2 != null && a2.c()) {
                throw new q(a2.b());
            } else if (isCancelled()) {
                b(this.g);
                this.f.g.f();
                try {
                    newWakeLock.release();
                } catch (Exception e3) {
                }
                return null;
            } else {
                if (a2 != null) {
                    if (!a2.f()) {
                        eVar = new e();
                        b(this.g);
                        this.f.g.f();
                        newWakeLock.release();
                        "" + eVar;
                        return eVar;
                    }
                }
                eVar = null;
                b(this.g);
                this.f.g.f();
                try {
                    newWakeLock.release();
                } catch (Exception e4) {
                }
                "" + eVar;
                return eVar;
            }
        } catch (q e5) {
            e eVar2 = new e(e5);
            b(this.g);
            this.f.g.f();
            try {
                newWakeLock.release();
                eVar = eVar2;
            } catch (Exception e6) {
                eVar = eVar2;
            }
        } catch (Throwable th) {
            b(this.g);
            this.f.g.f();
            try {
                newWakeLock.release();
            } catch (Exception e7) {
            }
            throw th;
        }
    }

    private static synchronized void a(Class cls, b bVar) {
        synchronized (b.class) {
            b.put(cls, bVar);
        }
    }

    private static synchronized void b(Class cls) {
        synchronized (b.class) {
            b.remove(cls);
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.h = bVar;
    }

    public final void a(boolean z) {
        try {
            this.i = z;
            cancel(false);
            if (this.h != null) {
                try {
                    this.h.d();
                } catch (Exception e2) {
                }
            }
            this.e.a(true);
            this.e.c();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public final void h() {
        publishProgress(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        try {
            if (!this.i) {
                EventListActivity_base.a(this.f, this.c, (e) null);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        e eVar = (e) obj;
        if (!isCancelled()) {
            EventListActivity_base.a(this.f, this.c, eVar);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        this.f.h();
    }
}
