package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    double f144a;
    double b;
    double c;
    String d;
    String e;
    String f;
    String g;
    String h;

    public b() {
    }

    public b(double d2, double d3, double d4) {
        this.f144a = d2;
        this.b = d3;
        this.c = d4;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
    }

    public final double a() {
        return this.f144a;
    }

    public final void a(double d2) {
        this.f144a = d2;
    }

    public final double b() {
        return this.b;
    }

    public final void b(double d2) {
        this.b = d2;
    }

    public final double c() {
        return this.c;
    }

    public final void c(double d2) {
        this.c = d2;
    }

    public final String toString() {
        return "Location: " + ("{\"latitude\":\"" + this.f144a + "\", \"" + "longitude" + "\":\"" + this.b + "\", \"" + "accuracy" + "\":\"" + this.c + "\", \"" + "country" + "\":\"" + this.d + "\", \"" + "country_code" + "\":\"" + this.e + "\", \"" + "region" + "\":\"" + this.f + "\", \"" + "city" + "\":\"" + this.g + "\", \"" + "street" + "\":\"" + this.h + "\"}");
    }
}
