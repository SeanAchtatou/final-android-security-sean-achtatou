package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private double f126a;
    private double b;

    public b(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f126a = cVar.e();
        this.b = cVar.e();
    }

    public final String b(Context context) {
        return context.getString(R.string.label_event_location_type_gps);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.content.Context.getString(int, java.lang.Object[]):java.lang.String in method: com.agilebinary.mobilemonitor.client.a.b.b.c(android.content.Context):java.lang.String, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.content.Context.getString(int, java.lang.Object[]):java.lang.String
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final java.lang.String c(android.content.Context r1) {
        /*
            r6 = this;
            boolean r0 = r6.d()
            if (r0 == 0) goto L_0x0009
            java.lang.String r0 = ""
        L_0x0008:
            return r0
        L_0x0009:
            r0 = 2131099912(0x7f060108, float:1.781219E38)
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            com.agilebinary.mobilemonitor.client.android.c.c r3 = com.agilebinary.mobilemonitor.client.android.c.c.a()
            long r4 = r6.c()
            java.lang.String r3 = r3.d(r4)
            r1[r2] = r3
            java.lang.String r0 = r7.getString(r0, r1)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.a.b.b.c(android.content.Context):java.lang.String");
    }

    public final byte k() {
        return 5;
    }
}
