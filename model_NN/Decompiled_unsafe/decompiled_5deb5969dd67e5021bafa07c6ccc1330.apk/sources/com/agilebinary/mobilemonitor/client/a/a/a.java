package com.agilebinary.mobilemonitor.client.a.a;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class a extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private volatile byte[] f123a;
    private int b;
    private int c;
    private int d = -1;
    private int e;
    private boolean f = false;

    public a(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        this.f123a = bArr;
    }

    private int a() {
        int i;
        if (this.d == -1 || this.e - this.d >= this.c) {
            i = this.in.read(this.f123a);
            if (i > 0) {
                this.d = -1;
                this.e = 0;
                this.b = i == -1 ? 0 : i;
            }
        } else {
            if (this.d == 0 && this.c > this.f123a.length) {
                int length = this.f123a.length * 2;
                if (length > this.c) {
                    length = this.c;
                }
                byte[] bArr = new byte[length];
                System.arraycopy(this.f123a, 0, bArr, 0, this.f123a.length);
                this.f123a = bArr;
            } else if (this.d > 0) {
                System.arraycopy(this.f123a, this.d, this.f123a, 0, this.f123a.length - this.d);
            }
            this.e -= this.d;
            this.d = 0;
            this.b = 0;
            i = this.in.read(this.f123a, this.e, this.f123a.length - this.e);
            this.b = i <= 0 ? this.e : this.e + i;
        }
        return i;
    }

    public final synchronized int available() {
        if (this.f123a == null) {
            throw new IOException("Stream is closed");
        }
        return (this.b - this.e) + this.in.available();
    }

    public final synchronized void close() {
        if (this.in != null) {
            super.close();
            this.in = null;
        }
        this.f123a = null;
        this.f = true;
    }

    public final synchronized void mark(int i) {
        this.c = i;
        this.d = this.e;
    }

    public final boolean markSupported() {
        return true;
    }

    public final synchronized int read() {
        byte b2;
        if (this.in == null) {
            throw new IOException("Stream is closed");
        } else if (this.e >= this.b && a() == -1) {
            b2 = -1;
        } else if (this.b - this.e > 0) {
            byte[] bArr = this.f123a;
            int i = this.e;
            this.e = i + 1;
            b2 = bArr[i] & 255;
        } else {
            b2 = -1;
        }
        return b2;
    }

    public final synchronized int read(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.f) {
            throw new IOException("Stream is closed");
        } else if (bArr == null) {
            throw new NullPointerException("K0047");
        } else if ((i | i2) < 0 || i > bArr.length - i2) {
            throw new IndexOutOfBoundsException("K002f");
        } else if (i2 == 0) {
            i6 = 0;
        } else if (this.f123a == null) {
            throw new IOException("K0059");
        } else {
            if (this.e < this.b) {
                i6 = this.b - this.e >= i2 ? i2 : this.b - this.e;
                System.arraycopy(this.f123a, this.e, bArr, i, i6);
                this.e += i6;
                if (!(i6 == i2 || this.in.available() == 0)) {
                    i3 = i + i6;
                    i4 = i2 - i6;
                }
            } else {
                i4 = i2;
                i3 = i;
            }
            while (true) {
                if (this.d == -1 && i4 >= this.f123a.length) {
                    i5 = this.in.read(bArr, i3, i4);
                    if (i5 == -1) {
                        i6 = i4 == i2 ? -1 : i2 - i4;
                    }
                } else if (a() == -1) {
                    i6 = i4 == i2 ? -1 : i2 - i4;
                } else {
                    i5 = this.b - this.e >= i4 ? i4 : this.b - this.e;
                    System.arraycopy(this.f123a, this.e, bArr, i3, i5);
                    this.e += i5;
                }
                i4 -= i5;
                if (i4 == 0) {
                    i6 = i2;
                    break;
                } else if (this.in.available() == 0) {
                    i6 = i2 - i4;
                    break;
                } else {
                    i3 += i5;
                }
            }
        }
        return i6;
    }

    public final synchronized void reset() {
        if (this.f) {
            throw new IOException("Stream is closed");
        } else if (-1 == this.d) {
            throw new IOException("Mark has been invalidated.");
        } else {
            this.e = this.d;
        }
    }

    public final synchronized long skip(long j) {
        long j2;
        if (this.in == null) {
            throw new IOException("K0059");
        } else if (j < 1) {
            j2 = 0;
        } else if (((long) (this.b - this.e)) >= j) {
            this.e = (int) (((long) this.e) + j);
            j2 = j;
        } else {
            j2 = (long) (this.b - this.e);
            this.e = this.b;
            if (this.d != -1) {
                if (j > ((long) this.c)) {
                    this.d = -1;
                } else if (a() != -1) {
                    if (((long) (this.b - this.e)) >= j - j2) {
                        this.e = (int) ((j - j2) + ((long) this.e));
                        j2 = j;
                    } else {
                        j2 += (long) (this.b - this.e);
                        this.e = this.b;
                    }
                }
            }
            j2 += this.in.skip(j - j2);
        }
        return j2;
    }
}
