package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.h.b.j;
import com.agilebinary.a.a.a.h.d.e;
import com.agilebinary.a.a.a.h.h;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public final class b implements j {

    /* renamed from: a  reason: collision with root package name */
    private static final String f150a = com.agilebinary.mobilemonitor.client.android.c.b.a();
    private static com.agilebinary.a.a.a.h.d.b b = new e();
    private static final b c = new b();
    private final SSLContext d;
    private final SSLSocketFactory e;
    private final com.agilebinary.a.a.a.h.b.b f;
    private volatile com.agilebinary.a.a.a.h.d.b g;

    private b() {
        this.g = b;
        this.d = null;
        this.e = HttpsURLConnection.getDefaultSSLSocketFactory();
        this.f = null;
    }

    public b(byte b2) {
        this.g = b;
        this.d = SSLContext.getInstance("TLS");
        this.d.init(null, new TrustManager[]{new r(this)}, null);
        this.e = this.d.getSocketFactory();
        this.f = null;
    }

    public final Socket a() {
        return (SSLSocket) this.e.createSocket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.a.e.e eVar) {
        if (str == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Parameters may not be null.");
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : a());
            if (inetAddress != null || i2 > 0) {
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2 < 0 ? 0 : i2));
            }
            int c2 = c.c(eVar);
            int a2 = c.a(eVar);
            InetSocketAddress inetSocketAddress = this.f != null ? new InetSocketAddress(this.f.a(), i) : new InetSocketAddress(str, i);
            try {
                sSLSocket.connect(inetSocketAddress, c2);
                sSLSocket.setSoTimeout(a2);
                try {
                    this.g.a(str, sSLSocket);
                    return sSLSocket;
                } catch (IOException e2) {
                    try {
                        sSLSocket.close();
                    } catch (Exception e3) {
                    }
                    throw e2;
                }
            } catch (SocketTimeoutException e4) {
                throw new h("Connect to " + inetSocketAddress + " timed out");
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory.");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }

    public final Socket a_(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.e.createSocket(socket, str, i, z);
        this.g.a(str, sSLSocket);
        return sSLSocket;
    }
}
