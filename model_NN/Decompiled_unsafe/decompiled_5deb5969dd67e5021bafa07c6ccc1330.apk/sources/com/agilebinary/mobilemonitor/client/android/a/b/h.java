package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a.a;
import com.agilebinary.mobilemonitor.client.android.a.a.c;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.p;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class h extends g implements p {
    private static final String b = b.a();

    public final List a(d dVar, List list, g gVar) {
        s sVar = null;
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(new JSONObject(((c) it.next()).a()));
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("gsm_cell_list", jSONArray);
            byte[] bytes = jSONObject.toString().getBytes("UTF-8");
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceLocationRetrieveGSMCell?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s").toString(), "1", dVar.b(), dVar.c());
            l lVar = this.f151a;
            t.a();
            s a2 = lVar.a(format, bytes, gVar);
            try {
                if (a2.a()) {
                    JSONArray jSONArray2 = new JSONObject(b(a2)).getJSONArray("geo_code_gsm_cell_list");
                    ArrayList arrayList = new ArrayList();
                    if (jSONArray2.length() > 0) {
                        for (int i = 0; i < jSONArray2.length(); i++) {
                            a aVar = new a();
                            aVar.a(jSONArray2.getJSONObject(i).getLong("create_date"));
                            aVar.a(jSONArray2.getJSONObject(i).getInt("geocoder_source_id"));
                            aVar.b(jSONArray2.getJSONObject(i).getInt("mobile_country_code"));
                            aVar.c(jSONArray2.getJSONObject(i).getInt("mobile_network_code"));
                            aVar.e(jSONArray2.getJSONObject(i).getInt("location_area_code"));
                            aVar.d(jSONArray2.getJSONObject(i).getInt("cell_id"));
                            com.agilebinary.mobilemonitor.client.android.a.a.b bVar = new com.agilebinary.mobilemonitor.client.android.a.a.b();
                            bVar.a(jSONArray2.getJSONObject(i).getDouble("latitude"));
                            bVar.b(jSONArray2.getJSONObject(i).getDouble("longitude"));
                            bVar.c(jSONArray2.getJSONObject(i).getDouble("accuracy"));
                            aVar.a(bVar);
                            arrayList.add(aVar);
                        }
                    }
                    a(a2);
                    return arrayList;
                }
                throw new q(a2.b());
            } catch (Exception e) {
                Exception exc = e;
                sVar = a2;
                e = exc;
                try {
                    throw new q(e);
                } catch (Throwable th) {
                    th = th;
                    a(sVar);
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                sVar = a2;
                th = th3;
                a(sVar);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
        }
    }
}
