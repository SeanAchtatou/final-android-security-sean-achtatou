package com.agilebinary.mobilemonitor.client.android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class a extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ i f142a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(i iVar, Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 6);
        this.f142a = iVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        String format = String.format("CREATE TABLE %1$s (%2$s INTEGER, %3$s INTEGER, %4$s INTEGER, %5$s INTEGER, %6$s DOUBLE, %7$s DOUBLE,%8$s DOUBLE,%9$s INTEGER, %10$s INTEGER, UNIQUE (%10$s, %2$s, %3$s, %4$s, %5$s) ON CONFLICT REPLACE );", c.f178a, c.c, c.d, c.e, c.f, c.g, c.h, c.i, c.b, c.j);
        String.format("Executing SQL: %1$s", format);
        sQLiteDatabase.execSQL(format);
        String format2 = String.format("CREATE UNIQUE INDEX IDX_%1$s  ON %1$s (%10$s, %2$s, %3$s, %4$s, %5$s);", c.f178a, c.c, c.d, c.e, c.f, c.g, c.h, c.i, c.b, c.j);
        String.format("Executing SQL: %1$s", format2);
        sQLiteDatabase.execSQL(format2);
        String format3 = String.format("CREATE TABLE %1$s (%2$s INTEGER, %3$s INTEGER, %4$s INTEGER, %5$s DOUBLE, %6$s DOUBLE,%7$s DOUBLE,%8$s INTEGER, %9$s INTEGER, UNIQUE (%9$s, %2$s, %3$s, %4$s) ON CONFLICT REPLACE );", b.f165a, b.c, b.d, b.e, b.f, b.g, b.h, b.b, b.i);
        String.format("Executing SQL: %1$s", format3);
        sQLiteDatabase.execSQL(format3);
        String format4 = String.format("CREATE UNIQUE INDEX IDX_%1$s  ON %1$s (%9$s, %2$s, %3$s, %4$s);", b.f165a, b.c, b.d, b.e, b.f, b.g, b.h, b.b, b.i);
        String.format("Executing SQL: %1$s", format4);
        sQLiteDatabase.execSQL(format4);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String.format("Upgrading database from version %1$s to %2$s, destroying all existing data", Integer.valueOf(i), Integer.valueOf(i2));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", c.f178a));
        sQLiteDatabase.execSQL(String.format("DROP INDEX IF EXISTS IDX_%1$s", c.f178a));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", b.f165a));
        sQLiteDatabase.execSQL(String.format("DROP INDEX IF EXISTS IDX_%1$s", b.f165a));
        onCreate(sQLiteDatabase);
    }
}
