package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.view.View;
import android.widget.AdapterView;
import java.util.Locale;

final class bh implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LoginActivity f247a;

    bh(LoginActivity loginActivity) {
        this.f247a = loginActivity;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        Locale locale = LoginActivity.j[i].f241a;
        if (!Locale.getDefault().getLanguage().equals(locale.getLanguage())) {
            Configuration configuration = this.f247a.getBaseContext().getResources().getConfiguration();
            configuration.locale = locale;
            Locale.setDefault(locale);
            this.f247a.getBaseContext().getResources().updateConfiguration(configuration, this.f247a.getBaseContext().getResources().getDisplayMetrics());
            Intent intent = new Intent(this.f247a, LoginActivity.class);
            intent.addFlags(67108864);
            this.f247a.finish();
            this.f247a.startActivity(intent);
        }
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
