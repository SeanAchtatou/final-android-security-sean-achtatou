package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class ap implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ResetPasswordActivity f232a;

    ap(ResetPasswordActivity resetPasswordActivity) {
        this.f232a = resetPasswordActivity;
    }

    public final void onClick(View view) {
        this.f232a.setResult(0);
        this.f232a.finish();
    }
}
