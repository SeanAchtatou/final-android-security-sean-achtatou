package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f255a;
    private /* synthetic */ MapActivity_OSM b;

    bq(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f255a = dialog;
    }

    public final void onClick(View view) {
        this.b.f.a(!this.b.f.a());
        this.b.b.invalidate();
        this.b.l.b("MAP_LAYER_DIRECTIONS__BOOL", this.b.f.a());
        this.f255a.dismiss();
    }
}
