package org.osmdroid.util;

final class a {

    /* renamed from: a  reason: collision with root package name */
    Integer f366a;
    Integer b;
    Integer c;
    Integer d;
    Integer e;
    Integer f;
    Long g;
    private /* synthetic */ d h;

    /* synthetic */ a(d dVar) {
        this(dVar, (byte) 0);
    }

    private a(d dVar, byte b2) {
        this.h = dVar;
    }

    public final String toString() {
        return String.format("GEMF Range: source=%d, zoom=%d, x=%d-%d, y=%d-%d, offset=0x%08X", this.f, this.f366a, this.b, this.c, this.d, this.e, this.g);
    }
}
