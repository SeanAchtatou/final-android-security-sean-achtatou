package org.osmdroid.b.a;

public enum b {
    CUSTOM,
    CENTER,
    BOTTOM_CENTER,
    TOP_CENTER,
    RIGHT_CENTER,
    LEFT_CENTER,
    UPPER_RIGHT_CORNER,
    LOWER_RIGHT_CORNER,
    UPPER_LEFT_CORNER,
    LOWER_LEFT_CORNER
}
