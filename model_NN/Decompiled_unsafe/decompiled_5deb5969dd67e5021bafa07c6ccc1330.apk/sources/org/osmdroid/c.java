package org.osmdroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import org.a.a;

public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final org.a.c f359a = a.a(c.class);
    private DisplayMetrics b;

    public c(Context context) {
        if (context != null) {
            this.b = context.getResources().getDisplayMetrics();
        }
    }

    private BitmapFactory.Options a() {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField("DENSITY_DEFAULT");
            Field declaredField2 = BitmapFactory.Options.class.getDeclaredField("inDensity");
            Field declaredField3 = BitmapFactory.Options.class.getDeclaredField("inTargetDensity");
            Field declaredField4 = DisplayMetrics.class.getDeclaredField("densityDpi");
            BitmapFactory.Options options = new BitmapFactory.Options();
            declaredField2.setInt(options, declaredField.getInt(null));
            declaredField3.setInt(options, declaredField4.getInt(this.b));
            return options;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return null;
        }
    }

    private Bitmap b(e eVar) {
        InputStream inputStream;
        try {
            String str = eVar.name() + ".png";
            inputStream = getClass().getResourceAsStream(str);
            if (inputStream == null) {
                try {
                    throw new IllegalArgumentException("Resource not found: " + str);
                } catch (Throwable th) {
                    th = th;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                        }
                    }
                    throw th;
                }
            } else {
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, this.b != null ? a() : null);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                    }
                }
                return decodeStream;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
        }
    }

    public final Drawable a(e eVar) {
        return new BitmapDrawable(b(eVar));
    }

    public final String a(a aVar, Object... objArr) {
        String str;
        switch (d.f362a[aVar.ordinal()]) {
            case 1:
                str = "Osmarender";
                break;
            case 2:
                str = "Mapnik";
                break;
            case 3:
                str = "Cycle Map";
                break;
            case 4:
                str = "Public transport";
                break;
            case 5:
                str = "OSM base layer";
                break;
            case 6:
                str = "Topographic";
                break;
            case 7:
                str = "Hills";
                break;
            case 8:
                str = "CloudMade (Standard tiles)";
                break;
            case 9:
                str = "CloudMade (small tiles)";
                break;
            case 10:
                str = "Mapquest";
                break;
            case 11:
                str = "OpenFietsKaart overlay";
                break;
            case 12:
                str = "Netherlands base overlay";
                break;
            case 13:
                str = "Netherlands roads overlay";
                break;
            case 14:
                str = "Unknown";
                break;
            case 15:
                str = "%s m";
                break;
            case 16:
                str = "%s km";
                break;
            case 17:
                str = "%s mi";
                break;
            case 18:
                str = "%s nm";
                break;
            case 19:
                str = "%s ft";
                break;
            default:
                throw new IllegalArgumentException();
        }
        return String.format(str, objArr);
    }
}
