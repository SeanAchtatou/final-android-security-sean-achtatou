package org.osmdroid.a.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class e implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final c f327a = a.a(e.class);
    private final SQLiteDatabase b;

    private e(SQLiteDatabase sQLiteDatabase) {
        this.b = sQLiteDatabase;
    }

    public static e a(File file) {
        return new e(SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null));
    }

    public final InputStream a(f fVar, org.osmdroid.a.f fVar2) {
        ByteArrayInputStream byteArrayInputStream;
        try {
            long a2 = (long) fVar2.a();
            Cursor query = this.b.query("tiles", new String[]{"tile"}, "key = " + (((long) fVar2.c()) + ((((long) fVar2.b()) + (a2 << ((int) a2))) << ((int) a2))) + " and provider = '" + fVar.c() + "'", null, null, null, null);
            if (query.getCount() != 0) {
                query.moveToFirst();
                byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            } else {
                byteArrayInputStream = null;
            }
            query.close();
            if (byteArrayInputStream != null) {
                return byteArrayInputStream;
            }
        } catch (Throwable th) {
            f327a.b("Error getting db stream: " + fVar2, th);
        }
        return null;
    }

    public String toString() {
        return "DatabaseFileArchive [mDatabase=" + this.b.getPath() + "]";
    }
}
