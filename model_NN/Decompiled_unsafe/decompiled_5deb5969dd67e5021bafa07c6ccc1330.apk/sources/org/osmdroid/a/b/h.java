package org.osmdroid.a.b;

import android.content.IntentFilter;
import android.os.Environment;
import org.a.a;
import org.a.c;

public abstract class h extends v {
    private static final c c = a.a(h.class);
    private boolean e = true;
    private final org.osmdroid.a.a f;
    private n g;

    public h(org.osmdroid.a.a aVar) {
        super(8);
        k();
        this.f = aVar;
        this.g = new n(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        aVar.a(this.g, intentFilter);
    }

    /* access modifiers changed from: private */
    public void k() {
        String externalStorageState = Environment.getExternalStorageState();
        c.b("sdcard state: " + externalStorageState);
        this.e = "mounted".equals(externalStorageState);
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.e;
    }

    public final void b() {
        if (this.g != null) {
            this.f.a(this.g);
            this.g = null;
        }
        super.b();
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void d() {
    }
}
