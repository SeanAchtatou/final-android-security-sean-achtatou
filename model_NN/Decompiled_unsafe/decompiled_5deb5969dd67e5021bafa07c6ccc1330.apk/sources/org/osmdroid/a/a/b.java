package org.osmdroid.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import org.a.a;
import org.a.c;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final d f322a = d;
    private static final c b = a.a(b.class);
    private static d c = new c("Osmarender", org.osmdroid.a.osmarender, 0, 17, ".png", "http://tah.openstreetmap.org/Tiles/tile/");
    private static d d = new c("Mapnik", org.osmdroid.a.mapnik, 0, 18, ".png", "http://tile.openstreetmap.org/");
    private static d e = new c("CycleMap", org.osmdroid.a.cyclemap, 0, 17, ".png", "http://a.andy.sandbox.cloudmade.com/tiles/cycle/", "http://b.andy.sandbox.cloudmade.com/tiles/cycle/", "http://c.andy.sandbox.cloudmade.com/tiles/cycle/");
    private static d f = new c("OSMPublicTransport", org.osmdroid.a.public_transport, 0, 17, ".png", "http://tile.xn--pnvkarte-m4a.de/tilegen/");
    private static d g = new c("Base", org.osmdroid.a.base, 4, 17, ".png", "http://topo.openstreetmap.de/base/");
    private static d h = new c("Topo", org.osmdroid.a.topo, 4, 17, ".png", "http://topo.openstreetmap.de/topo/");
    private static d i = new c("Hills", org.osmdroid.a.hills, 8, 17, ".png", "http://topo.geofabrik.de/hills/");
    private static d j = new a("CloudMadeStandardTiles", org.osmdroid.a.cloudmade_standard, 18, 256, ".png", "http://a.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://b.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://c.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s");
    private static d k = new a("CloudMadeSmallTiles", org.osmdroid.a.cloudmade_small, 21, 64, ".png", "http://a.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://b.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://c.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s");
    private static d l = new c("MapquestOSM", org.osmdroid.a.mapquest_osm, 0, 18, ".png", "http://otile1.mqcdn.com/tiles/1.0.0/osm/", "http://otile2.mqcdn.com/tiles/1.0.0/osm/", "http://otile3.mqcdn.com/tiles/1.0.0/osm/", "http://otile4.mqcdn.com/tiles/1.0.0/osm/");
    private static d m = new c("Fiets", org.osmdroid.a.fiets_nl, 3, 16, ".png", "http://overlay.openstreetmap.nl/openfietskaart-overlay/");
    private static d n = new c("BaseNL", org.osmdroid.a.base_nl, 0, 18, ".png", "http://overlay.openstreetmap.nl/basemap/");
    private static d o = new c("RoadsNL", org.osmdroid.a.roads_nl, 0, 18, ".png", "http://overlay.openstreetmap.nl/roads/");
    private static ArrayList p;

    static {
        ArrayList arrayList = new ArrayList();
        p = arrayList;
        arrayList.add(c);
        p.add(d);
        p.add(e);
        p.add(f);
        p.add(g);
        p.add(h);
        p.add(i);
        p.add(j);
        p.add(k);
        p.add(l);
    }

    public static f a(String str) {
        Iterator it = p.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.c().equals(str)) {
                return fVar;
            }
        }
        throw new IllegalArgumentException("No such tile source: " + str);
    }
}
