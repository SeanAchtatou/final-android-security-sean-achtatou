package org.meteoroid.core;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

public class MeteoroidActivity extends Activity {
    public static final String LOG_TAG = "Meteoroid";
    public static final int MSG_ACTIVITY_CONF_CHANGE = 40965;
    public static final int MSG_ACTIVITY_PAUSE = 40960;
    public static final int MSG_ACTIVITY_RESTART = 40963;
    public static final int MSG_ACTIVITY_RESUME = 40961;
    public static final int MSG_ACTIVITY_START = 40962;
    public static final int MSG_ACTIVITY_STOP = 40964;
    private static boolean hu = true;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        cf.b(cf.a((int) cl.MSG_SYSTEM_ACTIVITY_RESULT, new int[]{i, i2}));
        super.onActivityResult(i, i2, intent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        cf.b(cf.a((int) MSG_ACTIVITY_CONF_CHANGE, this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().setBackgroundDrawable(null);
        getWindow().requestFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(as.GAME_B_PRESSED, as.GAME_B_PRESSED);
        cf.a((int) MSG_ACTIVITY_PAUSE, "MSG_ACTIVITY_PAUSE");
        cf.a((int) MSG_ACTIVITY_RESUME, "MSG_ACTIVITY_RESUME");
        cf.a((int) MSG_ACTIVITY_START, "MSG_ACTIVITY_START");
        cf.a((int) MSG_ACTIVITY_RESTART, "MSG_ACTIVITY_RESTART");
        cf.a((int) MSG_ACTIVITY_STOP, "MSG_ACTIVITY_STOP");
        cf.a((int) MSG_ACTIVITY_CONF_CHANGE, "MSG_ACTIVITY_CONF_CHANGE");
        super.onCreate(bundle);
        cl.b(this);
        cf.b(cf.a((int) cl.MSG_SYSTEM_INIT_COMPLETE, bundle));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        cl.bF();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!cr.ax()) {
            cl.bG();
        }
        return true;
    }

    public void onLowMemory() {
        cl.bI();
        super.onLowMemory();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ch.a(menuItem);
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        cf.b(cf.a((int) MSG_ACTIVITY_PAUSE, this));
        cl.pause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        ch.onPrepareOptionsMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        cf.b(cf.a((int) MSG_ACTIVITY_RESTART, this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        cf.b(cf.a((int) MSG_ACTIVITY_RESUME, this));
        if (hu) {
            hu = false;
        } else {
            cl.resume();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        cf.b(cf.a((int) MSG_ACTIVITY_START, this));
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        cf.b(cf.a((int) MSG_ACTIVITY_STOP, this));
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        return bv.onTrackballEvent(motionEvent);
    }
}
