package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public abstract class AbstractRoundWidget extends AbstractButton {
    int centerX;
    int centerY;
    int jp;
    int jq;
    VirtualKey jr;

    public final float a(float f, float f2) {
        float f3 = f - ((float) this.centerX);
        float f4 = f2 - ((float) this.centerY);
        double degrees = Math.toDegrees(Math.atan((double) (f4 / f3)));
        return (float) ((f3 < 0.0f || f4 >= 0.0f) ? (f3 >= 0.0f || f4 > 0.0f) ? (f3 > 0.0f || f4 <= 0.0f) ? (f3 <= 0.0f || f4 < 0.0f) ? degrees : 360.0d - Math.abs(degrees) : Math.abs(degrees) + 180.0d : 180.0d - degrees : Math.abs(degrees));
    }

    public void a(AttributeSet attributeSet, String str) {
        this.centerX = attributeSet.getAttributeIntValue(str, "x", cl.bJ() / 2);
        this.centerY = attributeSet.getAttributeIntValue(str, "y", cl.bK() / 2);
        this.jp = attributeSet.getAttributeIntValue(str, "max", 60);
        this.jq = attributeSet.getAttributeIntValue(str, "min", 5);
    }

    public boolean h(int i, int i2, int i3, int i4) {
        return false;
    }

    public final void release() {
        this.id = -1;
        if (this.jr != null && this.jr.state == 0) {
            this.jr.state = 1;
            VirtualKey.b(this.jr);
        }
        reset();
    }

    public abstract void reset();
}
