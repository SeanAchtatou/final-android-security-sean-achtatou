package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;

public abstract class ScreenWidget implements cz {
    public final Rect jC = new Rect();
    public final Paint jD = new Paint();
    public Rect jE;
    public float jF;
    public float jG;
    public boolean jH;

    public abstract void a(int i, float f, float f2);

    public final boolean bU() {
        return true;
    }

    public abstract Bitmap bW();

    public final void ck() {
        if (bW() != null) {
            this.jF = ((float) this.jC.width()) / ((float) bW().getWidth());
            this.jG = ((float) this.jC.height()) / ((float) bW().getHeight());
            if (this.jF == 1.0f && this.jG == 1.0f) {
                this.jD.setFilterBitmap(false);
            } else {
                this.jD.setFilterBitmap(true);
            }
        }
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        if (!this.jC.contains(i2, i3) || !this.jH) {
            return false;
        }
        a(i, ((float) (i2 - this.jC.left)) / this.jF, ((float) (i3 - this.jC.top)) / this.jG);
        return false;
    }

    public final boolean isTouchable() {
        return this.jH;
    }
}
