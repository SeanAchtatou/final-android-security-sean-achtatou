package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;

public class SteeringWheel extends AbstractRoundWidget {
    private static final int ANTICLOCKWISE = 2;
    private static final int CLOCKWISE = 1;
    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UNKNOWN = 0;
    private static final VirtualKey[] jO = new VirtualKey[2];
    private final Matrix fk = new Matrix();
    private float jP = -1.0f;
    private float jQ = 0.0f;
    public int jR = 135;
    public int jS = 5;
    private int orientation = 0;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.jR = attributeSet.getAttributeIntValue(str, "angleMax", 135);
        this.jS = attributeSet.getAttributeIntValue(str, "angleMin", 5);
        this.jn = dt.L(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(cy cyVar) {
        super.a(cyVar);
        jO[0] = new VirtualKey();
        jO[0].jT = "LEFT";
        jO[1] = new VirtualKey();
        jO[1].jT = "RIGHT";
        reset();
    }

    public final boolean h(int i, int i2, int i3, int i4) {
        VirtualKey virtualKey;
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.jp) || sqrt < ((double) this.jq)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                if (this.jP == -1.0f) {
                    this.jP = a((float) i2, (float) i3);
                }
                this.id = i4;
                break;
            case 1:
                if (this.id == i4) {
                    release();
                    break;
                }
                break;
            case 2:
                if (this.id == i4) {
                    this.state = 0;
                    float a = a((float) i2, (float) i3);
                    float f = this.jP;
                    float f2 = a - f;
                    if (this.orientation == 0) {
                        if (Math.abs(f2) > 180.0f) {
                            if (f2 > 0.0f) {
                                this.orientation = 1;
                            } else if (f2 < 0.0f) {
                                this.orientation = 2;
                            }
                        } else if (a > f) {
                            this.orientation = 2;
                        } else if (a < f) {
                            this.orientation = 1;
                        }
                    }
                    this.jQ = (this.orientation != 1 || f2 <= 0.0f) ? (this.orientation != 2 || f2 >= 0.0f) ? f2 : 360.0f + f2 : f2 - 360.0f;
                    if (this.jQ > ((float) this.jR)) {
                        this.jQ = (float) this.jR;
                    } else if (this.jQ < ((float) (this.jR * -1))) {
                        this.jQ = (float) (this.jR * -1);
                    }
                    if (this.jQ >= ((float) this.jS) && this.jQ <= ((float) this.jR)) {
                        virtualKey = jO[0];
                    } else if (this.jQ > ((float) (this.jS * -1)) || this.jQ < ((float) (this.jR * -1))) {
                        if (this.jQ < ((float) this.jS) && this.jQ > ((float) (this.jS * -1))) {
                            this.orientation = 0;
                            break;
                        } else {
                            virtualKey = null;
                        }
                    } else {
                        virtualKey = jO[1];
                    }
                    if (this.jr != virtualKey) {
                        if (this.jr != null && this.jr.state == 0) {
                            this.jr.state = 1;
                            VirtualKey.b(this.jr);
                        }
                        this.jr = virtualKey;
                    }
                    if (this.jr != null) {
                        this.jr.state = 0;
                        VirtualKey.b(this.jr);
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public final void onDraw(Canvas canvas) {
        if (this.state == 1) {
            if (this.jQ > 0.0f) {
                this.jQ -= 15.0f;
                this.jQ = this.jQ < 0.0f ? 0.0f : this.jQ;
            } else if (this.jQ < 0.0f) {
                this.jQ += 15.0f;
                this.jQ = this.jQ > 0.0f ? 0.0f : this.jQ;
            }
        }
        if (this.jn != null) {
            canvas.save();
            if (this.jQ != 0.0f) {
                this.fk.setRotate(this.jQ * -1.0f, (float) (this.jn[this.state].getWidth() >> 1), (float) (this.jn[this.state].getHeight() >> 1));
            }
            canvas.translate((float) (this.centerX - (this.jn[this.state].getWidth() >> 1)), (float) (this.centerY - (this.jn[this.state].getHeight() >> 1)));
            canvas.drawBitmap(this.jn[this.state], this.fk, null);
            canvas.restore();
            this.fk.reset();
        }
    }

    public final void reset() {
        this.state = 1;
        this.jP = -1.0f;
        this.orientation = 0;
    }
}
