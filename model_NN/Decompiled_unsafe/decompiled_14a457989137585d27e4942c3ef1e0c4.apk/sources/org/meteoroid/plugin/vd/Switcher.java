package org.meteoroid.plugin.vd;

public abstract class Switcher extends AbstractButton {
    public abstract void cf();

    public final boolean h(int i, int i2, int i3, int i4) {
        if (!this.jl.contains(i2, i3) || i != 0) {
            return false;
        }
        cf();
        return false;
    }
}
