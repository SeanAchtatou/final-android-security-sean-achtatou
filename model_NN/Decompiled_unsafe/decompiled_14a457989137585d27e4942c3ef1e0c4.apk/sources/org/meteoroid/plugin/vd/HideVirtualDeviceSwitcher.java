package org.meteoroid.plugin.vd;

import java.util.Iterator;

public class HideVirtualDeviceSwitcher extends BooleanSwitcher {
    public final void cg() {
        Iterator it = ((DefaultVirtualDevice) cv.hK).ci().iterator();
        while (it.hasNext()) {
            cz czVar = (cz) it.next();
            if (czVar instanceof AbstractButton) {
                ((AbstractButton) czVar).jo = false;
            }
        }
    }

    public final void ch() {
        Iterator it = ((DefaultVirtualDevice) cv.hK).ci().iterator();
        while (it.hasNext()) {
            cz czVar = (cz) it.next();
            if (czVar instanceof AbstractButton) {
                ((AbstractButton) czVar).jo = true;
            }
        }
    }
}
