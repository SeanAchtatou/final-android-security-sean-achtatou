package org.meteoroid.plugin.vd;

public class CallOptionMenu extends SimpleButton {
    private boolean fu;

    public final void onClick() {
        if (this.fu) {
            cl.getActivity().closeOptionsMenu();
        } else {
            cl.getActivity().openOptionsMenu();
        }
        this.fu = !this.fu;
    }
}
