package org.meteoroid.plugin.vd;

public final class MuteSwitcher extends BooleanSwitcher {
    public final void cg() {
        cc.d(true);
    }

    public final void ch() {
        cc.d(false);
    }
}
