package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

public class Joystick extends AbstractRoundWidget {
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int EIGHT_DIR_CONTROL_NUM = 10;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int FOUR_DIR_CONTROL_NUM = 6;
    private static final VirtualKey[] js = new VirtualKey[5];
    private static final VirtualKey[] jx = new VirtualKey[9];
    Bitmap[] jA;
    Bitmap[] jB;
    private int jy;
    private int jz;
    private int mode;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.jA = dt.L(attributeSet.getAttributeValue(str, "stick"));
        this.jB = dt.L(attributeSet.getAttributeValue(str, "base"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 4);
    }

    public final void a(cy cyVar) {
        super.a(cyVar);
        if (this.mode == 8) {
            jx[0] = new VirtualKey();
            jx[0].jT = "RIGHT";
            jx[1] = new VirtualKey();
            jx[1].jT = "NUM_3";
            jx[2] = new VirtualKey();
            jx[2].jT = "UP";
            jx[3] = new VirtualKey();
            jx[3].jT = "NUM_1";
            jx[4] = new VirtualKey();
            jx[4].jT = "LEFT";
            jx[5] = new VirtualKey();
            jx[5].jT = "NUM_7";
            jx[6] = new VirtualKey();
            jx[6].jT = "DOWN";
            jx[7] = new VirtualKey();
            jx[7].jT = "NUM_9";
            jx[8] = jx[0];
        } else if (this.mode == 10) {
            jx[0] = new VirtualKey();
            jx[0].jT = "NUM_6";
            jx[1] = new VirtualKey();
            jx[1].jT = "NUM_3";
            jx[2] = new VirtualKey();
            jx[2].jT = "NUM_2";
            jx[3] = new VirtualKey();
            jx[3].jT = "NUM_1";
            jx[4] = new VirtualKey();
            jx[4].jT = "NUM_4";
            jx[5] = new VirtualKey();
            jx[5].jT = "NUM_7";
            jx[6] = new VirtualKey();
            jx[6].jT = "NUM_8";
            jx[7] = new VirtualKey();
            jx[7].jT = "NUM_9";
            jx[8] = jx[0];
        } else if (this.mode == 6) {
            js[0] = new VirtualKey();
            js[0].jT = "NUM_6";
            js[1] = new VirtualKey();
            js[1].jT = "NUM_2";
            js[2] = new VirtualKey();
            js[2].jT = "NUM_4";
            js[3] = new VirtualKey();
            js[3].jT = "NUM_8";
            js[4] = js[0];
        } else {
            js[0] = new VirtualKey();
            js[0].jT = "RIGHT";
            js[1] = new VirtualKey();
            js[1].jT = "UP";
            js[2] = new VirtualKey();
            js[2].jT = "LEFT";
            js[3] = new VirtualKey();
            js[3].jT = "DOWN";
            js[4] = js[0];
        }
        reset();
    }

    public final boolean bU() {
        return (this.jB == null && this.jA == null) ? false : true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean h(int r9, int r10, int r11, int r12) {
        /*
            r8 = this;
            r4 = 4611686018427387904(0x4000000000000000, double:2.0)
            r7 = 1
            r6 = 0
            int r0 = r8.centerX
            int r0 = r10 - r0
            double r0 = (double) r0
            double r0 = java.lang.Math.pow(r0, r4)
            int r2 = r8.centerY
            int r2 = r11 - r2
            double r2 = (double) r2
            double r2 = java.lang.Math.pow(r2, r4)
            double r0 = r0 + r2
            double r0 = java.lang.Math.sqrt(r0)
            java.lang.Thread.yield()
            int r2 = r8.jp
            double r2 = (double) r2
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0092
            int r2 = r8.jq
            double r2 = (double) r2
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0092
            switch(r9) {
                case 0: goto L_0x0031;
                case 1: goto L_0x008a;
                case 2: goto L_0x0033;
                default: goto L_0x002f;
            }
        L_0x002f:
            r0 = r7
        L_0x0030:
            return r0
        L_0x0031:
            r8.id = r12
        L_0x0033:
            int r0 = r8.id
            if (r0 != r12) goto L_0x002f
            r8.state = r6
            r8.jy = r10
            r8.jz = r11
            float r0 = (float) r10
            float r1 = (float) r11
            float r0 = r8.a(r0, r1)
            int r1 = r8.mode
            r2 = 8
            if (r1 != r2) goto L_0x007e
            org.meteoroid.plugin.vd.VirtualKey[] r1 = org.meteoroid.plugin.vd.Joystick.jx
            double r2 = (double) r0
            r4 = 4627026404658118656(0x4036800000000000, double:22.5)
            double r2 = r2 + r4
            r4 = 4631530004285489152(0x4046800000000000, double:45.0)
            double r2 = r2 / r4
            int r0 = (int) r2
            r0 = r1[r0]
        L_0x005b:
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.jr
            if (r1 == r0) goto L_0x0074
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.jr
            if (r1 == 0) goto L_0x0072
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.jr
            int r1 = r1.state
            if (r1 != 0) goto L_0x0072
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.jr
            r1.state = r7
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.jr
            org.meteoroid.plugin.vd.VirtualKey.b(r1)
        L_0x0072:
            r8.jr = r0
        L_0x0074:
            org.meteoroid.plugin.vd.VirtualKey r0 = r8.jr
            r0.state = r6
            org.meteoroid.plugin.vd.VirtualKey r0 = r8.jr
            org.meteoroid.plugin.vd.VirtualKey.b(r0)
            goto L_0x002f
        L_0x007e:
            org.meteoroid.plugin.vd.VirtualKey[] r1 = org.meteoroid.plugin.vd.Joystick.js
            r2 = 1110704128(0x42340000, float:45.0)
            float r0 = r0 + r2
            r2 = 1119092736(0x42b40000, float:90.0)
            float r0 = r0 / r2
            int r0 = (int) r0
            r0 = r1[r0]
            goto L_0x005b
        L_0x008a:
            int r0 = r8.id
            if (r0 != r12) goto L_0x002f
            r8.release()
            goto L_0x002f
        L_0x0092:
            if (r9 != r7) goto L_0x009b
            int r0 = r8.id
            if (r0 != r12) goto L_0x009b
            r8.release()
        L_0x009b:
            r0 = r6
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.vd.Joystick.h(int, int, int, int):boolean");
    }

    public final void onDraw(Canvas canvas) {
        if (a(this.jB)) {
            canvas.drawBitmap(this.jB[this.state], (float) (this.centerX - (this.jB[this.state].getWidth() / 2)), (float) (this.centerY - (this.jB[this.state].getHeight() / 2)), (Paint) null);
        }
        if (a(this.jA)) {
            canvas.drawBitmap(this.jA[this.state], (float) (this.jy - (this.jA[this.state].getWidth() / 2)), (float) (this.jz - (this.jA[this.state].getHeight() / 2)), (Paint) null);
        }
    }

    public final void reset() {
        this.jy = this.centerX;
        this.jz = this.centerY;
        this.state = 1;
    }
}
