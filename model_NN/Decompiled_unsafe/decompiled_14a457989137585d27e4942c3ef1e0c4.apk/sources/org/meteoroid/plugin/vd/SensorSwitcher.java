package org.meteoroid.plugin.vd;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

public final class SensorSwitcher extends BooleanSwitcher implements SensorEventListener {
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int UP = 0;
    private static int jI = 2;
    private static int jJ = 3;
    private static final VirtualKey[] js = new VirtualKey[4];
    private VirtualKey jK;
    private final int[] jL = new int[3];
    private int jM = 0;
    private int jN;
    private int x;
    private int y;
    private int z;

    private void a(VirtualKey virtualKey) {
        if (virtualKey != this.jK) {
            cl();
        }
        if (virtualKey != null) {
            virtualKey.state = 0;
            VirtualKey.b(virtualKey);
            this.jK = virtualKey;
        }
    }

    private void cl() {
        if (this.jK != null && this.jK.state == 0) {
            this.jK.state = 1;
            VirtualKey.b(this.jK);
            this.jK = null;
        }
    }

    public final void cg() {
        bv.a((SensorEventListener) this);
    }

    public final void ch() {
        cl();
        bv.b((SensorEventListener) this);
        js[0] = new VirtualKey();
        js[0].jT = "UP";
        js[1] = new VirtualKey();
        js[1].jT = "DOWN";
        js[2] = new VirtualKey();
        js[2].jT = "LEFT";
        js[3] = new VirtualKey();
        js[3].jT = "RIGHT";
        this.jN = cl.bL();
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.jM = 0;
        this.jL[0] = 0;
        this.jL[1] = 0;
        this.jL[2] = 0;
        cl();
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (this.jN == 1) {
            this.x = -((int) sensorEvent.values[0]);
            this.y = (int) sensorEvent.values[1];
        } else if (this.jN == 0) {
            this.x = (int) sensorEvent.values[1];
            this.y = (int) sensorEvent.values[0];
        } else {
            Log.w("SensorSwitcher", "deviceOrientation:UNKNOWN");
            return;
        }
        this.z = (int) sensorEvent.values[2];
        if (this.jM <= 0) {
            this.jL[0] = this.x;
            this.jL[1] = this.y;
            this.jL[2] = this.z;
            this.jM++;
            return;
        }
        int i = this.x - this.jL[0];
        int i2 = this.y - this.jL[1];
        int i3 = this.z - this.jL[2];
        if (Math.abs(i) < jI && Math.abs(i2) < jJ && Math.abs(i3) < jJ) {
            cl();
        } else if (Math.abs(i) >= jI) {
            if (i < 0) {
                a(js[2]);
            } else {
                a(js[3]);
            }
        } else if (Math.abs(i2) >= jJ) {
            if (i2 > 0) {
                a(js[1]);
            } else {
                a(js[0]);
            }
            a(this.jK);
        } else if (Math.abs(i3) < jJ) {
        } else {
            if (i3 > 0) {
                a(js[0]);
            } else {
                a(js[1]);
            }
        }
    }
}
