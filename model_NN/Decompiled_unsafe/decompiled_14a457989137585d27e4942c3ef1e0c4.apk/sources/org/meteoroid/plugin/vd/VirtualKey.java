package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public class VirtualKey extends AbstractButton {
    public static final int MSG_VIRTUAL_KEY_EVENT = 7833601;
    public String jT;

    public static final synchronized void b(VirtualKey virtualKey) {
        synchronized (VirtualKey.class) {
            cf.b(cf.a((int) MSG_VIRTUAL_KEY_EVENT, virtualKey));
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.jT = attributeSet.getAttributeValue(str, "keyname");
    }

    public final String cm() {
        return this.jT;
    }

    public final boolean h(int i, int i2, int i3, int i4) {
        if (this.jl.contains(i2, i3)) {
            switch (i) {
                case 0:
                case 2:
                    this.id = i4;
                    this.state = 0;
                    b(this);
                    break;
                case 1:
                    this.id = -1;
                    this.state = 1;
                    b(this);
                    break;
            }
        } else if (this.state == 0 && this.id == i4) {
            this.id = -1;
            this.state = 1;
            b(this);
        }
        return false;
    }
}
