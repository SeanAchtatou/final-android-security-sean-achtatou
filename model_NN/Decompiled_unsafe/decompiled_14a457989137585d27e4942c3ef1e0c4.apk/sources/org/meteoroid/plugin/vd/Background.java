package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

public class Background implements cz {
    int color = -16777216;
    Rect ex;
    cy hK;
    private Paint it;
    Bitmap jt;

    public void a(AttributeSet attributeSet, String str) {
        this.ex = dt.J(attributeSet.getAttributeValue(str, "rect"));
        String attributeValue = attributeSet.getAttributeValue(str, "bitmap");
        if (attributeValue != null) {
            this.jt = dt.K(attributeValue);
        } else {
            this.color = Integer.valueOf(attributeSet.getAttributeValue(str, "color"), 16).intValue();
        }
    }

    public final void a(cy cyVar) {
        this.hK = cyVar;
        if (this.jt == null) {
            this.it = new Paint();
            this.it.setAntiAlias(true);
            this.it.setStyle(Paint.Style.FILL);
        }
    }

    public final boolean bU() {
        return true;
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        return false;
    }

    public final boolean isTouchable() {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (this.jt == null) {
            this.it.setColor(this.color);
            canvas.drawRect(this.ex, this.it);
            return;
        }
        canvas.drawBitmap(this.jt, (Rect) null, this.ex, (Paint) null);
    }
}
