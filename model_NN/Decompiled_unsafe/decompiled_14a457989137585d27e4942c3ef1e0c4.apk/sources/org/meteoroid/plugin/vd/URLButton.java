package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public final class URLButton extends SimpleButton {
    public String value;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.value = attributeSet.getAttributeValue(str, "value");
    }

    public final void onClick() {
        if (this.value != null) {
            cl.B(this.value);
        }
    }
}
