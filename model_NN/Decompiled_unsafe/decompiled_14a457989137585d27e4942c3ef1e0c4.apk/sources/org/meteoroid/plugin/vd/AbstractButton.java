package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

public abstract class AbstractButton implements cz {
    public static final int PRESSED = 0;
    public static final int RELEASED = 1;
    public static final int UNAVAILABLE_POINTER_ID = -1;
    private cy hK;
    int id = -1;
    Rect jl;
    Rect jm;
    Bitmap[] jn;
    boolean jo = true;
    int state = 1;

    public void a(AttributeSet attributeSet, String str) {
        this.jn = dt.L(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.jl = dt.J(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.jm = dt.J(attributeValue2);
        }
    }

    public void a(cy cyVar) {
        this.hK = cyVar;
        if (this.jl == null) {
            this.jl = this.jm;
        }
    }

    public final boolean a(Bitmap[] bitmapArr) {
        return bitmapArr != null && this.state >= 0 && this.state <= bitmapArr.length - 1 && bitmapArr[this.state] != null;
    }

    public boolean bU() {
        return this.jn != null;
    }

    public final int cd() {
        return this.state;
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        return h(i, i2, i3, i4);
    }

    public abstract boolean h(int i, int i2, int i3, int i4);

    public final boolean isTouchable() {
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (a(this.jn)) {
            canvas.drawBitmap(this.jn[this.state], (Rect) null, this.jm, (Paint) null);
        }
    }
}
