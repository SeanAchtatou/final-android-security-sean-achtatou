package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public abstract class BooleanSwitcher extends Switcher {
    public static final int OFF = 0;
    public static final int ON = 1;

    private void ce() {
        if (this.state == 1) {
            cg();
        } else {
            ch();
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.state = attributeSet.getAttributeBooleanValue(str, "value", false) ? 1 : 0;
        ce();
    }

    public final void cf() {
        this.state = this.state == 0 ? 1 : 0;
        ce();
    }

    public abstract void cg();

    public abstract void ch();
}
