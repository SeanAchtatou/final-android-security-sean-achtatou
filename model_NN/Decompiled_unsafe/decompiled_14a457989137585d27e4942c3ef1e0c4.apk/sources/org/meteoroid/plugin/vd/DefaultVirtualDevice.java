package org.meteoroid.plugin.vd;

import android.util.Log;
import android.view.Display;
import java.util.LinkedHashSet;
import java.util.Properties;

public class DefaultVirtualDevice implements cy {
    public static final int WIDGET_TYPE_ARCADE_JOYSTICK = 11;
    public static final int WIDGET_TYPE_BACKGROUND = 0;
    public static final int WIDGET_TYPE_CALL_OPTIONMENU = 12;
    public static final int WIDGET_TYPE_CHECKIN = 13;
    public static final int WIDGET_TYPE_DEVICE_SCREEN = 2;
    public static final int WIDGET_TYPE_EXIT_BUTTON = 6;
    public static final int WIDGET_TYPE_HIDE_VD = 10;
    public static final int WIDGET_TYPE_INTELEGENCE_BG = 7;
    public static final int WIDGET_TYPE_JOYSTICK = 3;
    public static final int WIDGET_TYPE_MUTE_SWITCHER = 4;
    public static final int WIDGET_TYPE_SENSOR_SWITCHER = 5;
    public static final int WIDGET_TYPE_STEERINGWHEEL = 9;
    public static final int WIDGET_TYPE_URL_BUTTON = 8;
    public static final int WIDGET_TYPE_VIRTUAL_BUTTON = 1;
    public static final String[] jw = {"Background", "VirtualKey", "ScreenWidget", "Joystick", "MuteSwitcher", "SensorSwitcher", "ExitButton", "IntellegenceBackground", "URLButton", "SteeringWheel", "HideVirtualDeviceSwitcher", "ArcadeJoyStick", "CallOptionMenu", "CheckinButton"};
    private final LinkedHashSet ju = new LinkedHashSet();
    private ScreenWidget jv;

    private void a(cz czVar) {
        czVar.a(this);
        this.ju.add(czVar);
        if (czVar.bU()) {
            br.a(czVar);
        }
        if (czVar.isTouchable()) {
            bv.a(czVar);
        }
    }

    public final LinkedHashSet ci() {
        return this.ju;
    }

    public final ScreenWidget cj() {
        return this.jv;
    }

    public final void onCreate() {
        String str;
        int i;
        cf.a((int) VirtualKey.MSG_VIRTUAL_KEY_EVENT, "MSG_VIRTUAL_BUTTON_EVENT");
        Display defaultDisplay = cl.getActivity().getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int min = Math.min(width, height);
        int max = Math.max(width, height);
        if (min == 480 && max == 854) {
            str = "fwvga";
        } else if (min == 480 && max == 800) {
            str = "wvga";
        } else if (min == 360 && max == 640) {
            str = "nhd";
        } else if (min == 320 && max == 480) {
            str = "hvga";
        } else if (min == 240 && max == 320) {
            str = "qvga";
        } else if (min == 240 && max == 400) {
            str = "wqvga";
        } else if (min == 480 && max == 640) {
            str = "vga";
        } else if (min == 600 && max == 800) {
            str = "svga";
        } else if (min == 600 && max == 1024) {
            str = "wsvga";
        } else if (min == 640 && max == 960) {
            str = "retina";
        } else if (min == 540 && max == 960) {
            str = "qhd";
        } else if (min == 800 && max == 1280) {
            str = "wxga";
        } else {
            Log.w(cy.LOG_TAG, "Unkown screen resolution:" + min + "x" + max);
            str = null;
        }
        Properties properties = new Properties();
        try {
            properties.load(cl.getActivity().getResources().openRawResource(dt.I("vd_" + str)));
            if (properties.containsKey("widget.num")) {
                int parseInt = Integer.parseInt(properties.getProperty("widget.num"));
                dr drVar = new dr(properties);
                for (int i2 = 1; i2 <= parseInt; i2++) {
                    try {
                        if (properties.containsKey("widget." + i2 + ".type")) {
                            String property = properties.getProperty("widget." + i2 + ".type");
                            try {
                                int parseInt2 = Integer.parseInt(property);
                                if (parseInt2 < 0 || parseInt2 >= jw.length) {
                                    Log.w(cy.LOG_TAG, "Unknown widget type:" + parseInt2);
                                } else {
                                    property = jw[parseInt2];
                                    if (property.equals(jw[2])) {
                                        ScreenWidget screenWidget = (ScreenWidget) bp.gC;
                                        screenWidget.a(drVar, "widget." + i2 + ".");
                                        screenWidget.ck();
                                        if (this.jv != null) {
                                            ScreenWidget screenWidget2 = this.jv;
                                            this.ju.remove(screenWidget2);
                                            br.b(screenWidget2);
                                            bv.b(screenWidget2);
                                        }
                                        if (screenWidget != null) {
                                            a(screenWidget);
                                        }
                                        this.jv = screenWidget;
                                    } else {
                                        cz czVar = (cz) Class.forName("org.meteoroid.plugin.vd." + property).newInstance();
                                        czVar.a(drVar, "widget." + i2 + ".");
                                        a(czVar);
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e2) {
                        Log.w(cy.LOG_TAG, "Init widget[" + i2 + "] error." + e2);
                    }
                }
            }
            if (properties.containsKey("widget.orientation")) {
                String property2 = properties.getProperty("widget.orientation");
                if (property2.equals("landscape")) {
                    i = 0;
                } else if (property2.equals("portrait")) {
                    i = 1;
                } else if (property2.equals("auto")) {
                    i = 4;
                } else {
                    Log.w(cy.LOG_TAG, "Orientation not specificed. It will be decided by user. ");
                    i = 2;
                }
                cl.H(i);
            }
            properties.clear();
            System.gc();
        } catch (Exception e3) {
            Log.e(cy.LOG_TAG, "Load virtualdevice properties error. Maybe the file is missing." + e3);
        }
    }

    public final void onDestroy() {
        this.ju.clear();
    }
}
