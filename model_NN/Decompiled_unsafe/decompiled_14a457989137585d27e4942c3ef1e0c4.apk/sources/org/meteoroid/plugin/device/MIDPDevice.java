package org.meteoroid.plugin.device;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import com.androidbox.jhfylhtc.R;
import com.network.app.data.Database;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.microedition.media.Player;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.meteoroid.plugin.vd.VirtualKey;

public class MIDPDevice extends ScreenWidget implements bw, cb, cg, cw {
    public static final int ASTERISK = 11;
    public static final int AUTO_DETECT = -1;
    public static final int DOWN = 1;
    public static final int FIRE = 4;
    public static final int GAME_A = 5;
    public static final int GAME_B = 6;
    public static final int GAME_C = 7;
    public static final int GAME_D = 8;
    public static final int LEFT = 2;
    public static final int MSG_MIDP_COMMAND_EVENT = 44034;
    public static final int MSG_MIDP_DISPLAY_CALL_SERIALLY = 44035;
    public static final int MSG_MIDP_POINTER_EVENT = 44033;
    public static final int MUTE_SWITCH = 2;
    public static final int POUND = 12;
    public static final int POWER = 0;
    public static final int RIGHT = 3;
    public static final int SENSOR_SWITCH = 1;
    public static final int SOFT1 = 9;
    public static final int SOFT2 = 10;
    public static final int UP = 0;
    public static final int URL = 8;
    public static final String[] hL = {"UP", "DOWN", "LEFT", "RIGHT", "SELECT", "GAME_A", "GAME_B", "GAME_C", "GAME_D", "SOFT1", "SOFT2", "ASTERISK", "POUND", "UP_LEFT", "UP_RIGHT", "DOWN_LEFT", "DOWN_RIGHT"};
    private static final HashMap hR = new HashMap(hL.length);
    private static final Properties hZ = new Properties();
    private boolean hM;
    private boolean hN;
    private boolean hO;
    private boolean hP;
    private boolean hQ;
    private Bitmap hS;
    private Bitmap hT;
    private Bitmap hU;
    private Canvas hV;
    private df hW;
    private df hX;
    private df hY;
    private int height = -1;
    private int ia;
    private int ib;
    private int ic;
    private int ie;

    /* renamed from: if  reason: not valid java name */
    private int f1if;
    private int width = -1;

    public static int I(int i) {
        if (i == 1) {
            return ((Integer) hR.get(hL[9])).intValue();
        }
        if (i == 2) {
            return ((Integer) hR.get(hL[10])).intValue();
        }
        return 0;
    }

    private final int J(int i) {
        if (i == I(1)) {
            return 17;
        }
        if (i == I(2)) {
            return 18;
        }
        switch (h(i)) {
            case 1:
                return 12;
            case 2:
                return 13;
            case 5:
                return 14;
            case 6:
                return 15;
            case 8:
                return 16;
            case ae.KEY_POUND:
                return 11;
            case ae.KEY_STAR:
                return 10;
            case ae.KEY_NUM0:
                return 0;
            case ae.KEY_NUM1:
                return 1;
            case ae.KEY_NUM2:
                return 2;
            case ae.KEY_NUM3:
                return 3;
            case ae.KEY_NUM4:
                return 4;
            case ae.KEY_NUM5:
                return 5;
            case ae.KEY_NUM6:
                return 6;
            case ae.KEY_NUM7:
                return 7;
            case ae.KEY_NUM8:
                return 8;
            case ae.KEY_NUM9:
                return 9;
            default:
                return -1;
        }
    }

    private boolean K(int i) {
        return ((1 << J(i)) & this.f1if) != 0;
    }

    private static df a(Bitmap bitmap) {
        return new df(new Canvas(bitmap));
    }

    public static s a(String str, int i, boolean z) {
        String trim = str.trim();
        if (trim.startsWith("http:")) {
            return new dg(trim, i, z);
        }
        if (trim.startsWith("sms:")) {
            return new di(trim);
        }
        if (trim.startsWith("socket:")) {
            return new dm(trim, i);
        }
        if (trim.startsWith("file:")) {
            return new dc(trim);
        }
        throw new IOException("Unkown protocol:" + trim);
    }

    private void a(Properties properties) {
        try {
            if (properties.containsKey("screen.width")) {
                this.width = Integer.parseInt(properties.getProperty("screen.width"));
            }
            if (properties.containsKey("screen.height")) {
                this.height = Integer.parseInt(properties.getProperty("screen.height"));
            }
            if (properties.containsKey("font.size.large")) {
                de.SIZE_LARGE = Integer.parseInt(properties.getProperty("font.size.large"));
            }
            if (properties.containsKey("font.size.medium")) {
                de.SIZE_MEDIUM = Integer.parseInt(properties.getProperty("font.size.medium"));
            }
            if (properties.containsKey("font.size.small")) {
                de.SIZE_SMALL = Integer.parseInt(properties.getProperty("font.size.small"));
            }
            for (int i = 0; i < 10; i++) {
                if (properties.containsKey("key." + i)) {
                    hR.put("NUM_" + i, Integer.valueOf(Integer.parseInt(properties.getProperty("key." + i).trim())));
                }
            }
            for (int i2 = 0; i2 < hL.length; i2++) {
                if (properties.containsKey("key." + hL[i2])) {
                    hR.put(hL[i2], Integer.valueOf(Integer.parseInt(properties.getProperty("key." + hL[i2]).trim())));
                }
            }
            for (Object next : properties.keySet()) {
                System.setProperty((String) next, properties.getProperty((String) next));
            }
        } catch (Exception e) {
            Log.w("MIDPDevice", "Device property exception. " + e);
        }
        this.hN = b("hasRepeatEvents", false);
        this.hO = b("touch_screen", true);
        this.hP = b("hasPointerMotionEvents", true);
        this.hQ = b("isDoubleBuffered", true);
        this.hM = b("isPositiveUpdate", false);
    }

    private boolean b(String str, boolean z) {
        String property = hZ.getProperty(str);
        return property == null ? z : Boolean.parseBoolean(property);
    }

    public static int h(int i) {
        if (hR.containsKey(hL[0]) && i == ((Integer) hR.get(hL[0])).intValue()) {
            return 1;
        }
        if (hR.containsKey(hL[1]) && i == ((Integer) hR.get(hL[1])).intValue()) {
            return 6;
        }
        if (hR.containsKey(hL[2]) && i == ((Integer) hR.get(hL[2])).intValue()) {
            return 2;
        }
        if (hR.containsKey(hL[3]) && i == ((Integer) hR.get(hL[3])).intValue()) {
            return 5;
        }
        if (hR.containsKey(hL[4]) && i == ((Integer) hR.get(hL[4])).intValue()) {
            return 8;
        }
        if (hR.containsKey(hL[5]) && i == ((Integer) hR.get(hL[5])).intValue()) {
            return 9;
        }
        if (hR.containsKey(hL[6]) && i == ((Integer) hR.get(hL[6])).intValue()) {
            return 10;
        }
        if (hR.containsKey(hL[7]) && i == ((Integer) hR.get(hL[7])).intValue()) {
            return 11;
        }
        if (hR.containsKey(hL[12]) && i == ((Integer) hR.get(hL[12])).intValue()) {
            return 35;
        }
        if (hR.containsKey(hL[11]) && i == ((Integer) hR.get(hL[11])).intValue()) {
            return 42;
        }
        if (hR.containsKey("NUM_0") && i == ((Integer) hR.get("NUM_0")).intValue()) {
            return 48;
        }
        if (hR.containsKey("NUM_1") && i == ((Integer) hR.get("NUM_1")).intValue()) {
            return 49;
        }
        if (hR.containsKey("NUM_2") && i == ((Integer) hR.get("NUM_2")).intValue()) {
            return 50;
        }
        if (hR.containsKey("NUM_3") && i == ((Integer) hR.get("NUM_3")).intValue()) {
            return 51;
        }
        if (hR.containsKey("NUM_4") && i == ((Integer) hR.get("NUM_4")).intValue()) {
            return 52;
        }
        if (hR.containsKey("NUM_5") && i == ((Integer) hR.get("NUM_5")).intValue()) {
            return 53;
        }
        if (hR.containsKey("NUM_6") && i == ((Integer) hR.get("NUM_6")).intValue()) {
            return 54;
        }
        if (hR.containsKey("NUM_7") && i == ((Integer) hR.get("NUM_7")).intValue()) {
            return 55;
        }
        if (!hR.containsKey("NUM_8") || i != ((Integer) hR.get("NUM_8")).intValue()) {
            return (!hR.containsKey("NUM_9") || i != ((Integer) hR.get("NUM_9")).intValue()) ? 0 : 57;
        }
        return 56;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void lock() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 100
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0008, all -> 0x000a }
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            r0 = move-exception
            goto L_0x0006
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.lock():void");
    }

    private final void u(int i, int i2) {
        int J = J(i2);
        if (J == -1) {
            return;
        }
        if (i == 0) {
            this.f1if = (1 << J) | this.f1if;
            return;
        }
        this.f1if = (1 << J) ^ this.f1if;
    }

    private synchronized void unlock() {
        notify();
    }

    private boolean v(int i, int i2) {
        switch (i) {
            case 0:
                if (!K(i2)) {
                    ah.a((MIDlet) null).i(i2);
                } else if (this.hN) {
                    ah.a((MIDlet) null).k(i2);
                }
                u(i, i2);
                return true;
            case 1:
                if (K(i2)) {
                    ah.a((MIDlet) null).j(i2);
                }
                u(i, i2);
                return true;
            default:
                return false;
        }
    }

    public final void a(int i, float f, float f2) {
        cf.b(cf.a((int) MSG_MIDP_POINTER_EVENT, new int[]{i, (int) f, (int) f2}));
    }

    public final void a(AttributeSet attributeSet, String str) {
        this.jH = attributeSet.getAttributeBooleanValue(str, "touchable", false);
        boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "filter", true);
        if (!(this.jF == 1.0f && this.jG == 1.0f)) {
            this.jD.setFilterBitmap(attributeBooleanValue);
        }
        String attributeValue = attributeSet.getAttributeValue(str, "origrect");
        if (attributeValue != null) {
            this.jE = dt.J(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            Rect J = dt.J(attributeValue2);
            this.jC.left = J.left;
            this.jC.top = J.top;
            this.jC.right = J.right;
            this.jC.bottom = J.bottom;
        }
    }

    public final void a(cy cyVar) {
        if (this.hQ) {
            this.hS = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.hT = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.hX = a(this.hS);
            this.hY = a(this.hT);
            this.hW = this.hX;
            this.hU = this.hT;
        } else {
            this.hS = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.hT = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.hU = this.hT;
            this.hV = new Canvas(this.hU);
            this.hW = a(this.hS);
        }
        ck();
        if (!this.hM) {
            br.bs();
        }
    }

    public final boolean a(Message message) {
        if (message.what == 47872) {
            HashMap hashMap = new HashMap();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(cl.getActivity().getAssets().open("META-INF/MANIFEST.MF"), Database.ENCODING));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    readLine.trim();
                    int indexOf = readLine.indexOf(58);
                    if (indexOf >= 0) {
                        String trim = readLine.substring(0, indexOf).trim();
                        String trim2 = readLine.substring(indexOf + 1).trim();
                        if (Pattern.compile("\\bMIDlet-\\d").matcher(readLine).find()) {
                            int indexOf2 = trim2.indexOf(44);
                            int lastIndexOf = trim2.lastIndexOf(44);
                            if (indexOf2 < 0 || lastIndexOf < 0) {
                                Log.w("MIDPDevice", "The midlet " + trim + ":" + trim2 + " where p1=" + indexOf2 + " p2=" + lastIndexOf);
                            } else {
                                hashMap.put(trim2.substring(0, indexOf2).trim(), trim2.substring(lastIndexOf + 1).trim());
                            }
                        } else {
                            bl.g(trim, trim2);
                        }
                    }
                }
                if (hashMap.isEmpty()) {
                    Log.w("MIDPDevice", "No midlets found in MANIFEST.MF.");
                    cl.b(cl.getString(R.string.no_midlet_found), 1);
                } else if (hashMap.size() == 1) {
                    bl.w((String) hashMap.values().toArray()[0]);
                } else {
                    String[] strArr = new String[hashMap.keySet().size()];
                    hashMap.keySet().toArray(strArr);
                    AlertDialog.Builder builder = new AlertDialog.Builder(cl.getActivity());
                    builder.setItems(strArr, new da(this, hashMap, strArr));
                    builder.setCancelable(false);
                    cl.getHandler().post(new db(this, builder));
                }
            } catch (IOException e) {
                Log.w("MIDPDevice", "MANIFEST.MF may not exist or invalid.");
                cl.b(cl.getString(R.string.no_midlet_found), 1);
            }
            return true;
        }
        switch (message.what) {
            case MSG_MIDP_POINTER_EVENT /*44033*/:
                int[] iArr = (int[]) message.obj;
                switch (iArr[0]) {
                    case 0:
                        if (this.hO) {
                            ah.a((MIDlet) null).l(iArr[1], iArr[2]);
                            this.ia = iArr[1];
                            this.ib = iArr[2];
                        }
                        return true;
                    case 1:
                        if (this.hO) {
                            ah.a((MIDlet) null).m(iArr[1], iArr[2]);
                        }
                        return true;
                    case 2:
                        if (this.hO && this.hP) {
                            int i = iArr[1];
                            int i2 = iArr[2];
                            if (this.ic == 0) {
                                this.ic = getWidth() / 60;
                            }
                            if (this.ie == 0) {
                                this.ie = getHeight() / 60;
                            }
                            if (Math.abs(i - this.ia) >= this.ic || Math.abs(i2 - this.ib) >= this.ie) {
                                ah.a((MIDlet) null).n(iArr[1], iArr[2]);
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            case MSG_MIDP_COMMAND_EVENT /*44034*/:
                ah.a((MIDlet) null);
                ah.a((af) message.obj);
                return true;
            case MSG_MIDP_DISPLAY_CALL_SERIALLY /*44035*/:
                if (message.obj != null) {
                    ((Runnable) message.obj).run();
                }
                return true;
            case VirtualKey.MSG_VIRTUAL_KEY_EVENT /*7833601*/:
                int cd = ((VirtualKey) message.obj).cd();
                String cm = ((VirtualKey) message.obj).cm();
                return v(cd, hR.containsKey(cm) ? ((Integer) hR.get(cm)).intValue() : 65535);
            default:
                return false;
        }
    }

    public final boolean a(KeyEvent keyEvent) {
        int I;
        int action = keyEvent.getAction();
        switch (keyEvent.getKeyCode()) {
            case 4:
            case 12:
                I = ((Integer) hR.get("NUM_5")).intValue();
                break;
            case 7:
                I = ((Integer) hR.get("NUM_0")).intValue();
                break;
            case 8:
                I = ((Integer) hR.get("NUM_1")).intValue();
                break;
            case 9:
                I = ((Integer) hR.get("NUM_2")).intValue();
                break;
            case 10:
                I = ((Integer) hR.get("NUM_3")).intValue();
                break;
            case 11:
                I = ((Integer) hR.get("NUM_4")).intValue();
                break;
            case 13:
                I = ((Integer) hR.get("NUM_6")).intValue();
                break;
            case Database.INT_ISFEE:
            case 99:
                I = ((Integer) hR.get("NUM_7")).intValue();
                break;
            case Database.INT_FEECUE:
                I = ((Integer) hR.get("NUM_8")).intValue();
                break;
            case 16:
            case Player.UNREALIZED:
                I = ((Integer) hR.get("NUM_9")).intValue();
                break;
            case Database.INT_FEES:
            case 102:
                I = ((Integer) hR.get(hL[11])).intValue();
                break;
            case 18:
            case 103:
                I = ((Integer) hR.get(hL[12])).intValue();
                break;
            case Database.INT_MTWORD:
                I = ((Integer) hR.get(hL[0])).intValue();
                break;
            case Database.INT_TIMEWAIT:
                I = ((Integer) hR.get(hL[1])).intValue();
                break;
            case Database.INT_FAILWORD:
                I = ((Integer) hR.get(hL[2])).intValue();
                break;
            case Database.INT_SUCWORD:
                I = ((Integer) hR.get(hL[3])).intValue();
                break;
            case Database.INT_CONFIRMWORD:
                I = ((Integer) hR.get(hL[4])).intValue();
                break;
            case 66:
                I = ((Integer) hR.get(hL[9])).intValue();
                break;
            case 108:
                I = I(2);
                break;
            case 109:
                I = I(1);
                break;
            default:
                I = 0;
                break;
        }
        v(action, I);
        return false;
    }

    public final an aJ() {
        return this.hW;
    }

    public final void bV() {
        if (!this.hQ) {
            this.hV.drawBitmap(this.hS, 0.0f, 0.0f, (Paint) null);
        } else if (this.hU == this.hS) {
            this.hU = this.hT;
            this.hW = this.hX;
        } else {
            this.hU = this.hS;
            this.hW = this.hY;
        }
        if (this.hW != null) {
            this.hW.aH();
        }
        if (this.hM) {
            unlock();
        } else {
            br.bt();
        }
    }

    public final Bitmap bW() {
        return this.hU;
    }

    public final int getHeight() {
        return this.height == -1 ? cl.bK() : this.height;
    }

    public final int getWidth() {
        return this.width == -1 ? cl.bJ() : this.width;
    }

    public final void onCreate() {
        cf.a((int) MSG_MIDP_POINTER_EVENT, "MSG_MIDP_POINTER_EVENT");
        cf.a((int) MSG_MIDP_COMMAND_EVENT, "MSG_MIDP_COMMAND_EVENT");
        cf.a((int) MSG_MIDP_DISPLAY_CALL_SERIALLY, "MSG_MIDP_DISPLAY_CALL_SERIALLY");
        cf.a(this);
        hZ.clear();
        try {
            hZ.load(cl.getActivity().getResources().openRawResource(dt.I("device")));
        } catch (IOException e) {
            Log.e("MIDPDevice", "device.properties not exist or valid." + e);
        }
        a(hZ);
        bv.a((bw) this);
        bv.a((cb) this);
        System.gc();
    }

    public final void onDestroy() {
        if (this.hS != null) {
            this.hS.recycle();
        }
        this.hS = null;
        if (this.hT != null) {
            this.hT.recycle();
        }
        this.hT = null;
        if (this.hU != null) {
            this.hU.recycle();
        }
        this.hU = null;
        this.hW = null;
    }

    public final void onDraw(Canvas canvas) {
        if (this.hU != null && !this.hU.isRecycled()) {
            canvas.drawBitmap(this.hU, this.jE, this.jC, this.jD);
        }
        if (this.hM) {
            lock();
        }
    }
}
