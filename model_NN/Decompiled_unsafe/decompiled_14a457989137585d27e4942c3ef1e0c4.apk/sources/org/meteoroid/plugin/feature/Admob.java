package org.meteoroid.plugin.feature;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Admob extends Advertisement implements AdListener {
    private AdView iZ;
    private AdRequest ja;

    public final void F(String str) {
        super.F(str);
        String G = G("ADMOB_PUBLISHER_ID");
        if (G == null) {
            Log.e(getClass().getSimpleName(), "Hey, the most important publish id is missed.");
        }
        AdSize adSize = AdSize.bx;
        String G2 = G("ADSIZE");
        if (G2 != null) {
            if (G2.equalsIgnoreCase("IAB")) {
                adSize = AdSize.bz;
            } else if (G2.equalsIgnoreCase("LEADERBOARD")) {
                adSize = AdSize.bA;
            } else if (G2.equalsIgnoreCase("MRECT")) {
                adSize = AdSize.by;
            }
        }
        this.iZ = new AdView(cl.getActivity(), adSize, G);
        this.iZ.setAdListener(this);
        this.iZ.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.ja = new AdRequest();
        String G3 = G("KEYWORDS");
        if (G3 != null) {
            String[] split = G3.split(";");
            for (String addKeyword : split) {
                this.ja.addKeyword(addKeyword);
            }
        }
        this.ja.setTesting(this.jg);
        this.iZ.loadAd(this.ja);
        View ca = ca();
        String str2 = this.jc;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        if (Advertisement.je == null) {
            RelativeLayout relativeLayout = new RelativeLayout(cl.getActivity());
            Advertisement.je = relativeLayout;
            relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        }
        if (!(str2 == null || str2 == null)) {
            if (str2.equalsIgnoreCase("bottom")) {
                layoutParams.addRule(12);
            } else if (str2.equalsIgnoreCase("top")) {
                layoutParams.addRule(10);
            }
        }
        Advertisement.je.addView(ca, layoutParams);
        cl.bM().schedule(this, 0, (long) (this.jd * 1000));
    }

    public final boolean bY() {
        if (this.iZ != null) {
            return this.iZ.isReady();
        }
        return false;
    }

    public final void bZ() {
        super.bZ();
        if (!this.iZ.isRefreshing()) {
            this.iZ.loadAd(this.ja);
        }
    }

    public final View ca() {
        return this.iZ;
    }

    public final void onDestroy() {
        this.iZ.stopLoading();
        if (this.iZ != null) {
            this.iZ.setAdListener(null);
        }
        super.onDestroy();
    }

    public void onDismissScreen(Ad ad) {
        if (!this.iZ.isRefreshing()) {
            this.iZ.loadAd(this.ja);
        }
        cf.b(cf.a((int) cl.MSG_SYSTEM_LOG_EVENT, new String[]{"OnClick", getClass().getSimpleName()}));
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
    }

    public void onLeaveApplication(Ad ad) {
    }

    public void onPresentScreen(Ad ad) {
    }

    public void onReceiveAd(Ad ad) {
        this.ji = true;
    }
}
