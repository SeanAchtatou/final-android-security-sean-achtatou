package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import java.util.ArrayList;

public abstract class AbstractPaymentManager implements DialogInterface.OnClickListener, cg, cx {
    private ArrayList iX;

    public interface Payment extends cx {
        public static final int MSG_PAYMENT_FAIL = 61699;
        public static final int MSG_PAYMENT_QUERY = 61697;
        public static final int MSG_PAYMENT_REQUEST = 61696;
        public static final int MSG_PAYMENT_SUCCESS = 61698;
    }

    public final void a(Payment payment) {
        this.iX.add(payment);
    }
}
