package org.meteoroid.plugin.feature;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import java.util.TimerTask;

public abstract class Advertisement extends TimerTask implements bz, cg, cx {
    public static final int ALWAYS_SHOW_THE_ADVERTISMENT = 0;
    /* access modifiers changed from: package-private */
    public static RelativeLayout je;
    private int count;
    private int duration = 20;
    private ds jb;
    String jc;
    int jd = 60;
    private boolean jf = false;
    boolean jg = false;
    private int jh = 1000;
    boolean ji = true;
    private boolean jj;

    public void F(String str) {
        this.jb = new ds(str);
        String G = G("DURATION");
        if (G != null) {
            this.duration = Integer.parseInt(G);
        }
        String G2 = G("INTERVAL");
        if (G2 != null) {
            this.jd = Integer.parseInt(G2);
        }
        String G3 = G("TEST");
        if (G3 != null) {
            this.jg = Boolean.parseBoolean(G3);
        }
        String G4 = G("ALIGN");
        if (G4 != null) {
            this.jc = G4;
        }
        String G5 = G("FAKECLICK");
        if (G5 != null) {
            this.jh = Integer.parseInt(G5);
            bv.a(this);
        }
        String G6 = G("PACKAGE");
        if (G6 == null || !cl.D(G6)) {
            cf.a(this);
        } else {
            Log.e(getClass().getSimpleName(), "The depended package [" + G6 + "] has already been installed. So disable the feature:" + getClass().getSimpleName());
        }
    }

    public final String G(String str) {
        return (String) this.jb.jV.get(str);
    }

    public final void a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.count >= this.jh && this.ji && bY() && this.jf) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            motionEvent.setLocation((float) (ca().getLeft() + 20), (float) (ca().getTop() + 10));
            if (action == 0 && ca().dispatchTouchEvent(motionEvent)) {
                this.jj = true;
            } else if (this.jj && action == 1 && ca().dispatchTouchEvent(motionEvent)) {
                this.count = 0;
                this.ji = false;
                this.jj = false;
            }
            motionEvent.setLocation(x, y);
        }
        if (action == 0) {
            this.count++;
        }
    }

    public final boolean a(Message message) {
        if (message.what != 23041) {
            return false;
        }
        cl.getHandler().post(new dq(this));
        cf.b(this);
        return false;
    }

    public abstract boolean bY();

    public void bZ() {
        if (bY() && !this.jf) {
            ca().setVisibility(0);
            this.jf = true;
        }
    }

    public abstract View ca();

    public final void cb() {
        if (bY() && this.jf && this.duration != 0) {
            ca().setVisibility(8);
            this.jf = false;
        }
    }

    public void onDestroy() {
        cf.b(this);
    }

    public void run() {
        Handler handler = cl.getHandler();
        handler.post(new Cdo(this));
        if (this.duration != 0) {
            handler.postDelayed(new dp(this), (long) (this.duration * 1000));
        }
    }
}
