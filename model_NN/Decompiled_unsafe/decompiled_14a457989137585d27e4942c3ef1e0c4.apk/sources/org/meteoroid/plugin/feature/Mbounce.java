package org.meteoroid.plugin.feature;

import android.os.Message;
import android.os.SystemClock;
import com.network.app.util.Pay;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public class Mbounce implements cg, cx, Runnable, AbstractPaymentManager.Payment {
    public final void F(String str) {
        Pay.j(cl.getActivity());
        Pay.dr = false;
        cf.a(this);
    }

    public final boolean a(Message message) {
        if (message.what == 15391744) {
            synchronized (this) {
                Pay.af();
                new Thread(this).start();
            }
            return true;
        }
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        return false;
    }

    public final void onDestroy() {
    }

    public void run() {
        while (Pay.ds == 0) {
            SystemClock.sleep(500);
        }
        if (Pay.ds == 2) {
            cf.b(cf.a((int) di.MSG_GCF_SMS_CONNECTION_SEND_FAIL, (Object) null));
            cf.b(cf.a((int) AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        } else if (Pay.ds == 1) {
            cf.b(cf.a((int) di.MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, (Object) null));
            cf.b(cf.a((int) AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
        }
    }
}
