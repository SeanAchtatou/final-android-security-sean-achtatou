package defpackage;

import com.google.ads.AdActivity;
import com.network.app.data.Database;
import com.nokia.mid.ui.DirectGraphics;
import java.lang.reflect.Array;
import javax.microedition.media.Player;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;

/* renamed from: bg  reason: default package */
public final class bg implements dy {
    public static final short[] ab = {42, 35, 40, 51, 45, 35, 42, 40, 52, 47, 45, 35, 52, 58, 45, 40, 35, 50, 51, 62, 54, 50, 35, 42, 35, 62, 35, 47, 62, 54, 48, 40, 51, 30, 35, 54, 52, 50, 54, 52, 47, 47, 80, 60, 74, 50, 65, 45, 50, 74, 75, 75, 75, 55};
    public static byte as;
    private static bg fB;
    public static byte gi;
    public ao[] I;
    private ao[] J;
    public byte[] L;
    public boolean M;
    public byte[] N = new byte[2];
    public short[] O;
    public short P = 20;
    public byte R = 0;
    public short S;
    public short T;
    public short U;
    public byte W;
    public byte X;
    public byte Y;
    public byte Z;
    public String[] aC;
    public String aF = "(下或8键凤羽飞火)";
    private String aG = "";
    public int aH;
    public int[] aI;
    public boolean aT;
    public byte aa;
    public int ac;
    public boolean ae;
    public byte af;
    public byte ah;
    public byte aj;
    public byte ak;
    public byte al;
    public byte am;
    public byte an;
    public byte ap;
    public short[] aq;
    public short[] ar;
    public byte at;
    public byte au;
    public byte[] aw = new byte[6];
    public byte[] ax = new byte[6];
    private byte[] ay;
    private byte[] az = {4, 0, 1, 2, 3};
    public int dN;
    public int dO;
    public byte[][] dP;
    private final byte[][][] dQ = {new byte[][]{new byte[]{-75, 35}}, new byte[][]{new byte[]{-85, ToneControl.C4}, new byte[]{-60, 15}}, new byte[][]{new byte[]{-70, 80}, new byte[]{-80, 35}, new byte[]{-50, -5}}};
    public byte[][] dR;
    public boolean dX;
    public boolean dY;
    public boolean dZ;
    public byte e = 0;
    public boolean ea;
    public int eb;
    private int ec;
    public boolean em;
    public byte[][] eq = ((byte[][]) Array.newInstance(Byte.TYPE, 6, 6));
    public byte[][] er;
    private byte[][] es = {new byte[]{2, -38}, new byte[]{36, -10}, new byte[]{26, 30}, new byte[]{-26, 30}, new byte[]{-36, -10}};
    public short[][] ev;
    public byte f = 0;
    public short[][] fA;
    private n[] fC;
    private final byte[][][] fD = {new byte[][]{new byte[]{65, 35}}, new byte[][]{new byte[]{80, 15}, new byte[]{65, 55}}, new byte[][]{new byte[]{58, 35}, new byte[]{80, 75}, new byte[]{85, 0}}};
    private boolean fE = false;
    private short[][] fF = ((short[][]) Array.newInstance(Short.TYPE, 10, 4));
    private byte fG = 0;
    private byte fH;
    private byte fI;
    private boolean fJ;
    public int[] fK;
    public int[] fL;
    public int[] fM;
    public int[] fN;
    private byte fO;
    private byte fP;
    private byte fQ;
    private byte fR;
    public boolean fS;
    public boolean fT;
    public boolean fU;
    public boolean fV;
    private byte fW;
    public boolean fX;
    public boolean[] fY;
    public boolean fZ;
    public n[] fv;
    public n[] fw;
    public n[] fx;
    public n[] fy;
    public short[][] fz;
    public byte g = 0;
    public short[][] ga = ((short[][]) Array.newInstance(Short.TYPE, 6, 2));
    private byte gb;
    public boolean gc = false;
    public byte gd;
    public byte ge;
    public byte gf;
    public byte gg;
    public byte gh;
    public boolean gj = false;
    public boolean gk;
    public boolean gl;
    public byte h;
    public byte i = 0;
    public byte j = 0;
    public short k;
    public short l;
    public byte[] m = {2, 8, 10, 15, 9, 6, 4, 0};
    public n n;
    private n o;
    public n[] p;
    public ao q;
    public ao r;
    public ao s;
    public ao t;
    public ao u;
    private ao v;

    private bg() {
    }

    private void A(an anVar) {
        int i2;
        int i3;
        byte b;
        if (this.fE) {
            anVar.setColor(0);
            anVar.c(0, 0, 240, 320);
        } else {
            anVar.a(this.v, 0, 30, 0);
        }
        if (this.dZ) {
            switch (this.af) {
                case 0:
                    this.p[this.R].aH = this.p[this.R].eb - 10;
                    break;
                case 1:
                    for (int i4 = 0; i4 < this.p.length; i4++) {
                        this.p[i4].aH = this.p[i4].eb + this.L[ee.q(0, this.L.length - 1)];
                        this.p[i4].dN = this.p[i4].ec + this.L[ee.q(0, this.L.length - 1)];
                    }
                    break;
                case 2:
                    this.k = (short) (this.k + 1);
                    this.p[this.R].aH = this.p[this.R].eb + this.L[this.fR];
                    if (this.k % 3 == 0) {
                        byte b2 = (byte) (this.fR + 1);
                        this.fR = b2;
                        this.fR = b2 > this.L.length - 1 ? 0 : this.fR;
                        break;
                    }
                    break;
                case 3:
                    this.p[this.R].dN = this.p[this.R].ec - 30;
                    break;
                case 4:
                    this.p[this.R].dN = this.p[this.R].ec - this.fR;
                    this.fR = (byte) (this.fR + 1);
                    if (this.fR > 10) {
                        this.fR = 10;
                        break;
                    }
                    break;
                case 5:
                    this.k = (short) (this.k + 1);
                    if (this.k % 3 == 0) {
                        this.p[this.R].aH = this.p[this.R].eb + this.L[ee.q(0, this.L.length - 1)];
                        this.p[this.R].dN = this.p[this.R].ec + this.L[ee.q(0, this.L.length - 1)];
                        break;
                    }
                    break;
            }
        }
        aY();
        if (this.ea) {
            switch (this.af) {
                case 0:
                    if (this.j < this.fv.length) {
                        if (this.fv[this.j].dO != 3) {
                            this.fv[this.j].a(1);
                            break;
                        } else {
                            this.fv[this.j].aH = this.fv[this.j].eb + 5;
                            break;
                        }
                    }
                    break;
                case 1:
                    for (int i5 = 0; i5 < this.fv.length; i5++) {
                        if (this.eq[eb.m[i5]][0] != -1) {
                            if (this.fv[this.j].dO == 3) {
                                this.fv[this.j].aH = this.fv[this.j].eb + 5;
                            } else {
                                this.fv[i5].c(2);
                            }
                        }
                    }
                    break;
                case 2:
                    if (this.j < this.fv.length) {
                        if (this.fv[this.j].dO != 3) {
                            this.fv[this.j].c(2);
                            break;
                        } else {
                            this.fv[this.j].aH = this.fv[this.j].eb + 5;
                            break;
                        }
                    }
                    break;
                case 3:
                    if (!(this.f == 4 || this.f == 6)) {
                        if (eb.ab[eb.m[this.j]] < (eb.ev[eb.m[this.j]][4] * 25) / 100) {
                            this.fv[this.j].c(3);
                            this.fv[this.j].aH = this.fv[this.j].eb + 5;
                        } else {
                            this.fv[this.j].c(2);
                        }
                    }
                    this.fv[this.j].dN = this.fv[this.j].ec + this.L[ee.q(0, this.L.length - 1)];
                    break;
                case 4:
                    for (int i6 = 0; i6 < this.fv.length; i6++) {
                        if (this.eq[eb.m[i6]][0] != -1) {
                            if (this.fv[this.j].dO == 3) {
                                this.fv[this.j].aH = this.fv[this.j].eb + 5;
                            } else {
                                this.fv[i6].c(1);
                            }
                        }
                    }
                    break;
            }
        }
        U();
        for (int i7 = 0; i7 < this.fx.length; i7++) {
            this.fx[i7].Q();
            this.fx[i7].c(anVar);
        }
        anVar.setColor(14336887);
        anVar.c(0, 0, 240, 30);
        anVar.setColor(7690321);
        anVar.b(0, 0, 239, 30);
        anVar.b(2, 2, 235, 26);
        anVar.setColor(9728116);
        anVar.c(3, 3, 234, 25);
        if (this.f == 1) {
            du.a(anVar, this.aG, 6, 15 - p.g, 20, 16777215);
            if (!(this.fJ || this.e == 5 || this.aG.length() == 0)) {
                l al2 = l.al();
                l al3 = l.al();
                int i8 = al3.ac + 1;
                al3.ac = i8;
                al2.ac = i8 > 31 ? 0 : l.al().ac;
                if ((l.al().ac >> 4) % 4 == 0) {
                    du.a(anVar, this.aF, (this.aG.length() * p.f) + 6, 15 - p.g, 20, 16711680, 16777215);
                    anVar.d(0, 0, 240, 15 - p.g);
                    du.a(anVar, this.aF, (this.aG.length() * p.f) + 6, 15 - p.g, 20, 0);
                    anVar.d(0, 0, 240, 320);
                }
            }
        } else {
            du.a(anVar, this.aG, 6, 15 - p.g, 20, 16777215);
        }
        ed.cB();
        ed.a(anVar, 70);
        byte b3 = 0;
        while (true) {
            byte b4 = b3;
            if (b4 < eb.m.length) {
                byte b5 = eb.m[b4];
                du.a(anVar, eb.ab[b5] <= 0 ? this.t : this.s, 0, b5 * 23, 69, 23, 0, (b4 * 75) + 14, 253);
                if (b(1, b4)) {
                    if (this.eq[b5][1] == -1) {
                        i2 = 5;
                        i3 = 4;
                        b = 75;
                    } else if (this.eq[b5][2] == -1) {
                        i2 = 3;
                        i3 = 0;
                        b = 77;
                    } else if (this.eq[b5][3] == -1) {
                        i2 = 2;
                        i3 = 3;
                        b = 78;
                    } else if (this.eq[b5][4] == -1) {
                        i2 = 3;
                        i3 = 2;
                        b = 78;
                    } else if (this.eq[b5][5] == -1) {
                        i2 = 3;
                        i3 = 1;
                        b = 78;
                    } else {
                        i2 = 0;
                        i3 = 0;
                        b = 0;
                    }
                    du.a(anVar, this.r, i3 * 25, 0, 25, 20, 0, i2 + (b * b4), 255);
                }
                a.M().b(anVar, (b4 * 77) + 6, 280, 24, 20, 10, eb.m[b4]);
                b3 = (byte) (b4 + 1);
            } else {
                return;
            }
        }
    }

    private void J() {
        this.ay = null;
        this.ay = new byte[5];
        for (int i2 = 0; i2 < this.ay.length; i2++) {
            this.ay[i2] = this.az[i2];
        }
        this.fI = this.ay[0];
    }

    private void U() {
        for (byte b = 0; b < this.fx.length - 1; b = (byte) (b + 1)) {
            byte b2 = b;
            for (byte b3 = (byte) (b + 1); b3 < this.fx.length; b3 = (byte) (b3 + 1)) {
                if (this.fx[b3].dN < this.fx[b2].dN) {
                    b2 = b3;
                }
            }
            if (b2 != b) {
                n nVar = this.fx[b];
                this.fx[b] = this.fx[b2];
                this.fx[b2] = nVar;
            }
        }
    }

    private boolean W() {
        for (int i2 = 0; i2 < this.fy.length / 2; i2++) {
            if (!this.fy[(this.fy.length / 2) + i2].M) {
                return false;
            }
        }
        return true;
    }

    private void a(int i2, int i3) {
        int i4;
        int e2 = e(i2, 3, 0, 1);
        if (as == 2) {
            e2 = 10;
        }
        short s2 = this.ev[this.R][5];
        if ((e2 + 10) / s2 >= s2) {
            this.fX = false;
            int q2 = ee.q(0, 99);
            int s3 = s(i2, 0);
            if (q2 < 5) {
                this.fU = true;
                i4 = s3 * 2;
            } else {
                this.fU = false;
                i4 = s3;
            }
            this.l = (short) ((i4 + e(i2, 0, 0, 1)) - r(i3, 0));
            this.l = this.l <= 0 ? 10 : this.l;
            return;
        }
        this.fX = true;
    }

    private void a(int i2, int i3, int i4, int i5, int i6) {
        switch (i4) {
            case 0:
                int e2 = e(i2, 3, 0, 1);
                if (as == 2) {
                    e2 = 10;
                }
                short s2 = this.ev[this.R][5];
                if ((e2 + 10) / s2 >= s2) {
                    this.fX = false;
                    this.l = (short) ((((s(i2, i5) + e(i2, 0, i5, 1)) * eb.ar[i6]) / 100) - r(i3, i5));
                    this.l = this.l <= 0 ? 15 : this.l;
                    return;
                }
                this.fX = true;
                return;
            case 1:
                this.fY = new boolean[this.p.length];
                this.aq = new short[this.p.length];
                int s3 = ((s(i2, i5) + e(i2, 0, i5, 1)) * eb.ar[i6]) / 100;
                int e3 = e(i2, 3, 0, 1);
                if (as == 2) {
                    e3 = 10;
                }
                for (int i7 = 0; i7 < this.p.length; i7++) {
                    short s4 = this.ev[i7][5];
                    if ((e3 + 10) / s4 >= s4) {
                        this.fY[i7] = false;
                        this.aq[i7] = (short) (s3 - ((short) r(i7, i5)));
                        this.aq[i7] = this.aq[i7] <= 0 ? 15 : this.aq[i7];
                    } else {
                        this.fY[i7] = true;
                    }
                }
                return;
            default:
                return;
        }
    }

    private void a(int i2, short s2, int i3, int i4) {
        this.fF[this.fG][0] = (short) i2;
        this.fF[this.fG][1] = s2;
        this.fF[this.fG][2] = (short) i3;
        this.fF[this.fG][3] = (short) i4;
        this.fG = (byte) (this.fG + 1);
    }

    private void a(boolean z, int i2, int i3, int i4, int i5, int i6) {
        int i7;
        if (i6 == 0) {
            this.fZ = false;
            if (!z) {
                a(i2, i3);
                return;
            }
            switch (i4) {
                case 0:
                case 2:
                    a(i2, i3, i5, 0, 7);
                    return;
                case 1:
                    a(i2, i3, i5, 1, 8);
                    return;
                case 3:
                default:
                    return;
                case 4:
                    a(i2, i3, i5, 1, 8);
                    return;
            }
        } else {
            int q2 = ee.q(0, 99);
            int s2 = s(i2, 0);
            if (q2 < 5) {
                this.fU = true;
                i7 = s2 * 2;
            } else {
                this.fU = false;
                i7 = s2;
            }
            this.an = (byte) ee.q(0, eb.m.length - 1);
            if (eb.m.length > 0) {
                if (this.an == i2) {
                    a(i2, i3);
                    this.fZ = false;
                } else if (this.eq[eb.m[this.an]][0] != -1) {
                    this.fZ = true;
                } else {
                    a(i2, i3);
                    this.fZ = false;
                }
            }
            if (this.fZ) {
                this.fX = false;
                this.l = (short) ((i7 + e(i2, 0, 0, 1)) - (t(this.an, 0) + (e(this.an, 1, 0, 2) + e(this.an, 1, 0, 3))));
                this.l = this.l <= 0 ? 1 : this.l;
            }
        }
    }

    public static bg aU() {
        if (fB == null) {
            fB = new bg();
        }
        return fB;
    }

    private void aV() {
        switch (this.Z) {
            case 1:
                this.fy = new n[30];
                for (int i2 = 0; i2 < this.fy.length / 2; i2++) {
                    this.fy[i2] = n.a(this.fw[1]);
                }
                for (int length = this.fy.length / 2; length < this.fy.length; length++) {
                    this.fy[length] = n.a(this.fw[0]);
                }
                return;
            case 2:
            case 3:
            case 7:
            case 8:
            case 12:
            case Database.INT_ISFEE /*14*/:
            case Database.INT_TIMEWAIT /*20*/:
            case Database.INT_SUCWORD /*22*/:
            default:
                return;
            case 4:
            case 10:
            case 16:
                this.fy = new n[1];
                for (int i3 = 0; i3 <= 0; i3++) {
                    this.fy[0] = n.a(this.fw[2]);
                }
                return;
            case 5:
            case Database.INT_FEES /*17*/:
                this.fy = new n[1];
                for (int i4 = 0; i4 <= 0; i4++) {
                    this.fy[0] = n.a(this.fw[0]);
                }
                return;
            case 6:
                this.fy = new n[1];
                for (int i5 = 0; i5 < this.fy.length; i5++) {
                    this.fy[i5] = c.g("/n/61");
                }
                return;
            case 9:
                this.fy = new n[5];
                for (int i6 = 0; i6 < this.fy.length; i6++) {
                    this.fy[i6] = n.a(this.fw[0]);
                    this.fy[i6].W = (byte) ee.q(12, 25);
                }
                return;
            case 11:
                this.fy = new n[6];
                for (int i7 = 0; i7 < this.fy.length / 2; i7++) {
                    this.fy[i7] = n.a(this.fw[1]);
                    this.fy[i7].W = (byte) ee.q(12, 25);
                    this.fy[(this.fy.length / 2) + i7] = n.a(this.fw[0]);
                }
                return;
            case 13:
                this.fy = new n[eb.m.length];
                for (int i8 = 0; i8 < this.fy.length; i8++) {
                    this.fy[i8] = n.a(this.fw[1]);
                }
                return;
            case Database.INT_FEECUE /*15*/:
                this.fy = new n[5];
                for (int i9 = 0; i9 < this.fy.length; i9++) {
                    this.fy[i9] = n.a(this.fw[3]);
                }
                return;
            case 18:
                this.fy = new n[(eb.m.length * 2)];
                for (int i10 = 0; i10 < this.fy.length / 2; i10++) {
                    this.fy[i10] = n.a(this.fw[0]);
                    this.fy[(this.fy.length / 2) + i10] = n.a(this.fw[1]);
                }
                return;
            case Database.INT_MTWORD /*19*/:
                this.fy = new n[eb.m.length];
                for (int i11 = 0; i11 < this.fy.length; i11++) {
                    this.fy[i11] = n.a(this.fw[1]);
                }
                return;
            case Database.INT_FAILWORD /*21*/:
                this.fy = new n[eb.m.length];
                for (int i12 = 0; i12 < this.fy.length; i12++) {
                    this.fy[i12] = n.a(this.fw[3]);
                }
                return;
            case Database.INT_CONFIRMWORD /*23*/:
                this.fy = new n[(eb.m.length * 2)];
                for (int i13 = 0; i13 < this.fy.length; i13++) {
                    this.fy[i13] = n.a(this.fw[1]);
                }
                return;
            case 24:
                this.fy = new n[1];
                for (int i14 = 0; i14 < this.fy.length; i14++) {
                    this.fy[i14] = n.a(this.fw[4]);
                }
                return;
            case 25:
                this.fy = new n[eb.m.length];
                for (int i15 = 0; i15 < this.fy.length; i15++) {
                    this.fy[i15] = n.a(this.fw[1]);
                }
                return;
        }
    }

    private boolean aW() {
        for (n W2 : this.fy) {
            if (!W2.W()) {
                return false;
            }
        }
        return true;
    }

    private boolean aX() {
        for (int i2 = 0; i2 < this.fy.length / 2; i2++) {
            if (!this.fy[i2].em) {
                return false;
            }
        }
        return true;
    }

    private void aY() {
        for (int i2 = 0; i2 < this.fv.length; i2++) {
            byte b = eb.m[i2];
            if (eb.ab[b] <= 0 || this.eq[b][0] == -1) {
                this.fv[i2].a(4);
            } else if (!(this.f == 4 || this.f == 6 || this.f == 3)) {
                if (eb.ab[b] < (eb.ev[b][4] * 25) / 100) {
                    this.fv[i2].c(3);
                } else {
                    this.fv[i2].c(0);
                }
            }
        }
    }

    private void aZ() {
        for (int i2 = 0; i2 < this.p.length; i2++) {
            this.p[this.R].aH = this.p[this.R].eb;
            this.p[this.R].dN = this.p[this.R].ec;
        }
    }

    private void b() {
        w();
        this.W = 0;
        this.fJ = false;
        if (!q(0)) {
            for (byte b = 0; b < this.fv.length; b = (byte) (b + 1)) {
                byte b2 = (byte) (this.j + 1);
                this.j = b2;
                this.j = b2 > ((byte) (this.fv.length - 1)) ? 0 : this.j;
                if (this.eq[eb.m[this.j]][0] != -1) {
                    break;
                }
                this.X = (byte) (this.X + 1);
            }
            a(0);
            J();
            this.X = (byte) (this.X + 1);
            if (this.X >= eb.m.length) {
                a((byte) 3);
                this.X = 0;
                this.j = 0;
                return;
            }
            return;
        }
        this.Y = 0;
        this.X = 0;
        this.j = 0;
        this.R = 0;
        this.M = true;
        a((byte) 5);
    }

    private void b(int i2) {
        char[] charArray;
        char[] charArray2;
        String str;
        String stringBuffer;
        this.fP = (byte) (this.fP + 1);
        if (this.fP < 26) {
            if (this.dY) {
                switch (this.aa) {
                    case 1:
                        str = "毒";
                        break;
                    case 2:
                        str = "混乱";
                        break;
                    case 3:
                        str = "冰封";
                        break;
                    case 4:
                        str = "诅咒";
                        break;
                    case 5:
                        str = "降低防御";
                        break;
                    default:
                        str = null;
                        break;
                }
                switch (this.au) {
                    case 0:
                        if (i2 != 0) {
                            stringBuffer = new StringBuffer().append(bj.aC[eb.m[this.j]]).append("已中").append(str).toString();
                            break;
                        } else {
                            stringBuffer = new StringBuffer().append(this.aC[this.R]).append("已中").append(str).toString();
                            break;
                        }
                    case 1:
                        if (i2 != 0) {
                            stringBuffer = new StringBuffer().append(bj.aC[eb.m[this.fO]]).append("已中").append(str).toString();
                            break;
                        } else {
                            stringBuffer = new StringBuffer().append(this.aC[this.fO]).append("已中").append(str).toString();
                            break;
                        }
                    default:
                        stringBuffer = null;
                        break;
                }
                ed.cB().a(5, stringBuffer.toCharArray());
            }
            if (this.ae) {
                if (i2 == 0) {
                    charArray = "成功盗取敌人物品".toCharArray();
                    charArray2 = new StringBuffer().append(r.aC[this.S]).append("*").append((int) this.am).toString().toCharArray();
                } else {
                    charArray = new StringBuffer().append(bj.aC[eb.m[this.j]]).append("被盗取物品").toString().toCharArray();
                    charArray2 = new StringBuffer().append(r.aC[this.S]).append("*").append((int) this.am).toString().toCharArray();
                }
                ed.cB().a(charArray, charArray2);
            }
            if (this.dX) {
                ed.cB().a(5, (i2 == 0 ? new StringBuffer().append(this.aC[this.R]).append("中降低防御").toString() : new StringBuffer().append(bj.aC[eb.m[this.j]]).append("中降低防御").toString()).toCharArray());
                return;
            }
            return;
        }
        this.fP = 0;
        ea.cp().ae = false;
        if (i2 == 0) {
            this.W = 2;
        } else {
            this.W = 3;
        }
    }

    private void b(int i2, int i3, int i4) {
        switch (i2) {
            case 0:
                short[] sArr = this.ev[i3];
                sArr[4] = (short) (sArr[4] - i4);
                if (this.ev[i3][4] <= 0) {
                    this.dP[i3][0] = -1;
                    this.p[i3].M = true;
                    for (int i5 = 0; i5 < this.er[i3].length; i5++) {
                        this.er[i3][i5] = 0;
                    }
                    return;
                }
                return;
            case 1:
                byte b = eb.m[i3];
                short[] sArr2 = eb.ab;
                sArr2[b] = (short) (sArr2[b] - i4);
                if (eb.ab[b] < 0) {
                    eb.ab[b] = 0;
                }
                if (eb.ab[b] <= 0) {
                    this.eq[b][0] = -1;
                    for (int i6 = 1; i6 < this.eq[b].length; i6++) {
                        this.eq[b][i6] = 0;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void b(an anVar) {
        switch (this.W) {
            case 0:
                this.aG = new StringBuffer().append(this.aC[this.R]).append("使技能:").append(bj.aE[this.Z]).toString();
                c(anVar, 1);
                return;
            case 1:
                this.p[this.R].a(0);
                this.p[this.R].a(this.p[this.R].eb, this.p[this.R].ec);
                this.ea = false;
                ba();
                b(anVar, this.m[this.Y]);
                this.Y = (byte) (this.Y + 1);
                if (this.Y > this.m.length - 1) {
                    this.Y = 0;
                    this.fG = 0;
                    if (this.at != 3) {
                        switch (this.au) {
                            case 0:
                                if (!this.fX) {
                                    b(1, this.j, this.l);
                                    break;
                                }
                                break;
                            case 1:
                                for (int i2 = 0; i2 < eb.m.length; i2++) {
                                    if (this.eq[eb.m[i2]][0] != -1 && !this.fY[i2]) {
                                        b(1, i2, this.aq[i2]);
                                    }
                                }
                                break;
                        }
                    }
                    this.W = 2;
                    return;
                }
                return;
            case 2:
                this.dY = false;
                if (this.aa != 0) {
                    switch (this.au) {
                        case 0:
                            if (this.eq[eb.m[this.j]][0] != -1 && ee.q(0, 99) < 10 && !b(1, this.j)) {
                                c(1, this.aa, this.j);
                                break;
                            }
                        case 1:
                            int i3 = 0;
                            while (true) {
                                if (i3 >= this.fv.length) {
                                    break;
                                } else {
                                    int q2 = ee.q(0, this.fv.length - 1);
                                    if (this.eq[eb.m[q2]][0] != -1 && ee.q(0, 99) < 10 && !b(1, q2)) {
                                        c(1, this.aa, q2);
                                        this.fO = (byte) q2;
                                        break;
                                    } else {
                                        i3++;
                                    }
                                }
                            }
                            break;
                    }
                }
                this.ae = false;
                this.dX = false;
                switch (this.at) {
                    case 2:
                        r(1);
                        break;
                    case 3:
                        int i4 = this.eb;
                        short s2 = this.U;
                        short[] sArr = this.ev[i4];
                        short[] sArr2 = this.ev[i4];
                        short s3 = (short) (s2 + sArr2[4]);
                        sArr2[4] = s3;
                        sArr[4] = s3 > this.O[i4] ? this.O[i4] : this.ev[i4][4];
                        break;
                    case 4:
                        s(1);
                        break;
                }
                if (this.dY || this.ae || this.dX) {
                    this.W = 4;
                    return;
                } else {
                    this.W = 3;
                    return;
                }
            case 3:
                o();
                return;
            case 4:
                b(1);
                return;
            default:
                return;
        }
    }

    private void b(an anVar, int i2) {
        byte b = 0;
        while (true) {
            byte b2 = b;
            if (b2 < this.fG) {
                switch (this.fF[b2][0]) {
                    case 1:
                        du.a(anVar, bj.I[0], 132, 0, 12, 14, 0, this.fF[b2][2] - 16, this.fF[b2][3] - i2);
                        du.b(anVar, this.fF[b2][1], this.fF[b2][2] - 4, this.fF[b2][3] - i2, 0, 0);
                        break;
                    case 2:
                        du.a(anVar, bj.I[0], MIDIControl.NOTE_ON, 0, 39, 14, 0, this.fF[b2][2] - 17, this.fF[b2][3] - i2);
                        break;
                    case 3:
                        du.a(anVar, bj.I[0], 120, 0, 12, 14, 0, this.fF[b2][2] - 16, this.fF[b2][3] - i2);
                        du.b(anVar, this.fF[b2][1], this.fF[b2][2] - 4, this.fF[b2][3] - i2, 0, 0);
                        break;
                }
                b = (byte) (b2 + 1);
            } else {
                return;
            }
        }
    }

    private boolean b(int i2, int i3) {
        if (i2 == 0) {
            for (int i4 = 1; i4 < this.dP[i3].length; i4++) {
                if (this.dP[i3][i4] == -1) {
                    return true;
                }
            }
        } else {
            for (int i5 = 1; i5 < this.eq[eb.m[i3]].length; i5++) {
                if (this.eq[eb.m[i3]][i5] == -1) {
                    return true;
                }
            }
        }
        return false;
    }

    private void ba() {
        for (int i2 = 0; i2 < this.fv.length; i2++) {
            if (this.eq[eb.m[i2]][0] != -1) {
                if (this.fv[this.j].dO != 3) {
                    this.fv[i2].a(0);
                }
                this.fv[i2].a(this.fv[i2].eb, this.fv[i2].ec);
            }
        }
    }

    private void bb() {
        for (byte b : eb.m) {
            if (this.eq[b][1] == -1) {
                this.ga[b][0] = (short) (eb.ev[b][4] / 10);
                short[] sArr = this.ga[b];
                sArr[1] = (short) (sArr[1] + 1);
                this.gc = true;
            }
        }
        a((byte) 1);
    }

    private void bc() {
        if (bi.y(8)) {
            for (byte b = 0; b < this.fv.length; b = (byte) (b + 1)) {
                byte b2 = (byte) (this.j + 1);
                this.j = b2;
                this.j = b2 > ((byte) (this.fv.length - 1)) ? 0 : this.j;
                if (this.eq[eb.m[this.j]][0] != -1) {
                    this.aG = bj.aC[eb.m[this.j]];
                    return;
                }
            }
        } else if (bi.y(4)) {
            for (byte b3 = 0; b3 < this.fv.length; b3 = (byte) (b3 + 1)) {
                byte b4 = (byte) (this.j - 1);
                this.j = b4;
                this.j = b4 < 0 ? (byte) (this.fv.length - 1) : this.j;
                if (this.eq[eb.m[this.j]][0] != -1) {
                    this.aG = bj.aC[eb.m[this.j]];
                    return;
                }
            }
        } else if (bi.y(16)) {
            a((byte) 2);
            v(this.ay[0]);
        }
    }

    private void bd() {
        if (this.fJ && bi.y(8)) {
            this.fJ = false;
            v(this.ay[0]);
        }
        if (!this.fJ) {
            if (bi.y(4)) {
                this.fJ = true;
                v(5);
            } else if (bi.y(2)) {
                this.fH = this.ay[0];
                for (int i2 = 0; i2 < this.ay.length; i2++) {
                    if (i2 < this.ay.length - 1) {
                        this.ay[i2] = this.ay[i2 + 1];
                    } else {
                        this.ay[i2] = this.fH;
                    }
                }
                v(this.ay[0]);
            } else if (bi.y(1)) {
                this.fH = this.ay[this.ay.length - 1];
                for (int length = this.ay.length - 1; length >= 0; length--) {
                    if (length > 0) {
                        this.ay[length] = this.ay[length - 1];
                    } else {
                        this.ay[length] = this.fH;
                    }
                }
                v(this.ay[0]);
            }
        }
        if (bi.y(16)) {
            switch (this.fI) {
                case 0:
                    if (as > 0) {
                        this.fQ = 1;
                    } else if (ee.q(0, 99) < 40) {
                        this.fQ = 2;
                    } else {
                        this.fQ = 1;
                    }
                    a(8);
                    return;
                case 1:
                    bi();
                    a.M().ah = 1;
                    a.M();
                    a.P();
                    a(5);
                    return;
                case 2:
                    a.M().S();
                    a.ak = 0;
                    a.aj = 0;
                    a.al = 3;
                    byte[] bArr = bj.es[bj.es.length - 1];
                    a.N = bArr;
                    a.P = (short) bArr[0];
                    a.av = new short[a.N.length];
                    for (int i3 = 0; i3 < a.av.length; i3++) {
                        a.av[i3] = r.ev[a.N[i3]][6];
                    }
                    a.am = (byte) a.N.length;
                    a(7);
                    return;
                case 3:
                    this.fT = true;
                    d();
                    bi();
                    a.M().ah = 1;
                    a.M();
                    a.a((int) eb.m[this.j]);
                    a(3);
                    return;
                case 4:
                    this.fT = false;
                    this.fS = false;
                    a(2);
                    return;
                case 5:
                    bi.ac = 0;
                    if (as < 2) {
                        this.at = 0;
                        this.aa = 0;
                        b.Z().b(b.Z().az, 2);
                        ea.cp().a(7);
                        return;
                    }
                    ed.cB().a(5, "此次战斗不可使用".toCharArray());
                    a(9);
                    return;
                default:
                    return;
            }
        } else if (bi.y(1280)) {
            a(1);
        }
    }

    private void be() {
        if (!this.fS) {
            if (bi.y(8)) {
                byte b = 0;
                while (true) {
                    if (b >= this.p.length) {
                        break;
                    }
                    byte b2 = (byte) (this.R + 1);
                    this.R = b2;
                    this.R = b2 > ((byte) (this.p.length - 1)) ? 0 : this.R;
                    if (this.dP[this.R][0] != -1) {
                        t(this.R);
                        break;
                    }
                    b = (byte) (b + 1);
                }
            } else if (bi.y(4)) {
                byte b3 = 0;
                while (true) {
                    if (b3 >= this.p.length) {
                        break;
                    }
                    byte b4 = (byte) (this.R - 1);
                    this.R = b4;
                    this.R = b4 < 0 ? (byte) (this.p.length - 1) : this.R;
                    if (this.dP[this.R][0] != -1) {
                        t(this.R);
                        break;
                    }
                    b3 = (byte) (b3 + 1);
                }
            }
        }
        if (bi.y(16)) {
            this.fG = 0;
            if (!this.fT) {
                int i2 = 0;
                if (this.eq[eb.m[this.j]][2] == -1) {
                    if (ee.q(0, 99) < 20) {
                        i2 = 1;
                    }
                    byte[] bArr = this.aw;
                    byte b5 = eb.m[this.j];
                    bArr[b5] = (byte) (bArr[b5] + 1);
                    if (this.aw[eb.m[this.j]] == 3) {
                        this.aw[eb.m[this.j]] = 0;
                        this.eq[eb.m[this.j]][2] = 0;
                    }
                }
                a(this.fT, this.j, this.R, 0, 0, i2);
                int i3 = !this.fX ? 1 : 2;
                if (!this.fZ) {
                    a(i3, this.l, this.p[this.R].aH - 8, this.p[this.R].dN - 30);
                } else {
                    a(i3, this.l, this.fv[this.an].aH - 8, this.fv[this.an].dN - 30);
                }
                a(4);
                return;
            }
            bf();
            a(this.fT, this.j, this.R, this.at, this.au, 0);
            switch (this.au) {
                case 0:
                    a(!this.fX ? 1 : 2, this.l, this.p[this.R].aH - 8, this.p[this.R].dN - 30);
                    break;
                case 1:
                    for (int i4 = 0; i4 < this.fY.length; i4++) {
                        if (this.dP[i4][0] != -1) {
                            a(!this.fY[i4] ? 1 : 2, this.aq[i4], this.p[i4].aH - 8, this.p[i4].dN - 30);
                        }
                    }
                    break;
            }
            a(6);
        } else if (!bi.y(1280)) {
        } else {
            if (!this.fT) {
                a(0);
            } else {
                a(3);
            }
        }
    }

    private void bf() {
        byte b = eb.m[this.j];
        short[] sArr = eb.aq;
        sArr[b] = (short) (sArr[b] - this.dO);
        if (eb.e(b, a.N[a.h], 0) < eb.e) {
            eb.b(b, a.N[a.h], 1);
        }
    }

    private void bg() {
        switch (this.W) {
            case 0:
                a.M();
                a.e(a.N.length - 1);
                this.Z = (byte) a.P;
                eb.a(eb.m[this.j], this.Z);
                if (bi.y(1280)) {
                    a(0);
                    bi();
                    a.M().ah = 0;
                    return;
                } else if (!bi.y(16)) {
                    return;
                } else {
                    if (a.am < 3) {
                        switch (a.am) {
                            case 1:
                                if (a.l == 0) {
                                    bh();
                                    return;
                                }
                                return;
                            case 2:
                                if (a.l < 2) {
                                    bh();
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    } else {
                        bh();
                        return;
                    }
                }
            case 1:
                if (bi.y(16)) {
                    a.M().c(1);
                    for (int i2 = 0; i2 < eb.m.length; i2++) {
                        byte b = eb.m[i2];
                        if (this.eq[b][0] != -1 && eb.ab[b] < eb.ev[b][4]) {
                            a(3, a.M().ab[i2], this.fv[i2].aH - 10, this.fv[i2].dN - 50);
                        }
                    }
                    this.W = 2;
                    return;
                } else if (bi.y(1280)) {
                    a.M().af = 0;
                    this.W = 0;
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void bh() {
        eb.a(eb.m[this.j], a.N[a.h]);
        this.dO = eb.ar[6];
        if (eb.aq[eb.m[this.j]] >= this.dO) {
            this.at = (byte) eb.ar[0];
            this.aa = (byte) eb.ar[4];
            aV();
            switch (this.at) {
                case 0:
                case 1:
                case 2:
                case 4:
                    this.au = (byte) eb.ar[3];
                    switch (this.au) {
                        case 0:
                            this.fS = false;
                            break;
                        case 1:
                            this.fS = true;
                            break;
                    }
                    a(2);
                    return;
                case 3:
                    this.W = 1;
                    return;
                default:
                    return;
            }
        } else {
            this.W = 4;
        }
    }

    private static void bi() {
        a.h = 0;
        a.l = 0;
        a.k = 0;
        a.aj = 0;
        a.ak = 0;
    }

    private void bj() {
        switch (this.W) {
            case 0:
                a.M();
                a.d(a.N.length - 1);
                if (bi.y(1280)) {
                    a(0);
                    bi();
                    a.M().ah = 0;
                    return;
                } else if (bi.y(16) && a.O[a.h] > 0) {
                    byte b = (byte) r.ev[a.P][5];
                    a.Z = b;
                    if (b != -1) {
                        a.W = (byte) r.ev[a.P][4];
                        a.Y = (byte) r.ev[a.P][1];
                        this.ap = this.j;
                        this.W = 1;
                        return;
                    }
                    return;
                } else {
                    return;
                }
            case 1:
                if (a.W == 0) {
                    if (bi.y(8)) {
                        byte b2 = 0;
                        while (b2 < eb.m.length) {
                            byte b3 = (byte) (this.ap + 1);
                            this.ap = b3;
                            this.ap = b3 > ((byte) (eb.m.length - 1)) ? 0 : this.ap;
                            if (a.Y != 7 && this.eq[eb.m[this.ap]][0] == -1) {
                                b2 = (byte) (b2 + 1);
                            }
                        }
                    } else if (bi.y(4)) {
                        byte b4 = 0;
                        while (b4 < this.fv.length) {
                            byte b5 = (byte) (this.ap - 1);
                            this.ap = b5;
                            this.ap = b5 < 0 ? (byte) (eb.m.length - 1) : this.ap;
                            if (a.Y != 7 && this.eq[eb.m[this.ap]][0] == -1) {
                                b4 = (byte) (b4 + 1);
                            }
                        }
                    }
                }
                if (bi.y(16)) {
                    a.M().b(this.ap);
                    this.W = 2;
                    return;
                } else if (bi.y(1280)) {
                    this.W = 0;
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void bk() {
        short s2;
        switch (this.f) {
            case 0:
                for (int i2 = 0; i2 < this.p.length; i2++) {
                    if (this.dP[i2][1] == -1) {
                        byte[] bArr = this.er[i2];
                        bArr[0] = (byte) (bArr[0] + 1);
                        this.T = (short) (this.O[i2] / 10);
                        this.T = this.T <= 0 ? 10 : this.T;
                        this.gc = true;
                    }
                }
                u(1);
                return;
            case 1:
                if (!this.gc) {
                    this.R = (byte) ee.q(0, this.p.length - 1);
                    for (byte b = 0; b < this.p.length; b = (byte) (b + 1)) {
                        byte b2 = (byte) (this.R + 1);
                        this.R = b2;
                        this.R = b2 > ((byte) (this.p.length - 1)) ? 0 : this.R;
                        if (this.dP[this.R][0] != -1) {
                            u(2);
                            return;
                        }
                    }
                    return;
                }
                return;
            case 2:
                if (this.dP[this.R][3] != -1) {
                    this.j = (byte) ee.q(0, this.fv.length - 1);
                    int i3 = 0;
                    while (i3 < this.fv.length) {
                        byte b3 = (byte) (this.j + 1);
                        this.j = b3;
                        this.j = b3 > ((byte) (this.fv.length - 1)) ? 0 : this.j;
                        if (this.eq[eb.m[this.j]][0] == -1) {
                            i3++;
                        } else {
                            u(3);
                            return;
                        }
                    }
                    u(3);
                    return;
                }
                this.gc = true;
                return;
            case 3:
                this.at = 0;
                this.aa = 0;
                this.au = 0;
                this.gd = (byte) ee.q(0, 3);
                switch (this.gd) {
                    case 0:
                        f(this.R, this.j, 0, (this.dP[this.R][2] != -1 || ee.q(0, 99) >= 50) ? 0 : 1);
                        u(4);
                        return;
                    default:
                        this.fZ = false;
                        switch (this.gd) {
                            case 1:
                                s2 = this.ev[this.R][11];
                                this.Z = (byte) this.ev[this.R][10];
                                break;
                            case 2:
                                s2 = this.ev[this.R][13];
                                this.Z = (byte) this.ev[this.R][12];
                                break;
                            case 3:
                                s2 = this.ev[this.R][15];
                                this.Z = (byte) this.ev[this.R][14];
                                break;
                            default:
                                s2 = 0;
                                break;
                        }
                        this.at = (byte) bj.fz[this.Z][1];
                        this.aa = (byte) bj.fz[this.Z][5];
                        this.au = (byte) bj.fz[this.Z][4];
                        if (s2 == 0) {
                            f(this.R, this.j, 0, 0);
                            u(4);
                            return;
                        } else if (ee.q(0, 99) > s2) {
                            d();
                            aV();
                            if (this.at != 3) {
                                f(this.R, this.j, 1, 0);
                            } else if (this.at == 3) {
                                this.au = 0;
                                if (bl()) {
                                    this.eb = 0;
                                    int i4 = 0;
                                    while (i4 < this.p.length) {
                                        int i5 = this.eb + 1;
                                        this.eb = i5;
                                        this.eb = i5 > this.p.length - 1 ? 0 : this.eb;
                                        if (this.dP[this.eb][0] == -1 || this.ev[this.eb][4] > this.O[this.eb]) {
                                            i4++;
                                        } else {
                                            this.U = (short) ((((short) (bj.fz[this.Z][11] * 2)) * this.O[this.eb]) / 100);
                                            a(3, this.U, this.p[this.eb].aH - 15, this.p[this.eb].dN - 10);
                                        }
                                    }
                                    this.U = (short) ((((short) (bj.fz[this.Z][11] * 2)) * this.O[this.eb]) / 100);
                                    a(3, this.U, this.p[this.eb].aH - 15, this.p[this.eb].dN - 10);
                                } else {
                                    f(this.R, this.j, 0, 0);
                                    u(4);
                                }
                            }
                            if (this.at != 3) {
                                u(5);
                                return;
                            } else if (bl()) {
                                u(5);
                                return;
                            } else {
                                return;
                            }
                        } else {
                            f(this.R, this.j, 0, 0);
                            u(4);
                            return;
                        }
                }
            default:
                return;
        }
    }

    private boolean bl() {
        for (int i2 = 0; i2 < this.p.length; i2++) {
            if (this.dP[i2][0] != -1 && this.ev[i2][4] < this.O[i2] / 2) {
                return true;
            }
        }
        return false;
    }

    private void bm() {
        this.q = ee.O("/u/fa");
        this.r = ee.O("/u/buff");
        this.v = ee.O(new StringBuffer().append("/u/").append((int) bj.e).append(AdActivity.TYPE_PARAM).toString());
        this.o = c.g("/u/bj");
        this.J = new ao[3];
        for (int i2 = 0; i2 < this.J.length; i2++) {
            this.J[i2] = ee.O(new StringBuffer().append("/u/f").append(i2).toString());
        }
        this.u = ee.O("/u/prar");
        this.fC = new n[2];
        for (int i3 = 0; i3 < this.fC.length; i3++) {
            this.fC[i3] = c.g(new StringBuffer().append("/u/fh").append(i3).toString());
        }
    }

    private void bn() {
        byte b;
        boolean z;
        if (this.ge == 0) {
            byte q2 = (byte) ee.q(0, 100);
            switch (eb.m.length) {
                case 1:
                    if (q2 >= 70) {
                        this.gg = 2;
                        break;
                    } else {
                        this.gg = 1;
                        break;
                    }
                case 2:
                    if (q2 >= 10) {
                        if (q2 >= 60) {
                            this.gg = 3;
                            break;
                        } else {
                            this.gg = 2;
                            break;
                        }
                    } else {
                        this.gg = 1;
                        break;
                    }
                case 3:
                    if (q2 >= 5) {
                        if (q2 >= 25) {
                            this.gg = 3;
                            break;
                        } else {
                            this.gg = 2;
                            break;
                        }
                    } else {
                        this.gg = 1;
                        break;
                    }
            }
            this.gh = dw.ak;
        }
        this.p = new c[this.gg];
        this.fz = (short[][]) Array.newInstance(Short.TYPE, this.gg, 2);
        this.dP = (byte[][]) Array.newInstance(Byte.TYPE, this.gg, 6);
        this.er = (byte[][]) Array.newInstance(Byte.TYPE, this.gg, 5);
        this.ev = (short[][]) Array.newInstance(Short.TYPE, this.gg, 17);
        this.aC = new String[this.gg];
        this.O = new short[this.gg];
        for (byte b2 = 0; b2 < this.gg; b2 = (byte) (b2 + 1)) {
            this.fz[b2][0] = (short) (this.dQ[this.gg - 1][b2][0] + 120);
            this.fz[b2][1] = (short) (this.dQ[this.gg - 1][b2][1] + 160);
            if (this.ge != 0) {
                this.ar = new short[this.gg];
                switch (this.gg) {
                    case 1:
                        this.ar[0] = (short) this.gf;
                        b = 0;
                        break;
                    case 2:
                        this.ar[0] = (short) this.gf;
                        this.ar[1] = (short) this.gh;
                        b = 0;
                        break;
                    case 3:
                        this.ar[0] = (short) this.gh;
                        this.ar[1] = (short) this.gf;
                        this.ar[2] = (short) this.gh;
                    default:
                        b = 0;
                        break;
                }
            } else {
                b = (byte) ee.q(this.gh - 1, this.gh + 1);
                if (b < 0) {
                    b = 0;
                }
            }
            if (this.ge == 0) {
                for (int i2 = 0; i2 < bj.fF[b].length; i2++) {
                    this.ev[b2][i2] = bj.fF[b][i2];
                }
                gi = 3;
            } else {
                switch (this.gg) {
                    case 1:
                        for (int i3 = 0; i3 < bj.fF[this.ar[0]].length; i3++) {
                            this.ev[b2][i3] = bj.fF[this.ar[0]][i3];
                        }
                        break;
                    case 2:
                        for (int i4 = 0; i4 < bj.fF[this.ar[b2]].length; i4++) {
                            this.ev[b2][i4] = bj.fF[this.ar[b2]][i4];
                        }
                        break;
                    case 3:
                        for (int i5 = 0; i5 < bj.fF[this.ar[b2]].length; i5++) {
                            this.ev[b2][i5] = bj.fF[this.ar[b2]][i5];
                        }
                        break;
                }
                gi = 2;
            }
            this.O[b2] = this.ev[b2][4];
            byte b3 = 0;
            while (true) {
                if (b3 >= b2) {
                    z = false;
                } else if (this.ev[b2][16] == this.ev[b3][16]) {
                    this.p[b2] = n.a(this.p[b3]);
                    this.p[b2].k = 0;
                    z = true;
                } else {
                    b3 = (byte) (b3 + 1);
                }
            }
            if (!z) {
                this.p[b2] = c.g(new StringBuffer().append("/u/npc").append((int) this.ev[b2][16]).toString());
                this.p[b2].k = 0;
            }
            this.p[b2].a(this.fz[b2][0], this.fz[b2][1]);
            this.p[b2].g(this.fz[b2][0], this.fz[b2][1]);
            if (this.ge == 0) {
                this.aC[b2] = bj.gu[b];
            } else {
                this.aC[b2] = bj.gu[this.ar[b2]];
            }
            this.p[b2].a(0);
        }
    }

    public static void c(int i2) {
        eb.cq();
        eb.c(i2);
        eb.ab[i2] = eb.ev[i2][4];
        eb.aq[i2] = eb.ev[i2][3];
    }

    private void c(int i2, int i3, int i4) {
        switch (i3) {
            case 1:
                if (i2 != 0) {
                    this.eq[eb.m[i4]][1] = -1;
                    break;
                } else {
                    this.dP[i4][1] = -1;
                    break;
                }
            case 2:
                if (i2 != 0) {
                    this.eq[eb.m[i4]][2] = -1;
                    break;
                } else {
                    this.dP[i4][2] = -1;
                    break;
                }
            case 3:
                if (i2 != 0) {
                    this.eq[eb.m[i4]][3] = -1;
                    break;
                } else {
                    this.dP[i4][3] = -1;
                    break;
                }
            case 4:
                if (i2 != 0) {
                    this.eq[eb.m[i4]][4] = -1;
                    break;
                } else {
                    this.dP[i4][4] = -1;
                    break;
                }
        }
        this.dY = true;
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [bg] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void c(defpackage.an r11, int r12) {
        /*
            r10 = this;
            r9 = 6
            r7 = 3
            r6 = 2
            r5 = 0
            r8 = 1
            byte r0 = r10.Z
            switch(r0) {
                case 0: goto L_0x000b;
                case 1: goto L_0x0189;
                case 2: goto L_0x03e0;
                case 3: goto L_0x04a6;
                case 4: goto L_0x05cf;
                case 5: goto L_0x06ba;
                case 6: goto L_0x07b6;
                case 7: goto L_0x0904;
                case 8: goto L_0x0960;
                case 9: goto L_0x0a0e;
                case 10: goto L_0x05cf;
                case 11: goto L_0x0b7f;
                case 12: goto L_0x0e00;
                case 13: goto L_0x0e81;
                case 14: goto L_0x0f62;
                case 15: goto L_0x1028;
                case 16: goto L_0x05cf;
                case 17: goto L_0x06ba;
                case 18: goto L_0x102d;
                case 19: goto L_0x1032;
                case 20: goto L_0x1037;
                case 21: goto L_0x103c;
                case 22: goto L_0x1041;
                case 23: goto L_0x1046;
                case 24: goto L_0x104b;
                case 25: goto L_0x1050;
                case 26: goto L_0x1055;
                default: goto L_0x000a;
            }
        L_0x000a:
            return
        L_0x000b:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0011;
                case 1: goto L_0x005f;
                case 2: goto L_0x00b3;
                case 3: goto L_0x0174;
                default: goto L_0x0010;
            }
        L_0x0010:
            goto L_0x000a
        L_0x0011:
            if (r12 != 0) goto L_0x0039
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r1 = 5
            r0.a(r1)
        L_0x0036:
            r10.ah = r8
            goto L_0x000a
        L_0x0039:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r8)
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            r2 = 100
            int r1 = r1 - r2
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            goto L_0x0036
        L_0x005f:
            if (r12 != 0) goto L_0x009a
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r5)
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.eb
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.ec
            r0.a(r1, r2)
            r10.ah = r6
            goto L_0x000a
        L_0x009a:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r5]
            r1 = 10
            r0.a(r1)
            r10.ah = r6
            goto L_0x000a
        L_0x00b3:
            if (r12 != 0) goto L_0x010b
            n[] r0 = r10.fw
            r0 = r0[r5]
            int r1 = r10.ac
            int r1 = r1 + 120
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            int r0 = r10.ac
            r1 = 20
            int r0 = r0 - r1
            r10.ac = r0
            int r0 = r10.ac
            int r0 = r0 + 120
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            if (r0 >= r1) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r5]
            r1 = 7
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 5
            r0.a(r1, r2)
            r10.dZ = r8
            r10.ah = r7
            goto L_0x000a
        L_0x010b:
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            r2 = 100
            int r1 = r1 - r2
            int r2 = r10.ac
            int r1 = r1 + r2
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            int r0 = r10.ac
            int r0 = r0 + 20
            r10.ac = r0
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            int r0 = r0.aH
            r1 = 100
            int r0 = r0 - r1
            int r1 = r10.ac
            int r0 = r0 + r1
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            if (r0 <= r1) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r5]
            r1 = 7
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 5
            r0.a(r1, r2)
            r10.ea = r8
            r10.ah = r7
            goto L_0x000a
        L_0x0174:
            n[] r0 = r10.fw
            r0 = r0[r5]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            goto L_0x000a
        L_0x0189:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0190;
                case 1: goto L_0x02d3;
                default: goto L_0x018e;
            }
        L_0x018e:
            goto L_0x000a
        L_0x0190:
            if (r12 != 0) goto L_0x0240
            byte[] r0 = defpackage.eb.m
            byte r1 = r10.j
            byte r0 = r0[r1]
            switch(r0) {
                case 0: goto L_0x01c2;
                default: goto L_0x019b;
            }
        L_0x019b:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.c(r9)
        L_0x01a4:
            r0 = r5
        L_0x01a5:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x01ce
            n[] r0 = r10.fy
            int r0 = r0.length
            int r0 = r0 / 2
        L_0x01b1:
            n[] r1 = r10.fy
            int r1 = r1.length
            if (r0 < r1) goto L_0x0212
            r0 = 4
            byte[] r0 = new byte[r0]
            r0 = {-1, 1, -1, 1} // fill-array
            r10.L = r0
        L_0x01be:
            r10.ah = r8
            goto L_0x000a
        L_0x01c2:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r1 = 8
            r0.c(r1)
            goto L_0x01a4
        L_0x01ce:
            r1 = 20
            r2 = 110(0x6e, float:1.54E-43)
            int r1 = defpackage.ee.q(r1, r2)
            r2 = -10
            r3 = 40
            int r2 = defpackage.ee.q(r2, r3)
            r3 = 120(0x78, float:1.68E-43)
            aU()
            r4 = 250(0xfa, float:3.5E-43)
            int r3 = defpackage.ee.q(r3, r4)
            n[] r4 = r10.fy
            r4 = r4[r0]
            n[] r5 = r10.fv
            byte r6 = r10.j
            r5 = r5[r6]
            int r5 = r5.aH
            int r2 = r2 + r5
            r4.j(r2, r3)
            n[] r2 = r10.fy
            r2 = r2[r0]
            r2.k(r1, r3)
            r1 = 15
            r2 = 20
            int r1 = defpackage.ee.q(r1, r2)
            n[] r2 = r10.fy
            r2 = r2[r0]
            r2.e(r1)
            int r0 = r0 + 1
            goto L_0x01a5
        L_0x0212:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r2 = 8
            r1.a(r2)
            n[] r1 = r10.fy
            r1 = r1[r0]
            n[] r2 = r10.fy
            n[] r3 = r10.fy
            int r3 = r3.length
            int r3 = r3 / 2
            int r3 = r0 - r3
            r2 = r2[r3]
            int r2 = r2.eh
            n[] r3 = r10.fy
            n[] r4 = r10.fy
            int r4 = r4.length
            int r4 = r4 / 2
            int r4 = r0 - r4
            r3 = r3[r4]
            int r3 = r3.ei
            r1.a(r2, r3)
            int r0 = r0 + 1
            goto L_0x01b1
        L_0x0240:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r8)
            r0 = r5
        L_0x024a:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x0288
            n[] r0 = r10.fy
            int r0 = r0.length
            int r0 = r0 / 2
        L_0x0256:
            n[] r1 = r10.fy
            int r1 = r1.length
            if (r0 >= r1) goto L_0x01be
            n[] r1 = r10.fy
            r1 = r1[r0]
            r2 = 8
            r1.a(r2)
            n[] r1 = r10.fy
            r1 = r1[r0]
            n[] r2 = r10.fy
            n[] r3 = r10.fy
            int r3 = r3.length
            int r3 = r3 / 2
            int r3 = r0 - r3
            r2 = r2[r3]
            int r2 = r2.eh
            n[] r3 = r10.fy
            n[] r4 = r10.fy
            int r4 = r4.length
            int r4 = r4 / 2
            int r4 = r0 - r4
            r3 = r3[r4]
            int r3 = r3.ei
            r1.a(r2, r3)
            int r0 = r0 + 1
            goto L_0x0256
        L_0x0288:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r2 = 26
            r1.a(r2)
            r1 = 180(0xb4, float:2.52E-43)
            r2 = 220(0xdc, float:3.08E-43)
            int r1 = defpackage.ee.q(r1, r2)
            r2 = -70
            r3 = 30
            int r2 = defpackage.ee.q(r2, r3)
            r3 = 100
            r4 = 240(0xf0, float:3.36E-43)
            int r3 = defpackage.ee.q(r3, r4)
            n[] r4 = r10.fy
            r4 = r4[r0]
            n[] r5 = r10.p
            byte r6 = r10.R
            r5 = r5[r6]
            int r5 = r5.aH
            int r2 = r2 + r5
            r4.j(r2, r3)
            n[] r2 = r10.fy
            r2 = r2[r0]
            r2.k(r1, r3)
            r1 = 60
            r2 = 65
            int r1 = defpackage.ee.q(r1, r2)
            n[] r2 = r10.fy
            r2 = r2[r0]
            r2.e(r1)
            int r0 = r0 + 1
            goto L_0x024a
        L_0x02d3:
            r0 = r5
        L_0x02d4:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x02fd
            r0 = r5
        L_0x02dc:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x03b1
            boolean r0 = r10.W()
            if (r0 == 0) goto L_0x000a
            byte r0 = r10.ak
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.ak = r0
            byte r0 = r10.ak
            if (r0 <= r7) goto L_0x000a
            r10.ak = r5
            r10.W = r8
            r0 = 0
            r10.fy = r0
            goto L_0x000a
        L_0x02fd:
            n[] r1 = r10.fy
            r1 = r1[r0]
            boolean r1 = r1.M
            if (r1 != 0) goto L_0x031c
            if (r12 != 0) goto L_0x03a4
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.f(r5)
        L_0x030e:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.c(r11)
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.Q()
        L_0x031c:
            n[] r1 = r10.fy
            r1 = r1[r0]
            boolean r1 = r1.am()
            if (r1 == 0) goto L_0x03a0
            r10.af = r8
            if (r12 != 0) goto L_0x03ad
            r10.dZ = r8
        L_0x032c:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.a(r5)
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.em
            if (r1 != 0) goto L_0x034d
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.em = r8
        L_0x034d:
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.em
            if (r1 == 0) goto L_0x0383
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.M
            if (r1 != 0) goto L_0x0383
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.c(r11)
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.Q()
        L_0x0383:
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.W()
            if (r1 == 0) goto L_0x03a0
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.a(r5)
        L_0x03a0:
            int r0 = r0 + 1
            goto L_0x02d4
        L_0x03a4:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.f(r8)
            goto L_0x030e
        L_0x03ad:
            r10.ea = r8
            goto L_0x032c
        L_0x03b1:
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.em
            if (r1 == 0) goto L_0x03dc
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.W()
            if (r1 == 0) goto L_0x03dc
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.a(r5)
        L_0x03dc:
            int r0 = r0 + 1
            goto L_0x02dc
        L_0x03e0:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x03e7;
                case 1: goto L_0x040a;
                case 2: goto L_0x0434;
                case 3: goto L_0x0491;
                default: goto L_0x03e5;
            }
        L_0x03e5:
            goto L_0x000a
        L_0x03e7:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r1 = 140(0x8c, float:1.96E-43)
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 10
            r0.a(r1, r2)
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r1 = 7
            r0.a(r1)
            r10.ah = r8
            goto L_0x000a
        L_0x040a:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            int r0 = r0.ac
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            byte[][] r1 = r1.dR
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dO
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 7
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r8)
            r10.ah = r6
            goto L_0x000a
        L_0x0434:
            n[] r0 = r10.fw
            r0 = r0[r8]
            int r1 = r10.ac
            int r1 = r1 + 90
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r3 = 16
            int r2 = r2 - r3
            r0.a(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r8)
            int r0 = r10.ac
            r1 = 15
            int r0 = r0 - r1
            r10.ac = r0
            n[] r0 = r10.fw
            r0 = r0[r8]
            int r0 = r0.aH
            int r1 = r10.ac
            int r0 = r0 + r1
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            if (r0 >= r1) goto L_0x000a
            r10.dZ = r8
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r6)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 5
            r0.a(r1, r2)
            r10.ah = r7
            goto L_0x000a
        L_0x0491:
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r5)
            goto L_0x000a
        L_0x04a6:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x04ad;
                case 1: goto L_0x050b;
                case 2: goto L_0x0522;
                case 3: goto L_0x0566;
                case 4: goto L_0x05b3;
                default: goto L_0x04ab;
            }
        L_0x04ab:
            goto L_0x000a
        L_0x04ad:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r6)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r7)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r8)
            byte[] r0 = new byte[r6]
            r0 = {-5, 5} // fill-array
            r10.L = r0
            r10.ah = r8
            goto L_0x000a
        L_0x050b:
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.ah = r6
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 25
            r0.a(r1)
            goto L_0x000a
        L_0x0522:
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            int r1 = r1 + 1
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r3 = 35
            int r2 = r2 - r3
            int r3 = r10.dN
            int r2 = r2 + r3
            r0.a(r1, r2)
            int r0 = r10.dN
            r1 = 30
            int r0 = r0 - r1
            r10.dN = r0
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            int r0 = r0.dN
            r1 = 35
            int r0 = r0 - r1
            int r1 = r10.dN
            int r0 = r0 + r1
            if (r0 >= 0) goto L_0x000a
            r10.ah = r7
            r10.dN = r5
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 4
            r0.a(r1)
            goto L_0x000a
        L_0x0566:
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            int r2 = r10.dN
            r0.a(r1, r2)
            int r0 = r10.dN
            int r0 = r0 + 30
            r10.dN = r0
            int r0 = r10.dN
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.dN
            if (r0 <= r1) goto L_0x000a
            r0 = 4
            r10.ah = r0
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 5
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 1
            r0.a(r1, r2)
            r10.dZ = r8
            r10.af = r6
            goto L_0x000a
        L_0x05b3:
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r5)
            goto L_0x000a
        L_0x05cf:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x05d6;
                case 1: goto L_0x05f6;
                default: goto L_0x05d4;
            }
        L_0x05d4:
            goto L_0x000a
        L_0x05d6:
            boolean r0 = r10.gl
            if (r0 != 0) goto L_0x05e3
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
        L_0x05e3:
            r10.ah = r8
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r8)
            r0 = 4
            byte[] r0 = new byte[r0]
            r0 = {-3, 3, -2, 1} // fill-array
            r10.L = r0
            goto L_0x000a
        L_0x05f6:
            r10.fE = r8
            n[] r0 = r10.fw
            r0 = r0[r6]
            int r1 = r10.aH
            int r1 = r1 + 230
            r2 = 190(0xbe, float:2.66E-43)
            r0.a(r1, r2)
            byte[] r0 = r10.N
            byte r0 = r0[r5]
            if (r0 != 0) goto L_0x0612
            n[] r0 = r10.fw
            r0 = r0[r6]
            r0.a(r8)
        L_0x0612:
            byte[] r0 = r10.N
            byte r0 = r0[r8]
            if (r0 != 0) goto L_0x061f
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r8)
        L_0x061f:
            n[] r0 = r10.fy
            r0 = r0[r5]
            int r1 = r10.ac
            int r1 = r1 + 220
            r2 = 220(0xdc, float:3.08E-43)
            r0.a(r1, r2)
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.c(r11)
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.Q()
            boolean r0 = r10.em
            if (r0 != 0) goto L_0x0645
            int r0 = r10.aH
            short r1 = r10.P
            int r0 = r0 - r1
            r10.aH = r0
        L_0x0645:
            boolean r0 = r10.aT
            if (r0 != 0) goto L_0x066a
            int r0 = r10.aH
            int r0 = r0 + 230
            r1 = 120(0x78, float:1.68E-43)
            if (r0 >= r1) goto L_0x066a
            r10.af = r8
            r10.em = r8
            byte r0 = r10.aj
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.aj = r0
            byte r0 = r10.aj
            r1 = 8
            if (r0 <= r1) goto L_0x066a
            r0 = 30
            r10.P = r0
            r10.em = r5
            r10.aT = r8
        L_0x066a:
            int r0 = r10.ac
            r1 = 30
            int r0 = r0 - r1
            r10.ac = r0
            int r0 = r10.aH
            int r0 = r0 + 230
            if (r0 >= 0) goto L_0x0682
            byte[] r0 = r10.N
            r0[r5] = r8
            n[] r0 = r10.fw
            r0 = r0[r6]
            r0.a(r5)
        L_0x0682:
            int r0 = r10.ac
            int r0 = r0 + 220
            if (r0 >= 0) goto L_0x0695
            r10.dZ = r8
            byte[] r0 = r10.N
            r0[r8] = r8
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r5)
        L_0x0695:
            byte[] r0 = r10.N
            byte r0 = r0[r5]
            if (r0 != r8) goto L_0x000a
            byte[] r0 = r10.N
            byte r0 = r0[r8]
            if (r0 != r8) goto L_0x000a
            r10.W = r8
            r0 = 0
            r10.fy = r0
            byte[] r0 = new byte[r6]
            r10.N = r0
            r10.fE = r5
            r10.em = r5
            r10.aT = r5
            r0 = 20
            r10.P = r0
            r10.aj = r5
            r10.gl = r5
            goto L_0x000a
        L_0x06ba:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x06c1;
                case 1: goto L_0x0732;
                case 2: goto L_0x0776;
                default: goto L_0x06bf;
            }
        L_0x06bf:
            goto L_0x000a
        L_0x06c1:
            byte[] r0 = defpackage.eb.m
            byte r1 = r10.j
            byte r0 = r0[r1]
            switch(r0) {
                case 0: goto L_0x0726;
                default: goto L_0x06ca;
            }
        L_0x06ca:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
        L_0x06d3:
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r6)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n r0 = defpackage.n.a(r0)
            r10.n = r0
            n r0 = r10.n
            r1 = 7
            r0.a(r1)
            n r0 = r10.n
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            int r1 = r1 + 10
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r3 = 20
            int r2 = r2 - r3
            r0.a(r1, r2)
            r10.ah = r8
            goto L_0x000a
        L_0x0726:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r1 = 8
            r0.a(r1)
            goto L_0x06d3
        L_0x0732:
            n r0 = r10.n
            r0.c(r11)
            n r0 = r10.n
            r0.Q()
            n r0 = r10.n
            int r0 = r0.ac
            r1 = 4
            if (r0 <= r1) goto L_0x0745
            r10.dZ = r8
        L_0x0745:
            n r0 = r10.n
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r7)
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fy
            r0 = r0[r5]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r0.a(r1, r2)
            r10.ah = r6
            goto L_0x000a
        L_0x0776:
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.c(r11)
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.Q()
            n r0 = r10.n
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n r0 = r10.n
            r0.a(r5)
            byte r0 = r10.ak
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.ak = r0
            byte r0 = r10.ak
            if (r0 <= r7) goto L_0x000a
            r10.W = r8
            r10.ak = r5
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r5)
            r0 = 0
            r10.fy = r0
            r0 = 0
            r10.n = r0
            goto L_0x000a
        L_0x07b6:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x07bd;
                case 1: goto L_0x0851;
                case 2: goto L_0x0866;
                default: goto L_0x07bb;
            }
        L_0x07bb:
            goto L_0x000a
        L_0x07bd:
            if (r12 != 0) goto L_0x0810
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            int r1 = r1 + 35
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 4
            r0.a(r1, r2)
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
        L_0x07fe:
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            r10.ah = r8
            goto L_0x000a
        L_0x0810:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            r2 = 15
            int r1 = r1 - r2
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 5
            r0.a(r1, r2)
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            goto L_0x07fe
        L_0x0851:
            n[] r0 = r10.fw
            r0 = r0[r5]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            r10.ah = r6
            goto L_0x000a
        L_0x0866:
            if (r12 != 0) goto L_0x08cd
            n[] r0 = r10.fy
            r0 = r0[r5]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            r3 = 5
            int r2 = r2 - r3
            int r3 = r10.dN
            int r2 = r2 + r3
            r0.a(r1, r2)
        L_0x0884:
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.a(r5)
            n[] r0 = r10.fy
            r0 = r0[r5]
            r0.c(r11)
            byte[] r0 = r10.m
            byte r1 = r10.aj
            byte r0 = r0[r1]
            r10.dN = r0
            byte r0 = r10.aj
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.aj = r0
            byte r0 = r10.aj
            byte[] r1 = r10.m
            int r1 = r1.length
            int r1 = r1 - r8
            if (r0 <= r1) goto L_0x000a
            if (r12 != 0) goto L_0x08ea
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.eb
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.ec
            r0.a(r1, r2)
        L_0x08c4:
            r10.aj = r5
            r0 = 0
            r10.fy = r0
            r10.W = r8
            goto L_0x000a
        L_0x08cd:
            n[] r0 = r10.fy
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r3 = 5
            int r2 = r2 - r3
            int r3 = r10.dN
            int r2 = r2 + r3
            r0.a(r1, r2)
            goto L_0x0884
        L_0x08ea:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.eb
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.ec
            r0.a(r1, r2)
            goto L_0x08c4
        L_0x0904:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x090b;
                case 1: goto L_0x094b;
                default: goto L_0x0909;
            }
        L_0x0909:
            goto L_0x000a
        L_0x090b:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            r10.ah = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r1 = 4
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            r10.dZ = r8
            r10.af = r6
            byte[] r0 = new byte[r6]
            r0 = {-3, 3} // fill-array
            r10.L = r0
            goto L_0x000a
        L_0x094b:
            n[] r0 = r10.fw
            r0 = r0[r5]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            goto L_0x000a
        L_0x0960:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0967;
                case 1: goto L_0x0977;
                case 2: goto L_0x09ae;
                case 3: goto L_0x09ed;
                default: goto L_0x0965;
            }
        L_0x0965:
            goto L_0x000a
        L_0x0967:
            r10.fE = r8
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r1 = 7
            r0.a(r1)
            r10.ah = r8
            goto L_0x000a
        L_0x0977:
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            int r0 = r0.ac
            if (r0 != r8) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 8
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            r10.ah = r6
            goto L_0x000a
        L_0x09ae:
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r5)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r1 = 5
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            r10.ah = r7
            r10.af = r7
            goto L_0x000a
        L_0x09ed:
            n[] r0 = r10.fw
            r0 = r0[r5]
            int r0 = r0.ac
            if (r0 <= r6) goto L_0x09f7
            r10.dZ = r8
        L_0x09f7:
            n[] r0 = r10.fw
            r0 = r0[r5]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            r10.fE = r5
            goto L_0x000a
        L_0x0a0e:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0a15;
                case 1: goto L_0x0a7a;
                case 2: goto L_0x0b47;
                default: goto L_0x0a13;
            }
        L_0x0a13:
            goto L_0x000a
        L_0x0a15:
            r0 = 5
            int[] r0 = new int[r0]
            r0 = {100, 110, 100, 120, 130} // fill-array
            r1 = 5
            int[] r1 = new int[r1]
            r1 = {95, 105, 115, 125, 135} // fill-array
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            r2.a(r9)
            r2 = r5
        L_0x0a2b:
            n[] r3 = r10.fy
            int r3 = r3.length
            if (r2 < r3) goto L_0x0a34
            r10.ah = r8
            goto L_0x000a
        L_0x0a34:
            n[] r3 = r10.fy
            r3 = r3[r2]
            r3.a(r8)
            n[] r3 = r10.fy
            r3 = r3[r2]
            r3.a(r9)
            r3 = r1[r2]
            r4 = r0[r2]
            n[] r5 = r10.fy
            r5 = r5[r2]
            n[] r6 = r10.p
            byte r7 = r10.R
            r6 = r6[r7]
            int r6 = r6.aH
            int r3 = r3 + r6
            n[] r6 = r10.p
            byte r7 = r10.R
            r6 = r6[r7]
            int r6 = r6.dN
            int r4 = r6 - r4
            r5.j(r3, r4)
            n[] r3 = r10.fy
            r3 = r3[r2]
            n[] r4 = r10.p
            byte r5 = r10.R
            r4 = r4[r5]
            int r4 = r4.aH
            n[] r5 = r10.p
            byte r6 = r10.R
            r5 = r5[r6]
            int r5 = r5.dN
            r3.k(r4, r5)
            int r2 = r2 + 1
            goto L_0x0a2b
        L_0x0a7a:
            r0 = r5
        L_0x0a7b:
            n[] r1 = r10.fy
            int r1 = r1.length
            if (r0 < r1) goto L_0x0af4
            r0 = r5
        L_0x0a81:
            n[] r1 = r10.fy
            int r1 = r1.length
            if (r0 >= r1) goto L_0x000a
            n[] r1 = r10.fy
            r1 = r1[r0]
            int r2 = r1.aH
            int r3 = r1.eh
            if (r2 >= r3) goto L_0x0b44
            int r2 = r1.dN
            int r1 = r1.ei
            if (r2 <= r1) goto L_0x0b44
            r1 = r8
        L_0x0a97:
            if (r1 == 0) goto L_0x0af1
            r10.dZ = r8
            n[] r1 = r10.fw
            r1 = r1[r5]
            r2 = 7
            r1.a(r2)
            n[] r1 = r10.fw
            r1 = r1[r5]
            r1.a(r8)
            n[] r1 = r10.fw
            r1 = r1[r5]
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.aH
            r3 = 15
            int r2 = r2 - r3
            n[] r3 = r10.p
            byte r4 = r10.R
            r3 = r3[r4]
            int r3 = r3.dN
            int r3 = r3 + 2
            r1.a(r2, r3)
            n[] r1 = r10.fw
            r1 = r1[r8]
            r2 = 9
            r1.a(r2)
            n[] r1 = r10.fw
            r1 = r1[r8]
            r1.a(r8)
            n[] r1 = r10.fw
            r1 = r1[r8]
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.aH
            n[] r3 = r10.p
            byte r4 = r10.R
            r3 = r3[r4]
            int r3 = r3.dN
            int r3 = r3 + 10
            r1.a(r2, r3)
            r10.ah = r6
        L_0x0af1:
            int r0 = r0 + 1
            goto L_0x0a81
        L_0x0af4:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.c(r11)
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.Q()
            n[] r1 = r10.fy
            r1 = r1[r0]
            int r2 = r1.el
            int r2 = r2 + 1
            r1.el = r2
            int r2 = r1.el
            byte r3 = r1.W
            int r2 = r2 * r3
            r1.ef = r2
            int r2 = r1.ed
            int r3 = r1.ef
            int r2 = r2 - r3
            r1.aH = r2
            int r2 = r1.ek
            if (r2 == 0) goto L_0x0b32
            int r2 = r1.el
            byte r3 = r1.W
            int r2 = r2 * r3
            r1.eg = r2
            int r2 = r1.ee
            int r3 = r1.ei
            if (r2 <= r3) goto L_0x0b36
            int r2 = r1.ee
            int r3 = r1.eg
            int r2 = r2 - r3
            r1.dN = r2
        L_0x0b32:
            int r0 = r0 + 1
            goto L_0x0a7b
        L_0x0b36:
            int r2 = r1.ee
            int r3 = r1.ei
            if (r2 >= r3) goto L_0x0b32
            int r2 = r1.ee
            int r3 = r1.eg
            int r2 = r2 + r3
            r1.dN = r2
            goto L_0x0b32
        L_0x0b44:
            r1 = r5
            goto L_0x0a97
        L_0x0b47:
            n[] r0 = r10.fw
            r0 = r0[r5]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            byte r0 = r10.ak
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.ak = r0
            r10.af = r7
            byte r0 = r10.ak
            if (r0 <= r7) goto L_0x000a
            r10.ak = r5
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r5)
            r0 = 0
            r10.fy = r0
            goto L_0x000a
        L_0x0b7f:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0b86;
                case 1: goto L_0x0c7c;
                case 2: goto L_0x0de9;
                default: goto L_0x0b84;
            }
        L_0x0b84:
            goto L_0x000a
        L_0x0b86:
            if (r12 != 0) goto L_0x0c24
            int[] r0 = new int[r7]
            r10.aI = r0
            int[] r0 = new int[r7]
            r10.fK = r0
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            r0 = r5
        L_0x0b9a:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x0bad
            r0 = 4
            byte[] r0 = new byte[r0]
            r0 = {-3, 3, -2, 1} // fill-array
            r10.L = r0
        L_0x0ba9:
            r10.ah = r8
            goto L_0x000a
        L_0x0bad:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r2 = 10
            r1.a(r2)
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.a(r8)
            r1 = 80
            r2 = 100
            int r1 = defpackage.ee.q(r1, r2)
            r2 = 100
            r3 = 120(0x78, float:1.68E-43)
            int r2 = defpackage.ee.q(r2, r3)
            n[] r3 = r10.fy
            r3 = r3[r0]
            n[] r4 = r10.p
            byte r5 = r10.R
            r4 = r4[r5]
            int r4 = r4.aH
            int r1 = r1 + r4
            n[] r4 = r10.p
            byte r5 = r10.R
            r4 = r4[r5]
            int r4 = r4.dN
            int r2 = r4 - r2
            r3.j(r1, r2)
            int[] r1 = r10.aI
            r2 = -15
            r3 = 25
            int r2 = defpackage.ee.q(r2, r3)
            r1[r0] = r2
            int[] r1 = r10.fK
            r2 = -10
            r3 = 10
            int r2 = defpackage.ee.q(r2, r3)
            r1[r0] = r2
            n[] r1 = r10.fy
            r1 = r1[r0]
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.aH
            int[] r3 = r10.aI
            r3 = r3[r0]
            int r2 = r2 + r3
            n[] r3 = r10.p
            byte r4 = r10.R
            r3 = r3[r4]
            int r3 = r3.dN
            int[] r4 = r10.fK
            r4 = r4[r0]
            int r3 = r3 + r4
            r1.k(r2, r3)
            int r0 = r0 + 1
            goto L_0x0b9a
        L_0x0c24:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 10
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            r2 = 100
            int r1 = r1 - r2
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r3 = 120(0x78, float:1.68E-43)
            int r2 = r2 - r3
            r0.j(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            r0.k(r1, r2)
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 18
            r0.e(r1)
            goto L_0x0ba9
        L_0x0c7c:
            if (r12 != 0) goto L_0x0da0
            r0 = r5
        L_0x0c7f:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x0ca8
            r0 = r5
        L_0x0c87:
            n[] r1 = r10.fy
            int r1 = r1.length
            int r1 = r1 / 2
            if (r0 < r1) goto L_0x0d71
            boolean r0 = r10.W()
            if (r0 == 0) goto L_0x000a
            byte r0 = r10.ak
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.ak = r0
            byte r0 = r10.ak
            if (r0 <= r7) goto L_0x000a
            r10.ak = r5
            r10.W = r8
            r0 = 0
            r10.fy = r0
            goto L_0x000a
        L_0x0ca8:
            n[] r1 = r10.fy
            r1 = r1[r0]
            boolean r1 = r1.M
            if (r1 != 0) goto L_0x0cc5
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.c(r11)
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.Q()
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.f(r12)
        L_0x0cc5:
            n[] r1 = r10.fy
            r1 = r1[r0]
            boolean r1 = r1.am()
            if (r1 == 0) goto L_0x0d37
            r10.dZ = r8
            r10.af = r6
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.a(r5)
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.em
            if (r1 != 0) goto L_0x0d37
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r2 = 8
            r1.a(r2)
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.em = r8
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.a(r8)
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.aH
            int[] r3 = r10.aI
            r3 = r3[r0]
            int r2 = r2 + r3
            n[] r3 = r10.p
            byte r4 = r10.R
            r3 = r3[r4]
            int r3 = r3.dN
            int[] r4 = r10.fK
            r4 = r4[r0]
            int r3 = r3 + r4
            r1.a(r2, r3)
        L_0x0d37:
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.em
            if (r1 == 0) goto L_0x0d6d
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.M
            if (r1 != 0) goto L_0x0d6d
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.c(r11)
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.Q()
        L_0x0d6d:
            int r0 = r0 + 1
            goto L_0x0c7f
        L_0x0d71:
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.em
            if (r1 == 0) goto L_0x0d9c
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            boolean r1 = r1.W()
            if (r1 == 0) goto L_0x0d9c
            n[] r1 = r10.fy
            n[] r2 = r10.fy
            int r2 = r2.length
            int r2 = r2 / 2
            int r2 = r2 + r0
            r1 = r1[r2]
            r1.a(r5)
        L_0x0d9c:
            int r0 = r0 + 1
            goto L_0x0c87
        L_0x0da0:
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.f(r12)
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.am()
            if (r0 == 0) goto L_0x000a
            r10.ea = r8
            r10.af = r6
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r5)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r1 = 8
            r0.a(r1)
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r5]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            r10.ah = r6
            goto L_0x000a
        L_0x0de9:
            if (r12 != r8) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r5]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r10.W = r8
            n[] r0 = r10.fw
            r0 = r0[r5]
            r0.a(r5)
            goto L_0x000a
        L_0x0e00:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0e07;
                case 1: goto L_0x0e30;
                default: goto L_0x0e05;
            }
        L_0x0e05:
            goto L_0x000a
        L_0x0e07:
            if (r12 != 0) goto L_0x0e24
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            r10.dZ = r8
            r0 = 4
            r10.af = r0
        L_0x0e17:
            n[] r0 = r10.fw
            r0 = r0[r8]
            r1 = 11
            r0.a(r1)
            r10.ah = r8
            goto L_0x000a
        L_0x0e24:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r8)
            r10.ea = r8
            goto L_0x0e17
        L_0x0e30:
            if (r12 != 0) goto L_0x0e67
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
        L_0x0e4b:
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r8)
            n[] r0 = r10.fw
            r0 = r0[r8]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            n[] r0 = r10.fw
            r0 = r0[r8]
            r0.a(r5)
            r10.W = r8
            goto L_0x000a
        L_0x0e67:
            n[] r0 = r10.fw
            r0 = r0[r8]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            goto L_0x0e4b
        L_0x0e81:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0e88;
                case 1: goto L_0x0ef5;
                default: goto L_0x0e86;
            }
        L_0x0e86:
            goto L_0x000a
        L_0x0e88:
            if (r12 != 0) goto L_0x0ec5
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            r0 = r5
        L_0x0e94:
            n[] r1 = r10.fy
            int r1 = r1.length
            if (r0 < r1) goto L_0x0e9d
        L_0x0e99:
            r10.ah = r8
            goto L_0x000a
        L_0x0e9d:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r2 = 12
            r1.a(r2)
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.a(r8)
            n[] r1 = r10.fy
            r1 = r1[r0]
            n[] r2 = r10.fv
            r2 = r2[r0]
            int r2 = r2.aH
            n[] r3 = r10.fv
            r3 = r3[r0]
            int r3 = r3.dN
            int r3 = r3 + 2
            r1.a(r2, r3)
            int r0 = r0 + 1
            goto L_0x0e94
        L_0x0ec5:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r6)
            n[] r0 = r10.fy
            int r1 = r10.eb
            r0 = r0[r1]
            r1 = 12
            r0.a(r1)
            n[] r0 = r10.fy
            int r1 = r10.eb
            r0 = r0[r1]
            n[] r1 = r10.p
            int r2 = r10.eb
            r1 = r1[r2]
            int r1 = r1.aH
            n[] r2 = r10.p
            int r3 = r10.eb
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 + 2
            r0.a(r1, r2)
            goto L_0x0e99
        L_0x0ef5:
            if (r12 != 0) goto L_0x0f3d
            r0 = r5
        L_0x0ef8:
            n[] r1 = r10.fy
            int r1 = r1.length
            if (r0 < r1) goto L_0x0f0a
            boolean r0 = r10.aW()
            if (r0 == 0) goto L_0x000a
            r0 = 0
            r10.fy = r0
            r10.W = r7
            goto L_0x000a
        L_0x0f0a:
            byte[][] r1 = r10.eq
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            r1 = r1[r2]
            byte r1 = r1[r5]
            r2 = -1
            if (r1 == r2) goto L_0x0f33
            short[] r1 = defpackage.eb.ab
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            short r1 = r1[r2]
            short[][] r2 = defpackage.eb.ev
            byte[] r3 = defpackage.eb.m
            byte r3 = r3[r0]
            r2 = r2[r3]
            r3 = 4
            short r2 = r2[r3]
            if (r1 >= r2) goto L_0x0f33
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.c(r11)
        L_0x0f33:
            n[] r1 = r10.fy
            r1 = r1[r0]
            r1.Q()
            int r0 = r0 + 1
            goto L_0x0ef8
        L_0x0f3d:
            n[] r0 = r10.fy
            int r1 = r10.eb
            r0 = r0[r1]
            r0.c(r11)
            n[] r0 = r10.fy
            int r1 = r10.eb
            r0 = r0[r1]
            r0.Q()
            n[] r0 = r10.fy
            int r1 = r10.eb
            r0 = r0[r1]
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x000a
            r0 = 0
            r10.fy = r0
            r10.W = r8
            goto L_0x000a
        L_0x0f62:
            byte r0 = r10.ah
            switch(r0) {
                case 0: goto L_0x0f69;
                case 1: goto L_0x0fa1;
                default: goto L_0x0f67;
            }
        L_0x0f67:
            goto L_0x000a
        L_0x0f69:
            int[] r0 = new int[r9]
            r0 = {0, 5, 10, -6, -12, 8} // fill-array
            r10.fL = r0
            int[] r0 = new int[r9]
            r0 = {0, 2, 4, -3, -1, 5} // fill-array
            r10.fM = r0
            if (r12 != 0) goto L_0x0f93
            n[] r0 = r10.fv
            byte r1 = r10.j
            r0 = r0[r1]
            r0.a(r9)
            r10.dZ = r8
            r0 = 5
            r10.af = r0
        L_0x0f87:
            r0 = 4
            byte[] r0 = new byte[r0]
            r0 = {-2, 2, 1, -1} // fill-array
            r10.L = r0
            r10.ah = r8
            goto L_0x000a
        L_0x0f93:
            n[] r0 = r10.p
            byte r1 = r10.R
            r0 = r0[r1]
            r0.a(r8)
            r10.ea = r8
            r10.af = r7
            goto L_0x0f87
        L_0x0fa1:
            if (r12 != 0) goto L_0x100a
            n[] r0 = r10.fw
            r0 = r0[r7]
            n[] r1 = r10.p
            byte r2 = r10.R
            r1 = r1[r2]
            int r1 = r1.aH
            int r2 = r10.ac
            int r1 = r1 + r2
            n[] r2 = r10.p
            byte r3 = r10.R
            r2 = r2[r3]
            int r2 = r2.dN
            int r3 = r10.dN
            int r2 = r2 + r3
            r0.a(r1, r2)
        L_0x0fc0:
            n[] r0 = r10.fw
            r0 = r0[r7]
            r0.a(r8)
            int r0 = r10.ac
            int[] r1 = r10.fL
            byte r2 = r10.al
            r1 = r1[r2]
            int r0 = r0 + r1
            r10.ac = r0
            int r0 = r10.dN
            int[] r1 = r10.fM
            byte r2 = r10.al
            r1 = r1[r2]
            int r0 = r0 + r1
            r10.dN = r0
            byte r0 = r10.al
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.al = r0
            byte r0 = r10.al
            int[] r1 = r10.fL
            int r1 = r1.length
            int r1 = r1 - r8
            if (r0 <= r1) goto L_0x000a
            r10.al = r5
            r10.ac = r5
            r10.dN = r5
            byte r0 = r10.ak
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r10.ak = r0
            byte r0 = r10.ak
            if (r0 <= r7) goto L_0x000a
            r10.W = r8
            r10.ak = r5
            n[] r0 = r10.fw
            r0 = r0[r7]
            r0.a(r5)
            goto L_0x000a
        L_0x100a:
            n[] r0 = r10.fw
            r0 = r0[r7]
            n[] r1 = r10.fv
            byte r2 = r10.j
            r1 = r1[r2]
            int r1 = r1.aH
            int r2 = r10.ac
            int r1 = r1 + r2
            n[] r2 = r10.fv
            byte r3 = r10.j
            r2 = r2[r3]
            int r2 = r2.dN
            int r3 = r10.dN
            int r2 = r2 + r3
            r0.a(r1, r2)
            goto L_0x0fc0
        L_0x1028:
            r10.d(r11, r12)
            goto L_0x000a
        L_0x102d:
            r10.u(r11)
            goto L_0x000a
        L_0x1032:
            r10.m(r11)
            goto L_0x000a
        L_0x1037:
            r10.r()
            goto L_0x000a
        L_0x103c:
            r10.v(r11)
            goto L_0x000a
        L_0x1041:
            r10.s()
            goto L_0x000a
        L_0x1046:
            r10.w(r11)
            goto L_0x000a
        L_0x104b:
            r10.e(r11, r12)
            goto L_0x000a
        L_0x1050:
            r10.x(r11)
            goto L_0x000a
        L_0x1055:
            r10.t(r11)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bg.c(an, int):void");
    }

    private void d() {
        this.aH = 0;
        this.ac = 0;
        this.dN = 0;
        this.ah = 0;
    }

    private void d(int i2, int i3, int i4) {
        int i5;
        if (ee.q(0, 99) > ((e(i3, 2, i4, 2) + e(i3, 2, i4, 3)) + eb.ev[eb.m[i3]][1]) - (this.ev[i2][1] * 2)) {
            this.fX = false;
            int q2 = q(i2, i4);
            int t2 = t(i3, i4) + e(i3, 1, i4, 2) + e(i3, 1, i4, 3);
            int q3 = ee.q(0, 99);
            if (q3 < 5) {
                this.l = (short) ((q2 - t2) * 2);
                this.fU = true;
            } else {
                this.l = (short) (q2 - t2);
                this.fU = false;
            }
            if (this.l <= 10) {
                short q4 = (short) ee.q(-3, 5);
                if (q3 < 5) {
                    this.l = (short) (q4 + 20);
                } else {
                    this.l = (short) (q4 + 10);
                }
            }
            i5 = 1;
        } else {
            this.fX = true;
            i5 = 2;
        }
        a(i5, this.l, this.fv[i3].aH, this.fv[i3].dN - 30);
    }

    private void d(an anVar) {
        A(anVar);
        p(anVar);
        if (!this.gc) {
            anVar.a(this.q, this.fv[this.j].aH + 15 + 0 + (((bi.e >> 2) % 2) << 1), (this.fv[this.j].dN - 40) + 0, 0);
            return;
        }
        if (as < 2) {
            this.gb = (byte) eb.m.length;
        } else {
            this.gb = (byte) this.ga.length;
        }
        if (this.f < this.gb) {
            byte b = eb.m[this.f];
            this.fP = (byte) (this.fP + 1);
            if (this.fP >= 26) {
                if (this.eq[b][1] == -1) {
                    if (this.ga[b][1] >= 2) {
                        this.eq[b][1] = 0;
                    }
                    b(1, this.f, this.ga[b][0]);
                    if (eb.ab[b] <= 0) {
                        for (byte b2 = 0; b2 < this.fv.length; b2 = (byte) (b2 + 1)) {
                            byte b3 = (byte) (this.j + 1);
                            this.j = b3;
                            this.j = b3 > ((byte) (this.fv.length - 1)) ? 0 : this.j;
                            if (this.eq[this.j][0] != -1) {
                                break;
                            }
                        }
                    }
                }
                this.fG = 0;
                this.Y = 0;
                this.f = (byte) (this.f + 1);
                this.g = 0;
                this.fP = 0;
                ea.cp().ae = false;
                if (q(1)) {
                    a((byte) 5);
                    this.fG = 0;
                    this.f = 0;
                    this.fP = 0;
                    this.Y = 0;
                    this.g = 0;
                    this.gc = false;
                }
            } else if (this.eq[b][1] == -1) {
                this.aG = new StringBuffer().append(bj.aC[b]).append("中毒，生命值-").append((int) this.ga[b][0]).toString();
                ed.cB().a(5, this.aG.toCharArray());
                if (this.g == 0) {
                    a(1, this.ga[b][0], this.fv[this.f].aH, this.fv[this.f].dN - 20);
                    this.g = 1;
                }
                byte b4 = (byte) (this.Y + 1);
                this.Y = b4;
                this.Y = b4 > this.m.length - 1 ? (byte) (this.m.length - 1) : this.Y;
                b(anVar, this.m[this.Y]);
            } else {
                this.Y = 0;
                this.fG = 0;
                this.f = (byte) (this.f + 1);
                this.g = 0;
                this.fP = 0;
            }
        } else {
            ea.cp().ae = false;
            this.fG = 0;
            this.aG = "选择英雄";
            this.f = 0;
            this.fP = 0;
            this.Y = 0;
            this.g = 0;
            this.gc = false;
        }
    }

    private void d(an anVar, int i2) {
        switch (this.ah) {
            case 0:
                this.fE = true;
                this.fL = new int[5];
                this.fM = new int[5];
                if (i2 == 0) {
                    for (int i3 = 0; i3 < this.fL.length; i3++) {
                        int q2 = ee.q(0, this.p.length - 1);
                        int q3 = ee.q(-30, 20);
                        this.fL[i3] = this.p[q2].aH + q3;
                        this.fM[i3] = this.p[q2].dN + q3;
                    }
                    this.fv[this.j].a(6);
                } else {
                    for (int i4 = 0; i4 < this.fL.length; i4++) {
                        int q4 = ee.q(0, this.fv.length - 1);
                        int q5 = ee.q(-20, 15);
                        this.fL[i4] = this.fv[q4].aH + q5;
                        this.fM[i4] = this.fv[q4].dN + q5;
                    }
                    this.p[this.R].a(1);
                }
                for (n a : this.fy) {
                    a.a(1);
                }
                this.ah = 1;
                return;
            case 1:
                if (!this.em) {
                    this.al = (byte) (this.al + 1);
                }
                if (this.al % 7 == 0) {
                    this.em = true;
                    if (i2 == 0) {
                        this.L = new byte[]{-2, 2, 1, -1};
                        this.dZ = true;
                    } else {
                        this.ea = true;
                    }
                    this.af = 1;
                    for (int i5 = 0; i5 < this.fy.length; i5++) {
                        this.fy[i5].a(ee.q(-10, 20) + this.fL[i5], ee.q(-35, 20) + this.fM[i5]);
                        this.fy[i5].a(true);
                        this.fy[i5].c(anVar);
                        this.fy[i5].Q();
                    }
                    if (aW()) {
                        if (i2 == 0) {
                            this.dZ = false;
                        }
                        this.em = false;
                    }
                }
                if (this.al > 21) {
                    this.em = false;
                    this.W = 1;
                    this.ak = 0;
                    this.fy = null;
                    this.em = false;
                    this.al = 0;
                    this.fE = false;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public static int e(int i2, int i3, int i4, int i5) {
        byte b;
        int i6 = 0;
        while (true) {
            if (i6 >= eb.dQ[eb.m[i2]].length) {
                b = 0;
                break;
            } else if (eb.dQ[eb.m[i2]][i6][1] == i5) {
                b = eb.dQ[eb.m[i2]][i6][0];
                break;
            } else if (eb.dQ[eb.m[i2]][i6][1] == 0) {
                return 0;
            } else {
                i6++;
            }
        }
        switch (i3) {
            case 0:
                return i4 == 0 ? bj.gw[b][4] : bj.gw[b][5];
            case 1:
                return i4 == 0 ? bj.gw[b][6] : bj.gw[b][7];
            case 2:
                return bj.gw[b][9];
            case 3:
                return bj.gw[b][8];
            default:
                return 0;
        }
    }

    private void e(an anVar) {
        char[] charArray;
        char[] charArray2;
        A(anVar);
        for (int i2 = 0; i2 < this.eq.length; i2++) {
            for (int i3 = 0; i3 < this.eq[i2].length; i3++) {
                this.eq[i2][i3] = 0;
            }
        }
        for (int i4 = 0; i4 < this.aw.length; i4++) {
            this.aw[i4] = 0;
            this.ax[i4] = 0;
        }
        for (int i5 = 0; i5 < this.ga.length; i5++) {
            for (int i6 = 0; i6 < this.ga[i5].length; i6++) {
                this.ga[i5][i6] = 0;
            }
        }
        if (ea.cp().ae) {
            ed.cB().c();
        } else if (!this.M) {
            switch (this.f) {
                case 0:
                    ed.cB().a(5, "全军覆没!".toCharArray());
                    this.f = 1;
                    return;
                case 1:
                    if (as < 2) {
                        b.Z().b(b.Z().ay, 2);
                        ea.cp().a(7);
                    } else {
                        c();
                        eb.m = new byte[]{0};
                        a((byte) 0);
                        eb.fA = (short[][]) Array.newInstance(Short.TYPE, 4, 2);
                        eb.cq().k(0, 10);
                        eb.cq().k(5, 10);
                        eb.ac = 100;
                        dz.cn().N();
                        as = 0;
                        bh.c();
                        bh.a(bh.g);
                    }
                    this.ge = 0;
                    return;
                default:
                    return;
            }
        } else if (this.f < this.i) {
            switch (this.dR[this.f][0]) {
                case 0:
                    int c = ee.c(this.dR[this.f][2], this.dR[this.f][3]);
                    charArray = new StringBuffer().append(bj.aC[this.dR[this.f][1]]).append("获得经验：").toString().toCharArray();
                    charArray2 = new StringBuffer().append("").append(c).toString().toCharArray();
                    break;
                case 1:
                    charArray = new StringBuffer().append(bj.aC[this.dR[this.f][1]]).append("升级到：").toString().toCharArray();
                    charArray2 = new StringBuffer().append("").append((int) this.dR[this.f][2]).append("级").toString().toCharArray();
                    break;
                case 2:
                    charArray = new StringBuffer().append(bj.aC[this.dR[this.f][1]]).append("学会技能：").toString().toCharArray();
                    charArray2 = bj.aE[this.dR[this.f][2]].toCharArray();
                    break;
                case 3:
                    charArray = bj.aE[this.dR[this.f][1]].toCharArray();
                    charArray2 = "提升一级".toCharArray();
                    break;
                case 4:
                    this.j = 0;
                    charArray = "获得物品：".toCharArray();
                    charArray2 = new StringBuffer().append(r.aC[this.dR[this.f][1]]).append("×").append((int) this.dR[this.f][2]).toString().toCharArray();
                    break;
                case 5:
                    charArray = "获得金钱：".toCharArray();
                    charArray2 = new StringBuffer().append("").append(ee.c(this.dR[this.f][1], this.dR[this.f][2])).toString().toCharArray();
                    break;
                default:
                    charArray = null;
                    charArray2 = null;
                    break;
            }
            ed.cB().a(charArray, charArray2);
            this.f = (byte) (this.f + 1);
        } else {
            bi.ac = 0;
            c();
            bi.bo().a(1);
            for (int i7 = 0; i7 < eb.m.length; i7++) {
                if (eb.ab[eb.m[i7]] <= 0) {
                    eb.ab[eb.m[i7]] = 10;
                }
            }
            if (as > 0) {
                dz.cn().N();
            } else {
                ea.cp().a(1);
            }
            as = 0;
            bh.c();
            bh.a(bh.g);
            a((byte) 0);
        }
    }

    private void e(an anVar, int i2) {
        switch (this.ah) {
            case 0:
                if (i2 == 0) {
                    this.fw[4].a(0);
                    this.dZ = true;
                    this.af = 2;
                    this.L = new byte[]{-2, 2, 1, -1};
                } else {
                    this.p[this.R].a(1);
                    this.fw[4].a(2);
                    this.ea = true;
                    this.af = 1;
                }
                for (n a : this.fy) {
                    a.a(1);
                }
                this.ah = 1;
                return;
            case 1:
                if (i2 == 0) {
                    this.fw[4].a(this.ac + 160, (int) Player.REALIZED);
                    this.fw[4].a(true);
                    for (int i3 = 0; i3 < this.p.length; i3++) {
                        if (this.dP[i3][0] != -1) {
                            this.fy[0].a(this.p[i3].aH, this.p[i3].dN + 2);
                            this.fy[0].c(anVar);
                            this.fy[0].Q();
                        }
                    }
                    this.ac -= 18;
                    if (this.ac + 160 < 0) {
                        this.W = 1;
                        this.fw[4].a(false);
                        this.fy = null;
                        return;
                    }
                    return;
                }
                this.fw[4].a(this.ac + 50, (int) Player.REALIZED);
                this.fw[4].a(true);
                for (int i4 = 0; i4 < this.fv.length; i4++) {
                    if (this.eq[eb.m[i4]][0] != -1) {
                        this.fy[0].a(this.fv[i4].aH, this.fv[i4].dN + 2);
                        this.fy[0].c(anVar);
                        this.fy[0].Q();
                    }
                }
                this.ac += 20;
                if (this.ac > 240) {
                    this.W = 1;
                    this.fw[4].a(false);
                    this.fy = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bg.a(int, short, int, int):void
     arg types: [int, int, int, int]
     candidates:
      bg.a(byte, byte, byte, byte):void
      bg.a(int, short, int, int):void */
    private void f(int i2, int i3, int i4, int i5) {
        int q2;
        short s2;
        int i6;
        if (i5 != 0) {
            int q3 = ee.q(0, 99);
            short s3 = this.ev[i2][6];
            if (q3 < 5) {
                this.fU = true;
                System.out.println("混乱物理暴击失血");
            } else {
                this.fU = false;
                System.out.println("混乱物理攻击失血");
            }
            this.an = (byte) ee.q(0, this.p.length - 1);
            int i7 = 0;
            while (true) {
                if (i7 >= this.p.length) {
                    break;
                } else if (this.an == i2) {
                    d(i2, i3, i4);
                    this.fZ = false;
                    i7++;
                } else if (this.dP[this.an][0] != -1) {
                    this.fZ = true;
                } else {
                    d(i2, i3, i4);
                    this.fZ = false;
                }
            }
            if (this.fZ) {
                this.fX = false;
                this.l = (short) (s3 - this.ev[i2][7]);
                if (q3 < 5) {
                    this.l = (short) (this.l * 2);
                }
                if (this.l > 0) {
                    return;
                }
                if (q3 < 5) {
                    this.l = 20;
                } else {
                    this.l = 10;
                }
            }
        } else if (i4 == 0) {
            d(i2, i3, i4);
        } else {
            switch (this.at) {
                case 0:
                case 2:
                case 4:
                    q2 = q(i2, 0);
                    s2 = bj.fz[this.Z][12];
                    break;
                case 1:
                    q2 = q(i2, 1);
                    s2 = bj.fz[this.Z][13];
                    break;
                case 3:
                default:
                    q2 = 0;
                    s2 = 0;
                    break;
            }
            switch (this.au) {
                case 0:
                    if (ee.q(0, 99) > ((e(i3, 2, i4, 2) + e(i3, 2, i4, 3)) + eb.ev[eb.m[i3]][1]) - (this.ev[i2][1] * 2)) {
                        this.fX = false;
                        int i8 = 0;
                        switch (this.at) {
                            case 0:
                            case 2:
                                i8 = 0;
                                break;
                            case 1:
                                i8 = 1;
                                break;
                        }
                        this.l = (short) ((s2 * (q2 - (e(i3, 1, i8, 3) + (t(i3, i8) + e(i3, 1, i8, 2))))) / 100);
                        if (this.l <= 15) {
                            this.l = (short) (((short) ee.q(-5, 10)) + 15);
                        }
                        i6 = 1;
                    } else {
                        i6 = 2;
                        this.fX = true;
                    }
                    a(i6, this.l, this.fv[this.j].aH, this.fv[this.j].dN - 30);
                    return;
                case 1:
                    this.aq = new short[this.fv.length];
                    this.fY = new boolean[this.fv.length];
                    for (int i9 = 0; i9 < this.aq.length; i9++) {
                        if (this.eq[eb.m[i9]][0] != -1) {
                            if (ee.q(0, 99) > ((e(i9, 2, i4, 2) + e(i9, 2, i4, 3)) + eb.ev[eb.m[i9]][1]) - (this.ev[i2][1] * 2)) {
                                this.fY[i9] = false;
                                int i10 = 0;
                                switch (this.at) {
                                    case 0:
                                    case 2:
                                        i10 = 0;
                                        break;
                                    case 1:
                                        i10 = 1;
                                        break;
                                }
                                this.aq[i9] = (short) (((q2 - (e(i9, 1, i10, 3) + (t(i9, i10) + e(i9, 1, i10, 2)))) * s2) / 100);
                                if (this.aq[i9] <= 15) {
                                    this.aq[i9] = (short) (((short) ee.q(-3, 12)) + 15);
                                }
                                a(1, this.aq[i9], this.fv[i9].aH, this.fv[i9].dN - 30);
                            } else {
                                this.fY[i9] = true;
                                a(2, (short) 0, this.fv[i9].aH, this.fv[i9].dN - 30);
                            }
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v41, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r0v42, types: [byte] */
    private void g(an anVar) {
        du.a(anVar, this.J[0], 0, 80, 120, 0);
        du.a(anVar, this.J[0], 2, 120, 120, 0);
        du.a(anVar, this.J[0], 1, 80, 160, 0);
        du.a(anVar, this.J[0], 3, 120, 160, 0);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= 5) {
                break;
            }
            du.a(anVar, this.J[2], 100, 0, 20, 20, 0, this.es[i3][0] + 110, this.es[i3][1] + 150);
            i2 = i3 + 1;
        }
        du.a(anVar, this.J[1], 0, 109, 109, 0);
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= 5) {
                break;
            }
            du.a(anVar, this.J[2], (i5 == 0 ? this.ay[i5] : this.ay[i5] + 6) * 20, 0, 20, 20, 0, this.es[i5][0] + 110, this.es[i5][1] + 150);
            i4 = i5 + 1;
        }
        if (!this.fJ) {
            this.fC[0].a(120, 187);
            this.fC[0].c(anVar);
            this.fC[0].Q();
            return;
        }
        this.fC[1].a(120, (int) DirectGraphics.ROTATE_180);
        this.fC[1].c(anVar);
        this.fC[1].Q();
    }

    private void h(int i2, int i3) {
        eb.cq().k(i2, i3);
        this.dR[this.i][0] = 4;
        this.dR[this.i][1] = (byte) i2;
        this.dR[this.i][2] = (byte) i3;
        this.i = (byte) (this.i + 1);
    }

    private void i(an anVar) {
        switch (this.W) {
            case 0:
                c(anVar, 0);
                return;
            case 1:
                this.dZ = false;
                aZ();
                b(anVar, this.m[this.Y]);
                this.Y = (byte) (this.Y + 1);
                if (this.Y > this.m.length - 1) {
                    this.fv[this.j].a(0);
                    this.fv[this.j].a(this.fv[this.j].eb, this.fv[this.j].ec);
                    if (this.at != 3) {
                        switch (this.au) {
                            case 0:
                                if (!this.fX) {
                                    b(0, this.R, this.l);
                                    break;
                                }
                                break;
                            case 1:
                                for (int i2 = 0; i2 < this.fY.length; i2++) {
                                    if (!this.fY[i2]) {
                                        b(0, i2, this.aq[i2]);
                                    }
                                }
                                break;
                        }
                    }
                    this.dY = false;
                    if (this.aa != 0) {
                        switch (this.au) {
                            case 0:
                                if (this.dP[this.R][0] != -1 && ee.q(0, 99) < 10 && !b(0, this.R)) {
                                    c(0, this.aa, this.R);
                                    break;
                                }
                            case 1:
                                for (int i3 = 0; i3 < this.p.length; i3++) {
                                    if (this.dP[i3][0] != -1 && ee.q(0, 99) < 10 && !b(0, i3)) {
                                        c(0, this.aa, i3);
                                        this.fO = (byte) i3;
                                    }
                                }
                                break;
                        }
                    }
                    this.ae = false;
                    this.dX = false;
                    switch (this.at) {
                        case 2:
                            r(0);
                            break;
                        case 4:
                            s(0);
                            break;
                    }
                    if (this.dY || this.ae || this.dX) {
                        this.W = 3;
                    } else {
                        this.W = 2;
                    }
                    this.Y = 0;
                    this.fG = 0;
                    return;
                }
                return;
            case 2:
                b();
                return;
            case 3:
                b(0);
                return;
            default:
                return;
        }
    }

    private void l(an anVar) {
        if (this.gc) {
            A(anVar);
            p(anVar);
            this.fP = (byte) (this.fP + 1);
            if (this.fP < 26) {
                this.aG = new StringBuffer().append(this.aC[this.R]).append("冰封一回合").toString();
                ed.cB().a(5, this.aG.toCharArray());
                return;
            }
            ea.cp().ae = false;
            this.X = (byte) (this.X + 1);
            this.fP = 0;
            this.dP[this.R][3] = 0;
            if (this.X < this.p.length) {
                for (byte b = 0; b < this.p.length; b = (byte) (b + 1)) {
                    byte b2 = (byte) (this.R + 1);
                    this.R = b2;
                    this.R = b2 > ((byte) (this.p.length - 1)) ? 0 : this.R;
                    if (this.dP[this.R][0] != -1) {
                        break;
                    }
                    this.X = (byte) (this.X + 1);
                }
                this.gc = false;
                return;
            }
            this.gc = false;
            this.X = 0;
            this.R = 0;
            a((byte) 2);
        }
    }

    private void m(an anVar) {
        switch (this.ah) {
            case 0:
                this.p[this.R].a(1);
                for (int i2 = 0; i2 < this.fy.length; i2++) {
                    this.fy[i2].a(14);
                    this.fy[i2].a(this.fv[i2].aH - 80, this.fv[i2].dN);
                    this.fy[i2].a(true);
                }
                this.ah = 1;
                return;
            case 1:
                for (int i3 = 0; i3 < this.fy.length; i3++) {
                    if (this.eq[eb.m[i3]][0] != -1) {
                        this.fy[i3].c(anVar);
                        this.fy[i3].Q();
                        if (this.fy[i3].ac > 4) {
                            this.ea = true;
                            this.af = 4;
                        }
                    } else {
                        this.fy[i3].Q();
                    }
                }
                if (aW()) {
                    this.fy = null;
                    this.W = 1;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void o() {
        this.W = 0;
        w();
        if (!q(1)) {
            for (byte b = 0; b < this.p.length; b = (byte) (b + 1)) {
                byte b2 = (byte) (this.R + 1);
                this.R = b2;
                this.R = b2 > ((byte) (this.p.length - 1)) ? 0 : this.R;
                if (this.dP[this.R][0] != -1) {
                    break;
                }
                this.X = (byte) (this.X + 1);
            }
            this.X = (byte) (this.X + 1);
            if (this.X >= this.p.length) {
                for (byte b3 = 0; b3 < this.fv.length; b3 = (byte) (b3 + 1)) {
                    byte b4 = (byte) (this.j + 1);
                    this.j = b4;
                    this.j = b4 > ((byte) (this.fv.length - 1)) ? 0 : this.j;
                    if (this.eq[eb.m[this.j]][0] != -1) {
                        break;
                    }
                }
                a((byte) 10);
                this.X = 0;
                this.f = 0;
                this.R = 0;
                return;
            }
            u(2);
            return;
        }
        this.Y = 0;
        this.X = 0;
        this.j = 0;
        this.R = 0;
        this.M = false;
        a((byte) 5);
    }

    private void p(an anVar) {
        byte b = 0;
        while (true) {
            byte b2 = b;
            if (b2 < this.p.length) {
                if (this.dP[b2][0] != -1) {
                    int i2 = (this.ev[b2][4] * 40) / this.O[b2];
                    int i3 = i2 <= 0 ? 2 : i2;
                    a.M();
                    a.c(anVar, this.fz[b2][0] - 15, (this.fz[b2][1] - ab[this.ev[b2][0]]) - 5, i3, 0, 40, 1);
                    if (b(0, b2)) {
                        int i4 = 0;
                        if (this.dP[b2][1] == -1) {
                            i4 = 4;
                        } else if (this.dP[b2][2] == -1) {
                            i4 = 0;
                        } else if (this.dP[b2][3] == -1) {
                            i4 = 3;
                        } else if (this.dP[b2][4] == -1) {
                            i4 = 2;
                        } else if (this.dP[b2][5] == -1) {
                            i4 = 1;
                        }
                        du.a(anVar, this.r, i4 * 25, 0, 25, 20, 0, this.fz[b2][0] - 10, (this.fz[b2][1] - ab[this.ev[b2][0]]) - 25);
                    }
                }
                b = (byte) (b2 + 1);
            } else {
                return;
            }
        }
    }

    private int q(int i2, int i3) {
        return i3 == 0 ? this.ev[i2][6] : this.ev[i2][8];
    }

    private void q(an anVar) {
        A(anVar);
        switch (this.f) {
            case 0:
                if (this.gc) {
                    A(anVar);
                    p(anVar);
                    this.fP = (byte) (this.fP + 1);
                    if (this.fP < 26) {
                        this.aG = new StringBuffer().append(bj.aC[eb.m[this.j]]).append("冰封一回合").toString();
                        ed.cB().a(5, this.aG.toCharArray());
                        return;
                    }
                    ea.cp().ae = false;
                    this.X = (byte) (this.X + 1);
                    this.fP = 0;
                    this.eq[eb.m[this.j]][3] = 0;
                    if (this.X < eb.m.length) {
                        this.gc = false;
                        byte b = 0;
                        while (b < this.fv.length) {
                            byte b2 = (byte) (this.j + 1);
                            this.j = b2;
                            this.j = b2 > ((byte) (this.fv.length - 1)) ? 0 : this.j;
                            if (this.eq[eb.m[this.j]][0] == -1) {
                                this.X = (byte) (this.X + 1);
                                b = (byte) (b + 1);
                            } else {
                                return;
                            }
                        }
                        return;
                    }
                    this.gc = false;
                    a((byte) 3);
                    this.X = 0;
                    this.j = 0;
                    return;
                }
                return;
            case 1:
                p(anVar);
                g(anVar);
                return;
            case 2:
                p(anVar);
                if (!this.fS) {
                    anVar.a(this.q, this.p[this.R].aH + 27 + 0 + (((bi.e >> 2) % 2) << 1), this.p[this.R].dN - 28, 0);
                    return;
                }
                for (int i2 = 0; i2 < this.p.length; i2++) {
                    if (this.dP[i2][0] != -1) {
                        anVar.a(this.q, this.p[i2].aH + 27 + 0 + (((bi.e >> 2) % 2) << 1), this.p[i2].dN - 28, 0);
                    }
                }
                return;
            case 3:
                r(anVar);
                return;
            case 4:
                switch (this.W) {
                    case 0:
                        if (!this.fZ) {
                            this.fv[this.j].a(this.p[this.R].aH + 50, this.p[this.R].dN + 4);
                        } else {
                            this.fv[this.j].a(this.fv[this.an].aH + 20, this.fv[this.an].dN + 3);
                        }
                        this.fv[this.j].a(5);
                        this.W = 1;
                        return;
                    case 1:
                        if (this.fv[this.j].ac > 2) {
                            this.dZ = true;
                        }
                        if (this.fv[this.j].ac > 3) {
                            if (this.fU) {
                                this.fV = true;
                            } else {
                                b(anVar, this.m[this.Y]);
                                this.Y = (byte) (this.Y + 1);
                                this.Y = this.Y > ((byte) (this.m.length - 1)) ? (byte) (this.m.length - 1) : this.Y;
                            }
                        }
                        if (this.fV) {
                            switch (this.fW) {
                                case 0:
                                    this.o.a(this.p[this.R].aH, this.p[this.R].dN - 10);
                                    this.o.c(anVar);
                                    this.o.Q();
                                    if (this.o.W()) {
                                        this.fW = 1;
                                        break;
                                    }
                                    break;
                                case 1:
                                    b(anVar, this.m[this.Y]);
                                    this.Y = (byte) (this.Y + 1);
                                    break;
                            }
                            if (this.fv[this.j].W()) {
                                aZ();
                                this.fv[this.j].a(0);
                                this.fv[this.j].a(this.fv[this.j].eb, this.fv[this.j].ec);
                            }
                        }
                        if (this.fU) {
                            if (this.fW == 1 && this.Y > ((byte) (this.m.length - 1))) {
                                this.fW = 0;
                                this.fV = false;
                                this.dZ = false;
                                aZ();
                                this.Y = 0;
                                this.fG = 0;
                                this.fv[this.j].a(0);
                                this.fv[this.j].a(this.fv[this.j].eb, this.fv[this.j].ec);
                                if (!this.fX) {
                                    if (!this.fZ) {
                                        b(0, this.R, this.l);
                                    } else {
                                        b(1, this.an, this.l);
                                    }
                                }
                                this.W = 2;
                                return;
                            }
                            return;
                        } else if (this.fv[this.j].W()) {
                            this.dZ = false;
                            aZ();
                            this.Y = 0;
                            this.fG = 0;
                            this.fv[this.j].a(0);
                            this.fv[this.j].a(this.fv[this.j].eb, this.fv[this.j].ec);
                            if (!this.fX) {
                                if (!this.fZ) {
                                    b(0, this.R, this.l);
                                } else {
                                    b(1, this.an, this.l);
                                }
                            }
                            this.W = 2;
                            return;
                        } else {
                            return;
                        }
                    case 2:
                        b();
                        return;
                    default:
                        return;
                }
            case 5:
                s(anVar);
                return;
            case 6:
                i(anVar);
                return;
            case 7:
                y(anVar);
                return;
            case 8:
                u();
                return;
            default:
                return;
        }
    }

    private boolean q(int i2) {
        switch (i2) {
            case 0:
                for (int i3 = 0; i3 < this.p.length; i3++) {
                    if (this.dP[i3][0] != -1) {
                        return false;
                    }
                }
                break;
            case 1:
                for (byte b : eb.m) {
                    if (this.eq[b][0] != -1) {
                        return false;
                    }
                }
                break;
        }
        return true;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private int r(int i2, int i3) {
        short s2 = i3 == 0 ? this.ev[i2][7] : this.ev[i2][9];
        int i4 = s2;
        if (this.dP[i2][5] == -1) {
            i4 = s2 - ((s2 * 20) / 100);
        }
        int i5 = i4;
        if (this.dP[i2][4] == -1) {
            i5 = i4 - ((i4 * 30) / 100);
            byte[] bArr = this.er[i2];
            bArr[3] = (byte) (bArr[3] + 1);
            if (this.er[i2][3] == 2) {
                this.er[i2][3] = 0;
                this.dP[i2][4] = 0;
            }
        }
        return i5;
    }

    private void r() {
        switch (this.ah) {
            case 0:
                this.p[this.R].a(1);
                this.fw[1].a(15);
                this.fw[1].a(this.fv[this.j].aH, this.fv[this.j].dN + 2);
                this.fw[1].a(true);
                this.ah = 1;
                return;
            case 1:
                if (this.fw[1].ac > 1) {
                    this.ea = true;
                }
                if (this.fw[1].W()) {
                    this.fw[1].a(false);
                    this.W = 1;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void r(int i2) {
        if (ee.q(0, 99) >= 15) {
            this.ae = false;
        } else if (i2 == 0) {
            int w = w(this.ev[this.R][0]);
            this.S = (short) ee.q(0, r.ai[w][1].length - 1);
            this.am = (byte) r.ai[w][2][ee.q(0, r.ai[w][2].length - 1)];
            eb.cq().k(this.S, this.am);
            this.ae = true;
        } else {
            for (int i3 = 0; i3 < eb.fA.length; i3++) {
                int q2 = ee.q(0, eb.fA.length - 1);
                if (eb.fA[q2][1] != 0) {
                    this.S = eb.fA[q2][0];
                    if (r.ev[this.S][5] != -1) {
                        this.am = (byte) ee.q(1, eb.fA[q2][1]);
                        eb.cq().y(this.S, this.am);
                        this.ae = true;
                        return;
                    }
                    this.ae = false;
                } else {
                    this.ae = false;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void r(defpackage.an r10) {
        /*
            r9 = this;
            r3 = 5
            r8 = 4
            r7 = -1
            r6 = 1
            r5 = 0
            byte r0 = r9.W
            switch(r0) {
                case 0: goto L_0x000b;
                case 1: goto L_0x0018;
                case 2: goto L_0x0053;
                case 3: goto L_0x00af;
                case 4: goto L_0x018f;
                default: goto L_0x000a;
            }
        L_0x000a:
            return
        L_0x000b:
            a r0 = defpackage.a.M()
            byte r1 = r9.j
            byte[] r2 = defpackage.a.N
            int r2 = r2.length
            r0.c(r10, r1, r2, r6)
            goto L_0x000a
        L_0x0018:
            r9.p(r10)
            r0 = r5
        L_0x001c:
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 >= r1) goto L_0x000a
            byte[][] r1 = r9.eq
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            r1 = r1[r2]
            byte r1 = r1[r5]
            if (r1 == r7) goto L_0x0050
            ao r1 = r9.q
            n[] r2 = r9.fv
            r2 = r2[r0]
            int r2 = r2.aH
            int r2 = r2 + 15
            int r2 = r2 + 0
            byte r3 = defpackage.bi.e
            int r3 = r3 >> 2
            int r3 = r3 % 2
            int r3 = r3 << 1
            int r2 = r2 + r3
            n[] r3 = r9.fv
            r3 = r3[r0]
            int r3 = r3.dN
            r4 = 40
            int r3 = r3 - r4
            int r3 = r3 + 0
            r10.a(r1, r2, r3, r5)
        L_0x0050:
            int r0 = r0 + 1
            goto L_0x001c
        L_0x0053:
            a r0 = defpackage.a.M()
            byte r0 = r0.af
            r1 = 10
            if (r0 >= r1) goto L_0x0081
            r9.c(r10, r5)
            byte[] r0 = r9.m
            byte r1 = r9.Y
            byte r0 = r0[r1]
            r9.b(r10, r0)
            byte r0 = r9.Y
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r9.Y = r0
            byte[] r1 = r9.m
            int r1 = r1.length
            int r1 = r1 - r6
            if (r0 <= r1) goto L_0x007e
            byte[] r0 = r9.m
            int r0 = r0.length
            int r0 = r0 - r6
            byte r0 = (byte) r0
        L_0x007b:
            r9.Y = r0
            goto L_0x000a
        L_0x007e:
            byte r0 = r9.Y
            goto L_0x007b
        L_0x0081:
            byte r0 = r9.fP
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r9.fP = r0
            byte r0 = r9.fP
            r1 = 25
            if (r0 <= r1) goto L_0x00a0
            r9.fP = r5
            r9.W = r5
            a r0 = defpackage.a.M()
            r0.af = r5
            ea r0 = defpackage.ea.cp()
            r0.ae = r5
            goto L_0x000a
        L_0x00a0:
            ed r0 = defpackage.ed.cB()
            java.lang.String r1 = "所有人生命已满"
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x000a
        L_0x00af:
            n[] r0 = r9.fv
            byte r1 = r9.j
            r0 = r0[r1]
            r0.a(r5)
            r0 = r5
        L_0x00b9:
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x0102
            r0 = r5
        L_0x00bf:
            short[][] r1 = r9.fF
            int r1 = r1.length
            if (r0 < r1) goto L_0x014c
            r9.a(r6)
            r9.W = r5
            r9.bf()
            a r0 = defpackage.a.M()
            r0.af = r5
            ea r0 = defpackage.ea.cp()
            r0.ae = r5
            r0 = r5
        L_0x00d9:
            n[] r1 = r9.fv
            int r1 = r1.length
            if (r0 < r1) goto L_0x0161
        L_0x00de:
            java.lang.String[] r0 = defpackage.bj.aC
            byte[] r1 = defpackage.eb.m
            byte r2 = r9.j
            byte r1 = r1[r2]
            r0 = r0[r1]
            r9.aG = r0
            byte r0 = r9.X
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r9.X = r0
            byte r0 = r9.X
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x000a
            r0 = 3
            r9.a(r0)
            r9.X = r5
            r9.j = r5
            goto L_0x000a
        L_0x0102:
            byte[][] r1 = r9.eq
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            r1 = r1[r2]
            byte r1 = r1[r5]
            if (r1 == r7) goto L_0x0148
            short[] r1 = defpackage.eb.ab
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            short r3 = r1[r2]
            a r4 = defpackage.a.M()
            short[] r4 = r4.ab
            short r4 = r4[r0]
            int r3 = r3 + r4
            short r3 = (short) r3
            r1[r2] = r3
            short[] r1 = defpackage.eb.ab
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            short r1 = r1[r2]
            short[][] r2 = defpackage.eb.ev
            byte[] r3 = defpackage.eb.m
            byte r3 = r3[r0]
            r2 = r2[r3]
            short r2 = r2[r8]
            if (r1 < r2) goto L_0x0148
            short[] r1 = defpackage.eb.ab
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            short[][] r3 = defpackage.eb.ev
            byte[] r4 = defpackage.eb.m
            byte r4 = r4[r0]
            r3 = r3[r4]
            short r3 = r3[r8]
            r1[r2] = r3
        L_0x0148:
            int r0 = r0 + 1
            goto L_0x00b9
        L_0x014c:
            r1 = r5
        L_0x014d:
            short[][] r2 = r9.fF
            r2 = r2[r0]
            int r2 = r2.length
            if (r1 < r2) goto L_0x0158
            int r0 = r0 + 1
            goto L_0x00bf
        L_0x0158:
            short[][] r2 = r9.fF
            r2 = r2[r0]
            r2[r1] = r5
            int r1 = r1 + 1
            goto L_0x014d
        L_0x0161:
            byte r1 = r9.j
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r9.j = r1
            n[] r2 = r9.fv
            int r2 = r2.length
            int r2 = r2 - r6
            byte r2 = (byte) r2
            if (r1 <= r2) goto L_0x018c
            r1 = r5
        L_0x0170:
            r9.j = r1
            byte[][] r1 = r9.eq
            byte[] r2 = defpackage.eb.m
            byte r3 = r9.j
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte r1 = r1[r5]
            if (r1 != r7) goto L_0x00de
            byte r1 = r9.X
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r9.X = r1
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x00d9
        L_0x018c:
            byte r1 = r9.j
            goto L_0x0170
        L_0x018f:
            a r0 = defpackage.a.M()
            byte r1 = r9.j
            byte[] r2 = defpackage.a.N
            int r2 = r2.length
            r0.c(r10, r1, r2, r6)
            byte r0 = r9.fP
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r9.fP = r0
            byte r0 = r9.fP
            r1 = 25
            if (r0 >= r1) goto L_0x01b7
            java.lang.String r0 = "真气不够"
            ed r1 = defpackage.ed.cB()
            char[] r0 = r0.toCharArray()
            r1.a(r3, r0)
            goto L_0x000a
        L_0x01b7:
            r9.fP = r5
            r9.W = r5
            ea r0 = defpackage.ea.cp()
            r0.ae = r5
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bg.r(an):void");
    }

    private static int s(int i2, int i3) {
        byte b = eb.m[i2];
        return i3 == 0 ? eb.ev[b][5] : eb.ev[b][7];
    }

    private void s() {
        switch (this.ah) {
            case 0:
                this.p[this.R].a(1);
                this.fw[1].a(24);
                this.fw[1].a(true);
                this.fw[1].j(this.fv[this.j].aH - 100, this.fv[this.j].dN - 120);
                this.fw[1].k(this.fv[this.j].aH - 8, this.fv[this.j].dN + 1);
                this.fw[1].W = (byte) 18;
                this.ah = 1;
                return;
            case 1:
                this.fw[1].f(1);
                if (this.fw[1].am()) {
                    this.ea = true;
                    this.af = 2;
                    this.fw[1].a(16);
                    this.fw[1].a(this.fv[this.j].aH, this.fv[this.j].dN + 2);
                    this.ah = 2;
                    return;
                }
                return;
            case 2:
                if (this.fw[1].W()) {
                    this.W = 1;
                    this.fw[1].a(false);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void s(int i2) {
        if (ee.q(0, 99) < 30) {
            this.dX = true;
            if (i2 == 0) {
                this.dP[this.R][5] = -1;
            } else {
                this.eq[eb.m[this.j]][5] = -1;
            }
        } else {
            this.dX = false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void s(defpackage.an r9) {
        /*
            r8 = this;
            r7 = 40
            r6 = -1
            r5 = 1
            r4 = 0
            byte r0 = r8.W
            switch(r0) {
                case 0: goto L_0x000b;
                case 1: goto L_0x0013;
                case 2: goto L_0x008d;
                default: goto L_0x000a;
            }
        L_0x000a:
            return
        L_0x000b:
            a r0 = defpackage.a.M()
            r0.d(r9)
            goto L_0x000a
        L_0x0013:
            r8.p(r9)
            byte r0 = defpackage.a.W
            if (r0 != 0) goto L_0x0055
            ao r0 = r8.q
            n[] r1 = r8.fv
            byte r2 = r8.ap
            r1 = r1[r2]
            int r1 = r1.aH
            int r1 = r1 + 15
            byte r2 = defpackage.bi.e
            int r2 = r2 >> 2
            int r2 = r2 % 2
            int r2 = r2 << 1
            int r1 = r1 + r2
            n[] r2 = r8.fv
            byte r3 = r8.ap
            r2 = r2[r3]
            int r2 = r2.dN
            int r2 = r2 - r7
            r9.a(r0, r1, r2, r4)
            ao r0 = r8.u
            byte r1 = r8.ap
            int r1 = r1 * 75
            int r1 = r1 + 45
            r2 = 245(0xf5, float:3.43E-43)
            r9.a(r0, r1, r2, r4)
            java.lang.String[] r0 = defpackage.bj.aC
            byte[] r1 = defpackage.eb.m
            byte r2 = r8.ap
            byte r1 = r1[r2]
            r0 = r0[r1]
            r8.aG = r0
            goto L_0x000a
        L_0x0055:
            r0 = r4
        L_0x0056:
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 >= r1) goto L_0x000a
            byte[][] r1 = r8.eq
            byte[] r2 = defpackage.eb.m
            byte r3 = r8.j
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte r1 = r1[r4]
            if (r1 == r6) goto L_0x008a
            ao r1 = r8.q
            n[] r2 = r8.fv
            r2 = r2[r0]
            int r2 = r2.aH
            int r2 = r2 + 15
            int r2 = r2 + 0
            byte r3 = defpackage.bi.e
            int r3 = r3 >> 2
            int r3 = r3 % 2
            int r3 = r3 << 1
            int r2 = r2 + r3
            n[] r3 = r8.fv
            r3 = r3[r0]
            int r3 = r3.dN
            int r3 = r3 - r7
            int r3 = r3 + 0
            r9.a(r1, r2, r3, r4)
        L_0x008a:
            int r0 = r0 + 1
            goto L_0x0056
        L_0x008d:
            byte r0 = r8.fP
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r8.fP = r0
            byte r0 = r8.fP
            r1 = 26
            if (r0 >= r1) goto L_0x00a5
            a r0 = defpackage.a.M()
            byte r1 = r8.ap
            r0.c(r1)
            goto L_0x000a
        L_0x00a5:
            a r0 = defpackage.a.M()
            byte r0 = r0.af
            r1 = 10
            if (r0 > r1) goto L_0x00e9
            eb r0 = defpackage.eb.cq()
            short r1 = defpackage.a.P
            r0.y(r1, r5)
            a r0 = defpackage.a.M()
            byte r1 = r8.ap
            r0.a(r1)
            short[][] r0 = defpackage.r.ev
            short r1 = defpackage.a.P
            r0 = r0[r1]
            r1 = 5
            short r0 = r0[r1]
            byte r0 = (byte) r0
            if (r0 != r5) goto L_0x00e9
            r0 = r4
        L_0x00ce:
            n[] r1 = r8.fv
            int r1 = r1.length
            if (r0 < r1) goto L_0x0104
        L_0x00d3:
            byte r0 = r8.X
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r8.X = r0
            byte r0 = r8.X
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x00e9
            r0 = 3
            r8.a(r0)
            r8.X = r4
            r8.j = r4
        L_0x00e9:
            r8.fP = r4
            ea r0 = defpackage.ea.cp()
            r0.ae = r4
            r8.a(r5)
            java.lang.String[] r0 = defpackage.bj.aC
            byte[] r1 = defpackage.eb.m
            byte r2 = r8.j
            byte r1 = r1[r2]
            r0 = r0[r1]
            r8.aG = r0
            r8.W = r4
            goto L_0x000a
        L_0x0104:
            byte r1 = r8.j
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r8.j = r1
            n[] r2 = r8.fv
            int r2 = r2.length
            int r2 = r2 - r5
            byte r2 = (byte) r2
            if (r1 <= r2) goto L_0x012e
            r1 = r4
        L_0x0113:
            r8.j = r1
            byte[][] r1 = r8.eq
            byte[] r2 = defpackage.eb.m
            byte r3 = r8.j
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte r1 = r1[r4]
            if (r1 != r6) goto L_0x00d3
            byte r1 = r8.X
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r8.X = r1
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x00ce
        L_0x012e:
            byte r1 = r8.j
            goto L_0x0113
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bg.s(an):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private int t(int i2, int i3) {
        byte b = eb.m[i2];
        short s2 = i3 == 0 ? eb.ev[b][6] : eb.ev[b][8];
        int i4 = s2;
        if (this.eq[b][5] == -1) {
            i4 = s2 - ((s2 * 20) / 100);
        }
        int i5 = i4;
        if (this.eq[b][4] == -1) {
            i5 = i4 - ((i4 * 30) / 100);
            byte[] bArr = this.ax;
            bArr[b] = (byte) (bArr[b] + 1);
            if (this.ax[b] == 2) {
                this.ax[b] = 0;
                this.eq[b][4] = 0;
            }
        }
        return i5;
    }

    private void t(int i2) {
        this.aG = new StringBuffer().append(this.aC[i2]).append("[等级:").append((int) this.ev[i2][1]).append("][生命:").append((int) this.ev[i2][4]).append("]").toString();
    }

    private void t(an anVar) {
        switch (this.ah) {
            case 0:
            case 1:
            case 2:
                anVar.setColor(16711680);
                break;
            case 3:
            case 4:
            case 5:
                anVar.setColor(16777215);
                break;
            case 10:
                this.ah = 0;
                this.W = 1;
                break;
        }
        anVar.c(0, 0, 240, 320);
        this.ah = (byte) (this.ah + 1);
    }

    private void u() {
        this.fP = (byte) (this.fP + 1);
        if (this.fP < 25) {
            String str = null;
            switch (this.fQ) {
                case 1:
                    str = "逃跑失败!";
                    break;
                case 2:
                    str = "逃跑成功!";
                    break;
            }
            ed.cB().a(5, str.toCharArray());
            return;
        }
        switch (this.fQ) {
            case 1:
                a((byte) 3);
                break;
            case 2:
                for (int i2 = 0; i2 < eb.m.length; i2++) {
                    if (this.eq[eb.m[i2]][0] == -1) {
                        this.eq[eb.m[i2]][0] = 0;
                        eb.ab[eb.m[i2]] = 10;
                    }
                }
                c();
                a((byte) 0);
                ea.cp().a(1);
                bh.c();
                bh.a(bh.g);
                break;
        }
        this.fP = 0;
        this.X = 0;
        ea.cp().ae = false;
    }

    private void u(int i2) {
        this.aG = "";
        this.f = (byte) i2;
    }

    private void u(an anVar) {
        boolean z;
        switch (this.ah) {
            case 0:
                this.p[this.R].a(1);
                for (int i2 = 0; i2 < this.fy.length / 2; i2++) {
                    this.fy[i2].a(3);
                    this.fy[i2].a(this.fv[i2].aH, this.fv[i2].dN + 2);
                    this.fy[i2].a(true);
                    this.fy[(this.fy.length / 2) + i2].a(13);
                    this.fy[(this.fy.length / 2) + i2].a(this.fv[i2].aH, this.fv[i2].dN + 2);
                    this.fy[(this.fy.length / 2) + i2].a(true);
                }
                this.ea = true;
                this.af = 1;
                this.ah = 1;
                return;
            case 1:
                for (int i3 = 0; i3 < this.fy.length / 2; i3++) {
                    if (this.eq[eb.m[i3]][0] != -1) {
                        this.fy[i3].c(anVar);
                        this.fy[i3].Q();
                        this.fy[(this.fy.length / 2) + i3].c(anVar);
                        this.fy[(this.fy.length / 2) + i3].Q();
                    } else {
                        this.fy[(this.fy.length / 2) + i3].Q();
                    }
                }
                int i4 = 0;
                while (true) {
                    if (i4 >= this.fy.length / 2) {
                        z = true;
                    } else if (!this.fy[(this.fy.length / 2) + i4].W()) {
                        z = false;
                    } else {
                        i4++;
                    }
                }
                if (z) {
                    this.fy = null;
                    this.W = 1;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void v(int i2) {
        this.fI = (byte) i2;
        switch (i2) {
            case 0:
                this.aG = "逃跑";
                return;
            case 1:
                this.aG = "道具";
                return;
            case 2:
                this.aG = "商店";
                return;
            case 3:
                this.aG = "技能";
                return;
            case 4:
                this.aG = "攻击";
                return;
            case 5:
                this.aG = "凤火飞舞(上或2键普通选项)";
                return;
            default:
                return;
        }
    }

    private void v(an anVar) {
        switch (this.ah) {
            case 0:
                this.fL = new int[]{0, 2, 3, -2, -3, 3};
                this.fM = new int[]{0, 2, 3, -3, -1, 3};
                this.p[this.R].a(1);
                this.ah = 1;
                this.ea = true;
                this.af = 1;
                return;
            case 1:
                if (this.p[this.R].W()) {
                    this.p[this.R].a(0);
                }
                for (int i2 = 0; i2 < this.fy.length; i2++) {
                    if (this.eq[eb.m[i2]][0] != -1) {
                        this.fy[i2].a(this.fv[i2].aH + this.ac, (this.fv[i2].dN - 10) + this.dN);
                        this.ac = this.fL[ee.q(0, this.fL.length - 1)] + this.ac;
                        this.dN = this.fM[ee.q(0, this.fM.length - 1)] + this.dN;
                        this.fy[i2].a(true);
                        this.fy[i2].c(anVar);
                        this.fy[i2].Q();
                    }
                }
                this.al = (byte) (this.al + 1);
                if (this.al > this.fL.length - 1) {
                    this.al = 0;
                    this.ac = 0;
                    this.dN = 0;
                    this.ak = (byte) (this.ak + 1);
                    if (this.ak > 3) {
                        this.W = 1;
                        this.ak = 0;
                        this.fy = null;
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private static int w(int i2) {
        for (int i3 = 0; i3 < r.ai.length; i3++) {
            for (short s2 : r.ai[i3][0]) {
                if (i2 == ((byte) s2)) {
                    return i3;
                }
            }
        }
        return -1;
    }

    private void w() {
        this.af = 0;
        this.dZ = false;
        this.ea = false;
        this.k = 0;
        this.fR = 0;
    }

    private void w(an anVar) {
        boolean z;
        switch (this.ah) {
            case 0:
                this.p[this.R].a(1);
                for (int i2 = 0; i2 < this.fy.length / 2; i2++) {
                    this.fy[i2].a(17);
                    this.fy[i2].a(true);
                    this.fy[(this.fy.length / 2) + i2].a(18);
                    this.fy[(this.fy.length / 2) + i2].a(true);
                    this.fy[(this.fy.length / 2) + i2].a(this.fv[i2].aH, this.fv[i2].dN + 2);
                }
                this.fN = new int[(this.fy.length / 2)];
                this.ah = 1;
                return;
            case 1:
                for (int i3 = 0; i3 < this.fy.length / 2; i3++) {
                    if (!this.fy[i3].M) {
                        if (this.eq[eb.m[i3]][0] != -1) {
                            this.fy[i3].a(this.fv[i3].aH, this.fN[i3]);
                            this.fy[i3].c(anVar);
                            this.fy[i3].Q();
                        } else {
                            this.fy[i3].M = true;
                        }
                    }
                    if (this.fN[i3] < this.fv[i3].dN - 10 && this.eq[eb.m[i3]][0] != -1) {
                        this.fy[(this.fy.length / 2) + i3].c(anVar);
                        this.fy[(this.fy.length / 2) + i3].Q();
                    }
                    if (!this.fy[i3].em) {
                        int[] iArr = this.fN;
                        iArr[i3] = iArr[i3] + 18;
                        if (this.fN[i3] > this.fv[i3].dN) {
                            if (this.eq[eb.m[i3]][0] != -1) {
                                this.fy[i3].a(19);
                                this.fy[i3].em = true;
                                this.fN[i3] = this.fv[i3].dN + 2;
                            } else {
                                this.fy[i3].em = true;
                            }
                        }
                    }
                    if (aX()) {
                        this.ea = true;
                        this.af = 1;
                        if (this.fy[i3].W()) {
                            this.fy[i3].a(false);
                        }
                    }
                }
                if (aX()) {
                    int i4 = 0;
                    while (true) {
                        if (i4 >= this.fy.length / 2) {
                            z = true;
                        } else if (!this.fy[i4].W()) {
                            z = false;
                        } else {
                            i4++;
                        }
                    }
                    if (z) {
                        this.W = 1;
                        this.fy = null;
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void x(an anVar) {
        switch (this.ah) {
            case 0:
                this.p[this.R].a(2);
                this.fw[1].a(20);
                this.fw[1].a(true);
                for (n a : this.fy) {
                    a.a(22);
                }
                this.ah = 1;
                return;
            case 1:
                this.fw[1].a(this.p[this.R].aH + 30 + this.ac, this.p[this.R].dN + 15);
                for (int i2 = 0; i2 < 122; i2++) {
                    for (int i3 = 0; i3 < this.fy.length; i3++) {
                        this.fy[i3].a(((-i2) * 2) + this.aH, (i3 * 70) + 80);
                        this.fy[i3].c(anVar);
                        this.fy[i3].Q();
                    }
                }
                this.ac += 35;
                if (this.p[this.R].aH + 30 + this.ac > 240) {
                    this.aH += 40;
                }
                if (this.p[this.R].aH + this.ac > 240) {
                    this.fw[1].a(false);
                }
                if (this.aH - 242 > 80) {
                    for (n a2 : this.fy) {
                        this.ea = true;
                        this.af = 4;
                        a2.a(23);
                    }
                }
                if (this.aH - 242 > 240) {
                    this.W = 1;
                    this.fy = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void y(an anVar) {
        switch (this.W) {
            case 0:
                z(anVar);
                return;
            case 1:
                z(anVar);
                a.M().b(anVar, 9923675, 70, 150, 100, 48);
                for (int i2 = 0; i2 < 2; i2++) {
                    a.M().a(anVar, 16777215, 5, i2 + 2, 95, (i2 * 18) + 158);
                }
                du.a(anVar, String.valueOf((int) a.k), 130, 155, 0, 16777215);
                du.a(anVar, String.valueOf(a.av[a.h] * a.k), 120, 173, 0, 16777215);
                return;
            case 2:
                z(anVar);
                this.fP = (byte) (this.fP + 1);
                if (this.fP < 25) {
                    String str = null;
                    switch (this.fQ) {
                        case 1:
                            str = "购买成功!";
                            break;
                        case 2:
                            str = "金钱不够!";
                            break;
                    }
                    ed.cB().a(5, str.toCharArray());
                    return;
                }
                this.W = 0;
                this.fP = 0;
                ea.cp().ae = false;
                if (as < 2 && this.fQ == 2) {
                    b.Z().a(2);
                    b.Z().b(b.Z().L, 2);
                    ea.cp().a(7);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private static void z(an anVar) {
        a.M().b(anVar, 0);
        a.M().b(anVar, 9728372, 95, 70, 50, 22);
        a.M().a(anVar, 16777215, 5, 0, 106, 75);
        a.M().b(anVar, 3548436, 40, 95, 157, 72);
        a.M().a(anVar, 7755858, 10914680, 3, a.N.length, a.l, a.aj, 46, 100, 146, 20);
        a.M().f(anVar);
        int i2 = a.aj;
        while (true) {
            int i3 = i2;
            if (i3 < a.aj + a.al && i3 <= a.am - 1) {
                int i4 = a.h == i3 ? 16777215 : 5849662;
                du.a(anVar, r.aC[a.N[i3]], 80, ((i3 - a.aj) * 21) + 120, 33, i4);
                du.a(anVar, String.valueOf((int) a.av[i3]), 145, ((i3 - a.aj) * 21) + 122, 33, i4);
                i2 = i3 + 1;
            }
        }
        a.M();
        a.a(anVar, 9071433, 11765343, 32, 168, 175, 65);
        a.M();
        a.a(anVar, 2035968, r.aE[a.P], 40, 169, 155, 0);
    }

    public final void N() {
    }

    public final void P() {
        bi.bo().gp = o.an();
        o.an().e = 0;
        for (byte b = 0; b < eb.m.length; b = (byte) (b + 1)) {
            if (eb.ab[eb.m[b]] <= 0) {
                eb.ab[eb.m[b]] = 10;
                this.fv[b].a(0);
            }
        }
        bh.c();
        bh.a(0);
        this.Y = 0;
        this.X = 0;
        this.j = 0;
        c();
        as = 0;
        a((byte) 0);
    }

    public final void Q() {
        int i2;
        switch (this.e) {
            case 0:
                switch (this.f) {
                    case 0:
                        bi.bo().dX = true;
                        bi.bo().dY = false;
                        ea.cp().ae = false;
                        bh.c();
                        break;
                    case 5:
                        this.R = 0;
                        this.j = 0;
                        bj.e = bj.L[bj.ac];
                        if (as < 2) {
                            i2 = eb.m.length;
                        } else {
                            eb.m = new byte[]{3, 4, 5};
                            i2 = 3;
                        }
                        this.fv = new n[i2];
                        this.fA = (short[][]) Array.newInstance(Short.TYPE, i2, 2);
                        for (int i3 = 0; i3 < i2; i3++) {
                            byte b = eb.m[i3];
                            this.fA[i3][0] = (short) (this.fD[this.fv.length - 1][i3][0] + 120);
                            this.fA[i3][1] = (short) (this.fD[this.fv.length - 1][i3][1] + 160);
                            if (as < 2) {
                                this.fv[i3] = c.g(new StringBuffer().append("/u/h").append((int) b).toString());
                            } else if (i3 == 0) {
                                this.fv[i3] = c.g("/u/df");
                            } else {
                                this.fv[i3] = n.a(this.fv[0]);
                            }
                            this.s = ee.O("/u/fh");
                            this.t = a.b(this.s);
                            this.fv[i3].a(this.fA[i3][0], this.fA[i3][1]);
                            this.fv[i3].g(this.fA[i3][0], this.fA[i3][1]);
                            eb.cq();
                            eb.c(b);
                            if (as == 2) {
                                eb.ab[b] = eb.ev[b][4];
                                eb.aq[b] = eb.ev[b][3];
                            }
                            if (eb.ab[b] > eb.ev[b][4]) {
                                eb.ab[b] = eb.ev[b][4];
                            }
                            if (eb.aq[b] > eb.ev[b][3]) {
                                eb.aq[b] = eb.ev[b][3];
                            }
                        }
                        aY();
                        break;
                    case 6:
                        this.fw = new c[5];
                        this.fw[0] = c.g("/u/skill_5");
                        this.fw[0].a(false);
                        this.fw[1] = c.g("/u/skill_1");
                        this.fw[1].a(false);
                        this.fw[2] = c.g("/u/skill_4");
                        this.fw[2].a(false);
                        this.fw[3] = c.g("/u/skill_2");
                        this.fw[3].a(false);
                        this.fw[4] = c.g("/u/skill_3");
                        this.fw[4].a(false);
                        break;
                    case 7:
                        bn();
                        break;
                    case 8:
                        bm();
                        break;
                    case 9:
                        this.fx = new n[(this.p.length + this.fv.length + this.fw.length)];
                        byte b2 = 0;
                        for (n nVar : this.p) {
                            this.fx[b2] = nVar;
                            b2 = (byte) (b2 + 1);
                        }
                        for (n nVar2 : this.fv) {
                            this.fx[b2] = nVar2;
                            b2 = (byte) (b2 + 1);
                        }
                        for (n nVar3 : this.fw) {
                            this.fx[b2] = nVar3;
                            b2 = (byte) (b2 + 1);
                        }
                        U();
                        System.gc();
                        break;
                    case 10:
                        bi.bo().dX = false;
                        bh.a(gi);
                        a((byte) 10);
                        return;
                }
                this.f = (byte) (this.f + 1);
                return;
            case 1:
                if (!this.gc) {
                    bc();
                    return;
                }
                return;
            case 2:
                switch (this.f) {
                    case 0:
                        if (this.eq[eb.m[this.j]][3] != -1) {
                            a(1);
                            return;
                        } else {
                            this.gc = true;
                            return;
                        }
                    case 1:
                        bd();
                        return;
                    case 2:
                        be();
                        return;
                    case 3:
                        bg();
                        return;
                    case 4:
                    case 6:
                    case 8:
                    default:
                        return;
                    case 5:
                        bj();
                        return;
                    case 7:
                        switch (this.W) {
                            case 0:
                                if (bi.y(8)) {
                                    byte b3 = (byte) (a.h - 1);
                                    a.h = b3;
                                    a.h = b3 < 0 ? 0 : a.h;
                                    a.P = (short) a.N[a.h];
                                    short s2 = (short) (a.l - 1);
                                    a.l = s2;
                                    a.l = s2 < 0 ? 0 : a.l;
                                    a.M();
                                    a.o();
                                    return;
                                } else if (bi.y(4)) {
                                    byte b4 = (byte) (a.h + 1);
                                    a.h = b4;
                                    a.h = b4 > ((byte) (a.N.length - 1)) ? (byte) (a.N.length - 1) : a.h;
                                    a.P = (short) a.N[a.h];
                                    if (a.al > 0) {
                                        short s3 = (short) (a.l + 1);
                                        a.l = s3;
                                        a.l = s3 > ((short) (a.al - 1)) ? (short) (a.al - 1) : a.l;
                                        a.M();
                                        a.U();
                                        return;
                                    }
                                    return;
                                } else if (bi.y(MIDIControl.NOTE_ON)) {
                                    a.k = 1;
                                    this.W = 1;
                                    return;
                                } else if (bi.y(1280)) {
                                    bi();
                                    a(0);
                                    return;
                                } else {
                                    return;
                                }
                            case 1:
                                if (bi.y(2)) {
                                    short s4 = (short) (a.k - 1);
                                    a.k = s4;
                                    a.k = s4 <= 0 ? 1 : a.k;
                                    return;
                                } else if (bi.y(1)) {
                                    a.k = (short) (a.k + 1);
                                    return;
                                } else if (bi.y(MIDIControl.NOTE_ON)) {
                                    if (eb.ac >= a.av[a.h] * a.k) {
                                        eb.cq().k(a.N[a.h], a.k);
                                        eb.ac -= a.av[a.h] * a.k;
                                        this.fQ = 1;
                                    } else {
                                        this.fQ = 2;
                                    }
                                    this.W = 2;
                                    return;
                                } else if (bi.y(1280)) {
                                    this.W = 0;
                                    return;
                                } else {
                                    return;
                                }
                            default:
                                return;
                        }
                    case 9:
                        if (bi.y(16)) {
                            ea.cp().ae = false;
                            a(1);
                            this.fJ = false;
                            v(this.ay[0]);
                            return;
                        }
                        return;
                }
            case 3:
                bk();
                return;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                return;
            case 10:
                bb();
                return;
        }
    }

    public final void R() {
        this.I = new ao[2];
        for (int i2 = 0; i2 < this.I.length; i2++) {
            this.I[i2] = ee.O(new StringBuffer().append("/u/lo").append(i2).toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:117:0x020b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01dc  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0262  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(byte r14) {
        /*
            r13 = this;
            r12 = 5
            r11 = 3
            r10 = 2
            r9 = 1
            r8 = 0
            switch(r14) {
                case 1: goto L_0x000d;
                case 5: goto L_0x004b;
                case 10: goto L_0x0047;
                default: goto L_0x0008;
            }
        L_0x0008:
            r13.e = r14
            r13.f = r8
            return
        L_0x000d:
            r0 = r8
        L_0x000e:
            n[] r1 = r13.fv
            int r1 = r1.length
            if (r0 < r1) goto L_0x0020
        L_0x0013:
            java.lang.String[] r0 = defpackage.bj.aC
            byte[] r1 = defpackage.eb.m
            byte r2 = r13.j
            byte r1 = r1[r2]
            r0 = r0[r1]
            r13.aG = r0
            goto L_0x0008
        L_0x0020:
            byte r1 = r13.j
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r13.j = r1
            n[] r2 = r13.fv
            int r2 = r2.length
            int r2 = r2 - r9
            byte r2 = (byte) r2
            if (r1 <= r2) goto L_0x0044
            r1 = r8
        L_0x002f:
            r13.j = r1
            byte[][] r1 = r13.eq
            byte[] r2 = defpackage.eb.m
            byte r3 = r13.j
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte r1 = r1[r8]
            r2 = -1
            if (r1 != r2) goto L_0x0013
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x000e
        L_0x0044:
            byte r1 = r13.j
            goto L_0x002f
        L_0x0047:
            r13.J()
            goto L_0x0008
        L_0x004b:
            boolean r0 = r13.M
            if (r0 == 0) goto L_0x0394
            java.lang.String r0 = "战斗胜利"
            r13.aG = r0
            r0 = 40
            r1 = 4
            int[] r0 = new int[]{r0, r1}
            java.lang.Class r1 = java.lang.Byte.TYPE
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            byte[][] r0 = (byte[][]) r0
            r13.dR = r0
            r13.i = r8
            r13.ec = r8
            r0 = r8
        L_0x0069:
            n[] r1 = r13.p
            int r1 = r1.length
            if (r0 < r1) goto L_0x0100
            byte[] r0 = defpackage.b.m
            r1 = 4
            byte r0 = r0[r1]
            if (r0 >= 0) goto L_0x0110
            byte[] r0 = defpackage.b.m
            byte r0 = r0[r12]
            if (r0 < 0) goto L_0x0110
            int r0 = r13.ec
            int r0 = r0 * 2
            r13.ec = r0
        L_0x0081:
            r0 = r8
        L_0x0082:
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x0141
            r0 = r8
            r1 = r8
        L_0x0089:
            n[] r2 = r13.p
            int r2 = r2.length
            if (r0 < r2) goto L_0x02fd
            defpackage.eb.cq()
            defpackage.eb.a(r1)
            byte[][] r0 = r13.dR
            byte r2 = r13.i
            r0 = r0[r2]
            r0[r8] = r12
            byte[][] r0 = r13.dR
            byte r2 = r13.i
            r0 = r0[r2]
            int r2 = r1 >> 8
            r2 = r2 & 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2
            r0[r9] = r2
            byte[][] r0 = r13.dR
            byte r2 = r13.i
            r0 = r0[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            byte r1 = (byte) r1
            r0[r10] = r1
            byte r0 = r13.i
            int r0 = r0 + 1
            byte r0 = (byte) r0
            r13.i = r0
            r0 = 99
            int r0 = defpackage.ee.q(r8, r0)
            r1 = 25
            if (r0 >= r1) goto L_0x0008
            byte r0 = defpackage.dw.ak
            int r0 = w(r0)
            short[][][] r1 = defpackage.r.ai
            r1 = r1[r0]
            r1 = r1[r9]
            int r1 = r1.length
            int r1 = defpackage.ee.q(r9, r1)
            switch(r1) {
                case 1: goto L_0x00db;
                case 2: goto L_0x0309;
                case 3: goto L_0x036f;
                default: goto L_0x00d9;
            }
        L_0x00d9:
            goto L_0x0008
        L_0x00db:
            short[][][] r1 = defpackage.r.ai
            r1 = r1[r0]
            r1 = r1[r9]
            int r1 = r1.length
            int r1 = r1 - r9
            int r1 = defpackage.ee.q(r8, r1)
            short[][][] r2 = defpackage.r.ai
            r2 = r2[r0]
            r2 = r2[r10]
            int r2 = r2.length
            int r2 = r2 - r9
            int r2 = defpackage.ee.q(r8, r2)
            short[][][] r3 = defpackage.r.ai
            r0 = r3[r0]
            r0 = r0[r10]
            short r0 = r0[r2]
            r13.h(r1, r0)
            goto L_0x0008
        L_0x0100:
            int r1 = r13.ec
            short[][] r2 = r13.ev
            r2 = r2[r0]
            short r2 = r2[r10]
            int r1 = r1 + r2
            r13.ec = r1
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x0069
        L_0x0110:
            byte[] r0 = defpackage.b.m
            r1 = 4
            byte r0 = r0[r1]
            if (r0 >= 0) goto L_0x012c
            byte[] r0 = defpackage.b.m
            byte r0 = r0[r12]
            if (r0 >= 0) goto L_0x012c
            byte[] r0 = defpackage.b.m
            r1 = 6
            byte r0 = r0[r1]
            if (r0 < 0) goto L_0x012c
            int r0 = r13.ec
            int r0 = r0 * 3
            r13.ec = r0
            goto L_0x0081
        L_0x012c:
            byte[] r0 = defpackage.b.m
            byte r0 = r0[r12]
            if (r0 >= 0) goto L_0x0081
            byte[] r0 = defpackage.b.m
            r1 = 6
            byte r0 = r0[r1]
            if (r0 >= 0) goto L_0x0081
            int r0 = r13.ec
            int r0 = r0 * 4
            r13.ec = r0
            goto L_0x0081
        L_0x0141:
            byte[] r1 = defpackage.eb.m
            byte r1 = r1[r0]
            byte[][] r2 = r13.eq
            r2 = r2[r1]
            byte r2 = r2[r8]
            r3 = -1
            if (r2 != r3) goto L_0x017b
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            r2[r8] = r8
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            byte r1 = (byte) r1
            r2[r9] = r1
            byte[][] r1 = r13.dR
            byte r2 = r13.i
            r1 = r1[r2]
            r1[r10] = r8
            byte[][] r1 = r13.dR
            byte r2 = r13.i
            r1 = r1[r2]
            r1[r11] = r8
            byte r1 = r13.i
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r13.i = r1
        L_0x0176:
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x0082
        L_0x017b:
            short[] r2 = defpackage.eb.O
            short r2 = r2[r1]
            r3 = 50
            if (r2 >= r3) goto L_0x0234
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            r2[r8] = r8
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            byte r3 = (byte) r1
            r2[r9] = r3
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            int r3 = r13.ec
            int r3 = r3 >> 8
            r3 = r3 & 255(0xff, float:3.57E-43)
            byte r3 = (byte) r3
            r2[r10] = r3
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            int r3 = r13.ec
            r3 = r3 & 255(0xff, float:3.57E-43)
            byte r3 = (byte) r3
            r2[r11] = r3
            byte r2 = r13.i
            int r2 = r2 + 1
            byte r2 = (byte) r2
            r13.i = r2
            int[] r2 = defpackage.eb.aI
            r3 = r2[r1]
            int r4 = r13.ec
            int r3 = r3 + r4
            r2[r1] = r3
        L_0x01c0:
            r13.gj = r8
            byte[][] r2 = r13.eq
            r2 = r2[r1]
            byte r2 = r2[r8]
            r3 = -1
            if (r2 == r3) goto L_0x020b
            short[] r2 = defpackage.eb.O
            short r2 = r2[r1]
            r3 = 50
            if (r2 >= r3) goto L_0x020b
            r2 = r8
        L_0x01d4:
            r3 = 50
            if (r2 < r3) goto L_0x0239
        L_0x01d8:
            boolean r2 = r13.gk
            if (r2 == 0) goto L_0x0203
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            r2[r8] = r9
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            byte r3 = (byte) r1
            r2[r9] = r3
            byte[][] r2 = r13.dR
            byte r3 = r13.i
            r2 = r2[r3]
            short[] r3 = defpackage.eb.O
            short r3 = r3[r1]
            byte r3 = (byte) r3
            r2[r10] = r3
            byte r2 = r13.i
            int r2 = r2 + 1
            byte r2 = (byte) r2
            r13.i = r2
            r13.gk = r8
        L_0x0203:
            r2 = r9
        L_0x0204:
            byte[][] r3 = defpackage.bj.dR
            r3 = r3[r1]
            int r3 = r3.length
            if (r2 < r3) goto L_0x0262
        L_0x020b:
            byte[][] r2 = defpackage.bj.eq
            r3 = r8
        L_0x020e:
            r4 = r2[r1]
            int r4 = r4.length
            if (r3 < r4) goto L_0x02af
            boolean r2 = r13.gj
            if (r2 == 0) goto L_0x0176
            defpackage.eb.cq()
            defpackage.eb.c(r1)
            short[] r2 = defpackage.eb.ab
            short[][] r3 = defpackage.eb.ev
            r3 = r3[r1]
            r4 = 4
            short r3 = r3[r4]
            r2[r1] = r3
            short[] r2 = defpackage.eb.aq
            short[][] r3 = defpackage.eb.ev
            r3 = r3[r1]
            short r3 = r3[r11]
            r2[r1] = r3
            goto L_0x0176
        L_0x0234:
            int[] r2 = defpackage.eb.aI
            r2[r1] = r8
            goto L_0x01c0
        L_0x0239:
            defpackage.eb.cq()
            int r3 = defpackage.eb.w(r1)
            int[] r4 = defpackage.eb.aI
            r4 = r4[r1]
            if (r4 < r3) goto L_0x01d8
            int[] r4 = defpackage.eb.aI
            r5 = r4[r1]
            int r3 = r5 - r3
            r4[r1] = r3
            r13.gj = r9
            short[] r3 = defpackage.eb.O
            short r4 = r3[r1]
            int r4 = r4 + 1
            short r4 = (short) r4
            r3[r1] = r4
            c(r1)
            r13.gk = r9
            int r2 = r2 + 1
            goto L_0x01d4
        L_0x0262:
            byte[][] r3 = defpackage.bj.dP
            r3 = r3[r1]
            byte r3 = r3[r2]
            if (r3 != 0) goto L_0x02aa
            short[] r3 = defpackage.eb.O
            short r3 = r3[r1]
            short[][] r4 = defpackage.bj.fz
            byte[][] r5 = defpackage.bj.dR
            r5 = r5[r1]
            byte r5 = r5[r2]
            r4 = r4[r5]
            short r4 = r4[r11]
            if (r3 < r4) goto L_0x02aa
            byte[][] r3 = r13.dR
            byte r4 = r13.i
            r3 = r3[r4]
            r3[r8] = r10
            byte[][] r3 = r13.dR
            byte r4 = r13.i
            r3 = r3[r4]
            byte r4 = (byte) r1
            r3[r9] = r4
            byte[][] r3 = r13.dR
            byte r4 = r13.i
            r3 = r3[r4]
            byte[][] r4 = defpackage.bj.dR
            r4 = r4[r1]
            byte r4 = r4[r2]
            r3[r10] = r4
            byte[][] r3 = defpackage.bj.dP
            r3 = r3[r1]
            r3[r2] = r9
            byte r2 = r13.i
            int r2 = r2 + 1
            byte r2 = (byte) r2
            r13.i = r2
            goto L_0x020b
        L_0x02aa:
            int r2 = r2 + 1
            byte r2 = (byte) r2
            goto L_0x0204
        L_0x02af:
            r4 = r2[r1]
            byte r4 = r4[r3]
            int r4 = defpackage.eb.e(r1, r4, r9)
            r5 = r2[r1]
            byte r5 = r5[r3]
            int r5 = defpackage.eb.e(r1, r5, r8)
            r6 = r2[r1]
            byte r6 = r6[r3]
            defpackage.eb.a(r1, r6)
            short[] r6 = defpackage.eb.ar
            short r6 = r6[r12]
            byte r7 = defpackage.eb.e
            if (r5 >= r7) goto L_0x02f9
            if (r4 < r6) goto L_0x02f9
            r4 = r2[r1]
            byte r4 = r4[r3]
            defpackage.eb.b(r1, r4, r8)
            r4 = r2[r1]
            byte r4 = r4[r3]
            defpackage.eb.g(r1, r4)
            byte[][] r4 = r13.dR
            byte r5 = r13.i
            r4 = r4[r5]
            r4[r8] = r11
            byte[][] r4 = r13.dR
            byte r5 = r13.i
            r4 = r4[r5]
            r5 = r2[r1]
            byte r5 = r5[r3]
            r4[r9] = r5
            byte r4 = r13.i
            int r4 = r4 + 1
            byte r4 = (byte) r4
            r13.i = r4
        L_0x02f9:
            int r3 = r3 + 1
            goto L_0x020e
        L_0x02fd:
            short[][] r2 = r13.ev
            r2 = r2[r0]
            short r2 = r2[r11]
            int r1 = r1 + r2
            short r1 = (short) r1
            int r0 = r0 + 1
            goto L_0x0089
        L_0x0309:
            short[][][] r1 = defpackage.r.ai
            r1 = r1[r0]
            r1 = r1[r9]
            int r1 = r1.length
            switch(r1) {
                case 2: goto L_0x0315;
                case 3: goto L_0x033a;
                default: goto L_0x0313;
            }
        L_0x0313:
            goto L_0x0008
        L_0x0315:
            r1 = r8
        L_0x0316:
            if (r1 >= r10) goto L_0x0008
            short[][][] r2 = defpackage.r.ai
            r2 = r2[r0]
            r2 = r2[r10]
            int r2 = r2.length
            int r2 = r2 - r9
            int r2 = defpackage.ee.q(r8, r2)
            short[][][] r3 = defpackage.r.ai
            r3 = r3[r0]
            r3 = r3[r9]
            short r3 = r3[r1]
            short[][][] r4 = defpackage.r.ai
            r4 = r4[r0]
            r4 = r4[r10]
            short r2 = r4[r2]
            r13.h(r3, r2)
            int r1 = r1 + 1
            goto L_0x0316
        L_0x033a:
            int[] r1 = new int[r10]
            r2 = r8
        L_0x033d:
            if (r2 < r10) goto L_0x035e
            r2 = r8
        L_0x0340:
            if (r2 >= r10) goto L_0x0008
            short[][][] r3 = defpackage.r.ai
            r3 = r3[r0]
            r3 = r3[r10]
            int r3 = r3.length
            int r3 = r3 - r9
            int r3 = defpackage.ee.q(r8, r3)
            r4 = r1[r2]
            short[][][] r5 = defpackage.r.ai
            r5 = r5[r0]
            r5 = r5[r10]
            short r3 = r5[r3]
            r13.h(r4, r3)
            int r2 = r2 + 1
            goto L_0x0340
        L_0x035e:
            short[][][] r3 = defpackage.r.ai
            r3 = r3[r0]
            r3 = r3[r9]
            int r3 = r3.length
            int r3 = r3 - r9
            int r3 = defpackage.ee.q(r8, r3)
            r1[r2] = r3
            int r2 = r2 + 1
            goto L_0x033d
        L_0x036f:
            r1 = r8
        L_0x0370:
            if (r1 >= r11) goto L_0x0008
            short[][][] r2 = defpackage.r.ai
            r2 = r2[r0]
            r2 = r2[r10]
            int r2 = r2.length
            int r2 = r2 - r9
            int r2 = defpackage.ee.q(r8, r2)
            short[][][] r3 = defpackage.r.ai
            r3 = r3[r0]
            r3 = r3[r9]
            short r3 = r3[r1]
            short[][][] r4 = defpackage.r.ai
            r4 = r4[r0]
            r4 = r4[r10]
            short r2 = r4[r2]
            r13.h(r3, r2)
            int r1 = r1 + 1
            goto L_0x0370
        L_0x0394:
            java.lang.String r0 = "战斗失败"
            r13.aG = r0
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bg.a(byte):void");
    }

    public final void a(byte b, byte b2, byte b3, byte b4) {
        this.ge = b;
        this.gf = b2;
        this.gh = b3;
        this.gg = b4;
    }

    public final void a(int i2) {
        switch (i2) {
            case 0:
                this.aG = bj.aC[eb.m[this.j]];
                break;
            case 2:
                byte b = 0;
                while (true) {
                    if (b >= this.p.length) {
                        break;
                    } else {
                        if (this.R >= this.p.length) {
                            byte b2 = (byte) (this.R + 1);
                            this.R = b2;
                            this.R = b2 > ((byte) (this.p.length - 1)) ? 0 : this.R;
                            if (this.dP[this.R][0] != -1) {
                                t(this.R);
                                break;
                            }
                        } else if (this.dP[this.R][0] != -1) {
                            t(this.R);
                        } else {
                            byte b3 = (byte) (this.R + 1);
                            this.R = b3;
                            this.R = b3 > ((byte) (this.p.length - 1)) ? 0 : this.R;
                            if (this.dP[this.R][0] != -1) {
                                t(this.R);
                                break;
                            }
                        }
                        b = (byte) (b + 1);
                    }
                }
            case 3:
                this.aG = "按上下键选择技能";
                break;
            case 5:
                this.aG = "按上下键选择物品";
                break;
            case 7:
                this.aG = "战斗商店";
                break;
            case 8:
                J();
                break;
        }
        this.f = (byte) i2;
        this.g = 0;
    }

    public final void a(an anVar) {
        switch (this.e) {
            case 0:
                byte b = this.f;
                du.a(anVar, 0, 0, 240, 320, 0);
                du.a(anVar, "江湖风云录之御剑问情", 120, 5, 17, 16439945);
                anVar.setColor(16777215);
                byte b2 = (byte) (230 / p.f);
                for (byte b3 = 0; b3 < ea.cp().aQ[ea.cp().g].length; b3 = (byte) (b3 + 1)) {
                    anVar.a(ea.cp().aQ[ea.cp().g][b3], (p.f * (b3 % b2)) + 5, p.e + 10 + (p.e * (b3 / b2)), 20);
                }
                a(anVar, this.f);
                du.a(anVar, new StringBuffer().append("加载中 ").append(b * 10).append("%").toString(), 235, 317 - p.e, 24, 3689850);
                break;
            case 1:
                d(anVar);
                break;
            case 2:
                q(anVar);
                break;
            case 3:
                A(anVar);
                switch (this.f) {
                    case 1:
                        if (this.gc) {
                            A(anVar);
                            p(anVar);
                            if (this.g >= this.p.length) {
                                ea.cp().ae = false;
                                this.fG = 0;
                                this.aG = "敌人攻击";
                                this.g = 0;
                                this.fP = 0;
                                this.Y = 0;
                                this.h = 0;
                                this.gc = false;
                                break;
                            } else {
                                this.fP = (byte) (this.fP + 1);
                                if (this.fP < 26) {
                                    if (this.dP[this.g][0] != -1) {
                                        if (this.dP[this.g][1] != -1) {
                                            this.g = (byte) (this.g + 1);
                                            this.fP = 0;
                                            break;
                                        } else {
                                            this.aG = new StringBuffer().append(this.aC[this.g]).append("中毒，生命值-").append((int) this.T).toString();
                                            ed.cB().a(5, this.aG.toCharArray());
                                            if (this.h == 0) {
                                                a(1, this.T, this.p[this.g].aH, this.p[this.g].dN - 40);
                                                this.h = 1;
                                            }
                                            byte b4 = (byte) (this.Y + 1);
                                            this.Y = b4;
                                            this.Y = b4 > this.m.length - 1 ? (byte) (this.m.length - 1) : this.Y;
                                            b(anVar, this.m[this.Y]);
                                            break;
                                        }
                                    } else {
                                        this.g = (byte) (this.g + 1);
                                        this.fP = 0;
                                        break;
                                    }
                                } else {
                                    if (this.dP[this.g][1] == -1) {
                                        if (this.er[this.g][0] >= 2) {
                                            this.er[this.g][0] = 0;
                                        }
                                        b(0, this.g, this.T);
                                        if (q(0)) {
                                            this.W = 0;
                                            this.Y = 0;
                                            this.X = 0;
                                            this.j = 0;
                                            this.M = true;
                                            this.gc = false;
                                            a((byte) 5);
                                        }
                                    }
                                    this.fG = 0;
                                    this.Y = 0;
                                    this.g = (byte) (this.g + 1);
                                    this.h = 0;
                                    this.fP = 0;
                                    ea.cp().ae = false;
                                    break;
                                }
                            }
                        }
                        break;
                    case 2:
                        l(anVar);
                        break;
                    case 4:
                        switch (this.W) {
                            case 0:
                                if (this.dP[this.R][2] == -1) {
                                    byte[] bArr = this.er[this.R];
                                    bArr[2] = (byte) (bArr[2] + 1);
                                    if (this.er[this.R][2] == 3) {
                                        this.er[this.R][2] = 0;
                                    }
                                }
                                this.aG = new StringBuffer().append(this.aC[this.R]).append("攻击").toString();
                                this.p[this.R].a(1);
                                if (!this.fZ) {
                                    this.p[this.R].a((this.fv[this.j].aH - 50) + 0, this.fv[this.j].dN + 5 + 0);
                                } else {
                                    this.p[this.R].a(this.p[this.an].aH - 20, this.p[this.an].dN + 5 + 0);
                                }
                                this.W = 1;
                                break;
                            case 1:
                                this.ea = true;
                                this.af = 0;
                                if (!this.fU) {
                                    b(anVar, this.m[this.Y]);
                                    this.Y = (byte) (this.Y + 1);
                                    this.Y = this.Y > ((byte) (this.m.length - 1)) ? (byte) (this.m.length - 1) : this.Y;
                                    if (this.p[this.R].W()) {
                                        this.ea = false;
                                        this.p[this.R].c(0);
                                        this.p[this.R].a(this.p[this.R].eb, this.p[this.R].ec);
                                        this.fv[this.j].a(0);
                                        this.W = 2;
                                        this.Y = 0;
                                        this.fG = 0;
                                        if (!this.fX) {
                                            if (this.fZ) {
                                                b(0, this.an, this.l);
                                                break;
                                            } else {
                                                b(1, this.j, this.l);
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    switch (this.fW) {
                                        case 0:
                                            this.o.a(this.fv[this.j].aH, this.fv[this.j].dN - 10);
                                            this.o.c(anVar);
                                            this.o.Q();
                                            if (this.o.W()) {
                                                this.fW = 1;
                                                break;
                                            }
                                            break;
                                        case 1:
                                            b(anVar, this.m[this.Y]);
                                            this.Y = (byte) (this.Y + 1);
                                            if (this.p[this.R].W()) {
                                                this.p[this.R].c(0);
                                                this.p[this.R].a(this.p[this.R].eb, this.p[this.R].ec);
                                                this.fv[this.j].a(0);
                                                ba();
                                            }
                                            if (this.Y >= ((byte) (this.m.length - 1))) {
                                                this.fW = 0;
                                                this.ea = false;
                                                ba();
                                                this.p[this.R].c(0);
                                                this.p[this.R].a(this.p[this.R].eb, this.p[this.R].ec);
                                                this.fv[this.j].a(0);
                                                this.W = 2;
                                                this.Y = 0;
                                                this.fG = 0;
                                                if (!this.fX) {
                                                    if (this.fZ) {
                                                        b(0, this.an, this.l);
                                                        break;
                                                    } else {
                                                        b(1, this.j, this.l);
                                                        break;
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                o();
                                break;
                        }
                    case 5:
                        b(anVar);
                        break;
                }
            case 5:
                e(anVar);
                break;
        }
        if (ea.cp().ae) {
            ed.cB().a(anVar);
        }
    }

    public final void a(an anVar, int i2) {
        du.a(anVar, this.I[0], 0, 50, 275, 0);
        an anVar2 = anVar;
        du.a(anVar2, this.I[1], 0, 0, i2 * 14, this.I[1].getHeight(), 0, 51, 275);
    }

    public final void c() {
        this.ge = 0;
        this.fv = null;
        this.p = null;
        this.fw = null;
        this.fx = null;
        this.s = null;
        this.t = null;
        this.q = null;
        this.r = null;
        this.v = null;
        this.J = null;
        this.fC = null;
        this.o = null;
        this.u = null;
    }

    public final void d(int i2) {
        this.fY = new boolean[this.p.length];
        this.au = 1;
        d();
        switch (i2) {
            case 13:
                this.gl = true;
                this.Z = 4;
                for (byte b = 0; b < this.fv.length; b = (byte) (b + 1)) {
                    this.eq[eb.m[b]][0] = 0;
                    this.fv[b].a(0);
                    if (eb.O[eb.m[b]] < 50) {
                        short[] sArr = eb.O;
                        byte b2 = eb.m[b];
                        sArr[b2] = (short) (sArr[b2] + 1);
                    }
                    aU();
                    c(eb.m[b]);
                }
                eb.cq().k(23, 1);
                this.aq = new short[this.p.length];
                int s2 = (s(0, 0) + e(0, 0, 0, 1)) * 3;
                for (int i3 = 0; i3 < this.p.length; i3++) {
                    if (this.dP[i3][0] != -1) {
                        this.aq[i3] = (short) (s2 - ((short) r(i3, 0)));
                        this.aq[i3] = this.aq[i3] <= 0 ? 15 : this.aq[i3];
                        a(1, this.aq[i3], this.p[i3].aH, (this.p[i3].dN - ab[this.ev[i3][0]]) - 5);
                    }
                }
                break;
            case Database.INT_ISFEE /*14*/:
                this.Z = 24;
                this.aq = new short[this.p.length];
                for (int i4 = 0; i4 < this.aq.length; i4++) {
                    if (this.dP[i4][0] != -1) {
                        this.aq[i4] = this.ev[i4][4];
                        a(1, this.aq[i4], this.p[i4].aH, (this.p[i4].dN - ab[this.ev[i4][0]]) - 5);
                    }
                }
                break;
        }
        aV();
    }
}
