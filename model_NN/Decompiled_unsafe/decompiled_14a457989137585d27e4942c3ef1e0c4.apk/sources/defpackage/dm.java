package defpackage;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import javax.microedition.media.PlayerListener;

/* renamed from: dm  reason: default package */
public final class dm implements z {
    private Socket iW;
    private int mode;

    public dm(String str, int i) {
        this.mode = i;
        try {
            if (!bX()) {
                throw new IOException("No avaliable connection.");
            }
            URI uri = new URI(str);
            this.iW = new Socket(uri.getHost(), uri.getPort());
        } catch (Exception e) {
            throw new IOException("URISyntaxException");
        }
    }

    private static boolean bX() {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager bH = cl.bH();
            return bH != null && (activeNetworkInfo = bH.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            Log.v(PlayerListener.ERROR, e.toString());
        }
    }

    public final void close() {
        this.iW.close();
        this.iW = null;
        System.gc();
    }
}
