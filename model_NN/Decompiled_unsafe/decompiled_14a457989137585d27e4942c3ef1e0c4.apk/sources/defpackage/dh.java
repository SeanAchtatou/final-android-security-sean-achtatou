package defpackage;

import android.media.MediaPlayer;
import android.util.Log;
import java.util.HashSet;
import java.util.Iterator;
import javax.microedition.media.Control;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;

/* renamed from: dh  reason: default package */
public final class dh implements MediaPlayer.OnCompletionListener, Player {
    private static final String LOG_TAG = "MIDP player";
    private ce iG;
    private HashSet iH;
    private int iI;
    private int iJ;
    private dn iK;
    private int state = 100;

    public dh(ce ceVar) {
        this.iG = ceVar;
        this.iI = 1;
        ceVar.hm.setOnCompletionListener(this);
        this.iH = new HashSet();
    }

    private final void a(String str, Object obj) {
        Iterator it = this.iH.iterator();
        while (it.hasNext()) {
            PlayerListener playerListener = (PlayerListener) it.next();
            if (playerListener != null) {
                playerListener.aM();
            }
        }
    }

    public final void aK() {
        try {
            if (this.state == 100) {
                this.iG.hm.reset();
                this.iG.hm.setDataSource(this.iG.hi);
                this.state = Player.REALIZED;
            }
        } catch (Exception e) {
            Log.w(LOG_TAG, e);
            throw new MediaException();
        }
    }

    public final void aL() {
        if (this.state == 200) {
            try {
                this.iG.hm.prepare();
                this.state = Player.PREFETCHED;
            } catch (Exception e) {
                a(PlayerListener.ERROR, e.getMessage());
                throw new MediaException();
            }
        }
    }

    public final void close() {
        try {
            stop();
        } catch (Exception e) {
            a(PlayerListener.ERROR, e.getMessage());
            Log.w(LOG_TAG, "Exception when stop before close.");
        }
        this.state = 0;
        a(PlayerListener.CLOSED, null);
        ce ceVar = this.iG;
        if (ceVar.name != null) {
            cc.he.remove(ceVar.name);
        }
        if (ceVar.hm != null) {
            ceVar.hm.release();
        }
        ceVar.hm = null;
    }

    public final int getState() {
        return this.state;
    }

    public final void m(int i) {
        this.iI = -1;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer == this.iG.hm) {
            this.iJ++;
            if (this.iJ < this.iI) {
                try {
                    this.iG.hm.reset();
                    this.iG.hm.setDataSource(this.iG.hi);
                    this.iG.hm.prepare();
                    this.iG.hm.start();
                } catch (Exception e) {
                    a(PlayerListener.ERROR, e.getMessage());
                }
            } else if (this.iH.isEmpty()) {
                try {
                    stop();
                } catch (MediaException e2) {
                    a(PlayerListener.ERROR, e2.getMessage());
                }
            } else {
                a(PlayerListener.END_OF_MEDIA, null);
            }
        }
    }

    public final Control q(String str) {
        if (str.indexOf("VolumeControl") == -1) {
            return null;
        }
        if (this.iK == null) {
            this.iK = new dn(this.iG);
        }
        return this.iK;
    }

    public final void start() {
        if (this.state == 100) {
            aK();
        }
        if (this.state == 200) {
            aL();
        }
        if (this.state == 300) {
            this.state = Player.STARTED;
            try {
                if (this.iI == -1) {
                    this.iG.hm.setLooping(true);
                } else {
                    this.iG.hm.setLooping(false);
                }
                this.iG.hm.start();
                this.iG.hj = true;
                a(PlayerListener.STARTED, null);
            } catch (Exception e) {
                a(PlayerListener.ERROR, e.getMessage());
                throw new MediaException();
            }
        }
    }

    public final void stop() {
        if (this.state == 400) {
            this.state = 100;
            try {
                this.iG.hm.stop();
                this.iG.hj = false;
                this.iJ = 1;
                a(PlayerListener.STOPPED, null);
            } catch (IllegalStateException e) {
                a(PlayerListener.ERROR, e.getMessage());
                throw new MediaException();
            }
        }
        aK();
        aL();
    }
}
