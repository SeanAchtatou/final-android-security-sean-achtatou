package defpackage;

import com.network.app.util.Pay;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Random;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.control.ToneControl;

/* renamed from: ee  reason: default package */
public final class ee {
    private static int aH = 218765834;
    private static int ac = -1991225785;
    private static int dN = 1229472850;
    private static int dO = 0;
    private static Random kn = new Random();
    public static DataInputStream ko = null;
    private static long[] kp = new long[as.FIRE_PRESSED];

    public static ao O(String str) {
        byte[] bArr = new byte[4];
        try {
            str.getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(new StringBuffer().append(str).append(".p").toString()));
            ko = dataInputStream;
            dataInputStream.read(bArr);
            byte b = (bArr[3] & ToneControl.SILENCE) | ((bArr[0] << 24) & Pay.WAIT) | ((bArr[1] << 16) & Pay.WAIT) | ((bArr[2] << 8) & Pay.WAIT);
            short readShort = ko.readShort();
            short readShort2 = ko.readShort();
            byte readByte = ko.readByte() & ToneControl.SILENCE;
            int i = b + 48;
            byte[] bArr2 = new byte[i];
            ko.read(bArr2, 41, (i - 41) - 12);
            d(bArr2, 0, ac);
            d(bArr2, 4, aH);
            d(bArr2, 8, 13);
            d(bArr2, 12, dN);
            d(bArr2, 16, readShort);
            d(bArr2, 20, readShort2);
            bArr2[24] = 8;
            d(bArr2, 25, 50331648);
            byte[] z = z(readShort, readShort2);
            d(bArr2, 29, (int) c(z, z.length));
            d(bArr2, 33, readByte * 3);
            d(bArr2, 37, 1347179589);
            d(bArr2, i - 12, 0);
            d(bArr2, i - 8, 1229278788);
            d(bArr2, i - 4, -1371381630);
            ao a = ao.a(bArr2, 0, i);
            ko.close();
            ko = null;
            return a;
        } catch (Exception e) {
            try {
                return ao.p(new StringBuffer().append(str).append(".png").toString());
            } catch (IOException e2) {
                return null;
            }
        }
    }

    public static int c(byte b, byte b2) {
        return (((short) (b & ToneControl.SILENCE)) << 8) | ((short) (b2 & ToneControl.SILENCE));
    }

    private static long c(byte[] bArr, int i) {
        if (dO == 0) {
            for (int i2 = 0; i2 < 256; i2++) {
                long j = (long) i2;
                for (int i3 = 0; i3 < 8; i3++) {
                    j = ((int) (1 & j)) != 0 ? (j >> 1) ^ 3988292384L : j >> 1;
                }
                kp[i2] = j;
            }
            dO = 1;
        }
        long j2 = 4294967295L;
        for (int i4 = 0; i4 < i; i4++) {
            j2 = (j2 >> 8) ^ kp[((int) (((long) bArr[i4]) ^ j2)) & 255];
        }
        return j2 ^ 4294967295L;
    }

    private static void d(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) ((i2 >>> 24) & 255);
        bArr[i + 1] = (byte) ((i2 >>> 16) & 255);
        bArr[i + 2] = (byte) ((i2 >>> 8) & 255);
        bArr[i + 3] = (byte) ((i2 >>> 0) & 255);
    }

    public static int q(int i, int i2) {
        return (Math.abs(kn.nextInt()) % ((i2 - i) + 1)) + i;
    }

    public static int w(int i) {
        return (i & 128) != 0 ? (i & 64) != 0 ? 3 : 2 : (i & 64) != 0 ? 1 : 0;
    }

    public static int x(int i) {
        return i * i;
    }

    private static byte[] z(int i, int i2) {
        byte[] bArr = new byte[17];
        try {
            d(bArr, 0, 1229472850);
            d(bArr, 4, i);
            d(bArr, 8, i2);
            bArr[12] = 8;
            d(bArr, 13, 50331648);
        } catch (IOException e) {
        }
        return bArr;
    }
}
