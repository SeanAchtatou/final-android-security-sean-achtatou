package defpackage;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: al  reason: default package */
public abstract class al implements cu {
    public static final int ALERT = 5;
    public static final int CANVAS = 0;
    public static final int FORM = 2;
    public static final int GAMECANVAS = 1;
    public static final int LIST = 3;
    public static final int TEXTBOX = 4;
    protected ah eT = null;
    public boolean eU = false;
    ArrayList eV = new ArrayList();
    ag eW = null;
    String eX;
    boolean eY;
    private int height = -1;
    private int width = -1;

    al() {
    }

    /* access modifiers changed from: protected */
    public void aC() {
    }

    /* access modifiers changed from: protected */
    public void aD() {
    }

    public void aE() {
        if (this.eY) {
            Iterator it = this.eV.iterator();
            while (it.hasNext()) {
                ch.b((af) it.next());
            }
            this.eY = false;
            try {
                aC();
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
        }
    }

    public abstract int at();

    public void aw() {
        if (!this.eY) {
            Iterator it = this.eV.iterator();
            while (it.hasNext()) {
                ch.a((af) it.next());
            }
            this.eY = true;
            try {
                aD();
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
        }
    }

    public boolean ax() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void i(int i) {
    }

    /* access modifiers changed from: protected */
    public void j(int i) {
    }
}
