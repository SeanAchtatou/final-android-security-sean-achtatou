package defpackage;

/* renamed from: bn  reason: default package */
public interface bn {
    public static final int EXIT = 3;
    public static final int INIT = 0;
    public static final int PAUSE = 2;
    public static final int RUN = 1;

    void aO();

    int getState();

    void onDestroy();

    void onPause();

    void onResume();

    void onStart();
}
