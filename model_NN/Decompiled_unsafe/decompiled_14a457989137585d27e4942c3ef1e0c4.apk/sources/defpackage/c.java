package defpackage;

/* renamed from: c  reason: default package */
public final class c extends n {
    public c(ao aoVar, byte[][] bArr, byte[][][] bArr2, byte[][] bArr3) {
        super(aoVar, bArr, bArr2, bArr3);
        this.j = 4;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v12, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static defpackage.c g(java.lang.String r21) {
        /*
            r7 = 0
            java.io.DataInputStream r8 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0101 }
            bi r5 = defpackage.bi.bo()     // Catch:{ IOException -> 0x0101 }
            r5.getClass()     // Catch:{ IOException -> 0x0101 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0101 }
            r5.<init>()     // Catch:{ IOException -> 0x0101 }
            r0 = r5
            r1 = r21
            java.lang.StringBuffer r5 = r0.append(r1)     // Catch:{ IOException -> 0x0101 }
            java.lang.String r6 = ".hy"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ IOException -> 0x0101 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0101 }
            java.io.InputStream r5 = javax.microedition.enhance.MIDPHelper.m(r5)     // Catch:{ IOException -> 0x0101 }
            r8.<init>(r5)     // Catch:{ IOException -> 0x0101 }
            int r6 = r8.readUnsignedByte()     // Catch:{ IOException -> 0x0101 }
            r5 = 4
            int[] r5 = new int[]{r6, r5}     // Catch:{ IOException -> 0x0101 }
            java.lang.Class r9 = java.lang.Byte.TYPE     // Catch:{ IOException -> 0x0101 }
            java.lang.Object r5 = java.lang.reflect.Array.newInstance(r9, r5)     // Catch:{ IOException -> 0x0101 }
            byte[][] r5 = (byte[][]) r5     // Catch:{ IOException -> 0x0101 }
            r9 = 0
        L_0x0039:
            if (r9 < r6) goto L_0x0064
            int r6 = r8.readUnsignedByte()     // Catch:{ IOException -> 0x0101 }
            byte[][][] r9 = new byte[r6][][]     // Catch:{ IOException -> 0x0101 }
            r6 = 0
            r10 = r6
        L_0x0043:
            int r6 = r9.length     // Catch:{ IOException -> 0x0101 }
            if (r10 < r6) goto L_0x0076
            int r6 = r8.readUnsignedByte()     // Catch:{ IOException -> 0x0101 }
            byte[][] r10 = new byte[r6][]     // Catch:{ IOException -> 0x0101 }
            r11 = 0
        L_0x004d:
            if (r11 < r6) goto L_0x00a4
            r8.close()     // Catch:{ IOException -> 0x0101 }
            ao r21 = defpackage.ee.O(r21)     // Catch:{ IOException -> 0x0101 }
            c r6 = new c     // Catch:{ IOException -> 0x0101 }
            r0 = r6
            r1 = r21
            r2 = r5
            r3 = r9
            r4 = r10
            r0.<init>(r1, r2, r3, r4)     // Catch:{ IOException -> 0x0101 }
            r21 = r6
        L_0x0063:
            return r21
        L_0x0064:
            r10 = 0
        L_0x0065:
            r11 = 4
            if (r10 < r11) goto L_0x006b
            int r9 = r9 + 1
            goto L_0x0039
        L_0x006b:
            r11 = r5[r9]     // Catch:{ IOException -> 0x0101 }
            byte r12 = r8.readByte()     // Catch:{ IOException -> 0x0101 }
            r11[r10] = r12     // Catch:{ IOException -> 0x0101 }
            int r10 = r10 + 1
            goto L_0x0065
        L_0x0076:
            int r11 = r8.readUnsignedByte()     // Catch:{ IOException -> 0x0101 }
            r6 = 4
            int[] r6 = new int[]{r11, r6}     // Catch:{ IOException -> 0x0101 }
            java.lang.Class r12 = java.lang.Byte.TYPE     // Catch:{ IOException -> 0x0101 }
            java.lang.Object r6 = java.lang.reflect.Array.newInstance(r12, r6)     // Catch:{ IOException -> 0x0101 }
            byte[][] r6 = (byte[][]) r6     // Catch:{ IOException -> 0x0101 }
            r9[r10] = r6     // Catch:{ IOException -> 0x0101 }
            r6 = 0
        L_0x008a:
            if (r6 < r11) goto L_0x0090
            int r6 = r10 + 1
            r10 = r6
            goto L_0x0043
        L_0x0090:
            r12 = 0
        L_0x0091:
            r13 = 4
            if (r12 < r13) goto L_0x0097
            int r6 = r6 + 1
            goto L_0x008a
        L_0x0097:
            r13 = r9[r10]     // Catch:{ IOException -> 0x0101 }
            r13 = r13[r6]     // Catch:{ IOException -> 0x0101 }
            byte r14 = r8.readByte()     // Catch:{ IOException -> 0x0101 }
            r13[r12] = r14     // Catch:{ IOException -> 0x0101 }
            int r12 = r12 + 1
            goto L_0x0091
        L_0x00a4:
            int r12 = r8.readUnsignedByte()     // Catch:{ IOException -> 0x0101 }
            byte[] r13 = new byte[r12]     // Catch:{ IOException -> 0x0101 }
            r14 = 0
            r15 = 0
            r20 = r15
            r15 = r14
            r14 = r20
        L_0x00b1:
            if (r14 < r12) goto L_0x00c8
            byte[] r14 = new byte[r15]     // Catch:{ IOException -> 0x0101 }
            r10[r11] = r14     // Catch:{ IOException -> 0x0101 }
            r14 = 0
            r15 = 0
            r20 = r15
            r15 = r14
            r14 = r20
        L_0x00be:
            int r16 = r12 >> 1
            r0 = r14
            r1 = r16
            if (r0 < r1) goto L_0x00dc
            int r11 = r11 + 1
            goto L_0x004d
        L_0x00c8:
            byte r16 = r8.readByte()     // Catch:{ IOException -> 0x0101 }
            r13[r14] = r16     // Catch:{ IOException -> 0x0101 }
            int r16 = r12 >> 1
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00d9
            byte r16 = r13[r14]     // Catch:{ IOException -> 0x0101 }
            int r15 = r15 + r16
        L_0x00d9:
            int r14 = r14 + 1
            goto L_0x00b1
        L_0x00dc:
            r16 = 0
            r20 = r16
            r16 = r15
            r15 = r20
        L_0x00e4:
            byte r17 = r13[r14]     // Catch:{ IOException -> 0x0101 }
            r0 = r15
            r1 = r17
            if (r0 < r1) goto L_0x00f0
            int r14 = r14 + 1
            r15 = r16
            goto L_0x00be
        L_0x00f0:
            r17 = r10[r11]     // Catch:{ IOException -> 0x0101 }
            int r18 = r16 + 1
            int r19 = r12 >> 1
            int r19 = r19 + r14
            byte r19 = r13[r19]     // Catch:{ IOException -> 0x0101 }
            r17[r16] = r19     // Catch:{ IOException -> 0x0101 }
            int r15 = r15 + 1
            r16 = r18
            goto L_0x00e4
        L_0x0101:
            r21 = move-exception
            r21 = r7
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.g(java.lang.String):c");
    }

    public final void N() {
        Q();
    }

    public final void b(byte[] bArr) {
    }

    public final void c() {
    }
}
