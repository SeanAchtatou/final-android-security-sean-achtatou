package defpackage;

import android.view.View;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: ae  reason: default package */
public abstract class ae extends al {
    public static final int DOWN = 6;
    public static final int FIRE = 8;
    public static final int GAME_A = 9;
    public static final int GAME_B = 10;
    public static final int GAME_C = 11;
    public static final int GAME_D = 12;
    public static final int KEY_NUM0 = 48;
    public static final int KEY_NUM1 = 49;
    public static final int KEY_NUM2 = 50;
    public static final int KEY_NUM3 = 51;
    public static final int KEY_NUM4 = 52;
    public static final int KEY_NUM5 = 53;
    public static final int KEY_NUM6 = 54;
    public static final int KEY_NUM7 = 55;
    public static final int KEY_NUM8 = 56;
    public static final int KEY_NUM9 = 57;
    public static final int KEY_POUND = 35;
    public static final int KEY_STAR = 42;
    public static final int LEFT = 2;
    public static final int RIGHT = 5;
    public static final int UP = 1;

    public static int h(int i) {
        cw cwVar = bp.gC;
        return MIDPDevice.h(i);
    }

    public final int at() {
        return 0;
    }

    public final void au() {
        if (this.eT != null) {
            this.eT.a(this);
        }
    }

    public final void av() {
        if (this.eT != null) {
            ah ahVar = this.eT;
            ah.av();
        }
    }

    public final void aw() {
        super.aw();
        au();
    }

    public final boolean ax() {
        return false;
    }

    public final View getView() {
        return br.gE;
    }

    /* access modifiers changed from: protected */
    public abstract void n(an anVar);
}
