package defpackage;

import android.app.AlertDialog;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: db  reason: default package */
public final class db implements Runnable {
    final /* synthetic */ MIDPDevice ig;
    private final /* synthetic */ AlertDialog.Builder ij;

    public db(MIDPDevice mIDPDevice, AlertDialog.Builder builder) {
        this.ig = mIDPDevice;
        this.ij = builder;
    }

    public final void run() {
        this.ij.create().show();
    }
}
