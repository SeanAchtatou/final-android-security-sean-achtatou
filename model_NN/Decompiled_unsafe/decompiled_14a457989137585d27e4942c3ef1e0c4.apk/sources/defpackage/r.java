package defpackage;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: r  reason: default package */
public final class r {
    public static String[] aC;
    public static String[] aE;
    public static short[][][] ai;
    private static r eu;
    public static short[][] ev;

    private r() {
    }

    public static r ap() {
        if (eu == null) {
            eu = new r();
        }
        return eu;
    }

    public final void N() {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/fp.hy"));
            int readShort = dataInputStream.readShort();
            ai = new short[readShort][][];
            for (int i = 0; i < readShort; i++) {
                int readShort2 = dataInputStream.readShort();
                ai[i] = new short[readShort2][];
                for (int i2 = 0; i2 < readShort2; i2++) {
                    int readShort3 = dataInputStream.readShort();
                    ai[i][i2] = new short[readShort3];
                    for (int i3 = 0; i3 < readShort3; i3++) {
                        ai[i][i2][i3] = (short) dataInputStream.readByte();
                    }
                }
            }
            dataInputStream.close();
        } catch (IOException e) {
        }
        System.gc();
    }

    public final void c() {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/pr.hy"));
            int readShort = dataInputStream.readShort();
            aC = new String[readShort];
            aE = new String[readShort];
            ev = new short[readShort][];
            for (int i = 0; i < readShort; i++) {
                aC[i] = dataInputStream.readUTF();
                aE[i] = dataInputStream.readUTF();
            }
            for (int i2 = 0; i2 < readShort; i2++) {
                int readShort2 = dataInputStream.readShort();
                ev[i2] = new short[readShort2];
                for (int i3 = 0; i3 < readShort2; i3++) {
                    ev[i2][i3] = dataInputStream.readShort();
                }
            }
            dataInputStream.close();
        } catch (IOException e) {
        }
        System.gc();
    }
}
