package defpackage;

import android.os.Environment;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: dc  reason: default package */
public final class dc implements ab {
    File file;
    String ik;
    DataInputStream il;
    DataOutputStream im;
    InputStream in;
    OutputStream io;

    public dc(String str) {
        this.ik = str;
        String substring = str.substring("file://".length());
        if (Environment.getExternalStorageState().equals("mounted")) {
            this.file = new File(Environment.getExternalStorageDirectory() + substring);
            return;
        }
        throw new IOException("SD card not ready.");
    }

    public final void close() {
        if (this.im != null) {
            this.im.close();
            this.im = null;
        }
        if (this.il != null) {
            this.il.close();
            this.il = null;
        }
        if (this.io != null) {
            this.io.close();
            this.io = null;
        }
        if (this.in != null) {
            this.in.close();
            this.in = null;
        }
    }
}
