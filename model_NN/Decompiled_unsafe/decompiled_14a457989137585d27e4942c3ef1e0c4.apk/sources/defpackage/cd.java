package defpackage;

import android.os.Message;

/* renamed from: cd  reason: default package */
final class cd implements cg {
    cd() {
    }

    public final boolean a(Message message) {
        if (message.what == 47873) {
            for (ce ceVar : cc.he.values()) {
                if (ceVar.hm != null && ceVar.hm.isPlaying() && ceVar.hj) {
                    ceVar.hm.pause();
                }
            }
            int unused = cc.hh = cc.bA();
            cc.D(cc.hb);
            return false;
        } else if (message.what != 47874) {
            return false;
        } else {
            cc.D(cc.hh);
            for (ce ceVar2 : cc.he.values()) {
                if (ceVar2.hm != null && ceVar2.hj && !ceVar2.hm.isPlaying()) {
                    ceVar2.hm.start();
                }
            }
            return false;
        }
    }
}
