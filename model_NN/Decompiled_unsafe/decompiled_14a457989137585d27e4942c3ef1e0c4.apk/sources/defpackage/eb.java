package defpackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: eb  reason: default package */
public final class eb {
    public static boolean M = false;
    public static short[] O = null;
    public static int aH = 0;
    public static int[] aI;
    public static short[] ab;
    public static int ac = 1000;
    public static short[][][] ai;
    public static short[] aq;
    public static short[] ar;
    public static byte[][][] dQ;
    public static boolean dX;
    public static byte e = 3;
    public static short[][] ev;
    public static byte f;
    public static short[][] fA;
    public static short[][] fz;
    public static short k;
    private static eb kd;
    public static d[] ke = new d[3];
    public static byte[] m;
    public boolean ae = true;
    private int dN;
    private boolean dY = false;
    private boolean dZ;
    public int[] fK;
    public int[][] ka;
    public short l;
    public n n;

    private eb() {
        c();
    }

    public static void Q() {
        M = true;
        for (byte b = 0; b < m.length; b = (byte) (b + 1)) {
            ke[m[b]].a(false);
        }
        bj.bp();
        bj.P();
        bj.gr.a(ke[aH].aH, ke[aH].dN);
        bj.gr.c(ke[aH].f);
        dX = false;
    }

    private void U() {
        for (int i = 0; i < fz.length; i++) {
            this.l = (short) (i + 1);
            if (fz[i][1] == 0 && this.l < fz.length) {
                fz[i] = fz[this.l];
                fz[this.l] = new short[]{0, 0};
            }
        }
    }

    private void X() {
        this.ka[this.fK[0]][0] = ke[m[0]].aH;
        this.ka[this.fK[0]][1] = ke[m[0]].dN;
        this.ka[this.fK[0]][2] = ke[m[0]].dO;
        this.ka[this.fK[0]][3] = ke[m[0]].ac;
        int[] iArr = this.fK;
        int[] iArr2 = this.fK;
        int i = iArr2[0] + 1;
        iArr2[0] = i;
        iArr[0] = i >= this.ka.length ? 0 : this.fK[0];
        for (int i2 = 1; i2 < this.fK.length; i2++) {
            ke[m[i2]].aH = this.ka[this.fK[i2]][0];
            ke[m[i2]].dN = this.ka[this.fK[i2]][1];
            ke[m[i2]].c(this.ka[this.fK[i2]][2]);
            ke[m[i2]].ac = this.ka[this.fK[i2]][3];
            int[] iArr3 = this.fK;
            int[] iArr4 = this.fK;
            int i3 = iArr4[i2] + 1;
            iArr4[i2] = i3;
            iArr3[i2] = i3 >= this.ka.length ? 0 : this.fK[i2];
        }
    }

    public static void a(int i) {
        ac += i;
    }

    public static void a(int i, int i2) {
        short[] sArr = new short[9];
        ar = sArr;
        sArr[0] = bj.fz[i2][1];
        ar[1] = bj.fz[i2][2];
        ar[2] = bj.fz[i2][3];
        ar[3] = bj.fz[i2][4];
        ar[4] = bj.fz[i2][5];
        int e2 = e(i, i2, 0);
        if (e2 < e) {
            ar[5] = (short) (bj.fz[i2][6] + (bj.fz[i2][7] * (e2 - 1)));
        }
        ar[6] = (short) (bj.fz[i2][8] + (bj.fz[i2][9] * (e2 - 1)));
        ar[7] = (short) (bj.fz[i2][12] + (bj.fz[i2][10] * (e2 - 1)));
        ar[8] = (short) (((e2 - 1) * bj.fz[i2][11]) + bj.fz[i2][13]);
    }

    public static void b(int i, int i2, int i3) {
        char c = 0;
        switch (i3) {
            case 1:
                c = 1;
                break;
        }
        int q = q(i, i2);
        short[] sArr = ai[i][c];
        sArr[q] = (short) (sArr[q] + 1);
    }

    public static void b(byte[] bArr) {
        aH = bArr[0];
        m = bArr;
    }

    public static boolean b(int i, int i2) {
        for (byte[] bArr : dQ[i]) {
            if (bArr[0] == i2) {
                return true;
            }
        }
        return false;
    }

    public static byte[] bq() {
        byte[] bArr = new byte[fz.length];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) fz[i][0];
        }
        return bArr;
    }

    public static void c(int i) {
        ev[i] = new short[9];
        ev[i][0] = bj.ev[i][0];
        short s = O[i];
        ev[i][1] = (short) bj.ev[i][1];
        ev[i][2] = (short) bj.ev[i][2];
        ev[i][3] = (short) (bj.ev[i][3] + ((s - 1) * bj.ev[i][4]));
        ev[i][4] = (short) (bj.ev[i][5] + ((s - 1) * bj.ev[i][6]));
        ev[i][5] = (short) (bj.ev[i][7] + ((s - 1) * 6));
        ev[i][6] = (short) (bj.ev[i][8] + ((s - 1) * 6));
        ev[i][7] = (short) (bj.ev[i][9] + ((s - 1) * 6));
        ev[i][8] = (short) (((s - 1) * 6) + bj.ev[i][10]);
    }

    public static eb cq() {
        if (kd == null) {
            kd = new eb();
        }
        return kd;
    }

    public static short[] cr() {
        short[] sArr = new short[fz.length];
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = fz[i][1];
        }
        return sArr;
    }

    public static byte[] cs() {
        byte[] bArr = new byte[fA.length];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) fA[i][0];
        }
        return bArr;
    }

    public static short[] ct() {
        short[] sArr = new short[fA.length];
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = fA[i][1];
        }
        return sArr;
    }

    public static void d(int i) {
        int i2 = 0;
        while (i2 < m.length) {
            if (ke[m[i2]].i != i) {
                i2++;
            } else {
                return;
            }
        }
        byte[] bArr = new byte[(m.length + 1)];
        for (int i3 = 0; i3 < m.length; i3++) {
            bArr[i3] = m[i3];
        }
        m = null;
        m = bArr;
        bArr[m.length - 1] = (byte) i;
    }

    public static int e(int i, int i2, int i3) {
        char c = 0;
        switch (i3) {
            case 1:
                c = 1;
                break;
        }
        return ai[i][c][q(i, i2)];
    }

    public static void e(int i) {
        int i2 = 0;
        byte[] bArr = new byte[(m.length - 1)];
        for (byte b = 0; b < m.length; b = (byte) (b + 1)) {
            if (ke[m[b]].i != i) {
                bArr[i2] = m[b];
                i2++;
            }
        }
        m = null;
        m = bArr;
    }

    public static int f(int i, int i2, int i3) {
        switch (i2) {
            case 0:
                switch (i3) {
                    case 0:
                        return ab[i];
                    case 1:
                        return aq[i];
                }
            case 1:
                c(i);
                switch (i3) {
                    case 0:
                        return ev[i][4];
                    case 1:
                        return ev[i][3];
                }
        }
        return 0;
    }

    public static void g(int i, int i2) {
        ai[i][1][q(i, i2)] = 0;
    }

    public static int q(int i, int i2) {
        byte[][] bArr = bj.eq;
        for (int i3 = 0; i3 < bArr[i].length; i3++) {
            if (bArr[i][i3] == i2) {
                return i3;
            }
        }
        return -1;
    }

    public static boolean q(int i) {
        for (short[] sArr : fz) {
            if (sArr[0] == i) {
                return true;
            }
        }
        return false;
    }

    public static int w(int i) {
        return (((O[i] - 1) * (O[i] - 1)) + (O[i] * 5)) * 2;
    }

    public static short w(int i, int i2) {
        int e2 = e(i, i2, 0);
        if (e2 < e) {
            k = (short) (((e2 - 1) * bj.fz[i2][7]) + bj.fz[i2][6]);
        }
        return k;
    }

    public static byte x(int i, int i2) {
        for (int i3 = 0; i3 < dQ[i].length; i3++) {
            if (dQ[i][i3][1] == i2) {
                return dQ[i][i3][0];
            }
        }
        return -1;
    }

    public static boolean y(int i) {
        for (short[] sArr : fA) {
            if (sArr[0] == i) {
                return true;
            }
        }
        return false;
    }

    public final void N() {
        this.ae = true;
        this.fK = new int[m.length];
        this.fK[0] = 0;
        for (int i = 1; i < m.length; i++) {
            ke[m[i]].a(true);
            this.fK[i] = (((m.length - i) - 1) * 5) + 1;
            ke[m[i]].aH = ke[m[0]].aH;
            ke[m[i]].dN = ke[m[0]].dN;
            ke[m[i]].dO = ke[m[0]].dO;
            ke[m[i]].ac = ke[m[0]].ac;
        }
        this.ka = (int[][]) Array.newInstance(Integer.TYPE, ((m.length - 1) * 5) + 1, 4);
        for (int i2 = 0; i2 < this.ka.length; i2++) {
            this.ka[i2][0] = ke[m[0]].aH;
            this.ka[i2][1] = ke[m[0]].dN;
            this.ka[i2][2] = ke[m[0]].dO;
            this.ka[i2][3] = ke[m[0]].ac;
        }
    }

    public final void P() {
        String valueOf;
        if (this.dZ && bi.ac != 0) {
            bi.ac = 0;
            ea.cp().ae = false;
            this.dZ = false;
        }
        if (ea.cp().fU) {
            if (bi.y(320)) {
                ea.cp().fU = false;
            }
        } else if (ea.cp().fV) {
            if (bi.y(288)) {
                ea.cp().fV = false;
            }
        } else if (this.dY) {
            ed.cB().c();
            if (!ea.cp().ae) {
                this.dY = false;
            }
        } else if (ea.cp().ae) {
            if (bi.y(16) || bi.y(128)) {
                ea.cp().ae = false;
            }
        } else if (bi.y(128)) {
            ea.cp().a(2);
        } else {
            if (bi.y(32)) {
                ea.cp().fV = true;
            }
            if (bi.y(as.FIRE_PRESSED)) {
                b.Z();
                b.c();
                b.Z().c(0);
                b.Z().b(b.N, 0);
                ea.cp().a(7);
            } else if (M) {
                if (!bi.q(8)) {
                    if (!bi.q(4)) {
                        if (!bi.q(2)) {
                            if (!bi.q(1)) {
                                if (!bi.y(64)) {
                                    switch (bj.gr.dO) {
                                        case 4:
                                            bj.gr.c(0);
                                            break;
                                        case 5:
                                            bj.gr.c(1);
                                            break;
                                        case 6:
                                            bj.gr.c(2);
                                            break;
                                        case 7:
                                            bj.gr.c(3);
                                            break;
                                    }
                                } else if (!ke[aH].c(ke[aH].aH, ke[aH].dN)) {
                                    f(1);
                                } else {
                                    this.dZ = true;
                                    ed.cB().a(5, "当前位置不能降落".toCharArray());
                                }
                            } else {
                                bj.gr.c(7);
                                ke[aH].h(8, 0);
                                if (ke[aH].e(ke[aH].aH, ke[aH].dN)) {
                                    ke[aH].h(((-(ke[aH].aH + ke[aH].dT)) % 16) - 1, 0);
                                }
                            }
                        } else {
                            bj.gr.c(6);
                            ke[aH].h(-8, 0);
                            if (ke[aH].e(ke[aH].aH, ke[aH].dN)) {
                                ke[aH].h(16 - (((ke[aH].aH + ke[aH].dS) + 16) % 16), 0);
                            }
                        }
                    } else {
                        bj.gr.c(4);
                        ke[aH].h(0, 8);
                        if (ke[aH].e(ke[aH].aH, ke[aH].dN)) {
                            ke[aH].h(0, ((-(ke[aH].dN + ke[aH].ao)) % 16) - 1);
                        }
                    }
                } else {
                    bj.gr.c(5);
                    ke[aH].h(0, -8);
                    if (ke[aH].e(ke[aH].aH, ke[aH].dN)) {
                        ke[aH].h(0, 16 - (((ke[aH].dU + ke[aH].dN) + 16) % 16));
                    }
                }
                bj.gr.a(ke[aH].aH, ke[aH].dN);
            } else if (!dX) {
                ke[aH].d(4);
                if (bi.q(8)) {
                    ke[aH].i(5, 1);
                    ke[aH].P();
                } else if (bi.q(4)) {
                    ke[aH].i(5, 0);
                    ke[aH].P();
                } else if (bi.q(2)) {
                    ke[aH].i(5, 2);
                    ke[aH].P();
                } else if (bi.q(1)) {
                    ke[aH].i(5, 3);
                    ke[aH].P();
                }
                if (bi.y(16) && ke[aH].o != null && !ke[aH].o.M) {
                    if (ke[aH].o.dZ) {
                        dz.cn().b(ke[aH].o.i);
                    } else if (ke[aH].o.ea) {
                        byte b = ke[aH].o.i;
                        if (!bj.fY[b]) {
                            ((bk) ke[aH].o).a(1);
                            String str = null;
                            switch (bj.gx[b][0]) {
                                case 0:
                                    short s = bj.gx[b][1];
                                    short s2 = bj.gx[b][2];
                                    cq().h(s, s2);
                                    valueOf = new StringBuffer().append(bj.gv[s]).append("*").append((int) s2).toString();
                                    str = "获得装备";
                                    break;
                                case 1:
                                    short s3 = bj.gx[b][1];
                                    short s4 = bj.gx[b][2];
                                    cq().k(s3, s4);
                                    valueOf = new StringBuffer().append(r.aC[s3]).append("*").append((int) s4).toString();
                                    str = "获得道具";
                                    break;
                                case 2:
                                    short s5 = bj.gx[b][2];
                                    cq();
                                    a(s5);
                                    str = "获得金钱";
                                    valueOf = String.valueOf((int) s5);
                                    break;
                                default:
                                    valueOf = null;
                                    break;
                            }
                            bj.fY[b] = true;
                            ed.cB().a(str.toCharArray(), valueOf.toCharArray());
                        }
                    }
                }
                if (bi.y(64)) {
                    if (b.m[7] < 0) {
                        f(0);
                    } else {
                        b.Z().b(b.Z().aA, 2);
                        ea.cp().a(7);
                    }
                }
            }
        }
        if (this.ae) {
            if (ke[aH].g == 5) {
                X();
            } else if (ke[aH].g == 6) {
                X();
            }
        }
        ke[aH].Q();
        if (ke[aH].b(ke[aH].aH, ke[aH].dN)) {
            if (ke[aH].aS != null) {
                dz.cn().a(ke[aH].aS.i);
            }
            if (ke[aH].o == null) {
                return;
            }
            if (ke[aH].o.ea) {
                if (!ea.cp().M) {
                    ed.cB().a("宝箱".toCharArray(), 10);
                }
            } else if (ke[aH].o.dZ && !ea.cp().M && ke[aH].o.i > 0) {
                ed.cB().a(dz.aC[ke[aH].o.i - 1].toCharArray(), 10);
            }
        }
    }

    public final void R() {
        if (M) {
            M = false;
            for (byte b = 0; b < m.length; b = (byte) (b + 1)) {
                ke[m[b]].d(4);
                ke[m[b]].a(true);
                ke[m[b]].c(bj.gr.f);
                ke[m[b]].ac = 0;
                ke[m[b]].j = 3;
            }
            N();
        }
        dX = false;
    }

    public final void a(an anVar) {
        this.n.a(ke[aH].aH, ke[aH].dN);
        this.n.a(anVar);
        this.n.Q();
        if (this.n.W()) {
            switch (f) {
                case 0:
                    M = true;
                    bj.bp();
                    bj.P();
                    bj.gr.a(ke[aH].aH, ke[aH].dN);
                    bj.gr.c(ke[aH].f);
                    dX = false;
                    return;
                case 1:
                    for (byte b = 0; b < m.length; b = (byte) (b + 1)) {
                        ke[m[b]].d(4);
                        ke[m[b]].a(true);
                        ke[m[b]].c(bj.gr.f);
                        ke[m[b]].ac = 0;
                        ke[m[b]].j = 3;
                    }
                    N();
                    dX = false;
                    return;
                default:
                    return;
            }
        }
    }

    public final void c() {
        m = new byte[]{0};
        M = false;
        this.ae = true;
        aH = 0;
        aI = new int[3];
        ev = new short[6][];
        dQ = (byte[][][]) Array.newInstance(Byte.TYPE, 6, 3, 2);
        fz = (short[][]) Array.newInstance(Short.TYPE, 3, 3);
        fA = (short[][]) Array.newInstance(Short.TYPE, 4, 2);
        dz.dQ = (byte[][][]) Array.newInstance(Byte.TYPE, 3, 1, 2);
        ai = (short[][][]) Array.newInstance(Short.TYPE, 6, 2, 7);
        for (int i = 0; i < ai.length; i++) {
            for (int i2 = 0; i2 < ai[i][0].length; i2++) {
                ai[i][0][i2] = 1;
            }
        }
        this.n = c.g("/u/fx");
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/init.hy"));
        try {
            O = new short[dataInputStream.readByte()];
            for (byte b = 0; b < O.length; b = (byte) (b + 1)) {
                O[b] = dataInputStream.readShort();
            }
            ab = new short[dataInputStream.readByte()];
            for (int i3 = 0; i3 < ab.length; i3++) {
                ab[i3] = dataInputStream.readShort();
            }
            aq = new short[dataInputStream.readByte()];
            for (int i4 = 0; i4 < aq.length; i4++) {
                aq[i4] = dataInputStream.readShort();
            }
            short readByte = (short) dataInputStream.readByte();
            for (byte b2 = 0; b2 < readByte; b2 = (byte) (b2 + 1)) {
                byte readByte2 = dataInputStream.readByte();
                for (byte b3 = 0; b3 < readByte2; b3 = (byte) (b3 + 1)) {
                    for (byte b4 = 0; b4 < 2; b4 = (byte) (b4 + 1)) {
                        dQ[b2][b3][b4] = (byte) dataInputStream.readShort();
                    }
                }
            }
            short readShort = dataInputStream.readShort();
            for (short s = 0; s < readShort; s = (short) (s + 1)) {
                k(dataInputStream.readShort(), dataInputStream.readShort());
            }
            ac = dataInputStream.readInt();
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    public final void f(int i) {
        byte b = (byte) i;
        f = b;
        switch (b) {
            case 0:
                for (byte b2 = 0; b2 < m.length; b2 = (byte) (b2 + 1)) {
                    ke[m[b2]].a(false);
                }
                this.n.a(0);
                dX = true;
                return;
            case 1:
                if (M) {
                    M = false;
                    this.n.a(1);
                    dX = true;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void f(int i, int i2, int i3, int i4) {
        for (int i5 = 0; i5 < dQ[i].length; i5++) {
            if (dQ[i][i5][1] == i3) {
                h(dQ[i][i5][0], 1);
                dQ[i][i5] = new byte[]{0, 0};
            }
        }
        switch (i3) {
            case 1:
                dQ[i][0] = new byte[]{(byte) i2, (byte) i3};
                break;
            case 2:
                dQ[i][1] = new byte[]{(byte) i2, (byte) i3};
                break;
            case 3:
                dQ[i][2] = new byte[]{(byte) i2, (byte) i3};
                break;
        }
        if (fz.length > 0) {
            short[] sArr = fz[i4];
            sArr[1] = (short) (sArr[1] - 1);
            if (fz[i4][1] <= 0) {
                fz[i4] = new short[]{0, 0};
            }
        }
        U();
    }

    public final void h(int i, int i2) {
        for (int i3 = 0; i3 < fz.length; i3++) {
            if (fz[i3][0] == i) {
                short[] sArr = fz[i3];
                sArr[1] = (short) (sArr[1] + i2);
                return;
            }
        }
        for (int i4 = 0; i4 < fz.length; i4++) {
            if (fz[i4][1] == 0) {
                fz[i4] = new short[]{(short) i, (short) i2};
                this.dN = 0;
                return;
            }
            this.dN = i4;
        }
        if (this.dN == fz.length - 1) {
            short[][] sArr2 = (short[][]) Array.newInstance(Short.TYPE, fz.length + 1, 2);
            for (int i5 = 0; i5 < fz.length; i5++) {
                for (int i6 = 0; i6 < 2; i6++) {
                    sArr2[i5][i6] = fz[i5][i6];
                }
            }
            sArr2[fz.length] = new short[]{(short) i, (short) i2};
            fz = sArr2;
            this.dN = 0;
        }
    }

    public final void i(int i, int i2) {
        h(dQ[i][i2][0], 1);
        dQ[i][i2] = new byte[]{0, 0};
    }

    public final void j(int i, int i2) {
        for (int i3 = 0; i3 < fz.length; i3++) {
            if (fz[i3][0] == i) {
                short[] sArr = fz[i3];
                sArr[1] = (short) (sArr[1] - i2);
                if (fz[i3][1] <= 0) {
                    fz[i3] = new short[]{0, 0};
                }
            }
        }
        U();
    }

    public final void k(int i, int i2) {
        int i3 = 0;
        while (i3 < fA.length) {
            if (fA[i3][1] == 0 || fA[i3][0] != i) {
                i3++;
            } else {
                short[] sArr = fA[i3];
                sArr[1] = (short) (sArr[1] + i2);
                return;
            }
        }
        for (int i4 = 0; i4 < fA.length; i4++) {
            if (fA[i4][1] == 0) {
                fA[i4] = new short[]{(short) i, (short) i2};
                this.dN = 0;
                return;
            }
            this.dN = i4;
        }
        if (this.dN == fA.length - 1) {
            short[][] sArr2 = (short[][]) Array.newInstance(Short.TYPE, fA.length + 1, 2);
            for (int i5 = 0; i5 < fA.length; i5++) {
                for (byte b = 0; b < 2; b = (byte) (b + 1)) {
                    sArr2[i5][b] = fA[i5][b];
                }
            }
            sArr2[fA.length] = new short[]{(short) i, (short) i2};
            fA = sArr2;
            this.dN = 0;
        }
    }

    public final void y(int i, int i2) {
        for (int i3 = 0; i3 < fA.length; i3++) {
            if (fA[i3][0] == i) {
                short[] sArr = fA[i3];
                sArr[1] = (short) (sArr[1] - i2);
                if (fA[i3][1] <= 0) {
                    fA[i3] = new short[]{0, 0};
                }
            }
        }
        for (int i4 = 0; i4 < fA.length; i4++) {
            this.l = (short) (i4 + 1);
            if (fA[i4][1] == 0 && this.l < fA.length) {
                fA[i4] = fA[this.l];
                fA[this.l] = new short[]{0, 0};
            }
        }
    }
}
