package defpackage;

import com.nokia.mid.ui.DirectGraphics;
import java.lang.reflect.Array;
import javax.microedition.media.Player;

/* renamed from: ea  reason: default package */
public final class ea implements dy {
    public static String aF;
    public static int aH;
    public static int ac;
    public static int dO;
    public static int eb;
    private static ea kc;
    public boolean M = false;
    short P = 0;
    public byte R = 17;
    short S = 0;
    private byte W;
    private byte X;
    private byte Y;
    byte Z = 1;
    private String[] aC = {"右软键中御剑飞行", "购买后按9键使用", "角色行动中不遇敌人", "并可以飞越障碍"};
    public char[][] aQ = {"关闭音乐可以使游戏更加流畅".toCharArray(), "战斗中也可购买药品（不消耗回合）".toCharArray(), "御剑飞行避开地图中的敌人".toCharArray(), "双倍经验可加快升级速度".toCharArray(), "装备能极大的提升人物战斗力".toCharArray()};
    public boolean aT;
    public boolean ae = false;
    public int dN = 0;
    public boolean dX = false;
    public boolean dY = false;
    public boolean dZ = false;
    public byte e = 0;
    public boolean ea = false;
    public int ec;
    public boolean em;
    private short[][] ev;
    public byte f;
    public boolean fS;
    public boolean fT;
    public boolean fU;
    public boolean fV;
    public byte g;
    private bj gt = bj.bp();
    public byte h;
    public byte i = 1;
    public byte j;
    public short k;
    public short l;
    public byte[] m;
    private n n;
    private n[] p;
    public ao q;

    private ea() {
    }

    private void Y() {
        a((byte) -1);
        this.fT = false;
        this.l = 0;
        this.i = 1;
        this.q = null;
        bh.c();
    }

    public static void a(int i2, int i3) {
        if (i2 - ac < 80) {
            ac = i2 - 80;
        } else if (i2 - ac > 160) {
            ac = (i2 + 80) - 240;
        }
        if (i3 - aH < 80) {
            aH = i3 - 80;
        } else if (i3 - aH > 240) {
            aH = (i3 + 80) - 320;
        }
        int i4 = ac < 0 ? 0 : ac;
        ac = i4;
        ac = i4 > ec.cA().ed - 240 ? ec.cA().ed - 240 > 0 ? ec.cA().ed - 240 : 0 : ac;
        int i5 = aH < 0 ? 0 : aH;
        aH = i5;
        aH = i5 > ec.cA().ee - 320 ? ec.cA().ee - 320 > 0 ? ec.cA().ee - 320 : 0 : aH;
    }

    private void c(an anVar) {
        o();
        ec cA = ec.cA();
        int i2 = ac;
        int i3 = aH;
        if ((Math.abs(i2 - cA.jY) * cA.ek) + (Math.abs(i3 - cA.kg) * cA.ej) > cA.ek * cA.ej) {
            cA.M = true;
        }
        if (cA.M) {
            cA.g(i2, i3);
            cA.M = false;
        } else {
            cA.jY = i2;
            cA.kg = i3;
            if (i2 > cA.ef + cA.kh) {
                while (cA.jY > cA.ef + cA.kh) {
                    cA.a((cA.ef + cA.ej) / cA.dN, cA.cv(), cA.cz(), cA.cw(), cA.cx());
                    cA.ef += cA.dN;
                }
            } else if (i2 < cA.ef) {
                do {
                    cA.ef -= cA.dN;
                    cA.a(cA.cu(), cA.cv(), cA.cz(), cA.cw(), cA.cx());
                } while (cA.jY < cA.ef);
            }
            if (i3 > cA.eg + cA.ki) {
                while (cA.kg > cA.eg + cA.ki) {
                    cA.b(cA.cu(), (cA.eg + cA.ek) / cA.dO, cA.cy(), cA.cw(), cA.cx());
                    cA.eg += cA.dO;
                }
            } else if (i3 < cA.eg) {
                do {
                    cA.eg -= cA.dO;
                    cA.b(cA.cu(), cA.cv(), cA.cy(), cA.cw(), cA.cx());
                } while (cA.eg > cA.kg);
            }
        }
        du.c(anVar, 0, 0, 0, 240, 320);
        ec cA2 = ec.cA();
        int i4 = cA2.jY % cA2.ej;
        int i5 = cA2.kg % cA2.ek;
        int i6 = cA2.ej - i4;
        int i7 = cA2.ek - i5;
        cA2.a(anVar, cA2.s, i4, i5, i6, i7, 0, 0, 0);
        cA2.a(anVar, cA2.s, 0, i5, cA2.eh - i6, i7, 0, i6 + 0, 0);
        cA2.a(anVar, cA2.s, i4, 0, i6, cA2.ei - i7, 0, 0, i7 + 0);
        cA2.a(anVar, cA2.s, 0, 0, cA2.eh - i6, cA2.ei - i7, 0, i6 + 0, i7 + 0);
        for (int i8 = 0; i8 < this.f; i8++) {
            this.gt.p[this.m[i8]].N();
            this.gt.p[this.m[i8]].a(anVar);
        }
        if (this.e == 1) {
            du.a(anVar, bi.q, 0, 240 - bi.q.getWidth(), 10, 0);
        }
        if (this.e == 1) {
            a.M().n.a(22, 320);
            a.M().n.c(anVar);
            a.M().n.Q();
        }
        if (this.e == 1) {
            a.M().o.a(225, 318);
            a.M().o.c(anVar);
            a.M().o.Q();
        }
    }

    public static ea cp() {
        if (kc == null) {
            kc = new ea();
        }
        return kc;
    }

    private void d(an anVar) {
        switch (this.X) {
            case 0:
                anVar.setColor(16777215);
                for (byte b = 0; b < this.ev.length; b = (byte) (b + 1)) {
                    if (this.ev[b][0] == 0) {
                        this.ev[b][0] = (short) ee.q(13, 17);
                        this.ev[b][1] = (short) ee.q(0, 560);
                        short[] sArr = this.ev[b];
                        short[] sArr2 = this.ev[b];
                        short s = (short) (this.ev[b][1] - 240);
                        sArr2[2] = s;
                        sArr[2] = s < 0 ? 0 : this.ev[b][0];
                        short[] sArr3 = this.ev[b];
                        sArr3[1] = (short) (sArr3[1] - this.ev[b][2]);
                        this.ev[b][3] = (short) ee.q(4, 20);
                    } else if (this.ev[b][2] > 320 || this.ev[b][1] < 0) {
                        this.ev[b][0] = 0;
                    } else {
                        if (this.ev[b][0] < 5) {
                            anVar.a(this.ev[b][1] - (this.ev[b][3] >> this.ev[b][0]), this.ev[b][2] - ((this.ev[b][3] >> this.ev[b][0]) >> 1), (this.ev[b][3] >> this.ev[b][0]) << 1, this.ev[b][3] >> this.ev[b][0], 0, 360);
                        } else {
                            anVar.a(this.ev[b][1], this.ev[b][2], this.ev[b][1] + ((this.ev[b][3] * this.Y) >> 4), this.ev[b][2] - this.ev[b][3]);
                            short[] sArr4 = this.ev[b];
                            sArr4[1] = (short) (sArr4[1] - ((this.ev[b][3] * this.Y) >> 3));
                            short[] sArr5 = this.ev[b];
                            sArr5[2] = (short) (sArr5[2] + (this.ev[b][3] << 1));
                        }
                        short[] sArr6 = this.ev[b];
                        sArr6[0] = (short) (sArr6[0] - 1);
                    }
                }
                return;
            case 1:
                for (byte b2 = 0; b2 < this.ev.length; b2 = (byte) (b2 + 1)) {
                    if (this.ev[b2][0] == 0) {
                        this.ev[b2][0] = 45;
                        this.ev[b2][1] = (short) ee.q(4, 8);
                        this.ev[b2][2] = (short) ee.q(-30, DirectGraphics.ROTATE_270);
                        this.ev[b2][3] = (short) ee.q(-100, DirectGraphics.ROTATE_180);
                        this.ev[b2][4] = this.ev[b2][1];
                    } else {
                        short[] sArr7 = this.ev[b2];
                        sArr7[3] = (short) (sArr7[3] + ee.q(2, 5));
                        if (this.ev[b2][4] > 2) {
                            short[] sArr8 = this.ev[b2];
                            sArr8[2] = (short) (sArr8[2] + ee.q((-this.ev[b2][1]) << 1, this.ev[b2][1] << 1));
                            this.ev[b2][4] = 0;
                        } else {
                            short[] sArr9 = this.ev[b2];
                            sArr9[4] = (short) (sArr9[4] + 1);
                        }
                        anVar.setColor(5592575);
                        anVar.b(this.ev[b2][2] - (this.ev[b2][1] >> 2), this.ev[b2][3], this.ev[b2][1], this.ev[b2][1], 0, 360);
                        anVar.setColor(16777215);
                        this.Y = (byte) ((this.ev[b2][1] - 2) - (this.ev[b2][1] >> 3));
                        anVar.b(this.ev[b2][2] - (this.Y >> 2), this.ev[b2][3], this.Y, this.Y, 0, 360);
                        if (this.ev[b2][3] > 320) {
                            this.ev[b2][0] = 0;
                        }
                    }
                }
                return;
            case 2:
                for (byte b3 = 0; b3 < this.ev.length; b3 = (byte) (b3 + 1)) {
                    if (this.ev[b3][0] == 0) {
                        this.ev[b3][0] = (short) ee.q(Player.PREFETCHED, 500);
                        this.ev[b3][1] = (short) ee.q(1, 5);
                        this.ev[b3][2] = (short) ee.q(0, 560);
                        this.ev[b3][3] = this.ev[b3][2] < 240 ? 0 : (short) (this.ev[b3][2] - 240);
                        this.ev[b3][2] = this.ev[b3][2] > 240 ? 240 : this.ev[b3][2];
                        this.ev[b3][4] = this.ev[b3][1];
                    } else {
                        if (this.ev[b3][4] > 2) {
                            short[] sArr10 = this.ev[b3];
                            sArr10[2] = (short) (sArr10[2] + ee.q(-((this.ev[b3][1] * 6) >> 3), (this.ev[b3][1] * 6) >> 3));
                        } else {
                            short[] sArr11 = this.ev[b3];
                            sArr11[4] = (short) (sArr11[4] + 1);
                        }
                        short[] sArr12 = this.ev[b3];
                        sArr12[2] = (short) (sArr12[2] - ((this.ev[b3][1] * 3) >> 3));
                        short[] sArr13 = this.ev[b3];
                        sArr13[3] = (short) (sArr13[3] + (this.ev[b3][1] >> 1));
                        short[] sArr14 = this.ev[b3];
                        sArr14[0] = (short) (sArr14[0] - 1);
                        switch (ee.q(0, 2)) {
                            case 0:
                                anVar.setColor(12679365);
                                break;
                            case 1:
                                anVar.setColor(7035029);
                                break;
                            case 2:
                                anVar.setColor(9004471);
                                break;
                        }
                        anVar.c(this.ev[b3][2] + (this.ev[b3][1] >> 2), this.ev[b3][3], this.ev[b3][1] >> 1, this.ev[b3][1]);
                        anVar.c(this.ev[b3][2], this.ev[b3][3] + (this.ev[b3][1] >> 2), this.ev[b3][1], this.ev[b3][1] >> 1);
                        if (this.ev[b3][2] < -20 || this.ev[b3][3] > 335) {
                            this.ev[b3][0] = 0;
                        }
                    }
                }
                return;
            case 3:
                for (byte b4 = 0; b4 < this.ev.length; b4 = (byte) (b4 + 1)) {
                    if (this.ev[b4][0] == 0) {
                        this.ev[b4][0] = (short) ee.q(1, 3);
                        this.ev[b4][1] = (short) ee.q(10, 230);
                        this.ev[b4][2] = (short) ee.q(20, 325);
                        this.ev[b4][3] = (short) ee.q(2, 3);
                        this.ev[b4][4] = (short) ee.q(0, 4);
                        this.p[b4].c(this.ev[b4][4]);
                    } else if (this.ev[b4][2] < 10) {
                        this.ev[b4][0] = 0;
                    } else {
                        this.p[b4].a(this.ev[b4][1], this.ev[b4][2]);
                        this.p[b4].c(anVar);
                        this.p[b4].Q();
                        short[] sArr15 = this.ev[b4];
                        sArr15[2] = (short) (sArr15[2] - (this.ev[b4][3] << 1));
                    }
                }
                return;
            case 4:
                for (byte b5 = 0; b5 < this.ev.length; b5 = (byte) (b5 + 1)) {
                    if (this.ev[b5][0] == 0) {
                        this.ev[b5][0] = (short) ee.q(1, 2);
                        this.ev[b5][1] = (short) ee.q(10, 230);
                        this.ev[b5][2] = (short) ee.q(-10, 30);
                        this.ev[b5][3] = (short) ee.q(1, 2);
                        this.ev[b5][4] = (short) ee.q(0, 1);
                        this.p[b5].c(this.ev[b5][4]);
                        this.ev[b5][5] = (short) ee.q(-1, 1);
                    } else if (this.ev[b5][2] > 320 || this.ev[b5][1] < 0 || this.ev[b5][1] > 240) {
                        this.ev[b5][0] = 0;
                    } else {
                        this.p[b5].a(this.ev[b5][1], this.ev[b5][2]);
                        this.p[b5].c(anVar);
                        this.p[b5].Q();
                        short[] sArr16 = this.ev[b5];
                        sArr16[2] = (short) (sArr16[2] + this.ev[b5][3]);
                        short[] sArr17 = this.ev[b5];
                        sArr17[1] = (short) (sArr17[1] + this.ev[b5][5]);
                    }
                }
                return;
            default:
                return;
        }
    }

    private void h(an anVar) {
        anVar.setColor(0);
        anVar.c(0, 0, 240, 320);
        d(anVar);
        anVar.a("跳过", 240 - (p.f * 2), 320 - p.e, 0);
        this.ec = this.q.getWidth() / this.R;
        this.l = (short) (this.l + 1);
        if (this.l % 30 == 0) {
            this.i = (byte) (this.i + 1);
            this.j = (byte) (this.j + 1);
        }
        if (this.h == 3) {
            if (this.i >= this.ec) {
                this.i = (byte) this.ec;
            }
            for (byte b = 0; b < this.i; b++) {
                du.a(anVar, this.q, b * this.R, 0, this.R, 139, 0, (246 - (this.i * 25)) + (b * 25), 160 - (this.q.getHeight() / 2));
            }
            if (this.j > this.ec + 1) {
                Y();
                dz.cn().N();
            }
        } else if (this.h != 4) {
        } else {
            if (this.j > this.ec) {
                Y();
                dz.cn().N();
                return;
            }
            for (byte b2 = 0; b2 < this.i; b2++) {
                du.a(anVar, this.q, (this.R * b2) + 0, 0, this.R, 139, 0, (95 - (this.q.getWidth() / 2)) + (b2 * 25), 160 - (this.q.getHeight() / 2));
            }
        }
    }

    private void l(an anVar) {
        a.M().b(anVar, 1);
        anVar.setColor(2035968);
        for (int i2 = 0; i2 < this.aC.length; i2++) {
            anVar.a(this.aC[i2], 120, ((160 - p.e) - 10) + (i2 * 20), 33);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0059 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void o() {
        /*
            r8 = this;
            r7 = 1
            r4 = 0
            r8.f = r4
            r0 = r4
        L_0x0005:
            byte[] r1 = r8.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x0011
            r0 = r4
        L_0x000b:
            byte r1 = r8.f
            int r1 = r1 - r7
            if (r0 < r1) goto L_0x005f
            return
        L_0x0011:
            bj r1 = defpackage.bj.bp()
            n[] r1 = r1.p
            r1 = r1[r0]
            boolean r2 = r1.M
            if (r2 == 0) goto L_0x0021
            boolean r2 = r1.ae
            if (r2 == 0) goto L_0x005d
        L_0x0021:
            short r2 = r1.S
            int r3 = r1.aH
            int r2 = r2 + r3
            int r3 = defpackage.ea.ac
            if (r2 <= r3) goto L_0x005d
            short r2 = r1.P
            int r3 = r1.aH
            int r2 = r2 + r3
            int r3 = defpackage.ea.ac
            int r3 = r3 + 240
            if (r2 >= r3) goto L_0x005d
            short r2 = r1.l
            int r3 = r1.dN
            int r2 = r2 + r3
            int r3 = defpackage.ea.aH
            int r3 = r3 + 320
            if (r2 >= r3) goto L_0x005d
            short r2 = r1.T
            int r1 = r1.dN
            int r1 = r1 + r2
            int r2 = defpackage.ea.aH
            if (r1 <= r2) goto L_0x005d
            r1 = r7
        L_0x004a:
            if (r1 == 0) goto L_0x0059
            byte[] r1 = r8.m
            byte r2 = r8.f
            r1[r2] = r0
            byte r1 = r8.f
            int r1 = r1 + 1
            byte r1 = (byte) r1
            r8.f = r1
        L_0x0059:
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x0005
        L_0x005d:
            r1 = r4
            goto L_0x004a
        L_0x005f:
            int r1 = r0 + 1
            byte r1 = (byte) r1
            r2 = r0
        L_0x0063:
            byte r3 = r8.f
            if (r1 < r3) goto L_0x007d
            if (r2 == r0) goto L_0x0079
            byte[] r1 = r8.m
            byte r1 = r1[r0]
            byte[] r3 = r8.m
            byte[] r4 = r8.m
            byte r4 = r4[r2]
            r3[r0] = r4
            byte[] r3 = r8.m
            r3[r2] = r1
        L_0x0079:
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x000b
        L_0x007d:
            bj r3 = r8.gt
            n[] r3 = r3.p
            byte[] r4 = r8.m
            byte r4 = r4[r1]
            r3 = r3[r4]
            int r3 = r3.dN
            bj r4 = r8.gt
            n[] r4 = r4.p
            byte[] r5 = r8.m
            byte r5 = r5[r1]
            r4 = r4[r5]
            short r4 = r4.ao
            int r3 = r3 + r4
            bj r4 = r8.gt
            n[] r4 = r4.p
            byte[] r5 = r8.m
            byte r5 = r5[r2]
            r4 = r4[r5]
            int r4 = r4.dN
            bj r5 = r8.gt
            n[] r5 = r5.p
            byte[] r6 = r8.m
            byte r6 = r6[r2]
            r5 = r5[r6]
            short r5 = r5.ao
            int r4 = r4 + r5
            if (r3 >= r4) goto L_0x00b2
            r2 = r1
        L_0x00b2:
            int r1 = r1 + 1
            byte r1 = (byte) r1
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ea.o():void");
    }

    public final void N() {
        a(0);
    }

    public final void Q() {
        switch (this.e) {
            case 0:
                switch (this.W) {
                    case 0:
                        bi.bo().dX = true;
                        bh.c();
                        bj.bp();
                        bj.bp().R();
                        bi.ac = 0;
                        dz.cn().M = false;
                        break;
                    case 1:
                        bi.bo().N();
                        break;
                    case 2:
                        bj.bp();
                        bj.a(bj.bp().eb);
                        break;
                    case 3:
                        bj.bp().c(bj.bp().eb);
                        break;
                    case 4:
                        bj.bp();
                        bj.d(bj.bp().eb);
                        this.m = new byte[this.gt.p.length];
                        break;
                    case 5:
                        eb.ke[eb.aH].a(this.gt.aH, this.gt.dN);
                        if (!eb.M) {
                            eb.ke[eb.aH].c(this.gt.dO);
                            eb.cq().N();
                        }
                        a(this.gt.aH, this.gt.dN);
                        this.gt.S();
                        break;
                    case 6:
                        dz.cn().R();
                        break;
                    case 7:
                        ec cA = ec.cA();
                        du.c(cA.kj, 0, 0, 0, cA.ej, cA.ek);
                        ec.cA().g(ac, aH);
                        o();
                        for (int i2 = 0; i2 < eb.ke.length; i2++) {
                            if (eb.m.length > i2) {
                                eb.ke[i2].M = false;
                            }
                        }
                        break;
                    case 8:
                        a.M().c();
                        break;
                    case 9:
                        bi.bo().N();
                        break;
                    case 10:
                        bi.bo().dX = false;
                        ed.cB().a(new StringBuffer().append(dz.aE[bj.ac]).append("").toString().toCharArray(), 40);
                        a(6);
                        dz.cn().P();
                        if (eb.M) {
                            eb.cq();
                            eb.Q();
                            bj.gr.c(this.gt.dO);
                        }
                        for (int i3 = 0; i3 < eb.m.length; i3++) {
                            eb.ke[i3].c(eb.ke[0].f);
                        }
                        this.W = 0;
                        return;
                }
                this.W = (byte) (this.W + 1);
                return;
            case 1:
                for (n c : this.gt.p) {
                    c.c();
                }
                eb.cq().P();
                a(eb.ke[eb.aH].aH, eb.ke[eb.aH].dN);
                return;
            case 2:
                a.M().Q();
                return;
            case 3:
                bg.aU().Q();
                return;
            case 4:
            default:
                return;
            case 5:
                a.M().J();
                return;
            case 6:
                dz.cn().P();
                if (this.ae) {
                    ed.cB().c();
                    return;
                }
                return;
            case 7:
                switch (b.Z().i) {
                    case 0:
                        b.Z().W();
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        if (!b.Z().W()) {
                            a(1);
                            return;
                        }
                        return;
                }
            case 8:
                q.ao().N();
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void X() {
        this.ea = false;
        System.out.println("free");
    }

    public final void a(byte b) {
        if (b == -1) {
            this.dY = false;
            this.ev = null;
            this.n = null;
            this.p = null;
            return;
        }
        this.dY = true;
        this.X = b;
        switch (b) {
            case 0:
                this.ev = (short[][]) Array.newInstance(Short.TYPE, 60, 4);
                this.Y = 6;
                return;
            case 1:
                this.ev = (short[][]) Array.newInstance(Short.TYPE, 60, 5);
                return;
            case 2:
                this.ev = (short[][]) Array.newInstance(Short.TYPE, 60, 5);
                this.Y = 6;
                return;
            case 3:
                this.n = c.g("/u/bs");
                this.p = new n[60];
                for (int i2 = 0; i2 < this.p.length; i2++) {
                    this.p[i2] = n.a(this.n);
                }
                this.ev = (short[][]) Array.newInstance(Short.TYPE, 60, 5);
                return;
            case 4:
                this.n = c.g("/u/hb");
                this.p = new n[15];
                for (int i3 = 0; i3 < this.p.length; i3++) {
                    this.p[i3] = n.a(this.n);
                }
                this.ev = (short[][]) Array.newInstance(Short.TYPE, 15, 6);
                return;
            default:
                return;
        }
    }

    public final void a(int i2) {
        switch (i2) {
            case 0:
                this.W = 0;
                this.g = (byte) ee.q(0, this.aQ.length - 1);
                break;
            case 1:
                cp().dX = false;
                break;
            case 2:
                a.M().N();
                break;
            case 3:
                this.g = (byte) ee.q(0, this.aQ.length - 1);
                break;
            case 6:
                if (ec.cA().q == null) {
                    ec.cA().P();
                }
                bi.ac = 0;
                break;
        }
        this.e = (byte) i2;
    }

    public final void a(an anVar) {
        switch (this.e) {
            case 0:
                du.a(anVar, 0, 0, 240, 320, 0);
                du.a(anVar, "江湖风云录之御剑问情", 120, 5, 17, 16439945);
                anVar.setColor(16777215);
                byte b = (byte) (230 / p.f);
                for (byte b2 = 0; b2 < this.aQ[this.g].length; b2 = (byte) (b2 + 1)) {
                    anVar.a(this.aQ[this.g][b2], (p.f * (b2 % b)) + 5, p.e + 10 + (p.e * (b2 / b)), 20);
                }
                du.a(anVar, new StringBuffer().append("加载中 ").append(this.W * 10).append("%").toString(), 120, 280, 33, 3689850);
                bg.aU().a(anVar, this.W);
                break;
            case 1:
            case 6:
                du.a(anVar, 0, 0, 240, 320, 0);
                c(anVar);
                if (eb.dX) {
                    eb.cq().a(anVar);
                }
                if (eb.M) {
                    bj.gr.Q();
                    bj.gr.a(anVar);
                }
                if (this.dX) {
                    bj.gq.Q();
                    bj.gq.a(anVar);
                }
                if (this.M) {
                    ed.cB().c(anVar);
                }
                if (this.fS) {
                    anVar.setColor(0);
                    anVar.c(0, 0, 240, 320);
                    du.a(anVar, this.q, 0, 120, (this.q.getHeight() / 2) + 160, 33);
                }
                if (this.dY) {
                    d(anVar);
                }
                if (this.fT) {
                    h(anVar);
                }
                if (this.aT) {
                    if (dO + this.k < (-aF.length()) * p.f) {
                        this.k = 0;
                    } else {
                        this.k = (short) (this.k - 2);
                    }
                    du.a(anVar, aF, dO + this.k, eb, 0, 16711680);
                }
                if (this.fU) {
                    l(anVar);
                }
                if (this.fV) {
                    a.M().c(anVar);
                    break;
                }
                break;
            case 2:
                if (!a.M().ae) {
                    if (this.dZ) {
                        c(anVar);
                        this.dZ = false;
                    }
                    a.M().a(anVar);
                    break;
                }
                break;
            case 3:
                bg.aU().a(anVar);
                break;
            case 5:
                a.M().l(anVar);
                break;
            case 7:
                if (this.dZ) {
                    c(anVar);
                    this.dZ = false;
                }
                b.Z().a(anVar);
                break;
            case 8:
                c(anVar);
                q.ao().a(anVar);
                break;
        }
        if (ed.M) {
            ed.cB().d(anVar);
        }
        if (this.em) {
            anVar.setColor(0);
            anVar.c(0, 0, 240, 320);
        }
        if (this.ae) {
            ed.cB().a(anVar);
        }
    }

    public final void c() {
    }
}
