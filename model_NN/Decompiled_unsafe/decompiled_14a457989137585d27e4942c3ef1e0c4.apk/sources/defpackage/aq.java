package defpackage;

import android.view.ViewGroup;
import java.util.Iterator;

/* renamed from: aq  reason: default package */
final class aq implements Runnable {
    final /* synthetic */ ap fl;

    aq(ap apVar) {
        this.fl = apVar;
    }

    public final void run() {
        Iterator it = this.fl.eV.iterator();
        while (it.hasNext()) {
            ((ViewGroup) this.fl.getView()).addView(((af) it.next()).az());
        }
    }
}
