package defpackage;

import android.graphics.Paint;
import android.graphics.Typeface;

/* renamed from: dd  reason: default package */
public final class dd {
    public final Paint ew = new Paint(1);
    public Paint.FontMetricsInt ip;
    final char[] iq = new char[1];

    public dd(Typeface typeface, int i, boolean z) {
        this.ew.setTypeface(typeface);
        this.ew.setTextSize((float) i);
        this.ew.setUnderlineText(z);
        this.ip = this.ew.getFontMetricsInt();
    }
}
