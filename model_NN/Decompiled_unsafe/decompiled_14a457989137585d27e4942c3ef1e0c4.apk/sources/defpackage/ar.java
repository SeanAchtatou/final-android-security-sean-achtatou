package defpackage;

import android.view.ViewGroup;
import java.util.Iterator;

/* renamed from: ar  reason: default package */
final class ar implements Runnable {
    final /* synthetic */ ap fl;

    ar(ap apVar) {
        this.fl = apVar;
    }

    public final void run() {
        Iterator it = this.fl.eV.iterator();
        while (it.hasNext()) {
            ((ViewGroup) this.fl.getView()).removeView(((af) it.next()).az());
        }
    }
}
