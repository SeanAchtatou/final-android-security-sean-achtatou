package defpackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: dz  reason: default package */
public final class dz {
    public static String[] aC;
    public static String[] aE;
    public static byte[][][] dQ;
    public static byte e;
    public static String[][] jZ = null;
    private static dz kb;
    public boolean M;
    public int aH;
    private int[] aI = new int[10];
    public int ac;
    private boolean ae = false;
    private int dN;
    public byte[][] dR;
    private boolean dX = false;
    private boolean dY = true;
    private boolean dZ = false;
    private byte f = 0;
    private byte g;
    private String[] gs;
    private bj gt = bj.bp();
    private int[][] ka;
    private byte[] m;

    private dz() {
    }

    private void J() {
        if (this.ae) {
            switch (this.dR[this.ac][0]) {
                case 1:
                case 41:
                    this.aI[3] = (this.aI[1] - ea.ac) / this.aI[0];
                    this.aI[4] = (this.aI[2] - ea.aH) / this.aI[0];
                    ea.ac += this.aI[3];
                    ea.aH += this.aI[4];
                    ea.ac = ea.ac < 0 ? 0 : ea.ac > ec.cA().ed - 240 ? ec.cA().ed - 240 > 0 ? ec.cA().ed - 240 : 0 : ea.ac;
                    ea.aH = ea.aH < 0 ? 0 : ea.aH > ec.cA().ee - 320 ? ec.cA().ee - 320 > 0 ? ec.cA().ee - 320 : 0 : ea.aH;
                    int[] iArr = this.aI;
                    iArr[0] = iArr[0] - 1;
                    if (this.aI[0] <= 0) {
                        N();
                        return;
                    }
                    return;
                case 2:
                    if (!ea.cp().ae) {
                        ea.cp().dX = false;
                        N();
                        return;
                    }
                    return;
                case 5:
                case 46:
                case 98:
                    if (!ea.cp().ae) {
                        N();
                        return;
                    }
                    return;
                case 10:
                    this.aI[6] = (-(this.gt.p[this.aI[1]].aH - this.aI[2])) / this.aI[0];
                    this.aI[7] = (-(this.gt.p[this.aI[1]].dN - this.aI[3])) / this.aI[0];
                    this.gt.p[this.aI[1]].h(this.aI[6], this.aI[7]);
                    if (this.aI[5] == 0) {
                        ea.cp();
                        ea.a(this.gt.p[this.aI[1]].aH, this.gt.p[this.aI[1]].dN);
                    }
                    int[] iArr2 = this.aI;
                    iArr2[0] = iArr2[0] - 1;
                    if (this.aI[0] <= 0) {
                        if (this.aI[4] != -1) {
                            this.gt.p[this.aI[1]].d(4);
                        }
                        N();
                        return;
                    }
                    return;
                case 13:
                    if (bj.gq.W()) {
                        ea.cp().dX = false;
                        N();
                        this.aI[0] = 0;
                    }
                    bj.gq.Q();
                    return;
                case 30:
                    if (!bi.bo().dY) {
                        ea.cp().a(0);
                        this.ac = 0;
                        U();
                        return;
                    }
                    return;
                case 31:
                    ea.ac += this.aI[1];
                    ea.aH += this.aI[2];
                    int i = ea.ac < 0 ? 0 : ea.ac;
                    ea.ac = i;
                    ea.ac = i > ec.cA().ed - 240 ? ec.cA().ed - 240 > 0 ? ec.cA().ed - 240 : 0 : ea.ac;
                    int i2 = ea.aH < 0 ? 0 : ea.aH;
                    ea.aH = i2;
                    ea.aH = i2 > ec.cA().ee - 320 ? ec.cA().ee - 320 > 0 ? ec.cA().ee - 320 : 0 : ea.aH;
                    int[] iArr3 = this.aI;
                    iArr3[0] = iArr3[0] - 1;
                    if (this.aI[0] <= 0) {
                        N();
                        return;
                    }
                    return;
                case 32:
                    this.gt.p[this.aI[1]].h(this.aI[2], this.aI[3]);
                    if (this.aI[5] == 0) {
                        ea.cp();
                        ea.a(this.gt.p[this.aI[1]].aH, this.gt.p[this.aI[1]].dN);
                    }
                    int[] iArr4 = this.aI;
                    iArr4[0] = iArr4[0] - 1;
                    if (this.aI[0] <= 0) {
                        if (this.aI[4] != -1) {
                            this.gt.p[this.aI[1]].d(4);
                        }
                        N();
                        return;
                    }
                    return;
                case 33:
                    if (!bi.bo().dY) {
                        bi.bo().dY = true;
                        N();
                        return;
                    }
                    return;
                case 39:
                    int[] iArr5 = this.aI;
                    iArr5[0] = iArr5[0] - 1;
                    if (this.aI[0] <= 0) {
                        N();
                        return;
                    }
                    return;
                case 60:
                    ea.ac = this.aI[1] + ee.q(-2, 2);
                    ea.aH = this.aI[2] + ee.q(-2, 2);
                    ea.ac = ea.ac < 0 ? 0 : (ea.ac <= ec.cA().ed - 240 || ec.cA().ed - 240 < 0) ? ea.ac : ec.cA().ed - 240;
                    ea.aH = ea.aH < 0 ? 0 : (ea.aH <= ec.cA().ee - 320 || ec.cA().ee - 320 < 0) ? ea.aH : ec.cA().ee - 320;
                    if (this.aI[0] <= 0) {
                        ea.ac = this.aI[1];
                        ea.aH = this.aI[2];
                        N();
                        return;
                    }
                    int[] iArr6 = this.aI;
                    iArr6[0] = iArr6[0] - 1;
                    return;
                case 62:
                    if (ea.cp().ea) {
                        ea cp = ea.cp();
                        if (cp.S > cp.P) {
                            cp.X();
                            return;
                        }
                        if (bi.y(128)) {
                            cp.X();
                        } else if (bi.q(16)) {
                            cp.S = (short) (cp.S + (cp.Z << 2));
                        }
                        cp.S = (short) (cp.S + cp.Z);
                        return;
                    }
                    N();
                    P();
                    return;
                case 71:
                    ed.cB().N();
                    if (ea.cp().dX) {
                        bj.gq.Q();
                        return;
                    }
                    return;
                case 79:
                    if (!ea.cp().ae) {
                        if (bj.bp().u("mn2_l1")) {
                            ed.cB().a(5, "存档成功!".toCharArray());
                            return;
                        }
                        return;
                    } else if (bi.y(16)) {
                        ea.cp().ae = false;
                        N();
                        return;
                    } else {
                        return;
                    }
                case 85:
                    switch (this.aI[0]) {
                        case 0:
                            bj.gr.h(0, this.aI[1]);
                            eb.ke[eb.aH].h(0, this.aI[1]);
                            break;
                        case 1:
                            bj.gr.h(0, -this.aI[1]);
                            eb.ke[eb.aH].h(0, -this.aI[1]);
                            break;
                        case 2:
                            bj.gr.h(-this.aI[1], 0);
                            eb.ke[eb.aH].h(-this.aI[1], 0);
                            break;
                        case 3:
                            bj.gr.h(this.aI[1], 0);
                            eb.ke[eb.aH].h(this.aI[1], 0);
                            break;
                    }
                    if (this.aI[3] == 0) {
                        ea.cp();
                        ea.a(bj.gr.aH, bj.gr.dN);
                    }
                    int[] iArr7 = this.aI;
                    iArr7[2] = iArr7[2] - 1;
                    if (this.aI[2] <= 0) {
                        N();
                        return;
                    }
                    return;
                case 92:
                    ea cp2 = ea.cp();
                    cp2.l = (short) (cp2.l + 1);
                    if (ea.cp().l > 80) {
                        ea.cp().fS = false;
                        ea.cp().l = 0;
                        ea.cp().q = null;
                        ea.cp().a((byte) -1);
                        N();
                        return;
                    }
                    return;
                case 95:
                    if (bi.y(1280)) {
                        ea.cp().j = (byte) (ea.cp().ec + 2);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static void Q() {
        try {
            "/s/".getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/s/n.hy"));
            int readUnsignedByte = dataInputStream.readUnsignedByte();
            aC = new String[readUnsignedByte];
            for (int i = 0; i < readUnsignedByte; i++) {
                aC[i] = dataInputStream.readUTF();
            }
            jZ = new String[dataInputStream.readUnsignedByte()][];
            for (short s = 0; s < jZ.length; s = (short) (s + 1)) {
                int readUnsignedByte2 = dataInputStream.readUnsignedByte();
                jZ[s] = new String[readUnsignedByte2];
                for (int i2 = 0; i2 < readUnsignedByte2; i2++) {
                    jZ[s][i2] = dataInputStream.readUTF();
                }
            }
            int readUnsignedByte3 = dataInputStream.readUnsignedByte();
            aE = new String[readUnsignedByte3];
            for (int i3 = 0; i3 < readUnsignedByte3; i3++) {
                aE[i3] = dataInputStream.readUTF();
            }
            dataInputStream.close();
            System.gc();
        } catch (IOException e2) {
        }
    }

    private void T() {
        byte b = b.m[7] < 0 ? (byte) 1 : 0;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 87 && this.dR[i][1] == b) {
                this.ac = i;
                N();
                return;
            }
        }
        d();
    }

    private void U() {
        if (!this.dZ) {
            this.dY = false;
            this.ae = false;
        }
    }

    private void V() {
        byte length = (byte) eb.m.length;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 86 && this.dR[i][1] == length) {
                this.ac = i;
                N();
                return;
            }
        }
        d();
    }

    private void a(byte b, byte b2) {
        if (b <= 89 || b == 116) {
            bj.N();
            this.aI[0] = bj.gq.dR[b2].length << 1;
            this.aI[1] = w(b);
            bj.gq.j = 2;
            bj.gq.a(b2);
            bj.gq.a(this.gt.p[this.aI[1]].aH - 10, this.gt.p[this.aI[1]].dN - 27);
            ea.cp().dX = true;
        }
        o();
    }

    private void a(byte b, byte b2, byte b3) {
        byte b4 = eb.ac >= ee.c(b2, b3) ? (byte) 1 : 0;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 82 && this.dR[i][1] == b && this.dR[i][4] == b4) {
                this.ac = i;
                N();
                return;
            }
        }
        d();
    }

    private void a(byte b, int i, int i2, byte b2) {
        int i3 = 0;
        while (i3 < this.dR.length) {
            if (this.dR[i3][0] != 30 || this.dR[i3][1] != this.g || bj.N[e] != this.dR[i3][5] || this.dR[i3][4] != e) {
                i3++;
            } else if (b != -1) {
                this.ac = i3;
                N();
                return;
            } else {
                d();
                return;
            }
        }
        if (b == -1) {
            N();
            return;
        }
        this.gt.aH = i;
        this.gt.dN = i2;
        this.gt.dO = b2;
        this.gt.eb = b;
        ea.cp().a((byte) -1);
        c(-1);
        o();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r8, int r9, byte r10) {
        /*
            r7 = this;
            r6 = 160(0xa0, float:2.24E-43)
            r5 = 120(0x78, float:1.68E-43)
            r4 = 320(0x140, float:4.48E-43)
            r2 = 240(0xf0, float:3.36E-43)
            r3 = 0
            if (r10 != 0) goto L_0x0063
            int r0 = r8 - r5
            defpackage.ea.ac = r0
            int r0 = r9 - r6
            defpackage.ea.aH = r0
            int r0 = defpackage.ea.ac
            if (r0 >= 0) goto L_0x0025
            r0 = r3
        L_0x0018:
            defpackage.ea.ac = r0
            int r0 = defpackage.ea.aH
            if (r0 >= 0) goto L_0x0044
            r0 = r3
        L_0x001f:
            defpackage.ea.aH = r0
            r7.N()
        L_0x0024:
            return
        L_0x0025:
            int r0 = defpackage.ea.ac
            ec r1 = defpackage.ec.cA()
            int r1 = r1.ed
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0041
            ec r0 = defpackage.ec.cA()
            int r0 = r0.ed
            int r0 = r0 - r2
            if (r0 < 0) goto L_0x0041
            ec r0 = defpackage.ec.cA()
            int r0 = r0.ed
            int r0 = r0 - r2
            goto L_0x0018
        L_0x0041:
            int r0 = defpackage.ea.ac
            goto L_0x0018
        L_0x0044:
            int r0 = defpackage.ea.aH
            ec r1 = defpackage.ec.cA()
            int r1 = r1.ee
            int r1 = r1 - r4
            if (r0 <= r1) goto L_0x0060
            ec r0 = defpackage.ec.cA()
            int r0 = r0.ee
            int r0 = r0 - r4
            if (r0 < 0) goto L_0x0060
            ec r0 = defpackage.ec.cA()
            int r0 = r0.ee
            int r0 = r0 - r4
            goto L_0x001f
        L_0x0060:
            int r0 = defpackage.ea.aH
            goto L_0x001f
        L_0x0063:
            int[] r0 = r7.aI
            r1 = 1
            int r2 = r8 - r5
            r0[r1] = r2
            int[] r0 = r7.aI
            r1 = 2
            int r2 = r9 - r6
            r0[r1] = r2
            int[] r0 = r7.aI
            r0[r3] = r10
            r7.o()
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dz.a(int, int, byte):void");
    }

    private void a(int i, int i2, int i3, int i4, byte b) {
        n nVar = null;
        byte b2 = 0;
        while (true) {
            if (b2 < bj.bp().p.length) {
                if (bj.bp().p[b2].dX && bj.bp().p[b2].i == i) {
                    nVar = bj.bp().p[b2];
                    break;
                } else if (b2 != bj.bp().p.length - 1) {
                    b2 = (byte) (b2 + 1);
                } else {
                    return;
                }
            } else {
                break;
            }
        }
        nVar.a(i4);
        nVar.b(new byte[]{0, 0, b, (byte) i});
        if (!(i2 == 0 && i3 == 0)) {
            nVar.a(i2, i3);
        }
        N();
    }

    private void a(String str) {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(new StringBuffer().append("/s/").append(str).append(".hy").toString()));
            int readInt = dataInputStream.readInt();
            this.gs = new String[readInt];
            for (int i = 0; i < readInt; i++) {
                this.gs[i] = dataInputStream.readUTF();
            }
            dataInputStream.close();
            System.gc();
        } catch (IOException e2) {
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v53, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v54, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v154, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v176, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v234, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v235, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v236, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v237, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v238, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v103, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v249, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v250, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v234, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v235, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v236, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r13 = this;
            r12 = 4
            r11 = 3
            r10 = 2
            r9 = 0
            r8 = 1
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r9]
            switch(r0) {
                case 1: goto L_0x0011;
                case 2: goto L_0x0046;
                case 3: goto L_0x0010;
                case 4: goto L_0x012c;
                case 5: goto L_0x0131;
                case 6: goto L_0x0010;
                case 7: goto L_0x0010;
                case 8: goto L_0x0010;
                case 9: goto L_0x0241;
                case 10: goto L_0x02ad;
                case 11: goto L_0x036e;
                case 12: goto L_0x0010;
                case 13: goto L_0x0380;
                case 14: goto L_0x0010;
                case 15: goto L_0x0395;
                case 16: goto L_0x0010;
                case 17: goto L_0x03b6;
                case 18: goto L_0x03e0;
                case 19: goto L_0x0010;
                case 20: goto L_0x0010;
                case 21: goto L_0x0010;
                case 22: goto L_0x0010;
                case 23: goto L_0x0010;
                case 24: goto L_0x0010;
                case 25: goto L_0x0010;
                case 26: goto L_0x0010;
                case 27: goto L_0x0010;
                case 28: goto L_0x0010;
                case 29: goto L_0x0010;
                case 30: goto L_0x04bc;
                case 31: goto L_0x0414;
                case 32: goto L_0x043d;
                case 33: goto L_0x04af;
                case 34: goto L_0x0010;
                case 35: goto L_0x0010;
                case 36: goto L_0x0578;
                case 37: goto L_0x0010;
                case 38: goto L_0x0010;
                case 39: goto L_0x05c3;
                case 40: goto L_0x05d4;
                case 41: goto L_0x05ff;
                case 42: goto L_0x062e;
                case 43: goto L_0x0010;
                case 44: goto L_0x0010;
                case 45: goto L_0x0677;
                case 46: goto L_0x068d;
                case 47: goto L_0x0010;
                case 48: goto L_0x0010;
                case 49: goto L_0x0708;
                case 50: goto L_0x075e;
                case 51: goto L_0x076d;
                case 52: goto L_0x07f3;
                case 53: goto L_0x0010;
                case 54: goto L_0x0010;
                case 55: goto L_0x0010;
                case 56: goto L_0x0010;
                case 57: goto L_0x0010;
                case 58: goto L_0x0831;
                case 59: goto L_0x0010;
                case 60: goto L_0x0814;
                case 61: goto L_0x0010;
                case 62: goto L_0x0845;
                case 63: goto L_0x0850;
                case 64: goto L_0x0010;
                case 65: goto L_0x08a5;
                case 66: goto L_0x0010;
                case 67: goto L_0x0010;
                case 68: goto L_0x0010;
                case 69: goto L_0x0010;
                case 70: goto L_0x090c;
                case 71: goto L_0x092a;
                case 72: goto L_0x0010;
                case 73: goto L_0x098d;
                case 74: goto L_0x09c4;
                case 75: goto L_0x0010;
                case 76: goto L_0x0a12;
                case 77: goto L_0x0a45;
                case 78: goto L_0x0a5c;
                case 79: goto L_0x0a94;
                case 80: goto L_0x0a99;
                case 81: goto L_0x0abc;
                case 82: goto L_0x0ac1;
                case 83: goto L_0x0ade;
                case 84: goto L_0x0aeb;
                case 85: goto L_0x0b08;
                case 86: goto L_0x0b9f;
                case 87: goto L_0x0b49;
                case 88: goto L_0x0b4e;
                case 89: goto L_0x0ba4;
                case 90: goto L_0x0bb5;
                case 91: goto L_0x0c01;
                case 92: goto L_0x0bc4;
                case 93: goto L_0x0c5a;
                case 94: goto L_0x0bf4;
                case 95: goto L_0x0c75;
                case 96: goto L_0x0cc6;
                case 97: goto L_0x0cde;
                case 98: goto L_0x01b9;
                case 99: goto L_0x0d30;
                default: goto L_0x0010;
            }
        L_0x0010:
            return
        L_0x0011:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            int r0 = defpackage.ee.c(r0, r1)
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r11]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r12]
            int r1 = defpackage.ee.c(r1, r2)
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            r3 = 5
            byte r2 = r2[r3]
            r13.a(r0, r1, r2)
            goto L_0x0010
        L_0x0046:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            java.lang.String[] r1 = r13.gs
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r10]
            r2 = r2 & 255(0xff, float:3.57E-43)
            r1 = r1[r2]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r12]
            r4 = -1
            if (r3 == r4) goto L_0x00b4
            r13.a(r0, r3)
        L_0x0072:
            switch(r0) {
                case -6: goto L_0x00b8;
                case -5: goto L_0x00ba;
                case -4: goto L_0x00bc;
                case -3: goto L_0x00bc;
                case -2: goto L_0x00bc;
                case -1: goto L_0x00bc;
                case 0: goto L_0x0075;
                case 1: goto L_0x0075;
                case 2: goto L_0x0075;
                case 3: goto L_0x00bc;
                case 4: goto L_0x0075;
                case 5: goto L_0x0075;
                case 6: goto L_0x0075;
                case 7: goto L_0x00bc;
                default: goto L_0x0075;
            }
        L_0x0075:
            r3 = r9
        L_0x0076:
            if (r0 <= 0) goto L_0x00be
            ed r4 = defpackage.ed.cB()
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String[] r6 = defpackage.dz.aC
            int r7 = r0 - r8
            r6 = r6[r7]
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r6 = ":"
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.StringBuffer r1 = r5.append(r1)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r4.a(r3, r1)
            r1 = 7
            if (r0 != r1) goto L_0x00ab
            r0 = -1
            if (r2 == r0) goto L_0x00ab
            n r0 = defpackage.ed.o
            r0.a(r2)
        L_0x00ab:
            if (r3 == 0) goto L_0x00b0
            r13.o()
        L_0x00b0:
            r13.M = r8
            goto L_0x0010
        L_0x00b4:
            r13.o()
            goto L_0x0072
        L_0x00b8:
            r3 = r8
            goto L_0x0076
        L_0x00ba:
            r3 = r10
            goto L_0x0076
        L_0x00bc:
            r3 = r0
            goto L_0x0076
        L_0x00be:
            if (r0 >= 0) goto L_0x00fe
            r4 = -4
            if (r0 <= r4) goto L_0x00fe
            ed r4 = defpackage.ed.cB()
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String[] r6 = defpackage.bj.aC
            int r0 = -r0
            int r0 = r0 - r8
            r0 = r6[r0]
            java.lang.StringBuffer r0 = r5.append(r0)
            java.lang.String r5 = ":"
            java.lang.StringBuffer r0 = r0.append(r5)
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            char[] r0 = r0.toCharArray()
            r4.a(r3, r0)
            r0 = -1
            if (r2 == r0) goto L_0x00ab
            a r0 = defpackage.a.M()
            n[] r0 = r0.p
            int r1 = -r3
            int r1 = r1 - r8
            r0 = r0[r1]
            defpackage.ed.n = r0
            r0.a(r2)
            goto L_0x00ab
        L_0x00fe:
            r2 = -5
            if (r0 != r2) goto L_0x0120
            ed r0 = defpackage.ed.cB()
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r4 = "任务提示："
            java.lang.StringBuffer r2 = r2.append(r4)
            java.lang.StringBuffer r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x00ab
        L_0x0120:
            ed r0 = defpackage.ed.cB()
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x00ab
        L_0x012c:
            r13.d()
            goto L_0x0010
        L_0x0131:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            switch(r0) {
                case 0: goto L_0x0151;
                case 1: goto L_0x0185;
                default: goto L_0x014c;
            }
        L_0x014c:
            r13.o()
            goto L_0x0010
        L_0x0151:
            eb r0 = defpackage.eb.cq()
            r0.k(r1, r2)
            ed r0 = defpackage.ed.cB()
            java.lang.String r3 = "获得物品:"
            char[] r3 = r3.toCharArray()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String[] r5 = defpackage.r.aC
            r1 = r5[r1]
            java.lang.StringBuffer r1 = r4.append(r1)
            java.lang.String r4 = "×"
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x014c
        L_0x0185:
            eb r0 = defpackage.eb.cq()
            r0.h(r1, r2)
            ed r0 = defpackage.ed.cB()
            java.lang.String r3 = "获得装备:"
            char[] r3 = r3.toCharArray()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String[] r5 = defpackage.bj.gv
            r1 = r5[r1]
            java.lang.StringBuffer r1 = r4.append(r1)
            java.lang.String r4 = "×"
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x014c
        L_0x01b9:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            switch(r0) {
                case 0: goto L_0x01d9;
                case 1: goto L_0x020d;
                default: goto L_0x01d4;
            }
        L_0x01d4:
            r13.o()
            goto L_0x0010
        L_0x01d9:
            eb r0 = defpackage.eb.cq()
            r0.y(r1, r2)
            ed r0 = defpackage.ed.cB()
            java.lang.String r3 = "失去物品:"
            char[] r3 = r3.toCharArray()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String[] r5 = defpackage.r.aC
            r1 = r5[r1]
            java.lang.StringBuffer r1 = r4.append(r1)
            java.lang.String r4 = "×"
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x01d4
        L_0x020d:
            eb r0 = defpackage.eb.cq()
            r0.j(r1, r2)
            ed r0 = defpackage.ed.cB()
            java.lang.String r3 = "失去装备:"
            char[] r3 = r3.toCharArray()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String[] r5 = defpackage.bj.gv
            r1 = r5[r1]
            java.lang.StringBuffer r1 = r4.append(r1)
            java.lang.String r4 = "×"
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r3, r1)
            goto L_0x01d4
        L_0x0241:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            int[] r3 = r13.aI
            int r0 = r13.w(r0)
            r3[r9] = r0
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r3 = r13.aI
            r3 = r3[r9]
            r0 = r0[r3]
            r0.c(r1)
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r3 = r13.aI
            r3 = r3[r9]
            r0 = r0[r3]
            r0.a(r1)
            if (r2 != 0) goto L_0x029b
            defpackage.ea.cp()
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r9]
            r0 = r0[r1]
            int r0 = r0.aH
            bj r1 = r13.gt
            n[] r1 = r1.p
            int[] r2 = r13.aI
            r2 = r2[r9]
            r1 = r1[r2]
            int r1 = r1.dN
            defpackage.ea.a(r0, r1)
        L_0x029b:
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r9]
            r0 = r0[r1]
            r0.a(r8)
            r13.N()
            goto L_0x0010
        L_0x02ad:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            int r1 = defpackage.ee.c(r1, r2)
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r12]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            r4 = 5
            byte r3 = r3[r4]
            int r2 = defpackage.ee.c(r2, r3)
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            r4 = 6
            byte r3 = r3[r4]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            r5 = 7
            byte r4 = r4[r5]
            byte[][] r5 = r13.dR
            int r6 = r13.ac
            r5 = r5[r6]
            r6 = 8
            byte r5 = r5[r6]
            int[] r6 = r13.aI
            int r0 = r13.w(r0)
            r6[r8] = r0
            bj r6 = r13.gt
            n[] r6 = r6.p
            int[] r7 = r13.aI
            r7 = r7[r8]
            r6 = r6[r7]
            r6.a(r8)
            if (r4 != 0) goto L_0x033b
            bj r4 = r13.gt
            n[] r4 = r4.p
            r4 = r4[r0]
            r4.a(r1, r2)
            r4 = -1
            if (r3 == r4) goto L_0x0326
            bj r4 = r13.gt
            n[] r4 = r4.p
            r4 = r4[r0]
            r4.i(r12, r3)
        L_0x0326:
            bj r3 = r13.gt
            n[] r3 = r3.p
            r0 = r3[r0]
            r0.ac = r9
            if (r5 != 0) goto L_0x0336
            defpackage.ea.cp()
            defpackage.ea.a(r1, r2)
        L_0x0336:
            r13.N()
            goto L_0x0010
        L_0x033b:
            int[] r6 = r13.aI
            r6[r10] = r1
            int[] r1 = r13.aI
            r1[r11] = r2
            int[] r1 = r13.aI
            r1[r12] = r3
            int[] r1 = r13.aI
            r1[r9] = r4
            int[] r1 = r13.aI
            r2 = 5
            r1[r2] = r5
            r1 = -1
            if (r3 == r1) goto L_0x0361
            bj r1 = r13.gt
            n[] r1 = r1.p
            int[] r2 = r13.aI
            r2 = r2[r8]
            r1 = r1[r2]
            r2 = 5
            r1.i(r2, r3)
        L_0x0361:
            bj r1 = r13.gt
            n[] r1 = r1.p
            r0 = r1[r0]
            r0.ac = r9
            r13.o()
            goto L_0x0010
        L_0x036e:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            int r1 = r13.ac
            int r0 = r0 + r1
            r13.ac = r0
            r13.N()
            goto L_0x0010
        L_0x0380:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            r13.a(r0, r1)
            goto L_0x0010
        L_0x0395:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            int[] r1 = r13.aI
            int r0 = r13.w(r0)
            r1[r9] = r0
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r9]
            r0 = r0[r1]
            r0.M = r8
            r13.N()
            goto L_0x0010
        L_0x03b6:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            defpackage.eb.cq()
            defpackage.eb.d(r0)
            defpackage.bg.aU()
            byte[] r1 = defpackage.eb.m
            byte r1 = r1[r0]
            defpackage.bg.c(r1)
            bj r1 = r13.gt
            n[] r1 = r1.p
            int r0 = r13.w(r0)
            r0 = r1[r0]
            r0.a(r8)
            r13.N()
            goto L_0x0010
        L_0x03e0:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            defpackage.eb.cq()
            defpackage.eb.e(r0)
            bj r2 = r13.gt
            n[] r2 = r2.p
            int r0 = r13.w(r0)
            r0 = r2[r0]
            if (r1 != 0) goto L_0x0412
            r1 = r8
        L_0x0403:
            r0.a(r1)
            eb r0 = defpackage.eb.cq()
            r0.N()
            r13.N()
            goto L_0x0010
        L_0x0412:
            r1 = r9
            goto L_0x0403
        L_0x0414:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            int[] r3 = r13.aI
            r3[r8] = r0
            int[] r0 = r13.aI
            r0[r10] = r1
            int[] r0 = r13.aI
            r0[r9] = r2
            r13.o()
            goto L_0x0010
        L_0x043d:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r12]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            r5 = 5
            byte r4 = r4[r5]
            byte[][] r5 = r13.dR
            int r6 = r13.ac
            r5 = r5[r6]
            r6 = 6
            byte r5 = r5[r6]
            int[] r6 = r13.aI
            int r0 = r13.w(r0)
            r6[r8] = r0
            int[] r0 = r13.aI
            r0[r10] = r1
            int[] r0 = r13.aI
            r0[r11] = r2
            int[] r0 = r13.aI
            r0[r12] = r3
            int[] r0 = r13.aI
            r0[r9] = r4
            int[] r0 = r13.aI
            r1 = 5
            r0[r1] = r5
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r8]
            r0 = r0[r1]
            r0.a(r8)
            r0 = -1
            if (r3 == r0) goto L_0x04aa
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r8]
            r0 = r0[r1]
            r1 = 5
            r0.i(r1, r3)
        L_0x04aa:
            r13.o()
            goto L_0x0010
        L_0x04af:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            r13.c(r0)
            goto L_0x0010
        L_0x04bc:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r11]
            int[] r0 = new int[]{r0, r10}
            java.lang.Class r1 = java.lang.Byte.TYPE
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            byte[][] r0 = (byte[][]) r0
            r1 = r9
        L_0x04d1:
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            if (r1 < r2) goto L_0x0554
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r10]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            int r2 = r2 << 1
            int r2 = r2 + 4
            byte r1 = r1[r2]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r11]
            int r3 = r3 << 1
            int r3 = r3 + 5
            byte r2 = r2[r3]
            int r1 = defpackage.ee.c(r1, r2)
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r11]
            int r3 = r3 << 1
            int r3 = r3 + 6
            byte r2 = r2[r3]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            byte r4 = r4[r11]
            int r4 = r4 << 1
            int r4 = r4 + 7
            byte r3 = r3[r4]
            int r2 = defpackage.ee.c(r2, r3)
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            byte r4 = r4[r11]
            int r4 = r4 << 1
            int r4 = r4 + 8
            byte r3 = r3[r4]
            r13.a(r0, r1, r2, r3)
            goto L_0x0010
        L_0x0554:
            r2 = r0[r1]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            int r4 = r1 << 1
            int r4 = r4 + 4
            byte r3 = r3[r4]
            r2[r9] = r3
            r2 = r0[r1]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            int r4 = r1 << 1
            int r4 = r4 + 5
            byte r3 = r3[r4]
            r2[r8] = r3
            int r1 = r1 + 1
            goto L_0x04d1
        L_0x0578:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "Script类中taskId  "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.StringBuffer r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            r2.println(r3)
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "Script类中taskState  "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.StringBuffer r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            r2.println(r3)
            defpackage.dz.e = r0
            byte[] r2 = defpackage.bj.N
            r2[r0] = r1
            r13.N()
            goto L_0x0010
        L_0x05c3:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            int[] r1 = r13.aI
            r1[r9] = r0
            r13.o()
            goto L_0x0010
        L_0x05d4:
            r13.M = r9
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[] r0 = new byte[r0]
            r1 = r9
        L_0x05e1:
            int r2 = r0.length
            if (r1 < r2) goto L_0x05ef
            defpackage.eb.cq()
            defpackage.eb.b(r0)
            r13.N()
            goto L_0x0010
        L_0x05ef:
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            int r3 = r1 + 2
            byte r2 = r2[r3]
            r0[r1] = r2
            int r1 = r1 + 1
            byte r1 = (byte) r1
            goto L_0x05e1
        L_0x05ff:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            int r0 = r13.w(r0)
            bj r1 = r13.gt
            n[] r1 = r1.p
            r1 = r1[r0]
            int r1 = r1.aH
            r2 = 120(0x78, float:1.68E-43)
            int r1 = r1 - r2
            bj r2 = r13.gt
            n[] r2 = r2.p
            r0 = r2[r0]
            int r0 = r0.dN
            r2 = 160(0xa0, float:2.24E-43)
            int r0 = r0 - r2
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r10]
            r13.a(r1, r0, r2)
            goto L_0x0010
        L_0x062e:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r1 = r0[r8]
            byte[][] r0 = r13.dR
            int r2 = r13.ac
            r0 = r0[r2]
            byte r0 = r0[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            int r2 = defpackage.ee.c(r0, r2)
            byte[][] r0 = r13.dR
            int r3 = r13.ac
            r0 = r0[r3]
            byte r0 = r0[r12]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            r4 = 5
            byte r3 = r3[r4]
            int r3 = defpackage.ee.c(r0, r3)
            byte[][] r0 = r13.dR
            int r4 = r13.ac
            r0 = r0[r4]
            r4 = 6
            byte r4 = r0[r4]
            byte[][] r0 = r13.dR
            int r5 = r13.ac
            r0 = r0[r5]
            r5 = 7
            byte r5 = r0[r5]
            r0 = r13
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x0010
        L_0x0677:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[] r1 = defpackage.bj.N
            byte r2 = r1[r0]
            int r2 = r2 + 1
            byte r2 = (byte) r2
            r1[r0] = r2
            r13.N()
            goto L_0x0010
        L_0x068d:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            int r1 = defpackage.ee.c(r1, r2)
            switch(r0) {
                case 0: goto L_0x06b1;
                case 1: goto L_0x06dd;
                default: goto L_0x06ac;
            }
        L_0x06ac:
            r13.o()
            goto L_0x0010
        L_0x06b1:
            defpackage.eb.cq()
            int r0 = -r1
            defpackage.eb.a(r0)
            ed r0 = defpackage.ed.cB()
            java.lang.String r2 = "失去金钱:"
            char[] r2 = r2.toCharArray()
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = ""
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.StringBuffer r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r2, r1)
            goto L_0x06ac
        L_0x06dd:
            defpackage.eb.cq()
            defpackage.eb.a(r1)
            ed r0 = defpackage.ed.cB()
            java.lang.String r2 = "获得金钱:"
            char[] r2 = r2.toCharArray()
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = ""
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.StringBuffer r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            char[] r1 = r1.toCharArray()
            r0.a(r2, r1)
            goto L_0x06ac
        L_0x0708:
            r0 = r9
        L_0x0709:
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x0713
            r13.N()
            goto L_0x0010
        L_0x0713:
            d[] r1 = defpackage.eb.ke
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            r1 = r1[r2]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r8]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r10]
            int r2 = defpackage.ee.c(r2, r3)
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r11]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            byte r4 = r4[r12]
            int r3 = defpackage.ee.c(r3, r4)
            r1.a(r2, r3)
            d[] r1 = defpackage.eb.ke
            byte[] r2 = defpackage.eb.m
            byte r2 = r2[r0]
            r1 = r1[r2]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            r3 = 5
            byte r2 = r2[r3]
            r1.a(r2)
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x0709
        L_0x075e:
            bi r0 = defpackage.bi.bo()
            o r1 = defpackage.o.an()
            r0.gp = r1
            r13.U()
            goto L_0x0010
        L_0x076d:
            int[][] r0 = r13.ka
            if (r0 != 0) goto L_0x0789
            r0 = 20
            r1 = 10
            int[] r0 = new int[]{r0, r1}
            java.lang.Class r1 = java.lang.Integer.TYPE
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            int[][] r0 = (int[][]) r0
            r13.ka = r0
            r0 = 20
            byte[] r0 = new byte[r0]
            r13.m = r0
        L_0x0789:
            int r0 = r13.ac
            int r0 = r0 + 1
        L_0x078d:
            byte[][] r1 = r13.dR
            r1 = r1[r0]
            byte r1 = r1[r9]
            r2 = 52
            if (r1 != r2) goto L_0x07a6
            int[][] r1 = r13.ka
            int r1 = r1.length
            int r1 = r1 - r8
            byte r1 = (byte) r1
        L_0x079c:
            if (r1 >= 0) goto L_0x07e3
        L_0x079e:
            int r0 = r0 - r8
            r13.ac = r0
            r13.N()
            goto L_0x0010
        L_0x07a6:
            r13.ac = r0
            r13.b()
            boolean r1 = r13.ae
            if (r1 == 0) goto L_0x07b5
            r1 = r9
        L_0x07b0:
            int[][] r2 = r13.ka
            int r2 = r2.length
            if (r1 < r2) goto L_0x07b8
        L_0x07b5:
            int r0 = r0 + 1
            goto L_0x078d
        L_0x07b8:
            int[][] r2 = r13.ka
            r2 = r2[r1]
            r2 = r2[r9]
            if (r2 != 0) goto L_0x07df
            byte[] r2 = r13.m
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r9]
            r2[r1] = r3
            r2 = r9
        L_0x07cd:
            r3 = 10
            if (r2 >= r3) goto L_0x07b5
            int[][] r3 = r13.ka
            r3 = r3[r1]
            int[] r4 = r13.aI
            r4 = r4[r2]
            r3[r2] = r4
            int r2 = r2 + 1
            byte r2 = (byte) r2
            goto L_0x07cd
        L_0x07df:
            int r1 = r1 + 1
            byte r1 = (byte) r1
            goto L_0x07b0
        L_0x07e3:
            int[][] r2 = r13.ka
            r2 = r2[r1]
            r2 = r2[r9]
            if (r2 == 0) goto L_0x07f0
            int r1 = r1 + 1
            r13.dN = r1
            goto L_0x079e
        L_0x07f0:
            int r1 = r1 - r8
            byte r1 = (byte) r1
            goto L_0x079c
        L_0x07f3:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            r13.f = r0
            r13.dX = r8
            int r0 = r13.ac
            int r0 = r0 + 1
            r13.ac = r0
            byte r0 = r13.f
            switch(r0) {
                case 0: goto L_0x080c;
                case 1: goto L_0x0810;
                default: goto L_0x080a;
            }
        L_0x080a:
            goto L_0x0010
        L_0x080c:
            r13.dY = r9
            goto L_0x0010
        L_0x0810:
            r13.dY = r8
            goto L_0x0010
        L_0x0814:
            int[] r0 = r13.aI
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r8]
            r0[r9] = r1
            int[] r0 = r13.aI
            int r1 = defpackage.ea.ac
            r0[r8] = r1
            int[] r0 = r13.aI
            int r1 = defpackage.ea.aH
            r0[r10] = r1
            r13.o()
            goto L_0x0010
        L_0x0831:
            ea r0 = defpackage.ea.cp()
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r8]
            r0.a(r1)
            r13.N()
            goto L_0x0010
        L_0x0845:
            ea r0 = defpackage.ea.cp()
            r0.ea = r8
            r13.o()
            goto L_0x0010
        L_0x0850:
            r1 = r9
        L_0x0851:
            bj r0 = r13.gt
            n[] r0 = r0.p
            int r0 = r0.length
            if (r1 < r0) goto L_0x085d
            r13.N()
            goto L_0x0010
        L_0x085d:
            bj r0 = r13.gt
            n[] r0 = r0.p
            r0 = r0[r1]
            boolean r0 = r0.dY
            if (r0 == 0) goto L_0x0883
            byte[][] r0 = r13.dR
            int r2 = r13.ac
            r0 = r0[r2]
            byte r0 = r0[r8]
            if (r0 != 0) goto L_0x0888
            bj r0 = r13.gt
            n[] r0 = r0.p
            r0 = r0[r1]
            dw r0 = (defpackage.dw) r0
            r0.aT = r8
            bj r0 = r13.gt
            n[] r0 = r0.p
            r0 = r0[r1]
            r0.M = r8
        L_0x0883:
            int r0 = r1 + 1
            short r0 = (short) r0
            r1 = r0
            goto L_0x0851
        L_0x0888:
            bj r0 = r13.gt
            n[] r0 = r0.p
            r0 = r0[r1]
            dw r0 = (defpackage.dw) r0
            r0.aT = r9
            bj r0 = r13.gt
            n[] r0 = r0.p
            r0 = r0[r1]
            r0.M = r9
            bj r0 = r13.gt
            n[] r0 = r0.p
            r0 = r0[r1]
            dw r0 = (defpackage.dw) r0
            r0.as = r9
            goto L_0x0883
        L_0x08a5:
            byte r0 = defpackage.dz.e
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            if (r0 <= r1) goto L_0x0d47
            r0 = r8
        L_0x08b2:
            byte[] r1 = defpackage.bj.N
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r10]
            byte r1 = r1[r2]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            if (r1 >= r2) goto L_0x08e4
            byte[] r1 = defpackage.bj.N
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r10]
            byte r1 = r1[r2]
            if (r1 < 0) goto L_0x08e4
        L_0x08d6:
            if (r0 == 0) goto L_0x08df
            int r0 = r13.ac
        L_0x08da:
            byte[][] r1 = r13.dR
            int r1 = r1.length
            if (r0 < r1) goto L_0x08ec
        L_0x08df:
            r13.N()
            goto L_0x0010
        L_0x08e4:
            byte[] r0 = defpackage.bj.N
            byte[][] r0 = r13.dR
            int r0 = r13.ac
            r0 = r8
            goto L_0x08d6
        L_0x08ec:
            byte[][] r1 = r13.dR
            r1 = r1[r0]
            byte r1 = r1[r9]
            r2 = 66
            if (r1 != r2) goto L_0x0909
            byte[][] r1 = r13.dR
            r1 = r1[r0]
            byte r1 = r1[r8]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r8]
            if (r1 != r2) goto L_0x0909
            r13.ac = r0
            goto L_0x08df
        L_0x0909:
            int r0 = r0 + 1
            goto L_0x08da
        L_0x090c:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            switch(r0) {
                case 0: goto L_0x091c;
                case 1: goto L_0x0923;
                default: goto L_0x0917;
            }
        L_0x0917:
            r13.N()
            goto L_0x0010
        L_0x091c:
            ea r0 = defpackage.ea.cp()
            r0.em = r8
            goto L_0x0917
        L_0x0923:
            ea r0 = defpackage.ea.cp()
            r0.em = r9
            goto L_0x0917
        L_0x092a:
            ed r0 = defpackage.ed.cB()
            r0.f = r8
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r11]
            defpackage.ed.g = r0
            r13.M = r8
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            java.lang.String[] r1 = new java.lang.String[r1]
            r2 = r9
        L_0x094f:
            int r3 = r1.length
            if (r2 < r3) goto L_0x0962
            r2 = -5
            if (r0 != r2) goto L_0x0988
            r2 = r10
        L_0x0956:
            ed r3 = defpackage.ed.cB()
            r3.a(r2, r0, r1)
            r13.o()
            goto L_0x0010
        L_0x0962:
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            int r4 = r2 << 1
            int r4 = r4 + 4
            byte r3 = r3[r4]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            int r5 = r2 << 1
            int r5 = r5 + 5
            byte r4 = r4[r5]
            int r3 = defpackage.ee.c(r3, r4)
            java.lang.String[] r4 = r13.gs
            r3 = r4[r3]
            r1[r2] = r3
            int r2 = r2 + 1
            short r2 = (short) r2
            goto L_0x094f
        L_0x0988:
            r13.a(r0, r9)
            r2 = r8
            goto L_0x0956
        L_0x098d:
            a r0 = defpackage.a.M()
            r0.S()
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            defpackage.a.ak = r9
            defpackage.a.aj = r9
            a r1 = defpackage.a.M()
            int r2 = defpackage.bj.ac
            r1.g(r2)
            a r1 = defpackage.a.M()
            r1.f(r0)
            a r0 = defpackage.a.M()
            r0.Y()
            ea r0 = defpackage.ea.cp()
            r1 = 5
            r0.a(r1)
            r13.o()
            goto L_0x0010
        L_0x09c4:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            switch(r0) {
                case 0: goto L_0x09ec;
                case 1: goto L_0x09f4;
                case 2: goto L_0x0a03;
                default: goto L_0x09d7;
            }
        L_0x09d7:
            q r0 = defpackage.q.ao()
            r0.c()
            ea r0 = defpackage.ea.cp()
            r1 = 8
            r0.a(r1)
            r13.o()
            goto L_0x0010
        L_0x09ec:
            q r1 = defpackage.q.ao()
            r1.a(r0)
            goto L_0x09d7
        L_0x09f4:
            q r2 = defpackage.q.ao()
            r2.a(r0)
            q r0 = defpackage.q.ao()
            r0.b(r1)
            goto L_0x09d7
        L_0x0a03:
            q r2 = defpackage.q.ao()
            r2.a(r0)
            q r0 = defpackage.q.ao()
            r0.c(r1)
            goto L_0x09d7
        L_0x0a12:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r12]
            bg r4 = defpackage.bg.aU()
            r4.a(r0, r1, r2, r3)
            ea r0 = defpackage.ea.cp()
            r0.a(r11)
            r13.U()
            goto L_0x0010
        L_0x0a45:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            r1 = -1
            if (r0 == r1) goto L_0x0a58
            defpackage.bh.a(r0)
        L_0x0a53:
            r13.N()
            goto L_0x0010
        L_0x0a58:
            defpackage.bh.c()
            goto L_0x0a53
        L_0x0a5c:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            int[] r2 = r13.aI
            int r0 = r13.x(r0)
            r2[r9] = r0
            if (r1 != 0) goto L_0x0a87
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r9]
            r0 = r0[r1]
            r0.M = r8
        L_0x0a82:
            r13.N()
            goto L_0x0010
        L_0x0a87:
            bj r0 = r13.gt
            n[] r0 = r0.p
            int[] r1 = r13.aI
            r1 = r1[r9]
            r0 = r0[r1]
            r0.M = r9
            goto L_0x0a82
        L_0x0a94:
            r13.o()
            goto L_0x0010
        L_0x0a99:
            defpackage.b.Z()
            defpackage.b.N()
            b r0 = defpackage.b.Z()
            r0.c(r8)
            b r0 = defpackage.b.Z()
            byte[] r1 = defpackage.b.N
            r0.b(r1, r9)
            ea r0 = defpackage.ea.cp()
            r1 = 7
            r0.a(r1)
            r13.o()
            goto L_0x0010
        L_0x0abc:
            r13.co()
            goto L_0x0010
        L_0x0ac1:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            r13.a(r0, r1, r2)
            goto L_0x0010
        L_0x0ade:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            r13.f(r0)
            goto L_0x0010
        L_0x0aeb:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            if (r0 != 0) goto L_0x0b00
            defpackage.eb.cq()
            defpackage.eb.Q()
        L_0x0afb:
            r13.N()
            goto L_0x0010
        L_0x0b00:
            eb r0 = defpackage.eb.cq()
            r0.R()
            goto L_0x0afb
        L_0x0b08:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r11]
            byte[][] r3 = r13.dR
            int r4 = r13.ac
            r3 = r3[r4]
            byte r3 = r3[r12]
            int[] r4 = r13.aI
            r4[r9] = r0
            int[] r4 = r13.aI
            r4[r8] = r1
            int[] r1 = r13.aI
            r1[r10] = r2
            int[] r1 = r13.aI
            r1[r11] = r3
            int[] r1 = r13.aI
            r1 = r1[r9]
            r2 = -1
            if (r1 == r2) goto L_0x0b44
            c r1 = defpackage.bj.gr
            r1.c(r0)
        L_0x0b44:
            r13.o()
            goto L_0x0010
        L_0x0b49:
            r13.T()
            goto L_0x0010
        L_0x0b4e:
            ea r0 = defpackage.ea.cp()
            r0.aT = r8
            defpackage.ea.cp()
            java.lang.String[] r0 = r13.gs
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            r2 = 5
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0[r1]
            defpackage.ea.aF = r0
            defpackage.ea.cp()
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            int r0 = defpackage.ee.c(r0, r1)
            defpackage.ea.dO = r0
            defpackage.ea.cp()
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r11]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r12]
            int r0 = defpackage.ee.c(r0, r1)
            defpackage.ea.eb = r0
            r13.N()
            goto L_0x0010
        L_0x0b9f:
            r13.V()
            goto L_0x0010
        L_0x0ba4:
            ea r0 = defpackage.ea.cp()
            r0.aT = r9
            ea r0 = defpackage.ea.cp()
            r0.k = r9
            r13.N()
            goto L_0x0010
        L_0x0bb5:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            defpackage.bg.as = r0
            r13.N()
            goto L_0x0010
        L_0x0bc4:
            ea r0 = defpackage.ea.cp()
            r0.fS = r8
            ea r0 = defpackage.ea.cp()
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "/u/insert"
            java.lang.StringBuffer r1 = r1.append(r2)
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r8]
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            ao r1 = defpackage.ee.O(r1)
            r0.q = r1
            r13.o()
            goto L_0x0010
        L_0x0bf4:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            r13.g(r0)
            goto L_0x0010
        L_0x0c01:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            switch(r0) {
                case 1: goto L_0x0c0e;
                case 2: goto L_0x0c33;
                default: goto L_0x0c0c;
            }
        L_0x0c0c:
            goto L_0x0010
        L_0x0c0e:
            byte[] r0 = defpackage.b.m
            byte r0 = r0[r9]
            if (r0 >= 0) goto L_0x0c19
            r13.N()
            goto L_0x0010
        L_0x0c19:
            b r0 = defpackage.b.Z()
            b r1 = defpackage.b.Z()
            byte[] r1 = r1.aw
            r0.b(r1, r10)
            ea r0 = defpackage.ea.cp()
            r1 = 7
            r0.a(r1)
            r13.o()
            goto L_0x0010
        L_0x0c33:
            byte[] r0 = defpackage.b.m
            r1 = 16
            byte r0 = r0[r1]
            if (r0 >= 0) goto L_0x0c40
            r13.N()
            goto L_0x0010
        L_0x0c40:
            b r0 = defpackage.b.Z()
            b r1 = defpackage.b.Z()
            byte[] r1 = r1.ax
            r0.b(r1, r10)
            ea r0 = defpackage.ea.cp()
            r1 = 7
            r0.a(r1)
            r13.o()
            goto L_0x0010
        L_0x0c5a:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            switch(r0) {
                case 0: goto L_0x0c6a;
                default: goto L_0x0c65;
            }
        L_0x0c65:
            r13.o()
            goto L_0x0010
        L_0x0c6a:
            bi r0 = defpackage.bi.bo()
            o r1 = defpackage.o.an()
            r0.gp = r1
            goto L_0x0c65
        L_0x0c75:
            defpackage.bh.c()
            defpackage.bh.a(r9)
            ea r0 = defpackage.ea.cp()
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r8]
            r0.h = r1
            ea r0 = defpackage.ea.cp()
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "/u/insert"
            java.lang.StringBuffer r1 = r1.append(r2)
            byte[][] r2 = r13.dR
            int r3 = r13.ac
            r2 = r2[r3]
            byte r2 = r2[r8]
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            ao r1 = defpackage.ee.O(r1)
            r0.q = r1
            ea r0 = defpackage.ea.cp()
            r0.a(r8)
            ea r0 = defpackage.ea.cp()
            r0.dY = r9
            ea r0 = defpackage.ea.cp()
            r0.fT = r8
            r13.o()
            goto L_0x0010
        L_0x0cc6:
            byte[][] r0 = r13.dR
            int r1 = r13.ac
            r0 = r0[r1]
            byte r0 = r0[r8]
            byte[][] r1 = r13.dR
            int r2 = r13.ac
            r1 = r1[r2]
            byte r1 = r1[r10]
            r13.b(r0, r1)
            r13.N()
            goto L_0x0010
        L_0x0cde:
            r0 = r9
        L_0x0cdf:
            byte[] r1 = defpackage.eb.m
            int r1 = r1.length
            if (r0 < r1) goto L_0x0ce9
            r13.N()
            goto L_0x0010
        L_0x0ce9:
            byte[] r1 = defpackage.eb.m
            byte r1 = r1[r0]
            short[] r2 = defpackage.eb.O
            short r2 = r2[r1]
            r3 = 50
            if (r2 >= r3) goto L_0x0d2d
            short[] r2 = defpackage.eb.O
            short r3 = r2[r1]
            byte[][] r4 = r13.dR
            int r5 = r13.ac
            r4 = r4[r5]
            byte r4 = r4[r8]
            int r3 = r3 + r4
            short r3 = (short) r3
            r2[r1] = r3
            short[] r2 = defpackage.eb.O
            short r2 = r2[r1]
            r3 = 50
            if (r2 < r3) goto L_0x0d13
            short[] r2 = defpackage.eb.O
            r3 = 50
            r2[r1] = r3
        L_0x0d13:
            defpackage.eb.cq()
            defpackage.eb.c(r1)
            short[] r2 = defpackage.eb.ab
            short[][] r3 = defpackage.eb.ev
            r3 = r3[r1]
            short r3 = r3[r12]
            r2[r1] = r3
            short[] r2 = defpackage.eb.aq
            short[][] r3 = defpackage.eb.ev
            r3 = r3[r1]
            short r3 = r3[r11]
            r2[r1] = r3
        L_0x0d2d:
            int r0 = r0 + 1
            goto L_0x0cdf
        L_0x0d30:
            b r0 = defpackage.b.Z()
            b r1 = defpackage.b.Z()
            byte[] r1 = r1.aB
            r0.b(r1, r10)
            ea r0 = defpackage.ea.cp()
            r1 = 7
            r0.a(r1)
            goto L_0x0010
        L_0x0d47:
            r0 = r9
            goto L_0x08b2
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.dz.b():void");
    }

    private void b(byte b, byte b2) {
        byte b3 = eb.m[b];
        for (int i = 0; i < dQ[b3].length; i++) {
            if (dQ[b3][i][0] == 0) {
                dQ[b3][i][0] = 1;
                dQ[b3][i][1] = b2;
                return;
            }
            this.aH = i;
        }
        if (this.aH == dQ[b3].length - 1) {
            byte[][] bArr = (byte[][]) Array.newInstance(Byte.TYPE, dQ[b3].length + 1, 2);
            for (int i2 = 0; i2 < bArr.length - 1; i2++) {
                for (byte b4 = 0; b4 < 2; b4 = (byte) (b4 + 1)) {
                    bArr[i2][b4] = dQ[b3][i2][b4];
                }
            }
            bArr[dQ[b3].length] = new byte[]{1, b2};
            dQ[b3] = bArr;
        }
    }

    private void c(int i) {
        bi.bo().a(i);
        o();
    }

    public static dz cn() {
        if (kb == null) {
            kb = new dz();
        }
        return kb;
    }

    private void co() {
        for (int i = 0; i < eb.m.length; i++) {
            byte b = eb.m[i];
            eb.cq();
            eb.c(b);
            eb.ab[eb.m[i]] = eb.ev[b][4];
            eb.aq[eb.m[i]] = eb.ev[b][3];
        }
        N();
    }

    private void d() {
        if (!eb.M && !this.M) {
            eb.cq().N();
        }
        ea.cp().a(1);
        U();
    }

    private void e(String str) {
        this.ac = -1;
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(new StringBuffer().append("/s/").append(str).append(".hy").toString()));
            this.dR = new byte[dataInputStream.readInt()][];
            for (int i = 0; i < this.dR.length; i++) {
                int readUnsignedByte = dataInputStream.readUnsignedByte();
                this.dR[i] = new byte[readUnsignedByte];
                for (int i2 = 0; i2 < readUnsignedByte; i2++) {
                    this.dR[i][i2] = dataInputStream.readByte();
                }
            }
            dataInputStream.close();
            System.gc();
        } catch (IOException e2) {
        }
    }

    private void f(byte b) {
        eb.cq();
        byte b2 = eb.y(b) ? (byte) 1 : 0;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 83 && this.dR[i][1] == b && this.dR[i][2] == b2) {
                eb.cq().y(b, 1);
                this.ac = i;
                N();
                return;
            }
        }
        d();
    }

    private void g(byte b) {
        byte b2 = (b == 1 ? b.m[0] : b.m[16]) < 0 ? (byte) 1 : 0;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 94 && this.dR[i][1] == b && this.dR[i][2] == b2) {
                this.ac = i;
                N();
                return;
            }
        }
        d();
    }

    private void o() {
        if (!this.dZ) {
            this.ae = true;
            this.dY = false;
        }
    }

    public static boolean q(int i) {
        for (byte[] bArr : dQ[i]) {
            if (bArr[0] == 1) {
                return true;
            }
        }
        return false;
    }

    private int w(int i) {
        for (int i2 = 0; i2 < this.gt.p.length; i2++) {
            if (this.gt.p[i2].i == i) {
                return i2;
            }
        }
        return 0;
    }

    private int x(int i) {
        for (int i2 = 0; i2 < this.gt.p.length; i2++) {
            if (this.gt.p[i2].k == 2 && this.gt.p[i2].i == i) {
                return i2;
            }
        }
        return 0;
    }

    public static byte[] z(int i) {
        byte[] bArr = new byte[dQ[i].length];
        for (int i2 = 0; i2 < dQ[i].length; i2++) {
            bArr[i2] = dQ[i][i2][1];
        }
        return bArr;
    }

    public final void N() {
        if (!this.dZ) {
            ea.cp().a(6);
            this.ae = false;
            this.dY = true;
            this.ac++;
        }
    }

    public final void P() {
        while (this.dY) {
            b();
        }
        if (this.dX) {
            this.dZ = true;
            boolean z = this.ae;
            byte b = this.dR[this.ac][0];
            int[] iArr = this.aI;
            boolean z2 = true;
            for (int i = 0; i < this.dN; i++) {
                if (this.ka[i][0] > 0) {
                    this.dR[this.ac][0] = this.m[i];
                    this.aI = this.ka[i];
                    this.ae = true;
                    J();
                    z2 = false;
                }
            }
            this.dR[this.ac][0] = b;
            this.aI = iArr;
            this.ae = false;
            this.dZ = false;
            if (z2) {
                this.ka = null;
                this.m = null;
                this.dX = false;
                switch (this.f) {
                    case 0:
                        this.dY = true;
                        break;
                }
            }
            if (this.f == 1) {
                this.ae = z;
            }
        }
        J();
    }

    public final void R() {
        boolean z;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 34) {
                int i2 = 0;
                while (true) {
                    if (i2 >= this.dR[i][1]) {
                        z = true;
                        break;
                    } else if (bj.N[this.dR[i][(i2 << 1) + 2]] != this.dR[i][(i2 << 1) + 3]) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    this.ac = i;
                    this.ae = false;
                    this.dY = true;
                    this.ac++;
                    return;
                }
            }
        }
        d();
    }

    public final void S() {
        byte b;
        byte b2 = q.ao().e;
        q ao = q.ao();
        switch (ao.e) {
            case 0:
                b = 0;
                break;
            case 1:
                b = ao.X;
                break;
            case 2:
                b = ao.af;
                break;
            default:
                b = 0;
                break;
        }
        byte b3 = (byte) b;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 75 && this.dR[i][1] == b2 && this.dR[i][2] == b3) {
                this.ac = i;
                N();
                return;
            }
        }
        d();
    }

    public final void a(byte b) {
        boolean z;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 35 && this.dR[i][1] == b) {
                int i2 = 0;
                while (true) {
                    if (i2 >= this.dR[i][2]) {
                        z = true;
                        break;
                    } else if (bj.N[this.dR[i][(i2 << 1) + 3]] != this.dR[i][(i2 << 1) + 4]) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    if (eb.M) {
                        eb.cq().R();
                    }
                    this.ac = i;
                    N();
                    return;
                }
            }
        }
    }

    public final void a(int i) {
        a(new StringBuffer().append("d").append(i).toString());
        e(new StringBuffer().append("e").append(i).toString());
    }

    public final void b(byte b) {
        boolean z;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 37 && this.dR[i][1] == b) {
                int i2 = 0;
                while (true) {
                    if (i2 >= this.dR[i][2]) {
                        z = true;
                        break;
                    } else if (bj.N[this.dR[i][(i2 << 1) + 3]] != this.dR[i][(i2 << 1) + 4]) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    this.ac = i;
                    N();
                    return;
                }
            }
        }
        d();
        this.M = false;
    }

    public final void c() {
        this.gs = null;
        this.dR = null;
    }

    public final void c(byte b) {
        this.g = b;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 30 && this.dR[i][1] == b) {
                this.ac = i - 1;
                N();
            }
        }
    }

    public final void d(byte b) {
        boolean z;
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 61 && this.dR[i][1] == b) {
                int i2 = 0;
                while (true) {
                    if (i2 >= this.dR[i][2]) {
                        z = true;
                        break;
                    } else if (bj.N[this.dR[i][(i2 << 1) + 3]] != this.dR[i][(i2 << 1) + 4]) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    this.ac = i;
                    N();
                    return;
                }
            }
        }
    }

    public final void e(byte b) {
        for (int i = 0; i < this.dR.length; i++) {
            if (this.dR[i][0] == 72 && this.dR[i][1] == ed.g && this.dR[i][2] == b) {
                this.ac = i;
                N();
                return;
            }
        }
        d();
        this.M = false;
    }
}
