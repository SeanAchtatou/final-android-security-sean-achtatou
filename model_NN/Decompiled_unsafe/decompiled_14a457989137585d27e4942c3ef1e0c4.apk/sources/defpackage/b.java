package defpackage;

import com.network.app.data.Database;
import javax.microedition.media.Player;
import javax.microedition.media.control.MIDIControl;

/* renamed from: b  reason: default package */
public final class b {
    private static boolean M = true;
    public static byte[] N;
    private static byte W;
    private static byte X;
    private static byte Y;
    private static byte Z;
    public static final byte[] aD = {4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    public static String aF = "";
    public static String aG = "";
    private static int aH = 2;
    public static final int[] aI = {4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    private static b aJ;
    private static String aK;
    private static String aL;
    private static String aM;
    public static String aN;
    private static byte[] aO;
    private static char[] aP = null;
    private static char[][] aQ = new char[40][];
    public static int ac = 16777215;
    public static byte g;
    private static short k;
    public static byte[] m = new byte[18];
    public byte[] L = {1};
    private byte R;
    public byte[] aA;
    public byte[] aB;
    private String[] aC;
    private String[] aE;
    private boolean ae;
    private byte af;
    public byte[] aw = {0};
    public byte[] ax = {16};
    public byte[] ay = {13};
    public byte[] az = {14};
    public byte e;
    public byte f;
    public byte h;
    public byte i;
    public byte j;

    private b() {
        new byte[1][0] = 15;
        this.aA = new byte[]{7};
        this.aB = new byte[]{17};
        this.aC = new String[]{"正版开通", "5000银两", "等级提升", "神功秘诀", "双倍经验+2000金", "三倍经验+3000金", "四倍经验+4000金", "御剑飞行", "凤火神丹", "凤尾草和灵芝丹", "极天神器", "开通15-30级", "开通31-45级", "全体复活", "弑神丸", "及时存档", "正版开通", "机关收费"};
        this.aE = new String[]{"该服务已开启", "无需重复购买"};
        this.ae = true;
    }

    public static void N() {
        N = new byte[]{8, 9, 10};
    }

    public static void P() {
        aK = "10086";
        aL = "ABCD";
        l.a(5);
    }

    private void Q() {
        if (this.i == 0) {
            e(0);
            return;
        }
        switch (aO[g]) {
            case 0:
            case 16:
                bi.bo().gp = o.an();
                bh.a(0);
                break;
            case 7:
                ea.cp().a(1);
                bh.c(bh.f);
                e(0);
                break;
            case 13:
                bg.aU().P();
                break;
            case Database.INT_ISFEE /*14*/:
                bg.aU().a((byte) 2);
                ea.cp().a(3);
                bh.c(bh.f);
                break;
            case Database.INT_FEECUE /*15*/:
                ea.cp().a(2);
                a.M().e = 1;
                bh.c(bh.f);
                e(0);
                break;
            case Database.INT_FEES /*17*/:
                switch (q.ao().Y) {
                    case 0:
                        q.ao().f = (byte) 3;
                        break;
                    case 1:
                        q.ao().f = (byte) 0;
                        break;
                }
                bh.c(bh.f);
                ea.cp().a(8);
                break;
        }
        bi.ae = false;
    }

    public static b Z() {
        if (aJ == null) {
            aJ = new b();
        }
        return aJ;
    }

    private static void a(char[] cArr) {
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < cArr.length; i4++) {
            i2 += p.eo.a(cArr[i4]);
            if (i2 > k) {
                aQ[W] = new char[(i4 - i3)];
                for (int i5 = 0; i5 < aQ[W].length; i5++) {
                    aQ[W][i5] = cArr[i3 + i5];
                }
                i2 = p.eo.a(cArr[i4]);
                W = (byte) (W + 1);
                i3 = i4;
            } else if (i4 == cArr.length - 1) {
                aQ[W] = new char[((i4 - i3) + 1)];
                for (int i6 = 0; i6 < aQ[W].length; i6++) {
                    aQ[W][i6] = cArr[i3 + i6];
                }
                W = (byte) (W + 1);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        r0 = false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean b(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ all -> 0x0036 }
            r0.<init>()     // Catch:{ all -> 0x0036 }
            java.lang.String r1 = "sms://"
            java.lang.StringBuffer r0 = r0.append(r1)     // Catch:{ all -> 0x0036 }
            java.lang.StringBuffer r0 = r0.append(r4)     // Catch:{ all -> 0x0036 }
            java.lang.String r2 = r0.toString()     // Catch:{ all -> 0x0036 }
            s r0 = defpackage.u.n(r2)     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            bc r0 = (defpackage.bc) r0     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            java.lang.String r1 = "text"
            bb r1 = r0.s(r1)     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            bf r1 = (defpackage.bf) r1     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            r1.setAddress(r2)     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            r1.t(r5)     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            r0.a(r1)     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            r0.close()     // Catch:{ Exception -> 0x0031, all -> 0x0034 }
            r0 = 1
        L_0x002f:
            monitor-exit(r3)
            return r0
        L_0x0031:
            r0 = move-exception
            r0 = 0
            goto L_0x002f
        L_0x0034:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b.b(java.lang.String, java.lang.String):boolean");
    }

    public static void c() {
        if (m[4] >= 0 && m[5] >= 0) {
            d(4);
        } else if (m[4] >= 0 || m[5] < 0) {
            if (m[5] < 0) {
                byte[] bArr = m;
            }
            d(6);
        } else {
            d(5);
        }
    }

    private static void d(int i2) {
        N = new byte[]{1, 2, 3, (byte) i2, 7};
    }

    private static void d(an anVar) {
        anVar.setColor(11765343);
        anVar.c(0, -2, 240, 322);
        a.M().b(anVar, 11765343, 0, 0, 240, 320);
    }

    private void e(int i2) {
        String stringBuffer;
        switch (i2) {
            case 0:
                ea.cp().dZ = true;
                bi.ae = false;
                break;
            case 1:
            case 2:
                byte b = aO[g];
                Z = (byte) b;
                W = 0;
                X = (byte) (260 / p.e);
                k = 190;
                Y = 0;
                switch (b) {
                    case 0:
                        stringBuffer = new StringBuffer().append(" ").append("开通正版游戏").toString();
                        break;
                    case 1:
                        stringBuffer = new StringBuffer().append(" ").append("获得5000金钱").toString();
                        break;
                    case 2:
                        stringBuffer = new StringBuffer().append(" ").append("当前队伍中全部角色提升5个等级").toString();
                        break;
                    case 3:
                        stringBuffer = new StringBuffer().append(" ").append("“神功秘诀”当前队列中所有角色现有技能提升一级").toString();
                        break;
                    case 4:
                        stringBuffer = new StringBuffer().append(" ").append("开通双倍经验,并附送2000金钱").toString();
                        break;
                    case 5:
                        stringBuffer = new StringBuffer().append(" ").append("开通三倍经验,并附送3000金钱").toString();
                        break;
                    case 6:
                        stringBuffer = new StringBuffer().append(" ").append("开通四倍经验,并附送4000金钱").toString();
                        break;
                    case 7:
                        stringBuffer = new StringBuffer().append(" ").append("角色行动中不遇敌,并加快行走，按9键使用").toString();
                        break;
                    case 8:
                        stringBuffer = new StringBuffer().append(" ").append("20颗凤火神丹,单体复活并恢复100%生命").toString();
                        break;
                    case 9:
                        stringBuffer = new StringBuffer().append(" ").append("凤尾草和灵芝丹各8个,解除全体异常状态,恢复全体生命和真气").toString();
                        break;
                    case 10:
                        stringBuffer = new StringBuffer().append(" ").append("极天神器,提高人物威力,让你所向睥睨").toString();
                        break;
                    case 11:
                        stringBuffer = new StringBuffer().append(" ").append("15-30级角色升级的资格").toString();
                        break;
                    case 12:
                        stringBuffer = new StringBuffer().append(" ").append("30级以上角色升级的资格").toString();
                        break;
                    case 13:
                        stringBuffer = new StringBuffer().append(" ").append("复活当前所有队员,当前所有人物提升一级,并给予敌三倍伤害,赠送1颗灵芝丹").toString();
                        break;
                    case Database.INT_ISFEE /*14*/:
                        stringBuffer = new StringBuffer().append(" ").append("本次战斗中秒杀所有敌人").toString();
                        break;
                    case Database.INT_FEECUE /*15*/:
                        stringBuffer = new StringBuffer().append(" ").append("即时存档,随时随地保存游戏").toString();
                        break;
                    case 16:
                        stringBuffer = new StringBuffer().append(" ").append("开通后期游戏,本次开通为最后一次").toString();
                        break;
                    case Database.INT_FEES /*17*/:
                        stringBuffer = new StringBuffer().append(" ").append("可以直接通过此机关").toString();
                        break;
                    default:
                        stringBuffer = " ";
                        break;
                }
                if (m[aO[g]] == 0) {
                    stringBuffer = new StringBuffer().append(stringBuffer).append("资费共").append((int) aD[aO[g]]).append("元，需发送").append(aD[aO[g]] / aH).append("条扣费短信").append(aH).append("元/条(不含通信费)。").toString();
                } else if (m[aO[g]] > 0) {
                    stringBuffer = new StringBuffer().append(stringBuffer).append("你已成功发送").append((int) m[aO[g]]).append("条，还需发送").append((aD[aO[g]] / aH) - m[aO[g]]).append("条。").toString();
                }
                a(stringBuffer.toCharArray());
                if (aP != null) {
                    a(aP);
                }
                bh.c();
                bi.ae = true;
                new StringBuffer().append(aN).append("。").append(aM).toString().toCharArray();
                break;
        }
        this.f = (byte) i2;
    }

    private static void f(an anVar) {
        d(anVar);
        du.c(anVar, 0, 0, 0, 240, 320);
        anVar.setColor(16439945);
        int i2 = 0;
        while (i2 < X && i2 < W) {
            anVar.a(aQ[Y + i2], 0, aQ[Y + i2].length, 120, (p.e * i2) + 30, 17);
            i2++;
        }
        if (W > X) {
            anVar.b(224, 30, 10, (p.e * X) - 1);
            anVar.c(224, (((p.e * Y) * X) / W) + 30, 10, ((p.e * X) * X) / W);
        }
        du.c(anVar, 4, 4);
    }

    public final boolean W() {
        int i2;
        int i3;
        boolean z;
        this.ae = true;
        switch (this.f) {
            case 0:
                if (!bi.y(8)) {
                    if (!bi.y(4)) {
                        if (!bi.y(MIDIControl.NOTE_ON)) {
                            if (bi.y(as.FIRE_PRESSED)) {
                                ea.cp().a(1);
                                break;
                            }
                        } else {
                            switch (aO[g]) {
                                case 1:
                                    e(1);
                                    break;
                                case 2:
                                    int i4 = 0;
                                    while (true) {
                                        if (i4 >= eb.m.length) {
                                            z = false;
                                        } else if (eb.O[eb.m[i4]] < 50) {
                                            z = true;
                                        } else {
                                            i4++;
                                        }
                                    }
                                    if (!z) {
                                        e(2);
                                        break;
                                    } else {
                                        e(1);
                                        break;
                                    }
                                case 3:
                                    a.M();
                                    if (!a.W()) {
                                        this.af = 1;
                                        e(3);
                                        break;
                                    } else {
                                        e(1);
                                        break;
                                    }
                                case 4:
                                    e(1);
                                    break;
                                case 5:
                                    e(1);
                                    break;
                                case 6:
                                    if (m[6] < 0) {
                                        e(2);
                                        break;
                                    } else {
                                        e(1);
                                        break;
                                    }
                                case 7:
                                    if (m[7] < 0) {
                                        e(2);
                                        break;
                                    } else {
                                        e(1);
                                        break;
                                    }
                                case 8:
                                    e(1);
                                    break;
                                case 9:
                                    e(1);
                                    break;
                                case 10:
                                    e(1);
                                    this.j = 0;
                                    break;
                            }
                        }
                    } else {
                        byte b = (byte) (g + 1);
                        g = b;
                        g = b >= this.h ? 0 : g;
                        break;
                    }
                } else {
                    byte b2 = (byte) (g - 1);
                    g = b2;
                    g = b2 < 0 ? (byte) (this.h - 1) : g;
                    break;
                }
                break;
            case 1:
                if (aO[g] != 10) {
                    if (!bi.y(128)) {
                        if (!bi.y(8)) {
                            if (!bi.y(4)) {
                                if (bi.y(as.FIRE_PRESSED)) {
                                    if (this.i != 0) {
                                        switch (aO[g]) {
                                            case 0:
                                            case 16:
                                                bi.bo().gp = o.an();
                                                bh.c();
                                                bh.a(0);
                                                break;
                                            case 1:
                                                switch (this.R) {
                                                    case 1:
                                                        ea.cp().a(5);
                                                        break;
                                                    case 2:
                                                        bg.aU().a((byte) 2);
                                                        ea.cp().a(3);
                                                        bg.aU().a(7);
                                                        break;
                                                }
                                                bh.c(bh.f);
                                                break;
                                            case 7:
                                                ea.cp().a(1);
                                                e(0);
                                                bh.c(bh.f);
                                                break;
                                            case 13:
                                                bg.aU().P();
                                                break;
                                            case Database.INT_ISFEE /*14*/:
                                                bg.aU().a((byte) 2);
                                                ea.cp().a(3);
                                                bh.c(bh.f);
                                                break;
                                            case Database.INT_FEECUE /*15*/:
                                                ea.cp().a(2);
                                                a.M().e = 1;
                                                bh.c(bh.f);
                                                e(0);
                                                break;
                                            case Database.INT_FEES /*17*/:
                                                switch (q.ao().Y) {
                                                    case 0:
                                                        q.ao().f = (byte) 3;
                                                        break;
                                                    case 1:
                                                        q.ao().f = (byte) 0;
                                                        break;
                                                }
                                                bh.c(bh.f);
                                                ea.cp().a(8);
                                                break;
                                        }
                                        bi.ae = false;
                                        break;
                                    } else {
                                        e(0);
                                        bh.c(bh.f);
                                        break;
                                    }
                                }
                            } else {
                                byte b3 = (byte) (Y + 1);
                                Y = b3;
                                Y = b3 > W - X ? W - X > 0 ? (byte) (W - X) : 0 : Y;
                                break;
                            }
                        } else {
                            byte b4 = (byte) (Y - 1);
                            Y = b4;
                            Y = b4 < 0 ? 0 : Y;
                            break;
                        }
                    } else {
                        e(5);
                        break;
                    }
                } else if (!bi.y(as.FIRE_PRESSED)) {
                    if (!bi.y(8)) {
                        if (!bi.y(4)) {
                            if (bi.y(MIDIControl.NOTE_ON)) {
                                switch (eb.m[this.j]) {
                                    case 0:
                                        i2 = 36;
                                        i3 = 0;
                                        break;
                                    case 1:
                                        i2 = 39;
                                        i3 = 1;
                                        break;
                                    case 2:
                                        i2 = 42;
                                        i3 = 2;
                                        break;
                                    default:
                                        i2 = 0;
                                        i3 = 0;
                                        break;
                                }
                                if (!eb.b(i3, i2) && !eb.q(i2)) {
                                    e(6);
                                    break;
                                } else {
                                    e(2);
                                    break;
                                }
                            }
                        } else {
                            byte b5 = (byte) (Y + 1);
                            Y = b5;
                            Y = b5 > W - X ? W - X > 0 ? (byte) (W - X) : 0 : Y;
                            byte b6 = (byte) (this.j + 1);
                            this.j = b6;
                            this.j = b6 > eb.m.length - 1 ? 0 : this.j;
                            break;
                        }
                    } else {
                        byte b7 = (byte) (this.j - 1);
                        this.j = b7;
                        this.j = b7 < 0 ? (byte) (eb.m.length - 1) : this.j;
                        break;
                    }
                } else if (this.i == 0) {
                    bh.c(bh.f);
                    e(0);
                    break;
                } else {
                    return false;
                }
                break;
            case 2:
                if (bi.y(Player.STARTED)) {
                    if (this.i == 0) {
                        if (aO[g] != 10) {
                            e(0);
                            bh.c(bh.f);
                            break;
                        } else {
                            e(1);
                            break;
                        }
                    } else {
                        return false;
                    }
                }
                break;
            case 3:
                if (bi.y(Player.STARTED)) {
                    if (this.i != 0) {
                        switch (aO[g]) {
                            case 0:
                            case 16:
                                dz.cn().N();
                                break;
                            case 1:
                                switch (this.R) {
                                    case 1:
                                        ea.cp().a(5);
                                        break;
                                    case 2:
                                        bg.aU().a((byte) 2);
                                        ea.cp().a(3);
                                        bg.aU().a(7);
                                        break;
                                }
                            case 7:
                                ea.cp().a(1);
                                e(0);
                                break;
                            case 11:
                            case 12:
                                ea.cp().a(1);
                                break;
                            case 13:
                                ea.cp().a(3);
                                bg.aU().a((byte) 2);
                                bg.aU().f = 6;
                                break;
                            case Database.INT_ISFEE /*14*/:
                                ea.cp().a(3);
                                bg.aU().a((byte) 2);
                                bg.aU().f = 6;
                                break;
                            case Database.INT_FEECUE /*15*/:
                                ea.cp().a(2);
                                a.M().e = 1;
                                e(0);
                                break;
                            case Database.INT_FEES /*17*/:
                                dz.cn().S();
                                break;
                        }
                    } else {
                        switch (this.e) {
                            case 0:
                                c();
                                b(N, 0);
                                break;
                        }
                        e(0);
                        bi.ac = 0;
                    }
                    bi.ae = false;
                    bi.ac = 0;
                    bh.c(bh.f);
                    break;
                }
                break;
            case 4:
                if (!bi.y(128)) {
                    if (bi.y(as.FIRE_PRESSED)) {
                        Q();
                        break;
                    }
                } else {
                    e(5);
                    break;
                }
                break;
            case 5:
                if (!b(aK, aL)) {
                    e(4);
                    break;
                } else {
                    byte[] bArr = m;
                    byte b8 = aO[g];
                    bArr[b8] = (byte) (bArr[b8] + 1);
                    if (m[aO[g]] < aI[aO[g]] / aH) {
                        e(1);
                    } else {
                        switch (aO[g]) {
                            case 0:
                                m[0] = -100;
                                break;
                            case 1:
                                eb.cq();
                                eb.a(5000);
                                m[1] = 0;
                                break;
                            case 2:
                                for (byte b9 : eb.m) {
                                    short[] sArr = eb.O;
                                    sArr[b9] = (short) (sArr[b9] + 5);
                                    if (eb.O[b9] >= 50) {
                                        eb.O[b9] = 50;
                                    }
                                    byte b10 = 1;
                                    while (true) {
                                        if (b10 < bj.dR[b9].length) {
                                            if (bj.dP[b9][b10] != 0 || eb.O[b9] < bj.fz[bj.dR[b9][b10]][3]) {
                                                b10 = (byte) (b10 + 1);
                                            } else {
                                                bj.dP[b9][b10] = 1;
                                            }
                                        }
                                    }
                                    bg.aU();
                                    bg.c(b9);
                                }
                                m[2] = 0;
                                break;
                            case 3:
                                a.M();
                                a.X();
                                this.af = 2;
                                m[3] = 0;
                                break;
                            case 4:
                                eb.cq();
                                eb.a(2000);
                                m[4] = -100;
                                break;
                            case 5:
                                eb.cq();
                                eb.a(3000);
                                m[5] = -100;
                                break;
                            case 6:
                                eb.cq();
                                eb.a(4000);
                                m[6] = -100;
                                break;
                            case 7:
                                eb.cq().f(0);
                                m[7] = -100;
                                break;
                            case 8:
                                eb.cq().k(24, 20);
                                m[8] = 0;
                                break;
                            case 9:
                                eb.cq().k(22, 8);
                                eb.cq().k(23, 8);
                                m[9] = 0;
                                break;
                            case 10:
                                switch (eb.m[this.j]) {
                                    case 0:
                                        eb.cq().h(36, 1);
                                        eb.cq().h(37, 1);
                                        eb.cq().h(38, 1);
                                        e(3);
                                        break;
                                    case 1:
                                        eb.cq().h(39, 1);
                                        eb.cq().h(40, 1);
                                        eb.cq().h(41, 1);
                                        e(3);
                                        break;
                                    case 2:
                                        eb.cq().h(42, 1);
                                        eb.cq().h(43, 1);
                                        eb.cq().h(44, 1);
                                        e(3);
                                        break;
                                }
                                m[10] = 0;
                                break;
                            case 13:
                                m[13] = 0;
                                bg.aU().d(13);
                                break;
                            case Database.INT_ISFEE /*14*/:
                                m[14] = 0;
                                bg.aU().d(14);
                                break;
                            case Database.INT_FEECUE /*15*/:
                                m[15] = -100;
                                break;
                            case 16:
                                m[16] = -100;
                                break;
                            case Database.INT_FEES /*17*/:
                                m[17] = -100;
                                break;
                        }
                        e(3);
                    }
                    bj.bp();
                    bj.W();
                    break;
                }
                break;
            case 6:
                if (bi.y(128)) {
                    e(5);
                } else if (bi.y(as.FIRE_PRESSED)) {
                    if (this.i != 0) {
                        return false;
                    }
                    ea.cp().dZ = true;
                    e(1);
                }
                if (aO[g] == 10) {
                    if (!bi.y(8)) {
                        if (bi.y(4)) {
                            byte b11 = (byte) (Y + 1);
                            Y = b11;
                            Y = b11 > W - X ? W - X > 0 ? (byte) (W - X) : 0 : Y;
                            break;
                        }
                    } else {
                        byte b12 = (byte) (Y - 1);
                        Y = b12;
                        Y = b12 < 0 ? 0 : Y;
                        break;
                    }
                }
                break;
            case 7:
                e(1);
                break;
            case 8:
                if (!bi.y(128)) {
                    if (bi.y(as.FIRE_PRESSED)) {
                        Q();
                        break;
                    }
                } else {
                    bi.ae = true;
                    e(7);
                    break;
                }
                break;
        }
        return true;
    }

    public final void a(int i2) {
        this.R = (byte) i2;
    }

    public final void a(an anVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        switch (this.f) {
            case 0:
                if (this.ae) {
                    this.ae = false;
                    a.M().b(anVar, 0);
                    switch (this.h) {
                        case 3:
                            i2 = 150;
                            i3 = 47;
                            i4 = 154;
                            i5 = 2;
                            i6 = 3;
                            break;
                        case 4:
                        default:
                            i2 = 0;
                            i3 = 0;
                            i4 = 0;
                            i5 = 0;
                            i6 = 0;
                            break;
                        case 5:
                            i2 = 150;
                            i3 = 28;
                            i4 = 154;
                            i5 = 4;
                            i6 = 5;
                            break;
                    }
                    a.M().b(anVar, 3548436, 45, 80, i2, i4);
                    a.M().a(anVar, 7755858, 10914680, i6, i5, g, 0, 50, 85, i2 - 10, i3);
                    byte b = 0;
                    while (true) {
                        byte b2 = b;
                        if (b2 < this.h) {
                            int i7 = 0;
                            switch (this.h) {
                                case 3:
                                    i7 = 55;
                                    break;
                                case 5:
                                    i7 = 60;
                                    break;
                            }
                            du.a(anVar, this.aC[aO[b2]], 120, ((b2 * 148) / this.h) + ((160 - i7) - p.g), 17, b2 == g ? 16777215 : 5849662);
                            b = (byte) (b2 + 1);
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
            case 1:
                if (aO[g] == 10) {
                    d(anVar);
                    a.M().b(anVar, 3548436, 82, 145, 75, 72);
                    a.M().a(anVar, 7755858, 10914680, 3, 2, this.j, 0, 86, 150, 67, 20);
                    byte b3 = 0;
                    while (true) {
                        byte b4 = b3;
                        if (b4 >= eb.m.length) {
                            anVar.setColor(16777215);
                            anVar.a("选择要购买的人", 120, 240, 33);
                            du.a(anVar, 5, 5);
                            return;
                        }
                        a.M().a(anVar, b4 == this.j ? 16777215 : 5849662, 1, eb.m[b4], 105, (b4 * 20) + 156);
                        b3 = (byte) (b4 + 1);
                    }
                } else {
                    f(anVar);
                    return;
                }
            case 2:
                d(anVar);
                if (aO[g] == 10) {
                    du.a(anVar, new StringBuffer().append("已购买过").append(bj.aC[eb.m[this.j]]).append("的装备").toString(), 120, (160 - p.g) + 20, 65, 16439945);
                    du.a(anVar, "无需重复购买", 120, (160 - p.g) + 45, 65, 16439945);
                } else {
                    byte b5 = 0;
                    while (true) {
                        byte b6 = b5;
                        if (b6 < this.aE.length) {
                            du.a(anVar, this.aE[b6], 120, (b6 * 25) + (160 - p.g), 65, 16439945);
                            b5 = (byte) (b6 + 1);
                        }
                    }
                }
                du.a(anVar, "返回", 2, 314 - p.e, 20, 65280);
                return;
            case 3:
                d(anVar);
                if (aO[g] == 8 || aO[g] == 9) {
                    du.a(anVar, "购买成功!", 120, 135 - p.g, 17, 16439945);
                    du.a(anVar, "已放在道具栏里!", 120, 154 - p.g, 17, 16439945);
                    du.a(anVar, "请注意存盘", 120, 177 - p.g, 17, 16711680);
                } else if (Z == 10) {
                    du.a(anVar, "购买成功!", 120, 135 - p.g, 17, 16439945);
                    du.a(anVar, "已放在装备背包里!", 120, 154 - p.g, 17, 16439945);
                    du.a(anVar, "请注意存盘", 120, 177 - p.g, 17, 16711680);
                } else if (aO[g] != 3) {
                    du.a(anVar, "购买成功!", 120, 152 - p.g, 17, 16439945);
                    du.a(anVar, "请注意存盘", 120, 177 - p.g, 17, 16711680);
                } else if (this.af == 1) {
                    du.a(anVar, "当前队列中的所有角色的", 120, 135 - p.g, 17, 16439945);
                    du.a(anVar, "现有技能已满级", 120, 154 - p.g, 17, 16439945);
                } else if (this.af == 2) {
                    du.a(anVar, "购买成功!", 120, 135 - p.g, 17, 16439945);
                    du.a(anVar, "当前未满级的技能提升1级!", 120, 154 - p.g, 17, 16439945);
                    du.a(anVar, "请注意存盘", 120, 177 - p.g, 17, 16711680);
                }
                du.a(anVar, "返回", 5, 314 - p.e, 20, 65280);
                return;
            case 4:
                d(anVar);
                du.a(anVar, "发送失败", 120, 152 - p.g, 17, 16439945);
                du.a(anVar, "是否重新发送？", 120, 177 - p.g, 17, 16711680);
                du.a(anVar, 4, 4);
                return;
            case 5:
                d(anVar);
                anVar.setColor(16777215);
                du.a(anVar, "发送中", 120, 152 - p.g, 17, 16439945);
                return;
            case 6:
                f(anVar);
                return;
            case 7:
                d(anVar);
                du.a(anVar, "游戏更新", 120, 160 - p.e, 17, 16439945);
                du.a(anVar, "请稍后", 120, 170, 17, 16439945);
                return;
            case 8:
                d(anVar);
                du.a(anVar, "更新失败", 120, 152 - p.g, 17, 16439945);
                du.a(anVar, "是否再次更新？", 120, 177 - p.g, 17, 16711680);
                du.a(anVar, 4, 4);
                return;
            default:
                return;
        }
    }

    public final void b(byte[] bArr, int i2) {
        this.i = (byte) i2;
        g = 0;
        aO = bArr;
        this.h = (byte) aO.length;
        this.ae = true;
        switch (i2) {
            case 0:
                e(0);
                return;
            default:
                e(1);
                return;
        }
    }

    public final void c(int i2) {
        this.e = (byte) i2;
    }
}
