package defpackage;

import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: ch  reason: default package */
public final class ch {
    public static final int OPTION_MENU_ITEM_ABOUT = 64160;
    public static final int OPTION_MENU_ITEM_EXIT = 64161;
    private static final ArrayList hv = new ArrayList();
    private static int[] hw;

    public static void a(MenuItem menuItem) {
        ((ck) hv.get(menuItem.getItemId())).aA();
    }

    public static void a(ck ckVar) {
        if (!hv.contains(ckVar)) {
            hv.add(0, ckVar);
        }
    }

    public static void b(ck ckVar) {
        hv.remove(ckVar);
    }

    protected static void br() {
        a(new ci());
        a(new cj());
    }

    protected static void onDestroy() {
        hv.clear();
    }

    public static void onPrepareOptionsMenu(Menu menu) {
        boolean z;
        int i;
        menu.clear();
        Iterator it = hv.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            ck ckVar = (ck) it.next();
            if (hw != null) {
                boolean z2 = false;
                for (int i4 : hw) {
                    if (i4 == ckVar.getId()) {
                        z2 = true;
                    }
                }
                z = z2;
            } else {
                z = false;
            }
            if (!z) {
                menu.add(131072, i3, i2, ckVar.ay());
                i = i2 + 1;
            } else {
                i = i2;
            }
            i3++;
            i2 = i;
        }
    }
}
