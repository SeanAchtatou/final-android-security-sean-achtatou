package defpackage;

import javax.microedition.midlet.MIDlet;

/* renamed from: Midlet  reason: default package */
public class Midlet extends MIDlet {
    public static Thread a;
    public static bi b;
    public static Midlet c;

    public Midlet() {
        c = this;
        ah.a(this);
    }

    public final void L() {
        if (b == null) {
            b = bi.bo();
            ah.a(this).a((al) b);
            Thread thread = new Thread(b);
            a = thread;
            thread.start();
        }
    }

    public final void x() {
        ah.a(this).a((al) null);
        MIDlet.aN();
    }
}
