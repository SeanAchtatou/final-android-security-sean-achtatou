package defpackage;

import java.io.InputStream;
import java.util.ArrayList;

/* renamed from: bo  reason: default package */
public final class bo {
    private static final String DATA_STORE_FILE = ".dat";
    private static final String LOG_TAG = "DataManager";

    public static boolean x(String str) {
        String[] fileList = cl.getActivity().fileList();
        ArrayList<String> arrayList = new ArrayList<>();
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].endsWith(DATA_STORE_FILE) && (!(str == null || fileList[i].indexOf(str) == -1) || str == null)) {
                    arrayList.add(fileList[i].substring(0, fileList[i].length() - DATA_STORE_FILE.length()));
                }
            }
        }
        for (String equals : arrayList) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static InputStream y(String str) {
        return cl.getActivity().openFileInput(str + DATA_STORE_FILE);
    }
}
