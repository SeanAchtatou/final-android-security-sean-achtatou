package defpackage;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: cf  reason: default package */
public final class cf implements Runnable {
    public static final String LOG_TAG = "MessageDispatchManager";
    public static final int MSG_ARG2_DONT_RECYCLE_ME = -13295;
    public static final int MSG_ARG_NONE = 0;
    private static final ConcurrentLinkedQueue ho = new ConcurrentLinkedQueue();
    private static final LinkedHashSet hp = new LinkedHashSet();
    private static final cf hq = new cf();
    private static final HashMap hr = new HashMap();
    private static final HashSet ht = new HashSet();
    private boolean hs;

    public static void G(int i) {
        b(a(-2004318072, null, 0, 0));
    }

    public static Message a(int i, Object obj) {
        return a(i, obj, 0, 0);
    }

    public static Message a(int i, Object obj, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        obtain.arg1 = 0;
        obtain.arg2 = i3;
        return obtain;
    }

    public static void a(int i, String str) {
        if (!hr.containsKey(Integer.valueOf(i))) {
            hr.put(Integer.valueOf(i), str);
        } else {
            Log.w(LOG_TAG, "The message id has already been registed by " + ((String) hr.get(Integer.valueOf(i))));
        }
    }

    public static void a(cg cgVar) {
        hp.add(cgVar);
    }

    public static void b(Message message) {
        if (!ho.contains(message)) {
            ho.add(message);
        }
    }

    public static void b(cg cgVar) {
        ht.add(cgVar);
    }

    protected static void br() {
        new Thread(hq).start();
    }

    protected static void onDestroy() {
        hq.hs = true;
        ho.clear();
        hp.clear();
    }

    public final void run() {
        boolean z;
        while (!this.hs) {
            while (true) {
                Message message = (Message) ho.poll();
                if (message == null) {
                    break;
                }
                Iterator it = hp.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((cg) it.next()).a(message)) {
                            z = true;
                            Thread.yield();
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    Log.w(LOG_TAG, "There is a message[" + (hr.containsKey(Integer.valueOf(message.what)) ? (Serializable) hr.get(Integer.valueOf(message.what)) : Integer.valueOf(message.what)) + "] which no consumer could deal with it.");
                }
                if (message.arg2 != -13295) {
                    message.recycle();
                }
            }
            if (!ht.isEmpty()) {
                Iterator it2 = ht.iterator();
                while (it2.hasNext()) {
                    hp.remove((cg) it2.next());
                }
                ht.clear();
            }
            SystemClock.sleep(1);
        }
    }
}
