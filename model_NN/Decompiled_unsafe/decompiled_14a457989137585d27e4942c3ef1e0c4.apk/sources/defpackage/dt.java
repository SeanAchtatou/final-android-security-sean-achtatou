package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import com.androidbox.jhfylhtc.R;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/* renamed from: dt  reason: default package */
public final class dt {
    public static final String LOG_TAG = "ResourceUtils";
    private static final HashMap jW = new HashMap();

    private static int H(String str) {
        try {
            return a(R.drawable.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get resource id:" + str);
        }
    }

    public static int I(String str) {
        try {
            return a(R.raw.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get raw id:" + str);
        }
    }

    public static Rect J(String str) {
        if (str == null) {
            Log.w(LOG_TAG, "Rect string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length != 4) {
            throw new IllegalArgumentException("Invalid rect string:" + str);
        }
        int parseInt = Integer.parseInt(split[0].trim());
        int parseInt2 = Integer.parseInt(split[1].trim());
        return new Rect(parseInt, parseInt2, Integer.parseInt(split[2].trim()) + parseInt, Integer.parseInt(split[3].trim()) + parseInt2);
    }

    public static Bitmap K(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        } else if (str.trim().length() == 0) {
            return null;
        } else {
            if (jW.containsKey(str)) {
                return (Bitmap) jW.get(str);
            }
            try {
                Bitmap decodeResource = BitmapFactory.decodeResource(cl.getActivity().getResources(), H(str.trim()));
                jW.put(str, decodeResource);
                return decodeResource;
            } catch (Exception e) {
                throw new IOException("Fail to load bitmap " + str + ":" + e.toString());
            }
        }
    }

    public static Bitmap[] L(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length <= 0) {
            return null;
        }
        Bitmap[] bitmapArr = new Bitmap[split.length];
        for (int i = 0; i < split.length; i++) {
            bitmapArr[i] = K(split[i].trim());
        }
        return bitmapArr;
    }

    public static final String M(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? "" : str.substring(0, lastIndexOf + 1);
    }

    public static final String N(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    private static int a(Class cls, String str) {
        return cls.getField(str).getInt(null);
    }
}
