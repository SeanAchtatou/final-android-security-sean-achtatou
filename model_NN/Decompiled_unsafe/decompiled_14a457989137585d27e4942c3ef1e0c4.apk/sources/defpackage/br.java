package defpackage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* renamed from: br  reason: default package */
public final class br implements SurfaceHolder.Callback {
    public static final String LOG_TAG = "GraphicsManager";
    public static SurfaceView gE;
    /* access modifiers changed from: private */
    public static final br gF = new br();
    /* access modifiers changed from: private */
    public static boolean gG = false;
    private static int gH = 33;
    private static SurfaceHolder gI;
    private static final Rect gJ = new Rect(-1, -1, -1, -1);
    private static final LinkedHashSet gL = new LinkedHashSet();
    /* access modifiers changed from: private */
    public static boolean gM;
    private static final Runnable gN = new bt();
    /* access modifiers changed from: private */
    public boolean gK;

    public static void A(int i) {
        gH = i;
    }

    public static Bitmap a(int i, int i2, boolean z, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
        createBitmap.eraseColor(-16777216);
        return createBitmap;
    }

    public static Bitmap a(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with InputStream error." + e);
            return null;
        }
    }

    public static Bitmap a(int[] iArr, int i, int i2) {
        if (iArr == null) {
            return null;
        }
        try {
            return Bitmap.createBitmap(iArr, i, i2, Bitmap.Config.ARGB_8888);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with rgb error." + e);
            return null;
        }
    }

    public static final void a(bu buVar) {
        gL.add(buVar);
    }

    protected static void b(Activity activity) {
        SurfaceView surfaceView = new SurfaceView(activity);
        gE = surfaceView;
        surfaceView.setId(268049792);
        gE.setFocusable(true);
        gE.setFocusableInTouchMode(true);
        gE.setLongClickable(true);
        gE.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        gI = gE.getHolder();
        bv.a(gE);
        gI.addCallback(gF);
        try {
            gI.setType(1);
        } catch (Exception e) {
            try {
                gI.setType(2);
            } catch (Exception e2) {
                gI.setType(0);
            }
        }
        cf.a(new bs());
        if (!gM) {
            gM = true;
            new Thread(gN).start();
        }
    }

    public static final void b(bu buVar) {
        gL.remove(buVar);
    }

    public static void bs() {
        gG = false;
        gM = false;
    }

    public static final void bt() {
        if (gI == null) {
            gI = gE.getHolder();
        }
        synchronized (gI) {
            Canvas lockCanvas = gI.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.drawColor(-16777216);
                Iterator it = gL.iterator();
                while (it.hasNext()) {
                    ((bu) it.next()).onDraw(lockCanvas);
                }
                gI.unlockCanvasAndPost(lockCanvas);
            }
        }
    }

    public static Bitmap c(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        if (i < 0 || i2 < 0 || bArr.length < 0) {
            throw new IllegalArgumentException();
        } else if (i + i2 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            try {
                return BitmapFactory.decodeByteArray(bArr, i, i2);
            } catch (Exception e) {
                Log.w(LOG_TAG, "createBitmap with imageData error." + e);
                return null;
            }
        }
    }

    protected static void onDestroy() {
        bs();
        gF.gK = false;
        gI.removeCallback(gF);
        bv.b(gE);
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        gF.gK = true;
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        gF.gK = false;
    }
}
