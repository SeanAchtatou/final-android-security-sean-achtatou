package defpackage;

import android.util.Log;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: bq  reason: default package */
public final class bq {
    public static final String LOG_TAG = "FeatureManager";
    private static HashSet gD = new HashSet();

    protected static void br() {
    }

    public static void h(String str, String str2) {
        try {
            cx cxVar = (cx) Class.forName(str).newInstance();
            cxVar.F(str2);
            gD.add(cxVar);
        } catch (Exception e) {
            Log.w(LOG_TAG, e);
        }
    }

    protected static void onDestroy() {
        if (!gD.isEmpty()) {
            Iterator it = gD.iterator();
            while (it.hasNext()) {
                ((cx) it.next()).onDestroy();
            }
        }
    }
}
