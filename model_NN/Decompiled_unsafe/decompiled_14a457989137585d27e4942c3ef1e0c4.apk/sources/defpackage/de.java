package defpackage;

import android.graphics.Typeface;
import java.util.HashMap;

/* renamed from: de  reason: default package */
public final class de {
    public static int SIZE_LARGE = 16;
    public static int SIZE_MEDIUM = 14;
    public static int SIZE_SMALL = 12;
    private static final HashMap ir = new HashMap();

    public static int a(am amVar, char c) {
        dd b = b(amVar);
        b.iq[0] = c;
        return (int) b.ew.measureText(b.iq, 0, 1);
    }

    public static int a(am amVar, String str) {
        return (int) b(amVar).ew.measureText(str);
    }

    public static dd b(am amVar) {
        dd ddVar = (dd) ir.get(amVar);
        if (ddVar != null) {
            return ddVar;
        }
        Typeface typeface = Typeface.SANS_SERIF;
        if (amVar.aG() == 0) {
            typeface = Typeface.SANS_SERIF;
        } else if (amVar.aG() == 32) {
            typeface = Typeface.MONOSPACE;
        } else if (amVar.aG() == 64) {
            typeface = Typeface.SANS_SERIF;
        }
        amVar.getStyle();
        int i = (amVar.getStyle() & 1) != 0 ? 1 : 0;
        if ((amVar.getStyle() & 2) != 0) {
            i |= 2;
        }
        dd ddVar2 = new dd(Typeface.create(typeface, i), amVar.getSize() == 8 ? SIZE_SMALL : amVar.getSize() == 0 ? SIZE_MEDIUM : amVar.getSize() == 16 ? SIZE_LARGE : 0, (amVar.getStyle() & 4) != 0);
        ir.put(amVar, ddVar2);
        return ddVar2;
    }

    public static int c(am amVar) {
        dd b = b(amVar);
        return b.ew.getFontMetricsInt(b.ip);
    }
}
