package defpackage;

import android.os.Message;
import java.io.InterruptedIOException;

/* renamed from: di  reason: default package */
public final class di implements bc, cg {
    public static final int MSG_GCF_SMS_CONNECTION_RECEIVE = 15391747;
    public static final int MSG_GCF_SMS_CONNECTION_SEND = 15391744;
    public static final int MSG_GCF_SMS_CONNECTION_SEND_COMPLETE = 15391745;
    public static final int MSG_GCF_SMS_CONNECTION_SEND_FAIL = 15391746;
    private static final int NOT_SENT = 0;
    private static final int SENT_FAIL = 2;
    private static final int SENT_OK = 1;
    private String cR;
    private int iL = 0;
    private bd iM;
    private bb iN;

    public di(String str) {
        cf.a((int) MSG_GCF_SMS_CONNECTION_SEND, "MSG_GCF_SMS_CONNECTION_SEND");
        cf.a((int) MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, "MSG_GCF_SMS_CONNECTION_SEND_COMPLETE");
        cf.a((int) MSG_GCF_SMS_CONNECTION_SEND_FAIL, "MSG_GCF_SMS_CONNECTION_SEND_FAIL");
        cf.a((int) MSG_GCF_SMS_CONNECTION_RECEIVE, "MSG_GCF_SMS_CONNECTION_RECEIVE");
        this.cR = str;
        cf.a(this);
    }

    public final void a(bb bbVar) {
        cf.a(this);
        cf.b(cf.a((int) MSG_GCF_SMS_CONNECTION_SEND, bbVar));
        synchronized (this) {
            try {
                this.iL = 0;
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        cf.b(this);
        if (this.iL != 1) {
            throw new InterruptedIOException("Fail to send.");
        }
        return;
    }

    public final boolean a(Message message) {
        if (message.what == 15391745) {
            this.iL = 1;
            synchronized (this) {
                notifyAll();
            }
            return true;
        } else if (message.what == 15391746) {
            this.iL = 2;
            synchronized (this) {
                notifyAll();
            }
            return true;
        } else if (message.what != 15391747) {
            return false;
        } else {
            this.iN = (bb) message.obj;
            if (this.iM != null) {
                bd bdVar = this.iM;
            }
            synchronized (this) {
                notifyAll();
            }
            return true;
        }
    }

    public final void close() {
    }

    public final bb s(String str) {
        bb dlVar;
        if (str.equals(bc.TEXT_MESSAGE)) {
            dlVar = new dk();
        } else if (str.equals(bc.BINARY_MESSAGE)) {
            dlVar = new dj();
        } else if (str.equals(bc.MULTIPART_MESSAGE)) {
            dlVar = new dl();
        } else {
            throw new IllegalArgumentException(str);
        }
        if (this.cR != null) {
            dlVar.setAddress(this.cR);
        }
        return dlVar;
    }
}
