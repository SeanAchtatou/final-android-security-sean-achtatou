package defpackage;

import android.content.DialogInterface;
import java.util.HashMap;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: da  reason: default package */
public final class da implements DialogInterface.OnClickListener {
    final /* synthetic */ MIDPDevice ig;
    private final /* synthetic */ HashMap ih;
    private final /* synthetic */ String[] ii;

    public da(MIDPDevice mIDPDevice, HashMap hashMap, String[] strArr) {
        this.ig = mIDPDevice;
        this.ih = hashMap;
        this.ii = strArr;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        bl.w((String) this.ih.get(this.ii[i]));
    }
}
