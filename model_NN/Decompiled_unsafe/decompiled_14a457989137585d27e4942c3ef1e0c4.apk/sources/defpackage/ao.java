package defpackage;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import java.io.IOException;

/* renamed from: ao  reason: default package */
public final class ao {
    private static final Matrix fk = new Matrix();
    public Bitmap fh;
    public boolean fi;
    private an fj;
    public int height;
    public int width;

    private ao(Bitmap bitmap) {
        this.fh = bitmap;
        if (bitmap != null) {
            this.width = bitmap.getWidth();
            this.height = bitmap.getHeight();
        }
    }

    public static ao a(byte[] bArr, int i, int i2) {
        Bitmap c = br.c(bArr, 0, i2);
        if (c == null) {
            return null;
        }
        ao aoVar = new ao(c);
        aoVar.fi = false;
        return aoVar;
    }

    public static ao a(int[] iArr, int i, int i2, boolean z) {
        Bitmap a = br.a(iArr, i, i2);
        if (a == null) {
            throw new IllegalArgumentException();
        }
        ao aoVar = new ao(a);
        aoVar.fi = false;
        return aoVar;
    }

    public static ao o(int i, int i2) {
        ao aoVar = new ao(br.a(i, i2, false, -16777216));
        aoVar.fi = true;
        return aoVar;
    }

    public static ao p(String str) {
        Bitmap a = br.a(cl.A(str));
        if (a == null) {
            throw new IOException();
        }
        ao aoVar = new ao(a);
        aoVar.fi = false;
        return aoVar;
    }

    public final void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6) {
        int i7;
        if (Math.abs(i2) < i5) {
            i7 = (i2 > 0 ? 1 : -1) * i5;
        } else {
            i7 = i2;
        }
        this.fh.getPixels(iArr, 0, i7, 0, 0, i5, i6);
    }

    public final an aJ() {
        if (this.fi) {
            if (this.fj == null) {
                this.fj = new df(this.fh);
                Log.d(getClass().getName(), "Create a MutableImage Graphics." + this.width + " " + this.height);
            }
            this.fj.aH();
            return this.fj;
        }
        throw new IllegalStateException("Image is immutable");
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }

    public final boolean isMutable() {
        return this.fi;
    }
}
