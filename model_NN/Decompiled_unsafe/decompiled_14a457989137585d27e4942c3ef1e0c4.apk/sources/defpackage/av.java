package defpackage;

import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: av  reason: default package */
public final class av {
    public static final int AUTHMODE_ANY = 1;
    public static final int AUTHMODE_PRIVATE = 0;
    public static final String LOG_TAG = "RMS";
    private static final String PREFIX = "rms_";
    private int dk;
    private HashMap fq = new HashMap();
    private int fr;
    private long fs;
    private HashSet ft = new HashSet();
    private boolean fu;
    private String name;
    private int version;

    private av(String str) {
        this.name = str;
    }

    public static av a(String str, boolean z) {
        av avVar = new av(str);
        if (bo.x(PREFIX + str)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(bo.y(PREFIX + str));
                avVar.a(dataInputStream);
                dataInputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                throw new aw();
            }
        } else {
            try {
                avVar.aR();
            } catch (Exception e2) {
                Log.e(LOG_TAG, e2.getMessage());
                throw new aw();
            }
        }
        avVar.fu = true;
        return avVar;
    }

    private synchronized void a(DataInputStream dataInputStream) {
        this.version = dataInputStream.readInt();
        this.fs = dataInputStream.readLong();
        this.fr = dataInputStream.readInt();
        int readInt = dataInputStream.readInt();
        for (int i = 0; i < readInt; i++) {
            int readInt2 = dataInputStream.readInt();
            byte[] bArr = new byte[dataInputStream.readInt()];
            dataInputStream.read(bArr);
            this.fq.put(Integer.valueOf(readInt2), bArr);
        }
        aP();
    }

    private synchronized void a(DataOutputStream dataOutputStream) {
        this.version++;
        dataOutputStream.writeInt(this.version);
        dataOutputStream.writeLong(System.currentTimeMillis());
        dataOutputStream.writeInt(this.fr);
        dataOutputStream.writeInt(this.fq.size());
        for (Map.Entry entry : this.fq.entrySet()) {
            dataOutputStream.writeInt(((Integer) entry.getKey()).intValue());
            byte[] bArr = (byte[]) entry.getValue();
            dataOutputStream.writeInt(bArr.length);
            dataOutputStream.write(bArr);
        }
    }

    private void aP() {
        this.dk = 0;
        for (byte[] length : this.fq.values()) {
            this.dk = length.length + this.dk;
        }
    }

    private synchronized void aQ() {
        if (bo.x(PREFIX + this.name)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(bo.y(PREFIX + this.name));
                int readInt = dataInputStream.readInt();
                dataInputStream.close();
                if (readInt > this.version) {
                    a(new DataInputStream(bo.y(PREFIX + this.name)));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return;
    }

    private synchronized void aR() {
        DataOutputStream dataOutputStream = new DataOutputStream(cl.getActivity().openFileOutput((PREFIX + this.name) + ".dat", 1));
        a(dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        aP();
    }

    private boolean isOpen() {
        if (this.fu) {
            return this.fu;
        }
        Log.e(LOG_TAG, "RecordStoreNotOpenException");
        throw new az();
    }

    public final void a(int i, byte[] bArr, int i2, int i3) {
        isOpen();
        if (this.fq.containsKey(1)) {
            this.fq.remove(1);
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, 0, bArr2, 0, i3);
            this.fq.put(1, bArr2);
            try {
                aR();
            } catch (Exception e) {
            }
            Iterator it = this.ft.iterator();
            while (it.hasNext()) {
                it.next();
            }
            return;
        }
        throw new au();
    }

    public final void aS() {
        isOpen();
        aQ();
        this.ft.clear();
        this.ft = null;
        this.fq.clear();
        this.fu = false;
        this.fq = null;
        this.name = null;
        System.gc();
    }

    public final int aT() {
        isOpen();
        aQ();
        return this.fq.size();
    }

    public final int b(byte[] bArr, int i, int i2) {
        isOpen();
        this.fr++;
        byte[] bArr2 = new byte[i2];
        if (i2 != 0) {
            System.arraycopy(bArr, 0, bArr2, 0, i2);
        }
        this.fq.put(Integer.valueOf(this.fr), bArr2);
        try {
            aR();
            Iterator it = this.ft.iterator();
            while (it.hasNext()) {
                it.next();
                int i3 = this.fr;
            }
            return this.fr;
        } catch (Exception e) {
            throw new aw();
        }
    }

    public final int o(int i) {
        isOpen();
        aQ();
        if (this.fq.containsKey(1)) {
            return ((byte[]) this.fq.get(1)).length;
        }
        throw new au();
    }

    public final byte[] p(int i) {
        isOpen();
        aQ();
        if (this.fq.containsKey(1)) {
            return (byte[]) this.fq.get(1);
        }
        throw new au();
    }
}
