package defpackage;

import com.network.app.data.Database;
import com.nokia.mid.ui.DirectGraphics;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;

/* renamed from: a  reason: default package */
public final class a {
    public static ao[] I;
    public static ao[] J;
    public static ao[] K;
    public static byte[] N;
    public static short[] O;
    public static short P;
    public static byte W;
    public static byte X;
    public static byte Y;
    public static byte Z;
    private static byte aa = 6;
    public static byte aj = 0;
    public static byte ak = 0;
    public static byte al;
    public static byte am;
    public static short[] av;
    private static a d;
    public static byte f;
    public static byte g;
    public static byte h;
    public static byte i;
    public static byte j;
    public static short k;
    public static short l;
    public ao A;
    public ao B;
    public ao C;
    public ao D;
    public ao F;
    public ao G;
    public ao H;
    private byte[] L = {5, 7, 6, 8, 1, 2};
    public boolean M = true;
    public byte R;
    public short S;
    public short T;
    public short U;
    public short V;
    public short[] ab;
    public int ac;
    public short ad;
    public boolean ae;
    public byte af;
    public short ag;
    public byte ah;
    private short[][][] ai = {new short[][]{new short[]{0, 1}, new short[]{2, 3}, new short[]{4, 5}, new short[]{6, 7}, new short[]{8, 9}, new short[]{10, 11}}, new short[][]{new short[]{71, 72}, new short[]{73, 74}, new short[]{75, 76}, new short[]{82, 83, 84}, new short[]{85, 86, 87}, new short[]{88, 89, 90}}, new short[][]{new short[]{37, 38}, new short[]{39, 40}, new short[]{41, 42}, new short[]{43, 44}}, new short[][]{new short[]{16, 17}, new short[]{18, 17}, new short[]{16, 19}, new short[]{18, 19}, new short[]{20, 21}, new short[]{22, 23}, new short[]{24, 25}, new short[]{26, 27}, new short[]{12, 13}}, new short[][]{new short[]{47, 48}, new short[]{49, 50}, new short[]{51, 52}, new short[]{4, 5}, new short[]{80, 81}}, new short[][]{new short[]{56, 57}, new short[]{58, 44}, new short[]{77, 78}, new short[]{55, 79}}};
    public byte an;
    public short ao;
    public byte ap;
    public short[] aq = new short[4];
    public short[] ar = new short[4];
    public byte as;
    public byte at;
    public byte au;
    public byte e;
    public byte[] m;
    public n n;
    public n o;
    public n[] p;
    public ao q;
    public ao r;
    public ao s;
    public ao t;
    public ao u;
    public ao v;
    public ao w;

    private a() {
    }

    public static a M() {
        if (d == null) {
            a aVar = new a();
            d = aVar;
            aVar.M = true;
            aVar.a("/u/font.hy");
            aVar.S();
            System.gc();
        }
        return d;
    }

    private void O() {
        this.R = (byte) bj.gw[P][2];
        this.S = bj.gw[P][3];
        this.T = bj.gw[P][1];
        switch (this.T) {
            case 1:
                this.U = bj.gw[P][4];
                this.V = bj.gw[P][5];
                return;
            case 2:
            case 3:
                this.U = bj.gw[P][6];
                this.V = bj.gw[P][7];
                return;
            default:
                return;
        }
    }

    public static void P() {
        al = 4;
        i = 0;
        int length = (short) eb.fA.length;
        N = new byte[length];
        O = new short[length];
        for (int i2 = 0; i2 < N.length; i2++) {
            N[i2] = (byte) eb.fA[i2][0];
            O[i2] = eb.fA[i2][1];
        }
        P = (short) N[0];
        am = (byte) N.length;
    }

    public static void R() {
        if (bi.y(1)) {
            if (bh.e < 6) {
                bh.e = (byte) (bh.e + 1);
                bh.a(bh.f);
            }
        } else if (bi.y(2) && bh.e > 0) {
            bh.e = (byte) (bh.e - 1);
            bh.a(bh.f);
        }
    }

    private void T() {
        if (bi.y(2)) {
            byte b = (byte) (g - 1);
            g = b;
            g = b < 0 ? (byte) (eb.m.length - 1) : g;
            V();
        } else if (bi.y(1)) {
            byte b2 = (byte) (g + 1);
            g = b2;
            g = b2 > eb.m.length - 1 ? 0 : g;
            V();
        }
    }

    public static void U() {
        if (ak < am - 1) {
            byte b = (byte) (ak + 1);
            ak = b;
            if (b - aj > al - 1) {
                aj = (byte) (aj + 1);
            }
        }
    }

    private void V() {
        ak = 0;
        aj = 0;
        eb.cq();
        eb.c(eb.m[g]);
        switch (this.ah) {
            case 1:
                a((int) eb.m[g]);
                return;
            case 2:
                al = 3;
                switch (j) {
                    case 0:
                        int length = (short) eb.dQ[eb.m[g]].length;
                        N = new byte[length];
                        O = new short[length];
                        for (int i2 = 0; i2 < N.length; i2++) {
                            N[i2] = eb.dQ[eb.m[g]][i2][0];
                            O[i2] = (short) eb.dQ[eb.m[g]][i2][1];
                        }
                        break;
                    case 1:
                        int length2 = (short) eb.fz.length;
                        N = new byte[length2];
                        O = new short[length2];
                        for (int i3 = 0; i3 < N.length; i3++) {
                            N[i3] = (byte) eb.fz[i3][0];
                            O[i3] = eb.fz[i3][1];
                        }
                        break;
                }
                P = (short) N[0];
                O();
                am = (byte) N.length;
                return;
            case 3:
                P();
                return;
            default:
                return;
        }
    }

    public static boolean W() {
        byte[] bArr;
        int i2 = 0;
        int i3 = 0;
        while (i2 < eb.m.length) {
            byte b = eb.m[i2];
            short s2 = eb.O[b];
            byte[] bArr2 = bj.dR[b];
            int i4 = i3;
            int i5 = 0;
            while (i5 < bArr2.length && bj.fz[bArr2[i5]][3] <= s2) {
                i4 = i5 + 1;
                i5++;
            }
            dz.cn();
            if (dz.q(b)) {
                dz.cn();
                byte[] z = dz.z(b);
                byte[] bArr3 = new byte[(z.length + i4)];
                for (int i6 = 0; i6 < bArr3.length; i6++) {
                    if (i6 < i4) {
                        bArr3[i6] = bArr2[i6];
                    } else {
                        bArr3[i6] = z[i6 - i4];
                    }
                }
                bArr = bArr3;
            } else {
                bArr = new byte[i4];
                for (int i7 = 0; i7 < i4; i7++) {
                    bArr[i7] = bArr2[i7];
                }
            }
            for (byte q2 : bArr) {
                if (eb.ai[b][0][eb.q(b, q2)] < eb.e) {
                    return true;
                }
            }
            i2++;
            i3 = i4;
        }
        return false;
    }

    public static void X() {
        byte[] bArr;
        int i2 = 0;
        int i3 = 0;
        while (i2 < eb.m.length) {
            byte b = eb.m[i2];
            short s2 = eb.O[b];
            byte[] bArr2 = bj.dR[b];
            int i4 = i3;
            int i5 = 0;
            while (i5 < bArr2.length && bj.fz[bArr2[i5]][3] <= s2) {
                i4 = i5 + 1;
                i5++;
            }
            dz.cn();
            if (dz.q(b)) {
                dz.cn();
                byte[] z = dz.z(b);
                byte[] bArr3 = new byte[(z.length + i4)];
                for (int i6 = 0; i6 < bArr3.length; i6++) {
                    if (i6 < i4) {
                        bArr3[i6] = bArr2[i6];
                    } else {
                        bArr3[i6] = z[i6 - i4];
                    }
                }
                bArr = bArr3;
            } else {
                bArr = new byte[i4];
                for (int i7 = 0; i7 < i4; i7++) {
                    bArr[i7] = bArr2[i7];
                }
            }
            for (byte q2 : bArr) {
                int q3 = eb.q(b, q2);
                if (eb.ai[b][0][q3] < eb.e) {
                    short[] sArr = eb.ai[b][0];
                    sArr[q3] = (short) (sArr[q3] + 1);
                }
            }
            i2++;
            i3 = i4;
        }
    }

    public static void a(int i2) {
        ak = 0;
        aj = 0;
        h = 0;
        l = 0;
        k = 0;
        al = 3;
        short s2 = eb.O[i2];
        byte[] bArr = bj.dR[i2];
        byte b = 0;
        int i3 = 0;
        while (b < bArr.length && (bg.as >= 2 || bj.fz[bArr[b]][3] <= s2)) {
            i3 = (byte) (b + 1);
            b = (byte) (b + 1);
        }
        if (i2 < 3) {
            dz.cn();
            if (dz.q(i2)) {
                dz.cn();
                byte[] z = dz.z(i2);
                N = new byte[(z.length + i3)];
                for (int i4 = 0; i4 < N.length; i4++) {
                    if (i4 < i3) {
                        N[i4] = bArr[i4];
                    } else {
                        N[i4] = z[i4 - i3];
                    }
                }
            } else {
                N = new byte[i3];
                for (int i5 = 0; i5 < N.length; i5++) {
                    N[i5] = bArr[i5];
                }
            }
        } else {
            N = new byte[i3];
            for (int i6 = 0; i6 < N.length; i6++) {
                N[i6] = bArr[i6];
            }
        }
        P = (short) N[ak];
        am = (byte) N.length;
        eb.a(i2, P);
    }

    private void a(int i2, int i3) {
        if (bj.fz[P][1] != 3) {
            return;
        }
        if (eb.aq[eb.m[i2]] < this.ad) {
            this.af = 11;
            b(3);
        } else if (c(i2, 0)) {
            c(0);
        } else {
            this.af = 10;
            b(3);
        }
    }

    private void a(an anVar, int i2, int i3) {
        String str;
        String str2;
        String str3 = null;
        String[] strArr = new String[3];
        switch (this.R) {
            case 0:
                str = "宁甄";
                break;
            case 1:
                str = "云凌";
                break;
            case 2:
                str = "晨昕";
                break;
            default:
                str = null;
                break;
        }
        switch (this.T) {
            case 1:
                str3 = "物理攻击";
                str2 = "法术攻击";
                break;
            case 2:
            case 3:
                str3 = "物理防御";
                str2 = "法术防御";
                break;
            default:
                str2 = null;
                break;
        }
        strArr[0] = new StringBuffer().append(str).append("专属 ").append("使用等级").append((int) this.S).toString();
        strArr[1] = new StringBuffer().append(str3).append((int) this.U).toString();
        if (this.V == 0) {
            strArr[2] = "";
        } else {
            strArr[2] = new StringBuffer().append(str2).append((int) this.V).toString();
        }
        anVar.setColor(2035968);
        for (int i4 = 0; i4 < 3; i4++) {
            anVar.a(strArr[i4], 120, (p.e * i4) + i3, 33);
        }
    }

    private void a(an anVar, int i2, int i3, int i4) {
        int i5 = i2 * 18;
        for (int i6 = 0; i6 < 18; i6++) {
            for (int i7 = 0; i7 < 8; i7++) {
                if ((this.m[i5 + i6] & (1 << (7 - i7))) != 0) {
                    anVar.a((((i6 << 3) + i7) % 12) + i3, (((i6 << 3) + i7) / 12) + i4, (((i6 << 3) + i7) % 12) + i3, (((i6 << 3) + i7) / 12) + i4);
                }
            }
        }
    }

    public static void a(an anVar, int i2, int i3, int i4, int i5, int i6, int i7) {
        anVar.setColor(i3);
        anVar.c(i4, i5, i6, i7);
        anVar.setColor(i2);
        anVar.b(i4, i5, i6, i7);
    }

    public static void a(an anVar, int i2, String str, int i3, int i4, int i5, int i6) {
        anVar.setColor(2035968);
        char[] charArray = str.toCharArray();
        am a = am.a(0, 0, 8);
        int o2 = a.o("哦");
        int i7 = 0;
        int i8 = 0;
        for (char a2 : charArray) {
            anVar.a(a2, (i8 * o2) + i3, i4 + i7, 0);
            i8++;
            if (i8 * o2 > i5) {
                i8 = 0;
                i7 += a.getHeight() + 2;
            }
        }
    }

    private void a(String str) {
        if (this.m == null) {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(str));
            try {
                this.m = new byte[dataInputStream.readInt()];
                dataInputStream.read(this.m);
                dataInputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private static int[] a(ao aoVar) {
        int width = aoVar.getWidth();
        int height = aoVar.getHeight();
        int[] iArr = new int[(width * height)];
        aoVar.a(iArr, 0, width, 0, 0, width, height);
        return iArr;
    }

    public static ao b(ao aoVar) {
        int width = aoVar.getWidth();
        int height = aoVar.getHeight();
        int[] a = a(aoVar);
        for (int i2 = 0; i2 < height; i2++) {
            for (int i3 = 0; i3 < width; i3++) {
                int i4 = a[(i2 * width) + i3];
                int i5 = (int) ((((double) ((65280 & i4) >> 8)) * 0.587d) + (0.299d * ((double) ((16711680 & i4) >> 16))) + (0.114d * ((double) (i4 & 255))));
                a[(i2 * width) + i3] = i5 | (((-16777216 & i4) >> 24) << 24) | (i5 << 16) | (i5 << 8);
            }
        }
        return ao.a(a, width, height, true);
    }

    private void b(int i2) {
        switch (i2) {
            case 0:
                ea.cp().dZ = true;
                g = 0;
                l = 0;
                h = 0;
                k = 0;
                i = 0;
                break;
            case 1:
                aj = 0;
                ak = 0;
                break;
        }
        this.e = (byte) i2;
        this.M = true;
    }

    private void b(an anVar) {
        b(anVar, 0);
        b(anVar, 3548436, 75, 116, 90, 92);
        a(anVar, 7755858, 10914680, 4, 3, h, aj, 80, 120, 80, 20);
        byte b = 0;
        while (b < 4) {
            a(anVar, h == b ? 16777215 : 5849662, 2, b, 108, (b * 21) + 125);
            b = (byte) (b + 1);
        }
    }

    private void b(an anVar, int i2, int i3) {
        du.a(anVar, this.G, 0, 32, 68, 0);
        a(anVar, 16777215, 6, eb.m[i2], 42, 90);
        if (i3 == 0 && eb.m.length > 1) {
            du.a(anVar, this.w, 0, 33, 149, 0);
            du.a(anVar, this.w, 2, 50, 149, 0);
        }
    }

    private static void b(an anVar, int i2, int i3, int i4) {
        eb.cq();
        int f2 = eb.f(eb.m[i2], 0, i3);
        eb.cq();
        int f3 = (f2 * 35) / eb.f(eb.m[i2], 1, i3);
        int i5 = f3 <= 0 ? 1 : f3;
        c(anVar, 105, (i2 * 20) + 142, i5, i5, 35, i4);
        du.b(anVar, f2, 160, (i2 * 20) + 140, 2, 24);
    }

    private static boolean b(int i2, int i3) {
        byte b = eb.m[i2];
        switch (i3) {
            case 0:
                if (bg.aU().eq[b][1] == -1) {
                    return true;
                }
                break;
            case 1:
                if (bg.aU().eq[b][4] == -1) {
                    return true;
                }
                break;
            case 2:
                if (bg.aU().eq[b][2] == -1) {
                    return true;
                }
                break;
            case 3:
                if (bg.aU().eq[b][3] == -1) {
                    return true;
                }
                break;
        }
        return false;
    }

    public static void c(an anVar, int i2, int i3, int i4, int i5, int i6, int i7) {
        switch (i7) {
            case 0:
                anVar.setColor(11017253);
                anVar.c(i2 + 1, i3, i4, 3);
                anVar.setColor(149188);
                anVar.c(i2 + 1, i3 + 17, i5, 3);
                anVar.setColor(0);
                anVar.a(i2 + 1, i3 - 1, i2 + i6, i3 - 1);
                anVar.a(i2 + 1, i3 + 3, i2 + i6, i3 + 3);
                anVar.a(i2, i3, i2, i3 + 2);
                anVar.a(i2 + i6 + 1, i3, i2 + i6 + 1, i3 + 2);
                anVar.a(i2 + 1, i3 + 16, i2 + i6, i3 + 16);
                anVar.a(i2 + 1, i3 + 20, i2 + i6, i3 + 20);
                anVar.a(i2, i3 + 17, i2, i3 + 19);
                anVar.a(i2 + i6 + 1, i3 + 17, i2 + i6 + 1, i3 + 19);
                return;
            case 1:
                anVar.setColor(11017253);
                anVar.c(i2 + 1, i3, i4, 3);
                anVar.setColor(0);
                anVar.a(i2 + 1, i3 - 1, i2 + i6, i3 - 1);
                anVar.a(i2 + 1, i3 + 3, i2 + i6, i3 + 3);
                anVar.a(i2, i3, i2, i3 + 2);
                anVar.a(i2 + i6 + 1, i3, i2 + i6 + 1, i3 + 2);
                return;
            case 2:
                anVar.setColor(149188);
                anVar.c(i2 + 1, i3, i5, 3);
                anVar.setColor(0);
                anVar.a(i2 + 1, i3 - 1, i2 + i6, i3 - 1);
                anVar.a(i2 + 1, i3 + 3, i2 + i6, i3 + 3);
                anVar.a(i2, i3, i2, i3 + 2);
                anVar.a(i2 + i6 + 1, i3, i2 + i6 + 1, i3 + 2);
                return;
            case 3:
                anVar.setColor(8177135);
                anVar.c(i2 + 1, i3, i4, 3);
                anVar.c(i2, i3, i4, 3);
                anVar.setColor(0);
                anVar.a(i2 + 1, i3 - 1, i2 + i6, i3 - 1);
                anVar.a(i2 + 1, i3 + 3, i2 + i6, i3 + 3);
                anVar.a(i2, i3, i2, i3 + 2);
                anVar.a(i2 + i6 + 1, i3, i2 + i6 + 1, i3 + 2);
                return;
            default:
                return;
        }
    }

    private static boolean c(int i2, int i3) {
        byte b = eb.m[i2];
        eb.cq();
        eb.c(b);
        switch (i3) {
            case 0:
                if (eb.ab[b] >= eb.ev[b][4]) {
                    return true;
                }
                break;
            case 1:
                if (eb.aq[b] >= eb.ev[b][3]) {
                    return true;
                }
                break;
        }
        return false;
    }

    private void d() {
        if (this.n == null) {
            this.n = c.g("/u/caidan");
        }
        if (this.o == null) {
            this.o = c.g("/u/yb");
        }
        if (this.v == null) {
            this.v = ee.O("/u/k");
        }
        if (this.A == null) {
            this.A = ee.O("/u/lv");
        }
        if (this.B == null) {
            this.B = ee.O("/u/ex");
        }
        if (J == null) {
            J = new ao[3];
            for (byte b = 0; b < 3; b = (byte) (b + 1)) {
                J[b] = ee.O(new StringBuffer().append("/u/e").append((int) b).toString());
            }
        }
    }

    public static void d(int i2) {
        if (bi.y(8)) {
            byte b = (byte) (h - 1);
            h = b;
            h = b < 0 ? 0 : h;
            P = (short) N[h];
            short s2 = (short) (l - 1);
            l = s2;
            l = s2 < 0 ? 0 : l;
            o();
        } else if (bi.y(4)) {
            byte b2 = (byte) (h + 1);
            h = b2;
            h = b2 > ((byte) i2) ? (byte) i2 : h;
            P = (short) N[h];
            if (al > 0) {
                short s3 = (short) (l + 1);
                l = s3;
                l = s3 > ((short) (al - 1)) ? (short) (al - 1) : l;
                U();
            }
        }
    }

    public static void e(int i2) {
        if (bi.y(8)) {
            if (am < 3) {
                if (l <= h) {
                    byte b = (byte) (h - 1);
                    h = b;
                    h = b < 0 ? 0 : h;
                    P = (short) N[h];
                }
                short s2 = (short) (l - 1);
                l = s2;
                l = s2 < 0 ? 0 : l;
                o();
                return;
            }
            byte b2 = (byte) (h - 1);
            h = b2;
            h = b2 < 0 ? 0 : h;
            P = (short) N[h];
            short s3 = (short) (l - 1);
            l = s3;
            l = s3 < 0 ? 0 : l;
            o();
        } else if (bi.y(4)) {
            byte b3 = (byte) (h + 1);
            h = b3;
            h = b3 > ((byte) i2) ? (byte) i2 : h;
            P = (short) N[h];
            if (al > 0) {
                short s4 = (short) (l + 1);
                l = s4;
                l = s4 > ((short) (al - 1)) ? (short) (al - 1) : l;
                U();
            }
        }
    }

    private void e(an anVar) {
        b(anVar, 0);
        b(anVar, g, 0);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= 2) {
                break;
            }
            b(anVar, (this.e < 2 || i3 != j) ? 11633501 : 9728372, (i3 * 55) + 80, 70, 50, 18);
            i2 = i3 + 1;
        }
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= 2) {
                break;
            }
            a(anVar, (this.e < 2 || i5 != j) ? 5849662 : 16777215, 4, i5 + 3, (i5 * 55) + 93, 73);
            i4 = i5 + 1;
        }
        b(anVar, 3548436, 65, 90, 137, 72);
        a(anVar, this.e < 3 ? 10914680 : 7755858, 10914680, 3, j == 1 ? eb.fz.length : 0, l, aj, 70, 95, 127, 20);
        if (j == 0) {
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i7 >= J.length) {
                    break;
                }
                du.a(anVar, J[i7], 0, 165, (i7 * 22) + 97, 0);
                i6 = i7 + 1;
            }
        }
        int i8 = aj;
        while (true) {
            int i9 = i8;
            if (i9 < aj + al && i9 <= am - 1) {
                int i10 = (this.e < 3 || i9 != h) ? 5849662 : 16777215;
                if (j == 1) {
                    byte b = N[i9];
                    if (O[i9] != 0) {
                        du.a(anVar, bj.gv[b], 115, ((i9 - aj) * 21) + 116, 33, i10);
                        du.a(anVar, new StringBuffer().append("X").append(String.valueOf((int) O[i9])).toString(), 170, ((i9 - aj) * 21) + 118, 33, i10);
                    } else {
                        du.a(anVar, "[空]", 130, ((i9 - aj) * 21) + 116, 33, i10);
                    }
                }
                if (j == 0) {
                    if (O[i9] != 0) {
                        du.a(anVar, bj.gv[N[i9]], 115, ((i9 - aj) * 21) + 116, 33, i10);
                    } else {
                        du.a(anVar, "[空]", 115, ((i9 - aj) * 21) + 116, 33, i10);
                    }
                }
                i8 = i9 + 1;
            }
        }
        a(anVar, 9071433, 11765343, 35, 164, 170, 75);
        if (O[h] != 0) {
            a(anVar, 120, 185);
        }
    }

    private void g(an anVar) {
        int i2;
        int i3;
        switch (this.ah) {
            case 0:
                b(anVar, 1);
                switch (eb.m[g]) {
                    case 0:
                        i2 = 42;
                        i3 = 5;
                        break;
                    case 1:
                        i2 = 50;
                        i3 = 0;
                        break;
                    case 2:
                        i2 = 41;
                        i3 = 0;
                        break;
                    default:
                        i2 = 0;
                        i3 = 0;
                        break;
                }
                this.p[eb.m[g]].a((120 - i2) - 0, i3 + 160);
                this.p[eb.m[g]].c(anVar);
                this.p[eb.m[g]].Q();
                byte b = g;
                anVar.setColor(3614226);
                anVar.c(136, 70, 50, 18);
                ao aoVar = this.v;
                int width = aoVar.getWidth();
                int height = aoVar.getHeight();
                int[] iArr = new int[126];
                int[] a = a(aoVar);
                for (int i4 = 0; i4 < 18; i4++) {
                    for (int i5 = 0; i5 < 7; i5++) {
                        iArr[(i4 * 7) + i5] = a[((i5 * width) / 7) + (((i4 * height) / 18) * width)];
                    }
                }
                ao a2 = ao.a(iArr, 7, 18, true);
                du.a(anVar, a2, 0, 130, 70, 0);
                du.a(anVar, a2, 2, 185, 70, 0);
                if (eb.m.length > 1) {
                    du.a(anVar, this.w, 0, 117, 72, 0);
                    du.a(anVar, this.w, 2, 195, 72, 0);
                }
                a(anVar, 16777215, 1, eb.m[b], 148, 73);
                anVar.setColor(3614226);
                anVar.c(136, 95, 50, 14);
                du.a(anVar, this.v, 0, 130, 95, 0);
                du.a(anVar, this.v, 2, 185, 95, 0);
                du.a(anVar, this.A, 0, 137, 97, 0);
                du.b(anVar, eb.O[eb.m[g]], 164, 99, 1, 0);
                b(anVar, 10914680, 115, 110, 90, 45);
                b(anVar, 120, 118, 28, 18, 10, eb.m[g]);
                du.a(anVar, this.B, 0, 85, 160, 0);
                byte b2 = eb.m[g];
                int i6 = eb.aI[b2];
                eb.cq();
                int w2 = eb.w(b2);
                du.a(anVar, bj.I[2], 60, 0, 5, 7, 0, 164, 158);
                du.b(anVar, i6, 164, 158, 2, 24);
                du.b(anVar, w2, 169, 158, 2, 20);
                anVar.setColor(0);
                anVar.b(112, 166, 95, 3);
                anVar.setColor(2817829);
                anVar.c(113, 167, (i6 * 94) / w2, 2);
                b(anVar, 6112574, 50, 175, 145, 55);
                byte b3 = 0;
                while (true) {
                    byte b4 = b3;
                    if (b4 >= 6) {
                        f(anVar);
                        return;
                    }
                    a(anVar, 16317367, 3, b4, ((b4 % 2) * 63) + 70, ((b4 / 2) * 17) + DirectGraphics.ROTATE_180);
                    int i7 = 0;
                    int i8 = 0;
                    switch (b4) {
                        case 0:
                            bg.aU();
                            i7 = bg.e(g, 0, 0, 1);
                            break;
                        case 1:
                            bg.aU();
                            i7 = bg.e(g, 0, 1, 1);
                            break;
                        case 2:
                            bg.aU();
                            i7 = bg.e(g, 1, 0, 2);
                            bg.aU();
                            i8 = bg.e(g, 1, 0, 3);
                            break;
                        case 3:
                            bg.aU();
                            i7 = bg.e(g, 1, 1, 2);
                            bg.aU();
                            i8 = bg.e(g, 1, 1, 3);
                            break;
                        case 4:
                            bg.aU();
                            i7 = bg.e(g, 2, 0, 2);
                            bg.aU();
                            i8 = bg.e(g, 2, 0, 3);
                            break;
                    }
                    du.b(anVar, eb.ev[eb.m[g]][this.L[b4]] + i8 + i7, ((b4 % 2) * 63) + 100, ((b4 / 2) * 17) + 183, 1, 0);
                    b3 = (byte) (b4 + 1);
                }
                break;
            case 1:
                c(anVar, g, N.length, 0);
                return;
            case 2:
                e(anVar);
                return;
            case 3:
                d(anVar);
                return;
            case 4:
                c(anVar);
                return;
            case 5:
                b(anVar);
                return;
            default:
                return;
        }
    }

    private void i(an anVar) {
        switch (k) {
            case 0:
                byte b = (byte) r.ev[P][1];
                if (b >= 3 || b <= 0) {
                    M().b(anVar, 3548436, 82, 135, 75, 72);
                    M().a(anVar, 7755858, 10914680, 3, 2, i, 0, 86, 140, 67, 20);
                    byte b2 = 0;
                    while (true) {
                        byte b3 = b2;
                        if (b3 < eb.m.length) {
                            a(anVar, b3 == i ? 16777215 : 5849662, 1, b3, 105, (b3 * 21) + MIDIControl.NOTE_ON);
                            b2 = (byte) (b3 + 1);
                        } else {
                            return;
                        }
                    }
                } else {
                    M().b(anVar, 3548436, 72, 127, 95, 72);
                    M().a(anVar, 7755858, 10914680, 3, 2, i, 0, 76, 132, 87, 20);
                    byte b4 = i;
                    byte b5 = 0;
                    while (true) {
                        byte b6 = b5;
                        if (b6 < eb.m.length) {
                            a(anVar, b6 == b4 ? 16777215 : 5849662, 1, b6, 80, (b6 * 21) + 136);
                            switch (b) {
                                case 1:
                                    b(anVar, b6, 0, 1);
                                    break;
                                case 2:
                                    b(anVar, b6, 1, 2);
                                    break;
                            }
                            b5 = (byte) (b6 + 1);
                        } else {
                            return;
                        }
                    }
                }
                break;
            case 1:
                b(anVar, 9923675, 70, 150, 100, 48);
                du.a(anVar, this.w, 0, 73, 165, 0);
                du.a(anVar, this.w, 2, 157, 165, 0);
                du.a(anVar, r.aC[N[h]], 120, 175, 33, 16777215);
                du.a(anVar, String.valueOf((int) j), 120, p.e + 175, 33, 16777215);
                return;
            default:
                return;
        }
    }

    private void j(an anVar) {
        byte b = 0;
        switch (j) {
            case 0:
                a(anVar, 6112574, 9923675, 85, 160, 75, 30);
                a(anVar, 0, 4, 0, 110, 170);
                return;
            case 1:
                M().b(anVar, 3548436, 90, 145, 75, 72);
                M().a(anVar, 7755858, 10914680, 3, 2, this.ap, 0, 94, 150, 67, 20);
                while (b < eb.m.length) {
                    a(anVar, b == this.ap ? 16777215 : 5849662, 1, eb.m[b], 115, (b * 20) + 156);
                    b = (byte) (b + 1);
                }
                return;
            default:
                return;
        }
    }

    private void k(an anVar) {
        String str;
        b(anVar, 10914680, 30, 68, DirectGraphics.ROTATE_180, 172);
        anVar.setColor(7755858);
        anVar.a(120, 70, 120, 237);
        a(anVar, 5849662, 1, eb.m[this.ap], 62, 80);
        a(anVar, 5849662, 4, 4, 152, 80);
        byte x = eb.x(eb.m[this.ap], this.T);
        if (x != -1) {
            str = bj.gv[x];
            this.aq[0] = bj.gw[x][4];
            this.aq[1] = bj.gw[x][5];
            this.aq[2] = bj.gw[x][6];
            this.aq[3] = bj.gw[x][7];
        } else {
            str = "无";
        }
        this.ar[0] = bj.gw[P][4];
        this.ar[1] = bj.gw[P][5];
        this.ar[2] = bj.gw[P][6];
        this.ar[3] = bj.gw[P][7];
        anVar.a(str, 75, 115, 33);
        anVar.a(bj.gv[P], 165, 115, 33);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 4) {
                if (x != -1) {
                    a(anVar, 5849662, 3, i3, 50, (i3 * 25) + 130);
                    du.b(anVar, this.aq[i3], 80, (i3 * 25) + 132, 2, 0);
                }
                a(anVar, 5849662, 3, i3, 140, (i3 * 25) + 130);
                du.b(anVar, this.ar[i3], 170, (i3 * 25) + 132, 2, 0);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void m(an anVar) {
        int i2;
        int i3;
        b(anVar, 0);
        f(anVar);
        byte b = 0;
        while (true) {
            byte b2 = b;
            if (b2 >= 2) {
                break;
            }
            if (g == b2) {
                i2 = 9728372;
                i3 = 16777215;
            } else {
                i2 = 11633501;
                i3 = 5849662;
            }
            b(anVar, i2, (b2 * ToneControl.C4) + 65, 68, 50, 22);
            a(anVar, i3, 5, b2, (b2 * ToneControl.C4) + 79, 73);
            b = (byte) (b2 + 1);
        }
        int i4 = this.as <= 0 ? 10914680 : 7755858;
        b(anVar, 3548436, 38, 92, 165, 72);
        a(anVar, i4, 10914680, 3, N.length, l, aj, 44, 97, 153, 20);
        int i5 = aj;
        while (true) {
            int i6 = i5;
            if (i6 < aj + al && i6 <= am - 1) {
                int i7 = this.as > 0 ? h == i6 ? 16777215 : 5849662 : 5849662;
                byte b3 = N[i6];
                String str = null;
                switch (g) {
                    case 0:
                        switch (this.at) {
                            case 1:
                                str = r.aC[b3];
                                break;
                            case 2:
                                str = bj.gv[b3];
                                break;
                        }
                        du.a(anVar, str, 78, ((i6 - aj) * 21) + 118, 33, i7);
                        du.a(anVar, String.valueOf((int) av[i6]), 140, ((i6 - aj) * 21) + 120, 33, i7);
                        break;
                    case 1:
                        if (O[i6] == 0) {
                            du.a(anVar, "空", 115, ((i6 - aj) * 21) + 118, 33, i7);
                            break;
                        } else {
                            switch (this.at) {
                                case 1:
                                    str = r.aC[b3];
                                    break;
                                case 2:
                                    str = bj.gv[b3];
                                    break;
                            }
                            du.a(anVar, str, 78, ((i6 - aj) * 21) + 118, 33, i7);
                            du.a(anVar, new StringBuffer().append("X").append(String.valueOf((int) O[i6])).toString(), 130, ((i6 - aj) * 21) + 119, 33, i7);
                            du.a(anVar, String.valueOf((int) av[i6]), 162, ((i6 - aj) * 21) + 118, 33, i7);
                            break;
                        }
                }
                i5 = i6 + 1;
            }
        }
        a(anVar, 9071433, 11765343, 34, 165, 170, 68);
        switch (g) {
            case 0:
                switch (this.at) {
                    case 1:
                        a(anVar, 2035968, r.aE[P], 38, 170, 150, 0);
                        return;
                    case 2:
                        a(anVar, 120, 186);
                        return;
                    default:
                        return;
                }
            case 1:
                switch (this.at) {
                    case 1:
                        if (O[h] != 0) {
                            a(anVar, 2035968, r.aE[P], 38, 170, 150, 0);
                            return;
                        }
                        return;
                    case 2:
                        if (O[h] != 0) {
                            a(anVar, 120, 186);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public static void o() {
        if (ak > 0) {
            byte b = (byte) (ak - 1);
            ak = b;
            if (b < aj) {
                aj = ak;
            }
        }
    }

    public final void J() {
        short s2;
        switch (this.as) {
            case 0:
                if (bi.y(2)) {
                    byte b = (byte) (g - 1);
                    g = b;
                    g = b < 0 ? 1 : g;
                    h = 0;
                    l = 0;
                    ak = 0;
                    aj = 0;
                    Y();
                    return;
                } else if (bi.y(1)) {
                    byte b2 = (byte) (g + 1);
                    g = b2;
                    g = b2 > 1 ? 0 : g;
                    h = 0;
                    l = 0;
                    ak = 0;
                    aj = 0;
                    Y();
                    return;
                } else if (bi.y(148)) {
                    this.as = (byte) 1;
                    return;
                } else if (bi.y(1280)) {
                    ea.cp().a(1);
                    return;
                } else {
                    return;
                }
            case 1:
                if (bi.y(2)) {
                    byte b3 = (byte) (g - 1);
                    g = b3;
                    g = b3 < 0 ? 1 : g;
                    h = 0;
                    l = 0;
                    ak = 0;
                    aj = 0;
                    Y();
                    return;
                } else if (bi.y(1)) {
                    byte b4 = (byte) (g + 1);
                    g = b4;
                    g = b4 > 1 ? 0 : g;
                    h = 0;
                    l = 0;
                    ak = 0;
                    aj = 0;
                    Y();
                    return;
                } else if (bi.y(8)) {
                    byte b5 = (byte) (h - 1);
                    h = b5;
                    if (b5 < 0) {
                        this.as = (byte) 0;
                        h = 0;
                    }
                    P = (short) N[h];
                    if (this.at == 2) {
                        O();
                    }
                    short s3 = (short) (l - 1);
                    l = s3;
                    l = s3 < 0 ? 0 : l;
                    o();
                    return;
                } else if (bi.y(4)) {
                    byte b6 = (byte) (h + 1);
                    h = b6;
                    h = b6 > ((byte) (N.length - 1)) ? (byte) (N.length - 1) : h;
                    P = (short) N[h];
                    if (this.at == 2) {
                        O();
                    }
                    if (al > 0) {
                        short s4 = (short) (l + 1);
                        l = s4;
                        l = s4 > ((short) (al - 1)) ? (short) (al - 1) : l;
                        U();
                        return;
                    }
                    return;
                } else if (bi.y(148)) {
                    switch (g) {
                        case 0:
                            k = 1;
                            this.as = (byte) 2;
                            return;
                        case 1:
                            if (O[h] > 0 && P < 36) {
                                k = 1;
                                this.as = (byte) 2;
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                } else if (bi.y(1280)) {
                    h = 0;
                    l = 0;
                    ak = 0;
                    aj = 0;
                    this.as = (byte) 0;
                    return;
                } else {
                    return;
                }
            case 2:
                if (bi.y(2)) {
                    short s5 = (short) (k - 1);
                    k = s5;
                    k = s5 <= 0 ? 1 : k;
                    return;
                } else if (bi.y(1)) {
                    switch (g) {
                        case 0:
                            s2 = (short) (k + 1);
                            break;
                        case 1:
                            short s6 = (short) (k + 1);
                            k = s6;
                            if (s6 <= O[h]) {
                                s2 = k;
                                break;
                            } else {
                                s2 = O[h];
                                break;
                            }
                        default:
                            return;
                    }
                    k = s2;
                    return;
                } else if (bi.y(MIDIControl.NOTE_ON)) {
                    switch (g) {
                        case 0:
                            if (eb.ac < av[h] * k) {
                                this.an = 2;
                                break;
                            } else {
                                switch (this.at) {
                                    case 1:
                                        eb.cq().k(N[h], k);
                                        break;
                                    case 2:
                                        eb.cq().h(N[h], k);
                                        break;
                                }
                                eb.ac -= av[h] * k;
                                this.an = 1;
                                break;
                            }
                        case 1:
                            switch (this.at) {
                                case 1:
                                    if (r.ev[N[h]][5] == -1) {
                                        this.an = 4;
                                        break;
                                    } else {
                                        eb.cq().y(N[h], k);
                                        this.an = 3;
                                        break;
                                    }
                                case 2:
                                    eb.cq().j(N[h], k);
                                    this.an = 3;
                                    break;
                            }
                            if (this.an == 3) {
                                eb.ac += av[h] * k;
                                break;
                            }
                            break;
                    }
                    k = 0;
                    this.as = (byte) 3;
                    return;
                } else if (bi.y(1280)) {
                    k = 0;
                    this.as = (byte) 1;
                    return;
                } else {
                    return;
                }
            case 3:
                this.ao = (short) (this.ao + 1);
                if (this.ao > 25) {
                    this.ao = 0;
                    byte b7 = aj;
                    byte b8 = ak;
                    Y();
                    aj = b7;
                    ak = b8;
                    P = (short) N[ak];
                    if (this.at == 2) {
                        O();
                    }
                    ea.cp().ae = false;
                    this.as = (byte) 1;
                    if (this.an == 2) {
                        b.Z().a(1);
                        b.Z().b(b.Z().L, 2);
                        ea.cp().a(7);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void N() {
        S();
        this.e = 0;
        this.M = true;
        this.ae = false;
    }

    public final void Q() {
        short s2;
        byte b;
        byte b2;
        byte b3;
        if (bi.ac != 0) {
            this.M = true;
        }
        switch (this.e) {
            case 0:
                if (bi.y(2)) {
                    byte b4 = (byte) (f - 1);
                    f = b4;
                    f = b4 < 0 ? (byte) (aa - 1) : f;
                    return;
                } else if (bi.y(1)) {
                    byte b5 = (byte) (f + 1);
                    f = b5;
                    f = b5 > aa - 1 ? 0 : f;
                    return;
                } else if (bi.y(MIDIControl.NOTE_ON)) {
                    if (f == 2) {
                        h = 0;
                        l = 0;
                        j = 0;
                    }
                    b(1);
                    this.ah = f;
                    ea.cp().dZ = true;
                    V();
                    return;
                } else if (bi.y(1280)) {
                    ea.cp().a(1);
                    return;
                } else {
                    return;
                }
            case 1:
                switch (this.ah) {
                    case 0:
                        T();
                        if (bi.y(1280)) {
                            b(0);
                            return;
                        }
                        return;
                    case 1:
                        T();
                        e(N.length - 1);
                        eb.a(eb.m[g], P);
                        if (bi.y(1280)) {
                            this.af = 0;
                            b(0);
                            return;
                        } else if (!bi.y(MIDIControl.NOTE_ON)) {
                            return;
                        } else {
                            if (am < 3) {
                                switch (am) {
                                    case 1:
                                        if (l == 0) {
                                            a(g, 0);
                                            return;
                                        }
                                        return;
                                    case 2:
                                        if (l < 2) {
                                            a(g, 0);
                                            return;
                                        }
                                        return;
                                    default:
                                        return;
                                }
                            } else {
                                a(g, 0);
                                return;
                            }
                        }
                    case 2:
                        T();
                        if (bi.y(1280)) {
                            b(0);
                            return;
                        } else if (!bi.y(148)) {
                            return;
                        }
                        break;
                    case 3:
                        d(N.length - 1);
                        if (bi.y(1280)) {
                            b(0);
                            return;
                        } else if (bi.y(MIDIControl.NOTE_ON) && O[h] != 0) {
                            byte b6 = (byte) r.ev[P][5];
                            Z = b6;
                            if (b6 == -1) {
                                return;
                            }
                        } else {
                            return;
                        }
                        break;
                    case 4:
                        if (bi.y(1280)) {
                            b(0);
                            return;
                        }
                        return;
                    case 5:
                        if (bi.y(8)) {
                            byte b7 = (byte) (h - 1);
                            h = b7;
                            h = b7 < 0 ? 3 : h;
                            return;
                        } else if (bi.y(4)) {
                            byte b8 = (byte) (h + 1);
                            h = b8;
                            h = b8 > 3 ? 0 : h;
                            return;
                        } else if (bi.y(1280)) {
                            b(0);
                            return;
                        } else if (bi.y(MIDIControl.NOTE_ON)) {
                            if (h != 0 && h == 2) {
                                o.an().a(3);
                                break;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
                b(2);
                return;
            case 2:
                switch (this.ah) {
                    case 1:
                        if (bi.y(1280)) {
                            b(1);
                            return;
                        } else if (bi.y(MIDIControl.NOTE_ON)) {
                            b(3);
                            return;
                        } else {
                            return;
                        }
                    case 2:
                        if (bi.y(2)) {
                            byte b9 = (byte) (j - 1);
                            j = b9;
                            j = b9 < 0 ? 1 : j;
                            h = 0;
                            l = 0;
                            V();
                        } else if (bi.y(1)) {
                            byte b10 = (byte) (j + 1);
                            j = b10;
                            j = b10 > 1 ? 0 : j;
                            h = 0;
                            l = 0;
                            V();
                        }
                        if (bi.y(1288)) {
                            b(1);
                            h = 0;
                            l = 0;
                            return;
                        } else if (bi.y(148)) {
                            b(3);
                            return;
                        } else {
                            return;
                        }
                    case 3:
                        if (bi.y(8)) {
                            short s3 = (short) (k - 1);
                            k = s3;
                            k = s3 < 0 ? 1 : k;
                            return;
                        } else if (bi.y(4)) {
                            short s4 = (short) (k + 1);
                            k = s4;
                            k = s4 > 1 ? 0 : k;
                            return;
                        } else if (bi.y(1280)) {
                            byte b11 = ak;
                            byte b12 = aj;
                            b(1);
                            ak = b11;
                            aj = b12;
                            return;
                        } else if (bi.y(MIDIControl.NOTE_ON)) {
                            switch (k) {
                                case 0:
                                    byte b13 = (byte) r.ev[P][3];
                                    X = b13;
                                    if (b13 == 0) {
                                        W = (byte) r.ev[P][4];
                                        Y = (byte) r.ev[P][1];
                                        if (W != 0) {
                                            if (W == 1) {
                                                b(i);
                                                b(4);
                                                return;
                                            }
                                            return;
                                        }
                                    } else {
                                        switch (k) {
                                            case 0:
                                                this.af = 20;
                                                b(4);
                                                return;
                                            case 1:
                                                break;
                                            default:
                                                return;
                                        }
                                    }
                                    b(3);
                                    return;
                                case 1:
                                    j = 1;
                                    b(3);
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    case 4:
                    default:
                        return;
                    case 5:
                        switch (h) {
                            case 0:
                                if (!ea.cp().ae) {
                                    if (bi.y(8)) {
                                        short s5 = (short) (l - 1);
                                        l = s5;
                                        l = s5 < 0 ? 2 : l;
                                    } else if (bi.y(4)) {
                                        short s6 = (short) (l + 1);
                                        l = s6;
                                        l = s6 > 2 ? 0 : l;
                                    } else if (bi.y(MIDIControl.NOTE_ON)) {
                                        if (bj.gz[l][1] == 0) {
                                            String str = "";
                                            switch (l) {
                                                case 0:
                                                    str = "mn2_l1";
                                                    break;
                                                case 1:
                                                    str = "mn2_l2";
                                                    break;
                                                case 2:
                                                    str = "mn2_l3";
                                                    break;
                                            }
                                            if (bj.bp().u(str)) {
                                                ed.cB().a(5, "存档成功!".toCharArray());
                                            } else {
                                                ed.cB().a(5, "存档失败!".toCharArray());
                                            }
                                            k = 0;
                                        } else {
                                            ed.cB().a(5, "是否覆盖存档？".toCharArray());
                                            k = 1;
                                        }
                                    }
                                    if (bi.y(1280)) {
                                        b(1);
                                        return;
                                    }
                                    return;
                                }
                                switch (k) {
                                    case 0:
                                        if (bi.y(MIDIControl.NOTE_ON)) {
                                            ea.cp().ae = false;
                                            return;
                                        }
                                        return;
                                    case 1:
                                        if (bi.y(MIDIControl.NOTE_ON)) {
                                            String str2 = "";
                                            switch (l) {
                                                case 0:
                                                    str2 = "mn2_l1";
                                                    break;
                                                case 1:
                                                    str2 = "mn2_l2";
                                                    break;
                                                case 2:
                                                    str2 = "mn2_l3";
                                                    break;
                                            }
                                            if (bj.bp().u(str2)) {
                                                ed.cB().a(5, "存档成功!".toCharArray());
                                            } else {
                                                ed.cB().a(5, "存档失败!".toCharArray());
                                            }
                                            k = 0;
                                            return;
                                        } else if (bi.y(1280)) {
                                            ea.cp().ae = false;
                                            return;
                                        } else {
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            case 1:
                                if (bi.y(1280)) {
                                    b(1);
                                }
                                R();
                                return;
                            case 2:
                                if (bi.y(1280)) {
                                    b(1);
                                }
                                o.an().R();
                                return;
                            case 3:
                                if (!ea.cp().ae) {
                                    ed.cB().a(5, "是否退出游戏？".toCharArray());
                                    return;
                                } else if (bi.y(MIDIControl.NOTE_ON)) {
                                    o.an().P();
                                    this.ae = true;
                                    b(3);
                                    ea.cp().ae = false;
                                    return;
                                } else if (bi.y(1280)) {
                                    ea.cp().ae = false;
                                    b(1);
                                    return;
                                } else {
                                    return;
                                }
                            default:
                                return;
                        }
                }
            case 3:
                switch (this.ah) {
                    case 1:
                        this.ao = (short) (this.ao + 1);
                        if (this.ao > 25) {
                            if (this.af < 10) {
                                for (int i2 = 0; i2 < eb.m.length; i2++) {
                                    short[] sArr = eb.ab;
                                    byte b14 = eb.m[i2];
                                    sArr[b14] = (short) (sArr[b14] + this.ab[i2]);
                                    if (eb.ab[eb.m[i2]] >= eb.ev[eb.m[i2]][4]) {
                                        eb.ab[eb.m[i2]] = eb.ev[eb.m[i2]][4];
                                    }
                                }
                                byte[][] bArr = bj.eq;
                                byte b15 = eb.m[g];
                                short[] sArr2 = eb.aq;
                                byte b16 = eb.m[g];
                                sArr2[b16] = (short) (sArr2[b16] - this.ad);
                                if (eb.e(b15, bArr[b15][h], 0) < eb.e) {
                                    eb.b(b15, bArr[b15][h], 1);
                                    if (eb.ai[b15][1][h] >= eb.ar[5]) {
                                        eb.b(b15, bArr[b15][h], 0);
                                        eb.g(b15, bArr[b15][h]);
                                    }
                                }
                                byte b17 = ak;
                                byte b18 = aj;
                                byte b19 = h;
                                short s7 = l;
                                V();
                                short s8 = s7;
                                b3 = b17;
                                s2 = s8;
                                byte b20 = b18;
                                b = b19;
                                b2 = b20;
                            } else {
                                s2 = 0;
                                b = 0;
                                b2 = 0;
                                b3 = 0;
                            }
                            this.ao = 0;
                            ea.cp().ae = false;
                            b(1);
                            if (this.af < 10) {
                                ak = b3;
                                aj = b2;
                                h = b;
                                l = s2;
                                P = (short) N[ak];
                                eb.a(eb.m[g], P);
                            }
                            this.af = 0;
                            return;
                        }
                        return;
                    case 2:
                        if (bi.y(2)) {
                            byte b21 = (byte) (j - 1);
                            j = b21;
                            j = b21 < 0 ? 1 : j;
                            h = 0;
                            l = 0;
                            V();
                            return;
                        } else if (bi.y(1)) {
                            byte b22 = (byte) (j + 1);
                            j = b22;
                            j = b22 > 1 ? 0 : j;
                            h = 0;
                            l = 0;
                            V();
                            return;
                        } else if (bi.y(8)) {
                            byte b23 = (byte) (h - 1);
                            h = b23;
                            if (b23 < 0) {
                                b(2);
                                h = 0;
                            }
                            P = (short) N[h];
                            O();
                            short s9 = (short) (l - 1);
                            l = s9;
                            l = s9 < 0 ? 0 : l;
                            o();
                            return;
                        } else if (bi.y(4)) {
                            byte b24 = (byte) (h + 1);
                            h = b24;
                            h = b24 > ((byte) (N.length - 1)) ? (byte) (N.length - 1) : h;
                            P = (short) N[h];
                            O();
                            if (al > 0) {
                                short s10 = (short) (l + 1);
                                l = s10;
                                l = s10 > ((short) (al - 1)) ? (short) (al - 1) : l;
                                U();
                                return;
                            }
                            return;
                        } else if (bi.y(1280)) {
                            b(2);
                            return;
                        } else if (bi.y(MIDIControl.NOTE_ON) && O[h] != 0) {
                            this.ap = 0;
                            b(4);
                            return;
                        } else {
                            return;
                        }
                    case 3:
                        switch (k) {
                            case 0:
                                byte length = (byte) (eb.m.length - 1);
                                if (bi.y(8)) {
                                    byte b25 = (byte) (i - 1);
                                    i = b25;
                                    if (b25 >= 0) {
                                        length = i;
                                    }
                                    i = length;
                                } else if (bi.y(4)) {
                                    byte b26 = (byte) (i + 1);
                                    i = b26;
                                    i = b26 > length ? 0 : i;
                                }
                                if (bi.y(1280)) {
                                    b(2);
                                    return;
                                } else if (bi.y(MIDIControl.NOTE_ON)) {
                                    b(i);
                                    b(4);
                                    return;
                                } else {
                                    return;
                                }
                            case 1:
                                if (bi.y(2)) {
                                    byte b27 = (byte) (j - 1);
                                    j = b27;
                                    j = b27 <= 0 ? 1 : j;
                                } else if (bi.y(1)) {
                                    byte b28 = (byte) (j + 1);
                                    j = b28;
                                    j = b28 > ((byte) O[h]) ? (byte) O[h] : j;
                                }
                                if (bi.y(1280)) {
                                    b(2);
                                    j = 0;
                                    return;
                                } else if (bi.y(MIDIControl.NOTE_ON)) {
                                    eb.cq().y(N[h], j);
                                    this.af = 21;
                                    b(4);
                                    return;
                                } else {
                                    return;
                                }
                            default:
                                return;
                        }
                    case 4:
                    default:
                        return;
                    case 5:
                        bh.c();
                        bh.a(0);
                        bi.bo().gp = o.an();
                        this.ah = 0;
                        b(0);
                        return;
                }
            case 4:
                switch (this.ah) {
                    case 2:
                        if (bi.y(1280)) {
                            b(3);
                        }
                        switch (j) {
                            case 0:
                                if (bi.y(MIDIControl.NOTE_ON)) {
                                    eb.cq().i(eb.m[g], h);
                                    V();
                                    this.an = 2;
                                    b(5);
                                    return;
                                }
                                return;
                            case 1:
                                if (bi.y(4)) {
                                    if (eb.m.length > 0) {
                                        byte b29 = (byte) (this.ap + 1);
                                        this.ap = b29;
                                        this.ap = b29 > eb.m.length - 1 ? 0 : this.ap;
                                        return;
                                    }
                                    return;
                                } else if (bi.y(8)) {
                                    if (eb.m.length > 0) {
                                        byte b30 = (byte) (this.ap - 1);
                                        this.ap = b30;
                                        this.ap = b30 < 0 ? (byte) (eb.m.length - 1) : this.ap;
                                        return;
                                    }
                                    return;
                                } else if (bi.y(MIDIControl.NOTE_ON)) {
                                    if (this.R == eb.m[this.ap]) {
                                        b(6);
                                        return;
                                    }
                                    this.an = 4;
                                    b(5);
                                    return;
                                } else {
                                    return;
                                }
                            default:
                                return;
                        }
                    case 3:
                        this.ao = (short) (this.ao + 1);
                        if (this.ao > 25) {
                            byte b31 = ak;
                            byte b32 = aj;
                            if (this.af < 10) {
                                eb.cq().y(N[h], 1);
                                a(i);
                                V();
                            }
                            this.ao = 0;
                            ea.cp().ae = false;
                            if (this.af == 21) {
                                V();
                            }
                            b(1);
                            ak = b31;
                            aj = b32;
                            this.af = 0;
                            P = (short) N[ak];
                            return;
                        }
                        return;
                    default:
                        return;
                }
            case 5:
                this.ao = (short) (this.ao + 1);
                if (this.ao > 25) {
                    this.ao = 0;
                    ea.cp().ae = false;
                    switch (this.ah) {
                        case 2:
                            b(3);
                            return;
                        default:
                            b(1);
                            return;
                    }
                } else {
                    return;
                }
            case 6:
                if (bi.y(1280)) {
                    b(4);
                }
                if (bi.y(MIDIControl.NOTE_ON)) {
                    b(7);
                    return;
                }
                return;
            case 7:
                if (bi.y(1280)) {
                    b(6);
                }
                if (bi.y(MIDIControl.NOTE_ON)) {
                    byte b33 = eb.m[this.ap];
                    if (this.R != b33) {
                        this.an = 4;
                    } else if (eb.O[b33] >= this.S) {
                        eb.cq().f(b33, P, this.T, h);
                        byte b34 = ak;
                        byte b35 = aj;
                        V();
                        ak = b34;
                        aj = b35;
                        P = (short) N[h];
                        O();
                        this.an = 1;
                    } else {
                        this.an = 5;
                    }
                    b(5);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void S() {
        this.e = 0;
        f = 0;
        g = 0;
        h = 0;
        l = 0;
        k = 0;
        i = 0;
        j = 0;
    }

    public final void Y() {
        al = 3;
        switch (g) {
            case 0:
                switch (this.at) {
                    case 1:
                        byte[] bArr = bj.es[this.au];
                        N = bArr;
                        av = new short[bArr.length];
                        for (int i2 = 0; i2 < av.length; i2++) {
                            av[i2] = r.ev[N[i2]][6];
                        }
                        break;
                    case 2:
                        byte[] bArr2 = bj.et[this.au];
                        N = bArr2;
                        av = new short[bArr2.length];
                        for (int i3 = 0; i3 < av.length; i3++) {
                            av[i3] = bj.gw[N[i3]][10];
                        }
                        break;
                }
            case 1:
                switch (this.at) {
                    case 1:
                        eb.cq();
                        N = eb.cs();
                        eb.cq();
                        O = eb.ct();
                        av = new short[N.length];
                        for (int i4 = 0; i4 < av.length; i4++) {
                            av[i4] = r.ev[N[i4]][7];
                        }
                        break;
                    case 2:
                        eb.cq();
                        N = eb.bq();
                        eb.cq();
                        O = eb.cr();
                        av = new short[N.length];
                        for (int i5 = 0; i5 < av.length; i5++) {
                            av[i5] = bj.gw[N[i5]][11];
                        }
                        break;
                }
        }
        P = (short) N[0];
        if (this.at == 2) {
            O();
        }
        am = (byte) N.length;
    }

    public final void a(byte b) {
        byte b2 = eb.m[b];
        eb.cq();
        eb.c(b2);
        switch (Y) {
            case 1:
                short[] sArr = eb.ab;
                sArr[b2] = (short) (sArr[b2] + this.ag);
                if (eb.ab[b2] >= eb.ev[b2][4]) {
                    eb.ab[b2] = eb.ev[b2][4];
                    return;
                }
                return;
            case 2:
                short[] sArr2 = eb.aq;
                sArr2[b2] = (short) (sArr2[b2] + this.ag);
                if (eb.aq[b2] >= eb.ev[b2][3]) {
                    eb.aq[b2] = eb.ev[b2][3];
                    return;
                }
                return;
            case 3:
                for (int i2 = 0; i2 < eb.m.length; i2++) {
                    eb.ab[eb.m[i2]] = eb.ev[eb.m[i2]][4];
                    eb.aq[eb.m[i2]] = eb.ev[eb.m[i2]][3];
                }
                return;
            case 4:
                bg.aU().eq[b2][1] = 0;
                return;
            case 5:
                for (byte b3 : eb.m) {
                    for (int i3 = 1; i3 < bg.aU().eq[b3].length; i3++) {
                        bg.aU().eq[b3][i3] = 0;
                    }
                }
                return;
            case 6:
                X();
                return;
            case 7:
                for (int i4 = 0; i4 < bg.aU().eq[b2].length; i4++) {
                    bg.aU().eq[b2][i4] = 0;
                }
                if (N[h] == 41) {
                    eb.ab[b2] = (short) ((eb.ev[b2][4] * 20) / 100);
                    return;
                } else {
                    eb.ab[b2] = eb.ev[b2][4];
                    return;
                }
            case 8:
            default:
                return;
            case 9:
                bg.aU().eq[b2][2] = 0;
                return;
            case 10:
                bg.aU().eq[b2][3] = 0;
                return;
            case 11:
                bg.aU().eq[b2][4] = 0;
                return;
        }
    }

    public final void a(an anVar) {
        int i2;
        String str;
        if (this.M) {
            switch (this.e) {
                case 0:
                    if (eb.M) {
                        bj.gr.a(anVar);
                    }
                    du.a(anVar, this.u, 0, 38, 255, 0);
                    du.a(anVar, this.u, 2, this.u.getWidth() + 38, 255, 0);
                    byte b = 0;
                    while (true) {
                        byte b2 = b;
                        if (b2 >= 6) {
                            break;
                        } else {
                            a(anVar, b2 == f ? 3613970 : 16115157, 0, b2, (b2 * 20) + 66, 274);
                            b = (byte) (b2 + 1);
                        }
                    }
                case 1:
                    g(anVar);
                    break;
                case 2:
                    g(anVar);
                    switch (this.ah) {
                        case 1:
                            a(anVar, 6112574, 9923675, 85, 150, 75, 30);
                            a(anVar, 0, 4, 1, 110, 160);
                            break;
                        case 2:
                            e(anVar);
                            break;
                        case 3:
                            M().b(anVar, 3548436, 82, 150, 75, 52);
                            M().a(anVar, 7755858, 10914680, 2, 1, k, 0, 86, 155, 67, 20);
                            byte b3 = 0;
                            while (true) {
                                byte b4 = b3;
                                if (b4 >= 2) {
                                    break;
                                } else {
                                    a(anVar, b4 == k ? 16777215 : 5849662, 4, b4 + 1, 105, (b4 * 20) + 160);
                                    b3 = (byte) (b4 + 1);
                                }
                            }
                        case 5:
                            if (h == 1 || h == 2) {
                                b(anVar, 1);
                            } else {
                                b(anVar, 0);
                            }
                            switch (h) {
                                case 0:
                                    a(anVar, l);
                                    break;
                                case 1:
                                    h(anVar);
                                    break;
                                case 2:
                                    o.an().a(o.an().aC, anVar, 20);
                                    break;
                                case 3:
                                    b(anVar);
                                    break;
                            }
                    }
                    break;
                case 3:
                    g(anVar);
                    switch (this.ah) {
                        case 1:
                            String str2 = null;
                            switch (this.af) {
                                case 1:
                                    str2 = new StringBuffer().append("所有人生命值+").append(this.ac).append("%").toString();
                                    break;
                                case 10:
                                    str2 = "所有人的生命值已满";
                                    break;
                                case 11:
                                    str2 = "真气值不够";
                                    break;
                            }
                            if (str2 != null) {
                                ed.cB().a(5, str2.toCharArray());
                                break;
                            }
                            break;
                        case 2:
                            e(anVar);
                            break;
                        case 3:
                            i(anVar);
                            break;
                        case 5:
                            anVar.setColor(0);
                            anVar.c(0, 0, 240, 320);
                            break;
                    }
                case 4:
                    g(anVar);
                    switch (this.ah) {
                        case 2:
                            j(anVar);
                            break;
                        case 3:
                            c(i);
                            break;
                    }
                case 5:
                    g(anVar);
                    switch (this.an) {
                        case 1:
                            i2 = 5;
                            str = "装备成功!";
                            break;
                        case 2:
                            i2 = 5;
                            str = "卸载成功!";
                            break;
                        case 3:
                            i2 = 5;
                            str = "丢弃成功!";
                            break;
                        case 4:
                            i2 = 5;
                            str = "不是专属装备!";
                            break;
                        case 5:
                            i2 = 5;
                            str = "等级不够!";
                            break;
                        default:
                            i2 = 0;
                            str = null;
                            break;
                    }
                    if (str != null) {
                        ed.cB().a(i2, str.toCharArray());
                        break;
                    }
                    break;
                case 6:
                    k(anVar);
                    break;
                case 7:
                    a(anVar, 6112574, 9923675, 85, 145, 75, 30);
                    a(anVar, 0, 4, 3, 110, 155);
                    break;
            }
            this.M = false;
        }
    }

    public final void a(an anVar, int i2) {
        b(anVar, 3548436, 50, 85, 140, 150);
        a(anVar, 7755858, 10914680, 3, 1, i2, 0, 54, 90, 132, 46);
        byte b = 0;
        while (true) {
            byte b2 = b;
            if (b2 < 3) {
                int i3 = i2 == b2 ? 16777215 : 5849662;
                if (bj.gz[b2][1] == 0) {
                    du.a(anVar, "<空>", 120, (b2 * 50) + 115, 33, i3);
                } else {
                    a(anVar, i3, 1, bj.gz[b2][0], 80, (b2 * 50) + 92);
                    a(anVar, i3, 3, 8, 130, (b2 * 50) + 92);
                    du.b(anVar, bj.gz[b2][1], 160, (b2 * 50) + 93, 1, 0);
                    du.a(anVar, dz.aE[bj.gz[b2][2]], 80, (b2 * 48) + 110, 0, i3);
                }
                b = (byte) (b2 + 1);
            } else {
                return;
            }
        }
    }

    public final void a(an anVar, int i2, int i3, int i4, int i5, int i6) {
        switch (i3) {
            case 0:
                anVar.setColor(i2);
                for (byte b = 0; b < this.ai[i3][i4].length; b = (byte) (b + 1)) {
                    a(anVar, this.ai[i3][i4][b], i5, (b * 13) + i6);
                }
                return;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                anVar.setColor(i2);
                for (byte b2 = 0; b2 < this.ai[i3][i4].length; b2 = (byte) (b2 + 1)) {
                    a(anVar, this.ai[i3][i4][b2], (b2 * 13) + i5, i6);
                }
                return;
            case 6:
                anVar.setColor(16777215);
                for (byte b3 = 0; b3 < this.ai[1][i4].length; b3 = (byte) (b3 + 1)) {
                    a(anVar, this.ai[1][i4][b3], i5, (b3 * 13) + i6);
                }
                return;
            default:
                return;
        }
    }

    public final void a(an anVar, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11) {
        int i12 = 0;
        while (true) {
            int i13 = i12;
            if (i13 < i4) {
                if (i13 == i6) {
                    anVar.setColor(i2);
                } else {
                    anVar.setColor(10914680);
                }
                anVar.c(i8, ((i11 + 1) * i13) + i9, i10, i11);
                if (i5 > i4 && i7 < i5 - 3) {
                    an anVar2 = anVar;
                    du.a(anVar2, this.D, 0, (i8 + i10) - 5, ((i4 - 1) * i11) + i9 + 22, 33);
                }
                if (i7 > 0 && i5 > i4) {
                    du.a(anVar, this.D, 1, (i8 + i10) - 5, i9 + 20, 33);
                }
                i12 = i13 + 1;
            } else {
                return;
            }
        }
    }

    public final void b(byte b) {
        boolean z;
        boolean z2;
        switch (Y) {
            case 1:
                if (eb.ab[eb.m[b]] <= 0) {
                    this.af = 22;
                    return;
                } else if (!c(b, 0)) {
                    this.af = 1;
                    this.ag = r.ev[P][2];
                    return;
                } else {
                    this.af = 11;
                    return;
                }
            case 2:
                if (eb.ab[eb.m[b]] <= 0) {
                    this.af = 22;
                    return;
                } else if (!c(b, 1)) {
                    this.af = 2;
                    this.ag = r.ev[P][2];
                    return;
                } else {
                    this.af = 12;
                    return;
                }
            case 3:
                int i2 = 0;
                while (true) {
                    if (i2 >= eb.m.length) {
                        z2 = false;
                    } else {
                        byte b2 = eb.m[i2];
                        eb.cq();
                        eb.c(b2);
                        if (eb.ab[b2] < eb.ev[b2][4] || eb.aq[b2] < eb.ev[b2][3]) {
                            z2 = true;
                        } else {
                            i2++;
                        }
                    }
                }
                z2 = true;
                if (z2) {
                    this.af = 3;
                    this.ag = r.ev[P][2];
                    return;
                }
                this.af = 13;
                return;
            case 4:
                if (eb.ab[eb.m[b]] <= 0) {
                    this.af = 22;
                    return;
                } else if (b(b, 0)) {
                    this.af = 4;
                    return;
                } else {
                    this.af = 14;
                    return;
                }
            case 5:
                int i3 = 0;
                while (true) {
                    if (i3 >= eb.m.length) {
                        z = false;
                    } else {
                        byte b3 = eb.m[i3];
                        int i4 = 1;
                        while (i4 < bg.aU().eq[b3].length) {
                            if (bg.aU().eq[b3][i4] != -1) {
                                z = true;
                            } else {
                                i4++;
                            }
                        }
                        i3++;
                    }
                }
                if (!z) {
                    this.af = 5;
                    return;
                } else {
                    this.af = 15;
                    return;
                }
            case 6:
                if (W()) {
                    this.af = 6;
                    return;
                } else {
                    this.af = 16;
                    return;
                }
            case 7:
                if (bg.aU().eq[eb.m[b]][0] == -1) {
                    this.af = 7;
                    return;
                } else {
                    this.af = 17;
                    return;
                }
            case 8:
            default:
                return;
            case 9:
                if (eb.ab[eb.m[b]] <= 0) {
                    this.af = 22;
                    return;
                } else if (b(b, 2)) {
                    this.af = 9;
                    return;
                } else {
                    this.af = 19;
                    return;
                }
            case 10:
                if (eb.ab[eb.m[b]] <= 0) {
                    this.af = 22;
                    return;
                } else if (b(b, 3)) {
                    this.af = 10;
                    return;
                } else {
                    this.af = 30;
                    return;
                }
            case 11:
                if (eb.ab[eb.m[b]] <= 0) {
                    this.af = 22;
                    return;
                } else if (b(b, 1)) {
                    this.af = 8;
                    return;
                } else {
                    this.af = 18;
                    return;
                }
        }
    }

    public final void b(an anVar, int i2) {
        du.a(anVar, this.s, 0, 120, (this.s.getHeight() / 2) + 160, 33);
        du.a(anVar, this.q, 0, (120 - (this.s.getWidth() / 4)) - 8, (160 - (this.s.getHeight() / 2)) + 2, 33);
        du.a(anVar, this.q, 2, 117, ((160 - (this.s.getHeight() / 2)) - this.q.getHeight()) + 2, 20);
        du.a(anVar, this.q, 0, (120 - (this.s.getWidth() / 4)) - 8, (((this.s.getHeight() / 2) + 160) + this.q.getHeight()) - 2, 33);
        du.a(anVar, this.q, 2, 117, ((this.s.getHeight() / 2) + 160) - 2, 20);
        du.a(anVar, this.r, 0, (120 - this.q.getWidth()) - 3, (160 - (this.s.getHeight() / 2)) - 22, 20);
        du.a(anVar, this.r, 2, (this.q.getWidth() + 120) - 15, (160 - (this.s.getHeight() / 2)) - 22, 20);
        if (i2 == 0) {
            for (int i3 = 0; i3 < 2; i3++) {
                du.a(anVar, I[i3], 0, (i3 * 138) + 44, 240, 0);
            }
            return;
        }
        du.a(anVar, I[1], 0, 182, 240, 0);
    }

    public final void b(an anVar, int i2, int i3, int i4, int i5, int i6) {
        anVar.setColor(i2);
        anVar.c(i3 + 2, i4 + 2, i5 - 5, i6 - 5);
        anVar.setColor(9665379);
        anVar.b(i3, i4, i5 - 1, i6 - 1);
        anVar.setColor(7755858);
        anVar.b(i3 + 2, i4 + 2, i5 - 5, i6 - 5);
        if (this.C != null) {
            du.a(anVar, this.C, 0, i3, i4, 0);
            an anVar2 = anVar;
            du.a(anVar2, this.C, 1, i3, (i4 + i6) - 6, 0);
            an anVar3 = anVar;
            du.a(anVar3, this.C, 2, (i3 + i5) - 8, i4, 0);
            an anVar4 = anVar;
            du.a(anVar4, this.C, 3, (i3 + i5) - 8, (i4 + i6) - 6, 0);
        }
    }

    public final void b(an anVar, int i2, int i3, int i4, int i5, int i6, int i7) {
        byte b = 0;
        while (true) {
            byte b2 = b;
            if (b2 >= 2) {
                break;
            }
            a(anVar, 16317367, 3, b2 + 6, i2, i3 + (i5 * b2));
            b = (byte) (b2 + 1);
        }
        eb.cq();
        int f2 = eb.f(i7, 0, 0);
        eb.cq();
        int f3 = eb.f(i7, 1, 0);
        eb.cq();
        int f4 = eb.f(i7, 0, 1);
        eb.cq();
        int i8 = (f2 * 50) / f3;
        int f5 = (f4 * 50) / eb.f(i7, 1, 1);
        c(anVar, i2 + i4, i3 + 10, i8, f5, 50, 0);
        int i9 = 0;
        while (true) {
            int i10 = i9;
            if (i10 < 2) {
                eb.cq();
                int f6 = eb.f(i7, 0, i10);
                eb.cq();
                int f7 = eb.f(i7, 1, i10);
                an anVar2 = anVar;
                du.a(anVar2, bj.I[2], 60, 0, 5, 7, 0, i2 + 47, i3 + (i10 * 18));
                an anVar3 = anVar;
                int i11 = f6;
                du.b(anVar3, i11, i2 + 47, i3 + (i10 * 18), 2, 24);
                an anVar4 = anVar;
                int i12 = f7;
                du.b(anVar4, i12, i2 + 52, i3 + (i10 * 18), 2, 20);
                i9 = i10 + 1;
            } else {
                return;
            }
        }
    }

    public final void c() {
        if (this.q == null) {
            this.q = ee.O("/u/zhou");
        }
        if (this.r == null) {
            this.r = ee.O("/u/zhui");
        }
        if (this.s == null) {
            this.s = ee.O("/u/back");
        }
        if (this.u == null) {
            this.u = ee.O("/u/u");
        }
        if (this.F == null) {
            this.F = ee.O("/u/f");
        }
        if (this.p == null) {
            this.p = new n[3];
            for (byte b = 0; b < 3; b = (byte) (b + 1)) {
                this.p[b] = c.g(new StringBuffer().append("/u/bq").append((int) b).toString());
            }
        }
        if (this.C == null) {
            this.C = ee.O("/u/ku");
        }
        if (this.H == null) {
            this.H = ee.O("/u/mon");
        }
        if (this.D == null) {
            this.D = ee.O("/u/yar");
        }
        if (this.G == null) {
            this.G = ee.O("/u/hImg");
        }
        if (I == null) {
            I = new ao[2];
            for (byte b2 = 0; b2 < 2; b2 = (byte) (b2 + 1)) {
                I[b2] = ee.O(new StringBuffer().append("/u/s").append((int) b2).toString());
            }
        }
        if (this.w == null) {
            this.w = ee.O("/u/ar");
        }
        if (K == null) {
            K = new ao[2];
            for (byte b3 = 0; b3 < 2; b3 = (byte) (b3 + 1)) {
                K[b3] = ee.O(new StringBuffer().append("/u/y").append((int) b3).toString());
            }
        }
        d();
    }

    public final void c(byte b) {
        String str = null;
        switch (this.af) {
            case 1:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("生命值+").append((int) this.ag).toString();
                break;
            case 2:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("真气值+").append((int) this.ag).toString();
                break;
            case 3:
                str = "恢复全体生命和真气";
                break;
            case 4:
                str = new StringBuffer().append("解除").append(bj.aC[eb.m[b]]).append("中毒状态").toString();
                break;
            case 5:
                str = "解除所有人异常状态";
                break;
            case 6:
                str = "当前未满级技能提升1级";
                break;
            case 7:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("复活").toString();
                break;
            case 8:
                str = new StringBuffer().append("解除").append(bj.aC[eb.m[b]]).append("诅咒状态").toString();
                break;
            case 9:
                str = new StringBuffer().append("解除").append(bj.aC[eb.m[b]]).append("混乱状态").toString();
                break;
            case 10:
                str = new StringBuffer().append("解除").append(bj.aC[eb.m[b]]).append("冰封状态").toString();
                break;
            case 11:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("生命值已满").toString();
                break;
            case 12:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("真气值已满").toString();
                break;
            case 13:
                str = "全体队友的气值已满";
                break;
            case Database.INT_ISFEE /*14*/:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("没有中毒").toString();
                break;
            case Database.INT_FEECUE /*15*/:
                str = "任何人没有异常";
                break;
            case 16:
                str = "所有技能已满级";
                break;
            case Database.INT_FEES /*17*/:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("没有死亡").toString();
                break;
            case 18:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("没有被诅咒").toString();
                break;
            case Database.INT_MTWORD /*19*/:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("没有混乱").toString();
                break;
            case Database.INT_TIMEWAIT /*20*/:
                str = "非战斗时不可使用";
                break;
            case Database.INT_FAILWORD /*21*/:
                str = "成功丢弃道具";
                break;
            case Database.INT_SUCWORD /*22*/:
                str = "死亡英雄不能使用";
                break;
            case 30:
                str = new StringBuffer().append(bj.aC[eb.m[b]]).append("没有被冰封").toString();
                break;
        }
        if (str != null) {
            ed.cB().a(5, str.toCharArray());
        }
    }

    public final void c(int i2) {
        this.af = 1;
        this.ab = new short[eb.m.length];
        this.ac = eb.ar[8];
        for (int i3 = 0; i3 < this.ab.length; i3++) {
            this.ab[i3] = (short) ((eb.ev[eb.m[i3]][4] * this.ac) / 100);
        }
        if (i2 == 0) {
            b(2);
        }
    }

    public final void c(an anVar) {
        b(anVar, 1);
        du.a(anVar, dz.jZ[dz.e][0], 120, 94, 33, 0);
        a(anVar, 9071433, 11765343, 45, 110, 154, 125);
        a(anVar, 2035968, dz.jZ[dz.e][1], 47, 115, 140, 0);
    }

    /* JADX WARN: Type inference failed for: r5v24, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(defpackage.an r19, int r20, int r21, int r22) {
        /*
            r18 = this;
            short[] r5 = defpackage.eb.ar
            r6 = 0
            short r17 = r5[r6]
            if (r22 != 0) goto L_0x00e4
            r5 = 3
            r0 = r17
            r1 = r5
            if (r0 == r1) goto L_0x00e4
            r5 = 1
            r0 = r18
            r1 = r19
            r2 = r5
            r0.b(r1, r2)
        L_0x0016:
            r0 = r18
            r1 = r19
            r2 = r20
            r3 = r22
            r0.b(r1, r2, r3)
            r7 = 3548436(0x362514, float:4.972418E-39)
            r8 = 65
            r9 = 68
            r10 = 140(0x8c, float:1.96E-43)
            r11 = 96
            r5 = r18
            r6 = r19
            r5.b(r6, r7, r8, r9, r10, r11)
            r7 = 7755858(0x765852, float:1.0868272E-38)
            r8 = 10914680(0xa68b78, float:1.5294724E-38)
            r9 = 3
            short r11 = defpackage.a.l
            byte r12 = defpackage.a.aj
            r13 = 70
            r14 = 73
            r15 = 129(0x81, float:1.81E-43)
            r16 = 28
            r5 = r18
            r6 = r19
            r10 = r21
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            r5 = 0
            byte r6 = defpackage.a.aj
            r12 = r6
            r13 = r5
        L_0x0054:
            byte r5 = defpackage.a.aj
            byte r6 = defpackage.a.al
            int r5 = r5 + r6
            if (r12 < r5) goto L_0x00ef
        L_0x005b:
            r6 = 9071433(0x8a6b49, float:1.2711785E-38)
            r7 = 11765343(0xb3865f, float:1.6486757E-38)
            r8 = 31
            r9 = 165(0xa5, float:2.31E-43)
            r10 = 178(0xb2, float:2.5E-43)
            r11 = 74
            r5 = r19
            a(r5, r6, r7, r8, r9, r10, r11)
            short r5 = defpackage.a.l
            byte r6 = defpackage.a.h
            if (r5 > r6) goto L_0x00d8
            short[] r5 = defpackage.eb.ar
            r6 = 3
            short r5 = r5[r6]
            r6 = 0
            r7 = 0
            short[] r8 = defpackage.eb.ar
            r9 = 6
            short r8 = r8[r9]
            switch(r17) {
                case 0: goto L_0x019a;
                case 1: goto L_0x01c7;
                case 2: goto L_0x01f5;
                case 3: goto L_0x0211;
                case 4: goto L_0x0234;
                default: goto L_0x0083;
            }
        L_0x0083:
            r5 = r6
            r6 = r7
        L_0x0085:
            r7 = 3
            if (r13 < r7) goto L_0x0257
            java.lang.String r7 = "，已满级"
        L_0x008a:
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.String r10 = "消耗真气"
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.StringBuffer r8 = r9.append(r8)
            java.lang.StringBuffer r7 = r8.append(r7)
            java.lang.String r8 = "点"
            java.lang.StringBuffer r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r8 = 2035968(0x1f1100, float:2.852999E-39)
            r0 = r19
            r1 = r8
            r0.setColor(r1)
            java.lang.String r8 = " "
            boolean r8 = r6.equals(r8)
            if (r8 == 0) goto L_0x025b
            r6 = 120(0x78, float:1.68E-43)
            r8 = 188(0xbc, float:2.63E-43)
            r9 = 33
            r0 = r19
            r1 = r5
            r2 = r6
            r3 = r8
            r4 = r9
            r0.a(r1, r2, r3, r4)
            r5 = 120(0x78, float:1.68E-43)
            byte r6 = defpackage.p.e
            int r6 = r6 + 188
            r8 = 33
            r0 = r19
            r1 = r7
            r2 = r5
            r3 = r6
            r4 = r8
            r0.a(r1, r2, r3, r4)
        L_0x00d8:
            byte r5 = defpackage.a.am
            r6 = 3
            if (r5 >= r6) goto L_0x00e3
            r5 = 2
            r11 = r5
        L_0x00df:
            byte r5 = defpackage.a.am
            if (r11 >= r5) goto L_0x0290
        L_0x00e3:
            return
        L_0x00e4:
            r5 = 0
            r0 = r18
            r1 = r19
            r2 = r5
            r0.b(r1, r2)
            goto L_0x0016
        L_0x00ef:
            byte r5 = defpackage.a.am
            r6 = 1
            int r5 = r5 - r6
            if (r12 > r5) goto L_0x005b
            byte[] r5 = defpackage.a.N
            byte r5 = r5[r12]
            short r6 = defpackage.a.l
            byte r7 = defpackage.a.h
            if (r6 > r7) goto L_0x018d
            byte r6 = defpackage.a.h
            if (r12 != r6) goto L_0x0187
            r6 = 16777215(0xffffff, float:2.3509886E-38)
            r10 = r6
        L_0x0107:
            byte[] r6 = defpackage.eb.m
            byte r6 = r6[r20]
            int r7 = defpackage.eb.q(r6, r5)
            short[][][] r8 = defpackage.eb.ai
            r8 = r8[r6]
            r9 = 0
            r8 = r8[r9]
            short r13 = r8[r7]
            r8 = 3
            if (r13 >= r8) goto L_0x0193
            short[][][] r8 = defpackage.eb.ai
            r8 = r8[r6]
            r9 = 1
            r8 = r8[r9]
            short r7 = r8[r7]
            short r6 = defpackage.eb.w(r6, r5)
            r11 = r7
            r14 = r6
        L_0x012a:
            java.lang.String[] r6 = defpackage.bj.aE
            r6 = r6[r5]
            r7 = 114(0x72, float:1.6E-43)
            byte r5 = defpackage.a.aj
            int r5 = r12 - r5
            int r5 = r5 * 28
            int r8 = r5 + 96
            r9 = 33
            r5 = r19
            defpackage.du.a(r5, r6, r7, r8, r9, r10)
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r6 = "Lv"
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r6 = java.lang.String.valueOf(r13)
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r6 = 162(0xa2, float:2.27E-43)
            byte r7 = defpackage.a.aj
            int r7 = r12 - r7
            int r7 = r7 * 28
            int r7 = r7 + 96
            r8 = 33
            r0 = r19
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.a(r1, r2, r3, r4)
            r6 = 70
            byte r5 = defpackage.a.aj
            int r5 = r12 - r5
            int r5 = r5 * 29
            int r7 = r5 + 97
            int r5 = r11 * 127
            int r8 = r5 / r14
            r9 = 0
            r10 = 127(0x7f, float:1.78E-43)
            r11 = 3
            r5 = r19
            c(r5, r6, r7, r8, r9, r10, r11)
            int r5 = r12 + 1
            r12 = r5
            goto L_0x0054
        L_0x0187:
            r6 = 5849662(0x59423e, float:8.197122E-39)
            r10 = r6
            goto L_0x0107
        L_0x018d:
            r6 = 5849662(0x59423e, float:8.197122E-39)
            r10 = r6
            goto L_0x0107
        L_0x0193:
            r6 = 127(0x7f, float:1.78E-43)
            r7 = 127(0x7f, float:1.78E-43)
            r11 = r6
            r14 = r7
            goto L_0x012a
        L_0x019a:
            if (r5 != 0) goto L_0x01c4
            java.lang.String r5 = "单体"
        L_0x019e:
            java.lang.String r6 = "物理伤害"
            short[] r7 = defpackage.eb.ar
            r9 = 7
            short r7 = r7[r9]
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.StringBuffer r5 = r9.append(r5)
            java.lang.StringBuffer r5 = r5.append(r7)
            java.lang.String r7 = "%"
            java.lang.StringBuffer r5 = r5.append(r7)
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = " "
            goto L_0x0085
        L_0x01c4:
            java.lang.String r5 = "全体"
            goto L_0x019e
        L_0x01c7:
            if (r5 != 0) goto L_0x01f2
            java.lang.String r5 = "单体"
        L_0x01cb:
            java.lang.String r6 = "法术伤害"
            short[] r7 = defpackage.eb.ar
            r9 = 8
            short r7 = r7[r9]
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.StringBuffer r5 = r9.append(r5)
            java.lang.StringBuffer r5 = r5.append(r7)
            java.lang.String r7 = "%"
            java.lang.StringBuffer r5 = r5.append(r7)
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = " "
            goto L_0x0085
        L_0x01f2:
            java.lang.String r5 = "全体"
            goto L_0x01cb
        L_0x01f5:
            short[] r5 = defpackage.eb.ar
            r6 = 7
            short r5 = r5[r6]
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            java.lang.StringBuffer r5 = r6.append(r5)
            java.lang.String r6 = "%物理伤害"
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = "15%几率盗取敌物品"
            goto L_0x0085
        L_0x0211:
            short[] r5 = defpackage.eb.ar
            r6 = 8
            short r5 = r5[r6]
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            java.lang.String r7 = "全体恢复"
            java.lang.StringBuffer r6 = r6.append(r7)
            java.lang.StringBuffer r5 = r6.append(r5)
            java.lang.String r6 = "%的生命值"
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = " "
            goto L_0x0085
        L_0x0234:
            short[] r5 = defpackage.eb.ar
            r6 = 8
            short r5 = r5[r6]
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            java.lang.String r7 = "单体"
            java.lang.StringBuffer r6 = r6.append(r7)
            java.lang.StringBuffer r5 = r6.append(r5)
            java.lang.String r6 = "%法术伤害"
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = "20%降低敌20%防御"
            goto L_0x0085
        L_0x0257:
            java.lang.String r7 = ""
            goto L_0x008a
        L_0x025b:
            r8 = 120(0x78, float:1.68E-43)
            r9 = 188(0xbc, float:2.63E-43)
            r10 = 33
            r0 = r19
            r1 = r5
            r2 = r8
            r3 = r9
            r4 = r10
            r0.a(r1, r2, r3, r4)
            r5 = 120(0x78, float:1.68E-43)
            byte r8 = defpackage.p.e
            int r8 = r8 + 188
            r9 = 33
            r0 = r19
            r1 = r6
            r2 = r5
            r3 = r8
            r4 = r9
            r0.a(r1, r2, r3, r4)
            r5 = 120(0x78, float:1.68E-43)
            byte r6 = defpackage.p.e
            int r6 = r6 * 2
            int r6 = r6 + 188
            r8 = 33
            r0 = r19
            r1 = r7
            r2 = r5
            r3 = r6
            r4 = r8
            r0.a(r1, r2, r3, r4)
            goto L_0x00d8
        L_0x0290:
            short r5 = defpackage.a.l
            if (r11 != r5) goto L_0x02ac
            r5 = 16777215(0xffffff, float:2.3509886E-38)
            r10 = r5
        L_0x0298:
            java.lang.String r6 = "[未知]"
            r7 = 135(0x87, float:1.89E-43)
            int r5 = r11 * 30
            int r8 = r5 + 95
            r9 = 33
            r5 = r19
            defpackage.du.a(r5, r6, r7, r8, r9, r10)
            int r5 = r11 + -1
            r11 = r5
            goto L_0x00df
        L_0x02ac:
            r5 = 5849662(0x59423e, float:8.197122E-39)
            r10 = r5
            goto L_0x0298
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.a.c(an, int, int, int):void");
    }

    public final void d(an anVar) {
        b(anVar, 0);
        b(anVar, 3548436, 42, 75, 156, 92);
        a(anVar, 7755858, 10914680, 4, eb.fA.length, l, aj, 48, 80, MIDIControl.NOTE_ON, 20);
        int i2 = aj;
        while (true) {
            int i3 = i2;
            if (i3 < aj + al && i3 <= am - 1) {
                int i4 = i3 == h ? 16777215 : 5849662;
                byte b = N[i3];
                if (O[i3] != 0) {
                    du.a(anVar, r.aC[b], 88, ((i3 - aj) * 21) + 100, 33, i4);
                    du.a(anVar, new StringBuffer().append("X").append(String.valueOf((int) O[i3])).toString(), 148, ((i3 - aj) * 21) + 100, 33, i4);
                } else {
                    du.a(anVar, "空", 115, ((i3 - aj) * 21) + 100, 33, i4);
                }
                i2 = i3 + 1;
            }
        }
        a(anVar, 9071433, 11765343, 36, 170, 167, 65);
        if (O[h] != 0) {
            a(anVar, 2035968, r.aE[P], 40, 172, 145, 0);
        }
    }

    public final void f(int i2) {
        this.at = (byte) i2;
        g = 0;
        h = 0;
        l = 0;
        this.as = 0;
    }

    public final void f(an anVar) {
        b(anVar, 9663327, 88, 235, 76, 20);
        du.a(anVar, this.H, 0, 91, 238, 0);
        du.b(anVar, eb.ac, 135, 245, 2, 3);
    }

    public final void g(int i2) {
        this.au = (byte) i2;
    }

    public final void h(an anVar) {
        a(anVar, 9071433, 11765343, 69, 125, 100, 70);
        du.a(anVar, this.w, 0, 75, 155, 0);
        du.a(anVar, this.w, 2, 155, 155, 0);
        for (int i2 = 0; i2 < 6; i2++) {
            du.a(anVar, K[0], (i2 * 6) + 0, 0, 6, 18, 0, (i2 * 10) + 90, 150);
        }
        for (byte b = 0; b < bh.e; b++) {
            du.a(anVar, K[1], (b * 6) + 0, 0, 6, 18, 0, (b * 10) + 90, 150);
        }
        anVar.setColor(16317367);
        anVar.a("关", 85, 170, 0);
        anVar.a("开", 140, 170, 0);
    }

    public final void l(an anVar) {
        String str;
        int i2;
        switch (this.as) {
            case 0:
            case 1:
                m(anVar);
                return;
            case 2:
                b(anVar, 9923675, 70, 150, 100, 48);
                du.a(anVar, this.w, 0, 75, 165, 0);
                du.a(anVar, this.w, 2, 155, 165, 0);
                for (int i3 = 0; i3 < 2; i3++) {
                    a(anVar, 16777215, 5, i3 + 2, 95, (i3 * 18) + 158);
                }
                du.a(anVar, String.valueOf((int) k), 130, 155, 0, 16777215);
                du.a(anVar, String.valueOf(av[h] * k), 120, 174, 0, 16777215);
                return;
            case 3:
                m(anVar);
                switch (this.an) {
                    case 1:
                        str = "购买成功!";
                        i2 = 5;
                        break;
                    case 2:
                        str = "金钱不够!";
                        i2 = 5;
                        break;
                    case 3:
                        str = "成功卖出!";
                        i2 = 5;
                        break;
                    case 4:
                        str = "剧情道具不可卖!";
                        i2 = 5;
                        break;
                    default:
                        str = null;
                        i2 = 0;
                        break;
                }
                ed.cB().a(i2, str.toCharArray());
                return;
            default:
                return;
        }
    }
}
