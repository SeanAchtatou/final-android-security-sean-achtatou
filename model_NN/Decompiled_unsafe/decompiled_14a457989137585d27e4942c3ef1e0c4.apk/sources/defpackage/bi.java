package defpackage;

import com.nokia.mid.ui.FullCanvas;
import javax.microedition.midlet.MIDlet;

/* renamed from: bi  reason: default package */
public final class bi extends ae implements Runnable {
    public static boolean M = false;
    public static int ac;
    public static boolean ae = false;
    private static bi b;
    public static byte e;
    public static ao q;
    public int[] aI = new int[5];
    public boolean dX = false;
    public boolean dY = false;
    private boolean dZ = true;
    private boolean ea = false;
    private long gn;
    public dy go;
    public dy gp;

    private bi() {
        bg.aU().R();
        this.go = l.al();
        this.go.N();
    }

    public static bi bo() {
        if (b == null) {
            b = new bi();
        }
        return b;
    }

    public static boolean q(int i) {
        return (ac & i) != 0;
    }

    private int w(int i) {
        return ((i >= 48 && i <= 57) || Math.abs(i) == Math.abs(-6) || Math.abs(i) == Math.abs(-7) || i == 35 || i == 42) ? i : ae.h(i);
    }

    private static int x(int i) {
        switch (i) {
            case FullCanvas.KEY_END /*-11*/:
                return as.GAME_B_PRESSED;
            case FullCanvas.KEY_SOFTKEY2 /*-7*/:
                return as.FIRE_PRESSED;
            case FullCanvas.KEY_SOFTKEY1 /*-6*/:
                return 128;
            case 1:
            case ae.KEY_NUM2:
                return 8;
            case 2:
            case ae.KEY_NUM4:
                return 2;
            case 5:
            case ae.KEY_NUM6:
                return 1;
            case 6:
            case ae.KEY_NUM8:
                return 4;
            case 8:
            case ae.KEY_NUM5:
                return 16;
            case ae.KEY_NUM0:
                return 32;
            case ae.KEY_NUM9:
                return 64;
            default:
                if (Math.abs(i) == Math.abs(-6)) {
                    return 128;
                }
                if (Math.abs(i) == Math.abs(-7)) {
                    return as.FIRE_PRESSED;
                }
                return 0;
        }
    }

    public static boolean y(int i) {
        if (!q(i)) {
            return false;
        }
        ac &= i ^ -1;
        return true;
    }

    public final void N() {
        System.gc();
        this.gn = 0;
    }

    public final void a(int i) {
        this.aI[0] = 30;
        this.aI[1] = (319 / this.aI[0]) + 1;
        this.aI[2] = i;
        this.aI[3] = ee.q(0, 7);
        this.aI[4] = 0;
        this.dY = true;
    }

    public final void a(String str) {
        try {
            bh.c();
            Midlet midlet = Midlet.c;
            MIDlet.r(str);
            c();
        } catch (Exception e2) {
        }
    }

    public final void aC() {
        bh.P();
        ac = 0;
        M = true;
        this.ea = true;
    }

    public final void aD() {
        bh.P();
        ac = 0;
        if (ae) {
            M = false;
        }
        this.ea = false;
    }

    public final void c() {
        ae = true;
        bh.c();
        this.dZ = false;
    }

    public final void i(int i) {
        ac |= x(w(i));
    }

    public final void j(int i) {
        ac &= x(w(i)) ^ -1;
    }

    public final void n(an anVar) {
        anVar.a(p.eo);
        if (!M) {
            this.go.a(anVar);
            if (this.dY) {
                anVar.setColor(0);
                int i = 0;
                int i2 = 0;
                while (i < 8) {
                    int i3 = i2;
                    for (int i4 = 0; i4 < this.aI[1]; i4++) {
                        switch (this.aI[2]) {
                            case -1:
                                i3 = 0;
                                break;
                            case 1:
                                i3 = this.aI[0];
                                break;
                        }
                        switch (this.aI[3]) {
                            case 0:
                                i3 += (i4 - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 1:
                                i3 += ((this.aI[1] - i4) - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 2:
                                i3 += (i - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 3:
                                i3 += ((8 - i) - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 4:
                                i3 += (((i + i4) >> 1) - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 5:
                                i3 += ((((8 - i) + i4) >> 1) - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 6:
                                i3 += ((((this.aI[1] + i) - i4) >> 1) - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                            case 7:
                                i3 += (((((8 - i) + this.aI[1]) - i4) >> 1) - this.aI[4]) * this.aI[2];
                                if (i3 >= 0) {
                                    if (i3 <= this.aI[0]) {
                                        break;
                                    } else {
                                        i3 = this.aI[0];
                                        break;
                                    }
                                } else {
                                    i3 = 0;
                                    break;
                                }
                        }
                        anVar.c((this.aI[0] * i) + ((this.aI[0] - i3) >> 1), (this.aI[0] * i4) + ((this.aI[0] - i3) >> 1), i3, i3);
                    }
                    i++;
                    i2 = i3;
                }
                int[] iArr = this.aI;
                iArr[4] = iArr[4] + 2;
                if (this.aI[4] > 40) {
                    this.dY = false;
                    return;
                }
                return;
            }
            return;
        }
        du.c(anVar, 0, 0, 0, 240, 320);
        anVar.setColor(9071433);
        anVar.c(92, 135, 58, 30);
        a.M().b(anVar, 9071433, 92, 135, 58, 30);
        anVar.setColor(16777215);
        anVar.a("暂停", 120, 160, 33);
    }

    public final void run() {
        b.P();
        while (this.dZ) {
            if (!this.ea) {
                long currentTimeMillis = System.currentTimeMillis();
                bh.N();
                if (!M) {
                    byte b2 = (byte) (e + 1);
                    e = b2;
                    e = b2 > 31 ? 0 : e;
                    if (this.gp != null) {
                        if (this.go != null) {
                            this.go.c();
                            this.go = null;
                        }
                        this.go = this.gp;
                        this.gp = null;
                        this.go.N();
                        this.gp = null;
                    }
                    this.go.Q();
                } else if (ac != 0) {
                    ac = 0;
                    M = false;
                    ae = false;
                    ea.cp().dZ = true;
                    a.M().M = true;
                    bh.c(bh.f);
                }
                au();
                av();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                this.gn += currentTimeMillis2 > 40 ? currentTimeMillis2 : 40;
                if (this.gn > 7000) {
                    System.gc();
                    this.gn = 0;
                }
                if (!this.dX && currentTimeMillis2 < 40) {
                    try {
                        Thread.sleep(40 - currentTimeMillis2);
                    } catch (InterruptedException e2) {
                    }
                }
            }
        }
        Midlet.c.x();
        Midlet.c = null;
    }
}
