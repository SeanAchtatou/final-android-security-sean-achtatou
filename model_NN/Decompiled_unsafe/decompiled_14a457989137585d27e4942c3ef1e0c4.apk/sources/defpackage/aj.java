package defpackage;

import android.app.AlertDialog;

/* renamed from: aj  reason: default package */
final class aj implements Runnable {
    final /* synthetic */ ah eP;
    private final /* synthetic */ AlertDialog eR;

    aj(ah ahVar, AlertDialog alertDialog) {
        this.eP = ahVar;
        this.eR = alertDialog;
    }

    public final void run() {
        this.eR.dismiss();
    }
}
