package defpackage;

/* renamed from: o  reason: default package */
public final class o implements dy {
    private static o en;
    public ao[] I;
    private byte R;
    public String[] aC = {"游戏简介：", "乱世烽烟，再现江湖。", "没落的王侯之子，", "神奇的玄冥神珠", "雄霸江湖的乱世枭雄。", "以及翩翩佳丽的出现，", "将为我们演绎", "怎样传奇故事。", " ", "操作说明", "方向键上(2键)：向上", "方向键左(4键)：向左", "方向键右(6键)：向右", "方向键下(8键)：向下", "中心键(5键)：", "确定、对话、开锁", "9键：御剑飞行", " ", "左软键：菜单", "右软键：增值服务", "0键：查看任务", " "};
    public String[] aE = {"<游戏开发商>", "华益天信科技", "(北京)有限公司", "<客服电话>", "010-62962997-204", "<客服邮箱>", "cs@huayigame.cn", " "};
    public int ac = 1;
    public byte e;
    public byte f;
    public byte g;
    public byte h;
    public byte i = 1;
    private byte j;
    public byte[] m = {0, 1, 3, 4, 5, 7};
    public ao q = null;
    private ao r;
    private ao s;

    private o() {
    }

    private static byte a(String[] strArr, int i2) {
        for (int i3 = i2; i3 < strArr.length; i3++) {
            if (strArr[i3].equals(" ")) {
                return (byte) i3;
            }
        }
        return 0;
    }

    public static o an() {
        if (en == null) {
            en = new o();
        }
        return en;
    }

    public final void N() {
        if (this.I == null) {
            P();
        }
        if (l.al().I == null) {
            l.al().I = new ao[4];
            for (int i2 = 0; i2 < l.al().I.length; i2++) {
                l.al().I[i2] = ee.O(new StringBuffer().append("/u/me").append(i2).toString());
            }
        }
        if (l.al().p == null) {
            l.al().p = new n[4];
            for (int i3 = 0; i3 < l.al().p.length; i3++) {
                l.al().p[i3] = c.g(new StringBuffer().append("/u/mea").append(i3).toString());
            }
            l.al().P();
            l.al().p[0].ac = 5;
        }
        this.r = ee.O("/u/meh");
        this.s = ee.O("/u/mear");
        bj.bp().v("mn2_llv");
        if (bj.gz[0][1] == 0 && bj.gz[1][1] == 0 && bj.gz[2][1] == 0) {
            this.j = 0;
        } else {
            this.j = 1;
        }
        a(0);
        bj.bp();
        bj.Q();
    }

    public final void P() {
        if (this.I == null) {
            this.I = new ao[9];
            for (int i2 = 0; i2 < this.I.length; i2++) {
                this.I[i2] = ee.O(new StringBuffer().append("/u/mes").append(i2).toString());
            }
        }
    }

    public final void Q() {
        a.M().M = true;
        switch (this.e) {
            case 0:
                l al = l.al();
                l al2 = l.al();
                int i2 = al2.ac + 1;
                al2.ac = i2;
                al.ac = i2 > 31 ? 0 : l.al().ac;
                if (bi.y(2)) {
                    byte b = (byte) (this.j - 1);
                    this.j = b;
                    this.j = b < 0 ? (byte) (this.m.length - 1) : this.j;
                    return;
                } else if (bi.y(1)) {
                    byte b2 = (byte) (this.j + 1);
                    this.j = b2;
                    this.j = b2 > this.m.length - 1 ? 0 : this.j;
                    return;
                } else if (!bi.y(16) && !bi.y(128)) {
                    return;
                } else {
                    if (this.m[this.j] == 0) {
                        eb.cq().c();
                        bj.bp().c();
                        bi.bo().gp = ea.cp();
                        return;
                    }
                    a(this.m[this.j]);
                    return;
                }
            case 1:
                if (bi.y(1280)) {
                    a(0);
                    return;
                } else if (bi.y(8)) {
                    byte b3 = (byte) (this.R - 1);
                    this.R = b3;
                    this.R = b3 < 0 ? 2 : this.R;
                    return;
                } else if (bi.y(4)) {
                    byte b4 = (byte) (this.R + 1);
                    this.R = b4;
                    this.R = b4 > 2 ? 0 : this.R;
                    return;
                } else if ((bi.y(16) || bi.y(128)) && bj.gz[this.R][1] != 0) {
                    String str = "";
                    switch (this.R) {
                        case 0:
                            str = "mn2_l1";
                            break;
                        case 1:
                            str = "mn2_l2";
                            break;
                        case 2:
                            str = "mn2_l3";
                            break;
                    }
                    bj.bp().v(str);
                    this.R = 0;
                    bi.bo().gp = ea.cp();
                    ea.cp().a(0);
                    return;
                } else {
                    return;
                }
            case 2:
                if (b.aG != null && !b.aG.equals("")) {
                    bi.bo().a(b.aG);
                    a(0);
                    return;
                }
                return;
            case 3:
                if (bi.y(1280)) {
                    a(0);
                }
                R();
                return;
            case 4:
                if (bi.y(1280)) {
                    a(0);
                }
                a.M();
                a.R();
                return;
            case 5:
                if (bi.y(1280)) {
                    a(0);
                    return;
                } else if (bi.y(2)) {
                    if (this.h > 0) {
                        this.i = (byte) (this.i - 1);
                        byte b5 = (byte) (this.g - 1);
                        this.h = b5;
                        if (b5 < 0) {
                            this.h = 0;
                            b5 = 0;
                        }
                        this.g = (byte) (this.h - this.g);
                        this.f = a(this.aE, b5);
                        this.h = b5;
                        return;
                    }
                    return;
                } else if (bi.y(1) && this.f < this.aE.length - 1) {
                    this.g = (byte) (this.h + 1);
                    this.h = (byte) (this.f + 1);
                    this.f = a(this.aE, this.h + 1);
                    this.i = (byte) (this.i + 1);
                    return;
                } else {
                    return;
                }
            case 6:
                if (bi.y(128)) {
                    a(12);
                    return;
                } else if (bi.y(1280)) {
                    a(0);
                    return;
                } else {
                    return;
                }
            case 7:
                if (bi.y(128)) {
                    bi.bo().c();
                    return;
                } else if (bi.y(1280)) {
                    a(0);
                    return;
                } else {
                    return;
                }
            case 8:
                if (b.aF != null && !b.aF.equals("")) {
                    bi.bo().a(b.aF);
                    a(0);
                    return;
                }
                return;
            case 9:
            case 10:
            case 11:
            default:
                return;
            case 12:
                if (b.aF == null || b.aF.equals("")) {
                    a(0);
                    return;
                }
                bi.bo().a(b.aF);
                a(0);
                return;
        }
    }

    public final void R() {
        if (bi.y(2)) {
            if (this.h > 0) {
                this.i = (byte) (this.i - 1);
                byte b = (byte) (this.g - 1);
                this.h = b;
                if (b < 0) {
                    b = 0;
                    this.h = 0;
                }
                this.g = (byte) (this.h - this.g);
                this.f = a(this.aC, b);
                this.h = b;
            }
        } else if (bi.y(1) && this.f < this.aC.length - 1) {
            this.g = (byte) (this.h + 1);
            this.h = (byte) (this.f + 1);
            this.f = a(this.aC, this.h + 1);
            this.i = (byte) (this.i + 1);
        }
    }

    public final void a(int i2) {
        this.e = (byte) i2;
        switch (i2) {
            case 3:
                this.f = a(this.aC, 0);
                break;
            case 5:
                this.f = a(this.aE, 0);
                break;
        }
        this.i = 1;
        this.g = 0;
        this.h = 0;
    }

    public final void a(an anVar) {
        l.al().c(anVar);
        du.a(anVar, this.r, 0, 34, 315, 33);
        switch (this.e) {
            case 0:
                du.a(anVar, this.I[this.m[this.j]], 0, 32, 318, 33);
                if (l.al().ac % 5 == 0) {
                    this.ac *= -1;
                }
                du.a(anVar, this.s, 0, 5 - this.ac, (320 - (this.I[this.m[this.j]].getHeight() / 2)) - 15, 0);
                du.a(anVar, this.s, 2, this.ac + 50, (320 - (this.I[this.m[this.j]].getHeight() / 2)) - 15, 0);
                return;
            case 1:
                a.M().b(anVar, 0);
                a.M().a(anVar, this.R);
                return;
            case 2:
            default:
                return;
            case 3:
                a.M().b(anVar, 1);
                a(this.aC, anVar, 20);
                return;
            case 4:
                a.M().b(anVar, 1);
                a.M().h(anVar);
                return;
            case 5:
                a.M().b(anVar, 1);
                a(this.aE, anVar, 20);
                return;
            case 6:
                anVar.setColor(0);
                anVar.c(0, 0, 240, 320);
                du.a(anVar, this.q, 0, 120, 160, 3);
                du.a(anVar, "确定", 2, 314 - p.e, 20, 16711680);
                du.a(anVar, "返回", 238, 314 - p.e, 24, 65280);
                return;
            case 7:
                du.c(anVar, 0, 0, 0, 240, 320);
                du.a(anVar, "是否退出游戏", 120, 160, 33, 16777215);
                du.a(anVar, 6, 6);
                return;
        }
    }

    public final void a(String[] strArr, an anVar, int i2) {
        int i3 = 0;
        for (int i4 = this.h; i4 < this.f; i4++) {
            anVar.setColor(0);
            anVar.a(strArr[i4], 120, (i3 * 20) + 85, 33);
            i3++;
        }
        anVar.a(new StringBuffer().append((this.i <= 1 || this.f != strArr.length - 1) ? ">>" : "<<").append(String.valueOf((int) this.i)).toString(), 120, 255, 33);
    }

    public final void c() {
        l.al().p = null;
        l.al().I = null;
        this.I = null;
        this.s = null;
        this.r = null;
        this.q = null;
    }
}
