package defpackage;

import android.view.View;
import android.widget.Button;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: af  reason: default package */
public final class af implements View.OnClickListener, ck {
    public static final int BACK = 2;
    public static final int CANCEL = 3;
    public static final int EXIT = 7;
    public static final int HELP = 5;
    public static final int ITEM = 8;
    public static final int OK = 4;
    public static final int SCREEN = 1;
    public static final int STOP = 6;
    private int eH = 4;
    private Button eI = new Button(cl.getActivity());
    private String label;
    private int priority = 0;

    public af(String str, int i, int i2) {
        this.label = str;
        this.eI.setText(str);
        this.eI.setOnClickListener(this);
    }

    public final void aA() {
        cf.b(cf.a((int) MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }

    public final String ay() {
        return this.label;
    }

    public final Button az() {
        return this.eI;
    }

    public final int getId() {
        return this.priority;
    }

    public final void onClick(View view) {
        cf.b(cf.a((int) MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }
}
