package defpackage;

import com.network.app.data.Database;

/* renamed from: du  reason: default package */
public final class du {
    public static final byte[][] dR = {new byte[]{11, 14, 10}, new byte[]{7, 8, 7}, new byte[]{6, 7, 5}};

    public static void a(an anVar, int i, int i2) {
        a(anVar, "是", 2, (320 - i) - p.e, 20, 65280);
        a(anVar, "否", 238, (320 - i2) - p.e, 24, 65280);
    }

    public static void a(an anVar, int i, int i2, int i3, int i4, int i5) {
        anVar.setColor(0);
        anVar.c(0, 0, 240, 320);
    }

    public static void a(an anVar, ao aoVar, int i, int i2, int i3, int i4) {
        if (i == 0) {
            anVar.a(aoVar, i2, i3, i4);
            return;
        }
        anVar.a(aoVar, 0, 0, aoVar.getWidth(), aoVar.getHeight(), i, i2, i3, i4);
    }

    public static void a(an anVar, ao aoVar, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        if (i3 > 0 && i4 > 0) {
            if (i5 == 0) {
                anVar.d(i6, i7, i3, i4);
                anVar.a(aoVar, i6 - i, i7 - i2, 0);
                anVar.d(0, 0, 1000, 1000);
                return;
            }
            anVar.a(aoVar, i, i2, i3, i4, i5, i6, i7, 0);
        }
    }

    public static void a(an anVar, String str, int i, int i2, int i3, int i4) {
        anVar.setColor(i4);
        anVar.a(str, i, i2, i3);
    }

    public static void a(an anVar, String str, int i, int i2, int i3, int i4, int i5) {
        anVar.a(p.eo);
        anVar.setColor(16777215);
        anVar.a(str, i, i2 + 1, i3);
        anVar.a(str, i + 1, i2, i3);
        anVar.a(str, i + 1, i2 + 1, i3);
        anVar.setColor(16711680);
        anVar.a(str, i, i2, i3);
    }

    public static void b(an anVar, int i, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        switch (i5) {
            case 0:
            case Database.INT_TIMEWAIT:
                int i9 = i;
                i7 = i2;
                do {
                    i7 += dR[i4][2];
                    i9 /= 10;
                } while (i9 != 0);
                i8 = i3;
                i6 = i;
                break;
            case 3:
                int i10 = i;
                i7 = i2;
                do {
                    i7 += dR[i4][2] >> 1;
                    i10 /= 10;
                } while (i10 != 0);
                i8 = i3 - (dR[i4][1] >> 1);
                i6 = i;
                break;
            case 6:
                int i11 = i;
                i7 = i2;
                do {
                    i7 += dR[i4][2];
                    i11 /= 10;
                } while (i11 != 0);
                i8 = i3 - (dR[i4][1] >> 1);
                i6 = i;
                break;
            case 10:
                i8 = i3 - (dR[i4][1] >> 1);
                i7 = i2;
                i6 = i;
                break;
            case Database.INT_FEES:
                int i12 = i;
                i7 = i2;
                do {
                    i7 += dR[i4][2] >> 1;
                    i12 /= 10;
                } while (i12 != 0);
                i8 = i3;
                i6 = i;
                break;
            case 24:
                i8 = i3;
                i7 = i2;
                i6 = i;
                break;
            case 33:
                int i13 = i;
                i7 = i2;
                do {
                    i7 += dR[i4][2] >> 1;
                    i13 /= 10;
                } while (i13 != 0);
                i8 = i3 - dR[i4][1];
                i6 = i;
                break;
            case 36:
                int i14 = i;
                i7 = i2;
                do {
                    i7 += dR[i4][2];
                    i14 /= 10;
                } while (i14 != 0);
                i8 = i3 - dR[i4][1];
                i6 = i;
                break;
            case 40:
                i8 = i3 - dR[i4][1];
                i7 = i2;
                i6 = i;
                break;
            default:
                i8 = i3;
                i7 = i2;
                i6 = i;
                break;
        }
        do {
            i7 -= dR[i4][2];
            anVar.d(i7, i8, dR[i4][0], dR[i4][1]);
            anVar.a(bj.I[i4], i7 - ((i6 % 10) * dR[i4][0]), i8, 20);
            i6 /= 10;
        } while (i6 != 0);
        anVar.d(0, 0, 240, 320);
    }

    public static void c(an anVar, int i, int i2) {
        a(anVar, "点播", 2, 316 - p.e, 20, 65280);
        a(anVar, "返回", 238, 316 - p.e, 24, 65280);
    }

    public static void c(an anVar, int i, int i2, int i3, int i4, int i5) {
        anVar.setColor(i);
        anVar.c(i2, i3, i4, i5);
    }
}
