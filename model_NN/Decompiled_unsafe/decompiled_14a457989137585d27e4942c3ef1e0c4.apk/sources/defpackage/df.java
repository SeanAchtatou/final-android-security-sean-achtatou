package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;

/* renamed from: df  reason: default package */
public final class df extends an {
    private static final DashPathEffect iv = new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f);
    private Bitmap iA;
    private int iB;
    private int iC;
    private int iD;
    private int iE;
    private Paint is;
    private Paint it;
    public dd iu;
    public Canvas iw;
    private am ix;
    private int iy;
    private Matrix iz;

    private df() {
        this.is = new Paint();
        this.it = new Paint();
        this.iy = 0;
        this.iz = new Matrix();
        this.iB = 0;
        this.iC = 0;
        this.iD = 0;
        this.iE = 0;
        this.is.setAntiAlias(true);
        this.is.setStyle(Paint.Style.STROKE);
        this.it.setAntiAlias(true);
        this.it.setStyle(Paint.Style.FILL);
    }

    public df(Bitmap bitmap) {
        this();
        this.iA = bitmap;
        a(new Canvas(bitmap));
    }

    public df(Canvas canvas) {
        this();
        a(canvas);
    }

    private void a(Canvas canvas) {
        this.iw = canvas;
        a(am.aF());
        translate(-this.fe, -this.ff);
        if (this.iA != null) {
            d(0, 0, this.iA.getWidth(), this.iA.getHeight());
        } else {
            d(0, 0, bp.gC.getWidth(), bp.gC.getHeight());
        }
    }

    public final void a(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        if (this.iD > 0 && this.iE > 0) {
            if (i > i3) {
                i5 = i3;
                i6 = i + 1;
            } else {
                i5 = i3 + 1;
                i6 = i;
            }
            if (i2 > i4) {
                i7 = i4;
                i8 = i2 + 1;
            } else {
                i7 = i4 + 1;
                i8 = i2;
            }
            this.iw.drawLine((float) i6, (float) i8, (float) i5, (float) i7, this.is);
        }
    }

    public final void a(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.iD > 0 && this.iE > 0) {
            this.iw.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), 0.0f, 360.0f, true, this.is);
        }
    }

    public final void a(am amVar) {
        am aF = amVar == null ? am.aF() : amVar;
        this.ix = aF;
        this.iu = de.b(aF);
    }

    public final void a(ao aoVar, int i, int i2, int i3) {
        if (aoVar != null && this.iD > 0 && this.iE > 0) {
            int i4 = i3 == 0 ? 20 : i3;
            this.iw.drawBitmap(aoVar.fh, (float) ((i4 & 8) != 0 ? i - aoVar.width : (i4 & 1) != 0 ? i - (aoVar.width / 2) : i), (float) ((i4 & 32) != 0 ? i2 - aoVar.height : (i4 & 2) != 0 ? i2 - (aoVar.height / 2) : i2), (Paint) null);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0120  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(defpackage.ao r10, int r11, int r12, int r13, int r14, int r15, int r16, int r17, int r18) {
        /*
            r9 = this;
            if (r10 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            boolean r0 = r10.isMutable()
            if (r0 == 0) goto L_0x001c
            an r0 = r10.aJ()
            if (r0 != r9) goto L_0x001c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Image is source and target"
            r0.<init>(r1)
            throw r0
        L_0x001c:
            android.graphics.Matrix r0 = r9.iz
            r0.reset()
            switch(r15) {
                case 0: goto L_0x002c;
                case 1: goto L_0x00b7;
                case 2: goto L_0x008c;
                case 3: goto L_0x006d;
                case 4: goto L_0x00d0;
                case 5: goto L_0x005e;
                case 6: goto L_0x007d;
                case 7: goto L_0x009d;
                default: goto L_0x0024;
            }
        L_0x0024:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Bad transform"
            r0.<init>(r1)
            throw r0
        L_0x002c:
            int r0 = -r11
            int r1 = -r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
        L_0x0032:
            r4 = 0
            if (r18 != 0) goto L_0x0150
            r5 = 20
        L_0x0037:
            r6 = r5 & 64
            if (r6 == 0) goto L_0x003c
            r4 = 1
        L_0x003c:
            r6 = r5 & 16
            if (r6 == 0) goto L_0x00e8
            r6 = r5 & 34
            if (r6 == 0) goto L_0x014b
        L_0x0044:
            r4 = 1
            r6 = r4
            r4 = r17
        L_0x0048:
            r7 = r5 & 4
            if (r7 == 0) goto L_0x0107
            r5 = r5 & 9
            if (r5 == 0) goto L_0x0147
        L_0x0050:
            r5 = 1
            r6 = r5
            r5 = r16
        L_0x0054:
            if (r6 == 0) goto L_0x0120
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Bad Anchor"
            r0.<init>(r1)
            throw r0
        L_0x005e:
            android.graphics.Matrix r0 = r9.iz
            r1 = 1119092736(0x42b40000, float:90.0)
            r0.preRotate(r1)
            int r0 = r14 + r12
            int r1 = -r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x0032
        L_0x006d:
            android.graphics.Matrix r0 = r9.iz
            r1 = 1127481344(0x43340000, float:180.0)
            r0.preRotate(r1)
            int r0 = r13 + r11
            int r1 = r14 + r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
            goto L_0x0032
        L_0x007d:
            android.graphics.Matrix r0 = r9.iz
            r1 = 1132920832(0x43870000, float:270.0)
            r0.preRotate(r1)
            int r0 = -r12
            int r1 = r13 + r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x0032
        L_0x008c:
            android.graphics.Matrix r0 = r9.iz
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            int r0 = r13 + r11
            int r1 = -r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
            goto L_0x0032
        L_0x009d:
            android.graphics.Matrix r0 = r9.iz
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            android.graphics.Matrix r0 = r9.iz
            r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r0.preRotate(r1)
            int r0 = r14 + r12
            int r1 = r13 + r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x0032
        L_0x00b7:
            android.graphics.Matrix r0 = r9.iz
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            android.graphics.Matrix r0 = r9.iz
            r1 = -1020002304(0xffffffffc3340000, float:-180.0)
            r0.preRotate(r1)
            int r0 = -r11
            int r1 = r14 + r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
            goto L_0x0032
        L_0x00d0:
            android.graphics.Matrix r0 = r9.iz
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            android.graphics.Matrix r0 = r9.iz
            r1 = -1014562816(0xffffffffc3870000, float:-270.0)
            r0.preRotate(r1)
            int r0 = -r12
            int r1 = -r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x0032
        L_0x00e8:
            r6 = r5 & 32
            if (r6 == 0) goto L_0x00f7
            r6 = r5 & 2
            if (r6 != 0) goto L_0x0044
            int r6 = r17 - r0
            r8 = r6
            r6 = r4
            r4 = r8
            goto L_0x0048
        L_0x00f7:
            r6 = r5 & 2
            if (r6 == 0) goto L_0x0044
            r6 = 1
            int r6 = r0 - r6
            int r6 = r6 >>> 1
            int r6 = r17 - r6
            r8 = r6
            r6 = r4
            r4 = r8
            goto L_0x0048
        L_0x0107:
            r7 = r5 & 8
            if (r7 == 0) goto L_0x0113
            r5 = r5 & 1
            if (r5 != 0) goto L_0x0050
            int r5 = r16 - r1
            goto L_0x0054
        L_0x0113:
            r5 = r5 & 1
            if (r5 == 0) goto L_0x0050
            r5 = 1
            int r5 = r1 - r5
            int r5 = r5 >>> 1
            int r5 = r16 - r5
            goto L_0x0054
        L_0x0120:
            android.graphics.Canvas r6 = r9.iw
            r6.save()
            android.graphics.Canvas r6 = r9.iw
            int r1 = r1 + r5
            int r0 = r0 + r4
            r6.clipRect(r5, r4, r1, r0)
            android.graphics.Canvas r0 = r9.iw
            int r1 = r5 + r2
            float r1 = (float) r1
            int r2 = r4 + r3
            float r2 = (float) r2
            r0.translate(r1, r2)
            android.graphics.Canvas r0 = r9.iw
            android.graphics.Bitmap r1 = r10.fh
            android.graphics.Matrix r2 = r9.iz
            r3 = 0
            r0.drawBitmap(r1, r2, r3)
            android.graphics.Canvas r0 = r9.iw
            r0.restore()
            return
        L_0x0147:
            r5 = r16
            goto L_0x0054
        L_0x014b:
            r6 = r4
            r4 = r17
            goto L_0x0048
        L_0x0150:
            r5 = r18
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.df.a(ao, int, int, int, int, int, int, int, int):void");
    }

    public final void a(String str, int i, int i2, int i3) {
        if (this.iD > 0 && this.iE > 0) {
            int i4 = i3 == 0 ? 20 : i3;
            int i5 = (i4 & 16) != 0 ? i2 - this.iu.ip.top : (i4 & 32) != 0 ? i2 - this.iu.ip.bottom : (i4 & 2) != 0 ? (((this.iu.ip.descent - this.iu.ip.ascent) / 2) - this.iu.ip.descent) + i2 : i2;
            if ((i4 & 1) != 0) {
                this.iu.ew.setTextAlign(Paint.Align.CENTER);
            } else if ((i4 & 8) != 0) {
                this.iu.ew.setTextAlign(Paint.Align.RIGHT);
            } else if ((i4 & 4) != 0) {
                this.iu.ew.setTextAlign(Paint.Align.LEFT);
            }
            this.iu.ew.setColor(this.is.getColor());
            this.iw.drawText(str, (float) i, (float) i5, this.iu.ew);
        }
    }

    public final void aH() {
        a(this.iw);
    }

    public final void b(int i, int i2, int i3, int i4) {
        if (this.iD > 0 && this.iE > 0) {
            this.iw.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.is);
        }
    }

    public final void b(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.iD > 0 && this.iE > 0) {
            this.iw.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), 0.0f, 360.0f, true, this.it);
        }
    }

    public final void c(int i, int i2, int i3, int i4) {
        if (this.iD > 0 && this.iE > 0) {
            this.iw.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.it);
        }
    }

    public final void c(int i, int i2, int i3, int i4, int i5, int i6) {
        if (this.iD > 0 && this.iE > 0) {
            Path path = new Path();
            path.moveTo((float) i, (float) i2);
            path.lineTo((float) i3, (float) i4);
            path.lineTo((float) i5, (float) i6);
            path.lineTo((float) i, (float) i2);
            this.iw.drawPath(path, this.it);
        }
    }

    public final void d(int i, int i2, int i3, int i4) {
        this.iB = i;
        this.iC = i2;
        this.iD = i3;
        this.iE = i4;
        if (i3 > 0 && i4 > 0) {
            this.iw.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.REPLACE);
        }
    }

    public final void setColor(int i) {
        this.is.setColor(-16777216 | i);
        this.it.setColor(-16777216 | i);
        super.setColor(i);
    }

    public final void translate(int i, int i2) {
        super.translate(i, i2);
        this.iw.translate((float) i, (float) i2);
    }
}
