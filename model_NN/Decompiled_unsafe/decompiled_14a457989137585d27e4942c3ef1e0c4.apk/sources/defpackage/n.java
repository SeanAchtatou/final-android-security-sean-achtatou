package defpackage;

import com.network.app.data.Database;
import javax.microedition.media.control.ToneControl;

/* renamed from: n  reason: default package */
public abstract class n {
    public boolean M = false;
    public short P;
    public byte R = 0;
    public short S;
    public short T;
    public short U = 0;
    public short V = -6;
    public byte W;
    public int aH;
    public int ac;
    public short ad = 6;
    public boolean ae;
    public short ag = -6;
    public short ao = 0;
    public int dN;
    public int dO;
    private byte[][] dP;
    private byte[][][] dQ;
    public byte[][] dR;
    public short dS = -6;
    public short dT = 6;
    public short dU = -6;
    public short dV = 12;
    public short dW = 6;
    public boolean dX;
    public boolean dY;
    public boolean dZ;
    public boolean ea;
    public int eb;
    public int ec;
    public int ed;
    public int ee;
    public int ef;
    public int eg;
    public int eh;
    public int ei;
    public int ej;
    public int ek;
    public int el;
    public boolean em;
    public byte f;
    public byte g;
    public byte h = 4;
    public byte i;
    public byte j = 1;
    public short k;
    public short l;
    private ao q;

    public n(ao aoVar, byte[][] bArr, byte[][][] bArr2, byte[][] bArr3) {
        this.q = aoVar;
        this.dP = bArr;
        this.dQ = bArr2;
        this.dR = bArr3;
    }

    public static n a(n nVar) {
        switch (nVar.k) {
            case 2:
                return new dv(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
            case 4:
                return new dw(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
            case 8:
                return new dx(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
            case 16:
                return new d(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
            case 32:
                return new bk(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
            case 64:
                return new m(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
            default:
                return new c(nVar.q, nVar.dP, nVar.dQ, nVar.dR);
        }
    }

    public abstract void N();

    public final void Q() {
        this.R = (byte) (this.R + 1);
        if (this.ac >= this.dR[this.dO].length) {
            this.R = this.j;
        }
        if (this.R >= this.j) {
            this.R = 0;
            int i2 = this.ac + 1;
            this.ac = i2;
            this.ac = i2 >= this.dR[this.dO].length ? 0 : this.ac;
        }
    }

    public final boolean W() {
        return this.ac >= this.dR[this.dO].length - 1;
    }

    public final void a(int i2) {
        this.dO = i2;
        this.ac = 0;
        this.R = 0;
    }

    public final void a(int i2, int i3) {
        this.aH = i2;
        this.dN = i3;
    }

    public void a(an anVar) {
        byte b = this.dR[this.dO][this.ac] & ToneControl.SILENCE;
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.dQ[b].length) {
            int i4 = (this.aH + this.dQ[b][i3][2]) - ea.ac;
            int i5 = (this.dN + this.dQ[b][i3][3]) - ea.aH;
            switch (this.dQ[b][i3][1]) {
                case 0:
                    i2 = 0;
                    break;
                case 1:
                    i2 = 1;
                    i5 -= this.dP[this.dQ[b][i3][0]][0];
                    break;
                case 2:
                    i2 = 2;
                    i4 -= this.dP[this.dQ[b][i3][0]][1];
                    break;
                case 3:
                    i2 = 3;
                    i4 -= this.dP[this.dQ[b][i3][0]][1];
                    i5 -= this.dP[this.dQ[b][i3][0]][0];
                    break;
            }
            int i6 = i2;
            int i7 = i4;
            du.a(anVar, this.q, this.dP[this.dQ[b][i3][0]][2] & ToneControl.SILENCE, this.dP[this.dQ[b][i3][0]][3] & ToneControl.SILENCE, this.dP[this.dQ[b][i3][0]][1], this.dP[this.dQ[b][i3][0]][0], i6, i7, i5);
            i3++;
            i2 = i6;
        }
    }

    public final void a(boolean z) {
        this.M = !z;
    }

    public final void a(short[] sArr, byte[] bArr) {
        a(sArr[0], sArr[1]);
        this.dO = bArr[0];
        this.ae = (bArr[1] & 64) != 0;
        this.M = (bArr[1] & 1) != 0;
        this.dX = (bArr[1] & 2) != 0;
        this.dY = (bArr[1] & 4) != 0;
        this.dZ = (bArr[1] & 8) != 0;
        this.ea = (bArr[1] & 32) != 0;
        b(bArr);
    }

    public final boolean a(int i2, int i3, n nVar) {
        return this.ag + i3 < nVar.T + nVar.dN && this.U + i3 > nVar.l + nVar.dN && this.V + i2 < nVar.S + nVar.aH && this.ad + i2 > nVar.P + nVar.aH;
    }

    public final boolean am() {
        if (this.aH != this.eh || this.dN != this.ei) {
            return false;
        }
        this.el = 0;
        return true;
    }

    public abstract void b(byte[] bArr);

    public final boolean b(int i2, int i3, n nVar) {
        return this.ag + i3 < nVar.ao + nVar.dN && this.U + i3 > nVar.dU + nVar.dN && this.V + i2 < nVar.dT + nVar.aH && this.ad + i2 > nVar.dS + nVar.aH;
    }

    public abstract void c();

    public final void c(int i2) {
        this.dO = i2;
        switch (this.dO) {
            case 0:
                this.f = 0;
                this.g = 4;
                return;
            case 1:
                this.f = 1;
                this.g = 4;
                return;
            case 2:
                this.f = 2;
                this.g = 4;
                return;
            case 3:
                this.f = 3;
                this.g = 4;
                return;
            case 4:
                this.f = 0;
                this.g = 5;
                return;
            case 5:
                this.f = 1;
                this.g = 5;
                return;
            case 6:
                this.f = 2;
                this.g = 5;
                return;
            case 7:
                this.f = 3;
                this.g = 5;
                return;
            case 8:
                this.f = 1;
                this.g = 6;
                return;
            case 9:
                this.f = 0;
                this.g = 6;
                return;
            case 10:
                this.f = 2;
                this.g = 6;
                return;
            case 11:
                this.f = 3;
                this.g = 6;
                return;
            case 12:
                this.f = 1;
                this.g = 7;
                return;
            case 13:
                this.f = 0;
                this.g = 7;
                return;
            case Database.INT_ISFEE:
                this.f = 2;
                this.g = 7;
                return;
            case Database.INT_FEECUE:
                this.f = 3;
                this.g = 7;
                return;
            default:
                return;
        }
    }

    public final void c(an anVar) {
        if (!this.M) {
            byte b = this.dR[this.dO][this.ac] & ToneControl.SILENCE;
            int i2 = 0;
            int i3 = 0;
            while (i3 < this.dQ[b].length) {
                int i4 = this.aH + this.dQ[b][i3][2];
                int i5 = this.dN + this.dQ[b][i3][3];
                switch (this.dQ[b][i3][1]) {
                    case 0:
                        i2 = 0;
                        break;
                    case 1:
                        i2 = 1;
                        i5 -= this.dP[this.dQ[b][i3][0]][0];
                        break;
                    case 2:
                        i2 = 2;
                        i4 -= this.dP[this.dQ[b][i3][0]][1];
                        break;
                    case 3:
                        i2 = 3;
                        i4 -= this.dP[this.dQ[b][i3][0]][1];
                        i5 -= this.dP[this.dQ[b][i3][0]][0];
                        break;
                }
                int i6 = i2;
                int i7 = i4;
                du.a(anVar, this.q, this.dP[this.dQ[b][i3][0]][2] & ToneControl.SILENCE, this.dP[this.dQ[b][i3][0]][3] & ToneControl.SILENCE, this.dP[this.dQ[b][i3][0]][1], this.dP[this.dQ[b][i3][0]][0], i6, i7, i5);
                i3++;
                i2 = i6;
            }
        }
    }

    public final void d(int i2) {
        i(4, this.f);
    }

    public final void e(int i2) {
        this.W = (byte) i2;
    }

    public final void f(int i2) {
        this.el++;
        this.ef = this.el * this.W;
        if (i2 == 0) {
            this.aH = Math.max(this.eh, this.ed - this.ef);
            if (this.ek != 0) {
                this.eg = ((this.ek * this.W) / this.ej) * this.el;
                if (this.ee > this.ei) {
                    this.dN = Math.max(this.ei, this.ee - this.eg);
                } else if (this.ee < this.ei) {
                    this.dN = Math.min(this.ei, this.ee + this.eg);
                }
            }
        } else {
            this.aH = Math.min(this.eh, this.ed + this.ef);
            if (this.ek != 0) {
                this.eg = ((this.ek * this.W) / this.ej) * this.el;
                if (this.ee > this.ei) {
                    this.dN = Math.max(this.ei, this.ee - this.eg);
                } else if (this.ee < this.ei) {
                    this.dN = Math.min(this.ei, this.ee + this.eg);
                }
            }
        }
    }

    public final void g(int i2, int i3) {
        this.eb = i2;
        this.ec = i3;
    }

    public final void h(int i2, int i3) {
        this.aH += i2;
        this.dN += i3;
    }

    public final void i(int i2, int i3) {
        switch (i3) {
            case 0:
                switch (i2) {
                    case 4:
                        c(0);
                        return;
                    case 5:
                        c(4);
                        return;
                    case 6:
                        c(9);
                        return;
                    case 7:
                        c(13);
                        return;
                    default:
                        return;
                }
            case 1:
                switch (i2) {
                    case 4:
                        c(1);
                        return;
                    case 5:
                        c(5);
                        return;
                    case 6:
                        c(8);
                        return;
                    case 7:
                        c(12);
                        return;
                    default:
                        return;
                }
            case 2:
                switch (i2) {
                    case 4:
                        c(2);
                        return;
                    case 5:
                        c(6);
                        return;
                    case 6:
                        c(10);
                        return;
                    case 7:
                        c(14);
                        return;
                    default:
                        return;
                }
            case 3:
                switch (i2) {
                    case 4:
                        c(3);
                        return;
                    case 5:
                        c(7);
                        return;
                    case 6:
                        c(11);
                        return;
                    case 7:
                        c(15);
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public final void j(int i2, int i3) {
        this.aH = i2;
        this.ed = i2;
        this.dN = i3;
        this.ee = i3;
    }

    public final void k(int i2, int i3) {
        this.eh = i2;
        this.ei = i3;
        this.ej = Math.abs(this.eh - this.ed);
        this.ek = Math.abs(this.ei - this.ee);
    }
}
