package defpackage;

import com.google.ads.AdActivity;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: bj  reason: default package */
public final class bj {
    public static ao[] I = new ao[3];
    public static byte[] L = null;
    public static byte[] N;
    public static String[] aC = null;
    public static String[] aE = null;
    public static int ac;
    public static byte[][] dP = null;
    public static byte[][] dR = null;
    public static byte e;
    public static byte[][] eq = null;
    public static byte[][] es = null;
    public static byte[][] et = null;
    public static short[][] ev = null;
    public static short[][] fF = null;
    public static boolean[] fY;
    public static short[][] fz = null;
    public static c gq;
    public static c gr;
    private static bj gt;
    public static String[] gu = null;
    public static String[] gv = null;
    public static short[][] gw = null;
    public static short[][] gx = null;
    private static av gy;
    public static byte[][] gz = {new byte[]{0, 0, 0}, new byte[]{0, 0, 0}, new byte[]{0, 0, 0}};
    public int aH;
    public int dN;
    public int dO;
    public int eb;
    public byte[][] er;
    private byte f = 0;
    public short[][] fA = null;
    public short[][] ga;
    public String[] gs = null;
    public byte[] m;
    public n[] p;

    private bj() {
        c();
    }

    private void J() {
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/enemyName.hy"));
        try {
            gu = new String[dataInputStream.readShort()];
            for (int i = 0; i < gu.length; i++) {
                gu[i] = dataInputStream.readUTF();
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    public static void N() {
        if (gq == null) {
            gq = c.g("/u/face");
        }
    }

    public static void P() {
        if (gr == null) {
            gr = c.g("/n/59");
        }
    }

    public static void Q() {
        for (byte b = 0; b < 3; b = (byte) (b + 1)) {
            if (I[b] == null) {
                I[b] = ee.O(new StringBuffer().append("/u/n").append((int) b).toString());
            }
        }
    }

    public static boolean W() {
        try {
            gy = av.a("mn2_s", true);
            int aT = gy.aT();
            byte[] bArr = new byte[b.m.length];
            for (int i = 0; i < b.m.length; i++) {
                bArr[i] = b.m[i];
            }
            if (aT <= 0) {
                gy.b(bArr, 0, bArr.length);
            } else {
                gy.a(1, bArr, 0, bArr.length);
            }
            gy.aS();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private void X() {
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/box.hy"));
        try {
            gx = new short[dataInputStream.readShort()][];
            for (int i = 0; i < gx.length; i++) {
                gx[i] = new short[dataInputStream.readShort()];
                for (byte b = 0; b < gx[i].length; b = (byte) (b + 1)) {
                    gx[i][b] = dataInputStream.readShort();
                }
            }
            L = new byte[dataInputStream.readByte()];
            for (int i2 = 0; i2 < L.length; i2++) {
                L[i2] = dataInputStream.readByte();
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    private void Y() {
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/hs.hy"));
        try {
            int readByte = dataInputStream.readByte();
            dR = new byte[readByte][];
            if (dP == null) {
                dP = new byte[readByte][];
            }
            for (int i = 0; i < readByte; i++) {
                int readByte2 = dataInputStream.readByte();
                dR[i] = new byte[readByte2];
                if (dP[i] == null) {
                    dP[i] = new byte[readByte2];
                }
                for (int i2 = 0; i2 < readByte2; i2++) {
                    dR[i][i2] = dataInputStream.readByte();
                }
            }
            byte readByte3 = dataInputStream.readByte();
            eq = new byte[readByte][];
            for (int i3 = 0; i3 < readByte3; i3++) {
                int readByte4 = dataInputStream.readByte();
                eq[i3] = new byte[readByte4];
                for (int i4 = 0; i4 < readByte4; i4++) {
                    eq[i3][i4] = dataInputStream.readByte();
                }
            }
            int readByte5 = dataInputStream.readByte();
            aE = new String[readByte5];
            fz = new short[readByte5][];
            for (int i5 = 0; i5 < readByte5; i5++) {
                aE[i5] = dataInputStream.readUTF();
            }
            for (int i6 = 0; i6 < readByte5; i6++) {
                int readByte6 = dataInputStream.readByte();
                fz[i6] = new short[readByte6];
                for (int i7 = 0; i7 < readByte6; i7++) {
                    fz[i6][i7] = dataInputStream.readShort();
                }
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    public static void a(int i) {
        ac = i;
        Q();
    }

    public static boolean aW() {
        try {
            av a = av.a("mn2_s", true);
            gy = a;
            if (a.aT() <= 0) {
                return false;
            }
            if (gy.o(1) == 0) {
                return false;
            }
            b.m = gy.p(1);
            gy.aS();
            System.gc();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private void b() {
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/enemyList.hy"));
        try {
            fF = new short[dataInputStream.readShort()][];
            for (short s = 0; s < fF.length; s = (short) (s + 1)) {
                fF[s] = new short[dataInputStream.readShort()];
                for (short s2 = 0; s2 < fF[s].length; s2 = (short) (s2 + 1)) {
                    fF[s][s2] = dataInputStream.readShort();
                }
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    private static void b(DataInputStream dataInputStream) {
        byte b = 0;
        while (b < gz.length) {
            try {
                for (byte b2 = 0; b2 < gz[b].length; b2 = (byte) (b2 + 1)) {
                    gz[b][b2] = dataInputStream.readByte();
                }
                b = (byte) (b + 1);
            } catch (IOException e2) {
                return;
            }
        }
    }

    public static bj bp() {
        if (gt == null) {
            gt = new bj();
        }
        return gt;
    }

    private static byte[] bq() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            dataOutputStream.writeByte(ac);
            dataOutputStream.writeByte(dz.e);
            dataOutputStream.writeBoolean(eb.M);
            for (byte writeByte : N) {
                dataOutputStream.writeByte(writeByte);
            }
            dataOutputStream.writeShort(fY.length);
            for (byte b = 0; b < fY.length; b = (byte) (b + 1)) {
                dataOutputStream.writeBoolean(fY[b]);
            }
            dataOutputStream.writeByte(eb.m.length);
            for (byte b2 = 0; b2 < eb.m.length; b2 = (byte) (b2 + 1)) {
                dataOutputStream.writeByte(eb.m[b2]);
            }
            dataOutputStream.writeShort(eb.ke[eb.aH].aH);
            dataOutputStream.writeShort(eb.ke[eb.aH].dN);
            dataOutputStream.writeByte(eb.ke[eb.aH].dO);
            dataOutputStream.writeInt(eb.ac);
            dataOutputStream.writeByte(dz.dQ.length);
            for (int i = 0; i < dz.dQ.length; i++) {
                int length = dz.dQ[i].length;
                dataOutputStream.writeByte(length);
                for (int i2 = 0; i2 < length; i2++) {
                    dataOutputStream.writeByte(r6);
                    for (byte writeByte2 : dz.dQ[i][i2]) {
                        dataOutputStream.writeByte(writeByte2);
                    }
                }
            }
            dataOutputStream.writeByte(dP.length);
            for (int i3 = 0; i3 < dP.length; i3++) {
                dataOutputStream.writeByte(r4);
                for (byte writeByte3 : dP[i3]) {
                    dataOutputStream.writeByte(writeByte3);
                }
            }
            dataOutputStream.writeByte(eb.ai.length);
            for (int i4 = 0; i4 < eb.ai.length; i4++) {
                dataOutputStream.writeByte(eb.ai[i4].length);
                for (int i5 = 0; i5 < eb.ai[i4].length; i5++) {
                    dataOutputStream.writeByte(eb.ai[i4][i5].length);
                    for (short writeByte4 : eb.ai[i4][i5]) {
                        dataOutputStream.writeByte(writeByte4);
                    }
                }
            }
            for (byte b3 = 0; b3 < 3; b3 = (byte) (b3 + 1)) {
                dataOutputStream.writeInt(eb.aI[b3]);
            }
            for (byte b4 = 0; b4 < 3; b4 = (byte) (b4 + 1)) {
                dataOutputStream.writeByte(eb.O[b4]);
            }
            for (byte b5 = 0; b5 < 3; b5 = (byte) (b5 + 1)) {
                dataOutputStream.writeShort(eb.ab[b5]);
            }
            for (byte b6 = 0; b6 < 3; b6 = (byte) (b6 + 1)) {
                dataOutputStream.writeShort(eb.aq[b6]);
            }
            for (byte b7 = 0; b7 < 3; b7 = (byte) (b7 + 1)) {
                for (byte b8 = 0; b8 < 3; b8 = (byte) (b8 + 1)) {
                    for (byte b9 = 0; b9 < 2; b9 = (byte) (b9 + 1)) {
                        dataOutputStream.writeByte(eb.dQ[b7][b8][b9]);
                    }
                }
            }
            dataOutputStream.writeByte(eb.fz.length);
            for (int i6 = 0; i6 < eb.fz.length; i6++) {
                dataOutputStream.writeByte(eb.fz[i6].length);
                for (short writeShort : eb.fz[i6]) {
                    dataOutputStream.writeShort(writeShort);
                }
            }
            dataOutputStream.writeByte(eb.fA.length);
            for (int i7 = 0; i7 < eb.fA.length; i7++) {
                dataOutputStream.writeByte(eb.fA[i7].length);
                for (short writeShort2 : eb.fA[i7]) {
                    dataOutputStream.writeShort(writeShort2);
                }
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            dataOutputStream.close();
            byteArrayOutputStream.close();
            return byteArray;
        } catch (IOException e2) {
            return null;
        }
    }

    private void c(DataInputStream dataInputStream) {
        eb.cq();
        try {
            byte readByte = dataInputStream.readByte();
            ac = readByte;
            this.eb = readByte;
            dz.e = dataInputStream.readByte();
            eb.M = dataInputStream.readBoolean();
            for (int i = 0; i < N.length; i++) {
                N[i] = dataInputStream.readByte();
            }
            fY = new boolean[dataInputStream.readShort()];
            for (byte b = 0; b < fY.length; b = (byte) (b + 1)) {
                fY[b] = dataInputStream.readBoolean();
            }
            int readByte2 = dataInputStream.readByte();
            eb.m = new byte[readByte2];
            for (byte b2 = 0; b2 < readByte2; b2 = (byte) (b2 + 1)) {
                eb.m[b2] = dataInputStream.readByte();
            }
            eb.aH = eb.m[0];
            this.aH = dataInputStream.readShort();
            this.dN = dataInputStream.readShort();
            this.dO = dataInputStream.readByte();
            eb.ac = dataInputStream.readInt();
            dz.dQ = new byte[dataInputStream.readByte()][][];
            for (int i2 = 0; i2 < dz.dQ.length; i2++) {
                int readByte3 = dataInputStream.readByte();
                dz.dQ[i2] = new byte[readByte3][];
                for (int i3 = 0; i3 < readByte3; i3++) {
                    int readByte4 = dataInputStream.readByte();
                    dz.dQ[i2][i3] = new byte[readByte4];
                    for (int i4 = 0; i4 < readByte4; i4++) {
                        dz.dQ[i2][i3][i4] = dataInputStream.readByte();
                    }
                }
            }
            int readByte5 = dataInputStream.readByte();
            dP = new byte[readByte5][];
            for (int i5 = 0; i5 < readByte5; i5++) {
                int readByte6 = dataInputStream.readByte();
                dP[i5] = new byte[readByte6];
                for (int i6 = 0; i6 < readByte6; i6++) {
                    dP[i5][i6] = dataInputStream.readByte();
                }
            }
            int readByte7 = dataInputStream.readByte();
            eb.ai = new short[readByte7][][];
            for (int i7 = 0; i7 < readByte7; i7++) {
                int readByte8 = dataInputStream.readByte();
                eb.ai[i7] = new short[readByte8][];
                for (int i8 = 0; i8 < readByte8; i8++) {
                    eb.ai[i7][i8] = new short[dataInputStream.readByte()];
                    for (int i9 = 0; i9 < eb.ai[i7][i8].length; i9++) {
                        eb.ai[i7][i8][i9] = (short) dataInputStream.readByte();
                    }
                }
            }
            for (byte b3 = 0; b3 < 3; b3 = (byte) (b3 + 1)) {
                eb.aI[b3] = dataInputStream.readInt();
            }
            for (byte b4 = 0; b4 < 3; b4 = (byte) (b4 + 1)) {
                eb.O[b4] = (short) dataInputStream.readByte();
            }
            for (byte b5 = 0; b5 < 3; b5 = (byte) (b5 + 1)) {
                eb.ab[b5] = dataInputStream.readShort();
            }
            for (byte b6 = 0; b6 < 3; b6 = (byte) (b6 + 1)) {
                eb.aq[b6] = dataInputStream.readShort();
            }
            for (byte b7 = 0; b7 < 3; b7 = (byte) (b7 + 1)) {
                for (byte b8 = 0; b8 < 3; b8 = (byte) (b8 + 1)) {
                    for (byte b9 = 0; b9 < 2; b9 = (byte) (b9 + 1)) {
                        eb.dQ[b7][b8][b9] = dataInputStream.readByte();
                    }
                }
            }
            int readByte9 = dataInputStream.readByte();
            eb.fz = new short[readByte9][];
            for (int i10 = 0; i10 < readByte9; i10++) {
                int readByte10 = dataInputStream.readByte();
                eb.fz[i10] = new short[readByte10];
                for (int i11 = 0; i11 < readByte10; i11++) {
                    eb.fz[i10][i11] = dataInputStream.readShort();
                }
            }
            int readByte11 = dataInputStream.readByte();
            eb.fA = new short[readByte11][];
            for (int i12 = 0; i12 < readByte11; i12++) {
                int readByte12 = dataInputStream.readByte();
                eb.fA[i12] = new short[readByte12];
                for (int i13 = 0; i13 < readByte12; i13++) {
                    eb.fA[i12][i13] = dataInputStream.readShort();
                }
            }
        } catch (IOException e2) {
        }
    }

    private void d() {
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/eq.hy"));
        try {
            int readShort = dataInputStream.readShort();
            gv = new String[readShort];
            gw = new short[readShort][];
            for (short s = 0; s < readShort; s = (short) (s + 1)) {
                gv[s] = dataInputStream.readUTF();
            }
            for (short s2 = 0; s2 < readShort; s2 = (short) (s2 + 1)) {
                int readShort2 = dataInputStream.readShort();
                gw[s2] = new short[readShort2];
                for (int i = 0; i < readShort2; i++) {
                    gw[s2][i] = dataInputStream.readShort();
                }
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    public static void d(int i) {
        dz.cn().a(i);
    }

    private void f(int i) {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(new StringBuffer().append("/n/").append((int) this.m[i]).append(".hy").toString()));
            int readUnsignedByte = dataInputStream.readUnsignedByte();
            byte[][] bArr = (byte[][]) Array.newInstance(Byte.TYPE, readUnsignedByte, 4);
            for (int i2 = 0; i2 < readUnsignedByte; i2++) {
                for (int i3 = 0; i3 < 4; i3++) {
                    bArr[i2][i3] = dataInputStream.readByte();
                }
            }
            byte[][][] bArr2 = new byte[dataInputStream.readUnsignedByte()][][];
            for (int i4 = 0; i4 < bArr2.length; i4++) {
                int readUnsignedByte2 = dataInputStream.readUnsignedByte();
                bArr2[i4] = (byte[][]) Array.newInstance(Byte.TYPE, readUnsignedByte2, 4);
                for (int i5 = 0; i5 < readUnsignedByte2; i5++) {
                    for (int i6 = 0; i6 < 4; i6++) {
                        bArr2[i4][i5][i6] = dataInputStream.readByte();
                    }
                }
            }
            int readUnsignedByte3 = dataInputStream.readUnsignedByte();
            byte[][] bArr3 = new byte[readUnsignedByte3][];
            for (int i7 = 0; i7 < readUnsignedByte3; i7++) {
                int readUnsignedByte4 = dataInputStream.readUnsignedByte();
                byte[] bArr4 = new byte[readUnsignedByte4];
                int i8 = 0;
                for (int i9 = 0; i9 < readUnsignedByte4; i9++) {
                    bArr4[i9] = dataInputStream.readByte();
                    if (i9 < (readUnsignedByte4 >> 1)) {
                        i8 += bArr4[i9];
                    }
                }
                bArr3[i7] = new byte[i8];
                int i10 = 0;
                int i11 = 0;
                while (i11 < (readUnsignedByte4 >> 1)) {
                    int i12 = i10;
                    int i13 = 0;
                    while (i13 < bArr4[i11]) {
                        bArr3[i7][i12] = bArr4[(readUnsignedByte4 >> 1) + i11];
                        i13++;
                        i12++;
                    }
                    i11++;
                    i10 = i12;
                }
            }
            dataInputStream.close();
            ao O = ee.O(new StringBuffer().append("/n/").append((int) this.m[i]).toString());
            if ((this.er[i][1] & 32) != 0) {
                this.p[i] = new bk(O, bArr, bArr2, bArr3);
            } else if ((this.er[i][1] & 2) != 0) {
                this.p[i] = new dv(O, bArr, bArr2, bArr3);
            } else if ((this.er[i][1] & 4) != 0) {
                this.p[i] = new dw(O, bArr, bArr2, bArr3);
            } else if ((this.er[i][1] & 16) != 0) {
                this.p[i] = new d(O, bArr, bArr2, bArr3);
            } else if ((this.er[i][1] & 8) != 0) {
                this.p[i] = new dx(O, bArr, bArr2, bArr3);
            } else if ((this.er[i][1] & 64) != 0) {
                this.p[i] = new m(O, bArr, bArr2, bArr3);
            }
            this.p[i].a(this.ga[i], this.er[i]);
        } catch (Exception e2) {
        }
    }

    private void o() {
        getClass();
        DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m("/d/shop.hy"));
        try {
            es = new byte[dataInputStream.readByte()][];
            for (byte b = 0; b < es.length; b = (byte) (b + 1)) {
                es[b] = new byte[dataInputStream.readByte()];
                for (byte b2 = 0; b2 < es[b].length; b2 = (byte) (b2 + 1)) {
                    es[b][b2] = dataInputStream.readByte();
                }
            }
            et = new byte[dataInputStream.readByte()][];
            for (byte b3 = 0; b3 < et.length; b3 = (byte) (b3 + 1)) {
                et[b3] = new byte[dataInputStream.readByte()];
                for (byte b4 = 0; b4 < et[b3].length; b4 = (byte) (b4 + 1)) {
                    et[b3][b4] = dataInputStream.readByte();
                }
            }
            dataInputStream.close();
        } catch (IOException e2) {
        }
    }

    private boolean q(int i) {
        for (int i2 = i - 1; i2 >= 0; i2--) {
            if (this.m[i] == this.m[i2] && this.er[i][1] == this.er[i2][1]) {
                this.p[i] = n.a(this.p[i2]);
                this.p[i].a(this.ga[i], this.er[i]);
                return true;
            }
        }
        return false;
    }

    private static byte[] z(int i) {
        gz[i][0] = eb.m[0];
        gz[i][1] = (byte) eb.O[eb.m[0]];
        gz[i][2] = (byte) ac;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        byte b = 0;
        while (b < gz.length) {
            try {
                for (byte b2 = 0; b2 < gz[b].length; b2 = (byte) (b2 + 1)) {
                    dataOutputStream.writeByte(gz[b][b2]);
                }
                b = (byte) (b + 1);
            } catch (IOException e2) {
                return null;
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        dataOutputStream.close();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public final void R() {
        if (this.p != null) {
            for (int i = 0; i < this.p.length; i++) {
                if (this.p[i] != null) {
                    this.p[i] = null;
                }
            }
        }
        this.m = null;
        this.ga = null;
        this.er = null;
        dz.cn().c();
    }

    public final void S() {
        if (ev == null || aC == null) {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(new StringBuffer().append("/d/").append("hero").append(".hy").toString()));
            try {
                int readByte = dataInputStream.readByte();
                this.gs = new String[readByte];
                for (int i = 0; i < readByte; i++) {
                    this.gs[i] = dataInputStream.readUTF();
                }
                int readByte2 = dataInputStream.readByte();
                this.fA = (short[][]) Array.newInstance(Short.TYPE, readByte, readByte2);
                for (int i2 = 0; i2 < readByte; i2++) {
                    for (int i3 = 0; i3 < readByte2; i3++) {
                        this.fA[i2][i3] = dataInputStream.readShort();
                    }
                }
                dataInputStream.close();
            } catch (IOException e2) {
            }
            aC = this.gs;
            ev = this.fA;
        }
        if (dR == null || aE == null) {
            Y();
        }
        if (gu == null) {
            J();
        }
        if (fF == null) {
            b();
        }
        if (r.aC == null) {
            r.ap().c();
        }
        if (r.ai == null) {
            r.ap().N();
        }
        if (gv == null) {
            d();
        }
        if (es == null) {
            o();
        }
        if (gx == null) {
            X();
        }
    }

    public final void c() {
        N = new byte[128];
        fY = new boolean[104];
        this.eb = 24;
    }

    public final void c(int i) {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(new StringBuffer().append("/w/").append(i).append(".hy").toString()));
            ec.cA().e = dataInputStream.readByte();
            ec.cA().aF = new StringBuffer().append((int) dataInputStream.readByte()).append(AdActivity.TYPE_PARAM).toString();
            ec.cA().aG = new StringBuffer().append((int) dataInputStream.readByte()).append("c").toString();
            ec cA = ec.cA();
            cA.P();
            cA.a(new StringBuffer().append("/m/").append((int) cA.e).append(".hy").toString());
            cA.eb = cA.q.getWidth() / 16;
            cA.ec = cA.r.getWidth() / 16;
            if (cA.s == null) {
                cA.eh = 240;
                cA.ei = 320;
                cA.el = ((cA.eh + (cA.dN >> 1)) / cA.dN) + 2;
                cA.jX = ((cA.ei + (cA.dO >> 1)) / cA.dO) + 2;
                cA.ej = cA.el * cA.dN;
                cA.ek = cA.jX * cA.dO;
                cA.kh = cA.dN * 2;
                cA.ki = cA.dO * 2;
                cA.s = ao.o(cA.ej, cA.ek);
                cA.kj = cA.s.aJ();
            }
            int readUnsignedByte = dataInputStream.readUnsignedByte();
            this.m = new byte[readUnsignedByte];
            this.ga = (short[][]) Array.newInstance(Short.TYPE, readUnsignedByte, 2);
            this.er = new byte[readUnsignedByte][];
            for (int i2 = 0; i2 < readUnsignedByte; i2++) {
                this.m[i2] = dataInputStream.readByte();
                this.ga[i2][0] = dataInputStream.readShort();
                this.ga[i2][1] = dataInputStream.readShort();
                int readByte = dataInputStream.readByte();
                if (readByte != 0) {
                    this.er[i2] = new byte[readByte];
                    for (int i3 = 0; i3 < readByte; i3++) {
                        this.er[i2][i3] = dataInputStream.readByte();
                    }
                } else {
                    this.er[i2] = new byte[]{0, 0};
                }
            }
            dataInputStream.close();
        } catch (Exception e2) {
        }
        this.p = new n[this.m.length];
        int i4 = 0;
        while (i4 < this.er.length) {
            try {
                if (!q(i4)) {
                    f(i4);
                }
                i4++;
            } catch (RuntimeException e3) {
                e3.printStackTrace();
                return;
            }
        }
    }

    public final boolean u(String str) {
        byte[] z;
        try {
            gy = av.a(str, true);
            boolean z2 = gy.aT() <= 0;
            if (str.equals("mn2_l1")) {
                this.f = 0;
                z = bq();
            } else if (str.equals("mn2_l2")) {
                this.f = 1;
                z = bq();
            } else if (str.equals("mn2_l3")) {
                this.f = 2;
                z = bq();
            } else {
                z = z(this.f);
            }
            if (z2) {
                gy.b(z, 0, z.length);
            } else {
                gy.a(1, z, 0, z.length);
            }
            gy.aS();
            return str.equals("mn2_llv") || u("mn2_llv");
        } catch (Exception e2) {
            return false;
        }
    }

    public final boolean v(String str) {
        try {
            av a = av.a(str, true);
            gy = a;
            if (a.aT() <= 0) {
                return false;
            }
            if (gy.o(1) == 0) {
                return false;
            }
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(gy.p(1)));
            if (str.equals("mn2_llv")) {
                b(dataInputStream);
            } else {
                c(dataInputStream);
            }
            gy.aS();
            gy = null;
            System.gc();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }
}
