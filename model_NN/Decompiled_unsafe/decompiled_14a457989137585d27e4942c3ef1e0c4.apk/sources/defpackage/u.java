package defpackage;

import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: u  reason: default package */
public final class u {
    public static final int READ = 1;
    public static final int READ_WRITE = 3;
    public static final int WRITE = 2;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.plugin.device.MIDPDevice.a(java.lang.String, int, boolean):s
     arg types: [java.lang.String, int, int]
     candidates:
      org.meteoroid.plugin.device.MIDPDevice.a(int, float, float):void
      org.meteoroid.plugin.vd.ScreenWidget.a(int, float, float):void
      org.meteoroid.plugin.device.MIDPDevice.a(java.lang.String, int, boolean):s */
    public static s n(String str) {
        cw cwVar = bp.gC;
        return MIDPDevice.a(str, 3, true);
    }
}
