package defpackage;

import android.util.Log;
import java.util.HashMap;

/* renamed from: bl  reason: default package */
public final class bl {
    public static final String LOG_TAG = "ApplicationManager";
    public static final int MSG_APPLICATION_LAUNCH = 47617;
    public static final int MSG_APPLICATION_PAUSE = 47619;
    public static final int MSG_APPLICATION_QUIT = 47621;
    public static final int MSG_APPLICATION_RESUME = 47620;
    public static final int MSG_APPLICATION_START = 47618;
    public static bn gA;
    private static final HashMap gB = new HashMap();

    protected static void br() {
        cf.a((int) MSG_APPLICATION_LAUNCH, "MSG_APPLICATION_LAUNCH");
        cf.a((int) MSG_APPLICATION_START, "MSG_APPLICATION_START");
        cf.a((int) MSG_APPLICATION_PAUSE, "MSG_APPLICATION_PAUSE");
        cf.a((int) MSG_APPLICATION_RESUME, "MSG_APPLICATION_RESUME");
        cf.a((int) MSG_APPLICATION_QUIT, "MSG_APPLICATION_QUIT");
        cf.a(new bm());
    }

    public static void g(String str, String str2) {
        gB.put(str, str2);
    }

    protected static void onDestroy() {
        if (gA != null && gA.getState() != 3) {
            gA.onDestroy();
            cf.b(cf.a((int) MSG_APPLICATION_QUIT, gA));
        }
    }

    public static void pause() {
        if (gA == null) {
            return;
        }
        if (gA.getState() == 1) {
            gA.onPause();
            cf.b(cf.a((int) MSG_APPLICATION_PAUSE, gA));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + gA.getState());
    }

    public static void resume() {
        if (gA == null) {
            return;
        }
        if (gA.getState() == 2) {
            gA.onResume();
            cf.b(cf.a((int) MSG_APPLICATION_RESUME, gA));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + gA.getState());
    }

    public static void start() {
        if (gA == null) {
            return;
        }
        if (gA.getState() == 0) {
            gA.onStart();
            cf.b(cf.a((int) MSG_APPLICATION_START, gA));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + gA.getState());
    }

    public static void w(String str) {
        if (str != null) {
            try {
                gA = (bn) Class.forName(str).newInstance();
                cf.b(cf.a((int) MSG_APPLICATION_LAUNCH, gA));
            } catch (Exception e) {
                Log.w(LOG_TAG, e.toString());
            }
            if (gA != null) {
                gA.aO();
            } else {
                Log.w(LOG_TAG, "The application failed to launch.");
            }
        } else {
            Log.w(LOG_TAG, "No available application could launch.");
        }
    }
}
