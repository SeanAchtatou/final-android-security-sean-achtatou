package defpackage;

/* renamed from: an  reason: default package */
public class an {
    public static final int BASELINE = 64;
    public static final int BOTTOM = 32;
    public static final int DOTTED = 1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int SOLID = 0;
    public static final int TOP = 16;
    public static final int VCENTER = 2;
    private static final StringBuffer fg = new StringBuffer();
    private int color;
    public int fe = 0;
    public int ff = 0;

    private static void aI() {
        try {
            throw new RuntimeException("Must be implemented in DisplayGraphics");
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public final void a(char c, int i, int i2, int i3) {
        fg.delete(0, fg.length());
        fg.append(c);
        a(fg.toString(), i, i2, i3);
    }

    public void a(int i, int i2, int i3, int i4) {
        aI();
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6) {
        aI();
    }

    public void a(am amVar) {
        aI();
    }

    public void a(ao aoVar, int i, int i2, int i3) {
        aI();
    }

    public void a(ao aoVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        aI();
    }

    public void a(String str, int i, int i2, int i3) {
        aI();
    }

    public final void a(char[] cArr, int i, int i2, int i3, int i4, int i5) {
        fg.delete(0, fg.length());
        fg.append(cArr, 0, i2);
        a(fg.toString(), 120, i4, 17);
    }

    public void aH() {
    }

    public void b(int i, int i2, int i3, int i4) {
        aI();
    }

    public void b(int i, int i2, int i3, int i4, int i5, int i6) {
        aI();
    }

    public void c(int i, int i2, int i3, int i4) {
        aI();
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6) {
        aI();
    }

    public void d(int i, int i2, int i3, int i4) {
    }

    public void setColor(int i) {
        this.color = i;
    }

    public void translate(int i, int i2) {
        this.fe += i;
        this.ff += i2;
    }
}
