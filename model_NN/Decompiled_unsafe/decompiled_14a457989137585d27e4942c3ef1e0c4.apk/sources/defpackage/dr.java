package defpackage;

import android.util.AttributeSet;
import java.util.Properties;

/* renamed from: dr  reason: default package */
public final class dr implements AttributeSet {
    private Properties jU;

    public dr(Properties properties) {
        this.jU = properties;
    }

    public final boolean getAttributeBooleanValue(int i, boolean z) {
        return false;
    }

    public final boolean getAttributeBooleanValue(String str, String str2, boolean z) {
        return this.jU.containsKey(new StringBuilder().append(str).append(str2).toString()) ? Boolean.parseBoolean(this.jU.getProperty(str + str2)) : z;
    }

    public final int getAttributeCount() {
        return this.jU.size();
    }

    public final float getAttributeFloatValue(int i, float f) {
        return 0.0f;
    }

    public final float getAttributeFloatValue(String str, String str2, float f) {
        return this.jU.containsKey(new StringBuilder().append(str).append(str2).toString()) ? Float.parseFloat(this.jU.getProperty(str + str2)) : f;
    }

    public final int getAttributeIntValue(int i, int i2) {
        return 0;
    }

    public final int getAttributeIntValue(String str, String str2, int i) {
        return this.jU.containsKey(new StringBuilder().append(str).append(str2).toString()) ? Integer.parseInt(this.jU.getProperty(str + str2)) : i;
    }

    public final int getAttributeListValue(int i, String[] strArr, int i2) {
        return 0;
    }

    public final int getAttributeListValue(String str, String str2, String[] strArr, int i) {
        return this.jU.containsKey(new StringBuilder().append(str).append(str2).toString()) ? this.jU.getProperty(str + str2).split(",").length : i;
    }

    public final String getAttributeName(int i) {
        return null;
    }

    public final int getAttributeNameResource(int i) {
        return 0;
    }

    public final int getAttributeResourceValue(int i, int i2) {
        return 0;
    }

    public final int getAttributeResourceValue(String str, String str2, int i) {
        return 0;
    }

    public final int getAttributeUnsignedIntValue(int i, int i2) {
        return 0;
    }

    public final int getAttributeUnsignedIntValue(String str, String str2, int i) {
        return 0;
    }

    public final String getAttributeValue(int i) {
        return null;
    }

    public final String getAttributeValue(String str, String str2) {
        return this.jU.getProperty(str + str2);
    }

    public final String getClassAttribute() {
        return null;
    }

    public final String getIdAttribute() {
        return null;
    }

    public final int getIdAttributeResourceValue(int i) {
        return 0;
    }

    public final String getPositionDescription() {
        return null;
    }

    public final int getStyleAttribute() {
        return 0;
    }
}
