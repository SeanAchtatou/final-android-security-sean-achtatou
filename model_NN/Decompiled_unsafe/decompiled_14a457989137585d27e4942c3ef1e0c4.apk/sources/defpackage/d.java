package defpackage;

import com.network.app.data.Database;

/* renamed from: d  reason: default package */
public final class d extends n {
    private short aR = 20;
    public n aS;
    private boolean aT = false;
    public byte e;
    public n n;
    public n o;

    public d(ao aoVar, byte[][] bArr, byte[][][] bArr2, byte[][] bArr3) {
        super(aoVar, bArr, bArr2, bArr3);
        this.k = 16;
        this.j = 3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean d(int r7, int r8) {
        /*
            r6 = this;
            r5 = 0
            ea r0 = defpackage.ea.cp()
            bj r1 = defpackage.bj.bp()
            r2 = r5
        L_0x000a:
            byte r3 = r0.f
            if (r2 < r3) goto L_0x0010
            r0 = r5
        L_0x000f:
            return r0
        L_0x0010:
            n[] r3 = r1.p
            byte[] r4 = r0.m
            byte r4 = r4[r2]
            r3 = r3[r4]
            boolean r3 = r3.M
            if (r3 != 0) goto L_0x0029
            n[] r3 = r1.p
            byte[] r4 = r0.m
            byte r4 = r4[r2]
            r3 = r3[r4]
            short r3 = r3.k
            switch(r3) {
                case 2: goto L_0x002c;
                case 8: goto L_0x002c;
                case 32: goto L_0x002c;
                default: goto L_0x0029;
            }
        L_0x0029:
            int r2 = r2 + 1
            goto L_0x000a
        L_0x002c:
            n[] r3 = r1.p
            byte[] r4 = r0.m
            byte r4 = r4[r2]
            r3 = r3[r4]
            boolean r3 = r6.b(r7, r8, r3)
            if (r3 == 0) goto L_0x0029
            n[] r1 = r1.p
            byte[] r0 = r0.m
            byte r0 = r0[r2]
            r0 = r1[r0]
            r6.n = r0
            r0 = 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.d.d(int, int):boolean");
    }

    private boolean f(int i, int i2) {
        int i3 = (this.dT + i) >> 4;
        int i4 = (this.dU + i2) >> 4;
        int i5 = (this.ao + i2) >> 4;
        for (int i6 = (this.dS + i) >> 4; i6 <= i3; i6++) {
            for (int i7 = i4; i7 <= i5; i7++) {
                byte x = ec.cA().x(i6, i7);
                switch (x) {
                    case -2:
                    case 0:
                        return true;
                    case 1:
                        if (this.dU + i2 >= (i7 << 4)) {
                            break;
                        } else {
                            return true;
                        }
                    case 2:
                        if (this.dS + i >= (i6 << 4)) {
                            break;
                        } else {
                            return true;
                        }
                    case 3:
                        if (this.ao + i2 <= ((i7 + 1) << 4)) {
                            break;
                        } else {
                            return true;
                        }
                    case 4:
                        if (this.dT + i <= ((i6 + 1) << 4)) {
                            break;
                        } else {
                            return true;
                        }
                    case 5:
                        if (this.dS + i >= (i6 << 4) && this.dU + i2 >= (i7 << 4)) {
                            break;
                        } else {
                            return true;
                        }
                        break;
                    case 6:
                        if (this.dS + i >= (i6 << 4) && this.ao + i2 <= ((i7 + 1) << 4)) {
                            break;
                        } else {
                            return true;
                        }
                        break;
                    case 7:
                        if (this.ao + i2 <= ((i7 + 1) << 4) && this.dT + i <= ((i6 + 1) << 4)) {
                            break;
                        } else {
                            return true;
                        }
                    case 8:
                        if (this.dT + i <= ((i6 + 1) << 4) && this.dU + i2 >= (i7 << 4)) {
                            break;
                        } else {
                            return true;
                        }
                        break;
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case Database.INT_ISFEE:
                    case Database.INT_FEECUE:
                    case 16:
                    case Database.INT_FEES:
                        switch (this.f) {
                            case 0:
                                h(0, -this.h);
                                break;
                            case 1:
                                h(0, this.h);
                                break;
                            case 2:
                                h(this.h, 0);
                                break;
                            case 3:
                                h(-this.h, 0);
                                break;
                        }
                        d(4);
                        bi.ac = 0;
                        dz.cn().c(x);
                        break;
                }
            }
        }
        return false;
    }

    public final void N() {
        Q();
    }

    public final void P() {
        switch (this.f) {
            case 0:
                h(0, this.h);
                if (f(this.aH, this.dN)) {
                    h(0, ((-(this.dN + this.ao)) % 16) - 1);
                    this.aT = true;
                }
                if (d(this.aH, this.dN)) {
                    h(0, ((this.n.dN + this.n.dU) - this.dN) - this.U);
                    this.aT = true;
                    break;
                }
                break;
            case 1:
                h(0, -this.h);
                if (f(this.aH, this.dN)) {
                    h(0, 16 - (((this.dU + this.dN) + 16) % 16));
                    this.aT = true;
                }
                if (d(this.aH, this.dN)) {
                    h(0, ((this.n.dN + this.n.ao) - this.dN) - this.ag);
                    this.aT = true;
                    break;
                }
                break;
            case 2:
                h(-this.h, 0);
                if (f(this.aH, this.dN)) {
                    h(16 - (((this.aH + this.dS) + 16) % 16), 0);
                    this.aT = true;
                }
                if (d(this.aH, this.dN)) {
                    h(((this.n.aH + this.n.dT) - this.aH) - this.V, 0);
                    this.aT = true;
                    break;
                }
                break;
            case 3:
                h(this.h, 0);
                if (f(this.aH, this.dN)) {
                    h(((-(this.aH + this.dT)) % 16) - 1, 0);
                    this.aT = true;
                }
                if (d(this.aH, this.dN)) {
                    h(((this.n.aH + this.n.dS) - this.aH) - this.ad, 0);
                    this.aT = true;
                    break;
                }
                break;
        }
        if (this.aT) {
            switch (this.f) {
                case 0:
                    int i = 0;
                    boolean z = true;
                    boolean z2 = true;
                    while (true) {
                        if (i >= this.aR) {
                            break;
                        } else {
                            if (z2) {
                                if (!c(this.aH + i, this.dN)) {
                                    if (!c(this.aH + i, this.dN + this.h)) {
                                        if (i > this.h) {
                                            i = this.h;
                                        }
                                        h(i, 0);
                                        break;
                                    }
                                } else {
                                    z2 = false;
                                }
                            }
                            if (z) {
                                if (!c(this.aH - i, this.dN)) {
                                    if (!c(this.aH - i, this.dN + this.h)) {
                                        h(i > this.h ? -this.h : -i, 0);
                                        break;
                                    }
                                } else {
                                    z = false;
                                }
                            }
                            if (i > (this.aR - this.dV) - 1 && i < this.aR - 1) {
                                i = (this.aR - this.dV) - 1;
                            }
                            i += this.dV;
                        }
                    }
                case 1:
                    int i2 = 0;
                    boolean z3 = true;
                    boolean z4 = true;
                    while (true) {
                        if (i2 >= this.aR) {
                            break;
                        } else {
                            if (z4) {
                                if (!c(this.aH + i2, this.dN)) {
                                    if (!c(this.aH + i2, this.dN - this.h)) {
                                        if (i2 > this.h) {
                                            i2 = this.h;
                                        }
                                        h(i2, 0);
                                        break;
                                    }
                                } else {
                                    z4 = false;
                                }
                            }
                            if (z3) {
                                if (!c(this.aH - i2, this.dN)) {
                                    if (!c(this.aH - i2, this.dN - this.h)) {
                                        h(i2 > this.h ? -this.h : -i2, 0);
                                        break;
                                    }
                                } else {
                                    z3 = false;
                                }
                            }
                            if (i2 > (this.aR - this.dV) - 1 && i2 < this.aR - 1) {
                                i2 = (this.aR - this.dV) - 1;
                            }
                            i2 += this.dV;
                        }
                    }
                case 2:
                    int i3 = 0;
                    boolean z5 = true;
                    boolean z6 = true;
                    while (true) {
                        if (i3 >= this.aR) {
                            break;
                        } else {
                            if (z6) {
                                if (!c(this.aH, this.dN + i3)) {
                                    if (!c(this.aH - this.h, this.dN + i3)) {
                                        if (i3 > this.h) {
                                            i3 = this.h;
                                        }
                                        h(0, i3);
                                        break;
                                    }
                                } else {
                                    z6 = false;
                                }
                            }
                            if (z5) {
                                if (!c(this.aH, this.dN - i3)) {
                                    if (!c(this.aH - this.h, this.dN - i3)) {
                                        h(0, i3 > this.h ? -this.h : -i3);
                                        break;
                                    }
                                } else {
                                    z5 = false;
                                }
                            }
                            if (i3 > (this.aR - this.dW) - 1 && i3 < this.aR - 1) {
                                i3 = (this.aR - this.dW) - 1;
                            }
                            i3 += this.dW;
                        }
                    }
                case 3:
                    int i4 = 0;
                    boolean z7 = true;
                    boolean z8 = true;
                    while (true) {
                        if (i4 >= this.aR) {
                            break;
                        } else {
                            if (z8) {
                                if (!c(this.aH, this.dN + i4)) {
                                    if (!c(this.aH + this.h, this.dN + i4)) {
                                        if (i4 > this.h) {
                                            i4 = this.h;
                                        }
                                        h(0, i4);
                                        break;
                                    }
                                } else {
                                    z8 = false;
                                }
                            }
                            if (z7) {
                                if (!c(this.aH, this.dN - i4)) {
                                    if (!c(this.aH + this.h, this.dN - i4)) {
                                        h(0, i4 > this.h ? -this.h : -i4);
                                        break;
                                    }
                                } else {
                                    z7 = false;
                                }
                            }
                            if (i4 > (this.aR - this.dW) - 1 && i4 < this.aR - 1) {
                                i4 = (this.aR - this.dW) - 1;
                            }
                            i4 += this.dW;
                        }
                    }
            }
            this.aT = false;
        }
    }

    public final void b(byte[] bArr) {
        this.i = bArr[2];
        this.e = (byte) ((-bArr[2]) - 1);
        this.dV = 6;
        this.dW = 6;
        this.h = 4;
        this.U = 1;
        this.V = -5;
        this.ad = 5;
        this.ag = -6;
        this.ao = 0;
        this.dS = -3;
        this.dT = 3;
        this.dU = -6;
        this.T = 0;
        this.P = -10;
        this.S = 10;
        this.l = -35;
        this.k = 16;
        eb.ke[this.e] = this;
    }

    public final boolean b(int i, int i2) {
        ea cp = ea.cp();
        bj bp = bj.bp();
        this.o = null;
        this.aS = null;
        boolean z = false;
        for (int i3 = 0; i3 < cp.f; i3++) {
            switch (bp.p[cp.m[i3]].k) {
                case 8:
                case 32:
                    if (!bp.p[cp.m[i3]].M && a(i, i2, bp.p[cp.m[i3]])) {
                        this.o = bp.p[cp.m[i3]];
                        z = true;
                        break;
                    }
                case 64:
                    if (!a(i, i2, bp.p[cp.m[i3]])) {
                        break;
                    } else {
                        this.aS = bp.p[cp.m[i3]];
                        z = true;
                        break;
                    }
            }
        }
        return z;
    }

    public final void c() {
    }

    public final boolean c(int i, int i2) {
        return f(i, i2) || d(i, i2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e(int r11, int r12) {
        /*
            r10 = this;
            r9 = 8
            r8 = 1
            r7 = -8
            r6 = 0
            short r0 = r10.dS
            int r0 = r0 + r11
            int r0 = r0 >> 4
            short r1 = r10.dT
            int r1 = r1 + r11
            int r1 = r1 >> 4
            short r2 = r10.dU
            int r2 = r2 + r12
            int r2 = r2 >> 4
            short r3 = r10.ao
            int r3 = r3 + r12
            int r3 = r3 >> 4
        L_0x0019:
            if (r0 <= r1) goto L_0x001d
            r0 = r6
        L_0x001c:
            return r0
        L_0x001d:
            r4 = r2
        L_0x001e:
            if (r4 <= r3) goto L_0x0023
            int r0 = r0 + 1
            goto L_0x0019
        L_0x0023:
            ec r5 = defpackage.ec.cA()
            byte r5 = r5.x(r0, r4)
            switch(r5) {
                case -2: goto L_0x0031;
                case -1: goto L_0x002e;
                case 0: goto L_0x002e;
                case 1: goto L_0x002e;
                case 2: goto L_0x002e;
                case 3: goto L_0x002e;
                case 4: goto L_0x002e;
                case 5: goto L_0x002e;
                case 6: goto L_0x002e;
                case 7: goto L_0x002e;
                case 8: goto L_0x002e;
                case 9: goto L_0x0033;
                case 10: goto L_0x0033;
                case 11: goto L_0x0033;
                case 12: goto L_0x0033;
                case 13: goto L_0x0033;
                case 14: goto L_0x0033;
                case 15: goto L_0x0033;
                case 16: goto L_0x0033;
                case 17: goto L_0x0033;
                default: goto L_0x002e;
            }
        L_0x002e:
            int r4 = r4 + 1
            goto L_0x001e
        L_0x0031:
            r0 = r8
            goto L_0x001c
        L_0x0033:
            byte r0 = r10.f
            switch(r0) {
                case 0: goto L_0x0047;
                case 1: goto L_0x0043;
                case 2: goto L_0x004b;
                case 3: goto L_0x004f;
                default: goto L_0x0038;
            }
        L_0x0038:
            defpackage.bi.ac = r6
            dz r0 = defpackage.dz.cn()
            r0.c(r5)
            r0 = r8
            goto L_0x001c
        L_0x0043:
            r10.h(r6, r9)
            goto L_0x0038
        L_0x0047:
            r10.h(r6, r7)
            goto L_0x0038
        L_0x004b:
            r10.h(r9, r6)
            goto L_0x0038
        L_0x004f:
            r10.h(r7, r6)
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.d.e(int, int):boolean");
    }
}
