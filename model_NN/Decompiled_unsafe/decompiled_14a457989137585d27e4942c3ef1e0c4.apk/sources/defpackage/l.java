package defpackage;

import com.nokia.mid.ui.DirectGraphics;
import javax.microedition.media.Player;

/* renamed from: l  reason: default package */
public final class l implements dy {
    private static int aH;
    private static final int[] aI = {0, 178, 350, as.GAME_A_PRESSED, 658, 784, 887, 962, 1008, as.GAME_B_PRESSED};
    private static l dM;
    public ao[] I;
    public int ac = 0;
    private int dN;
    public byte e;
    private byte f;
    public n[] p;
    public ao q;
    public ao r;
    public ao s;

    private l() {
    }

    public static void a(int i) {
        aH = i;
    }

    public static l al() {
        if (dM == null) {
            dM = new l();
        }
        return dM;
    }

    private void h(an anVar) {
        anVar.setColor(16777215);
        anVar.c(0, 0, 240, 190);
        du.a(anVar, this.I[0], 0, 120, 320, 33);
        ao aoVar = this.I[0];
        int height = this.I[0].getHeight() >> 1;
        int height2 = this.I[0].getHeight() >> 1;
        int i = height2 >> 3;
        int i2 = -height2;
        this.dN++;
        for (int i3 = 0; i3 < height2; i3++) {
            int i4 = i * 2 * (i3 + 20);
            int i5 = (int) ((((long) (((((height2 - i3) * i) / (i3 + 1)) + this.dN) << 6)) % 360) / 9);
            if (i5 < 0) {
                i5 += 39;
            }
            int i6 = ((i4 * (i5 < 10 ? aI[i5] : i5 < 20 ? aI[19 - i5] : i5 < 30 ? -aI[i5 - 20] : -aI[39 - i5])) / height2) >> 13;
            if (i3 + i6 < 0) {
                i6 = -i3;
            } else if (i3 + i6 >= height2) {
                i6 = (height2 - i3) - 1;
            }
            anVar.d(0, i2 + 320 + i3, 240, 1);
            anVar.a(aoVar, 0, ((i2 + 320) - height) - i6, 20);
        }
        anVar.d(0, 0, 240, 320);
    }

    public final void N() {
        this.p = new n[4];
        if (this.I == null) {
            this.I = new ao[4];
            for (int i = 0; i < this.I.length; i++) {
                this.I[i] = ee.O(new StringBuffer().append("/u/me").append(i).toString());
            }
        }
        if (this.r == null) {
            this.r = ee.O("/l/l");
        }
        if (this.s == null) {
            this.s = ee.O("/l/c");
        }
        aH = 1;
    }

    public final void P() {
        this.p[0].a(128, 140);
        this.p[2].a(185, 90);
    }

    public final void Q() {
        switch (aH) {
            case 0:
                this.ac += 40;
                if (this.ac >= 1500) {
                    this.ac = 0;
                    aH++;
                    return;
                }
                return;
            case 1:
                this.ac += 40;
                if (this.ac >= 1500) {
                    this.ac = 0;
                    aH = 5;
                    return;
                }
                return;
            case 2:
                if (bi.y(16) || bi.y(128)) {
                    bh.e = 2;
                    aH = 3;
                    bh.a(0);
                    for (int i = 0; i < this.p.length; i++) {
                        this.p[i] = c.g(new StringBuffer().append("/u/mea").append(i).toString());
                    }
                    return;
                } else if (bi.y(as.FIRE_PRESSED)) {
                    bh.P();
                    aH = 3;
                    bh.a(0);
                    for (int i2 = 0; i2 < this.p.length; i2++) {
                        this.p[i2] = c.g(new StringBuffer().append("/u/mea").append(i2).toString());
                    }
                    return;
                } else {
                    return;
                }
            case 3:
            default:
                return;
            case 4:
                int i3 = this.ac + 1;
                this.ac = i3;
                this.ac = i3 > 31 ? 0 : this.ac;
                if (bi.ac != 0) {
                    bi.ac = 0;
                    bi.bo().gp = o.an();
                    return;
                }
                return;
            case 5:
                switch (this.f) {
                    case 1:
                        o.an().P();
                        break;
                    case 2:
                        if (dz.aC == null) {
                            dz.cn();
                            dz.Q();
                            break;
                        }
                        break;
                    case 3:
                        bj.bp().c();
                        break;
                    case 4:
                        a.M().c();
                        break;
                    case 7:
                        bi.q = ee.O("/u/lp");
                        break;
                    case 8:
                        bj.aW();
                        break;
                    case 10:
                        this.f = 0;
                        aH = 2;
                        break;
                }
                this.f = (byte) (this.f + 1);
                return;
        }
    }

    public final void a(an anVar) {
        switch (aH) {
            case 0:
                du.c(anVar, b.ac, 0, 0, 240, 320);
                if (this.q != null) {
                    du.a(anVar, this.q, 0, 120, 160, 3);
                    return;
                }
                return;
            case 1:
                du.c(anVar, 0, 0, 0, 240, 320);
                if (this.r != null) {
                    du.a(anVar, this.r, 0, 120, 160, 3);
                    du.a(anVar, this.s, 0, 120, 320, 33);
                    return;
                }
                return;
            case 2:
                du.c(anVar, 0, 0, 0, 240, 320);
                du.a(anVar, "是否开启音乐", 120, 160, 65, 16439945);
                du.a(anVar, 6, 6);
                return;
            case 3:
                h(anVar);
                switch (this.e) {
                    case 0:
                        this.p[3].a(120, (int) Player.REALIZED);
                        this.p[3].c(anVar);
                        this.p[3].Q();
                        if (this.p[3].W()) {
                            this.e = 1;
                            this.p[1].a(85, (int) DirectGraphics.ROTATE_180);
                            return;
                        }
                        return;
                    case 1:
                        this.p[1].c(anVar);
                        this.p[1].Q();
                        if (this.p[1].W()) {
                            P();
                            this.e = 2;
                            return;
                        }
                        return;
                    case 2:
                        this.p[0].c(anVar);
                        this.p[0].Q();
                        if (this.p[0].W()) {
                            this.e = 3;
                            return;
                        }
                        return;
                    case 3:
                        this.p[0].c(anVar);
                        this.p[2].c(anVar);
                        this.p[2].Q();
                        if (this.p[2].W()) {
                            this.e = 4;
                            return;
                        }
                        return;
                    case 4:
                        this.p[0].c(anVar);
                        this.p[2].c(anVar);
                        this.p[2].Q();
                        du.a(anVar, this.I[1], 0, 215, 10, 0);
                        du.a(anVar, this.I[2], 0, 216, 120, 0);
                        this.ac += 4;
                        du.a(anVar, this.I[3], 0, 0, this.I[3].getWidth(), this.ac, 0, 65, 80);
                        if (this.ac > this.I[3].getHeight()) {
                            this.e = 0;
                            aH = 4;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            case 4:
                c(anVar);
                if ((this.ac >> 4) % 2 == 0) {
                    du.a(anVar, "-按中心键继续-", 120, 316, 65, 16711680, 16777215);
                    anVar.d(0, (320 - p.g) - 4, 240, p.g + 4);
                    du.a(anVar, "-按中心键继续-", 120, 316, 65, 0);
                    anVar.d(0, 0, 240, 320);
                    return;
                }
                return;
            case 5:
                du.a(anVar, 0, 0, 240, 320, 0);
                du.a(anVar, "江湖风云录之御剑问情", 120, 5, 17, 16439945);
                du.a(anVar, new StringBuffer().append("加载中 ").append(this.f * 10).append("%").toString(), 120, 280, 33, 3689850);
                bg.aU().a(anVar, this.f);
                return;
            default:
                return;
        }
    }

    public final void c() {
        this.q = null;
        this.r = null;
        this.s = null;
    }

    public final void c(an anVar) {
        h(anVar);
        this.p[0].c(anVar);
        this.p[2].c(anVar);
        this.p[2].Q();
        du.a(anVar, this.I[1], 0, 215, 10, 0);
        du.a(anVar, this.I[2], 0, 216, 120, 0);
        du.a(anVar, this.I[3], 0, 65, 80, 0);
    }
}
