package defpackage;

import android.util.Log;
import java.util.HashMap;

/* renamed from: ds  reason: default package */
public final class ds {
    public HashMap jV = new HashMap();

    public ds(String str) {
        if (str != null && str.length() > 0) {
            String[] split = str.split(",");
            for (int i = 0; i < split.length; i++) {
                int indexOf = split[i].indexOf(":");
                if (indexOf != -1) {
                    this.jV.put(split[i].substring(0, indexOf).trim(), split[i].substring(indexOf + 1));
                } else {
                    Log.w("Configuration", "Unkown args for configuration." + split[i]);
                }
            }
        }
    }
}
