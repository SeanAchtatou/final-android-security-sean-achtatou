package defpackage;

/* renamed from: dv  reason: default package */
public final class dv extends n {
    public dv(ao aoVar, byte[][] bArr, byte[][][] bArr2, byte[][] bArr3) {
        super(aoVar, bArr, bArr2, bArr3);
        this.j = 3;
    }

    public final void N() {
        Q();
    }

    public final void b(byte[] bArr) {
        this.U = 0;
        this.V = 0;
        this.ad = 0;
        this.ag = 0;
        switch (bArr[2]) {
            case 0:
                this.ao = 0;
                this.dS = 10000;
                this.dT = -10000;
                this.dU = 0;
                this.T = 0;
                this.P = -12;
                this.S = 12;
                this.l = -24;
                break;
            case 1:
                this.ao = 0;
                this.dS = -7;
                this.dT = 7;
                this.dU = -7;
                this.T = 0;
                this.P = -12;
                this.S = 12;
                this.l = -24;
                break;
            case 2:
                this.ao = 0;
                this.dS = -13;
                this.dT = 13;
                this.dU = -15;
                this.T = 0;
                this.P = -23;
                this.S = 23;
                this.l = -70;
                break;
            case 3:
            default:
                this.ao = 0;
                this.dS = 0;
                this.dT = 0;
                this.dU = 0;
                this.T = 0;
                this.P = -12;
                this.S = 12;
                this.l = -24;
                break;
            case 4:
                this.ao = 0;
                this.dS = -14;
                this.dT = 14;
                this.dU = -30;
                this.T = 0;
                this.P = -23;
                this.S = 23;
                this.l = -72;
                break;
        }
        this.i = bArr[3];
        this.k = 2;
        if (bArr.length >= 6) {
            if (bArr[4] == 0 && bArr[5] == 0) {
                this.T = 0;
                this.P = -12;
                this.S = 12;
                this.l = -24;
                return;
            }
            this.T = 0;
            this.P = (short) (-bArr[4]);
            this.S = (short) bArr[4];
            this.l = (short) bArr[5];
        }
    }

    public final void c() {
    }
}
