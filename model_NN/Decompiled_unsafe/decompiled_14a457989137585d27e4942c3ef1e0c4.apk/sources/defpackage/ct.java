package defpackage;

/* renamed from: ct  reason: default package */
final class ct implements Runnable {
    final /* synthetic */ cu hJ;

    ct(cu cuVar) {
        this.hJ = cuVar;
    }

    public final void run() {
        cr.hG.removeAllViews();
        if (cr.hF != null) {
            cr.hF.aE();
        }
        cu unused = cr.hF = this.hJ;
        cr.hG.addView(cr.hF.getView(), cr.hI);
        cr.hG.invalidate();
        cr.hF.getView().requestFocus();
        cr.hF.aw();
        cf.b(cf.a((int) cr.MSG_VIEW_CHANGED, cr.hF.getView()));
    }
}
