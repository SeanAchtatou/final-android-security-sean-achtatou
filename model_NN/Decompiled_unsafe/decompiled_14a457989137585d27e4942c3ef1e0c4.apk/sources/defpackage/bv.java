package defpackage;

import android.app.Activity;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* renamed from: bv  reason: default package */
public final class bv extends GestureDetector.SimpleOnGestureListener implements View.OnKeyListener, View.OnTouchListener {
    public static final String LOG_TAG = "InputManager";
    private static final bv gO = new bv();
    private static final LinkedHashSet gR = new LinkedHashSet();
    private static final LinkedHashSet gS = new LinkedHashSet();
    private static final LinkedHashSet gT = new LinkedHashSet();
    /* access modifiers changed from: private */
    public static final LinkedHashSet gU = new LinkedHashSet();
    /* access modifiers changed from: private */
    public static final LinkedHashSet gV = new LinkedHashSet();
    /* access modifiers changed from: private */
    public GestureDetector gP;
    private SensorManager gQ;

    public static final void a(SensorEventListener sensorEventListener) {
        gO.gQ.registerListener(sensorEventListener, gO.gQ.getDefaultSensor(1), 3);
    }

    protected static void a(View view) {
        View.OnTouchListener onTouchListener;
        if (!cr.hH) {
            view.setOnKeyListener(gO);
        }
        if (cl.bN() >= 5) {
            bv bvVar = gO;
            bvVar.getClass();
            onTouchListener = new by(bvVar);
        } else {
            onTouchListener = gO;
        }
        view.setOnTouchListener(onTouchListener);
    }

    public static final void a(bw bwVar) {
        gR.add(bwVar);
    }

    public static final void a(bx bxVar) {
        gS.add(bxVar);
    }

    public static final void a(bz bzVar) {
        gV.add(bzVar);
    }

    public static final void a(cb cbVar) {
        gT.add(cbVar);
    }

    protected static void b(Activity activity) {
        gO.gP = new GestureDetector(activity, gO);
        gO.gQ = (SensorManager) activity.getSystemService("sensor");
    }

    public static final void b(SensorEventListener sensorEventListener) {
        gO.gQ.unregisterListener(sensorEventListener);
    }

    protected static void b(View view) {
        view.setOnKeyListener(null);
        view.setOnTouchListener(null);
    }

    public static final void b(bx bxVar) {
        gS.remove(bxVar);
    }

    public static final boolean g(int i, int i2, int i3, int i4) {
        Iterator it = gS.iterator();
        while (it.hasNext()) {
            if (((bx) it.next()).g(i, i2, i3, i4)) {
                return true;
            }
        }
        return false;
    }

    protected static void onDestroy() {
        gU.clear();
        gS.clear();
        gT.clear();
        gR.clear();
    }

    public static boolean onTrackballEvent(MotionEvent motionEvent) {
        if (gT.isEmpty()) {
            return false;
        }
        Iterator it = gT.iterator();
        while (it.hasNext()) {
            it.next();
        }
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Iterator it = gU.iterator();
        while (it.hasNext()) {
            if (((ca) it.next()).bz()) {
                return true;
            }
        }
        return false;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (gR.isEmpty()) {
            return false;
        }
        Iterator it = gR.iterator();
        while (it.hasNext()) {
            ((bw) it.next()).a(keyEvent);
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!gU.isEmpty()) {
            this.gP.onTouchEvent(motionEvent);
        }
        if (!gV.isEmpty()) {
            Iterator it = gV.iterator();
            while (it.hasNext()) {
                ((bz) it.next()).a(motionEvent);
            }
        }
        return g(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }
}
