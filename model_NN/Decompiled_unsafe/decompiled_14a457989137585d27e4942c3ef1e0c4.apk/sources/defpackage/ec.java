package defpackage;

import java.io.DataInputStream;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.control.ToneControl;

/* renamed from: ec  reason: default package */
public final class ec {
    private static ec kf;
    public boolean M = true;
    public byte[] N;
    public String aF;
    public String aG;
    public int aH;
    public int ac;
    public int dN = 16;
    public int dO = 16;
    public byte e;
    public int eb;
    public int ec;
    public int ed;
    public int ee;
    public int ef;
    public int eg;
    public int eh;
    public int ei;
    public int ej;
    public int ek;
    public int el;
    public int jX;
    public int jY;
    public int kg;
    public int kh;
    public int ki;
    public an kj;
    public byte[] m;
    public ao q;
    public ao r;
    public ao s;

    private ec() {
    }

    private void a(an anVar, int i, int i2, int i3, int i4, int i5) {
        byte b;
        if (i2 >= 0 && i2 < this.aH && i3 >= 0 && i3 < this.ac) {
            if (i3 >= 0 && i2 >= 0 && i3 < this.ac && i2 < this.aH) {
                switch (i) {
                    case 0:
                        b = this.m[((this.ac * i2) + i3) << 1];
                        break;
                    case 1:
                        b = this.m[(((this.ac * i2) + i3) << 1) + 1];
                        break;
                    default:
                        b = 0;
                        break;
                }
            } else {
                b = 0;
            }
            byte b2 = (byte) (b & 63);
            switch (i) {
                case 0:
                    if (b2 == 63) {
                        du.c(anVar, 0, i4, i5, this.dN, this.dO);
                        return;
                    }
                    du.a(anVar, this.q, (b2 % this.eb) * this.dN, (b2 / this.eb) * this.dO, this.dN, this.dO, ee.w(b), i4, i5);
                    return;
                case 1:
                    if (b2 != 63) {
                        du.a(anVar, this.r, (b2 % this.ec) * this.dN, (b2 / this.ec) * this.dO, this.dN, this.dO, ee.w(b), i4, i5);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static ec cA() {
        if (kf == null) {
            kf = new ec();
        }
        return kf;
    }

    public final void P() {
        this.q = ee.O(new StringBuffer().append("/m/").append(this.aF).toString());
        this.r = ee.O(new StringBuffer().append("/m/").append(this.aG).toString());
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, int i3, int i4, int i5) {
        for (int i6 = 0; i6 < i3; i6++) {
            int i7 = i5 + (this.dO * i6);
            a(this.kj, 0, i2 + i6, i, i4, i7);
            a(this.kj, 1, i2 + i6, i, i4, i7);
        }
        for (int i8 = i3; i8 < this.jX; i8++) {
            int i9 = (i8 - i3) * this.dO;
            a(this.kj, 0, i2 + i8, i, i4, i9);
            a(this.kj, 1, i2 + i8, i, i4, i9);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(an anVar, ao aoVar, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        if (i3 > 0 && i4 > 0) {
            du.a(anVar, aoVar, i, i2, i3 > this.eh ? this.eh : i3, i4 > this.ei ? this.ei : i4, 0, i6, i7);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.m(str));
            this.ac = dataInputStream.readByte() & ToneControl.SILENCE;
            this.aH = dataInputStream.readByte() & ToneControl.SILENCE;
            this.dN = 16;
            this.dO = 16;
            this.ed = this.dN * this.ac;
            this.ee = this.dO * this.aH;
            this.m = new byte[((this.ac * this.aH) << 1)];
            this.N = new byte[(this.ac * this.aH)];
            dataInputStream.readFully(this.m);
            dataInputStream.readFully(this.N);
            dataInputStream.close();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i, int i2, int i3, int i4, int i5) {
        for (int i6 = 0; i6 < i3; i6++) {
            int i7 = i4 + (this.dN * i6);
            a(this.kj, 0, i2, i + i6, i7, i5);
            a(this.kj, 1, i2, i + i6, i7, i5);
        }
        for (int i8 = i3; i8 < this.el; i8++) {
            int i9 = (i8 - i3) * this.dN;
            a(this.kj, 0, i2, i + i8, i9, i5);
            a(this.kj, 1, i2, i + i8, i9, i5);
        }
    }

    /* access modifiers changed from: package-private */
    public int cu() {
        return this.ef / this.dN;
    }

    /* access modifiers changed from: package-private */
    public int cv() {
        return this.eg / this.dO;
    }

    /* access modifiers changed from: package-private */
    public int cw() {
        return this.ef % this.ej;
    }

    /* access modifiers changed from: package-private */
    public int cx() {
        return this.eg % this.ek;
    }

    /* access modifiers changed from: package-private */
    public int cy() {
        return (this.ej - (this.ef % this.ej)) / this.dN;
    }

    /* access modifiers changed from: package-private */
    public int cz() {
        return (this.ek - (this.eg % this.ek)) / this.dO;
    }

    public final void g(int i, int i2) {
        int i3 = i2 / this.dO;
        int i4 = i / this.dN;
        int i5 = i3 + this.jX;
        int i6 = i4 + this.el;
        for (int i7 = i3; i7 < i5; i7++) {
            int i8 = (this.dO * i7) % this.ek;
            for (int i9 = i4; i9 < i6; i9++) {
                int i10 = (this.dN * i9) % this.ej;
                a(this.kj, 0, i7, i9, i10, i8);
                a(this.kj, 1, i7, i9, i10, i8);
            }
        }
        this.ef = (i / this.dN) * this.dN;
        this.eg = (i2 / this.dO) * this.dO;
        this.jY = i;
        this.kg = i2;
    }

    public final byte x(int i, int i2) {
        if (i < 0 || i2 < 0 || i >= this.ac || i2 >= this.aH) {
            return -2;
        }
        return this.N[(this.ac * i2) + i];
    }
}
