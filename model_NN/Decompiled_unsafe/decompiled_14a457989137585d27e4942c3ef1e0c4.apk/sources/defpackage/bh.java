package defpackage;

import java.io.InputStream;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.VolumeControl;

/* renamed from: bh  reason: default package */
public final class bh {
    public static boolean M = false;
    public static boolean ae = false;
    public static byte e = 0;
    public static byte f;
    public static byte g;
    private static Player gm = null;

    public static void N() {
        if (M) {
            P();
            M = false;
        } else if (ae) {
            if (gm == null) {
                try {
                    String stringBuffer = new StringBuffer().append("/l/").append((int) f).append(".mid").toString();
                    stringBuffer.getClass();
                    InputStream m = MIDPHelper.m(stringBuffer);
                    Player a = Manager.a(m, "audio/midi");
                    gm = a;
                    a.aK();
                    gm.aL();
                    gm.m(-1);
                    d(e * 20);
                    gm.start();
                    m.close();
                } catch (Exception e2) {
                }
            } else {
                d(e * 20);
            }
            ae = false;
        }
    }

    public static void P() {
        if (gm != null) {
            try {
                if (gm.getState() == 400) {
                    gm.stop();
                }
                gm.close();
                gm = null;
            } catch (Exception e2) {
            }
        }
    }

    private static void Q() {
        if (e == 0) {
            M = true;
        } else {
            ae = true;
        }
    }

    public static void a(int i) {
        g = f;
        f = (byte) i;
        Q();
    }

    public static void c() {
        if (e != 0) {
            M = true;
        }
    }

    public static void c(int i) {
        f = (byte) i;
        Q();
    }

    private static void d(int i) {
        VolumeControl volumeControl;
        try {
            if (gm != null && (volumeControl = (VolumeControl) gm.q("VolumeControl")) != null) {
                volumeControl.n(i);
                volumeControl.b(false);
            }
        } catch (Exception e2) {
        }
    }
}
