package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Message;
import android.util.Log;
import android.view.View;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: ah  reason: default package */
public final class ah {
    public static final int ALERT = 3;
    public static final int CHOICE_GROUP_ELEMENT = 2;
    public static final int COLOR_BACKGROUND = 0;
    public static final int COLOR_BORDER = 4;
    public static final int COLOR_FOREGROUND = 1;
    public static final int COLOR_HIGHLIGHTED_BACKGROUND = 2;
    public static final int COLOR_HIGHLIGHTED_BORDER = 5;
    public static final int COLOR_HIGHLIGHTED_FOREGROUND = 3;
    public static final int LIST_ELEMENT = 1;
    public static final String LOG_TAG = "Display";
    private static al eJ = null;
    public static final ah eK = new ah();
    private static MIDlet eL;
    private Message eM = cf.a(MIDPDevice.MSG_MIDP_DISPLAY_CALL_SERIALLY, null, 0, cf.MSG_ARG2_DONT_RECYCLE_ME);
    private boolean eN = true;
    private MIDPDevice eO;

    private ah() {
    }

    public static ah a(MIDlet mIDlet) {
        if (mIDlet != null) {
            eL = mIDlet;
        }
        return eK;
    }

    public static void a(af afVar) {
        if (eJ != null) {
            ag agVar = eJ.eW;
            al alVar = eJ;
        }
    }

    private static boolean aB() {
        return eL.getState() == 1;
    }

    public static void av() {
        Thread.yield();
    }

    public final void a(ae aeVar) {
        if (this.eN && aB()) {
            this.eN = false;
            if (this.eO == null) {
                this.eO = (MIDPDevice) bp.gC;
            }
            try {
                aeVar.n(this.eO.aJ());
            } catch (Exception e) {
                Log.w(LOG_TAG, "Exception in paint!" + e);
                e.printStackTrace();
            }
            if (aB()) {
                if (this.eO == null) {
                    this.eO = (MIDPDevice) bp.gC;
                }
                this.eO.bV();
            }
            this.eN = true;
        }
    }

    public final void a(al alVar) {
        if (alVar != eJ && alVar != null) {
            if (alVar.at() != 5) {
                if (eJ != null && eJ.eY) {
                    eJ.eT = null;
                }
                eJ = alVar;
                alVar.eT = this;
                cr.a(eJ);
                return;
            }
            ac acVar = (ac) alVar;
            Activity activity = cl.getActivity();
            View view = acVar.getView();
            String str = acVar.eX;
            ad aq = acVar.aq();
            int as = acVar.as();
            AlertDialog create = new AlertDialog.Builder(activity).create();
            cl.getHandler().post(new ai(this, str, create, view));
            if (as == -2) {
                return;
            }
            if (aq != ad.eG && aq != ad.eD && aq != ad.eF) {
                cl.getHandler().postDelayed(new ak(this, create), (long) (acVar.as() <= ac.ar() ? ac.ar() : acVar.as()));
            } else if (acVar.as() != 0) {
                cl.getHandler().postDelayed(new aj(this, create), (long) (acVar.as() <= ac.ar() ? ac.ar() : acVar.as()));
            }
        }
    }

    public final void i(int i) {
        if (eJ != null && aB()) {
            if (!eJ.eV.isEmpty()) {
                cw cwVar = bp.gC;
                if (i == MIDPDevice.I(1)) {
                    a((af) eJ.eV.get(0));
                } else if (eJ.eV.size() > 1) {
                    cw cwVar2 = bp.gC;
                    if (i == MIDPDevice.I(2)) {
                        a((af) eJ.eV.get(1));
                    }
                }
            }
            if (eJ.at() == 1) {
                ((as) eJ).p(0, i);
            }
            eJ.i(i);
        }
    }

    public final void j(int i) {
        if (eJ != null && aB()) {
            if (eJ.at() == 1) {
                ((as) eJ).p(1, i);
            }
            eJ.j(i);
        }
    }

    public final void k(int i) {
        if (eJ != null && aB()) {
            al alVar = eJ;
        }
    }

    public final void l(int i, int i2) {
        if (eJ != null && aB()) {
            al alVar = eJ;
        }
    }

    public final void m(int i, int i2) {
        if (eJ != null && aB()) {
            al alVar = eJ;
        }
    }

    public final void n(int i, int i2) {
        if (eJ != null && aB()) {
            al alVar = eJ;
        }
    }
}
