package defpackage;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* renamed from: cr  reason: default package */
public final class cr {
    public static final String LOG_TAG = "ViewManager";
    public static final int MSG_VIEW_CHANGED = 23041;
    private static final int ROOT_VIEW_ID = 792998026;
    /* access modifiers changed from: private */
    public static cu hF;
    /* access modifiers changed from: private */
    public static FrameLayout hG;
    public static boolean hH = false;
    /* access modifiers changed from: private */
    public static final ViewGroup.LayoutParams hI = new ViewGroup.LayoutParams(-1, -1);

    public static void a(cu cuVar) {
        if (cuVar != null) {
            cl.getHandler().post(new ct(cuVar));
        }
    }

    public static boolean ax() {
        if (hF != null) {
            return hF.ax() || hH;
        }
        return false;
    }

    protected static void b(Activity activity) {
        cf.a((int) MSG_VIEW_CHANGED, "MSG_VIEW_CHANGED");
        FrameLayout frameLayout = new FrameLayout(activity);
        hG = frameLayout;
        frameLayout.setBackgroundColor(-16777216);
        hG.setId(ROOT_VIEW_ID);
        hG.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        activity.setContentView(hG);
        hG.requestFocus();
        cf.a(new cs());
    }

    public static cu bP() {
        return hF;
    }

    public static FrameLayout bQ() {
        return hG;
    }

    protected static void onDestroy() {
        hF = null;
    }
}
