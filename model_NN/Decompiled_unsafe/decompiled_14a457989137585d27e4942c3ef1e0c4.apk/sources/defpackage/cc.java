package defpackage;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;

/* renamed from: cc  reason: default package */
public final class cc {
    public static final String LOG_TAG = "MediaManager";
    public static final String MEDIA_TEMP_FILE = "temp";
    public static final int VOLUME_CONTROL_BY_CLIP = -1;
    public static final int VOLUME_CONTROL_BY_DEVICE = -2;
    private static final cc gX = new cc();
    private static int gY = -1;
    private static String gZ;
    private static int ha = 15;
    /* access modifiers changed from: private */
    public static int hb;
    private static float hc;
    private static AudioManager hd;
    /* access modifiers changed from: private */
    public static final HashMap he = new HashMap();
    private static int hf;
    private static int hg = 0;
    /* access modifiers changed from: private */
    public static int hh = 0;

    public static void B(int i) {
        gY = i;
    }

    public static void C(int i) {
        if (gY == -1) {
            D(i);
        } else if (gY != -2) {
        } else {
            if (i == 0) {
                hf = bA();
                D(0);
            } else if (hf != 0) {
                D(hf);
                hf = 0;
            }
        }
    }

    /* access modifiers changed from: private */
    public static void D(int i) {
        hd.setStreamVolume(3, (int) (((float) i) * hc), 16);
    }

    public static ce a(String str, InputStream inputStream, String str2) {
        if (inputStream == null) {
            return null;
        }
        int available = inputStream.available();
        String str3 = MEDIA_TEMP_FILE + (available <= 0 ? "" : Integer.valueOf(available));
        if (he.containsKey(str3)) {
            ce ceVar = (ce) he.get(str3);
            ceVar.hj = false;
            ceVar.hl = false;
            ceVar.hk = bA();
            ceVar.hm.reset();
            return ceVar;
        }
        cc ccVar = gX;
        ccVar.getClass();
        ce ceVar2 = new ce(ccVar);
        String str4 = "";
        if (str2.indexOf("mid") != -1) {
            str4 = ".mid";
        } else if (str2.indexOf("mpeg") != -1) {
            str4 = ".mp3";
        } else if (str2.indexOf("amr") != -1) {
            str4 = ".amr";
        }
        ceVar2.name = str3;
        ceVar2.type = str2;
        ceVar2.hi = str3 + str4;
        FileOutputStream openFileOutput = cl.getActivity().openFileOutput(ceVar2.hi, 1);
        byte[] bArr = new byte[as.FIRE_PRESSED];
        while (true) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                ceVar2.hm = new MediaPlayer();
                ceVar2.hi = gZ + ceVar2.hi;
                he.put(str3, ceVar2);
                return ceVar2;
            }
        }
    }

    protected static void b(Activity activity) {
        gZ = activity.getFilesDir().getAbsolutePath() + File.separator;
        AudioManager audioManager = (AudioManager) activity.getSystemService("audio");
        hd = audioManager;
        int streamMaxVolume = audioManager.getStreamMaxVolume(3);
        ha = streamMaxVolume;
        hc = ((float) streamMaxVolume) / 100.0f;
        hb = bA();
        cf.a(new cd());
    }

    public static int bA() {
        return (int) (((float) hd.getStreamVolume(3)) / hc);
    }

    public static void d(boolean z) {
        if (z) {
            hg = bA();
            C(0);
        } else if (hg != 0) {
            C(hg);
        }
    }

    protected static void onDestroy() {
        for (ce ceVar : he.values()) {
            if (ceVar.hm != null) {
                ceVar.hm.release();
            }
            ceVar.hm = null;
        }
        he.clear();
        D(hb);
    }
}
