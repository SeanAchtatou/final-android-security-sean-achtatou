package defpackage;

import android.util.Log;

/* renamed from: bp  reason: default package */
public final class bp {
    private static final String LOG_TAG = "DeviceManager";
    public static cw gC;

    protected static cw z(String str) {
        try {
            cw cwVar = (cw) Class.forName(str).newInstance();
            gC = cwVar;
            cwVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create device. " + e);
            e.printStackTrace();
        }
        return gC;
    }
}
