package defpackage;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import com.androidbox.jhfylhtc.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.Timer;

/* renamed from: cl  reason: default package */
public final class cl {
    public static final String LOG_TAG = "SystemManager";
    public static final int MSG_SYSTEM_ACTIVITY_RESULT = 47888;
    public static final int MSG_SYSTEM_EXIT = 47875;
    public static final int MSG_SYSTEM_FUNCTION_REQUEST = 47887;
    public static final int MSG_SYSTEM_INIT_COMPLETE = 47872;
    public static final int MSG_SYSTEM_LOG_EVENT = 47904;
    public static final int MSG_SYSTEM_ON_PAUSE = 47873;
    public static final int MSG_SYSTEM_ON_RESUME = 47874;
    public static boolean hA = false;
    /* access modifiers changed from: private */
    public static boolean hB = false;
    private static final Timer hC = new Timer();
    private static Handler handler;
    /* access modifiers changed from: private */
    public static Activity hx;
    private static int hy = 0;
    private static final String[] hz = {".png", ".mp3", ".jpg", ".jpeg", ".mpeg", ".bmp"};

    public static final InputStream A(String str) {
        String str2;
        if (str == null) {
            throw new IOException("Can't load resource noname.");
        }
        String str3 = str;
        while (str3.startsWith(File.separator)) {
            str3 = str3.substring(1);
        }
        String[] split = str3.split("\\.");
        String M = dt.M(split[0]);
        String N = dt.N(split[0]);
        if (split.length == 1) {
            str2 = "a_b";
        } else if (split.length == 2) {
            str2 = split[1];
        } else {
            String str4 = "";
            for (int i = 1; i < split.length; i++) {
                str4 = str4 + split[i];
            }
            str2 = str4;
        }
        if (N.length() == 0 || N.charAt(0) == '_') {
            N = "b_a" + N;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= hz.length) {
                break;
            } else if (str2.equalsIgnoreCase(hz[i2])) {
                str2 = str2.toLowerCase();
                break;
            } else {
                i2++;
            }
        }
        String str5 = M + N + "." + str2;
        try {
            return hx.getAssets().open(str5);
        } catch (Exception e) {
            Log.w(LOG_TAG, "Can't load resource:" + str5 + " is not exist.");
            if (!hA) {
                return null;
            }
            throw new IOException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cf.a(int, java.lang.Object):android.os.Message
     arg types: [?, java.lang.String]
     candidates:
      cf.a(int, java.lang.String):void
      cf.a(int, java.lang.Object):android.os.Message */
    public static boolean B(String str) {
        cf.a(new cp());
        cf.b(cf.a((int) MSG_SYSTEM_FUNCTION_REQUEST, (Object) str));
        return true;
    }

    public static boolean C(String str) {
        if (str.startsWith("http://") || str.startsWith("market://")) {
            hx.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } else if (str.startsWith("tel:")) {
            hx.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
            return true;
        } else {
            Log.w(LOG_TAG, "Not supported " + str);
            return false;
        }
    }

    public static boolean D(String str) {
        try {
            return hx.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void H(int i) {
        hx.setRequestedOrientation(i);
    }

    public static void b(Activity activity) {
        hx = activity;
        Properties properties = new Properties();
        try {
            properties.load(activity.getResources().openRawResource(dt.I("globe")));
            handler = new Handler();
            bI();
            if (properties.containsKey("ThrowIOExceptions")) {
                hA = Boolean.parseBoolean(properties.getProperty("ThrowIOExceptions"));
            }
            if (properties.containsKey("DontQuit")) {
                cr.hH = Boolean.parseBoolean(properties.getProperty("DontQuit"));
            }
            cf.br();
            br.b(activity);
            if (properties.containsKey("fps")) {
                br.A(1000 / Integer.parseInt(properties.getProperty("fps")));
            }
            bq.br();
            if (properties.containsKey("feature")) {
                String[] split = properties.getProperty("feature").split("\\}");
                for (int i = 0; i < split.length; i++) {
                    int indexOf = split[i].indexOf("{");
                    if (indexOf != -1) {
                        bq.h(split[i].trim().substring(0, indexOf), split[i].trim().substring(indexOf + 1));
                    } else {
                        Log.w(LOG_TAG, "Failed to create feature:" + split[i]);
                    }
                }
            }
            bl.br();
            bv.b(activity);
            cc.b(activity);
            if (properties.containsKey("VolumeMode")) {
                cc.B(Integer.parseInt(properties.getProperty("VolumeMode")));
            }
            cr.b(activity);
            ch.br();
            bp.z(properties.getProperty("device"));
            cv.E(properties.getProperty("virtualdevice"));
            properties.clear();
            System.gc();
            cf.a((int) MSG_SYSTEM_INIT_COMPLETE, "MSG_SYSTEM_INIT_COMPLETE");
            cf.a((int) MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
            cf.a((int) MSG_SYSTEM_ON_RESUME, "MSG_SYSTEM_ON_RESUME");
            cf.a((int) MSG_SYSTEM_FUNCTION_REQUEST, "MSG_SYSTEM_FUNCTION_REQUEST");
            cf.a((int) MSG_SYSTEM_EXIT, "MSG_SYSTEM_EXIT");
            cf.a((int) MSG_SYSTEM_ACTIVITY_RESULT, "MSG_SYSTEM_ACTIVITY_RESULT");
            cf.a((int) MSG_SYSTEM_LOG_EVENT, "MSG_SYSTEM_LOG_EVENT");
            cf.a(new cm());
            cf.b(cf.a((int) MSG_SYSTEM_LOG_EVENT, new String[]{"Launch"}));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Load globe.properties error." + e);
        }
    }

    public static void b(String str, int i) {
        handler.post(new cq(str, 1));
    }

    protected static void bE() {
        hC.cancel();
        hC.purge();
        ch.onDestroy();
        bl.onDestroy();
        br.onDestroy();
        bv.onDestroy();
        cc.onDestroy();
        cr.onDestroy();
        if (bp.gC != null) {
            bp.gC.onDestroy();
        }
        if (cv.hK != null) {
            cv.hK.onDestroy();
        }
        bq.onDestroy();
        cf.onDestroy();
        hx.finish();
        System.gc();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    public static void bF() {
        cf.b(cf.a((int) MSG_SYSTEM_EXIT, (Object) null));
    }

    public static void bG() {
        if (!hB) {
            AlertDialog.Builder builder = new AlertDialog.Builder(hx);
            builder.setTitle((int) R.string.alert);
            builder.setMessage((int) R.string.exit_dialog);
            builder.setPositiveButton((int) R.string.yes, new cn());
            builder.setNegativeButton((int) R.string.no, new co());
            builder.setCancelable(false);
            pause();
            builder.create().show();
            hB = true;
        }
    }

    public static ConnectivityManager bH() {
        return (ConnectivityManager) hx.getSystemService("connectivity");
    }

    public static void bI() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) hx.getSystemService("activity")).getRunningAppProcesses();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid != myPid && next.importance > 300) {
                Process.killProcess(next.pid);
            }
        }
    }

    public static int bJ() {
        return hx.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int bK() {
        return hx.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static int bL() {
        return hx.getResources().getConfiguration().orientation;
    }

    public static Timer bM() {
        return hC;
    }

    public static int bN() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 4;
        }
    }

    public static Activity getActivity() {
        return hx;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static String getString(int i) {
        return hx.getString(i);
    }

    public static void pause() {
        int i = hy + 1;
        hy = i;
        if (i == 1) {
            cf.b(cf.a((int) MSG_SYSTEM_ON_PAUSE, (Object) null));
        } else {
            Log.w(LOG_TAG, "The system has already paused." + hy);
        }
    }

    public static void resume() {
        int i = hy - 1;
        hy = i;
        if (i == 0) {
            cf.b(cf.a((int) MSG_SYSTEM_ON_RESUME, (Object) null));
        } else {
            Log.w(LOG_TAG, "The system do not need resumed." + hy);
        }
        if (hy <= 0) {
            hy = 0;
        }
    }
}
