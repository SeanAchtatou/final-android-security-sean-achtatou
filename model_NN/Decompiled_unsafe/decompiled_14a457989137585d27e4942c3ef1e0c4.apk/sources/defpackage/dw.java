package defpackage;

/* renamed from: dw  reason: default package */
public final class dw extends n {
    public static byte ak;
    public byte X;
    public byte Y;
    public byte Z;
    public boolean aT = false;
    public byte af;
    public byte ah;
    public byte aj;
    public byte al;
    public byte am;
    public byte an;
    public byte ap;
    public byte as;
    public byte at;
    public byte au = 2;
    public byte e;
    public int jX;
    public int jY;

    public dw(ao aoVar, byte[][] bArr, byte[][][] bArr2, byte[][] bArr3) {
        super(aoVar, bArr, bArr2, bArr3);
        this.k = 4;
        this.j = 2;
    }

    public final void N() {
        if (!this.aT) {
            Q();
        }
    }

    public final void b(byte[] bArr) {
        this.jX = this.aH;
        this.jY = this.dN;
        ak = bArr[2];
        byte b = bArr[3];
        this.Z = b;
        this.X = b;
        byte b2 = bArr[4];
        this.af = b2;
        this.Y = b2;
        this.ah = bArr[5];
        this.aj = bArr[6];
        this.e = 15;
        this.h = 2;
        this.U = 32;
        this.V = -32;
        this.ad = 32;
        this.ag = -32;
        this.ao = 0;
        this.dS = -8;
        this.dT = 8;
        this.dU = -16;
        this.T = 0;
        this.P = -15;
        this.S = 15;
        this.l = -30;
    }

    public final void c() {
        if (!this.aT) {
            switch (this.as) {
                case 0:
                    if (this.ah != 0) {
                        if (this.an >= this.ah) {
                            this.Z = (byte) (-this.Z);
                            this.an = 0;
                            c(1);
                        }
                        this.an = (byte) (this.an + 1);
                    }
                    if (this.aj != 0) {
                        if (this.ap >= this.aj) {
                            this.af = (byte) (-this.af);
                            this.ap = 0;
                            c(1);
                        }
                        this.ap = (byte) (this.ap + 1);
                    }
                    h(this.Z, this.af);
                    if (!eb.M && !eb.dX && b(this.aH, this.dN, eb.ke[eb.m[0]])) {
                        this.as = 1;
                        return;
                    }
                    return;
                case 1:
                    if (this.aH > eb.ke[eb.m[0]].aH) {
                        c(1);
                        this.aH -= this.au;
                    } else {
                        c(1);
                        this.aH += this.au;
                    }
                    if (this.dN > eb.ke[eb.m[0]].dN) {
                        c(1);
                        this.dN -= this.au;
                    } else {
                        c(1);
                        this.dN += this.au;
                    }
                    if (ee.x(this.aH - eb.ke[eb.m[0]].aH) + ee.x(this.dN - eb.ke[eb.m[0]].dN) > 1600) {
                        this.as = 2;
                        return;
                    } else if (ee.x(this.aH - eb.ke[eb.m[0]].aH) + ee.x(this.dN - eb.ke[eb.m[0]].dN) < 225) {
                        if (this.at != 0) {
                            dz.cn().d(this.at);
                        } else {
                            ea cp = ea.cp();
                            bg.aU().e = 0;
                            bg.aU().f = 0;
                            cp.a(3);
                        }
                        ea.cp().M = false;
                        this.as = 3;
                        a(false);
                        this.al = 0;
                        this.am = 0;
                        return;
                    } else {
                        return;
                    }
                case 2:
                    if (this.aH > this.jX) {
                        this.aH--;
                    } else {
                        this.aH++;
                    }
                    if (this.dN > this.jY) {
                        this.dN--;
                    } else {
                        this.dN++;
                    }
                    if (ee.x(this.aH - this.jX) + ee.x(this.dN - this.jY) < 100) {
                        this.aH = this.jX;
                        this.dN = this.jY;
                        this.as = 0;
                        this.an = 0;
                        this.ap = 0;
                        this.Z = this.X;
                        this.af = this.Y;
                    }
                    if (b(this.aH, this.dN, eb.ke[eb.m[0]])) {
                        this.as = 1;
                        return;
                    }
                    return;
                case 3:
                    this.al = (byte) (this.al + 1);
                    if (this.al == 25) {
                        this.al = 0;
                        this.am = (byte) (this.am + 1);
                        if (this.am == this.e) {
                            this.as = 0;
                            a(this.jX, this.jY);
                            this.an = 0;
                            this.ap = 0;
                            this.Z = this.X;
                            this.af = this.Y;
                            a(true);
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
