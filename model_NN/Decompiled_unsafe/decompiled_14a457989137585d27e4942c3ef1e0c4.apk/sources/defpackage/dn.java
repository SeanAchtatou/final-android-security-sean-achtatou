package defpackage;

import javax.microedition.media.control.VolumeControl;

/* renamed from: dn  reason: default package */
public final class dn implements VolumeControl {
    private ce iG;

    public dn(ce ceVar) {
        this.iG = ceVar;
    }

    public final void b(boolean z) {
        this.iG.hl = false;
        cc.d(false);
    }

    public final int n(int i) {
        int i2 = i < 0 ? 0 : i;
        if (i2 > 100) {
            i2 = 100;
        }
        this.iG.hk = i2;
        cc.C(i2);
        return i2;
    }
}
