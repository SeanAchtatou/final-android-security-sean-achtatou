package defpackage;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

/* renamed from: ac  reason: default package */
public final class ac extends ap {
    public static final int FOREVER = -2;
    protected static Context dh = null;
    public static final af ey = new af("", 4, 0);
    private int eA;
    private LinearLayout eB;
    private ad ez;

    public static int ar() {
        return 2000;
    }

    public final ad aq() {
        return this.ez;
    }

    public final int as() {
        return this.eA;
    }

    public final int at() {
        return 5;
    }

    public final View getView() {
        return this.eB;
    }
}
