package defpackage;

import java.util.Hashtable;

/* renamed from: am  reason: default package */
public final class am {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int FONT_INPUT_TEXT = 1;
    public static final int FONT_STATIC_TEXT = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 4;
    private static final am eZ = new am(0, 0, 0);
    private static am[] fa = {eZ, eZ};
    private static final Hashtable fb = new Hashtable();
    private int dk;
    private int fc;
    private int fd;

    private am(int i, int i2, int i3) {
        if (i == 0 || i == 32 || i == 64) {
            if (!(this.fd == 0)) {
                if (!((this.fd & 1) != 0)) {
                    if (!((this.fd & 2) != 0)) {
                        if (!((this.fd & 4) != 0)) {
                            throw new IllegalArgumentException();
                        }
                    }
                }
            }
            if (i3 == 8 || i3 == 0 || i3 == 16) {
                this.fc = i;
                this.fd = i2;
                this.dk = i3;
                return;
            }
            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException();
    }

    public static am a(int i, int i2, int i3) {
        Integer num = new Integer(i3 + 0 + 0);
        am amVar = (am) fb.get(num);
        if (amVar != null) {
            return amVar;
        }
        am amVar2 = new am(0, 0, i3);
        fb.put(num, amVar2);
        return amVar2;
    }

    public static am aF() {
        return eZ;
    }

    public final int a(char c) {
        return de.a(this, c);
    }

    public final int aG() {
        return this.fc;
    }

    public final int getHeight() {
        return de.c(this);
    }

    public final int getSize() {
        return this.dk;
    }

    public final int getStyle() {
        return this.fd;
    }

    public final int hashCode() {
        return this.fd + this.dk + this.fc;
    }

    public final int o(String str) {
        return de.a(this, str);
    }
}
