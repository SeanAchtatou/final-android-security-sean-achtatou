package defpackage;

import android.app.AlertDialog;
import com.androidbox.jhfylhtc.R;

/* renamed from: cj  reason: default package */
final class cj implements ck {
    /* synthetic */ cj() {
        this((byte) 0);
    }

    private cj(byte b) {
    }

    public final void aA() {
        AlertDialog.Builder builder = new AlertDialog.Builder(cl.getActivity());
        builder.setTitle((int) R.string.about);
        builder.setMessage((int) R.string.about_dialog);
        builder.create().show();
    }

    public final String ay() {
        return cl.getString(R.string.about);
    }

    public final int getId() {
        return ch.OPTION_MENU_ITEM_ABOUT;
    }
}
