package defpackage;

import com.nokia.mid.ui.DirectGraphics;
import com.nokia.mid.ui.FullCanvas;
import javax.microedition.media.control.MIDIControl;

/* renamed from: ed  reason: default package */
public final class ed {
    public static boolean M = false;
    public static byte g;
    public static byte h;
    private static ed kk;
    public static n n;
    public static n o;
    public String[] aC;
    public int aH;
    public char[] aP;
    public int ac;
    public int dN;
    public int dO;
    public byte e = 0;
    public int eb = 0;
    public int ec = 0;
    public int ed = 65535;
    private int ee;
    public byte f = 1;
    public char[] kl;
    public char[] km;

    private ed() {
    }

    private void Q() {
        switch (this.eb) {
            case 0:
                this.aH++;
                if (this.aH % this.dN == 0 || this.aH == this.aP.length) {
                    this.eb = 1;
                    return;
                }
                return;
            case 1:
                int i = this.ec + 1;
                this.ec = i;
                this.ec = i > 10 ? 0 : this.ec;
                return;
            default:
                return;
        }
    }

    public static void a(an anVar, int i) {
        anVar.setColor(13413246);
        anVar.c(30, 320 - i, 240 - a.M().F.getWidth(), 320 - i);
        anVar.setColor(9730137);
        anVar.a(30, 320 - i, 240 - a.M().F.getWidth(), 320 - i);
        anVar.a(30, (320 - i) + 2, 240 - a.M().F.getWidth(), (320 - i) + 2);
        if (i >= a.M().F.getHeight()) {
            anVar.a(30, 319, 240 - a.M().F.getWidth(), 319);
            anVar.a(30, 317, 240 - a.M().F.getWidth(), 317);
        }
        du.a(anVar, a.M().F, 0, 0, 320 - i, 0);
        du.a(anVar, a.M().F, 2, 240 - a.M().F.getWidth(), 320 - i, 0);
    }

    private static void a(an anVar, int i, int i2, int i3, int i4) {
        anVar.setColor(3548436);
        anVar.c(i, i2, i3, i4);
        a.M().b(anVar, 3548436, i, i2, i3, i4);
    }

    private void a(char[] cArr) {
        this.aP = cArr;
        this.ac = 0;
        this.aH = 0;
        this.eb = 0;
        this.ec = 0;
        this.dO = 210 / p.f;
        this.dN = this.dO << 1;
    }

    private void b(an anVar, int i) {
        a(anVar, 60);
        anVar.setColor(i);
        int i2 = this.dN * this.ac;
        for (int i3 = i2; i3 < this.aH; i3++) {
            anVar.a(this.aP[i3], (((i3 - i2) % this.dO) * p.f) + 15, (277 - p.g) + (((i3 - i2) / this.dO) * 25), 20);
        }
        if (this.eb == 1 && this.ec > 5) {
            anVar.setColor(2035968);
            anVar.c(120, 315, 115, 312, 125, 312);
        }
    }

    public static ed cB() {
        if (kk == null) {
            kk = new ed();
        }
        return kk;
    }

    public final void N() {
        if (this.aH == this.aP.length) {
            if (bi.y(4)) {
                byte b = (byte) (this.f - 1);
                this.f = b;
                this.f = b <= 0 ? (byte) (this.aC.length - 1) : this.f;
            } else if (bi.y(8)) {
                byte b2 = (byte) (this.f + 1);
                this.f = b2;
                this.f = b2 > this.aC.length - 1 ? 1 : this.f;
            } else if (bi.y(16)) {
                dz.cn().e(this.f);
                M = false;
            }
        } else if (bi.y(MIDIControl.NOTE_ON)) {
            switch (this.eb) {
                case 0:
                    int i = this.aH + (this.dN - (this.aH % this.dN));
                    this.aH = i;
                    this.aH = i > this.aP.length ? this.aP.length : this.aH;
                    this.eb = 1;
                    return;
                case 1:
                    this.ac++;
                    this.eb = 0;
                    return;
                default:
                    return;
            }
        }
    }

    public final void a(byte b, byte b2, String[] strArr) {
        M = true;
        this.aC = strArr;
        h = b;
        a(b == 1 ? new StringBuffer().append(dz.aC[b2 - 1]).append(":").append(this.aC[0]).toString().toCharArray() : new StringBuffer().append("任务提示:").append(this.aC[0]).toString().toCharArray());
    }

    public final void a(int i, char[] cArr) {
        this.e = (byte) i;
        switch (i) {
            case FullCanvas.KEY_RIGHT_ARROW:
                this.ed = 16776960;
                break;
            case FullCanvas.KEY_LEFT_ARROW:
            case -2:
            case -1:
                this.ed = 16777215;
                break;
            case 0:
            case 1:
            case 4:
            case 5:
            default:
                this.ed = 16777215;
                break;
            case 2:
                this.ed = 16711680;
                break;
            case 3:
                this.ed = 16777215;
                a.M().t = ee.O("/u/qiu");
                break;
            case 6:
                this.ed = 16777215;
                this.aP = cArr;
                this.eb = 1;
                this.ac = 0;
                this.aH = 0;
                this.ec = 0;
                this.dO = 160 / p.f;
                this.dN = this.dO << 1;
                ea.cp().ae = true;
                return;
            case 7:
                this.ed = 16777215;
                o = c.g("/u/lf");
                break;
        }
        a(cArr);
        ea.cp().ae = true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(defpackage.an r8) {
        /*
            r7 = this;
            r3 = 30
            r5 = 240(0xf0, float:3.36E-43)
            r4 = 120(0x78, float:1.68E-43)
            r6 = 17
            r2 = 0
            r7.Q()
            byte r0 = r7.e
            switch(r0) {
                case -4: goto L_0x007d;
                case -3: goto L_0x0012;
                case -2: goto L_0x0012;
                case -1: goto L_0x0012;
                case 0: goto L_0x006e;
                case 1: goto L_0x0083;
                case 2: goto L_0x007d;
                case 3: goto L_0x0035;
                case 4: goto L_0x00bb;
                case 5: goto L_0x009e;
                case 6: goto L_0x0011;
                case 7: goto L_0x0057;
                default: goto L_0x0011;
            }
        L_0x0011:
            return
        L_0x0012:
            n r0 = defpackage.ed.n
            if (r0 == 0) goto L_0x0035
            byte r0 = r7.e
            r1 = -1
            if (r0 == r1) goto L_0x0020
            byte r0 = r7.e
            r1 = -2
            if (r0 != r1) goto L_0x0075
        L_0x0020:
            r0 = 50
        L_0x0022:
            n r1 = defpackage.ed.n
            int r0 = r5 - r0
            r3 = 260(0x104, float:3.64E-43)
            r1.a(r0, r3)
            n r0 = defpackage.ed.n
            r0.Q()
            n r0 = defpackage.ed.n
            r0.c(r8)
        L_0x0035:
            a r0 = defpackage.a.M()
            ao r0 = r0.t
            if (r0 == 0) goto L_0x0057
            a r0 = defpackage.a.M()
            ao r1 = r0.t
            a r0 = defpackage.a.M()
            ao r0 = r0.t
            int r0 = r0.getWidth()
            int r3 = r5 - r0
            r4 = 260(0x104, float:3.64E-43)
            r5 = 36
            r0 = r8
            defpackage.du.a(r0, r1, r2, r3, r4, r5)
        L_0x0057:
            n r0 = defpackage.ed.o
            if (r0 == 0) goto L_0x006e
            n r0 = defpackage.ed.o
            r1 = 194(0xc2, float:2.72E-43)
            r2 = 260(0x104, float:3.64E-43)
            r0.a(r1, r2)
            n r0 = defpackage.ed.o
            r0.Q()
            n r0 = defpackage.ed.o
            r0.c(r8)
        L_0x006e:
            r0 = 2035968(0x1f1100, float:2.852999E-39)
            r7.b(r8, r0)
            goto L_0x0011
        L_0x0075:
            byte r0 = r7.e
            r1 = -3
            if (r0 != r1) goto L_0x00e7
            r0 = 55
            goto L_0x0022
        L_0x007d:
            r0 = 16711680(0xff0000, float:2.3418052E-38)
            r7.b(r8, r0)
            goto L_0x0011
        L_0x0083:
            r0 = 145(0x91, float:2.03E-43)
            a(r8, r2, r0, r5, r3)
            int r0 = r7.ed
            r8.setColor(r0)
            char[] r1 = r7.aP
            char[] r0 = r7.aP
            int r3 = r0.length
            r0 = 160(0xa0, float:2.24E-43)
            byte r5 = defpackage.p.g
            int r5 = r0 - r5
            r0 = r8
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x0011
        L_0x009e:
            r0 = 145(0x91, float:2.03E-43)
            r1 = 180(0xb4, float:2.52E-43)
            a(r8, r3, r0, r1, r3)
            int r0 = r7.ed
            r8.setColor(r0)
            char[] r1 = r7.aP
            char[] r0 = r7.aP
            int r3 = r0.length
            r0 = 160(0xa0, float:2.24E-43)
            byte r5 = defpackage.p.g
            int r5 = r0 - r5
            r0 = r8
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x0011
        L_0x00bb:
            r0 = 135(0x87, float:1.89E-43)
            r1 = 50
            a(r8, r2, r0, r5, r1)
            int r0 = r7.ed
            r8.setColor(r0)
            char[] r1 = r7.aP
            char[] r0 = r7.aP
            int r3 = r0.length
            r0 = 148(0x94, float:2.07E-43)
            byte r5 = defpackage.p.g
            int r5 = r0 - r5
            r0 = r8
            r0.a(r1, r2, r3, r4, r5, r6)
            char[] r1 = r7.kl
            char[] r0 = r7.kl
            int r3 = r0.length
            r0 = 172(0xac, float:2.41E-43)
            byte r5 = defpackage.p.g
            int r5 = r0 - r5
            r0 = r8
            r0.a(r1, r2, r3, r4, r5, r6)
            goto L_0x0011
        L_0x00e7:
            r0 = r2
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ed.a(an):void");
    }

    public final void a(char[] cArr, int i) {
        this.ee = 16777215;
        this.km = cArr;
        ea.cp().dN = i;
        ea.cp().M = true;
    }

    public final void a(char[] cArr, char[] cArr2) {
        this.e = 4;
        this.ac = 0;
        this.aH = 0;
        this.eb = 0;
        this.ec = 0;
        this.aP = cArr;
        this.kl = cArr2;
        this.ed = 16777215;
        this.dO = 210 / p.f;
        this.dN = this.dO << 1;
        ea.cp().ae = true;
    }

    public final void c() {
        if (bi.y(MIDIControl.NOTE_ON)) {
            switch (this.eb) {
                case 0:
                    int i = this.aH + (this.dN - (this.aH % this.dN));
                    this.aH = i;
                    this.aH = i > this.aP.length ? this.aP.length : this.aH;
                    this.eb = 1;
                    return;
                case 1:
                    if (this.aH == this.aP.length) {
                        ea.cp().ae = false;
                        ea.cp().dX = false;
                        n = null;
                        a.M().t = null;
                        o = null;
                    }
                    this.ac++;
                    this.eb = 0;
                    return;
                default:
                    return;
            }
        }
    }

    public final void c(an anVar) {
        a(anVar, 60, 10, 120, 30);
        anVar.setColor(this.ee);
        anVar.a(this.km, 0, this.km.length, 120, 25 - p.g, 17);
        ea.cp().dN--;
        if (ea.cp().dN <= 0) {
            ea.cp().dN = 0;
            ea.cp().M = false;
        }
    }

    public final void d(an anVar) {
        Q();
        a(anVar, 60);
        if (h == 1) {
            anVar.setColor(2035968);
        } else {
            anVar.setColor(16711680);
        }
        int i = this.dN * this.ac;
        for (int i2 = i; i2 < this.aH; i2++) {
            anVar.a(this.aP[i2], (((i2 - i) % this.dO) * p.f) + 15, (277 - p.g) + (((i2 - i) / this.dO) * 25), 20);
        }
        if (this.eb == 1 && this.ec > 5 && this.aH != this.aP.length) {
            anVar.setColor(2035968);
            anVar.c(120, 315, 115, 312, 125, 312);
        }
        if (this.aH == this.aP.length) {
            for (int i3 = 1; i3 < this.aC.length; i3++) {
                a(anVar, 190, ((i3 - 1) * 20) + 220, 50, 20);
                l al = l.al();
                l al2 = l.al();
                int i4 = al2.ac + 1;
                al2.ac = i4;
                al.ac = i4 > 30 ? 0 : l.al().ac;
                if ((l.al().ac >> 4) % 4 == 0) {
                    anVar.setColor(16776960);
                } else {
                    anVar.setColor(16711680);
                }
                anVar.c(DirectGraphics.ROTATE_180, ((this.f - 1) * 20) + 225, DirectGraphics.ROTATE_180, ((this.f - 1) * 20) + 235, 190, ((this.f - 1) * 20) + 230);
                anVar.setColor(16777215);
                anVar.a(this.aC[i3], 215, ((i3 - 1) * 20) + 240, 33);
            }
        }
    }
}
