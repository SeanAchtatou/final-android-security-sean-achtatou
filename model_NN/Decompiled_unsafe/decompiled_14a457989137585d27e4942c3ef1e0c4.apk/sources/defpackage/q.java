package defpackage;

import com.nokia.mid.ui.DirectGraphics;
import java.lang.reflect.Array;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;

/* renamed from: q  reason: default package */
public final class q {
    private static q ep;
    ao A;
    ao B;
    public byte[] L = {3, 0, 4, 2, 1};
    public boolean M;
    public byte[] N;
    public short[] O = {50, 100, 180, 240, 300};
    public byte R;
    public byte W;
    public byte X;
    public byte Y;
    private byte Z;
    public byte[] aA = {0, -10, -20, -10, 0};
    public byte[] aB;
    public String[] aC = {"进入机关", "付费通过"};
    public String[] aE = {"进入机关", "直接通过"};
    public String aF = null;
    private int aH;
    private int ac;
    byte af;
    private byte ah;
    private byte aj = 6;
    private byte ak;
    public byte[] aw = {0, 2, 3, 1, 4};
    public byte[] ax = {0, 1, 2, 3, 4};
    public byte[] ay;
    public byte[] az;
    private int dN;
    private int dO;
    private byte[][] dP;
    public byte[][] dR = {new byte[]{-10, -47}, new byte[]{31, -21}, new byte[]{14, 23}, new byte[]{-34, 23}, new byte[]{-50, -21}};
    public byte e;
    private byte[][] eq = {new byte[]{1, 2}, new byte[]{1, 2}, new byte[]{0, 0}};
    private byte[][] er = {new byte[]{2, 1}, new byte[]{1, 2}, new byte[]{0, 0}};
    private byte[][] es = {new byte[]{2, 2}, new byte[]{1, 1}, new byte[]{0, 0}};
    private byte[][] et;
    public byte f;
    public byte g;
    public byte h;
    public byte i;
    public byte j;
    public byte[] m = null;
    ao q;
    ao r;
    ao s;
    ao t;
    ao u;
    ao v;
    ao w;

    private static void a(an anVar, ao aoVar, byte[][] bArr, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = 0;
        while (true) {
            int i9 = i8;
            if (i9 < bArr.length) {
                int i10 = 0;
                while (true) {
                    int i11 = i10;
                    if (i11 >= bArr[i9].length) {
                        i8 = i9 + 1;
                    } else {
                        if (bArr[i9][i11] > 0) {
                            int i12 = 0;
                            switch (bArr[i9][i11]) {
                                case 1:
                                    i12 = 0;
                                    break;
                                case 2:
                                    i12 = 1;
                                    break;
                            }
                            du.a(anVar, aoVar, i12 * i4, 0, i4, i5, 0, i2 + (i11 * i6), i3 + (i9 * i7));
                        }
                        i10 = i11 + 1;
                    }
                }
            } else {
                return;
            }
        }
    }

    public static q ao() {
        if (ep == null) {
            ep = new q();
        }
        return ep;
    }

    private void b(an anVar) {
        int i2;
        f(anVar);
        switch (this.m[this.g]) {
            case 1:
                this.aF = r.aC[this.N[this.g]];
                i2 = 2;
                break;
            case 2:
                this.aF = bj.gv[this.N[this.g]];
                i2 = 0;
                break;
            case 3:
                this.aF = String.valueOf((int) this.O[this.N[this.g]]);
                i2 = 3;
                break;
            case 4:
                this.aF = "谢谢惠顾";
                i2 = 1;
                break;
            default:
                i2 = 0;
                break;
        }
        for (int i3 = 0; i3 < 5; i3++) {
            if (i3 != this.g) {
                anVar.a("选择木签", 120, (i3 * 30) + 103, 33);
            }
        }
        anVar.setColor(0);
        anVar.a(this.aF, 120, (this.g * 30) + 103, 33);
        du.a(anVar, this.q, i2 * 26, 0, 26, 22, 0, 150, (this.g * 30) + 82);
        anVar.setColor(65280);
        anVar.b((120 - (this.r.getWidth() / 2)) - 1, (104 - this.r.getHeight()) + (this.g * 30), 125, 24);
    }

    private void f(an anVar) {
        anVar.setColor(0);
        for (int i2 = 0; i2 < 5; i2++) {
            du.a(anVar, this.r, 0, 120, (i2 * 30) + 105, 33);
        }
    }

    private void h(an anVar) {
        for (int i2 = 0; i2 < this.az.length; i2++) {
            du.a(anVar, this.A, (this.az[i2] + 5) * 28, 0, 28, 21, 0, (i2 * 30) + 47, this.aA[i2] + 90);
        }
        du.a(anVar, this.v, 0, 120, ((this.v.getHeight() / 2) + 160) - 2, 33);
        du.a(anVar, this.B, 0, 40, 190, 20);
        du.a(anVar, this.B, 2, 166, 191, 20);
        if (this.M) {
            for (int i3 = 0; i3 < this.aB.length; i3++) {
                if (this.aB[i3] != 0) {
                    du.a(anVar, this.A, this.az[i3] * 28, 0, 28, 21, 0, (i3 * 30) + 47, this.aA[i3] + 90);
                }
            }
        }
        for (int i4 = 0; i4 < this.ay.length; i4++) {
            du.a(anVar, this.w, this.ay[i4] * 20, 0, 20, 20, 0, this.dR[i4][0] + 120, this.dR[i4][1] + 160);
        }
    }

    private void l(an anVar) {
        int i2;
        int i3;
        if (this.ah == 1) {
            anVar.setColor(65535);
            anVar.c((this.dN * 35) + 50, (this.dO * 35) + 100, 35, 35);
        }
        anVar.setColor(65280);
        anVar.c((this.ac * 35) + 50, (this.aH * 35) + 100, 35, 35);
        anVar.setColor(0);
        for (int i4 = 0; i4 < 4; i4++) {
            anVar.c(50, (i4 * 35) + 100, 70, 3);
        }
        for (int i5 = 0; i5 < 3; i5++) {
            anVar.c((i5 * 35) + 47, 100, 3, 108);
        }
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i6 < 6) {
            du.a(anVar, this.u, 0, (i8 * 35) + 55, (i7 * 35) + 107, 0);
            int i9 = i8 + 1;
            if (i9 > 1) {
                i2 = 0;
                i3 = i7 + 1;
            } else {
                i2 = i9;
                i3 = i7;
            }
            i6++;
            i7 = i3;
            i8 = i2;
        }
        a(anVar, this.s, this.dP, 57, 109, 20, 20, 35, 35);
        anVar.setColor(14406824);
        anVar.c(140, 75, 50, 50);
        anVar.setColor(0);
        for (int i10 = 0; i10 < 3; i10++) {
            anVar.c(140, (i10 * 25) + 75, 50, 1);
            anVar.c((i10 * 25) + 140, 75, 1, 51);
        }
        a(anVar, this.t, this.et, 146, 81, 14, 13, 25, 25);
        anVar.setColor(14406824);
        anVar.c(125, DirectGraphics.ROTATE_180, 80, 30);
        anVar.setColor(12759685);
        anVar.c(125, 204, 80, 28);
        anVar.setColor(0);
        anVar.b(123, 178, 84, 54);
        anVar.b(125, DirectGraphics.ROTATE_180, 80, 50);
        anVar.a(125, 204, 204, 204);
        anVar.setColor(5849662);
        anVar.a("剩余步数", 166, 204, 33);
        anVar.a(String.valueOf((int) this.aj), 165, 230, 33);
    }

    private void o(an anVar) {
        for (int i2 = 0; i2 < 2; i2++) {
            a.M().b(anVar, 13413246, 74, (i2 * 60) + 108, 90, 40);
        }
        anVar.setColor(3548436);
        anVar.c(76, (this.ak * ToneControl.C4) + 110, 86, 36);
        for (int i3 = 0; i3 < 2; i3++) {
            if (i3 == this.ak) {
                anVar.setColor(16777215);
            } else {
                anVar.setColor(16711680);
            }
            if (b.m[17] < 0) {
                anVar.a(this.aE[i3], 120, (i3 * 60) + 140, 33);
            } else {
                anVar.a(this.aC[i3], 120, (i3 * 60) + 140, 33);
            }
        }
    }

    public final void N() {
        boolean z;
        if (bi.ac != 0) {
            ea.cp().dZ = true;
        }
        switch (this.f) {
            case 0:
                switch (this.e) {
                    case 0:
                        if (bi.y(8)) {
                            byte b = (byte) (this.g - 1);
                            this.g = b;
                            this.g = b < 0 ? (byte) (this.m.length - 1) : this.g;
                            return;
                        } else if (bi.y(4)) {
                            byte b2 = (byte) (this.g + 1);
                            this.g = b2;
                            this.g = b2 > this.m.length - 1 ? 0 : this.g;
                            return;
                        } else if (bi.y(16)) {
                            this.f = (byte) 1;
                            return;
                        } else if (bi.y(1280)) {
                            ea.cp().a(1);
                            this.g = 0;
                            return;
                        } else {
                            return;
                        }
                    case 1:
                        if (bi.y(2)) {
                            this.j = this.ay[0];
                            for (int i2 = 0; i2 < this.ay.length; i2++) {
                                if (i2 < this.ay.length - 1) {
                                    this.ay[i2] = this.ay[i2 + 1];
                                } else {
                                    this.ay[i2] = this.j;
                                }
                            }
                            return;
                        } else if (bi.y(1)) {
                            this.j = this.ay[this.ay.length - 1];
                            for (int length = this.ay.length - 1; length >= 0; length--) {
                                if (length > 0) {
                                    this.ay[length] = this.ay[length - 1];
                                } else {
                                    this.ay[length] = this.j;
                                }
                            }
                            return;
                        } else if (bi.y(16)) {
                            this.R = this.ay[0];
                            if (this.az[this.W] == this.R) {
                                this.M = true;
                                this.aB[this.W] = 1;
                                this.W = (byte) (this.W + 1);
                                if (this.W == this.az.length) {
                                    this.f = (byte) 1;
                                    return;
                                }
                                return;
                            }
                            this.f = (byte) 2;
                            return;
                        } else if (bi.y(1280)) {
                            this.f = (byte) 3;
                            return;
                        } else if (bi.y(128)) {
                            this.Y = 1;
                            if (b.m[17] < 0) {
                                dz.cn().S();
                                return;
                            }
                            b.Z().b(b.Z().aB, 2);
                            ea.cp().a(7);
                            return;
                        } else {
                            return;
                        }
                    case 2:
                        if (bi.y(8)) {
                            int i3 = this.aH - 1;
                            this.aH = i3;
                            this.aH = i3 < 0 ? 0 : this.aH;
                            return;
                        } else if (bi.y(4)) {
                            int i4 = this.aH + 1;
                            this.aH = i4;
                            this.aH = i4 > this.dP.length - 1 ? this.dP.length - 1 : this.aH;
                            return;
                        } else if (bi.y(2)) {
                            int i5 = this.ac - 1;
                            this.ac = i5;
                            this.ac = i5 < 0 ? 0 : this.ac;
                            return;
                        } else if (bi.y(1)) {
                            int i6 = this.ac + 1;
                            this.ac = i6;
                            this.ac = i6 > this.dP[0].length - 1 ? this.dP[0].length - 1 : this.ac;
                            return;
                        } else if (bi.y(16)) {
                            if (this.ah > 0 || this.dP[this.aH][this.ac] != 0) {
                                this.ah = (byte) (this.ah + 1);
                            }
                            switch (this.ah) {
                                case 1:
                                    this.dN = this.ac;
                                    this.dO = this.aH;
                                    break;
                                case 2:
                                    this.ah = 0;
                                    if (this.dP[this.aH][this.ac] == 0 && ((Math.abs(this.ac - this.dN) == 0 && Math.abs(this.aH - this.dO) == 1) || (Math.abs(this.aH - this.dO) == 0 && Math.abs(this.ac - this.dN) == 1))) {
                                        this.aj = (byte) (this.aj - 1);
                                        this.dP[this.aH][this.ac] = this.dP[this.dO][this.dN];
                                        this.dP[this.dO][this.dN] = 0;
                                        break;
                                    }
                            }
                            if (this.aj == 0) {
                                int i7 = 0;
                                while (true) {
                                    if (i7 >= this.dP.length) {
                                        z = true;
                                    } else {
                                        int i8 = 0;
                                        while (i8 < this.dP[i7].length) {
                                            if (this.dP[i7][i8] != this.et[i7][i8]) {
                                                z = false;
                                            } else {
                                                i8++;
                                            }
                                        }
                                        i7++;
                                    }
                                }
                                if (z) {
                                    this.f = (byte) 1;
                                    return;
                                } else {
                                    this.f = (byte) 2;
                                    return;
                                }
                            } else {
                                return;
                            }
                        } else if (bi.y(1280)) {
                            this.f = (byte) 3;
                            return;
                        } else if (!bi.y(128)) {
                            return;
                        } else {
                            if (b.m[17] < 0) {
                                dz.cn().S();
                                return;
                            }
                            this.Y = 1;
                            b.Z().b(b.Z().aB, 2);
                            ea.cp().a(7);
                            return;
                        }
                    default:
                        return;
                }
            case 1:
                switch (this.e) {
                    case 0:
                        this.h = (byte) (this.h + 1);
                        if (this.h > 25) {
                            this.h = 0;
                            switch (this.m[this.g]) {
                                case 1:
                                    eb.cq().k(this.N[this.g], 1);
                                    break;
                                case 2:
                                    eb.cq().h(this.N[this.g], 1);
                                    break;
                                case 3:
                                    eb.cq();
                                    eb.a(this.O[this.N[this.g]]);
                                    break;
                            }
                            this.f = (byte) 2;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            case 2:
                if (bi.y(16)) {
                    this.i = (byte) (this.i + 1);
                    this.M = false;
                    return;
                }
                return;
            case 3:
                if (bi.y(8)) {
                    byte b3 = (byte) (this.ak - 1);
                    this.ak = b3;
                    this.ak = b3 < 0 ? 1 : this.ak;
                    return;
                } else if (bi.y(4)) {
                    byte b4 = (byte) (this.ak + 1);
                    this.ak = b4;
                    this.ak = b4 > 1 ? 0 : this.ak;
                    return;
                } else if (bi.y(1280)) {
                    ea.cp().a(1);
                    return;
                } else if (bi.y(MIDIControl.NOTE_ON)) {
                    switch (this.ak) {
                        case 0:
                            this.f = (byte) 0;
                            return;
                        case 1:
                            this.Y = 0;
                            if (b.m[17] < 0) {
                                dz.cn().S();
                                return;
                            }
                            b.Z().b(b.Z().aB, 2);
                            ea.cp().a(7);
                            return;
                        default:
                            return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public final void a(byte b) {
        this.e = b;
        if (this.e == 0) {
            this.f = (byte) 0;
        } else {
            this.f = (byte) 3;
        }
    }

    public final void a(an anVar) {
        char[] charArray;
        char[] charArray2;
        if (this.e == 0 || this.f >= 3) {
            a.M().b(anVar, 0);
        } else {
            a.M().b(anVar, 1);
            anVar.setColor(16711680);
            if (b.m[17] < 0) {
                anVar.a("直接通过", 62, 255, 33);
            } else {
                anVar.a("付费通过", 62, 255, 33);
            }
        }
        switch (this.f) {
            case 0:
                switch (this.e) {
                    case 0:
                        anVar.setColor(65280);
                        anVar.b((120 - (this.r.getWidth() / 2)) - 1, (104 - this.r.getHeight()) + (this.g * 30), 125, 24);
                        f(anVar);
                        anVar.a(p.eo);
                        for (int i2 = 0; i2 < 5; i2++) {
                            anVar.a("选择木签", 120, (i2 * 30) + 103, 33);
                        }
                        return;
                    case 1:
                        h(anVar);
                        return;
                    case 2:
                        l(anVar);
                        return;
                    default:
                        return;
                }
            case 1:
                switch (this.e) {
                    case 0:
                        b(anVar);
                        return;
                    case 1:
                        dz.cn().S();
                        this.v = null;
                        this.w = null;
                        this.A = null;
                        this.B = null;
                        return;
                    case 2:
                        dz.cn().S();
                        this.s = null;
                        this.t = null;
                        this.u = null;
                        return;
                    default:
                        return;
                }
            case 2:
                switch (this.e) {
                    case 0:
                        b(anVar);
                        if (this.i <= 0) {
                            switch (this.m[this.g]) {
                                case 1:
                                    charArray = "获得物品：".toCharArray();
                                    charArray2 = new StringBuffer().append(r.aC[this.N[this.g]]).append("*1").toString().toCharArray();
                                    break;
                                case 2:
                                    charArray = "获得装备：".toCharArray();
                                    charArray2 = new StringBuffer().append(bj.gv[this.N[this.g]]).append("*1").toString().toCharArray();
                                    break;
                                case 3:
                                    charArray = "获得金钱：".toCharArray();
                                    charArray2 = String.valueOf((int) this.O[this.N[this.g]]).toCharArray();
                                    break;
                                case 4:
                                    charArray = "谢谢惠顾".toCharArray();
                                    charArray2 = null;
                                    break;
                                default:
                                    charArray = null;
                                    charArray2 = null;
                                    break;
                            }
                            if (this.m[this.g] == 4) {
                                ed.cB().a(5, charArray);
                                return;
                            } else {
                                ed.cB().a(charArray, charArray2);
                                return;
                            }
                        } else {
                            ea.cp().ae = false;
                            ea.cp().a(1);
                            this.g = 0;
                            this.i = 0;
                            this.q = null;
                            this.r = null;
                            this.f = (byte) 0;
                            return;
                        }
                    case 1:
                        h(anVar);
                        if (this.i <= 0) {
                            ed.cB().a(5, "调整失败".toCharArray());
                            return;
                        }
                        ea.cp().ae = false;
                        this.i = 0;
                        b(this.X);
                        this.f = (byte) 0;
                        return;
                    case 2:
                        l(anVar);
                        if (this.i <= 0) {
                            ed.cB().a(5, "超过规定次数".toCharArray());
                            return;
                        }
                        ea.cp().ae = false;
                        this.i = 0;
                        c(this.Z);
                        this.f = (byte) 0;
                        return;
                    default:
                        return;
                }
            case 3:
                o(anVar);
                return;
            default:
                return;
        }
    }

    public final void b(byte b) {
        this.M = false;
        this.X = b;
        this.ay = new byte[5];
        this.aB = new byte[5];
        for (int i2 = 0; i2 < this.ay.length; i2++) {
            this.ay[i2] = this.ax[i2];
        }
        switch (this.X) {
            case 1:
                this.az = this.L;
                break;
            case 2:
                this.az = this.aw;
                break;
        }
        this.W = 0;
    }

    public final void c() {
        switch (this.e) {
            case 0:
                this.q = ee.O("/u/qian");
                this.r = ee.O("/u/tiao");
                this.m = new byte[5];
                this.N = new byte[5];
                for (int i2 = 0; i2 < this.m.length; i2++) {
                    int q2 = ee.q(0, 99);
                    if (q2 < 50) {
                        this.m[i2] = 4;
                    } else if (q2 >= 50 && q2 < 65) {
                        this.m[i2] = 1;
                        this.N[i2] = (byte) ee.q(0, 21);
                    } else if (q2 < 65 || q2 >= 80) {
                        this.m[i2] = 3;
                        this.N[i2] = (byte) ee.q(0, this.O.length - 1);
                    } else {
                        this.m[i2] = 2;
                        this.N[i2] = (byte) ee.q(0, 35);
                    }
                }
                return;
            case 1:
                this.v = ee.O("/u/fiveB");
                this.w = ee.O("/u/fiveW");
                this.A = ee.O("/u/five");
                this.B = ee.O("/u/fArrow");
                return;
            case 2:
                this.s = ee.O("/u/dazhu");
                this.t = ee.O("/u/xzhu");
                this.u = ee.O("/u/zhucao");
                return;
            default:
                return;
        }
    }

    public final void c(int i2) {
        this.af = (byte) i2;
        this.Z = this.af;
        this.dP = null;
        this.dP = (byte[][]) Array.newInstance(Byte.TYPE, 3, 2);
        for (int i3 = 0; i3 < this.dP.length; i3++) {
            for (int i4 = 0; i4 < this.dP[i3].length; i4++) {
                this.dP[i3][i4] = this.eq[i3][i4];
            }
        }
        switch (this.af) {
            case 1:
                this.et = this.er;
                break;
            case 2:
                this.et = this.es;
                break;
        }
        this.aj = 6;
        this.ac = 0;
        this.aH = 0;
        this.ah = 0;
        this.i = 0;
    }
}
