package defpackage;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.microedition.media.PlayerListener;

/* renamed from: dg  reason: default package */
public final class dg implements w {
    private HttpURLConnection iF;
    private URL url;

    public dg(String str, int i, boolean z) {
        try {
            if (!bX()) {
                throw new IOException("No avaliable connection.");
            }
            this.url = new URL(str);
            this.iF = (HttpURLConnection) this.url.openConnection();
            this.iF.setDoInput(true);
            if (i != 1) {
                this.iF.setDoOutput(true);
            }
            if (z) {
                this.iF.setConnectTimeout(10000);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static boolean bX() {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager bH = cl.bH();
            return bH != null && (activeNetworkInfo = bH.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            Log.v(PlayerListener.ERROR, e.toString());
        }
    }

    public final void close() {
        this.iF.disconnect();
    }
}
