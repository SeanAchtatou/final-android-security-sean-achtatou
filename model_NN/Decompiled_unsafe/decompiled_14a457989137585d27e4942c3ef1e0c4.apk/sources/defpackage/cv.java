package defpackage;

import android.util.Log;

/* renamed from: cv  reason: default package */
public final class cv {
    public static final String LOG_TAG = "VDManager";
    public static cy hK;

    protected static cy E(String str) {
        try {
            cy cyVar = (cy) Class.forName(str).newInstance();
            hK = cyVar;
            cyVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create virtual device. " + e);
            e.printStackTrace();
        }
        return hK;
    }
}
