package defpackage;

import android.app.AlertDialog;
import android.view.View;

/* renamed from: ai  reason: default package */
final class ai implements Runnable {
    final /* synthetic */ ah eP;
    private final /* synthetic */ String eQ;
    private final /* synthetic */ AlertDialog eR;
    private final /* synthetic */ View eS;

    ai(ah ahVar, String str, AlertDialog alertDialog, View view) {
        this.eP = ahVar;
        this.eQ = str;
        this.eR = alertDialog;
        this.eS = view;
    }

    public final void run() {
        if (this.eQ != null) {
            this.eR.setTitle(this.eQ);
            this.eR.setView(this.eS);
            this.eR.show();
        }
    }
}
