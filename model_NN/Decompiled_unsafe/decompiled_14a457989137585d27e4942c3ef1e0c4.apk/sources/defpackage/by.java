package defpackage;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;

/* renamed from: by  reason: default package */
final class by implements View.OnTouchListener {
    final /* synthetic */ bv gW;

    /* synthetic */ by(bv bvVar) {
        this(bvVar, (byte) 0);
    }

    private by(bv bvVar, byte b) {
        this.gW = bvVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!bv.gU.isEmpty()) {
            this.gW.gP.onTouchEvent(motionEvent);
        }
        if (!bv.gV.isEmpty()) {
            Iterator it = bv.gV.iterator();
            while (it.hasNext()) {
                ((bz) it.next()).a(motionEvent);
            }
        }
        int action = motionEvent.getAction();
        int i = action & 255;
        int i2 = action >> 8;
        if (i == 6) {
            return bv.g(1, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
        }
        if (i == 5) {
            return bv.g(0, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
        }
        int pointerCount = motionEvent.getPointerCount();
        boolean z = false;
        for (int i3 = 0; i3 < pointerCount; i3++) {
            if (bv.g(action, (int) motionEvent.getX(i3), (int) motionEvent.getY(i3), i3)) {
                z = true;
            }
        }
        return z;
    }
}
