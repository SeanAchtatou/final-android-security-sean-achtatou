package com.google;

import android.webkit.WebView;
import com.google.ads.util.a;
import java.util.HashMap;

public final class t implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("applicationTimeout");
        if (str != null) {
            try {
                dVar.a((long) (Float.parseFloat(str) * 1000.0f));
            } catch (NumberFormatException e) {
                a.c("Trying to set applicationTimeout to invalid value: " + str, e);
            }
        }
    }
}
