package com.google;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

public final class e {
    private String bs;
    private HashMap cD;

    public e(Bundle bundle) {
        this.bs = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.cD = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    public e(String str) {
        this.bs = str;
    }

    public e(String str, HashMap hashMap) {
        this(str);
        this.cD = hashMap;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.bs);
        bundle.putSerializable("params", this.cD);
        return bundle;
    }

    public final String b() {
        return this.bs;
    }

    public final HashMap c() {
        return this.cD;
    }
}
