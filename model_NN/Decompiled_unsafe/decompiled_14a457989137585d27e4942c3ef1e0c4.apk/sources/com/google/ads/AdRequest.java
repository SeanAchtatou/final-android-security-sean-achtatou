package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdRequest {
    public static final String LOGTAG = "Ads";
    public static final String TEST_EMULATOR = AdUtil.a("emulator");
    public static final String VERSION = "4.1.0";
    private boolean E = false;
    private Gender bh = null;
    private String bi = null;
    private Set bj = null;
    private Map bk = null;
    private Location bl = null;
    private boolean bm = false;
    private Set bn = null;

    public enum ErrorCode {
        INVALID_REQUEST("Invalid Google Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private String bs;

        private ErrorCode(String str) {
            this.bs = str;
        }

        public final String toString() {
            return this.bs;
        }
    }

    public enum Gender {
        MALE(AdActivity.TYPE_PARAM),
        FEMALE("f");
        
        private String bs;

        private Gender(String str) {
            this.bs = str;
        }

        public final String toString() {
            return this.bs;
        }
    }

    public void addKeyword(String str) {
        if (this.bj == null) {
            this.bj = new HashSet();
        }
        this.bj.add(str);
    }

    public Map getRequestMap(Context context) {
        HashMap hashMap = new HashMap();
        if (this.bj != null) {
            hashMap.put("kw", this.bj);
        }
        if (this.bh != null) {
            hashMap.put("cust_gender", this.bh.toString());
        }
        if (this.bi != null) {
            hashMap.put("cust_age", this.bi);
        }
        if (this.bl != null) {
            hashMap.put("uule", AdUtil.a(this.bl));
        }
        if (this.bm) {
            hashMap.put("testing", 1);
        }
        if (isTestDevice(context)) {
            hashMap.put("adtest", "on");
        } else if (!this.E) {
            a.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.a() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.E = true;
        }
        if (this.bk != null) {
            hashMap.put("extras", this.bk);
        }
        return hashMap;
    }

    public boolean isTestDevice(Context context) {
        if (this.bn != null) {
            String a = AdUtil.a(context);
            if (a == null) {
                return false;
            }
            if (this.bn.contains(a)) {
                return true;
            }
        }
        return false;
    }

    public void setTesting(boolean z) {
        this.bm = z;
    }
}
