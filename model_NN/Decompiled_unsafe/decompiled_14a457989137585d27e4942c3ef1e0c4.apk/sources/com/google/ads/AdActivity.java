package com.google.ads;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.google.a;
import com.google.d;
import com.google.g;

public class AdActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    public static final String BASE_URL_PARAM = "baseurl";
    public static final String HTML_PARAM = "html";
    public static final String INTENT_ACTION_PARAM = "i";
    public static final String ORIENTATION_PARAM = "o";
    public static final String TYPE_PARAM = "m";
    public static final String URL_PARAM = "u";
    private static final Object aW = new Object();
    private static AdActivity aX = null;
    private static d aY = null;
    private static AdActivity aZ = null;
    private static AdActivity ba = null;
    private g bb;
    private long bc;
    private RelativeLayout bd;
    private AdActivity be = null;
    private boolean bf;
    private VideoView bg;

    private void a(g gVar, boolean z, int i) {
        requestWindowFeature(1);
        getWindow().setFlags(as.GAME_B_PRESSED, as.GAME_B_PRESSED);
        if (gVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (gVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i);
            gVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            int applyDimension = (int) TypedValue.applyDimension(1, 1.0f, getResources().getDisplayMetrics());
            imageButton.setPadding(applyDimension, applyDimension, 0, 0);
            imageButton.setOnClickListener(this);
            this.bd.addView(gVar, new ViewGroup.LayoutParams(-1, -1));
            this.bd.addView(imageButton);
            setContentView(this.bd);
            if (z) {
                a.a(gVar);
            }
        }
    }

    private void a(String str) {
        com.google.ads.util.a.b(str);
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.google.ads.util.a.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        com.google.ads.util.a.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        com.google.ads.util.a.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void launchAdActivity(com.google.d r4, com.google.e r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.aW
            monitor-enter(r0)
            com.google.d r1 = com.google.ads.AdActivity.aY     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.aY = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.e()
            if (r0 != 0) goto L_0x0024
            java.lang.String r0 = "activity was null while launching an AdActivity."
            com.google.ads.util.a.e(r0)
        L_0x0015:
            return
        L_0x0016:
            com.google.d r1 = com.google.ads.AdActivity.aY     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            com.google.ads.util.a.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0024:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            com.google.ads.util.a.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            goto L_0x0015
        L_0x0041:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.a.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.launchAdActivity(com.google.d, com.google.e):void");
    }

    public g getOpeningAdWebView() {
        if (this.be != null) {
            return this.be.bb;
        }
        synchronized (aW) {
            if (aY == null) {
                com.google.ads.util.a.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            g i = aY.i();
            if (i != this.bb) {
                return i;
            }
            return null;
        }
    }

    public VideoView getVideoView() {
        return this.bg;
    }

    public void onClick(View view) {
        finish();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        com.google.ads.util.a.d("Video finished playing.");
        if (this.bg != null) {
            this.bg.setVisibility(8);
        }
        this.bb.loadUrl("javascript:AFMA_ReceiveMessage('onVideoEvent', {'event': 'finish'});");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r11.be != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (com.google.ads.AdActivity.ba == null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001e, code lost:
        r11.be = com.google.ads.AdActivity.ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0022, code lost:
        com.google.ads.AdActivity.ba = r11;
        r11.bd = null;
        r11.bf = false;
        r11.bg = null;
        r1 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        if (r1 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        r2 = new com.google.e(r1);
        r1 = r2.b();
        r4 = r2.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0055, code lost:
        if (r11 != com.google.ads.AdActivity.aZ) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0057, code lost:
        r8.s();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0060, code lost:
        if (r1.equals("intent") == false) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0062, code lost:
        r11.bb = null;
        r11.bc = android.os.SystemClock.elapsedRealtime();
        r11.bf = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006c, code lost:
        if (r4 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006e, code lost:
        a("Could not get the paramMap in launchIntent()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0074, code lost:
        r1 = (java.lang.String) r4.get(com.google.ads.AdActivity.URL_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007c, code lost:
        if (r1 != null) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007e, code lost:
        a("Could not get the URL parameter in launchIntent().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        r2 = (java.lang.String) r4.get(com.google.ads.AdActivity.INTENT_ACTION_PARAM);
        r3 = (java.lang.String) r4.get(com.google.ads.AdActivity.TYPE_PARAM);
        r1 = android.net.Uri.parse(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0098, code lost:
        if (r2 != null) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009a, code lost:
        r1 = new android.content.Intent("android.intent.action.VIEW", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a2, code lost:
        r2 = com.google.ads.AdActivity.aW;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a4, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a7, code lost:
        if (com.google.ads.AdActivity.aX != null) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a9, code lost:
        com.google.ads.AdActivity.aX = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ad, code lost:
        if (com.google.ads.AdActivity.aY == null) goto L_0x00dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00af, code lost:
        com.google.ads.AdActivity.aY.t();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b4, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        com.google.ads.util.a.a("Launching an intent from AdActivity.");
        startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00bf, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c0, code lost:
        com.google.ads.util.a.a(r1.getMessage(), r1);
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00cc, code lost:
        r4 = new android.content.Intent(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d1, code lost:
        if (r3 == null) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d3, code lost:
        r4.setDataAndType(r1, r3);
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d8, code lost:
        r4.setData(r1);
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        com.google.ads.util.a.e("currentAdManager is null while trying to call onLeaveApplication().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e6, code lost:
        r11.bd = new android.widget.RelativeLayout(getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f7, code lost:
        if (r1.equals("webapp") == false) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00f9, code lost:
        r11.bb = new com.google.g(getApplicationContext(), null);
        r1 = new com.google.h(r8, com.google.a.aV, true, true);
        r1.b();
        r11.bb.setWebViewClient(r1);
        r1 = (java.lang.String) r4.get(com.google.ads.AdActivity.URL_PARAM);
        r2 = (java.lang.String) r4.get(com.google.ads.AdActivity.BASE_URL_PARAM);
        r3 = (java.lang.String) r4.get(com.google.ads.AdActivity.HTML_PARAM);
        r7 = (java.lang.String) r4.get(com.google.ads.AdActivity.ORIENTATION_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0135, code lost:
        if (r1 == null) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0137, code lost:
        r11.bb.loadUrl(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0142, code lost:
        if ("p".equals(r7) == false) goto L_0x015f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0144, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0145, code lost:
        a(r11.bb, false, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014c, code lost:
        if (r3 == null) goto L_0x0158;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x014e, code lost:
        r11.bb.loadDataWithBaseURL(r2, r3, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0158, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0165, code lost:
        if ("l".equals(r7) == false) goto L_0x0169;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0167, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0169, code lost:
        r1 = r8.m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0174, code lost:
        if (r1.equals("interstitial") == false) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0176, code lost:
        r11.bb = r8.i();
        a(r11.bb, true, r8.m());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0187, code lost:
        a("Unknown AdOpener, <action: " + r1 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0012, code lost:
        if (com.google.ads.AdActivity.aZ != null) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        com.google.ads.AdActivity.aZ = r11;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r12) {
        /*
            r11 = this;
            r10 = 0
            r6 = 0
            r9 = 1
            super.onCreate(r12)
            java.lang.Object r1 = com.google.ads.AdActivity.aW
            monitor-enter(r1)
            com.google.d r2 = com.google.ads.AdActivity.aY     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x003c
            com.google.d r8 = com.google.ads.AdActivity.aY     // Catch:{ all -> 0x0043 }
            monitor-exit(r1)
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.aZ
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.aZ = r11
        L_0x0016:
            com.google.ads.AdActivity r1 = r11.be
            if (r1 != 0) goto L_0x0022
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.ba
            if (r1 == 0) goto L_0x0022
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.ba
            r11.be = r1
        L_0x0022:
            com.google.ads.AdActivity.ba = r11
            r11.bd = r6
            r11.bf = r10
            r11.bg = r6
            android.content.Intent r1 = r11.getIntent()
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r1 = r1.getBundleExtra(r2)
            if (r1 != 0) goto L_0x0046
            java.lang.String r1 = "Could not get the Bundle used to create AdActivity."
            r11.a(r1)
        L_0x003b:
            return
        L_0x003c:
            java.lang.String r2 = "Could not get currentAdManager."
            r11.a(r2)     // Catch:{ all -> 0x0043 }
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            goto L_0x003b
        L_0x0043:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x0046:
            com.google.e r2 = new com.google.e
            r2.<init>(r1)
            java.lang.String r1 = r2.b()
            java.util.HashMap r4 = r2.c()
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.aZ
            if (r11 != r2) goto L_0x005a
            r8.s()
        L_0x005a:
            java.lang.String r2 = "intent"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x00e6
            r11.bb = r6
            long r1 = android.os.SystemClock.elapsedRealtime()
            r11.bc = r1
            r11.bf = r9
            if (r4 != 0) goto L_0x0074
            java.lang.String r1 = "Could not get the paramMap in launchIntent()"
            r11.a(r1)
            goto L_0x003b
        L_0x0074:
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0084
            java.lang.String r1 = "Could not get the URL parameter in launchIntent()."
            r11.a(r1)
            goto L_0x003b
        L_0x0084:
            java.lang.String r2 = "i"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "m"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            android.net.Uri r1 = android.net.Uri.parse(r1)
            if (r2 != 0) goto L_0x00cc
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3, r1)
            r1 = r2
        L_0x00a2:
            java.lang.Object r2 = com.google.ads.AdActivity.aW
            monitor-enter(r2)
            com.google.ads.AdActivity r3 = com.google.ads.AdActivity.aX     // Catch:{ all -> 0x00e3 }
            if (r3 != 0) goto L_0x00b4
            com.google.ads.AdActivity.aX = r11     // Catch:{ all -> 0x00e3 }
            com.google.d r3 = com.google.ads.AdActivity.aY     // Catch:{ all -> 0x00e3 }
            if (r3 == 0) goto L_0x00dd
            com.google.d r3 = com.google.ads.AdActivity.aY     // Catch:{ all -> 0x00e3 }
            r3.t()     // Catch:{ all -> 0x00e3 }
        L_0x00b4:
            monitor-exit(r2)     // Catch:{ all -> 0x00e3 }
            java.lang.String r2 = "Launching an intent from AdActivity."
            com.google.ads.util.a.a(r2)     // Catch:{ ActivityNotFoundException -> 0x00bf }
            r11.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x00bf }
            goto L_0x003b
        L_0x00bf:
            r1 = move-exception
            java.lang.String r2 = r1.getMessage()
            com.google.ads.util.a.a(r2, r1)
            r11.finish()
            goto L_0x003b
        L_0x00cc:
            android.content.Intent r4 = new android.content.Intent
            r4.<init>(r2)
            if (r3 == 0) goto L_0x00d8
            r4.setDataAndType(r1, r3)
            r1 = r4
            goto L_0x00a2
        L_0x00d8:
            r4.setData(r1)
            r1 = r4
            goto L_0x00a2
        L_0x00dd:
            java.lang.String r3 = "currentAdManager is null while trying to call onLeaveApplication()."
            com.google.ads.util.a.e(r3)     // Catch:{ all -> 0x00e3 }
            goto L_0x00b4
        L_0x00e3:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x00e6:
            android.widget.RelativeLayout r2 = new android.widget.RelativeLayout
            android.content.Context r3 = r11.getApplicationContext()
            r2.<init>(r3)
            r11.bd = r2
            java.lang.String r2 = "webapp"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x016e
            com.google.g r1 = new com.google.g
            android.content.Context r2 = r11.getApplicationContext()
            r1.<init>(r2, r6)
            r11.bb = r1
            com.google.h r1 = new com.google.h
            java.util.Map r2 = com.google.a.aV
            r1.<init>(r8, r2, r9, r9)
            r1.b()
            com.google.g r2 = r11.bb
            r2.setWebViewClient(r1)
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "baseurl"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "html"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r5 = "o"
            java.lang.Object r4 = r4.get(r5)
            r0 = r4
            java.lang.String r0 = (java.lang.String) r0
            r7 = r0
            if (r1 == 0) goto L_0x014c
            com.google.g r2 = r11.bb
            r2.loadUrl(r1)
        L_0x013c:
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x015f
            r1 = r9
        L_0x0145:
            com.google.g r2 = r11.bb
            r11.a(r2, r10, r1)
            goto L_0x003b
        L_0x014c:
            if (r3 == 0) goto L_0x0158
            com.google.g r1 = r11.bb
            java.lang.String r4 = "text/html"
            java.lang.String r5 = "utf-8"
            r1.loadDataWithBaseURL(r2, r3, r4, r5, r6)
            goto L_0x013c
        L_0x0158:
            java.lang.String r1 = "Could not get the URL or HTML parameter to show a web app."
            r11.a(r1)
            goto L_0x003b
        L_0x015f:
            java.lang.String r1 = "l"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x0169
            r1 = r10
            goto L_0x0145
        L_0x0169:
            int r1 = r8.m()
            goto L_0x0145
        L_0x016e:
            java.lang.String r2 = "interstitial"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0187
            com.google.g r1 = r8.i()
            r11.bb = r1
            int r1 = r8.m()
            com.google.g r2 = r11.bb
            r11.a(r2, r9, r1)
            goto L_0x003b
        L_0x0187:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ">"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r11.a(r1)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        if (this.bd != null) {
            this.bd.removeAllViews();
        }
        if (this.bb != null) {
            a.b(this.bb);
            this.bb.a(null);
        }
        if (isFinishing()) {
            if (this.bg != null) {
                this.bg.stopPlayback();
                this.bg = null;
            }
            synchronized (aW) {
                if (!(aY == null || this.bb == null)) {
                    if (this.bb == aY.i()) {
                        aY.a();
                    }
                    this.bb.stopLoading();
                    this.bb.destroy();
                }
                if (this == aZ) {
                    if (aY != null) {
                        aY.r();
                        aY = null;
                    } else {
                        com.google.ads.util.a.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                    aZ = null;
                }
            }
            if (this == aX) {
                aX = null;
            }
            ba = this.be;
        }
        com.google.ads.util.a.a("AdActivity is closing.");
        super.onDestroy();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        com.google.ads.util.a.e("Video threw error! <what:" + i + ", extra:" + i2 + ">");
        finish();
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        com.google.ads.util.a.d("Video is ready to play.");
        this.bb.loadUrl("javascript:AFMA_ReceiveMessage('onVideoEvent', {'event': 'load'});");
    }

    public void onWindowFocusChanged(boolean z) {
        if (this.bf && z && SystemClock.elapsedRealtime() - this.bc > 250) {
            com.google.ads.util.a.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(z);
    }

    public void showVideo(VideoView videoView) {
        this.bg = videoView;
        if (this.bb == null) {
            a("Couldn't get adWebView to show the video.");
            return;
        }
        this.bb.setBackgroundColor(0);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setOnErrorListener(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setGravity(17);
        linearLayout.addView(videoView, layoutParams);
        this.bd.addView(linearLayout, 0, layoutParams);
    }
}
