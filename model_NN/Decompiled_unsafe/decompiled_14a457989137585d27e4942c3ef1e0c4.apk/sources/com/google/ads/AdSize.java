package com.google.ads;

import javax.microedition.media.Player;

public class AdSize {
    public static final AdSize bA = new AdSize(728, 90, "728x90_as");
    public static final AdSize bx = new AdSize(320, 50, "320x50_mb");
    public static final AdSize by = new AdSize(Player.PREFETCHED, 250, "300x250_as");
    public static final AdSize bz = new AdSize(468, 60, "468x60_as");
    private int bB;
    private int bC;
    private String bD;

    private AdSize(int i, int i2, String str) {
        this.bB = i;
        this.bC = i2;
        this.bD = str;
    }

    public int getHeight() {
        return this.bC;
    }

    public int getWidth() {
        return this.bB;
    }

    public String toString() {
        return this.bD;
    }
}
