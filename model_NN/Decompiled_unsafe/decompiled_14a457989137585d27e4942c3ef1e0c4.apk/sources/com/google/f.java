package com.google;

import android.os.SystemClock;
import com.google.ads.util.a;
import java.util.LinkedList;

public final class f {
    private static long cI = 0;
    public String bs;
    private LinkedList cE = new LinkedList();
    private long cF;
    private long cG;
    private LinkedList cH = new LinkedList();
    private String cJ;
    private boolean ch = false;
    private boolean ci = false;

    f() {
        a();
    }

    static long E() {
        return cI;
    }

    /* access modifiers changed from: package-private */
    public final long A() {
        if (this.cE.size() != this.cH.size()) {
            return -1;
        }
        return (long) this.cE.size();
    }

    /* access modifiers changed from: package-private */
    public final String B() {
        if (this.cE.isEmpty() || this.cE.size() != this.cH.size()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.cE.size()) {
                return sb.toString();
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.cH.get(i2)).longValue() - ((Long) this.cE.get(i2)).longValue()));
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final String C() {
        if (this.cE.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.cE.size()) {
                return sb.toString();
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.cE.get(i2)).longValue() - this.cF));
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final long D() {
        return this.cF - this.cG;
    }

    /* access modifiers changed from: package-private */
    public final String F() {
        return this.cJ;
    }

    /* access modifiers changed from: package-private */
    public final boolean G() {
        return this.ch;
    }

    /* access modifiers changed from: package-private */
    public final void H() {
        a.d("Interstitial network error.");
        this.ch = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean I() {
        return this.ci;
    }

    /* access modifiers changed from: package-private */
    public final void J() {
        a.d("Interstitial no fill.");
        this.ci = true;
    }

    /* access modifiers changed from: package-private */
    public final String K() {
        return this.bs;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.cE.clear();
        this.cF = 0;
        this.cG = 0;
        this.cH.clear();
        this.cJ = null;
        this.ch = false;
        this.ci = false;
    }

    public final void a(String str) {
        a.d("Prior ad identifier = " + str);
        this.cJ = str;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        a.d("Ad clicked.");
        this.cE.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    public final void b(String str) {
        a.d("Prior impression ticket = " + str);
        this.bs = str;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        a.d("Ad request loaded.");
        this.cF = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        a.d("Ad request started.");
        this.cG = SystemClock.elapsedRealtime();
        cI++;
    }

    public final void o() {
        a.d("Landing page dismissed.");
        this.cH.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }
}
