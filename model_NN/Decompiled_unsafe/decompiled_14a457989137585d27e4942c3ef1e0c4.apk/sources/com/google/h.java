package com.google;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.Map;

public final class h extends WebViewClient {
    private Map aV;
    private d bE;
    private boolean bW;
    private boolean bX = false;
    private boolean bm = false;
    private boolean cM;

    public h(d dVar, Map map, boolean z, boolean z2) {
        this.bE = dVar;
        this.aV = map;
        this.cM = z;
        this.bW = z2;
    }

    public final void a() {
        this.bX = true;
    }

    public final void b() {
        this.bm = true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.bX) {
            c g = this.bE.g();
            if (g != null) {
                g.a();
            } else {
                a.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.bX = false;
        }
        if (this.bm) {
            a.a(webView);
            this.bm = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a.a("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        if (a.a(parse)) {
            a.a(this.bE, this.aV, parse, webView);
            return true;
        } else if (this.bW) {
            if (AdUtil.a(parse)) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            HashMap hashMap = new HashMap();
            hashMap.put(AdActivity.URL_PARAM, str);
            AdActivity.launchAdActivity(this.bE, new e("intent", hashMap));
            return true;
        } else if (this.cM) {
            HashMap b = AdUtil.b(parse);
            if (b == null) {
                a.e("An error occurred while parsing the url parameters.");
                return true;
            }
            this.bE.l().a((String) b.get("ai"));
            String str2 = (!this.bE.v() || !AdUtil.a(parse)) ? "intent" : "webapp";
            HashMap hashMap2 = new HashMap();
            hashMap2.put(AdActivity.URL_PARAM, parse.toString());
            AdActivity.launchAdActivity(this.bE, new e(str2, hashMap2));
            return true;
        } else {
            a.e("URL is not a GMSG and can't handle URL: " + str);
            return true;
        }
    }
}
