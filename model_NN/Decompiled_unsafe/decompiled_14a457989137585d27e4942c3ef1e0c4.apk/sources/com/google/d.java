package com.google;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

public final class d {
    private static final Object aW = new Object();
    private LinkedList cA;
    private LinkedList cB;
    private int cC = 4;
    private WeakReference cj;
    private Ad ck;
    private AdListener cl;
    private c cm;
    private AdRequest cn;
    private AdSize co;
    private f cp;
    private String cq;
    private g cr;
    private h cs;
    private Handler ct;
    private long cu;
    private boolean cv;
    private boolean cw;
    private SharedPreferences cx;
    private long cy;
    private x cz;

    public d(Activity activity, Ad ad, AdSize adSize, String str) {
        this.cj = new WeakReference(activity);
        this.ck = ad;
        this.co = adSize;
        this.cq = str;
        this.cp = new f();
        this.cl = null;
        this.cm = null;
        this.cn = null;
        this.cv = false;
        this.ct = new Handler();
        this.cy = 0;
        this.cw = false;
        synchronized (aW) {
            this.cx = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            long j = this.cx.getLong("InterstitialTimeout" + str, -1);
            if (j < 0) {
                this.cu = 5000;
            } else {
                this.cu = j;
            }
        }
        this.cz = new x(this);
        this.cA = new LinkedList();
        this.cB = new LinkedList();
        a();
        AdUtil.h(activity.getApplicationContext());
    }

    private synchronized boolean q() {
        return this.cm != null;
    }

    public final synchronized void a() {
        Activity e = e();
        if (e == null) {
            a.a("activity was null while trying to create an AdWebView.");
        } else {
            this.cr = new g(e.getApplicationContext(), this.co);
            this.cr.setVisibility(8);
            if (this.ck instanceof AdView) {
                this.cs = new h(this, a.aV, true, false);
            } else {
                this.cs = new h(this, a.aV, true, true);
            }
            this.cr.setWebViewClient(this.cs);
        }
    }

    public final synchronized void a(float f) {
        this.cy = (long) (1000.0f * f);
    }

    public final synchronized void a(int i) {
        this.cC = i;
    }

    public final void a(long j) {
        synchronized (aW) {
            SharedPreferences.Editor edit = this.cx.edit();
            edit.putLong("InterstitialTimeout" + this.cq, j);
            edit.commit();
            this.cu = j;
        }
    }

    public final synchronized void a(AdListener adListener) {
        this.cl = adListener;
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.cm = null;
        if (this.ck instanceof InterstitialAd) {
            if (errorCode == AdRequest.ErrorCode.NO_FILL) {
                this.cp.J();
            } else if (errorCode == AdRequest.ErrorCode.NETWORK_ERROR) {
                this.cp.H();
            }
        }
        a.c("onFailedToReceiveAd(" + errorCode + ")");
        if (this.cl != null) {
            this.cl.onFailedToReceiveAd(this.ck, errorCode);
        }
    }

    public final synchronized void a(AdRequest adRequest) {
        if (q()) {
            a.e("loadAd called while the ad is already loading.");
        } else {
            Activity e = e();
            if (e == null) {
                a.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e.getApplicationContext()) && AdUtil.b(e.getApplicationContext())) {
                this.cv = false;
                this.cn = adRequest;
                this.cm = new c(this);
                this.cm.execute(adRequest);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.cA.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(String str) {
        this.cB.add(str);
    }

    public final synchronized void c() {
        if (this.cw) {
            a.a("Disabling refreshing.");
            this.ct.removeCallbacks(this.cz);
            this.cw = false;
        } else {
            a.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.ck instanceof AdView)) {
            a.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.cw) {
            a.a("Enabling refreshing every " + this.cy + " milliseconds.");
            this.ct.postDelayed(this.cz, this.cy);
            this.cw = true;
        } else {
            a.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return (Activity) this.cj.get();
    }

    /* access modifiers changed from: package-private */
    public final Ad f() {
        return this.ck;
    }

    public final synchronized c g() {
        return this.cm;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.cq;
    }

    public final synchronized g i() {
        return this.cr;
    }

    /* access modifiers changed from: package-private */
    public final synchronized h j() {
        return this.cs;
    }

    public final AdSize k() {
        return this.co;
    }

    public final f l() {
        return this.cp;
    }

    public final synchronized int m() {
        return this.cC;
    }

    public final long n() {
        long j;
        if (!(this.ck instanceof InterstitialAd)) {
            return 60000;
        }
        synchronized (aW) {
            j = this.cu;
        }
        return j;
    }

    public final synchronized boolean o() {
        return this.cv;
    }

    public final synchronized boolean p() {
        return this.cw;
    }

    public final synchronized void r() {
        this.cp.o();
        a.c("onDismissScreen()");
        if (this.cl != null) {
            this.cl.onDismissScreen(this.ck);
        }
    }

    public final synchronized void s() {
        this.cp.b();
        a.c("onPresentScreen()");
        if (this.cl != null) {
            this.cl.onPresentScreen(this.ck);
        }
    }

    public final synchronized void t() {
        a.c("onLeaveApplication()");
        if (this.cl != null) {
            this.cl.onLeaveApplication(this.ck);
        }
    }

    public final synchronized void u() {
        Activity activity = (Activity) this.cj.get();
        if (activity == null) {
            a.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.cA.iterator();
            while (it.hasNext()) {
                new Thread(new w((String) it.next(), activity.getApplicationContext())).start();
            }
            this.cA.clear();
        }
    }

    public final synchronized boolean v() {
        boolean z;
        boolean z2 = !this.cB.isEmpty();
        Activity activity = (Activity) this.cj.get();
        if (activity == null) {
            a.e("activity was null while trying to ping click tracking URLs.");
            z = z2;
        } else {
            Iterator it = this.cB.iterator();
            while (it.hasNext()) {
                new Thread(new w((String) it.next(), activity.getApplicationContext())).start();
            }
            this.cB.clear();
            z = z2;
        }
        return z;
    }

    public final synchronized void w() {
        if (this.cn == null) {
            a.a("Tried to refresh before calling loadAd().");
        } else if (this.ck instanceof AdView) {
            if (!((AdView) this.ck).isShown() || !AdUtil.b()) {
                a.a("Not refreshing because the ad is not visible.");
            } else {
                a.c("Refreshing ad.");
                a(this.cn);
            }
            this.ct.postDelayed(this.cz, this.cy);
        } else {
            a.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void y() {
        if (this.cm != null) {
            this.cm.cancel(false);
            this.cm = null;
        }
        this.cr.stopLoading();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void z() {
        this.cm = null;
        this.cv = true;
        this.cr.setVisibility(0);
        this.cp.c();
        if (this.ck instanceof AdView) {
            u();
        }
        a.c("onReceiveAd()");
        if (this.cl != null) {
            this.cl.onReceiveAd(this.ck);
        }
    }
}
