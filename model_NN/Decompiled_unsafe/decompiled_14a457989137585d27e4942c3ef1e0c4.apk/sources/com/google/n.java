package com.google;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

public final class n implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get(AdActivity.URL_PARAM);
        if (str == null) {
            a.e("Could not get URL from click gmsg.");
        } else {
            new Thread(new w(str, webView.getContext().getApplicationContext())).start();
        }
    }
}
