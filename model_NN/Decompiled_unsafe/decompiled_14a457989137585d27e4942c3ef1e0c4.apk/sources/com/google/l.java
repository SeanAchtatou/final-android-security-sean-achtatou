package com.google;

import android.webkit.WebView;
import com.google.ads.util.a;
import java.util.HashMap;

public final class l implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        if (webView instanceof g) {
            ((g) webView).a();
        } else {
            a.b("Trying to close WebView that isn't an AdWebView");
        }
    }
}
