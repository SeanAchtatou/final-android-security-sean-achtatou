package com.google;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

public final class m implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("js");
        if (str == null) {
            a.b("Could not get the JS to evaluate.");
        }
        if (webView instanceof g) {
            AdActivity b = ((g) webView).b();
            if (b == null) {
                a.b("Could not get the AdActivity from the AdWebView.");
                return;
            }
            g openingAdWebView = b.getOpeningAdWebView();
            if (openingAdWebView == null) {
                a.b("Could not get the opening WebView.");
            } else {
                a.a(openingAdWebView, str);
            }
        } else {
            a.b("Trying to evaluate JS in a WebView that isn't an AdWebView");
        }
    }
}
