package com.google;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.AdSize;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;

public final class g extends WebView {
    private AdActivity cK;
    private AdSize cL;

    public g(Context context, AdSize adSize) {
        super(context);
        this.cL = adSize;
        setBackgroundColor(0);
        AdUtil.a(this);
        getSettings().setJavaScriptEnabled(true);
        setScrollBarStyle(0);
    }

    public final void a() {
        if (this.cK != null) {
            this.cK.finish();
        }
    }

    public final void a(AdActivity adActivity) {
        this.cK = adActivity;
    }

    public final AdActivity b() {
        return this.cK;
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        if (isInEditMode()) {
            super.onMeasure(i, i2);
        } else if (this.cL == null) {
            super.onMeasure(i, i2);
        } else {
            int mode = View.MeasureSpec.getMode(i);
            int size = View.MeasureSpec.getSize(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size2 = View.MeasureSpec.getSize(i2);
            float f = getContext().getResources().getDisplayMetrics().density;
            int width = (int) (((float) this.cL.getWidth()) * f);
            int height = (int) (((float) this.cL.getHeight()) * f);
            if (mode == 0 || mode2 == 0) {
                super.onMeasure(i, i2);
            } else if (((float) width) - (6.0f * f) > ((float) size) || height > size2) {
                a.e("Not enough space to show ad! Wants: <" + width + ", " + height + ">, Has: <" + size + ", " + size2 + ">");
                setVisibility(8);
                setMeasuredDimension(0, 0);
            } else {
                super.onMeasure(i, i2);
            }
        }
    }
}
