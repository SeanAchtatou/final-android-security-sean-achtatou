package com.google;

import android.content.Context;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public final class w implements Runnable {
    private String bi;
    private Context cN;

    public w(String str, Context context) {
        this.bi = str;
        this.cN = context;
    }

    public final void run() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.bi).openConnection();
            AdUtil.a(httpURLConnection, this.cN);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() != 200) {
                a.e("Did not receive HTTP_OK from URL: " + this.bi);
            }
        } catch (IOException e) {
            a.c("Unable to ping the URL: " + this.bi, e);
        }
    }
}
