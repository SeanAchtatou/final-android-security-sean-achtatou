package com.network.app.tools;

import android.content.Context;

public class ResourceLoader {
    public static Context dh = null;

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036 A[SYNTHETIC, Splitter:B:16:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b A[Catch:{ Exception -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0045 A[SYNTHETIC, Splitter:B:24:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004a A[Catch:{ Exception -> 0x004e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] j(java.lang.String r6) {
        /*
            r4 = 0
            java.lang.String r0 = r6.toLowerCase()
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Exception -> 0x005c, all -> 0x0040 }
            android.content.Context r2 = com.network.app.tools.ResourceLoader.dh     // Catch:{ Exception -> 0x005c, all -> 0x0040 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x005c, all -> 0x0040 }
            java.io.InputStream r0 = r2.open(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0040 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0040 }
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0060, all -> 0x0050 }
            r0.<init>()     // Catch:{ Exception -> 0x0060, all -> 0x0050 }
        L_0x0019:
            int r2 = r1.read()     // Catch:{ Exception -> 0x0033, all -> 0x0054 }
            r3 = -1
            if (r2 != r3) goto L_0x002f
            byte[] r2 = r0.toByteArray()     // Catch:{ Exception -> 0x0033, all -> 0x0054 }
            r0.close()     // Catch:{ Exception -> 0x0033, all -> 0x0054 }
            r1.close()     // Catch:{ Exception -> 0x0063 }
            r0.close()     // Catch:{ Exception -> 0x0063 }
        L_0x002d:
            r0 = r2
        L_0x002e:
            return r0
        L_0x002f:
            r0.write(r2)     // Catch:{ Exception -> 0x0033, all -> 0x0054 }
            goto L_0x0019
        L_0x0033:
            r2 = move-exception
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ Exception -> 0x005a }
        L_0x0039:
            if (r0 == 0) goto L_0x003e
            r0.close()     // Catch:{ Exception -> 0x005a }
        L_0x003e:
            r0 = r4
            goto L_0x002e
        L_0x0040:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x0043:
            if (r2 == 0) goto L_0x0048
            r2.close()     // Catch:{ Exception -> 0x004e }
        L_0x0048:
            if (r1 == 0) goto L_0x004d
            r1.close()     // Catch:{ Exception -> 0x004e }
        L_0x004d:
            throw r0
        L_0x004e:
            r1 = move-exception
            goto L_0x004d
        L_0x0050:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0043
        L_0x0054:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0043
        L_0x005a:
            r0 = move-exception
            goto L_0x003e
        L_0x005c:
            r0 = move-exception
            r0 = r4
            r1 = r4
            goto L_0x0034
        L_0x0060:
            r0 = move-exception
            r0 = r4
            goto L_0x0034
        L_0x0063:
            r0 = move-exception
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.network.app.tools.ResourceLoader.j(java.lang.String):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038 A[SYNTHETIC, Splitter:B:15:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d A[Catch:{ IOException -> 0x0042 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004b A[SYNTHETIC, Splitter:B:25:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0050 A[Catch:{ IOException -> 0x0054 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.DataInputStream k(java.lang.String r8) {
        /*
            r6 = 0
            r0 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x001d, all -> 0x0047 }
            byte[] r2 = j(r8)     // Catch:{ Exception -> 0x001d, all -> 0x0047 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x001d, all -> 0x0047 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0060, all -> 0x0059 }
            r1.close()     // Catch:{ IOException -> 0x0018 }
            r2.close()     // Catch:{ IOException -> 0x0018 }
        L_0x0016:
            r0 = r2
        L_0x0017:
            return r0
        L_0x0018:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0016
        L_0x001d:
            r1 = move-exception
            r2 = r6
        L_0x001f:
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ all -> 0x005e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x005e }
            java.lang.String r5 = "!\tERROR\tload stream error:\t"
            r4.<init>(r5)     // Catch:{ all -> 0x005e }
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ all -> 0x005e }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x005e }
            r3.println(r4)     // Catch:{ all -> 0x005e }
            r1.printStackTrace()     // Catch:{ all -> 0x005e }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0042 }
        L_0x003b:
            if (r6 == 0) goto L_0x0040
            r0.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0040:
            r0 = r6
            goto L_0x0017
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0040
        L_0x0047:
            r1 = move-exception
            r2 = r6
        L_0x0049:
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ IOException -> 0x0054 }
        L_0x004e:
            if (r6 == 0) goto L_0x0053
            r0.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r1
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x0059:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0049
        L_0x005e:
            r1 = move-exception
            goto L_0x0049
        L_0x0060:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.network.app.tools.ResourceLoader.k(java.lang.String):java.io.DataInputStream");
    }

    public static void setContext(Context context) {
        dh = context;
    }
}
