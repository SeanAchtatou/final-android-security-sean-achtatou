package com.network.app.tools;

import java.util.Vector;

public class StringTools {
    public static String[] a(String str, char c) {
        int i = 0;
        if (str == null) {
            return null;
        }
        Vector vector = new Vector();
        int indexOf = str.indexOf(c);
        if (indexOf < 0) {
            return new String[]{str};
        }
        while (indexOf >= 0) {
            String trim = str.substring(i, indexOf).trim();
            if (trim.length() > 0) {
                vector.addElement(trim);
            }
            int i2 = indexOf + 1;
            i = i2;
            indexOf = str.indexOf(c, i2);
        }
        if (i <= str.length() - 1) {
            String trim2 = str.substring(i).trim();
            if (trim2.length() > 0) {
                vector.addElement(trim2);
            }
        }
        String[] strArr = vector.size() > 0 ? new String[vector.size()] : null;
        vector.copyInto(strArr);
        return strArr;
    }
}
