package com.network.app.data;

import android.content.Context;
import com.network.app.tools.StringTools;
import com.network.app.util.Pay;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class Database {
    public static final String DB_NAME = "data.db";
    public static final String ENCODING = "UTF-8";
    public static final String FAIL_DATA = "<data><result>fail</result></data>";
    public static final int INT_CHANNEL = 3;
    public static final int INT_COMNET = 1;
    public static final int INT_CONFIRMWORD = 23;
    public static final int INT_FAILWORD = 21;
    public static final int INT_FEECUE = 15;
    public static final int INT_FEES = 17;
    public static final int INT_FEETYPE = 2;
    public static final int INT_FEEVALUE = 10;
    public static final int INT_GAMEID = 5;
    public static final int INT_GAME_FEE_TYPE = 0;
    public static final int INT_IMEI = 4;
    public static final int INT_ISFEE = 14;
    public static final int INT_KEYACTION = 16;
    public static final int INT_KEYWORD = 7;
    public static final int INT_MTWORD = 19;
    public static final int INT_READFEE = 13;
    public static final int INT_SCN = 11;
    public static final int INT_SENDEDNUM = 12;
    public static final int INT_SHORTCODE = 6;
    public static final int INT_SUCWORD = 22;
    public static final int INT_TIMEWAIT = 20;
    public static final int INT_TRIAL = 8;
    public static final int INT_VERSION = 9;
    public static final String STRING_CHANNEL = "channel";
    public static final String STRING_COMNET = "comnet";
    public static final String STRING_CONFIRMWORD = "confirmword";
    public static final String STRING_FAILWORD = "failword";
    public static final String STRING_FEECUE = "feecue";
    public static final String STRING_FEES = "fees";
    public static final String STRING_FEETYPE = "feetype";
    public static final String STRING_FEEVALUE = "feevalue";
    public static final String STRING_GAMEID = "gameid";
    public static final String STRING_GAME_FEE_TYPE = "gamefeetype";
    public static final String STRING_IMEI = "imei";
    public static final String STRING_ISFEE = "isfee";
    public static final String STRING_KEYACTION = "keyaction";
    public static final String STRING_KEYWORD = "keyword";
    public static final String STRING_MTCODE = "mtcode";
    public static final String STRING_MTWORD = "mtword";
    public static final String STRING_READFEE = "readfee";
    public static final String STRING_SCN = "scn";
    public static final String STRING_SENDEDNUM = "sendednum";
    public static final String STRING_SHORTCODE = "shortcode";
    public static final String STRING_SUCWORD = "successword";
    public static final String STRING_TIMEWAIT = "timewait";
    public static final String STRING_TRIAL = "trial";
    public static final String STRING_VERSION = "version";
    public static final String xmlURL = "http://www.mobilehotdog.com/cnxmlrpc/xml.php";
    public final int dd = 18;
    public String[] de = null;
    public int df = 0;
    public String dg = null;
    private Context dh = null;
    public HashMap di = new HashMap();

    public Database(Context context, String str, String str2, String str3) {
        this.dh = context;
        try {
            ad();
            this.de[1] = str2;
            this.de[4] = str;
            this.de[11] = str3;
            if (Pay.dr) {
                System.out.println(" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ****  *" + str3 + "*");
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("<data>");
            stringBuffer.append("<comnet>" + this.de[1] + "</" + STRING_COMNET + ">");
            stringBuffer.append("<feetype>" + this.de[2] + "</" + STRING_FEETYPE + ">");
            stringBuffer.append("<channel>" + this.de[3] + "</" + STRING_CHANNEL + ">");
            stringBuffer.append("<imei>" + this.de[4] + "</" + STRING_IMEI + ">");
            stringBuffer.append("<gameid>" + this.de[5] + "</" + STRING_GAMEID + ">");
            stringBuffer.append("<trial>" + this.de[8] + "</" + STRING_TRIAL + ">");
            stringBuffer.append("<version>" + this.de[9] + "</" + STRING_VERSION + ">");
            stringBuffer.append("<feevalue>" + this.de[10] + "</" + STRING_FEEVALUE + ">");
            stringBuffer.append("<scn>" + this.de[11] + "</" + STRING_SCN + ">");
            stringBuffer.append("</data>");
            String i = i(stringBuffer.toString());
            if (!i.equals(FAIL_DATA)) {
                h(i);
                String str4 = (String) this.di.get(STRING_SHORTCODE);
                if (str4 != null) {
                    this.de[6] = c(this.de[6], str4);
                }
                String str5 = (String) this.di.get(STRING_KEYWORD);
                if (Pay.dr) {
                    System.out.println("STRING_KEYWORD===" + str5);
                }
                if (str5 != null) {
                    if (str5.equals("")) {
                        this.dg = ab();
                    } else {
                        this.dg = str5;
                    }
                    this.de[7] = c(this.de[7], StringTools.a(str5, ',')[0]);
                }
                String str6 = (String) this.di.get(STRING_ISFEE);
                if (str6 != null) {
                    this.de[14] = c(this.de[14], str6);
                }
                String str7 = (String) this.di.get(STRING_FEECUE);
                if (str7 != null) {
                    this.de[15] = c(this.de[15], str7);
                }
                String str8 = (String) this.di.get(STRING_KEYACTION);
                if (str8 != null) {
                    this.de[16] = str8;
                }
                String str9 = (String) this.di.get(STRING_FEES);
                if (Pay.dr) {
                    System.out.println("STRING_FEES===" + str9);
                }
                if (str9 != null) {
                    this.de[17] = c(this.de[17], str9);
                }
                String str10 = (String) this.di.get(STRING_READFEE);
                if (str10 != null) {
                    this.de[13] = str10;
                }
                String str11 = (String) this.di.get(STRING_MTCODE);
                if (str11 != null) {
                    this.de[18] = str11;
                }
                String str12 = (String) this.di.get(STRING_MTWORD);
                if (str12 != null) {
                    this.de[19] = str12;
                }
                String str13 = (String) this.di.get(STRING_TIMEWAIT);
                if (str13 != null) {
                    this.de[20] = c(this.de[20], str13);
                }
                String str14 = (String) this.di.get(STRING_FAILWORD);
                if (str14 != null) {
                    this.de[21] = str14;
                }
                String str15 = (String) this.di.get(STRING_SUCWORD);
                if (str15 != null) {
                    this.de[22] = str15;
                }
                String str16 = (String) this.di.get(STRING_CONFIRMWORD);
                if (str16 != null) {
                    this.de[23] = str16;
                }
            } else {
                this.dg = ab();
            }
            ae();
        } catch (Exception e) {
        }
    }

    private String ab() {
        char c;
        StringBuffer stringBuffer = new StringBuffer();
        String[] a = StringTools.a(this.de[7], '-');
        switch (this.de[1].charAt(0)) {
            case 'm':
                c = 0;
                break;
            case 't':
                c = 2;
                break;
            case 'u':
                c = 1;
                break;
            default:
                c = 0;
                break;
        }
        stringBuffer.append(String.valueOf(a[c]) + ",");
        stringBuffer.append(String.valueOf(this.de[5]) + ",");
        stringBuffer.append(String.valueOf(this.de[4]) + ",");
        stringBuffer.append(String.valueOf(this.de[9]) + ",");
        stringBuffer.append(String.valueOf(this.de[8]) + ",");
        stringBuffer.append(this.de[3]);
        return stringBuffer.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void ac() {
        /*
            r5 = this;
            r4 = 0
            java.lang.String r0 = "data.bin"
            java.io.DataInputStream r0 = com.network.app.tools.ResourceLoader.k(r0)     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            int r1 = r0.readInt()     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            r5.de = r1     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            r1 = r4
        L_0x0010:
            java.lang.String[] r2 = r5.de     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            int r2 = r2.length     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            if (r1 < r2) goto L_0x0023
            boolean r1 = com.network.app.util.Pay.dr     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            if (r1 == 0) goto L_0x001f
            r1 = r4
        L_0x001a:
            java.lang.String[] r2 = r5.de     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            int r2 = r2.length     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            if (r1 < r2) goto L_0x002e
        L_0x001f:
            r0.close()     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
        L_0x0022:
            return
        L_0x0023:
            java.lang.String[] r2 = r5.de     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.String r3 = r0.readUTF()     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            r2[r1] = r3     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            int r1 = r1 + 1
            goto L_0x0010
        L_0x002e:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.String r4 = "dest: "
            r3.<init>(r4)     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.String[] r4 = r5.de     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            r4 = r4[r1]     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            r2.println(r3)     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            int r1 = r1 + 1
            goto L_0x001a
        L_0x0049:
            r0 = move-exception
            throw r0
        L_0x004b:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.network.app.data.Database.ac():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003e A[SYNTHETIC, Splitter:B:22:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0043 A[Catch:{ Exception -> 0x0047 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004d A[SYNTHETIC, Splitter:B:30:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0052 A[Catch:{ Exception -> 0x0058 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void ad() {
        /*
            r6 = this;
            r2 = 0
            android.content.Context r0 = r6.dh     // Catch:{ Exception -> 0x0036, all -> 0x0049 }
            java.lang.String r1 = "data.db"
            java.io.FileInputStream r0 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x0036, all -> 0x0049 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Exception -> 0x006b, all -> 0x005a }
            r1.<init>(r0)     // Catch:{ Exception -> 0x006b, all -> 0x005a }
            int r2 = r1.readInt()     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            r6.de = r2     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            r2 = 0
        L_0x0017:
            java.lang.String[] r3 = r6.de     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            int r3 = r3.length     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            if (r2 < r3) goto L_0x002b
            int r2 = r1.readInt()     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            r6.df = r2     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            r1.close()     // Catch:{ Exception -> 0x0056 }
            if (r0 == 0) goto L_0x002a
            r0.close()     // Catch:{ Exception -> 0x0056 }
        L_0x002a:
            return
        L_0x002b:
            java.lang.String[] r3 = r6.de     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            java.lang.String r4 = r1.readUTF()     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            r3[r2] = r4     // Catch:{ Exception -> 0x006f, all -> 0x0060 }
            int r2 = r2 + 1
            goto L_0x0017
        L_0x0036:
            r0 = move-exception
            r0 = r2
            r1 = r2
        L_0x0039:
            r6.ac()     // Catch:{ all -> 0x0065 }
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0041:
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ Exception -> 0x0047 }
            goto L_0x002a
        L_0x0047:
            r0 = move-exception
            goto L_0x002a
        L_0x0049:
            r0 = move-exception
            r1 = r2
        L_0x004b:
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ Exception -> 0x0058 }
        L_0x0050:
            if (r2 == 0) goto L_0x0055
            r2.close()     // Catch:{ Exception -> 0x0058 }
        L_0x0055:
            throw r0
        L_0x0056:
            r0 = move-exception
            goto L_0x002a
        L_0x0058:
            r1 = move-exception
            goto L_0x0055
        L_0x005a:
            r1 = move-exception
            r5 = r1
            r1 = r2
            r2 = r0
            r0 = r5
            goto L_0x004b
        L_0x0060:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x004b
        L_0x0065:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x004b
        L_0x006b:
            r1 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x0039
        L_0x006f:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.network.app.data.Database.ad():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0042 A[SYNTHETIC, Splitter:B:20:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0047 A[Catch:{ Exception -> 0x004b }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0052 A[SYNTHETIC, Splitter:B:28:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0057 A[Catch:{ Exception -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void ae() {
        /*
            r5 = this;
            r3 = 0
            android.content.Context r0 = r5.dh     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            java.lang.String r1 = "data.db"
            r0.deleteFile(r1)     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            android.content.Context r0 = r5.dh     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            java.lang.String r1 = "data.db"
            r2 = 2
            java.io.FileOutputStream r0 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.lang.String[] r2 = r5.de     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            int r2 = r2.length     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r1.writeInt(r2)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r2 = 0
        L_0x001d:
            java.lang.String[] r3 = r5.de     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            int r3 = r3.length     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            if (r2 < r3) goto L_0x0033
            int r2 = r5.df     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r1.writeInt(r2)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r1.flush()     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r1.close()     // Catch:{ Exception -> 0x005b }
            if (r0 == 0) goto L_0x0032
            r0.close()     // Catch:{ Exception -> 0x005b }
        L_0x0032:
            return
        L_0x0033:
            java.lang.String[] r3 = r5.de     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r3 = r3[r2]     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            r1.writeUTF(r3)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            int r2 = r2 + 1
            goto L_0x001d
        L_0x003d:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0040:
            if (r0 == 0) goto L_0x0045
            r0.close()     // Catch:{ Exception -> 0x004b }
        L_0x0045:
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ Exception -> 0x004b }
            goto L_0x0032
        L_0x004b:
            r0 = move-exception
            goto L_0x0032
        L_0x004d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ Exception -> 0x005d }
        L_0x0055:
            if (r2 == 0) goto L_0x005a
            r2.close()     // Catch:{ Exception -> 0x005d }
        L_0x005a:
            throw r0
        L_0x005b:
            r0 = move-exception
            goto L_0x0032
        L_0x005d:
            r1 = move-exception
            goto L_0x005a
        L_0x005f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0050
        L_0x0064:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0050
        L_0x0069:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0040
        L_0x006d:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.network.app.data.Database.ae():void");
    }

    private static String b(String str, String str2, String str3) {
        int indexOf = str.indexOf(str2);
        String str4 = str;
        while (indexOf >= 0) {
            String str5 = String.valueOf(str4.substring(0, indexOf)) + str3 + str4.substring(indexOf + str2.length(), str4.length());
            str4 = str5;
            indexOf = str5.indexOf(str2);
        }
        return str4;
    }

    private String c(String str, String str2) {
        int i;
        if (str2.equals("")) {
            return str;
        }
        String[] a = StringTools.a(str, '-');
        switch (this.de[1].charAt(0)) {
            case 'm':
                i = 0;
                break;
            case 't':
                i = 2;
                break;
            case 'u':
                i = 1;
                break;
            default:
                i = 0;
                break;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < a.length; i2++) {
            if (i2 == i) {
                stringBuffer.append(str2);
            } else {
                stringBuffer.append(a[i2]);
            }
            if (i2 < a.length - 1) {
                stringBuffer.append("-");
            }
        }
        return stringBuffer.toString();
    }

    private void h(String str) {
        if (Pay.dr) {
            System.out.println("xmlStr=" + str);
        }
        String b = b(b(str, "<data>", ""), "</data>", "");
        while (!b.equals("")) {
            String substring = b.substring(b.indexOf("<") + 1, b.indexOf(">"));
            String substring2 = b.substring(b.indexOf(">") + 1, b.indexOf("</"));
            this.di.put(substring, substring2);
            b = b(b, "<" + substring + ">" + substring2 + "</" + substring + ">", "");
            if (Pay.dr) {
                System.out.println("key=" + substring + ",value=" + substring2);
            }
        }
    }

    private static String i(String str) {
        try {
            HttpPost httpPost = new HttpPost(xmlURL);
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("xml", str));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, ENCODING));
            HttpResponse execute = new DefaultHttpClient().execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(execute.getEntity(), ENCODING);
            }
        } catch (Exception e) {
        }
        return FAIL_DATA;
    }
}
