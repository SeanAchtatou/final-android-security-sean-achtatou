package javax.microedition.midlet;

import android.util.Log;

public abstract class MIDlet implements bn {
    private int fp;

    public static void aN() {
        cf.b(cf.a((int) cl.MSG_SYSTEM_EXIT, (Object) null));
    }

    public static boolean r(String str) {
        return cl.B(str);
    }

    /* access modifiers changed from: protected */
    public abstract void L();

    public final void aO() {
        this.fp = 0;
        bl.start();
    }

    public final int getState() {
        return this.fp;
    }

    public final void onDestroy() {
        this.fp = 3;
        try {
            x();
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " in MIDlet destroyApp");
        }
    }

    public final void onPause() {
        this.fp = 2;
    }

    public final void onResume() {
        this.fp = 1;
    }

    public final void onStart() {
        this.fp = 1;
        try {
            L();
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " in MIDlet startApp");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void x();
}
