This is the location where we have designed the models for the Artificial Intelligence. You should have 4 folders, 5 models, 1 python file and the read me file.

/Application-ArtificialIntelligence : All the applications which have been used to 					      train, test and validate the Artificial 					      Intelligence models.

/Decompiled_safe and /Decompiled_unsafe : Contains all the applications from /	Application-ArtificialIntelligence that have been through reverse engineering.

model_resources-K_NN-NN-DANGEROUS-ALL : Represents the different models of Machine Learning (K-NN) and Deep Learning (Neural Network) which have been produced with ALL the permissions, then which only the DANGEROUS permissions.

resources_model_AI.py : Program which have been used to use the applications in Decompiled_safe and Decompiled_unsafe, in order to create the models.

Note : Do not alter, rename or change the code of any of the files, folders.
