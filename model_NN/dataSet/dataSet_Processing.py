import numpy as np
import os

path = 'benign500'

apks = os.listdir(path)

for apk in apks:
    extension = apk.split(".")[1]
    name = apk.split(".")[0]
    rename = name + "BIS" + "." + extension
    os.rename(f'{path}/{apk}',f'{path}/{rename}')

