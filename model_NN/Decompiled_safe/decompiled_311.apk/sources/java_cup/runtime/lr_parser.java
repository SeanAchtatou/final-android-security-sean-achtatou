package java_cup.runtime;

import java.lang.reflect.Array;
import java.util.Stack;

public abstract class lr_parser {
    protected static final int _error_sync_size = 3;
    protected boolean _done_parsing;
    private Scanner _scanner;
    protected short[][] action_tab;
    protected Symbol cur_token;
    protected Symbol[] lookahead;
    protected int lookahead_pos;
    protected short[][] production_tab;
    protected short[][] reduce_tab;
    protected Stack stack;
    protected int tos;

    public abstract int EOF_sym();

    public abstract short[][] action_table();

    public abstract Symbol do_action(int i, lr_parser lr_parser, Stack stack2, int i2) throws Exception;

    public abstract int error_sym();

    /* access modifiers changed from: protected */
    public abstract void init_actions() throws Exception;

    public abstract short[][] production_table();

    public abstract short[][] reduce_table();

    public abstract int start_production();

    public abstract int start_state();

    public lr_parser() {
        this._done_parsing = false;
        this.stack = new Stack();
    }

    public lr_parser(Scanner s) {
        this();
        setScanner(s);
    }

    /* access modifiers changed from: protected */
    public int error_sync_size() {
        return 3;
    }

    public void done_parsing() {
        this._done_parsing = true;
    }

    public void setScanner(Scanner s) {
        this._scanner = s;
    }

    public Scanner getScanner() {
        return this._scanner;
    }

    public void user_init() throws Exception {
    }

    public Symbol scan() throws Exception {
        Symbol sym = getScanner().next_token();
        return sym != null ? sym : new Symbol(EOF_sym());
    }

    public void report_fatal_error(String message, Object info) throws Exception {
        done_parsing();
        report_error(message, info);
        throw new Exception("Can't recover from previous error(s)");
    }

    public void report_error(String message, Object info) {
        System.err.print(message);
        if (!(info instanceof Symbol)) {
            System.err.println("");
        } else if (((Symbol) info).left != -1) {
            System.err.println(new StringBuffer().append(" at character ").append(((Symbol) info).left).append(" of input").toString());
        } else {
            System.err.println("");
        }
    }

    public void syntax_error(Symbol cur_token2) {
        report_error("Syntax error", cur_token2);
    }

    public void unrecovered_syntax_error(Symbol cur_token2) throws Exception {
        report_fatal_error("Couldn't repair and continue parse", cur_token2);
    }

    /* access modifiers changed from: protected */
    public final short get_action(int state, int sym) {
        short[] row = this.action_tab[state];
        if (row.length < 20) {
            int probe = 0;
            while (probe < row.length) {
                int probe2 = probe + 1;
                short tag = row[probe];
                if (tag == sym || tag == -1) {
                    return row[probe2];
                }
                probe = probe2 + 1;
            }
            return (short) 0;
        }
        int first = 0;
        int last = ((row.length - 1) / 2) - 1;
        while (first <= last) {
            int probe3 = (first + last) / 2;
            if (sym == row[probe3 * 2]) {
                return row[(probe3 * 2) + 1];
            }
            if (sym > row[probe3 * 2]) {
                first = probe3 + 1;
            } else {
                last = probe3 - 1;
            }
        }
        return row[row.length - 1];
    }

    /* access modifiers changed from: protected */
    public final short get_reduce(int state, int sym) {
        short[] row = this.reduce_tab[state];
        if (row == null) {
            return (short) -1;
        }
        int probe = 0;
        while (probe < row.length) {
            int probe2 = probe + 1;
            short tag = row[probe];
            if (tag == sym || tag == -1) {
                return row[probe2];
            }
            probe = probe2 + 1;
        }
        return (short) -1;
    }

    public Symbol parse() throws Exception {
        Symbol lhs_sym = null;
        this.production_tab = production_table();
        this.action_tab = action_table();
        this.reduce_tab = reduce_table();
        init_actions();
        user_init();
        this.cur_token = scan();
        this.stack.removeAllElements();
        this.stack.push(new Symbol(0, start_state()));
        this.tos = 0;
        this._done_parsing = false;
        while (!this._done_parsing) {
            if (this.cur_token.used_by_parser) {
                throw new Error("Symbol recycling detected (fix your scanner).");
            }
            int act = get_action(((Symbol) this.stack.peek()).parse_state, this.cur_token.sym);
            if (act > 0) {
                this.cur_token.parse_state = act - 1;
                this.cur_token.used_by_parser = true;
                this.stack.push(this.cur_token);
                this.tos++;
                this.cur_token = scan();
            } else if (act < 0) {
                lhs_sym = do_action((-act) - 1, this, this.stack, this.tos);
                short lhs_sym_num = this.production_tab[(-act) - 1][0];
                short handle_size = this.production_tab[(-act) - 1][1];
                for (int i = 0; i < handle_size; i++) {
                    this.stack.pop();
                    this.tos--;
                }
                lhs_sym.parse_state = get_reduce(((Symbol) this.stack.peek()).parse_state, lhs_sym_num);
                lhs_sym.used_by_parser = true;
                this.stack.push(lhs_sym);
                this.tos++;
            } else if (act == 0) {
                syntax_error(this.cur_token);
                if (!error_recovery(false)) {
                    unrecovered_syntax_error(this.cur_token);
                    done_parsing();
                } else {
                    lhs_sym = (Symbol) this.stack.peek();
                }
            }
        }
        return lhs_sym;
    }

    public void debug_message(String mess) {
        System.err.println(mess);
    }

    public void dump_stack() {
        if (this.stack == null) {
            debug_message("# Stack dump requested, but stack is null");
            return;
        }
        debug_message("============ Parse Stack Dump ============");
        for (int i = 0; i < this.stack.size(); i++) {
            debug_message(new StringBuffer().append("Symbol: ").append(((Symbol) this.stack.elementAt(i)).sym).append(" State: ").append(((Symbol) this.stack.elementAt(i)).parse_state).toString());
        }
        debug_message("==========================================");
    }

    public void debug_reduce(int prod_num, int nt_num, int rhs_size) {
        debug_message(new StringBuffer().append("# Reduce with prod #").append(prod_num).append(" [NT=").append(nt_num).append(", ").append("SZ=").append(rhs_size).append("]").toString());
    }

    public void debug_shift(Symbol shift_tkn) {
        debug_message(new StringBuffer().append("# Shift under term #").append(shift_tkn.sym).append(" to state #").append(shift_tkn.parse_state).toString());
    }

    public void debug_stack() {
        StringBuffer sb = new StringBuffer("## STACK:");
        for (int i = 0; i < this.stack.size(); i++) {
            Symbol s = (Symbol) this.stack.elementAt(i);
            sb.append(new StringBuffer().append(" <state ").append(s.parse_state).append(", sym ").append(s.sym).append(">").toString());
            if (i % 3 == 2 || i == this.stack.size() - 1) {
                debug_message(sb.toString());
                sb = new StringBuffer("         ");
            }
        }
    }

    public Symbol debug_parse() throws Exception {
        Symbol lhs_sym = null;
        this.production_tab = production_table();
        this.action_tab = action_table();
        this.reduce_tab = reduce_table();
        debug_message("# Initializing parser");
        init_actions();
        user_init();
        this.cur_token = scan();
        debug_message(new StringBuffer().append("# Current Symbol is #").append(this.cur_token.sym).toString());
        this.stack.removeAllElements();
        this.stack.push(new Symbol(0, start_state()));
        this.tos = 0;
        this._done_parsing = false;
        while (!this._done_parsing) {
            if (this.cur_token.used_by_parser) {
                throw new Error("Symbol recycling detected (fix your scanner).");
            }
            int act = get_action(((Symbol) this.stack.peek()).parse_state, this.cur_token.sym);
            if (act > 0) {
                this.cur_token.parse_state = act - 1;
                this.cur_token.used_by_parser = true;
                debug_shift(this.cur_token);
                this.stack.push(this.cur_token);
                this.tos++;
                this.cur_token = scan();
                debug_message(new StringBuffer().append("# Current token is ").append(this.cur_token).toString());
            } else if (act < 0) {
                lhs_sym = do_action((-act) - 1, this, this.stack, this.tos);
                short lhs_sym_num = this.production_tab[(-act) - 1][0];
                short handle_size = this.production_tab[(-act) - 1][1];
                debug_reduce((-act) - 1, lhs_sym_num, handle_size);
                for (int i = 0; i < handle_size; i++) {
                    this.stack.pop();
                    this.tos--;
                }
                int act2 = get_reduce(((Symbol) this.stack.peek()).parse_state, lhs_sym_num);
                debug_message(new StringBuffer().append("# Reduce rule: top state ").append(((Symbol) this.stack.peek()).parse_state).append(", lhs sym ").append((int) lhs_sym_num).append(" -> state ").append(act2).toString());
                lhs_sym.parse_state = act2;
                lhs_sym.used_by_parser = true;
                this.stack.push(lhs_sym);
                this.tos++;
                debug_message(new StringBuffer().append("# Goto state #").append(act2).toString());
            } else if (act == 0) {
                syntax_error(this.cur_token);
                if (!error_recovery(true)) {
                    unrecovered_syntax_error(this.cur_token);
                    done_parsing();
                } else {
                    lhs_sym = (Symbol) this.stack.peek();
                }
            }
        }
        return lhs_sym;
    }

    /* access modifiers changed from: protected */
    public boolean error_recovery(boolean debug) throws Exception {
        if (debug) {
            debug_message("# Attempting error recovery");
        }
        if (!find_recovery_config(debug)) {
            if (debug) {
                debug_message("# Error recovery fails");
            }
            return false;
        }
        read_lookahead();
        while (true) {
            if (debug) {
                debug_message("# Trying to parse ahead");
            }
            if (try_parse_ahead(debug)) {
                if (debug) {
                    debug_message("# Parse-ahead ok, going back to normal parse");
                }
                parse_lookahead(debug);
                return true;
            } else if (this.lookahead[0].sym == EOF_sym()) {
                if (debug) {
                    debug_message("# Error recovery fails at EOF");
                }
                return false;
            } else {
                if (debug) {
                    debug_message(new StringBuffer().append("# Consuming Symbol #").append(this.lookahead[0].sym).toString());
                }
                restart_lookahead();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean shift_under_error() {
        return get_action(((Symbol) this.stack.peek()).parse_state, error_sym()) > 0;
    }

    /* access modifiers changed from: protected */
    public boolean find_recovery_config(boolean debug) {
        if (debug) {
            debug_message("# Finding recovery state on stack");
        }
        int right_pos = ((Symbol) this.stack.peek()).right;
        int left_pos = ((Symbol) this.stack.peek()).left;
        while (!shift_under_error()) {
            if (debug) {
                debug_message(new StringBuffer().append("# Pop stack by one, state was # ").append(((Symbol) this.stack.peek()).parse_state).toString());
            }
            left_pos = ((Symbol) this.stack.pop()).left;
            this.tos--;
            if (this.stack.empty()) {
                if (debug) {
                    debug_message("# No recovery state found on stack");
                }
                return false;
            }
        }
        int act = get_action(((Symbol) this.stack.peek()).parse_state, error_sym());
        if (debug) {
            debug_message(new StringBuffer().append("# Recover state found (#").append(((Symbol) this.stack.peek()).parse_state).append(")").toString());
            debug_message(new StringBuffer().append("# Shifting on error to state #").append(act - 1).toString());
        }
        Symbol error_token = new Symbol(error_sym(), left_pos, right_pos);
        error_token.parse_state = act - 1;
        error_token.used_by_parser = true;
        this.stack.push(error_token);
        this.tos++;
        return true;
    }

    /* access modifiers changed from: protected */
    public void read_lookahead() throws Exception {
        this.lookahead = new Symbol[error_sync_size()];
        for (int i = 0; i < error_sync_size(); i++) {
            this.lookahead[i] = this.cur_token;
            this.cur_token = scan();
        }
        this.lookahead_pos = 0;
    }

    /* access modifiers changed from: protected */
    public Symbol cur_err_token() {
        return this.lookahead[this.lookahead_pos];
    }

    /* access modifiers changed from: protected */
    public boolean advance_lookahead() {
        this.lookahead_pos++;
        return this.lookahead_pos < error_sync_size();
    }

    /* access modifiers changed from: protected */
    public void restart_lookahead() throws Exception {
        for (int i = 1; i < error_sync_size(); i++) {
            this.lookahead[i - 1] = this.lookahead[i];
        }
        this.lookahead[error_sync_size() - 1] = this.cur_token;
        this.cur_token = scan();
        this.lookahead_pos = 0;
    }

    /* access modifiers changed from: protected */
    public boolean try_parse_ahead(boolean debug) throws Exception {
        virtual_parse_stack vstack = new virtual_parse_stack(this.stack);
        while (true) {
            int act = get_action(vstack.top(), cur_err_token().sym);
            if (act == 0) {
                return false;
            }
            if (act > 0) {
                vstack.push(act - 1);
                if (debug) {
                    debug_message(new StringBuffer().append("# Parse-ahead shifts Symbol #").append(cur_err_token().sym).append(" into state #").append(act - 1).toString());
                }
                if (!advance_lookahead()) {
                    return true;
                }
            } else if ((-act) - 1 == start_production()) {
                if (debug) {
                    debug_message("# Parse-ahead accepts");
                }
                return true;
            } else {
                short lhs = this.production_tab[(-act) - 1][0];
                short rhs_size = this.production_tab[(-act) - 1][1];
                for (int i = 0; i < rhs_size; i++) {
                    vstack.pop();
                }
                if (debug) {
                    debug_message(new StringBuffer().append("# Parse-ahead reduces: handle size = ").append((int) rhs_size).append(" lhs = #").append((int) lhs).append(" from state #").append(vstack.top()).toString());
                }
                vstack.push(get_reduce(vstack.top(), lhs));
                if (debug) {
                    debug_message(new StringBuffer().append("# Goto state #").append(vstack.top()).toString());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void parse_lookahead(boolean debug) throws Exception {
        Symbol lhs_sym = null;
        this.lookahead_pos = 0;
        if (debug) {
            debug_message("# Reparsing saved input with actions");
            debug_message(new StringBuffer().append("# Current Symbol is #").append(cur_err_token().sym).toString());
            debug_message(new StringBuffer().append("# Current state is #").append(((Symbol) this.stack.peek()).parse_state).toString());
        }
        while (!this._done_parsing) {
            int act = get_action(((Symbol) this.stack.peek()).parse_state, cur_err_token().sym);
            if (act > 0) {
                cur_err_token().parse_state = act - 1;
                cur_err_token().used_by_parser = true;
                if (debug) {
                    debug_shift(cur_err_token());
                }
                this.stack.push(cur_err_token());
                this.tos++;
                if (!advance_lookahead()) {
                    if (debug) {
                        debug_message("# Completed reparse");
                        return;
                    }
                    return;
                } else if (debug) {
                    debug_message(new StringBuffer().append("# Current Symbol is #").append(cur_err_token().sym).toString());
                }
            } else if (act < 0) {
                lhs_sym = do_action((-act) - 1, this, this.stack, this.tos);
                short lhs_sym_num = this.production_tab[(-act) - 1][0];
                short handle_size = this.production_tab[(-act) - 1][1];
                if (debug) {
                    debug_reduce((-act) - 1, lhs_sym_num, handle_size);
                }
                for (int i = 0; i < handle_size; i++) {
                    this.stack.pop();
                    this.tos--;
                }
                int act2 = get_reduce(((Symbol) this.stack.peek()).parse_state, lhs_sym_num);
                lhs_sym.parse_state = act2;
                lhs_sym.used_by_parser = true;
                this.stack.push(lhs_sym);
                this.tos++;
                if (debug) {
                    debug_message(new StringBuffer().append("# Goto state #").append(act2).toString());
                }
            } else if (act == 0) {
                report_fatal_error("Syntax error", lhs_sym);
                return;
            }
        }
    }

    protected static short[][] unpackFromStrings(String[] sa) {
        StringBuffer sb = new StringBuffer(sa[0]);
        for (int i = 1; i < sa.length; i++) {
            sb.append(sa[i]);
        }
        int size1 = (sb.charAt(0) << 16) | sb.charAt(0 + 1);
        int n = 0 + 2;
        short[][] result = (short[][]) Array.newInstance(short[].class, size1);
        int i2 = 0;
        while (i2 < size1) {
            int size2 = (sb.charAt(n) << 16) | sb.charAt(n + 1);
            result[i2] = new short[size2];
            int j = 0;
            int n2 = n + 2;
            while (j < size2) {
                result[i2][j] = (short) (sb.charAt(n2) - 2);
                j++;
                n2++;
            }
            i2++;
            n = n2;
        }
        return result;
    }
}
