package java_cup.runtime;

public class Symbol {
    public int left;
    public int parse_state;
    public int right;
    public int sym;
    boolean used_by_parser;
    public Object value;

    public Symbol(int id, int l, int r, Object o) {
        this(id);
        this.left = l;
        this.right = r;
        this.value = o;
    }

    public Symbol(int id, Object o) {
        this(id, -1, -1, o);
    }

    public Symbol(int id, int l, int r) {
        this(id, l, r, null);
    }

    public Symbol(int sym_num) {
        this(sym_num, -1);
        this.left = -1;
        this.right = -1;
        this.value = null;
    }

    Symbol(int sym_num, int state) {
        this.used_by_parser = false;
        this.sym = sym_num;
        this.parse_state = state;
    }

    public String toString() {
        return new StringBuffer().append("#").append(this.sym).toString();
    }
}
