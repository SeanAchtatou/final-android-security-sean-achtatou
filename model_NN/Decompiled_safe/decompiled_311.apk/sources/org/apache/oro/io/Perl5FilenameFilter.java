package org.apache.oro.io;

import org.apache.oro.text.PatternCache;
import org.apache.oro.text.PatternCacheLRU;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Matcher;

public class Perl5FilenameFilter extends RegexFilenameFilter {
    private static final PatternCache __CACHE = new PatternCacheLRU();
    private static final PatternMatcher __MATCHER = new Perl5Matcher();

    public Perl5FilenameFilter(String str, int i) {
        super(__CACHE, __MATCHER, str, i);
    }

    public Perl5FilenameFilter(String str) {
        super(__CACHE, __MATCHER, str);
    }

    public Perl5FilenameFilter() {
        super(__CACHE, __MATCHER);
    }
}
