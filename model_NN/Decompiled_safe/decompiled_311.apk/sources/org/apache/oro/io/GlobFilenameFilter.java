package org.apache.oro.io;

import org.apache.oro.text.GlobCompiler;
import org.apache.oro.text.PatternCache;
import org.apache.oro.text.PatternCacheLRU;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Matcher;

public class GlobFilenameFilter extends RegexFilenameFilter {
    private static final PatternCache __CACHE = new PatternCacheLRU(new GlobCompiler());
    private static final PatternMatcher __MATCHER = new Perl5Matcher();

    public GlobFilenameFilter(String str, int i) {
        super(__CACHE, __MATCHER, str, i);
    }

    public GlobFilenameFilter(String str) {
        super(__CACHE, __MATCHER, str);
    }

    public GlobFilenameFilter() {
        super(__CACHE, __MATCHER);
    }
}
