package org.apache.oro.text;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Vector;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Util;

public final class MatchActionProcessor {
    private Vector __actions;
    private PatternCompiler __compiler;
    private MatchAction __defaultAction;
    private Pattern __fieldSeparator;
    private PatternMatcher __matcher;
    private Vector __patterns;

    public MatchActionProcessor(PatternCompiler patternCompiler, PatternMatcher patternMatcher) {
        this.__patterns = new Vector();
        this.__actions = new Vector();
        this.__defaultAction = new DefaultMatchAction();
        this.__compiler = patternCompiler;
        this.__matcher = patternMatcher;
    }

    public MatchActionProcessor() {
        this(new Perl5Compiler(), new Perl5Matcher());
    }

    public void addAction(String str, int i, MatchAction matchAction) throws MalformedPatternException {
        if (str != null) {
            this.__patterns.addElement(this.__compiler.compile(str, i));
        } else {
            this.__patterns.addElement(null);
        }
        this.__actions.addElement(matchAction);
    }

    public void addAction(String str, int i) throws MalformedPatternException {
        addAction(str, i, this.__defaultAction);
    }

    public void addAction(String str) throws MalformedPatternException {
        addAction(str, 0);
    }

    public void addAction(String str, MatchAction matchAction) throws MalformedPatternException {
        addAction(str, 0, matchAction);
    }

    public void setFieldSeparator(String str, int i) throws MalformedPatternException {
        if (str == null) {
            this.__fieldSeparator = null;
        } else {
            this.__fieldSeparator = this.__compiler.compile(str, i);
        }
    }

    public void setFieldSeparator(String str) throws MalformedPatternException {
        setFieldSeparator(str, 0);
    }

    public void processMatches(InputStream inputStream, OutputStream outputStream, String str) throws IOException {
        processMatches(new InputStreamReader(inputStream, str), new OutputStreamWriter(outputStream));
    }

    public void processMatches(InputStream inputStream, OutputStream outputStream) throws IOException {
        processMatches(new InputStreamReader(inputStream), new OutputStreamWriter(outputStream));
    }

    public void processMatches(Reader reader, Writer writer) throws IOException {
        LineNumberReader lineNumberReader = new LineNumberReader(reader);
        PrintWriter printWriter = new PrintWriter(writer);
        MatchActionInfo matchActionInfo = new MatchActionInfo();
        Vector vector = new Vector();
        matchActionInfo.matcher = this.__matcher;
        matchActionInfo.fieldSeparator = this.__fieldSeparator;
        matchActionInfo.input = lineNumberReader;
        matchActionInfo.output = printWriter;
        matchActionInfo.fields = null;
        int size = this.__patterns.size();
        matchActionInfo.lineNumber = 0;
        while (true) {
            String readLine = lineNumberReader.readLine();
            matchActionInfo.line = readLine;
            if (readLine == null) {
                printWriter.flush();
                lineNumberReader.close();
                return;
            }
            matchActionInfo.charLine = matchActionInfo.line.toCharArray();
            for (int i = 0; i < size; i++) {
                if (this.__patterns.elementAt(i) != null) {
                    Pattern pattern = (Pattern) this.__patterns.elementAt(i);
                    if (this.__matcher.contains(matchActionInfo.charLine, pattern)) {
                        matchActionInfo.match = this.__matcher.getMatch();
                        matchActionInfo.lineNumber = lineNumberReader.getLineNumber();
                        matchActionInfo.pattern = pattern;
                        if (this.__fieldSeparator != null) {
                            vector.removeAllElements();
                            Util.split(vector, this.__matcher, this.__fieldSeparator, matchActionInfo.line);
                            matchActionInfo.fields = vector;
                        } else {
                            matchActionInfo.fields = null;
                        }
                        ((MatchAction) this.__actions.elementAt(i)).processMatch(matchActionInfo);
                    }
                } else {
                    matchActionInfo.match = null;
                    matchActionInfo.lineNumber = lineNumberReader.getLineNumber();
                    if (this.__fieldSeparator != null) {
                        vector.removeAllElements();
                        Util.split(vector, this.__matcher, this.__fieldSeparator, matchActionInfo.line);
                        matchActionInfo.fields = vector;
                    } else {
                        matchActionInfo.fields = null;
                    }
                    ((MatchAction) this.__actions.elementAt(i)).processMatch(matchActionInfo);
                }
            }
        }
    }
}
