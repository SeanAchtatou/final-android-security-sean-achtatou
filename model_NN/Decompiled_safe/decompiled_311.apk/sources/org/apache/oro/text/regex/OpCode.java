package org.apache.oro.text.regex;

final class OpCode {
    static final char _ALNUM = '\u0012';
    static final char _ALNUMC = '2';
    static final char _ALPHA = '&';
    static final char _ANY = '\u0007';
    static final char _ANYOF = '\t';
    static final char _ANYOFUN = '#';
    static final char _ASCII = '3';
    static final char _BACK = '\r';
    static final char _BLANK = '\'';
    static final char _BOL = '\u0001';
    static final char _BOUND = '\u0014';
    static final char _BRANCH = '\f';
    static final char _CLOSE = '\u001c';
    static final char _CNTRL = '(';
    static final char _CURLY = '\n';
    static final char _CURLYX = '\u000b';
    static final char _DIGIT = '\u0018';
    static final char _END = '\u0000';
    static final char _EOL = '\u0004';
    static final char _EXACTLY = '\u000e';
    static final char _GBOL = '\u001e';
    static final char _GRAPH = ')';
    static final char _IFMATCH = '\u001f';
    static final char _LOWER = '*';
    static final char _MBOL = '\u0002';
    static final char _MEOL = '\u0005';
    static final char _MINMOD = '\u001d';
    static final char _NALNUM = '\u0013';
    static final char _NANYOFUN = '$';
    static final char _NBOUND = '\u0015';
    static final char _NDIGIT = '\u0019';
    static final char _NOPCODE = '0';
    static final char _NOTHING = '\u000f';
    static final char _NSPACE = '\u0017';
    static final int _NULL_OFFSET = -1;
    static final char _NULL_POINTER = '\u0000';
    static final char _ONECHAR = '1';
    static final char _OPCODE = '/';
    static final char _OPEN = '\u001b';
    static final char _PLUS = '\u0011';
    static final char _PRINT = '+';
    static final char _PUNCT = ',';
    static final char _RANGE = '%';
    static final char _REF = '\u001a';
    static final char _SANY = '\b';
    static final char _SBOL = '\u0003';
    static final char _SEOL = '\u0006';
    static final char _SPACE = '\u0016';
    static final char _STAR = '\u0010';
    static final char _SUCCEED = '!';
    static final char _UNLESSM = ' ';
    static final char _UPPER = '-';
    static final char _WHILEM = '\"';
    static final char _XDIGIT = '.';
    static final char[] _opLengthOne = {_ANY, _SANY, _ANYOF, _ALNUM, _NALNUM, _SPACE, _NSPACE, _DIGIT, _NDIGIT, _ANYOFUN, '$', _ALPHA, _BLANK, _CNTRL, _GRAPH, _LOWER, _PRINT, _PUNCT, _UPPER, _XDIGIT, _OPCODE, _NOPCODE, _ONECHAR, _ALNUMC, _ASCII};
    static final char[] _opLengthVaries = {_BRANCH, _BACK, _STAR, _PLUS, _CURLY, _CURLYX, _REF, _WHILEM};
    static final char[] _opType;
    static final int[] _operandLength;

    private OpCode() {
    }

    static {
        int[] iArr = new int[52];
        iArr[10] = 2;
        iArr[11] = 2;
        iArr[26] = 1;
        iArr[27] = 1;
        iArr[28] = 1;
        _operandLength = iArr;
        char[] cArr = new char[52];
        cArr[1] = _BOL;
        cArr[2] = _BOL;
        cArr[3] = _BOL;
        cArr[4] = _EOL;
        cArr[5] = _EOL;
        cArr[6] = _EOL;
        cArr[7] = _ANY;
        cArr[8] = _ANY;
        cArr[9] = _ANYOF;
        cArr[10] = _CURLY;
        cArr[11] = _CURLY;
        cArr[12] = _BRANCH;
        cArr[13] = _BACK;
        cArr[14] = _EXACTLY;
        cArr[15] = _NOTHING;
        cArr[16] = _STAR;
        cArr[17] = _PLUS;
        cArr[18] = _ALNUM;
        cArr[19] = _NALNUM;
        cArr[20] = _BOUND;
        cArr[21] = _NBOUND;
        cArr[22] = _SPACE;
        cArr[23] = _NSPACE;
        cArr[24] = _DIGIT;
        cArr[25] = _NDIGIT;
        cArr[26] = _REF;
        cArr[27] = _OPEN;
        cArr[28] = _CLOSE;
        cArr[29] = _MINMOD;
        cArr[30] = _BOL;
        cArr[31] = _BRANCH;
        cArr[32] = _BRANCH;
        cArr[34] = _WHILEM;
        cArr[35] = _ANYOFUN;
        cArr[36] = '$';
        cArr[37] = _RANGE;
        cArr[38] = _ALPHA;
        cArr[39] = _BLANK;
        cArr[40] = _CNTRL;
        cArr[41] = _GRAPH;
        cArr[42] = _LOWER;
        cArr[43] = _PRINT;
        cArr[44] = _PUNCT;
        cArr[45] = _UPPER;
        cArr[46] = _XDIGIT;
        cArr[47] = _OPCODE;
        cArr[48] = _NOPCODE;
        cArr[49] = _ONECHAR;
        cArr[50] = _ALNUMC;
        cArr[51] = _ASCII;
        _opType = cArr;
    }

    static final int _getNextOffset(char[] cArr, int i) {
        return cArr[i + 1];
    }

    static final char _getArg1(char[] cArr, int i) {
        return cArr[i + 2];
    }

    static final char _getArg2(char[] cArr, int i) {
        return cArr[i + 3];
    }

    static final int _getOperand(int i) {
        return i + 2;
    }

    static final boolean _isInArray(char c, char[] cArr, int i) {
        int i2 = i;
        while (i2 < cArr.length) {
            int i3 = i2 + 1;
            if (c == cArr[i2]) {
                return true;
            }
            i2 = i3;
        }
        return false;
    }

    static final int _getNextOperator(int i) {
        return i + 2;
    }

    static final int _getPrevOperator(int i) {
        return i - 2;
    }

    static final int _getNext(char[] cArr, int i) {
        if (cArr == null) {
            return -1;
        }
        int _getNextOffset = _getNextOffset(cArr, i);
        if (_getNextOffset == 0) {
            return -1;
        }
        if (cArr[i] == 13) {
            return i - _getNextOffset;
        }
        return _getNextOffset + i;
    }

    static final boolean _isWordCharacter(char c) {
        return Character.isLetterOrDigit(c) || c == '_';
    }
}
