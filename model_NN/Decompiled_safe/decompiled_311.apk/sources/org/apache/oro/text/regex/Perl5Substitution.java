package org.apache.oro.text.regex;

public class Perl5Substitution extends StringSubstitution {
    public static final int INTERPOLATE_ALL = 0;
    public static final int INTERPOLATE_NONE = -1;
    static final int _OPCODE_COPY = -1;
    static final int _OPCODE_ENDCASE_MODE = -6;
    static final int _OPCODE_LOWERCASE_CHAR = -2;
    static final int _OPCODE_LOWERCASE_MODE = -4;
    static final int _OPCODE_UPPERCASE_CHAR = -3;
    static final int _OPCODE_UPPERCASE_MODE = -5;
    private static final int __MAX_GROUPS = 65535;
    private static final int __OPCODE_STORAGE_SIZE = 32;
    transient String _lastInterpolation;
    int _numInterpolations;
    int[] _subOpcodes;
    int _subOpcodesCount;
    char[] _substitutionChars;

    private static final boolean __isInterpolationCharacter(char c) {
        return Character.isDigit(c) || c == '&';
    }

    private void __addElement(int i) {
        int length = this._subOpcodes.length;
        if (this._subOpcodesCount == length) {
            int[] iArr = new int[(length + 32)];
            System.arraycopy(this._subOpcodes, 0, iArr, 0, length);
            this._subOpcodes = iArr;
        }
        int[] iArr2 = this._subOpcodes;
        int i2 = this._subOpcodesCount;
        this._subOpcodesCount = i2 + 1;
        iArr2[i2] = i;
    }

    private void __parseSubs(String str) {
        char[] charArray = str.toCharArray();
        this._substitutionChars = charArray;
        int length = charArray.length;
        this._subOpcodes = new int[32];
        this._subOpcodesCount = 0;
        int i = -1;
        boolean z = false;
        int i2 = 0;
        int i3 = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (i3 < length) {
            char c = charArray[i3];
            int i4 = i3 + 1;
            if (z) {
                int digit = Character.digit(c, 10);
                if (digit > -1) {
                    if (i2 <= __MAX_GROUPS) {
                        i2 = (i2 * 10) + digit;
                    }
                    if (i4 == length) {
                        __addElement(i2);
                    }
                } else if (c == '&' && charArray[i3 - 1] == '$') {
                    __addElement(0);
                    i2 = 0;
                    z = false;
                } else {
                    __addElement(i2);
                    i2 = 0;
                    z = false;
                }
                i3++;
            }
            if ((c == '$' || c == '\\') && !z3) {
                if (i >= 0) {
                    __addElement(i3 - i);
                    i = -1;
                }
                if (i4 != length) {
                    char c2 = charArray[i4];
                    if (c == '$') {
                        z = __isInterpolationCharacter(c2);
                    } else if (c == '\\') {
                        if (c2 == 'l') {
                            if (!z2) {
                                __addElement(_OPCODE_LOWERCASE_CHAR);
                                i3++;
                            }
                        } else if (c2 == 'u') {
                            if (!z2) {
                                __addElement(_OPCODE_UPPERCASE_CHAR);
                                i3++;
                            }
                        } else if (c2 == 'L') {
                            __addElement(_OPCODE_LOWERCASE_MODE);
                            i3++;
                            z2 = true;
                        } else if (c2 == 'U') {
                            __addElement(_OPCODE_UPPERCASE_MODE);
                            i3++;
                            z2 = true;
                        } else if (c2 == 'E') {
                            __addElement(_OPCODE_ENDCASE_MODE);
                            i3++;
                            z2 = false;
                        } else {
                            z3 = true;
                        }
                    }
                }
                i3++;
            } else {
                z3 = false;
                if (i < 0) {
                    __addElement(-1);
                    __addElement(i3);
                    i = i3;
                }
                if (i4 == length) {
                    __addElement(i4 - i);
                }
                i3++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String _finalInterpolatedSub(MatchResult matchResult) {
        StringBuffer stringBuffer = new StringBuffer(10);
        _calcSub(stringBuffer, matchResult);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void _calcSub(java.lang.StringBuffer r12, org.apache.oro.text.regex.MatchResult r13) {
        /*
            r11 = this;
            int[] r0 = r11._subOpcodes
            r1 = 0
            char[] r2 = r11._substitutionChars
            r3 = 0
            java.lang.String r3 = r13.group(r3)
            char[] r3 = r3.toCharArray()
            int r4 = r11._subOpcodesCount
            r5 = 0
            r10 = r5
            r5 = r1
            r1 = r10
        L_0x0014:
            if (r1 < r4) goto L_0x0017
            return
        L_0x0017:
            r6 = r0[r1]
            if (r6 < 0) goto L_0x0053
            int r7 = r13.groups()
            if (r6 >= r7) goto L_0x0053
            int r7 = r13.begin(r6)
            if (r7 < 0) goto L_0x0037
            int r6 = r13.end(r6)
            if (r6 < 0) goto L_0x0037
            int r8 = r13.length()
            if (r7 >= r8) goto L_0x0037
            if (r6 > r8) goto L_0x0037
            if (r7 < r6) goto L_0x003a
        L_0x0037:
            int r1 = r1 + 1
            goto L_0x0014
        L_0x003a:
            int r6 = r6 - r7
            r8 = r7
            r7 = r6
            r6 = r3
        L_0x003e:
            r9 = -2
            if (r5 != r9) goto L_0x0080
            int r5 = r8 + 1
            char r8 = r6[r8]
            char r8 = java.lang.Character.toLowerCase(r8)
            r12.append(r8)
            int r7 = r7 + -1
            r12.append(r6, r5, r7)
            r5 = 0
            goto L_0x0037
        L_0x0053:
            r7 = -1
            if (r6 != r7) goto L_0x0065
            int r1 = r1 + 1
            if (r1 >= r4) goto L_0x0037
            r6 = r0[r1]
            int r1 = r1 + 1
            if (r1 >= r4) goto L_0x0037
            r7 = r0[r1]
            r8 = r6
            r6 = r2
            goto L_0x003e
        L_0x0065:
            r7 = -2
            if (r6 == r7) goto L_0x006b
            r7 = -3
            if (r6 != r7) goto L_0x0073
        L_0x006b:
            r7 = -4
            if (r5 == r7) goto L_0x0037
            r7 = -5
            if (r5 == r7) goto L_0x0037
            r5 = r6
            goto L_0x0037
        L_0x0073:
            r7 = -4
            if (r6 == r7) goto L_0x0079
            r7 = -5
            if (r6 != r7) goto L_0x007b
        L_0x0079:
            r5 = r6
            goto L_0x0037
        L_0x007b:
            r7 = -6
            if (r6 != r7) goto L_0x0037
            r5 = 0
            goto L_0x0037
        L_0x0080:
            r9 = -3
            if (r5 != r9) goto L_0x0095
            int r5 = r8 + 1
            char r8 = r6[r8]
            char r8 = java.lang.Character.toUpperCase(r8)
            r12.append(r8)
            int r7 = r7 + -1
            r12.append(r6, r5, r7)
            r5 = 0
            goto L_0x0037
        L_0x0095:
            r9 = -4
            if (r5 != r9) goto L_0x00a8
            int r7 = r7 + r8
        L_0x0099:
            if (r8 >= r7) goto L_0x0037
            int r9 = r8 + 1
            char r8 = r6[r8]
            char r8 = java.lang.Character.toLowerCase(r8)
            r12.append(r8)
            r8 = r9
            goto L_0x0099
        L_0x00a8:
            r9 = -5
            if (r5 != r9) goto L_0x00bb
            int r7 = r7 + r8
        L_0x00ac:
            if (r8 >= r7) goto L_0x0037
            int r9 = r8 + 1
            char r8 = r6[r8]
            char r8 = java.lang.Character.toUpperCase(r8)
            r12.append(r8)
            r8 = r9
            goto L_0x00ac
        L_0x00bb:
            r12.append(r6, r8, r7)
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Substitution._calcSub(java.lang.StringBuffer, org.apache.oro.text.regex.MatchResult):void");
    }

    public Perl5Substitution() {
        this("", 0);
    }

    public Perl5Substitution(String str) {
        this(str, 0);
    }

    public Perl5Substitution(String str, int i) {
        setSubstitution(str, i);
    }

    public void setSubstitution(String str) {
        setSubstitution(str, 0);
    }

    public void setSubstitution(String str, int i) {
        super.setSubstitution(str);
        this._numInterpolations = i;
        if (i == -1 || (str.indexOf(36) == -1 && str.indexOf(92) == -1)) {
            this._subOpcodes = null;
        } else {
            __parseSubs(str);
        }
        this._lastInterpolation = null;
    }

    public void appendSubstitution(StringBuffer stringBuffer, MatchResult matchResult, int i, PatternMatcherInput patternMatcherInput, PatternMatcher patternMatcher, Pattern pattern) {
        if (this._subOpcodes == null) {
            super.appendSubstitution(stringBuffer, matchResult, i, patternMatcherInput, patternMatcher, pattern);
        } else if (this._numInterpolations < 1 || i < this._numInterpolations) {
            _calcSub(stringBuffer, matchResult);
        } else {
            if (i == this._numInterpolations) {
                this._lastInterpolation = _finalInterpolatedSub(matchResult);
            }
            stringBuffer.append(this._lastInterpolation);
        }
    }
}
