package org.apache.oro.text;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.util.Cache;

public abstract class GenericPatternCache implements PatternCache {
    public static final int DEFAULT_CAPACITY = 20;
    Cache _cache;
    PatternCompiler _compiler;

    GenericPatternCache(Cache cache, PatternCompiler patternCompiler) {
        this._cache = cache;
        this._compiler = patternCompiler;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r0.getOptions() == r4) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized org.apache.oro.text.regex.Pattern addPattern(java.lang.String r3, int r4) throws org.apache.oro.text.regex.MalformedPatternException {
        /*
            r2 = this;
            monitor-enter(r2)
            org.apache.oro.util.Cache r0 = r2._cache     // Catch:{ all -> 0x001f }
            java.lang.Object r0 = r0.getElement(r3)     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x0013
            org.apache.oro.text.regex.Pattern r0 = (org.apache.oro.text.regex.Pattern) r0     // Catch:{ all -> 0x001f }
            int r1 = r0.getOptions()     // Catch:{ all -> 0x001f }
            if (r1 != r4) goto L_0x0013
        L_0x0011:
            monitor-exit(r2)
            return r0
        L_0x0013:
            org.apache.oro.text.regex.PatternCompiler r0 = r2._compiler     // Catch:{ all -> 0x001f }
            org.apache.oro.text.regex.Pattern r0 = r0.compile(r3, r4)     // Catch:{ all -> 0x001f }
            org.apache.oro.util.Cache r1 = r2._cache     // Catch:{ all -> 0x001f }
            r1.addElement(r3, r0)     // Catch:{ all -> 0x001f }
            goto L_0x0011
        L_0x001f:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.GenericPatternCache.addPattern(java.lang.String, int):org.apache.oro.text.regex.Pattern");
    }

    public final synchronized Pattern addPattern(String str) throws MalformedPatternException {
        return addPattern(str, 0);
    }

    public final synchronized Pattern getPattern(String str, int i) throws MalformedCachePatternException {
        try {
        } catch (MalformedPatternException e) {
            throw new MalformedCachePatternException(new StringBuffer("Invalid expression: ").append(str).append("\n").append(e.getMessage()).toString());
        }
        return addPattern(str, i);
    }

    public final synchronized Pattern getPattern(String str) throws MalformedCachePatternException {
        return getPattern(str, 0);
    }

    public final int size() {
        return this._cache.size();
    }

    public final int capacity() {
        return this._cache.capacity();
    }
}
