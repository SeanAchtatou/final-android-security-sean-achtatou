package org.apache.oro.text;

import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.util.CacheFIFO;

public final class PatternCacheFIFO extends GenericPatternCache {
    public PatternCacheFIFO(int i, PatternCompiler patternCompiler) {
        super(new CacheFIFO(i), patternCompiler);
    }

    public PatternCacheFIFO(PatternCompiler patternCompiler) {
        this(20, patternCompiler);
    }

    public PatternCacheFIFO(int i) {
        this(i, new Perl5Compiler());
    }

    public PatternCacheFIFO() {
        this(20);
    }
}
