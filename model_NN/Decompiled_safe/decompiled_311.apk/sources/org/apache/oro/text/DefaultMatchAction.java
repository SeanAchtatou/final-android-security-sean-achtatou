package org.apache.oro.text;

final class DefaultMatchAction implements MatchAction {
    DefaultMatchAction() {
    }

    public void processMatch(MatchActionInfo matchActionInfo) {
        matchActionInfo.output.println(matchActionInfo.line);
    }
}
