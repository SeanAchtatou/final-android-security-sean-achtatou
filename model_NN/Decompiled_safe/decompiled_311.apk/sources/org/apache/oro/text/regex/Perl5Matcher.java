package org.apache.oro.text.regex;

import frink.parser.sym;
import java.util.Stack;

public final class Perl5Matcher implements PatternMatcher {
    private static final int __DEFAULT_LAST_MATCH_END_OFFSET = -100;
    private static final char __EOS = '￿';
    private static final int __INITIAL_NUM_OFFSETS = 20;
    private int[] __beginMatchOffsets;
    private int __bol;
    private boolean __caseInsensitive = false;
    private int __currentOffset;
    private Perl5Repetition __currentRep;
    private int[] __endMatchOffsets;
    private int __endOffset;
    private int __eol;
    private int __expSize;
    private char[] __input;
    private int __inputOffset;
    private int __lastMatchInputEndOffset = -100;
    private Perl5MatchResult __lastMatchResult;
    private int __lastParen;
    private boolean __lastSuccess = false;
    private boolean __multiline = false;
    private int __numParentheses;
    private char[] __originalInput;
    private char __previousChar;
    private char[] __program;
    private Stack __stack = new Stack();

    private static boolean __compare(char[] cArr, int i, char[] cArr2, int i2, int i3) {
        int i4 = 0;
        int i5 = i2;
        int i6 = i;
        while (i4 < i3) {
            if (i6 >= cArr.length) {
                return false;
            }
            if (i5 >= cArr2.length) {
                return false;
            }
            if (cArr[i6] != cArr2[i5]) {
                return false;
            }
            i4++;
            i6++;
            i5++;
        }
        return true;
    }

    private static int __findFirst(char[] cArr, int i, int i2, char[] cArr2) {
        if (cArr.length == 0) {
            return i2;
        }
        char c = cArr2[0];
        int i3 = i;
        while (i3 < i2) {
            if (c == cArr[i3]) {
                int i4 = 0;
                int i5 = i3;
                while (i5 < i2 && i4 < cArr2.length && cArr2[i4] == cArr[i5]) {
                    i4++;
                    i5++;
                }
                if (i4 >= cArr2.length) {
                    return i3;
                }
            }
            i3++;
        }
        return i3;
    }

    private void __pushState(int i) {
        int[] iArr;
        int i2 = (this.__expSize - i) * 3;
        if (i2 <= 0) {
            iArr = new int[3];
        } else {
            iArr = new int[(i2 + 3)];
        }
        iArr[0] = this.__expSize;
        iArr[1] = this.__lastParen;
        iArr[2] = this.__inputOffset;
        int i3 = i2;
        int i4 = this.__expSize;
        while (i4 > i) {
            iArr[i3] = this.__endMatchOffsets[i4];
            iArr[i3 + 1] = this.__beginMatchOffsets[i4];
            iArr[i3 + 2] = i4;
            i4 -= 3;
            i3 -= 3;
        }
        this.__stack.push(iArr);
    }

    private void __popState() {
        int[] iArr = (int[]) this.__stack.pop();
        this.__expSize = iArr[0];
        this.__lastParen = iArr[1];
        this.__inputOffset = iArr[2];
        for (int i = 3; i < iArr.length; i += 3) {
            int i2 = iArr[i + 2];
            this.__beginMatchOffsets[i2] = iArr[i + 1];
            if (i2 <= this.__lastParen) {
                this.__endMatchOffsets[i2] = iArr[i];
            }
        }
        int i3 = this.__lastParen;
        while (true) {
            i3++;
            if (i3 <= this.__numParentheses) {
                if (i3 > this.__expSize) {
                    this.__beginMatchOffsets[i3] = -1;
                }
                this.__endMatchOffsets[i3] = -1;
            } else {
                return;
            }
        }
    }

    private void __initInterpreterGlobals(Perl5Pattern perl5Pattern, char[] cArr, int i, int i2, int i3) {
        this.__caseInsensitive = perl5Pattern._isCaseInsensitive;
        this.__input = cArr;
        this.__endOffset = i2;
        this.__currentRep = new Perl5Repetition();
        this.__currentRep._numInstances = 0;
        this.__currentRep._lastRepetition = null;
        this.__program = perl5Pattern._program;
        this.__stack.setSize(0);
        if (i3 == i || i3 <= 0) {
            this.__previousChar = 10;
        } else {
            this.__previousChar = cArr[i3 - 1];
            if (!this.__multiline && this.__previousChar == 10) {
                this.__previousChar = 0;
            }
        }
        this.__numParentheses = perl5Pattern._numParentheses;
        this.__currentOffset = i3;
        this.__bol = i;
        this.__eol = i2;
        int i4 = this.__numParentheses + 1;
        if (this.__beginMatchOffsets == null || i4 > this.__beginMatchOffsets.length) {
            if (i4 < 20) {
                i4 = 20;
            }
            this.__beginMatchOffsets = new int[i4];
            this.__endMatchOffsets = new int[i4];
        }
    }

    private void __setLastMatchResult() {
        this.__lastMatchResult = new Perl5MatchResult(this.__numParentheses + 1);
        if (this.__endMatchOffsets[0] > this.__originalInput.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.__lastMatchResult._matchBeginOffset = this.__beginMatchOffsets[0];
        int i = 0;
        while (this.__numParentheses >= 0) {
            int i2 = this.__beginMatchOffsets[this.__numParentheses];
            if (i2 >= 0) {
                this.__lastMatchResult._beginGroupOffset[this.__numParentheses] = i2 - this.__lastMatchResult._matchBeginOffset;
            } else {
                this.__lastMatchResult._beginGroupOffset[this.__numParentheses] = -1;
            }
            int i3 = this.__endMatchOffsets[this.__numParentheses];
            if (i3 >= 0) {
                this.__lastMatchResult._endGroupOffset[this.__numParentheses] = i3 - this.__lastMatchResult._matchBeginOffset;
                if (i3 > i && i3 <= this.__originalInput.length) {
                    i = i3;
                }
            } else {
                this.__lastMatchResult._endGroupOffset[this.__numParentheses] = -1;
            }
            this.__numParentheses--;
        }
        this.__lastMatchResult._match = new String(this.__originalInput, this.__beginMatchOffsets[0], i - this.__beginMatchOffsets[0]);
        this.__originalInput = null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00c2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean __interpret(org.apache.oro.text.regex.Perl5Pattern r11, char[] r12, int r13, int r14, int r15) {
        /*
            r10 = this;
            r9 = 0
            r3 = 32768(0x8000, float:4.5918E-41)
            r8 = 0
            r7 = 1
            r10.__initInterpreterGlobals(r11, r12, r13, r14, r15)
            char[] r0 = r11._mustString
            if (r0 == 0) goto L_0x0387
            int r1 = r11._anchor
            r1 = r1 & 3
            if (r1 == 0) goto L_0x0021
            boolean r1 = r10.__multiline
            if (r1 != 0) goto L_0x001d
            int r1 = r11._anchor
            r1 = r1 & 2
            if (r1 == 0) goto L_0x0387
        L_0x001d:
            int r1 = r11._back
            if (r1 < 0) goto L_0x0387
        L_0x0021:
            char[] r1 = r10.__input
            int r2 = r10.__currentOffset
            int r1 = __findFirst(r1, r2, r14, r0)
            r10.__currentOffset = r1
            int r1 = r10.__currentOffset
            if (r1 < r14) goto L_0x0040
            int r0 = r11._options
            r0 = r0 & r3
            if (r0 != 0) goto L_0x003a
            int r0 = r11._mustUtility
            int r0 = r0 + 1
            r11._mustUtility = r0
        L_0x003a:
            r0 = r8
        L_0x003b:
            r10.__lastSuccess = r0
            r10.__lastMatchResult = r9
            return r0
        L_0x0040:
            int r1 = r11._back
            if (r1 < 0) goto L_0x0067
            int r1 = r10.__currentOffset
            int r2 = r11._back
            int r1 = r1 - r2
            r10.__currentOffset = r1
            int r1 = r10.__currentOffset
            if (r1 >= r15) goto L_0x0051
            r10.__currentOffset = r15
        L_0x0051:
            int r1 = r11._back
            int r0 = r0.length
            int r0 = r0 + r1
        L_0x0055:
            int r1 = r11._anchor
            r1 = r1 & 3
            if (r1 == 0) goto L_0x00c2
            int r1 = r10.__currentOffset
            if (r1 != r13) goto L_0x0081
            boolean r1 = r10.__tryExpression(r13)
            if (r1 == 0) goto L_0x0081
            r0 = r7
            goto L_0x003b
        L_0x0067:
            boolean r1 = r11._isExpensive
            if (r1 != 0) goto L_0x007d
            int r1 = r11._options
            r1 = r1 & r3
            if (r1 != 0) goto L_0x007d
            int r1 = r11._mustUtility
            int r1 = r1 - r7
            r11._mustUtility = r1
            if (r1 >= 0) goto L_0x007d
            r11._mustString = r9
            r10.__currentOffset = r15
            r0 = r8
            goto L_0x0055
        L_0x007d:
            r10.__currentOffset = r15
            int r0 = r0.length
            goto L_0x0055
        L_0x0081:
            boolean r1 = r10.__multiline
            if (r1 != 0) goto L_0x0091
            int r1 = r11._anchor
            r1 = r1 & 2
            if (r1 != 0) goto L_0x0091
            int r1 = r11._anchor
            r1 = r1 & 8
            if (r1 == 0) goto L_0x0143
        L_0x0091:
            if (r0 <= 0) goto L_0x0384
            int r0 = r0 - r7
        L_0x0094:
            int r0 = r14 - r0
            int r1 = r10.__currentOffset
            if (r1 <= r15) goto L_0x009f
            int r1 = r10.__currentOffset
            int r1 = r1 - r7
            r10.__currentOffset = r1
        L_0x009f:
            int r1 = r10.__currentOffset
            if (r1 < r0) goto L_0x00a5
            r0 = r8
            goto L_0x003b
        L_0x00a5:
            char[] r1 = r10.__input
            int r2 = r10.__currentOffset
            int r3 = r2 + 1
            r10.__currentOffset = r3
            char r1 = r1[r2]
            r2 = 10
            if (r1 != r2) goto L_0x009f
            int r1 = r10.__currentOffset
            if (r1 >= r0) goto L_0x009f
            int r1 = r10.__currentOffset
            boolean r1 = r10.__tryExpression(r1)
            if (r1 == 0) goto L_0x009f
            r0 = r7
            goto L_0x003b
        L_0x00c2:
            char[] r1 = r11._startString
            if (r1 == 0) goto L_0x012a
            char[] r0 = r11._startString
            int r1 = r11._anchor
            r1 = r1 & 4
            if (r1 == 0) goto L_0x011b
            char r0 = r0[r8]
        L_0x00d0:
            int r1 = r10.__currentOffset
            if (r1 < r14) goto L_0x00d7
            r0 = r8
            goto L_0x003b
        L_0x00d7:
            char[] r1 = r10.__input
            int r2 = r10.__currentOffset
            char r1 = r1[r2]
            if (r0 != r1) goto L_0x00fc
            int r1 = r10.__currentOffset
            boolean r1 = r10.__tryExpression(r1)
            if (r1 == 0) goto L_0x00ea
            r0 = r7
            goto L_0x003b
        L_0x00ea:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x00f0:
            int r1 = r10.__currentOffset
            if (r1 >= r14) goto L_0x00fc
            char[] r1 = r10.__input
            int r2 = r10.__currentOffset
            char r1 = r1[r2]
            if (r1 == r0) goto L_0x0103
        L_0x00fc:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
            goto L_0x00d0
        L_0x0103:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
            goto L_0x00f0
        L_0x010a:
            int r1 = r10.__currentOffset
            boolean r1 = r10.__tryExpression(r1)
            if (r1 == 0) goto L_0x0115
            r0 = r7
            goto L_0x003b
        L_0x0115:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x011b:
            char[] r1 = r10.__input
            int r2 = r10.__currentOffset
            int r1 = __findFirst(r1, r2, r14, r0)
            r10.__currentOffset = r1
            if (r1 < r14) goto L_0x010a
            r0 = r8
            goto L_0x003b
        L_0x012a:
            int r1 = r11._startClassOffset
            r2 = -1
            if (r1 == r2) goto L_0x034e
            int r2 = r11._anchor
            r2 = r2 & 4
            if (r2 == 0) goto L_0x0146
            r2 = r8
        L_0x0136:
            if (r0 <= 0) goto L_0x0381
            int r3 = r0 - r7
        L_0x013a:
            int r4 = r14 - r3
            char[] r5 = r10.__program
            char r5 = r5[r1]
            switch(r5) {
                case 9: goto L_0x0148;
                case 18: goto L_0x0371;
                case 19: goto L_0x0374;
                case 20: goto L_0x01b4;
                case 21: goto L_0x0206;
                case 22: goto L_0x0377;
                case 23: goto L_0x037a;
                case 24: goto L_0x037d;
                case 25: goto L_0x037f;
                case 35: goto L_0x0183;
                case 36: goto L_0x0183;
                default: goto L_0x0143;
            }
        L_0x0143:
            r0 = r8
            goto L_0x003b
        L_0x0146:
            r2 = r7
            goto L_0x0136
        L_0x0148:
            int r0 = org.apache.oro.text.regex.OpCode._getOperand(r1)
            r1 = r7
        L_0x014d:
            int r3 = r10.__currentOffset
            if (r3 < r4) goto L_0x0154
            r0 = r8
            goto L_0x003b
        L_0x0154:
            char[] r3 = r10.__input
            int r5 = r10.__currentOffset
            char r3 = r3[r5]
            r5 = 256(0x100, float:3.59E-43)
            if (r3 >= r5) goto L_0x0181
            char[] r5 = r10.__program
            int r6 = r3 >> 4
            int r6 = r6 + r0
            char r5 = r5[r6]
            r3 = r3 & 15
            int r3 = r7 << r3
            r3 = r3 & r5
            if (r3 != 0) goto L_0x0181
            if (r1 == 0) goto L_0x0179
            int r1 = r10.__currentOffset
            boolean r1 = r10.__tryExpression(r1)
            if (r1 == 0) goto L_0x0179
            r0 = r7
            goto L_0x003b
        L_0x0179:
            r1 = r2
        L_0x017a:
            int r3 = r10.__currentOffset
            int r3 = r3 + 1
            r10.__currentOffset = r3
            goto L_0x014d
        L_0x0181:
            r1 = r7
            goto L_0x017a
        L_0x0183:
            int r0 = org.apache.oro.text.regex.OpCode._getOperand(r1)
            r1 = r7
        L_0x0188:
            int r3 = r10.__currentOffset
            if (r3 < r4) goto L_0x018f
            r0 = r8
            goto L_0x003b
        L_0x018f:
            char[] r3 = r10.__input
            int r6 = r10.__currentOffset
            char r3 = r3[r6]
            char[] r6 = r10.__program
            boolean r3 = r10.__matchUnicodeClass(r3, r6, r0, r5)
            if (r3 == 0) goto L_0x01b2
            if (r1 == 0) goto L_0x01aa
            int r1 = r10.__currentOffset
            boolean r1 = r10.__tryExpression(r1)
            if (r1 == 0) goto L_0x01aa
            r0 = r7
            goto L_0x003b
        L_0x01aa:
            r1 = r2
        L_0x01ab:
            int r3 = r10.__currentOffset
            int r3 = r3 + 1
            r10.__currentOffset = r3
            goto L_0x0188
        L_0x01b2:
            r1 = r7
            goto L_0x01ab
        L_0x01b4:
            if (r0 <= 0) goto L_0x036e
            int r1 = r3 + 1
            int r1 = r4 + -1
        L_0x01ba:
            int r2 = r10.__currentOffset
            if (r2 == r13) goto L_0x01dc
            char[] r2 = r10.__input
            int r3 = r10.__currentOffset
            int r3 = r3 - r7
            char r2 = r2[r3]
            boolean r2 = org.apache.oro.text.regex.OpCode._isWordCharacter(r2)
        L_0x01c9:
            int r3 = r10.__currentOffset
            if (r3 < r1) goto L_0x01e3
            if (r0 > 0) goto L_0x01d1
            if (r2 == 0) goto L_0x0143
        L_0x01d1:
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x0143
            r0 = r7
            goto L_0x003b
        L_0x01dc:
            char r2 = r10.__previousChar
            boolean r2 = org.apache.oro.text.regex.OpCode._isWordCharacter(r2)
            goto L_0x01c9
        L_0x01e3:
            char[] r3 = r10.__input
            int r4 = r10.__currentOffset
            char r3 = r3[r4]
            boolean r3 = org.apache.oro.text.regex.OpCode._isWordCharacter(r3)
            if (r2 == r3) goto L_0x01ff
            if (r2 == 0) goto L_0x01fd
            r2 = r8
        L_0x01f2:
            int r3 = r10.__currentOffset
            boolean r3 = r10.__tryExpression(r3)
            if (r3 == 0) goto L_0x01ff
            r0 = r7
            goto L_0x003b
        L_0x01fd:
            r2 = r7
            goto L_0x01f2
        L_0x01ff:
            int r3 = r10.__currentOffset
            int r3 = r3 + 1
            r10.__currentOffset = r3
            goto L_0x01c9
        L_0x0206:
            if (r0 <= 0) goto L_0x036b
            int r1 = r3 + 1
            int r1 = r4 + -1
        L_0x020c:
            int r2 = r10.__currentOffset
            if (r2 == r13) goto L_0x022e
            char[] r2 = r10.__input
            int r3 = r10.__currentOffset
            int r3 = r3 - r7
            char r2 = r2[r3]
            boolean r2 = org.apache.oro.text.regex.OpCode._isWordCharacter(r2)
        L_0x021b:
            int r3 = r10.__currentOffset
            if (r3 < r1) goto L_0x0235
            if (r0 > 0) goto L_0x0223
            if (r2 != 0) goto L_0x0143
        L_0x0223:
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x0143
            r0 = r7
            goto L_0x003b
        L_0x022e:
            char r2 = r10.__previousChar
            boolean r2 = org.apache.oro.text.regex.OpCode._isWordCharacter(r2)
            goto L_0x021b
        L_0x0235:
            char[] r3 = r10.__input
            int r4 = r10.__currentOffset
            char r3 = r3[r4]
            boolean r3 = org.apache.oro.text.regex.OpCode._isWordCharacter(r3)
            if (r2 == r3) goto L_0x024d
            if (r2 == 0) goto L_0x024b
            r2 = r8
        L_0x0244:
            int r3 = r10.__currentOffset
            int r3 = r3 + 1
            r10.__currentOffset = r3
            goto L_0x021b
        L_0x024b:
            r2 = r7
            goto L_0x0244
        L_0x024d:
            int r3 = r10.__currentOffset
            boolean r3 = r10.__tryExpression(r3)
            if (r3 == 0) goto L_0x0244
            r0 = r7
            goto L_0x003b
        L_0x0258:
            char[] r1 = r10.__input
            int r3 = r10.__currentOffset
            char r1 = r1[r3]
            boolean r1 = org.apache.oro.text.regex.OpCode._isWordCharacter(r1)
            if (r1 == 0) goto L_0x027f
            if (r0 == 0) goto L_0x0271
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x0271
            r0 = r7
            goto L_0x003b
        L_0x0271:
            r0 = r2
        L_0x0272:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x0278:
            int r1 = r10.__currentOffset
            if (r1 < r4) goto L_0x0258
            r0 = r8
            goto L_0x003b
        L_0x027f:
            r0 = r7
            goto L_0x0272
        L_0x0281:
            char[] r1 = r10.__input
            int r3 = r10.__currentOffset
            char r1 = r1[r3]
            boolean r1 = org.apache.oro.text.regex.OpCode._isWordCharacter(r1)
            if (r1 != 0) goto L_0x02a8
            if (r0 == 0) goto L_0x029a
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x029a
            r0 = r7
            goto L_0x003b
        L_0x029a:
            r0 = r2
        L_0x029b:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x02a1:
            int r1 = r10.__currentOffset
            if (r1 < r4) goto L_0x0281
            r0 = r8
            goto L_0x003b
        L_0x02a8:
            r0 = r7
            goto L_0x029b
        L_0x02aa:
            char[] r1 = r10.__input
            int r3 = r10.__currentOffset
            char r1 = r1[r3]
            boolean r1 = java.lang.Character.isWhitespace(r1)
            if (r1 == 0) goto L_0x02d1
            if (r0 == 0) goto L_0x02c3
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x02c3
            r0 = r7
            goto L_0x003b
        L_0x02c3:
            r0 = r2
        L_0x02c4:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x02ca:
            int r1 = r10.__currentOffset
            if (r1 < r4) goto L_0x02aa
            r0 = r8
            goto L_0x003b
        L_0x02d1:
            r0 = r7
            goto L_0x02c4
        L_0x02d3:
            char[] r1 = r10.__input
            int r3 = r10.__currentOffset
            char r1 = r1[r3]
            boolean r1 = java.lang.Character.isWhitespace(r1)
            if (r1 != 0) goto L_0x02fa
            if (r0 == 0) goto L_0x02ec
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x02ec
            r0 = r7
            goto L_0x003b
        L_0x02ec:
            r0 = r2
        L_0x02ed:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x02f3:
            int r1 = r10.__currentOffset
            if (r1 < r4) goto L_0x02d3
            r0 = r8
            goto L_0x003b
        L_0x02fa:
            r0 = r7
            goto L_0x02ed
        L_0x02fc:
            char[] r1 = r10.__input
            int r3 = r10.__currentOffset
            char r1 = r1[r3]
            boolean r1 = java.lang.Character.isDigit(r1)
            if (r1 == 0) goto L_0x0323
            if (r0 == 0) goto L_0x0315
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x0315
            r0 = r7
            goto L_0x003b
        L_0x0315:
            r0 = r2
        L_0x0316:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x031c:
            int r1 = r10.__currentOffset
            if (r1 < r4) goto L_0x02fc
            r0 = r8
            goto L_0x003b
        L_0x0323:
            r0 = r7
            goto L_0x0316
        L_0x0325:
            char[] r1 = r10.__input
            int r3 = r10.__currentOffset
            char r1 = r1[r3]
            boolean r1 = java.lang.Character.isDigit(r1)
            if (r1 != 0) goto L_0x034c
            if (r0 == 0) goto L_0x033e
            int r0 = r10.__currentOffset
            boolean r0 = r10.__tryExpression(r0)
            if (r0 == 0) goto L_0x033e
            r0 = r7
            goto L_0x003b
        L_0x033e:
            r0 = r2
        L_0x033f:
            int r1 = r10.__currentOffset
            int r1 = r1 + 1
            r10.__currentOffset = r1
        L_0x0345:
            int r1 = r10.__currentOffset
            if (r1 < r4) goto L_0x0325
            r0 = r8
            goto L_0x003b
        L_0x034c:
            r0 = r7
            goto L_0x033f
        L_0x034e:
            if (r0 <= 0) goto L_0x0369
            int r0 = r0 - r7
        L_0x0351:
            int r0 = r14 - r0
        L_0x0353:
            int r1 = r10.__currentOffset
            boolean r1 = r10.__tryExpression(r1)
            if (r1 == 0) goto L_0x035e
            r0 = r7
            goto L_0x003b
        L_0x035e:
            int r1 = r10.__currentOffset
            int r2 = r1 + 1
            r10.__currentOffset = r2
            if (r1 < r0) goto L_0x0353
            r0 = r8
            goto L_0x003b
        L_0x0369:
            r0 = r8
            goto L_0x0351
        L_0x036b:
            r1 = r4
            goto L_0x020c
        L_0x036e:
            r1 = r4
            goto L_0x01ba
        L_0x0371:
            r0 = r7
            goto L_0x0278
        L_0x0374:
            r0 = r7
            goto L_0x02a1
        L_0x0377:
            r0 = r7
            goto L_0x02ca
        L_0x037a:
            r0 = r7
            goto L_0x02f3
        L_0x037d:
            r0 = r7
            goto L_0x031c
        L_0x037f:
            r0 = r7
            goto L_0x0345
        L_0x0381:
            r3 = r8
            goto L_0x013a
        L_0x0384:
            r0 = r8
            goto L_0x0094
        L_0x0387:
            r0 = r8
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Matcher.__interpret(org.apache.oro.text.regex.Perl5Pattern, char[], int, int, int):boolean");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x000f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x000f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean __matchUnicodeClass(char r7, char[] r8, int r9, char r10) {
        /*
            r6 = this;
            r5 = 1
            r4 = 0
            r0 = 35
            if (r10 == r0) goto L_0x0010
            r0 = r4
        L_0x0007:
            r1 = r9
        L_0x0008:
            char r2 = r8[r1]
            if (r2 != 0) goto L_0x0012
            if (r0 == 0) goto L_0x00ed
            r0 = r4
        L_0x000f:
            return r0
        L_0x0010:
            r0 = r5
            goto L_0x0007
        L_0x0012:
            char r2 = r8[r1]
            r3 = 37
            if (r2 != r3) goto L_0x0027
            int r1 = r1 + 1
            char r2 = r8[r1]
            if (r7 < r2) goto L_0x0024
            int r2 = r1 + 1
            char r2 = r8[r2]
            if (r7 <= r2) goto L_0x000f
        L_0x0024:
            int r1 = r1 + 2
            goto L_0x0008
        L_0x0027:
            char r2 = r8[r1]
            r3 = 49
            if (r2 != r3) goto L_0x0037
            int r1 = r1 + 1
            int r2 = r1 + 1
            char r1 = r8[r1]
            if (r1 == r7) goto L_0x000f
        L_0x0035:
            r1 = r2
            goto L_0x0008
        L_0x0037:
            char r2 = r8[r1]
            r3 = 47
            if (r2 != r3) goto L_0x004e
        L_0x003d:
            int r1 = r1 + 1
            int r2 = r1 + 1
            char r1 = r8[r1]
            switch(r1) {
                case 18: goto L_0x0047;
                case 19: goto L_0x0054;
                case 20: goto L_0x0046;
                case 21: goto L_0x0046;
                case 22: goto L_0x005b;
                case 23: goto L_0x0062;
                case 24: goto L_0x0069;
                case 25: goto L_0x0070;
                case 26: goto L_0x0046;
                case 27: goto L_0x0046;
                case 28: goto L_0x0046;
                case 29: goto L_0x0046;
                case 30: goto L_0x0046;
                case 31: goto L_0x0046;
                case 32: goto L_0x0046;
                case 33: goto L_0x0046;
                case 34: goto L_0x0046;
                case 35: goto L_0x0046;
                case 36: goto L_0x0046;
                case 37: goto L_0x0046;
                case 38: goto L_0x007e;
                case 39: goto L_0x0085;
                case 40: goto L_0x008c;
                case 41: goto L_0x00be;
                case 42: goto L_0x0094;
                case 43: goto L_0x00b8;
                case 44: goto L_0x00c4;
                case 45: goto L_0x00a6;
                case 46: goto L_0x00cd;
                case 47: goto L_0x0046;
                case 48: goto L_0x0046;
                case 49: goto L_0x0046;
                case 50: goto L_0x0077;
                case 51: goto L_0x00e7;
                default: goto L_0x0046;
            }
        L_0x0046:
            goto L_0x0035
        L_0x0047:
            boolean r1 = org.apache.oro.text.regex.OpCode._isWordCharacter(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x004e:
            if (r0 == 0) goto L_0x0052
            r0 = r4
            goto L_0x003d
        L_0x0052:
            r0 = r5
            goto L_0x003d
        L_0x0054:
            boolean r1 = org.apache.oro.text.regex.OpCode._isWordCharacter(r7)
            if (r1 != 0) goto L_0x0035
            goto L_0x000f
        L_0x005b:
            boolean r1 = java.lang.Character.isWhitespace(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x0062:
            boolean r1 = java.lang.Character.isWhitespace(r7)
            if (r1 != 0) goto L_0x0035
            goto L_0x000f
        L_0x0069:
            boolean r1 = java.lang.Character.isDigit(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x0070:
            boolean r1 = java.lang.Character.isDigit(r7)
            if (r1 != 0) goto L_0x0035
            goto L_0x000f
        L_0x0077:
            boolean r1 = java.lang.Character.isLetterOrDigit(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x007e:
            boolean r1 = java.lang.Character.isLetter(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x0085:
            boolean r1 = java.lang.Character.isSpaceChar(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x008c:
            boolean r1 = java.lang.Character.isISOControl(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x0094:
            boolean r1 = java.lang.Character.isLowerCase(r7)
            if (r1 != 0) goto L_0x000f
            boolean r1 = r6.__caseInsensitive
            if (r1 == 0) goto L_0x0035
            boolean r1 = java.lang.Character.isUpperCase(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x00a6:
            boolean r1 = java.lang.Character.isUpperCase(r7)
            if (r1 != 0) goto L_0x000f
            boolean r1 = r6.__caseInsensitive
            if (r1 == 0) goto L_0x0035
            boolean r1 = java.lang.Character.isLowerCase(r7)
            if (r1 == 0) goto L_0x0035
            goto L_0x000f
        L_0x00b8:
            boolean r1 = java.lang.Character.isSpaceChar(r7)
            if (r1 != 0) goto L_0x000f
        L_0x00be:
            boolean r1 = java.lang.Character.isLetterOrDigit(r7)
            if (r1 != 0) goto L_0x000f
        L_0x00c4:
            int r1 = java.lang.Character.getType(r7)
            switch(r1) {
                case 20: goto L_0x000f;
                case 21: goto L_0x000f;
                case 22: goto L_0x000f;
                case 23: goto L_0x000f;
                case 24: goto L_0x000f;
                case 25: goto L_0x000f;
                case 26: goto L_0x000f;
                case 27: goto L_0x000f;
                default: goto L_0x00cb;
            }
        L_0x00cb:
            goto L_0x0035
        L_0x00cd:
            r1 = 48
            if (r7 < r1) goto L_0x00d5
            r1 = 57
            if (r7 <= r1) goto L_0x000f
        L_0x00d5:
            r1 = 97
            if (r7 < r1) goto L_0x00dd
            r1 = 102(0x66, float:1.43E-43)
            if (r7 <= r1) goto L_0x000f
        L_0x00dd:
            r1 = 65
            if (r7 < r1) goto L_0x0035
            r1 = 70
            if (r7 > r1) goto L_0x0035
            goto L_0x000f
        L_0x00e7:
            r1 = 128(0x80, float:1.794E-43)
            if (r7 >= r1) goto L_0x0035
            goto L_0x000f
        L_0x00ed:
            r0 = r5
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Matcher.__matchUnicodeClass(char, char[], int, char):boolean");
    }

    private boolean __tryExpression(int i) {
        this.__inputOffset = i;
        this.__lastParen = 0;
        this.__expSize = 0;
        if (this.__numParentheses > 0) {
            for (int i2 = 0; i2 <= this.__numParentheses; i2++) {
                this.__beginMatchOffsets[i2] = -1;
                this.__endMatchOffsets[i2] = -1;
            }
        }
        if (!__match(1)) {
            return false;
        }
        this.__beginMatchOffsets[0] = i;
        this.__endMatchOffsets[0] = this.__inputOffset;
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private int __repeat(int i, int i2) {
        char c;
        int i3 = this.__inputOffset;
        int i4 = this.__eol;
        if (i2 != 65535 && i2 < i4 - i3) {
            i4 = i3 + i2;
        }
        int _getOperand = OpCode._getOperand(i);
        char c2 = this.__program[i];
        switch (c2) {
            case 7:
                while (i3 < i4 && this.__input[i3] != 10) {
                    i3++;
                }
                break;
            case 8:
                i3 = i4;
                break;
            case 9:
                if (i3 < i4 && (c = this.__input[i3]) < 256) {
                    char c3 = c;
                    int i5 = i3;
                    char c4 = c3;
                    while (true) {
                        if (c4 < 256) {
                            if (((1 << (c4 & 15)) & this.__program[(c4 >> 4) + _getOperand]) == 0) {
                                i3 = i5 + 1;
                                if (i3 >= i4) {
                                    break;
                                } else {
                                    i5 = i3;
                                    c4 = this.__input[i3];
                                }
                            } else {
                                i3 = i5;
                                break;
                            }
                        } else {
                            i3 = i5;
                            break;
                        }
                    }
                }
            case 14:
                int i6 = _getOperand + 1;
                while (i3 < i4 && this.__program[i6] == this.__input[i3]) {
                    i3++;
                }
                break;
            case 18:
                while (i3 < i4 && OpCode._isWordCharacter(this.__input[i3])) {
                    i3++;
                }
                break;
            case sym.DOUBLEPIPE:
                while (i3 < i4 && !OpCode._isWordCharacter(this.__input[i3])) {
                    i3++;
                }
                break;
            case 22:
                while (i3 < i4 && Character.isWhitespace(this.__input[i3])) {
                    i3++;
                }
                break;
            case sym.LOGICALNAND:
                while (i3 < i4 && !Character.isWhitespace(this.__input[i3])) {
                    i3++;
                }
                break;
            case 24:
                while (i3 < i4 && Character.isDigit(this.__input[i3])) {
                    i3++;
                }
                break;
            case sym.LOGICALIMPLIES:
                while (i3 < i4 && !Character.isDigit(this.__input[i3])) {
                    i3++;
                }
                break;
            case sym.BOOLEANLIT:
            case sym.COMPARISON:
                if (i3 < i4) {
                    char c5 = this.__input[i3];
                    while (true) {
                        char c6 = c5;
                        int i7 = i3;
                        if (__matchUnicodeClass(c6, this.__program, _getOperand, c2)) {
                            i3 = i7 + 1;
                            if (i3 >= i4) {
                                break;
                            } else {
                                c5 = this.__input[i3];
                            }
                        } else {
                            i3 = i7;
                            break;
                        }
                    }
                }
                break;
        }
        int i8 = i3 - this.__inputOffset;
        this.__inputOffset = i3;
        return i8;
    }

    private boolean __match(int i) {
        int _getNextOperator;
        char c;
        char c2;
        char c3;
        boolean z;
        char c4;
        boolean _isWordCharacter;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        int i2 = this.__inputOffset;
        boolean z5 = i2 < this.__endOffset;
        char c5 = z5 ? this.__input[i2] : __EOS;
        int length = this.__program.length;
        char c6 = c5;
        int i3 = i;
        int i4 = i2;
        boolean z6 = z5;
        int i5 = i4;
        while (i3 < length) {
            int _getNext = OpCode._getNext(this.__program, i3);
            char c7 = this.__program[i3];
            switch (c7) {
                case 0:
                case sym.BREAK:
                    this.__inputOffset = i5;
                    if (this.__inputOffset == this.__lastMatchInputEndOffset) {
                        return false;
                    }
                    return true;
                case 1:
                    if (i5 == this.__bol) {
                        if (this.__previousChar != 10) {
                            z3 = false;
                        }
                    } else if (!this.__multiline || ((!z6 && i5 >= this.__eol) || this.__input[i5 - 1] != 10)) {
                        z3 = false;
                    }
                    if (!z3) {
                        return false;
                    }
                    break;
                case 2:
                    if (i5 == this.__bol) {
                        if (this.__previousChar != 10) {
                            z2 = false;
                        }
                    } else if ((!z6 && i5 >= this.__eol) || this.__input[i5 - 1] != 10) {
                        z2 = false;
                    }
                    if (!z2) {
                        return false;
                    }
                    i3 = _getNext;
                    continue;
                    break;
                case 3:
                    if (!(i5 == this.__bol && this.__previousChar == 10)) {
                        return false;
                    }
                case 4:
                    if ((z6 || i5 < this.__eol) && c6 != 10) {
                        return false;
                    }
                    if (!this.__multiline && this.__eol - i5 > 1) {
                        return false;
                    }
                    break;
                case 5:
                    if ((z6 || i5 < this.__eol) && c6 != 10) {
                        return false;
                    }
                case 6:
                    if (((z6 || i5 < this.__eol) && c6 != 10) || this.__eol - i5 > 1) {
                        return false;
                    }
                case 7:
                    if ((!z6 && i5 >= this.__eol) || c6 == 10) {
                        return false;
                    }
                    int i6 = i5 + 1;
                    boolean z7 = i6 < this.__endOffset;
                    c6 = z7 ? this.__input[i6] : __EOS;
                    i3 = _getNext;
                    int i7 = i6;
                    z6 = z7;
                    i5 = i7;
                    continue;
                    break;
                case 8:
                    if (!z6 && i5 >= this.__eol) {
                        return false;
                    }
                    int i8 = i5 + 1;
                    boolean z8 = i8 < this.__endOffset;
                    c6 = z8 ? this.__input[i8] : __EOS;
                    i3 = _getNext;
                    int i9 = i8;
                    z6 = z8;
                    i5 = i9;
                    continue;
                    break;
                case 9:
                    int _getOperand = OpCode._getOperand(i3);
                    if (c6 == 65535 && z6) {
                        c6 = this.__input[i5];
                    }
                    if (c6 >= 256 || (this.__program[_getOperand + (c6 >> 4)] & (1 << (c6 & 15))) != 0) {
                        return false;
                    }
                    if (!z6 && i5 >= this.__eol) {
                        return false;
                    }
                    int i10 = i5 + 1;
                    boolean z9 = i10 < this.__endOffset;
                    c6 = z9 ? this.__input[i10] : __EOS;
                    i3 = _getNext;
                    int i11 = i10;
                    z6 = z9;
                    i5 = i11;
                    continue;
                    break;
                case 10:
                case 16:
                case sym.PIPE:
                    if (c7 == 10) {
                        char _getArg1 = OpCode._getArg1(this.__program, i3);
                        char _getArg2 = OpCode._getArg2(this.__program, i3);
                        _getNextOperator = OpCode._getNextOperator(i3) + 2;
                        c = _getArg1;
                        c2 = _getArg2;
                    } else if (c7 == 16) {
                        _getNextOperator = OpCode._getNextOperator(i3);
                        c = 0;
                        c2 = 65535;
                    } else {
                        _getNextOperator = OpCode._getNextOperator(i3);
                        c = 1;
                        c2 = 65535;
                    }
                    if (this.__program[_getNext] == 14) {
                        c3 = this.__program[OpCode._getOperand(_getNext) + 1];
                        z = false;
                    } else {
                        c3 = 65535;
                        z = true;
                    }
                    this.__inputOffset = i5;
                    if (!z4) {
                        int __repeat = __repeat(_getNextOperator, c2);
                        if (c >= __repeat || OpCode._opType[this.__program[_getNext]] != 4 || (this.__multiline && this.__program[_getNext] != 6)) {
                            c4 = c;
                        } else {
                            c4 = __repeat;
                        }
                        while (__repeat >= c4) {
                            if ((z || this.__inputOffset >= this.__endOffset || this.__input[this.__inputOffset] == c3) && __match(_getNext)) {
                                return true;
                            }
                            __repeat--;
                            this.__inputOffset = i5 + __repeat;
                        }
                    } else if (c > 0 && __repeat(_getNextOperator, c) < c) {
                        return false;
                    } else {
                        int i12 = c;
                        while (true) {
                            if (c2 >= i12 || (c2 == 65535 && i12 > 0)) {
                                if ((z || this.__inputOffset >= this.__endOffset || this.__input[this.__inputOffset] == c3) && __match(_getNext)) {
                                    return true;
                                }
                                this.__inputOffset = i5 + i12;
                                if (__repeat(_getNextOperator, 1) == 0) {
                                    return false;
                                }
                                i12++;
                                this.__inputOffset = i5 + i12;
                            }
                        }
                    }
                    return false;
                case sym.EVAL:
                    Perl5Repetition perl5Repetition = new Perl5Repetition();
                    perl5Repetition._lastRepetition = this.__currentRep;
                    this.__currentRep = perl5Repetition;
                    perl5Repetition._parenFloor = this.__lastParen;
                    perl5Repetition._numInstances = -1;
                    perl5Repetition._min = OpCode._getArg1(this.__program, i3);
                    perl5Repetition._max = OpCode._getArg2(this.__program, i3);
                    perl5Repetition._scan = OpCode._getNextOperator(i3) + 2;
                    perl5Repetition._next = _getNext;
                    perl5Repetition._minMod = z4;
                    perl5Repetition._lastLocation = -1;
                    this.__inputOffset = i5;
                    boolean __match = __match(OpCode._getPrevOperator(_getNext));
                    this.__currentRep = perl5Repetition._lastRepetition;
                    return __match;
                case 12:
                    if (this.__program[_getNext] != 12) {
                        i3 = OpCode._getNextOperator(i3);
                        continue;
                    } else {
                        int i13 = this.__lastParen;
                        int i14 = i3;
                        do {
                            this.__inputOffset = i5;
                            if (__match(OpCode._getNextOperator(i14))) {
                                return true;
                            }
                            int i15 = this.__lastParen;
                            while (i15 > i13) {
                                this.__endMatchOffsets[i15] = -1;
                                i15--;
                            }
                            this.__lastParen = i15;
                            i14 = OpCode._getNext(this.__program, i14);
                            if (i14 != -1) {
                            }
                            return false;
                        } while (this.__program[i14] == 12);
                        return false;
                    }
                case 14:
                    int _getOperand2 = OpCode._getOperand(i3);
                    int i16 = _getOperand2 + 1;
                    char c8 = this.__program[_getOperand2];
                    if (this.__program[i16] != c6 || this.__eol - i5 < c8) {
                        return false;
                    }
                    if (c8 > 1 && !__compare(this.__program, i16, this.__input, i5, c8)) {
                        return false;
                    }
                    int i17 = c8 + i5;
                    boolean z10 = i17 < this.__endOffset;
                    c6 = z10 ? this.__input[i17] : __EOS;
                    i3 = _getNext;
                    int i18 = i17;
                    z6 = z10;
                    i5 = i18;
                    continue;
                    break;
                case 18:
                    if (!z6 || !OpCode._isWordCharacter(c6)) {
                        return false;
                    }
                    int i19 = i5 + 1;
                    boolean z11 = i19 < this.__endOffset;
                    c6 = z11 ? this.__input[i19] : __EOS;
                    i3 = _getNext;
                    int i20 = i19;
                    z6 = z11;
                    i5 = i20;
                    continue;
                    break;
                case sym.DOUBLEPIPE:
                    if ((!z6 && i5 >= this.__eol) || OpCode._isWordCharacter(c6)) {
                        return false;
                    }
                    int i21 = i5 + 1;
                    boolean z12 = i21 < this.__endOffset;
                    c6 = z12 ? this.__input[i21] : __EOS;
                    i3 = _getNext;
                    int i22 = i21;
                    z6 = z12;
                    i5 = i22;
                    continue;
                    break;
                case 20:
                case sym.LOGICALNOR:
                    if (i5 == this.__bol) {
                        _isWordCharacter = OpCode._isWordCharacter(this.__previousChar);
                    } else {
                        _isWordCharacter = OpCode._isWordCharacter(this.__input[i5 - 1]);
                    }
                    if ((_isWordCharacter == OpCode._isWordCharacter(c6)) == (this.__program[i3] == 20)) {
                        return false;
                    }
                    break;
                case 22:
                    if ((!z6 && i5 >= this.__eol) || !Character.isWhitespace(c6)) {
                        return false;
                    }
                    int i23 = i5 + 1;
                    boolean z13 = i23 < this.__endOffset;
                    c6 = z13 ? this.__input[i23] : __EOS;
                    i3 = _getNext;
                    int i24 = i23;
                    z6 = z13;
                    i5 = i24;
                    continue;
                    break;
                case sym.LOGICALNAND:
                    if (!z6 || Character.isWhitespace(c6)) {
                        return false;
                    }
                    int i25 = i5 + 1;
                    boolean z14 = i25 < this.__endOffset;
                    c6 = z14 ? this.__input[i25] : __EOS;
                    i3 = _getNext;
                    int i26 = i25;
                    z6 = z14;
                    i5 = i26;
                    continue;
                    break;
                case 24:
                    if (!Character.isDigit(c6)) {
                        return false;
                    }
                    int i27 = i5 + 1;
                    boolean z15 = i27 < this.__endOffset;
                    c6 = z15 ? this.__input[i27] : __EOS;
                    i3 = _getNext;
                    int i28 = i27;
                    z6 = z15;
                    i5 = i28;
                    continue;
                case sym.LOGICALIMPLIES:
                    if ((!z6 && i5 >= this.__eol) || Character.isDigit(c6)) {
                        return false;
                    }
                    int i29 = i5 + 1;
                    boolean z16 = i29 < this.__endOffset;
                    c6 = z16 ? this.__input[i29] : __EOS;
                    i3 = _getNext;
                    int i30 = i29;
                    z6 = z16;
                    i5 = i30;
                    continue;
                    break;
                case 26:
                    char _getArg12 = OpCode._getArg1(this.__program, i3);
                    int i31 = this.__beginMatchOffsets[_getArg12];
                    if (i31 == -1 || this.__endMatchOffsets[_getArg12] == -1) {
                        return false;
                    }
                    if (i31 != this.__endMatchOffsets[_getArg12]) {
                        if (this.__input[i31] != c6) {
                            return false;
                        }
                        int i32 = this.__endMatchOffsets[_getArg12] - i31;
                        if (i5 + i32 > this.__eol) {
                            return false;
                        }
                        if (i32 > 1 && !__compare(this.__input, i31, this.__input, i5, i32)) {
                            return false;
                        }
                        int i33 = i32 + i5;
                        boolean z17 = i33 < this.__endOffset;
                        c6 = z17 ? this.__input[i33] : __EOS;
                        i3 = _getNext;
                        int i34 = i33;
                        z6 = z17;
                        i5 = i34;
                        continue;
                    }
                    break;
                case sym.UNDEF:
                    char _getArg13 = OpCode._getArg1(this.__program, i3);
                    this.__beginMatchOffsets[_getArg13] = i5;
                    if (_getArg13 > this.__expSize) {
                        this.__expSize = _getArg13;
                        i3 = _getNext;
                        continue;
                    }
                    break;
                case sym.MOD:
                    char _getArg14 = OpCode._getArg1(this.__program, i3);
                    this.__endMatchOffsets[_getArg14] = i5;
                    if (_getArg14 > this.__lastParen) {
                        this.__lastParen = _getArg14;
                        i3 = _getNext;
                        continue;
                    }
                    break;
                case sym.DIV:
                    z4 = true;
                    i3 = _getNext;
                    continue;
                case 30:
                    if (i5 != this.__bol) {
                        return true;
                    }
                    break;
                case sym.RETURN:
                    this.__inputOffset = i5;
                    if (!__match(OpCode._getNextOperator(i3))) {
                        return false;
                    }
                    break;
                case ' ':
                    this.__inputOffset = i5;
                    if (__match(OpCode._getNextOperator(i3))) {
                        return false;
                    }
                    break;
                case sym.CONSTRAINT_MARKER:
                    Perl5Repetition perl5Repetition2 = this.__currentRep;
                    int i35 = perl5Repetition2._numInstances + 1;
                    this.__inputOffset = i5;
                    if (i5 == perl5Repetition2._lastLocation) {
                        this.__currentRep = perl5Repetition2._lastRepetition;
                        int i36 = this.__currentRep._numInstances;
                        if (__match(perl5Repetition2._next)) {
                            return true;
                        }
                        this.__currentRep._numInstances = i36;
                        this.__currentRep = perl5Repetition2;
                        return false;
                    } else if (i35 < perl5Repetition2._min) {
                        perl5Repetition2._numInstances = i35;
                        perl5Repetition2._lastLocation = i5;
                        if (__match(perl5Repetition2._scan)) {
                            return true;
                        }
                        perl5Repetition2._numInstances = i35 - 1;
                        return false;
                    } else if (perl5Repetition2._minMod) {
                        this.__currentRep = perl5Repetition2._lastRepetition;
                        int i37 = this.__currentRep._numInstances;
                        if (__match(perl5Repetition2._next)) {
                            return true;
                        }
                        this.__currentRep._numInstances = i37;
                        this.__currentRep = perl5Repetition2;
                        if (i35 >= perl5Repetition2._max) {
                            return false;
                        }
                        this.__inputOffset = i5;
                        perl5Repetition2._numInstances = i35;
                        perl5Repetition2._lastLocation = i5;
                        if (__match(perl5Repetition2._scan)) {
                            return true;
                        }
                        perl5Repetition2._numInstances = i35 - 1;
                        return false;
                    } else {
                        if (i35 < perl5Repetition2._max) {
                            __pushState(perl5Repetition2._parenFloor);
                            perl5Repetition2._numInstances = i35;
                            perl5Repetition2._lastLocation = i5;
                            if (__match(perl5Repetition2._scan)) {
                                return true;
                            }
                            __popState();
                            this.__inputOffset = i5;
                        }
                        this.__currentRep = perl5Repetition2._lastRepetition;
                        int i38 = this.__currentRep._numInstances;
                        if (__match(perl5Repetition2._next)) {
                            return true;
                        }
                        perl5Repetition2._numInstances = i38;
                        this.__currentRep = perl5Repetition2;
                        perl5Repetition2._numInstances = i35 - 1;
                        return false;
                    }
                case sym.BOOLEANLIT:
                case sym.COMPARISON:
                    int _getOperand3 = OpCode._getOperand(i3);
                    if (c6 == 65535 && z6) {
                        c6 = this.__input[i5];
                    }
                    if (!__matchUnicodeClass(c6, this.__program, _getOperand3, c7)) {
                        return false;
                    }
                    if (!z6 && i5 >= this.__eol) {
                        return false;
                    }
                    int i39 = i5 + 1;
                    boolean z18 = i39 < this.__endOffset;
                    c6 = z18 ? this.__input[i39] : __EOS;
                    i3 = _getNext;
                    int i40 = i39;
                    z6 = z18;
                    i5 = i40;
                    continue;
                    break;
            }
            i3 = _getNext;
        }
        return false;
    }

    public void setMultiline(boolean z) {
        this.__multiline = z;
    }

    public boolean isMultiline() {
        return this.__multiline;
    }

    /* access modifiers changed from: package-private */
    public char[] _toLower(char[] cArr) {
        char[] cArr2 = new char[cArr.length];
        System.arraycopy(cArr, 0, cArr2, 0, cArr.length);
        for (int i = 0; i < cArr2.length; i++) {
            if (Character.isUpperCase(cArr2[i])) {
                cArr2[i] = Character.toLowerCase(cArr2[i]);
            }
        }
        return cArr2;
    }

    public boolean matchesPrefix(char[] cArr, Pattern pattern, int i) {
        char[] cArr2;
        Perl5Pattern perl5Pattern = (Perl5Pattern) pattern;
        this.__originalInput = cArr;
        if (perl5Pattern._isCaseInsensitive) {
            cArr2 = _toLower(cArr);
        } else {
            cArr2 = cArr;
        }
        __initInterpreterGlobals(perl5Pattern, cArr2, 0, cArr2.length, i);
        this.__lastSuccess = __tryExpression(i);
        this.__lastMatchResult = null;
        return this.__lastSuccess;
    }

    public boolean matchesPrefix(char[] cArr, Pattern pattern) {
        return matchesPrefix(cArr, pattern, 0);
    }

    public boolean matchesPrefix(String str, Pattern pattern) {
        return matchesPrefix(str.toCharArray(), pattern, 0);
    }

    public boolean matchesPrefix(PatternMatcherInput patternMatcherInput, Pattern pattern) {
        char[] cArr;
        Perl5Pattern perl5Pattern = (Perl5Pattern) pattern;
        this.__originalInput = patternMatcherInput._originalBuffer;
        if (perl5Pattern._isCaseInsensitive) {
            if (patternMatcherInput._toLowerBuffer == null) {
                patternMatcherInput._toLowerBuffer = _toLower(this.__originalInput);
            }
            cArr = patternMatcherInput._toLowerBuffer;
        } else {
            cArr = this.__originalInput;
        }
        __initInterpreterGlobals(perl5Pattern, cArr, patternMatcherInput._beginOffset, patternMatcherInput._endOffset, patternMatcherInput._currentOffset);
        this.__lastSuccess = __tryExpression(patternMatcherInput._currentOffset);
        this.__lastMatchResult = null;
        return this.__lastSuccess;
    }

    public boolean matches(char[] cArr, Pattern pattern) {
        char[] cArr2;
        boolean z;
        Perl5Pattern perl5Pattern = (Perl5Pattern) pattern;
        this.__originalInput = cArr;
        if (perl5Pattern._isCaseInsensitive) {
            cArr2 = _toLower(cArr);
        } else {
            cArr2 = cArr;
        }
        __initInterpreterGlobals(perl5Pattern, cArr2, 0, cArr2.length, 0);
        if (!__tryExpression(0) || this.__endMatchOffsets[0] != cArr2.length) {
            z = false;
        } else {
            z = true;
        }
        this.__lastSuccess = z;
        this.__lastMatchResult = null;
        return this.__lastSuccess;
    }

    public boolean matches(String str, Pattern pattern) {
        return matches(str.toCharArray(), pattern);
    }

    public boolean matches(PatternMatcherInput patternMatcherInput, Pattern pattern) {
        char[] cArr;
        Perl5Pattern perl5Pattern = (Perl5Pattern) pattern;
        this.__originalInput = patternMatcherInput._originalBuffer;
        if (perl5Pattern._isCaseInsensitive) {
            if (patternMatcherInput._toLowerBuffer == null) {
                patternMatcherInput._toLowerBuffer = _toLower(this.__originalInput);
            }
            cArr = patternMatcherInput._toLowerBuffer;
        } else {
            cArr = this.__originalInput;
        }
        __initInterpreterGlobals(perl5Pattern, cArr, patternMatcherInput._beginOffset, patternMatcherInput._endOffset, patternMatcherInput._beginOffset);
        this.__lastMatchResult = null;
        if (!__tryExpression(patternMatcherInput._beginOffset) || !(this.__endMatchOffsets[0] == patternMatcherInput._endOffset || patternMatcherInput.length() == 0 || patternMatcherInput._beginOffset == patternMatcherInput._endOffset)) {
            this.__lastSuccess = false;
            return false;
        }
        this.__lastSuccess = true;
        return true;
    }

    public boolean contains(String str, Pattern pattern) {
        return contains(str.toCharArray(), pattern);
    }

    public boolean contains(char[] cArr, Pattern pattern) {
        char[] cArr2;
        Perl5Pattern perl5Pattern = (Perl5Pattern) pattern;
        this.__originalInput = cArr;
        if (perl5Pattern._isCaseInsensitive) {
            cArr2 = _toLower(cArr);
        } else {
            cArr2 = cArr;
        }
        return __interpret(perl5Pattern, cArr2, 0, cArr2.length, 0);
    }

    public boolean contains(PatternMatcherInput patternMatcherInput, Pattern pattern) {
        char[] cArr;
        if (patternMatcherInput._currentOffset > patternMatcherInput._endOffset) {
            return false;
        }
        Perl5Pattern perl5Pattern = (Perl5Pattern) pattern;
        this.__originalInput = patternMatcherInput._originalBuffer;
        this.__originalInput = patternMatcherInput._originalBuffer;
        if (perl5Pattern._isCaseInsensitive) {
            if (patternMatcherInput._toLowerBuffer == null) {
                patternMatcherInput._toLowerBuffer = _toLower(this.__originalInput);
            }
            cArr = patternMatcherInput._toLowerBuffer;
        } else {
            cArr = this.__originalInput;
        }
        this.__lastMatchInputEndOffset = patternMatcherInput.getMatchEndOffset();
        boolean __interpret = __interpret(perl5Pattern, cArr, patternMatcherInput._beginOffset, patternMatcherInput._endOffset, patternMatcherInput._currentOffset);
        if (__interpret) {
            patternMatcherInput.setCurrentOffset(this.__endMatchOffsets[0]);
            patternMatcherInput.setMatchOffsets(this.__beginMatchOffsets[0], this.__endMatchOffsets[0]);
        } else {
            patternMatcherInput.setCurrentOffset(patternMatcherInput._endOffset + 1);
        }
        this.__lastMatchInputEndOffset = -100;
        return __interpret;
    }

    public MatchResult getMatch() {
        if (!this.__lastSuccess) {
            return null;
        }
        if (this.__lastMatchResult == null) {
            __setLastMatchResult();
        }
        return this.__lastMatchResult;
    }
}
