package org.apache.oro.text;

public class MalformedCachePatternException extends RuntimeException {
    public MalformedCachePatternException() {
    }

    public MalformedCachePatternException(String str) {
        super(str);
    }
}
