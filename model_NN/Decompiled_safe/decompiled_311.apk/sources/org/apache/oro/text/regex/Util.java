package org.apache.oro.text.regex;

import java.util.Vector;

public final class Util {
    public static final int SPLIT_ALL = 0;
    public static final int SUBSTITUTE_ALL = -1;

    private Util() {
    }

    public static void split(Vector vector, PatternMatcher patternMatcher, Pattern pattern, String str, int i) {
        PatternMatcherInput patternMatcherInput = new PatternMatcherInput(str);
        int i2 = 0;
        int i3 = i;
        while (true) {
            i3--;
            if (i3 == 0 || !patternMatcher.contains(patternMatcherInput, pattern)) {
                vector.addElement(str.substring(i2, str.length()));
            } else {
                MatchResult match = patternMatcher.getMatch();
                vector.addElement(str.substring(i2, match.beginOffset(0)));
                i2 = match.endOffset(0);
            }
        }
        vector.addElement(str.substring(i2, str.length()));
    }

    public static void split(Vector vector, PatternMatcher patternMatcher, Pattern pattern, String str) {
        split(vector, patternMatcher, pattern, str, 0);
    }

    public static Vector split(PatternMatcher patternMatcher, Pattern pattern, String str, int i) {
        Vector vector = new Vector(20);
        split(vector, patternMatcher, pattern, str, i);
        return vector;
    }

    public static Vector split(PatternMatcher patternMatcher, Pattern pattern, String str) {
        return split(patternMatcher, pattern, str, 0);
    }

    public static String substitute(PatternMatcher patternMatcher, Pattern pattern, Substitution substitution, String str, int i) {
        StringBuffer stringBuffer = new StringBuffer(str.length());
        if (substitute(stringBuffer, patternMatcher, pattern, substitution, new PatternMatcherInput(str), i) != 0) {
            return stringBuffer.toString();
        }
        return str;
    }

    public static String substitute(PatternMatcher patternMatcher, Pattern pattern, Substitution substitution, String str) {
        return substitute(patternMatcher, pattern, substitution, str, 1);
    }

    public static int substitute(StringBuffer stringBuffer, PatternMatcher patternMatcher, Pattern pattern, Substitution substitution, String str, int i) {
        return substitute(stringBuffer, patternMatcher, pattern, substitution, new PatternMatcherInput(str), i);
    }

    public static int substitute(StringBuffer stringBuffer, PatternMatcher patternMatcher, Pattern pattern, Substitution substitution, PatternMatcherInput patternMatcherInput, int i) {
        int i2 = 0;
        int beginOffset = patternMatcherInput.getBeginOffset();
        char[] buffer = patternMatcherInput.getBuffer();
        int i3 = i;
        while (i3 != 0 && patternMatcher.contains(patternMatcherInput, pattern)) {
            int i4 = i2 + 1;
            stringBuffer.append(buffer, beginOffset, patternMatcherInput.getMatchBeginOffset() - beginOffset);
            substitution.appendSubstitution(stringBuffer, patternMatcher.getMatch(), i4, patternMatcherInput, patternMatcher, pattern);
            beginOffset = patternMatcherInput.getMatchEndOffset();
            i3--;
            i2 = i4;
        }
        stringBuffer.append(buffer, beginOffset, patternMatcherInput.length() - beginOffset);
        return i2;
    }
}
