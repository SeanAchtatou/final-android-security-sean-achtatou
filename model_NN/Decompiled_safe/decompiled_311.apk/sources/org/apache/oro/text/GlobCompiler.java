package org.apache.oro.text;

import frink.parser.sym;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.Perl5Compiler;

public final class GlobCompiler implements PatternCompiler {
    public static final int CASE_INSENSITIVE_MASK = 1;
    public static final int DEFAULT_MASK = 0;
    public static final int QUESTION_MATCHES_ZERO_OR_ONE_MASK = 4;
    public static final int READ_ONLY_MASK = 8;
    public static final int STAR_CANNOT_MATCH_NULL_MASK = 2;
    private Perl5Compiler __perl5Compiler = new Perl5Compiler();

    private static boolean __isPerl5MetaCharacter(char c) {
        return c == '*' || c == '?' || c == '+' || c == '[' || c == ']' || c == '(' || c == ')' || c == '|' || c == '^' || c == '$' || c == '.' || c == '{' || c == '}' || c == '\\';
    }

    private static boolean __isGlobMetaCharacter(char c) {
        return c == '*' || c == '?' || c == '[' || c == ']';
    }

    public static String globToPerl5(char[] cArr, int i) {
        boolean z;
        StringBuffer stringBuffer = new StringBuffer(cArr.length * 2);
        boolean z2 = (i & 4) != 0;
        if ((i & 2) == 0) {
            z = false;
        } else {
            z = true;
        }
        int i2 = 0;
        boolean z3 = false;
        while (i2 < cArr.length) {
            switch (cArr[i2]) {
                case sym.IF:
                    if (!z3) {
                        if (!z) {
                            stringBuffer.append(".*");
                            break;
                        } else {
                            stringBuffer.append(".+");
                            break;
                        }
                    } else {
                        stringBuffer.append('*');
                        break;
                    }
                case sym.BLOCKBEGIN:
                    if (!z3) {
                        if (!z2) {
                            stringBuffer.append('.');
                            break;
                        } else {
                            stringBuffer.append(".?");
                            break;
                        }
                    } else {
                        stringBuffer.append('?');
                        break;
                    }
                case sym.TRANSFORMATIONS:
                    stringBuffer.append(cArr[i2]);
                    if (i2 + 1 < cArr.length) {
                        switch (cArr[i2 + 1]) {
                            case sym.BREAK:
                            case '^':
                                stringBuffer.append('^');
                                i2++;
                                z3 = true;
                                break;
                            case sym.FINALLY:
                                stringBuffer.append(']');
                                i2++;
                                z3 = true;
                                break;
                        }
                    }
                    z3 = true;
                    break;
                case sym.TRY:
                    stringBuffer.append('\\');
                    if (i2 != cArr.length - 1) {
                        if (!__isGlobMetaCharacter(cArr[i2 + 1])) {
                            stringBuffer.append('\\');
                            break;
                        } else {
                            i2++;
                            stringBuffer.append(cArr[i2]);
                            break;
                        }
                    } else {
                        stringBuffer.append('\\');
                        break;
                    }
                case sym.FINALLY:
                    stringBuffer.append(cArr[i2]);
                    z3 = false;
                    break;
                default:
                    if (!z3 && __isPerl5MetaCharacter(cArr[i2])) {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(cArr[i2]);
                    break;
            }
            i2++;
        }
        return stringBuffer.toString();
    }

    public Pattern compile(char[] cArr, int i) throws MalformedPatternException {
        int i2 = 0;
        if ((i & 1) != 0) {
            i2 = 0 | 1;
        }
        if ((i & 8) != 0) {
            i2 |= Perl5Compiler.READ_ONLY_MASK;
        }
        return this.__perl5Compiler.compile(globToPerl5(cArr, i), i2);
    }

    public Pattern compile(char[] cArr) throws MalformedPatternException {
        return compile(cArr, 0);
    }

    public Pattern compile(String str) throws MalformedPatternException {
        return compile(str.toCharArray(), 0);
    }

    public Pattern compile(String str, int i) throws MalformedPatternException {
        return compile(str.toCharArray(), i);
    }
}
