package org.apache.oro.text.regex;

import frink.parser.sym;
import frink.text.StringInterpolator;

public final class Perl5Debug {
    private Perl5Debug() {
    }

    public static String printProgram(Perl5Pattern perl5Pattern) {
        char[] cArr = perl5Pattern._program;
        StringBuffer stringBuffer = new StringBuffer();
        char c = 27;
        int i = 1;
        while (c != 0) {
            c = cArr[i];
            stringBuffer.append(i);
            _printOperator(cArr, i, stringBuffer);
            int _getNext = OpCode._getNext(cArr, i);
            int i2 = i + OpCode._operandLength[c];
            stringBuffer.append(new StringBuffer("(").append(_getNext).append(")").toString());
            i = i2 + 2;
            if (c == 9) {
                i += 16;
            } else if (c == '#' || c == '$') {
                while (cArr[i] != 0) {
                    if (cArr[i] == '%') {
                        i += 3;
                    } else {
                        i += 2;
                    }
                }
                i++;
            } else if (c == 14) {
                int i3 = i + 1;
                stringBuffer.append(" <");
                while (cArr[i3] != 65535) {
                    stringBuffer.append(cArr[i3]);
                    i3++;
                }
                stringBuffer.append(">");
                i = i3 + 1;
            }
            stringBuffer.append(10);
        }
        if (perl5Pattern._startString != null) {
            stringBuffer.append(new StringBuffer("start `").append(new String(perl5Pattern._startString)).append("' ").toString());
        }
        if (perl5Pattern._startClassOffset != -1) {
            stringBuffer.append("stclass `");
            _printOperator(cArr, perl5Pattern._startClassOffset, stringBuffer);
            stringBuffer.append("' ");
        }
        if ((perl5Pattern._anchor & 3) != 0) {
            stringBuffer.append("anchored ");
        }
        if ((perl5Pattern._anchor & 4) != 0) {
            stringBuffer.append("plus ");
        }
        if ((perl5Pattern._anchor & 8) != 0) {
            stringBuffer.append("implicit ");
        }
        if (perl5Pattern._mustString != null) {
            stringBuffer.append(new StringBuffer("must have \"").append(new String(perl5Pattern._mustString)).append("\" back ").append(perl5Pattern._back).append(" ").toString());
        }
        stringBuffer.append(new StringBuffer("minlen ").append(perl5Pattern._minLength).append(10).toString());
        return stringBuffer.toString();
    }

    static void _printOperator(char[] cArr, int i, StringBuffer stringBuffer) {
        String str = null;
        stringBuffer.append(":");
        switch (cArr[i]) {
            case 0:
                str = "END";
                break;
            case 1:
                str = "BOL";
                break;
            case 2:
                str = "MBOL";
                break;
            case 3:
                str = "SBOL";
                break;
            case 4:
                str = "EOL";
                break;
            case 5:
                str = "MEOL";
                break;
            case 6:
            case sym.THREEWAYCOMPARISON:
            case sym.NEW:
            case sym.QUESTIONMARK:
            case sym.COLON:
            default:
                stringBuffer.append("Operator is unrecognized.  Faulty expression code!");
                break;
            case 7:
                str = "ANY";
                break;
            case 8:
                str = "SANY";
                break;
            case 9:
                str = "ANYOF";
                break;
            case 10:
                stringBuffer.append("CURLY {");
                stringBuffer.append((int) OpCode._getArg1(cArr, i));
                stringBuffer.append(',');
                stringBuffer.append((int) OpCode._getArg2(cArr, i));
                stringBuffer.append((char) StringInterpolator.END_BRACKET);
                break;
            case sym.EVAL:
                stringBuffer.append("CURLYX {");
                stringBuffer.append((int) OpCode._getArg1(cArr, i));
                stringBuffer.append(',');
                stringBuffer.append((int) OpCode._getArg2(cArr, i));
                stringBuffer.append((char) StringInterpolator.END_BRACKET);
                break;
            case 12:
                str = "BRANCH";
                break;
            case sym.NOEVAL:
                str = "BACK";
                break;
            case 14:
                str = "EXACTLY";
                break;
            case sym.PERCENT:
                str = "NOTHING";
                break;
            case 16:
                str = "STAR";
                break;
            case sym.PIPE:
                str = "PLUS";
                break;
            case 18:
                str = "ALNUM";
                break;
            case sym.DOUBLEPIPE:
                str = "NALNUM";
                break;
            case 20:
                str = "BOUND";
                break;
            case sym.LOGICALNOR:
                str = "NBOUND";
                break;
            case 22:
                str = "SPACE";
                break;
            case sym.LOGICALNAND:
                str = "NSPACE";
                break;
            case 24:
                str = "DIGIT";
                break;
            case sym.LOGICALIMPLIES:
                str = "NDIGIT";
                break;
            case 26:
                stringBuffer.append("REF");
                stringBuffer.append((int) OpCode._getArg1(cArr, i));
                break;
            case sym.UNDEF:
                stringBuffer.append("OPEN");
                stringBuffer.append((int) OpCode._getArg1(cArr, i));
                break;
            case sym.MOD:
                stringBuffer.append("CLOSE");
                stringBuffer.append((int) OpCode._getArg1(cArr, i));
                break;
            case sym.DIV:
                str = "MINMOD";
                break;
            case 30:
                str = "GBOL";
                break;
            case sym.RETURN:
                str = "IFMATCH";
                break;
            case ' ':
                str = "UNLESSM";
                break;
            case sym.BREAK:
                str = "SUCCEED";
                break;
            case sym.CONSTRAINT_MARKER:
                str = "WHILEM";
                break;
            case sym.BOOLEANLIT:
                str = "ANYOFUN";
                break;
            case sym.COMPARISON:
                str = "NANYOFUN";
                break;
            case sym.LBRACKET:
                str = "ALPHA";
                break;
            case sym.RBRACKET:
                str = "BLANK";
                break;
            case '(':
                str = "CNTRL";
                break;
            case sym.DO:
                str = "GRAPH";
                break;
            case sym.IF:
                str = "LOWER";
                break;
            case sym.THEN:
                str = "PRINT";
                break;
            case sym.ELSE:
                str = "PUNCT";
                break;
            case sym.FOR:
                str = "UPPER";
                break;
            case sym.MULTIFOR:
                str = "XDIGIT";
                break;
            case '2':
                str = "ALNUMC";
                break;
            case sym.STEP:
                str = "ASCII";
                break;
        }
        if (str != null) {
            stringBuffer.append(str);
        }
    }
}
