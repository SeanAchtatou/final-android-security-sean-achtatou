package org.apache.oro.text.regex;

final class Perl5Repetition {
    int _lastLocation;
    Perl5Repetition _lastRepetition;
    int _max;
    int _min;
    boolean _minMod;
    int _next;
    int _numInstances;
    int _parenFloor;
    int _scan;

    Perl5Repetition() {
    }
}
