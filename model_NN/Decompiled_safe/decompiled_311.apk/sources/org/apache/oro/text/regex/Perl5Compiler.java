package org.apache.oro.text.regex;

import frink.parser.sym;
import frink.text.StringInterpolator;
import java.util.Hashtable;

public final class Perl5Compiler implements PatternCompiler {
    public static final int CASE_INSENSITIVE_MASK = 1;
    public static final int DEFAULT_MASK = 0;
    public static final int EXTENDED_MASK = 32;
    public static final int MULTILINE_MASK = 8;
    public static final int READ_ONLY_MASK = 32768;
    public static final int SINGLELINE_MASK = 16;
    private static final char __CASE_INSENSITIVE = '\u0001';
    private static final char __EXTENDED = ' ';
    private static final char __GLOBAL = '\u0002';
    private static final String __HEX_DIGIT = "0123456789abcdef0123456789ABCDEFx";
    private static final char __KEEP = '\u0004';
    private static final char __MULTILINE = '\b';
    private static final int __NONNULL = 1;
    private static final char __READ_ONLY = '耀';
    private static final int __SIMPLE = 2;
    private static final char __SINGLELINE = '\u0010';
    private static final int __SPSTART = 4;
    private static final int __TRYAGAIN = 8;
    private static final int __WORSTCASE = 0;
    private static final Hashtable __hashPOSIX = new Hashtable();
    private int __cost;
    private CharStringPointer __input;
    private char[] __modifierFlags = new char[1];
    private int __numParentheses;
    private char[] __program;
    private int __programSize;
    private boolean __sawBackreference;

    static {
        __hashPOSIX.put("alnum", new Character('2'));
        __hashPOSIX.put("word", new Character(18));
        __hashPOSIX.put("alpha", new Character('&'));
        __hashPOSIX.put("blank", new Character('\''));
        __hashPOSIX.put("cntrl", new Character('('));
        __hashPOSIX.put("digit", new Character(24));
        __hashPOSIX.put("graph", new Character(')'));
        __hashPOSIX.put("lower", new Character('*'));
        __hashPOSIX.put("print", new Character('+'));
        __hashPOSIX.put("punct", new Character(','));
        __hashPOSIX.put("space", new Character(22));
        __hashPOSIX.put("upper", new Character('-'));
        __hashPOSIX.put("xdigit", new Character('.'));
        __hashPOSIX.put("ascii", new Character('3'));
    }

    public static final String quotemeta(char[] cArr) {
        StringBuffer stringBuffer = new StringBuffer(cArr.length * 2);
        for (int i = 0; i < cArr.length; i++) {
            if (!OpCode._isWordCharacter(cArr[i])) {
                stringBuffer.append('\\');
            }
            stringBuffer.append(cArr[i]);
        }
        return stringBuffer.toString();
    }

    public static final String quotemeta(String str) {
        return quotemeta(str.toCharArray());
    }

    private static boolean __isSimpleRepetitionOp(char c) {
        return c == '*' || c == '+' || c == '?';
    }

    private static boolean __isComplexRepetitionOp(char[] cArr, int i) {
        if (i >= cArr.length || i < 0) {
            return false;
        }
        return cArr[i] == '*' || cArr[i] == '+' || cArr[i] == '?' || (cArr[i] == '{' && __parseRepetition(cArr, i));
    }

    private static boolean __parseRepetition(char[] cArr, int i) {
        if (cArr[i] != '{') {
            return false;
        }
        int i2 = i + 1;
        if (i2 >= cArr.length || !Character.isDigit(cArr[i2])) {
            return false;
        }
        while (i2 < cArr.length && Character.isDigit(cArr[i2])) {
            i2++;
        }
        if (i2 < cArr.length && cArr[i2] == ',') {
            i2++;
        }
        while (i2 < cArr.length && Character.isDigit(cArr[i2])) {
            i2++;
        }
        if (i2 >= cArr.length || cArr[i2] != '}') {
            return false;
        }
        return true;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static int __parseHex(char[] r6, int r7, int r8, int[] r9) {
        /*
            r5 = 0
            r9[r5] = r5
            r0 = r5
            r1 = r8
            r2 = r7
        L_0x0006:
            int r3 = r6.length
            if (r2 >= r3) goto L_0x0018
            int r3 = r1 + -1
            if (r1 <= 0) goto L_0x0018
            java.lang.String r1 = "0123456789abcdef0123456789ABCDEFx"
            char r4 = r6[r2]
            int r1 = r1.indexOf(r4)
            r4 = -1
            if (r1 != r4) goto L_0x0019
        L_0x0018:
            return r0
        L_0x0019:
            int r0 = r0 << 4
            r1 = r1 & 15
            r0 = r0 | r1
            int r1 = r2 + 1
            r2 = r9[r5]
            int r2 = r2 + 1
            r9[r5] = r2
            r2 = r1
            r1 = r3
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Compiler.__parseHex(char[], int, int, int[]):int");
    }

    private static int __parseOctal(char[] cArr, int i, int i2, int[] iArr) {
        iArr[0] = 0;
        int i3 = 0;
        int i4 = i2;
        int i5 = i;
        while (i5 < cArr.length && i4 > 0 && cArr[i5] >= '0' && cArr[i5] <= '7') {
            i3 = (i3 << 3) | (cArr[i5] - '0');
            i4--;
            i5++;
            iArr[0] = iArr[0] + 1;
        }
        return i3;
    }

    private static void __setModifierFlag(char[] cArr, char c) {
        switch (c) {
            case 'g':
                cArr[0] = (char) (cArr[0] | __GLOBAL);
                return;
            case 'i':
                cArr[0] = (char) (cArr[0] | __CASE_INSENSITIVE);
                return;
            case 'm':
                cArr[0] = (char) (cArr[0] | __MULTILINE);
                return;
            case 'o':
                cArr[0] = (char) (cArr[0] | __KEEP);
                return;
            case 's':
                cArr[0] = (char) (cArr[0] | __SINGLELINE);
                return;
            case 'x':
                cArr[0] = (char) (cArr[0] | __EXTENDED);
                return;
            default:
                return;
        }
    }

    private void __emitCode(char c) {
        if (this.__program != null) {
            this.__program[this.__programSize] = c;
        }
        this.__programSize++;
    }

    private int __emitNode(char c) {
        int i = this.__programSize;
        if (this.__program == null) {
            this.__programSize += 2;
        } else {
            char[] cArr = this.__program;
            int i2 = this.__programSize;
            this.__programSize = i2 + 1;
            cArr[i2] = c;
            char[] cArr2 = this.__program;
            int i3 = this.__programSize;
            this.__programSize = i3 + 1;
            cArr2[i3] = 0;
        }
        return i;
    }

    private int __emitArgNode(char c, char c2) {
        int i = this.__programSize;
        if (this.__program == null) {
            this.__programSize += 3;
        } else {
            char[] cArr = this.__program;
            int i2 = this.__programSize;
            this.__programSize = i2 + 1;
            cArr[i2] = c;
            char[] cArr2 = this.__program;
            int i3 = this.__programSize;
            this.__programSize = i3 + 1;
            cArr2[i3] = 0;
            char[] cArr3 = this.__program;
            int i4 = this.__programSize;
            this.__programSize = i4 + 1;
            cArr3[i4] = c2;
        }
        return i;
    }

    private void __programInsertOperator(char c, int i) {
        int i2 = OpCode._opType[c] == 10 ? 2 : 0;
        if (this.__program == null) {
            this.__programSize = i2 + 2 + this.__programSize;
            return;
        }
        int i3 = this.__programSize;
        this.__programSize += i2 + 2;
        int i4 = i3;
        int i5 = this.__programSize;
        while (i4 > i) {
            i4--;
            i5--;
            this.__program[i5] = this.__program[i4];
        }
        int i6 = i + 1;
        this.__program[i] = c;
        int i7 = i6 + 1;
        this.__program[i6] = 0;
        while (true) {
            int i8 = i7;
            int i9 = i2 - 1;
            if (i2 > 0) {
                i7 = i8 + 1;
                this.__program[i8] = 0;
                i2 = i9;
            } else {
                return;
            }
        }
    }

    private void __programAddTail(int i, int i2) {
        int i3;
        if (this.__program != null && i != -1) {
            int i4 = i;
            while (true) {
                int _getNext = OpCode._getNext(this.__program, i4);
                if (_getNext == -1) {
                    break;
                }
                i4 = _getNext;
            }
            if (this.__program[i4] == 13) {
                i3 = i4 - i2;
            } else {
                i3 = i2 - i4;
            }
            this.__program[i4 + 1] = (char) i3;
        }
    }

    private void __programAddOperatorTail(int i, int i2) {
        if (this.__program != null && i != -1 && OpCode._opType[this.__program[i]] == 12) {
            __programAddTail(OpCode._getNextOperator(i), i2);
        }
    }

    private char __getNextChar() {
        char _postIncrement = this.__input._postIncrement();
        while (true) {
            char _getValue = this.__input._getValue();
            if (_getValue != '(' || this.__input._getValueRelative(1) != '?' || this.__input._getValueRelative(2) != '#') {
                if ((this.__modifierFlags[0] & __EXTENDED) != 0) {
                    if (!Character.isWhitespace(_getValue)) {
                        if (_getValue != '#') {
                            break;
                        }
                        while (_getValue != 65535 && _getValue != 10) {
                            _getValue = this.__input._increment();
                        }
                        this.__input._increment();
                    } else {
                        this.__input._increment();
                    }
                } else {
                    break;
                }
            } else {
                while (_getValue != 65535 && _getValue != ')') {
                    _getValue = this.__input._increment();
                }
                this.__input._increment();
            }
        }
        return _postIncrement;
    }

    private int __parseAlternation(int[] iArr) throws MalformedPatternException {
        iArr[0] = 0;
        int __emitNode = __emitNode(12);
        if (this.__input._getOffset() == 0) {
            this.__input._setOffset(-1);
            __getNextChar();
        } else {
            this.__input._decrement();
            __getNextChar();
        }
        char _getValue = this.__input._getValue();
        boolean z = false;
        int i = -1;
        while (_getValue != 65535 && _getValue != '|' && _getValue != ')') {
            boolean z2 = z & true;
            int __parseBranch = __parseBranch(iArr);
            if (__parseBranch != -1) {
                boolean z3 = z2 & true;
                iArr[0] = iArr[0] | 0;
                if (i == -1) {
                    boolean z4 = z2 & true;
                    iArr[0] = iArr[0] | 0;
                } else {
                    this.__cost++;
                    __programAddTail(i, __parseBranch);
                }
                i = __parseBranch;
                z = z2;
                _getValue = this.__input._getValue();
            } else if (!z2 || !true) {
                return -1;
            } else {
                z = z2;
                _getValue = this.__input._getValue();
            }
        }
        if (i != -1) {
            return __emitNode;
        }
        __emitNode(15);
        return __emitNode;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0309  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0441  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int __parseAtom(int[] r12) throws org.apache.oro.text.regex.MalformedPatternException {
        /*
            r11 = this;
            r0 = 1
            int[] r0 = new int[r0]
            r1 = 0
            r2 = 0
            r12[r1] = r2
            r1 = 0
            r2 = -1
        L_0x0009:
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            char r3 = r3._getValue()
            switch(r3) {
                case 35: goto L_0x02b5;
                case 36: goto L_0x007b;
                case 40: goto L_0x00dd;
                case 41: goto L_0x00ff;
                case 42: goto L_0x0130;
                case 43: goto L_0x0130;
                case 46: goto L_0x00a4;
                case 63: goto L_0x0130;
                case 91: goto L_0x00cb;
                case 92: goto L_0x0138;
                case 94: goto L_0x0054;
                case 124: goto L_0x00ff;
                default: goto L_0x0012;
            }
        L_0x0012:
            org.apache.oro.text.regex.CharStringPointer r0 = r11.__input
            r0._increment()
            r0 = 1
            r1 = r0
            r0 = r2
        L_0x001a:
            if (r1 == 0) goto L_0x00f0
            r0 = 14
            int r0 = r11.__emitNode(r0)
            r1 = 65535(0xffff, float:9.1834E-41)
            r11.__emitCode(r1)
            r1 = 0
            org.apache.oro.text.regex.CharStringPointer r2 = r11.__input
            int r2 = r2._getOffset()
            r3 = 1
            int r2 = r2 - r3
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            int r3 = r3._getLength()
            r10 = r2
            r2 = r1
            r1 = r10
        L_0x003a:
            r4 = 127(0x7f, float:1.78E-43)
            if (r2 >= r4) goto L_0x0040
            if (r1 < r3) goto L_0x02e0
        L_0x0040:
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            r4 = 1
            int r1 = r1 - r4
            r3._setOffset(r1)
            r11.__getNextChar()
            if (r2 >= 0) goto L_0x0451
            org.apache.oro.text.regex.MalformedPatternException r0 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r1 = "Unexpected compilation failure.  Please report this bug!"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            r11.__getNextChar()
            char[] r0 = r11.__modifierFlags
            r2 = 0
            char r0 = r0[r2]
            r0 = r0 & 8
            if (r0 == 0) goto L_0x0066
            r0 = 2
            int r0 = r11.__emitNode(r0)
            goto L_0x001a
        L_0x0066:
            char[] r0 = r11.__modifierFlags
            r2 = 0
            char r0 = r0[r2]
            r0 = r0 & 16
            if (r0 == 0) goto L_0x0075
            r0 = 3
            int r0 = r11.__emitNode(r0)
            goto L_0x001a
        L_0x0075:
            r0 = 1
            int r0 = r11.__emitNode(r0)
            goto L_0x001a
        L_0x007b:
            r11.__getNextChar()
            char[] r0 = r11.__modifierFlags
            r2 = 0
            char r0 = r0[r2]
            r0 = r0 & 8
            if (r0 == 0) goto L_0x008d
            r0 = 5
            int r0 = r11.__emitNode(r0)
            goto L_0x001a
        L_0x008d:
            char[] r0 = r11.__modifierFlags
            r2 = 0
            char r0 = r0[r2]
            r0 = r0 & 16
            if (r0 == 0) goto L_0x009d
            r0 = 6
            int r0 = r11.__emitNode(r0)
            goto L_0x001a
        L_0x009d:
            r0 = 4
            int r0 = r11.__emitNode(r0)
            goto L_0x001a
        L_0x00a4:
            r11.__getNextChar()
            char[] r0 = r11.__modifierFlags
            r2 = 0
            char r0 = r0[r2]
            r0 = r0 & 16
            if (r0 == 0) goto L_0x00c5
            r0 = 8
            int r0 = r11.__emitNode(r0)
        L_0x00b6:
            int r2 = r11.__cost
            int r2 = r2 + 1
            r11.__cost = r2
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            goto L_0x001a
        L_0x00c5:
            r0 = 7
            int r0 = r11.__emitNode(r0)
            goto L_0x00b6
        L_0x00cb:
            org.apache.oro.text.regex.CharStringPointer r0 = r11.__input
            r0._increment()
            int r0 = r11.__parseUnicodeClass()
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            goto L_0x001a
        L_0x00dd:
            r11.__getNextChar()
            r2 = 1
            int r2 = r11.__parseExpression(r2, r0)
            r3 = -1
            if (r2 != r3) goto L_0x00f1
            r3 = 0
            r3 = r0[r3]
            r3 = r3 & 8
            if (r3 != 0) goto L_0x0009
            r0 = -1
        L_0x00f0:
            return r0
        L_0x00f1:
            r3 = 0
            r4 = r12[r3]
            r5 = 0
            r0 = r0[r5]
            r0 = r0 & 5
            r0 = r0 | r4
            r12[r3] = r0
            r0 = r2
            goto L_0x001a
        L_0x00ff:
            r1 = 0
            r0 = r0[r1]
            r0 = r0 & 8
            if (r0 == 0) goto L_0x010f
            r0 = 0
            r1 = r12[r0]
            r1 = r1 | 8
            r12[r0] = r1
            r0 = -1
            goto L_0x00f0
        L_0x010f:
            org.apache.oro.text.regex.MalformedPatternException r0 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "Error in expression at "
            r1.<init>(r2)
            org.apache.oro.text.regex.CharStringPointer r2 = r11.__input
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            int r3 = r3._getOffset()
            java.lang.String r2 = r2._toString(r3)
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0130:
            org.apache.oro.text.regex.MalformedPatternException r0 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r1 = "?+* follows nothing in expression"
            r0.<init>(r1)
            throw r0
        L_0x0138:
            org.apache.oro.text.regex.CharStringPointer r0 = r11.__input
            char r0 = r0._increment()
            switch(r0) {
                case 0: goto L_0x02a5;
                case 48: goto L_0x020a;
                case 49: goto L_0x020f;
                case 50: goto L_0x020f;
                case 51: goto L_0x020f;
                case 52: goto L_0x020f;
                case 53: goto L_0x020f;
                case 54: goto L_0x020f;
                case 55: goto L_0x020f;
                case 56: goto L_0x020f;
                case 57: goto L_0x020f;
                case 65: goto L_0x0146;
                case 66: goto L_0x01b0;
                case 68: goto L_0x01f8;
                case 71: goto L_0x0157;
                case 83: goto L_0x01d4;
                case 87: goto L_0x018c;
                case 90: goto L_0x0169;
                case 97: goto L_0x020a;
                case 98: goto L_0x019e;
                case 99: goto L_0x020a;
                case 100: goto L_0x01e6;
                case 101: goto L_0x020a;
                case 102: goto L_0x020a;
                case 110: goto L_0x020a;
                case 114: goto L_0x020a;
                case 115: goto L_0x01c2;
                case 116: goto L_0x020a;
                case 119: goto L_0x017a;
                case 120: goto L_0x020a;
                case 65535: goto L_0x02a5;
                default: goto L_0x0141;
            }
        L_0x0141:
            r0 = 1
            r1 = r0
            r0 = r2
            goto L_0x001a
        L_0x0146:
            r0 = 3
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 2
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x0157:
            r0 = 30
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 2
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x0169:
            r0 = 6
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 2
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x017a:
            r0 = 18
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x018c:
            r0 = 19
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x019e:
            r0 = 20
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 2
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x01b0:
            r0 = 21
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 2
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x01c2:
            r0 = 22
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x01d4:
            r0 = 23
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x01e6:
            r0 = 24
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x01f8:
            r0 = 25
            int r0 = r11.__emitNode(r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 3
            r12[r2] = r3
            r11.__getNextChar()
            goto L_0x001a
        L_0x020a:
            r0 = 1
            r1 = r0
            r0 = r2
            goto L_0x001a
        L_0x020f:
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r3 = 10
            r0.<init>(r3)
            r3 = 0
            org.apache.oro.text.regex.CharStringPointer r4 = r11.__input
            char r4 = r4._getValueRelative(r3)
        L_0x021d:
            boolean r5 = java.lang.Character.isDigit(r4)
            if (r5 != 0) goto L_0x0238
            java.lang.String r0 = r0.toString()     // Catch:{ NumberFormatException -> 0x0244 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0244 }
            r3 = 9
            if (r0 <= r3) goto L_0x025e
            int r3 = r11.__numParentheses
            if (r0 < r3) goto L_0x025e
            r0 = 1
            r1 = r0
            r0 = r2
            goto L_0x001a
        L_0x0238:
            r0.append(r4)
            int r3 = r3 + 1
            org.apache.oro.text.regex.CharStringPointer r4 = r11.__input
            char r4 = r4._getValueRelative(r3)
            goto L_0x021d
        L_0x0244:
            r0 = move-exception
            org.apache.oro.text.regex.MalformedPatternException r1 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "Unexpected number format exception.  Please report this bug.NumberFormatException message: "
            r2.<init>(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x025e:
            int r2 = r11.__numParentheses
            if (r0 < r2) goto L_0x0277
            org.apache.oro.text.regex.MalformedPatternException r1 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "Invalid backreference: \\"
            r2.<init>(r3)
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0277:
            r2 = 1
            r11.__sawBackreference = r2
            r2 = 26
            char r0 = (char) r0
            int r0 = r11.__emitArgNode(r2, r0)
            r2 = 0
            r3 = r12[r2]
            r3 = r3 | 1
            r12[r2] = r3
            org.apache.oro.text.regex.CharStringPointer r2 = r11.__input
            char r2 = r2._getValue()
        L_0x028e:
            boolean r2 = java.lang.Character.isDigit(r2)
            if (r2 != 0) goto L_0x029e
            org.apache.oro.text.regex.CharStringPointer r2 = r11.__input
            r2._decrement()
            r11.__getNextChar()
            goto L_0x001a
        L_0x029e:
            org.apache.oro.text.regex.CharStringPointer r2 = r11.__input
            char r2 = r2._increment()
            goto L_0x028e
        L_0x02a5:
            org.apache.oro.text.regex.CharStringPointer r0 = r11.__input
            boolean r0 = r0._isAtEnd()
            if (r0 == 0) goto L_0x0141
            org.apache.oro.text.regex.MalformedPatternException r0 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r1 = "Trailing \\ in expression."
            r0.<init>(r1)
            throw r0
        L_0x02b5:
            char[] r3 = r11.__modifierFlags
            r4 = 0
            char r3 = r3[r4]
            r3 = r3 & 32
            if (r3 == 0) goto L_0x0012
        L_0x02be:
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            boolean r3 = r3._isAtEnd()
            if (r3 != 0) goto L_0x02d0
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            char r3 = r3._getValue()
            r4 = 10
            if (r3 != r4) goto L_0x02da
        L_0x02d0:
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            boolean r3 = r3._isAtEnd()
            if (r3 != 0) goto L_0x0012
            goto L_0x0009
        L_0x02da:
            org.apache.oro.text.regex.CharStringPointer r3 = r11.__input
            r3._increment()
            goto L_0x02be
        L_0x02e0:
            org.apache.oro.text.regex.CharStringPointer r4 = r11.__input
            char r4 = r4._getValue(r1)
            switch(r4) {
                case 9: goto L_0x0479;
                case 10: goto L_0x0479;
                case 11: goto L_0x0479;
                case 12: goto L_0x0479;
                case 13: goto L_0x0479;
                case 32: goto L_0x0479;
                case 35: goto L_0x0422;
                case 36: goto L_0x0040;
                case 40: goto L_0x0040;
                case 41: goto L_0x0040;
                case 46: goto L_0x0040;
                case 91: goto L_0x0040;
                case 92: goto L_0x031e;
                case 94: goto L_0x0040;
                case 124: goto L_0x0040;
                default: goto L_0x02e9;
            }
        L_0x02e9:
            r4 = r1
        L_0x02ea:
            org.apache.oro.text.regex.CharStringPointer r5 = r11.__input
            int r6 = r4 + 1
            char r4 = r5._getValue(r4)
            r5 = r4
            r4 = r6
        L_0x02f4:
            char[] r6 = r11.__modifierFlags
            r7 = 0
            char r6 = r6[r7]
            r6 = r6 & 1
            if (r6 == 0) goto L_0x0307
            boolean r6 = java.lang.Character.isUpperCase(r5)
            if (r6 == 0) goto L_0x0307
            char r5 = java.lang.Character.toLowerCase(r5)
        L_0x0307:
            if (r4 >= r3) goto L_0x044c
            org.apache.oro.text.regex.CharStringPointer r6 = r11.__input
            char[] r6 = r6._array
            boolean r6 = __isComplexRepetitionOp(r6, r4)
            if (r6 == 0) goto L_0x044c
            if (r2 > 0) goto L_0x0040
            int r1 = r2 + 1
            r11.__emitCode(r5)
            r2 = r1
            r1 = r4
            goto L_0x0040
        L_0x031e:
            org.apache.oro.text.regex.CharStringPointer r4 = r11.__input
            int r5 = r1 + 1
            char r4 = r4._getValue(r5)
            switch(r4) {
                case 0: goto L_0x0418;
                case 48: goto L_0x0398;
                case 49: goto L_0x0398;
                case 50: goto L_0x0398;
                case 51: goto L_0x0398;
                case 52: goto L_0x0398;
                case 53: goto L_0x0398;
                case 54: goto L_0x0398;
                case 55: goto L_0x0398;
                case 56: goto L_0x0398;
                case 57: goto L_0x0398;
                case 65: goto L_0x0334;
                case 66: goto L_0x0334;
                case 68: goto L_0x0334;
                case 71: goto L_0x0334;
                case 83: goto L_0x0334;
                case 87: goto L_0x0334;
                case 90: goto L_0x0334;
                case 97: goto L_0x0360;
                case 98: goto L_0x0334;
                case 99: goto L_0x037d;
                case 100: goto L_0x0334;
                case 101: goto L_0x0358;
                case 102: goto L_0x0350;
                case 110: goto L_0x0338;
                case 114: goto L_0x0340;
                case 115: goto L_0x0334;
                case 116: goto L_0x0348;
                case 119: goto L_0x0334;
                case 120: goto L_0x0367;
                case 65535: goto L_0x0418;
                default: goto L_0x0329;
            }
        L_0x0329:
            org.apache.oro.text.regex.CharStringPointer r4 = r11.__input
            int r6 = r5 + 1
            char r4 = r4._getValue(r5)
            r5 = r4
            r4 = r6
            goto L_0x02f4
        L_0x0334:
            int r1 = r5 + -1
            goto L_0x0040
        L_0x0338:
            r4 = 10
            int r5 = r5 + 1
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x02f4
        L_0x0340:
            r4 = 13
            int r5 = r5 + 1
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x02f4
        L_0x0348:
            r4 = 9
            int r5 = r5 + 1
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x02f4
        L_0x0350:
            r4 = 12
            int r5 = r5 + 1
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x02f4
        L_0x0358:
            r4 = 27
            int r5 = r5 + 1
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x02f4
        L_0x0360:
            r4 = 7
            int r5 = r5 + 1
            r10 = r5
            r5 = r4
            r4 = r10
            goto L_0x02f4
        L_0x0367:
            r4 = 1
            int[] r4 = new int[r4]
            org.apache.oro.text.regex.CharStringPointer r6 = r11.__input
            char[] r6 = r6._array
            int r5 = r5 + 1
            r7 = 2
            int r6 = __parseHex(r6, r5, r7, r4)
            char r6 = (char) r6
            r7 = 0
            r4 = r4[r7]
            int r4 = r4 + r5
            r5 = r6
            goto L_0x02f4
        L_0x037d:
            int r4 = r5 + 1
            org.apache.oro.text.regex.CharStringPointer r5 = r11.__input
            int r6 = r4 + 1
            char r4 = r5._getValue(r4)
            boolean r5 = java.lang.Character.isLowerCase(r4)
            if (r5 == 0) goto L_0x0391
            char r4 = java.lang.Character.toUpperCase(r4)
        L_0x0391:
            r4 = r4 ^ 64
            char r4 = (char) r4
            r5 = r4
            r4 = r6
            goto L_0x02f4
        L_0x0398:
            r4 = 0
            org.apache.oro.text.regex.CharStringPointer r6 = r11.__input
            char r6 = r6._getValue(r5)
            r7 = 48
            if (r6 != r7) goto L_0x03a4
            r4 = 1
        L_0x03a4:
            org.apache.oro.text.regex.CharStringPointer r6 = r11.__input
            int r7 = r5 + 1
            char r6 = r6._getValue(r7)
            boolean r6 = java.lang.Character.isDigit(r6)
            if (r6 == 0) goto L_0x03d6
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r7 = 10
            r6.<init>(r7)
            org.apache.oro.text.regex.CharStringPointer r7 = r11.__input
            char r7 = r7._getValue(r5)
            r8 = r7
            r7 = r5
        L_0x03c1:
            boolean r9 = java.lang.Character.isDigit(r8)
            if (r9 != 0) goto L_0x03ec
            java.lang.String r6 = r6.toString()     // Catch:{ NumberFormatException -> 0x03f8 }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ NumberFormatException -> 0x03f8 }
            if (r4 != 0) goto L_0x03d6
            int r4 = r11.__numParentheses
            if (r6 >= r4) goto L_0x0412
            r4 = 0
        L_0x03d6:
            if (r4 == 0) goto L_0x0414
            r4 = 1
            int[] r4 = new int[r4]
            org.apache.oro.text.regex.CharStringPointer r6 = r11.__input
            char[] r6 = r6._array
            r7 = 3
            int r6 = __parseOctal(r6, r5, r7, r4)
            char r6 = (char) r6
            r7 = 0
            r4 = r4[r7]
            int r4 = r4 + r5
            r5 = r6
            goto L_0x02f4
        L_0x03ec:
            r6.append(r8)
            int r7 = r7 + 1
            org.apache.oro.text.regex.CharStringPointer r8 = r11.__input
            char r8 = r8._getValue(r7)
            goto L_0x03c1
        L_0x03f8:
            r0 = move-exception
            org.apache.oro.text.regex.MalformedPatternException r1 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "Unexpected number format exception.  Please report this bug.NumberFormatException message: "
            r2.<init>(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0412:
            r4 = 1
            goto L_0x03d6
        L_0x0414:
            int r1 = r5 + -1
            goto L_0x0040
        L_0x0418:
            if (r5 < r3) goto L_0x0329
            org.apache.oro.text.regex.MalformedPatternException r0 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r1 = "Trailing \\ in expression."
            r0.<init>(r1)
            throw r0
        L_0x0422:
            char[] r4 = r11.__modifierFlags
            r5 = 0
            char r4 = r4[r5]
            r4 = r4 & 32
            if (r4 == 0) goto L_0x0479
            r4 = r1
        L_0x042c:
            if (r4 >= r3) goto L_0x0438
            org.apache.oro.text.regex.CharStringPointer r5 = r11.__input
            char r5 = r5._getValue(r4)
            r6 = 10
            if (r5 != r6) goto L_0x0449
        L_0x0438:
            char[] r5 = r11.__modifierFlags
            r6 = 0
            char r5 = r5[r6]
            r5 = r5 & 32
            if (r5 == 0) goto L_0x02ea
            int r1 = r4 + 1
            int r2 = r2 + -1
        L_0x0445:
            int r2 = r2 + 1
            goto L_0x003a
        L_0x0449:
            int r4 = r4 + 1
            goto L_0x042c
        L_0x044c:
            r11.__emitCode(r5)
            r1 = r4
            goto L_0x0445
        L_0x0451:
            if (r2 <= 0) goto L_0x045a
            r1 = 0
            r3 = r12[r1]
            r3 = r3 | 1
            r12[r1] = r3
        L_0x045a:
            r1 = 1
            if (r2 != r1) goto L_0x0464
            r1 = 0
            r3 = r12[r1]
            r3 = r3 | 2
            r12[r1] = r3
        L_0x0464:
            char[] r1 = r11.__program
            if (r1 == 0) goto L_0x0471
            char[] r1 = r11.__program
            int r3 = org.apache.oro.text.regex.OpCode._getOperand(r0)
            char r2 = (char) r2
            r1[r3] = r2
        L_0x0471:
            r1 = 65535(0xffff, float:9.1834E-41)
            r11.__emitCode(r1)
            goto L_0x00f0
        L_0x0479:
            r4 = r1
            goto L_0x0438
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Compiler.__parseAtom(int[]):int");
    }

    private int __parseUnicodeClass() throws MalformedPatternException {
        int __emitNode;
        boolean z;
        boolean z2;
        char c;
        char c2;
        char c3;
        char c4;
        boolean z3;
        char c5;
        char c6;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        int[] iArr = new int[1];
        boolean[] zArr = new boolean[1];
        if (this.__input._getValue() == '^') {
            __emitNode = __emitNode(StringInterpolator.INDICATOR);
            this.__input._increment();
        } else {
            __emitNode = __emitNode('#');
        }
        char _getValue = this.__input._getValue();
        if (_getValue == ']' || _getValue == '-') {
            c = _getValue;
            z2 = true;
            z = false;
            c2 = 65535;
        } else {
            c = _getValue;
            z2 = false;
            z = false;
            c2 = 65535;
        }
        while (true) {
            if ((!this.__input._isAtEnd() && (c = this.__input._getValue()) != ']') || z2) {
                this.__input._increment();
                if (c == '\\' || c == '[') {
                    if (c == '\\') {
                        c3 = this.__input._postIncrement();
                        z7 = false;
                    } else {
                        c3 = __parsePOSIX(zArr);
                        if (c3 != 0) {
                            z7 = true;
                        } else {
                            c3 = c;
                            z7 = false;
                        }
                    }
                    if (!z7) {
                        switch (c3) {
                            case sym.QUESTIONMARK:
                            case sym.COLON:
                            case '2':
                            case sym.STEP:
                            case sym.CONDITION_MARKER:
                            case sym.DEFINITION:
                            case sym.EXCLAMATION:
                            case sym.PREFIXDEFINITION:
                            case sym.DOUBLEDEFINITION:
                            case sym.BASEDIMENSIONDEF:
                                c3 = (char) __parseOctal(this.__input._array, this.__input._getOffset() - 1, 3, iArr);
                                this.__input._increment(iArr[0] - 1);
                                boolean z8 = z7;
                                c4 = c2;
                                z3 = z8;
                                break;
                            case sym.SHEBANG:
                                c4 = 65535;
                                c3 = 25;
                                z3 = true;
                                break;
                            case sym.SQUARE:
                                c4 = 65535;
                                c3 = 23;
                                z3 = true;
                                break;
                            case sym.SQUARED:
                                c4 = 65535;
                                c3 = 19;
                                z3 = true;
                                break;
                            case 'a':
                                c3 = 7;
                                boolean z9 = z7;
                                c4 = c2;
                                z3 = z9;
                                break;
                            case 'b':
                                c3 = __MULTILINE;
                                boolean z10 = z7;
                                c4 = c2;
                                z3 = z10;
                                break;
                            case 'c':
                                char _postIncrement = this.__input._postIncrement();
                                if (Character.isLowerCase(_postIncrement)) {
                                    _postIncrement = Character.toUpperCase(_postIncrement);
                                }
                                c3 = (char) (_postIncrement ^ '@');
                                boolean z11 = z7;
                                c4 = c2;
                                z3 = z11;
                                break;
                            case 'd':
                                c4 = 65535;
                                c3 = 24;
                                z3 = true;
                                break;
                            case 'e':
                                c3 = 27;
                                boolean z12 = z7;
                                c4 = c2;
                                z3 = z12;
                                break;
                            case 'f':
                                c3 = 12;
                                boolean z13 = z7;
                                c4 = c2;
                                z3 = z13;
                                break;
                            case 'n':
                                c3 = 10;
                                boolean z14 = z7;
                                c4 = c2;
                                z3 = z14;
                                break;
                            case 'r':
                                c3 = 13;
                                boolean z15 = z7;
                                c4 = c2;
                                z3 = z15;
                                break;
                            case 's':
                                c4 = 65535;
                                c3 = 22;
                                z3 = true;
                                break;
                            case 't':
                                c3 = 9;
                                boolean z16 = z7;
                                c4 = c2;
                                z3 = z16;
                                break;
                            case 'w':
                                c4 = 65535;
                                c3 = 18;
                                z3 = true;
                                break;
                            case 'x':
                                c3 = (char) __parseHex(this.__input._array, this.__input._getOffset(), 2, iArr);
                                this.__input._increment(iArr[0]);
                                boolean z17 = z7;
                                c4 = c2;
                                z3 = z17;
                                break;
                        }
                    }
                    boolean z18 = z7;
                    c4 = c2;
                    z3 = z18;
                } else {
                    c3 = c;
                    c4 = c2;
                    z3 = false;
                }
                if (z) {
                    if (c4 > c3) {
                        throw new MalformedPatternException("Invalid [] range in expression.");
                    }
                    z = false;
                } else if (z3 || this.__input._getValue() != '-' || this.__input._getOffset() + 1 >= this.__input._getLength() || this.__input._getValueRelative(1) == ']') {
                    c4 = c3;
                } else {
                    this.__input._increment();
                    c5 = c3;
                    c6 = c3;
                    z4 = true;
                    z5 = false;
                }
                if (c4 == c3) {
                    if (!z3) {
                        __emitCode('1');
                    } else if (!zArr[0]) {
                        __emitCode('/');
                    } else {
                        __emitCode('0');
                    }
                    __emitCode(c3);
                    if ((this.__modifierFlags[0] & __CASE_INSENSITIVE) != 0 && Character.isUpperCase(c3) && Character.isUpperCase(c4)) {
                        this.__programSize--;
                        __emitCode(Character.toLowerCase(c3));
                    }
                }
                if (c4 < c3) {
                    __emitCode('%');
                    __emitCode(c4);
                    __emitCode(c3);
                    if ((this.__modifierFlags[0] & __CASE_INSENSITIVE) != 0 && Character.isUpperCase(c3) && Character.isUpperCase(c4)) {
                        this.__programSize -= 2;
                        __emitCode(Character.toLowerCase(c4));
                        __emitCode(Character.toLowerCase(c3));
                    }
                    z6 = false;
                } else {
                    z6 = z;
                }
                c6 = c3;
                z4 = z6;
                c5 = c3;
                z5 = false;
            } else if (this.__input._getValue() != ']') {
                throw new MalformedPatternException("Unmatched [] in expression.");
            } else {
                __getNextChar();
                __emitCode(0);
                return __emitNode;
            }
        }
    }

    private char __parsePOSIX(boolean[] zArr) throws MalformedPatternException {
        int i;
        int i2;
        int _getOffset = this.__input._getOffset();
        int _getLength = this.__input._getLength();
        int i3 = _getOffset + 1;
        if (this.__input._getValue(_getOffset) != ':') {
            return 0;
        }
        if (this.__input._getValue(i3) == '^') {
            zArr[0] = true;
            i = i3 + 1;
        } else {
            zArr[0] = false;
            i = i3;
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            try {
                i2 = i + 1;
                char _getValue = this.__input._getValue(i);
                if (_getValue == ':' || i2 >= _getLength) {
                    int i4 = i2 + 1;
                } else {
                    stringBuffer.append(_getValue);
                    i = i2;
                }
            } catch (Exception e) {
                return 0;
            }
        }
        int i42 = i2 + 1;
        if (this.__input._getValue(i2) != ']') {
            return 0;
        }
        Object obj = __hashPOSIX.get(stringBuffer.toString());
        if (obj == null) {
            return 0;
        }
        this.__input._setOffset(i42);
        return ((Character) obj).charValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:110:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x027c  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0284  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int __parseBranch(int[] r15) throws org.apache.oro.text.regex.MalformedPatternException {
        /*
            r14 = this;
            r0 = 0
            r1 = 0
            r2 = 1
            int[] r2 = new int[r2]
            r3 = 0
            r4 = 65535(0xffff, float:9.1834E-41)
            int r5 = r14.__parseAtom(r2)
            r6 = -1
            if (r5 != r6) goto L_0x0020
            r0 = 0
            r0 = r2[r0]
            r0 = r0 & 8
            if (r0 == 0) goto L_0x001e
            r0 = 0
            r1 = r15[r0]
            r1 = r1 | 8
            r15[r0] = r1
        L_0x001e:
            r0 = -1
        L_0x001f:
            return r0
        L_0x0020:
            org.apache.oro.text.regex.CharStringPointer r6 = r14.__input
            char r6 = r6._getValue()
            r7 = 40
            if (r6 != r7) goto L_0x0057
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            r8 = 1
            char r7 = r7._getValueRelative(r8)
            r8 = 63
            if (r7 != r8) goto L_0x0057
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            r8 = 2
            char r7 = r7._getValueRelative(r8)
            r8 = 35
            if (r7 != r8) goto L_0x0057
        L_0x0040:
            r7 = 65535(0xffff, float:9.1834E-41)
            if (r6 == r7) goto L_0x0049
            r7 = 41
            if (r6 != r7) goto L_0x0114
        L_0x0049:
            r7 = 65535(0xffff, float:9.1834E-41)
            if (r6 == r7) goto L_0x0057
            r14.__getNextChar()
            org.apache.oro.text.regex.CharStringPointer r6 = r14.__input
            char r6 = r6._getValue()
        L_0x0057:
            r7 = 123(0x7b, float:1.72E-43)
            if (r6 != r7) goto L_0x0290
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            char[] r7 = r7._array
            org.apache.oro.text.regex.CharStringPointer r8 = r14.__input
            int r8 = r8._getOffset()
            boolean r7 = __parseRepetition(r7, r8)
            if (r7 == 0) goto L_0x0290
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            int r7 = r7._getOffset()
            int r7 = r7 + 1
            org.apache.oro.text.regex.CharStringPointer r8 = r14.__input
            int r8 = r8._getLength()
            org.apache.oro.text.regex.CharStringPointer r9 = r14.__input
            char r9 = r9._getValue(r7)
            r10 = r7
            r7 = r8
        L_0x0081:
            boolean r11 = java.lang.Character.isDigit(r9)
            if (r11 != 0) goto L_0x011c
            r11 = 44
            if (r9 == r11) goto L_0x011c
        L_0x008b:
            r11 = 125(0x7d, float:1.75E-43)
            if (r9 != r11) goto L_0x0290
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r1 = 10
            r0.<init>(r1)
            if (r7 != r8) goto L_0x028d
            r1 = r10
        L_0x0099:
            org.apache.oro.text.regex.CharStringPointer r3 = r14.__input
            r3._increment()
            org.apache.oro.text.regex.CharStringPointer r3 = r14.__input
            int r3 = r3._getOffset()
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            char r7 = r7._getValue(r3)
        L_0x00aa:
            boolean r8 = java.lang.Character.isDigit(r7)
            if (r8 != 0) goto L_0x0130
            java.lang.String r0 = r0.toString()     // Catch:{ NumberFormatException -> 0x013d }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x013d }
            org.apache.oro.text.regex.CharStringPointer r3 = r14.__input
            char r3 = r3._getValue(r1)
            r7 = 44
            if (r3 != r7) goto L_0x0157
            int r1 = r1 + 1
        L_0x00c4:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r7 = 10
            r3.<init>(r7)
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            char r7 = r7._getValue(r1)
            r8 = r7
            r7 = r1
        L_0x00d3:
            boolean r9 = java.lang.Character.isDigit(r8)
            if (r9 != 0) goto L_0x015f
            if (r7 == r1) goto L_0x028a
            java.lang.String r3 = r3.toString()     // Catch:{ NumberFormatException -> 0x016c }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ NumberFormatException -> 0x016c }
        L_0x00e3:
            if (r3 != 0) goto L_0x0287
            org.apache.oro.text.regex.CharStringPointer r4 = r14.__input
            char r1 = r4._getValue(r1)
            r4 = 48
            if (r1 == r4) goto L_0x0287
            r1 = 65535(0xffff, float:9.1834E-41)
        L_0x00f2:
            org.apache.oro.text.regex.CharStringPointer r3 = r14.__input
            r3._setOffset(r10)
            r14.__getNextChar()
            r3 = 1
            r4 = 1
            r12 = r1
            r1 = r0
            r0 = r12
            r13 = r4
            r4 = r3
            r3 = r13
        L_0x0102:
            if (r4 != 0) goto L_0x01a7
            r3 = 0
            boolean r4 = __isSimpleRepetitionOp(r6)
            if (r4 != 0) goto L_0x0186
            r0 = 0
            r1 = 0
            r1 = r2[r1]
            r15[r0] = r1
            r0 = r5
            goto L_0x001f
        L_0x0114:
            org.apache.oro.text.regex.CharStringPointer r6 = r14.__input
            char r6 = r6._increment()
            goto L_0x0040
        L_0x011c:
            r11 = 44
            if (r9 != r11) goto L_0x0123
            if (r7 != r8) goto L_0x008b
            r7 = r10
        L_0x0123:
            int r9 = r10 + 1
            org.apache.oro.text.regex.CharStringPointer r10 = r14.__input
            char r10 = r10._getValue(r9)
            r12 = r10
            r10 = r9
            r9 = r12
            goto L_0x0081
        L_0x0130:
            r0.append(r7)
            int r3 = r3 + 1
            org.apache.oro.text.regex.CharStringPointer r7 = r14.__input
            char r7 = r7._getValue(r3)
            goto L_0x00aa
        L_0x013d:
            r0 = move-exception
            org.apache.oro.text.regex.MalformedPatternException r1 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "Unexpected number format exception.  Please report this bug.NumberFormatException message: "
            r2.<init>(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0157:
            org.apache.oro.text.regex.CharStringPointer r1 = r14.__input
            int r1 = r1._getOffset()
            goto L_0x00c4
        L_0x015f:
            r3.append(r8)
            int r7 = r7 + 1
            org.apache.oro.text.regex.CharStringPointer r8 = r14.__input
            char r8 = r8._getValue(r7)
            goto L_0x00d3
        L_0x016c:
            r0 = move-exception
            org.apache.oro.text.regex.MalformedPatternException r1 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "Unexpected number format exception.  Please report this bug.NumberFormatException message: "
            r2.<init>(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0186:
            r14.__getNextChar()
            r4 = 0
            r7 = 43
            if (r6 == r7) goto L_0x01ef
            r7 = 4
        L_0x018f:
            r15[r4] = r7
            r4 = 42
            if (r6 != r4) goto L_0x01f1
            r4 = 0
            r4 = r2[r4]
            r4 = r4 & 2
            if (r4 == 0) goto L_0x01f1
            r4 = 16
            r14.__programInsertOperator(r4, r5)
            int r4 = r14.__cost
            int r4 = r4 + 4
            r14.__cost = r4
        L_0x01a7:
            if (r3 == 0) goto L_0x0255
            r3 = 0
            r2 = r2[r3]
            r2 = r2 & 2
            if (r2 == 0) goto L_0x0221
            int r2 = r14.__cost
            int r3 = r14.__cost
            int r3 = r3 + 2
            int r3 = r3 / 2
            int r2 = r2 + r3
            r14.__cost = r2
            r2 = 10
            r14.__programInsertOperator(r2, r5)
        L_0x01c0:
            if (r1 <= 0) goto L_0x01c6
            r2 = 0
            r3 = 1
            r15[r2] = r3
        L_0x01c6:
            if (r0 == 0) goto L_0x0243
            if (r0 >= r1) goto L_0x0243
            org.apache.oro.text.regex.MalformedPatternException r2 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            java.lang.String r4 = "Invalid interval {"
            r3.<init>(r4)
            java.lang.StringBuffer r1 = r3.append(r1)
            java.lang.String r3 = ","
            java.lang.StringBuffer r1 = r1.append(r3)
            java.lang.StringBuffer r0 = r1.append(r0)
            java.lang.String r1 = "}"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            throw r2
        L_0x01ef:
            r7 = 1
            goto L_0x018f
        L_0x01f1:
            r4 = 42
            if (r6 != r4) goto L_0x01f8
            r1 = 0
            r3 = 1
            goto L_0x01a7
        L_0x01f8:
            r4 = 43
            if (r6 != r4) goto L_0x020f
            r4 = 0
            r4 = r2[r4]
            r4 = r4 & 2
            if (r4 == 0) goto L_0x020f
            r4 = 17
            r14.__programInsertOperator(r4, r5)
            int r4 = r14.__cost
            int r4 = r4 + 3
            r14.__cost = r4
            goto L_0x01a7
        L_0x020f:
            r4 = 43
            if (r6 != r4) goto L_0x0216
            r1 = 1
            r3 = 1
            goto L_0x01a7
        L_0x0216:
            r4 = 63
            if (r6 != r4) goto L_0x01a7
            r0 = 0
            r1 = 1
            r3 = 1
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x01a7
        L_0x0221:
            int r2 = r14.__cost
            int r3 = r14.__cost
            int r3 = r3 + 4
            int r2 = r2 + r3
            r14.__cost = r2
            r2 = 34
            int r2 = r14.__emitNode(r2)
            r14.__programAddTail(r5, r2)
            r2 = 11
            r14.__programInsertOperator(r2, r5)
            r2 = 15
            int r2 = r14.__emitNode(r2)
            r14.__programAddTail(r5, r2)
            goto L_0x01c0
        L_0x0243:
            char[] r2 = r14.__program
            if (r2 == 0) goto L_0x0255
            char[] r2 = r14.__program
            int r3 = r5 + 2
            char r1 = (char) r1
            r2[r3] = r1
            char[] r1 = r14.__program
            int r2 = r5 + 3
            char r0 = (char) r0
            r1[r2] = r0
        L_0x0255:
            org.apache.oro.text.regex.CharStringPointer r0 = r14.__input
            char r0 = r0._getValue()
            r1 = 63
            if (r0 != r1) goto L_0x026c
            r14.__getNextChar()
            r0 = 29
            r14.__programInsertOperator(r0, r5)
            int r0 = r5 + 2
            r14.__programAddTail(r5, r0)
        L_0x026c:
            org.apache.oro.text.regex.CharStringPointer r0 = r14.__input
            char[] r0 = r0._array
            org.apache.oro.text.regex.CharStringPointer r1 = r14.__input
            int r1 = r1._getOffset()
            boolean r0 = __isComplexRepetitionOp(r0, r1)
            if (r0 == 0) goto L_0x0284
            org.apache.oro.text.regex.MalformedPatternException r0 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r1 = "Nested repetitions *?+ in expression"
            r0.<init>(r1)
            throw r0
        L_0x0284:
            r0 = r5
            goto L_0x001f
        L_0x0287:
            r1 = r3
            goto L_0x00f2
        L_0x028a:
            r3 = r4
            goto L_0x00e3
        L_0x028d:
            r1 = r7
            goto L_0x0099
        L_0x0290:
            r12 = r4
            r4 = r0
            r0 = r12
            r13 = r1
            r1 = r3
            r3 = r13
            goto L_0x0102
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Compiler.__parseBranch(int[]):int");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private int __parseExpression(boolean z, int[] iArr) throws MalformedPatternException {
        int i;
        int i2;
        char c;
        int __emitNode;
        char[] cArr = new char[1];
        char[] cArr2 = new char[1];
        int[] iArr2 = new int[1];
        iArr[0] = 1;
        if (!z) {
            i = 0;
            i2 = -1;
            c = 0;
        } else if (this.__input._getValue() == '?') {
            this.__input._increment();
            char _postIncrement = this.__input._postIncrement();
            switch (_postIncrement) {
                case sym.BREAK:
                case sym.UNITDISPLAYDEFINITION:
                case sym.SINGLEEQUALS:
                    i = 0;
                    i2 = -1;
                    c = _postIncrement;
                    break;
                case sym.BOOLEANLIT:
                    char _getValue = this.__input._getValue();
                    while (_getValue != 65535 && _getValue != ')') {
                        _getValue = this.__input._increment();
                    }
                    if (_getValue != ')') {
                        throw new MalformedPatternException("Sequence (?#... not terminated");
                    }
                    __getNextChar();
                    iArr[0] = 8;
                    return -1;
                default:
                    this.__input._decrement();
                    char _getValue2 = this.__input._getValue();
                    char[] cArr3 = cArr;
                    while (_getValue2 != 65535 && "iogmsx-".indexOf(_getValue2) != -1) {
                        if (_getValue2 == '-') {
                            cArr3 = cArr2;
                        } else {
                            __setModifierFlag(cArr3, _getValue2);
                        }
                        _getValue2 = this.__input._increment();
                    }
                    char[] cArr4 = this.__modifierFlags;
                    cArr4[0] = (char) (cArr[0] | cArr4[0]);
                    char[] cArr5 = this.__modifierFlags;
                    cArr5[0] = (char) ((cArr2[0] ^ 65535) & cArr5[0]);
                    if (_getValue2 != ')') {
                        throw new MalformedPatternException(new StringBuffer("Sequence (?").append(_getValue2).append("...) not recognized").toString());
                    }
                    __getNextChar();
                    iArr[0] = 8;
                    return -1;
            }
        } else {
            i = this.__numParentheses;
            this.__numParentheses++;
            i2 = __emitArgNode(27, (char) i);
            c = 1;
        }
        int __parseAlternation = __parseAlternation(iArr2);
        if (__parseAlternation == -1) {
            return -1;
        }
        if (i2 != -1) {
            __programAddTail(i2, __parseAlternation);
        } else {
            i2 = __parseAlternation;
        }
        if ((iArr2[0] & 1) == 0) {
            iArr[0] = iArr[0] & -2;
        }
        iArr[0] = iArr[0] | (iArr2[0] & 4);
        while (this.__input._getValue() == '|') {
            __getNextChar();
            int __parseAlternation2 = __parseAlternation(iArr2);
            if (__parseAlternation2 == -1) {
                return -1;
            }
            __programAddTail(i2, __parseAlternation2);
            if ((iArr2[0] & 1) == 0) {
                iArr[0] = iArr[0] & -2;
            }
            iArr[0] = iArr[0] | (iArr2[0] & 4);
        }
        switch (c) {
            case 1:
                __emitNode = __emitArgNode(28, (char) i);
                break;
            case sym.BREAK:
            case sym.SINGLEEQUALS:
                __emitNode = __emitNode('!');
                iArr[0] = iArr[0] & -2;
                break;
            case sym.UNITDISPLAYDEFINITION:
                __emitNode = __emitNode(15);
                break;
            default:
                __emitNode = __emitNode(0);
                break;
        }
        __programAddTail(i2, __emitNode);
        int i3 = i2;
        while (i3 != -1) {
            __programAddOperatorTail(i3, __emitNode);
            i3 = OpCode._getNext(this.__program, i3);
        }
        if (c == '=') {
            __programInsertOperator(31, i2);
            __programAddTail(i2, __emitNode(15));
        } else if (c == '!') {
            __programInsertOperator(__EXTENDED, i2);
            __programAddTail(i2, __emitNode(15));
        }
        if (c != 0 && (this.__input._isAtEnd() || __getNextChar() != ')')) {
            throw new MalformedPatternException("Unmatched parentheses.");
        } else if (c != 0 || this.__input._isAtEnd()) {
            return i2;
        } else {
            if (this.__input._getValue() == ')') {
                throw new MalformedPatternException("Unmatched parentheses.");
            }
            throw new MalformedPatternException("Unreached characters at end of expression.  Please report this bug!");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(char[], int, int):void}
     arg types: [char[], int, char]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0119, code lost:
        if (1 == 0) goto L_0x011b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.oro.text.regex.Pattern compile(char[] r24, int r25) throws org.apache.oro.text.regex.MalformedPatternException {
        /*
            r23 = this;
            r4 = 1
            int[] r4 = new int[r4]
            r5 = 0
            r6 = 0
            r7 = 0
            org.apache.oro.text.regex.CharStringPointer r8 = new org.apache.oro.text.regex.CharStringPointer
            r0 = r8
            r1 = r24
            r0.<init>(r1)
            r0 = r8
            r1 = r23
            r1.__input = r0
            r8 = r25 & 1
            r0 = r23
            char[] r0 = r0.__modifierFlags
            r8 = r0
            r9 = 0
            r0 = r25
            char r0 = (char) r0
            r10 = r0
            r8[r9] = r10
            r8 = 0
            r0 = r8
            r1 = r23
            r1.__sawBackreference = r0
            r8 = 1
            r0 = r8
            r1 = r23
            r1.__numParentheses = r0
            r8 = 0
            r0 = r8
            r1 = r23
            r1.__programSize = r0
            r8 = 0
            r0 = r8
            r1 = r23
            r1.__cost = r0
            r8 = 0
            r0 = r8
            r1 = r23
            r1.__program = r0
            r8 = 0
            r0 = r23
            r1 = r8
            r0.__emitCode(r1)
            r8 = 0
            r0 = r23
            r1 = r8
            r2 = r4
            int r8 = r0.__parseExpression(r1, r2)
            r9 = -1
            if (r8 != r9) goto L_0x005a
            org.apache.oro.text.regex.MalformedPatternException r4 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r5 = "Unknown compilation error."
            r4.<init>(r5)
            throw r4
        L_0x005a:
            r0 = r23
            int r0 = r0.__programSize
            r8 = r0
            r9 = 65534(0xfffe, float:9.1833E-41)
            if (r8 < r9) goto L_0x006c
            org.apache.oro.text.regex.MalformedPatternException r4 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r5 = "Expression is too large."
            r4.<init>(r5)
            throw r4
        L_0x006c:
            r0 = r23
            int r0 = r0.__programSize
            r8 = r0
            char[] r8 = new char[r8]
            r0 = r8
            r1 = r23
            r1.__program = r0
            org.apache.oro.text.regex.Perl5Pattern r8 = new org.apache.oro.text.regex.Perl5Pattern
            r8.<init>()
            r0 = r23
            char[] r0 = r0.__program
            r9 = r0
            r8._program = r9
            java.lang.String r9 = new java.lang.String
            r0 = r9
            r1 = r24
            r0.<init>(r1)
            r8._expression = r9
            r0 = r23
            org.apache.oro.text.regex.CharStringPointer r0 = r0.__input
            r9 = r0
            r10 = 0
            r9._setOffset(r10)
            r9 = 1
            r0 = r9
            r1 = r23
            r1.__numParentheses = r0
            r9 = 0
            r0 = r9
            r1 = r23
            r1.__programSize = r0
            r9 = 0
            r0 = r9
            r1 = r23
            r1.__cost = r0
            r9 = 0
            r0 = r23
            r1 = r9
            r0.__emitCode(r1)
            r9 = 0
            r0 = r23
            r1 = r9
            r2 = r4
            int r4 = r0.__parseExpression(r1, r2)
            r9 = -1
            if (r4 != r9) goto L_0x00c4
            org.apache.oro.text.regex.MalformedPatternException r4 = new org.apache.oro.text.regex.MalformedPatternException
            java.lang.String r5 = "Unknown compilation error."
            r4.<init>(r5)
            throw r4
        L_0x00c4:
            r0 = r23
            char[] r0 = r0.__modifierFlags
            r4 = r0
            r9 = 0
            char r4 = r4[r9]
            r4 = r4 & 1
            r0 = r23
            int r0 = r0.__cost
            r9 = r0
            r10 = 10
            if (r9 >= r10) goto L_0x01ea
            r9 = 0
        L_0x00d8:
            r8._isExpensive = r9
            r9 = -1
            r8._startClassOffset = r9
            r9 = 0
            r8._anchor = r9
            r9 = -1
            r8._back = r9
            r0 = r25
            r1 = r8
            r1._options = r0
            r9 = 0
            r8._startString = r9
            r9 = 0
            r8._mustString = r9
            r9 = 0
            r10 = 0
            r11 = 1
            r0 = r23
            char[] r0 = r0.__program
            r12 = r0
            r0 = r23
            char[] r0 = r0.__program
            r13 = r0
            int r13 = org.apache.oro.text.regex.OpCode._getNext(r13, r11)
            char r12 = r12[r13]
            if (r12 != 0) goto L_0x0496
            int r7 = org.apache.oro.text.regex.OpCode._getNextOperator(r11)
            r0 = r23
            char[] r0 = r0.__program
            r11 = r0
            char r11 = r11[r7]
            r12 = r7
            r21 = r5
            r5 = r11
            r11 = r21
        L_0x0114:
            r13 = 27
            if (r5 != r13) goto L_0x011b
            r11 = 1
            if (r11 != 0) goto L_0x01ed
        L_0x011b:
            r13 = 12
            if (r5 != r13) goto L_0x0133
            r0 = r23
            char[] r0 = r0.__program
            r13 = r0
            r0 = r23
            char[] r0 = r0.__program
            r14 = r0
            int r14 = org.apache.oro.text.regex.OpCode._getNext(r14, r12)
            char r13 = r13[r14]
            r14 = 12
            if (r13 != r14) goto L_0x01ed
        L_0x0133:
            r13 = 17
            if (r5 == r13) goto L_0x01ed
            r13 = 29
            if (r5 == r13) goto L_0x01ed
            char[] r13 = org.apache.oro.text.regex.OpCode._opType
            char r13 = r13[r5]
            r14 = 10
            if (r13 != r14) goto L_0x014e
            r0 = r23
            char[] r0 = r0.__program
            r13 = r0
            char r13 = org.apache.oro.text.regex.OpCode._getArg1(r13, r12)
            if (r13 > 0) goto L_0x01ed
        L_0x014e:
            r5 = 1
            r21 = r12
            r12 = r10
            r10 = r21
        L_0x0154:
            if (r5 != 0) goto L_0x0211
            if (r6 == 0) goto L_0x0167
            if (r11 == 0) goto L_0x0161
            r0 = r23
            boolean r0 = r0.__sawBackreference
            r5 = r0
            if (r5 != 0) goto L_0x0167
        L_0x0161:
            int r5 = r8._anchor
            r5 = r5 | 4
            r8._anchor = r5
        L_0x0167:
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r11 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r21 = r16
            r16 = r7
            r7 = r14
            r14 = r5
            r5 = r21
            r22 = r6
            r6 = r15
            r15 = r10
            r10 = r13
            r13 = r22
        L_0x0186:
            if (r16 <= 0) goto L_0x0192
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            char r17 = r17[r16]
            if (r17 != 0) goto L_0x02a3
        L_0x0192:
            int r7 = r14.length()
            char[] r11 = org.apache.oro.text.regex.OpCode._opType
            r0 = r23
            char[] r0 = r0.__program
            r16 = r0
            char r15 = r16[r15]
            char r11 = r11[r15]
            r15 = 4
            if (r11 != r15) goto L_0x0483
            r11 = 1
        L_0x01a6:
            int r7 = r7 + r11
            int r11 = r13.length()
            if (r7 <= r11) goto L_0x0486
            r5 = r6
            r6 = r14
        L_0x01af:
            int r7 = r6.length()
            if (r7 <= 0) goto L_0x048e
            if (r12 != 0) goto L_0x048e
            java.lang.String r6 = r6.toString()
            if (r5 >= 0) goto L_0x01be
            r5 = -1
        L_0x01be:
            r8._back = r5
            r5 = r10
            r7 = r6
            r6 = r12
        L_0x01c3:
            r4 = r4 & 1
            if (r4 != 0) goto L_0x0493
            r4 = 0
        L_0x01c8:
            r8._isCaseInsensitive = r4
            r0 = r23
            int r0 = r0.__numParentheses
            r4 = r0
            r9 = 1
            int r4 = r4 - r9
            r8._numParentheses = r4
            r8._minLength = r5
            if (r7 == 0) goto L_0x01e1
            char[] r4 = r7.toCharArray()
            r8._mustString = r4
            r4 = 100
            r8._mustUtility = r4
        L_0x01e1:
            if (r6 == 0) goto L_0x01e9
            char[] r4 = r6.toCharArray()
            r8._startString = r4
        L_0x01e9:
            return r8
        L_0x01ea:
            r9 = 1
            goto L_0x00d8
        L_0x01ed:
            r13 = 17
            if (r5 != r13) goto L_0x0206
            r5 = 1
            r6 = r12
        L_0x01f3:
            int r6 = org.apache.oro.text.regex.OpCode._getNextOperator(r6)
            r0 = r23
            char[] r0 = r0.__program
            r12 = r0
            char r12 = r12[r6]
            r21 = r12
            r12 = r6
            r6 = r5
            r5 = r21
            goto L_0x0114
        L_0x0206:
            int[] r13 = org.apache.oro.text.regex.OpCode._operandLength
            r5 = r13[r5]
            int r5 = r5 + r12
            r21 = r6
            r6 = r5
            r5 = r21
            goto L_0x01f3
        L_0x0211:
            r5 = 0
            r0 = r23
            char[] r0 = r0.__program
            r13 = r0
            char r13 = r13[r10]
            r14 = 14
            if (r13 != r14) goto L_0x023a
            java.lang.String r12 = new java.lang.String
            r0 = r23
            char[] r0 = r0.__program
            r13 = r0
            int r14 = r10 + 1
            int r14 = org.apache.oro.text.regex.OpCode._getOperand(r14)
            r0 = r23
            char[] r0 = r0.__program
            r15 = r0
            int r16 = org.apache.oro.text.regex.OpCode._getOperand(r10)
            char r15 = r15[r16]
            r12.<init>(r13, r14, r15)
            goto L_0x0154
        L_0x023a:
            char[] r14 = org.apache.oro.text.regex.OpCode._opLengthOne
            r15 = 2
            boolean r14 = org.apache.oro.text.regex.OpCode._isInArray(r13, r14, r15)
            if (r14 == 0) goto L_0x0247
            r8._startClassOffset = r10
            goto L_0x0154
        L_0x0247:
            r14 = 20
            if (r13 == r14) goto L_0x024f
            r14 = 21
            if (r13 != r14) goto L_0x0253
        L_0x024f:
            r8._startClassOffset = r10
            goto L_0x0154
        L_0x0253:
            char[] r14 = org.apache.oro.text.regex.OpCode._opType
            char r14 = r14[r13]
            r15 = 1
            if (r14 != r15) goto L_0x0277
            r5 = 1
            if (r13 != r5) goto L_0x026c
            r5 = 1
            r8._anchor = r5
        L_0x0260:
            int r5 = org.apache.oro.text.regex.OpCode._getNextOperator(r10)
            r10 = 1
            r21 = r10
            r10 = r5
            r5 = r21
            goto L_0x0154
        L_0x026c:
            r5 = 2
            if (r13 != r5) goto L_0x0273
            r5 = 2
            r8._anchor = r5
            goto L_0x0260
        L_0x0273:
            r5 = 3
            r8._anchor = r5
            goto L_0x0260
        L_0x0277:
            r14 = 16
            if (r13 != r14) goto L_0x0154
            char[] r13 = org.apache.oro.text.regex.OpCode._opType
            r0 = r23
            char[] r0 = r0.__program
            r14 = r0
            int r15 = org.apache.oro.text.regex.OpCode._getNextOperator(r10)
            char r14 = r14[r15]
            char r13 = r13[r14]
            r14 = 7
            if (r13 != r14) goto L_0x0154
            int r13 = r8._anchor
            r13 = r13 & 3
            if (r13 == 0) goto L_0x0154
            r5 = 11
            r8._anchor = r5
            int r5 = org.apache.oro.text.regex.OpCode._getNextOperator(r10)
            r10 = 1
            r21 = r10
            r10 = r5
            r5 = r21
            goto L_0x0154
        L_0x02a3:
            r18 = 12
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x02f0
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            r0 = r23
            char[] r0 = r0.__program
            r18 = r0
            r0 = r18
            r1 = r16
            int r18 = org.apache.oro.text.regex.OpCode._getNext(r0, r1)
            char r17 = r17[r18]
            r18 = 12
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x02ea
            r7 = -30000(0xffffffffffff8ad0, float:NaN)
        L_0x02cb:
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            char r17 = r17[r16]
            r18 = 12
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0186
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            r0 = r17
            r1 = r16
            int r16 = org.apache.oro.text.regex.OpCode._getNext(r0, r1)
            goto L_0x02cb
        L_0x02ea:
            int r16 = org.apache.oro.text.regex.OpCode._getNextOperator(r16)
            goto L_0x0186
        L_0x02f0:
            r18 = 32
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x030a
            r7 = -30000(0xffffffffffff8ad0, float:NaN)
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            r0 = r17
            r1 = r16
            int r16 = org.apache.oro.text.regex.OpCode._getNext(r0, r1)
            goto L_0x0186
        L_0x030a:
            r18 = 14
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x03e3
            r15 = r16
        L_0x0314:
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            r0 = r23
            char[] r0 = r0.__program
            r18 = r0
            r0 = r18
            r1 = r15
            int r18 = org.apache.oro.text.regex.OpCode._getNext(r0, r1)
            char r17 = r17[r18]
            r19 = 28
            r0 = r17
            r1 = r19
            if (r0 == r1) goto L_0x0398
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            int r18 = org.apache.oro.text.regex.OpCode._getOperand(r16)
            char r17 = r17[r18]
            int r10 = r10 + r17
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            int r18 = org.apache.oro.text.regex.OpCode._getOperand(r16)
            char r17 = r17[r18]
            int r18 = r7 - r6
            r0 = r18
            r1 = r11
            if (r0 != r1) goto L_0x039c
            java.lang.String r18 = new java.lang.String
            r0 = r23
            char[] r0 = r0.__program
            r19 = r0
            int r16 = org.apache.oro.text.regex.OpCode._getOperand(r16)
            int r16 = r16 + 1
            r0 = r18
            r1 = r19
            r2 = r16
            r3 = r17
            r0.<init>(r1, r2, r3)
            r0 = r14
            r1 = r18
            r0.append(r1)
            int r11 = r11 + r17
            int r7 = r7 + r17
            r0 = r23
            char[] r0 = r0.__program
            r16 = r0
            r0 = r16
            r1 = r15
            int r16 = org.apache.oro.text.regex.OpCode._getNext(r0, r1)
            r21 = r16
            r16 = r15
            r15 = r21
        L_0x0388:
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            r0 = r17
            r1 = r16
            int r16 = org.apache.oro.text.regex.OpCode._getNext(r0, r1)
            goto L_0x0186
        L_0x0398:
            r15 = r18
            goto L_0x0314
        L_0x039c:
            if (r7 < 0) goto L_0x03d7
            r18 = 1
        L_0x03a0:
            int r18 = r18 + r11
            r0 = r17
            r1 = r18
            if (r0 < r1) goto L_0x03da
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            java.lang.String r11 = new java.lang.String
            r0 = r23
            char[] r0 = r0.__program
            r14 = r0
            int r16 = org.apache.oro.text.regex.OpCode._getOperand(r16)
            int r16 = r16 + 1
            r0 = r11
            r1 = r14
            r2 = r16
            r3 = r17
            r0.<init>(r1, r2, r3)
            r6.<init>(r11)
            int r11 = r7 + r17
            r0 = r23
            char[] r0 = r0.__program
            r14 = r0
            int r14 = org.apache.oro.text.regex.OpCode._getNext(r14, r15)
            r16 = r15
            r15 = r14
            r14 = r6
            r6 = r7
            r7 = r11
            r11 = r17
            goto L_0x0388
        L_0x03d7:
            r18 = 0
            goto L_0x03a0
        L_0x03da:
            int r7 = r7 + r17
            r21 = r16
            r16 = r15
            r15 = r21
            goto L_0x0388
        L_0x03e3:
            char[] r18 = org.apache.oro.text.regex.OpCode._opLengthVaries
            r19 = 0
            boolean r18 = org.apache.oro.text.regex.OpCode._isInArray(r17, r18, r19)
            if (r18 == 0) goto L_0x045d
            r7 = -30000(0xffffffffffff8ad0, float:NaN)
            r11 = 0
            int r18 = r14.length()
            int r19 = r13.length()
            r0 = r18
            r1 = r19
            if (r0 <= r1) goto L_0x0400
            r5 = r6
            r13 = r14
        L_0x0400:
            java.lang.StringBuffer r14 = new java.lang.StringBuffer
            r14.<init>()
            r18 = 17
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0427
            r0 = r23
            char[] r0 = r0.__program
            r18 = r0
            int r19 = org.apache.oro.text.regex.OpCode._getNextOperator(r16)
            char r18 = r18[r19]
            char[] r19 = org.apache.oro.text.regex.OpCode._opLengthOne
            r20 = 0
            boolean r18 = org.apache.oro.text.regex.OpCode._isInArray(r18, r19, r20)
            if (r18 == 0) goto L_0x0427
            int r10 = r10 + 1
            goto L_0x0388
        L_0x0427:
            char[] r18 = org.apache.oro.text.regex.OpCode._opType
            char r17 = r18[r17]
            r18 = 10
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0388
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            int r18 = org.apache.oro.text.regex.OpCode._getNextOperator(r16)
            int r18 = r18 + 2
            char r17 = r17[r18]
            char[] r18 = org.apache.oro.text.regex.OpCode._opLengthOne
            r19 = 0
            boolean r17 = org.apache.oro.text.regex.OpCode._isInArray(r17, r18, r19)
            if (r17 == 0) goto L_0x0388
            r0 = r23
            char[] r0 = r0.__program
            r17 = r0
            r0 = r17
            r1 = r16
            char r17 = org.apache.oro.text.regex.OpCode._getArg1(r0, r1)
            int r10 = r10 + r17
            goto L_0x0388
        L_0x045d:
            char[] r18 = org.apache.oro.text.regex.OpCode._opLengthOne
            r19 = 0
            boolean r17 = org.apache.oro.text.regex.OpCode._isInArray(r17, r18, r19)
            if (r17 == 0) goto L_0x0388
            int r7 = r7 + 1
            int r10 = r10 + 1
            r11 = 0
            int r17 = r14.length()
            int r18 = r13.length()
            r0 = r17
            r1 = r18
            if (r0 <= r1) goto L_0x047c
            r5 = r6
            r13 = r14
        L_0x047c:
            java.lang.StringBuffer r14 = new java.lang.StringBuffer
            r14.<init>()
            goto L_0x0388
        L_0x0483:
            r11 = 0
            goto L_0x01a6
        L_0x0486:
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r6 = r13
            goto L_0x01af
        L_0x048e:
            r5 = r10
            r6 = r12
            r7 = r9
            goto L_0x01c3
        L_0x0493:
            r4 = 1
            goto L_0x01c8
        L_0x0496:
            r5 = r7
            r6 = r10
            r7 = r9
            goto L_0x01c3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.oro.text.regex.Perl5Compiler.compile(char[], int):org.apache.oro.text.regex.Pattern");
    }

    public Pattern compile(char[] cArr) throws MalformedPatternException {
        return compile(cArr, 0);
    }

    public Pattern compile(String str) throws MalformedPatternException {
        return compile(str.toCharArray(), 0);
    }

    public Pattern compile(String str, int i) throws MalformedPatternException {
        return compile(str.toCharArray(), i);
    }
}
