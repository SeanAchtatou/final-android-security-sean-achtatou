package org.apache.oro.text.regex;

public class MalformedPatternException extends Exception {
    public MalformedPatternException() {
    }

    public MalformedPatternException(String str) {
        super(str);
    }
}
