package org.apache.oro.text.regex;

final class Perl5MatchResult implements MatchResult {
    int[] _beginGroupOffset;
    int[] _endGroupOffset;
    String _match;
    int _matchBeginOffset;

    Perl5MatchResult(int i) {
        this._beginGroupOffset = new int[i];
        this._endGroupOffset = new int[i];
    }

    public int length() {
        int i = this._endGroupOffset[0] - this._beginGroupOffset[0];
        if (i > 0) {
            return i;
        }
        return 0;
    }

    public int groups() {
        return this._beginGroupOffset.length;
    }

    public String group(int i) {
        if (i < this._beginGroupOffset.length) {
            int i2 = this._beginGroupOffset[i];
            int i3 = this._endGroupOffset[i];
            int length = this._match.length();
            if (i2 >= 0 && i3 >= 0) {
                if (i2 < length && i3 <= length && i3 > i2) {
                    return this._match.substring(i2, i3);
                }
                if (i2 <= i3) {
                    return "";
                }
            }
        }
        return null;
    }

    public int begin(int i) {
        if (i < this._beginGroupOffset.length) {
            int i2 = this._beginGroupOffset[i];
            int i3 = this._endGroupOffset[i];
            if (i2 < 0 || i3 < 0) {
                return -1;
            }
            return i2;
        }
        return -1;
    }

    public int end(int i) {
        if (i < this._beginGroupOffset.length) {
            int i2 = this._beginGroupOffset[i];
            int i3 = this._endGroupOffset[i];
            if (i2 >= 0 && i3 >= 0) {
                return i3;
            }
        }
        return -1;
    }

    public int beginOffset(int i) {
        if (i < this._beginGroupOffset.length) {
            int i2 = this._beginGroupOffset[i];
            int i3 = this._endGroupOffset[i];
            if (i2 >= 0 && i3 >= 0) {
                return i2 + this._matchBeginOffset;
            }
        }
        return -1;
    }

    public int endOffset(int i) {
        if (i < this._endGroupOffset.length) {
            int i2 = this._beginGroupOffset[i];
            int i3 = this._endGroupOffset[i];
            if (i2 >= 0 && i3 >= 0) {
                return this._matchBeginOffset + i3;
            }
        }
        return -1;
    }

    public String toString() {
        return group(0);
    }
}
