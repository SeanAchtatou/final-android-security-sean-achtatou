package org.apache.oro.text.regex;

final class CharStringPointer {
    static final char _END_OF_STRING = '￿';
    char[] _array;
    int _offset;

    CharStringPointer(char[] cArr, int i) {
        this._array = cArr;
        this._offset = i;
    }

    CharStringPointer(char[] cArr) {
        this(cArr, 0);
    }

    /* access modifiers changed from: package-private */
    public char _getValue() {
        return _getValue(this._offset);
    }

    /* access modifiers changed from: package-private */
    public char _getValue(int i) {
        if (i >= this._array.length || i < 0) {
            return _END_OF_STRING;
        }
        return this._array[i];
    }

    /* access modifiers changed from: package-private */
    public char _getValueRelative(int i) {
        return _getValue(this._offset + i);
    }

    /* access modifiers changed from: package-private */
    public int _getLength() {
        return this._array.length;
    }

    /* access modifiers changed from: package-private */
    public int _getOffset() {
        return this._offset;
    }

    /* access modifiers changed from: package-private */
    public void _setOffset(int i) {
        this._offset = i;
    }

    /* access modifiers changed from: package-private */
    public boolean _isAtEnd() {
        return this._offset >= this._array.length;
    }

    /* access modifiers changed from: package-private */
    public char _increment(int i) {
        this._offset += i;
        if (!_isAtEnd()) {
            return this._array[this._offset];
        }
        this._offset = this._array.length;
        return _END_OF_STRING;
    }

    /* access modifiers changed from: package-private */
    public char _increment() {
        return _increment(1);
    }

    /* access modifiers changed from: package-private */
    public char _decrement(int i) {
        this._offset -= i;
        if (this._offset < 0) {
            this._offset = 0;
        }
        return this._array[this._offset];
    }

    /* access modifiers changed from: package-private */
    public char _decrement() {
        return _decrement(1);
    }

    /* access modifiers changed from: package-private */
    public char _postIncrement() {
        char _getValue = _getValue();
        _increment();
        return _getValue;
    }

    /* access modifiers changed from: package-private */
    public char _postDecrement() {
        char _getValue = _getValue();
        _decrement();
        return _getValue;
    }

    /* access modifiers changed from: package-private */
    public String _toString(int i) {
        return new String(this._array, i, this._array.length - i);
    }

    public String toString() {
        return _toString(0);
    }
}
