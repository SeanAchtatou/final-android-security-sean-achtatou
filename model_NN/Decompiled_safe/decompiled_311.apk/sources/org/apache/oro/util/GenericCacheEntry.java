package org.apache.oro.util;

import java.io.Serializable;

final class GenericCacheEntry implements Serializable {
    int _index;
    Object _key = null;
    Object _value = null;

    GenericCacheEntry(int i) {
        this._index = i;
    }
}
