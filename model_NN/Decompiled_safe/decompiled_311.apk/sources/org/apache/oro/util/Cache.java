package org.apache.oro.util;

public interface Cache {
    void addElement(Object obj, Object obj2);

    int capacity();

    Object getElement(Object obj);

    int size();
}
