package org.apache.oro.util;

public final class CacheLRU extends GenericCache {
    private int __head;
    private int[] __next;
    private int[] __prev;
    private int __tail;

    public CacheLRU(int i) {
        super(i);
        this.__next = new int[this._cache.length];
        this.__prev = new int[this._cache.length];
        for (int i2 = 0; i2 < this.__next.length; i2++) {
            int[] iArr = this.__next;
            this.__prev[i2] = -1;
            iArr[i2] = -1;
        }
    }

    public CacheLRU() {
        this(20);
    }

    private void __moveToFront(int i) {
        if (this.__head != i) {
            int i2 = this.__next[i];
            int i3 = this.__prev[i];
            this.__next[i3] = i2;
            if (i2 >= 0) {
                this.__prev[i2] = i3;
            } else {
                this.__tail = i3;
            }
            this.__prev[i] = -1;
            this.__next[i] = this.__head;
            this.__prev[this.__head] = i;
            this.__head = i;
        }
    }

    public synchronized Object getElement(Object obj) {
        Object obj2;
        Object obj3 = this._table.get(obj);
        if (obj3 != null) {
            GenericCacheEntry genericCacheEntry = (GenericCacheEntry) obj3;
            __moveToFront(genericCacheEntry._index);
            obj2 = genericCacheEntry._value;
        } else {
            obj2 = null;
        }
        return obj2;
    }

    public final synchronized void addElement(Object obj, Object obj2) {
        Object obj3 = this._table.get(obj);
        if (obj3 != null) {
            GenericCacheEntry genericCacheEntry = (GenericCacheEntry) obj3;
            genericCacheEntry._value = obj2;
            genericCacheEntry._key = obj;
            __moveToFront(genericCacheEntry._index);
        } else {
            if (!isFull()) {
                if (this._numEntries > 0) {
                    this.__prev[this._numEntries] = this.__tail;
                    this.__next[this._numEntries] = -1;
                    __moveToFront(this._numEntries);
                }
                this._numEntries++;
            } else {
                this._table.remove(this._cache[this.__tail]._key);
                __moveToFront(this.__tail);
            }
            this._cache[this.__head]._value = obj2;
            this._cache[this.__head]._key = obj;
            this._table.put(obj, this._cache[this.__head]);
        }
    }
}
