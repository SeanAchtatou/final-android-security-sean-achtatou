package org.apache.oro.util;

public final class CacheFIFO2 extends GenericCache {
    private int __current;
    private boolean[] __tryAgain;

    public CacheFIFO2(int i) {
        super(i);
        this.__tryAgain = new boolean[this._cache.length];
    }

    public CacheFIFO2() {
        this(20);
    }

    public synchronized Object getElement(Object obj) {
        Object obj2;
        Object obj3 = this._table.get(obj);
        if (obj3 != null) {
            GenericCacheEntry genericCacheEntry = (GenericCacheEntry) obj3;
            this.__tryAgain[genericCacheEntry._index] = true;
            obj2 = genericCacheEntry._value;
        } else {
            obj2 = null;
        }
        return obj2;
    }

    public final synchronized void addElement(Object obj, Object obj2) {
        int i;
        Object obj3 = this._table.get(obj);
        if (obj3 != null) {
            GenericCacheEntry genericCacheEntry = (GenericCacheEntry) obj3;
            genericCacheEntry._value = obj2;
            genericCacheEntry._key = obj;
            this.__tryAgain[genericCacheEntry._index] = true;
        } else {
            if (!isFull()) {
                i = this._numEntries;
                this._numEntries++;
            } else {
                i = this.__current;
                while (this.__tryAgain[i]) {
                    this.__tryAgain[i] = false;
                    i++;
                    if (i >= this.__tryAgain.length) {
                        i = 0;
                    }
                }
                this.__current = i + 1;
                if (this.__current >= this._cache.length) {
                    this.__current = 0;
                }
                this._table.remove(this._cache[i]._key);
            }
            this._cache[i]._value = obj2;
            this._cache[i]._key = obj;
            this._table.put(obj, this._cache[i]);
        }
    }
}
