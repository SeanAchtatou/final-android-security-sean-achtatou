package org.apache.oro.util;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

public abstract class GenericCache implements Cache, Serializable {
    public static final int DEFAULT_CAPACITY = 20;
    GenericCacheEntry[] _cache;
    int _numEntries = 0;
    Hashtable _table;

    public abstract void addElement(Object obj, Object obj2);

    GenericCache(int i) {
        this._table = new Hashtable(i);
        this._cache = new GenericCacheEntry[i];
        int i2 = i;
        while (true) {
            i2--;
            if (i2 >= 0) {
                this._cache[i2] = new GenericCacheEntry(i2);
            } else {
                return;
            }
        }
    }

    public synchronized Object getElement(Object obj) {
        Object obj2;
        Object obj3 = this._table.get(obj);
        if (obj3 != null) {
            obj2 = ((GenericCacheEntry) obj3)._value;
        } else {
            obj2 = null;
        }
        return obj2;
    }

    public final Enumeration keys() {
        return this._table.keys();
    }

    public final int size() {
        return this._numEntries;
    }

    public final int capacity() {
        return this._cache.length;
    }

    public final boolean isFull() {
        return this._numEntries >= this._cache.length;
    }
}
