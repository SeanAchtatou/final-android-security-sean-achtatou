package frink.symbolic;

import frink.expr.BasicListExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import java.util.Vector;

public class BasicTransformationRuleSet implements TransformationRuleSet {
    private static final boolean DEBUG = false;
    private String name;
    private Vector<TransformationRule> rules = new Vector<>();

    public BasicTransformationRuleSet(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public void add(TransformationRule transformationRule) {
        this.rules.addElement(transformationRule);
    }

    public Expression applyRulesOnce(Expression expression, MatchingContext matchingContext, boolean z, boolean z2, BasicListExpression basicListExpression, Environment environment) throws InvalidChildException, EvaluationException {
        int size = this.rules.size();
        int i = 0;
        Expression expression2 = expression;
        while (i < size) {
            TransformationRule elementAt = this.rules.elementAt(i);
            Expression from = elementAt.getFrom();
            Expression condition = elementAt.getCondition();
            Expression to = elementAt.getTo();
            int beginMatch = matchingContext.beginMatch();
            try {
                Expression substitute = SymbolicUtils.substitute(expression2, from, to, condition, environment, matchingContext);
                if (expression2 != substitute) {
                    if (z) {
                        if (z2) {
                            BasicListExpression basicListExpression2 = new BasicListExpression(2);
                            basicListExpression2.appendChild(substitute);
                            basicListExpression2.appendChild(elementAt);
                            basicListExpression.appendChild(basicListExpression2);
                        } else {
                            basicListExpression.appendChild(substitute);
                        }
                    }
                    return substitute;
                }
                i++;
                expression2 = substitute;
            } finally {
                matchingContext.rollbackMatch(beginMatch);
            }
        }
        return expression2;
    }
}
