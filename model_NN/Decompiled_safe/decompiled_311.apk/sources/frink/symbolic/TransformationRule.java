package frink.symbolic;

import frink.expr.Environment;
import frink.expr.Expression;

public interface TransformationRule extends Expression {
    public static final String TYPE = "TransformationRule";

    Expression getCondition();

    Expression getFrom();

    Expression getTo();

    String toString(Environment environment);
}
