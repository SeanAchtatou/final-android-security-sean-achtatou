package frink.symbolic;

import frink.expr.Environment;

public interface Backtrackable {
    String getStateAsString();

    boolean nextState(Environment environment);
}
