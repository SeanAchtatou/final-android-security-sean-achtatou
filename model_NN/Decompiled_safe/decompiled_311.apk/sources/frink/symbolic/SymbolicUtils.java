package frink.symbolic;

import frink.expr.BasicListExpression;
import frink.expr.BasicSetExpression;
import frink.expr.BasicStringExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.SetExpression;
import frink.expr.SymbolExpression;
import frink.expr.Truth;
import frink.function.Sorter;
import frink.function.SymbolOrderer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class SymbolicUtils {
    private static final boolean DEBUG = false;

    /* JADX INFO: finally extract failed */
    public static boolean contains(Expression expression, Expression expression2, MatchingContext matchingContext, Environment environment) throws InvalidChildException {
        int beginMatch = matchingContext.beginMatch();
        int beginBacktrackContext = matchingContext.beginBacktrackContext();
        do {
            try {
                if (expression2.structureEquals(expression, matchingContext, environment, true)) {
                    matchingContext.rollbackMatch(beginMatch);
                    matchingContext.rollbackBacktrackContext(beginBacktrackContext);
                    return true;
                }
            } catch (Throwable th) {
                matchingContext.rollbackMatch(beginMatch);
                matchingContext.rollbackBacktrackContext(beginBacktrackContext);
                throw th;
            }
        } while (matchingContext.nextState(environment));
        matchingContext.rollbackMatch(beginMatch);
        matchingContext.rollbackBacktrackContext(beginBacktrackContext);
        int childCount = expression.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (contains(expression.getChild(i), expression2, matchingContext, environment)) {
                return true;
            }
        }
        return false;
    }

    public static boolean freeOf(Expression expression, Expression expression2, MatchingContext matchingContext, Environment environment) throws InvalidChildException {
        return !contains(expression, expression2, matchingContext, environment);
    }

    public static Expression substitute(Expression expression, Expression expression2, Expression expression3, Expression expression4, Environment environment, MatchingContext matchingContext) throws InvalidChildException, EvaluationException {
        int beginMatch = matchingContext.beginMatch();
        try {
            return substitutePrivate(expression, expression2, expression3, expression4, environment, matchingContext);
        } finally {
            matchingContext.rollbackMatch(beginMatch);
        }
    }

    /* JADX INFO: finally extract failed */
    private static Expression substitutePrivate(Expression expression, Expression expression2, Expression expression3, Expression expression4, Environment environment, MatchingContext matchingContext) throws InvalidChildException, EvaluationException {
        int beginMatch;
        boolean z;
        BasicListExpression basicListExpression;
        BasicListExpression basicListExpression2;
        int beginMatch2;
        Expression replaceBoundPatterns;
        if (!contains(expression, expression2, matchingContext, environment)) {
            return expression;
        }
        int beginBacktrackContext = matchingContext.beginBacktrackContext();
        do {
            try {
                beginMatch = matchingContext.beginMatch();
                if (expression2.structureEquals(expression, matchingContext, environment, false)) {
                    if (expression4 != null) {
                        beginMatch2 = matchingContext.beginMatch();
                        replaceBoundPatterns = replaceBoundPatterns(expression4, environment, matchingContext);
                        if (Truth.isTrue(environment, replaceBoundPatterns.evaluate(environment))) {
                            Expression replaceBoundPatterns2 = replaceBoundPatterns(expression3, environment, matchingContext);
                            matchingContext.rollbackMatch(beginMatch2);
                            matchingContext.rollbackMatch(beginMatch);
                            matchingContext.rollbackBacktrackContext(beginBacktrackContext);
                            return replaceBoundPatterns2;
                        }
                        matchingContext.rollbackMatch(beginMatch2);
                    } else {
                        Expression replaceBoundPatterns3 = replaceBoundPatterns(expression3, environment, matchingContext);
                        matchingContext.rollbackMatch(beginMatch);
                        matchingContext.rollbackBacktrackContext(beginBacktrackContext);
                        return replaceBoundPatterns3;
                    }
                }
                matchingContext.rollbackMatch(beginMatch);
            } catch (EvaluationException e) {
                environment.outputln("Exception when evaluating condition in rule.  Rule was:\n   " + environment.format(replaceBoundPatterns) + "\n   " + e);
                matchingContext.rollbackMatch(beginMatch2);
                matchingContext.rollbackMatch(beginMatch);
                matchingContext.rollbackBacktrackContext(beginBacktrackContext);
                return expression;
            } catch (Throwable th) {
                matchingContext.rollbackBacktrackContext(beginBacktrackContext);
                throw th;
            }
        } while (matchingContext.nextState(environment));
        matchingContext.rollbackBacktrackContext(beginBacktrackContext);
        int childCount = expression.getChildCount();
        int i = 0;
        boolean z2 = false;
        BasicListExpression basicListExpression3 = null;
        while (i < childCount) {
            int beginMatch3 = matchingContext.beginMatch();
            try {
                Expression child = expression.getChild(i);
                Expression substitutePrivate = substitutePrivate(child, expression2, expression3, expression4, environment, matchingContext);
                if (substitutePrivate != child) {
                    z = true;
                    if (basicListExpression3 == null) {
                        basicListExpression2 = new BasicListExpression(childCount);
                        for (int i2 = 0; i2 < i; i2++) {
                            basicListExpression2.appendChild(expression.getChild(i2));
                        }
                    } else {
                        basicListExpression2 = basicListExpression3;
                    }
                    basicListExpression2.appendChild(substitutePrivate);
                    basicListExpression = basicListExpression2;
                } else {
                    if (basicListExpression3 != null) {
                        basicListExpression3.appendChild(child);
                    }
                    z = z2;
                    basicListExpression = basicListExpression3;
                }
                matchingContext.rollbackMatch(beginMatch3);
                i++;
                z2 = z;
                basicListExpression3 = basicListExpression;
            } catch (Throwable th2) {
                matchingContext.rollbackMatch(beginMatch3);
                throw th2;
            }
        }
        if (z2) {
            return ExpressionConstructor.construct(expression.getExpressionType(), basicListExpression3, environment);
        }
        return expression;
    }

    public static boolean containsPattern(Expression expression) throws InvalidChildException {
        if (expression instanceof ExpressionPattern) {
            return true;
        }
        int childCount = expression.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (containsPattern(expression.getChild(i))) {
                return true;
            }
        }
        return false;
    }

    private static Expression replaceBoundPatterns(Expression expression, Environment environment, MatchingContext matchingContext) throws EvaluationException, InvalidChildException {
        if (!containsPattern(expression)) {
            return expression;
        }
        if (expression instanceof ExpressionPattern) {
            Expression variable = matchingContext.getVariable(((ExpressionPattern) expression).getName());
            if (variable == null) {
                return expression;
            }
            return variable;
        }
        int childCount = expression.getChildCount();
        BasicListExpression basicListExpression = new BasicListExpression(childCount);
        for (int i = 0; i < childCount; i++) {
            Expression child = expression.getChild(i);
            if (containsPattern(child)) {
                basicListExpression.appendChild(replaceBoundPatterns(child, environment, matchingContext));
            } else {
                basicListExpression.appendChild(child);
            }
        }
        return ExpressionConstructor.construct(expression.getExpressionType(), basicListExpression, environment);
    }

    public static boolean containsAnySymbol(Expression expression) throws InvalidChildException {
        if (expression instanceof SymbolExpression) {
            return true;
        }
        int childCount = expression.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (containsAnySymbol(expression.getChild(i))) {
                return true;
            }
        }
        return false;
    }

    public static void sortOperands(ListExpression listExpression, Environment environment) throws InvalidChildException {
        boolean z;
        int childCount = listExpression.getChildCount();
        if (childCount > 1) {
            Expression child = listExpression.getChild(0);
            SymbolOrderer symbolOrderer = SymbolOrderer.INSTANCE;
            Expression expression = child;
            int i = 1;
            while (true) {
                if (i >= childCount) {
                    z = false;
                    break;
                }
                try {
                    Expression child2 = listExpression.getChild(i);
                    if (symbolOrderer.compare(expression, child2, environment) > 0) {
                        z = true;
                        break;
                    } else {
                        i++;
                        expression = child2;
                    }
                } catch (EvaluationException e) {
                    return;
                }
            }
            if (z) {
                try {
                    Sorter.sort(listExpression, SymbolOrderer.INSTANCE, environment);
                } catch (EvaluationException e2) {
                }
            }
        }
    }

    public static Vector<TransformationRule> expandDefaultValues(TransformationRule transformationRule, Environment environment) throws InvalidChildException, EvaluationException {
        return expandDefaultValues(transformationRule, new Vector(), new BasicMatchingContext(), environment);
    }

    /* JADX INFO: finally extract failed */
    private static Vector<TransformationRule> expandDefaultValues(TransformationRule transformationRule, Vector<TransformationRule> vector, MatchingContext matchingContext, Environment environment) throws InvalidChildException, EvaluationException {
        Expression expression;
        MultiPattern firstMultiPattern = getFirstMultiPattern(transformationRule);
        if (firstMultiPattern == null) {
            vector.addElement(transformationRule);
            return vector;
        }
        int childCount = firstMultiPattern.getChildCount();
        int i = 0;
        while (i < childCount) {
            Expression child = firstMultiPattern.getChild(i);
            int beginMatch = matchingContext.beginMatch();
            try {
                Expression substitute = substitute(transformationRule, firstMultiPattern, child, null, environment, matchingContext);
                matchingContext.rollbackMatch(beginMatch);
                if ((child instanceof ExpressionPattern) || i == 0) {
                    expression = substitute;
                } else {
                    AnythingPattern anythingPattern = (AnythingPattern) firstMultiPattern.getChild(0);
                    int beginMatch2 = matchingContext.beginMatch();
                    try {
                        anythingPattern.setShouldMatchLiterally(true);
                        Expression substitute2 = substitute(substitute, anythingPattern, child, null, environment, matchingContext);
                        anythingPattern.setShouldMatchLiterally(false);
                        matchingContext.rollbackMatch(beginMatch2);
                        expression = substitute2;
                    } catch (Throwable th) {
                        matchingContext.rollbackMatch(beginMatch2);
                        throw th;
                    }
                }
                expandDefaultValues((TransformationRule) expression, vector, matchingContext, environment);
                i++;
            } catch (Throwable th2) {
                matchingContext.rollbackMatch(beginMatch);
                throw th2;
            }
        }
        return vector;
    }

    public static MultiPattern getFirstMultiPattern(Expression expression) throws InvalidChildException {
        if (expression instanceof MultiPattern) {
            return (MultiPattern) expression;
        }
        int childCount = expression.getChildCount();
        for (int i = 0; i < childCount; i++) {
            MultiPattern firstMultiPattern = getFirstMultiPattern(expression.getChild(i));
            if (firstMultiPattern != null) {
                return firstMultiPattern;
            }
        }
        return null;
    }

    public static SetExpression getSymbols(Expression expression) {
        BasicSetExpression basicSetExpression = new BasicSetExpression();
        getSymbols(expression, basicSetExpression);
        return basicSetExpression;
    }

    private static void getSymbols(Expression expression, SetExpression setExpression) {
        if (expression instanceof SymbolExpression) {
            setExpression.put(new BasicStringExpression(((SymbolExpression) expression).getName()));
            return;
        }
        int childCount = expression.getChildCount();
        int i = 0;
        while (i < childCount) {
            try {
                getSymbols(expression.getChild(i), setExpression);
                i++;
            } catch (InvalidChildException e) {
                System.err.println("SymbolicUtils.getSymbols() got InvalidChildException:\n  " + e);
                return;
            }
        }
    }

    public static ListExpression getSymbolsByComplexity(Expression expression, Environment environment) {
        Hashtable hashtable = new Hashtable();
        getSymbolsByComplexity(expression, hashtable, 0, environment);
        BasicListExpression basicListExpression = new BasicListExpression(hashtable.size());
        Enumeration keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            BasicListExpression basicListExpression2 = new BasicListExpression(2);
            basicListExpression2.appendChild(new BasicStringExpression(str));
            basicListExpression2.appendChild(DimensionlessUnitExpression.construct((Integer) hashtable.get(str)));
            basicListExpression.appendChild(basicListExpression2);
        }
        try {
            Sorter.sort(basicListExpression, SymbolComplexityOrderer.INSTANCE, environment);
        } catch (EvaluationException e) {
            environment.outputln("Unexpected EvaluationException in getSymbolsByComplexity.\n" + e);
        }
        return basicListExpression;
    }

    private static void getSymbolsByComplexity(Expression expression, Hashtable<String, Integer> hashtable, int i, Environment environment) {
        int i2 = 0;
        if (expression instanceof SymbolExpression) {
            String name = ((SymbolExpression) expression).getName();
            Integer num = hashtable.get(name);
            if (num != null) {
                i2 = num.intValue();
            }
            hashtable.put(name, new Integer(i2 + i));
            return;
        }
        int childCount = expression.getChildCount();
        while (i2 < childCount) {
            try {
                getSymbolsByComplexity(expression.getChild(i2), hashtable, i + 1, environment);
                i2++;
            } catch (InvalidChildException e) {
                System.err.println("SymbolicUtils.getSymbolsByComplexity() got InvalidChildException:\n  " + e);
                return;
            }
        }
    }
}
