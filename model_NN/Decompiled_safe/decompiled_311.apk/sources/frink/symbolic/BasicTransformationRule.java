package frink.symbolic;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.SelfDisplayingExpression;

public class BasicTransformationRule implements TransformationRule, SelfDisplayingExpression {
    private Expression condition;
    private Expression fromExpr;
    private Expression toExpr;

    public BasicTransformationRule(Expression expression, Expression expression2, Expression expression3) {
        this.fromExpr = expression;
        this.toExpr = expression2;
        this.condition = expression3;
    }

    public String toString(Environment environment, boolean z) {
        return environment.format(this.fromExpr, z) + " <-> " + environment.format(this.toExpr, z);
    }

    public String toString(Environment environment) {
        return environment.format(this.fromExpr, false) + " <-> " + environment.format(this.toExpr, false);
    }

    public int getChildCount() {
        if (this.condition != null) {
            return 3;
        }
        return 2;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.fromExpr;
        }
        if (i == 1) {
            return this.toExpr;
        }
        if (i == 2 && this.condition != null) {
            return this.condition;
        }
        throw new InvalidChildException("BasicTransformationRule.getChild:  Invalid child " + i, this);
    }

    public boolean isConstant() {
        return false;
    }

    public Expression getFrom() {
        return this.fromExpr;
    }

    public Expression getTo() {
        return this.toExpr;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        System.err.println("Unexpected evaluation of BasicTransformationRule");
        return this;
    }

    public boolean isBidirectional() {
        return false;
    }

    public Expression getCondition() {
        return this.condition;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof BasicTransformationRule) || !this.fromExpr.structureEquals(((BasicTransformationRule) expression).fromExpr, matchingContext, environment, z) || !this.toExpr.structureEquals(((BasicTransformationRule) expression).toExpr, matchingContext, environment, z)) {
            return false;
        }
        return true;
    }

    public String getExpressionType() {
        return "TransformationRule";
    }
}
