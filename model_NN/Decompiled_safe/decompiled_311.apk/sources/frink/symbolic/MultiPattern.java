package frink.symbolic;

import frink.expr.AssignableExpression;
import frink.expr.CannotAssignException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.NonTerminalExpression;
import frink.expr.SelfDisplayingExpression;

public class MultiPattern extends NonTerminalExpression implements ExpressionPattern, SelfDisplayingExpression, AssignableExpression {
    private static final boolean DEBUG = false;
    public static final String TYPE = "MultiPattern";

    public static MultiPattern construct(AnythingPattern anythingPattern, Expression expression) {
        return new MultiPattern(anythingPattern, expression);
    }

    private MultiPattern(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    public int getSpecificity() {
        return 30;
    }

    public Expression evaluate(Environment environment) {
        environment.outputln("Unexpected eval of MultiPattern.");
        return this;
    }

    public boolean isConstant() {
        return DEBUG;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof MultiPattern) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return DEBUG;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String toString(Environment environment, boolean z) {
        StringBuffer stringBuffer = new StringBuffer("MULTIPATTERN");
        int childCount = getChildCount();
        int i = 0;
        while (i < childCount) {
            try {
                stringBuffer.append(":" + environment.format(getChild(i), z));
                i++;
            } catch (InvalidChildException e) {
                System.err.println("Unexpected InvalidChildException in MultiPattern.toString:\n  " + e);
            }
        }
        return stringBuffer.toString();
    }

    public String getName() {
        return null;
    }

    public void assign(Expression expression, Environment environment) throws CannotAssignException {
        throw new CannotAssignException("Cannot assign to " + toString(environment, true), this);
    }
}
