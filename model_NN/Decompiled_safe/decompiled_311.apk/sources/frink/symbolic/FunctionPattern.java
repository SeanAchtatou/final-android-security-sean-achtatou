package frink.symbolic;

import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.SelfDisplayingExpression;
import frink.function.FunctionCallExpression;

public class FunctionPattern implements Expression, ExpressionPattern, SelfDisplayingExpression {
    private static final boolean DEBUG = false;
    public static final String TYPE = "FunctionPattern";
    private ListExpression args;
    private String name;
    private BasicStringExpression nameExpression;

    public FunctionPattern(String str, ListExpression listExpression) {
        if (str == null || str.length() <= 0) {
            this.name = null;
        } else {
            this.name = str;
        }
        this.args = listExpression;
        this.nameExpression = new BasicStringExpression(str);
    }

    public String getName() {
        return this.name;
    }

    public int getSpecificity() {
        return 30;
    }

    public Expression evaluate(Environment environment) {
        environment.outputln("Unexpected eval of FunctionPattern.");
        return this;
    }

    public boolean isConstant() {
        return DEBUG;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        int i;
        boolean z2;
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof FunctionPattern) || this.name == null) {
            if (expression instanceof FunctionCallExpression) {
                try {
                    if (this.name != null) {
                        Expression variable = matchingContext.getVariable(this.name);
                        int childCount = getChildCount();
                        if (childCount != expression.getChildCount()) {
                            return DEBUG;
                        }
                        if (z) {
                            i = matchingContext.beginMatch();
                        } else {
                            i = 0;
                        }
                        if (variable == null) {
                            matchingContext.setVariable(this.name, expression);
                        }
                        int i2 = 1;
                        while (true) {
                            if (i2 >= childCount) {
                                z2 = true;
                                break;
                            } else if (!getChild(i2).structureEquals(expression.getChild(i2), matchingContext, environment, z)) {
                                z2 = false;
                                break;
                            } else {
                                i2++;
                            }
                        }
                        if (z2) {
                            matchingContext.saveMatches();
                        }
                        if (!z) {
                            return z2;
                        }
                        matchingContext.rollbackMatch(i);
                        return z2;
                    }
                } catch (InvalidChildException e) {
                    System.out.println("FunctionPattern: unexpected InvalidChildException\n  " + e);
                } catch (Throwable th) {
                    if (z) {
                        matchingContext.rollbackMatch(i);
                    }
                    throw th;
                }
            }
            return DEBUG;
        }
        FunctionPattern functionPattern = (FunctionPattern) expression;
        return this.name.equals(functionPattern.getName()) ? this.args.structureEquals(functionPattern.args, matchingContext, environment, z) : DEBUG;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public int getChildCount() {
        return this.args.getChildCount() + 1;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.nameExpression;
        }
        return this.args.getChild(i - 1);
    }

    public String toString(Environment environment, boolean z) {
        if (this.name == null) {
            return "PATTERN:";
        }
        return "PATTERN:_" + this.name;
    }
}
