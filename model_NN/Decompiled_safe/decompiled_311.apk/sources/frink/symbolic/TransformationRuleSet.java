package frink.symbolic;

import frink.expr.BasicListExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidChildException;

public interface TransformationRuleSet {
    void add(TransformationRule transformationRule);

    Expression applyRulesOnce(Expression expression, MatchingContext matchingContext, boolean z, boolean z2, BasicListExpression basicListExpression, Environment environment) throws InvalidChildException, EvaluationException;

    String getName();
}
