package frink.symbolic;

import frink.expr.BasicListExpression;
import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import java.util.Hashtable;
import java.util.Vector;

public class BasicTransformationRuleManager implements TransformationRuleManager {
    public static final BasicStringExpression CANONICAL = new BasicStringExpression("(After default simplification applied)");
    public static final String DEFAULT_RULESET_NAME = "default";
    private TransformationRuleSet parsingRuleSet = getTransformationRuleSet(DEFAULT_RULESET_NAME);
    private Hashtable<String, TransformationRuleSet> ruleHash = new Hashtable<>();
    private Vector<TransformationRuleSet> ruleVector = new Vector<>();

    public TransformationRuleSet getTransformationRuleSet(String str) {
        String str2 = str == null ? DEFAULT_RULESET_NAME : str;
        TransformationRuleSet transformationRuleSet = this.ruleHash.get(str2);
        if (transformationRuleSet != null) {
            return transformationRuleSet;
        }
        BasicTransformationRuleSet basicTransformationRuleSet = new BasicTransformationRuleSet(str2);
        this.ruleHash.put(str2, basicTransformationRuleSet);
        this.ruleVector.addElement(basicTransformationRuleSet);
        return basicTransformationRuleSet;
    }

    public void setParsingTransformationRuleSet(String str) {
        String str2;
        if (str == null) {
            str2 = DEFAULT_RULESET_NAME;
        } else {
            str2 = str;
        }
        this.parsingRuleSet = getTransformationRuleSet(str2);
    }

    public TransformationRuleSet getParsingTransformationRuleSet() {
        return this.parsingRuleSet;
    }

    public void addRule(TransformationRule transformationRule, Environment environment) throws FrinkSecurityException {
        environment.getSecurityHelper().checkCreateTransformationRule();
        try {
            Vector<TransformationRule> expandDefaultValues = SymbolicUtils.expandDefaultValues(transformationRule, environment);
            int size = expandDefaultValues.size();
            for (int i = 0; i < size; i++) {
                this.parsingRuleSet.add(expandDefaultValues.elementAt(i));
            }
        } catch (InvalidChildException e) {
            System.err.println("InvalidChildException when adding transformation rule.\n " + e);
        } catch (EvaluationException e2) {
            System.err.println("EvaluationException when adding transformation rule.\n " + e2);
        }
    }

    private static Expression applyRules(Expression expression, Vector<TransformationRuleSet> vector, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException {
        Expression expression2;
        BasicListExpression basicListExpression;
        Expression expression3;
        int size = vector.size();
        BasicMatchingContext basicMatchingContext = new BasicMatchingContext();
        if (z) {
            BasicListExpression basicListExpression2 = new BasicListExpression(1);
            if (z2) {
                BasicListExpression basicListExpression3 = new BasicListExpression(2);
                basicListExpression3.appendChild(expression);
                basicListExpression3.appendChild(CANONICAL);
                basicListExpression2.appendChild(basicListExpression3);
                basicListExpression = basicListExpression2;
                expression2 = expression;
            } else {
                basicListExpression2.appendChild(expression);
                basicListExpression = basicListExpression2;
                expression2 = expression;
            }
        } else {
            basicListExpression = null;
            expression2 = expression;
        }
        while (true) {
            int i = 0;
            expression3 = expression2;
            while (i < size) {
                Expression applyRulesOnce = vector.elementAt(i).applyRulesOnce(expression3, basicMatchingContext, z, z2, basicListExpression, environment);
                i++;
                expression3 = applyRulesOnce;
            }
            if (expression2.structureEquals(expression3, basicMatchingContext, environment, true)) {
                break;
            }
            expression2 = expression3;
        }
        if (z) {
            return basicListExpression;
        }
        return expression3;
    }

    public Expression applyRules(Expression expression, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException {
        return applyRules(expression, this.ruleVector, z, z2, environment);
    }

    public Expression applyRules(Expression expression, ListExpression listExpression, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException {
        boolean z3;
        int childCount = listExpression.getChildCount();
        Vector vector = new Vector(childCount);
        int i = 0;
        boolean z4 = false;
        while (i < childCount) {
            TransformationRuleSet transformationRuleSet = getTransformationRuleSet(((BasicStringExpression) listExpression.getChild(i)).getString());
            if (transformationRuleSet != null) {
                vector.addElement(transformationRuleSet);
                z3 = true;
            } else {
                z3 = z4;
            }
            i++;
            z4 = z3;
        }
        if (!z4) {
            return expression;
        }
        return applyRules(expression, vector, z, z2, environment);
    }

    public Expression applyRules(Expression expression, String str, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException {
        Vector vector = new Vector(1);
        TransformationRuleSet transformationRuleSet = getTransformationRuleSet(str);
        if (transformationRuleSet == null) {
            return expression;
        }
        vector.addElement(transformationRuleSet);
        return applyRules(expression, vector, z, z2, environment);
    }
}
