package frink.symbolic;

import frink.expr.Environment;
import frink.expr.Expression;

public interface MatchingContext {
    int beginBacktrackContext();

    int beginMatch();

    void clearState(Backtrackable backtrackable, boolean z, Environment environment);

    void dumpState(Environment environment);

    int getBoundVariableCount();

    Backtrackable getParentState();

    Backtrackable getState(Expression expression, Expression expression2, Environment environment);

    Expression getVariable(String str);

    String getVariableName(int i);

    boolean nextState(Environment environment);

    boolean nextState(Backtrackable backtrackable, Environment environment);

    void pushState(Expression expression, Expression expression2, Backtrackable backtrackable, Backtrackable backtrackable2);

    void rollbackBacktrackContext(int i);

    void rollbackMatch(int i);

    void saveMatches();

    void setParentState(Backtrackable backtrackable);

    void setVariable(String str, Expression expression);
}
