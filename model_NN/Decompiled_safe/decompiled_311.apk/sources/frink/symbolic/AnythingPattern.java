package frink.symbolic;

import frink.expr.AssignableExpression;
import frink.expr.CannotAssignException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.SelfDisplayingExpression;
import frink.expr.TerminalExpression;
import java.util.Hashtable;

public class AnythingPattern extends TerminalExpression implements ExpressionPattern, SelfDisplayingExpression, AssignableExpression {
    private static final boolean DEBUG = false;
    public static final String TYPE = "Anything";
    private static final Hashtable<String, AnythingPattern> patternHash = new Hashtable<>();
    private String name;
    private boolean shouldMatchLiterally = DEBUG;

    public static AnythingPattern construct(String str) {
        AnythingPattern anythingPattern = patternHash.get(str);
        if (anythingPattern != null) {
            return anythingPattern;
        }
        AnythingPattern anythingPattern2 = new AnythingPattern(str);
        patternHash.put(str, anythingPattern2);
        return anythingPattern2;
    }

    private AnythingPattern(String str) {
        if (str == null || str.length() <= 0) {
            this.name = null;
        } else {
            this.name = str;
        }
    }

    public void setShouldMatchLiterally(boolean z) {
        this.shouldMatchLiterally = z;
    }

    public String getName() {
        return this.name;
    }

    public int getSpecificity() {
        return 30;
    }

    public Expression evaluate(Environment environment) {
        environment.outputln("Unexpected eval of AnythingPattern.");
        return this;
    }

    public boolean isConstant() {
        return DEBUG;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if ((expression instanceof AnythingPattern) && this.name != null) {
            return this.name.equals(((ExpressionPattern) expression).getName());
        }
        if (this.shouldMatchLiterally) {
            return DEBUG;
        }
        if (this.name != null) {
            Expression variable = matchingContext.getVariable(this.name);
            if (variable != null) {
                return expression.structureEquals(variable, matchingContext, environment, z);
            }
            matchingContext.setVariable(this.name, expression);
        }
        return true;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String toString(Environment environment, boolean z) {
        if (this.name == null) {
            return "UNNAMED_PATTERN";
        }
        return "_" + this.name;
    }

    public void assign(Expression expression, Environment environment) throws CannotAssignException {
        throw new CannotAssignException("Cannot assign to " + toString(environment, true), this);
    }
}
