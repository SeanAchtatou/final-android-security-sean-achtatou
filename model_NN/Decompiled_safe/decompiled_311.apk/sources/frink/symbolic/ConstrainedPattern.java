package frink.symbolic;

import frink.expr.Constraint;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.SelfDisplayingExpression;
import frink.expr.TerminalExpression;
import frink.expr.UnknownConstraintException;
import java.util.Vector;

public class ConstrainedPattern extends TerminalExpression implements ExpressionPattern, SelfDisplayingExpression {
    public static final String TYPE = "ConstrainedPattern";
    private Vector<String> constraintNames;
    private Vector<Constraint> constraints = null;
    private boolean constraintsInitialized = false;
    private String name;

    public ConstrainedPattern(String str, Vector<String> vector) {
        if (str == null || str.length() <= 0) {
            this.name = null;
        } else {
            this.name = str;
        }
        this.constraintNames = vector;
    }

    public String getName() {
        return this.name;
    }

    public int getSpecificity() {
        return 500;
    }

    public Expression evaluate(Environment environment) {
        environment.outputln("Unexpected eval of ConstrainedPattern.");
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        Expression variable;
        if (this == expression) {
            return true;
        }
        if ((expression instanceof ConstrainedPattern) && this.name != null && this.name.equals(((ConstrainedPattern) expression).getName())) {
            return true;
        }
        if (this.name != null && (variable = matchingContext.getVariable(this.name)) != null) {
            return expression.structureEquals(variable, matchingContext, environment, z) && meetsConstraints(variable, environment);
        }
        if (!meetsConstraints(expression, environment)) {
            return false;
        }
        matchingContext.setVariable(this.name, expression);
        return true;
    }

    private boolean meetsConstraints(Expression expression, Environment environment) {
        if (!this.constraintsInitialized) {
            try {
                initializeConstraints(environment);
            } catch (UnknownConstraintException e) {
                System.out.println("ConstrainedPattern:  Unknown constraint:\n  " + e);
            }
        }
        int size = this.constraints.size();
        for (int i = 0; i < size; i++) {
            if (!this.constraints.elementAt(i).meetsConstraint(expression)) {
                return false;
            }
        }
        return true;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String toString(Environment environment, boolean z) {
        if (this.name == null) {
            return "PATTERN:";
        }
        return "CONSTRAINED:_" + this.name;
    }

    private void initializeConstraints(Environment environment) throws UnknownConstraintException {
        if (!this.constraintsInitialized) {
            synchronized (this) {
                if (!this.constraintsInitialized) {
                    this.constraints = environment.getConstraintFactory().createConstraints(this.constraintNames);
                    this.constraintsInitialized = true;
                }
            }
        }
    }
}
