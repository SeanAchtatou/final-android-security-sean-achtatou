package frink.symbolic;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;

public interface TransformationRuleManager {
    void addRule(TransformationRule transformationRule, Environment environment) throws FrinkSecurityException;

    Expression applyRules(Expression expression, ListExpression listExpression, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException;

    Expression applyRules(Expression expression, String str, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException;

    Expression applyRules(Expression expression, boolean z, boolean z2, Environment environment) throws InvalidChildException, EvaluationException;

    TransformationRuleSet getParsingTransformationRuleSet();

    TransformationRuleSet getTransformationRuleSet(String str);

    void setParsingTransformationRuleSet(String str);
}
