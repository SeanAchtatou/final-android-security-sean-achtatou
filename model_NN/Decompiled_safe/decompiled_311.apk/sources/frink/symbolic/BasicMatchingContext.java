package frink.symbolic;

import frink.expr.Environment;
import frink.expr.Expression;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

public class BasicMatchingContext implements MatchingContext {
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_STATE = false;
    private Backtrackable parentState = null;
    private Stack<StateVector> stateStack = new Stack<>();
    private VariableStack<String, Expression> variableStack = new VariableStack<>();

    public BasicMatchingContext() {
        this.stateStack.push(new StateVector());
    }

    public Expression getVariable(String str) {
        return this.variableStack.getValue(str);
    }

    public int getBoundVariableCount() {
        return this.variableStack.getCurrentItemCount();
    }

    public String getVariableName(int i) {
        return this.variableStack.getKey(i);
    }

    public void setVariable(String str, Expression expression) {
        this.variableStack.setValue(str, expression);
    }

    public Backtrackable getParentState() {
        return this.parentState;
    }

    public void setParentState(Backtrackable backtrackable) {
        this.parentState = backtrackable;
    }

    public int beginMatch() {
        return this.variableStack.beginMatch();
    }

    public void rollbackMatch(int i) {
        this.variableStack.rollbackMatch(i);
    }

    public void saveMatches() {
        this.variableStack.saveMatches();
    }

    public int beginBacktrackContext() {
        this.stateStack.push(new StateVector());
        return this.stateStack.size() - 1;
    }

    public void rollbackBacktrackContext(int i) {
        for (int size = this.stateStack.size() - 1; size >= i; size--) {
            this.stateStack.pop();
        }
    }

    public void pushState(Expression expression, Expression expression2, Backtrackable backtrackable, Backtrackable backtrackable2) {
        this.stateStack.elementAt(this.stateStack.size() - 1).pushState(new StateData(expression, expression2, backtrackable, backtrackable2));
    }

    public Backtrackable getState(Expression expression, Expression expression2, Environment environment) {
        return this.stateStack.elementAt(this.stateStack.size() - 1).getState(expression, expression2, environment);
    }

    public void clearState(Backtrackable backtrackable, boolean z, Environment environment) {
        this.stateStack.elementAt(this.stateStack.size() - 1).clearState(backtrackable, z, environment);
    }

    public boolean nextState(Environment environment) {
        return nextState(null, environment);
    }

    public boolean nextState(Backtrackable backtrackable, Environment environment) {
        return this.stateStack.elementAt(this.stateStack.size() - 1).nextState(backtrackable, environment);
    }

    public void dumpState(Environment environment) {
        this.stateStack.elementAt(this.stateStack.size() - 1).dumpState(environment);
    }

    private static class VariableStack<KeyType, ValueType> {
        public int highestFrameIndex = 0;
        public int lowestFrameIndex = 0;
        public int[] lowestFrames = new int[20];
        private Stack<KeyType> modifiedVariables = null;
        private Hashtable<KeyType, ValueType> variables = null;

        public VariableStack() {
            this.lowestFrames[0] = 0;
        }

        public ValueType getValue(KeyType keytype) {
            if (this.variables != null) {
                return this.variables.get(keytype);
            }
            return null;
        }

        public int getCurrentItemCount() {
            return this.highestFrameIndex - this.lowestFrames[this.lowestFrameIndex];
        }

        public KeyType getKey(int i) {
            return this.modifiedVariables.elementAt(this.lowestFrames[this.lowestFrameIndex] + i);
        }

        public void setValue(KeyType keytype, ValueType valuetype) {
            if (this.variables == null) {
                this.variables = new Hashtable<>();
            }
            if (this.variables.get(keytype) == null) {
                if (this.modifiedVariables == null) {
                    this.modifiedVariables = new Stack<>();
                }
                this.modifiedVariables.push(keytype);
                this.highestFrameIndex++;
            }
            this.variables.put(keytype, valuetype);
        }

        public int beginMatch() {
            int i = this.lowestFrameIndex;
            this.lowestFrameIndex++;
            int length = this.lowestFrames.length;
            if (this.lowestFrameIndex >= length) {
                int[] iArr = new int[(length * 2)];
                System.arraycopy(this.lowestFrames, 0, iArr, 0, length);
                this.lowestFrames = iArr;
            }
            this.lowestFrames[this.lowestFrameIndex] = this.highestFrameIndex;
            return i;
        }

        public void rollbackMatch(int i) {
            while (this.lowestFrameIndex > i) {
                rollbackMatch();
            }
        }

        private void rollbackMatch() {
            int i = this.lowestFrames[this.lowestFrameIndex];
            this.lowestFrameIndex--;
            for (int i2 = this.highestFrameIndex - 1; i2 >= i; i2--) {
                KeyType pop = this.modifiedVariables.pop();
                if (this.variables != null) {
                    this.variables.remove(pop);
                }
            }
            this.highestFrameIndex = i;
        }

        public void saveMatches() {
            this.lowestFrames[this.lowestFrameIndex] = this.highestFrameIndex;
        }

        public void clear(KeyType keytype) {
            if (this.variables.remove(keytype) != null) {
                this.highestFrameIndex--;
                int lastIndexOf = this.modifiedVariables.lastIndexOf(keytype);
                if (lastIndexOf == -1) {
                    System.err.println("Removing unfound state");
                } else {
                    this.modifiedVariables.removeElementAt(lastIndexOf);
                }
            }
        }
    }

    private static class StateData {
        public Expression a;
        public Expression b;
        public Backtrackable parentState;
        public Backtrackable state;

        public StateData(Expression expression, Expression expression2, Backtrackable backtrackable, Backtrackable backtrackable2) {
            this.a = expression;
            this.b = expression2;
            this.state = backtrackable;
            this.parentState = backtrackable2;
        }
    }

    private static class StateVector {
        private Vector<StateData> vec = null;

        public void pushState(StateData stateData) {
            if (this.vec == null) {
                this.vec = new Vector<>();
            }
            this.vec.addElement(stateData);
        }

        public Backtrackable getState(Expression expression, Expression expression2, Environment environment) {
            if (this.vec == null) {
                return null;
            }
            int size = this.vec.size();
            for (int i = 0; i < size; i++) {
                StateData elementAt = this.vec.elementAt(i);
                if (elementAt.a == expression && elementAt.b == expression2) {
                    return elementAt.state;
                }
            }
            return null;
        }

        /* access modifiers changed from: private */
        public void dumpState(Environment environment) {
            if (this.vec != null) {
                int size = this.vec.size();
                for (int i = 0; i < size; i++) {
                    StateData elementAt = this.vec.elementAt(i);
                    System.err.print("  " + i + ": " + environment.format(elementAt.a) + ", " + environment.format(elementAt.b) + "=" + elementAt.state.getStateAsString());
                }
            }
            System.err.println();
        }

        public boolean nextState(Backtrackable backtrackable, Environment environment) {
            if (this.vec == null) {
                return false;
            }
            for (int size = this.vec.size() - 1; size >= 0; size--) {
                Backtrackable backtrackable2 = this.vec.elementAt(size).state;
                if (backtrackable2 == backtrackable) {
                    return false;
                }
                if (backtrackable2.nextState(environment)) {
                    return true;
                }
                this.vec.removeElementAt(size);
            }
            return false;
        }

        public void clearState(Backtrackable backtrackable, boolean z, Environment environment) {
            if (this.vec != null) {
                clearChildren(backtrackable);
                int size = this.vec.size() - 1;
                if (!z) {
                    for (int i = size; i >= 0; i--) {
                        if (backtrackable == this.vec.elementAt(i).state) {
                            this.vec.removeElementAt(i);
                        }
                    }
                }
            }
        }

        public void clearChildren(Backtrackable backtrackable) {
            int size = this.vec.size() - 1;
            while (size >= 0) {
                StateData elementAt = this.vec.elementAt(size);
                if (elementAt.parentState == backtrackable) {
                    this.vec.removeElementAt(size);
                    clearChildren(elementAt.state);
                    size = this.vec.size() - 1;
                } else {
                    size--;
                }
            }
        }

        public void clear() {
            if (this.vec != null) {
                this.vec.removeAllElements();
            }
        }
    }
}
