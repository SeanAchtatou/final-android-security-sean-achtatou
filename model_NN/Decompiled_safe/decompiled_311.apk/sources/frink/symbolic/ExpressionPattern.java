package frink.symbolic;

public interface ExpressionPattern {
    public static final int SPECIFICITY_ANYTHING = 30;
    public static final int SPECIFICITY_CONSTRAINED_PATTERN = 500;
    public static final int SPECIFICITY_LITERAL = 1000;

    String getName();

    int getSpecificity();
}
