package frink.symbolic;

import frink.errors.NotComparableException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.ThreeWayComparison;
import frink.function.Orderer;
import frink.numeric.OverlapException;

public class SymbolComplexityOrderer implements Orderer {
    public static final SymbolComplexityOrderer INSTANCE = new SymbolComplexityOrderer();

    private SymbolComplexityOrderer() {
    }

    public int compare(Expression expression, Expression expression2, Environment environment) {
        try {
            int compare = ThreeWayComparison.compare(expression.getChild(1), expression2.getChild(1), environment);
            if (compare != 0) {
                return compare;
            }
            return ThreeWayComparison.compare(expression.getChild(0), expression2.getChild(0), environment);
        } catch (InvalidChildException e) {
            environment.outputln("Unexpected InvalidChildException when sorting in getUnknownsByComplexity.\n" + e);
            return 0;
        } catch (NotComparableException e2) {
            environment.outputln("Unexpected NotComparableException when sorting in getUnknownsByComplexity.\n" + e2);
            return 0;
        } catch (OverlapException e3) {
            environment.outputln("Unexpected OverlapException when sorting in getUnknownsByComplexity.\n" + e3);
            return 0;
        }
    }
}
