package frink.symbolic;

import frink.errors.ConformanceException;
import frink.expr.AddExpression;
import frink.expr.AssignableExpression;
import frink.expr.AssignmentExpression;
import frink.expr.BasicListExpression;
import frink.expr.BooleanAlgebraExpression;
import frink.expr.BooleanAlgebraExpressionType;
import frink.expr.BuiltinObjectSource;
import frink.expr.ComparisonExpression;
import frink.expr.ComparisonType;
import frink.expr.ConstructableExpression;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.EvaluationNumericException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.ModulusExpression;
import frink.expr.MultiplyExpression;
import frink.expr.NewExpression;
import frink.expr.PowerExpression;
import frink.expr.RangeExpression;
import frink.expr.SolveExpression;
import frink.expr.StringExpression;
import frink.expr.SymbolExpression;
import frink.function.FunctionCallExpression;
import frink.numeric.NumericException;
import frink.object.FrinkObject;
import frink.object.MethodCallExpression;
import java.util.Enumeration;
import java.util.Hashtable;

public class ExpressionConstructor {
    private static final Hashtable<String, ConstructableExpression> constructors = new Hashtable<>();
    private static boolean initialized = false;

    public static void addConstructor(String str, ConstructableExpression constructableExpression) {
        constructors.put(str, constructableExpression);
    }

    public static Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
        if (!initialized) {
            initializeConstructors();
        }
        ConstructableExpression constructableExpression = constructors.get(str);
        if (constructableExpression != null) {
            return constructableExpression.construct(str, listExpression, environment);
        }
        environment.outputln("Error:  No expression constructor implemented yet for (" + str + " " + environment.format(listExpression) + ").\nIf this is critical, please report this to eliasen@mindspring.com .");
        return null;
    }

    private static synchronized void initializeConstructors() {
        synchronized (ExpressionConstructor.class) {
            if (!initialized) {
                constructors.put(AddExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationNumericException, EvaluationConformanceException, InvalidChildException {
                        try {
                            return AddExpression.construct(listExpression, environment);
                        } catch (ConformanceException e) {
                            throw new EvaluationConformanceException("ExpressionConstructor: conformance error in construction of AddExpression:\n " + e, listExpression);
                        } catch (NumericException e2) {
                            throw new EvaluationNumericException("ExpressionConstructor: numeric error in construction of AddExpression:\n " + e2, listExpression);
                        }
                    }
                });
                constructors.put(MultiplyExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
                        try {
                            return MultiplyExpression.construct(listExpression, environment);
                        } catch (NumericException e) {
                            throw new EvaluationNumericException("ExpressionConstructor: numeric error in construction of MultiplyExpression:\n " + e, listExpression);
                        } catch (ConformanceException e2) {
                            throw new EvaluationNumericException("ExpressionConstructor: Conformance error in construction of MultiplyExpression:\n " + e2, listExpression);
                        }
                    }
                });
                constructors.put(PowerExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
                        if (listExpression.getChildCount() != 2) {
                            throw new InvalidArgumentException("ExpressionConstructor: PowerExpression constructor requires 2 arguments.", listExpression);
                        }
                        try {
                            return PowerExpression.construct(listExpression.getChild(0), listExpression.getChild(1), environment);
                        } catch (NumericException e) {
                            throw new EvaluationNumericException("ExpressionConstructor: unexpected NumericException:\n " + e, listExpression);
                        }
                    }
                });
                constructors.put("Function", new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws InvalidArgumentException, InvalidChildException {
                        int childCount = listExpression.getChildCount();
                        Expression child = listExpression.getChild(0);
                        if (childCount < 1 || !(child instanceof StringExpression)) {
                            throw new InvalidArgumentException("ExpressionConstructor: Argument 0 should be name of the function.", listExpression);
                        }
                        String string = ((StringExpression) child).getString();
                        BasicListExpression basicListExpression = new BasicListExpression(childCount - 1);
                        for (int i = 1; i < childCount; i++) {
                            basicListExpression.appendChild(listExpression.getChild(i));
                        }
                        return new FunctionCallExpression(string, basicListExpression);
                    }
                });
                constructors.put(SymbolExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws InvalidArgumentException, InvalidChildException {
                        int childCount = listExpression.getChildCount();
                        Expression child = listExpression.getChild(0);
                        if (childCount >= 1 && (child instanceof StringExpression)) {
                            return new SymbolExpression(((StringExpression) child).getString());
                        }
                        throw new InvalidArgumentException("ExpressionConstructor for SymbolExpression:  The first argument in the list of children should be name of the symbol.", listExpression);
                    }
                });
                constructors.put(AssignmentExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationNumericException, EvaluationConformanceException, InvalidChildException {
                        return new AssignmentExpression((AssignableExpression) listExpression.getChild(0), listExpression.getChild(1));
                    }
                });
                constructors.put(ListExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) {
                        return listExpression;
                    }
                });
                constructors.put(ModulusExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
                        return new ModulusExpression(listExpression.getChild(0), listExpression.getChild(1));
                    }
                });
                constructors.put("TransformationRule", new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
                        if (listExpression.getChildCount() == 2) {
                            return new BasicTransformationRule(listExpression.getChild(0), listExpression.getChild(1), null);
                        }
                        return new BasicTransformationRule(listExpression.getChild(0), listExpression.getChild(1), listExpression.getChild(2));
                    }
                });
                constructors.put(MethodCallExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws InvalidArgumentException, InvalidChildException {
                        if (listExpression.getChildCount() != 2) {
                            throw new InvalidArgumentException("ExpressionConstructor for MethodCallExpression: must have 2 children", listExpression);
                        }
                        Expression child = listExpression.getChild(0);
                        Expression child2 = listExpression.getChild(1);
                        if (!(child instanceof FrinkObject)) {
                            throw new InvalidArgumentException("ExpressionConstructor for MethodCallExpression: Argument 0 should be a FrinkObject.", listExpression);
                        } else if (child2 instanceof FunctionCallExpression) {
                            return new MethodCallExpression(child, (FunctionCallExpression) child2);
                        } else {
                            throw new InvalidArgumentException("ExpressionConstructor for MethodCallExpression: Argument 1 should be a FunctionCallExpression", listExpression);
                        }
                    }
                });
                constructors.put(SolveExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
                        return new SolveExpression(listExpression.getChild(0), listExpression.getChild(1));
                    }
                });
                constructors.put("new", new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws InvalidArgumentException, InvalidChildException {
                        int childCount = listExpression.getChildCount();
                        Expression child = listExpression.getChild(0);
                        if (childCount < 1 || !(child instanceof StringExpression)) {
                            throw new InvalidArgumentException("ExpressionConstructor: Argument 0 should be name of the object to create.", listExpression);
                        }
                        String string = ((StringExpression) child).getString();
                        if (childCount <= 1) {
                            return new NewExpression(string);
                        }
                        BasicListExpression basicListExpression = new BasicListExpression(childCount - 1);
                        for (int i = 1; i < childCount; i++) {
                            basicListExpression.appendChild(listExpression.getChild(i));
                        }
                        return new NewExpression(string, basicListExpression);
                    }
                });
                constructors.put(RangeExpression.TYPE, new ConstructableExpression() {
                    public Expression construct(String str, ListExpression listExpression, Environment environment) throws InvalidArgumentException, InvalidChildException, EvaluationException {
                        return BuiltinObjectSource.constructRange(listExpression, environment);
                    }
                });
                Enumeration<String> keys = ComparisonType.getKeys();
                while (keys.hasMoreElements()) {
                    AnonymousClass14 r3 = new ConstructableExpression() {
                        public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationNumericException, EvaluationConformanceException, InvalidChildException {
                            ComparisonExpression comparisonExpression = new ComparisonExpression(ComparisonType.getComparisonType(str));
                            int childCount = listExpression.getChildCount();
                            for (int i = 0; i < childCount; i++) {
                                comparisonExpression.appendChild(listExpression.getChild(i));
                            }
                            return comparisonExpression;
                        }
                    };
                    constructors.put(keys.nextElement(), r3);
                }
                Enumeration<String> keys2 = BooleanAlgebraExpressionType.getKeys();
                while (keys2.hasMoreElements()) {
                    AnonymousClass15 r32 = new ConstructableExpression() {
                        public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationNumericException, EvaluationConformanceException, InvalidChildException {
                            return BooleanAlgebraExpression.construct(BooleanAlgebraExpressionType.getBooleanAlgebraExpressionType(str), listExpression, environment);
                        }
                    };
                    constructors.put(keys2.nextElement(), r32);
                }
                initialized = true;
            }
        }
    }
}
