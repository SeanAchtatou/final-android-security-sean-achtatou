package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;

public abstract class FourArgFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException;

    public FourArgFunction(boolean z) {
        super(4, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        try {
            return doFunction(environment, expression.getChild(0), expression.getChild(1), expression.getChild(2), expression.getChild(3));
        } catch (InvalidChildException e) {
            throw new InvalidArgumentException("Bad child", expression);
        }
    }
}
