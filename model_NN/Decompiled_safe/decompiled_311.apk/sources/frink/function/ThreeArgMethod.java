package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public abstract class ThreeArgMethod<T extends Expression> extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doMethod(Environment environment, T t, Expression expression, Expression expression2, Expression expression3) throws EvaluationException;

    public ThreeArgMethod(boolean z) {
        super(3, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return doMethod(environment, environment.getSymbolDefinition("this", false).getValue(), expression.getChild(0), expression.getChild(1), expression.getChild(2));
    }
}
