package frink.function;

import frink.expr.EnumerationStacker;
import frink.expr.Environment;
import frink.expr.ListExpression;
import frink.object.FrinkObject;
import java.util.Enumeration;
import java.util.Stack;

public class BasicFunctionManager implements FunctionManager {
    private static final boolean DEBUG = false;
    private int highestFrame;
    private int[] lowestArray = new int[16];
    private int lowestFrame;
    private Stack<FrinkObject> objStack;
    private Stack<FunctionSource> sources = new Stack<>();

    public BasicFunctionManager() {
        this.lowestArray[0] = 0;
        this.lowestFrame = 0;
        this.highestFrame = 0;
        this.objStack = new Stack<>();
        pushObject(null);
    }

    private FunctionDefinition getBestMatch(String str, ListExpression listExpression, Environment environment) {
        FunctionDefinition functionDefinition;
        int childCount = listExpression.getChildCount();
        int size = this.sources.size() - 1;
        FunctionDefinition functionDefinition2 = null;
        while (size >= 0) {
            FunctionSource elementAt = this.sources.elementAt(size);
            if (elementAt == null || (functionDefinition = elementAt.getBestMatch(str, listExpression, environment)) == null) {
                functionDefinition = functionDefinition2;
            } else if (childCount == functionDefinition.getArgumentCount()) {
                return functionDefinition;
            }
            size--;
            functionDefinition2 = functionDefinition;
        }
        return functionDefinition2;
    }

    public FunctionDefinition getBestMatch(String str, int i, Environment environment) throws RequiresArgumentsException {
        FunctionDefinition functionDefinition;
        int size = this.sources.size() - 1;
        FunctionDefinition functionDefinition2 = null;
        while (size >= 0) {
            FunctionSource elementAt = this.sources.elementAt(size);
            if (elementAt == null || (functionDefinition = elementAt.getBestMatch(str, i, environment)) == null) {
                functionDefinition = functionDefinition2;
            } else if (i == functionDefinition.getArgumentCount()) {
                return functionDefinition;
            }
            size--;
            functionDefinition2 = functionDefinition;
        }
        return functionDefinition2;
    }

    public void addFunctionSource(FunctionSource functionSource, boolean z) {
        this.highestFrame++;
        if (z) {
            this.lowestFrame = this.highestFrame;
        }
        synchronized (this.lowestArray) {
            if (this.highestFrame >= this.lowestArray.length) {
                enlargeLowestArray();
            }
            this.lowestArray[this.highestFrame] = this.lowestFrame;
        }
        this.sources.push(functionSource);
    }

    public void popFunctionSource() {
        if (this.sources.size() == 0) {
            System.out.println("BasicFunctionManager: attempted to pop empty stack");
            return;
        }
        this.highestFrame--;
        this.sources.pop();
        this.lowestFrame = this.lowestArray[this.highestFrame];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.BasicFunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.ListExpression, int, frink.object.FrinkObject, boolean]
     candidates:
      frink.function.BasicFunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.BasicFunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.BasicFunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.BasicListExpression, int, frink.object.FrinkObject, boolean]
     candidates:
      frink.function.BasicFunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.BasicFunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0048 A[LOOP:0: B:22:0x0046->B:23:0x0048, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public frink.expr.Expression execute(java.lang.String r9, frink.expr.Environment r10, frink.expr.ListExpression r11, frink.object.FrinkObject r12, boolean r13, frink.function.FunctionCacher r14) throws frink.expr.EvaluationException {
        /*
            r8 = this;
            if (r11 == 0) goto L_0x0026
            int r0 = r11.getChildCount()
            r7 = r0
        L_0x0007:
            if (r12 != 0) goto L_0x00b4
            frink.object.FrinkObject r0 = r8.getObject()     // Catch:{ RequiresArgumentsException -> 0x003e }
            r5 = r0
        L_0x000e:
            if (r5 == 0) goto L_0x0029
            frink.function.FunctionSource r0 = r5.getFunctionSource(r10)     // Catch:{ RequiresArgumentsException -> 0x00af }
            if (r0 == 0) goto L_0x0029
            frink.function.FunctionDefinition r1 = r0.getBestMatch(r9, r7, r10)     // Catch:{ RequiresArgumentsException -> 0x00af }
            if (r1 == 0) goto L_0x0029
            r4 = 1
            r0 = r8
            r2 = r10
            r3 = r11
            r6 = r13
            frink.expr.Expression r0 = r0.execute(r1, r2, r3, r4, r5, r6)     // Catch:{ RequiresArgumentsException -> 0x00af }
        L_0x0025:
            return r0
        L_0x0026:
            r0 = 0
            r7 = r0
            goto L_0x0007
        L_0x0029:
            frink.function.FunctionDefinition r1 = r8.getBestMatch(r9, r7, r10)     // Catch:{ RequiresArgumentsException -> 0x00af }
            if (r1 == 0) goto L_0x008f
            if (r14 == 0) goto L_0x0034
            r14.setCachedFunction(r1)     // Catch:{ RequiresArgumentsException -> 0x00af }
        L_0x0034:
            r4 = 1
            r0 = r8
            r2 = r10
            r3 = r11
            r6 = r13
            frink.expr.Expression r0 = r0.execute(r1, r2, r3, r4, r5, r6)     // Catch:{ RequiresArgumentsException -> 0x00af }
            goto L_0x0025
        L_0x003e:
            r0 = move-exception
            r0 = r12
        L_0x0040:
            frink.expr.BasicListExpression r3 = new frink.expr.BasicListExpression
            r3.<init>(r7)
            r1 = 0
        L_0x0046:
            if (r1 >= r7) goto L_0x0056
            frink.expr.Expression r2 = r11.getChild(r1)
            frink.expr.Expression r2 = r2.evaluate(r10)
            r3.setChild(r1, r2)
            int r1 = r1 + 1
            goto L_0x0046
        L_0x0056:
            if (r0 != 0) goto L_0x00b2
            frink.object.FrinkObject r0 = r8.getObject()
            r5 = r0
        L_0x005d:
            if (r5 == 0) goto L_0x007a
            frink.function.FunctionSource r0 = r5.getFunctionSource(r10)
            if (r0 == 0) goto L_0x007a
            frink.function.FunctionDefinition r1 = r0.getBestMatch(r9, r3, r10)
            if (r1 == 0) goto L_0x007a
            if (r14 == 0) goto L_0x0071
            r0 = 0
            r14.setCachedFunction(r0)
        L_0x0071:
            r4 = 0
            r0 = r8
            r2 = r10
            r6 = r13
            frink.expr.Expression r0 = r0.execute(r1, r2, r3, r4, r5, r6)
            goto L_0x0025
        L_0x007a:
            frink.function.FunctionDefinition r1 = r8.getBestMatch(r9, r3, r10)
            if (r1 == 0) goto L_0x008f
            if (r14 == 0) goto L_0x0086
            r0 = 0
            r14.setCachedFunction(r0)
        L_0x0086:
            r4 = 0
            r0 = r8
            r2 = r10
            r6 = r13
            frink.expr.Expression r0 = r0.execute(r1, r2, r3, r4, r5, r6)
            goto L_0x0025
        L_0x008f:
            frink.function.NoSuchFunctionException r0 = new frink.function.NoSuchFunctionException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Function "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = " not found that matches argument list."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r2 = 0
            r0.<init>(r9, r1, r2)
            throw r0
        L_0x00af:
            r0 = move-exception
            r0 = r5
            goto L_0x0040
        L_0x00b2:
            r5 = r0
            goto L_0x005d
        L_0x00b4:
            r5 = r12
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.function.BasicFunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v0, resolved type: frink.expr.ListExpression} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: frink.expr.ListExpression} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: frink.expr.BasicListExpression} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v2, resolved type: frink.expr.ListExpression} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: frink.expr.ListExpression} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public frink.expr.Expression execute(frink.function.FunctionDefinition r19, frink.expr.Environment r20, frink.expr.Expression r21, boolean r22, frink.object.FrinkObject r23, boolean r24) throws frink.expr.EvaluationException {
        /*
            r18 = this;
            boolean r5 = r19.shouldEvaluateFirst()
            if (r5 != 0) goto L_0x01ae
            r5 = 0
        L_0x0007:
            r6 = 0
            if (r21 != 0) goto L_0x0060
            r7 = 0
        L_0x000b:
            int r8 = r19.getArgumentCount()
            boolean r9 = r19.hasNamedArguments()
            if (r8 <= 0) goto L_0x006f
            if (r9 == 0) goto L_0x006f
            frink.expr.BasicContextFrame r10 = new frink.expr.BasicContextFrame
            r10.<init>()
        L_0x001c:
            r11 = 0
            r12 = 0
            if (r5 == 0) goto L_0x0071
            frink.expr.BasicListExpression r13 = new frink.expr.BasicListExpression
            r13.<init>(r8)
        L_0x0025:
            r14 = 0
            r17 = r14
            r14 = r11
            r11 = r17
        L_0x002b:
            if (r11 >= r8) goto L_0x0134
            r0 = r19
            r1 = r11
            frink.function.FunctionArgument r15 = r0.getArgument(r1)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            if (r11 >= r7) goto L_0x0091
            if (r6 == 0) goto L_0x0089
            r16 = r21
        L_0x003a:
            if (r5 == 0) goto L_0x00d7
            r0 = r16
            r1 = r20
            frink.expr.Expression r12 = r0.evaluate(r1)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
        L_0x0044:
            r13.setChild(r11, r12)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            if (r9 == 0) goto L_0x005d
            java.lang.String r14 = r15.getName()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            if (r14 == 0) goto L_0x005d
            java.util.Vector r15 = r15.getConstraints()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            if (r15 != 0) goto L_0x00db
            r0 = r10
            r1 = r14
            r2 = r12
            r3 = r20
            r0.setSymbolDefinition(r1, r2, r3)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
        L_0x005d:
            int r11 = r11 + 1
            goto L_0x002b
        L_0x0060:
            r0 = r21
            boolean r0 = r0 instanceof frink.expr.ListExpression
            r7 = r0
            if (r7 == 0) goto L_0x006c
            int r7 = r21.getChildCount()
            goto L_0x000b
        L_0x006c:
            r6 = 1
            r7 = 1
            goto L_0x000b
        L_0x006f:
            r10 = 0
            goto L_0x001c
        L_0x0071:
            if (r6 == 0) goto L_0x0080
            frink.expr.BasicListExpression r13 = new frink.expr.BasicListExpression
            r14 = 1
            r13.<init>(r14)
            r0 = r13
            r1 = r21
            r0.appendChild(r1)
            goto L_0x0025
        L_0x0080:
            r0 = r21
            frink.expr.ListExpression r0 = (frink.expr.ListExpression) r0
            r22 = r0
            r13 = r22
            goto L_0x0025
        L_0x0089:
            r0 = r21
            r1 = r11
            frink.expr.Expression r16 = r0.getChild(r1)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            goto L_0x003a
        L_0x0091:
            frink.expr.Expression r16 = r15.getDefaultValue()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            if (r16 != 0) goto L_0x003a
            frink.function.FunctionCallException r5 = new frink.function.FunctionCallException     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            r6.<init>()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.String r7 = "Argument mismatch--argument number "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            int r7 = r11 + 1
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.String r7 = " ("
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.String r7 = r15.getName()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.String r7 = ") was passed no value and has no default value."
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            java.lang.String r6 = r6.toString()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            r0 = r5
            r1 = r6
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            throw r5     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
        L_0x00ca:
            r5 = move-exception
            frink.function.FunctionCallException r5 = new frink.function.FunctionCallException
            java.lang.String r6 = "Unexpectedly short arguments mismatch"
            r0 = r5
            r1 = r6
            r2 = r21
            r0.<init>(r1, r2)
            throw r5
        L_0x00d7:
            r12 = r16
            goto L_0x0044
        L_0x00db:
            frink.expr.ConstraintFactory r16 = r20.getConstraintFactory()     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            r0 = r16
            r1 = r15
            java.util.Vector r15 = r0.createConstraints(r1)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            r0 = r10
            r1 = r14
            r2 = r15
            r3 = r12
            r4 = r20
            r0.declareVariable(r1, r2, r3, r4)     // Catch:{ InvalidChildException -> 0x00ca, ConstraintException -> 0x00f1 }
            goto L_0x005d
        L_0x00f1:
            r5 = move-exception
            r6 = r12
            r7 = r14
            frink.expr.InvalidArgumentException r8 = new frink.expr.InvalidArgumentException
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Invalid argument "
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r20
            r1 = r6
            java.lang.String r10 = r0.format(r1)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = " passed as argument number "
            java.lang.StringBuilder r9 = r9.append(r10)
            int r10 = r11 + 1
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = " ("
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r7 = r9.append(r7)
            java.lang.String r9 = "):\n  "
            java.lang.StringBuilder r7 = r7.append(r9)
            java.lang.StringBuilder r5 = r7.append(r5)
            java.lang.String r5 = r5.toString()
            r8.<init>(r5, r6)
            throw r8
        L_0x0134:
            boolean r5 = r19.shouldEvaluateFirst()
            if (r5 != 0) goto L_0x01ab
            r5 = 0
        L_0x013b:
            r6 = 0
            if (r23 == 0) goto L_0x0190
            r0 = r18
            r1 = r23
            r0.pushObject(r1)
            r0 = r23
            r1 = r20
            frink.expr.ContextFrame r6 = r0.getContextFrame(r1)
            if (r6 == 0) goto L_0x0188
            r0 = r20
            r1 = r6
            r2 = r5
            r0.addContextFrame(r1, r2)
            r7 = 0
            r0 = r20
            r1 = r10
            r2 = r7
            r0.addContextFrame(r1, r2)
        L_0x015e:
            r0 = r23
            r1 = r20
            frink.function.FunctionSource r7 = r0.getFunctionSource(r1)
            r0 = r18
            r1 = r7
            r2 = r5
            r0.addFunctionSource(r1, r2)
            r5 = r6
        L_0x016e:
            r0 = r19
            r1 = r20
            r2 = r13
            frink.expr.Expression r6 = r0.performEvaluation(r1, r2)     // Catch:{ all -> 0x0199 }
            r20.removeContextFrame()
            if (r23 == 0) goto L_0x0187
            if (r5 == 0) goto L_0x0181
            r20.removeContextFrame()
        L_0x0181:
            r18.popFunctionSource()
            r18.popObject()
        L_0x0187:
            return r6
        L_0x0188:
            r0 = r20
            r1 = r10
            r2 = r5
            r0.addContextFrame(r1, r2)
            goto L_0x015e
        L_0x0190:
            r0 = r20
            r1 = r10
            r2 = r5
            r0.addContextFrame(r1, r2)
            r5 = r6
            goto L_0x016e
        L_0x0199:
            r6 = move-exception
            r20.removeContextFrame()
            if (r23 == 0) goto L_0x01aa
            if (r5 == 0) goto L_0x01a4
            r20.removeContextFrame()
        L_0x01a4:
            r18.popFunctionSource()
            r18.popObject()
        L_0x01aa:
            throw r6
        L_0x01ab:
            r5 = r24
            goto L_0x013b
        L_0x01ae:
            r5 = r22
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.function.BasicFunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression");
    }

    public FunctionSource getFunctionSource(String str) {
        for (int size = this.sources.size() - 1; size >= 0; size--) {
            try {
                FunctionSource elementAt = this.sources.elementAt(size);
                if (elementAt != null && elementAt.getName().equals(str)) {
                    return elementAt;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
        return null;
    }

    private synchronized void enlargeLowestArray() {
        int length = this.lowestArray.length;
        int[] iArr = new int[(length * 2)];
        System.arraycopy(this.lowestArray, 0, iArr, 0, length);
        this.lowestArray = iArr;
    }

    public FrinkObject getObject() {
        return this.objStack.peek();
    }

    public void pushObject(FrinkObject frinkObject) {
        this.objStack.push(frinkObject);
    }

    public FrinkObject popObject() {
        return this.objStack.pop();
    }

    public Enumeration<FunctionDescriptor> getFunctionDescriptors() {
        EnumerationStacker enumerationStacker = new EnumerationStacker();
        synchronized (this.sources) {
            int size = this.sources.size();
            for (int i = 0; i < size; i++) {
                enumerationStacker.addEnumeration(this.sources.elementAt(i).getFunctionDescriptors());
            }
        }
        return enumerationStacker;
    }
}
