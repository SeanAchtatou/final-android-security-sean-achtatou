package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;

public class BasicFunctionDefinition extends AbstractFunctionDefinition {
    private Expression body;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.AbstractFunctionDefinition.<init>(frink.expr.ListExpression, boolean):void
     arg types: [frink.expr.ListExpression, int]
     candidates:
      frink.function.AbstractFunctionDefinition.<init>(int, boolean):void
      frink.function.AbstractFunctionDefinition.<init>(java.lang.reflect.Method, boolean):void
      frink.function.AbstractFunctionDefinition.<init>(frink.expr.ListExpression, boolean):void */
    public BasicFunctionDefinition(ListExpression listExpression, Expression expression) {
        super(listExpression, false);
        this.body = expression;
    }

    public BasicFunctionDefinition(FunctionSignature functionSignature, Expression expression) {
        this(functionSignature.getArgumentList(), expression);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return this.body.evaluate(environment);
    }
}
