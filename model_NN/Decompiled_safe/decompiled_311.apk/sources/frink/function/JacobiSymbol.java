package frink.function;

import frink.errors.NotAnIntegerException;
import frink.expr.InvalidArgumentException;
import frink.numeric.FrinkBigInteger;
import frink.numeric.FrinkInteger;
import java.math.BigInteger;

public class JacobiSymbol {
    public static int jacobiSymbol(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) throws InvalidArgumentException {
        try {
            return jacobiSymbol(frinkInteger.getInt(), frinkInteger2.getInt());
        } catch (NotAnIntegerException e) {
            return jacobiSymbol(frinkInteger.getBigInt(), frinkInteger2.getBigInt());
        }
    }

    public static int jacobiSymbol(int i, int i2) throws InvalidArgumentException {
        int i3;
        int i4;
        int i5;
        if (i2 <= 0 || (i2 & 1) != 1) {
            throw new InvalidArgumentException("Second argument to JacobiSymbol[a,n] must be a positive, odd integer.  Value was " + i2, null);
        }
        int i6 = i % i2;
        if (i6 < 0) {
            i4 = i2;
            i3 = i6 + i2;
            i5 = 1;
        } else {
            i4 = i2;
            i3 = i6;
            i5 = 1;
        }
        while (i3 != 0) {
            while ((i3 & 1) == 0) {
                i3 /= 2;
                int i7 = i4 % 8;
                if (i7 == 3 || i7 == 5) {
                    i5 = -i5;
                }
            }
            if (i4 % 4 == 3 && i3 % 4 == 3) {
                i5 = -i5;
            }
            int i8 = i3;
            i3 = i4 % i3;
            i4 = i8;
        }
        if (i4 == 1) {
            return i5;
        }
        return 0;
    }

    public static int jacobiSymbol(BigInteger bigInteger, BigInteger bigInteger2) throws InvalidArgumentException {
        if (bigInteger2.compareTo(FrinkBigInteger.ZERO) <= 0 || !bigInteger2.testBit(0)) {
            throw new InvalidArgumentException("Second argument to JacobiSymbol[a,n] must be a positive, odd integer.  Value was " + bigInteger2, null);
        }
        BigInteger bigInteger3 = bigInteger2;
        BigInteger mod = bigInteger.mod(bigInteger2);
        int i = 1;
        while (mod.compareTo(FrinkBigInteger.ZERO) != 0) {
            while (!mod.testBit(0)) {
                mod = mod.shiftRight(1);
                BigInteger mod2 = bigInteger3.mod(FrinkBigInteger.EIGHT);
                if (mod2.compareTo(FrinkBigInteger.THREE) == 0 || mod2.compareTo(FrinkBigInteger.FIVE) == 0) {
                    i = -i;
                }
            }
            if (bigInteger3.mod(FrinkBigInteger.FOUR).compareTo(FrinkBigInteger.THREE) == 0 && mod.mod(FrinkBigInteger.FOUR).compareTo(FrinkBigInteger.THREE) == 0) {
                i = -i;
            }
            BigInteger bigInteger4 = mod;
            mod = bigInteger3.mod(mod);
            bigInteger3 = bigInteger4;
        }
        if (bigInteger3.compareTo(FrinkBigInteger.ONE) == 0) {
            return i;
        }
        return 0;
    }
}
