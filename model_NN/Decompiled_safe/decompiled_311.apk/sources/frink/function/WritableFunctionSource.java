package frink.function;

public interface WritableFunctionSource extends FunctionSource {
    void addFunctionDefinition(String str, FunctionDefinition functionDefinition);
}
