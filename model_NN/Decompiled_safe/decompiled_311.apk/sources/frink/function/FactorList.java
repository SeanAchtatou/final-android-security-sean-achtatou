package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.numeric.FrinkInteger;
import java.util.Enumeration;
import java.util.Vector;

public class FactorList {
    private Vector<Factors> factors = new Vector<>();

    public void addFactor(FrinkInteger frinkInteger, int i) {
        int size = this.factors.size();
        int i2 = 0;
        while (i2 < size) {
            int compare = FrinkInteger.compare(frinkInteger, this.factors.elementAt(i2).num);
            if (compare == 0) {
                this.factors.elementAt(i2).power += i;
                return;
            } else if (compare > 0) {
                this.factors.insertElementAt(new Factors(frinkInteger, i), i2);
                return;
            } else {
                i2++;
            }
        }
        this.factors.insertElementAt(new Factors(frinkInteger, i), size);
    }

    public Enumeration<Factors> getEnumeration() {
        return this.factors.elements();
    }

    public Factors getSmallestFactor() {
        return this.factors.elementAt(this.factors.size() - 1);
    }

    public Factors getLargestFactor() {
        return this.factors.elementAt(0);
    }

    public BasicListExpression toExpression() {
        int size = this.factors.size();
        BasicListExpression basicListExpression = new BasicListExpression(size);
        for (int i = size - 1; i >= 0; i--) {
            Factors elementAt = this.factors.elementAt(i);
            BasicListExpression basicListExpression2 = new BasicListExpression(2);
            basicListExpression2.appendChild(DimensionlessUnitExpression.construct(elementAt.num));
            basicListExpression2.appendChild(DimensionlessUnitExpression.construct(elementAt.power));
            basicListExpression.appendChild(basicListExpression2);
        }
        return basicListExpression;
    }

    public static class Factors {
        public FrinkInteger num;
        public int power;

        public Factors(FrinkInteger frinkInteger) {
            this.num = frinkInteger;
            this.power = 1;
        }

        public Factors(FrinkInteger frinkInteger, int i) {
            this.num = frinkInteger;
            this.power = i;
        }

        public FrinkInteger getFactor() {
            return this.num;
        }

        public int getPower() {
            return this.power;
        }
    }
}
