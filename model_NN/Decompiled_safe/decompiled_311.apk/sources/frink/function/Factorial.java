package frink.function;

import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.RealMath;
import java.util.Vector;

public class Factorial {
    private static int largestCached = 10000;
    private static Vector<FrinkInteger> results = null;

    public static FrinkInteger factorial(int i) {
        populateResults(i);
        if (i <= largestCached) {
            return results.elementAt(i);
        }
        int i2 = largestCached + 1;
        FrinkInteger elementAt = results.elementAt(largestCached);
        for (int i3 = i2; i3 <= i; i3++) {
            elementAt = RealMath.multiplyInts(elementAt, FrinkInteger.construct(i3));
        }
        return elementAt;
    }

    private static synchronized void populateResults(int i) {
        int i2;
        int size;
        synchronized (Factorial.class) {
            if (i < 1) {
                i2 = 1;
            } else {
                i2 = i;
            }
            if (i2 > largestCached) {
                i2 = largestCached;
            }
            if (results == null) {
                results = new Vector<>(i2 + 1);
                results.addElement(FrinkInt.ONE);
                results.addElement(FrinkInt.ONE);
                size = 2;
            } else {
                size = results.size();
                results.ensureCapacity(i2 + 1);
            }
            FrinkInteger elementAt = results.elementAt(size - 1);
            while (size <= i2) {
                elementAt = RealMath.multiplyInts(elementAt, FrinkInteger.construct(size));
                results.addElement(elementAt);
                size++;
            }
        }
    }
}
