package frink.function;

import frink.expr.Expression;

public class NoSuchFunctionException extends FunctionCallException {
    private String functionName;

    public NoSuchFunctionException(String str, String str2, Expression expression) {
        super(str2, expression);
        this.functionName = str;
    }

    public String getFunctionName() {
        return this.functionName;
    }
}
