package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.numeric.NumericException;

public abstract class DoubleArgFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, NumericException;

    public DoubleArgFunction(boolean z) {
        super(2, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException, NumericException {
        try {
            return doFunction(environment, expression.getChild(0), expression.getChild(1));
        } catch (InvalidChildException e) {
            throw new InvalidArgumentException("Bad child", expression);
        }
    }
}
