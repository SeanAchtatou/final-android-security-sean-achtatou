package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;
import java.util.Random;

public class Sorter {
    static Random rand = new Random();

    private Sorter() {
    }

    public static void sort(ListExpression listExpression, Orderer orderer, Environment environment) throws EvaluationException {
        synchronized (listExpression) {
            sort(listExpression, 0, listExpression.getChildCount() - 1, orderer, environment);
        }
    }

    private static void sort(ListExpression listExpression, int i, int i2, Orderer orderer, Environment environment) throws EvaluationException {
        if (i2 > i) {
            int partition = partition(listExpression, i, i2, orderer, environment);
            sort(listExpression, i, partition - 1, orderer, environment);
            sort(listExpression, partition + 1, i2, orderer, environment);
        }
    }

    private static int partition(ListExpression listExpression, int i, int i2, Orderer orderer, Environment environment) throws EvaluationException {
        int nextInt = ((rand.nextInt() >>> 1) % ((i2 - i) + 1)) + i;
        Expression child = listExpression.getChild(nextInt);
        if (nextInt != i2) {
            swap(listExpression, nextInt, i2);
        }
        int i3 = i - 1;
        int i4 = i2;
        while (true) {
            i3++;
            if (orderer.compare(child, listExpression.getChild(i3), environment) != 1 || i3 == i2) {
                do {
                    i4--;
                    if (orderer.compare(child, listExpression.getChild(i4), environment) != -1) {
                        break;
                    }
                } while (i4 != i);
                if (i3 >= i4) {
                    swap(listExpression, i3, i2);
                    return i3;
                }
                swap(listExpression, i3, i4);
            }
        }
    }

    private static void swap(ListExpression listExpression, int i, int i2) throws EvaluationException {
        Expression child = listExpression.getChild(i);
        listExpression.setChild(i, listExpression.getChild(i2));
        listExpression.setChild(i2, child);
    }
}
