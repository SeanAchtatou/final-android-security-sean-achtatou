package frink.function;

import frink.errors.NotComparableException;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.ThreeWayComparison;
import frink.numeric.OverlapException;

public class DefaultOrderer implements Orderer {
    public static final DefaultOrderer INSTANCE = new DefaultOrderer();

    private DefaultOrderer() {
    }

    public int compare(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        try {
            return ThreeWayComparison.compare(expression, expression2, environment);
        } catch (NotComparableException e) {
            throw new InvalidArgumentException("Sorter.compare: Arguments were not comparable:\n " + e, expression);
        } catch (OverlapException e2) {
            throw new InvalidArgumentException("Sorter.compare: Arguments were not orderable:\n " + e2, expression);
        }
    }
}
