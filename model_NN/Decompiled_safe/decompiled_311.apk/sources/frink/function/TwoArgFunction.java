package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public abstract class TwoArgFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException;

    public TwoArgFunction(boolean z) {
        super(2, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return doFunction(environment, expression.getChild(0), expression.getChild(1));
    }
}
