package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public interface Orderer {
    int compare(Expression expression, Expression expression2, Environment environment) throws EvaluationException;
}
