package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;
import frink.object.FrinkObject;
import java.util.Enumeration;

public interface FunctionManager {
    void addFunctionSource(FunctionSource functionSource, boolean z);

    Expression execute(FunctionDefinition functionDefinition, Environment environment, Expression expression, boolean z, FrinkObject frinkObject, boolean z2) throws EvaluationException;

    Expression execute(String str, Environment environment, ListExpression listExpression, FrinkObject frinkObject, boolean z, FunctionCacher functionCacher) throws EvaluationException;

    FunctionDefinition getBestMatch(String str, int i, Environment environment) throws RequiresArgumentsException;

    Enumeration<FunctionDescriptor> getFunctionDescriptors();

    FunctionSource getFunctionSource(String str);

    FrinkObject getObject();

    void popFunctionSource();

    FrinkObject popObject();

    void pushObject(FrinkObject frinkObject);
}
