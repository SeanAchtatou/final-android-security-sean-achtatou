package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public abstract class SixArgMethod<T extends Expression> extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doMethod(Environment environment, T t, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5, Expression expression6) throws EvaluationException;

    public SixArgMethod(boolean z) {
        super(6, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return doMethod(environment, environment.getSymbolDefinition("this", false).getValue(), expression.getChild(0), expression.getChild(1), expression.getChild(2), expression.getChild(3), expression.getChild(4), expression.getChild(5));
    }
}
