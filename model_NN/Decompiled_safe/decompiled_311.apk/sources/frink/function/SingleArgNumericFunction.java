package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.InvalidArgumentException;
import frink.expr.ListExpression;
import frink.expr.UnitExpression;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;

public abstract class SingleArgNumericFunction extends AbstractFunctionDefinition {
    private Unit inUnit;
    private String inUnitName;
    private boolean initialized = false;
    private Unit outUnit;
    private String outUnitName;

    /* access modifiers changed from: protected */
    public abstract Numeric doFunction(Environment environment, Numeric numeric) throws EvaluationException, NumericException;

    public SingleArgNumericFunction(String str, String str2, boolean z) {
        super(1, z);
        this.inUnitName = str;
        this.outUnitName = str2;
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException, NumericException {
        if (!this.initialized) {
            initializeUnits(environment);
        }
        Expression child = expression.getChild(0);
        if (isMappable()) {
            if (child instanceof ListExpression) {
                ListExpression listExpression = (ListExpression) child;
                int childCount = listExpression.getChildCount();
                BasicListExpression basicListExpression = new BasicListExpression(childCount);
                for (int i = 0; i < childCount; i++) {
                    basicListExpression.appendChild(doSingleEvaluation(environment, listExpression.getChild(i)));
                }
                return basicListExpression;
            } else if (child instanceof EnumeratingExpression) {
                EnumeratingExpression enumeratingExpression = (EnumeratingExpression) child;
                FrinkEnumeration frinkEnumeration = null;
                BasicListExpression basicListExpression2 = new BasicListExpression(0);
                try {
                    FrinkEnumeration enumeration = enumeratingExpression.getEnumeration(environment);
                    while (true) {
                        try {
                            Expression next = enumeration.getNext(environment);
                            if (next == null) {
                                break;
                            }
                            basicListExpression2.appendChild(doSingleEvaluation(environment, next));
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            frinkEnumeration = enumeration;
                            th = th2;
                            if (frinkEnumeration != null) {
                                frinkEnumeration.dispose();
                            }
                            throw th;
                        }
                    }
                    if (enumeration != null) {
                        enumeration.dispose();
                    }
                    return basicListExpression2;
                } catch (Throwable th3) {
                    th = th3;
                }
            }
        }
        return doSingleEvaluation(environment, child);
    }

    private Expression doSingleEvaluation(Environment environment, Expression expression) throws EvaluationException, NumericException {
        if (!(expression instanceof UnitExpression)) {
            throw new InvalidArgumentException("SingleArgNumericFunction: Bad argument: " + environment.format(expression), expression);
        }
        Unit unit = ((UnitExpression) expression).getUnit();
        if (this.inUnit != null) {
            unit = UnitMath.divide(unit, this.inUnit);
        }
        if (!UnitMath.isDimensionless(unit)) {
            throw new EvaluationConformanceException("Not of type " + this.inUnitName, expression);
        }
        Numeric doFunction = doFunction(environment, unit.getScale());
        if (this.outUnit != null) {
            return BasicUnitExpression.construct(UnitMath.multiply(DimensionlessUnitExpression.construct(doFunction), this.outUnit));
        }
        return DimensionlessUnitExpression.construct(doFunction);
    }

    private void initializeUnits(Environment environment) throws EvaluationException {
        if (this.inUnitName != null && this.inUnit == null) {
            this.inUnit = environment.getUnitManager().getUnit(this.inUnitName);
            if (this.inUnit == null) {
                throw new InvalidArgumentException("Can't find " + this.inUnitName, null);
            }
        }
        if (this.outUnitName != null && this.outUnit == null) {
            this.outUnit = environment.getUnitManager().getUnit(this.outUnitName);
            if (this.outUnit == null) {
                throw new InvalidArgumentException("Can't find " + this.outUnitName, null);
            }
        }
        this.initialized = true;
    }

    public boolean isMappable() {
        return false;
    }
}
