package frink.function;

import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FunctionDefinitionExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.ReturnException;
import frink.expr.SymbolDefinition;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import frink.symbolic.SymbolicUtils;

public class FunctionCallExpression implements Expression, FunctionCacher {
    private static final byte CONSTANT = 2;
    private static final boolean DEBUG = false;
    private static final byte NON_CONSTANT = 1;
    public static final String TYPE = "Function";
    private static final byte UNKNOWN = 0;
    private ListExpression args;
    private FunctionDefinition boundFunc = null;
    private byte constantFlag = 0;
    private String functionName;
    private BasicStringExpression nameExpression;
    private Expression result;

    public FunctionCallExpression(String str, ListExpression listExpression) {
        this.functionName = str;
        this.nameExpression = new BasicStringExpression(str);
        this.args = listExpression;
        int childCount = listExpression.getChildCount();
        int i = 0;
        while (i < childCount) {
            try {
                if (!listExpression.getChild(i).isConstant()) {
                    this.constantFlag = NON_CONSTANT;
                }
                i++;
            } catch (InvalidChildException e) {
                System.out.println("Invalid child in FunctionCallExpression constructor: " + e);
                return;
            }
        }
    }

    public String getName() {
        return this.functionName;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.ListExpression, int, frink.object.FrinkObject, boolean]
     candidates:
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    public Expression evaluate(Environment environment, boolean z, FrinkObject frinkObject) throws EvaluationException {
        Expression returnValue;
        FunctionDefinition functionDefinition;
        FunctionDefinition functionDefinition2;
        if (this.constantFlag == 2 && this.result != null) {
            return this.result;
        }
        try {
            if (this.boundFunc != null) {
                functionDefinition2 = this.boundFunc;
                returnValue = null;
            } else {
                returnValue = environment.getFunctionManager().execute(this.functionName, environment, this.args, frinkObject, z, this);
                functionDefinition2 = null;
            }
        } catch (NoSuchFunctionException e) {
            SymbolDefinition symbolDefinition = environment.getSymbolDefinition(this.functionName, DEBUG);
            if (symbolDefinition != null) {
                Expression value = symbolDefinition.getValue();
                if (value instanceof FunctionDefinitionExpression) {
                    functionDefinition = ((FunctionDefinitionExpression) value).getFunctionDefinition();
                } else {
                    functionDefinition = null;
                }
                if (functionDefinition == null) {
                    if (!environment.getSymbolicMode()) {
                        if (frinkObject != null) {
                            environment.outputln("Object does not have method called " + this.functionName + " with " + this.args.getChildCount() + " arguments.");
                        } else {
                            environment.outputln("No function called " + this.functionName + " with " + this.args.getChildCount() + " arguments.");
                        }
                    }
                    return new FunctionCallExpression(this.functionName, (ListExpression) this.args.evaluate(environment));
                }
                functionDefinition2 = functionDefinition;
                returnValue = null;
            } else if (!e.getFunctionName().equals(this.functionName)) {
                throw new NoSuchFunctionException(e.getFunctionName(), "When calling function " + this.functionName + ":\n  " + e, e.getExpression());
            } else {
                if (!environment.getSymbolicMode()) {
                    environment.outputln("No function match found for " + this.functionName + " with " + this.args.getChildCount() + " arguments.");
                }
                return new FunctionCallExpression(this.functionName, (ListExpression) this.args.evaluate(environment));
            }
        } catch (FunctionCallException e2) {
            throw new FunctionCallException("When calling " + environment.format(this) + ",\n " + e2.getMessage(), this);
        } catch (InvalidArgumentException e3) {
            if (environment.getSymbolicMode() && SymbolicUtils.containsAnySymbol(this.args)) {
                return new FunctionCallExpression(getName(), (ListExpression) this.args.evaluate(environment));
            }
            throw new InvalidArgumentException("Error when calling function " + this.functionName + ":\n " + e3, this);
        } catch (ReturnException e4) {
            returnValue = e4.getReturnValue();
        }
        if (functionDefinition2 != null) {
            returnValue = environment.getFunctionManager().execute(functionDefinition2, environment, (Expression) this.args, true, frinkObject, z);
        }
        if (this.constantFlag == 0) {
            resolveConstantFlag();
        }
        this.result = returnValue;
        return returnValue;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return evaluate(environment, true, null);
    }

    public int getChildCount() {
        return this.args.getChildCount() + 1;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.nameExpression;
        }
        return this.args.getChild(i - 1);
    }

    private void resolveConstantFlag() {
        if (this.constantFlag != 0 || this.boundFunc == null) {
            return;
        }
        if (this.boundFunc.isDeterministicAndHasNoSideeffects()) {
            this.constantFlag = CONSTANT;
        } else {
            this.constantFlag = NON_CONSTANT;
        }
    }

    public boolean isConstant() {
        if (this.constantFlag == 0) {
            resolveConstantFlag();
        }
        if (this.constantFlag == 2) {
            return true;
        }
        return DEBUG;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        int i;
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof FunctionCallExpression)) {
            return DEBUG;
        }
        FunctionCallExpression functionCallExpression = (FunctionCallExpression) expression;
        if (!this.functionName.equals(functionCallExpression.functionName)) {
            return DEBUG;
        }
        int childCount = this.args.getChildCount();
        if (functionCallExpression.args.getChildCount() != childCount) {
            return DEBUG;
        }
        if (z) {
            i = matchingContext.beginMatch();
        } else {
            i = 0;
        }
        int i2 = childCount - 1;
        while (i2 >= 0) {
            try {
                if (!this.args.getChild(i2).structureEquals(functionCallExpression.args.getChild(i2), matchingContext, environment, DEBUG)) {
                    if (z) {
                        matchingContext.rollbackMatch(i);
                    }
                    return DEBUG;
                }
                i2--;
            } catch (InvalidChildException e) {
                if (z) {
                    matchingContext.rollbackMatch(i);
                }
                return DEBUG;
            } catch (Throwable th) {
                if (z) {
                    matchingContext.rollbackMatch(i);
                }
                throw th;
            }
        }
        if (z) {
            matchingContext.rollbackMatch(i);
        }
        return true;
    }

    public void setCachedFunction(FunctionDefinition functionDefinition) {
        this.boundFunc = functionDefinition;
    }

    public String getExpressionType() {
        return "Function";
    }
}
