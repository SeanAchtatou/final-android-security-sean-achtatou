package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.UnitExpression;
import frink.numeric.Numeric;
import frink.units.Unit;
import frink.units.UnitMath;

public abstract class DoubleArgNumericFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doFunction(Environment environment, Numeric numeric, Numeric numeric2) throws EvaluationException;

    public DoubleArgNumericFunction(boolean z) {
        super(2, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        Expression child = expression.getChild(0);
        Expression child2 = expression.getChild(1);
        if (!(child instanceof UnitExpression)) {
            throw new InvalidArgumentException("DoubleArgNumericFunction: Bad argument 1: " + environment.format(child), child);
        } else if (!(child2 instanceof UnitExpression)) {
            throw new InvalidArgumentException("DoubleArgNumericFunction: Bad argument 2: " + environment.format(child2), child2);
        } else {
            Unit unit = ((UnitExpression) child).getUnit();
            Unit unit2 = ((UnitExpression) child2).getUnit();
            if (!UnitMath.isDimensionless(unit)) {
                throw new InvalidArgumentException("First argument not dimensionless:", child);
            } else if (UnitMath.isDimensionless(unit2)) {
                return doFunction(environment, unit.getScale(), unit2.getScale());
            } else {
                throw new InvalidArgumentException("Second argument not dimensionless:", child2);
            }
        }
    }
}
