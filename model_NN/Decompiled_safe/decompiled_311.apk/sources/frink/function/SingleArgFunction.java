package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.ListExpression;
import frink.numeric.NumericException;

public abstract class SingleArgFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException;

    public SingleArgFunction(boolean z) {
        super(1, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException, NumericException {
        Expression child = expression.getChild(0);
        if (isMappable()) {
            if (child instanceof ListExpression) {
                ListExpression listExpression = (ListExpression) child;
                int childCount = listExpression.getChildCount();
                BasicListExpression basicListExpression = new BasicListExpression(childCount);
                for (int i = 0; i < childCount; i++) {
                    basicListExpression.appendChild(doFunction(environment, listExpression.getChild(i)));
                }
                return basicListExpression;
            } else if (child instanceof EnumeratingExpression) {
                EnumeratingExpression enumeratingExpression = (EnumeratingExpression) child;
                FrinkEnumeration frinkEnumeration = null;
                BasicListExpression basicListExpression2 = new BasicListExpression(0);
                try {
                    FrinkEnumeration enumeration = enumeratingExpression.getEnumeration(environment);
                    while (true) {
                        try {
                            Expression next = enumeration.getNext(environment);
                            if (next == null) {
                                break;
                            }
                            basicListExpression2.appendChild(doFunction(environment, next));
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            frinkEnumeration = enumeration;
                            th = th2;
                            if (frinkEnumeration != null) {
                                frinkEnumeration.dispose();
                            }
                            throw th;
                        }
                    }
                    if (enumeration != null) {
                        enumeration.dispose();
                    }
                    return basicListExpression2;
                } catch (Throwable th3) {
                    th = th3;
                }
            }
        }
        return doFunction(environment, child);
    }

    public boolean isMappable() {
        return false;
    }
}
