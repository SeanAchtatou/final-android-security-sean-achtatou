package frink.function;

import frink.expr.AssignmentExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.EvaluationNumericException;
import frink.expr.Expression;
import frink.expr.FunctionDefinitionExpression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.SymbolExpression;
import frink.expr.VariableDeclarationExpression;
import frink.java.ClassUtils;
import frink.numeric.NumericException;
import frink.symbolic.MatchingContext;
import java.lang.reflect.Method;

public abstract class AbstractFunctionDefinition implements Expression, FunctionDefinition, FunctionDefinitionExpression {
    public static final String TYPE = "Function";
    private static final BasicFunctionArgument dummyArg = new BasicFunctionArgument();
    private BasicFunctionArgument[] args;
    private boolean hasNamed;
    private boolean isDeterministic;
    private String returnTypeString;

    public abstract Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException, NumericException;

    public AbstractFunctionDefinition(int i, boolean z) {
        this.args = new BasicFunctionArgument[i];
        this.isDeterministic = z;
        this.returnTypeString = null;
        this.hasNamed = false;
        for (int i2 = 0; i2 < i; i2++) {
            this.args[i2] = dummyArg;
        }
    }

    public AbstractFunctionDefinition(Method method, boolean z) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        int length = parameterTypes.length;
        this.args = new BasicFunctionArgument[length];
        this.isDeterministic = z;
        this.returnTypeString = ClassUtils.getCanonicalName(method.getReturnType());
        this.hasNamed = false;
        for (int i = 0; i < length; i++) {
            this.args[i] = new BasicFunctionArgument(null, null, null, ClassUtils.getCanonicalName(parameterTypes[i]));
        }
    }

    public AbstractFunctionDefinition(ListExpression listExpression, boolean z) {
        int childCount = listExpression.getChildCount();
        this.args = new BasicFunctionArgument[childCount];
        this.isDeterministic = z;
        this.returnTypeString = null;
        if (childCount > 0) {
            this.hasNamed = true;
        } else {
            this.hasNamed = false;
        }
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = listExpression.getChild(i);
                if (child instanceof SymbolExpression) {
                    this.args[i] = new BasicFunctionArgument(((SymbolExpression) child).getName());
                } else if (child instanceof AssignmentExpression) {
                    this.args[i] = new BasicFunctionArgument(((SymbolExpression) child.getChild(0)).getName(), null, child.getChild(1));
                } else if (child instanceof VariableDeclarationExpression) {
                    VariableDeclarationExpression variableDeclarationExpression = (VariableDeclarationExpression) child;
                    this.args[i] = new BasicFunctionArgument(variableDeclarationExpression.getName(), variableDeclarationExpression.getConstraints(), child.getChild(0));
                }
                i++;
            } catch (InvalidChildException e) {
                System.out.println("BasicFunctionDefinition: bad child");
                return;
            }
        }
    }

    public int getArgumentCount() {
        return this.args.length;
    }

    public void setDefaultValue(int i, Expression expression) {
        BasicFunctionArgument basicFunctionArgument = this.args[i];
        this.args[i] = new BasicFunctionArgument(basicFunctionArgument.getName(), basicFunctionArgument.getConstraints(), expression);
    }

    public FunctionArgument getArgument(int i) {
        return this.args[i];
    }

    public String getReturnTypeString() {
        return this.returnTypeString;
    }

    public int getChildCount() {
        return 0;
    }

    public Expression getChild(int i) throws InvalidChildException {
        throw new InvalidChildException("AbstractFunctionDefinition: no children", this);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public Expression performEvaluation(Environment environment, Expression expression) throws EvaluationException {
        try {
            return doEvaluation(environment, expression);
        } catch (NumericException e) {
            throw new EvaluationNumericException(e.toString(), this);
        }
    }

    public boolean isConstant() {
        return true;
    }

    public FunctionDefinition getFunctionDefinition() {
        return this;
    }

    public boolean isDeterministicAndHasNoSideeffects() {
        return this.isDeterministic;
    }

    public boolean hasNamedArguments() {
        return this.hasNamed;
    }

    public boolean shouldEvaluateFirst() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return "Function";
    }
}
