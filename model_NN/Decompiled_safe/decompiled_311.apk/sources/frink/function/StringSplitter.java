package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.BasicStringExpression;
import frink.expr.ListExpression;
import frink.expr.StringExpression;
import frink.text.BasicRegexpExpression;
import java.util.Vector;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Util;

public class StringSplitter {
    private static final Pattern newlinePattern = BasicRegexpExpression.getPattern("\r|\n|\r\n| | |\u000b|\f|");

    public static ListExpression split(StringExpression stringExpression, String str) {
        int i = 0;
        int length = str.length();
        BasicListExpression basicListExpression = new BasicListExpression(1);
        String string = stringExpression.getString();
        int length2 = string.length();
        int i2 = length2 - length;
        while (i <= i2) {
            int indexOf = string.indexOf(str, i);
            if (indexOf == -1) {
                break;
            }
            basicListExpression.appendChild(new BasicStringExpression(string.substring(i, indexOf)));
            i = indexOf + length;
        }
        if (i == 0) {
            basicListExpression.appendChild(stringExpression);
            return basicListExpression;
        }
        if (i - 1 < length2) {
            basicListExpression.appendChild(new BasicStringExpression(string.substring(i)));
        }
        return basicListExpression;
    }

    public static Vector<String> split(String str, Pattern pattern) {
        return Util.split(new Perl5Matcher(), pattern, str);
    }

    public static Vector<String> splitOnNewlines(String str) {
        return split(str, newlinePattern);
    }
}
