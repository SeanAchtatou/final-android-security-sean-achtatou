package frink.function;

import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.StringExpression;

public abstract class StringFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract String doFunction(String str);

    public StringFunction(boolean z) {
        super(1, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        try {
            Expression child = expression.getChild(0);
            if (child instanceof StringExpression) {
                return new BasicStringExpression(doFunction(((StringExpression) child).getString()));
            }
            throw new InvalidArgumentException("String Function: Bad argument: " + environment.format(child), child);
        } catch (InvalidChildException e) {
            throw new InvalidArgumentException("Bad child", expression);
        }
    }
}
