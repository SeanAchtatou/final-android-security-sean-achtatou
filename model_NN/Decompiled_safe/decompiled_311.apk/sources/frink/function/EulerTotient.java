package frink.function;

import frink.function.FactorList;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import java.util.Enumeration;

public class EulerTotient {
    public static Numeric eulerTotient(FrinkInteger frinkInteger) throws NumericException {
        Enumeration<FactorList.Factors> enumeration = Factor.factorToFactorList(frinkInteger).getEnumeration();
        Numeric numeric = frinkInteger;
        while (enumeration.hasMoreElements()) {
            numeric = NumericMath.multiply(numeric, NumericMath.subtract(FrinkInt.ONE, NumericMath.divide(FrinkInt.ONE, enumeration.nextElement().getFactor())));
        }
        return numeric;
    }
}
