package frink.function;

import frink.expr.Environment;
import frink.expr.Expression;

public class FunctionDescriptor {
    private FunctionDefinition definition;
    private String name;

    public FunctionDescriptor(String str, FunctionDefinition functionDefinition) {
        this.name = str;
        this.definition = functionDefinition;
    }

    public String getName() {
        return this.name;
    }

    public FunctionDefinition getFunctionDefinition() {
        return this.definition;
    }

    public String toString(Environment environment) {
        StringBuffer stringBuffer = new StringBuffer();
        String returnTypeString = this.definition.getReturnTypeString();
        if (returnTypeString != null) {
            stringBuffer.append(returnTypeString + " ");
        }
        stringBuffer.append(this.name + "[");
        int argumentCount = this.definition.getArgumentCount();
        for (int i = 0; i < argumentCount; i++) {
            FunctionArgument argument = this.definition.getArgument(i);
            String type = argument.getType();
            if (type != null) {
                stringBuffer.append(type + " ");
            }
            String name2 = argument.getName();
            if (name2 == null) {
                stringBuffer.append("arg" + (i + 1));
            } else {
                stringBuffer.append(name2);
            }
            Expression defaultValue = argument.getDefaultValue();
            if (defaultValue != null) {
                stringBuffer.append("=" + environment.format(defaultValue, false));
            }
            if (i < argumentCount - 1) {
                stringBuffer.append(",");
            }
        }
        stringBuffer.append("]");
        return new String(stringBuffer);
    }
}
