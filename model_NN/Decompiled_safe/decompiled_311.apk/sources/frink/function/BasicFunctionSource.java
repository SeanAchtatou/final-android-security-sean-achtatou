package frink.function;

import frink.expr.Environment;
import frink.expr.ListExpression;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;

public class BasicFunctionSource implements WritableFunctionSource {
    /* access modifiers changed from: private */
    public Hashtable<String, Vector<FunctionDefinition>> funcTable = new Hashtable<>();
    private String name;

    public BasicFunctionSource(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public void addFunctionDefinition(String str, FunctionDefinition functionDefinition) {
        Vector vector = this.funcTable.get(str);
        if (vector == null) {
            vector = new Vector(1);
            vector.addElement(functionDefinition);
        } else {
            int size = vector.size();
            int argumentCount = functionDefinition.getArgumentCount();
            int i = 0;
            while (i < size && argumentCount > ((FunctionDefinition) vector.elementAt(i)).getArgumentCount()) {
                i++;
            }
            if (i >= size || argumentCount != ((FunctionDefinition) vector.elementAt(i)).getArgumentCount()) {
                vector.insertElementAt(functionDefinition, i);
            } else {
                vector.setElementAt(functionDefinition, i);
                System.err.println("Warning: redefining function " + str + " with " + argumentCount + " arguments.");
            }
        }
        this.funcTable.put(str, vector);
    }

    public FunctionDefinition getBestMatch(String str, ListExpression listExpression, Environment environment) {
        return getBestMatch(str, listExpression.getChildCount(), environment);
    }

    public FunctionDefinition getBestMatch(String str, int i, Environment environment) {
        boolean z;
        Vector vector = this.funcTable.get(str);
        if (vector == null) {
            return null;
        }
        int size = vector.size();
        for (int i2 = 0; i2 < size; i2++) {
            FunctionDefinition functionDefinition = (FunctionDefinition) vector.elementAt(i2);
            int argumentCount = functionDefinition.getArgumentCount();
            if (argumentCount == i) {
                return functionDefinition;
            }
            if (argumentCount > i) {
                int i3 = i;
                while (true) {
                    if (i3 >= argumentCount) {
                        z = true;
                        break;
                    } else if (functionDefinition.getArgument(i3).getDefaultValue() == null) {
                        z = false;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (z) {
                    return functionDefinition;
                }
            }
        }
        return null;
    }

    public Enumeration<FunctionDescriptor> getFunctionDescriptors() {
        return new FunctionEnumerator();
    }

    private class FunctionEnumerator implements Enumeration<FunctionDescriptor> {
        private String currentName;
        private Enumeration<FunctionDefinition> funcEnum = null;
        private Enumeration<String> keyEnum;
        private FunctionDescriptor nextElement;

        public FunctionEnumerator() {
            this.keyEnum = BasicFunctionSource.this.funcTable.keys();
            getNextElement();
        }

        public boolean hasMoreElements() {
            return this.nextElement != null;
        }

        public FunctionDescriptor nextElement() {
            if (this.nextElement != null) {
                FunctionDescriptor functionDescriptor = this.nextElement;
                getNextElement();
                return functionDescriptor;
            }
            throw new NoSuchElementException();
        }

        private void getNextElement() {
            if (this.funcEnum != null) {
                if (this.funcEnum.hasMoreElements()) {
                    this.nextElement = new FunctionDescriptor(this.currentName, this.funcEnum.nextElement());
                    return;
                }
                this.funcEnum = null;
            }
            if (this.keyEnum == null) {
                return;
            }
            if (this.keyEnum.hasMoreElements()) {
                this.currentName = this.keyEnum.nextElement();
                this.funcEnum = ((Vector) BasicFunctionSource.this.funcTable.get(this.currentName)).elements();
                getNextElement();
                return;
            }
            this.nextElement = null;
            this.keyEnum = null;
        }
    }
}
