package frink.function;

import frink.expr.ListExpression;

public class FunctionSignature {
    private ListExpression argList;
    private String name;

    public FunctionSignature(String str, ListExpression listExpression) {
        this.name = str;
        this.argList = listExpression;
    }

    public String getName() {
        return this.name;
    }

    public ListExpression getArgumentList() {
        return this.argList;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.name + "[");
        stringBuffer.append(this.argList.getChildCount() + " arguments");
        stringBuffer.append("]");
        return new String(stringBuffer);
    }
}
