package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public abstract class ZeroArgMethod<T> extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doMethod(Environment environment, Object obj) throws EvaluationException;

    public ZeroArgMethod(boolean z) {
        super(0, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return doMethod(environment, environment.getSymbolDefinition("this", false).getValue());
    }
}
