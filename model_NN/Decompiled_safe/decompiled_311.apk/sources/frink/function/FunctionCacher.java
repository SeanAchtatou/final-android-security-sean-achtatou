package frink.function;

public interface FunctionCacher {
    void setCachedFunction(FunctionDefinition functionDefinition);
}
