package frink.function;

import frink.expr.BasicUnitExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.UnitExpression;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.units.Unit;
import frink.units.UnitMath;

public class ArcTanFunction extends DoubleArgFunction {
    private static final String RADIAN = "radian";
    private Unit radian = null;

    public ArcTanFunction() {
        super(true);
    }

    public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
        if (!(expression instanceof UnitExpression)) {
            throw new InvalidArgumentException("Bad argument", expression);
        } else if (!(expression2 instanceof UnitExpression)) {
            throw new InvalidArgumentException("Bad argument", expression2);
        } else {
            if (this.radian == null) {
                initializeRadian(environment);
            }
            return BasicUnitExpression.construct(arctan(((UnitExpression) expression).getUnit(), ((UnitExpression) expression2).getUnit(), this.radian, environment));
        }
    }

    public static Unit arctan(Unit unit, Unit unit2, Unit unit3, Environment environment) throws EvaluationException {
        if (!UnitMath.areConformal(unit, unit2)) {
            throw new InvalidArgumentException("atan2: arguments must be of same unit type.", null);
        }
        try {
            return UnitMath.multiply(DimensionlessUnitExpression.construct(NumericMath.arctan(unit.getScale(), unit2.getScale())), unit3);
        } catch (NumericException e) {
            throw new InvalidArgumentException("arctan[x,y] not yet implemented for these arguments: " + e.getMessage(), null);
        }
    }

    private synchronized void initializeRadian(Environment environment) {
        if (this.radian == null) {
            this.radian = environment.getUnitManager().getUnit(RADIAN);
        }
    }
}
