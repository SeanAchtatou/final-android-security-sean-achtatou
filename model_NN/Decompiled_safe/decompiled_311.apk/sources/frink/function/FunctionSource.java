package frink.function;

import frink.expr.Environment;
import frink.expr.ListExpression;
import java.util.Enumeration;

public interface FunctionSource {
    FunctionDefinition getBestMatch(String str, int i, Environment environment) throws RequiresArgumentsException;

    FunctionDefinition getBestMatch(String str, ListExpression listExpression, Environment environment);

    Enumeration<FunctionDescriptor> getFunctionDescriptors();

    String getName();
}
