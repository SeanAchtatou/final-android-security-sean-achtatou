package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.CannotAssignException;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.InvalidChildException;
import frink.expr.TerminalExpression;
import frink.symbolic.MatchingContext;

public class Permutation extends TerminalExpression implements EnumeratingExpression {
    public static final String TYPE = "Permutation";
    BasicListExpression listCopy;
    Orderer orderer = null;

    public Permutation(BasicListExpression basicListExpression) {
        this.listCopy = new BasicListExpression(basicListExpression);
        this.orderer = null;
    }

    public Permutation(BasicListExpression basicListExpression, Orderer orderer2) {
        this.listCopy = new BasicListExpression(basicListExpression);
        this.orderer = orderer2;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    public FrinkEnumeration getEnumeration(Environment environment) {
        if (this.orderer == null) {
            return new PlainEnumeration(this.listCopy);
        }
        return new LexicographicEnumeration(this.listCopy, environment, this.orderer);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public static void swap(BasicListExpression basicListExpression, int i, int i2) throws InvalidChildException, CannotAssignException {
        Expression child = basicListExpression.getChild(i);
        basicListExpression.setChild(i, basicListExpression.getChild(i2));
        basicListExpression.setChild(i2, child);
    }

    private static class PlainEnumeration implements FrinkEnumeration {
        private int[] c = new int[(this.size + 1)];
        private int j;
        private BasicListExpression list;
        private int[] o;
        private int s;
        private int size = this.list.getChildCount();

        public PlainEnumeration(BasicListExpression basicListExpression) {
            this.list = new BasicListExpression(basicListExpression);
            for (int i = 1; i <= this.size; i++) {
                this.c[i] = 0;
            }
            this.o = new int[(this.size + 1)];
            for (int i2 = 1; i2 <= this.size; i2++) {
                this.o[i2] = 1;
            }
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.list == null) {
                return null;
            }
            BasicListExpression basicListExpression = new BasicListExpression(this.list);
            if (this.size == 0) {
                this.list = null;
                return basicListExpression;
            }
            this.j = this.size;
            this.s = 0;
            while (true) {
                int i = this.c[this.j] + this.o[this.j];
                if (i < 0) {
                    this.o[this.j] = -this.o[this.j];
                    this.j--;
                } else if (i != this.j) {
                    int i2 = ((this.j - this.c[this.j]) + this.s) - 1;
                    int i3 = ((this.j - i) + this.s) - 1;
                    Expression child = this.list.getChild(i2);
                    this.list.setChild(i2, this.list.getChild(i3));
                    this.list.setChild(i3, child);
                    this.c[this.j] = i;
                    return basicListExpression;
                } else if (this.j == 1) {
                    this.list = null;
                    return basicListExpression;
                } else {
                    this.s++;
                    this.o[this.j] = -this.o[this.j];
                    this.j--;
                }
            }
        }

        public void dispose() {
            this.list = null;
        }
    }

    private static class LexicographicEnumeration implements FrinkEnumeration {
        private boolean first;
        private BasicListExpression list;
        private Orderer orderer;
        private int size;

        public LexicographicEnumeration(BasicListExpression basicListExpression, Environment environment) {
            this.list = new BasicListExpression(basicListExpression);
            this.size = this.list.getChildCount();
            this.orderer = DefaultOrderer.INSTANCE;
            this.first = true;
        }

        public LexicographicEnumeration(BasicListExpression basicListExpression, Environment environment, Orderer orderer2) {
            this.list = new BasicListExpression(basicListExpression);
            this.size = this.list.getChildCount();
            this.orderer = orderer2;
            this.first = true;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.list == null) {
                return null;
            }
            if (this.first) {
                this.first = false;
                Sorter.sort(this.list, this.orderer, environment);
                return new BasicListExpression(this.list);
            }
            int i = this.size - 1;
            int i2 = this.size - 1;
            while (i > 0 && this.orderer.compare(this.list.getChild(i), this.list.getChild(i - 1), environment) <= 0) {
                i--;
            }
            int i3 = i - 1;
            if (i3 < 0) {
                this.list = null;
                return null;
            }
            int i4 = this.size - 1;
            while (i4 > i3 && this.orderer.compare(this.list.getChild(i4), this.list.getChild(i3), environment) <= 0) {
                i4--;
            }
            Permutation.swap(this.list, i3, i4);
            int i5 = i3 + 1;
            for (int i6 = this.size - 1; i6 > i5; i6--) {
                Permutation.swap(this.list, i6, i5);
                i5++;
            }
            return new BasicListExpression(this.list);
        }

        public void dispose() {
            this.list = null;
        }
    }
}
