package frink.function;

import frink.errors.NotAnIntegerException;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.NumericMath;
import java.util.Vector;

public class Partitions {
    private static FrinkInteger largestPent = FrinkInteger.construct(0);
    private static int partIdx = 1;
    private static Vector<FrinkInteger> partitions = new Vector<>();
    private static int pentIdx = 1;
    private static Vector<FrinkInteger> pentagonalNumbers = new Vector<>();

    static {
        partitions.addElement(FrinkInt.ONE);
    }

    public static FrinkInteger getPartitionCount(int i) throws NotAnIntegerException {
        FrinkInteger frinkInteger = FrinkInt.ZERO;
        if (partitions.size() - 1 >= i) {
            return partitions.elementAt(i);
        }
        calculatePentagonal(FrinkInteger.construct(i));
        int size = pentagonalNumbers.size();
        int size2 = partitions.size();
        while (size2 <= i) {
            int i2 = 0;
            FrinkInteger frinkInteger2 = FrinkInt.ZERO;
            while (i2 < size) {
                FrinkInteger elementAt = pentagonalNumbers.elementAt(i2);
                FrinkInteger construct = FrinkInteger.construct(partIdx);
                if (FrinkInt.compare(NumericMath.subtractInts(construct, elementAt), FrinkInt.ZERO) < 0) {
                    break;
                }
                int i3 = (i2 % 4) / 2 == 0 ? 1 : -1;
                i2++;
                frinkInteger2 = NumericMath.addInts(frinkInteger2, NumericMath.multiplyInts(FrinkInteger.construct(i3), partitions.elementAt(NumericMath.subtractInts(construct, elementAt).getInt())));
            }
            partitions.addElement(frinkInteger2);
            partIdx++;
            size2++;
            frinkInteger = frinkInteger2;
        }
        return frinkInteger;
    }

    private static void calculatePentagonal(FrinkInteger frinkInteger) {
        synchronized (pentagonalNumbers) {
            while (FrinkInteger.compare(largestPent, frinkInteger) < 0) {
                FrinkInteger construct = FrinkInteger.construct(pentIdx);
                largestPent = NumericMath.divInts(NumericMath.multiplyInts(construct, NumericMath.subtractInts(NumericMath.multiplyInts(FrinkInt.THREE, construct), FrinkInt.ONE)), FrinkInt.TWO);
                pentagonalNumbers.addElement(largestPent);
                if (pentIdx < 0) {
                    pentIdx = (-pentIdx) + 1;
                } else {
                    pentIdx = -pentIdx;
                }
            }
        }
    }

    public static void main(String[] strArr) {
        try {
            System.out.println(getPartitionCount(1000));
            System.out.println(getPartitionCount(5000));
            System.out.println(getPartitionCount(6000));
            System.out.println(getPartitionCount(5000));
            System.out.println(getPartitionCount(10000));
            System.out.println(getPartitionCount(10000));
        } catch (NotAnIntegerException e) {
        }
    }
}
