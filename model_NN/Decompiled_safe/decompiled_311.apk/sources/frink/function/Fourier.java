package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.CannotAssignException;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.numeric.FrinkComplex;
import frink.numeric.FrinkFloat;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.units.BasicUnit;
import frink.units.DimensionList;
import frink.units.Unit;

public class Fourier {
    public static ComplexArray DFT(ComplexArray complexArray, int i, int i2) {
        double d;
        int length = complexArray.getLength();
        ComplexArray complexArray2 = new ComplexArray(length);
        double d2 = ((((double) i2) * 2.0d) * 3.141592653589793d) / ((double) length);
        double d3 = 1.0d;
        if (i == -1) {
            d3 = 1.0d / ((double) length);
        }
        if (i == 0) {
            d = 1.0d / Math.sqrt((double) length);
        } else {
            d = d3;
        }
        for (int i3 = 0; i3 < length; i3++) {
            double d4 = ((double) i3) * d2;
            double d5 = 0.0d;
            double d6 = 0.0d;
            for (int i4 = 0; i4 < length; i4++) {
                double d7 = ((double) i4) * d4;
                double cos = Math.cos(d7);
                double sin = Math.sin(d7);
                d6 += (complexArray.re[i4] * cos) - (complexArray.im[i4] * sin);
                d5 += (sin * complexArray.re[i4]) + (cos * complexArray.im[i4]);
            }
            complexArray2.set(i3, d6 * d, d5 * d);
        }
        return complexArray2;
    }

    public static ComplexArray InverseDFT(ComplexArray complexArray, int i, int i2) {
        return DFT(complexArray, -i, -i2);
    }

    public static ListExpression DFT(ListExpression listExpression, int i, int i2) throws InvalidChildException, InvalidArgumentException, NumericException {
        return DFT(new ComplexArray(listExpression), i, i2).toListExpression(BuiltinFunctionSource.getUnitValue(listExpression.getChild(0)).getDimensionList());
    }

    public static ListExpression InverseDFT(ListExpression listExpression, int i, int i2) throws InvalidChildException, InvalidArgumentException, NumericException {
        return DFT(listExpression, -i, -i2);
    }

    public static final class ComplexArray {
        public double[] im;
        public double[] re;

        public ComplexArray(int i) {
            this.re = new double[i];
            this.im = new double[i];
        }

        public ComplexArray(ListExpression listExpression) throws InvalidChildException, InvalidArgumentException, NumericException {
            int childCount = listExpression.getChildCount();
            this.re = new double[childCount];
            this.im = new double[childCount];
            for (int i = 0; i < childCount; i++) {
                set(i, BuiltinFunctionSource.getUnitValue(listExpression.getChild(i)));
            }
        }

        public int getLength() {
            return this.re.length;
        }

        public void set(int i, double d, double d2) {
            this.re[i] = d;
            this.im[i] = d2;
        }

        public void set(int i, Numeric numeric) throws NumericException {
            this.re[i] = NumericMath.realPart(numeric).doubleValue();
            this.im[i] = NumericMath.imaginaryPart(numeric).doubleValue();
        }

        public void set(int i, Unit unit) throws NumericException {
            Numeric scale = unit.getScale();
            this.re[i] = NumericMath.realPart(scale).doubleValue();
            this.im[i] = NumericMath.imaginaryPart(scale).doubleValue();
        }

        public ListExpression toListExpression(DimensionList dimensionList) throws NumericException {
            boolean z;
            int length = getLength();
            BasicListExpression basicListExpression = new BasicListExpression(length);
            if (dimensionList == null || dimensionList.getHighestIndex() == -1) {
                z = false;
            } else {
                z = true;
            }
            int i = 0;
            while (i < length) {
                try {
                    Numeric construct = FrinkComplex.construct(new FrinkFloat(this.re[i]), new FrinkFloat(this.im[i]));
                    if (z) {
                        basicListExpression.setChild(i, BasicUnitExpression.construct(BasicUnit.construct(construct, dimensionList)));
                    } else {
                        basicListExpression.setChild(i, DimensionlessUnitExpression.construct(construct));
                    }
                    i++;
                } catch (CannotAssignException e) {
                    System.err.println("Fourier.ComplexArray.toListExpression:  Unexpected CannotAssignException:\n  " + e);
                    return null;
                }
            }
            return basicListExpression;
        }
    }
}
