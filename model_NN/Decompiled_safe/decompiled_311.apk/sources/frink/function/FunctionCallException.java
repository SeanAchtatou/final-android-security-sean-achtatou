package frink.function;

import frink.expr.EvaluationException;
import frink.expr.Expression;

public class FunctionCallException extends EvaluationException {
    public FunctionCallException(String str, Expression expression) {
        super(str, expression);
    }
}
