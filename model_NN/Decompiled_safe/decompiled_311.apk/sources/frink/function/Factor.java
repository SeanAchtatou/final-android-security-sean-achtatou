package frink.function;

import frink.errors.NotAnIntegerException;
import frink.expr.BasicListExpression;
import frink.function.FactorList;
import frink.numeric.FrinkBigInteger;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import java.math.BigInteger;
import java.util.BitSet;
import java.util.Enumeration;

public class Factor {
    private static final int[] A_4759123141 = {2, 7, 61};
    private static final int[] A_9080191 = {31, 73};
    private static final BigInteger DIGIT_5 = new BigInteger("2152302898747");
    private static final BigInteger[] DIGIT_5_ARRAY = {FrinkBigInteger.TWO, FrinkBigInteger.THREE, FrinkBigInteger.FIVE, FrinkBigInteger.SEVEN, FrinkBigInteger.ELEVEN};
    private static final BigInteger DIGIT_6 = new BigInteger("3474749660383");
    private static final BigInteger[] DIGIT_6_ARRAY = {FrinkBigInteger.TWO, FrinkBigInteger.THREE, FrinkBigInteger.FIVE, FrinkBigInteger.SEVEN, FrinkBigInteger.ELEVEN, FrinkBigInteger.THIRTEEN};
    private static final BigInteger DIGIT_7 = new BigInteger("341550071728321");
    private static final BigInteger[] DIGIT_7_ARRAY = {FrinkBigInteger.TWO, FrinkBigInteger.THREE, FrinkBigInteger.FIVE, FrinkBigInteger.SEVEN, FrinkBigInteger.ELEVEN, FrinkBigInteger.THIRTEEN, FrinkBigInteger.SEVENTEEN};
    private static final BigInteger MAX_INT = BigInteger.valueOf(2147483647L);
    private static final int SIEVE_SIZE = 46341;
    private static final BigInteger[] STRONG_TEST_ARRAY = {new BigInteger("19"), new BigInteger("23"), new BigInteger("29"), new BigInteger("31"), new BigInteger("37"), new BigInteger("41"), new BigInteger("43"), new BigInteger("47"), new BigInteger("53"), new BigInteger("59"), new BigInteger("61"), new BigInteger("67"), new BigInteger("71"), new BigInteger("73"), new BigInteger("79"), new BigInteger("83"), new BigInteger("89"), new BigInteger("97"), new BigInteger("101"), new BigInteger("103"), new BigInteger("107"), new BigInteger("109"), new BigInteger("113"), new BigInteger("127"), new BigInteger("131"), new BigInteger("127"), new BigInteger("131"), new BigInteger("137"), new BigInteger("139"), new BigInteger("149"), new BigInteger("151"), new BigInteger("157"), new BigInteger("163"), new BigInteger("167"), new BigInteger("173"), new BigInteger("179"), new BigInteger("181"), new BigInteger("191"), new BigInteger("193"), new BigInteger("197"), new BigInteger("199"), new BigInteger("211"), new BigInteger("223"), new BigInteger("227"), new BigInteger("229"), new BigInteger("233"), new BigInteger("239"), new BigInteger("241"), new BigInteger("251"), new BigInteger("257"), new BigInteger("263"), new BigInteger("269"), new BigInteger("271"), new BigInteger("277"), new BigInteger("281"), new BigInteger("283"), new BigInteger("293"), new BigInteger("307"), new BigInteger("311"), new BigInteger("313"), new BigInteger("317"), new BigInteger("331"), new BigInteger("337"), new BigInteger("347"), new BigInteger("349"), new BigInteger("353"), new BigInteger("359"), new BigInteger("367"), new BigInteger("373"), new BigInteger("379"), new BigInteger("383"), new BigInteger("389"), new BigInteger("397")};
    private static BigInteger[] bigIntWheelAddends;
    private static BigInteger bigIntWheelSize;
    private static BigInteger[] bigPrimes = null;
    private static int[] intWheelAddends;
    private static int largestWheelableInt;
    private static BitSet primeSieve = null;
    private static int[] primes = null;
    private static boolean primesInitialized = false;
    private static final int[] wheelElements = {2, 3, 5, 7};
    private static BitSet wheelFlags = null;
    private static boolean wheelInitialized = false;
    private static int wheelSize;

    public static BasicListExpression factor(FrinkInteger frinkInteger) {
        return factorToFactorList(frinkInteger).toExpression();
    }

    public static FactorList factorToFactorList(FrinkInteger frinkInteger) {
        FactorList factorList = new FactorList();
        try {
            factor(frinkInteger.getInt(), factorList);
        } catch (NotAnIntegerException e) {
            factor(frinkInteger.getBigInt(), factorList);
        }
        return factorList;
    }

    public static void factor(int i, FactorList factorList) {
        if (i == 1) {
            factorList.addFactor(FrinkInt.ONE, 1);
        } else if (isPrime(i)) {
            factorList.addFactor(FrinkInteger.construct(i), 1);
        } else {
            factorTrial(i, factorList);
        }
    }

    public static void factor(BigInteger bigInteger, FactorList factorList) {
        if (isPrime(bigInteger)) {
            factorList.addFactor(FrinkInteger.construct(bigInteger), 1);
        } else {
            factorBeyondTrial(factorTrial(detectPerfectPowers(bigInteger, factorList), factorList), factorList);
        }
    }

    private static void factorBeyondTrial(BigInteger bigInteger, FactorList factorList) {
        if (bigInteger != null && bigInteger.compareTo(FrinkBigInteger.ONE) != 0) {
            if (isPrime(bigInteger)) {
                factorList.addFactor(FrinkInteger.construct(bigInteger), 1);
            } else {
                factorBeyondTrialNotPrime(bigInteger, factorList);
            }
        }
    }

    private static void factorBeyondTrialNotPrime(BigInteger bigInteger, FactorList factorList) {
        BigInteger factorPollardPMinus1 = factorPollardPMinus1(bigInteger, factorList);
        if (factorPollardPMinus1 != null && factorPollardPMinus1.compareTo(FrinkBigInteger.ONE) != 0) {
            factorPollardRho(factorPollardPMinus1, factorList);
        }
    }

    public static void factorTrial(int i, FactorList factorList) {
        factorTrial(i, factorList, 0);
    }

    public static void factorTrial(int i, FactorList factorList, int i2) {
        int sqrt = (int) Math.sqrt((double) i);
        if (!primesInitialized) {
            calcPrimes();
        }
        int i3 = primes[i2];
        int length = primes.length;
        int i4 = sqrt;
        int i5 = i;
        int i6 = i2;
        while (i5 != 1 && i6 < length) {
            int i7 = primes[i6];
            if (i7 > i4) {
                break;
            }
            int i8 = i5;
            boolean z = false;
            while (i8 % i7 == 0) {
                factorList.addFactor(FrinkInteger.construct(i7), 1);
                i8 /= i7;
                z = true;
            }
            if (z) {
                i4 = (int) Math.sqrt((double) i8);
            }
            i6++;
            i5 = i8;
        }
        if (i5 != 1) {
            factorList.addFactor(FrinkInteger.construct(i5), 1);
        }
    }

    public static BigInteger factorTrial(BigInteger bigInteger, FactorList factorList) {
        BigInteger bigInteger2;
        int lowestSetBit = bigInteger.getLowestSetBit();
        if (lowestSetBit > 0) {
            factorList.addFactor(FrinkInt.TWO, lowestSetBit);
            bigInteger2 = bigInteger.shiftRight(lowestSetBit);
        } else {
            bigInteger2 = bigInteger;
        }
        if (bigInteger2.compareTo(MAX_INT) <= 0) {
            int intValue = bigInteger2.intValue();
            if (intValue == 1) {
                return null;
            }
            factorTrial(intValue, factorList, 1);
            return null;
        }
        if (!primesInitialized) {
            calcPrimes();
        }
        int length = primes.length;
        BigInteger bigInteger3 = bigInteger2;
        for (int i = 1; i < length && bigInteger3.compareTo(FrinkBigInteger.ONE) != 0; i++) {
            BigInteger bigInteger4 = bigPrimes[i];
            while (bigInteger3.mod(bigInteger4).compareTo(FrinkBigInteger.ZERO) == 0) {
                bigInteger3 = bigInteger3.divide(bigInteger4);
                factorList.addFactor(FrinkInteger.construct(bigInteger4), 1);
                if (bigInteger3.compareTo(MAX_INT) <= 0) {
                    int intValue2 = bigInteger3.intValue();
                    if (intValue2 == 1) {
                        return null;
                    }
                    factorTrial(intValue2, factorList, i);
                    return null;
                }
            }
        }
        if (bigInteger3.compareTo(FrinkBigInteger.ONE) == 0) {
            return null;
        }
        return bigInteger3;
    }

    public static BigInteger factorPollardPMinus1(BigInteger bigInteger, FactorList factorList) {
        BigInteger singleFactorPollardPMinus1 = singleFactorPollardPMinus1(bigInteger, factorList, 1000000, new PollardPStatus());
        if (singleFactorPollardPMinus1 == null) {
            return bigInteger;
        }
        checkRepeatedFactors(bigInteger, singleFactorPollardPMinus1, factorList);
        return null;
    }

    public static void factorPollardRho(BigInteger bigInteger, FactorList factorList) {
        BigInteger singleFactorPollardRhoBrent = singleFactorPollardRhoBrent(bigInteger, factorList);
        if (singleFactorPollardRhoBrent != null) {
            checkRepeatedFactors(bigInteger, singleFactorPollardRhoBrent, factorList);
        } else {
            factorList.addFactor(FrinkInteger.construct(bigInteger), 1);
        }
    }

    private static void checkRepeatedFactors(BigInteger bigInteger, BigInteger bigInteger2, FactorList factorList) {
        if (isPrime(bigInteger2)) {
            int i = 0;
            BigInteger bigInteger3 = bigInteger;
            do {
                bigInteger3 = bigInteger3.divide(bigInteger2);
                i++;
            } while (bigInteger3.mod(bigInteger2).compareTo(FrinkBigInteger.ZERO) == 0);
            factorList.addFactor(FrinkInteger.construct(bigInteger2), i);
            factorBeyondTrial(bigInteger3, factorList);
            return;
        }
        factorBeyondTrialNotPrime(bigInteger2, factorList);
        factorBeyondTrial(bigInteger.divide(bigInteger2), factorList);
    }

    public static BigInteger singleFactorPollardRho(BigInteger bigInteger, FactorList factorList) {
        BigInteger bigInteger2 = FrinkBigInteger.ONE;
        BigInteger bigInteger3 = FrinkBigInteger.ONE;
        BigInteger mod = bigInteger3.modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger2).mod(bigInteger);
        BigInteger bigInteger4 = FrinkBigInteger.ONE;
        while (true) {
            BigInteger bigInteger5 = bigInteger4;
            BigInteger bigInteger6 = bigInteger2;
            BigInteger bigInteger7 = bigInteger3;
            BigInteger bigInteger8 = mod;
            BigInteger bigInteger9 = bigInteger5;
            int i = 0;
            while (bigInteger9.equals(FrinkBigInteger.ONE)) {
                i++;
                bigInteger9 = bigInteger7.subtract(bigInteger8).gcd(bigInteger);
                bigInteger7 = bigInteger7.modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger6).mod(bigInteger);
                bigInteger8 = bigInteger8.modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger6).modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger6).mod(bigInteger);
            }
            if (!bigInteger9.equals(bigInteger)) {
                return bigInteger9;
            }
            bigInteger2 = bigInteger6.add(FrinkBigInteger.ONE);
            bigInteger3 = FrinkBigInteger.ONE;
            mod = bigInteger3.modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger2).mod(bigInteger);
            bigInteger4 = FrinkBigInteger.ONE;
        }
    }

    public static BigInteger singleFactorPollardRhoBrent(BigInteger bigInteger, FactorList factorList) {
        BigInteger bigInteger2 = FrinkBigInteger.ONE;
        BigInteger bigInteger3 = FrinkBigInteger.TWO;
        BigInteger add = FrinkBigInteger.FOUR.add(bigInteger2);
        BigInteger bigInteger4 = bigInteger2;
        BigInteger bigInteger5 = FrinkBigInteger.ONE;
        BigInteger bigInteger6 = bigInteger3;
        int i = 0;
        BigInteger bigInteger7 = add;
        int i2 = 1;
        while (i <= 2000000000) {
            BigInteger bigInteger8 = bigInteger7;
            int i3 = i;
            BigInteger bigInteger9 = bigInteger5;
            int i4 = 1;
            while (true) {
                if (i4 <= i2) {
                    i3++;
                    bigInteger8 = bigInteger8.modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger4).mod(bigInteger);
                    bigInteger9 = bigInteger9.multiply(bigInteger6.subtract(bigInteger8)).mod(bigInteger);
                    if (i3 % 10 == 0) {
                        BigInteger gcd = bigInteger.gcd(bigInteger9);
                        if (gcd.compareTo(FrinkBigInteger.ONE) <= 0) {
                            bigInteger9 = FrinkBigInteger.ONE;
                        } else if (gcd.compareTo(bigInteger) != 0) {
                            return gcd;
                        } else {
                            BigInteger add2 = bigInteger4.add(FrinkBigInteger.ONE);
                            BigInteger bigInteger10 = FrinkBigInteger.TWO;
                            BigInteger add3 = FrinkBigInteger.FOUR.add(add2);
                            bigInteger4 = add2;
                            bigInteger5 = FrinkBigInteger.ONE;
                            bigInteger6 = bigInteger10;
                            i = i3;
                            bigInteger7 = add3;
                            i2 = 1;
                        }
                    }
                    i4++;
                } else {
                    int i5 = i2 * 2;
                    BigInteger bigInteger11 = bigInteger8;
                    for (int i6 = 1; i6 <= i5; i6++) {
                        bigInteger11 = bigInteger11.modPow(FrinkBigInteger.TWO, bigInteger).add(bigInteger4).mod(bigInteger);
                    }
                    i2 = i5;
                    bigInteger5 = bigInteger9;
                    i = i3;
                    bigInteger7 = bigInteger11;
                    bigInteger6 = bigInteger8;
                }
            }
        }
        return null;
    }

    public static BigInteger singleFactorPollardPMinus1(BigInteger bigInteger, FactorList factorList, int i, PollardPStatus pollardPStatus) {
        BigInteger bigInteger2 = pollardPStatus.m;
        int i2 = 0;
        BigInteger bigInteger3 = pollardPStatus.c;
        int i3 = pollardPStatus.i;
        BigInteger bigInteger4 = bigInteger3;
        while (i2 < i) {
            i3++;
            i2++;
            bigInteger2 = bigInteger2.modPow(BigInteger.valueOf((long) i3), bigInteger);
            if (i3 % 10 == 0) {
                BigInteger gcd = bigInteger2.subtract(FrinkBigInteger.ONE).gcd(bigInteger);
                if (gcd.compareTo(FrinkBigInteger.ONE) <= 0) {
                    continue;
                } else if (gcd.compareTo(bigInteger) != 0) {
                    return gcd;
                } else {
                    BigInteger bigInteger5 = bigInteger4;
                    do {
                        bigInteger5 = bigInteger5.add(FrinkBigInteger.ONE);
                    } while (!isPrime(bigInteger5));
                    bigInteger4 = bigInteger5;
                    bigInteger2 = bigInteger5;
                    i3 = 0;
                }
            }
        }
        pollardPStatus.m = bigInteger2;
        pollardPStatus.c = bigInteger4;
        pollardPStatus.i = i3;
        return null;
    }

    public static int modPow(int i, int i2, int i3) {
        long j = (long) (i % i3);
        long j2 = 1;
        for (int i4 = i2; i4 != 0; i4 >>= 1) {
            if ((i4 & 1) != 0) {
                j2 = (j2 * j) % ((long) i3);
            }
            j = (j * j) % ((long) i3);
        }
        return (int) j2;
    }

    public static boolean isPrime(FrinkInteger frinkInteger) {
        try {
            return isPrime(frinkInteger.getInt());
        } catch (NotAnIntegerException e) {
            return isPrime(frinkInteger.getBigInt());
        }
    }

    public static boolean isStrongPseudoprime(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return isStrongPseudoprime(frinkInteger.getInt(), new int[]{frinkInteger2.getInt()});
        } catch (NotAnIntegerException e) {
            return isStrongPseudoprime(frinkInteger.getBigInt(), new BigInteger[]{frinkInteger2.getBigInt()});
        }
    }

    public static boolean isPrime(int i) {
        if (i == 1) {
            return true;
        }
        if (!primesInitialized) {
            calcPrimes();
        }
        if (i < SIEVE_SIZE) {
            return !primeSieve.get(i);
        }
        int i2 = 73 * 73;
        if (!isPrimeByTrialDivision(i, 73)) {
            return false;
        }
        if (i <= i2) {
            return true;
        }
        if (i < 9080191) {
            return isStrongPseudoprime(i, A_9080191);
        }
        return isStrongPseudoprime(i, A_4759123141);
    }

    private static boolean isStrongPseudoprime(int i, int[] iArr) {
        int i2 = i - 1;
        int i3 = i2;
        int i4 = 0;
        while ((i3 & 1) == 0) {
            i3 >>= 1;
            i4++;
        }
        boolean z = false;
        for (int i5 : iArr) {
            if (i5 <= i2) {
                int modPow = modPow(i5, i3, i);
                if (!(modPow == 1 || modPow == i2)) {
                    for (int i6 = 1; i6 < i4 && modPow != i2; i6++) {
                        modPow = modPow(modPow, 2, i);
                    }
                    if (modPow != i2) {
                        return false;
                    }
                }
                z = true;
            }
        }
        return z;
    }

    public static boolean isPrime(BigInteger bigInteger) {
        if (!bigInteger.testBit(0)) {
            return false;
        }
        int mersenneExponent = getMersenneExponent(bigInteger);
        if (mersenneExponent > 0) {
            return isMersennePrime(bigInteger, mersenneExponent);
        }
        if (bigInteger.compareTo(DIGIT_5) < 0) {
            return isPrimeTest(bigInteger, DIGIT_5_ARRAY);
        }
        if (bigInteger.compareTo(DIGIT_6) < 0) {
            return isPrimeTest(bigInteger, DIGIT_6_ARRAY);
        }
        if (bigInteger.compareTo(DIGIT_7) < 0) {
            return isPrimeTest(bigInteger, DIGIT_7_ARRAY);
        }
        if (!isPrimeTest(bigInteger, DIGIT_7_ARRAY)) {
            return false;
        }
        if (!isStrongPseudoprime(bigInteger, STRONG_TEST_ARRAY)) {
            return false;
        }
        return true;
    }

    public static boolean isPrimeTest(BigInteger bigInteger, BigInteger[] bigIntegerArr) {
        BigInteger bigInteger2 = bigIntegerArr[bigIntegerArr.length - 1];
        boolean isPrimeByTrialDivision = isPrimeByTrialDivision(bigInteger, bigInteger2);
        BigInteger multiply = bigInteger2.multiply(bigInteger2);
        if (!isPrimeByTrialDivision) {
            return false;
        }
        if (bigInteger.compareTo(multiply) <= 0) {
            return true;
        }
        return isStrongPseudoprime(bigInteger, bigIntegerArr);
    }

    public static boolean isPrimeByTrialDivision(BigInteger bigInteger, BigInteger bigInteger2) {
        if (!primesInitialized) {
            calcPrimes();
        }
        int length = primes.length;
        int i = 0;
        while (i < length) {
            BigInteger valueOf = BigInteger.valueOf((long) primes[i]);
            if (valueOf.compareTo(bigInteger2) > 0) {
                break;
            } else if (bigInteger.mod(valueOf).compareTo(FrinkBigInteger.ZERO) == 0) {
                return false;
            } else {
                i++;
            }
        }
        return true;
    }

    private static boolean isStrongPseudoprime(BigInteger bigInteger, BigInteger[] bigIntegerArr) {
        BigInteger bigInteger2;
        BigInteger subtract = bigInteger.subtract(FrinkBigInteger.ONE);
        int lowestSetBit = subtract.getLowestSetBit();
        if (lowestSetBit > 0) {
            bigInteger2 = subtract.shiftRight(lowestSetBit);
        } else {
            bigInteger2 = subtract;
        }
        boolean z = false;
        for (BigInteger bigInteger3 : bigIntegerArr) {
            if (bigInteger3.compareTo(subtract) <= 0) {
                BigInteger modPow = bigInteger3.modPow(bigInteger2, bigInteger);
                if (!modPow.equals(FrinkBigInteger.ONE) && !modPow.equals(subtract)) {
                    if (1 >= lowestSetBit) {
                        return false;
                    }
                    int i = 1;
                    do {
                        modPow = modPow.modPow(FrinkBigInteger.TWO, bigInteger);
                        i++;
                        if (i >= lowestSetBit) {
                            break;
                        }
                    } while (!modPow.equals(subtract));
                    if (!modPow.equals(subtract)) {
                        return false;
                    }
                }
                z = true;
            }
        }
        return z;
    }

    public static boolean isPrimeByTrialDivision(int i, int i2) {
        int sqrt = (int) Math.sqrt((double) i);
        if (!primesInitialized) {
            calcPrimes();
        }
        int length = primes.length;
        int i3 = 0;
        while (i3 < length) {
            int i4 = primes[i3];
            if (i4 > sqrt || i4 > i2) {
                break;
            } else if (i % i4 == 0) {
                return false;
            } else {
                i3++;
            }
        }
        return true;
    }

    private static synchronized void calcPrimes() {
        synchronized (Factor.class) {
            if (!primesInitialized) {
                int sqrt = (int) Math.sqrt(46340.0d);
                primeSieve = new BitSet(SIEVE_SIZE);
                synchronized (primeSieve) {
                    primeSieve.set(1);
                    for (int i = 2; i <= sqrt; i++) {
                        if (!primeSieve.get(i)) {
                            for (int i2 = i * i; i2 < SIEVE_SIZE; i2 += i) {
                                primeSieve.set(i2);
                            }
                        }
                    }
                    int i3 = 0;
                    for (int i4 = 2; i4 < SIEVE_SIZE; i4++) {
                        if (!primeSieve.get(i4)) {
                            i3++;
                        }
                    }
                    primes = new int[i3];
                    bigPrimes = new BigInteger[i3];
                    int i5 = 0;
                    for (int i6 = 2; i6 < SIEVE_SIZE; i6++) {
                        if (!primeSieve.get(i6)) {
                            primes[i5] = i6;
                            bigPrimes[i5] = BigInteger.valueOf((long) i6);
                            i5++;
                        }
                    }
                }
                primesInitialized = true;
            }
        }
    }

    public static BigInteger detectPerfectPowers(BigInteger bigInteger, FactorList factorList) {
        return detectMersenne(bigInteger, factorList);
    }

    public static BigInteger detectMersenne(BigInteger bigInteger, FactorList factorList) {
        int mersenneExponent = getMersenneExponent(bigInteger);
        if (mersenneExponent == 0) {
            return bigInteger;
        }
        if (isPrime(mersenneExponent)) {
            return bigInteger;
        }
        FactorList factorList2 = new FactorList();
        factor(mersenneExponent, factorList2);
        try {
            BigInteger subtract = FrinkBigInteger.ONE.shiftLeft(mersenneExponent / factorList2.getSmallestFactor().getFactor().getInt()).subtract(FrinkBigInteger.ONE);
            factor(subtract, factorList);
            return bigInteger.divide(subtract);
        } catch (NotAnIntegerException e) {
            Enumeration<FactorList.Factors> enumeration = factorList2.getEnumeration();
            while (enumeration.hasMoreElements()) {
                try {
                    System.out.println("Reduced exponent " + (mersenneExponent / enumeration.nextElement().getFactor().getInt()));
                } catch (NotAnIntegerException e2) {
                }
            }
            return bigInteger;
        }
    }

    public static int getMersenneExponent(BigInteger bigInteger) {
        int bitLength = bigInteger.bitLength();
        if (bitLength < 2) {
            return 0;
        }
        if (bigInteger.bitCount() != bitLength) {
            return 0;
        }
        return bitLength;
    }

    private static boolean isMersennePrime(BigInteger bigInteger, int i) {
        if (!isPrime(i)) {
            return false;
        }
        BigInteger bigInteger2 = FrinkBigInteger.FOUR;
        for (int i2 = 2; i2 < i; i2++) {
            bigInteger2 = bigInteger2.modPow(FrinkBigInteger.TWO, bigInteger).subtract(FrinkBigInteger.TWO);
            if (bigInteger2.signum() == -1) {
                bigInteger2 = bigInteger2.add(bigInteger);
            }
        }
        return bigInteger2.signum() == 0;
    }

    private static synchronized void initWheel() {
        synchronized (Factor.class) {
            if (!wheelInitialized) {
                int i = 1;
                for (int i2 : wheelElements) {
                    i *= i2;
                }
                wheelSize = i;
                bigIntWheelSize = BigInteger.valueOf((long) i);
                wheelFlags = new BitSet(wheelSize);
                for (int i3 = 0; i3 < wheelSize; i3++) {
                    wheelFlags.clear(i3);
                }
                for (int i4 : wheelElements) {
                    for (int i5 = 0; i5 < wheelSize; i5 += i4) {
                        wheelFlags.set(i5);
                    }
                }
                int i6 = 0;
                for (int i7 = 0; i7 < wheelSize; i7++) {
                    if (!wheelFlags.get(i7)) {
                        i6++;
                    }
                }
                intWheelAddends = new int[wheelSize];
                bigIntWheelAddends = new BigInteger[wheelSize];
                intWheelAddends[wheelSize - 1] = 2;
                bigIntWheelAddends[wheelSize - 1] = FrinkBigInteger.TWO;
                int i8 = 1;
                for (int i9 = wheelSize - 2; i9 >= 0; i9--) {
                    intWheelAddends[i9] = i8;
                    bigIntWheelAddends[i9] = BigInteger.valueOf((long) i8);
                    if (wheelFlags.get(i9)) {
                        i8++;
                    } else {
                        i8 = 1;
                    }
                }
                largestWheelableInt = (Integer.MAX_VALUE - wheelSize) - 2;
                wheelInitialized = true;
            }
        }
    }

    public static FrinkInteger nextPrime(FrinkInteger frinkInteger) {
        try {
            return nextPrime(frinkInteger.getInt());
        } catch (NotAnIntegerException e) {
            return nextPrime(frinkInteger.getBigInt());
        }
    }

    private static FrinkInteger nextPrime(int i) {
        int i2;
        if (i < 2) {
            return FrinkInt.TWO;
        }
        if (i == 2) {
            return FrinkInt.THREE;
        }
        if (!primesInitialized) {
            calcPrimes();
        }
        if (!wheelInitialized) {
            initWheel();
        }
        if (i < wheelSize) {
            if (i % 2 == 0) {
                i2 = i - 1;
            } else {
                i2 = i;
            }
            do {
                i2 += 2;
            } while (!isPrime(i2));
            return FrinkInt.construct(i2);
        }
        int i3 = i;
        while (i3 <= largestWheelableInt) {
            i3 += intWheelAddends[i3 % wheelSize];
            if (isPrime(i3)) {
                return FrinkInt.construct(i3);
            }
        }
        return nextPrime(BigInteger.valueOf((long) i3));
    }

    private static FrinkInteger nextPrime(BigInteger bigInteger) {
        if (!primesInitialized) {
            calcPrimes();
        }
        if (!wheelInitialized) {
            initWheel();
        }
        if (bigInteger.compareTo(bigIntWheelSize) < 0) {
            return nextPrime(bigInteger.intValue());
        }
        BigInteger bigInteger2 = bigInteger;
        do {
            bigInteger2 = bigInteger2.add(bigIntWheelAddends[bigInteger2.mod(bigIntWheelSize).intValue()]);
        } while (!isPrime(bigInteger2));
        return FrinkInteger.construct(bigInteger2);
    }
}
