package frink.function;

import frink.errors.ConformanceException;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.EvaluationNumericException;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.MultiplyExpression;
import frink.numeric.NumericException;

public class Productator {
    public static Expression multiply(Expression expression, Environment environment) throws EvaluationException, InvalidChildException {
        if (expression instanceof ListExpression) {
            return multiplyList((ListExpression) expression, environment);
        }
        return expression instanceof EnumeratingExpression ? multiplyEnum((EnumeratingExpression) expression, environment) : expression;
    }

    private static Expression multiplyList(ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException {
        int childCount = listExpression.getChildCount();
        if (childCount == 0) {
            return DimensionlessUnitExpression.ONE;
        }
        if (childCount == 1) {
            return listExpression.getChild(0);
        }
        try {
            return MultiplyExpression.construct(listExpression, environment).evaluate(environment);
        } catch (ConformanceException e) {
            throw new EvaluationConformanceException(e.getMessage(), listExpression);
        } catch (NumericException e2) {
            throw new EvaluationNumericException(e2.getMessage(), listExpression);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0051  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0044=Splitter:B:21:0x0044, B:30:0x0057=Splitter:B:30:0x0057} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static frink.expr.Expression multiplyEnum(frink.expr.EnumeratingExpression r9, frink.expr.Environment r10) throws frink.expr.EvaluationException, frink.expr.InvalidChildException {
        /*
            r7 = 0
            r6 = 0
            frink.expr.FrinkEnumeration r0 = r9.getEnumeration(r10)     // Catch:{ ConformanceException -> 0x0042, NumericException -> 0x0055, all -> 0x0061 }
            r1 = r7
            r2 = r6
            r3 = r6
        L_0x0009:
            frink.expr.Expression r4 = r0.getNext(r10)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            if (r4 == 0) goto L_0x0032
            if (r3 != 0) goto L_0x001d
            frink.expr.BasicListExpression r3 = new frink.expr.BasicListExpression     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            r5 = 1
            r3.<init>(r5)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            if (r2 == 0) goto L_0x001d
            r3.appendChild(r2)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            r2 = r6
        L_0x001d:
            r3.appendChild(r4)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            int r1 = r1 + 1
            r4 = 1000(0x3e8, float:1.401E-42)
            if (r1 != r4) goto L_0x0009
            frink.expr.Expression r1 = frink.expr.MultiplyExpression.construct(r3, r10)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            frink.expr.Expression r1 = r1.evaluate(r10)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            r2 = r1
            r3 = r6
            r1 = r7
            goto L_0x0009
        L_0x0032:
            if (r3 == 0) goto L_0x0073
            frink.expr.Expression r1 = frink.expr.MultiplyExpression.construct(r3, r10)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
            frink.expr.Expression r1 = r1.evaluate(r10)     // Catch:{ ConformanceException -> 0x006e, NumericException -> 0x0069, all -> 0x0064 }
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.dispose()
        L_0x0041:
            return r1
        L_0x0042:
            r0 = move-exception
            r1 = r6
        L_0x0044:
            frink.expr.EvaluationConformanceException r2 = new frink.expr.EvaluationConformanceException     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x004e }
            r2.<init>(r0, r9)     // Catch:{ all -> 0x004e }
            throw r2     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.dispose()
        L_0x0054:
            throw r0
        L_0x0055:
            r0 = move-exception
            r1 = r6
        L_0x0057:
            frink.expr.EvaluationNumericException r2 = new frink.expr.EvaluationNumericException     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x004e }
            r2.<init>(r0, r9)     // Catch:{ all -> 0x004e }
            throw r2     // Catch:{ all -> 0x004e }
        L_0x0061:
            r0 = move-exception
            r1 = r6
            goto L_0x004f
        L_0x0064:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x004f
        L_0x0069:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0057
        L_0x006e:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0044
        L_0x0073:
            r1 = r2
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.function.Productator.multiplyEnum(frink.expr.EnumeratingExpression, frink.expr.Environment):frink.expr.Expression");
    }
}
