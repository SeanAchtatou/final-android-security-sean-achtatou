package frink.function;

import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.ExpressionFactory;

public class FunctionDescriptorExpressionFactory implements ExpressionFactory<FunctionDescriptor> {
    public static final FunctionDescriptorExpressionFactory INSTANCE = new FunctionDescriptorExpressionFactory();

    public Expression makeExpression(FunctionDescriptor functionDescriptor, Environment environment) {
        return new BasicStringExpression(functionDescriptor.toString(environment));
    }
}
