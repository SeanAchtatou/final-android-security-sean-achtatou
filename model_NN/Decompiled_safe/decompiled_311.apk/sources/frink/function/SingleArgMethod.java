package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public abstract class SingleArgMethod<T extends Expression> extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doMethod(Environment environment, Expression expression, Expression expression2) throws EvaluationException;

    public SingleArgMethod(boolean z) {
        super(1, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return doMethod(environment, environment.getSymbolDefinition("this", false).getValue(), expression.getChild(0));
    }
}
