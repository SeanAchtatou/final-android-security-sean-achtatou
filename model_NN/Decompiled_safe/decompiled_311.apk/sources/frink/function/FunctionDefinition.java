package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public interface FunctionDefinition {
    FunctionArgument getArgument(int i);

    int getArgumentCount();

    String getReturnTypeString();

    boolean hasNamedArguments();

    boolean isDeterministicAndHasNoSideeffects();

    Expression performEvaluation(Environment environment, Expression expression) throws EvaluationException;

    boolean shouldEvaluateFirst();
}
