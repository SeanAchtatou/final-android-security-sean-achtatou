package frink.function;

import frink.expr.Expression;
import java.util.Vector;

public interface FunctionArgument {
    Vector<String> getConstraints();

    Expression getDefaultValue();

    String getName();

    String getType();
}
