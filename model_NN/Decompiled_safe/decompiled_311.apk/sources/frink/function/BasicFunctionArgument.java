package frink.function;

import frink.expr.Expression;
import java.util.Vector;

public class BasicFunctionArgument implements FunctionArgument {
    private Vector<String> constraints;
    private Expression defaultValue;
    private String name;
    private String type;

    public BasicFunctionArgument(String str, Vector<String> vector, Expression expression) {
        this.name = str;
        this.constraints = vector;
        this.defaultValue = expression;
        this.type = null;
    }

    public BasicFunctionArgument(String str, Vector<String> vector, Expression expression, String str2) {
        this.name = str;
        this.constraints = vector;
        this.defaultValue = expression;
        this.type = str2;
    }

    public BasicFunctionArgument(String str) {
        this(str, null, null, null);
    }

    public String getType() {
        return this.type;
    }

    public BasicFunctionArgument() {
        this(null, null, null, null);
    }

    public String getName() {
        return this.name;
    }

    public Vector<String> getConstraints() {
        return this.constraints;
    }

    public Expression getDefaultValue() {
        return this.defaultValue;
    }

    public void setDefaultValue(Expression expression) {
        this.defaultValue = expression;
    }
}
