package frink.function;

import frink.expr.BasicListExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.FunctionDefinitionExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.ListExpression;
import frink.expr.RegexpExpression;
import frink.expr.StringExpression;
import frink.expr.Truth;
import frink.object.FrinkObject;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Matcher;

public class SelectFunction {
    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.BasicListExpression, int, ?[OBJECT, ARRAY], int]
     candidates:
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    public static Expression select(Expression expression, Expression expression2, Expression expression3, Environment environment) throws EvaluationException {
        int i = 0;
        if (expression2 instanceof RegexpExpression) {
            Pattern pattern = ((RegexpExpression) expression2).getPattern();
            Perl5Matcher perl5Matcher = new Perl5Matcher();
            BasicListExpression basicListExpression = new BasicListExpression(1);
            if (expression instanceof EnumeratingExpression) {
                FrinkEnumeration enumeration = ((EnumeratingExpression) expression).getEnumeration(environment);
                while (true) {
                    try {
                        Expression next = enumeration.getNext(environment);
                        if (next == null) {
                            enumeration.dispose();
                            return basicListExpression;
                        } else if (!(next instanceof StringExpression)) {
                            throw new InvalidArgumentException("List element in select[list, enumerator] is not a string: " + environment.format(next), expression);
                        } else if (perl5Matcher.contains(((StringExpression) next).getString(), pattern)) {
                            basicListExpression.appendChild(next);
                        }
                    } catch (Throwable th) {
                        enumeration.dispose();
                        throw th;
                    }
                }
            } else if (expression instanceof ListExpression) {
                ListExpression listExpression = (ListExpression) expression;
                int childCount = listExpression.getChildCount();
                while (i < childCount) {
                    Expression child = listExpression.getChild(i);
                    if (child instanceof StringExpression) {
                        if (perl5Matcher.contains(((StringExpression) child).getString(), pattern)) {
                            basicListExpression.appendChild(child);
                        }
                        i++;
                    } else {
                        throw new InvalidArgumentException("List element in select[list, regex] is not a string: " + environment.format(child), expression);
                    }
                }
                return basicListExpression;
            }
        }
        if (!(expression2 instanceof FunctionDefinitionExpression)) {
            throw new InvalidArgumentException("Second argument to select must be an anonymous function or a regular expression.", expression2);
        }
        FunctionDefinition functionDefinition = ((FunctionDefinitionExpression) expression2).getFunctionDefinition();
        BasicListExpression basicListExpression2 = new BasicListExpression(1);
        BasicListExpression basicListExpression3 = new BasicListExpression(2);
        if (expression3 != null) {
            basicListExpression3.setChild(1, expression3);
        }
        if (expression instanceof EnumeratingExpression) {
            FrinkEnumeration enumeration2 = ((EnumeratingExpression) expression).getEnumeration(environment);
            while (true) {
                try {
                    Expression next2 = enumeration2.getNext(environment);
                    if (next2 != null) {
                        basicListExpression3.setChild(0, next2);
                        if (Truth.isTrue(environment, environment.getFunctionManager().execute(functionDefinition, environment, (Expression) basicListExpression3, false, (FrinkObject) null, false))) {
                            basicListExpression2.appendChild(next2);
                        }
                    } else {
                        enumeration2.dispose();
                        return basicListExpression2;
                    }
                } catch (Throwable th2) {
                    enumeration2.dispose();
                    throw th2;
                }
            }
        } else if (expression instanceof ListExpression) {
            ListExpression listExpression2 = (ListExpression) expression;
            int childCount2 = listExpression2.getChildCount();
            for (int i2 = 0; i2 < childCount2; i2++) {
                Expression child2 = listExpression2.getChild(i2);
                basicListExpression3.setChild(0, child2);
                if (Truth.isTrue(environment, environment.getFunctionManager().execute(functionDefinition, environment, (Expression) basicListExpression3, false, (FrinkObject) null, false))) {
                    basicListExpression2.appendChild(child2);
                }
            }
            return basicListExpression2;
        } else {
            throw new InvalidArgumentException("First argument to select must be a list or enumeration.", expression);
        }
    }
}
