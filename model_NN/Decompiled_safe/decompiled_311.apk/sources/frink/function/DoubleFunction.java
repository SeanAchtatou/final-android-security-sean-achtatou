package frink.function;

import frink.expr.BasicUnitExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.UnitExpression;
import frink.numeric.FrinkFloat;
import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;

public abstract class DoubleFunction extends AbstractFunctionDefinition {
    private Unit inUnit;
    private String inUnitName;
    private boolean initialized = false;
    private Unit outUnit;
    private String outUnitName;

    /* access modifiers changed from: protected */
    public abstract double doFunction(double d);

    public DoubleFunction(String str, String str2, boolean z) {
        super(1, z);
        this.inUnitName = str;
        this.outUnitName = str2;
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        if (!this.initialized) {
            initializeUnits(environment);
        }
        try {
            Expression child = expression.getChild(0);
            if (!(child instanceof UnitExpression)) {
                throw new InvalidArgumentException("DoubleFunction: Bad argument: " + environment.format(child), child);
            }
            Unit unit = ((UnitExpression) child).getUnit();
            try {
                if (this.inUnit != null) {
                    unit = UnitMath.divide(unit, this.inUnit);
                }
                if (!UnitMath.isDimensionless(unit)) {
                    throw new InvalidArgumentException("Not of type " + this.inUnitName, child);
                }
                double doFunction = doFunction(unit.getScale().doubleValue());
                if (this.outUnit != null) {
                    return BasicUnitExpression.construct(UnitMath.multiply(DimensionlessUnitExpression.construct(new FrinkFloat(doFunction)), this.outUnit));
                }
                return DimensionlessUnitExpression.construct(doFunction);
            } catch (NumericException e) {
                throw new InvalidArgumentException("DoubleFunction: numeric error:\n " + e, child);
            }
        } catch (InvalidChildException e2) {
            throw new InvalidArgumentException("Bad child", expression);
        }
    }

    private void initializeUnits(Environment environment) throws EvaluationException {
        if (this.inUnitName != null && this.inUnit == null) {
            this.inUnit = environment.getUnitManager().getUnit(this.inUnitName);
            if (this.inUnit == null) {
                throw new InvalidArgumentException("Can't find " + this.inUnitName, null);
            }
        }
        if (this.outUnitName != null && this.outUnit == null) {
            this.outUnit = environment.getUnitManager().getUnit(this.outUnitName);
            if (this.outUnit == null) {
                throw new InvalidArgumentException("Can't find " + this.outUnitName, null);
            }
        }
    }
}
