package frink.function;

import frink.errors.NotComparableException;
import frink.expr.AddExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FactorialExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.MultiplyExpression;
import frink.expr.PowerExpression;
import frink.expr.SymbolExpression;
import frink.expr.ThreeWayComparison;
import frink.expr.UnitExpression;
import frink.numeric.OverlapException;
import frink.symbolic.ExpressionPattern;

public class SymbolOrderer implements Orderer {
    public static final SymbolOrderer INSTANCE = new SymbolOrderer();
    private static final InvalidArgumentException SORTING_PATTERNS_EXCEPTION = new InvalidArgumentException("Shouldn't sort patterns.", null);

    private SymbolOrderer() {
    }

    public int compare(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        try {
            return staticCompare(expression, expression2, environment);
        } catch (InvalidChildException e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return 0;
        }
    }

    public static int staticCompare(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        if ((expression instanceof ExpressionPattern) || (expression2 instanceof ExpressionPattern)) {
            throw SORTING_PATTERNS_EXCEPTION;
        } else if (expression instanceof UnitExpression) {
            if (!(expression2 instanceof UnitExpression)) {
                return -1;
            }
            try {
                return ThreeWayComparison.compare(expression, expression2, environment);
            } catch (NotComparableException e) {
                return 0;
            } catch (OverlapException e2) {
                return 0;
            }
        } else if (expression2 instanceof UnitExpression) {
            return 1;
        } else {
            if (expression instanceof MultiplyExpression) {
                return compareMultiplyOrAddSame(expression, expression2, environment);
            }
            if (expression2 instanceof MultiplyExpression) {
                return -compareMultiplyOrAddSame(expression2, expression, environment);
            }
            if (expression instanceof PowerExpression) {
                return comparePower(expression, expression2, environment);
            }
            if (expression2 instanceof PowerExpression) {
                return -comparePower(expression2, expression, environment);
            }
            if (expression instanceof AddExpression) {
                return compareMultiplyOrAddSame(expression, expression2, environment);
            }
            if (expression2 instanceof AddExpression) {
                return -compareMultiplyOrAddSame(expression2, expression, environment);
            }
            if (expression instanceof FactorialExpression) {
                return compareFactorial(expression, expression2, environment);
            }
            if (expression2 instanceof FactorialExpression) {
                return -compareFactorial(expression2, expression, environment);
            }
            if (expression instanceof FunctionCallExpression) {
                return compareFunction(expression, expression2, environment);
            }
            if (expression2 instanceof FunctionCallExpression) {
                return -compareFunction(expression2, expression, environment);
            }
            if (expression instanceof SymbolExpression) {
                return compareSymbol(expression, expression2, environment);
            }
            if (expression2 instanceof SymbolExpression) {
                return -compareSymbol(expression2, expression, environment);
            }
            throw new InvalidArgumentException("Cannot sort " + environment.format(expression) + " (" + expression.getExpressionType() + ") and " + environment.format(expression2) + " (" + expression.getExpressionType() + ").", expression);
        }
    }

    private static int compareMultiplyOrAddSame(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        int staticCompare;
        int childCount = expression.getChildCount() - 1;
        int childCount2 = expression2.getChildCount() - 1;
        if ((expression.getExpressionType() != expression2.getExpressionType() || (childCount2 < 0 && childCount >= 0)) && (staticCompare = staticCompare(expression.getChild(childCount), expression2, environment)) != 0) {
            return staticCompare;
        }
        int i = childCount2;
        int i2 = childCount;
        int i3 = i;
        while (i2 >= 0 && i3 >= 0) {
            int staticCompare2 = staticCompare(expression.getChild(i2), expression2.getChild(i3), environment);
            if (staticCompare2 != 0) {
                return staticCompare2;
            }
            i2--;
            i3--;
        }
        return threeWayCompare(i2, i3);
    }

    private static int compareLastChild(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        int childCount = expression.getChildCount();
        int staticCompare = staticCompare(expression.getChild(childCount - 1), expression2, environment);
        if (staticCompare != 0) {
            return staticCompare;
        }
        return threeWayCompare(childCount, 1);
    }

    private static int comparePower(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        if (expression2 instanceof PowerExpression) {
            int staticCompare = staticCompare(expression.getChild(0), expression2.getChild(0), environment);
            if (staticCompare != 0) {
                return staticCompare;
            }
            return staticCompare(expression.getChild(1), expression2.getChild(1), environment);
        }
        int staticCompare2 = staticCompare(expression.getChild(0), expression2, environment);
        return staticCompare2 == 0 ? staticCompare(expression.getChild(1), DimensionlessUnitExpression.ONE, environment) : staticCompare2;
    }

    private static int compareFactorial(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        if (expression2 instanceof FactorialExpression) {
            return staticCompare(expression.getChild(0), expression2.getChild(0), environment);
        }
        return compareLastChild(expression, expression2, environment);
    }

    private static int compareFunction(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        if (expression2 instanceof FunctionCallExpression) {
            int compareTo = ((FunctionCallExpression) expression).getName().compareTo(((FunctionCallExpression) expression2).getName());
            if (compareTo != 0) {
                return threeWayCompare(compareTo, 0);
            }
            int childCount = expression.getChildCount();
            int childCount2 = expression2.getChildCount();
            int i = childCount < childCount2 ? childCount : childCount2;
            for (int i2 = 1; i2 < i; i2++) {
                int staticCompare = staticCompare(expression.getChild(i2), expression2.getChild(i2), environment);
                if (staticCompare != 0) {
                    return staticCompare;
                }
            }
            return threeWayCompare(childCount, childCount2);
        } else if (expression2 instanceof SymbolExpression) {
            return 1;
        } else {
            return -1;
        }
    }

    private static int compareSymbol(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        if (expression2 instanceof SymbolExpression) {
            return threeWayCompare(((SymbolExpression) expression).getName().compareTo(((SymbolExpression) expression2).getName()), 0);
        }
        return -1;
    }

    private static int threeWayCompare(int i, int i2) {
        if (i == i2) {
            return 0;
        }
        if (i < i2) {
            return -1;
        }
        return 1;
    }
}
