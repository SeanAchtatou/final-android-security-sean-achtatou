package frink.function;

import frink.numeric.FrinkBigInteger;
import java.math.BigInteger;

class PollardPStatus {
    public BigInteger c = FrinkBigInteger.TWO;
    public int i = 1;
    public BigInteger m = FrinkBigInteger.TWO;

    PollardPStatus() {
    }
}
