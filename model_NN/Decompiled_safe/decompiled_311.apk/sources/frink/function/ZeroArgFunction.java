package frink.function;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public abstract class ZeroArgFunction extends AbstractFunctionDefinition {
    /* access modifiers changed from: protected */
    public abstract Expression doFunction(Environment environment) throws EvaluationException;

    public ZeroArgFunction(boolean z) {
        super(0, z);
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        return doFunction(environment);
    }
}
