package frink.function;

import frink.errors.NotAnIntegerException;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import java.math.BigInteger;

public class GrayCodes {
    public static FrinkInteger binaryToGray(FrinkInteger frinkInteger) {
        try {
            return FrinkInteger.construct(binaryToGray(frinkInteger.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInt.construct(binaryToGray(frinkInteger.getBigInt()));
        }
    }

    public static int binaryToGray(int i) {
        return (i >>> 1) ^ i;
    }

    public static BigInteger binaryToGray(BigInteger bigInteger) {
        return bigInteger.xor(bigInteger.shiftRight(1));
    }

    public static FrinkInteger grayToBinary(FrinkInteger frinkInteger) {
        try {
            return FrinkInteger.construct(grayToBinary(frinkInteger.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInt.construct(grayToBinary(frinkInteger.getBigInt()));
        }
    }

    public static int grayToBinary(int i) {
        int i2 = i;
        for (int i3 = i >> 1; i3 != 0; i3 >>= 1) {
            i2 ^= i3;
        }
        return i2;
    }

    public static BigInteger grayToBinary(BigInteger bigInteger) {
        BigInteger bigInteger2 = bigInteger;
        for (BigInteger shiftRight = bigInteger.shiftRight(1); shiftRight.compareTo(BigInteger.ZERO) != 0; shiftRight = shiftRight.shiftRight(1)) {
            bigInteger2 = bigInteger2.xor(shiftRight);
        }
        return bigInteger2;
    }
}
