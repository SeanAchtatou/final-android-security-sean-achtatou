package frink.function;

import frink.errors.NotAnIntegerException;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import java.math.BigInteger;

public class Binomial {
    public static FrinkInteger binomial(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return binomial(frinkInteger.getInt(), frinkInteger2.getInt());
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(binomial(frinkInteger.getBigInt(), frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger binomial(int i, int i2) {
        if (i < i2) {
            return FrinkInt.ZERO;
        }
        int i3 = i - i2;
        if (i3 >= i2) {
            i3 = i2;
        }
        int i4 = i;
        long j = 1;
        for (int i5 = 1; i5 <= i3; i5++) {
            i4--;
            j = (j * ((long) i4)) / ((long) i5);
            if (Long.MAX_VALUE / j < ((long) i4)) {
                return FrinkInteger.construct(binomial(BigInteger.valueOf((long) i4), BigInteger.valueOf((long) i3), BigInteger.valueOf(j), BigInteger.valueOf((long) (i5 + 1))));
            }
        }
        return FrinkInteger.construct(j);
    }

    public static BigInteger binomial(BigInteger bigInteger, BigInteger bigInteger2) {
        if (bigInteger.compareTo(bigInteger2) < 0) {
            return BigInteger.ZERO;
        }
        BigInteger subtract = bigInteger.subtract(bigInteger2);
        if (subtract.compareTo(bigInteger2) >= 0) {
            subtract = bigInteger2;
        }
        return binomial(bigInteger, subtract, BigInteger.ONE, BigInteger.ONE);
    }

    private static BigInteger binomial(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) {
        BigInteger bigInteger5 = bigInteger3;
        BigInteger bigInteger6 = bigInteger;
        for (BigInteger bigInteger7 = bigInteger4; bigInteger7.compareTo(bigInteger2) <= 0; bigInteger7 = bigInteger7.add(BigInteger.ONE)) {
            BigInteger multiply = bigInteger5.multiply(bigInteger6);
            bigInteger6 = bigInteger6.subtract(BigInteger.ONE);
            bigInteger5 = multiply.divide(bigInteger7);
        }
        return bigInteger5;
    }
}
