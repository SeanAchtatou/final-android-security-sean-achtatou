package frink.function;

import frink.date.DateMath;
import frink.date.FrinkDate;
import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import frink.expr.BasicDateExpression;
import frink.expr.BasicStringExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.DateExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkBoolean;
import frink.expr.FrinkSecurityException;
import frink.expr.InvalidArgumentException;
import frink.expr.ListExpression;
import frink.expr.PowerExpression;
import frink.expr.Truth;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.VoidExpression;
import frink.numeric.BitwiseOperators;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.RealInterval;
import frink.numeric.RealMath;
import frink.units.Unit;
import frink.units.UnitMath;
import java.math.BigInteger;

public class MathFunctionSource extends BasicFunctionSource {
    private static final String RADIAN = "radian";
    private Unit radian = null;

    public MathFunctionSource() {
        super("MathFunctionSource");
        initializeFunctions();
    }

    private void initializeFunctions() {
        AnonymousClass1 r1 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.sin(numeric);
            }
        };
        addFunctionDefinition("sin", r1);
        addFunctionDefinition("sine", r1);
        AnonymousClass2 r12 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.csc(numeric);
            }
        };
        addFunctionDefinition("csc", r12);
        addFunctionDefinition("cosecant", r12);
        AnonymousClass3 r13 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.cos(numeric);
            }
        };
        addFunctionDefinition("cos", r13);
        addFunctionDefinition("cosine", r13);
        AnonymousClass4 r14 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.sec(numeric);
            }
        };
        addFunctionDefinition("sec", r14);
        addFunctionDefinition("secant", r14);
        AnonymousClass5 r15 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.tan(numeric);
            }
        };
        addFunctionDefinition("tan", r15);
        addFunctionDefinition("tangent", r15);
        AnonymousClass6 r16 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.cot(numeric);
            }
        };
        addFunctionDefinition("cot", r16);
        addFunctionDefinition("cotangent", r16);
        AnonymousClass7 r17 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arcsin(numeric);
            }
        };
        addFunctionDefinition("asin", r17);
        addFunctionDefinition("arcsin", r17);
        addFunctionDefinition("arccsc", new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arccsc(numeric);
            }
        });
        AnonymousClass9 r18 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arccos(numeric);
            }
        };
        addFunctionDefinition("acos", r18);
        addFunctionDefinition("arccos", r18);
        addFunctionDefinition("arcsec", new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arcsec(numeric);
            }
        });
        AnonymousClass11 r19 = new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arctan(numeric);
            }
        };
        addFunctionDefinition("atan", r19);
        addFunctionDefinition("arctan", r19);
        addFunctionDefinition("arccot", new SingleArgNumericFunction(RADIAN, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arccot(numeric);
            }
        });
        addFunctionDefinition("ln", new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.ln(numeric);
            }
        });
        addFunctionDefinition("exp", new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.exp(numeric);
            }
        });
        addFunctionDefinition("log", new SingleArgNumericFunction(null, null, true) {
            private final FrinkFloat log10 = new FrinkFloat(Math.log(10.0d));

            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.divide(NumericMath.ln(numeric), this.log10);
            }
        });
        ArcTanFunction arcTanFunction = new ArcTanFunction();
        addFunctionDefinition("atan", arcTanFunction);
        addFunctionDefinition("arctan", arcTanFunction);
        AnonymousClass16 r110 = new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    return PowerExpression.reciprocal(expression, environment).evaluate(environment);
                }
                throw new InvalidArgumentException("Argument to reciprocal should be Unit.", expression);
            }
        };
        addFunctionDefinition("inv", r110);
        addFunctionDefinition("recip", r110);
        addFunctionDefinition("reciprocal", r110);
        addFunctionDefinition("floor", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    try {
                        return BasicUnitExpression.construct(UnitMath.floor(((UnitExpression) expression).getUnit()));
                    } catch (ConformanceException e) {
                        throw new InvalidArgumentException(e.getMessage(), expression);
                    }
                } else {
                    throw new InvalidArgumentException("Argument to floor should be a Unit.", expression);
                }
            }
        });
        AnonymousClass18 r111 = new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    try {
                        return BasicUnitExpression.construct(UnitMath.ceil(((UnitExpression) expression).getUnit()));
                    } catch (ConformanceException e) {
                        throw new InvalidArgumentException(e.getMessage(), expression);
                    }
                } else {
                    throw new InvalidArgumentException("Argument to ceil should be Unit.", expression);
                }
            }
        };
        addFunctionDefinition("ceil", r111);
        addFunctionDefinition("ceiling", r111);
        addFunctionDefinition("round", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    try {
                        return BasicUnitExpression.construct(UnitMath.round(((UnitExpression) expression).getUnit()));
                    } catch (ConformanceException e) {
                        throw new InvalidArgumentException(e.getMessage(), expression);
                    }
                } else {
                    throw new InvalidArgumentException("Argument to round should be Unit.", expression);
                }
            }
        });
        addFunctionDefinition("round", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, NumericException {
                if (!(expression instanceof UnitExpression) || !(expression2 instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Arguments to round[x,y] should be Unit.", expression);
                }
                try {
                    return BasicUnitExpression.construct(UnitMath.round(((UnitExpression) expression).getUnit(), ((UnitExpression) expression2).getUnit()));
                } catch (ConformanceException e) {
                    throw new EvaluationConformanceException("Arguments to round[x,y] must be conformal.", expression);
                }
            }
        });
        addFunctionDefinition("gcd", new DoubleArgNumericFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Numeric numeric, Numeric numeric2) throws EvaluationException {
                if (numeric.isInt() && numeric2.isInt()) {
                    return DimensionlessUnitExpression.construct(FrinkInteger.gcd(((FrinkInt) numeric).getInt(), ((FrinkInt) numeric2).getInt()));
                }
                if (numeric2.isFrinkInteger() && numeric2.isFrinkInteger()) {
                    return DimensionlessUnitExpression.construct(((FrinkInteger) numeric).getBigInt().gcd(((FrinkInteger) numeric2).getBigInt()));
                }
                throw new InvalidArgumentException("Arguments to gcd[x,y] must both be integers.", this);
            }
        });
        addFunctionDefinition("lcm", new DoubleArgNumericFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Numeric numeric, Numeric numeric2) throws EvaluationException {
                if (numeric2.isFrinkInteger() && numeric2.isFrinkInteger()) {
                    return DimensionlessUnitExpression.construct(FrinkInteger.lcm((FrinkInteger) numeric, (FrinkInteger) numeric2));
                }
                throw new InvalidArgumentException("Arguments to lcm[x,y] must both be integers.", this);
            }
        });
        AnonymousClass23 r112 = new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    try {
                        return BasicUnitExpression.construct(UnitMath.truncate(((UnitExpression) expression).getUnit()));
                    } catch (ConformanceException e) {
                        throw new InvalidArgumentException(e.getMessage(), expression);
                    }
                } else {
                    throw new InvalidArgumentException("Argument to int should be Unit.", expression);
                }
            }
        };
        addFunctionDefinition("int", r112);
        addFunctionDefinition("truncate", r112);
        addFunctionDefinition("trunc", r112);
        addFunctionDefinition("sqrt", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    return PowerExpression.construct(expression, DimensionlessUnitExpression.ONE_HALF, environment).evaluate(environment);
                }
                throw new InvalidArgumentException("Argument to sqrt should be Unit.", expression);
            }
        });
        addFunctionDefinition("base", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof UnitExpression) || !(expression2 instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Arguments to base[num, base] should be integers.", expression);
                }
                Unit unit = ((UnitExpression) expression).getUnit();
                Unit unit2 = ((UnitExpression) expression2).getUnit();
                if (!UnitMath.isDimensionless(unit) || !UnitMath.isDimensionless(unit2) || !unit.getScale().isFrinkInteger() || !unit2.getScale().isInt()) {
                    throw new InvalidArgumentException("Arguments to base[num,base] must be dimensionless integers.", expression);
                }
                FrinkInteger frinkInteger = (FrinkInteger) unit.getScale();
                int i = ((FrinkInt) unit2.getScale()).getInt();
                if (i >= 2 && i <= 36) {
                    return new BasicStringExpression(frinkInteger.toString(i));
                }
                throw new InvalidArgumentException("Arguments for base base[num,base] must be 2 <= base <= 36", expression);
            }
        });
        addFunctionDefinition("JacobiSymbol", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof UnitExpression) || !(expression2 instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Arguments to JacobiSymbol[a, n] should be integers.", expression);
                }
                Unit unit = ((UnitExpression) expression).getUnit();
                Unit unit2 = ((UnitExpression) expression2).getUnit();
                if (UnitMath.isDimensionless(unit) && UnitMath.isDimensionless(unit2) && unit.getScale().isFrinkInteger() && unit2.getScale().isFrinkInteger()) {
                    return DimensionlessUnitExpression.construct(JacobiSymbol.jacobiSymbol((FrinkInteger) unit.getScale(), (FrinkInteger) unit2.getScale()));
                }
                throw new InvalidArgumentException("Arguments to JacobiSymbol[a,n] must be dimensionless integers.", expression);
            }
        });
        addFunctionDefinition("abs", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    Unit abs = UnitMath.abs(unit);
                    if (abs == unit) {
                        return expression;
                    }
                    return BasicUnitExpression.construct(abs);
                }
                throw new InvalidArgumentException("Argument to abs should be Unit.", expression);
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("magnitude", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    Unit magnitude = UnitMath.magnitude(unit);
                    if (magnitude == unit) {
                        return expression;
                    }
                    return BasicUnitExpression.construct(magnitude);
                }
                throw new InvalidArgumentException("Argument to magnitude should be Unit.", expression);
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("mignitude", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    Unit mignitude = UnitMath.mignitude(unit);
                    if (mignitude == unit) {
                        return expression;
                    }
                    return BasicUnitExpression.construct(mignitude);
                }
                throw new InvalidArgumentException("Argument to mignitude should be Unit.", expression);
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("infimum", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    Unit infimum = UnitMath.infimum(unit);
                    if (infimum == unit) {
                        return expression;
                    }
                    return BasicUnitExpression.construct(infimum);
                } else if (expression instanceof DateExpression) {
                    FrinkDate frinkDate = ((DateExpression) expression).getFrinkDate();
                    FrinkDate infimum2 = DateMath.infimum(frinkDate);
                    if (infimum2 == frinkDate) {
                        return expression;
                    }
                    return new BasicDateExpression(infimum2);
                } else {
                    throw new InvalidArgumentException("Argument to infimum should be Unit or Date.", expression);
                }
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("supremum", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    Unit supremum = UnitMath.supremum(unit);
                    if (supremum == unit) {
                        return expression;
                    }
                    return BasicUnitExpression.construct(supremum);
                } else if (expression instanceof DateExpression) {
                    FrinkDate frinkDate = ((DateExpression) expression).getFrinkDate();
                    FrinkDate supremum2 = DateMath.supremum(frinkDate);
                    if (supremum2 == frinkDate) {
                        return expression;
                    }
                    return new BasicDateExpression(supremum2);
                } else {
                    throw new InvalidArgumentException("Argument to supremum should be Unit or Date.", expression);
                }
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("mainValue", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    Unit main = UnitMath.main(unit);
                    if (main == null) {
                        return UndefExpression.UNDEF;
                    }
                    if (main == unit) {
                        return expression;
                    }
                    return BasicUnitExpression.construct(main);
                } else if (expression instanceof DateExpression) {
                    FrinkDate frinkDate = ((DateExpression) expression).getFrinkDate();
                    FrinkDate main2 = DateMath.main(frinkDate);
                    if (main2 == frinkDate) {
                        return expression;
                    }
                    return new BasicDateExpression(main2);
                } else {
                    throw new InvalidArgumentException("Argument to main should be Unit or Date.", expression);
                }
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("numerator", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) {
                try {
                    Numeric numericValue = BuiltinFunctionSource.getNumericValue(expression);
                    if (numericValue.isRational()) {
                        return DimensionlessUnitExpression.construct(((FrinkRational) numericValue).getNumerator());
                    }
                    return expression;
                } catch (NotRealException e) {
                    return expression;
                }
            }
        });
        addFunctionDefinition("denominator", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) {
                try {
                    Numeric numericValue = BuiltinFunctionSource.getNumericValue(expression);
                    if (numericValue.isRational()) {
                        return DimensionlessUnitExpression.construct(((FrinkRational) numericValue).getDenominator());
                    }
                    return DimensionlessUnitExpression.ONE;
                } catch (NotRealException e) {
                    return DimensionlessUnitExpression.ONE;
                }
            }
        });
        for (char c = 2; c <= '$'; c = (char) (c + 1)) {
            new BaseConverter(c);
        }
        new BaseConverter(2, "binary");
        new BaseConverter(3, "ternary");
        new BaseConverter(3, "trinary");
        new BaseConverter(4, "quaternary");
        new BaseConverter(5, "quinary");
        new BaseConverter(6, "senary");
        new BaseConverter(6, "sexenary");
        new BaseConverter(7, "septenary");
        new BaseConverter(8, "octal");
        new BaseConverter(8, "oct");
        new BaseConverter(8, "octonary");
        new BaseConverter(9, "nonary");
        new BaseConverter(10, "decimal");
        new BaseConverter(10, "denary");
        new BaseConverter(11, "undenary");
        new BaseConverter(12, "duodecimal");
        new BaseConverter(12, "duodenary");
        new BaseConverter(13, "tridecimal");
        new BaseConverter(14, "quattuordecimal");
        new BaseConverter(15, "quindecimal");
        new BaseConverter(16, "hexadecimal");
        new BaseConverter(16, "sexadecimal");
        new BaseConverter(16, "hex");
        new BaseConverter(17, "septendecimal");
        new BaseConverter(18, "octodecimal");
        new BaseConverter(19, "nonadecimal");
        new BaseConverter(20, "vigesimal");
        addFunctionDefinition("factorial", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(Factorial.factorial(BuiltinFunctionSource.getIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to factorial[x] must be a dimensionless integer", expression);
                }
            }
        });
        addFunctionDefinition("factor", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (!(expression instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Argument to factor[num] should be a positive integer.", expression);
                }
                Unit unit = ((UnitExpression) expression).getUnit();
                if (UnitMath.isDimensionless(unit)) {
                    Numeric scale = unit.getScale();
                    if (scale.isFrinkInteger()) {
                        FrinkInteger frinkInteger = (FrinkInteger) scale;
                        if (frinkInteger.realSignum() > 0) {
                            return Factor.factor(frinkInteger);
                        }
                    }
                }
                throw new InvalidArgumentException("Argument to factor[num] must be a positive, dimensionless integer.", expression);
            }
        });
        addFunctionDefinition("modPow", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BuiltinFunctionSource.getBigIntegerValue(expression).modPow(BuiltinFunctionSource.getBigIntegerValue(expression2), BuiltinFunctionSource.getBigIntegerValue(expression3)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to modPow must all be dimensionless integers.", this);
                }
            }
        });
        addFunctionDefinition("modDiv", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                try {
                    BigInteger bigIntegerValue = BuiltinFunctionSource.getBigIntegerValue(expression);
                    BigInteger bigIntegerValue2 = BuiltinFunctionSource.getBigIntegerValue(expression2);
                    BigInteger bigIntegerValue3 = BuiltinFunctionSource.getBigIntegerValue(expression3);
                    return DimensionlessUnitExpression.construct(bigIntegerValue.multiply(bigIntegerValue2.modInverse(bigIntegerValue3)).mod(bigIntegerValue3));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to modDiv must all be dimensionless integers.", this);
                } catch (ArithmeticException e2) {
                    return UndefExpression.UNDEF;
                }
            }
        });
        addFunctionDefinition("modInverse", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BuiltinFunctionSource.getBigIntegerValue(expression).modInverse(BuiltinFunctionSource.getBigIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to modInverse must be dimensionless integers.", this);
                } catch (ArithmeticException e2) {
                    return UndefExpression.UNDEF;
                }
            }
        });
        addFunctionDefinition("approxLog2", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    if (UnitMath.isDimensionless(unit)) {
                        return DimensionlessUnitExpression.construct(NumericMath.approxLog2(unit.getScale()));
                    }
                }
                throw new InvalidArgumentException("Argument to approxLog2 should be a dimensionless number.", expression);
            }
        });
        addFunctionDefinition("Re", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws NumericException, InvalidArgumentException {
                if (expression instanceof UnitExpression) {
                    return BasicUnitExpression.construct(UnitMath.realPart(BuiltinFunctionSource.getUnitValue(expression)));
                }
                throw new InvalidArgumentException("Re[x] requires unit to be a unit.  Value was " + environment.format(expression), expression);
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("Im", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws NumericException, InvalidArgumentException {
                if (expression instanceof UnitExpression) {
                    return BasicUnitExpression.construct(UnitMath.imaginaryPart(BuiltinFunctionSource.getUnitValue(expression)));
                }
                throw new InvalidArgumentException("Im[x] requires unit to be a unit.  Value was " + environment.format(expression), expression);
            }

            public boolean isMappable() {
                return true;
            }
        });
        addFunctionDefinition("sinh", new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.sinh(numeric);
            }
        });
        AnonymousClass44 r113 = new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arcsinh(numeric);
            }
        };
        addFunctionDefinition("arcsinh", r113);
        addFunctionDefinition("asinh", r113);
        addFunctionDefinition("cosh", new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.cosh(numeric);
            }
        });
        AnonymousClass46 r114 = new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arccosh(numeric);
            }
        };
        addFunctionDefinition("arccosh", r114);
        addFunctionDefinition("acosh", r114);
        AnonymousClass47 r115 = new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arccsch(numeric);
            }
        };
        addFunctionDefinition("arccsch", r115);
        addFunctionDefinition("acsch", r115);
        addFunctionDefinition("tanh", new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.tanh(numeric);
            }
        });
        AnonymousClass49 r116 = new SingleArgNumericFunction(null, null, true) {
            /* access modifiers changed from: protected */
            public Numeric doFunction(Environment environment, Numeric numeric) throws NumericException {
                return NumericMath.arctanh(numeric);
            }
        };
        addFunctionDefinition("arctanh", r116);
        addFunctionDefinition("atanh", r116);
        addFunctionDefinition("isPrime", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    if (UnitMath.isDimensionless(unit)) {
                        Numeric scale = unit.getScale();
                        if (scale.isFrinkInteger()) {
                            FrinkInteger frinkInteger = (FrinkInteger) scale;
                            if (frinkInteger.realSignum() > 0) {
                                return FrinkBoolean.create(Factor.isPrime(frinkInteger));
                            }
                        }
                    }
                }
                throw new InvalidArgumentException("Argument to isPrime[x] must be a dimensionless positive integer", expression);
            }
        });
        addFunctionDefinition("isPositive", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    if (UnitMath.isDimensionless(unit)) {
                        Numeric scale = unit.getScale();
                        if (scale.isReal()) {
                            return FrinkBoolean.create(((FrinkReal) scale).realSignum() > 0);
                        }
                    }
                }
                return FrinkBoolean.FALSE;
            }
        });
        addFunctionDefinition("isNegative", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    if (UnitMath.isDimensionless(unit)) {
                        Numeric scale = unit.getScale();
                        if (scale.isReal()) {
                            return FrinkBoolean.create(((FrinkReal) scale).realSignum() < 0);
                        }
                    }
                }
                return FrinkBoolean.FALSE;
            }
        });
        addFunctionDefinition("isStrongPseudoprime", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return FrinkBoolean.create(Factor.isStrongPseudoprime(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to isStrongPseudoprime[num, base] must be integers.", expression);
                }
            }
        });
        addFunctionDefinition("collapseIntervals", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    RealInterval.setCollapseIntervals(Truth.isTrue(environment, expression));
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to collapseIntervals[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("partitionCount", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                try {
                    return DimensionlessUnitExpression.construct(Partitions.getPartitionCount(BuiltinFunctionSource.getIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to partitionCount[x] must be a dimensionless integer", expression);
                }
            }
        });
        addFunctionDefinition("isInteger", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    if (UnitMath.isDimensionless(unit)) {
                        return FrinkBoolean.create(unit.getScale().isFrinkInteger());
                    }
                }
                return FrinkBoolean.FALSE;
            }
        });
        addFunctionDefinition("isRational", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                if (expression instanceof UnitExpression) {
                    Unit unit = ((UnitExpression) expression).getUnit();
                    if (UnitMath.isDimensionless(unit)) {
                        return FrinkBoolean.create(unit.getScale().isRational());
                    }
                }
                return FrinkBoolean.FALSE;
            }
        });
        addFunctionDefinition("isUnit", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                return FrinkBoolean.create(expression instanceof UnitExpression);
            }
        });
        addFunctionDefinition("isInterval", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                if (expression instanceof UnitExpression) {
                    return FrinkBoolean.create(((UnitExpression) expression).getUnit().getScale().isInterval());
                }
                return FrinkBoolean.FALSE;
            }
        });
        addFunctionDefinition("sum", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                return Summator.sum(expression, environment);
            }
        });
        addFunctionDefinition("product", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                return Productator.multiply(expression, environment);
            }
        });
        addFunctionDefinition("binomial", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(Binomial.binomial(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to binomial[n,m] must be integers.  Arguments were binomial[" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
            }
        });
        addFunctionDefinition("nextPrime", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(Factor.nextPrime(BuiltinFunctionSource.getFrinkIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    try {
                        return DimensionlessUnitExpression.construct(Factor.nextPrime(RealMath.floor(BuiltinFunctionSource.getFrinkRealValue(expression))));
                    } catch (NotRealException e2) {
                        throw new InvalidArgumentException("Argument to nextPrime[x] must be a real number", expression);
                    } catch (NumericException e3) {
                        throw new InvalidArgumentException("Numeric exception in nextPrime:\n  " + e3, expression);
                    }
                }
            }
        });
        AnonymousClass64 r117 = new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(EulerTotient.eulerTotient(BuiltinFunctionSource.getFrinkIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to eulerPhi[x] must be a real number", expression);
                } catch (NumericException e2) {
                    throw new InvalidArgumentException("Numeric exception in eulerPhi:\n  " + e2, expression);
                }
            }
        };
        addFunctionDefinition("eulerPhi", r117);
        addFunctionDefinition("eulerTotient", r117);
        AnonymousClass65 r2 = new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                try {
                    if (!(expression instanceof ListExpression)) {
                        throw new InvalidArgumentException("First argument to DFT[x] must be an array of numbers.", this);
                    }
                    return Fourier.DFT((ListExpression) expression, BuiltinFunctionSource.getIntegerValue(expression2), BuiltinFunctionSource.getIntegerValue(expression3));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to DFT[x] must be a real number", this);
                } catch (NumericException e2) {
                    throw new InvalidArgumentException("Numeric exception in DFT:\n  " + e2, this);
                }
            }
        };
        r2.setDefaultValue(1, DimensionlessUnitExpression.NEGATIVE_ONE);
        r2.setDefaultValue(2, DimensionlessUnitExpression.ONE);
        addFunctionDefinition("DFT", r2);
        AnonymousClass66 r22 = new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                try {
                    if (!(expression instanceof ListExpression)) {
                        throw new InvalidArgumentException("First argument to InverseDFT[x] must be an array of numbers.", this);
                    }
                    return Fourier.InverseDFT((ListExpression) expression, BuiltinFunctionSource.getIntegerValue(expression2), BuiltinFunctionSource.getIntegerValue(expression3));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to InverseDFT[x] must be a real number", this);
                } catch (NumericException e2) {
                    throw new InvalidArgumentException("Numeric exception in InverseDFT:\n  " + e2, this);
                }
            }
        };
        r22.setDefaultValue(1, DimensionlessUnitExpression.NEGATIVE_ONE);
        r22.setDefaultValue(2, DimensionlessUnitExpression.ONE);
        addFunctionDefinition("InverseDFT", r22);
        addFunctionDefinition("signum", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                return BasicUnitExpression.construct(UnitMath.signum(BuiltinFunctionSource.getUnitValue(expression)));
            }
        });
        addFunctionDefinition("bitOr", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BitwiseOperators.bitOr(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to bitOr[n,m] must be integers.  Arguments were bitOr[" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
            }
        });
        addFunctionDefinition("bitAnd", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BitwiseOperators.bitAnd(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to bitAnd[n,m] must be integers.  Arguments were bitAnd[" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
            }
        });
        addFunctionDefinition("bitXor", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BitwiseOperators.bitXor(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to bitXor[n,m] must be integers.  Arguments were bitXor[" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
            }
        });
        addFunctionDefinition("bitNot", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BitwiseOperators.bitNot(BuiltinFunctionSource.getFrinkIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to bitNot[n] must be an integer.  Argument was bitNot[" + environment.format(expression) + "]", this);
                }
            }
        });
        addFunctionDefinition("bitNor", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BitwiseOperators.bitNor(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to bitNor[n,m] must be integers.  Arguments were bitNor[" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
            }
        });
        addFunctionDefinition("bitNand", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BitwiseOperators.bitNand(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to bitNand[n,m] must be integers.  Arguments were bitNand[" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
            }
        });
        addFunctionDefinition("binaryToGray", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(GrayCodes.binaryToGray(BuiltinFunctionSource.getFrinkIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to binaryToGray[n] must be an integer.  Argument was binaryToGray[" + environment.format(expression) + "]", this);
                }
            }
        });
        addFunctionDefinition("grayToBinary", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(GrayCodes.grayToBinary(BuiltinFunctionSource.getFrinkIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to grayToBinary[n] must be an integer.  Argument was grayToBinary[" + environment.format(expression) + "]", this);
                }
            }
        });
    }

    private void initializeRadian(Environment environment) {
        if (this.radian == null) {
            this.radian = environment.getUnitManager().getUnit(RADIAN);
        }
    }

    private class BaseConverter extends SingleArgFunction {
        private int base;
        private String name;

        public BaseConverter(int i) {
            super(true);
            this.base = i;
            this.name = "base" + i;
            MathFunctionSource.this.addFunctionDefinition(this.name, this);
        }

        public BaseConverter(int i, String str) {
            super(true);
            this.base = i;
            this.name = str;
            MathFunctionSource.this.addFunctionDefinition(str, this);
        }

        public boolean isMappable() {
            return true;
        }

        /* access modifiers changed from: protected */
        public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
            if (!(expression instanceof UnitExpression)) {
                throw new InvalidArgumentException("Argument to " + this.name + "[num] should be integer.", expression);
            }
            Unit unit = ((UnitExpression) expression).getUnit();
            if (UnitMath.isDimensionless(unit) && unit.getScale().isFrinkInteger()) {
                return new BasicStringExpression(((FrinkInteger) unit.getScale()).toString(this.base));
            }
            throw new InvalidArgumentException("Argument to " + this.name + "[num] must be a dimensionless integer.", expression);
        }
    }
}
