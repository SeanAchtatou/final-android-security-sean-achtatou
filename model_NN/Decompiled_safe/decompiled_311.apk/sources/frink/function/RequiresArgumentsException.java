package frink.function;

import frink.errors.FrinkException;

public class RequiresArgumentsException extends FrinkException {
    public static final RequiresArgumentsException INSTANCE = new RequiresArgumentsException("RequiresArgumentsException");

    private RequiresArgumentsException(String str) {
        super(str);
    }
}
