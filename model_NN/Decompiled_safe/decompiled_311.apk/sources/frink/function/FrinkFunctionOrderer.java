package frink.function;

import frink.errors.NotAnIntegerException;
import frink.expr.BasicListExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.object.FrinkObject;

public class FrinkFunctionOrderer implements Orderer {
    private BasicListExpression args = new BasicListExpression(2);
    private FunctionDefinition sortFunc;

    public FrinkFunctionOrderer(FunctionDefinition functionDefinition) {
        this.sortFunc = functionDefinition;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.BasicListExpression, int, ?[OBJECT, ARRAY], int]
     candidates:
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    public int compare(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        this.args.setChild(0, expression);
        this.args.setChild(1, expression2);
        Expression execute = environment.getFunctionManager().execute(this.sortFunc, environment, (Expression) this.args, true, (FrinkObject) null, false);
        try {
            int integerValue = BuiltinFunctionSource.getIntegerValue(execute);
            if (integerValue < 0) {
                return -1;
            }
            if (integerValue > 0) {
                return 1;
            }
            return 0;
        } catch (NotAnIntegerException e) {
            throw new InvalidArgumentException("Sorter: sort function returned a non-integer value.", execute);
        }
    }
}
