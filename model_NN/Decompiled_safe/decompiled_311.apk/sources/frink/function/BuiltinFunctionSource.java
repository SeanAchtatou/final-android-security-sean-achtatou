package frink.function;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.errors.NotComparableException;
import frink.errors.NotRealException;
import frink.expr.ArrayUtils;
import frink.expr.BasicDictionaryExpression;
import frink.expr.BasicListExpression;
import frink.expr.BasicStringExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.CondensedUnitExpression;
import frink.expr.DeepCopyable;
import frink.expr.DictionaryExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.EnumerationWrapper;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.EvaluationNumericException;
import frink.expr.ExitException;
import frink.expr.Expression;
import frink.expr.FrinkBoolean;
import frink.expr.FrinkEnumeration;
import frink.expr.FrinkSecurityException;
import frink.expr.FunctionDefinitionExpression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.RegexpExpression;
import frink.expr.SetExpression;
import frink.expr.SetUtils;
import frink.expr.StringExpression;
import frink.expr.StringExpressionFactory;
import frink.expr.SymbolExpression;
import frink.expr.ThreeWayComparison;
import frink.expr.Truth;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.VoidExpression;
import frink.format.BasicExpressionFormatter;
import frink.graphics.BoundingBox;
import frink.io.InputItem;
import frink.io.SystemInputManager;
import frink.numeric.BaseConverter;
import frink.numeric.FrinkBigDecimal;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.numeric.RealInterval;
import frink.object.FrinkObject;
import frink.symbolic.BasicMatchingContext;
import frink.symbolic.SymbolicUtils;
import frink.text.BasicRegexpExpression;
import frink.text.BasicSubstitutionExpression;
import frink.text.EditDistance;
import frink.text.StringUtils;
import frink.text.TempURLEncoder;
import frink.units.DimensionList;
import frink.units.Unit;
import frink.units.UnitMatcher;
import frink.units.UnitMath;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Util;

public class BuiltinFunctionSource extends BasicFunctionSource {
    public static final BasicFunctionSource INSTANCE = new BuiltinFunctionSource();
    /* access modifiers changed from: private */
    public static final Hashtable<String, MessageDigest> mdTable = new Hashtable<>();
    static final Random random = new Random();

    private BuiltinFunctionSource() {
        super("BuiltinFunctionSource");
        initializeFunctions();
    }

    private void initializeFunctions() {
        addFunctionDefinition("input", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) {
                String input = environment.input(environment.format(expression), (String) null);
                if (input == null) {
                    return UndefExpression.UNDEF;
                }
                return new BasicStringExpression(input);
            }
        });
        addFunctionDefinition("input", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) {
                if (expression2 instanceof ListExpression) {
                    int childCount = ((ListExpression) expression2).getChildCount();
                    if (childCount == 0) {
                        return new BasicListExpression(0);
                    }
                    InputItem[] inputItemArr = new InputItem[childCount];
                    int i = 0;
                    while (i < childCount) {
                        try {
                            inputItemArr[i] = new InputItem(expression2.getChild(i), environment);
                            i++;
                        } catch (InvalidChildException e) {
                        }
                    }
                    BasicListExpression basicListExpression = new BasicListExpression(r2);
                    for (String str : environment.input(environment.format(expression), inputItemArr)) {
                        if (str != null) {
                            basicListExpression.appendChild(new BasicStringExpression(str));
                        } else {
                            basicListExpression.appendChild(UndefExpression.UNDEF);
                        }
                    }
                    return basicListExpression;
                }
                String input = environment.input(environment.format(expression), environment.format(expression2));
                if (input != null) {
                    return new BasicStringExpression(input);
                }
                return UndefExpression.UNDEF;
            }
        });
        addFunctionDefinition("readStdin", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) {
                String input = SystemInputManager.INSTANCE.input((String) null, (String) null, environment);
                if (input == null) {
                    return UndefExpression.UNDEF;
                }
                return new BasicStringExpression(input);
            }
        });
        addFunctionDefinition("print", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) {
                environment.output(expression);
                return VoidExpression.VOID;
            }
        });
        addFunctionDefinition("println", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) {
                environment.outputln(expression);
                return VoidExpression.VOID;
            }
        });
        addFunctionDefinition("println", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) {
                environment.outputln("");
                return VoidExpression.VOID;
            }
        });
        addFunctionDefinition("length", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                int childCount;
                if (expression instanceof StringExpression) {
                    childCount = ((StringExpression) expression).getString().length();
                } else if (expression instanceof DictionaryExpression) {
                    childCount = ((DictionaryExpression) expression).getSize();
                } else if (expression instanceof SetExpression) {
                    childCount = ((SetExpression) expression).getSize();
                } else if (expression instanceof EnumeratingExpression) {
                    int i = 0;
                    while (((EnumeratingExpression) expression).getEnumeration(environment).getNext(environment) != null) {
                        i++;
                    }
                    childCount = i;
                } else {
                    childCount = expression.getChildCount();
                }
                return DimensionlessUnitExpression.construct(childCount);
            }
        });
        addFunctionDefinition(ListExpression.TYPE, new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                return ArrayUtils.toArray(expression, environment);
            }
        });
        addFunctionDefinition("toSet", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                return SetUtils.makeSet(expression, environment);
            }
        });
        addFunctionDefinition("regex", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    return new BasicRegexpExpression(((StringExpression) expression).getString());
                }
                throw new InvalidArgumentException("Argument to regex must be a string.", expression);
            }
        });
        addFunctionDefinition("regex", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("Argument to regex must be a string.", expression);
                } else if (expression2 instanceof StringExpression) {
                    return new BasicRegexpExpression(((StringExpression) expression).getString(), ((StringExpression) expression2).getString());
                } else {
                    throw new InvalidArgumentException("Options to regex must be a string.", expression2);
                }
            }
        });
        addFunctionDefinition("subst", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("First argument to subst[from, to] must be a string.", expression);
                } else if (expression2 instanceof StringExpression) {
                    return new BasicSubstitutionExpression(((StringExpression) expression).getString(), ((StringExpression) expression2).getString());
                } else {
                    throw new InvalidArgumentException("Second argument to subst[from, to] must be a string.", expression2);
                }
            }
        });
        addFunctionDefinition("subst", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("First argument to subst[from, to, options] must be a string.", expression);
                } else if (!(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("Second argument to subst[from, to, options] must be a string.", expression2);
                } else if (expression3 instanceof StringExpression) {
                    return new BasicSubstitutionExpression(((StringExpression) expression).getString(), ((StringExpression) expression2).getString(), ((StringExpression) expression3).getString());
                } else {
                    throw new InvalidArgumentException("Second argument to subst[from, to, options] must be a string.", expression3);
                }
            }
        });
        addFunctionDefinition("split", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("Second argument to split must be a string.", expression2);
                } else if (expression instanceof RegexpExpression) {
                    Vector split = Util.split(new Perl5Matcher(), ((RegexpExpression) expression).getPattern(), ((StringExpression) expression2).getString());
                    int size = split.size();
                    BasicListExpression basicListExpression = new BasicListExpression(size);
                    for (int i = 0; i < size; i++) {
                        basicListExpression.appendChild(new BasicStringExpression((String) split.elementAt(i)));
                    }
                    return basicListExpression;
                } else if (expression instanceof StringExpression) {
                    return StringSplitter.split((StringExpression) expression2, ((StringExpression) expression).getString());
                } else {
                    throw new InvalidArgumentException("First argument to split must be a pattern or a string.", expression);
                }
            }
        });
        addFunctionDefinition("join", new DoubleArgFunction(true) {
            /* JADX INFO: finally extract failed */
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("First argument to join must be a string.", expression);
                }
                String string = ((StringExpression) expression).getString();
                StringBuffer stringBuffer = new StringBuffer();
                if (expression2 instanceof EnumeratingExpression) {
                    FrinkEnumeration enumeration = ((EnumeratingExpression) expression2).getEnumeration(environment);
                    boolean z = true;
                    while (true) {
                        try {
                            Expression next = enumeration.getNext(environment);
                            if (next != null) {
                                if (!z) {
                                    stringBuffer.append(string);
                                }
                                stringBuffer.append(environment.format(next));
                                z = false;
                            } else {
                                enumeration.dispose();
                                return new BasicStringExpression(new String(stringBuffer));
                            }
                        } catch (Throwable th) {
                            enumeration.dispose();
                            throw th;
                        }
                    }
                } else if (expression2 instanceof ListExpression) {
                    ListExpression listExpression = (ListExpression) expression2;
                    int childCount = listExpression.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        Expression child = listExpression.getChild(i);
                        if (i > 0) {
                            stringBuffer.append(string);
                        }
                        stringBuffer.append(environment.format(child));
                    }
                    return new BasicStringExpression(new String(stringBuffer));
                } else {
                    throw new InvalidArgumentException("Second argument to join must be a list.", expression2);
                }
            }
        });
        addFunctionDefinition("select", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                return SelectFunction.select(expression, expression2, null, environment);
            }
        });
        addFunctionDefinition("select", new TripleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                return SelectFunction.select(expression, expression2, expression3, environment);
            }
        });
        addFunctionDefinition("keys", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException {
                if (expression instanceof DictionaryExpression) {
                    return ((DictionaryExpression) expression).keys();
                }
                throw new InvalidArgumentException("Argument to keys must be a dictionary.", expression);
            }
        });
        addFunctionDefinition("sort", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                Expression expression3;
                if (!(expression instanceof ListExpression)) {
                    expression3 = ArrayUtils.toArray(expression, environment);
                } else {
                    expression3 = expression;
                }
                if (expression2 instanceof FunctionDefinitionExpression) {
                    Sorter.sort((ListExpression) expression3, new FrinkFunctionOrderer(((FunctionDefinitionExpression) expression2).getFunctionDefinition()), environment);
                    return expression3;
                }
                throw new InvalidArgumentException("Second argument to sort must be a proc.", expression2);
            }
        });
        addFunctionDefinition("sort", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                Expression expression2;
                if (!(expression instanceof ListExpression)) {
                    expression2 = ArrayUtils.toArray(expression, environment);
                } else {
                    expression2 = expression;
                }
                Sorter.sort((ListExpression) expression2, DefaultOrderer.INSTANCE, environment);
                return expression2;
            }
        });
        addFunctionDefinition("reverse", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                ListExpression array;
                if (expression instanceof StringExpression) {
                    return new BasicStringExpression(new StringBuffer(((StringExpression) expression).getString()).reverse());
                }
                if (expression instanceof ListExpression) {
                    array = (ListExpression) expression;
                } else {
                    array = ArrayUtils.toArray(expression, environment);
                }
                int childCount = array.getChildCount();
                int i = childCount / 2;
                int i2 = childCount;
                for (int i3 = 0; i3 < i; i3++) {
                    i2--;
                    Expression child = array.getChild(i3);
                    array.setChild(i3, array.getChild(i2));
                    array.setChild(i2, child);
                }
                return array;
            }
        });
        AnonymousClass22 r0 = new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof ListExpression) {
                    return expression.getChild(BuiltinFunctionSource.randomInt(expression.getChildCount()));
                }
                try {
                    return DimensionlessUnitExpression.construct(BuiltinFunctionSource.randomInt(BuiltinFunctionSource.getIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to random[x] must be an integer", expression);
                }
            }
        };
        addFunctionDefinition("random", r0);
        addFunctionDefinition("rand", r0);
        addFunctionDefinition("randomBits", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                    if (integerValue <= 0) {
                        throw new InvalidArgumentException("Argument to randomBits[x] must be a positive integer, argument was " + integerValue, expression);
                    } else if (integerValue < 31) {
                        return DimensionlessUnitExpression.construct(BuiltinFunctionSource.random.nextInt(1 << integerValue));
                    } else {
                        return DimensionlessUnitExpression.construct(FrinkInteger.construct(new BigInteger(integerValue, BuiltinFunctionSource.random)));
                    }
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to randomBits[x] must be a positive integer", expression);
                }
            }
        });
        AnonymousClass24 r02 = new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                    return DimensionlessUnitExpression.construct(integerValue + BuiltinFunctionSource.randomInt((BuiltinFunctionSource.getIntegerValue(expression2) - integerValue) + 1));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Arguments to random[from, to] must be integers.", expression);
                }
            }
        };
        addFunctionDefinition("random", r02);
        addFunctionDefinition("rand", r02);
        addFunctionDefinition("randomFloat", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    FrinkReal frinkRealValue = BuiltinFunctionSource.getFrinkRealValue(expression);
                    return DimensionlessUnitExpression.construct(NumericMath.add(NumericMath.multiply(new FrinkFloat(BuiltinFunctionSource.random.nextDouble()), NumericMath.subtract(BuiltinFunctionSource.getFrinkRealValue(expression2), frinkRealValue)), frinkRealValue));
                } catch (NotRealException e) {
                    throw new InvalidArgumentException("Arguments to randomFloat[from, to] must be dimensionless real numbers", expression);
                } catch (NumericException e2) {
                    throw new EvaluationNumericException("Unexpected numeric exception when calling randomFloat[" + environment.format(expression) + "," + environment.format(expression2) + "]:\n " + e2, expression);
                }
            }
        });
        addFunctionDefinition("randomGaussian", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(NumericMath.add(NumericMath.multiply(new FrinkFloat(BuiltinFunctionSource.random.nextGaussian()), BuiltinFunctionSource.getFrinkRealValue(expression2)), BuiltinFunctionSource.getFrinkRealValue(expression)));
                } catch (NotRealException e) {
                    throw new InvalidArgumentException("Arguments to randomGaussian[from, to] must be dimensionless real numbers", expression);
                } catch (NumericException e2) {
                    throw new EvaluationNumericException("Unexpected numeric exception when calling randomGaussian[" + environment.format(expression) + "," + environment.format(expression2) + "]:\n " + e2, expression);
                }
            }
        });
        addFunctionDefinition("randomSeed", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    environment.getSecurityHelper().checkSetGlobalFlag();
                    BuiltinFunctionSource.random.setSeed(BuiltinFunctionSource.getLongValue(expression));
                    return VoidExpression.VOID;
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to randomSeed[x] must be a positive integer", expression);
                }
            }
        });
        addFunctionDefinition("bitLength", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return DimensionlessUnitExpression.construct(BuiltinFunctionSource.getBigIntegerValue(expression).bitLength());
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to bitLength[x] must be an integer", expression);
                }
            }
        });
        addFunctionDefinition("getBit", new DoubleArgNumericFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Numeric numeric, Numeric numeric2) throws EvaluationException {
                if (numeric2.isInt()) {
                    int i = ((FrinkInt) numeric2).getInt();
                    if (numeric.isInt()) {
                        return ((1 << i) & ((FrinkInt) numeric).getInt()) != 0 ? DimensionlessUnitExpression.ONE : DimensionlessUnitExpression.ZERO;
                    } else if (numeric.isFrinkInteger()) {
                        return ((FrinkInteger) numeric).getBigInt().testBit(i) ? DimensionlessUnitExpression.ONE : DimensionlessUnitExpression.ZERO;
                    }
                }
                throw new InvalidArgumentException("Arguments to getBit[num, bit] must both be integers.", this);
            }
        });
        addFunctionDefinition("oldToString", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return new BasicStringExpression(BuiltinFunctionSource.getBigIntegerValue(expression).toString());
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to oldToString[x] must be an integer", expression);
                }
            }
        });
        addFunctionDefinition("newToString", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                try {
                    return new BasicStringExpression(BaseConverter.recursiveToString(BuiltinFunctionSource.getBigIntegerValue(expression)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to newToString[x] must be an integer", expression);
                }
            }
        });
        addFunctionDefinition("flatten", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                Expression expression2;
                if (!(expression instanceof ListExpression)) {
                    expression2 = ArrayUtils.toArray(expression, environment);
                } else {
                    expression2 = expression;
                }
                BasicListExpression basicListExpression = new BasicListExpression(expression2.getChildCount());
                flatten((ListExpression) expression2, basicListExpression);
                return basicListExpression;
            }

            private void flatten(ListExpression listExpression, BasicListExpression basicListExpression) throws EvaluationException {
                int childCount = listExpression.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    Expression child = listExpression.getChild(i);
                    if (child instanceof ListExpression) {
                        flatten((ListExpression) child, basicListExpression);
                    } else {
                        basicListExpression.appendChild(child);
                    }
                }
            }
        });
        addFunctionDefinition("units", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new EnumerationWrapper(environment.getUnitManager().getNames(), StringExpressionFactory.INSTANCE);
            }
        });
        addFunctionDefinition("units", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                Expression evaluate;
                String str;
                CondensedUnitExpression evaluate2;
                DimensionList dimensionList = null;
                if (expression instanceof SymbolExpression) {
                    String name = ((SymbolExpression) expression).getName();
                    dimensionList = environment.getDimensionListManager().getDimensionList(name);
                    if (dimensionList == null) {
                        String str2 = name;
                        evaluate = expression.evaluate(environment);
                        str = str2;
                    } else {
                        str = name;
                        evaluate = expression;
                    }
                } else {
                    evaluate = expression.evaluate(environment);
                    str = null;
                }
                if (evaluate instanceof StringExpression) {
                    String string = ((StringExpression) evaluate).getString();
                    str = string;
                    dimensionList = environment.getDimensionListManager().getDimensionList(string);
                }
                if (dimensionList != null) {
                    evaluate2 = new CondensedUnitExpression(FrinkInt.ONE, dimensionList);
                } else {
                    Unit unit = environment.getUnitManager().getUnit(str);
                    if (unit != null) {
                        evaluate2 = BasicUnitExpression.construct(unit);
                    } else {
                        evaluate2 = evaluate.evaluate(environment);
                    }
                }
                if (evaluate2 instanceof UnitExpression) {
                    return new EnumerationWrapper(UnitMatcher.getConformalUnits(((UnitExpression) evaluate2).getUnit(), environment.getUnitManager()), StringExpressionFactory.INSTANCE);
                }
                throw new InvalidArgumentException("Argument to units[x] must be a unit, a symbol or a string.  Actual argument was " + environment.format(evaluate2), evaluate2);
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("dimensions", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new EnumerationWrapper(environment.getDimensionListManager().getNames(), StringExpressionFactory.INSTANCE);
            }
        });
        addFunctionDefinition("unit", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof UnitExpression) {
                    return expression;
                }
                String str = null;
                if (expression instanceof SymbolExpression) {
                    str = ((SymbolExpression) expression).getName();
                } else if (expression instanceof StringExpression) {
                    str = ((StringExpression) expression).getString();
                }
                if (str != null) {
                    Unit unit = environment.getUnitManager().getUnit(str);
                    if (unit == null) {
                        return UndefExpression.UNDEF;
                    }
                    return BasicUnitExpression.construct(unit);
                }
                throw new InvalidArgumentException("Argument to unit[x] must be a string.", expression);
            }
        });
        addFunctionDefinition("char", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    String string = ((StringExpression) expression).getString();
                    int length = string.length();
                    if (length == 1) {
                        return DimensionlessUnitExpression.construct((int) string.charAt(0));
                    }
                    BasicListExpression basicListExpression = new BasicListExpression(length);
                    for (int i = 0; i < length; i++) {
                        basicListExpression.appendChild(DimensionlessUnitExpression.construct((int) string.charAt(i)));
                    }
                    return basicListExpression;
                }
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                    if (integerValue >= 0 && integerValue <= 65535) {
                        return new BasicStringExpression(Character.toString((char) integerValue));
                    }
                    throw new InvalidArgumentException("Argument to char must be in the range 0 <= char <= 65535, value was " + integerValue, expression);
                } catch (NotAnIntegerException e) {
                    if (expression instanceof ListExpression) {
                        int childCount = expression.getChildCount();
                        StringBuffer stringBuffer = new StringBuffer(childCount);
                        for (int i2 = 0; i2 < childCount; i2++) {
                            int integerValue2 = BuiltinFunctionSource.getIntegerValue(expression.getChild(i2));
                            if (integerValue2 < 0 || integerValue2 > 65535) {
                                throw new InvalidArgumentException("Argument to char must be in the range 0 <= char <= 65535, value was " + integerValue2, expression);
                            }
                            stringBuffer.append((char) integerValue2);
                        }
                        return new BasicStringExpression(new String(stringBuffer));
                    }
                    throw new InvalidArgumentException("Invalid argument to char--must be a string, an integer, or an array of integers.", expression);
                } catch (NotAnIntegerException e2) {
                    throw new InvalidArgumentException("Invalid argument to char--must be a string, an integer, or an array of integers.", expression);
                }
            }
        });
        addFunctionDefinition("chars", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    String string = ((StringExpression) expression).getString();
                    int length = string.length();
                    BasicListExpression basicListExpression = new BasicListExpression(length);
                    for (int i = 0; i < length; i++) {
                        basicListExpression.appendChild(DimensionlessUnitExpression.construct((int) string.charAt(i)));
                    }
                    return basicListExpression;
                }
                throw new InvalidArgumentException("Argument to chars[x] must be a string.", expression);
            }
        });
        addFunctionDefinition("charList", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    String string = ((StringExpression) expression).getString();
                    int length = string.length();
                    BasicListExpression basicListExpression = new BasicListExpression(length);
                    for (int i = 0; i < length; i++) {
                        basicListExpression.appendChild(new BasicStringExpression(string.substring(i, i + 1)));
                    }
                    return basicListExpression;
                }
                throw new InvalidArgumentException("Argument to charList[x] must be a string.", expression);
            }
        });
        AnonymousClass40 r03 = new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    return new BasicStringExpression(((StringExpression) expression).getString().toUpperCase());
                }
                throw new InvalidArgumentException("Argument to uppercase must be a string.", expression);
            }
        };
        addFunctionDefinition("uc", r03);
        addFunctionDefinition("uppercase", r03);
        AnonymousClass41 r04 = new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    return new BasicStringExpression(((StringExpression) expression).getString().toLowerCase());
                }
                throw new InvalidArgumentException("Argument to lowercase must be a string.", expression);
            }
        };
        addFunctionDefinition("lc", r04);
        addFunctionDefinition("lowercase", r04);
        addFunctionDefinition("setPrecision", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                if (!(expression instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Argument to setPrecision[num] should be integer.", expression);
                }
                try {
                    NumericMath.setPrecision(UnitMath.getIntegerValue(((UnitExpression) expression).getUnit()));
                    return VoidExpression.VOID;
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Argument to setPrecision[num] should be an integer.", expression);
                }
            }
        });
        addFunctionDefinition("getPrecision", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return DimensionlessUnitExpression.construct(NumericMath.getPrecision());
            }
        });
        addFunctionDefinition("format", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                String str;
                FrinkBigDecimal frinkBigDecimal;
                Expression expression4;
                if (!(expression3 instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Third argument to format[n, unit, dec] should be an integer.", expression);
                }
                try {
                    int integerValue = UnitMath.getIntegerValue(((UnitExpression) expression3).getUnit());
                    if (!(expression instanceof UnitExpression)) {
                        throw new InvalidArgumentException("First argument to format[n, unit, dec] should be a unit.", expression);
                    }
                    Unit unit = null;
                    boolean z = false;
                    if (expression2 instanceof StringExpression) {
                        String string = ((StringExpression) expression2).getString();
                        Unit unit2 = environment.getUnitManager().getUnit(string);
                        if (unit2 == null) {
                            environment.getSecurityHelper().checkUnsafeEval();
                            try {
                                expression4 = environment.eval(string).evaluate(environment);
                            } catch (Exception e) {
                                throw new InvalidArgumentException("Second argument to format[n, unit, dec] should be a unit or a string containing a unit expression:\n " + e, expression);
                            }
                        } else {
                            expression4 = expression2;
                        }
                        str = string;
                        expression2 = expression4;
                        unit = unit2;
                        z = true;
                    } else {
                        str = null;
                    }
                    if (unit == null && (expression2 instanceof UnitExpression)) {
                        unit = ((UnitExpression) expression2).getUnit();
                    }
                    if (unit == null) {
                        throw new InvalidArgumentException("Second argument to format[n, unit, dec] should be a unit or a string containing a unit expression.", expression);
                    }
                    try {
                        Unit divide = UnitMath.divide(((UnitExpression) expression).getUnit(), unit);
                        if (!UnitMath.isDimensionless(divide)) {
                            throw new EvaluationConformanceException("First two arguments to format[x,y,digits] must be conformal.", expression);
                        }
                        Numeric scale = divide.getScale();
                        if (!scale.isInterval()) {
                            if (scale.isFloat()) {
                                frinkBigDecimal = ((FrinkFloat) scale).getBigDec();
                            } else {
                                try {
                                    frinkBigDecimal = new FrinkBigDecimal(scale.doubleValue());
                                } catch (NotRealException e2) {
                                    environment.outputln("format[x,y,digits] expects real numbers as arguments.");
                                    return this;
                                }
                            }
                            if (z) {
                                return new BasicStringExpression(frinkBigDecimal.format(-1, integerValue, -1, -1, 2, 4) + " " + str);
                            }
                            return new BasicStringExpression(frinkBigDecimal.format(-1, integerValue, -1, -1, 2, 4));
                        } else if (z) {
                            return new BasicStringExpression(((RealInterval) scale).toString(integerValue) + " " + str);
                        } else {
                            return new BasicStringExpression(((RealInterval) scale).toString(integerValue));
                        }
                    } catch (NumericException e3) {
                        throw new EvaluationNumericException("Numeric exception in round:\n  " + e3.toString(), this);
                    }
                } catch (NotAnIntegerException e4) {
                    throw new InvalidArgumentException("Third argument to format[num, roundTo, dec] should be an integer.", expression3);
                }
            }
        });
        AnonymousClass45 r05 = new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                    int integerValue2 = BuiltinFunctionSource.getIntegerValue(expression3);
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        int i = integerValue2 + integerValue;
                        int length = string.length();
                        if (integerValue >= length) {
                            return BasicStringExpression.EMPTY_STRING;
                        }
                        if (i > length) {
                            i = length;
                        }
                        return new BasicStringExpression(string.substring(integerValue, i));
                    }
                    throw new InvalidArgumentException("First argument to substrLen must be a string.", expression);
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Second and third arguments to substrLen must be integers.", expression2);
                }
            }
        };
        addFunctionDefinition("substrLen", r05);
        addFunctionDefinition("substringLen", r05);
        AnonymousClass46 r06 = new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                    int integerValue2 = BuiltinFunctionSource.getIntegerValue(expression3);
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        int length = string.length();
                        if (integerValue >= length) {
                            return BasicStringExpression.EMPTY_STRING;
                        }
                        if (integerValue2 > length) {
                            integerValue2 = length;
                        }
                        return new BasicStringExpression(string.substring(integerValue, integerValue2));
                    }
                    throw new InvalidArgumentException("First argument to substr must be a string.", expression);
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Second and third arguments to substr must be integers.", expression2);
                }
            }
        };
        addFunctionDefinition("substr", r06);
        addFunctionDefinition("substring", r06);
        addFunctionDefinition(DictionaryExpression.TYPE, new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new BasicDictionaryExpression();
            }
        });
        addFunctionDefinition("exit", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws ExitException {
                throw new ExitException(this);
            }
        });
        addFunctionDefinition("trim", new StringFunction(true) {
            /* access modifiers changed from: protected */
            public String doFunction(String str) {
                return str.trim();
            }
        });
        addFunctionDefinition("parseInt", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof EnumeratingExpression) {
                    BasicListExpression basicListExpression = new BasicListExpression(0);
                    FrinkEnumeration enumeration = ((EnumeratingExpression) expression).getEnumeration(environment);
                    while (true) {
                        try {
                            Expression next = enumeration.getNext(environment);
                            if (next == null) {
                                break;
                            } else if (!(next instanceof StringExpression)) {
                                throw new InvalidArgumentException("Argument to parseInt must be a string or array of strings.", next);
                            } else {
                                basicListExpression.appendChild(DimensionlessUnitExpression.construct(FrinkInteger.construct(((StringExpression) next).getString(), 10)));
                            }
                        } finally {
                            enumeration.dispose();
                        }
                    }
                }
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("Argument to parseInt must be a string or array of strings.", expression);
                }
                try {
                    return DimensionlessUnitExpression.construct(FrinkInteger.construct(((StringExpression) expression).getString(), 10));
                } catch (NumberFormatException e) {
                    System.out.println("parseInt[x] passed non-integer argument " + environment.format(expression));
                    return UndefExpression.UNDEF;
                }
            }
        });
        addFunctionDefinition("parseInt", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("First argument to parseInt must be a string.", expression);
                }
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                    if (integerValue < 2 || integerValue > 36) {
                        throw new InvalidArgumentException("Second argument to parseInt[number, base] must be a integer between 2 and 36 (inclusive).", expression2);
                    }
                    try {
                        return DimensionlessUnitExpression.construct(FrinkInteger.construct(((StringExpression) expression).getString(), integerValue));
                    } catch (NumberFormatException e) {
                        System.out.println("parseInt[x] passed non-integer argument " + environment.format(expression));
                        return UndefExpression.UNDEF;
                    }
                } catch (NotAnIntegerException e2) {
                    throw new InvalidArgumentException("Second argument to parseInt must be an integer.", expression2);
                }
            }
        });
        addFunctionDefinition("editDistance", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException {
                if (!(expression instanceof StringExpression)) {
                    throw new InvalidArgumentException("Arguments to editDistance must be strings.", expression);
                } else if (expression2 instanceof StringExpression) {
                    return DimensionlessUnitExpression.construct(EditDistance.getEditDistance(((StringExpression) expression).getString(), ((StringExpression) expression2).getString()));
                } else {
                    throw new InvalidArgumentException("Arguments to editDistance must be strings.", expression2);
                }
            }
        });
        addFunctionDefinition("min", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException {
                try {
                    if (ThreeWayComparison.compare(expression, expression2, environment) < 0) {
                        return expression;
                    }
                    return expression2;
                } catch (NotComparableException e) {
                    throw new InvalidArgumentException("Arguments not comparable in min[" + environment.format(expression) + "," + environment.format(expression2) + "]\n " + e, this);
                } catch (OverlapException e2) {
                    throw new InvalidArgumentException("Arguments not comparable in min[" + environment.format(expression) + "," + environment.format(expression2) + "]\n " + e2, this);
                }
            }
        });
        addFunctionDefinition("min", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, EvaluationException {
                ListExpression array;
                try {
                    if (expression instanceof ListExpression) {
                        array = (ListExpression) expression;
                    } else {
                        array = ArrayUtils.toArray(expression, environment);
                    }
                    int childCount = array.getChildCount();
                    Expression child = array.getChild(0);
                    for (int i = 1; i < childCount; i++) {
                        Expression child2 = array.getChild(i);
                        if (ThreeWayComparison.compare(child, child2, environment) > 0) {
                            child = child2;
                        }
                    }
                    return child;
                } catch (NotComparableException e) {
                    throw new InvalidArgumentException("Arguments not comparable in min[" + environment.format(expression) + "]\n " + e, this);
                } catch (OverlapException e2) {
                    throw new InvalidArgumentException("Arguments not comparable in min[" + environment.format(expression) + "]\n " + e2, this);
                }
            }
        });
        addFunctionDefinition("max", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, EvaluationException {
                ListExpression array;
                try {
                    if (expression instanceof ListExpression) {
                        array = (ListExpression) expression;
                    } else {
                        array = ArrayUtils.toArray(expression, environment);
                    }
                    int childCount = array.getChildCount();
                    Expression child = array.getChild(0);
                    for (int i = 1; i < childCount; i++) {
                        Expression child2 = array.getChild(i);
                        if (ThreeWayComparison.compare(child, child2, environment) < 0) {
                            child = child2;
                        }
                    }
                    return child;
                } catch (NotComparableException e) {
                    throw new InvalidArgumentException("Arguments not comparable in min[" + environment.format(expression) + "]\n " + e, this);
                } catch (OverlapException e2) {
                    throw new InvalidArgumentException("Arguments not comparable in max[" + environment.format(expression) + "]\n " + e2, this);
                }
            }
        });
        addFunctionDefinition("max", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException {
                try {
                    if (ThreeWayComparison.compare(expression, expression2, environment) >= 0) {
                        return expression;
                    }
                    return expression2;
                } catch (NotComparableException e) {
                    throw new InvalidArgumentException("Arguments not comparable in max[" + environment.format(expression) + "," + environment.format(expression2) + "]\n " + e, this);
                } catch (OverlapException e2) {
                    throw new InvalidArgumentException("Arguments not comparable in max[" + environment.format(expression) + "," + environment.format(expression2) + "]\n " + e2, this);
                }
            }
        });
        addFunctionDefinition("messageDigest", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException {
                MessageDigest messageDigest;
                BasicStringExpression basicStringExpression;
                if (!(expression instanceof StringExpression) || !(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("messageDigest: Passed invalid arguments.  First argument should be a string to hash and second argument should be a string indicating a hashing algorithm like \"MD5\" or \"SHA-1\".", this);
                }
                try {
                    synchronized (BuiltinFunctionSource.mdTable) {
                        String string = ((StringExpression) expression2).getString();
                        messageDigest = (MessageDigest) BuiltinFunctionSource.mdTable.get(string);
                        if (messageDigest == null) {
                            messageDigest = MessageDigest.getInstance(string);
                            BuiltinFunctionSource.mdTable.put(string, messageDigest);
                        }
                    }
                    synchronized (messageDigest) {
                        messageDigest.update(((StringExpression) expression).getString().getBytes());
                        basicStringExpression = new BasicStringExpression(BuiltinFunctionSource.toHex(messageDigest.digest()));
                    }
                    return basicStringExpression;
                } catch (NoSuchAlgorithmException e) {
                    throw new InvalidArgumentException("messageDigest: no digest algorithm called \"" + environment.format(expression2) + "\" is available.", this);
                }
            }
        });
        addFunctionDefinition("setEngineering", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    FrinkFloat.setEngineering(Truth.isTrue(environment, expression));
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to setEngineering[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("showApproximations", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    FrinkRational.showApprox = Truth.isTrue(environment, expression);
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to showApproximations[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("rationalAsFloat", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    FrinkRational.rationalAsFloat = Truth.isTrue(environment, expression);
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to rationalAsFloat[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("structureEquals", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                BasicMatchingContext basicMatchingContext = new BasicMatchingContext();
                int beginMatch = basicMatchingContext.beginMatch();
                try {
                    return FrinkBoolean.create(expression.structureEquals(expression2, basicMatchingContext, environment, true));
                } finally {
                    basicMatchingContext.rollbackMatch(beginMatch);
                }
            }
        });
        addFunctionDefinition("type", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException {
                return new BasicStringExpression(expression.getExpressionType());
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("childCount", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException {
                return DimensionlessUnitExpression.construct(expression.getChildCount());
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("child", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException, EvaluationException {
                String str;
                try {
                    return expression.getChild(BuiltinFunctionSource.getIntegerValue(expression2.evaluate(environment)));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Second argument to child must be integer.", expression);
                } catch (InvalidChildException e2) {
                    int childCount = expression.getChildCount();
                    if (childCount == 0) {
                        str = "no children.";
                    } else {
                        str = "children with indices 0 to " + (childCount - 1) + ".";
                    }
                    throw new InvalidArgumentException("Attempted to get child " + 0 + " of expression " + environment.format(expression) + " which has " + str, expression);
                }
            }
        });
        addFunctionDefinition("constructExpression", new DoubleArgFunction(true) {
            /* JADX WARN: Type inference failed for: r5v1, types: [frink.expr.ListExpression] */
            /* access modifiers changed from: protected */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public frink.expr.Expression doFunction(frink.expr.Environment r3, frink.expr.Expression r4, frink.expr.Expression r5) throws frink.expr.InvalidArgumentException, frink.expr.EvaluationException, frink.expr.FrinkSecurityException {
                /*
                    r2 = this;
                    frink.security.SecurityHelper r0 = r3.getSecurityHelper()
                    r0.checkConstructExpression()
                    boolean r0 = r5 instanceof frink.expr.ListExpression
                    if (r0 == 0) goto L_0x001d
                    frink.expr.ListExpression r5 = (frink.expr.ListExpression) r5
                    r0 = r5
                L_0x000e:
                    boolean r1 = r4 instanceof frink.expr.StringExpression
                    if (r1 == 0) goto L_0x0028
                    frink.expr.StringExpression r4 = (frink.expr.StringExpression) r4
                    java.lang.String r1 = r4.getString()
                    frink.expr.Expression r0 = frink.symbolic.ExpressionConstructor.construct(r1, r0, r3)
                    return r0
                L_0x001d:
                    frink.expr.BasicListExpression r0 = new frink.expr.BasicListExpression
                    r1 = 1
                    r0.<init>(r1)
                    r1 = 0
                    r0.setChild(r1, r5)
                    goto L_0x000e
                L_0x0028:
                    frink.expr.InvalidArgumentException r0 = new frink.expr.InvalidArgumentException
                    java.lang.String r1 = "First argument to constructExpression must be a string."
                    r0.<init>(r1, r4)
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: frink.function.BuiltinFunctionSource.AnonymousClass65.doFunction(frink.expr.Environment, frink.expr.Expression, frink.expr.Expression):frink.expr.Expression");
            }
        });
        addFunctionDefinition("left", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        int length = string.length();
                        if (length == 0) {
                            return BasicStringExpression.EMPTY_STRING;
                        }
                        if (integerValue > length) {
                            integerValue = length;
                        }
                        return new BasicStringExpression(string.substring(0, integerValue));
                    }
                    throw new InvalidArgumentException("First argument to left must be a string.", expression);
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Second argument to left must be integer.  Value was " + environment.format(expression2), expression2);
                }
            }
        });
        addFunctionDefinition("right", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        int length = string.length();
                        int i = length - integerValue;
                        if (length == 0) {
                            return BasicStringExpression.EMPTY_STRING;
                        }
                        if (i < 0) {
                            i = 0;
                        }
                        return new BasicStringExpression(string.substring(i, length));
                    }
                    throw new InvalidArgumentException("First argument to right must be a string.", expression);
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Second argument to right must be integer.  Value was " + environment.format(expression2), expression2);
                }
            }
        });
        addFunctionDefinition("indexOf", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof StringExpression) && (expression2 instanceof StringExpression)) {
                    return DimensionlessUnitExpression.construct(((StringExpression) expression).getString().indexOf(((StringExpression) expression2).getString()));
                }
                throw new InvalidArgumentException("Both arguments to indexOf[str, substr] must be strings.", expression);
            }
        });
        addFunctionDefinition("expressionContains", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return FrinkBoolean.create(SymbolicUtils.contains(expression, expression2, new BasicMatchingContext(), environment));
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("Invalid child in expressionContains: " + e, expression);
                }
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("freeOf", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                try {
                    return FrinkBoolean.create(SymbolicUtils.freeOf(expression, expression2, new BasicMatchingContext(), environment));
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("Invalid child in expressionContains: " + e, expression);
                }
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("substituteExpression", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                return SymbolicUtils.substitute(expression, expression2, expression3, null, environment, new BasicMatchingContext());
            }
        });
        addFunctionDefinition("showUndefinedSymbols", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    BasicExpressionFormatter.setShowUndefined(Truth.isTrue(environment, expression));
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to showUndefinedSymbols[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("transformExpression", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkTransformExpression();
                try {
                    return environment.getTransformationRuleManager().applyRules(expression, false, false, environment);
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("transformExpression: Unexpected InvalidChildException:\n  + ice", expression);
                }
            }
        });
        addFunctionDefinition("transformExpressionTrace", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkTransformExpression();
                try {
                    return environment.getTransformationRuleManager().applyRules(expression, true, false, environment);
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("transformExpressionTrace: Unexpected InvalidChildException:\n  + ice", expression);
                }
            }
        });
        addFunctionDefinition("transformExpressionDebug", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkTransformExpression();
                try {
                    return environment.getTransformationRuleManager().applyRules(expression, true, true, environment);
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("transformExpressionDebug: Unexpected InvalidChildException:\n  + ice", expression);
                }
            }
        });
        addFunctionDefinition("transformExpression", new DoubleArgFunction(false) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression
             arg types: [frink.expr.Expression, java.lang.String, int, int, frink.expr.Environment]
             candidates:
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression
             arg types: [frink.expr.Expression, frink.expr.ListExpression, int, int, frink.expr.Environment]
             candidates:
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression */
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkTransformExpression();
                try {
                    if (expression instanceof StringExpression) {
                        return environment.getTransformationRuleManager().applyRules(expression2, ((StringExpression) expression).getString(), false, false, environment);
                    } else if (expression instanceof ListExpression) {
                        return environment.getTransformationRuleManager().applyRules(expression2, (ListExpression) expression, false, false, environment);
                    } else {
                        throw new InvalidArgumentException("The first argument to transformExpression[rulesetName, expr] must be a string or array of strings.", this);
                    }
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("transformExpression: Unexpected InvalidChildException:\n  + ice", expression2);
                }
            }
        });
        addFunctionDefinition("transformExpressionTrace", new DoubleArgFunction(false) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression
             arg types: [frink.expr.Expression, java.lang.String, int, int, frink.expr.Environment]
             candidates:
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression
             arg types: [frink.expr.Expression, frink.expr.ListExpression, int, int, frink.expr.Environment]
             candidates:
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression */
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkTransformExpression();
                try {
                    if (expression instanceof StringExpression) {
                        return environment.getTransformationRuleManager().applyRules(expression2, ((StringExpression) expression).getString(), true, false, environment);
                    } else if (expression instanceof ListExpression) {
                        return environment.getTransformationRuleManager().applyRules(expression2, (ListExpression) expression, true, false, environment);
                    } else {
                        throw new InvalidArgumentException("The first argument to transformExpressionTrace[rulesetName, expr] must be a string or array of strings.", this);
                    }
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("transformExpressionTrace: Unexpected InvalidChildException:\n  + ice", expression2);
                }
            }
        });
        addFunctionDefinition("transformExpressionDebug", new DoubleArgFunction(false) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression
             arg types: [frink.expr.Expression, java.lang.String, int, int, frink.expr.Environment]
             candidates:
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression
             arg types: [frink.expr.Expression, frink.expr.ListExpression, int, int, frink.expr.Environment]
             candidates:
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, java.lang.String, boolean, boolean, frink.expr.Environment):frink.expr.Expression
              frink.symbolic.TransformationRuleManager.applyRules(frink.expr.Expression, frink.expr.ListExpression, boolean, boolean, frink.expr.Environment):frink.expr.Expression */
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, FrinkSecurityException {
                environment.getSecurityHelper().checkTransformExpression();
                try {
                    if (expression instanceof StringExpression) {
                        return environment.getTransformationRuleManager().applyRules(expression2, ((StringExpression) expression).getString(), true, true, environment);
                    } else if (expression instanceof ListExpression) {
                        return environment.getTransformationRuleManager().applyRules(expression2, (ListExpression) expression, true, true, environment);
                    } else {
                        throw new InvalidArgumentException("The first argument to transformExpressionDebug[rulesetName, expr] must be a string or array of strings.", this);
                    }
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("transformExpressionDebug: Unexpected InvalidChildException:\n  + ice", expression2);
                }
            }
        });
        addFunctionDefinition("symbolicMode", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    boolean isTrue = Truth.isTrue(environment, expression);
                    environment.setSymbolicMode(isTrue);
                    BasicExpressionFormatter.setShowUndefined(!isTrue);
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to symbolicMode[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("isConstant", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                expression.evaluate(environment);
                return FrinkBoolean.create(expression.isConstant());
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("parseToExpression", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws FrinkSecurityException, EvaluationException {
                if (expression instanceof StringExpression) {
                    environment.getSecurityHelper().checkUnsafeEval();
                    try {
                        return environment.eval(((StringExpression) expression).getString());
                    } catch (Throwable th) {
                        return UndefExpression.UNDEF;
                    }
                } else {
                    throw new InvalidArgumentException("Argument to parseToExpression must be a string.", expression);
                }
            }
        });
        addFunctionDefinition("FrinkVersion", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) {
                try {
                    return new BasicStringExpression((String) Class.forName("frink.parser.FrinkVersion").getField("VERSION").get(null));
                } catch (ClassNotFoundException e) {
                    System.out.println("Could not find version " + e);
                    return new BasicStringExpression("No version number compiled in.");
                } catch (NoSuchFieldException e2) {
                    System.out.println("Could not find version " + e2);
                    return new BasicStringExpression("No version number compiled in.");
                } catch (IllegalAccessException e3) {
                    System.out.println("Could not find version " + e3);
                    return new BasicStringExpression("No version number compiled in.");
                }
            }
        });
        addFunctionDefinition("deepCopy", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof DeepCopyable) {
                    return ((DeepCopyable) expression).deepCopy(-1);
                }
                return expression;
            }
        });
        addFunctionDefinition("showDimensionName", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                try {
                    BasicExpressionFormatter.setShowDimensionName(Truth.isTrue(environment, expression));
                    return VoidExpression.VOID;
                } catch (EvaluationException e) {
                    throw new InvalidArgumentException("Argument to showDimensionName[x] should be boolean.", expression);
                }
            }
        });
        addFunctionDefinition("setDimensionNameDelimiters", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                environment.getSecurityHelper().checkSetGlobalFlag();
                if (!(expression instanceof StringExpression) || !(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("Arguments to setDimensionNameDelimiters must be strings.", expression);
                }
                BasicExpressionFormatter.setDimensionNameDelimiters(((StringExpression) expression).getString(), ((StringExpression) expression2).getString());
                return VoidExpression.VOID;
            }
        });
        addFunctionDefinition("functions", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new EnumerationWrapper(environment.getFunctionManager().getFunctionDescriptors(), FunctionDescriptorExpressionFactory.INSTANCE);
            }
        });
        addFunctionDefinition("isDefined", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                boolean z;
                if (expression instanceof SymbolExpression) {
                    if (environment.getSymbolDefinition(((SymbolExpression) expression).getName(), false) != null) {
                        z = true;
                    } else {
                        z = false;
                    }
                    return FrinkBoolean.create(z);
                }
                Expression evaluate = expression.evaluate(environment);
                if (evaluate instanceof StringExpression) {
                    return FrinkBoolean.create(environment.getSymbolDefinition(((StringExpression) evaluate).getString(), false) != null);
                }
                throw new InvalidArgumentException("Argument to isDefined[x] should be a variable name or string.", evaluate);
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("isVariableDefined", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof SymbolExpression) {
                    return FrinkBoolean.create(environment.isVariableDefined(((SymbolExpression) expression).getName()));
                }
                Expression evaluate = expression.evaluate(environment);
                if (evaluate instanceof StringExpression) {
                    return FrinkBoolean.create(environment.isVariableDefined(((StringExpression) evaluate).getString()));
                }
                throw new InvalidArgumentException("Argument to isDefined[x] should be a variable name or string.", evaluate);
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
        addFunctionDefinition("sleep", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                long j;
                Unit unit = environment.getUnitManager().getUnit("ns");
                if (unit == null) {
                    environment.outputln("In sleep[] function:  Could not get unit 'ns' (for seconds.)  Unable to sleep.");
                }
                try {
                    Unit divide = UnitMath.divide(BuiltinFunctionSource.getUnitValue(expression), unit);
                    if (!UnitMath.isDimensionless(divide)) {
                        throw new InvalidArgumentException("Argument to sleep[time] must have dimensions of time.  Value was " + environment.format(expression), expression);
                    }
                    try {
                        j = UnitMath.getLongValue(UnitMath.ceil(divide));
                    } catch (NotAnIntegerException e) {
                        j = 0;
                    } catch (ConformanceException e2) {
                        j = 0;
                    }
                    long j2 = j / 1000000;
                    try {
                        Thread.sleep(j2, (int) (j - (1000000 * j2)));
                    } catch (InterruptedException e3) {
                    }
                    return VoidExpression.VOID;
                } catch (NumericException e4) {
                    throw new InvalidArgumentException("Argument to sleep[time] must be a real number with dimensions of time.  Value was " + environment.format(expression), expression);
                }
            }
        });
        addFunctionDefinition("methods", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                Enumeration<FunctionDescriptor> functionDescriptors;
                if (!(expression instanceof FrinkObject)) {
                    throw new InvalidArgumentException("Argument to methods is not an object.  Object was " + environment.format(expression), this);
                }
                BasicListExpression basicListExpression = new BasicListExpression(0);
                FunctionSource functionSource = ((FrinkObject) expression).getFunctionSource(environment);
                if (!(functionSource == null || (functionDescriptors = functionSource.getFunctionDescriptors()) == null)) {
                    while (functionDescriptors.hasMoreElements()) {
                        basicListExpression.appendChild(new BasicStringExpression("\n" + functionDescriptors.nextElement().toString(environment)));
                    }
                }
                return basicListExpression;
            }
        });
        addFunctionDefinition("getSymbols", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                return SymbolicUtils.getSymbols(expression);
            }
        });
        addFunctionDefinition("getSymbolsByComplexity", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                return SymbolicUtils.getSymbolsByComplexity(expression, environment);
            }
        });
        addFunctionDefinition("union", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof SetExpression) && (expression2 instanceof SetExpression)) {
                    return SetUtils.union((SetExpression) expression, (SetExpression) expression2, environment);
                }
                throw new InvalidArgumentException("Arguments to union[a,b] must both be sets.", this);
            }
        });
        addFunctionDefinition("intersection", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof SetExpression) && (expression2 instanceof SetExpression)) {
                    return SetUtils.intersection((SetExpression) expression, (SetExpression) expression2, environment);
                }
                throw new InvalidArgumentException("Arguments to intersection[a,b] must both be sets.", this);
            }
        });
        addFunctionDefinition("setsIntersect", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof SetExpression) && (expression2 instanceof SetExpression)) {
                    return FrinkBoolean.create(SetUtils.setsIntersect((SetExpression) expression, (SetExpression) expression2, environment));
                }
                throw new InvalidArgumentException("Arguments to setsIntersect[a,b] must both be sets.", this);
            }
        });
        addFunctionDefinition("setDifference", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof SetExpression) && (expression2 instanceof SetExpression)) {
                    return SetUtils.difference((SetExpression) expression, (SetExpression) expression2, environment);
                }
                throw new InvalidArgumentException("Arguments to setDifference[a,b] must both be sets.  Arguments were " + environment.format(expression) + ", " + environment.format(expression2), this);
            }
        });
        addFunctionDefinition("isSubset", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof SetExpression) && (expression2 instanceof SetExpression)) {
                    return FrinkBoolean.create(SetUtils.isSubset((SetExpression) expression, (SetExpression) expression2, false, environment));
                }
                throw new InvalidArgumentException("Arguments to isSubset[a,b] must both be sets.", this);
            }
        });
        addFunctionDefinition("isProperSubset", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if ((expression instanceof SetExpression) && (expression2 instanceof SetExpression)) {
                    return FrinkBoolean.create(SetUtils.isSubset((SetExpression) expression, (SetExpression) expression2, true, environment));
                }
                throw new InvalidArgumentException("Arguments to isProperSubset[a,b] must both be sets.", this);
            }
        });
        addFunctionDefinition("URLEncode", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof StringExpression) || !(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("Arguments to URLEncode must both be strings, with the second being a valid encoding type.", this);
                }
                try {
                    return new BasicStringExpression(TempURLEncoder.encode(((StringExpression) expression).getString(), ((StringExpression) expression2).getString()));
                } catch (UnsupportedEncodingException e) {
                    throw new InvalidArgumentException("Unsupported encoding in call to URLEncode:\n  " + e, this);
                }
            }
        });
        addFunctionDefinition("repeat", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                        if (integerValue >= 0) {
                            return new BasicStringExpression(StringUtils.repeat(((StringExpression) expression).getString(), integerValue));
                        }
                        throw new InvalidArgumentException("Second argument to repeat[str, times] must be a positive integer.  Value was " + integerValue + ".", this);
                    } catch (NotAnIntegerException e) {
                    }
                }
                throw new InvalidArgumentException("Arguments to repeat[str, times] must be [string, int].", this);
            }
        });
        addFunctionDefinition("padLeft", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                if ((expression instanceof StringExpression) && (expression3 instanceof StringExpression)) {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                        if (integerValue < 0) {
                            throw new InvalidArgumentException("Second argument to padLeft[str, width, padChar] must be a positive integer.  Value was " + integerValue + ".", this);
                        }
                        String string = ((StringExpression) expression3).getString();
                        if (string.length() == 1) {
                            return new BasicStringExpression(StringUtils.padLeft(((StringExpression) expression).getString(), integerValue, string.charAt(0)));
                        }
                        throw new InvalidArgumentException("Third argument to padLeft[str, width, padChar] must be a single-character string.  Value was \"" + string + "\".", this);
                    } catch (NotAnIntegerException e) {
                    }
                }
                throw new InvalidArgumentException("Arguments to padLeft[str, width, padChar] must be [string, int, string].", this);
            }
        });
        addFunctionDefinition("padRight", new TripleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                if ((expression instanceof StringExpression) && (expression3 instanceof StringExpression)) {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                        if (integerValue < 0) {
                            throw new InvalidArgumentException("Second argument to padRight[str, width, padChar] must be a positive integer.  Value was " + integerValue + ".", this);
                        }
                        String string = ((StringExpression) expression3).getString();
                        if (string.length() == 1) {
                            return new BasicStringExpression(StringUtils.padRight(((StringExpression) expression).getString(), integerValue, string.charAt(0)));
                        }
                        throw new InvalidArgumentException("Third argument to padRight[str, width, padChar] must be a single-character string.  Value was \"" + string + "\".", this);
                    } catch (NotAnIntegerException e) {
                    }
                }
                throw new InvalidArgumentException("Arguments to padRight[str, width, padChar] must be [string, int, string].", this);
            }
        });
        addFunctionDefinition("makeArray", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException {
                if (expression instanceof ListExpression) {
                    try {
                        return ArrayUtils.makeArray((ListExpression) expression, expression2, environment);
                    } catch (InvalidArgumentException e) {
                    }
                }
                throw new InvalidArgumentException("First argument to makeArray must be an array of integers indicating the dimensions of the array, such as [2,3].  Value was " + environment.format(expression), expression);
            }
        });
        addFunctionDefinition("makeArray", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException {
                if (expression instanceof ListExpression) {
                    try {
                        return ArrayUtils.makeArray((ListExpression) expression, (Expression) null, environment);
                    } catch (InvalidArgumentException e) {
                    }
                }
                throw new InvalidArgumentException("First argument to makeArray must be an array of integers indicating the dimensions of the array, such as [2,3].  Value was " + environment.format(expression), expression);
            }
        });
        addFunctionDefinition("toString", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) {
                if (expression instanceof StringExpression) {
                    return expression;
                }
                return new BasicStringExpression(environment.format(expression));
            }
        });
        addFunctionDefinition("getBoundingBox", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException {
                if (expression instanceof GraphicsExpression) {
                    BoundingBox boundingBox = ((GraphicsExpression) expression).getDrawable().getBoundingBox();
                    BasicListExpression basicListExpression = new BasicListExpression(4);
                    basicListExpression.appendChild(BasicUnitExpression.construct(boundingBox.getLeft()));
                    basicListExpression.appendChild(BasicUnitExpression.construct(boundingBox.getTop()));
                    basicListExpression.appendChild(BasicUnitExpression.construct(boundingBox.getRight()));
                    basicListExpression.appendChild(BasicUnitExpression.construct(boundingBox.getBottom()));
                    return basicListExpression;
                }
                throw new InvalidArgumentException("Argument to getBoundingBox() should be a GraphicsExpression.  Value was " + environment.format(expression), expression);
            }
        });
    }

    public static long getLongValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getLongValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static int getIntegerValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getIntegerValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkRational getRationalValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getRationalValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static short getShortValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getShortValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static byte getByteValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getByteValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static BigInteger getBigIntegerValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getBigIntegerValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkInteger getFrinkIntegerValue(Expression expression) throws NotAnIntegerException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getFrinkIntegerValue(((UnitExpression) expression).getUnit());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkReal getFrinkRealValue(Expression expression) throws NotRealException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getFrinkRealValue(((UnitExpression) expression).getUnit());
        }
        throw NotRealException.INSTANCE;
    }

    public static double getDoubleValue(Expression expression) throws NotRealException {
        return getFrinkRealValue(expression).doubleValue();
    }

    public static Numeric getNumericValue(Expression expression) throws NotRealException {
        if (expression instanceof UnitExpression) {
            return UnitMath.getNumericValue(((UnitExpression) expression).getUnit());
        }
        throw NotRealException.INSTANCE;
    }

    public static Unit getUnitValue(Expression expression) throws InvalidArgumentException {
        if (expression instanceof UnitExpression) {
            return ((UnitExpression) expression).getUnit();
        }
        throw new InvalidArgumentException("Argument is not a unit.", expression);
    }

    public static boolean isRational(Expression expression) {
        if (expression instanceof UnitExpression) {
            return ((UnitExpression) expression).getUnit().getScale().isRational();
        }
        return false;
    }

    public static boolean isNegativeUnit(Expression expression) {
        try {
            if (expression instanceof UnitExpression) {
                return UnitMath.isNegative(((UnitExpression) expression).getUnit());
            }
        } catch (NumericException | OverlapException e) {
        }
        return false;
    }

    public static int randomInt(int i) {
        int nextInt;
        int i2;
        if (((-i) & i) == i) {
            return (int) ((((long) i) * ((long) (random.nextInt() >>> 1))) >> 31);
        }
        do {
            nextInt = random.nextInt() >>> 1;
            i2 = nextInt % i;
        } while ((nextInt - i2) + (i - 1) < 0);
        return i2;
    }

    /* access modifiers changed from: private */
    public static String toHex(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(bArr.length * 2);
        for (byte b : bArr) {
            byte b2 = b & 255;
            if (b2 < 16) {
                stringBuffer.append("0");
            }
            stringBuffer.append(Integer.toHexString(b2));
        }
        return stringBuffer.toString();
    }
}
