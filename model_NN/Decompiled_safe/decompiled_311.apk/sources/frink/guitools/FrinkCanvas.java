package frink.guitools;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.function.FunctionDefinition;
import frink.java.JavaObject;
import frink.java.JavaObjectFactory;
import frink.object.FrinkObject;
import java.awt.Canvas;
import java.awt.Graphics;

public class FrinkCanvas extends Canvas {
    private JavaObject canvasWrapper;
    private Environment env;
    public FunctionDefinition onPaint = null;
    public FunctionDefinition onUpdate = null;

    public FrinkCanvas(Environment environment) {
        this.env = environment;
        this.canvasWrapper = JavaObjectFactory.create(this);
    }

    public void setPaint(FunctionDefinition functionDefinition) {
        this.onPaint = functionDefinition;
    }

    public void setUpdate(FunctionDefinition functionDefinition) {
        this.onUpdate = functionDefinition;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.java.JavaObject, int, frink.java.JavaObject, int]
     candidates:
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    public void update(Graphics graphics) {
        if (this.onUpdate != null) {
            try {
                this.env.getFunctionManager().execute(this.onUpdate, this.env, (Expression) JavaObjectFactory.create(graphics), false, (FrinkObject) this.canvasWrapper, true);
            } catch (EvaluationException e) {
                this.env.outputln("FrinkCanvas.update:\n  " + e);
            }
        } else {
            FrinkCanvas.super.update(graphics);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
     arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.java.JavaObject, int, frink.java.JavaObject, int]
     candidates:
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
    public void paint(Graphics graphics) {
        if (this.onPaint != null) {
            try {
                this.env.getFunctionManager().execute(this.onPaint, this.env, (Expression) JavaObjectFactory.create(graphics), false, (FrinkObject) this.canvasWrapper, true);
            } catch (EvaluationException e) {
                this.env.outputln("FrinkCanvas.paint:\n  " + e);
            }
        }
    }
}
