package frink.guitools;

import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.function.BasicFunctionSource;
import frink.function.ZeroArgFunction;
import frink.java.JavaObjectFactory;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GUIToolsFunctionSource extends BasicFunctionSource {
    public static final GUIToolsFunctionSource INSTANCE = new GUIToolsFunctionSource();

    private GUIToolsFunctionSource() {
        super("GUIToolsFunctionSource");
        initializeFunctions();
    }

    private void initializeFunctions() {
        addFunctionDefinition("createCanvasFrame", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws FrinkSecurityException {
                final Frame frame = new Frame();
                frame.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent windowEvent) {
                        frame.dispose();
                    }
                });
                FrinkCanvas frinkCanvas = new FrinkCanvas(environment);
                frame.add(frinkCanvas, "Center");
                return JavaObjectFactory.create(frinkCanvas);
            }
        });
    }
}
