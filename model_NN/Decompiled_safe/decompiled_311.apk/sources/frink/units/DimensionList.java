package frink.units;

public interface DimensionList {
    int getExponentDenominatorByIndex(int i);

    int getExponentNumeratorByIndex(int i);

    int getHighestIndex();

    boolean hasFractionalExponents();
}
