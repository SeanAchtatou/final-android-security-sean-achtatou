package frink.units;

public class BasicDimension implements Dimension {
    private String fundamentalUnitName;
    private String name;

    public BasicDimension(String str, String str2) {
        this.name = str;
        this.fundamentalUnitName = str2;
    }

    public String getName() {
        return this.name;
    }

    public String getFundamentalUnitName() {
        return this.fundamentalUnitName;
    }
}
