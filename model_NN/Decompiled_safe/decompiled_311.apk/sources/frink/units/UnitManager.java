package frink.units;

import java.util.Enumeration;

public interface UnitManager {
    void addPrefix(String str, Unit unit);

    void addUnitSource(Source<Unit> source);

    Enumeration<String> getNames();

    Unit getUnit(String str);

    Source<Unit> getUnitSource(String str);

    void removeUnitSource(String str);
}
