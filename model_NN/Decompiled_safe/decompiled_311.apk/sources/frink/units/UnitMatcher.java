package frink.units;

import java.util.Enumeration;

public class UnitMatcher {
    public static Enumeration<String> getConformalUnits(Unit unit, UnitManager unitManager) {
        return new ConformalUnitEnumeration(unit, unitManager);
    }
}
