package frink.units;

public interface Dimension {
    String getFundamentalUnitName();

    String getName();
}
