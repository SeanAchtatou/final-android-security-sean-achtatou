package frink.units;

public class FractionalDimensionList implements DimensionList {
    private int[] denominators;
    private int[] numerators;

    public FractionalDimensionList(int[] iArr, int[] iArr2) {
        this.numerators = iArr;
        this.denominators = iArr2;
    }

    public int getExponentNumeratorByIndex(int i) {
        if (this.numerators == null || i >= this.numerators.length) {
            return 0;
        }
        return this.numerators[i];
    }

    public int getExponentDenominatorByIndex(int i) {
        if (this.denominators == null || i >= this.denominators.length) {
            return 1;
        }
        return this.denominators[i];
    }

    public int getHighestIndex() {
        if (this.numerators == null) {
            return -1;
        }
        return this.numerators.length - 1;
    }

    public boolean hasFractionalExponents() {
        return true;
    }
}
