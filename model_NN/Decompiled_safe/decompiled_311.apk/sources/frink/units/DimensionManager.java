package frink.units;

public interface DimensionManager {
    Unit addDimension(String str, String str2) throws ExistsException;

    Dimension getDimensionByIndex(int i);

    Dimension getDimensionByName(String str);

    int getHighestIndex();

    int getIndex(Dimension dimension) throws InvalidIndexException;
}
