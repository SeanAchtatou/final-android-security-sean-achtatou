package frink.units;

import frink.numeric.Numeric;

public interface Unit {
    DimensionList getDimensionList();

    Numeric getScale();
}
