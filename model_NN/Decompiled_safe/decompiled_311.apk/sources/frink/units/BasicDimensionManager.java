package frink.units;

import frink.numeric.FrinkInt;
import java.util.Hashtable;
import java.util.Vector;

public class BasicDimensionManager implements DimensionManager {
    private Vector<Dimension> dimensions = new Vector<>();
    private int highestIndex = -1;
    private Hashtable<String, Dimension> nameMap = new Hashtable<>();

    public Dimension getDimensionByName(String str) {
        return this.nameMap.get(str);
    }

    public synchronized Unit addDimension(String str, String str2) throws ExistsException {
        BasicDimensionList basicDimensionList;
        if (this.nameMap.get(str) != null) {
            throw new ExistsException();
        }
        BasicDimension basicDimension = new BasicDimension(str, str2);
        this.nameMap.put(str, basicDimension);
        this.dimensions.addElement(basicDimension);
        this.highestIndex++;
        basicDimensionList = new BasicDimensionList(this.highestIndex);
        basicDimensionList.setIndexAt(this.highestIndex, 1);
        return new BasicUnit(FrinkInt.ONE, basicDimensionList);
    }

    public int getIndex(Dimension dimension) throws InvalidIndexException {
        int indexOf = this.dimensions.indexOf(dimension);
        if (indexOf != -1) {
            return indexOf;
        }
        throw new InvalidIndexException();
    }

    public int getHighestIndex() {
        return this.highestIndex;
    }

    public Dimension getDimensionByIndex(int i) {
        return this.dimensions.elementAt(i);
    }
}
