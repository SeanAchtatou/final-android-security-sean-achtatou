package frink.units;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class BasicObjectEnumeration<T> implements Enumeration<String> {
    private BasicManager<T> manager;
    private Enumeration<String> objIter;
    private int sourcesIndex = -1;

    protected BasicObjectEnumeration(BasicManager<T> basicManager) {
        this.manager = basicManager;
        getNextSource();
    }

    public boolean hasMoreElements() {
        if (this.objIter != null && this.objIter.hasMoreElements()) {
            return true;
        }
        getNextSource();
        return this.sourcesIndex < this.manager.getSourceCount();
    }

    public String nextElement() {
        if (this.objIter != null && this.objIter.hasMoreElements()) {
            return this.objIter.nextElement();
        }
        getNextSource();
        if (this.sourcesIndex < this.manager.getSourceCount()) {
            return this.objIter.nextElement();
        }
        throw new NoSuchElementException("BasicManager.java.ObjectEnumeration.nextElement:  no more units exist. ");
    }

    private void getNextSource() {
        if (this.sourcesIndex < this.manager.getSourceCount()) {
            while (this.sourcesIndex < this.manager.getSourceCount() - 1) {
                this.sourcesIndex++;
                Source<T> sourceAtIndex = this.manager.getSourceAtIndex(this.sourcesIndex);
                if (sourceAtIndex != null) {
                    this.objIter = sourceAtIndex.getNames();
                    if (this.objIter != null && this.objIter.hasMoreElements()) {
                        return;
                    }
                }
            }
            this.sourcesIndex++;
            this.objIter = null;
        }
    }
}
