package frink.units;

import java.util.Enumeration;
import java.util.Hashtable;

public class BasicSource<T> implements Source<T> {
    private Hashtable<String, T> map = new Hashtable<>();
    private String name;

    public BasicSource(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public T get(String str) {
        return this.map.get(str);
    }

    public Enumeration<String> getNames() {
        return this.map.keys();
    }

    public void add(String str, T t) {
        this.map.put(str, t);
    }
}
