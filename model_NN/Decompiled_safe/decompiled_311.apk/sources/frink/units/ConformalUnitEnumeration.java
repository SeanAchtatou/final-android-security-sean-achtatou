package frink.units;

import java.util.Enumeration;

public class ConformalUnitEnumeration implements Enumeration<String> {
    private Unit matchUnit;
    private String nextUnit = getNext();
    private UnitManager uMgr;
    private Enumeration<String> unitEnum;

    public ConformalUnitEnumeration(Unit unit, UnitManager unitManager) {
        this.matchUnit = unit;
        this.uMgr = unitManager;
        this.unitEnum = unitManager.getNames();
    }

    private String getNext() {
        while (this.unitEnum.hasMoreElements()) {
            String nextElement = this.unitEnum.nextElement();
            if (UnitMath.areConformal(this.matchUnit, this.uMgr.getUnit(nextElement))) {
                return nextElement;
            }
        }
        return null;
    }

    public boolean hasMoreElements() {
        return this.nextUnit != null;
    }

    public String nextElement() {
        String str = this.nextUnit;
        this.nextUnit = getNext();
        return str;
    }
}
