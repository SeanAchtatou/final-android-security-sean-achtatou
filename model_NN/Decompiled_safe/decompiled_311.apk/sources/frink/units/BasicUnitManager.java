package frink.units;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class BasicUnitManager implements UnitManager {
    protected Manager<Unit> mgr = new BasicManager();
    protected Hashtable<String, Unit> prefixMap = new Hashtable<>();
    protected Vector<Vector<String>> prefixVector = new Vector<>();

    public abstract Unit getUnit(String str);

    public Enumeration<String> getNames() {
        return this.mgr.getNames();
    }

    public void addUnitSource(Source<Unit> source) {
        this.mgr.addSource(source);
    }

    public void removeUnitSource(String str) {
        this.mgr.removeSource(str);
    }

    public Source<Unit> getUnitSource(String str) {
        return this.mgr.getSource(str);
    }

    public void addPrefix(String str, Unit unit) {
        if (str.length() != 0) {
            char charAt = str.charAt(0);
            int length = str.length();
            Vector vector = null;
            if (charAt >= this.prefixVector.size()) {
                this.prefixVector.setSize(charAt + 1);
            } else {
                vector = this.prefixVector.elementAt(charAt);
            }
            if (vector == null) {
                Vector vector2 = new Vector(1);
                vector2.addElement(str);
                this.prefixVector.setElementAt(vector2, charAt);
                this.prefixMap.put(str, unit);
                return;
            }
            int size = vector.size();
            this.prefixMap.put(str, unit);
            for (int i = 0; i < size; i++) {
                if (length > ((String) vector.elementAt(i)).length()) {
                    vector.insertElementAt(str, i);
                    return;
                }
            }
            vector.addElement(str);
        }
    }

    /* access modifiers changed from: protected */
    public Vector<String> findPrefixMatches(String str) {
        if (str.length() == 0) {
            return null;
        }
        char charAt = str.charAt(0);
        if (charAt > this.prefixVector.size()) {
            return null;
        }
        return this.prefixVector.elementAt(charAt);
    }
}
