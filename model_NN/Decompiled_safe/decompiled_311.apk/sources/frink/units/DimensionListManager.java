package frink.units;

import java.util.Enumeration;

public interface DimensionListManager {
    void addDefinition(String str, DimensionList dimensionList) throws ExistsException;

    DimensionList getDimensionList(String str);

    String getName(DimensionList dimensionList);

    Enumeration<String> getNames();
}
