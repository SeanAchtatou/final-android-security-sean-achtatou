package frink.units;

import frink.numeric.NumericException;
import java.util.Vector;

public class EnglishUnitManager extends BasicUnitManager {
    public Unit getUnit(String str) {
        Unit unit = (Unit) this.mgr.get(str);
        if (unit != null) {
            return unit;
        }
        boolean endsWith = str.endsWith("s");
        if (endsWith && str.length() > 2) {
            Unit unit2 = (Unit) this.mgr.get(str.substring(0, str.length() - 1));
            if (unit2 != null) {
                return unit2;
            }
        }
        Vector<String> findPrefixMatches = findPrefixMatches(str);
        if (findPrefixMatches != null) {
            int size = findPrefixMatches.size();
            for (int i = 0; i < size; i++) {
                String elementAt = findPrefixMatches.elementAt(i);
                if (str.startsWith(elementAt)) {
                    try {
                        String substring = str.substring(elementAt.length());
                        Unit unit3 = (Unit) this.mgr.get(substring);
                        if (unit3 != null) {
                            return UnitMath.multiply(unit3, (Unit) this.prefixMap.get(elementAt));
                        }
                        if (endsWith) {
                            Unit unit4 = (Unit) this.mgr.get(substring.substring(0, substring.length() - 1));
                            if (unit4 != null) {
                                return UnitMath.multiply(unit4, (Unit) this.prefixMap.get(elementAt));
                            }
                        } else {
                            continue;
                        }
                    } catch (NumericException e) {
                        System.err.println("Unexpected exception in EnglishUnitManager.getUnit: \n" + e);
                    }
                }
            }
        }
        return null;
    }
}
