package frink.units;

import java.util.Enumeration;

public interface Source<T> {
    T get(String str);

    String getName();

    Enumeration<String> getNames();
}
