package frink.units;

import java.util.Enumeration;

public interface Manager<T> {
    void addSource(Source<T> source);

    T get(String str);

    Enumeration<String> getNames();

    Source<T> getSource(String str);

    void removeSource(String str);
}
