package frink.units;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class BasicDimensionListManager implements DimensionListManager {
    private Vector<DimensionPair> dimensionVect = new Vector<>();
    private Hashtable<String, DimensionList> nameMap = new Hashtable<>();

    public DimensionList getDimensionList(String str) {
        return this.nameMap.get(str);
    }

    public String getName(DimensionList dimensionList) {
        try {
            int findNextLargest = findNextLargest(dimensionList, false);
            if (findNextLargest >= this.dimensionVect.size()) {
                return null;
            }
            DimensionPair elementAt = this.dimensionVect.elementAt(findNextLargest);
            if (DimensionListMath.equals(elementAt.getDimensionList(), dimensionList)) {
                return elementAt.getName();
            }
            return null;
        } catch (ExistsException e) {
            return null;
        }
    }

    public Enumeration<String> getNames() {
        return this.nameMap.keys();
    }

    public void addDefinition(String str, DimensionList dimensionList) throws ExistsException {
        if (this.nameMap.get(str) == null) {
            this.nameMap.put(str, dimensionList);
            insertDimensionList(str, dimensionList);
            return;
        }
        throw new ExistsException();
    }

    private void insertDimensionList(String str, DimensionList dimensionList) throws ExistsException {
        this.dimensionVect.insertElementAt(new DimensionPair(str, dimensionList), findNextLargest(dimensionList, true));
    }

    private int findNextLargest(DimensionList dimensionList, boolean z) throws ExistsException {
        int size = this.dimensionVect.size() - 1;
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compare = DimensionListMath.compare(dimensionList, this.dimensionVect.elementAt(i2).getDimensionList());
            if (compare == 0) {
                if (!z) {
                    return i2;
                }
                throw new ExistsException();
            } else if (compare < 0) {
                size = i2 - 1;
            } else if (compare > 0) {
                i = i2 + 1;
            }
        }
        return i;
    }

    private static class DimensionPair {
        private DimensionList dList;
        private String name;

        private DimensionPair(String str, DimensionList dimensionList) {
            this.name = str;
            this.dList = dimensionList;
        }

        /* access modifiers changed from: private */
        public String getName() {
            return this.name;
        }

        /* access modifiers changed from: private */
        public DimensionList getDimensionList() {
            return this.dList;
        }
    }
}
