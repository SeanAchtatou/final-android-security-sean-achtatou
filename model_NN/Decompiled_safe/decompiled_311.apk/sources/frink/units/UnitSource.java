package frink.units;

import java.util.Enumeration;

public interface UnitSource {
    String getName();

    Unit getUnit(String str);

    Enumeration<String> getUnitNames();
}
