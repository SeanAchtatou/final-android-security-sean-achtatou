package frink.units;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.NotANumber;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import java.math.BigInteger;
import java.util.Enumeration;

public class UnitMath {
    public static Unit multiply(Numeric numeric, Unit unit) throws NumericException {
        if (numeric != null && unit != null) {
            return new BasicUnit(NumericMath.multiply(numeric, unit.getScale()), unit.getDimensionList());
        }
        throw new NullPointerException();
    }

    public static Unit divide(Numeric numeric, Unit unit) throws NumericException {
        if (numeric != null && unit != null) {
            return new BasicUnit(NumericMath.divide(numeric, unit.getScale()), DimensionListMath.power(unit.getDimensionList(), -1));
        }
        throw new NullPointerException();
    }

    public static Unit reciprocal(Unit unit) throws NumericException {
        if (unit != null) {
            return new BasicUnit(NumericMath.reciprocal(unit.getScale()), DimensionListMath.power(unit.getDimensionList(), -1));
        }
        throw new NullPointerException();
    }

    public static Unit multiply(Unit unit, Unit unit2) throws NumericException {
        if (unit != null && unit2 != null) {
            return new BasicUnit(NumericMath.multiply(unit.getScale(), unit2.getScale()), DimensionListMath.multiply(unit.getDimensionList(), unit2.getDimensionList()));
        }
        throw new NullPointerException();
    }

    public static Unit divide(Unit unit, Unit unit2) throws NumericException {
        if (unit != null && unit2 != null) {
            return new BasicUnit(NumericMath.divide(unit.getScale(), unit2.getScale()), DimensionListMath.divide(unit.getDimensionList(), unit2.getDimensionList()));
        }
        throw new NullPointerException();
    }

    public static Unit add(Unit unit, Unit unit2) throws ConformanceException, NumericException {
        if (unit == null || unit2 == null) {
            throw new NullPointerException();
        } else if (DimensionListMath.equals(unit.getDimensionList(), unit2.getDimensionList())) {
            return new BasicUnit(NumericMath.add(unit.getScale(), unit2.getScale()), unit.getDimensionList());
        } else {
            throw new ConformanceException("Cannot add units with different dimensions.");
        }
    }

    public static Unit add(Unit unit, Unit unit2, Unit unit3) throws ConformanceException, NumericException {
        return add(add(unit, unit2), unit3);
    }

    public static Unit subtract(Unit unit, Unit unit2) throws ConformanceException, NumericException {
        if (unit == null || unit2 == null) {
            throw new NullPointerException();
        } else if (DimensionListMath.equals(unit.getDimensionList(), unit2.getDimensionList())) {
            return new BasicUnit(NumericMath.subtract(unit.getScale(), unit2.getScale()), unit.getDimensionList());
        } else {
            throw new ConformanceException("Cannot subtract units with differing dimensions.", unit.getDimensionList(), unit2.getDimensionList());
        }
    }

    public static Unit power(Unit unit, Unit unit2) throws ConformanceException, NumericException {
        if (isDimensionless(unit2)) {
            return new BasicUnit(NumericMath.power(unit.getScale(), unit2.getScale()), DimensionListMath.power(unit.getDimensionList(), unit2.getScale()));
        }
        throw new ConformanceException("Exponent is not a dimensionless integer.", unit2.getDimensionList(), null);
    }

    public static Unit square(Unit unit) throws ConformanceException, NumericException {
        return power(unit, DimensionlessUnit.TWO);
    }

    public static Unit sqrt(Unit unit) throws ConformanceException, NumericException {
        return power(unit, DimensionlessUnit.ONE_HALF);
    }

    public static Unit negate(Unit unit) throws NumericException {
        return new BasicUnit(NumericMath.negate(unit.getScale()), unit.getDimensionList());
    }

    public static String toString(Unit unit, DimensionManager dimensionManager) {
        Object scale = unit.getScale();
        if (scale == null) {
            scale = NotANumber.NaN;
        }
        StringBuffer stringBuffer = new StringBuffer(scale.toString());
        String dimensionListMath = DimensionListMath.toString(unit.getDimensionList(), dimensionManager);
        if (dimensionListMath.length() > 0) {
            stringBuffer.append(" " + dimensionListMath);
        }
        return new String(stringBuffer);
    }

    public static void dumpDimensionLists(UnitManager unitManager, DimensionManager dimensionManager) {
        Enumeration<String> names = unitManager.getNames();
        while (names.hasMoreElements()) {
            String nextElement = names.nextElement();
            System.out.println(DimensionListMath.toString(unitManager.getUnit(nextElement).getDimensionList(), dimensionManager) + "\t" + nextElement);
        }
    }

    public static boolean areConformal(Unit unit, Unit unit2) {
        return DimensionListMath.equals(unit.getDimensionList(), unit2.getDimensionList());
    }

    public static int compare(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        if (areConformal(unit, unit2)) {
            return NumericMath.compare(unit.getScale(), unit2.getScale());
        }
        throw new ConformanceException("Cannot compare units with different dimensions.", unit.getDimensionList(), unit2.getDimensionList());
    }

    public static Unit min(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        if (compare(unit, unit2) < 0) {
            return unit;
        }
        return unit2;
    }

    public static Unit max(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        if (compare(unit, unit2) > 0) {
            return unit;
        }
        return unit2;
    }

    public static boolean isPositive(Unit unit) throws NumericException, OverlapException {
        try {
            if (NumericMath.compare(unit.getScale(), FrinkInt.ZERO) > 0) {
                return true;
            }
            return false;
        } catch (NotRealException e) {
            return false;
        }
    }

    public static boolean isNegative(Unit unit) throws NumericException, OverlapException {
        try {
            if (NumericMath.compare(unit.getScale(), FrinkInt.ZERO) < 0) {
                return true;
            }
            return false;
        } catch (NotRealException e) {
            return false;
        }
    }

    public static boolean isRational(Unit unit) {
        return unit.getScale().isRational() && isDimensionless(unit);
    }

    public static boolean isDimensionless(Unit unit) {
        return DimensionListMath.isDimensionless(unit.getDimensionList());
    }

    public static Unit floor(Unit unit) throws ConformanceException, NumericException {
        if (isDimensionless(unit)) {
            return new BasicUnit(NumericMath.floor(unit.getScale()), unit.getDimensionList());
        }
        throw new ConformanceException("Argument to floor must be dimensionless.");
    }

    public static Unit ceil(Unit unit) throws ConformanceException, NumericException {
        if (isDimensionless(unit)) {
            return new BasicUnit(NumericMath.ceil(unit.getScale()), unit.getDimensionList());
        }
        throw new ConformanceException("Argument to ceil must be dimensionless.");
    }

    public static Unit round(Unit unit) throws ConformanceException, NumericException {
        if (isDimensionless(unit)) {
            return new BasicUnit(NumericMath.round(unit.getScale()), unit.getDimensionList());
        }
        throw new ConformanceException("Argument to ceil must be dimensionless.");
    }

    public static Unit round(Unit unit, Unit unit2) throws ConformanceException, NumericException {
        return multiply(round(divide(unit, unit2)), unit2);
    }

    public static Unit truncate(Unit unit) throws ConformanceException, NumericException {
        if (isDimensionless(unit)) {
            return new BasicUnit(NumericMath.truncate(unit.getScale()), unit.getDimensionList());
        }
        throw new ConformanceException("Argument to ceil must be dimensionless.");
    }

    public static long getLongValue(Unit unit) throws NotAnIntegerException {
        if (isDimensionless(unit)) {
            return NumericMath.getLongValue(unit.getScale());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkInteger getFrinkIntegerValue(Unit unit) throws NotAnIntegerException {
        if (isDimensionless(unit)) {
            return NumericMath.getFrinkIntegerValue(unit.getScale());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static int getIntegerValue(Unit unit) throws NotAnIntegerException {
        if (isDimensionless(unit)) {
            return NumericMath.getIntegerValue(unit.getScale());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkRational getRationalValue(Unit unit) throws NotAnIntegerException {
        if (isRational(unit)) {
            return (FrinkRational) unit.getScale();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static int getNearestIntValue(Unit unit) throws NotAnIntegerException, NumericException {
        if (isDimensionless(unit)) {
            return NumericMath.getIntegerValue(NumericMath.round(unit.getScale()));
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static short getShortValue(Unit unit) throws NotAnIntegerException {
        if (isDimensionless(unit)) {
            return NumericMath.getShortValue(unit.getScale());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static byte getByteValue(Unit unit) throws NotAnIntegerException {
        if (isDimensionless(unit)) {
            return NumericMath.getByteValue(unit.getScale());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static BigInteger getBigIntegerValue(Unit unit) throws NotAnIntegerException {
        if (isDimensionless(unit)) {
            return NumericMath.getBigIntegerValue(unit.getScale());
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkReal getFrinkRealValue(Unit unit) throws NotRealException {
        if (isDimensionless(unit)) {
            return NumericMath.getFrinkRealValue(unit.getScale());
        }
        throw NotRealException.INSTANCE;
    }

    public static double getDoubleValue(Unit unit) throws NotRealException {
        if (isDimensionless(unit)) {
            return NumericMath.getFrinkRealValue(unit.getScale()).doubleValue();
        }
        throw NotRealException.INSTANCE;
    }

    public static float getFloatValue(Unit unit) throws NotRealException {
        if (isDimensionless(unit)) {
            return (float) NumericMath.getFrinkRealValue(unit.getScale()).doubleValue();
        }
        throw NotRealException.INSTANCE;
    }

    public static Numeric getNumericValue(Unit unit) throws NotRealException {
        if (isDimensionless(unit)) {
            return unit.getScale();
        }
        throw NotRealException.INSTANCE;
    }

    public static Unit abs(Unit unit) throws NumericException {
        Numeric scale = unit.getScale();
        Numeric abs = NumericMath.abs(unit.getScale());
        if (abs == scale) {
            return unit;
        }
        return new BasicUnit(abs, unit.getDimensionList());
    }

    public static Unit magnitude(Unit unit) throws NumericException {
        Numeric scale = unit.getScale();
        Numeric magnitude = NumericMath.magnitude(unit.getScale());
        if (magnitude == scale) {
            return unit;
        }
        return new BasicUnit(magnitude, unit.getDimensionList());
    }

    public static Unit mignitude(Unit unit) throws NumericException {
        Numeric scale = unit.getScale();
        Numeric mignitude = NumericMath.mignitude(unit.getScale());
        if (mignitude == scale) {
            return unit;
        }
        return new BasicUnit(mignitude, unit.getDimensionList());
    }

    public static Unit halve(Unit unit) {
        try {
            return divide(unit, DimensionlessUnit.TWO);
        } catch (NumericException e) {
            System.err.println("Unexpected error in UnitMath.halve:\n  " + e);
            return null;
        }
    }

    public static Unit infimum(Unit unit) {
        Numeric scale = unit.getScale();
        Numeric infimum = NumericMath.infimum(scale);
        if (infimum == scale) {
            return unit;
        }
        return new BasicUnit(infimum, unit.getDimensionList());
    }

    public static Unit supremum(Unit unit) {
        Numeric scale = unit.getScale();
        Numeric supremum = NumericMath.supremum(scale);
        if (supremum == scale) {
            return unit;
        }
        return new BasicUnit(supremum, unit.getDimensionList());
    }

    public static Unit main(Unit unit) {
        Numeric scale = unit.getScale();
        Numeric main = NumericMath.main(scale);
        if (main == null) {
            return null;
        }
        if (main == scale) {
            return unit;
        }
        return new BasicUnit(main, unit.getDimensionList());
    }

    public static Unit realPart(Unit unit) throws NumericException {
        return BasicUnit.construct(NumericMath.realPart(unit.getScale()), unit.getDimensionList());
    }

    public static Unit imaginaryPart(Unit unit) throws NumericException {
        return BasicUnit.construct(NumericMath.imaginaryPart(unit.getScale()), unit.getDimensionList());
    }

    public static Unit signum(Unit unit) throws NumericException {
        return BasicUnit.construct(NumericMath.signum(unit.getScale()), unit.getDimensionList());
    }
}
