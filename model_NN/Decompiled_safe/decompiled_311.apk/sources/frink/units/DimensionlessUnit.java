package frink.units;

import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.Numeric;

public class DimensionlessUnit implements Unit {
    public static final DimensionlessUnit NEGATIVE_ONE = intCache[99];
    public static final DimensionlessUnit ONE = intCache[101];
    public static final DimensionlessUnit ONE_HALF = new DimensionlessUnit(FrinkRational.ONE_HALF);
    public static final DimensionlessUnit THREE = intCache[103];
    public static final DimensionlessUnit TWO = intCache[102];
    public static final DimensionlessUnit ZERO = intCache[100];
    private static final DimensionlessUnit[] intCache = new DimensionlessUnit[301];
    private Numeric scale;

    static {
        for (int i = -100; i <= 200; i++) {
            intCache[i - -100] = new DimensionlessUnit((long) i);
        }
    }

    public static DimensionlessUnit construct(int i) {
        if (i > 200 || i < -100) {
            return new DimensionlessUnit((long) i);
        }
        return intCache[i - -100];
    }

    public static DimensionlessUnit construct(Numeric numeric) {
        if (numeric.isInt()) {
            return construct(((FrinkInt) numeric).getInt());
        }
        return new DimensionlessUnit(numeric);
    }

    private DimensionlessUnit(Numeric numeric) {
        this.scale = numeric;
    }

    private DimensionlessUnit(long j) {
        this.scale = FrinkInteger.construct(j);
    }

    private DimensionlessUnit(double d) {
        this.scale = new FrinkFloat(d);
    }

    public static DimensionlessUnit construct(double d) {
        return new DimensionlessUnit(d);
    }

    public final Numeric getScale() {
        return this.scale;
    }

    public final DimensionList getDimensionList() {
        return DimensionlessList.INSTANCE;
    }
}
