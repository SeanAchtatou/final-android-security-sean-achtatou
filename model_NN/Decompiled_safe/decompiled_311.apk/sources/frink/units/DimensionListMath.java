package frink.units;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.Numeric;

public class DimensionListMath {
    public static DimensionList multiply(DimensionList dimensionList, DimensionList dimensionList2) {
        int highestIndex = dimensionList.getHighestIndex();
        if (highestIndex < 0) {
            return dimensionList2;
        }
        int highestIndex2 = dimensionList2.getHighestIndex();
        if (highestIndex2 < 0) {
            return dimensionList;
        }
        if (dimensionList.hasFractionalExponents() || dimensionList2.hasFractionalExponents()) {
            return multiplyFractional(dimensionList, dimensionList2);
        }
        BasicDimensionList basicDimensionList = null;
        for (int max = Math.max(highestIndex, highestIndex2); max >= 0; max--) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(max) + dimensionList2.getExponentNumeratorByIndex(max);
            if (exponentNumeratorByIndex != 0) {
                if (basicDimensionList == null) {
                    basicDimensionList = new BasicDimensionList(max);
                }
                basicDimensionList.setIndexAt(max, exponentNumeratorByIndex);
            }
        }
        if (basicDimensionList != null) {
            return basicDimensionList;
        }
        return DimensionlessList.INSTANCE;
    }

    private static DimensionList multiplyFractional(DimensionList dimensionList, DimensionList dimensionList2) {
        int i;
        int i2;
        int[] iArr = null;
        int[] iArr2 = null;
        for (int max = Math.max(dimensionList.getHighestIndex(), dimensionList2.getHighestIndex()); max >= 0; max--) {
            int exponentDenominatorByIndex = dimensionList.getExponentDenominatorByIndex(max);
            int exponentDenominatorByIndex2 = dimensionList2.getExponentDenominatorByIndex(max);
            if (exponentDenominatorByIndex == 1 && exponentDenominatorByIndex2 == 1) {
                i = dimensionList.getExponentNumeratorByIndex(max) + dimensionList2.getExponentNumeratorByIndex(max);
                i2 = 1;
            } else {
                int exponentNumeratorByIndex = (dimensionList.getExponentNumeratorByIndex(max) * exponentDenominatorByIndex2) + (dimensionList2.getExponentNumeratorByIndex(max) * exponentDenominatorByIndex);
                int i3 = exponentDenominatorByIndex * exponentDenominatorByIndex2;
                int gcd = FrinkInteger.gcd(exponentNumeratorByIndex, i3);
                i2 = i3 / gcd;
                i = exponentNumeratorByIndex / gcd;
            }
            if (i != 0) {
                if (iArr == null) {
                    iArr = new int[(max + 1)];
                }
                iArr[max] = i;
                if (i2 != 1) {
                    if (iArr2 == null) {
                        iArr2 = new int[(max + 1)];
                        for (int i4 = 0; i4 < max; i4++) {
                            iArr2[i4] = 1;
                        }
                    }
                    iArr2[max] = i2;
                }
            }
        }
        if (iArr == null) {
            return DimensionlessList.INSTANCE;
        }
        if (iArr2 == null) {
            return new BasicDimensionList(iArr);
        }
        return new FractionalDimensionList(iArr, iArr2);
    }

    public static DimensionList divide(DimensionList dimensionList, DimensionList dimensionList2) {
        if (dimensionList == dimensionList2) {
            return DimensionlessList.INSTANCE;
        }
        int highestIndex = dimensionList2.getHighestIndex();
        if (highestIndex < 0) {
            return dimensionList;
        }
        int highestIndex2 = dimensionList.getHighestIndex();
        if (highestIndex2 < 0) {
            return power(dimensionList2, -1);
        }
        if (dimensionList.hasFractionalExponents() || dimensionList2.hasFractionalExponents()) {
            return divideFractional(dimensionList, dimensionList2);
        }
        BasicDimensionList basicDimensionList = null;
        for (int max = Math.max(highestIndex2, highestIndex); max >= 0; max--) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(max) - dimensionList2.getExponentNumeratorByIndex(max);
            if (exponentNumeratorByIndex != 0) {
                if (basicDimensionList == null) {
                    basicDimensionList = new BasicDimensionList(max);
                }
                basicDimensionList.setIndexAt(max, exponentNumeratorByIndex);
            }
        }
        if (basicDimensionList != null) {
            return basicDimensionList;
        }
        return DimensionlessList.INSTANCE;
    }

    private static DimensionList divideFractional(DimensionList dimensionList, DimensionList dimensionList2) {
        int i;
        int i2;
        int[] iArr = null;
        int highestIndex = dimensionList.getHighestIndex();
        if (highestIndex < 0) {
            return dimensionList2;
        }
        int highestIndex2 = dimensionList2.getHighestIndex();
        if (highestIndex2 < 0) {
            return dimensionList;
        }
        int[] iArr2 = null;
        for (int max = Math.max(highestIndex, highestIndex2); max >= 0; max--) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(max);
            int exponentNumeratorByIndex2 = dimensionList2.getExponentNumeratorByIndex(max);
            int exponentDenominatorByIndex = dimensionList.getExponentDenominatorByIndex(max);
            int exponentDenominatorByIndex2 = dimensionList2.getExponentDenominatorByIndex(max);
            if (exponentDenominatorByIndex == 1 && exponentDenominatorByIndex2 == 1) {
                i = dimensionList.getExponentNumeratorByIndex(max) - dimensionList2.getExponentNumeratorByIndex(max);
                i2 = 1;
            } else {
                int i3 = (exponentNumeratorByIndex * exponentDenominatorByIndex2) - (exponentNumeratorByIndex2 * exponentDenominatorByIndex);
                int i4 = exponentDenominatorByIndex * exponentDenominatorByIndex2;
                int gcd = FrinkInteger.gcd(i3, i4);
                int i5 = i4 / gcd;
                i = i3 / gcd;
                i2 = i5;
            }
            if (i != 0) {
                if (iArr == null) {
                    iArr = new int[(max + 1)];
                }
                iArr[max] = i;
                if (i2 != 1) {
                    if (iArr2 == null) {
                        iArr2 = new int[(max + 1)];
                        for (int i6 = 0; i6 < max; i6++) {
                            iArr2[i6] = 1;
                        }
                    }
                    iArr2[max] = i2;
                }
            }
        }
        if (iArr == null) {
            return DimensionlessList.INSTANCE;
        }
        if (iArr2 == null) {
            return new BasicDimensionList(iArr);
        }
        return new FractionalDimensionList(iArr, iArr2);
    }

    public static DimensionList power(DimensionList dimensionList, int i) {
        if (i == 1) {
            return dimensionList;
        }
        if (i == 0) {
            return DimensionlessList.INSTANCE;
        }
        if (dimensionList.hasFractionalExponents()) {
            return power(dimensionList, i, 1);
        }
        int highestIndex = dimensionList.getHighestIndex();
        if (highestIndex < 0) {
            return dimensionList;
        }
        int i2 = highestIndex;
        BasicDimensionList basicDimensionList = null;
        for (int i3 = i2; i3 >= 0; i3--) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(i3);
            if (exponentNumeratorByIndex != 0) {
                if (basicDimensionList == null) {
                    basicDimensionList = new BasicDimensionList(i3);
                }
                basicDimensionList.setIndexAt(i3, exponentNumeratorByIndex * i);
            }
        }
        if (basicDimensionList != null) {
            return basicDimensionList;
        }
        return DimensionlessList.INSTANCE;
    }

    public static DimensionList power(DimensionList dimensionList, Numeric numeric) throws ConformanceException {
        if (dimensionList.getHighestIndex() < 0) {
            return dimensionList;
        }
        if (numeric.isInt()) {
            return power(dimensionList, ((FrinkInt) numeric).getInt());
        }
        if (numeric.isRational()) {
            try {
                return power(dimensionList, ((FrinkRational) numeric).getNumerator().getInt(), ((FrinkRational) numeric).getDenominator().getInt());
            } catch (NotAnIntegerException e) {
                throw new ConformanceException("Numerator or denominator was not a reasonably-sized integer when raising to power " + numeric);
            }
        } else {
            throw new ConformanceException("Could not raise units to power " + numeric + ".  Only integers and rational numbers are allowed in exponents of units.");
        }
    }

    public static DimensionList power(DimensionList dimensionList, int i, int i2) {
        int[] iArr = null;
        int highestIndex = dimensionList.getHighestIndex();
        if (highestIndex < 0) {
            return dimensionList;
        }
        int[] iArr2 = null;
        while (highestIndex >= 0) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(highestIndex);
            if (exponentNumeratorByIndex != 0) {
                int i3 = exponentNumeratorByIndex * i;
                if (iArr2 == null) {
                    iArr2 = new int[(highestIndex + 1)];
                }
                int exponentDenominatorByIndex = dimensionList.getExponentDenominatorByIndex(highestIndex) * i2;
                if (exponentDenominatorByIndex != 1) {
                    int gcd = FrinkInteger.gcd(i3, exponentDenominatorByIndex);
                    i3 /= gcd;
                    int i4 = exponentDenominatorByIndex / gcd;
                    if (i4 != 1) {
                        if (iArr == null) {
                            iArr = new int[(highestIndex + 1)];
                            for (int i5 = 0; i5 < highestIndex; i5++) {
                                iArr[i5] = 1;
                            }
                        }
                        iArr[highestIndex] = i4;
                    }
                }
                iArr2[highestIndex] = i3;
                iArr = iArr;
            }
            highestIndex--;
        }
        if (iArr2 == null) {
            return DimensionlessList.INSTANCE;
        }
        if (iArr == null) {
            return new BasicDimensionList(iArr2);
        }
        return new FractionalDimensionList(iArr2, iArr);
    }

    public static boolean equals(DimensionList dimensionList, DimensionList dimensionList2) {
        if (dimensionList == dimensionList2) {
            return true;
        }
        int max = Math.max(dimensionList.getHighestIndex(), dimensionList2.getHighestIndex());
        if (dimensionList.hasFractionalExponents() || dimensionList2.hasFractionalExponents()) {
            for (int i = 0; i <= max; i++) {
                if (dimensionList.getExponentNumeratorByIndex(i) != dimensionList2.getExponentNumeratorByIndex(i) || dimensionList.getExponentDenominatorByIndex(i) != dimensionList2.getExponentDenominatorByIndex(i)) {
                    return false;
                }
            }
            return true;
        }
        for (int i2 = 0; i2 <= max; i2++) {
            if (dimensionList.getExponentNumeratorByIndex(i2) != dimensionList2.getExponentNumeratorByIndex(i2)) {
                return false;
            }
        }
        return true;
    }

    public static String toDimensionString(DimensionList dimensionList, DimensionManager dimensionManager) {
        int highestIndex = dimensionList.getHighestIndex();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i <= highestIndex; i++) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(i);
            if (exponentNumeratorByIndex != 0) {
                int exponentDenominatorByIndex = dimensionList.getExponentDenominatorByIndex(i);
                Dimension dimensionByIndex = dimensionManager.getDimensionByIndex(i);
                if (dimensionByIndex != null) {
                    if (stringBuffer.length() > 0) {
                        stringBuffer.append(" ");
                    }
                    stringBuffer.append(dimensionByIndex.getName());
                    if (exponentNumeratorByIndex != 1 || exponentDenominatorByIndex != 1) {
                        if (exponentDenominatorByIndex != 1) {
                            stringBuffer.append("^(" + exponentNumeratorByIndex + "/" + exponentDenominatorByIndex + ")");
                        } else {
                            stringBuffer.append("^" + exponentNumeratorByIndex);
                        }
                    }
                }
            }
        }
        return new String(stringBuffer);
    }

    public static String toString(DimensionList dimensionList, DimensionManager dimensionManager) {
        Dimension dimensionByIndex;
        int highestIndex = dimensionList.getHighestIndex();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i <= highestIndex; i++) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(i);
            if (!(exponentNumeratorByIndex == 0 || (dimensionByIndex = dimensionManager.getDimensionByIndex(i)) == null)) {
                if (stringBuffer.length() > 0) {
                    stringBuffer.append(" ");
                }
                stringBuffer.append(dimensionByIndex.getFundamentalUnitName());
                int exponentDenominatorByIndex = dimensionList.getExponentDenominatorByIndex(i);
                if (exponentNumeratorByIndex != 1 || exponentDenominatorByIndex != 1) {
                    if (exponentDenominatorByIndex != 1) {
                        stringBuffer.append("^(" + exponentNumeratorByIndex + "/" + exponentDenominatorByIndex + ")");
                    } else {
                        stringBuffer.append("^" + exponentNumeratorByIndex);
                    }
                }
            }
        }
        return new String(stringBuffer);
    }

    public static int compare(DimensionList dimensionList, DimensionList dimensionList2) {
        int max = Math.max(dimensionList.getHighestIndex(), dimensionList2.getHighestIndex());
        for (int i = 0; i <= max; i++) {
            int exponentNumeratorByIndex = dimensionList.getExponentNumeratorByIndex(i);
            int exponentNumeratorByIndex2 = dimensionList2.getExponentNumeratorByIndex(i);
            int exponentDenominatorByIndex = dimensionList.getExponentDenominatorByIndex(i);
            int exponentDenominatorByIndex2 = exponentNumeratorByIndex * dimensionList2.getExponentDenominatorByIndex(i);
            int i2 = exponentNumeratorByIndex2 * exponentDenominatorByIndex;
            if (exponentDenominatorByIndex2 < i2) {
                return -1;
            }
            if (exponentDenominatorByIndex2 > i2) {
                return 1;
            }
        }
        return 0;
    }

    public static boolean isDimensionless(DimensionList dimensionList) {
        return equals(dimensionList, DimensionlessList.INSTANCE);
    }
}
