package frink.units;

import java.util.Enumeration;
import java.util.Vector;

public class BasicManager<T> implements Manager<T> {
    private Vector<Source<T>> sources = new Vector<>();

    public T get(String str) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                T t = this.sources.elementAt(i).get(str);
                if (t != null) {
                    return t;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                return null;
            }
        }
        return null;
    }

    public Enumeration<String> getNames() {
        return new BasicObjectEnumeration(this);
    }

    /* access modifiers changed from: package-private */
    public int getSourceCount() {
        return this.sources.size();
    }

    /* access modifiers changed from: package-private */
    public Source<T> getSourceAtIndex(int i) {
        return this.sources.elementAt(i);
    }

    public void addSource(Source<T> source) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                if (!this.sources.elementAt(i).getName().equals(source.getName())) {
                    i++;
                } else {
                    return;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
        this.sources.addElement(source);
    }

    public void removeSource(String str) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                if (this.sources.elementAt(i).getName().equals(str)) {
                    this.sources.removeElementAt(i);
                    return;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
    }

    public Source<T> getSource(String str) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                Source<T> elementAt = this.sources.elementAt(i);
                if (elementAt.getName().equals(str)) {
                    return elementAt;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
        return null;
    }
}
