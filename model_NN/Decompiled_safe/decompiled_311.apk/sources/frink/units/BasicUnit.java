package frink.units;

import frink.numeric.Numeric;

public class BasicUnit implements Unit {
    private DimensionList dimensions;
    private Numeric scale;

    public BasicUnit(Numeric numeric, DimensionList dimensionList) {
        this.scale = numeric;
        this.dimensions = dimensionList;
    }

    public static Unit construct(Numeric numeric, DimensionList dimensionList) {
        if (dimensionList.getHighestIndex() == -1) {
            return DimensionlessUnit.construct(numeric);
        }
        return new BasicUnit(numeric, dimensionList);
    }

    public Numeric getScale() {
        return this.scale;
    }

    public DimensionList getDimensionList() {
        return this.dimensions;
    }
}
