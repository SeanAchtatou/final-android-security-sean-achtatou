package frink.units;

public final class DimensionlessList implements DimensionList {
    public static final DimensionlessList INSTANCE = new DimensionlessList();

    private DimensionlessList() {
    }

    public final int getExponentNumeratorByIndex(int i) {
        return 0;
    }

    public final int getExponentDenominatorByIndex(int i) {
        return 1;
    }

    public final int getHighestIndex() {
        return -1;
    }

    public boolean hasFractionalExponents() {
        return false;
    }
}
