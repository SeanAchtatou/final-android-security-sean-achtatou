package frink.units;

public class BasicDimensionList implements DimensionList {
    private int[] dimensions;

    public BasicDimensionList(int i) {
        if (i >= 0) {
            this.dimensions = new int[(i + 1)];
        } else {
            this.dimensions = null;
        }
    }

    public BasicDimensionList(int[] iArr) {
        this.dimensions = iArr;
    }

    public int getExponentNumeratorByIndex(int i) {
        if (this.dimensions == null || i >= this.dimensions.length) {
            return 0;
        }
        return this.dimensions[i];
    }

    public int getExponentDenominatorByIndex(int i) {
        return 1;
    }

    public int getHighestIndex() {
        if (this.dimensions == null) {
            return -1;
        }
        return this.dimensions.length - 1;
    }

    public void setIndexAt(int i, int i2) {
        if (this.dimensions != null && i < this.dimensions.length) {
            this.dimensions[i] = i2;
        }
    }

    public boolean hasFractionalExponents() {
        return false;
    }
}
