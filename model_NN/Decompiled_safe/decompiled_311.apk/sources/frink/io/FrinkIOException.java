package frink.io;

import frink.expr.EvaluationException;
import frink.expr.Expression;

public class FrinkIOException extends EvaluationException {
    public FrinkIOException(String str, Expression expression) {
        super(str, expression);
    }
}
