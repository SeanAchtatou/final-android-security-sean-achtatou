package frink.io;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BrowserHelper {
    private static final BrowserHelper INSTANCE = new BrowserHelper();
    private Class desktopClass;
    private boolean hasDesktopFlag;

    private BrowserHelper() {
        this.hasDesktopFlag = false;
        this.desktopClass = null;
        this.hasDesktopFlag = false;
        try {
            this.desktopClass = Class.forName("java.awt.Desktop");
            if (this.desktopClass.getMethod("isDesktopSupported", null).invoke(null, null).equals(Boolean.TRUE)) {
                this.hasDesktopFlag = true;
            }
        } catch (Throwable th) {
        }
    }

    public static boolean canOpenURLs() {
        return INSTANCE.hasDesktopFlag;
    }

    public static void openURL(String str) {
        if (INSTANCE.hasDesktopFlag && INSTANCE.desktopClass != null) {
            try {
                Object invoke = INSTANCE.desktopClass.getMethod("getDesktop", null).invoke(null, null);
                Class<?> cls = Class.forName("java.net.URI");
                Class[] clsArr = {String.class};
                Object[] objArr = {str};
                Object newInstance = cls.getConstructor(clsArr).newInstance(objArr);
                clsArr[0] = cls;
                Method method = INSTANCE.desktopClass.getMethod("browse", clsArr);
                objArr[0] = newInstance;
                method.invoke(invoke, objArr);
            } catch (InvocationTargetException e) {
                System.err.println("Error in opening URL: " + e);
                System.err.println("Original cause: " + e.getTargetException());
            } catch (Exception e2) {
                System.err.println("Error in opening URL: " + e2);
            }
        }
    }

    public static void main(String[] strArr) {
        openURL("http://futureboy.us/frinkdocs/");
        try {
            System.out.println("Sleeping...");
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println("Interrupted.");
        }
        System.out.println("Done sleeping.");
    }
}
