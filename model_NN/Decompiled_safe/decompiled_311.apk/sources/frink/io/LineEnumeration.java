package frink.io;

import frink.expr.BasicStringExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.FrinkSecurityException;
import frink.expr.InvalidArgumentException;
import frink.expr.NonTerminalExpression;
import frink.expr.StringExpression;
import frink.symbolic.MatchingContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class LineEnumeration extends NonTerminalExpression implements EnumeratingExpression {
    public static final String TYPE = "LineEnumeration";
    private URL url;

    LineEnumeration(Expression expression, Environment environment) throws InvalidArgumentException, FrinkSecurityException {
        super(1);
        if (expression instanceof StringExpression) {
            String string = ((StringExpression) expression).getString();
            try {
                this.url = new URL(string);
                environment.getSecurityHelper().checkRead(this.url);
                appendChild(expression);
            } catch (MalformedURLException e) {
                throw new InvalidArgumentException("LineEnumeration:  Malformed URL " + string, this);
            }
        } else {
            throw new InvalidArgumentException("Argument to LineEnumeration constructor must be a string that evaluates to a URL.", this);
        }
    }

    LineEnumeration(Expression expression, Expression expression2, Environment environment) throws InvalidArgumentException, FrinkSecurityException {
        super(2);
        if (expression instanceof StringExpression) {
            try {
                this.url = new URL(((StringExpression) expression).getString());
                environment.getSecurityHelper().checkRead(this.url);
                appendChild(expression);
                appendChild(expression2);
            } catch (MalformedURLException e) {
                throw new InvalidArgumentException("LineEnumeration:  Malformed URL " + this.url, this);
            }
        } else {
            throw new InvalidArgumentException("Argument to LineEnumeration constructor must be a string that evaluates to a URL.", this);
        }
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
        String str;
        if (getChildCount() >= 2) {
            Expression child = getChild(1);
            if (!(child instanceof StringExpression)) {
                throw new InvalidArgumentException("Second argument to LineEnumeration must be a string representing an encoding.", this);
            }
            str = ((StringExpression) child).getString();
        } else {
            str = null;
        }
        environment.getSecurityHelper().checkRead(this.url);
        return new LineEnumerator(this, this.url, str, environment);
    }

    private static class LineEnumerator implements FrinkEnumeration {
        private URLConnection conn;
        private BufferedReader inputReader;
        private Expression parent;

        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0081, code lost:
            java.lang.System.err.println("Could not open " + r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x009b, code lost:
            r0 = r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0080 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x000c] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private LineEnumerator(frink.expr.Expression r5, java.net.URL r6, java.lang.String r7, frink.expr.Environment r8) throws frink.io.FrinkIOException, frink.expr.FrinkSecurityException {
            /*
                r4 = this;
                r4.<init>()
                r4.parent = r5
                frink.security.SecurityHelper r0 = r8.getSecurityHelper()
                r0.checkRead(r6)
                java.net.URLConnection r0 = r6.openConnection()     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                r4.conn = r0     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                if (r7 != 0) goto L_0x009d
                java.net.URLConnection r0 = r4.conn     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                java.lang.String r1 = "Content-Type"
                java.lang.String r0 = r0.getHeaderField(r1)     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                if (r0 == 0) goto L_0x009d
                java.lang.String r1 = "charset="
                int r1 = r0.indexOf(r1)     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                r2 = -1
                if (r1 == r2) goto L_0x009d
                int r1 = r1 + 8
                java.lang.String r0 = r0.substring(r1)     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                java.lang.String r0 = r0.trim()     // Catch:{ UnsupportedEncodingException -> 0x009a, IOException -> 0x0080 }
                int r1 = r0.length()     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                if (r1 != 0) goto L_0x0038
                r0 = 0
            L_0x0038:
                if (r0 != 0) goto L_0x0050
                java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                java.net.URLConnection r2 = r4.conn     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                java.io.InputStream r2 = r2.getInputStream()     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
            L_0x0045:
                java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                r3 = 32768(0x8000, float:4.5918E-41)
                r2.<init>(r1, r3)     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                r4.inputReader = r2     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
            L_0x004f:
                return
            L_0x0050:
                java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                java.net.URLConnection r2 = r4.conn     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                java.io.InputStream r2 = r2.getInputStream()     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                r1.<init>(r2, r0)     // Catch:{ UnsupportedEncodingException -> 0x005c, IOException -> 0x0080 }
                goto L_0x0045
            L_0x005c:
                r1 = move-exception
            L_0x005d:
                java.io.PrintStream r1 = java.lang.System.err
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "File encoding \""
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.StringBuilder r0 = r2.append(r0)
                java.lang.String r2 = "\"is not supported when reading "
                java.lang.StringBuilder r0 = r0.append(r2)
                java.lang.StringBuilder r0 = r0.append(r6)
                java.lang.String r0 = r0.toString()
                r1.println(r0)
                goto L_0x004f
            L_0x0080:
                r0 = move-exception
                java.io.PrintStream r0 = java.lang.System.err
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Could not open "
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.StringBuilder r1 = r1.append(r6)
                java.lang.String r1 = r1.toString()
                r0.println(r1)
                goto L_0x004f
            L_0x009a:
                r0 = move-exception
                r0 = r7
                goto L_0x005d
            L_0x009d:
                r0 = r7
                goto L_0x0038
            */
            throw new UnsupportedOperationException("Method not decompiled: frink.io.LineEnumeration.LineEnumerator.<init>(frink.expr.Expression, java.net.URL, java.lang.String, frink.expr.Environment):void");
        }

        public Expression getNext(Environment environment) throws FrinkIOException {
            if (this.inputReader == null) {
                return null;
            }
            try {
                String readLine = this.inputReader.readLine();
                if (readLine != null) {
                    return new BasicStringExpression(readLine);
                }
                this.inputReader.close();
                this.inputReader = null;
                return null;
            } catch (IOException e) {
                throw new FrinkIOException("LineEnumeration: error in read: " + e.toString(), this.parent);
            }
        }

        public void dispose() {
            try {
                if (this.inputReader != null) {
                    this.inputReader.close();
                    this.inputReader = null;
                }
            } catch (IOException e) {
            }
            this.conn = null;
            this.parent = null;
        }
    }

    public String getExpressionType() {
        return TYPE;
    }
}
