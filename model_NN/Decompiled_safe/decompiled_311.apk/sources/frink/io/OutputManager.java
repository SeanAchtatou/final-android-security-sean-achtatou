package frink.io;

import java.io.OutputStream;

public interface OutputManager {
    OutputStream getRawOutputStream();

    void output(String str);

    void outputln(String str);
}
