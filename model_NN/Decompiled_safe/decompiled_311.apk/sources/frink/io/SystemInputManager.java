package frink.io;

import frink.expr.Environment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInputManager implements InputManager {
    public static final SystemInputManager INSTANCE = new SystemInputManager();
    private static BufferedReader reader = null;

    private SystemInputManager() {
    }

    public String input(String str, String str2, Environment environment) {
        if (str != null && str.length() > 0) {
            environment.output(str + " ");
        }
        if (str2 != null && str2.length() > 0) {
            environment.output("[" + str2 + "] ");
        }
        String line = getLine();
        if (line == null) {
            return null;
        }
        if (line.length() == 0) {
            return str2 == null ? "" : str2;
        }
        return line;
    }

    public String[] input(String str, InputItem[] inputItemArr, Environment environment) {
        if (str != null && str.length() > 0) {
            environment.outputln(str);
        }
        int length = inputItemArr.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            InputItem inputItem = inputItemArr[i];
            String input = input(inputItem.label, inputItem.defaultValue, environment);
            strArr[i] = input;
            if (input == null) {
                return strArr;
            }
        }
        return strArr;
    }

    private static synchronized String getLine() {
        String str;
        synchronized (SystemInputManager.class) {
            if (reader == null) {
                reader = new BufferedReader(new InputStreamReader(System.in));
            }
            try {
                str = reader.readLine();
            } catch (IOException e) {
                System.out.println("Error in reading from std input:  " + e);
                str = null;
            }
        }
        return str;
    }
}
