package frink.io;

import frink.expr.Environment;

public interface InputManager {
    String input(String str, String str2, Environment environment);

    String[] input(String str, InputItem[] inputItemArr, Environment environment);
}
