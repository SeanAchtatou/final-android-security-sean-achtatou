package frink.io;

import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;

public class InputItem {
    public String defaultValue = null;
    public String label = null;

    public InputItem(Expression expression, Environment environment) {
        if (expression instanceof ListExpression) {
            ListExpression listExpression = (ListExpression) expression;
            int childCount = listExpression.getChildCount();
            if (childCount > 0) {
                try {
                    this.label = environment.format(listExpression.getChild(0));
                } catch (InvalidChildException e) {
                    return;
                }
            }
            if (childCount > 1) {
                this.defaultValue = environment.format(listExpression.getChild(1));
                return;
            }
            return;
        }
        this.label = environment.format(expression);
    }
}
