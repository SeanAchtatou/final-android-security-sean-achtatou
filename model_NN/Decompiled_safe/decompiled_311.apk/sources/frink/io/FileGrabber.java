package frink.io;

public class FileGrabber {
    private static final int BLOCK_SIZE = 32768;

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0065, code lost:
        java.lang.System.err.println("Malformed URL: " + r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0095, code lost:
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0096, code lost:
        java.lang.System.err.println("File encoding \"" + r0 + "\"is not supported when reading " + r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d9, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0064 A[ExcHandler: MalformedURLException (e java.net.MalformedURLException), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0094 A[ExcHandler: UnsupportedEncodingException (e java.io.UnsupportedEncodingException), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d6 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:4:0x0009] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String read(java.lang.String r9, java.lang.String r10, frink.expr.Environment r11) throws frink.expr.FrinkSecurityException {
        /*
            r8 = -1
            r7 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00ba }
            r0.<init>(r9)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00ba }
            if (r11 == 0) goto L_0x0010
            frink.security.SecurityHelper r1 = r11.getSecurityHelper()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
            r1.checkRead(r0)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
        L_0x0010:
            java.net.URLConnection r1 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
            if (r10 != 0) goto L_0x00db
            java.lang.String r2 = "Content-Type"
            java.lang.String r2 = r1.getHeaderField(r2)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
            if (r2 == 0) goto L_0x00db
            java.lang.String r3 = "charset="
            int r3 = r2.indexOf(r3)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
            if (r3 == r8) goto L_0x00db
            int r3 = r3 + 8
            java.lang.String r2 = r2.substring(r3)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
            java.lang.String r2 = r2.trim()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x0094, IOException -> 0x00d6 }
            int r3 = r2.length()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            if (r3 != 0) goto L_0x0037
            r2 = r7
        L_0x0037:
            if (r2 != 0) goto L_0x007f
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r3.<init>(r1)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r1 = r3
        L_0x0043:
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r4 = 32768(0x8000, float:4.5918E-41)
            r3.<init>(r1, r4)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r1 = 32768(0x8000, float:4.5918E-41)
            char[] r1 = new char[r1]     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
        L_0x0055:
            r5 = 0
            r6 = 32768(0x8000, float:4.5918E-41)
            int r5 = r3.read(r1, r5, r6)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            if (r5 == r8) goto L_0x008a
            r6 = 0
            r4.append(r1, r6, r5)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            goto L_0x0055
        L_0x0064:
            r0 = move-exception
            java.io.PrintStream r0 = java.lang.System.err
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Malformed URL: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r1 = r1.toString()
            r0.println(r1)
            r0 = r7
        L_0x007e:
            return r0
        L_0x007f:
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r3.<init>(r1, r2)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r1 = r3
            goto L_0x0043
        L_0x008a:
            r3.close()     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            java.lang.String r1 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r1.<init>(r4)     // Catch:{ MalformedURLException -> 0x0064, UnsupportedEncodingException -> 0x00d8, IOException -> 0x00d6 }
            r0 = r1
            goto L_0x007e
        L_0x0094:
            r0 = move-exception
            r0 = r10
        L_0x0096:
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "File encoding \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "\"is not supported when reading "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r7
            goto L_0x007e
        L_0x00ba:
            r0 = move-exception
            r0 = r7
        L_0x00bc:
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not open "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r7
            goto L_0x007e
        L_0x00d6:
            r1 = move-exception
            goto L_0x00bc
        L_0x00d8:
            r0 = move-exception
            r0 = r2
            goto L_0x0096
        L_0x00db:
            r2 = r10
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.io.FileGrabber.read(java.lang.String, java.lang.String, frink.expr.Environment):java.lang.String");
    }
}
