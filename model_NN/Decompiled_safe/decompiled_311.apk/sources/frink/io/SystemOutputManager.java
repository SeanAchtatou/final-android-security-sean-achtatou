package frink.io;

import java.io.OutputStream;

public class SystemOutputManager implements OutputManager {
    public static final SystemOutputManager INSTANCE = new SystemOutputManager();

    private SystemOutputManager() {
    }

    public void output(String str) {
        System.out.print(str);
    }

    public void outputln(String str) {
        System.out.println(str);
    }

    public OutputStream getRawOutputStream() {
        return System.out;
    }
}
