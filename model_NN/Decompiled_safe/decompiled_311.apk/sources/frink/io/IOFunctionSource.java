package frink.io;

import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.StringExpression;
import frink.expr.TypeException;
import frink.expr.UndefExpression;
import frink.function.BasicFunctionSource;
import frink.function.DoubleArgFunction;
import frink.function.SingleArgFunction;
import java.net.MalformedURLException;
import java.net.URL;

public class IOFunctionSource extends BasicFunctionSource {
    public IOFunctionSource() {
        super("IOFunctionSource");
        initializeFunctions();
    }

    private void initializeFunctions() {
        addFunctionDefinition("lines", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                return new LineEnumeration(expression, environment);
            }
        });
        addFunctionDefinition("lines", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                return new LineEnumeration(expression, expression2, environment);
            }
        });
        addFunctionDefinition("read", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new TypeException("Argument to read[] must be a string containing a URL.", expression);
                }
                String read = FileGrabber.read(((StringExpression) expression).getString(), null, environment);
                if (read != null) {
                    return new BasicStringExpression(read);
                }
                return UndefExpression.UNDEF;
            }
        });
        addFunctionDefinition("read", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new TypeException("First argument to read[url, enc] must be a string indicating a URL.", expression);
                } else if (!(expression2 instanceof StringExpression)) {
                    throw new TypeException("Second argument to read[url, enc] must be a string indicating a file encoding.", expression2);
                } else {
                    String read = FileGrabber.read(((StringExpression) expression).getString(), ((StringExpression) expression2).getString(), environment);
                    if (read != null) {
                        return new BasicStringExpression(read);
                    }
                    return UndefExpression.UNDEF;
                }
            }
        });
        addFunctionDefinition("url", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException {
                if (!(expression2 instanceof StringExpression) || !(expression instanceof StringExpression)) {
                    throw new TypeException("Arguments to url[base,expr] must be strings.", this);
                }
                try {
                    return new BasicStringExpression(new URL(new URL(((StringExpression) expression).getString()), ((StringExpression) expression2).getString()).toString());
                } catch (MalformedURLException e) {
                    throw new InvalidArgumentException("Malformed URL: " + e, this);
                }
            }
        });
        addFunctionDefinition("urlProtocol", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new TypeException("Argument to urlProtocol[] must be a string.", expression);
                }
                try {
                    return new BasicStringExpression(new URL(((StringExpression) expression).getString()).getProtocol());
                } catch (MalformedURLException e) {
                    throw new InvalidArgumentException("Malformed URL: " + e, this);
                }
            }
        });
        addFunctionDefinition("urlHost", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (!(expression instanceof StringExpression)) {
                    throw new TypeException("Argument to urlHost[] must be a string.", expression);
                }
                try {
                    return new BasicStringExpression(new URL(((StringExpression) expression).getString()).getHost());
                } catch (MalformedURLException e) {
                    throw new InvalidArgumentException("Malformed URL: " + e, this);
                }
            }
        });
    }
}
