package frink.object;

import frink.expr.EvaluationException;
import frink.expr.Expression;

public class InvalidPropertyException extends EvaluationException {
    public InvalidPropertyException(String str, Expression expression) {
        super("Invalid property: " + str, expression);
    }
}
