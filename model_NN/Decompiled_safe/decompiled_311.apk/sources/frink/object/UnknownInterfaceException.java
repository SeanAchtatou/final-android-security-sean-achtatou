package frink.object;

import frink.parser.ParsingException;

public class UnknownInterfaceException extends ParsingException {
    public UnknownInterfaceException(FrinkClass frinkClass, String str) {
        super("When defining class " + frinkClass.getName() + ", interface \"" + str + "\" was referenced but not declared.");
    }
}
