package frink.object;

import frink.expr.CannotAssignException;
import frink.expr.Constraint;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.SymbolDefinition;
import frink.expr.UnmodifiableSymbolDefinition;
import java.util.Vector;

public class EmptyObjectContextFrame implements ContextFrame, ThisContextFrame {
    private SymbolDefinition thisSD;

    public EmptyObjectContextFrame(Expression expression) {
        if (expression != null) {
            this.thisSD = new UnmodifiableSymbolDefinition(expression);
        } else {
            this.thisSD = null;
        }
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws CannotAssignException {
        if (str.equals("this")) {
            return this.thisSD;
        }
        if (!z) {
            return null;
        }
        throw new CannotAssignException("EmptyObjectContextFrame: cannot careate variable.", this.thisSD.getValue());
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException {
        throw new CannotAssignException("Cannot assign to " + str + " in this object.", this.thisSD.getValue());
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws CannotAssignException {
        throw new CannotAssignException("Cannot declare variable " + str + " in this object.", this.thisSD.getValue());
    }

    public boolean canCreateNewVariables() {
        return false;
    }

    public boolean canModifyVariables() {
        return false;
    }

    public void setThis(Expression expression) {
        if (this.thisSD != null) {
            System.out.println("EmptyObjectContextFrame.setThis: overwriting existing 'this' pointer.");
        }
        this.thisSD = new UnmodifiableSymbolDefinition(expression);
    }

    public EmptyObjectContextFrame deepCopy(int i) {
        return new EmptyObjectContextFrame(null);
    }
}
