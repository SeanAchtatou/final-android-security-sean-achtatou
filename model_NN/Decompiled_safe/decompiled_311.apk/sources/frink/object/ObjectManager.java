package frink.object;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;

public interface ObjectManager {
    void addSource(ObjectSource objectSource);

    Expression construct(String str, Expression expression, Environment environment) throws EvaluationException;

    void removeSource(String str);
}
