package frink.object;

import frink.function.FunctionSignature;
import java.util.Enumeration;
import java.util.Vector;

public class BasicFrinkInterface implements FrinkInterface {
    private String name;
    private Vector<FunctionSignature> signatures = new Vector<>();

    public BasicFrinkInterface(String str) {
        this.name = str;
    }

    public void addSignature(FunctionSignature functionSignature) {
        this.signatures.addElement(functionSignature);
    }

    public String getName() {
        return this.name;
    }

    public Enumeration<FunctionSignature> getSignatures() {
        return this.signatures.elements();
    }
}
