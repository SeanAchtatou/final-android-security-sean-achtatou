package frink.object;

import frink.expr.EnumerationStacker;
import frink.expr.Environment;
import frink.expr.ListExpression;
import frink.function.FunctionDefinition;
import frink.function.FunctionDescriptor;
import frink.function.FunctionSource;
import frink.function.RequiresArgumentsException;
import java.util.Enumeration;

public class ClassFunctionSource implements FunctionSource {
    private FunctionSource classMethods;
    private FunctionSource instanceMethods;

    public ClassFunctionSource(FunctionSource functionSource, FunctionSource functionSource2) {
        this.classMethods = functionSource;
        this.instanceMethods = functionSource2;
    }

    public String getName() {
        return this.instanceMethods.getName();
    }

    public FunctionDefinition getBestMatch(String str, int i, Environment environment) throws RequiresArgumentsException {
        FunctionDefinition functionDefinition = null;
        if (this.classMethods != null && (functionDefinition = this.classMethods.getBestMatch(str, i, environment)) != null) {
            return functionDefinition;
        }
        if (this.instanceMethods != null) {
            return this.instanceMethods.getBestMatch(str, i, environment);
        }
        return functionDefinition;
    }

    public FunctionDefinition getBestMatch(String str, ListExpression listExpression, Environment environment) {
        FunctionDefinition functionDefinition = null;
        if (this.classMethods != null && (functionDefinition = this.classMethods.getBestMatch(str, listExpression, environment)) != null) {
            return functionDefinition;
        }
        if (this.instanceMethods != null) {
            return this.instanceMethods.getBestMatch(str, listExpression, environment);
        }
        return functionDefinition;
    }

    public Enumeration<FunctionDescriptor> getFunctionDescriptors() {
        EnumerationStacker enumerationStacker = new EnumerationStacker();
        enumerationStacker.addEnumeration(this.classMethods.getFunctionDescriptors());
        enumerationStacker.addEnumeration(this.instanceMethods.getFunctionDescriptors());
        return enumerationStacker;
    }
}
