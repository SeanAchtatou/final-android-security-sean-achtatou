package frink.object;

import frink.expr.BasicSymbolDefinition;
import frink.expr.CannotAssignException;
import frink.expr.Constraint;
import frink.expr.ContextFrame;
import frink.expr.DeepCopyable;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.SymbolDefinition;
import frink.expr.UnmodifiableSymbolDefinition;
import frink.expr.VariableExistsException;
import java.util.Vector;

public class FixedSizeContextFrame implements ContextFrame, ThisContextFrame {
    private SymbolDefinition[] values;
    private VariableMap varMap;

    public FixedSizeContextFrame(VariableMap variableMap, Environment environment) throws EvaluationException {
        this.varMap = variableMap;
        this.values = variableMap.createSymbolDefinitions(environment);
    }

    public FixedSizeContextFrame(FixedSizeContextFrame fixedSizeContextFrame, int i) {
        this.varMap = fixedSizeContextFrame.varMap;
        int length = fixedSizeContextFrame.values.length;
        this.values = new SymbolDefinition[length];
        for (int i2 = 1; i2 < length; i2++) {
            Expression value = fixedSizeContextFrame.values[i2].getValue();
            int i3 = i < 0 ? i : i == 0 ? 0 : i - 1;
            if (i != 0 && (value instanceof DeepCopyable)) {
                value = ((DeepCopyable) value).deepCopy(i3);
            }
            this.values[i2] = new BasicSymbolDefinition(value);
        }
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) {
        int index = this.varMap.getIndex(str);
        if (index == -1) {
            return null;
        }
        return this.values[index];
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException {
        int index = this.varMap.getIndex(str);
        if (index < 1) {
            throw new CannotAssignException("FixedSizeContextFrame: Variable " + str + " does not exist in class.", expression);
        }
        SymbolDefinition symbolDefinition = this.values[index];
        symbolDefinition.setValue(expression);
        return symbolDefinition;
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException {
        throw new VariableExistsException("FixedSizeContextFrame:  Cannot declare new variables: " + str, expression);
    }

    public boolean canCreateNewVariables() {
        return false;
    }

    public boolean canModifyVariables() {
        return true;
    }

    public void setThis(Expression expression) {
        this.values[0] = new UnmodifiableSymbolDefinition(expression);
    }

    public FixedSizeContextFrame deepCopy(int i) {
        return new FixedSizeContextFrame(this, i);
    }
}
