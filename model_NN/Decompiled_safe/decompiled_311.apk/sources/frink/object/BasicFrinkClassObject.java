package frink.object;

import frink.expr.CannotAssignException;
import frink.expr.ContextFrame;
import frink.expr.DeepCopyable;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.HashingExpression;
import frink.expr.SelfDisplayingExpression;
import frink.expr.TerminalExpression;
import frink.expr.VariableDeclarationExpression;
import frink.function.FunctionSource;
import frink.symbolic.MatchingContext;
import java.util.Enumeration;

public class BasicFrinkClassObject extends TerminalExpression implements FrinkObject, SelfDisplayingExpression, HashingExpression, DeepCopyable {
    private ThisContextFrame contextFrame;
    private BasicFrinkClass fClass;

    public BasicFrinkClassObject(BasicFrinkClass basicFrinkClass, ThisContextFrame thisContextFrame, FunctionSource functionSource) {
        this.fClass = basicFrinkClass;
        this.contextFrame = thisContextFrame;
        this.contextFrame.setThis(this);
    }

    public BasicFrinkClassObject(BasicFrinkClassObject basicFrinkClassObject, int i) {
        this.fClass = basicFrinkClassObject.fClass;
        this.contextFrame = basicFrinkClassObject.contextFrame.deepCopy(i);
        this.contextFrame.setThis(this);
    }

    public FunctionSource getFunctionSource(Environment environment) throws EvaluationException {
        return this.fClass.getFunctionSource(environment);
    }

    public ContextFrame getContextFrame(Environment environment) {
        return this.contextFrame;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    public boolean isA(String str) {
        if (str.equals(this.fClass.getName())) {
            return true;
        }
        if (this.fClass.implementsInterface(str)) {
            return true;
        }
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String toString(Environment environment, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.fClass.getName() + "\n{\n");
        Enumeration<VariableDeclarationExpression> variableDeclarations = this.fClass.getVariableDeclarations();
        if (variableDeclarations != null) {
            while (variableDeclarations.hasMoreElements()) {
                VariableDeclarationExpression nextElement = variableDeclarations.nextElement();
                stringBuffer.append("   " + nextElement.getName() + " = ");
                try {
                    stringBuffer.append(environment.format(this.contextFrame.getSymbolDefinition(nextElement.getName(), false, environment).getValue()) + "\n");
                } catch (CannotAssignException | FrinkSecurityException e) {
                }
            }
        }
        stringBuffer.append("}\n");
        return new String(stringBuffer);
    }

    public String getExpressionType() {
        return "class:" + this.fClass.getName();
    }

    public Expression deepCopy(int i) {
        return new BasicFrinkClassObject(this, i);
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }
}
