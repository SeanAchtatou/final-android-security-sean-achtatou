package frink.object;

import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.function.FunctionSource;

public interface FrinkObject {
    ContextFrame getContextFrame(Environment environment) throws EvaluationException;

    FunctionSource getFunctionSource(Environment environment) throws EvaluationException;

    boolean isA(String str);
}
