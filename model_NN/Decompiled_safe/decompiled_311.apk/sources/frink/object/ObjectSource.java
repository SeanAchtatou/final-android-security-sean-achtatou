package frink.object;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;

public interface ObjectSource {
    Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException;

    String getName();
}
