package frink.object;

import frink.expr.BasicContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.SecuredContextFrame;
import frink.expr.VariableDeclarationExpression;
import frink.function.BasicFunctionSource;
import frink.function.FunctionDefinition;
import frink.function.FunctionSource;
import java.util.Enumeration;
import java.util.Vector;

public class ClassLevelData {
    private SecuredContextFrame classFrame = null;
    private BasicFunctionSource classMethods = null;
    private Vector<VariableDeclarationExpression> classVariables = new Vector<>();
    private boolean initialized = false;
    private String name;

    public ClassLevelData(String str) {
        this.name = str;
    }

    public void addClassVariable(VariableDeclarationExpression variableDeclarationExpression) {
        this.classVariables.addElement(variableDeclarationExpression);
    }

    public BasicContextFrame getClassContextFrame(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            initializeClassVariables(environment);
        }
        return this.classFrame;
    }

    public void addClassMethod(String str, FunctionDefinition functionDefinition) {
        if (this.classMethods == null) {
            this.classMethods = new BasicFunctionSource("class " + this.name);
        }
        this.classMethods.addFunctionDefinition(str, functionDefinition);
    }

    public FunctionSource getClassFunctionSource(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            initializeClassVariables(environment);
        }
        return this.classMethods;
    }

    public Enumeration<VariableDeclarationExpression> getVariableDeclarations() {
        return this.classVariables.elements();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initializeClassVariables(frink.expr.Environment r8) throws frink.expr.EvaluationException {
        /*
            r7 = this;
            r3 = 0
            boolean r0 = r7.initialized
            if (r0 == 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            monitor-enter(r7)
            boolean r0 = r7.initialized     // Catch:{ all -> 0x000d }
            if (r0 == 0) goto L_0x0010
            monitor-exit(r7)     // Catch:{ all -> 0x000d }
            goto L_0x0005
        L_0x000d:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x000d }
            throw r0
        L_0x0010:
            r0 = 1
            r7.initialized = r0     // Catch:{ all -> 0x000d }
            java.util.Vector<frink.expr.VariableDeclarationExpression> r0 = r7.classVariables     // Catch:{ all -> 0x000d }
            int r1 = r0.size()     // Catch:{ all -> 0x000d }
            if (r1 <= 0) goto L_0x005b
            frink.expr.SecuredContextFrame r0 = new frink.expr.SecuredContextFrame     // Catch:{ all -> 0x000d }
            java.lang.String r2 = r7.name     // Catch:{ all -> 0x000d }
            r0.<init>(r2)     // Catch:{ all -> 0x000d }
            r7.classFrame = r0     // Catch:{ all -> 0x000d }
            frink.expr.SecuredContextFrame r0 = r7.classFrame     // Catch:{ all -> 0x000d }
            r2 = 1
            r8.addContextFrame(r0, r2)     // Catch:{ all -> 0x000d }
            r2 = r3
        L_0x002b:
            if (r2 >= r1) goto L_0x0052
            java.util.Vector<frink.expr.VariableDeclarationExpression> r0 = r7.classVariables     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ all -> 0x005d }
            frink.expr.VariableDeclarationExpression r0 = (frink.expr.VariableDeclarationExpression) r0     // Catch:{ all -> 0x005d }
            frink.expr.SecuredContextFrame r3 = r7.classFrame     // Catch:{ all -> 0x005d }
            java.lang.String r4 = r0.getName()     // Catch:{ all -> 0x005d }
            frink.expr.ConstraintFactory r5 = r8.getConstraintFactory()     // Catch:{ all -> 0x005d }
            java.util.Vector r6 = r0.getConstraints()     // Catch:{ all -> 0x005d }
            java.util.Vector r5 = r5.createConstraints(r6)     // Catch:{ all -> 0x005d }
            frink.expr.Expression r0 = r0.getInitialValue(r8)     // Catch:{ all -> 0x005d }
            r3.declareVariable(r4, r5, r0, r8)     // Catch:{ all -> 0x005d }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x002b
        L_0x0052:
            r8.removeContextFrame()     // Catch:{ all -> 0x000d }
            frink.expr.SecuredContextFrame r0 = r7.classFrame     // Catch:{ all -> 0x000d }
            r1 = 0
            r0.setCanCreateNewVariables(r1)     // Catch:{ all -> 0x000d }
        L_0x005b:
            monitor-exit(r7)     // Catch:{ all -> 0x000d }
            goto L_0x0005
        L_0x005d:
            r0 = move-exception
            r8.removeContextFrame()     // Catch:{ all -> 0x000d }
            frink.expr.SecuredContextFrame r1 = r7.classFrame     // Catch:{ all -> 0x000d }
            r2 = 0
            r1.setCanCreateNewVariables(r2)     // Catch:{ all -> 0x000d }
            throw r0     // Catch:{ all -> 0x000d }
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.object.ClassLevelData.initializeClassVariables(frink.expr.Environment):void");
    }
}
