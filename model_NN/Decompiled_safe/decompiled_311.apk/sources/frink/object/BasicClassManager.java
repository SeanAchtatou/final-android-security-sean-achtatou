package frink.object;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;
import frink.units.BasicManager;

public class BasicClassManager extends BasicManager<FrinkClass> implements ClassManager, ObjectSource {
    public String getName() {
        return "BasicClassManager";
    }

    public FrinkClass getClass(String str) {
        return (FrinkClass) get(str);
    }

    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException {
        FrinkClass frinkClass = getClass(str);
        if (frinkClass == null) {
            return null;
        }
        return frinkClass.createInstance(listExpression, environment);
    }
}
