package frink.object;

import frink.function.FunctionSignature;
import frink.parser.ParsingException;
import java.util.Vector;

public class InterfaceNotImplementedException extends ParsingException {
    private Vector<FunctionSignature> sigs = new Vector<>();

    public InterfaceNotImplementedException(FrinkClass frinkClass, FrinkInterface frinkInterface) {
        super("Class " + frinkClass.getName() + " does not properly implement interface \"" + frinkInterface.getName() + "\".\n  Missing methods are:");
    }

    public void addMissingMethod(FunctionSignature functionSignature) {
        this.sigs.addElement(functionSignature);
    }

    public String toString() {
        int size = this.sigs.size();
        if (size == 0) {
            return super.getMessage();
        }
        StringBuffer stringBuffer = new StringBuffer(super.getMessage());
        for (int i = 0; i < size; i++) {
            stringBuffer.append("\n  " + this.sigs.elementAt(i));
        }
        return new String(stringBuffer);
    }
}
