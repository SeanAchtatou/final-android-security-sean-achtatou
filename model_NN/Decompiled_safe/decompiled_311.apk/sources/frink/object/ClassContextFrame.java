package frink.object;

import frink.expr.CannotAssignException;
import frink.expr.Constraint;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.SymbolDefinition;
import frink.expr.VariableExistsException;
import java.util.Vector;

public class ClassContextFrame implements ContextFrame, ThisContextFrame {
    private ContextFrame classFrame;
    private FixedSizeContextFrame instanceFrame;

    public ClassContextFrame(ContextFrame contextFrame, FixedSizeContextFrame fixedSizeContextFrame) {
        this.classFrame = contextFrame;
        this.instanceFrame = fixedSizeContextFrame;
    }

    public ClassContextFrame(ClassContextFrame classContextFrame, int i) {
        this.classFrame = classContextFrame.classFrame;
        this.instanceFrame = classContextFrame.instanceFrame.deepCopy(i);
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws CannotAssignException, FrinkSecurityException {
        SymbolDefinition symbolDefinition;
        try {
            if (!(this.classFrame == null || (symbolDefinition = this.classFrame.getSymbolDefinition(str, z, environment)) == null)) {
                return symbolDefinition;
            }
        } catch (CannotAssignException e) {
        }
        if (this.instanceFrame != null) {
            return this.instanceFrame.getSymbolDefinition(str, z, environment);
        }
        return null;
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException {
        SymbolDefinition symbolDefinition;
        try {
            if (!(this.instanceFrame == null || (symbolDefinition = this.instanceFrame.setSymbolDefinition(str, expression, environment)) == null)) {
                return symbolDefinition;
            }
        } catch (CannotAssignException e) {
        }
        if (this.classFrame != null) {
            return this.classFrame.setSymbolDefinition(str, expression, environment);
        }
        return null;
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException {
        throw new CannotAssignException("Cannot add new fields to class.", expression);
    }

    public boolean canCreateNewVariables() {
        return false;
    }

    public boolean canModifyVariables() {
        return true;
    }

    public void setThis(Expression expression) {
        this.instanceFrame.setThis(expression);
    }

    public ClassContextFrame deepCopy(int i) {
        return new ClassContextFrame(this, i);
    }
}
