package frink.object;

import frink.expr.ContextFrame;
import frink.expr.Expression;

public interface ThisContextFrame extends ContextFrame {
    void setThis(Expression expression);
}
