package frink.object;

import frink.function.FunctionSignature;
import java.util.Enumeration;

public interface FrinkInterface {
    String getName();

    Enumeration<FunctionSignature> getSignatures();
}
