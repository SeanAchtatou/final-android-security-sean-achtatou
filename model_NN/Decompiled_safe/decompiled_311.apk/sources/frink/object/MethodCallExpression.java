package frink.object;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.NonTerminalExpression;
import frink.function.FunctionCallExpression;
import frink.function.FunctionSource;
import frink.function.NoSuchFunctionException;
import frink.symbolic.MatchingContext;

public class MethodCallExpression extends NonTerminalExpression {
    public static final String TYPE = "MethodCall";
    private FunctionCallExpression fce;

    public MethodCallExpression(Expression expression, FunctionCallExpression functionCallExpression) {
        super(2);
        this.fce = functionCallExpression;
        appendChild(expression);
        appendChild(functionCallExpression);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        FrinkObject frinkObject;
        FunctionSource functionSource;
        Expression evaluate = getChild(0).evaluate(environment);
        if (evaluate instanceof FrinkObject) {
            frinkObject = (FrinkObject) evaluate;
            functionSource = frinkObject.getFunctionSource(environment);
        } else {
            frinkObject = null;
            functionSource = null;
        }
        if (functionSource != null) {
            return this.fce.evaluate(environment, true, frinkObject);
        }
        throw new NoSuchFunctionException(this.fce.getName(), "Object \"" + environment.format(getChild(0)) + "\" has no methods when calling method \"" + this.fce.getName() + "\".", this);
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof MethodCallExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
