package frink.object;

import frink.expr.EvaluationException;
import frink.expr.Expression;

public class NotObjectException extends EvaluationException {
    public NotObjectException(String str, Expression expression) {
        super(str, expression);
    }
}
