package frink.object;

import frink.units.BasicSource;

public class BasicInterfaceSource extends BasicSource<FrinkInterface> {
    public BasicInterfaceSource(String str) {
        super(str);
    }
}
