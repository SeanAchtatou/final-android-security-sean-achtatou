package frink.object;

import frink.expr.EvaluationException;
import frink.expr.Expression;

public class InvalidMethodException extends EvaluationException {
    public InvalidMethodException(String str, Expression expression) {
        super("Invalid method: name", expression);
    }
}
