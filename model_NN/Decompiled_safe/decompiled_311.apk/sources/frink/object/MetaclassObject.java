package frink.object;

import frink.expr.BasicContextFrame;
import frink.expr.CannotAssignException;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.SelfDisplayingExpression;
import frink.expr.TerminalExpression;
import frink.expr.VariableDeclarationExpression;
import frink.function.FunctionSource;
import frink.symbolic.MatchingContext;
import java.util.Enumeration;

public class MetaclassObject extends TerminalExpression implements FrinkObject, SelfDisplayingExpression {
    public static final String TYPE = "MetaclassObject";
    private ClassLevelData classData;
    private FrinkClass fClass;
    private boolean initialized = false;

    public MetaclassObject(FrinkClass frinkClass, ClassLevelData classLevelData) {
        this.fClass = frinkClass;
        this.classData = classLevelData;
    }

    public FunctionSource getFunctionSource(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            initialize(environment);
        }
        return this.classData.getClassFunctionSource(environment);
    }

    public FrinkClass getFrinkClass() {
        return this.fClass;
    }

    public ContextFrame getContextFrame(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            initialize(environment);
        }
        return this.classData.getClassContextFrame(environment);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            initialize(environment);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void initialize(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            synchronized (this) {
                if (!this.initialized) {
                    this.initialized = true;
                    this.classData.initializeClassVariables(environment);
                }
            }
        }
    }

    public boolean isA(String str) {
        return str.equals(this.fClass.getName() + "_class");
    }

    public boolean isConstant() {
        return false;
    }

    public String toString(Environment environment, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (!this.initialized) {
                initialize(environment);
            }
            stringBuffer.append(this.fClass.getName() + "_class\n{\n");
            Enumeration<VariableDeclarationExpression> variableDeclarations = this.classData.getVariableDeclarations();
            if (variableDeclarations != null) {
                try {
                    BasicContextFrame classContextFrame = this.classData.getClassContextFrame(environment);
                    while (variableDeclarations.hasMoreElements()) {
                        VariableDeclarationExpression nextElement = variableDeclarations.nextElement();
                        stringBuffer.append("   " + nextElement.getName() + " = ");
                        try {
                            stringBuffer.append(environment.format(classContextFrame.getSymbolDefinition(nextElement.getName(), false, environment).getValue()) + "\n");
                        } catch (CannotAssignException e) {
                            System.out.println("Got unexpected CannotAssignException in MetaclassObject.toString.");
                        }
                    }
                } catch (EvaluationException e2) {
                    return "Error in MetaclassObject.toString() for " + this.fClass.getName() + ": exception when initializing:\n  " + e2;
                }
            }
            stringBuffer.append("}\n");
        } catch (EvaluationException e3) {
            stringBuffer.append("EXCEPTION OCCURRED: " + e3.toString());
        }
        return new String(stringBuffer);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE + this.fClass.getName();
    }
}
