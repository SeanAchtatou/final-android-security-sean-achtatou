package frink.object;

import frink.expr.ListExpression;
import frink.function.FunctionSignature;
import java.util.Enumeration;

public class InterfaceChecker {
    public static void verifyImplements(FrinkClass frinkClass, FrinkInterface frinkInterface) throws InterfaceNotImplementedException {
        InterfaceNotImplementedException interfaceNotImplementedException;
        InterfaceNotImplementedException interfaceNotImplementedException2;
        Enumeration<FunctionSignature> signatures = frinkInterface.getSignatures();
        if (signatures != null) {
            InterfaceNotImplementedException interfaceNotImplementedException3 = null;
            while (signatures.hasMoreElements()) {
                FunctionSignature nextElement = signatures.nextElement();
                String name = nextElement.getName();
                ListExpression argumentList = nextElement.getArgumentList();
                Enumeration<FunctionSignature> methods = frinkClass.getMethods();
                if (methods == null) {
                    if (interfaceNotImplementedException3 == null) {
                        interfaceNotImplementedException2 = new InterfaceNotImplementedException(frinkClass, frinkInterface);
                    } else {
                        interfaceNotImplementedException2 = interfaceNotImplementedException3;
                    }
                    interfaceNotImplementedException2.addMissingMethod(nextElement);
                    interfaceNotImplementedException3 = interfaceNotImplementedException2;
                } else {
                    while (true) {
                        if (methods.hasMoreElements()) {
                            FunctionSignature nextElement2 = methods.nextElement();
                            if (nextElement2.getName().equals(name) && isAdequate(nextElement2.getArgumentList(), argumentList)) {
                                break;
                            }
                        } else {
                            if (interfaceNotImplementedException3 == null) {
                                interfaceNotImplementedException = new InterfaceNotImplementedException(frinkClass, frinkInterface);
                            } else {
                                interfaceNotImplementedException = interfaceNotImplementedException3;
                            }
                            interfaceNotImplementedException.addMissingMethod(nextElement);
                            interfaceNotImplementedException3 = interfaceNotImplementedException;
                        }
                    }
                }
            }
            if (interfaceNotImplementedException3 != null) {
                throw interfaceNotImplementedException3;
            }
        }
    }

    private static boolean isAdequate(ListExpression listExpression, ListExpression listExpression2) {
        int childCount = listExpression.getChildCount();
        int childCount2 = listExpression2.getChildCount();
        if (childCount2 > childCount) {
            return false;
        }
        return childCount2 == childCount;
    }
}
