package frink.object;

import frink.expr.BasicListExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;
import java.util.Vector;

public class BasicObjectManager implements ObjectManager {
    private Vector<ObjectSource> sources = new Vector<>();

    public Expression construct(String str, Expression expression, Environment environment) throws EvaluationException {
        BasicListExpression basicListExpression;
        int size = this.sources.size();
        if (expression == null) {
            basicListExpression = null;
        } else if (expression instanceof ListExpression) {
            basicListExpression = (ListExpression) expression;
        } else {
            BasicListExpression basicListExpression2 = new BasicListExpression(1);
            basicListExpression2.appendChild(expression);
            basicListExpression = basicListExpression2;
        }
        int i = 0;
        while (i < size) {
            try {
                Expression construct = this.sources.elementAt(i).construct(str, basicListExpression, environment);
                if (construct != null) {
                    return construct;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                return null;
            }
        }
        return null;
    }

    public void addSource(ObjectSource objectSource) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                if (!this.sources.elementAt(i).getName().equals(objectSource.getName())) {
                    i++;
                } else {
                    return;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
        this.sources.addElement(objectSource);
    }

    public void removeSource(String str) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                if (this.sources.elementAt(i).getName().equals(str)) {
                    this.sources.removeElementAt(i);
                    return;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
    }
}
