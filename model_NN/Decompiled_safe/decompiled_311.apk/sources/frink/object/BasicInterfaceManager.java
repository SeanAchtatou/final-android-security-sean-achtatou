package frink.object;

import frink.units.BasicManager;

public class BasicInterfaceManager extends BasicManager<FrinkInterface> implements InterfaceManager {
    public FrinkInterface getInterface(String str) {
        return (FrinkInterface) get(str);
    }
}
