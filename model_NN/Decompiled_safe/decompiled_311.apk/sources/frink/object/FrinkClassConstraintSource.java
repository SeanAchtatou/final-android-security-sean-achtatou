package frink.object;

import frink.expr.Constraint;
import frink.expr.ConstraintException;
import frink.expr.EnumerationStacker;
import frink.expr.Expression;
import frink.units.Source;
import java.util.Enumeration;
import java.util.Hashtable;

public class FrinkClassConstraintSource implements Source<Constraint> {
    private ClassManager classMgr;
    private Hashtable<String, FrinkClassConstraint> constrDict = new Hashtable<>();
    private InterfaceManager interfaceMgr;

    public FrinkClassConstraintSource(ClassManager classManager, InterfaceManager interfaceManager) {
        this.classMgr = classManager;
        this.interfaceMgr = interfaceManager;
    }

    public String getName() {
        return "FrinkClassConstraintSource";
    }

    public Constraint get(String str) {
        FrinkClassConstraint frinkClassConstraint = this.constrDict.get(str);
        if (frinkClassConstraint != null) {
            return frinkClassConstraint;
        }
        if (this.classMgr.getClass(str) != null) {
            FrinkClassConstraint frinkClassConstraint2 = new FrinkClassConstraint(str);
            this.constrDict.put(str, frinkClassConstraint2);
            return frinkClassConstraint2;
        } else if (this.interfaceMgr.getInterface(str) == null) {
            return null;
        } else {
            FrinkClassConstraint frinkClassConstraint3 = new FrinkClassConstraint(str);
            this.constrDict.put(str, frinkClassConstraint3);
            return frinkClassConstraint3;
        }
    }

    public Enumeration<String> getNames() {
        EnumerationStacker enumerationStacker = new EnumerationStacker();
        enumerationStacker.addEnumeration(this.classMgr.getNames());
        enumerationStacker.addEnumeration(this.interfaceMgr.getNames());
        return enumerationStacker;
    }

    public static class FrinkClassConstraint implements Constraint {
        private String name;

        public FrinkClassConstraint(String str) {
            this.name = str;
        }

        public boolean meetsConstraint(Expression expression) {
            if (expression instanceof FrinkObject) {
                return ((FrinkObject) expression).isA(this.name);
            }
            return false;
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!(expression instanceof FrinkObject)) {
                throw new ConstraintException("ConstraintException: Expression is not an object, much less a " + this.name, expression);
            } else if (!((FrinkObject) expression).isA(this.name)) {
                throw new ConstraintException("ConstraintException: Object is not a " + this.name, expression);
            }
        }

        public String getDescription() {
            return "Object must be of type " + this.name;
        }
    }
}
