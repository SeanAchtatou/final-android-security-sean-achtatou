package frink.object;

import frink.units.BasicSource;

public class BasicClassSource extends BasicSource<FrinkClass> {
    public BasicClassSource(String str) {
        super(str);
    }
}
