package frink.object;

import frink.expr.AssignableExpression;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.NonTerminalExpression;
import frink.expr.SymbolDefinition;
import frink.symbolic.MatchingContext;

public class ObjectDerefExpression extends NonTerminalExpression implements AssignableExpression {
    public static final String TYPE = "ObjectDereference";
    private ContextFrame contextCache = null;
    private String member;
    private SymbolDefinition symbolCache = null;

    public ObjectDerefExpression(Expression expression, String str) {
        super(1);
        appendChild(expression);
        this.member = str;
    }

    /* JADX INFO: finally extract failed */
    public Expression evaluate(Environment environment) throws EvaluationException {
        Expression child = getChild(0);
        Expression evaluate = child.evaluate(environment);
        ContextFrame contextFrame = null;
        if ((evaluate instanceof FrinkObject) && (contextFrame = ((FrinkObject) evaluate).getContextFrame(environment)) == this.contextCache && this.symbolCache != null) {
            return this.symbolCache.getValue();
        }
        if (contextFrame == null) {
            throw new NotObjectException("ObjectDerefExpression.evaluate:  Attempt to get member of non-object.\n  Object is " + environment.format(child) + ", member is " + this.member, evaluate);
        }
        try {
            environment.addContextFrame(contextFrame, true);
            this.symbolCache = environment.getSymbolDefinition(this.member, false);
            this.contextCache = contextFrame;
            if (this.symbolCache == null) {
                environment.removeContextFrame();
                return this;
            }
            Expression value = this.symbolCache.getValue();
            environment.removeContextFrame();
            return value;
        } catch (Throwable th) {
            environment.removeContextFrame();
            throw th;
        }
    }

    public boolean isConstant() {
        return false;
    }

    public void assign(Expression expression, Environment environment) throws EvaluationException {
        Expression evaluate = getChild(0).evaluate(environment);
        if (!(evaluate instanceof FrinkObject)) {
            throw new NotObjectException("ObjectDerefExpression.assign:  Attempt to get member of non-object.\n  Object is " + environment.format(evaluate) + ", member is " + this.member, evaluate);
        }
        ((FrinkObject) evaluate).getContextFrame(environment).setSymbolDefinition(this.member, expression, environment);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ObjectDerefExpression) {
            ObjectDerefExpression objectDerefExpression = (ObjectDerefExpression) expression;
            if (childrenEqual(expression, matchingContext, environment, z) && this.member.equals(objectDerefExpression.member)) {
                return true;
            }
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
