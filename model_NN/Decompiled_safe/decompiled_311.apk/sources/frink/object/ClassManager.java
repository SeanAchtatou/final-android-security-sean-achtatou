package frink.object;

import frink.units.Source;
import java.util.Enumeration;

public interface ClassManager {
    void addSource(Source<FrinkClass> source);

    FrinkClass getClass(String str);

    Enumeration<String> getNames();

    Source<FrinkClass> getSource(String str);

    void removeSource(String str);
}
