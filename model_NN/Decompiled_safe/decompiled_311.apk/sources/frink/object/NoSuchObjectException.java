package frink.object;

import frink.expr.EvaluationException;
import frink.expr.Expression;

public class NoSuchObjectException extends EvaluationException {
    public NoSuchObjectException(String str, Expression expression) {
        super(str, expression);
    }
}
