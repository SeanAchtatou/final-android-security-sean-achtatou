package frink.object;

import frink.units.Source;
import java.util.Enumeration;

public interface InterfaceManager {
    void addSource(Source<FrinkInterface> source);

    FrinkInterface getInterface(String str);

    Enumeration<String> getNames();

    Source<FrinkInterface> getSource(String str);

    void removeSource(String str);
}
