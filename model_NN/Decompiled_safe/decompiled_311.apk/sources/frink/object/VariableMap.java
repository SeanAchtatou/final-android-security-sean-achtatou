package frink.object;

import frink.expr.BasicSymbolDefinition;
import frink.expr.ConstrainedSymbolDefinition;
import frink.expr.Constraint;
import frink.expr.ConstraintFactory;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.SymbolDefinition;
import frink.expr.VariableDeclarationExpression;
import java.util.Hashtable;
import java.util.Vector;

public class VariableMap {
    private static final Integer ZERO = new Integer(0);
    private Vector<Vector<Constraint>> constraints = null;
    private boolean constraintsInitialized = false;
    private Hashtable<String, Integer> varMap = new Hashtable<>();
    private Vector<VariableDeclarationExpression> variables;

    public VariableMap(Vector<VariableDeclarationExpression> vector) {
        this.variables = vector;
        int size = vector.size();
        this.varMap.put("this", ZERO);
        for (int i = 0; i < size; i++) {
            this.varMap.put(vector.elementAt(i).getName(), new Integer(i + 1));
        }
    }

    public int getIndex(String str) {
        Integer num = this.varMap.get(str);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    public SymbolDefinition[] createSymbolDefinitions(Environment environment) throws EvaluationException {
        if (!this.constraintsInitialized) {
            synchronized (this) {
                if (!this.constraintsInitialized) {
                    initializeConstraints(environment);
                }
            }
        }
        int size = this.variables.size();
        if (size == 0) {
            return null;
        }
        SymbolDefinition[] symbolDefinitionArr = new SymbolDefinition[(size + 1)];
        symbolDefinitionArr[0] = null;
        for (int i = 0; i < size; i++) {
            Vector elementAt = this.constraints.elementAt(i);
            Expression initialValue = this.variables.elementAt(i).getInitialValue(environment);
            if (elementAt == null) {
                symbolDefinitionArr[i + 1] = new BasicSymbolDefinition(initialValue);
            } else {
                symbolDefinitionArr[i + 1] = new ConstrainedSymbolDefinition(elementAt, initialValue);
            }
        }
        return symbolDefinitionArr;
    }

    private void initializeConstraints(Environment environment) throws EvaluationException {
        synchronized (this) {
            if (!this.constraintsInitialized) {
                this.constraintsInitialized = true;
                int size = this.variables.size();
                this.constraints = new Vector<>(size);
                ConstraintFactory constraintFactory = environment.getConstraintFactory();
                for (int i = 0; i < size; i++) {
                    Vector<String> constraints2 = this.variables.elementAt(i).getConstraints();
                    if (constraints2 == null || constraints2.size() == 0) {
                        this.constraints.addElement(null);
                    } else {
                        this.constraints.addElement(constraintFactory.createConstraints(constraints2));
                    }
                }
            }
        }
    }
}
