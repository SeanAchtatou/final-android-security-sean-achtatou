package frink.object;

import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;
import frink.expr.VariableDeclarationExpression;
import frink.function.BasicFunctionDefinition;
import frink.function.BasicFunctionSource;
import frink.function.FunctionCacher;
import frink.function.FunctionDefinition;
import frink.function.FunctionSignature;
import frink.function.FunctionSource;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class BasicFrinkClass implements FrinkClass {
    private ClassLevelData classData = null;
    private FunctionSource completeMethods = null;
    private boolean haveConstructors = false;
    private boolean initialized = false;
    private BasicFunctionSource instanceMethods = null;
    private MetaclassObject metaclassObject = null;
    private String name;
    private Vector<String> pendingInterfaces = null;
    private Vector<FunctionSignature> signatures = null;
    private VariableMap varMap = null;
    private Vector<VariableDeclarationExpression> variables = new Vector<>();
    private Hashtable<String, FrinkInterface> verifiedInterfaces = null;

    public BasicFrinkClass(String str) {
        this.name = str;
    }

    public void addVariable(VariableDeclarationExpression variableDeclarationExpression) {
        this.variables.addElement(variableDeclarationExpression);
    }

    public void addClassVariable(VariableDeclarationExpression variableDeclarationExpression) {
        if (this.classData == null) {
            this.classData = new ClassLevelData(this.name);
        }
        this.classData.addClassVariable(variableDeclarationExpression);
    }

    public MetaclassObject getMetaclassObject() {
        if (this.metaclassObject != null) {
            return this.metaclassObject;
        }
        if (this.classData == null) {
            return null;
        }
        this.metaclassObject = new MetaclassObject(this, this.classData);
        return this.metaclassObject;
    }

    public void addMethod(FunctionSignature functionSignature, Expression expression) {
        if (this.instanceMethods == null) {
            this.instanceMethods = new BasicFunctionSource(this.name);
        }
        this.instanceMethods.addFunctionDefinition(functionSignature.getName(), new BasicFunctionDefinition(functionSignature, expression));
        addSignature(functionSignature);
    }

    private void addSignature(FunctionSignature functionSignature) {
        if (this.signatures == null) {
            this.signatures = new Vector<>();
        }
        this.signatures.addElement(functionSignature);
    }

    public void addClassMethod(String str, FunctionDefinition functionDefinition) {
        if (this.classData == null) {
            this.classData = new ClassLevelData(this.name);
        }
        if (str.equals("new")) {
            this.haveConstructors = true;
        }
        this.classData.addClassMethod(str, functionDefinition);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
     arg types: [java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.BasicFrinkClassObject, int, ?[OBJECT, ARRAY]]
     candidates:
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression */
    public Expression createInstance(ListExpression listExpression, Environment environment) throws EvaluationException {
        if (!this.initialized) {
            synchronized (this) {
                if (!this.initialized) {
                    initializeFields(environment);
                }
            }
        }
        BasicFrinkClassObject createDefault = createDefault(environment);
        if (this.haveConstructors) {
            try {
                environment.getFunctionManager().execute("new", environment, listExpression, (FrinkObject) createDefault, true, (FunctionCacher) null);
            } catch (EvaluationException e) {
                environment.output("Error when calling constructor " + this.name + ".new" + environment.format(listExpression) + ":\n  ");
                throw e;
            }
        }
        return createDefault;
    }

    public FunctionSource getFunctionSource(Environment environment) throws EvaluationException {
        if (!this.initialized) {
            initializeFields(environment);
        }
        return this.completeMethods;
    }

    private BasicFrinkClassObject createDefault(Environment environment) throws EvaluationException {
        ThisContextFrame thisContextFrame;
        ThisContextFrame thisContextFrame2;
        if (this.classData != null) {
            thisContextFrame = this.classData.getClassContextFrame(environment);
        } else {
            thisContextFrame = null;
        }
        if (this.varMap != null) {
            FixedSizeContextFrame fixedSizeContextFrame = new FixedSizeContextFrame(this.varMap, environment);
            if (thisContextFrame != null) {
                thisContextFrame2 = new ClassContextFrame(thisContextFrame, fixedSizeContextFrame);
            } else {
                thisContextFrame2 = fixedSizeContextFrame;
            }
            return new BasicFrinkClassObject(this, thisContextFrame2, this.completeMethods);
        }
        if (thisContextFrame == null) {
            thisContextFrame = new EmptyObjectContextFrame(null);
        }
        return new BasicFrinkClassObject(this, thisContextFrame, this.completeMethods);
    }

    public String getName() {
        return this.name;
    }

    public Enumeration<VariableDeclarationExpression> getVariableDeclarations() {
        return this.variables.elements();
    }

    public ContextFrame getClassContextFrame(Environment environment) throws EvaluationException {
        if (this.classData != null) {
            return this.classData.getClassContextFrame(environment);
        }
        return null;
    }

    public FunctionSource getClassFunctionSource(Environment environment) throws EvaluationException {
        if (this.classData != null) {
            return this.classData.getClassFunctionSource(environment);
        }
        return null;
    }

    private void initializeVariableMap() {
        if (this.variables.size() > 0) {
            this.varMap = new VariableMap(this.variables);
        }
    }

    private void initializeCompleteMethods(Environment environment) throws EvaluationException {
        FunctionSource functionSource;
        if (this.classData != null) {
            functionSource = this.classData.getClassFunctionSource(environment);
        } else {
            functionSource = null;
        }
        if (functionSource == null) {
            this.completeMethods = this.instanceMethods;
        } else if (this.instanceMethods != null) {
            this.completeMethods = new ClassFunctionSource(functionSource, this.instanceMethods);
        } else {
            this.completeMethods = functionSource;
        }
        this.instanceMethods = null;
    }

    private void initializeFields(Environment environment) throws EvaluationException {
        synchronized (this) {
            if (!this.initialized) {
                if (this.metaclassObject != null) {
                    this.metaclassObject.initialize(environment);
                }
                if (!this.initialized) {
                    initializeCompleteMethods(environment);
                    initializeVariableMap();
                    if (this.classData != null) {
                        this.classData.initializeClassVariables(environment);
                    }
                    this.initialized = true;
                }
            }
        }
    }

    public Enumeration<FunctionSignature> getMethods() {
        if (this.signatures == null) {
            return null;
        }
        return this.signatures.elements();
    }

    public void addPendingInterface(String str) {
        if (this.pendingInterfaces == null) {
            this.pendingInterfaces = new Vector<>();
        }
        this.pendingInterfaces.addElement(str);
    }

    public void testPendingInterfaces(InterfaceManager interfaceManager) throws InterfaceNotImplementedException, UnknownInterfaceException {
        if (this.pendingInterfaces != null) {
            int size = this.pendingInterfaces.size();
            for (int i = 0; i < size; i++) {
                String elementAt = this.pendingInterfaces.elementAt(i);
                FrinkInterface frinkInterface = interfaceManager.getInterface(elementAt);
                if (frinkInterface == null) {
                    throw new UnknownInterfaceException(this, elementAt);
                }
                InterfaceChecker.verifyImplements(this, frinkInterface);
                if (this.verifiedInterfaces == null) {
                    this.verifiedInterfaces = new Hashtable<>(7);
                }
                this.verifiedInterfaces.put(elementAt, frinkInterface);
            }
            this.pendingInterfaces = null;
        }
    }

    public boolean implementsInterface(String str) {
        return this.verifiedInterfaces.get(str) != null;
    }
}
