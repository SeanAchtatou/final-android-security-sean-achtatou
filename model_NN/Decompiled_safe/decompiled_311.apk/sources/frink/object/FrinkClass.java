package frink.object;

import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.ListExpression;
import frink.expr.VariableDeclarationExpression;
import frink.function.FunctionSignature;
import frink.function.FunctionSource;
import java.util.Enumeration;

public interface FrinkClass {
    Expression createInstance(ListExpression listExpression, Environment environment) throws EvaluationException;

    ContextFrame getClassContextFrame(Environment environment) throws EvaluationException;

    FunctionSource getClassFunctionSource(Environment environment) throws EvaluationException;

    Enumeration<FunctionSignature> getMethods();

    String getName();

    Enumeration<VariableDeclarationExpression> getVariableDeclarations();

    boolean implementsInterface(String str);
}
