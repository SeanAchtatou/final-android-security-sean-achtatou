package frink.date;

public interface DateParser {
    String getName();

    FrinkDate parse(String str);
}
