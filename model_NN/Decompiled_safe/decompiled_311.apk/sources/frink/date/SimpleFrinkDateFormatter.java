package frink.date;

import frink.expr.Environment;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.RealInterval;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class SimpleFrinkDateFormatter implements FrinkDateFormatter {
    private DateFormat format;

    public SimpleFrinkDateFormatter(String str) {
        this.format = new SimpleDateFormat(str);
    }

    public SimpleFrinkDateFormatter(DateFormat dateFormat) {
        this.format = dateFormat;
    }

    public String format(FrinkReal frinkReal, TimeZone timeZone, Environment environment) {
        return format(CalendarDate.calculateCalendar(frinkReal).getTime(), timeZone, environment);
    }

    public String format(Date date, TimeZone timeZone, Environment environment) {
        String format2;
        synchronized (this.format) {
            if (timeZone != null) {
                this.format.setTimeZone(timeZone);
            } else {
                this.format.setTimeZone(environment.getDefaultTimeZone());
            }
            format2 = this.format.format(date);
        }
        return format2;
    }

    public String format(FrinkDate frinkDate, TimeZone timeZone, Environment environment) {
        if (frinkDate instanceof CalendarDate) {
            return format(((CalendarDate) frinkDate).getDate(), timeZone, environment);
        }
        Numeric julian = frinkDate.getJulian();
        if (julian.isInterval()) {
            RealInterval realInterval = (RealInterval) julian;
            StringBuffer stringBuffer = new StringBuffer("[");
            stringBuffer.append(format(realInterval.getLower(), timeZone, environment) + ", ");
            FrinkReal main = realInterval.getMain();
            if (main != null) {
                stringBuffer.append(format(main, timeZone, environment) + ", ");
            }
            stringBuffer.append(format(realInterval.getUpper(), timeZone, environment) + "]");
            return new String(stringBuffer);
        } else if (julian.isReal()) {
            return format((FrinkReal) julian, timeZone, environment);
        } else {
            System.err.println("SimpleFrinkDateFormatter:  Got unexpected date: " + frinkDate);
            return null;
        }
    }
}
