package frink.date;

import java.util.Calendar;

public class EmptyDateParser implements DateParser {
    public String getName() {
        return "EmptyDateParser";
    }

    public FrinkDate parse(String str) {
        if (str.length() == 0) {
            return new CalendarDate(Calendar.getInstance());
        }
        return null;
    }
}
