package frink.date;

import frink.errors.ConformanceException;
import frink.errors.NotRealException;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.numeric.RealInterval;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitManager;
import frink.units.UnitMath;

public class DateMath {
    private static Unit day = null;

    public static Unit subtract(FrinkDate frinkDate, FrinkDate frinkDate2, UnitManager unitManager) throws NumericException {
        if (day == null) {
            day = unitManager.getUnit("day");
        }
        return UnitMath.multiply(DimensionlessUnit.construct(NumericMath.subtract(frinkDate.getJulian(), frinkDate2.getJulian())), day);
    }

    public static FrinkDate add(FrinkDate frinkDate, Unit unit, UnitManager unitManager) throws ConformanceException, NumericException {
        if (day == null) {
            day = unitManager.getUnit("day");
        }
        Unit divide = UnitMath.divide(unit, day);
        if (!UnitMath.isDimensionless(divide)) {
            throw new ConformanceException("Interval does not have the same dimensions as \"day\".");
        }
        Numeric add = NumericMath.add(frinkDate.getJulian(), divide.getScale());
        if (add.isReal()) {
            return new CalendarDate((FrinkReal) add);
        }
        if (add.isInterval()) {
            return new DateInterval((RealInterval) add);
        }
        throw new NotRealException("DateMath.add: Time Interval is not real: " + add);
    }

    public static int compare(FrinkDate frinkDate, FrinkDate frinkDate2) {
        if (frinkDate == frinkDate2) {
            return 0;
        }
        try {
            return NumericMath.compare(frinkDate.getJulian(), frinkDate2.getJulian());
        } catch (NumericException e) {
            System.out.println("Warning:  Dates were somehow constructed with non-real parts.");
            return 0;
        } catch (OverlapException e2) {
            System.out.println("Warning:  Dates were somehow constructed with non-real parts.");
            return 0;
        }
    }

    public static FrinkDate infimum(FrinkDate frinkDate) {
        if (frinkDate instanceof DateInterval) {
            return ((DateInterval) frinkDate).getLower();
        }
        return frinkDate;
    }

    public static FrinkDate supremum(FrinkDate frinkDate) {
        if (frinkDate instanceof DateInterval) {
            return ((DateInterval) frinkDate).getUpper();
        }
        return frinkDate;
    }

    public static FrinkDate main(FrinkDate frinkDate) {
        if (frinkDate instanceof DateInterval) {
            return ((DateInterval) frinkDate).getMain();
        }
        return frinkDate;
    }
}
