package frink.date;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Vector;

public class SimpleDateParser implements DateParser {
    private Vector<FormatPair> formats = new Vector<>();
    private FormatPair lastPair = null;

    public void addFormat(String str) {
        int size = this.formats.size();
        int length = str.length();
        this.lastPair = null;
        for (int i = 0; i < size; i++) {
            if (length > this.formats.elementAt(i).getPattern().length()) {
                this.formats.insertElementAt(new FormatPair(str), i);
                return;
            }
        }
        this.formats.addElement(new FormatPair(str));
    }

    public String getName() {
        return "SimpleDateParser";
    }

    public FrinkDate parse(String str) {
        FrinkDate parsePair;
        int size = this.formats.size();
        ParsePosition parsePosition = new ParsePosition(0);
        if (this.lastPair != null && (parsePair = parsePair(str, this.lastPair, parsePosition)) != null) {
            return parsePair;
        }
        for (int i = 0; i < size; i++) {
            FormatPair elementAt = this.formats.elementAt(i);
            if (elementAt != this.lastPair) {
                FrinkDate parsePair2 = parsePair(str, elementAt, parsePosition);
                if (parsePair2 != null) {
                    this.lastPair = elementAt;
                    return parsePair2;
                }
                parsePosition.setIndex(0);
            }
        }
        return null;
    }

    private FrinkDate parsePair(String str, FormatPair formatPair, ParsePosition parsePosition) {
        Date parse;
        DateFormat access$200 = formatPair.getFormat();
        int length = formatPair.getPattern().length();
        synchronized (access$200) {
            parse = access$200.parse(str, parsePosition);
        }
        int index = parsePosition.getIndex();
        if (parse != null && index >= length) {
            if (index >= str.length()) {
                Calendar instance = Calendar.getInstance();
                instance.setTime(parse);
                return new CalendarDate(fixupTime(str, instance));
            }
            TimeZone timeZone = TimeZoneSource.getTimeZone(str.substring(index + 1));
            if (timeZone != null) {
                GregorianCalendar gregorianCalendar = new GregorianCalendar(timeZone);
                gregorianCalendar.setTime(parse);
                GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
                gregorianCalendar2.setTime(parse);
                gregorianCalendar2.setTime(new Date(parse.getTime() - (((long) (gregorianCalendar.get(15) + gregorianCalendar.get(16))) - ((long) (gregorianCalendar2.get(15) + gregorianCalendar2.get(16))))));
                return new CalendarDate(fixupTime(str, gregorianCalendar2));
            }
        }
        return null;
    }

    private Calendar fixupTime(String str, Calendar calendar) {
        int i = calendar.get(1);
        int i2 = calendar.get(2);
        int i3 = calendar.get(5);
        if ((i == 1970 && i2 == 0 && i3 == 1 && str.indexOf("1970") == -1) || (i == 1969 && i2 == 11 && i3 == 31 && str.indexOf("1970") == -1)) {
            Calendar instance = Calendar.getInstance();
            calendar.set(1, instance.get(1));
            calendar.set(2, instance.get(2));
            calendar.set(5, instance.get(5));
        }
        return calendar;
    }

    private static class FormatPair {
        private DateFormat format;
        private String pattern;

        private FormatPair(String str) {
            this.pattern = str;
            this.format = new SimpleDateFormat(str);
            this.format.setLenient(false);
        }

        /* access modifiers changed from: private */
        public String getPattern() {
            return this.pattern;
        }

        /* access modifiers changed from: private */
        public DateFormat getFormat() {
            return this.format;
        }
    }
}
