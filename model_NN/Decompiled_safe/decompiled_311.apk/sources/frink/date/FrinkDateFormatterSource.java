package frink.date;

public interface FrinkDateFormatterSource {
    FrinkDateFormatter getFormatter(String str);

    String getName();
}
