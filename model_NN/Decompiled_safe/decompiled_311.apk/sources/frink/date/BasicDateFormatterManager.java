package frink.date;

import java.util.Vector;

public class BasicDateFormatterManager implements DateFormatterManager {
    private Vector<FrinkDateFormatterSource> sources = new Vector<>();

    public FrinkDateFormatter getFormatter(String str) {
        int size = this.sources.size();
        for (int i = 0; i < size; i++) {
            FrinkDateFormatter formatter = this.sources.elementAt(i).getFormatter(str);
            if (formatter != null) {
                return formatter;
            }
        }
        return null;
    }

    public void addFormatterSource(FrinkDateFormatterSource frinkDateFormatterSource) {
        this.sources.addElement(frinkDateFormatterSource);
    }
}
