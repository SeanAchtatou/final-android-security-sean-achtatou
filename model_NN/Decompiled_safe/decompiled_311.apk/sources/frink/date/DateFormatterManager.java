package frink.date;

public interface DateFormatterManager {
    void addFormatterSource(FrinkDateFormatterSource frinkDateFormatterSource);

    FrinkDateFormatter getFormatter(String str);
}
