package frink.date;

public interface DateParserManager {
    void addParser(DateParser dateParser);

    DateParser getParser(String str);

    FrinkDate parse(String str);
}
