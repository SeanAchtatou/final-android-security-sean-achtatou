package frink.date;

import frink.numeric.FrinkInt;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.RealMath;
import java.util.Vector;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Compiler;

public class LeapSeconds {
    private static boolean initialized = false;
    private static Vector<LeapData> leapTable = null;
    private static Pattern linePattern;

    static {
        try {
            linePattern = new Perl5Compiler().compile("=JD\\s*([\\d\\.]+)\\s*TAI-UTC=\\s*([\\d\\.]+)\\s*S\\s*\\+\\s+\\(MJD\\s*-\\s*([\\d\\.]+)\\s*\\)\\s*X\\s*([\\d\\.]+)", (int) Perl5Compiler.READ_ONLY_MASK);
        } catch (MalformedPatternException e) {
            System.out.println("LeapSeconds:  Error in compiling pattern: " + e);
        }
    }

    public static Numeric getTAIMinusUTC(FrinkReal frinkReal) {
        if (!initialized) {
            initializeTables();
        }
        LeapData leapData = getLeapData(frinkReal);
        if (leapData == null) {
            return FrinkInt.ZERO;
        }
        if (leapData.scale == null) {
            return leapData.offset;
        }
        try {
            return NumericMath.add(leapData.offset, NumericMath.multiply(NumericMath.subtract(frinkReal, leapData.scaleStart), leapData.scale));
        } catch (NumericException e) {
            System.err.println("LeapSeconds:  NumericException:\n  " + e);
            return null;
        }
    }

    private static LeapData getLeapData(FrinkReal frinkReal) {
        int i = 0;
        int size = leapTable.size() - 1;
        if (RealMath.compare(frinkReal, leapTable.elementAt(size).jdStart) >= 0) {
            return leapTable.elementAt(size);
        }
        if (RealMath.compare(frinkReal, leapTable.elementAt(0).jdStart) < 0) {
            return null;
        }
        while (i <= size) {
            int i2 = (size + i) / 2;
            if (RealMath.compare(frinkReal, leapTable.elementAt(i2).jdStart) < 0) {
                size = i2 - 1;
            } else if (RealMath.compare(frinkReal, leapTable.elementAt(i2 + 1).jdStart) < 0) {
                return leapTable.elementAt(i2);
            } else {
                i = i2 + 1;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0118 A[SYNTHETIC, Splitter:B:43:0x0118] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized void initializeTables() {
        /*
            r11 = 2
            r10 = 0
            java.lang.String r0 = "Error closing leap seconds file:\n "
            java.lang.Class<frink.date.LeapSeconds> r0 = frink.date.LeapSeconds.class
            monitor-enter(r0)
            boolean r1 = frink.date.LeapSeconds.initialized     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r0)
            return
        L_0x000d:
            org.apache.oro.text.regex.Pattern r1 = frink.date.LeapSeconds.linePattern     // Catch:{ all -> 0x00c2 }
            java.lang.Class r1 = r1.getClass()     // Catch:{ all -> 0x00c2 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x013a }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x013a }
            java.lang.String r4 = "/data/tai-utc.dat"
            java.io.InputStream r1 = r1.getResourceAsStream(r4)     // Catch:{ all -> 0x013a }
            r3.<init>(r1)     // Catch:{ all -> 0x013a }
            r2.<init>(r3)     // Catch:{ all -> 0x013a }
            org.apache.oro.text.regex.Perl5Matcher r1 = new org.apache.oro.text.regex.Perl5Matcher     // Catch:{ all -> 0x0115 }
            r1.<init>()     // Catch:{ all -> 0x0115 }
            java.util.Vector r3 = new java.util.Vector     // Catch:{ all -> 0x0115 }
            r3.<init>()     // Catch:{ all -> 0x0115 }
            frink.date.LeapSeconds.leapTable = r3     // Catch:{ all -> 0x0115 }
        L_0x002f:
            java.lang.String r3 = r2.readLine()     // Catch:{ IOException -> 0x0087 }
            if (r3 == 0) goto L_0x011c
            org.apache.oro.text.regex.PatternMatcherInput r4 = new org.apache.oro.text.regex.PatternMatcherInput     // Catch:{ IOException -> 0x0087 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0087 }
            org.apache.oro.text.regex.Pattern r5 = frink.date.LeapSeconds.linePattern     // Catch:{ IOException -> 0x0087 }
            boolean r4 = r1.contains(r4, r5)     // Catch:{ IOException -> 0x0087 }
            if (r4 == 0) goto L_0x00fb
            org.apache.oro.text.regex.MatchResult r3 = r1.getMatch()     // Catch:{ IOException -> 0x0087 }
            frink.numeric.FrinkFloat r4 = new frink.numeric.FrinkFloat     // Catch:{ IOException -> 0x0087 }
            r5 = 1
            java.lang.String r5 = r3.group(r5)     // Catch:{ IOException -> 0x0087 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0087 }
            r5 = 2
            java.lang.String r5 = r3.group(r5)     // Catch:{ IOException -> 0x0087 }
            java.lang.String r6 = ".0"
            boolean r6 = r5.endsWith(r6)     // Catch:{ IOException -> 0x0087 }
            if (r6 == 0) goto L_0x00c5
            r6 = 0
            int r7 = r5.length()     // Catch:{ IOException -> 0x0087 }
            int r7 = r7 - r11
            java.lang.String r5 = r5.substring(r6, r7)     // Catch:{ IOException -> 0x0087 }
            r6 = 10
            frink.numeric.FrinkInteger r5 = frink.numeric.FrinkInteger.construct(r5, r6)     // Catch:{ IOException -> 0x0087 }
        L_0x006d:
            r6 = 4
            java.lang.String r6 = r3.group(r6)     // Catch:{ IOException -> 0x0087 }
            java.lang.String r7 = "0.0"
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x0087 }
            if (r6 == 0) goto L_0x00cc
            r3 = r10
            r6 = r10
        L_0x007c:
            java.util.Vector<frink.date.LeapSeconds$LeapData> r7 = frink.date.LeapSeconds.leapTable     // Catch:{ IOException -> 0x0087 }
            frink.date.LeapSeconds$LeapData r8 = new frink.date.LeapSeconds$LeapData     // Catch:{ IOException -> 0x0087 }
            r8.<init>(r4, r5, r3, r6)     // Catch:{ IOException -> 0x0087 }
            r7.addElement(r8)     // Catch:{ IOException -> 0x0087 }
            goto L_0x002f
        L_0x0087:
            r1 = move-exception
            java.io.PrintStream r3 = java.lang.System.err     // Catch:{ all -> 0x0115 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0115 }
            r4.<init>()     // Catch:{ all -> 0x0115 }
            java.lang.String r5 = "LeapSeconds:  IOException:\n  "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0115 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0115 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0115 }
            r3.println(r1)     // Catch:{ all -> 0x0115 }
        L_0x00a0:
            if (r2 == 0) goto L_0x000b
            r2.close()     // Catch:{ IOException -> 0x00a7 }
            goto L_0x000b
        L_0x00a7:
            r1 = move-exception
            java.io.PrintStream r2 = java.lang.System.err     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r3.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r4 = "Error closing leap seconds file:\n "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x00c2 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00c2 }
            r2.println(r1)     // Catch:{ all -> 0x00c2 }
            goto L_0x000b
        L_0x00c2:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x00c5:
            frink.numeric.FrinkFloat r6 = new frink.numeric.FrinkFloat     // Catch:{ IOException -> 0x0087 }
            r6.<init>(r5)     // Catch:{ IOException -> 0x0087 }
            r5 = r6
            goto L_0x006d
        L_0x00cc:
            frink.numeric.FrinkFloat r6 = new frink.numeric.FrinkFloat     // Catch:{ IOException -> 0x0087 }
            r7 = 4
            java.lang.String r7 = r3.group(r7)     // Catch:{ IOException -> 0x0087 }
            r6.<init>(r7)     // Catch:{ IOException -> 0x0087 }
            frink.numeric.FrinkFloat r7 = new frink.numeric.FrinkFloat     // Catch:{ IOException -> 0x0087 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0087 }
            r8.<init>()     // Catch:{ IOException -> 0x0087 }
            java.lang.String r9 = "24"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0087 }
            r9 = 3
            java.lang.String r3 = r3.group(r9)     // Catch:{ IOException -> 0x0087 }
            java.lang.StringBuilder r3 = r8.append(r3)     // Catch:{ IOException -> 0x0087 }
            java.lang.String r8 = "5"
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ IOException -> 0x0087 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0087 }
            r7.<init>(r3)     // Catch:{ IOException -> 0x0087 }
            r3 = r7
            goto L_0x007c
        L_0x00fb:
            java.io.PrintStream r4 = java.lang.System.err     // Catch:{ IOException -> 0x0087 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0087 }
            r5.<init>()     // Catch:{ IOException -> 0x0087 }
            java.lang.String r6 = "LeapSeconds:  Unmatched line:\n"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0087 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x0087 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0087 }
            r4.println(r3)     // Catch:{ IOException -> 0x0087 }
            goto L_0x002f
        L_0x0115:
            r1 = move-exception
        L_0x0116:
            if (r2 == 0) goto L_0x011b
            r2.close()     // Catch:{ IOException -> 0x0120 }
        L_0x011b:
            throw r1     // Catch:{ all -> 0x00c2 }
        L_0x011c:
            r1 = 1
            frink.date.LeapSeconds.initialized = r1     // Catch:{ IOException -> 0x0087 }
            goto L_0x00a0
        L_0x0120:
            r2 = move-exception
            java.io.PrintStream r3 = java.lang.System.err     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r4.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r5 = "Error closing leap seconds file:\n "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00c2 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x00c2 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c2 }
            r3.println(r2)     // Catch:{ all -> 0x00c2 }
            goto L_0x011b
        L_0x013a:
            r1 = move-exception
            r2 = r10
            goto L_0x0116
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.date.LeapSeconds.initializeTables():void");
    }

    private static class LeapData {
        /* access modifiers changed from: private */
        public FrinkReal jdStart;
        /* access modifiers changed from: private */
        public FrinkReal offset;
        /* access modifiers changed from: private */
        public FrinkReal scale;
        /* access modifiers changed from: private */
        public FrinkReal scaleStart;

        public LeapData(FrinkReal frinkReal, FrinkReal frinkReal2, FrinkReal frinkReal3, FrinkReal frinkReal4) {
            this.jdStart = frinkReal;
            this.offset = frinkReal2;
            this.scaleStart = frinkReal3;
            this.scale = frinkReal4;
        }
    }
}
