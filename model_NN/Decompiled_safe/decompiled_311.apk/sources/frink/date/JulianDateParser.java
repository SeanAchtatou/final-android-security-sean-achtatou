package frink.date;

import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;

public class JulianDateParser implements DateParser {
    private static Numeric MJDOffset;

    static {
        try {
            MJDOffset = FrinkRational.construct(FrinkInt.construct(24000005), FrinkInt.TEN);
        } catch (InvalidDenominatorException e) {
        }
    }

    public String getName() {
        return "JulianDateParser";
    }

    public FrinkDate parse(String str) {
        if (str.startsWith("JD ")) {
            try {
                return new JulianDate(new FrinkFloat(str.substring(3)));
            } catch (NumberFormatException e) {
                return null;
            }
        } else if (str.startsWith("MJD ")) {
            try {
                Numeric add = NumericMath.add(MJDOffset, new FrinkFloat(str.substring(4)));
                if (add.isReal()) {
                    return new JulianDate((FrinkReal) add);
                }
                System.err.println("Got non-real value in JulianDateParser.parse");
                return null;
            } catch (NumberFormatException e2) {
                return null;
            } catch (NumericException e3) {
                return null;
            }
        } else if (!str.startsWith("JDE ")) {
            return null;
        } else {
            try {
                double doubleValue = Double.valueOf(str.substring(4)).doubleValue();
                return new JulianDate(doubleValue - (DynamicalTime.getDeltaT(doubleValue) / 86400.0d));
            } catch (NumberFormatException e4) {
                return null;
            }
        }
    }
}
