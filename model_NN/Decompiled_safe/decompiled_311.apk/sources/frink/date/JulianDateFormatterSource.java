package frink.date;

import frink.expr.Environment;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.RealInterval;
import java.util.TimeZone;

public class JulianDateFormatterSource implements FrinkDateFormatterSource {
    private static final String JDEident = "JDE";
    private static final String JDident = "JD";
    public static Numeric MJDOffset = null;
    private static final String MJDident = "MJD";
    private static final FrinkDateFormatter jdef = new FrinkDateFormatter() {
        private String format(FrinkReal frinkReal, TimeZone timeZone, Environment environment) {
            return format(frinkReal.doubleValue(), timeZone, environment);
        }

        private String format(double d, TimeZone timeZone, Environment environment) {
            return "JDE " + ((DynamicalTime.getDeltaT(d) / 86400.0d) + d);
        }

        public String format(FrinkDate frinkDate, TimeZone timeZone, Environment environment) {
            Numeric julian = frinkDate.getJulian();
            if (julian.isReal()) {
                return format((FrinkReal) julian, timeZone, environment);
            }
            if (julian.isInterval()) {
                RealInterval realInterval = (RealInterval) julian;
                StringBuffer stringBuffer = new StringBuffer("[");
                stringBuffer.append(format(realInterval.getLower(), timeZone, environment) + ", ");
                FrinkReal main = realInterval.getMain();
                if (main != null) {
                    stringBuffer.append(format(main, timeZone, environment) + ", ");
                }
                stringBuffer.append(format(realInterval.getUpper(), timeZone, environment) + "]");
                return new String(stringBuffer);
            }
            System.err.println("JulianDateFormatter:  Got unexpected date: " + frinkDate);
            return null;
        }
    };
    private static final FrinkDateFormatter jdf = new FrinkDateFormatter() {
        public String format(FrinkDate frinkDate, TimeZone timeZone, Environment environment) {
            Numeric julian = frinkDate.getJulian();
            if (julian.isFloat()) {
                return "JD " + ((FrinkFloat) julian).getBigDec().format(-1, -1);
            }
            return "JD " + julian.toString();
        }
    };
    private static final FrinkDateFormatter mjdf = new FrinkDateFormatter() {
        public String format(FrinkDate frinkDate, TimeZone timeZone, Environment environment) {
            try {
                return "MJD " + NumericMath.subtract(frinkDate.getJulian(), JulianDateFormatterSource.MJDOffset).toString();
            } catch (NumericException e) {
                System.err.println("Got unexpected error in JulianDateFormatterSource.format():\n " + e);
                return "???";
            }
        }
    };

    static {
        try {
            MJDOffset = FrinkRational.construct(FrinkInt.construct(24000005), FrinkInt.TEN);
        } catch (InvalidDenominatorException e) {
        }
    }

    public String getName() {
        return "JulianDateFormatterSource";
    }

    public FrinkDateFormatter getFormatter(String str) {
        if (str.equals(JDident)) {
            return jdf;
        }
        if (str.equals(JDEident)) {
            return jdef;
        }
        if (str.equals(MJDident)) {
            return mjdf;
        }
        return null;
    }
}
