package frink.date;

import frink.numeric.FrinkFloat;
import frink.numeric.FrinkReal;

public class JulianDate implements FrinkDate {
    private FrinkReal julian;

    public JulianDate(FrinkReal frinkReal) {
        this.julian = frinkReal;
    }

    public JulianDate(double d) {
        this.julian = new FrinkFloat(d);
    }

    public FrinkReal getJulian() {
        return this.julian;
    }
}
