package frink.date;

import frink.expr.Environment;
import java.util.TimeZone;

public interface FrinkDateFormatter {
    String format(FrinkDate frinkDate, TimeZone timeZone, Environment environment);
}
