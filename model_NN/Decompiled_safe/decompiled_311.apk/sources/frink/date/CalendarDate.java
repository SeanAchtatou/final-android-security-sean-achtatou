package frink.date;

import frink.expr.OperatorExpression;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class CalendarDate implements FrinkDate {
    private static final int MS_IN_DAY = 86400000;
    private static final int MS_IN_HALF_DAY = 43200000;
    private static final int MS_IN_HOUR = 3600000;
    private static Calendar refCal = Calendar.getInstance();
    private static FrinkReal refJulian = calculateJulian(refCal);
    private static final int refYear = 2000;
    private Calendar cal;
    private FrinkReal julian;

    static {
        refCal.clear();
        refCal.set(2000, 0, 1, 12, 0, 0);
    }

    public CalendarDate(Calendar calendar) {
        this.julian = null;
        this.cal = calendar;
    }

    public CalendarDate(FrinkReal frinkReal) {
        this.julian = frinkReal;
        this.cal = null;
    }

    public CalendarDate(Date date) {
        this.julian = null;
        this.cal = Calendar.getInstance();
        this.cal.setTime(date);
    }

    /* access modifiers changed from: package-private */
    public Date getDate() {
        if (this.cal != null) {
            return this.cal.getTime();
        }
        this.cal = calculateCalendar(this.julian);
        return this.cal.getTime();
    }

    public FrinkReal getJulian() {
        if (this.julian != null) {
            return this.julian;
        }
        this.julian = calculateJulian(this.cal);
        return this.julian;
    }

    public static FrinkReal calculateJulian(Calendar calendar) {
        int i;
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5 = calendar.get(0);
        int i6 = calendar.get(1);
        int i7 = calendar.get(2) + 1;
        int i8 = calendar.get(5);
        int i9 = (calendar.get(11) * 3600 * 1000) + (calendar.get(12) * 60 * 1000) + (calendar.get(13) * 1000) + calendar.get(14);
        int i10 = calendar.get(15);
        if (calendar.getTimeZone().inDaylightTime(calendar.getTime())) {
            i10 += calendar.get(16);
        }
        if (i5 == 0) {
            i = (-i6) + 1;
        } else {
            i = i6;
        }
        if (calendar instanceof GregorianCalendar) {
            z = calendar.getTime().after(((GregorianCalendar) calendar).getGregorianChange());
        } else {
            z = true;
        }
        int i11 = (14 - i7) / 12;
        int i12 = (i + 4800) - i11;
        int i13 = ((i11 * 12) + i7) - 3;
        if (z) {
            if (i12 >= 0) {
                i4 = ((i12 / 4) - (i12 / 100)) + (i12 / OperatorExpression.PREC_ADD);
            } else {
                i4 = (((i12 - 3) / 4) - ((i12 - 99) / 100)) + ((i12 - 399) / OperatorExpression.PREC_ADD);
            }
            i3 = i4 - 32045;
        } else {
            if (i12 >= 0) {
                i2 = i12 / 4;
            } else {
                i2 = (i12 - 3) / 4;
            }
            i3 = i2 - 32083;
        }
        try {
            Numeric add = NumericMath.add(FrinkInteger.construct(i3 + (((i13 * 153) + 2) / 5) + i8 + (i12 * 365)), FrinkRational.construct((i9 - i10) - MS_IN_HALF_DAY, (int) MS_IN_DAY));
            if (add.isReal()) {
                return (FrinkReal) add;
            }
        } catch (NumericException e) {
            System.err.println("CalendarDate.calculateJulian:  Unexpected numeric exception:\n " + e.getMessage());
        }
        System.err.println("Falling back to dumb Julian day calculation.  This should be considered a bug.");
        return new FrinkFloat(((((double) i8) + (((((double) ((i / OperatorExpression.PREC_ADD) + (((i * 365) + (i / 4)) - (i / 100)))) - 1200820.5d) + ((double) (((i7 * 153) + 3) / 5))) - 92.0d)) - 1.0d) + ((double) ((i9 - i10) / MS_IN_DAY)));
    }

    public static Calendar calculateCalendar(FrinkReal frinkReal) {
        try {
            double doubleValue = NumericMath.subtract(frinkReal, refJulian).doubleValue();
            Calendar calendar = (Calendar) refCal.clone();
            calendar.setTimeInMillis(Math.round((doubleValue * 8.64E7d) + ((double) refCal.getTimeInMillis())));
            return calendar;
        } catch (NumericException e) {
            System.err.println("CalendarDate.calculateCalendar: Somehow got a numeric exception:\n " + e);
            return null;
        }
    }

    public static void main(String[] strArr) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("G yyyy-MM-dd hh:mm:ss.SSS a (E) zzzz");
        System.out.println("Calculated  Correct");
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        instance.clear();
        instance.set(1993, 0, 1, 12, 0, 0);
        FrinkReal calculateJulian = calculateJulian(instance);
        System.out.println(calculateJulian.toString() + "\n2448989 (should match this)");
        System.out.println(simpleDateFormat.format(instance.getTime()));
        System.out.println(simpleDateFormat.format(calculateCalendar(calculateJulian).getTime()));
        instance.clear();
        instance.set(2000, 0, 1, 12, 0, 0);
        FrinkReal calculateJulian2 = calculateJulian(instance);
        System.out.println(calculateJulian2.toString() + "\n2451545 (should match this)");
        System.out.println(simpleDateFormat.format(instance.getTime()));
        System.out.println(simpleDateFormat.format(calculateCalendar(calculateJulian2).getTime()));
        Calendar instance2 = Calendar.getInstance();
        FrinkReal calculateJulian3 = calculateJulian(instance2);
        System.out.println("Now is " + calculateJulian3.toString());
        System.out.println(simpleDateFormat.format(instance2.getTime()));
        System.out.println(simpleDateFormat.format(calculateCalendar(calculateJulian3).getTime()));
        Calendar instance3 = Calendar.getInstance();
        FrinkReal calculateJulian4 = calculateJulian(instance3);
        System.out.println("Now is " + calculateJulian4.toString());
        DateFormat dateFormat = (DateFormat) simpleDateFormat.clone();
        dateFormat.setTimeZone(TimeZone.getTimeZone("America/Indianapolis"));
        System.out.println(dateFormat.format(instance3.getTime()));
        System.out.println(dateFormat.format(calculateCalendar(calculateJulian4).getTime()));
        instance3.clear();
        instance3.set(1, 0, 1, 12, 0, 0);
        FrinkReal calculateJulian5 = calculateJulian(instance3);
        System.out.println(simpleDateFormat.format(instance3.getTime()));
        System.out.println(simpleDateFormat.format(calculateCalendar(calculateJulian5).getTime()));
        System.out.println(calculateJulian5.toString());
        instance3.clear();
        instance3.set(0, 11, 31, 12, 0, 0);
        System.out.println(simpleDateFormat.format(instance3.getTime()));
        FrinkReal calculateJulian6 = calculateJulian(instance3);
        System.out.println(simpleDateFormat.format(calculateCalendar(calculateJulian6).getTime()));
        System.out.println(calculateJulian6.toString());
    }
}
