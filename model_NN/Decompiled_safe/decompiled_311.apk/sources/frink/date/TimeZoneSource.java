package frink.date;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class TimeZoneSource {
    private static final boolean DEBUG = false;
    private static Hashtable<String, TimeZoneWrapper> timezones = new Hashtable<>();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static {
        String[] availableIDs = TimeZone.getAvailableIDs();
        for (String str : availableIDs) {
            TimeZoneWrapper timeZoneWrapper = new TimeZoneWrapper(str);
            timezones.put(str, timeZoneWrapper);
            int indexOf = str.indexOf("/");
            if (indexOf != -1) {
                String substring = str.substring(indexOf + 1);
                if (timezones.get(substring) == null) {
                    timezones.put(substring, timeZoneWrapper);
                }
                if (substring.indexOf(95) != -1) {
                    timezones.put(substring.replace('_', ' '), timeZoneWrapper);
                }
            }
        }
        addAliases();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static void addAlias(String str, String str2) {
        if (str2.length() != 0) {
            TimeZoneWrapper timeZoneWrapper = timezones.get(str);
            if (timezones.get(str2) == null) {
                if (timeZoneWrapper != null) {
                    timezones.put(str2, timeZoneWrapper);
                } else {
                    return;
                }
            }
            if (str2.indexOf(32) != -1) {
                addAlias(str, str2.replace(' ', '_'));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static void addAlias(TimeZoneWrapper timeZoneWrapper, String str) {
        if (str.length() != 0) {
            if (timezones.get(str) == null) {
                timezones.put(str, timeZoneWrapper);
            }
            if (str.indexOf(32) != -1) {
                addAlias(timeZoneWrapper, str.replace(' ', '_'));
            }
        }
    }

    private static void addAliases() {
        addAlias("ACT", "");
        addAlias("AET", "");
        addAlias("AGT", "");
        addAlias("ART", "");
        addAlias("AST", "");
        addAlias("Africa/Abidjan", "Cote D'Ivoire");
        addAlias("Africa/Abidjan", "Ivory Coast");
        addAlias("Africa/Accra", "Ghana");
        addAlias("Africa/Addis_Ababa", "Ethiopia");
        addAlias("Africa/Algiers", "Algeria");
        addAlias("Africa/Asmera", "Eritrea");
        addAlias("Africa/Bangui", "Central African Republic");
        addAlias("Africa/Banjul", "Gambia");
        addAlias("Africa/Bissau", "Guinea-Bissau");
        addAlias("Africa/Blantyre", "Malawi");
        addAlias("Africa/Bujumbura", "Burundi");
        addAlias("Africa/Cairo", "Egypt");
        addAlias("Africa/Casablanca", "Morocco");
        addAlias("Africa/Conakry", "Guinea");
        addAlias("Africa/Dakar", "Senegal");
        addAlias("Africa/Dar_es_Salaam", "Tanzania");
        addAlias("Africa/Djibouti", "");
        addAlias("Africa/Douala", "Cameroon");
        addAlias("Africa/Freetown", "Sierra Leone");
        addAlias("Africa/Gaborone", "Botswana");
        addAlias("Africa/Harare", "Zimbabwe");
        addAlias("Africa/Johannesburg", "South Africa");
        addAlias("Africa/Kampala", "Uganda");
        addAlias("Africa/Khartoum", "Sudan");
        addAlias("Africa/Kigali", "Rwanda");
        addAlias("Africa/Kinshasa", "");
        addAlias("Africa/Lagos", "Nigeria");
        addAlias("Africa/Libreville", "Gabon");
        addAlias("Africa/Lome", "Togo");
        addAlias("Africa/Luanda", "Angola");
        addAlias("Africa/Lubumbashi", "");
        addAlias("Africa/Lusaka", "Zambia");
        addAlias("Africa/Malabo", "Equatorial Guinea");
        addAlias("Africa/Maputo", "Mozambique");
        addAlias("Africa/Maseru", "Lesotho");
        addAlias("Africa/Mbabane", "Swaziland");
        addAlias("Africa/Mogadishu", "Somalia");
        addAlias("Africa/Monrovia", "Liberia");
        addAlias("Africa/Nairobi", "Kenya");
        addAlias("Africa/Ndjamena", "Chad");
        addAlias("Africa/Niamey", "Niger");
        addAlias("Africa/Nouakchott", "Mauritania");
        addAlias("Africa/Ouagadougou", "Burkina Faso");
        addAlias("Africa/Porto-Novo", "Benin");
        addAlias("Africa/Sao_Tome", "Sao Tome and Principe");
        addAlias("Africa/Sao_Tome", "Principe");
        addAlias("Africa/Timbuktu", "");
        addAlias("Africa/Tripoli", "Libya");
        addAlias("Africa/Tunis", "Tunisia");
        addAlias("Africa/Windhoek", "Namibia");
        addAlias("America/Adak", "Aleutian Islands");
        addAlias("America/Anchorage", "Alaska");
        addAlias("America/Anguilla", "");
        addAlias("America/Antigua", "Antigua and Barbuda");
        addAlias("America/Antigua", "Barbuda");
        addAlias("America/Aruba", "");
        addAlias("America/Asuncion", "Paraguay");
        addAlias("America/Barbados", "");
        addAlias("America/Belize", "");
        addAlias("America/Bogota", "Colombia");
        addAlias("America/Buenos_Aires", "");
        addAlias("America/Caracas", "Venezuela");
        addAlias("America/Cayenne", "French Guiana");
        addAlias("America/Cayman", "Cayman Islands");
        addAlias("America/Chicago", "Central");
        addAlias("America/Costa_Rica", "");
        addAlias("America/Cuiaba", "");
        addAlias("America/Curacao", "Netherlands Antilles");
        addAlias("America/Dawson_Creek", "");
        addAlias("America/Denver", "Colorado");
        addAlias("America/Denver", "Mountain");
        addAlias("America/Dominica", "");
        addAlias("America/Edmonton", "");
        addAlias("America/El_Salvador", "");
        addAlias("America/Fortaleza", "Brazil");
        addAlias("America/Godthab", "Western Greenland");
        addAlias("America/Grand_Turk", "Turks and Caicos Islands");
        addAlias("America/Grand_Turk", "Turks");
        addAlias("America/Grand_Turk", "Caicos");
        addAlias("America/Grenada", "");
        addAlias("America/Guadeloupe", "");
        addAlias("America/Guatemala", "");
        addAlias("America/Guayaquil", "Ecuador");
        addAlias("America/Guyana", "");
        addAlias("America/Halifax", "Nova Scotia");
        addAlias("America/Havana", "Cuba");
        addAlias("America/Indianapolis", "");
        addAlias("America/Jamaica", "");
        addAlias("America/La_Paz", "Bolivia");
        addAlias("America/Lima", "Peru");
        addAlias("America/Los_Angeles", "Pacific");
        addAlias("America/Managua", "Nicaragua");
        addAlias("America/Manaus", "Amazon");
        addAlias("America/Martinique", "");
        addAlias("America/Mazatlan", "");
        addAlias("America/Mexico_City", "");
        addAlias("America/Miquelon", "St. Pierre");
        addAlias("America/Miquelon", "Saint Pierre");
        addAlias("America/Montevideo", "Uruguay");
        addAlias("America/Montreal", "");
        addAlias("America/Montserrat", "");
        addAlias("America/Nassau", "Bahamas");
        addAlias("America/New_York", "Eastern");
        addAlias("America/Noronha", "");
        addAlias("America/Panama", "");
        addAlias("America/Paramaribo", "Suriname");
        addAlias("America/Phoenix", "Arizona");
        addAlias("America/Port-au-Prince", "Haiti");
        addAlias("America/Port_of_Spain", "Trinidad and Tobago");
        addAlias("America/Port_of_Spain", "Trinidad");
        addAlias("America/Port_of_Spain", "Tobago");
        addAlias("America/Porto_Acre", "");
        addAlias("America/Puerto_Rico", "");
        addAlias("America/Regina", "");
        addAlias("America/Rio_Branco", "");
        addAlias("America/Santiago", "Chile");
        addAlias("America/Santo_Domingo", "Dominican Republic");
        addAlias("America/Sao_Paulo", "");
        addAlias("America/Scoresbysund", "Eastern Greenland");
        addAlias("America/St_Johns", "Newfoundland");
        addAlias("America/St_Kitts", "Saint Kitts and Nevis");
        addAlias("America/St_Kitts", "Saint Kitts");
        addAlias("America/St_Kitts", "Nevis");
        addAlias("America/St_Lucia", "Saint Lucia");
        addAlias("America/St_Thomas", "US Virgin Islands");
        addAlias("America/St_Vincent", "Saint Vincent and the Grenadines");
        addAlias("America/Tegucigalpa", "Honduras");
        addAlias("America/Thule", "");
        addAlias("America/Tijuana", "");
        addAlias("America/Tortola", "British Virgin Islands");
        addAlias("America/Vancouver", "");
        addAlias("America/Winnipeg", "");
        addAlias("Antarctica/Casey", "");
        addAlias("Antarctica/DumontDUrville", "");
        addAlias("Antarctica/Mawson", "");
        addAlias("Antarctica/McMurdo", "");
        addAlias("Antarctica/Palmer", "");
        addAlias("Asia/Aden", "Yemen");
        addAlias("Asia/Almaty", "Alma-Ata");
        addAlias("Asia/Amman", "Jordan");
        addAlias("Asia/Anadyr", "");
        addAlias("Asia/Aqtau", "");
        addAlias("Asia/Aqtobe", "");
        addAlias("Asia/Ashgabat", "Turkmenistan");
        addAlias("Asia/Ashkhabad", "");
        addAlias("Asia/Baghdad", "Iraq");
        addAlias("Asia/Bahrain", "");
        addAlias("Asia/Baku", "Azerbaijan");
        addAlias("Asia/Bangkok", "Thailand");
        addAlias("Asia/Beirut", "Lebanon");
        addAlias("Asia/Bishkek", "Kyrgyzstan");
        addAlias("Asia/Brunei", "");
        addAlias("Asia/Calcutta", "India");
        addAlias("Asia/Colombo", "Sri Lanka");
        addAlias("Asia/Dacca", "Bangladesh");
        addAlias("Asia/Damascus", "Syria");
        addAlias("Asia/Dhaka", "");
        addAlias("Asia/Dubai", "United Arab Emirates");
        addAlias("Asia/Dubai", "UAE");
        addAlias("Asia/Dushanbe", "Tajikstan");
        addAlias("Asia/Hong_Kong", "");
        addAlias("Asia/Irkutsk", "");
        addAlias("Asia/Jakarta", "Java");
        addAlias("Asia/Jakarta", "Sumatra");
        addAlias("Asia/Jayapura", "");
        addAlias("Asia/Jayapura", "Moluccas");
        addAlias("Asia/Jerusalem", "Israel");
        addAlias("Asia/Kabul", "Afghanistan");
        addAlias("Asia/Kamchatka", "");
        addAlias("Asia/Karachi", "Pakistan");
        addAlias("Asia/Katmandu", "Nepal");
        addAlias("Asia/Krasnoyarsk", "");
        addAlias("Asia/Kuala_Lumpur", "Malaysia");
        addAlias("Asia/Kuwait", "");
        addAlias("Asia/Macao", "");
        addAlias("Asia/Magadan", "");
        addAlias("Asia/Manila", "Philippines");
        addAlias("Asia/Muscat", "Oman");
        addAlias("Asia/Nicosia", "Cyprus");
        addAlias("Asia/Novosibirsk", "");
        addAlias("Asia/Phnom_Penh", "Cambodia");
        addAlias("Asia/Pyongyang", "North Korea");
        addAlias("Asia/Qatar", "");
        addAlias("Asia/Rangoon", "Myanmar");
        addAlias("Asia/Rangoon", "Burma");
        addAlias("Asia/Riyadh", "Saudi Arabia");
        addAlias("Asia/Saigon", "Vietnam");
        addAlias("Asia/Seoul", "South Korea");
        addAlias("Asia/Shanghai", "China");
        addAlias("Asia/Singapore", "");
        addAlias("Asia/Taipei", "Taiwan");
        addAlias("Asia/Tashkent", "Uzbekistan");
        addAlias("Asia/Tbilisi", "Georgia");
        addAlias("Asia/Tehran", "Iran");
        addAlias("Asia/Thimbu", "Bhutan");
        addAlias("Asia/Thimphu", "");
        addAlias("Asia/Tokyo", "Japan");
        addAlias("Asia/Ujung_Pandang", "Celebes");
        addAlias("Asia/Ujung_Pandang", "Bali");
        addAlias("Asia/Ujung_Pandang", "West Timor");
        addAlias("Asia/Ulaanbaatar", "Mongolia");
        addAlias("Asia/Ulan_Bator", "");
        addAlias("Asia/Vientiane", "Laos");
        addAlias("Asia/Vladivostok", "");
        addAlias("Asia/Yakutsk", "");
        addAlias("Asia/Yekaterinburg", "");
        addAlias("Asia/Yerevan", "Armenia");
        addAlias("Atlantic/Azores", "");
        addAlias("Atlantic/Bermuda", "");
        addAlias("Atlantic/Canary", "");
        addAlias("Atlantic/Cape_Verde", "");
        addAlias("Atlantic/Faeroe", "Faeroe Islands");
        addAlias("Atlantic/Jan_Mayen", "");
        addAlias("Atlantic/Reykjavik", "Iceland");
        addAlias("Atlantic/South_Georgia", "");
        addAlias("Atlantic/St_Helena", "Saint Helena");
        addAlias("Atlantic/Stanley", "Falkand Islands");
        addAlias("Australia/Adelaide", "");
        addAlias("Australia/Brisbane", "");
        addAlias("Australia/Broken_Hill", "");
        addAlias("Australia/Darwin", "");
        addAlias("Australia/Hobart", "");
        addAlias("Australia/Lord_Howe", "");
        addAlias("Australia/Perth", "");
        addAlias("Australia/Sydney", "");
        addAlias("BET", "");
        addAlias("BST", "");
        addAlias("CAT", "");
        addAlias("CNT", "");
        addAlias("CST", "");
        addAlias("CTT", "");
        addAlias("EAT", "");
        addAlias("ECT", "");
        addAlias("EET", "");
        addAlias("EST", "");
        addAlias("Europe/Amsterdam", "Netherlands");
        addAlias("Europe/Amsterdam", "Holland");
        addAlias("Europe/Andorra", "");
        addAlias("Europe/Athens", "Greece");
        addAlias("Europe/Belgrade", "Serbia");
        addAlias("Europe/Berlin", "Germany");
        addAlias("Europe/Brussels", "Belgium");
        addAlias("Europe/Bucharest", "Romania");
        addAlias("Europe/Budapest", "Hungary");
        addAlias("Europe/Chisinau", "Moldova");
        addAlias("Europe/Copenhagen", "Denmark");
        addAlias("Europe/Dublin", "Ireland");
        addAlias("Europe/Gibraltar", "");
        addAlias("Europe/Helsinki", "Finland");
        addAlias("Europe/Istanbul", "Turkey");
        addAlias("Europe/Kaliningrad", "");
        addAlias("Europe/Kiev", "Ukraine");
        addAlias("Europe/Lisbon", "Portugal");
        addAlias("Europe/London", "England");
        addAlias("Europe/Luxembourg", "");
        addAlias("Europe/Madrid", "Spain");
        addAlias("Europe/Malta", "");
        addAlias("Europe/Minsk", "Belarus");
        addAlias("Europe/Monaco", "");
        addAlias("Europe/Moscow", "");
        addAlias("Europe/Oslo", "Norway");
        addAlias("Europe/Paris", "France");
        addAlias("Europe/Prague", "Czech Republic");
        addAlias("Europe/Riga", "Latvia");
        addAlias("Europe/Rome", "Italy");
        addAlias("Europe/Samara", "");
        addAlias("Europe/Simferopol", "");
        addAlias("Europe/Sofia", "Bulgaria");
        addAlias("Europe/Stockholm", "Sweden");
        addAlias("Europe/Tallinn", "Estonia");
        addAlias("Europe/Tirane", "Albania");
        addAlias("Europe/Vaduz", "Liechtenstein");
        addAlias("Europe/Vienna", "Austria");
        addAlias("Europe/Vilnius", "Lithuania");
        addAlias("Europe/Warsaw", "Poland");
        addAlias("Europe/Warsaw", "Europe/Sarajevo");
        addAlias("Europe/Sarajevo", "Sarajevo");
        addAlias("Europe/Sarajevo", "Bosnia and Herzegovina");
        addAlias("Europe/Sarajevo", "Bosnia");
        addAlias("Europe/Sarajevo", "Herzegovina");
        addAlias("Europe/Sarajevo", "Medjugorje");
        addAlias("Europe/Zurich", "Switzerland");
        addAlias("GMT", "Greenwich Mean Time");
        addAlias("HST", "");
        addAlias("IET", "");
        addAlias("IST", "");
        addAlias("Indian/Antananarivo", "Madagascar");
        addAlias("Indian/Chagos", "British Indian Ocean Territory");
        addAlias("Indian/Christmas", "Christmas Island");
        addAlias("Indian/Cocos", "Cocos Islands");
        addAlias("Indian/Cocos", "Keeling Islands");
        addAlias("Indian/Comoro", "Comoros");
        addAlias("Indian/Kerguelen", "French Southern Territories");
        addAlias("Indian/Mahe", "Seychelles");
        addAlias("Indian/Maldives", "");
        addAlias("Indian/Mauritius", "");
        addAlias("Indian/Mayotte", "");
        addAlias("Indian/Reunion", "");
        addAlias("JST", "");
        addAlias("MET", "");
        addAlias("MIT", "");
        addAlias("MST", "");
        addAlias("NET", "");
        addAlias("NST", "");
        addAlias("PLT", "");
        addAlias("PNT", "");
        addAlias("PRT", "");
        addAlias("PST", "");
        addAlias("Pacific/Apia", "Samoa");
        addAlias("Pacific/Apia", "West Samoa");
        addAlias("Pacific/Auckland", "New Zealand");
        addAlias("Pacific/Chatham", "Chatham Islands");
        addAlias("Pacific/Easter", "");
        addAlias("Pacific/Efate", "Vanuatu");
        addAlias("Pacific/Enderbury", "Phoenix Islands");
        addAlias("Pacific/Fakaofo", "Tokelau");
        addAlias("Pacific/Fiji", "");
        addAlias("Pacific/Funafuti", "Tuvalu");
        addAlias("Pacific/Galapagos", "");
        addAlias("Pacific/Gambier", "");
        addAlias("Pacific/Guadalcanal", "Solomon Islands");
        addAlias("Pacific/Guam", "");
        addAlias("Pacific/Honolulu", "");
        addAlias("Pacific/Kiritimati", "Line Islands");
        addAlias("Pacific/Kosrae", "");
        addAlias("Pacific/Majuro", "");
        addAlias("Pacific/Marquesas", "");
        addAlias("Pacific/Nauru", "");
        addAlias("Pacific/Niue", "");
        addAlias("Pacific/Norfolk", "Norfolk Island");
        addAlias("Pacific/Noumea", "New Caledonia");
        addAlias("Pacific/Pago_Pago", "American Samoa");
        addAlias("Pacific/Palau", "");
        addAlias("Pacific/Pitcairn", "");
        addAlias("Pacific/Ponape", "");
        addAlias("Pacific/Port_Moresby", "");
        addAlias("Pacific/Rarotonga", "");
        addAlias("Pacific/Saipan", "Northern Mariana Islands");
        addAlias("Pacific/Tahiti", "");
        addAlias("Pacific/Tarawa", "Gilbert Islands");
        addAlias("Pacific/Tongatapu", "Tonga");
        addAlias("Pacific/Truk", "");
        addAlias("Pacific/Wake", "");
        addAlias("Pacific/Wallis", "Futuna");
        addAlias("Pacific/Wallis", "Wallis and Futuna");
        addAlias("SST", "");
        addAlias("UTC", "Zulu");
        addAlias("UTC", "UT");
        addAlias("VST", "");
        addAlias("WET", "");
        addAlias("Central", "Alabama");
        addAlias("Central", "Arkansas");
        addAlias("Pacific", "California");
        addAlias("Eastern", "Connecticut");
        addAlias("Eastern", "Delaware");
        addAlias("HST", "Hawaii");
        addAlias("Central", "Illinois");
        addAlias("Central", "Iowa");
        addAlias("Central", "Louisiana");
        addAlias("Eastern", "Maine");
        addAlias("Eastern", "Maryland");
        addAlias("Eastern", "Massachusetts");
        addAlias("Eastern", "Michigan");
        addAlias("Central", "Minnesota");
        addAlias("Central", "Mississippi");
        addAlias("Central", "Missouri");
        addAlias("Pacific", "Nevada");
        addAlias("Eastern", "New Hampshire");
        addAlias("Eastern", "New Jersey");
        addAlias("Mountain", "New Mexico");
        addAlias("Eastern", "North Carolina");
        addAlias("Eastern", "Ohio");
        addAlias("Central", "Oklahoma");
        addAlias("Eastern", "Pennsylvania");
        addAlias("Eastern", "Rhode Island");
        addAlias("Eastern", "South Carolina");
        addAlias("Central", "Texas");
        addAlias("Mountain", "Utah");
        addAlias("Eastern", "Vermont");
        addAlias("Eastern", "Virginia");
        addAlias("Pacific", "Washington");
        addAlias("Eastern", "West Virginia");
        addAlias("Mountain", "Wyoming");
        TimeZoneWrapper timeZoneWrapper = new TimeZoneWrapper("TAI", new SimpleTimeZone(32000, "TAI"));
        addAlias(timeZoneWrapper, "TAI");
        addAlias(timeZoneWrapper, "International Atomic Time");
        TimeZoneWrapper timeZoneWrapper2 = new TimeZoneWrapper("TT", DynamicalTimeTimeZone.INSTANCE);
        addAlias(timeZoneWrapper2, "TT");
        addAlias(timeZoneWrapper2, "Terrestrial Time");
        addAlias(timeZoneWrapper2, "TD");
        addAlias(timeZoneWrapper2, "Dynamical Time");
        addAlias(new TimeZoneWrapper("Ujjain", new SimpleTimeZone(18184400, "Ujjain Mean Time")), "Ujjain");
    }

    public static TimeZone getTimeZone(String str) {
        TimeZoneWrapper timeZoneWrapper = timezones.get(str);
        if (timeZoneWrapper != null) {
            return timeZoneWrapper.getTimeZone();
        }
        return null;
    }

    public static TimeZoneWrapper getTimeZoneWrapper(String str) {
        return timezones.get(str);
    }

    public static Enumeration<String> getTimeZoneNames() {
        return timezones.keys();
    }

    public static class TimeZoneWrapper {
        private String canonicalName;
        private TimeZone tz;

        public TimeZoneWrapper(String str) {
            this.canonicalName = str;
            this.tz = null;
        }

        public TimeZoneWrapper(String str, TimeZone timeZone) {
            this.canonicalName = str;
            this.tz = timeZone;
        }

        public TimeZone getTimeZone() {
            if (this.tz == null) {
                this.tz = TimeZone.getTimeZone(this.canonicalName);
            }
            return this.tz;
        }
    }
}
