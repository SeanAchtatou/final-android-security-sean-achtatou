package frink.date;

import java.util.Vector;

public class BasicDateParserManager implements DateParserManager {
    private Vector<DateParser> parsers = new Vector<>(5);

    public BasicDateParserManager() {
        addParser(new EmptyDateParser());
    }

    public FrinkDate parse(String str) {
        int size = this.parsers.size();
        for (int i = 0; i < size; i++) {
            FrinkDate parse = this.parsers.elementAt(i).parse(str);
            if (parse != null) {
                return parse;
            }
        }
        return null;
    }

    public void addParser(DateParser dateParser) {
        this.parsers.addElement(dateParser);
    }

    public DateParser getParser(String str) {
        int size = this.parsers.size();
        for (int i = 0; i < size; i++) {
            DateParser elementAt = this.parsers.elementAt(i);
            if (elementAt.getName().equals(str)) {
                return elementAt;
            }
        }
        return null;
    }
}
