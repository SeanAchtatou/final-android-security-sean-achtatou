package frink.date;

import frink.errors.Cat;
import frink.errors.ConformanceException;
import frink.errors.Log;
import frink.errors.NotRealException;
import frink.errors.Sev;
import frink.expr.BasicDateExpression;
import frink.expr.BasicStringExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.DateExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.EnumerationWrapper;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.InvalidArgumentException;
import frink.expr.StringExpression;
import frink.expr.StringExpressionFactory;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.ValueNotFoundException;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.DoubleArgFunction;
import frink.function.SingleArgFunction;
import frink.function.ZeroArgFunction;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkReal;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.RealInterval;
import frink.units.Unit;
import frink.units.UnitManager;
import frink.units.UnitMath;
import java.util.Calendar;

public class DateFunctionSource extends BasicFunctionSource {
    /* access modifiers changed from: private */
    public Unit DAY = null;
    private Unit SECOND = null;

    public DateFunctionSource() {
        super("DateFunctionSource");
        initializeFunctions();
    }

    private void initializeFunctions() {
        addFunctionDefinition("timezones", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new EnumerationWrapper(TimeZoneSource.getTimeZoneNames(), StringExpressionFactory.INSTANCE);
            }
        });
        addFunctionDefinition("timezone", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new BasicStringExpression(environment.getDefaultTimeZone().getID());
            }
        });
        addFunctionDefinition("setDefaultTimezone", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, FrinkSecurityException {
                if (expression instanceof StringExpression) {
                    environment.getSecurityHelper().checkSetGlobalFlag();
                    environment.setDefaultTimeZone(((StringExpression) expression).getString());
                    return VoidExpression.VOID;
                }
                throw new InvalidArgumentException("Argument to setDefaultTimezone must be a string.", expression);
            }
        });
        addFunctionDefinition("now", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment) throws EvaluationException {
                return new BasicDateExpression(new CalendarDate(Calendar.getInstance()));
            }
        });
        addFunctionDefinition("JD", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof DateExpression) {
                    if (DateFunctionSource.this.DAY == null) {
                        DateFunctionSource.this.initDay(environment, this);
                    }
                    return BasicUnitExpression.construct(UnitMath.multiply(DimensionlessUnitExpression.construct(((DateExpression) expression).getFrinkDate().getJulian()), DateFunctionSource.this.DAY));
                }
                if (expression instanceof UnitExpression) {
                    if (DateFunctionSource.this.DAY == null) {
                        DateFunctionSource.this.initDay(environment, this);
                    }
                    Unit divide = UnitMath.divide(((UnitExpression) expression).getUnit(), DateFunctionSource.this.DAY);
                    if (UnitMath.isDimensionless(divide)) {
                        Numeric scale = divide.getScale();
                        if (scale.isReal()) {
                            return new BasicDateExpression(new JulianDate((FrinkReal) scale));
                        }
                    }
                }
                throw new InvalidArgumentException("Argument to JD[x] must be a date or a unit with dimensions of time.", expression);
            }
        });
        addFunctionDefinition("MJD", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof DateExpression) {
                    if (DateFunctionSource.this.DAY == null) {
                        DateFunctionSource.this.initDay(environment, this);
                    }
                    return BasicUnitExpression.construct(UnitMath.multiply(DimensionlessUnitExpression.construct(NumericMath.subtract(((DateExpression) expression).getFrinkDate().getJulian(), JulianDateFormatterSource.MJDOffset)), DateFunctionSource.this.DAY));
                }
                if (expression instanceof UnitExpression) {
                    if (DateFunctionSource.this.DAY == null) {
                        DateFunctionSource.this.initDay(environment, this);
                    }
                    Unit divide = UnitMath.divide(((UnitExpression) expression).getUnit(), DateFunctionSource.this.DAY);
                    if (UnitMath.isDimensionless(divide)) {
                        Numeric add = NumericMath.add(divide.getScale(), JulianDateFormatterSource.MJDOffset);
                        if (add.isReal()) {
                            return new BasicDateExpression(new JulianDate((FrinkReal) add));
                        }
                    }
                }
                throw new InvalidArgumentException("Argument to MJD[x] must be a date or unit with dimensions of time.", expression);
            }
        });
        addFunctionDefinition("JDE", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof DateExpression) {
                    if (DateFunctionSource.this.DAY == null) {
                        DateFunctionSource.this.initDay(environment, this);
                    }
                    FrinkDate frinkDate = ((DateExpression) expression).getFrinkDate();
                    try {
                        return BasicUnitExpression.construct(UnitMath.add(UnitMath.multiply(DimensionlessUnitExpression.construct(frinkDate.getJulian()), DateFunctionSource.this.DAY), DateFunctionSource.this.getDeltaTAsUnit(frinkDate, environment, this)));
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException("JDE[x]: Data error: 'day' and 's' (indicating second) don't have the same dimensions in the data file.", this);
                    }
                } else {
                    if (expression instanceof UnitExpression) {
                        try {
                            FrinkReal frinkRealValue = UnitMath.getFrinkRealValue(((UnitExpression) expression).getUnit());
                            return new BasicDateExpression(new JulianDate(frinkRealValue.doubleValue() - (DynamicalTime.getDeltaT(frinkRealValue) / 86400.0d)));
                        } catch (NotRealException e2) {
                        }
                    }
                    throw new InvalidArgumentException("Argument to JDE[x] must be a date or a dimensionless number.", expression);
                }
            }
        });
        addFunctionDefinition("deltaT", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof DateExpression) {
                    return BasicUnitExpression.construct(DateFunctionSource.this.getDeltaTAsUnit(((DateExpression) expression).getFrinkDate(), environment, this));
                }
                throw new InvalidArgumentException("Argument to deltaT[x] must be a date.", expression);
            }
        });
        addFunctionDefinition("TAIMinusUTC", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException, NumericException {
                if (expression instanceof DateExpression) {
                    return BasicUnitExpression.construct(DateFunctionSource.this.getTAIMinusUTCAsUnit(((DateExpression) expression).getFrinkDate(), environment, this));
                }
                throw new InvalidArgumentException("Argument to TAIMinusUTC[x] must be a date.", expression);
            }
        });
        addFunctionDefinition("parseDate", new SingleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws EvaluationException {
                if (expression instanceof StringExpression) {
                    FrinkDate parse = environment.getDateParserManager().parse(((StringExpression) expression).getString().trim());
                    if (parse == null) {
                        return UndefExpression.UNDEF;
                    }
                    return new BasicDateExpression(parse);
                }
                throw new InvalidArgumentException("Argument to parseDate[str] must be a string with a valid date format.", expression);
            }
        });
        addFunctionDefinition("subtractLeap", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, NumericException {
                if (!(expression instanceof DateExpression) || !(expression2 instanceof DateExpression)) {
                    throw new InvalidArgumentException("Arguments to subtractLeap[d2, d1] must be dates.", null);
                }
                try {
                    return BasicUnitExpression.construct(DateFunctionSource.this.subtractWithLeapSeconds(((DateExpression) expression).getFrinkDate(), ((DateExpression) expression2).getFrinkDate(), environment));
                } catch (ConformanceException e) {
                    throw new EvaluationConformanceException("subtractLeap: Unexpected ConformanceException: " + e.getMessage(), expression);
                }
            }
        });
        addFunctionDefinition("addLeap", new DoubleArgFunction(true) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, NumericException {
                if (!(expression instanceof DateExpression) || !(expression2 instanceof UnitExpression)) {
                    throw new InvalidArgumentException("Arguments to addLeap[date, offset] must be date and offset.", null);
                }
                try {
                    return new BasicDateExpression(DateFunctionSource.this.addWithLeapSeconds(((DateExpression) expression).getFrinkDate(), ((UnitExpression) expression2).getUnit(), environment));
                } catch (ConformanceException e) {
                    throw new EvaluationConformanceException("addLeap: Unexpected ConformanceException: " + e.getMessage(), expression);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public Unit getDeltaTAsUnit(FrinkDate frinkDate, Environment environment, Expression expression) throws EvaluationException, NumericException {
        Numeric construct;
        if (this.SECOND == null) {
            initSecond(environment, expression);
        }
        Numeric julian = frinkDate.getJulian();
        if (julian.isInterval()) {
            RealInterval realInterval = (RealInterval) julian;
            FrinkReal main = realInterval.getMain();
            if (main == null) {
                construct = RealInterval.construct(new FrinkFloat(DynamicalTime.getDeltaT(realInterval.getLower())), new FrinkFloat(DynamicalTime.getDeltaT(realInterval.getUpper())));
            } else {
                construct = RealInterval.construct(new FrinkFloat(DynamicalTime.getDeltaT(realInterval.getLower())), new FrinkFloat(DynamicalTime.getDeltaT(main)), new FrinkFloat(DynamicalTime.getDeltaT(realInterval.getUpper())));
            }
            return UnitMath.multiply(DimensionlessUnitExpression.construct(construct), this.SECOND);
        } else if (julian.isReal()) {
            return UnitMath.multiply(DimensionlessUnitExpression.construct(DynamicalTime.getDeltaT((FrinkReal) julian)), this.SECOND);
        } else {
            System.err.println("DateFunctionSource:  Passed unexpected date.");
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Unit getTAIMinusUTCAsUnit(FrinkDate frinkDate, Environment environment, Expression expression) throws EvaluationException, NumericException {
        Numeric construct;
        if (this.SECOND == null) {
            initSecond(environment, expression);
        }
        Numeric julian = frinkDate.getJulian();
        if (julian.isInterval()) {
            RealInterval realInterval = (RealInterval) julian;
            FrinkReal main = realInterval.getMain();
            if (main == null) {
                construct = RealInterval.construct(LeapSeconds.getTAIMinusUTC(realInterval.getLower()), LeapSeconds.getTAIMinusUTC(realInterval.getUpper()));
            } else {
                construct = RealInterval.construct(LeapSeconds.getTAIMinusUTC(realInterval.getLower()), LeapSeconds.getTAIMinusUTC(main), LeapSeconds.getTAIMinusUTC(realInterval.getUpper()));
            }
            return UnitMath.multiply(DimensionlessUnitExpression.construct(construct), this.SECOND);
        } else if (julian.isReal()) {
            return UnitMath.multiply(DimensionlessUnitExpression.construct(LeapSeconds.getTAIMinusUTC((FrinkReal) julian)), this.SECOND);
        } else {
            System.err.println("DateFunctionSource:  Passed unexpected date.");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void initDay(Environment environment, Expression expression) throws EvaluationException {
        if (this.DAY == null) {
            this.DAY = environment.getUnitManager().getUnit("day");
            if (this.DAY == null) {
                Log.message("DateFunctionSource.dayNotFound", Sev.UNRECOVERABLE_ERROR, Cat.Units, "DateFunctionSource could not find a needed unit called 'day'.");
                throw new ValueNotFoundException("DateFunctionSource could not find a needed unit called 'day'.", expression);
            }
        }
    }

    private synchronized void initSecond(Environment environment, Expression expression) throws EvaluationException {
        if (this.SECOND == null) {
            this.SECOND = environment.getUnitManager().getUnit("s");
            if (this.SECOND == null) {
                Log.message("DateFunctionSource.secondNotFound", Sev.UNRECOVERABLE_ERROR, Cat.Units, "DateFunctionSource could not find a needed unit called 'second'.");
                throw new ValueNotFoundException("DateFunctionSource could not find a needed unit called 's' (for second.)", expression);
            }
        }
    }

    public Unit subtractWithLeapSeconds(FrinkDate frinkDate, FrinkDate frinkDate2, Environment environment) throws NumericException, EvaluationException, ConformanceException {
        return UnitMath.add(DateMath.subtract(frinkDate, frinkDate2, environment.getUnitManager()), UnitMath.subtract(getTAIMinusUTCAsUnit(frinkDate, environment, null), getTAIMinusUTCAsUnit(frinkDate2, environment, null)));
    }

    public FrinkDate addWithLeapSeconds(FrinkDate frinkDate, Unit unit, Environment environment) throws NumericException, EvaluationException, ConformanceException {
        UnitManager unitManager = environment.getUnitManager();
        FrinkDate add = DateMath.add(frinkDate, unit, unitManager);
        return DateMath.add(add, UnitMath.subtract(getTAIMinusUTCAsUnit(frinkDate, environment, null), getTAIMinusUTCAsUnit(add, environment, null)), unitManager);
    }
}
