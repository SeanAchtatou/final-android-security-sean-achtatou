package frink.date;

import frink.numeric.FrinkReal;

public class DynamicalTime {
    private static final double DAYS_IN_CENTURY = 36524.219878100004d;
    private static final double DAYS_IN_YEAR = 365.242198781d;
    private static final double[] OFFSETS = {121.0d, 112.0d, 102.0d, 95.0d, 88.0d, 82.0d, 77.0d, 72.0d, 68.0d, 63.0d, 60.0d, 56.0d, 53.0d, 51.0d, 48.0d, 46.0d, 44.0d, 42.0d, 40.0d, 38.0d, 35.0d, 33.0d, 31.0d, 29.0d, 26.0d, 24.0d, 22.0d, 20.0d, 18.0d, 16.0d, 14.0d, 12.0d, 11.0d, 10.0d, 9.0d, 8.0d, 7.0d, 7.0d, 7.0d, 7.0d, 7.0d, 7.0d, 8.0d, 8.0d, 9.0d, 9.0d, 9.0d, 9.0d, 9.0d, 10.0d, 10.0d, 10.0d, 10.0d, 10.0d, 10.0d, 10.0d, 10.0d, 11.0d, 11.0d, 11.0d, 11.0d, 11.0d, 12.0d, 12.0d, 12.0d, 12.0d, 13.0d, 13.0d, 13.0d, 14.0d, 14.0d, 14.0d, 14.0d, 15.0d, 15.0d, 15.0d, 15.0d, 15.0d, 16.0d, 16.0d, 16.0d, 16.0d, 16.0d, 16.0d, 16.0d, 16.0d, 15.0d, 15.0d, 14.0d, 13.0d, 13.1d, 12.5d, 12.2d, 12.0d, 12.0d, 12.0d, 12.0d, 12.0d, 12.0d, 11.9d, 11.6d, 11.0d, 10.2d, 9.2d, 8.2d, 7.1d, 6.2d, 5.6d, 5.4d, 5.3d, 5.4d, 5.6d, 5.9d, 6.2d, 6.5d, 6.8d, 7.1d, 7.3d, 7.5d, 7.6d, 7.7d, 7.3d, 6.2d, 5.2d, 2.7d, 1.4d, -1.2d, -2.8d, -3.8d, -4.8d, -5.5d, -5.3d, -5.6d, -5.7d, -5.9d, -6.0d, -6.3d, -6.5d, -6.2d, -4.7d, -2.8d, -0.1d, 2.6d, 5.3d, 7.7d, 10.4d, 13.3d, 16.0d, 18.2d, 20.2d, 21.1d, 22.4d, 23.5d, 23.8d, 24.3d, 24.0d, 23.9d, 23.9d, 23.7d, 24.0d, 24.3d, 25.3d, 26.2d, 27.3d, 28.2d, 29.1d, 30.0d, 30.7d, 31.4d, 32.2d, 33.1d, 34.0d, 35.0d, 36.5d, 38.3d, 40.2d, 42.2d, 44.5d, 46.5d, 48.5d, 50.5d, 52.2d, 53.8d, 54.9d, 55.8d, 56.9d, 58.3d, 60.0d, 61.6d, 63.0d};
    private static final double YEAR_1820 = 2385800.5d;
    private static final double YEAR_2000 = 2451544.5d;
    private static final double YEAR_2050 = 2469807.5d;
    private static final double YEAR_2100 = 2488069.5d;
    private static final double YEAR_2150 = 2506331.5d;

    public static double getDeltaT(FrinkReal frinkReal) {
        return getDeltaT(frinkReal.doubleValue());
    }

    public static double getDeltaT(double d) {
        if (d < 2455927.5d) {
            if (d > 2450814.5d) {
                return interpolateClose(d);
            }
            if (d > 2312752.5d) {
                return interpolate10A(d);
            }
            if (d < 2067309.5d) {
                return pre948(d);
            }
            return otherYears(d);
        } else if (d < YEAR_2050) {
            return deltaTBefore2050(d);
        } else {
            if (d < YEAR_2150) {
                return deltaT2050to2150(d);
            }
            return deltaTAfter2150(d);
        }
    }

    public static double deltaTBefore2050(double d) {
        double d2 = (d - YEAR_2000) / DAYS_IN_YEAR;
        return (d2 * 0.005589d * d2) + 62.92d + (0.32217d * d2);
    }

    public static double deltaT2050to2150(double d) {
        double d2 = (YEAR_2150 - d) / DAYS_IN_YEAR;
        double d3 = (d - YEAR_1820) / DAYS_IN_CENTURY;
        return ((d3 * (32.0d * d3)) - 0.21875d) - (d2 * 0.5628d);
    }

    public static double deltaTAfter2150(double d) {
        double d2 = (d - YEAR_1820) / DAYS_IN_CENTURY;
        return (d2 * (32.0d * d2)) - 0.21875d;
    }

    private static double interpolateClose(double d) {
        if (d > 2454832.5d) {
            return 66.184d;
        }
        if (d > 2453736.5d) {
            return 65.184d;
        }
        if (d > 2452640.5d) {
            return 64.184d;
        }
        if (d > 2452275.5d) {
            return 64.3d;
        }
        if (d > 2451910.5d) {
            return 64.09d;
        }
        if (d > YEAR_2000) {
            return 63.83d;
        }
        if (d > 2451179.5d) {
            return 63.46d;
        }
        return 62.97d;
    }

    private static double pre948(double d) {
        double meeust = getMeeust(d);
        return Math.floor(((meeust * (497.0d + (44.1d * meeust))) + 2177.0d) * 1000.0d) / 1000.0d;
    }

    private static double otherYears(double d) {
        double meeust = getMeeust(d);
        double d2 = (meeust * ((25.3d * meeust) + 102.0d)) + 102.0d;
        if (d > YEAR_2000 && d < YEAR_2100) {
            d2 += 0.37d * ((d - YEAR_2100) / DAYS_IN_YEAR);
        }
        return Math.floor(d2 * 1000.0d) / 1000.0d;
    }

    private static double interpolate10A(double d) {
        double d2 = (d - 2312752.5d) / 730.4867724867725d;
        int floor = (int) Math.floor(d2);
        double d3 = d2 - ((double) floor);
        double d4 = OFFSETS[floor];
        return Math.floor(((d3 * (OFFSETS[floor + 1] - d4)) + d4) * 1000.0d) / 1000.0d;
    }

    public static double getMeeust(double d) {
        return (d - YEAR_2000) / DAYS_IN_CENTURY;
    }
}
