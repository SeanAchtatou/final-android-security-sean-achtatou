package frink.date;

import frink.date.TimeZoneSource;
import frink.expr.Environment;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimeZone;

public class TimeZoneDateFormatterSource implements FrinkDateFormatterSource {
    private Hashtable<String, TZFormatter> timezones = new Hashtable<>();

    public TimeZoneDateFormatterSource() {
        Enumeration<String> timeZoneNames = TimeZoneSource.getTimeZoneNames();
        while (timeZoneNames.hasMoreElements()) {
            String nextElement = timeZoneNames.nextElement();
            this.timezones.put(nextElement, new TZFormatter(TimeZoneSource.getTimeZoneWrapper(nextElement)));
        }
    }

    public String getName() {
        return "TimezoneDateFormatterSource";
    }

    public FrinkDateFormatter getFormatter(String str) {
        return this.timezones.get(str);
    }

    private static class TZFormatter implements FrinkDateFormatter {
        private TimeZoneSource.TimeZoneWrapper tzw;

        private TZFormatter(TimeZoneSource.TimeZoneWrapper timeZoneWrapper) {
            this.tzw = timeZoneWrapper;
        }

        public String format(FrinkDate frinkDate, TimeZone timeZone, Environment environment) {
            return environment.getOutputDateFormatter().format(frinkDate, this.tzw.getTimeZone(), environment);
        }
    }
}
