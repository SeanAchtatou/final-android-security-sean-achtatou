package frink.date;

import frink.numeric.Numeric;

public class NegativeJulianDate implements FrinkDate {
    private Numeric julian;

    public NegativeJulianDate(Numeric numeric) {
        this.julian = numeric;
    }

    public Numeric getJulian() {
        return this.julian;
    }
}
