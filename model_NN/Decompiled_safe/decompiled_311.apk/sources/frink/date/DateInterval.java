package frink.date;

import frink.numeric.FrinkReal;
import frink.numeric.Interval;
import frink.numeric.NotImplementedException;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.RealInterval;
import frink.numeric.RealMath;

public class DateInterval implements FrinkDate, Interval<FrinkDate> {
    private FrinkDate lower;
    private FrinkDate main;
    private FrinkDate upper;

    public DateInterval(RealInterval realInterval) {
        this.lower = new CalendarDate(realInterval.getLower());
        if (realInterval.getMain() != null) {
            this.main = new CalendarDate(realInterval.getMain());
        }
        this.upper = new CalendarDate(realInterval.getUpper());
    }

    private DateInterval(FrinkDate frinkDate, FrinkDate frinkDate2) {
        this.lower = frinkDate;
        this.upper = frinkDate2;
        this.main = null;
    }

    private DateInterval(FrinkDate frinkDate, FrinkDate frinkDate2, FrinkDate frinkDate3) {
        this.lower = frinkDate;
        this.main = frinkDate2;
        this.upper = frinkDate3;
    }

    public static FrinkDate construct(Numeric numeric, Numeric numeric2) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return construct((FrinkReal) numeric, (FrinkReal) numeric2);
        }
        throw new NotImplementedException("DateInterval.construct passed non-real values.", true);
    }

    public static FrinkDate construct(Numeric numeric, Numeric numeric2, Numeric numeric3) throws NumericException {
        if (numeric.isReal() && numeric3.isReal() && (numeric2 == null || numeric2.isReal())) {
            return construct((FrinkReal) numeric, (FrinkReal) numeric2, (FrinkReal) numeric3);
        }
        throw new NotImplementedException("DateInterval.construct passed non-real values.", true);
    }

    public static FrinkDate construct(FrinkReal frinkReal, FrinkReal frinkReal2) throws NumericException {
        switch (RealMath.compare(frinkReal, frinkReal2)) {
            case -1:
                return new DateInterval(new CalendarDate(frinkReal), new CalendarDate(frinkReal2));
            case 0:
                return new CalendarDate(frinkReal);
            case 1:
                return new DateInterval(new CalendarDate(frinkReal2), new CalendarDate(frinkReal));
            default:
                return null;
        }
    }

    public static FrinkDate construct(FrinkReal frinkReal, FrinkReal frinkReal2, FrinkReal frinkReal3) throws NumericException {
        FrinkReal frinkReal4;
        FrinkReal frinkReal5;
        if (frinkReal2 == null) {
            return construct(frinkReal, frinkReal3);
        }
        int compare = RealMath.compare(frinkReal, frinkReal3);
        switch (compare) {
            case -1:
                frinkReal4 = frinkReal3;
                frinkReal5 = frinkReal;
                break;
            case 0:
                if (RealMath.compare(frinkReal, frinkReal2) == 0) {
                    return new CalendarDate(frinkReal);
                }
                throw new NotImplementedException("DateInterval::construct: main value does not lie between bounds", true);
            case 1:
                frinkReal4 = frinkReal;
                frinkReal5 = frinkReal3;
                break;
            default:
                throw new NotImplementedException("DateInterval::construct: compare returned invalid value " + compare, true);
        }
        if (RealMath.compare(frinkReal5, frinkReal2) <= 0 && RealMath.compare(frinkReal2, frinkReal4) <= 0) {
            return new DateInterval(new CalendarDate(frinkReal5), new CalendarDate(frinkReal2), new CalendarDate(frinkReal4));
        }
        throw new NotImplementedException("DateInterval::construct: main value does not lie between bounds", true);
    }

    public FrinkDate getLower() {
        return this.lower;
    }

    public FrinkDate getUpper() {
        return this.upper;
    }

    public FrinkDate getMain() {
        return this.main;
    }

    public Numeric getJulian() {
        try {
            if (this.main == null) {
                return RealInterval.construct(this.lower.getJulian(), this.upper.getJulian());
            }
            return RealInterval.construct(this.lower.getJulian(), this.main.getJulian(), this.upper.getJulian());
        } catch (NumericException e) {
            System.err.println("DateInterval.getJulian threw error: " + e);
            return null;
        }
    }
}
