package frink.date;

import frink.expr.OperatorExpression;
import java.util.Date;
import java.util.TimeZone;

public class DynamicalTimeTimeZone extends TimeZone {
    public static final DynamicalTimeTimeZone INSTANCE = new DynamicalTimeTimeZone();
    private Date cache = null;

    private DynamicalTimeTimeZone() {
        setID("TD");
    }

    public boolean inDaylightTime(Date date) {
        this.cache = date;
        return true;
    }

    public boolean useDaylightTime() {
        return true;
    }

    public int getRawOffset() {
        return 0;
    }

    public int getOffset(long j) {
        return (int) (DynamicalTime.getDeltaT(2440587.5d + ((double) (j / 86400000))) * 1000.0d);
    }

    public int getDSTSavings() {
        return getOffset(this.cache.getTime());
    }

    public void setRawOffset(int i) {
        System.out.println("Something called DynamicalTimeTimeZone.setRawOffset!");
    }

    public Object clone() {
        System.out.println("Something called DynamicalTimeTimeZone.clone!");
        return this;
    }

    public int getOffset(int i, int i2, int i3, int i4, int i5, int i6) {
        int i7 = i3 + 1;
        int i8 = i2 + 8000;
        if (i7 < 3) {
            i8--;
            i7 += 12;
        }
        return (int) (DynamicalTime.getDeltaT(((((((double) ((i8 / OperatorExpression.PREC_ADD) + (((i8 * 365) + (i8 / 4)) - (i8 / 100)))) - 1200820.5d) + ((double) (((i7 * 153) + 3) / 5))) - 92.0d) + ((double) i4)) - 1.0d) * 1000.0d);
    }
}
