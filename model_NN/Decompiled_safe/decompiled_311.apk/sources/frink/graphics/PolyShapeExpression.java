package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.BasicListExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.FunctionSource;
import frink.function.TwoArgMethod;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import frink.units.Unit;

public class PolyShapeExpression implements Drawable, GraphicsExpression, FrinkObject, DeferredDrawable {
    public static final String TYPE = "Poly";
    private static final PolyShapeFunctionSource methods = new PolyShapeFunctionSource();
    private boolean closed;
    private EmptyObjectContextFrame contextFrame = null;
    private boolean filled;
    private FrinkPointList points = new FrinkPointList();

    public PolyShapeExpression(boolean z, boolean z2) {
        this.closed = z;
        this.filled = z2;
    }

    public void addPoint(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        this.points.addPoint(unit, unit2);
    }

    public int getChildCount() {
        return this.points.getPointCount();
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i >= this.points.getPointCount()) {
            throw new InvalidChildException("Invalid child " + i + " requested for PolyShapeExpression.", this);
        }
        BasicListExpression basicListExpression = new BasicListExpression(2);
        FrinkPoint point = this.points.getPoint(i);
        basicListExpression.appendChild(BasicUnitExpression.construct(point.getX()));
        basicListExpression.appendChild(BasicUnitExpression.construct(point.getY()));
        return basicListExpression;
    }

    public void setStrokeWidth(Unit unit) {
        this.points.setStrokeWidth(unit, this.filled);
    }

    public BoundingBox getBoundingBox() {
        return this.points.getBoundingBox();
    }

    public Drawable getDrawable() {
        return this;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression.getExpressionType() == getExpressionType()) {
            try {
                int pointCount = this.points.getPointCount();
                for (int i = 0; i < pointCount; i++) {
                    if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                        return false;
                    }
                }
                return true;
            } catch (InvalidChildException e) {
            }
        }
        return false;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return methods;
    }

    public boolean isA(String str) {
        return (this.filled && str.equals("polygon")) || (!this.filled && str.equals("polyline"));
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.drawPoly(this.points, this.closed, this.filled);
    }

    public String getExpressionType() {
        return TYPE;
    }

    private static class PolyShapeFunctionSource extends BasicFunctionSource {
        PolyShapeFunctionSource() {
            super("PolyShapeExpression");
            addFunctionDefinition("addPoint", new TwoArgMethod<PolyShapeExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, PolyShapeExpression polyShapeExpression, Expression expression, Expression expression2) throws EvaluationException {
                    try {
                        polyShapeExpression.addPoint(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
        }
    }
}
