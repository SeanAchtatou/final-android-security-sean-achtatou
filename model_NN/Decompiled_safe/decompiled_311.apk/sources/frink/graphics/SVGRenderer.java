package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.graphics.EllipticalArcPathSegment;
import frink.numeric.FrinkInt;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.text.SGMLUtils;
import frink.units.BasicUnit;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitMath;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class SVGRenderer extends AbstractGraphicsView {
    private static final Unit defaultViewSize = DimensionlessUnit.construct(1000);
    private static final DimensionlessUnit roundTo = DimensionlessUnit.construct(1.0E-6d);
    private static final int viewSize = 1000;
    private FrinkColor background;
    private BoundingBox bbox;
    private FrinkColor color;
    private Environment env;
    private Unit fontHeight;
    private String fontName;
    private int fontStyle;
    private Unit hScale;
    private Unit height;
    private Object outTo;
    private boolean renderBackgroundTransparent;
    private Unit resolution;
    private Unit strokeWidth;
    private Unit vScale;
    private Unit width;
    private PrintWriter writer;

    public SVGRenderer(Environment environment, ImageFileArguments imageFileArguments) throws FrinkSecurityException {
        this(environment, imageFileArguments.width, imageFileArguments.height, imageFileArguments.outTo, imageFileArguments.format, imageFileArguments.backgroundTransparent);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private SVGRenderer(Environment environment, Unit unit, Unit unit2, Object obj, String str, boolean z) throws FrinkSecurityException {
        File file;
        this.env = environment;
        this.renderBackgroundTransparent = z;
        this.width = unit;
        this.height = unit2;
        this.fontName = null;
        this.fontHeight = null;
        this.fontStyle = 0;
        this.strokeWidth = null;
        try {
            this.bbox = new BoundingBox(new BasicUnit(FrinkInt.ZERO, this.width.getDimensionList()), new BasicUnit(FrinkInt.ZERO, this.height.getDimensionList()), this.width, this.height);
        } catch (ConformanceException | NumericException | OverlapException e) {
        }
        this.resolution = GraphicsUtils.get72PerInch(environment);
        if (obj instanceof File) {
            File possiblyFixRelativePath = GraphicsUtils.possiblyFixRelativePath((File) obj);
            environment.getSecurityHelper().checkWrite(possiblyFixRelativePath);
            file = possiblyFixRelativePath;
        } else {
            file = obj;
        }
        this.outTo = file;
        this.color = BasicFrinkColor.BLACK;
        this.writer = null;
    }

    public FrinkColor getColor() {
        return this.color;
    }

    public void setColor(FrinkColor frinkColor) {
        this.color = frinkColor;
    }

    public FrinkColor getBackgroundColor() {
        return this.background;
    }

    public void setBackgroundColor(FrinkColor frinkColor) {
        this.background = frinkColor;
        if (!this.renderBackgroundTransparent) {
        }
    }

    public Unit getDeviceResolution() {
        return this.resolution;
    }

    public BoundingBox getRendererBoundingBox() {
        return this.bbox;
    }

    public void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        this.writer.println("  <line x1=" + qCoord(unit, this.env) + " y1=" + qCoord(unit2, this.env) + " x2=" + qCoord(unit3, this.env) + " y2=" + qCoord(unit4, this.env) + getColor(this.color, false, false, this.strokeWidth, this.env) + " />");
    }

    public void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        this.writer.println("  <rect x=" + qCoord(unit, this.env) + " y=" + qCoord(unit2, this.env) + " width=" + qCoord(unit3, this.env) + " height=" + qCoord(unit4, this.env) + getColor(this.color, true, z, this.strokeWidth, this.env) + " />");
    }

    public void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        String color2 = getColor(this.color, true, z, this.strokeWidth, this.env);
        try {
            Unit divide = UnitMath.divide(unit3, DimensionlessUnit.TWO);
            Unit divide2 = UnitMath.divide(unit4, DimensionlessUnit.TWO);
            this.writer.println("  <ellipse cx=" + qCoord(UnitMath.add(unit, divide), this.env) + " cy=" + qCoord(UnitMath.add(unit2, divide2), this.env) + " rx=" + qCoord(divide, this.env) + " ry=" + qCoord(divide2, this.env) + color2 + " />");
        } catch (NumericException e) {
            this.env.outputln("SVGRenderer:  NumericException when writing ellipse:\n " + e);
        } catch (ConformanceException e2) {
            this.env.outputln("SVGRenderer:  ConformanceException when writing ellipse:\n " + e2);
        }
    }

    public void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2) {
        String str;
        String color2 = getColor(this.color, z, z2, this.strokeWidth, this.env);
        if (z) {
            str = "polygon";
        } else {
            str = "polyline";
        }
        this.writer.print(" <" + str + color2 + "\n    points=\"");
        int pointCount = frinkPointList.getPointCount();
        for (int i = 0; i < pointCount; i++) {
            FrinkPoint point = frinkPointList.getPoint(i);
            this.writer.print(coordNoUnits(point.getX(), this.hScale) + "," + coordNoUnits(point.getY(), this.vScale) + " ");
        }
        this.writer.println("\" />");
    }

    public void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z) {
        int i;
        int i2;
        this.writer.print(" <path fill-rule=\"evenodd\" " + getColor(this.color, false, z, this.strokeWidth, this.env) + "\n d=\"");
        int segmentCount = frinkGeneralPath.getSegmentCount();
        for (int i3 = 0; i3 < segmentCount; i3++) {
            try {
                PathSegment segment = frinkGeneralPath.getSegment(i3);
                String segmentType = segment.getSegmentType();
                if (segmentType == PathSegment.TYPE_LINE) {
                    FrinkPoint point = segment.getPoint(0);
                    this.writer.print("L " + coordNoUnits(point.getX(), this.hScale) + " " + coordNoUnits(point.getY(), this.vScale) + " ");
                } else if (segmentType == PathSegment.TYPE_MOVE_TO) {
                    FrinkPoint point2 = segment.getPoint(0);
                    this.writer.print("M " + coordNoUnits(point2.getX(), this.hScale) + " " + coordNoUnits(point2.getY(), this.vScale) + " ");
                } else if (segmentType == PathSegment.TYPE_QUADRATIC) {
                    FrinkPoint point3 = segment.getPoint(0);
                    FrinkPoint point4 = segment.getPoint(1);
                    this.writer.print("Q " + coordNoUnits(point3.getX(), this.hScale) + " " + coordNoUnits(point3.getY(), this.vScale) + " " + coordNoUnits(point4.getX(), this.hScale) + " " + coordNoUnits(point4.getY(), this.vScale) + " ");
                } else if (segmentType == PathSegment.TYPE_CUBIC) {
                    FrinkPoint point5 = segment.getPoint(0);
                    FrinkPoint point6 = segment.getPoint(1);
                    FrinkPoint point7 = segment.getPoint(2);
                    this.writer.print("C " + coordNoUnits(point5.getX(), this.hScale) + " " + coordNoUnits(point5.getY(), this.vScale) + " " + coordNoUnits(point6.getX(), this.hScale) + " " + coordNoUnits(point6.getY(), this.vScale) + " " + coordNoUnits(point7.getX(), this.hScale) + " " + coordNoUnits(point7.getY(), this.vScale) + " ");
                } else if (segmentType == PathSegment.TYPE_ELLIPSE) {
                    FrinkPoint point8 = segment.getPoint(0);
                    FrinkPoint point9 = segment.getPoint(1);
                    Unit x = point8.getX();
                    Unit y = point8.getY();
                    Unit x2 = point9.getX();
                    Unit y2 = point9.getY();
                    Unit subtract = UnitMath.subtract(x2, x);
                    Unit subtract2 = UnitMath.subtract(y2, y);
                    Unit divide = UnitMath.divide(subtract, DimensionlessUnit.TWO);
                    Unit divide2 = UnitMath.divide(subtract2, DimensionlessUnit.TWO);
                    Unit divide3 = UnitMath.divide(UnitMath.add(y, y2), DimensionlessUnit.TWO);
                    this.writer.print("M " + coordNoUnits(x2, this.hScale) + " " + coordNoUnits(divide3, this.vScale) + " " + "A " + coordNoUnits(divide, this.hScale) + " " + coordNoUnits(divide2, this.vScale) + " " + "0 0 0 " + coordNoUnits(x, this.hScale) + " " + coordNoUnits(divide3, this.vScale) + " " + "A " + coordNoUnits(divide, this.hScale) + " " + coordNoUnits(divide2, this.vScale) + " " + "0 0 0 " + coordNoUnits(x2, this.hScale) + " " + coordNoUnits(divide3, this.vScale) + " ");
                } else if (segmentType == PathSegment.TYPE_ELLIPTICAL_ARC) {
                    FrinkPoint point10 = segment.getPoint(2);
                    FrinkPoint point11 = segment.getPoint(3);
                    FrinkPoint point12 = segment.getPoint(4);
                    Unit x3 = point10.getX();
                    Unit y3 = point10.getY();
                    Unit x4 = point11.getX();
                    Unit y4 = point11.getY();
                    Unit subtract3 = UnitMath.subtract(x4, x3);
                    Unit subtract4 = UnitMath.subtract(y4, y3);
                    Unit halve = UnitMath.halve(subtract3);
                    Unit halve2 = UnitMath.halve(subtract4);
                    EllipticalArcPathSegment.ArcData arcData = (EllipticalArcPathSegment.ArcData) segment.getExtraData();
                    if (Math.abs(arcData.angularExtent) > 3.141592653589793d) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    if (arcData.angularExtent > 0.0d) {
                        i2 = 0;
                    } else {
                        i2 = 1;
                    }
                    this.writer.println("A " + coordNoUnits(halve, this.hScale) + " " + coordNoUnits(halve2, this.vScale) + " 0 " + i + " " + i2 + " " + coordNoUnits(point12.getX(), this.hScale) + " " + coordNoUnits(point12.getY(), this.vScale));
                } else if (segmentType == PathSegment.TYPE_CLOSE) {
                    this.writer.print("Z ");
                } else {
                    System.err.println("SVGRenderer.drawGeneralPath:  Unhandled segment type " + segmentType);
                }
            } catch (ConformanceException e) {
                System.err.println("SVGRenderer.drawGeneralPath: ConformanceException:\n  " + e);
            } catch (NumericException e2) {
                System.err.println("SVGRenderer.drawGeneralPath: NumericException:\n  " + e2);
            }
        }
        this.writer.println("\" />");
    }

    public void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        System.err.println("SVGRenderer:  drawing bitmapped images not supported.");
    }

    public void setFont(String str, int i, Unit unit) {
        this.fontName = str;
        this.fontStyle = i;
        this.fontHeight = unit;
    }

    public void drawText(String str, Unit unit, Unit unit2, int i, int i2) {
        String str2;
        String str3;
        String str4;
        String str5;
        String color2 = getColor(this.color, true, true, this.strokeWidth, this.env);
        String XMLEncode = SGMLUtils.XMLEncode(str);
        PrintWriter printWriter = this.writer;
        StringBuilder append = new StringBuilder().append("  <text x=").append(qCoord(unit, this.env)).append(" y=").append(qCoord(unit2, this.env));
        if (this.fontName == null) {
            str2 = "";
        } else {
            str2 = " font-family=\"" + SGMLUtils.XMLEncode(normalizeFontName(this.fontName)) + "\"";
        }
        StringBuilder append2 = append.append(str2);
        if ((this.fontStyle & 1) != 0) {
            str3 = " font-weight=\"bold\"";
        } else {
            str3 = "";
        }
        StringBuilder append3 = append2.append(str3);
        if ((this.fontStyle & 2) != 0) {
            str4 = " font-style=\"italic\"";
        } else {
            str4 = "";
        }
        StringBuilder append4 = append3.append(str4);
        if (this.fontHeight == null) {
            str5 = "";
        } else {
            str5 = " font-size=" + qCoord(this.fontHeight, this.env);
        }
        printWriter.println(append4.append(str5).append(makeAlignment(i, i2)).append(color2).append(">").append(XMLEncode).append("</text>").toString());
    }

    public void setStroke(Unit unit) {
        this.strokeWidth = unit;
    }

    private static String getColor(FrinkColor frinkColor, boolean z, boolean z2, Unit unit, Environment environment) {
        String str;
        String str2;
        double alphaDouble = GraphicsUtils.getAlphaDouble(frinkColor);
        if (alphaDouble < 1.0d) {
            str = " opacity=\"" + alphaDouble + "\"";
        } else {
            str = "";
        }
        if (unit != null) {
            str2 = " stroke-width=" + qCoord(unit, environment);
        } else {
            str2 = "";
        }
        if (z2) {
            return " fill=" + qColor(frinkColor) + " stroke=\"none\"" + str + str2;
        }
        return " fill=\"none\" stroke=" + qColor(frinkColor) + str + str2;
    }

    public void drawableModified() {
        if (this.child != null) {
            this.child.drawableModified();
        }
        doRender();
    }

    private void doRender() {
        this.color = BasicFrinkColor.BLACK;
        try {
            if (this.outTo instanceof File) {
                this.writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream((File) this.outTo), "UTF-8")));
            } else if (this.outTo instanceof OutputStream) {
                this.writer = new PrintWriter(new OutputStreamWriter((OutputStream) this.outTo, "UTF-8"));
            }
            Unit inch = GraphicsUtils.getInch(this.env);
            try {
                if (UnitMath.areConformal(this.width, inch)) {
                    this.hScale = UnitMath.divide(defaultViewSize, this.width);
                } else {
                    this.hScale = null;
                }
                if (UnitMath.areConformal(this.height, inch)) {
                    this.vScale = UnitMath.divide(defaultViewSize, this.height);
                } else {
                    this.vScale = null;
                }
            } catch (NumericException e) {
                this.env.outputln("SVGRenderer: Error when calculating scale:\n " + e);
            }
            printPreamble();
            paintRequested();
            printPostamble();
            if (this.writer != null) {
                this.writer.close();
            }
            this.writer = null;
        } catch (IOException e2) {
            this.env.outputln("SVGRenderer.doRender:  IO error:\n  " + e2);
            if (this.writer != null) {
                this.writer.close();
            }
            this.writer = null;
        } catch (Throwable th) {
            if (this.writer != null) {
                this.writer.close();
            }
            this.writer = null;
            throw th;
        }
    }

    public void rendererResized() {
        if (this.parent != null) {
            this.parent.rendererResized();
        }
    }

    private void printPreamble() {
        String str = "";
        if (!(this.hScale == null && this.vScale == null)) {
            str = " viewBox=\"0 0 1000 1000\"";
        }
        this.writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
        this.writer.println("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">");
        this.writer.println(" <svg width=" + qCoord(this.width, this.env) + " height=" + qCoord(this.height, this.env) + str + " version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">");
    }

    private void printPostamble() {
        this.writer.println(" </svg>");
    }

    public void paintRequested() {
        if (this.parent != null) {
            this.parent.paintRequested();
        }
        callPaintListeners();
    }

    private static String coord(Unit unit, Environment environment) {
        String str;
        Unit unit2;
        try {
            Unit inch = GraphicsUtils.getInch(environment);
            if (UnitMath.areConformal(unit, inch)) {
                unit2 = UnitMath.divide(unit, inch);
                str = "in";
            } else {
                str = "";
                unit2 = unit;
            }
            return format(unit2, environment) + str;
        } catch (NumericException e) {
            environment.outputln("SVGRenderer:  Numeric exception when writing coordinates:\n  " + e);
            return "";
        }
    }

    private String coordNoUnits(Unit unit, Unit unit2) {
        Unit unit3;
        if (unit2 != null) {
            try {
                unit3 = UnitMath.multiply(unit, unit2);
            } catch (NumericException e) {
                this.env.outputln("SVGRenderer:  Numeric exception when writing coordinates:\n  " + e);
                return "";
            }
        } else {
            unit3 = unit;
        }
        return format(unit3, this.env);
    }

    private static String qCoord(Unit unit, Environment environment) {
        if (unit == null) {
            return "";
        }
        return '\"' + coord(unit, environment) + '\"';
    }

    private static String format(Unit unit, Environment environment) {
        try {
            return UnitMath.toString(UnitMath.round(unit, roundTo), environment.getDimensionManager());
        } catch (NumericException e) {
            environment.outputln("SVGRenderer:  Numeric exception when formatting coordinates:\n  " + e);
            return "";
        } catch (ConformanceException e2) {
            environment.outputln("SVGRenderer:  Conformance exception when formatting coordinates:\n  " + e2);
            return "";
        }
    }

    public void printRequested() {
    }

    private static String qColor(FrinkColor frinkColor) {
        return "\"#" + hexPad(frinkColor.getRed(), 2) + hexPad(frinkColor.getGreen(), 2) + hexPad(frinkColor.getBlue(), 2) + '\"';
    }

    private static String hexPad(int i, int i2) {
        String hexString = Integer.toHexString(i);
        if (hexString.length() >= i2) {
            return hexString;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = i2 - hexString.length();
        for (int i3 = 0; i3 < length; i3++) {
            stringBuffer.append('0');
        }
        stringBuffer.append(hexString);
        return new String(stringBuffer);
    }

    private String normalizeFontName(String str) {
        if (str == null) {
            return null;
        }
        if (str.equalsIgnoreCase("SansSerif")) {
            return "sans-serif";
        }
        if (str.equalsIgnoreCase("Serif")) {
            return "serif";
        }
        return str.equalsIgnoreCase("Monospaced") ? "monospace" : str;
    }

    private String makeAlignment(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        switch (i) {
            case 1:
                stringBuffer.append(" text-anchor=\"start\"");
                break;
            case 2:
                stringBuffer.append(" text-anchor=\"end\"");
                break;
            default:
                stringBuffer.append(" text-anchor=\"middle\"");
                break;
        }
        switch (i2) {
            case 1:
                stringBuffer.append(" alignment-baseline=\"text-before-edge\" dominant-baseline=\"text-before-edge\"");
                break;
            case 2:
                stringBuffer.append(" alignment-baseline=\"text-after-edge\" dominant-baseline=\"text-after-edge\"");
                break;
            case 3:
                break;
            default:
                stringBuffer.append(" alignment-baseline=\"central\" dominant-baseline=\"central\"");
                break;
        }
        return new String(stringBuffer);
    }
}
