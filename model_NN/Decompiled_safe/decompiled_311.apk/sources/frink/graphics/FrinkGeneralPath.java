package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import frink.units.UnitMath;
import java.util.Vector;

public class FrinkGeneralPath {
    private boolean boundingBoxNeedsRecalculation = false;
    private BoundingBox boxWithStroke = null;
    private boolean filled = false;
    private int recalculateFrom = 0;
    private Vector<PathSegment> segments = new Vector<>();
    private BoundingBox shapeBox = null;
    private Unit strokeWidth = null;

    public void addSegment(PathSegment pathSegment) throws ConformanceException, NumericException, OverlapException {
        this.boundingBoxNeedsRecalculation = true;
        this.segments.addElement(pathSegment);
    }

    public int getSegmentCount() {
        return this.segments.size();
    }

    public PathSegment getSegment(int i) {
        return this.segments.elementAt(i);
    }

    public FrinkPoint getLastPoint() {
        for (int segmentCount = getSegmentCount() - 1; segmentCount >= 0; segmentCount--) {
            FrinkPoint lastPoint = this.segments.elementAt(segmentCount).getLastPoint();
            if (lastPoint != null) {
                return lastPoint;
            }
        }
        return null;
    }

    public void setStrokeWidth(Unit unit, boolean z) {
        this.strokeWidth = unit;
        this.filled = z;
        this.boundingBoxNeedsRecalculation = true;
    }

    public BoundingBox getBoundingBox() {
        if (this.boundingBoxNeedsRecalculation) {
            recalculateBoundingBox();
        }
        if (this.boxWithStroke != null) {
            return this.boxWithStroke;
        }
        return this.shapeBox;
    }

    public FrinkGeneralPath scaleAndTranslate(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws NumericException, ConformanceException, OverlapException {
        FrinkPoint[] frinkPointArr;
        FrinkGeneralPath frinkGeneralPath = new FrinkGeneralPath();
        int segmentCount = getSegmentCount();
        for (int i = 0; i < segmentCount; i++) {
            PathSegment segment = getSegment(i);
            int pointCount = segment.getPointCount();
            if (pointCount == 0) {
                frinkPointArr = null;
            } else {
                frinkPointArr = new FrinkPoint[pointCount];
            }
            for (int i2 = 0; i2 < pointCount; i2++) {
                frinkPointArr[i2] = segment.getPoint(i2).scaleAndTranslate(unit, unit2, unit3, unit4);
            }
            frinkGeneralPath.addSegment(PathSegmentFactory.create(segment.getSegmentType(), frinkPointArr, segment.getExtraData()));
        }
        return frinkGeneralPath;
    }

    private synchronized void recalculateBoundingBox() {
        Unit y;
        Unit unit;
        Unit unit2;
        Unit unit3;
        Unit unit4;
        Unit unit5;
        Unit unit6;
        BoundingBox boundingBox;
        Unit unit7;
        Unit unit8;
        Unit unit9;
        Unit unit10;
        if (this.boundingBoxNeedsRecalculation) {
            int size = this.segments.size();
            if (size != 0) {
                try {
                    if (this.recalculateFrom != 0) {
                        Unit left = this.shapeBox.getLeft();
                        Unit right = this.shapeBox.getRight();
                        Unit top = this.shapeBox.getTop();
                        unit2 = left;
                        y = this.shapeBox.getBottom();
                        Unit unit11 = right;
                        unit3 = top;
                        unit = unit11;
                    } else {
                        PathSegment elementAt = this.segments.elementAt(0);
                        if (elementAt.calculatesBoundingBox()) {
                            BoundingBox boundingBox2 = elementAt.getBoundingBox();
                            Unit left2 = boundingBox2.getLeft();
                            unit = boundingBox2.getRight();
                            Unit top2 = boundingBox2.getTop();
                            y = boundingBox2.getBottom();
                            this.recalculateFrom++;
                            Unit unit12 = top2;
                            unit2 = left2;
                            unit3 = unit12;
                        } else {
                            FrinkPoint point = elementAt.getPoint(0);
                            Unit x = point.getX();
                            y = point.getY();
                            unit = x;
                            unit2 = x;
                            unit3 = y;
                        }
                    }
                    Unit unit13 = unit2;
                    Unit unit14 = unit3;
                    BoundingBox boundingBox3 = null;
                    int i = this.recalculateFrom;
                    Unit unit15 = unit;
                    Unit unit16 = y;
                    while (i < size) {
                        PathSegment elementAt2 = this.segments.elementAt(i);
                        if (elementAt2.calculatesBoundingBox()) {
                            boundingBox = elementAt2.getBoundingBox();
                            if (boundingBox3 == null) {
                                unit8 = unit16;
                                unit7 = unit14;
                                unit10 = unit15;
                                unit9 = unit13;
                            } else {
                                boundingBox = BoundingBox.union(boundingBox3, boundingBox);
                                unit8 = unit16;
                                unit7 = unit14;
                                unit10 = unit15;
                                unit9 = unit13;
                            }
                        } else {
                            int pointCount = elementAt2.getPointCount();
                            Unit unit17 = unit13;
                            Unit unit18 = unit15;
                            Unit unit19 = unit14;
                            Unit unit20 = unit16;
                            for (int i2 = 0; i2 < pointCount; i2++) {
                                FrinkPoint point2 = elementAt2.getPoint(i2);
                                Unit x2 = point2.getX();
                                Unit y2 = point2.getY();
                                if (UnitMath.compare(x2, unit17) < 0) {
                                    unit17 = x2;
                                } else if (UnitMath.compare(x2, unit18) > 0) {
                                    unit18 = x2;
                                }
                                if (UnitMath.compare(y2, unit19) < 0) {
                                    unit19 = y2;
                                } else if (UnitMath.compare(y2, unit20) > 0) {
                                    unit20 = y2;
                                }
                            }
                            boundingBox = boundingBox3;
                            unit7 = unit19;
                            unit8 = unit20;
                            unit9 = unit17;
                            unit10 = unit18;
                        }
                        i++;
                        unit13 = unit9;
                        unit15 = unit10;
                        unit14 = unit7;
                        unit16 = unit8;
                        boundingBox3 = boundingBox;
                    }
                    this.recalculateFrom = size;
                    this.shapeBox = new BoundingBox(unit13, unit14, unit15, unit16);
                    if (boundingBox3 != null) {
                        this.shapeBox = BoundingBox.union(this.shapeBox, boundingBox3);
                        Unit left3 = this.shapeBox.getLeft();
                        Unit right2 = this.shapeBox.getRight();
                        unit4 = this.shapeBox.getTop();
                        unit16 = this.shapeBox.getBottom();
                        unit5 = right2;
                        unit6 = left3;
                    } else {
                        unit4 = unit14;
                        unit5 = unit15;
                        unit6 = unit13;
                    }
                    if (!this.filled && this.strokeWidth != null) {
                        this.boxWithStroke = new BoundingBox(unit6, unit4, unit5, unit16, this.strokeWidth, this.filled);
                    }
                } catch (ConformanceException e) {
                    System.err.println("FrinkPointList: Error in calculating bounding box:\n " + e);
                } catch (NumericException e2) {
                    System.err.println("FrinkPointList: Error in calculating bounding box:\n " + e2);
                } catch (OverlapException e3) {
                    System.err.println("FrinkPointList: Error in calculating bounding box:\n " + e3);
                }
                this.boundingBoxNeedsRecalculation = false;
            }
        }
    }
}
