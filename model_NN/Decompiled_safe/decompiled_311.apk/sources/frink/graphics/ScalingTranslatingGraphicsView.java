package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkRational;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.units.BasicUnit;
import frink.units.DimensionListMath;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitMath;

public class ScalingTranslatingGraphicsView extends MiddleGraphicsView {
    private static Unit defaultInsets;
    private boolean autocenter;
    private boolean autoscale;
    private Environment env;
    private Unit horizontalOffset;
    private Unit horizontalScale;
    private Unit insets;
    private boolean offsetsNeedRecalculation;
    private boolean preserveAspectRatio;
    private boolean scaleNeedsRecalculation;
    private Unit verticalOffset;
    private Unit verticalScale;

    static {
        try {
            defaultInsets = DimensionlessUnit.construct(FrinkRational.construct(95, 100));
        } catch (InvalidDenominatorException e) {
        }
    }

    public ScalingTranslatingGraphicsView(Environment environment) {
        this(true, true, true, environment);
    }

    public ScalingTranslatingGraphicsView(Environment environment, Unit unit) {
        this(true, true, true, environment, unit);
    }

    public ScalingTranslatingGraphicsView(boolean z, boolean z2, boolean z3, Environment environment) {
        this(z, z2, z3, environment, null);
    }

    public ScalingTranslatingGraphicsView(boolean z, boolean z2, boolean z3, Environment environment, Unit unit) {
        this.autoscale = z;
        this.preserveAspectRatio = z2;
        this.autocenter = z3;
        this.env = environment;
        this.scaleNeedsRecalculation = true;
        this.offsetsNeedRecalculation = true;
        if (unit == null) {
            this.insets = defaultInsets;
        } else {
            this.insets = unit;
        }
    }

    private synchronized void recalculateScale() {
        BoundingBox rendererBoundingBox;
        BoundingBox drawableBoundingBox;
        boolean z;
        boolean z2;
        boolean z3;
        if (this.scaleNeedsRecalculation && this.autocenter) {
            DimensionlessUnit dimensionlessUnit = DimensionlessUnit.ONE;
            this.verticalScale = dimensionlessUnit;
            this.horizontalScale = dimensionlessUnit;
            if (!(this.child == null || (rendererBoundingBox = this.child.getRendererBoundingBox()) == null || (drawableBoundingBox = super.getDrawableBoundingBox()) == null)) {
                Unit width = drawableBoundingBox.getWidth();
                Unit height = drawableBoundingBox.getHeight();
                Unit width2 = rendererBoundingBox.getWidth();
                Unit height2 = rendererBoundingBox.getHeight();
                Unit inch = GraphicsUtils.getInch(this.env);
                try {
                    if (UnitMath.areConformal(width, inch)) {
                        z = false;
                    } else {
                        z = true;
                    }
                    if (UnitMath.areConformal(height, inch)) {
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                    boolean z4 = NumericMath.compare(FrinkInt.ZERO, width.getScale()) == 0;
                    if (NumericMath.compare(FrinkInt.ZERO, height.getScale()) == 0) {
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                    if (z4 && z) {
                        this.horizontalScale = new BasicUnit(FrinkInt.ONE, DimensionListMath.divide(width2.getDimensionList(), width.getDimensionList()));
                        z = false;
                    }
                    if (z3 && z2) {
                        this.verticalScale = new BasicUnit(FrinkInt.ONE, DimensionListMath.divide(height2.getDimensionList(), height.getDimensionList()));
                        z2 = false;
                    }
                    if (z2 && z4) {
                        if (!z3) {
                            this.verticalScale = UnitMath.multiply(UnitMath.divide(height2, height), this.insets);
                        }
                        this.scaleNeedsRecalculation = false;
                    } else if (!z || !z3) {
                        if (z) {
                            this.horizontalScale = UnitMath.multiply(UnitMath.divide(width2, width), this.insets);
                        }
                        if (z2) {
                            this.verticalScale = UnitMath.multiply(UnitMath.divide(height2, height), this.insets);
                        }
                        if (this.preserveAspectRatio) {
                            try {
                                if (UnitMath.compare(this.horizontalScale, this.verticalScale) < 1) {
                                    this.verticalScale = this.horizontalScale;
                                } else {
                                    this.horizontalScale = this.verticalScale;
                                }
                            } catch (ConformanceException e) {
                                this.scaleNeedsRecalculation = false;
                            } catch (NumericException e2) {
                                this.env.outputln("ScalingTranslatingGraphicsView:  Numeric exception in scaling:\n  " + e2);
                                Unit unit = this.insets;
                                this.verticalScale = unit;
                                this.horizontalScale = unit;
                            } catch (OverlapException e3) {
                                this.env.outputln("ScalingTranslatingGraphicsView:  Overlap exception in scaling:\n  " + e3);
                                Unit unit2 = this.insets;
                                this.verticalScale = unit2;
                                this.horizontalScale = unit2;
                            }
                        }
                        this.scaleNeedsRecalculation = false;
                    } else {
                        if (!z4) {
                            this.horizontalScale = UnitMath.multiply(UnitMath.divide(width2, width), this.insets);
                        }
                        this.scaleNeedsRecalculation = false;
                    }
                } catch (OverlapException e4) {
                    this.env.outputln("ScalingTranslatingGraphicsView:  Overlap exception in scaling:\n  " + e4);
                    Unit unit3 = this.insets;
                    this.verticalScale = unit3;
                    this.horizontalScale = unit3;
                } catch (NumericException e5) {
                    this.env.outputln("ScalingTranslatingGraphicsView:  Numeric exception in scaling:\n  " + e5);
                    Unit unit4 = this.insets;
                    this.verticalScale = unit4;
                    this.horizontalScale = unit4;
                }
            }
        }
        return;
    }

    private synchronized void recalculateOffsets() {
        BoundingBox drawableBoundingBox;
        boolean z;
        boolean z2;
        if (this.scaleNeedsRecalculation && this.autoscale) {
            recalculateScale();
        }
        if (this.offsetsNeedRecalculation && this.autocenter) {
            DimensionlessUnit dimensionlessUnit = DimensionlessUnit.ZERO;
            this.verticalOffset = dimensionlessUnit;
            this.horizontalOffset = dimensionlessUnit;
            if (this.child != null) {
                BoundingBox rendererBoundingBox = this.child.getRendererBoundingBox();
                Unit deviceResolution = this.child.getDeviceResolution();
                if (!(rendererBoundingBox == null || (drawableBoundingBox = super.getDrawableBoundingBox()) == null)) {
                    Unit left = drawableBoundingBox.getLeft();
                    Unit top = drawableBoundingBox.getTop();
                    Unit inch = GraphicsUtils.getInch(this.env);
                    try {
                        if (UnitMath.areConformal(left, inch)) {
                            if (UnitMath.areConformal(rendererBoundingBox.getLeft(), left)) {
                                this.horizontalOffset = UnitMath.subtract(rendererBoundingBox.getLeft(), left);
                            } else {
                                this.horizontalOffset = UnitMath.subtract(UnitMath.divide(rendererBoundingBox.getLeft(), deviceResolution), left);
                            }
                            z = false;
                        } else {
                            z = true;
                        }
                        if (UnitMath.areConformal(top, inch)) {
                            if (UnitMath.areConformal(rendererBoundingBox.getTop(), top)) {
                                this.verticalOffset = UnitMath.subtract(rendererBoundingBox.getTop(), top);
                            } else {
                                this.verticalOffset = UnitMath.subtract(UnitMath.divide(rendererBoundingBox.getTop(), deviceResolution), top);
                            }
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        if (z) {
                            Unit add = UnitMath.add(rendererBoundingBox.getLeft(), rendererBoundingBox.getRight());
                            Unit multiply = UnitMath.multiply(UnitMath.add(left, drawableBoundingBox.getRight()), this.horizontalScale);
                            if (NumericMath.compare(FrinkInt.ZERO, drawableBoundingBox.getWidth().getScale()) == 0) {
                                this.horizontalOffset = UnitMath.divide(add, DimensionlessUnit.TWO);
                            } else {
                                this.horizontalOffset = UnitMath.divide(UnitMath.subtract(add, multiply), DimensionlessUnit.TWO);
                            }
                        }
                        if (z2) {
                            Unit add2 = UnitMath.add(rendererBoundingBox.getTop(), rendererBoundingBox.getBottom());
                            Unit multiply2 = UnitMath.multiply(UnitMath.add(top, drawableBoundingBox.getBottom()), this.verticalScale);
                            if (NumericMath.compare(FrinkInt.ZERO, drawableBoundingBox.getHeight().getScale()) == 0) {
                                this.verticalOffset = UnitMath.divide(add2, DimensionlessUnit.TWO);
                            } else {
                                this.verticalOffset = UnitMath.divide(UnitMath.subtract(add2, multiply2), DimensionlessUnit.TWO);
                            }
                        }
                        this.offsetsNeedRecalculation = false;
                    } catch (NumericException e) {
                        this.env.outputln("CenteringGraphicsView:  Numeric exception in offsets:\n  " + e);
                        DimensionlessUnit dimensionlessUnit2 = DimensionlessUnit.ZERO;
                        this.verticalOffset = dimensionlessUnit2;
                        this.horizontalOffset = dimensionlessUnit2;
                    } catch (ConformanceException e2) {
                        this.env.outputln("CenteringGraphicsView:  Conformance exception in offsets:\n  " + e2);
                        DimensionlessUnit dimensionlessUnit3 = DimensionlessUnit.ZERO;
                        this.verticalOffset = dimensionlessUnit3;
                        this.horizontalOffset = dimensionlessUnit3;
                    } catch (OverlapException e3) {
                        this.env.outputln("CenteringGraphicsView:  Overlap exception in offsets:\n  " + e3);
                        DimensionlessUnit dimensionlessUnit4 = DimensionlessUnit.ZERO;
                        this.verticalOffset = dimensionlessUnit4;
                        this.horizontalOffset = dimensionlessUnit4;
                    }
                }
            }
        }
        return;
    }

    private Unit getHorizontalScale() {
        if (this.scaleNeedsRecalculation) {
            recalculateScale();
        }
        return this.horizontalScale;
    }

    private Unit getVerticalScale() {
        if (this.scaleNeedsRecalculation) {
            recalculateScale();
        }
        return this.verticalScale;
    }

    private Unit getHorizontalOffset() {
        if (this.offsetsNeedRecalculation) {
            recalculateOffsets();
        }
        return this.horizontalOffset;
    }

    private Unit getVerticalOffset() {
        if (this.offsetsNeedRecalculation) {
            recalculateOffsets();
        }
        return this.verticalOffset;
    }

    public void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        Unit unit5;
        Unit unit6;
        Unit unit7;
        Unit unit8;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                if (this.autoscale) {
                    Unit multiply = UnitMath.multiply(unit, getHorizontalScale());
                    Unit multiply2 = UnitMath.multiply(unit2, getVerticalScale());
                    Unit multiply3 = UnitMath.multiply(unit3, getHorizontalScale());
                    unit8 = multiply;
                    unit5 = UnitMath.multiply(unit4, getVerticalScale());
                    Unit unit9 = multiply2;
                    unit6 = multiply3;
                    unit7 = unit9;
                } else {
                    unit5 = unit4;
                    unit6 = unit3;
                    unit7 = unit2;
                    unit8 = unit;
                }
                if (this.autocenter) {
                    unit8 = UnitMath.add(unit8, getHorizontalOffset());
                    unit7 = UnitMath.add(unit7, getVerticalOffset());
                    unit6 = UnitMath.add(unit6, getHorizontalOffset());
                    unit5 = UnitMath.add(unit5, getVerticalOffset());
                }
                this.child.drawLine(unit8, unit7, unit6, unit5);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawLine:\n  " + e);
            } catch (ConformanceException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawLine:\n  " + e2);
            }
        }
    }

    public void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        Unit unit5;
        Unit unit6;
        Unit unit7;
        Unit unit8;
        Unit unit9;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                if (this.autoscale) {
                    Unit multiply = UnitMath.multiply(unit, getHorizontalScale());
                    Unit multiply2 = UnitMath.multiply(unit2, getVerticalScale());
                    Unit multiply3 = UnitMath.multiply(unit3, getHorizontalScale());
                    unit5 = UnitMath.multiply(unit4, getVerticalScale());
                    unit6 = multiply3;
                    Unit unit10 = multiply2;
                    unit8 = multiply;
                    unit7 = unit10;
                } else {
                    unit5 = unit4;
                    unit6 = unit3;
                    unit7 = unit2;
                    unit8 = unit;
                }
                if (this.autocenter) {
                    unit8 = UnitMath.add(unit8, getHorizontalOffset());
                    unit9 = UnitMath.add(unit7, getVerticalOffset());
                } else {
                    unit9 = unit7;
                }
                this.child.drawRectangle(unit8, unit9, unit6, unit5, z);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawRectangle:\n  " + e);
            } catch (ConformanceException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawRectangle:\n  " + e2);
            }
        }
    }

    public void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        Unit unit5;
        Unit unit6;
        Unit unit7;
        Unit unit8;
        Unit unit9;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                if (this.autoscale) {
                    Unit multiply = UnitMath.multiply(unit, getHorizontalScale());
                    Unit multiply2 = UnitMath.multiply(unit2, getVerticalScale());
                    Unit multiply3 = UnitMath.multiply(unit3, getHorizontalScale());
                    unit5 = UnitMath.multiply(unit4, getVerticalScale());
                    unit6 = multiply3;
                    Unit unit10 = multiply2;
                    unit8 = multiply;
                    unit7 = unit10;
                } else {
                    unit5 = unit4;
                    unit6 = unit3;
                    unit7 = unit2;
                    unit8 = unit;
                }
                if (this.autocenter) {
                    unit8 = UnitMath.add(unit8, getHorizontalOffset());
                    unit9 = UnitMath.add(unit7, getVerticalOffset());
                } else {
                    unit9 = unit7;
                }
                this.child.drawEllipse(unit8, unit9, unit6, unit5, z);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawEllipse:\n  " + e);
            } catch (ConformanceException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawEllipse:\n  " + e2);
            }
        }
    }

    public void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2) {
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                int pointCount = frinkPointList.getPointCount();
                FrinkPointList frinkPointList2 = new FrinkPointList();
                for (int i = 0; i < pointCount; i++) {
                    FrinkPoint point = frinkPointList.getPoint(i);
                    Unit x = point.getX();
                    Unit y = point.getY();
                    if (this.autoscale) {
                        x = UnitMath.multiply(x, getHorizontalScale());
                        y = UnitMath.multiply(y, getVerticalScale());
                    }
                    if (this.autocenter) {
                        x = UnitMath.add(x, getHorizontalOffset());
                        y = UnitMath.add(y, getVerticalOffset());
                    }
                    frinkPointList2.addPoint(x, y);
                }
                this.child.drawPoly(frinkPointList2, z, z2);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawPoly:\n  " + e);
            } catch (OverlapException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawPoly:\n  " + e2);
            } catch (ConformanceException e3) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawPoly:\n  " + e3);
            }
        }
    }

    public void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z) {
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                this.child.drawGeneralPath(frinkGeneralPath.scaleAndTranslate(getHorizontalScale(), getVerticalScale(), getHorizontalOffset(), getVerticalOffset()), z);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawGeneralPath:\n  " + e);
            } catch (ConformanceException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawGeneralPath:\n  " + e2);
            } catch (OverlapException e3) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawGeneralPath:\n  " + e3);
            }
        }
    }

    public void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        Unit unit5;
        Unit unit6;
        Unit unit7;
        Unit unit8;
        Unit unit9;
        Unit unit10;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                if (this.autoscale) {
                    Unit multiply = UnitMath.multiply(unit, getHorizontalScale());
                    Unit multiply2 = UnitMath.multiply(unit2, getVerticalScale());
                    Unit multiply3 = UnitMath.multiply(unit3, getHorizontalScale());
                    unit5 = UnitMath.multiply(unit4, getVerticalScale());
                    unit6 = multiply3;
                    Unit unit11 = multiply2;
                    unit8 = multiply;
                    unit7 = unit11;
                } else {
                    unit5 = unit4;
                    unit6 = unit3;
                    unit7 = unit2;
                    unit8 = unit;
                }
                if (this.autocenter) {
                    Unit add = UnitMath.add(unit8, getHorizontalOffset());
                    unit9 = UnitMath.add(unit7, getVerticalOffset());
                    unit10 = add;
                } else {
                    unit9 = unit7;
                    unit10 = unit8;
                }
                this.child.drawImage(frinkImage, unit10, unit9, unit6, unit5);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawGeneralPath:\n  " + e);
            } catch (ConformanceException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawGeneralPath:\n  " + e2);
            }
        }
    }

    public void setFont(String str, int i, Unit unit) {
        Unit unit2;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            try {
                if (UnitMath.areConformal(unit, GraphicsUtils.getInch(this.env)) || !this.autoscale) {
                    unit2 = unit;
                } else {
                    unit2 = UnitMath.multiply(unit, getVerticalScale());
                }
                this.child.setFont(str, i, unit2);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  setFont:\n  " + e);
            }
        }
    }

    public void setStroke(Unit unit) {
        Unit unit2;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            try {
                if (UnitMath.areConformal(unit, GraphicsUtils.getInch(this.env)) || !this.autoscale) {
                    unit2 = unit;
                } else {
                    Unit verticalScale2 = getVerticalScale();
                    if (!UnitMath.areConformal(unit, verticalScale2)) {
                        verticalScale2 = getHorizontalScale();
                    }
                    unit2 = UnitMath.multiply(unit, verticalScale2);
                }
                this.child.setStroke(unit2);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  setStroke:\n  " + e);
            }
        }
    }

    public void drawText(String str, Unit unit, Unit unit2, int i, int i2) {
        Unit unit3;
        Unit unit4;
        Unit unit5;
        Unit unit6;
        if (this.child != null) {
            if (this.scaleNeedsRecalculation) {
                recalculateScale();
            }
            if (this.offsetsNeedRecalculation) {
                recalculateOffsets();
            }
            try {
                if (this.autoscale) {
                    Unit multiply = UnitMath.multiply(unit, getHorizontalScale());
                    unit4 = multiply;
                    unit3 = UnitMath.multiply(unit2, getVerticalScale());
                } else {
                    unit3 = unit2;
                    unit4 = unit;
                }
                if (this.autocenter) {
                    Unit add = UnitMath.add(unit4, getHorizontalOffset());
                    unit5 = UnitMath.add(unit3, getVerticalOffset());
                    unit6 = add;
                } else {
                    unit5 = unit3;
                    unit6 = unit4;
                }
                this.child.drawText(str, unit6, unit5, i, i2);
            } catch (NumericException e) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawText:\n  " + e);
            } catch (ConformanceException e2) {
                this.env.outputln("ScalingTranslatingGraphicsView:  drawText:\n  " + e2);
            }
        }
    }

    public BoundingBox getRendererBoundingBox() {
        BoundingBox rendererBoundingBox = super.getRendererBoundingBox();
        if (rendererBoundingBox == null) {
            return null;
        }
        if (this.scaleNeedsRecalculation) {
            recalculateScale();
        }
        if (this.offsetsNeedRecalculation) {
            recalculateOffsets();
        }
        try {
            return rendererBoundingBox.scale(UnitMath.reciprocal(this.horizontalScale), UnitMath.reciprocal(this.verticalScale)).translate(this.horizontalOffset, this.verticalOffset);
        } catch (ConformanceException | NumericException | OverlapException e) {
            return rendererBoundingBox;
        }
    }

    public BoundingBox getDrawableBoundingBox() {
        if (this.scaleNeedsRecalculation) {
            recalculateScale();
        }
        if (this.offsetsNeedRecalculation) {
            recalculateOffsets();
        }
        BoundingBox drawableBoundingBox = super.getDrawableBoundingBox();
        if (drawableBoundingBox == null) {
            return drawableBoundingBox;
        }
        try {
            return drawableBoundingBox.scale(this.horizontalScale, this.verticalScale).translate(UnitMath.negate(this.horizontalOffset), UnitMath.negate(this.verticalOffset));
        } catch (ConformanceException | NumericException | OverlapException e) {
            return drawableBoundingBox;
        }
    }

    public void drawableModified() {
        this.scaleNeedsRecalculation = true;
        this.offsetsNeedRecalculation = true;
        super.drawableModified();
    }

    public void rendererResized() {
        this.scaleNeedsRecalculation = true;
        this.offsetsNeedRecalculation = true;
        super.rendererResized();
    }
}
