package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import java.io.IOException;
import java.net.URL;

public interface FrinkImageLoader {
    FrinkImage getFrinkImage(URL url, Environment environment) throws IOException, FrinkSecurityException;
}
