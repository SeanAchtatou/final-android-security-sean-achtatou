package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.BasicUnitExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.FrinkBoolean;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidChildException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public abstract class ShapeExpression implements Drawable, GraphicsExpression {
    protected BoundingBox boxWithStroke;
    private boolean filled;
    protected BoundingBox shapeBox;

    public ShapeExpression(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z, Unit unit5) throws ConformanceException, NumericException, OverlapException {
        this.shapeBox = new BoundingBox(unit, unit2, UnitMath.add(unit, unit3), UnitMath.add(unit2, unit4));
        this.filled = z;
        if (z) {
            this.boxWithStroke = null;
            return;
        }
        this.boxWithStroke = new BoundingBox(unit, unit2, UnitMath.add(unit, unit3), UnitMath.add(unit2, unit4), unit5, z);
    }

    public int getChildCount() {
        return 5;
    }

    public Expression getChild(int i) throws InvalidChildException {
        switch (i) {
            case 0:
                return BasicUnitExpression.construct(this.shapeBox.getLeft());
            case 1:
                return BasicUnitExpression.construct(this.shapeBox.getTop());
            case 2:
                return BasicUnitExpression.construct(this.shapeBox.getWidth());
            case 3:
                return BasicUnitExpression.construct(this.shapeBox.getHeight());
            case 4:
                return FrinkBoolean.create(this.filled);
            default:
                throw new InvalidChildException("Invalid child " + i + " requested for ShapeExpression.", this);
        }
    }

    public boolean isFilled() {
        return this.filled;
    }

    public BoundingBox getBoundingBox() {
        if (this.boxWithStroke != null) {
            return this.boxWithStroke;
        }
        return this.shapeBox;
    }

    public Drawable getDrawable() {
        return this;
    }

    public boolean isConstant() {
        return true;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression.getExpressionType() == getExpressionType()) {
            int i = 0;
            while (i <= 3) {
                try {
                    if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                        return false;
                    }
                    i++;
                } catch (InvalidChildException e) {
                }
            }
            return true;
        }
        return false;
    }
}
