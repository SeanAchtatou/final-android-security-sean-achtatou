package frink.graphics;

public class BasicFrinkColor implements FrinkColor {
    public static final BasicFrinkColor BLACK = new BasicFrinkColor(-16777216);
    private static final int CHANNEL_MASK = 255;
    public static final BasicFrinkColor WHITE = new BasicFrinkColor(-1);
    private int packed;

    public BasicFrinkColor(int i) {
        this.packed = i;
    }

    public BasicFrinkColor(int i, int i2, int i3, int i4) {
        this.packed = GraphicsUtils.packColor(i, i2, i3, i4);
    }

    public BasicFrinkColor(int i, int i2, int i3) {
        this(i, i2, i3, (int) CHANNEL_MASK);
    }

    public BasicFrinkColor(double d, double d2, double d3, double d4) {
        this(GraphicsUtils.packColor(d, d2, d3, d4));
    }

    public BasicFrinkColor(double d, double d2, double d3) {
        this(d, d2, d3, 1.0d);
    }

    public int getPacked() {
        return this.packed;
    }

    public int getRed() {
        return (this.packed >>> 16) & CHANNEL_MASK;
    }

    public int getGreen() {
        return (this.packed >>> 8) & CHANNEL_MASK;
    }

    public int getBlue() {
        return this.packed & CHANNEL_MASK;
    }

    public int getAlpha() {
        return (this.packed >>> 24) & CHANNEL_MASK;
    }
}
