package frink.graphics;

import frink.expr.Environment;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class JComponentGraphicsView implements BackgroundChangedListener {
    private JComponent canvas = new InternalJComponent();
    /* access modifiers changed from: private */
    public JFrame f;
    /* access modifiers changed from: private */
    public AWTComponentGraphicsView gv;
    private SwingPopupMenu popMenu;

    public JComponentGraphicsView(Environment environment) {
        this.gv = new AWTComponentGraphicsView(this.canvas, environment);
        this.gv.setBackgroundChangedListener(this);
        this.f = null;
        this.popMenu = new SwingPopupMenu(this.canvas, getGraphicsView());
    }

    public Component getCanvas() {
        return this.canvas;
    }

    public GraphicsView getGraphicsView() {
        return this.gv;
    }

    public void backgroundChanged(FrinkColor frinkColor) {
        this.gv.backgroundChanged(frinkColor);
        if (this.f != null) {
            Color color = new Color(frinkColor.getPacked(), true);
            this.f.setBackground(color);
            this.f.getContentPane().setBackground(color);
        }
    }

    public JFrame getFrame() {
        return this.f;
    }

    public static JComponentGraphicsView createInFrame(Environment environment, String str, GraphicsWindowArguments graphicsWindowArguments) {
        JComponentGraphicsView jComponentGraphicsView = new JComponentGraphicsView(environment);
        jComponentGraphicsView.f = new JFrame(str);
        jComponentGraphicsView.f.setBackground(Color.white);
        jComponentGraphicsView.f.getContentPane().setBackground(Color.white);
        jComponentGraphicsView.f.addWindowListener(new WindowAdapter(jComponentGraphicsView) {
            final /* synthetic */ JComponentGraphicsView val$c;

            {
                this.val$c = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                this.val$c.f.dispose();
            }
        });
        jComponentGraphicsView.f.getContentPane().add(jComponentGraphicsView.getCanvas(), "Center");
        if (graphicsWindowArguments.showControls) {
            JButton jButton = new JButton("Close");
            jButton.addActionListener(new ActionListener(jComponentGraphicsView) {
                final /* synthetic */ JComponentGraphicsView val$c;

                {
                    this.val$c = r1;
                }

                public void actionPerformed(ActionEvent actionEvent) {
                    this.val$c.f.hide();
                    this.val$c.f.dispose();
                }
            });
            jComponentGraphicsView.f.getContentPane().add(jButton, "South");
        }
        return jComponentGraphicsView;
    }

    private final class InternalJComponent extends JComponent {
        private InternalJComponent() {
        }

        public void paintComponent(Graphics graphics) {
            JComponentGraphicsView.this.gv.setGraphics(graphics);
            JComponentGraphicsView.super.paintComponent(graphics);
            JComponentGraphicsView.this.gv.paintRequested();
        }
    }
}
