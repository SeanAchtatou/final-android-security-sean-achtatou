package frink.graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PrintActionListener implements ActionListener {
    private PrintRequestListener prl;

    public PrintActionListener(PrintRequestListener printRequestListener) {
        this.prl = printRequestListener;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.prl.printRequested();
    }
}
