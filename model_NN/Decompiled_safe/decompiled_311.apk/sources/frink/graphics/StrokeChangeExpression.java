package frink.graphics;

import frink.expr.BasicUnitExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidChildException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;

public class StrokeChangeExpression implements Drawable, GraphicsExpression {
    public static final String TYPE = "StrokeChangeExpression";
    private Unit strokeWidth;

    public StrokeChangeExpression(Unit unit) {
        this.strokeWidth = unit;
    }

    public BoundingBox getBoundingBox() {
        return null;
    }

    public Drawable getDrawable() {
        return this;
    }

    public int getChildCount() {
        return 3;
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.setStroke(this.strokeWidth);
    }

    public Expression getChild(int i) throws InvalidChildException {
        switch (i) {
            case 0:
                break;
            default:
                throw new InvalidChildException("Invalid child for stroke", this);
        }
        return BasicUnitExpression.construct(this.strokeWidth);
    }

    public boolean isConstant() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression.getExpressionType() == getExpressionType()) {
            int i = 0;
            while (i <= 3) {
                try {
                    if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                        return false;
                    }
                    i++;
                } catch (InvalidChildException e) {
                }
            }
            return true;
        }
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
