package frink.graphics;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import frink.units.UnitMath;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

public class ImageRendererGraphicsView extends AWTGraphicsView {
    private BoundingBox bbox;
    private Environment env;
    private String format;
    private int height;
    private BufferedImage image;
    private Object outTo;
    private boolean renderBackgroundTransparent;
    private Unit resolution;
    private int width;

    public ImageRendererGraphicsView(Environment environment, ImageFileArguments imageFileArguments) throws FrinkSecurityException {
        this(environment, imageFileArguments.width, imageFileArguments.height, imageFileArguments.outTo, imageFileArguments.format, imageFileArguments.backgroundTransparent);
        if (imageFileArguments.outTo instanceof File) {
            environment.getSecurityHelper().checkWrite((File) imageFileArguments.outTo);
        }
    }

    private ImageRendererGraphicsView(Environment environment, Unit unit, Unit unit2, Object obj, String str, boolean z) {
        Color color;
        this.env = environment;
        this.renderBackgroundTransparent = z;
        try {
            this.width = UnitMath.getIntegerValue(unit);
            this.height = UnitMath.getIntegerValue(unit2);
        } catch (NotAnIntegerException e) {
            environment.outputln("ImageRenderer:  Invalid dimensions passed in.  Defaulting.");
            this.width = 640;
            this.height = 480;
        }
        try {
            this.bbox = new BoundingBox(0, 0, this.width, this.height);
        } catch (ConformanceException | NumericException | OverlapException e2) {
        }
        this.resolution = GraphicsUtils.get72PerInch(environment);
        this.outTo = obj;
        this.format = str;
        if (str == null) {
            this.format = GraphicsUtils.guessFormat(obj, environment);
        }
        if (z) {
            this.image = new BufferedImage(this.width, this.height, 2);
        } else {
            this.image = new BufferedImage(this.width, this.height, 1);
        }
        Graphics graphics = this.image.getGraphics();
        setGraphics(graphics);
        if (!z) {
            Color color2 = graphics.getColor();
            FrinkColor backgroundColor = getBackgroundColor();
            if (backgroundColor == null) {
                color = Color.white;
            } else {
                color = new Color(backgroundColor.getPacked(), true);
            }
            graphics.setColor(color);
            graphics.fillRect(0, 0, this.width, this.height);
            graphics.setColor(color2);
        }
    }

    public void setBackgroundColor(FrinkColor frinkColor) {
        super.setBackgroundColor(frinkColor);
        if (!this.renderBackgroundTransparent) {
            Color color = this.g.getColor();
            this.g.setColor(new Color(frinkColor.getPacked(), true));
            this.g.fillRect(0, 0, this.width, this.height);
            this.g.setColor(color);
        }
    }

    public Unit getDeviceResolution() {
        return this.resolution;
    }

    public BoundingBox getRendererBoundingBox() {
        return this.bbox;
    }

    public void drawableModified() {
        if (this.child != null) {
            this.child.drawableModified();
        }
        doRender();
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0086 A[SYNTHETIC, Splitter:B:25:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doRender() {
        /*
            r7 = this;
            java.lang.String r5 = "ImageRendererGraphicsView.doRender:  IO error on close:\n  "
            java.awt.Graphics r0 = r7.g
            java.awt.Color r1 = java.awt.Color.black
            r0.setColor(r1)
            r7.paintRequested()
            r0 = 0
            java.lang.Object r1 = r7.outTo     // Catch:{ IOException -> 0x00ab, all -> 0x0080 }
            javax.imageio.stream.ImageOutputStream r0 = javax.imageio.ImageIO.createImageOutputStream(r1)     // Catch:{ IOException -> 0x00ab, all -> 0x0080 }
            if (r0 != 0) goto L_0x0022
            frink.expr.Environment r1 = r7.env     // Catch:{ IOException -> 0x002a, all -> 0x00a4 }
            java.lang.String r2 = "ImageRenderer: Got null output stream."
            r1.outputln(r2)     // Catch:{ IOException -> 0x002a, all -> 0x00a4 }
        L_0x001c:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0021:
            return
        L_0x0022:
            java.awt.image.BufferedImage r1 = r7.image     // Catch:{ IOException -> 0x002a, all -> 0x00a4 }
            java.lang.String r2 = r7.format     // Catch:{ IOException -> 0x002a, all -> 0x00a4 }
            javax.imageio.ImageIO.write(r1, r2, r0)     // Catch:{ IOException -> 0x002a, all -> 0x00a4 }
            goto L_0x001c
        L_0x002a:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x002e:
            frink.expr.Environment r2 = r7.env     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a9 }
            r3.<init>()     // Catch:{ all -> 0x00a9 }
            java.lang.String r4 = "ImageRendererGraphicsView.doRender:  IO error:\n  "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a9 }
            r2.outputln(r0)     // Catch:{ all -> 0x00a9 }
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x0021
        L_0x004c:
            r0 = move-exception
            frink.expr.Environment r1 = r7.env
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ImageRendererGraphicsView.doRender:  IO error on close:\n  "
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x0021
        L_0x0066:
            r0 = move-exception
            frink.expr.Environment r1 = r7.env
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ImageRendererGraphicsView.doRender:  IO error on close:\n  "
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x0021
        L_0x0080:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x0089:
            throw r0
        L_0x008a:
            r1 = move-exception
            frink.expr.Environment r2 = r7.env
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "ImageRendererGraphicsView.doRender:  IO error on close:\n  "
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.outputln(r1)
            goto L_0x0089
        L_0x00a4:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0084
        L_0x00a9:
            r0 = move-exception
            goto L_0x0084
        L_0x00ab:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.graphics.ImageRendererGraphicsView.doRender():void");
    }

    public void rendererResized() {
        if (this.parent != null) {
            this.parent.rendererResized();
        }
    }
}
