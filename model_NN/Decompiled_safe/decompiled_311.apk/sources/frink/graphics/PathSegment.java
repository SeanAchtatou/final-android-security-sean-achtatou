package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;

public interface PathSegment {
    public static final String TYPE_CLOSE = "close";
    public static final String TYPE_CUBIC = "cubic";
    public static final String TYPE_ELLIPSE = "ellipse";
    public static final String TYPE_ELLIPTICAL_ARC = "ellipticalArc";
    public static final String TYPE_LINE = "line";
    public static final String TYPE_MOVE_TO = "moveTo";
    public static final String TYPE_QUADRATIC = "quadratic";

    boolean calculatesBoundingBox();

    BoundingBox getBoundingBox() throws ConformanceException, NumericException, OverlapException;

    Object getExtraData();

    FrinkPoint getLastPoint();

    FrinkPoint getPoint(int i);

    int getPointCount();

    String getSegmentType();
}
