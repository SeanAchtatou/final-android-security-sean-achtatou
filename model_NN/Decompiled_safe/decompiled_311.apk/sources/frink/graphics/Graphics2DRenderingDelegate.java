package frink.graphics;

import frink.errors.ConformanceException;
import frink.graphics.EllipticalArcPathSegment;
import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;
import java.awt.BasicStroke;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;

public class Graphics2DRenderingDelegate implements RenderingDelegate {
    private AWTGraphicsView awtgv;
    private Ellipse2D.Double ellipse = new Ellipse2D.Double();
    private Graphics2D g;
    private Line2D.Double line = new Line2D.Double();
    private Rectangle2D.Double rect = new Rectangle2D.Double();

    public Graphics2DRenderingDelegate(AWTGraphicsView aWTGraphicsView, Graphics graphics) {
        this.awtgv = aWTGraphicsView;
        setGraphics(graphics);
    }

    public void setGraphics(Graphics graphics) {
        this.g = (Graphics2D) graphics;
        this.g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        this.g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    public void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                synchronized (this.line) {
                    this.line.setLine(Coord.getDoubleCoord(unit, width, deviceResolution), Coord.getDoubleCoord(unit2, height, deviceResolution), Coord.getDoubleCoord(unit3, width, deviceResolution), Coord.getDoubleCoord(unit4, height, deviceResolution));
                    this.g.draw(this.line);
                }
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawLine:  NumericException:\n  " + e);
            }
        }
    }

    public void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                synchronized (this.rect) {
                    this.rect.setRect(Coord.getDoubleCoord(unit, width, deviceResolution), Coord.getDoubleCoord(unit2, height, deviceResolution), Coord.getDoubleCoord(unit3, width, deviceResolution), Coord.getDoubleCoord(unit4, height, deviceResolution));
                    if (z) {
                        this.g.fill(this.rect);
                    } else {
                        this.g.draw(this.rect);
                    }
                }
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawRectangle:  NumericException:\n  " + e);
            }
        }
    }

    public void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                synchronized (this.ellipse) {
                    this.ellipse.setFrame(Coord.getDoubleCoord(unit, width, deviceResolution), Coord.getDoubleCoord(unit2, height, deviceResolution), Coord.getDoubleCoord(unit3, width, deviceResolution), Coord.getDoubleCoord(unit4, height, deviceResolution));
                    if (z) {
                        this.g.fill(this.ellipse);
                    } else {
                        this.g.draw(this.ellipse);
                    }
                }
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawEllipse:  NumericException:\n  " + e);
            }
        }
    }

    public void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                int pointCount = frinkPointList.getPointCount();
                int[] iArr = new int[pointCount];
                int[] iArr2 = new int[pointCount];
                for (int i = 0; i < pointCount; i++) {
                    FrinkPoint point = frinkPointList.getPoint(i);
                    iArr[i] = Coord.getIntCoord(point.getX(), width, deviceResolution);
                    iArr2[i] = Coord.getIntCoord(point.getY(), height, deviceResolution);
                }
                if (!z) {
                    this.g.drawPolyline(iArr, iArr2, pointCount);
                } else if (z2) {
                    this.g.fillPolygon(iArr, iArr2, pointCount);
                } else {
                    this.g.drawPolygon(iArr, iArr2, pointCount);
                }
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawPoly:  NumericException:\n  " + e);
            }
        }
    }

    public void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                int segmentCount = frinkGeneralPath.getSegmentCount();
                GeneralPath generalPath = new GeneralPath(0);
                for (int i = 0; i < segmentCount; i++) {
                    PathSegment segment = frinkGeneralPath.getSegment(i);
                    String segmentType = segment.getSegmentType();
                    if (segmentType == PathSegment.TYPE_LINE) {
                        FrinkPoint point = segment.getPoint(0);
                        generalPath.lineTo(Coord.getFloatCoord(point.getX(), width, deviceResolution), Coord.getFloatCoord(point.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_MOVE_TO) {
                        FrinkPoint point2 = segment.getPoint(0);
                        generalPath.moveTo(Coord.getFloatCoord(point2.getX(), width, deviceResolution), Coord.getFloatCoord(point2.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_QUADRATIC) {
                        FrinkPoint point3 = segment.getPoint(0);
                        FrinkPoint point4 = segment.getPoint(1);
                        generalPath.quadTo(Coord.getFloatCoord(point3.getX(), width, deviceResolution), Coord.getFloatCoord(point3.getY(), height, deviceResolution), Coord.getFloatCoord(point4.getX(), width, deviceResolution), Coord.getFloatCoord(point4.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_CUBIC) {
                        FrinkPoint point5 = segment.getPoint(0);
                        FrinkPoint point6 = segment.getPoint(1);
                        FrinkPoint point7 = segment.getPoint(2);
                        generalPath.curveTo(Coord.getFloatCoord(point5.getX(), width, deviceResolution), Coord.getFloatCoord(point5.getY(), height, deviceResolution), Coord.getFloatCoord(point6.getX(), width, deviceResolution), Coord.getFloatCoord(point6.getY(), height, deviceResolution), Coord.getFloatCoord(point7.getX(), width, deviceResolution), Coord.getFloatCoord(point7.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_ELLIPSE) {
                        FrinkPoint point8 = segment.getPoint(0);
                        FrinkPoint point9 = segment.getPoint(1);
                        generalPath.append(new Ellipse2D.Double(Coord.getDoubleCoord(point8.getX(), width, deviceResolution), Coord.getDoubleCoord(point8.getY(), height, deviceResolution), Coord.getDoubleCoord(UnitMath.subtract(point9.getX(), point8.getX()), width, deviceResolution), Coord.getDoubleCoord(UnitMath.subtract(point9.getY(), point8.getY()), height, deviceResolution)), false);
                    } else if (segmentType == PathSegment.TYPE_ELLIPTICAL_ARC) {
                        FrinkPoint point10 = segment.getPoint(2);
                        FrinkPoint point11 = segment.getPoint(3);
                        Unit x = point10.getX();
                        Unit y = point10.getY();
                        Unit x2 = point11.getX();
                        Unit y2 = point11.getY();
                        Unit subtract = UnitMath.subtract(x2, x);
                        Unit subtract2 = UnitMath.subtract(y2, y);
                        EllipticalArcPathSegment.ArcData arcData = (EllipticalArcPathSegment.ArcData) segment.getExtraData();
                        generalPath.append(new Arc2D.Double(new Rectangle2D.Double(Coord.getDoubleCoord(x, width, deviceResolution), Coord.getDoubleCoord(y, height, deviceResolution), Coord.getDoubleCoord(subtract, width, deviceResolution), Coord.getDoubleCoord(subtract2, height, deviceResolution)), Math.toDegrees(arcData.startAngle), Math.toDegrees(arcData.angularExtent), 0), true);
                    } else if (segmentType == PathSegment.TYPE_CLOSE) {
                        generalPath.closePath();
                    } else {
                        System.err.println("Graphics2DRenderingDelegate:  unhandled segment type " + segmentType);
                    }
                }
                if (z) {
                    this.g.fill(generalPath);
                } else {
                    this.g.draw(generalPath);
                }
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawGeneralPath:  NumericException:\n  " + e);
            } catch (ConformanceException e2) {
                System.err.println("Graphics2DRenderingDelegate.drawGeneralPath: ConformanceException:\n  " + e2);
            }
        }
    }

    public void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        ImageObserver imageObserver;
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                if (frinkImage instanceof ImageObserver) {
                    imageObserver = (ImageObserver) frinkImage;
                } else {
                    imageObserver = null;
                }
                this.g.drawImage((Image) frinkImage.getImage(), Coord.getIntCoord(unit, width, deviceResolution), Coord.getIntCoord(unit2, height, deviceResolution), Coord.getIntCoord(unit3, width, deviceResolution), Coord.getIntCoord(unit4, height, deviceResolution), imageObserver);
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawImage:  NumericException:\n  " + e);
            }
        }
    }

    public void drawText(String str, Unit unit, Unit unit2, int i, int i2) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                float floatCoord = Coord.getFloatCoord(unit, width, deviceResolution);
                float floatCoord2 = Coord.getFloatCoord(unit2, height, deviceResolution);
                FontMetrics fontMetrics = this.g.getFontMetrics();
                float stringWidth = (float) fontMetrics.stringWidth(str);
                float ascent = (float) fontMetrics.getAscent();
                float descent = (float) fontMetrics.getDescent();
                switch (i) {
                    case 1:
                        break;
                    case 2:
                        floatCoord -= stringWidth;
                        break;
                    default:
                        floatCoord -= stringWidth / 2.0f;
                        break;
                }
                switch (i2) {
                    case 1:
                        floatCoord2 += ascent;
                        break;
                    case 2:
                        floatCoord2 -= descent;
                        break;
                    case 3:
                        break;
                    default:
                        floatCoord2 += (ascent - descent) / 2.0f;
                        break;
                }
                this.g.drawString(str, floatCoord, floatCoord2);
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.drawText:  NumericException:\n  " + e);
            }
        }
    }

    public void setStroke(Unit unit) {
        Unit unit2;
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            if (!UnitMath.areConformal(height, unit)) {
                unit2 = rendererBoundingBox.getWidth();
            } else {
                unit2 = height;
            }
            try {
                double doubleCoord = Coord.getDoubleCoord(unit, unit2, this.awtgv.getDeviceResolution());
                if (this.g != null) {
                    this.g.setStroke(new BasicStroke((float) doubleCoord, 0, 0));
                }
            } catch (NumericException e) {
                System.err.println("Graphics2DRenderingDelegate.setStroke:  NumericException:\n  " + e);
            }
        }
    }
}
