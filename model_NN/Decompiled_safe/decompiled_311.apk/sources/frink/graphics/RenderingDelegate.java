package frink.graphics;

import frink.units.Unit;
import java.awt.Graphics;

public interface RenderingDelegate {
    void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z);

    void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z);

    void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4);

    void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4);

    void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2);

    void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z);

    void drawText(String str, Unit unit, Unit unit2, int i, int i2);

    void setGraphics(Graphics graphics);

    void setStroke(Unit unit);
}
