package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.InvalidArgumentException;
import frink.function.ArcTanFunction;
import frink.numeric.FrinkFloat;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.numeric.RealInterval;
import frink.units.Unit;
import frink.units.UnitMath;

public class EllipticalArcPathSegment extends BasicPathSegment {
    private static final String RADIAN = "radian";
    private BoundingBox bBox = null;
    private ArcData extraData;

    public EllipticalArcPathSegment(FrinkPoint[] frinkPointArr, Object obj) {
        super(PathSegment.TYPE_ELLIPTICAL_ARC, frinkPointArr);
        this.extraData = (ArcData) obj;
    }

    public static EllipticalArcPathSegment construct(FrinkPoint frinkPoint, Unit unit, Unit unit2, Unit unit3, Unit unit4, Unit unit5, Environment environment) throws EvaluationException {
        try {
            Unit x = frinkPoint.getX();
            Unit y = frinkPoint.getY();
            Unit subtract = UnitMath.subtract(x, unit);
            Unit subtract2 = UnitMath.subtract(unit2, y);
            Unit subtract3 = UnitMath.subtract(unit, unit3);
            Unit add = UnitMath.add(unit, unit3);
            Unit subtract4 = UnitMath.subtract(unit2, unit4);
            Unit add2 = UnitMath.add(unit2, unit4);
            Unit radian = GraphicsUtils.getRadian(environment);
            Unit arctan = ArcTanFunction.arctan(subtract2, subtract, radian, environment);
            double doubleValue = UnitMath.getDoubleValue(arctan);
            if (!UnitMath.areConformal(arctan, radian) || !UnitMath.areConformal(unit5, radian)) {
                throw new InvalidArgumentException("EllipticalArcPathSegment:  Angle does not have dimensions of an angle.", null);
            }
            Numeric scale = UnitMath.divide(UnitMath.add(arctan, unit5), radian).getScale();
            Unit add3 = UnitMath.add(UnitMath.multiply(NumericMath.cos(scale), unit3), unit);
            Unit subtract5 = UnitMath.subtract(unit2, UnitMath.multiply(NumericMath.sin(scale), unit4));
            double doubleValue2 = UnitMath.getDoubleValue(UnitMath.divide(unit5, radian));
            return new EllipticalArcPathSegment(new FrinkPoint[]{new FrinkPoint(unit, unit2), frinkPoint, new FrinkPoint(subtract3, subtract4), new FrinkPoint(add, add2), new FrinkPoint(add3, subtract5)}, new ArcData(doubleValue, doubleValue2));
        } catch (ConformanceException e) {
            throw new InvalidArgumentException("EllipticalArcPathSegment:  Conformance exception in construction.\n  " + e, null);
        } catch (NumericException e2) {
            throw new InvalidArgumentException("EllipticalArcPathSegment:  Numeric exception in construction.\n  " + e2, null);
        }
    }

    public boolean calculatesBoundingBox() {
        return true;
    }

    public BoundingBox getBoundingBox() throws ConformanceException, NumericException, OverlapException {
        if (this.bBox == null) {
            calculateBoundingBox();
        }
        return this.bBox;
    }

    private synchronized void calculateBoundingBox() throws ConformanceException, NumericException, OverlapException {
        if (this.bBox == null) {
            FrinkPoint point = getPoint(0);
            FrinkPoint point2 = getPoint(2);
            FrinkPoint point3 = getPoint(3);
            Unit x = point.getX();
            Unit y = point.getY();
            Unit x2 = point2.getX();
            Unit y2 = point2.getY();
            Unit x3 = point3.getX();
            Unit y3 = point3.getY();
            Unit subtract = UnitMath.subtract(x3, x2);
            Unit subtract2 = UnitMath.subtract(y3, y2);
            Unit halve = UnitMath.halve(subtract);
            Unit halve2 = UnitMath.halve(subtract2);
            Numeric construct = RealInterval.construct(new FrinkFloat(this.extraData.startAngle), new FrinkFloat(this.extraData.startAngle + this.extraData.angularExtent));
            Unit add = UnitMath.add(UnitMath.multiply(NumericMath.cos(construct), halve), x);
            Unit subtract3 = UnitMath.subtract(y, UnitMath.multiply(NumericMath.sin(construct), halve2));
            this.bBox = new BoundingBox(UnitMath.infimum(add), UnitMath.infimum(subtract3), UnitMath.supremum(add), UnitMath.supremum(subtract3));
        }
    }

    public ArcData getExtraData() {
        return this.extraData;
    }

    public static class ArcData {
        public double angularExtent;
        public double startAngle;

        public ArcData(double d, double d2) {
            this.startAngle = d;
            this.angularExtent = d2;
        }
    }
}
