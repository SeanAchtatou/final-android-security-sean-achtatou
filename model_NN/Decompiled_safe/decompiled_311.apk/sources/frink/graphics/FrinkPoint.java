package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;

public class FrinkPoint {
    private Unit x;
    private Unit y;

    public FrinkPoint(Unit unit, Unit unit2) {
        this.x = unit;
        this.y = unit2;
    }

    public Unit getX() {
        return this.x;
    }

    public Unit getY() {
        return this.y;
    }

    public FrinkPoint scaleAndTranslate(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws NumericException, ConformanceException {
        Unit unit5 = this.x;
        Unit unit6 = this.y;
        if (unit != null) {
            unit5 = UnitMath.multiply(unit5, unit);
        }
        if (unit2 != null) {
            unit6 = UnitMath.multiply(unit6, unit2);
        }
        if (unit3 != null) {
            unit5 = UnitMath.add(unit5, unit3);
        }
        if (unit4 != null) {
            unit6 = UnitMath.add(unit6, unit4);
        }
        return new FrinkPoint(unit5, unit6);
    }
}
