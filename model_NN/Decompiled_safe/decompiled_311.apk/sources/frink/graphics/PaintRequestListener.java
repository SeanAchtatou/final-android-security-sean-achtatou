package frink.graphics;

public interface PaintRequestListener {
    BoundingBox getDrawableBoundingBox();

    void paintRequested(GraphicsView graphicsView);
}
