package frink.graphics;

import frink.expr.Environment;
import java.awt.Button;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CanvasGraphicsView implements BackgroundChangedListener {
    private Canvas canvas = new Canvas() {
        public void paint(Graphics graphics) {
            CanvasGraphicsView.this.gv.setGraphics(graphics);
            CanvasGraphicsView.this.gv.paintRequested();
        }
    };
    /* access modifiers changed from: private */
    public Frame f;
    /* access modifiers changed from: private */
    public AWTComponentGraphicsView gv;

    public CanvasGraphicsView(Environment environment) {
        this.gv = new AWTComponentGraphicsView(this.canvas, environment);
        this.gv.setBackgroundChangedListener(this);
        this.f = null;
    }

    public void backgroundChanged(FrinkColor frinkColor) {
        this.gv.backgroundChanged(frinkColor);
        if (this.f != null) {
            this.f.setBackground(new Color(frinkColor.getPacked(), true));
        }
    }

    public Canvas getCanvas() {
        return this.canvas;
    }

    public GraphicsView getGraphicsView() {
        return this.gv;
    }

    public Frame getFrame() {
        return this.f;
    }

    public static CanvasGraphicsView createInFrame(Environment environment, String str, GraphicsWindowArguments graphicsWindowArguments) {
        CanvasGraphicsView canvasGraphicsView = new CanvasGraphicsView(environment);
        canvasGraphicsView.f = new Frame(str);
        canvasGraphicsView.f.addWindowListener(new WindowAdapter(canvasGraphicsView) {
            final /* synthetic */ CanvasGraphicsView val$c;

            {
                this.val$c = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                this.val$c.f.dispose();
            }
        });
        canvasGraphicsView.f.add(canvasGraphicsView.getCanvas(), "Center");
        if (graphicsWindowArguments.showControls) {
            Button button = new Button("Close");
            button.addActionListener(new ActionListener(canvasGraphicsView) {
                final /* synthetic */ CanvasGraphicsView val$c;

                {
                    this.val$c = r1;
                }

                public void actionPerformed(ActionEvent actionEvent) {
                    this.val$c.f.hide();
                    this.val$c.f.dispose();
                }
            });
            canvasGraphicsView.f.add(button, "South");
        }
        return canvasGraphicsView;
    }
}
