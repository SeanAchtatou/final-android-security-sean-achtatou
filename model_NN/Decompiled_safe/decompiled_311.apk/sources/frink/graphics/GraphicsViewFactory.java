package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;

public interface GraphicsViewFactory {
    void addConstructor(String str, GraphicsViewConstructor graphicsViewConstructor);

    GraphicsView create(String str, Environment environment, String str2, Object obj) throws FrinkSecurityException;

    GraphicsView createDefault(Environment environment, String str, Object obj) throws FrinkSecurityException;

    PaintController createDefaultPaintController(Drawable drawable, Environment environment, String str, Object obj) throws FrinkSecurityException;

    PaintController createPaintController(String str, Drawable drawable, Environment environment, String str2, Object obj) throws FrinkSecurityException;

    int getNewWindowHeight();

    int getNewWindowWidth();

    boolean getShowControlsOnWindows();

    void setDefaultConstructorName(String str);

    void setNewWindowSize(int i, int i2);

    void setShowControlsOnWindows(boolean z);
}
