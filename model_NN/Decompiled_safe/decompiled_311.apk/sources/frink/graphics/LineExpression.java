package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;

public class LineExpression extends SingleGraphicsExpression implements Drawable {
    private BoundingBox bb;
    Unit x1;
    Unit x2;
    Unit y1;
    Unit y2;

    public LineExpression(Unit unit, Unit unit2, Unit unit3, Unit unit4, Unit unit5) throws ConformanceException, NumericException, OverlapException {
        this.bb = new BoundingBox(unit, unit2, unit3, unit4, unit5, false);
        this.x1 = unit;
        this.y1 = unit2;
        this.x2 = unit3;
        this.y2 = unit4;
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.drawLine(this.x1, this.y1, this.x2, this.y2);
    }

    public BoundingBox getBoundingBox() {
        return this.bb;
    }

    public String getExpressionType() {
        return "LineExpression";
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        return false;
    }

    public Drawable getDrawable() {
        return this;
    }
}
