package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.DimensionManager;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitMath;

public class BoundingBox {
    private Unit bottom;
    private Unit height;
    private Unit left;
    private Unit right;
    private Unit top;
    private Unit width;

    public BoundingBox(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        if (UnitMath.compare(unit, unit3) > 0) {
            this.right = unit;
            this.left = unit3;
        } else {
            this.right = unit3;
            this.left = unit;
        }
        if (UnitMath.compare(unit2, unit4) > 0) {
            this.bottom = unit2;
            this.top = unit4;
        } else {
            this.bottom = unit4;
            this.top = unit2;
        }
        this.width = UnitMath.subtract(this.right, this.left);
        this.height = UnitMath.subtract(this.bottom, this.top);
    }

    public BoundingBox(Unit unit, Unit unit2, Unit unit3, Unit unit4, Unit unit5, boolean z) throws ConformanceException, NumericException, OverlapException {
        if (UnitMath.compare(unit, unit3) > 0) {
            this.right = unit;
            this.left = unit3;
        } else {
            this.right = unit3;
            this.left = unit;
        }
        if (unit5 != null && !z && UnitMath.areConformal(this.right, unit5)) {
            Unit divide = UnitMath.divide(unit5, DimensionlessUnit.TWO);
            this.left = UnitMath.subtract(this.left, divide);
            this.right = UnitMath.add(this.right, divide);
        }
        if (UnitMath.compare(unit2, unit4) > 0) {
            this.bottom = unit2;
            this.top = unit4;
        } else {
            this.bottom = unit4;
            this.top = unit2;
        }
        if (unit5 != null && !z && UnitMath.areConformal(this.top, unit5)) {
            Unit divide2 = UnitMath.divide(unit5, DimensionlessUnit.TWO);
            this.top = UnitMath.subtract(this.top, divide2);
            this.bottom = UnitMath.add(this.bottom, divide2);
        }
        this.width = UnitMath.subtract(this.right, this.left);
        this.height = UnitMath.subtract(this.bottom, this.top);
    }

    public BoundingBox(int i, int i2, int i3, int i4) throws ConformanceException, NumericException, OverlapException {
        this(DimensionlessUnit.construct(i), DimensionlessUnit.construct(i2), DimensionlessUnit.construct(i3), DimensionlessUnit.construct(i4));
    }

    public Unit getLeft() {
        return this.left;
    }

    public Unit getRight() {
        return this.right;
    }

    public Unit getTop() {
        return this.top;
    }

    public Unit getBottom() {
        return this.bottom;
    }

    public Unit getWidth() {
        return this.width;
    }

    public Unit getHeight() {
        return this.height;
    }

    public static BoundingBox union(BoundingBox boundingBox, BoundingBox boundingBox2) throws ConformanceException, NumericException, OverlapException {
        if (boundingBox == null) {
            return boundingBox2;
        }
        if (boundingBox2 == null) {
            return boundingBox;
        }
        return new BoundingBox(UnitMath.min(boundingBox.left, boundingBox2.left), UnitMath.min(boundingBox.top, boundingBox2.top), UnitMath.max(boundingBox.right, boundingBox2.right), UnitMath.max(boundingBox.bottom, boundingBox2.bottom));
    }

    public BoundingBox addPoint(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        return isInside(unit, unit2) ? this : new BoundingBox(UnitMath.min(this.left, unit), UnitMath.min(this.top, unit2), UnitMath.max(this.right, unit), UnitMath.max(this.bottom, unit2));
    }

    public boolean isInside(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        return UnitMath.compare(unit, this.left) >= 0 && UnitMath.compare(unit, this.right) <= 0 && UnitMath.compare(unit2, this.top) >= 0 && UnitMath.compare(unit2, this.bottom) <= 0;
    }

    public BoundingBox scale(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        return new BoundingBox(UnitMath.multiply(this.left, unit), UnitMath.multiply(this.top, unit2), UnitMath.multiply(this.right, unit), UnitMath.multiply(this.bottom, unit2));
    }

    public BoundingBox translate(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        return new BoundingBox(UnitMath.add(this.left, unit), UnitMath.add(this.top, unit2), UnitMath.add(this.right, unit), UnitMath.add(this.bottom, unit2));
    }

    public String toString(Environment environment) {
        DimensionManager dimensionManager = environment.getDimensionManager();
        return "left:" + UnitMath.toString(this.left, dimensionManager) + " top: " + UnitMath.toString(this.top, dimensionManager) + " right: " + UnitMath.toString(this.right, dimensionManager) + " bottom: " + UnitMath.toString(this.bottom, dimensionManager);
    }
}
