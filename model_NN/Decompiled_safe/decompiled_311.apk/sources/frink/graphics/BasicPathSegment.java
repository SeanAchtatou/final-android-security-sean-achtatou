package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;

public class BasicPathSegment implements PathSegment {
    private FrinkPoint[] points;
    private String segmentType;

    public BasicPathSegment(String str, FrinkPoint[] frinkPointArr) {
        this.segmentType = str;
        this.points = frinkPointArr;
    }

    public String getSegmentType() {
        return this.segmentType;
    }

    public int getPointCount() {
        if (this.points == null) {
            return 0;
        }
        return this.points.length;
    }

    public FrinkPoint getPoint(int i) {
        return this.points[i];
    }

    public FrinkPoint getLastPoint() {
        if (this.segmentType.equals(PathSegment.TYPE_ELLIPSE)) {
            return null;
        }
        int pointCount = getPointCount();
        if (pointCount == 0) {
            return null;
        }
        return getPoint(pointCount - 1);
    }

    public Object getExtraData() {
        return null;
    }

    public boolean calculatesBoundingBox() {
        return false;
    }

    public BoundingBox getBoundingBox() throws ConformanceException, NumericException, OverlapException {
        return null;
    }
}
