package frink.graphics;

import java.util.Hashtable;

public class ColorChangeExpression extends AbstractColorChangeExpression {
    public static final String TYPE = "ColorChangeExpression";
    private static final Hashtable<Integer, ColorChangeExpression> table = new Hashtable<>();

    public static ColorChangeExpression construct(FrinkColor frinkColor) {
        Integer num = new Integer(frinkColor.getPacked());
        ColorChangeExpression colorChangeExpression = table.get(num);
        if (colorChangeExpression != null) {
            return colorChangeExpression;
        }
        ColorChangeExpression colorChangeExpression2 = new ColorChangeExpression(frinkColor);
        table.put(num, colorChangeExpression2);
        return colorChangeExpression2;
    }

    public static ColorChangeExpression construct(int i, int i2, int i3) {
        return construct(new BasicFrinkColor(i, i2, i3));
    }

    private ColorChangeExpression(FrinkColor frinkColor) {
        super(frinkColor);
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.setColor(this.color);
    }

    public String getExpressionType() {
        return TYPE;
    }
}
