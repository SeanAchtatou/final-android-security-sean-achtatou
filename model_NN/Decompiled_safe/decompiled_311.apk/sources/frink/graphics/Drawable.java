package frink.graphics;

public interface Drawable {
    void draw(GraphicsView graphicsView);

    BoundingBox getBoundingBox();
}
