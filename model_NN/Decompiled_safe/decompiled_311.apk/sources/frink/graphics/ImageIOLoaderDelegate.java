package frink.graphics;

import frink.expr.Environment;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

public class ImageIOLoaderDelegate implements ImageLoader {
    public BufferedImage getImage(URL url, Environment environment) throws IOException {
        return ImageIO.read(url);
    }
}
