package frink.graphics;

import java.util.Hashtable;

public class BackgroundColorChangeExpression extends AbstractColorChangeExpression {
    public static final String TYPE = "BackgroundColorChangeExpression";
    private static final Hashtable<Integer, BackgroundColorChangeExpression> table = new Hashtable<>();

    public static BackgroundColorChangeExpression construct(FrinkColor frinkColor) {
        Integer num = new Integer(frinkColor.getPacked());
        BackgroundColorChangeExpression backgroundColorChangeExpression = table.get(num);
        if (backgroundColorChangeExpression != null) {
            return backgroundColorChangeExpression;
        }
        BackgroundColorChangeExpression backgroundColorChangeExpression2 = new BackgroundColorChangeExpression(frinkColor);
        table.put(num, backgroundColorChangeExpression2);
        return backgroundColorChangeExpression2;
    }

    public static BackgroundColorChangeExpression construct(int i, int i2, int i3) {
        return construct(new BasicFrinkColor(i, i2, i3));
    }

    public static BackgroundColorChangeExpression construct(int i, int i2, int i3, int i4) {
        return construct(new BasicFrinkColor(i, i2, i3, i4));
    }

    private BackgroundColorChangeExpression(FrinkColor frinkColor) {
        super(frinkColor);
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.setBackgroundColor(this.color);
    }

    public String getExpressionType() {
        return TYPE;
    }
}
