package frink.graphics;

import frink.expr.Environment;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

public class BasicImageLoaderDelegate implements ImageLoader<Image> {
    public Image getImage(URL url, Environment environment) {
        return Toolkit.getDefaultToolkit().getImage(url);
    }
}
