package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;

public interface GraphicsViewConstructor {
    GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException;
}
