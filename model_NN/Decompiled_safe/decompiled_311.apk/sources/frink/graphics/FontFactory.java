package frink.graphics;

import java.awt.Font;

public class FontFactory {
    public static Font construct(String str, int i, int i2) {
        return new Font(str, i, i2);
    }
}
