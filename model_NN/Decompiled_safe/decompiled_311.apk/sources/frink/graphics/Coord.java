package frink.graphics;

import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;

public class Coord {
    public static Unit getCoord(Unit unit, Unit unit2, Unit unit3) throws NumericException {
        if (!UnitMath.isDimensionless(unit2)) {
            return UnitMath.isDimensionless(unit) ? UnitMath.divide(unit, unit3) : unit;
        }
        if (UnitMath.isDimensionless(unit)) {
            return unit;
        }
        return UnitMath.multiply(unit, unit3);
    }

    public static int getIntCoord(Unit unit, Unit unit2, Unit unit3) throws NumericException {
        return UnitMath.getNearestIntValue(getCoord(unit, unit2, unit3));
    }

    public static double getDoubleCoord(Unit unit, Unit unit2, Unit unit3) throws NumericException {
        return UnitMath.getDoubleValue(getCoord(unit, unit2, unit3));
    }

    public static float getFloatCoord(Unit unit, Unit unit2, Unit unit3) throws NumericException {
        return UnitMath.getFloatValue(getCoord(unit, unit2, unit3));
    }
}
