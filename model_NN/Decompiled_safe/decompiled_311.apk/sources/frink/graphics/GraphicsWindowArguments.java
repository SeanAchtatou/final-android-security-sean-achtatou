package frink.graphics;

import frink.units.Unit;

public class GraphicsWindowArguments {
    public Integer height = null;
    public Unit insets = null;
    public boolean showControls = false;
    public boolean sizeOfContents = false;
    public Integer width = null;
}
