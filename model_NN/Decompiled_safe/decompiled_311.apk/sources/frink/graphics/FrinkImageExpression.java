package frink.graphics;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import frink.expr.BasicListExpression;
import frink.expr.ContextFrame;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.StringExpression;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.FiveArgMethod;
import frink.function.FourArgMethod;
import frink.function.FunctionSource;
import frink.function.SingleArgMethod;
import frink.function.SixArgMethod;
import frink.function.ThreeArgMethod;
import frink.function.TwoArgMethod;
import frink.function.ZeroArgMethod;
import frink.io.FrinkIOException;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkReal;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.numeric.RealMath;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitMath;
import java.io.File;
import java.io.IOException;

public class FrinkImageExpression extends SingleGraphicsExpression implements FrinkObject {
    private static FrinkImageExpressionFunctionSource bufferedMethods = null;
    private static FrinkImageExpressionFunctionSource unbufferedMethods = null;
    private BoundingBox bBox;
    private EmptyObjectContextFrame context;
    /* access modifiers changed from: private */
    public FrinkImage img;
    private FrinkImageExpressionFunctionSource myMethods = null;

    public FrinkImageExpression(FrinkImage frinkImage) {
        this.img = frinkImage;
        this.bBox = null;
        this.context = new EmptyObjectContextFrame(this);
    }

    public FrinkImageExpression(FrinkImageExpression frinkImageExpression, Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        this.img = frinkImageExpression.img;
        this.bBox = new BoundingBox(unit, unit2, UnitMath.add(unit, unit3), UnitMath.add(unit2, unit4));
        this.context = new EmptyObjectContextFrame(this);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return expression == this;
    }

    public String getExpressionType() {
        return "FrinkImageExpression";
    }

    public void draw(GraphicsView graphicsView) {
        BoundingBox boundingBox = getBoundingBox();
        graphicsView.drawImage(this.img, boundingBox.getLeft(), boundingBox.getTop(), boundingBox.getWidth(), boundingBox.getHeight());
    }

    public synchronized BoundingBox getBoundingBox() {
        BoundingBox boundingBox;
        if (this.bBox == null) {
            try {
                this.bBox = new BoundingBox(DimensionlessUnit.ZERO, DimensionlessUnit.ZERO, DimensionlessUnit.construct(this.img.getWidth()), DimensionlessUnit.construct(this.img.getHeight()));
            } catch (ConformanceException e) {
                System.err.println("FrinkImageExpression.getBoundingBox:\n  " + e);
                boundingBox = null;
            } catch (NumericException e2) {
                System.err.println("FrinkImageExpression.getBoundingBox:\n  " + e2);
                boundingBox = null;
            } catch (OverlapException e3) {
                System.err.println("FrinkImageExpression.getBoundingBox:\n  " + e3);
                boundingBox = null;
            }
        }
        boundingBox = this.bBox;
        return boundingBox;
    }

    public synchronized FunctionSource getFunctionSource(Environment environment) throws EvaluationException {
        if (this.myMethods == null) {
            if (unbufferedMethods == null) {
                unbufferedMethods = new FrinkImageExpressionFunctionSource(false);
                bufferedMethods = new FrinkImageExpressionFunctionSource(true);
            }
            this.myMethods = bufferedMethods;
        }
        return this.myMethods;
    }

    public ContextFrame getContextFrame(Environment environment) throws EvaluationException {
        return this.context;
    }

    public boolean isA(String str) {
        return str.equals("image") || str.equals("FrinkImageExpression");
    }

    public Drawable getDrawable() {
        return this;
    }

    private static class FrinkImageExpressionFunctionSource extends BasicFunctionSource {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        FrinkImageExpressionFunctionSource(boolean z) {
            super("FrinkImageExpression" + (z ? "(Buffered)" : "(Unbuffered)"));
            addFunctionDefinition("show", new ZeroArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression) throws EvaluationException, SecurityException {
                    GraphicsWindowArguments graphicsWindowArguments = new GraphicsWindowArguments();
                    graphicsWindowArguments.insets = DimensionlessUnit.ONE;
                    graphicsWindowArguments.width = new Integer(frinkImageExpression.img.getWidth());
                    graphicsWindowArguments.height = new Integer(frinkImageExpression.img.getHeight());
                    graphicsWindowArguments.sizeOfContents = true;
                    return environment.getGraphicsViewFactory().createDefaultPaintController(frinkImageExpression.getDrawable(), environment, frinkImageExpression.img.getSource() + " - Frink", graphicsWindowArguments);
                }
            });
            addFunctionDefinition("getWidth", new ZeroArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression) throws EvaluationException, SecurityException {
                    return DimensionlessUnitExpression.construct(frinkImageExpression.img.getWidth());
                }
            });
            addFunctionDefinition("getHeight", new ZeroArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression) throws EvaluationException, SecurityException {
                    return DimensionlessUnitExpression.construct(frinkImageExpression.img.getHeight());
                }
            });
            addFunctionDefinition("getSize", new ZeroArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression) throws EvaluationException, SecurityException {
                    BasicListExpression basicListExpression = new BasicListExpression(2);
                    basicListExpression.appendChild(DimensionlessUnitExpression.construct(frinkImageExpression.img.getWidth()));
                    basicListExpression.appendChild(DimensionlessUnitExpression.construct(frinkImageExpression.img.getHeight()));
                    return basicListExpression;
                }
            });
            if (z) {
                addFunctionDefinition("getPixelInt", new TwoArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2) throws EvaluationException, SecurityException {
                        try {
                            return GraphicsUtils.ARGBToIntListExpression(frinkImageExpression.img.getPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2)));
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to getPixelInt[x,y] must be integers.", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("getPixel", new TwoArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2) throws EvaluationException, SecurityException {
                        try {
                            return GraphicsUtils.ARGBToRationalListExpression(frinkImageExpression.img.getPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2)));
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to getPixel[x,y] must be integers.", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("getPixelAsColor", new TwoArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2) throws EvaluationException, SecurityException {
                        try {
                            return ColorChangeExpression.construct(new BasicFrinkColor(frinkImageExpression.img.getPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2))));
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to getPixelAsColor[x,y] must be integers.", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("averagePixels", new FourArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException, SecurityException {
                        try {
                            FrinkReal frinkRealValue = BuiltinFunctionSource.getFrinkRealValue(expression);
                            FrinkReal frinkRealValue2 = BuiltinFunctionSource.getFrinkRealValue(expression2);
                            FrinkReal frinkRealValue3 = BuiltinFunctionSource.getFrinkRealValue(expression3);
                            FrinkReal frinkRealValue4 = BuiltinFunctionSource.getFrinkRealValue(expression4);
                            FrinkInteger ceil = RealMath.ceil(frinkRealValue);
                            FrinkInteger ceil2 = RealMath.ceil(frinkRealValue2);
                            FrinkInteger floor = RealMath.floor(frinkRealValue3);
                            FrinkInteger floor2 = RealMath.floor(frinkRealValue4);
                            FrinkInteger floor3 = RealMath.floor(frinkRealValue);
                            FrinkInteger floor4 = RealMath.floor(frinkRealValue2);
                            FrinkInteger ceil3 = RealMath.ceil(frinkRealValue3);
                            FrinkInteger ceil4 = RealMath.ceil(frinkRealValue4);
                            int i = floor3.getInt();
                            int i2 = ceil3.getInt() - 1;
                            int i3 = floor4.getInt();
                            int i4 = ceil4.getInt() - 1;
                            double doubleValue = RealMath.subtract(ceil, frinkRealValue).doubleValue();
                            if (doubleValue == 0.0d) {
                                doubleValue = 1.0d;
                            }
                            double doubleValue2 = RealMath.subtract(frinkRealValue3, floor).doubleValue();
                            if (doubleValue2 == 0.0d) {
                                doubleValue2 = 1.0d;
                            }
                            double doubleValue3 = RealMath.subtract(ceil2, frinkRealValue2).doubleValue();
                            if (doubleValue3 == 0.0d) {
                                doubleValue3 = 1.0d;
                            }
                            double doubleValue4 = RealMath.subtract(frinkRealValue4, floor2).doubleValue();
                            if (doubleValue4 == 0.0d) {
                                doubleValue4 = 1.0d;
                            }
                            double d = 0.0d;
                            double d2 = 0.0d;
                            double d3 = 0.0d;
                            double d4 = 0.0d;
                            int i5 = i;
                            double d5 = 0.0d;
                            while (i5 <= i2) {
                                double d6 = 1.0d;
                                if (i5 == i) {
                                    d6 = doubleValue;
                                }
                                if (i5 == i2) {
                                    d6 -= 1.0d - doubleValue2;
                                }
                                double d7 = d5;
                                for (int i6 = i3; i6 <= i4; i6++) {
                                    double d8 = 1.0d;
                                    if (i6 == i3) {
                                        d8 = doubleValue3;
                                    }
                                    if (i6 == i4) {
                                        d8 -= 1.0d - doubleValue4;
                                    }
                                    double d9 = d8 * d6;
                                    int pixelPacked = frinkImageExpression.img.getPixelPacked(i5, i6);
                                    d4 += d9;
                                    d3 += ((double) ((pixelPacked >>> 16) & 255)) * d9;
                                    d2 += ((double) ((pixelPacked >>> 8) & 255)) * d9;
                                    d += ((double) (pixelPacked & 255)) * d9;
                                    d7 += d9 * ((double) ((pixelPacked >>> 24) & 255));
                                }
                                i5++;
                                d5 = d7;
                            }
                            BasicListExpression basicListExpression = new BasicListExpression(4);
                            basicListExpression.appendChild(DimensionlessUnitExpression.construct(Math.min((d3 / d4) / 255.0d, 1.0d)));
                            basicListExpression.appendChild(DimensionlessUnitExpression.construct(Math.min((d2 / d4) / 255.0d, 1.0d)));
                            basicListExpression.appendChild(DimensionlessUnitExpression.construct(Math.min((d / d4) / 255.0d, 1.0d)));
                            basicListExpression.appendChild(DimensionlessUnitExpression.construct(Math.min((d5 / d4) / 255.0d, 1.0d)));
                            return basicListExpression;
                        } catch (NumericException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to averagePixels[left, top, right, bottom] must be dimensionless numbers.", this);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("setPixelInt", new SixArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5, Expression expression6) throws EvaluationException, SecurityException {
                        try {
                            frinkImageExpression.img.setPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2), GraphicsUtils.packColor(BuiltinFunctionSource.getIntegerValue(expression3), BuiltinFunctionSource.getIntegerValue(expression4), BuiltinFunctionSource.getIntegerValue(expression5), BuiltinFunctionSource.getIntegerValue(expression6)));
                            return VoidExpression.VOID;
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to setPixelInt[x,y,r,g,b,a] must be integers.", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("setPixelInt", new FiveArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5) throws EvaluationException, SecurityException {
                        try {
                            frinkImageExpression.img.setPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2), GraphicsUtils.packColor(BuiltinFunctionSource.getIntegerValue(expression3), BuiltinFunctionSource.getIntegerValue(expression4), BuiltinFunctionSource.getIntegerValue(expression5)));
                            return VoidExpression.VOID;
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to setPixelInt[x,y,r,g,b] must be integers.", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("setPixel", new SixArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5, Expression expression6) throws EvaluationException, SecurityException {
                        try {
                            frinkImageExpression.img.setPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2), GraphicsUtils.packColor(BuiltinFunctionSource.getDoubleValue(expression3), BuiltinFunctionSource.getDoubleValue(expression4), BuiltinFunctionSource.getDoubleValue(expression5), BuiltinFunctionSource.getDoubleValue(expression6)));
                            return VoidExpression.VOID;
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  The first two arguments to setPixel[x,y,r,g,b,a] must be integers.", expression);
                        } catch (NotRealException e2) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to setPixel[x,y,r,g,b,a] must be real numbers", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("setPixel", new FiveArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5) throws EvaluationException, SecurityException {
                        try {
                            frinkImageExpression.img.setPixelPacked(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2), GraphicsUtils.packColor(BuiltinFunctionSource.getDoubleValue(expression3), BuiltinFunctionSource.getDoubleValue(expression4), BuiltinFunctionSource.getDoubleValue(expression5), 1.0d));
                            return VoidExpression.VOID;
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  The first two arguments to setPixel[x,y,r,g,b] must be integers.", expression);
                        } catch (NotRealException e2) {
                            throw new InvalidArgumentException("FrinkImageExpression:  Arguments to setPixel[x,y,r,g,b] must be real numbers", expression);
                        }
                    }
                });
            }
            if (z) {
                addFunctionDefinition("setPixel", new ThreeArgMethod<FrinkImageExpression>(false) {
                    /* access modifiers changed from: protected */
                    public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3) throws EvaluationException, SecurityException {
                        try {
                            int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                            int integerValue2 = BuiltinFunctionSource.getIntegerValue(expression2);
                            if (expression3 instanceof ColorChangeExpression) {
                                frinkImageExpression.img.setPixelPacked(integerValue, integerValue2, ((ColorChangeExpression) expression3).getColor().getPacked());
                                return VoidExpression.VOID;
                            }
                            throw new InvalidArgumentException("FrinkImageExpression:  Third argument to setPixel[x,y,color] must be a color object.", expression);
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("FrinkImageExpression:  The first two arguments to setPixel[x,y,color] must be integers.", expression);
                        }
                    }
                });
            }
            addFunctionDefinition("makeARGB", new ZeroArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression) throws EvaluationException {
                    frinkImageExpression.img.makeARGB();
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("write", new SingleArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression) throws EvaluationException, SecurityException {
                    if (expression instanceof StringExpression) {
                        File file = new File(((StringExpression) expression).getString());
                        String guessFormat = GraphicsUtils.guessFormat(file, environment);
                        environment.getSecurityHelper().checkWrite(file);
                        try {
                            frinkImageExpression.img.write(file, guessFormat, environment);
                            return VoidExpression.VOID;
                        } catch (IOException e) {
                            throw new FrinkIOException("image.write:  IO Exception:\n  " + e.getMessage(), expression);
                        }
                    } else {
                        throw new InvalidArgumentException("Argument to image.write should be a filename.", expression);
                    }
                }
            });
            addFunctionDefinition("print", new ZeroArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    environment.getGraphicsViewFactory().createPaintController("ScalingTranslatingPrinter", frinkImageExpression.getDrawable(), environment, "Print Job", new PrintingArguments());
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("print", new SingleArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    environment.getGraphicsViewFactory().createPaintController("ScalingTranslatingPrinter", frinkImageExpression.getDrawable(), environment, "Print Job", new PrintingArguments(BuiltinFunctionSource.getUnitValue(expression)));
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("printTiled", new TwoArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    try {
                        environment.getGraphicsViewFactory().createPaintController("TilingPrinter", frinkImageExpression.getDrawable(), environment, "Print Job", new TilingArguments(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2)));
                        return VoidExpression.VOID;
                    } catch (NotAnIntegerException e) {
                        return null;
                    }
                }
            });
            addFunctionDefinition("printTiled", new ThreeArgMethod<FrinkImageExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, FrinkImageExpression frinkImageExpression, Expression expression, Expression expression2, Expression expression3) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    try {
                        environment.getGraphicsViewFactory().createPaintController("TilingPrinter", frinkImageExpression.getDrawable(), environment, "Print Job", new TilingArguments(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2), BuiltinFunctionSource.getUnitValue(expression3)));
                        return VoidExpression.VOID;
                    } catch (NotAnIntegerException e) {
                        return null;
                    }
                }
            });
        }
    }
}
