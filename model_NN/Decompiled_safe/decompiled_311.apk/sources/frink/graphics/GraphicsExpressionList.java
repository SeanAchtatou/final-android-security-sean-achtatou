package frink.graphics;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.NonTerminalExpression;
import frink.expr.StringExpression;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.FiveArgMethod;
import frink.function.FourArgMethod;
import frink.function.FunctionSource;
import frink.function.SingleArgMethod;
import frink.function.ThreeArgMethod;
import frink.function.TwoArgMethod;
import frink.function.ZeroArgMethod;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitMath;
import java.io.File;

public class GraphicsExpressionList extends NonTerminalExpression implements Drawable, GraphicsExpression, FrinkObject, DeferredDrawable {
    public static final String TYPE = "GraphicsExpressionList";
    private static GraphicsFunctionSource methods = null;
    private BoundingBox bbox = null;
    private EmptyObjectContextFrame contextFrame = null;
    /* access modifiers changed from: private */
    public Unit lastFontHeight = null;
    /* access modifiers changed from: private */
    public String lastFontName = null;
    /* access modifiers changed from: private */
    public Unit lastStrokeWidth = null;

    public GraphicsExpressionList() {
        super(0);
    }

    public void appendChild(Expression expression) {
        if (expression instanceof GraphicsExpression) {
            Drawable drawable = ((GraphicsExpression) expression).getDrawable();
            try {
                if (this.bbox == null) {
                    this.bbox = drawable.getBoundingBox();
                } else {
                    this.bbox = BoundingBox.union(this.bbox, drawable.getBoundingBox());
                }
                super.appendChild(expression);
            } catch (ConformanceException e) {
                System.err.println("GraphicsExpressionList.appendChild:  " + e);
            } catch (NumericException e2) {
                System.err.println("GraphicsExpressionList.appendChild:  " + e2);
            } catch (OverlapException e3) {
                System.err.println("GraphicsExpressionList.appendChild:  " + e3);
            }
        } else {
            System.err.println("GraphicsExpressionList:  not a GraphicsExpression!");
            System.err.println("GraphicsExpressionList:  appendChild error.");
        }
    }

    public void draw(GraphicsView graphicsView) {
        int childCount = getChildCount();
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = getChild(i);
                if (child instanceof GraphicsExpression) {
                    ((GraphicsExpression) child).getDrawable().draw(graphicsView);
                }
                i++;
            } catch (InvalidChildException e) {
                System.out.println("GraphicsExpressionList: Unexpected InvalidChildException");
                return;
            }
        }
    }

    public BoundingBox getBoundingBox() {
        return this.bbox;
    }

    public Drawable getDrawable() {
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression instanceof GraphicsExpressionList) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public synchronized FunctionSource getFunctionSource(Environment environment) {
        if (methods == null) {
            methods = new GraphicsFunctionSource();
        }
        return methods;
    }

    public boolean isA(String str) {
        return str.equals("graphics") || TYPE.equals(str);
    }

    public void setStrokeWidth(Unit unit) {
        int childCount = getChildCount();
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = getChild(i);
                if (!(child instanceof StrokeChangeExpression)) {
                    if (child instanceof DeferredDrawable) {
                        ((DeferredDrawable) child).setStrokeWidth(unit);
                    }
                    i++;
                } else {
                    return;
                }
            } catch (InvalidChildException e) {
                System.err.println("GraphicsExpressionList:  unexpected InvalidChildException");
                return;
            }
        }
    }

    private static class GraphicsFunctionSource extends BasicFunctionSource {
        GraphicsFunctionSource() {
            super(GraphicsExpressionList.TYPE);
            addFunctionDefinition(PathSegment.TYPE_LINE, new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        graphicsExpressionList.appendChild(new LineExpression(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("color", new SingleArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression) throws EvaluationException {
                    if (expression instanceof ColorChangeExpression) {
                        graphicsExpressionList.appendChild((ColorChangeExpression) expression);
                        return expression;
                    }
                    throw new InvalidArgumentException("graphics.color:  Argument to color[x] must be a ColorChangeExpression.", this);
                }
            });
            addFunctionDefinition("color", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    try {
                        ColorChangeExpression construct = ColorChangeExpression.construct(GraphicsExpressionList.constructColorFromExpressions(environment, expression, expression2, expression3));
                        graphicsExpressionList.appendChild(construct);
                        return construct;
                    } catch (InvalidArgumentException e) {
                        throw new InvalidArgumentException("graphics.color:  Invalid color.  Value must be between 0.0 and 1.0 inclusive.", this);
                    }
                }
            });
            addFunctionDefinition("color", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        ColorChangeExpression construct = ColorChangeExpression.construct(GraphicsExpressionList.constructColorFromExpressions(environment, expression, expression2, expression3, expression4));
                        graphicsExpressionList.appendChild(construct);
                        return construct;
                    } catch (InvalidArgumentException e) {
                        throw new InvalidArgumentException("graphics.color:  Invalid color.  Value must be between 0.0 and 1.0 inclusive.", this);
                    }
                }
            });
            addFunctionDefinition("fillRectSize", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        graphicsExpressionList.appendChild(new RectangleExpression(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), true, graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("fillRectSides", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression2);
                        graphicsExpressionList.appendChild(new RectangleExpression(unitValue, unitValue3, UnitMath.subtract(unitValue2, unitValue), UnitMath.subtract(BuiltinFunctionSource.getUnitValue(expression4), unitValue3), true, graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("fillRectCenter", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression2);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue4 = BuiltinFunctionSource.getUnitValue(expression4);
                        graphicsExpressionList.appendChild(new RectangleExpression(UnitMath.subtract(unitValue, UnitMath.divide(unitValue3, DimensionlessUnit.TWO)), UnitMath.subtract(unitValue2, UnitMath.divide(unitValue4, DimensionlessUnit.TWO)), unitValue3, unitValue4, true, graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("fillEllipseSize", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        graphicsExpressionList.appendChild(new EllipseExpression(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), true, graphicsExpressionList.lastStrokeWidth, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("fillEllipseSides", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression2);
                        graphicsExpressionList.appendChild(new EllipseExpression(unitValue, unitValue3, UnitMath.subtract(unitValue2, unitValue), UnitMath.subtract(BuiltinFunctionSource.getUnitValue(expression4), unitValue3), true, graphicsExpressionList.lastStrokeWidth, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("fillEllipseCenter", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression2);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue4 = BuiltinFunctionSource.getUnitValue(expression4);
                        graphicsExpressionList.appendChild(new EllipseExpression(UnitMath.subtract(unitValue, UnitMath.divide(unitValue3, DimensionlessUnit.TWO)), UnitMath.subtract(unitValue2, UnitMath.divide(unitValue4, DimensionlessUnit.TWO)), unitValue3, unitValue4, true, graphicsExpressionList.lastStrokeWidth, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("drawRectSize", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        graphicsExpressionList.appendChild(new RectangleExpression(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), false, graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("drawRectSides", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression2);
                        graphicsExpressionList.appendChild(new RectangleExpression(unitValue, unitValue3, UnitMath.subtract(unitValue2, unitValue), UnitMath.subtract(BuiltinFunctionSource.getUnitValue(expression4), unitValue3), false, graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("drawRectCenter", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression2);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue4 = BuiltinFunctionSource.getUnitValue(expression4);
                        graphicsExpressionList.appendChild(new RectangleExpression(UnitMath.subtract(unitValue, UnitMath.divide(unitValue3, DimensionlessUnit.TWO)), UnitMath.subtract(unitValue2, UnitMath.divide(unitValue4, DimensionlessUnit.TWO)), unitValue3, unitValue4, false, graphicsExpressionList.lastStrokeWidth));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("drawEllipseSize", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        graphicsExpressionList.appendChild(new EllipseExpression(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), false, graphicsExpressionList.lastStrokeWidth, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("drawEllipseSides", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression2);
                        graphicsExpressionList.appendChild(new EllipseExpression(unitValue, unitValue3, UnitMath.subtract(unitValue2, unitValue), UnitMath.subtract(BuiltinFunctionSource.getUnitValue(expression4), unitValue3), false, graphicsExpressionList.lastStrokeWidth, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("drawEllipseCenter", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression2);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue4 = BuiltinFunctionSource.getUnitValue(expression4);
                        graphicsExpressionList.appendChild(new EllipseExpression(UnitMath.subtract(unitValue, UnitMath.divide(unitValue3, DimensionlessUnit.TWO)), UnitMath.subtract(unitValue2, UnitMath.divide(unitValue4, DimensionlessUnit.TWO)), unitValue3, unitValue4, false, graphicsExpressionList.lastStrokeWidth, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("print", new ZeroArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    environment.getGraphicsViewFactory().createPaintController("ScalingTranslatingPrinter", graphicsExpressionList.getDrawable(), environment, "Print Job", new PrintingArguments());
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("print", new SingleArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    environment.getGraphicsViewFactory().createPaintController("ScalingTranslatingPrinter", graphicsExpressionList.getDrawable(), environment, "Print Job", new PrintingArguments(BuiltinFunctionSource.getUnitValue(expression)));
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("printTiled", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    try {
                        environment.getGraphicsViewFactory().createPaintController("TilingPrinter", graphicsExpressionList.getDrawable(), environment, "Print Job", new TilingArguments(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2), BuiltinFunctionSource.getUnitValue(expression3)));
                        return VoidExpression.VOID;
                    } catch (NotAnIntegerException e) {
                        return null;
                    }
                }
            });
            addFunctionDefinition("printTiled", new TwoArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2) throws EvaluationException, SecurityException {
                    environment.getSecurityHelper().checkPrint();
                    try {
                        environment.getGraphicsViewFactory().createPaintController("TilingPrinter", graphicsExpressionList.getDrawable(), environment, "Print Job", new TilingArguments(BuiltinFunctionSource.getIntegerValue(expression), BuiltinFunctionSource.getIntegerValue(expression2)));
                        return VoidExpression.VOID;
                    } catch (NotAnIntegerException e) {
                        return null;
                    }
                }
            });
            addFunctionDefinition("show", new ZeroArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList) throws EvaluationException, SecurityException {
                    return environment.getGraphicsViewFactory().createDefaultPaintController(graphicsExpressionList.getDrawable(), environment, "Frink graphics", null);
                }
            });
            addFunctionDefinition("show", new TwoArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2) throws EvaluationException, SecurityException {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                        int integerValue2 = BuiltinFunctionSource.getIntegerValue(expression2);
                        GraphicsWindowArguments graphicsWindowArguments = new GraphicsWindowArguments();
                        graphicsWindowArguments.width = new Integer(integerValue);
                        graphicsWindowArguments.height = new Integer(integerValue2);
                        return environment.getGraphicsViewFactory().createDefaultPaintController(graphicsExpressionList.getDrawable(), environment, "Frink graphics", graphicsWindowArguments);
                    } catch (NotAnIntegerException e) {
                        throw new InvalidArgumentException("Arguments to graphics.show[width, height] must both be dimensionless integers.", this);
                    }
                }
            });
            addFunctionDefinition("show", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException, SecurityException {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                        int integerValue2 = BuiltinFunctionSource.getIntegerValue(expression2);
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression3);
                        GraphicsWindowArguments graphicsWindowArguments = new GraphicsWindowArguments();
                        graphicsWindowArguments.width = new Integer(integerValue);
                        graphicsWindowArguments.height = new Integer(integerValue2);
                        graphicsWindowArguments.insets = unitValue;
                        return environment.getGraphicsViewFactory().createDefaultPaintController(graphicsExpressionList.getDrawable(), environment, "Frink graphics", graphicsWindowArguments);
                    } catch (NotAnIntegerException e) {
                        throw new InvalidArgumentException("Arguments to graphics.show[width, height] must both be dimensionless integers.", this);
                    }
                }
            });
            addFunctionDefinition("writeFormat", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    Object file;
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        if (string.equals("-")) {
                            Object rawOutputStream = environment.getOutputManager().getRawOutputStream();
                            if (rawOutputStream == null) {
                                rawOutputStream = System.out;
                            }
                            file = rawOutputStream;
                        } else {
                            file = new File(string);
                            environment.getSecurityHelper().checkWrite((File) file);
                        }
                        if (expression2 instanceof StringExpression) {
                            environment.getGraphicsViewFactory().createPaintController("ImageRenderer", graphicsExpressionList.getDrawable(), environment, "Frink graphics", new ImageFileArguments(file, ((StringExpression) expression2).getString(), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), false));
                            return VoidExpression.VOID;
                        }
                        throw new InvalidArgumentException("graphics.writeFormat[filename, format, width, height]:  Second argument must be a string indicating a format.", this);
                    }
                    throw new InvalidArgumentException("graphics.writeFormat[filename, format, width, height]:  First argument must be a filename.", this);
                }
            });
            addFunctionDefinition("write", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        if (string.equals("-")) {
                            throw new InvalidArgumentException("graphics.write[filename, width, height]:  Filename \"-\" not allowed, as format to write is unspecified.  Use writeFormat method instead and specify the file format.", this);
                        }
                        File file = new File(string);
                        environment.getSecurityHelper().checkWrite(file);
                        environment.getGraphicsViewFactory().createPaintController("ImageRenderer", graphicsExpressionList.getDrawable(), environment, "Frink graphics", new ImageFileArguments(file, null, BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), false));
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("graphics.write[filename, width, height]:  First argument must be a filename.", this);
                }
            });
            addFunctionDefinition("write", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        if (string.equals("-")) {
                            throw new InvalidArgumentException("graphics.write[filename, width, height, insets]:  Filename \"-\" not allowed, as format to write is unspecified.  Use writeFormat method instead and specify the file format.", this);
                        }
                        File file = new File(string);
                        environment.getSecurityHelper().checkWrite(file);
                        environment.getGraphicsViewFactory().createPaintController("ImageRenderer", graphicsExpressionList.getDrawable(), environment, "Frink graphics", new ImageFileArguments(file, null, BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), false));
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("graphics.write[filename, format, width, height]:  First argument must be a filename.", this);
                }
            });
            addFunctionDefinition("writeTransparent", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        if (string.equals("-")) {
                            throw new InvalidArgumentException("graphics.writeTransparent[filename, width, height]:  Filename \"-\" not allowed, as format to write is unspecified.  Use writeFormatTransparent method instead and specify the file format.", this);
                        }
                        File file = new File(string);
                        environment.getSecurityHelper().checkWrite(file);
                        environment.getGraphicsViewFactory().createPaintController("ImageRenderer", graphicsExpressionList.getDrawable(), environment, "Frink graphics", new ImageFileArguments(file, null, BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), true));
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("graphics.writeTransparent[filename, width, height]:  First argument must be a filename.", this);
                }
            });
            addFunctionDefinition("writeTransparent", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        if (string.equals("-")) {
                            throw new InvalidArgumentException("graphics.writeTransparent[filename, width, height, insets]:  Filename \"-\" not allowed, as format to write is unspecified.  Use writeFormat method instead and specify the file format.", this);
                        }
                        File file = new File(string);
                        environment.getSecurityHelper().checkWrite(file);
                        environment.getGraphicsViewFactory().createPaintController("ImageRenderer", graphicsExpressionList.getDrawable(), environment, "Frink graphics", new ImageFileArguments(file, null, BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), true));
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("graphics.writeTransparent[filename, width, height, insets]:  First argument must be a filename.", this);
                }
            });
            addFunctionDefinition("writeFormatTransparent", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException, SecurityException {
                    Object file;
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        if (string.equals("-")) {
                            Object rawOutputStream = environment.getOutputManager().getRawOutputStream();
                            if (rawOutputStream == null) {
                                rawOutputStream = System.out;
                            }
                            file = rawOutputStream;
                        } else {
                            file = new File(string);
                            environment.getSecurityHelper().checkWrite((File) file);
                        }
                        if (expression2 instanceof StringExpression) {
                            environment.getGraphicsViewFactory().createPaintController("ImageRenderer", graphicsExpressionList.getDrawable(), environment, "Frink graphics", new ImageFileArguments(file, ((StringExpression) expression2).getString(), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), true));
                            return VoidExpression.VOID;
                        }
                        throw new InvalidArgumentException("graphics.writeFormatTransparent[filename, format, width, height]:  Second argument must be a string indicating a format.", this);
                    }
                    throw new InvalidArgumentException("graphics.writeFormatTransparent[filename, format, width, height]:  First argument must be a filename.", this);
                }
            });
            addFunctionDefinition("backgroundColor", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    try {
                        double doubleValue = BuiltinFunctionSource.getDoubleValue(expression);
                        double doubleValue2 = BuiltinFunctionSource.getDoubleValue(expression2);
                        double doubleValue3 = BuiltinFunctionSource.getDoubleValue(expression3);
                        if (doubleValue >= 0.0d && doubleValue <= 1.0d && doubleValue2 >= 0.0d && doubleValue2 <= 1.0d && doubleValue3 >= 0.0d && doubleValue3 <= 1.0d) {
                            BackgroundColorChangeExpression construct = BackgroundColorChangeExpression.construct(new BasicFrinkColor(doubleValue, doubleValue2, doubleValue3));
                            if (graphicsExpressionList.getChildCount() < 1 || !(graphicsExpressionList.getChild(0) instanceof BackgroundColorChangeExpression)) {
                                graphicsExpressionList.insertBefore(0, construct);
                            } else {
                                System.err.println("Warning:  setBackground already called for this graphics object!");
                                graphicsExpressionList.replaceChild(0, construct);
                            }
                            return VoidExpression.VOID;
                        }
                    } catch (NotRealException e) {
                    }
                    throw new InvalidArgumentException("graphics.backgroundColor:  Invalid color.  Value must be between 0.0 and 1.0 inclusive.", this);
                }
            });
            addFunctionDefinition("backgroundColor", new FourArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        double doubleValue = BuiltinFunctionSource.getDoubleValue(expression);
                        double doubleValue2 = BuiltinFunctionSource.getDoubleValue(expression2);
                        double doubleValue3 = BuiltinFunctionSource.getDoubleValue(expression3);
                        double doubleValue4 = BuiltinFunctionSource.getDoubleValue(expression4);
                        if (doubleValue >= 0.0d && doubleValue <= 1.0d && doubleValue2 >= 0.0d && doubleValue2 <= 1.0d && doubleValue3 >= 0.0d && doubleValue3 <= 1.0d && doubleValue4 >= 0.0d && doubleValue4 <= 1.0d) {
                            BackgroundColorChangeExpression construct = BackgroundColorChangeExpression.construct(new BasicFrinkColor(doubleValue, doubleValue2, doubleValue3, doubleValue4));
                            if (graphicsExpressionList.getChildCount() < 1 || !(graphicsExpressionList.getChild(0) instanceof BackgroundColorChangeExpression)) {
                                graphicsExpressionList.insertBefore(0, construct);
                            } else {
                                System.err.println("Warning:  setBackground already called for this graphics object!");
                                graphicsExpressionList.replaceChild(0, construct);
                            }
                            return VoidExpression.VOID;
                        }
                    } catch (NotRealException e) {
                    }
                    throw new InvalidArgumentException("graphics.backgroundColor:  Invalid color.  Value must be between 0.0 and 1.0 inclusive.", this);
                }
            });
            addFunctionDefinition("add", new SingleArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression) throws EvaluationException, SecurityException {
                    if (expression instanceof DeferredDrawable) {
                        ((DeferredDrawable) expression).setStrokeWidth(graphicsExpressionList.lastStrokeWidth);
                    }
                    graphicsExpressionList.appendChild(expression);
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("text", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    String format;
                    if (expression instanceof StringExpression) {
                        format = ((StringExpression) expression).getString();
                    } else {
                        format = environment.format(expression);
                    }
                    try {
                        graphicsExpressionList.appendChild(new DrawTextExpression(format, BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), graphicsExpressionList.lastFontHeight, graphicsExpressionList.lastFontName, 0, 0, environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException("In graphics.drawText:\n " + e, this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException("In graphics.drawText: \n " + e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException("In graphics.drawText: \n " + e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("text", new FiveArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5) throws EvaluationException {
                    String format;
                    if (expression instanceof StringExpression) {
                        format = ((StringExpression) expression).getString();
                    } else {
                        format = environment.format(expression);
                    }
                    Unit unitValue = BuiltinFunctionSource.getUnitValue(expression2);
                    Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                    if (!(expression4 instanceof StringExpression) || !(expression5 instanceof StringExpression)) {
                        throw new InvalidArgumentException("In graphics.text: Arguments 4 and 5 must be alignment specifiers.", this);
                    }
                    try {
                        graphicsExpressionList.appendChild(new DrawTextExpression(format, unitValue, unitValue2, graphicsExpressionList.lastFontHeight, graphicsExpressionList.lastFontName, TextConstants.parseHorizontalAlignment(((StringExpression) expression4).getString()), TextConstants.parseVerticalAlignment(((StringExpression) expression5).getString()), environment));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException("In graphics.text:\n " + e, this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException("In graphics.text: \n " + e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException("In graphics.text: \n " + e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("font", new ThreeArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    int integerValue;
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        try {
                            if (expression2 instanceof StringExpression) {
                                integerValue = FontChangeExpression.parseStyle(((StringExpression) expression2).getString());
                            } else {
                                integerValue = BuiltinFunctionSource.getIntegerValue(expression2);
                            }
                            Unit unitValue = BuiltinFunctionSource.getUnitValue(expression3);
                            Unit unused = graphicsExpressionList.lastFontHeight = unitValue;
                            String unused2 = graphicsExpressionList.lastFontName = string;
                            graphicsExpressionList.appendChild(new FontChangeExpression(string, integerValue, unitValue));
                            return VoidExpression.VOID;
                        } catch (NumericException e) {
                            throw new InvalidArgumentException("In graphics.font:  second argument (style) should be a string.", this);
                        }
                    } else {
                        throw new InvalidArgumentException("In graphics.font: first argument should be string.", this);
                    }
                }
            });
            addFunctionDefinition("font", new TwoArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2) throws EvaluationException {
                    if (expression instanceof StringExpression) {
                        String string = ((StringExpression) expression).getString();
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression2);
                        Unit unused = graphicsExpressionList.lastFontHeight = unitValue;
                        String unused2 = graphicsExpressionList.lastFontName = string;
                        graphicsExpressionList.appendChild(new FontChangeExpression(string, 0, unitValue));
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("In graphics.font: first argument should be string.", this);
                }
            });
            addFunctionDefinition("stroke", new SingleArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression) throws EvaluationException {
                    Unit unitValue = BuiltinFunctionSource.getUnitValue(expression);
                    Unit unused = graphicsExpressionList.lastStrokeWidth = unitValue;
                    graphicsExpressionList.appendChild(new StrokeChangeExpression(unitValue));
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("draw", new FiveArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5) throws EvaluationException {
                    Unit unitValue = BuiltinFunctionSource.getUnitValue(expression2);
                    Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                    Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression4);
                    Unit unitValue4 = BuiltinFunctionSource.getUnitValue(expression5);
                    if (expression instanceof FrinkImageExpression) {
                        try {
                            graphicsExpressionList.appendChild(new FrinkImageExpression((FrinkImageExpression) expression, unitValue, unitValue2, unitValue3, unitValue4));
                            return VoidExpression.VOID;
                        } catch (ConformanceException e) {
                            throw new EvaluationConformanceException("In graphics.draw:\n " + e, this);
                        } catch (NumericException e2) {
                            throw new InvalidArgumentException("In graphics.draw: \n " + e2.toString(), this);
                        } catch (OverlapException e3) {
                            throw new InvalidArgumentException("In graphics.draw: \n " + e3.toString(), this);
                        }
                    } else {
                        throw new InvalidArgumentException("Invalid object type passed to graphics.draw.  Type was " + expression.getExpressionType(), expression);
                    }
                }
            });
            addFunctionDefinition("fitCenter", new FiveArgMethod<GraphicsExpressionList>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GraphicsExpressionList graphicsExpressionList, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5) throws EvaluationException {
                    Unit unit;
                    Unit unit2;
                    try {
                        Unit unitValue = BuiltinFunctionSource.getUnitValue(expression2);
                        Unit unitValue2 = BuiltinFunctionSource.getUnitValue(expression3);
                        Unit unitValue3 = BuiltinFunctionSource.getUnitValue(expression4);
                        Unit unitValue4 = BuiltinFunctionSource.getUnitValue(expression5);
                        if (expression instanceof FrinkImageExpression) {
                            FrinkImageExpression frinkImageExpression = (FrinkImageExpression) expression;
                            BoundingBox boundingBox = frinkImageExpression.getBoundingBox();
                            Unit width = boundingBox.getWidth();
                            Unit height = boundingBox.getHeight();
                            if (UnitMath.compare(UnitMath.divide(width, height), UnitMath.divide(unitValue3, unitValue4)) > 0) {
                                unit = UnitMath.multiply(height, UnitMath.divide(unitValue3, width));
                                unit2 = unitValue3;
                            } else {
                                Unit multiply = UnitMath.multiply(width, UnitMath.divide(unitValue4, height));
                                unit = unitValue4;
                                unit2 = multiply;
                            }
                            graphicsExpressionList.appendChild(new FrinkImageExpression(frinkImageExpression, UnitMath.subtract(unitValue, UnitMath.halve(unit2)), UnitMath.subtract(unitValue2, UnitMath.halve(unit)), unit2, unit));
                            return VoidExpression.VOID;
                        }
                        throw new InvalidArgumentException("Invalid object type passed to graphics.fitCenter.  Type was " + expression.getExpressionType(), expression);
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException("In graphics.fitCenter:\n " + e, this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException("In graphics.draw: \n " + e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException("In graphics.draw: \n " + e3.toString(), this);
                    }
                }
            });
        }
    }

    public static FrinkColor constructColorFromExpressions(Environment environment, Expression expression, Expression expression2, Expression expression3) throws InvalidArgumentException {
        try {
            double doubleValue = BuiltinFunctionSource.getDoubleValue(expression);
            double doubleValue2 = BuiltinFunctionSource.getDoubleValue(expression2);
            double doubleValue3 = BuiltinFunctionSource.getDoubleValue(expression3);
            if (doubleValue >= 0.0d && doubleValue <= 1.0d && doubleValue2 >= 0.0d && doubleValue2 <= 1.0d && doubleValue3 >= 0.0d && doubleValue3 <= 1.0d) {
                return new BasicFrinkColor(doubleValue, doubleValue2, doubleValue3);
            }
        } catch (NotRealException e) {
        }
        throw new InvalidArgumentException("graphics.color:  Invalid color.  Value must be between 0.0 and 1.0 inclusive.", expression);
    }

    public static FrinkColor constructColorFromExpressions(Environment environment, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws InvalidArgumentException {
        try {
            double doubleValue = BuiltinFunctionSource.getDoubleValue(expression);
            double doubleValue2 = BuiltinFunctionSource.getDoubleValue(expression2);
            double doubleValue3 = BuiltinFunctionSource.getDoubleValue(expression3);
            double doubleValue4 = BuiltinFunctionSource.getDoubleValue(expression4);
            if (doubleValue >= 0.0d && doubleValue <= 1.0d && doubleValue2 >= 0.0d && doubleValue2 <= 1.0d && doubleValue3 >= 0.0d && doubleValue3 <= 1.0d && doubleValue4 >= 0.0d && doubleValue4 <= 1.0d) {
                return new BasicFrinkColor(doubleValue, doubleValue2, doubleValue3, doubleValue4);
            }
        } catch (NotRealException e) {
        }
        throw new InvalidArgumentException("graphics.color:  Invalid color.  Value must be between 0.0 and 1.0 inclusive.", expression);
    }
}
