package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.expr.OperatorExpression;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.Hashtable;

public class BasicGraphicsViewFactory implements GraphicsViewFactory {
    private Hashtable<String, GraphicsViewConstructor> constructors = null;
    private String defaultConstructorName;
    private int defaultHeight = OperatorExpression.PREC_COMPARISON;
    private int defaultWidth = OperatorExpression.PREC_ADD;
    private boolean initialized = false;
    private boolean showControls = false;

    public BasicGraphicsViewFactory(String str) {
        this.defaultConstructorName = str;
    }

    public void setNewWindowSize(int i, int i2) {
        this.defaultWidth = i;
        this.defaultHeight = i2;
    }

    public int getNewWindowWidth() {
        return this.defaultWidth;
    }

    public int getNewWindowHeight() {
        return this.defaultHeight;
    }

    public void setShowControlsOnWindows(boolean z) {
        this.showControls = z;
    }

    public boolean getShowControlsOnWindows() {
        return this.showControls;
    }

    public void setDefaultConstructorName(String str) {
        this.defaultConstructorName = str;
    }

    private synchronized void initializeConstructors() {
        if (!this.initialized) {
            if (GraphicsUtils.hasFrame()) {
                addConstructor("CanvasFrame", new GraphicsViewConstructor() {
                    public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                        int i;
                        GraphicsWindowArguments graphicsWindowArguments;
                        int i2;
                        int i3;
                        environment.getSecurityHelper().checkOpenGraphicsWindow();
                        int newWindowWidth = graphicsViewFactory.getNewWindowWidth();
                        int newWindowHeight = graphicsViewFactory.getNewWindowHeight();
                        if (obj instanceof GraphicsWindowArguments) {
                            GraphicsWindowArguments graphicsWindowArguments2 = (GraphicsWindowArguments) obj;
                            graphicsWindowArguments2.showControls = graphicsViewFactory.getShowControlsOnWindows();
                            if (graphicsWindowArguments2.width != null) {
                                newWindowWidth = graphicsWindowArguments2.width.intValue();
                            }
                            if (graphicsWindowArguments2.height != null) {
                                newWindowHeight = graphicsWindowArguments2.height.intValue();
                                i = newWindowWidth;
                                graphicsWindowArguments = graphicsWindowArguments2;
                            } else {
                                i = newWindowWidth;
                                graphicsWindowArguments = graphicsWindowArguments2;
                            }
                        } else {
                            GraphicsWindowArguments graphicsWindowArguments3 = new GraphicsWindowArguments();
                            graphicsWindowArguments3.showControls = graphicsViewFactory.getShowControlsOnWindows();
                            GraphicsWindowArguments graphicsWindowArguments4 = graphicsWindowArguments3;
                            i = newWindowWidth;
                            graphicsWindowArguments = graphicsWindowArguments4;
                        }
                        CanvasGraphicsView createInFrame = CanvasGraphicsView.createInFrame(environment, str, graphicsWindowArguments);
                        createInFrame.getFrame().show();
                        if (graphicsWindowArguments.sizeOfContents) {
                            Insets insets = createInFrame.getFrame().getInsets();
                            int i4 = i + insets.left + insets.right;
                            i2 = insets.bottom + newWindowHeight + insets.top;
                            i3 = i4;
                        } else {
                            i2 = newWindowHeight;
                            i3 = i;
                        }
                        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                        if (screenSize.width < i3) {
                            i3 = (int) (((double) screenSize.width) * 0.95d);
                        }
                        if (screenSize.height < i2) {
                            i2 = (int) (((double) screenSize.height) * 0.95d);
                        }
                        createInFrame.getFrame().setSize(i3, i2);
                        createInFrame.getFrame().show();
                        createInFrame.getFrame().toFront();
                        return createInFrame.getGraphicsView();
                    }
                });
            }
            if (GraphicsUtils.hasFrame()) {
                addConstructor("ScalingTranslatingCanvasFrame", new GraphicsViewConstructor() {
                    public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                        GraphicsWindowArguments graphicsWindowArguments;
                        int i;
                        int i2;
                        environment.getSecurityHelper().checkOpenGraphicsWindow();
                        int newWindowWidth = graphicsViewFactory.getNewWindowWidth();
                        int newWindowHeight = graphicsViewFactory.getNewWindowHeight();
                        if (obj instanceof GraphicsWindowArguments) {
                            GraphicsWindowArguments graphicsWindowArguments2 = (GraphicsWindowArguments) obj;
                            graphicsWindowArguments2.showControls = graphicsViewFactory.getShowControlsOnWindows();
                            if (graphicsWindowArguments2.width != null) {
                                newWindowWidth = graphicsWindowArguments2.width.intValue();
                            }
                            if (graphicsWindowArguments2.height != null) {
                                graphicsWindowArguments = graphicsWindowArguments2;
                                int i3 = newWindowWidth;
                                i2 = graphicsWindowArguments2.height.intValue();
                                i = i3;
                            } else {
                                graphicsWindowArguments = graphicsWindowArguments2;
                                int i4 = newWindowWidth;
                                i2 = newWindowHeight;
                                i = i4;
                            }
                        } else {
                            graphicsWindowArguments = new GraphicsWindowArguments();
                            graphicsWindowArguments.showControls = graphicsViewFactory.getShowControlsOnWindows();
                            int i5 = newWindowHeight;
                            i = newWindowWidth;
                            i2 = i5;
                        }
                        CanvasGraphicsView createInFrame = CanvasGraphicsView.createInFrame(environment, str, graphicsWindowArguments);
                        createInFrame.getFrame().show();
                        if (graphicsWindowArguments.sizeOfContents) {
                            Insets insets = createInFrame.getFrame().getInsets();
                            i = i + insets.left + insets.right;
                            i2 = i2 + insets.top + insets.bottom;
                        }
                        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                        if (screenSize.width < i) {
                            i = (int) (((double) screenSize.width) * 0.95d);
                        }
                        if (screenSize.height < i2) {
                            i2 = (int) (((double) screenSize.height) * 0.95d);
                        }
                        createInFrame.getFrame().setSize(i, i2);
                        ScalingTranslatingGraphicsView scalingTranslatingGraphicsView = new ScalingTranslatingGraphicsView(environment, graphicsWindowArguments.insets);
                        scalingTranslatingGraphicsView.setChildView(createInFrame.getGraphicsView());
                        createInFrame.getFrame().show();
                        createInFrame.getFrame().toFront();
                        return scalingTranslatingGraphicsView;
                    }
                });
            }
            if (GraphicsUtils.hasJFrame()) {
                addConstructor("ScalingTranslatingJComponentFrame", new GraphicsViewConstructor() {
                    public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                        int i;
                        GraphicsWindowArguments graphicsWindowArguments;
                        environment.getSecurityHelper().checkOpenGraphicsWindow();
                        int newWindowWidth = graphicsViewFactory.getNewWindowWidth();
                        int newWindowHeight = graphicsViewFactory.getNewWindowHeight();
                        if (obj instanceof GraphicsWindowArguments) {
                            GraphicsWindowArguments graphicsWindowArguments2 = (GraphicsWindowArguments) obj;
                            graphicsWindowArguments2.showControls = graphicsViewFactory.getShowControlsOnWindows();
                            if (graphicsWindowArguments2.width != null) {
                                newWindowWidth = graphicsWindowArguments2.width.intValue();
                            }
                            if (graphicsWindowArguments2.height != null) {
                                newWindowHeight = graphicsWindowArguments2.height.intValue();
                                i = newWindowWidth;
                                graphicsWindowArguments = graphicsWindowArguments2;
                            } else {
                                i = newWindowWidth;
                                graphicsWindowArguments = graphicsWindowArguments2;
                            }
                        } else {
                            GraphicsWindowArguments graphicsWindowArguments3 = new GraphicsWindowArguments();
                            graphicsWindowArguments3.showControls = graphicsViewFactory.getShowControlsOnWindows();
                            GraphicsWindowArguments graphicsWindowArguments4 = graphicsWindowArguments3;
                            i = newWindowWidth;
                            graphicsWindowArguments = graphicsWindowArguments4;
                        }
                        JComponentGraphicsView createInFrame = JComponentGraphicsView.createInFrame(environment, str, graphicsWindowArguments);
                        createInFrame.getFrame().show();
                        if (graphicsWindowArguments.sizeOfContents) {
                            Insets insets = createInFrame.getFrame().getInsets();
                            i = i + insets.left + insets.right;
                            newWindowHeight = newWindowHeight + insets.top + insets.bottom;
                        }
                        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                        if (screenSize.width < i) {
                            i = (int) (((double) screenSize.width) * 0.95d);
                        }
                        if (screenSize.height < newWindowHeight) {
                            newWindowHeight = (int) (((double) screenSize.height) * 0.95d);
                        }
                        createInFrame.getFrame().setSize(i, newWindowHeight);
                        ScalingTranslatingGraphicsView scalingTranslatingGraphicsView = new ScalingTranslatingGraphicsView(environment, graphicsWindowArguments.insets);
                        scalingTranslatingGraphicsView.setChildView(createInFrame.getGraphicsView());
                        createInFrame.getFrame().show();
                        createInFrame.getFrame().toFront();
                        return scalingTranslatingGraphicsView;
                    }
                });
            }
            if (GraphicsUtils.hasPrintable()) {
                addConstructor("ScalingTranslatingPrinter", new GraphicsViewConstructor() {
                    public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                        environment.getSecurityHelper().checkPrint();
                        if (!(obj instanceof PrintingArguments)) {
                            return null;
                        }
                        ScalingTranslatingGraphicsView scalingTranslatingGraphicsView = new ScalingTranslatingGraphicsView(environment, ((PrintingArguments) obj).insets);
                        scalingTranslatingGraphicsView.setChildView(new SimplePrintingGraphicsView(environment));
                        return scalingTranslatingGraphicsView;
                    }
                });
            }
            if (GraphicsUtils.hasPrintable()) {
                addConstructor("TilingPrinter", new GraphicsViewConstructor() {
                    public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                        environment.getSecurityHelper().checkPrint();
                        if (!(obj instanceof TilingArguments)) {
                            return null;
                        }
                        TilingArguments tilingArguments = (TilingArguments) obj;
                        ScalingTranslatingGraphicsView scalingTranslatingGraphicsView = new ScalingTranslatingGraphicsView(environment, tilingArguments.insets);
                        scalingTranslatingGraphicsView.setChildView(new TilingPrintingGraphicsView(environment, tilingArguments.pagesWide, tilingArguments.pagesHigh));
                        return scalingTranslatingGraphicsView;
                    }
                });
            }
            addConstructor("ImageRenderer", new GraphicsViewConstructor() {
                /* JADX WARNING: Removed duplicated region for block: B:15:0x0062 A[SYNTHETIC, Splitter:B:15:0x0062] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public frink.graphics.GraphicsView create(frink.expr.Environment r10, java.lang.String r11, java.lang.Object r12, frink.graphics.GraphicsViewFactory r13) throws frink.expr.FrinkSecurityException {
                    /*
                        r9 = this;
                        r8 = 0
                        java.lang.Class<frink.expr.Environment> r1 = frink.expr.Environment.class
                        boolean r1 = r12 instanceof frink.graphics.ImageFileArguments
                        if (r1 == 0) goto L_0x00b2
                        r0 = r12
                        frink.graphics.ImageFileArguments r0 = (frink.graphics.ImageFileArguments) r0
                        r1 = r0
                        java.lang.Object r2 = r1.outTo
                        boolean r2 = r2 instanceof java.io.File
                        if (r2 == 0) goto L_0x001c
                        frink.security.SecurityHelper r3 = r10.getSecurityHelper()
                        java.lang.Object r2 = r1.outTo
                        java.io.File r2 = (java.io.File) r2
                        r3.checkWrite(r2)
                    L_0x001c:
                        frink.graphics.ScalingTranslatingGraphicsView r3 = new frink.graphics.ScalingTranslatingGraphicsView
                        frink.units.Unit r2 = r1.insets
                        r3.<init>(r10, r2)
                        frink.graphics.GraphicsView r4 = frink.graphics.InternalRendererManager.create(r10, r11, r12, r13)
                        if (r4 != 0) goto L_0x00b8
                        boolean r2 = frink.graphics.GraphicsUtils.hasImageIO()
                        if (r2 == 0) goto L_0x00b6
                        r9.getClass()     // Catch:{ Exception -> 0x00a7 }
                        java.lang.String r2 = "frink.graphics.ImageRendererGraphicsView"
                        java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x00a7 }
                        if (r2 == 0) goto L_0x00b6
                        r5 = 2
                        java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x00a7 }
                        r6 = 0
                        java.lang.Class<frink.expr.Environment> r7 = frink.expr.Environment.class
                        r5[r6] = r7     // Catch:{ Exception -> 0x00a7 }
                        r6 = 1
                        java.lang.Class r7 = r1.getClass()     // Catch:{ Exception -> 0x00a7 }
                        r5[r6] = r7     // Catch:{ Exception -> 0x00a7 }
                        java.lang.reflect.Constructor r2 = r2.getConstructor(r5)     // Catch:{ Exception -> 0x00a7 }
                        r5 = 2
                        java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00a7 }
                        r6 = 0
                        r5[r6] = r10     // Catch:{ Exception -> 0x00a7 }
                        r6 = 1
                        r5[r6] = r1     // Catch:{ Exception -> 0x00a7 }
                        java.lang.Object r2 = r2.newInstance(r5)     // Catch:{ Exception -> 0x00a7 }
                        frink.graphics.GraphicsView r2 = (frink.graphics.GraphicsView) r2     // Catch:{ Exception -> 0x00a7 }
                    L_0x005c:
                        boolean r4 = frink.graphics.GraphicsUtils.hasAndroidBitmap()
                        if (r4 == 0) goto L_0x00b4
                        r9.getClass()     // Catch:{ Exception -> 0x00aa }
                        java.lang.String r4 = "frink.android.AndroidImageRenderer"
                        java.lang.Class r4 = java.lang.Class.forName(r4)     // Catch:{ Exception -> 0x00aa }
                        if (r4 == 0) goto L_0x00b4
                        r5 = 2
                        java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x00aa }
                        r6 = 0
                        java.lang.Class<frink.expr.Environment> r7 = frink.expr.Environment.class
                        r5[r6] = r7     // Catch:{ Exception -> 0x00aa }
                        r6 = 1
                        java.lang.Class r7 = r1.getClass()     // Catch:{ Exception -> 0x00aa }
                        r5[r6] = r7     // Catch:{ Exception -> 0x00aa }
                        java.lang.reflect.Constructor r5 = r4.getConstructor(r5)     // Catch:{ Exception -> 0x00aa }
                        r6 = 2
                        java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x00aa }
                        r7 = 0
                        r6[r7] = r10     // Catch:{ Exception -> 0x00aa }
                        r7 = 1
                        r6[r7] = r1     // Catch:{ Exception -> 0x00aa }
                        java.lang.Object r1 = r5.newInstance(r6)     // Catch:{ Exception -> 0x00aa }
                        java.lang.String r5 = "getGraphicsView"
                        r6 = 0
                        java.lang.reflect.Method r4 = r4.getMethod(r5, r6)     // Catch:{ Exception -> 0x00aa }
                        r5 = 0
                        java.lang.Object r9 = r4.invoke(r1, r5)     // Catch:{ Exception -> 0x00aa }
                        frink.graphics.GraphicsView r9 = (frink.graphics.GraphicsView) r9     // Catch:{ Exception -> 0x00aa }
                        frink.graphics.GraphicsView r9 = (frink.graphics.GraphicsView) r9     // Catch:{ Exception -> 0x00aa }
                        r1 = r9
                    L_0x009e:
                        if (r1 != 0) goto L_0x00ad
                        java.lang.String r1 = "Could not construct suitable image renderer."
                        r10.outputln(r1)
                        r1 = r8
                    L_0x00a6:
                        return r1
                    L_0x00a7:
                        r2 = move-exception
                        r2 = r4
                        goto L_0x005c
                    L_0x00aa:
                        r1 = move-exception
                        r1 = r2
                        goto L_0x009e
                    L_0x00ad:
                        r3.setChildView(r1)
                        r1 = r3
                        goto L_0x00a6
                    L_0x00b2:
                        r1 = r8
                        goto L_0x00a6
                    L_0x00b4:
                        r1 = r2
                        goto L_0x009e
                    L_0x00b6:
                        r2 = r4
                        goto L_0x005c
                    L_0x00b8:
                        r1 = r4
                        goto L_0x009e
                    */
                    throw new UnsupportedOperationException("Method not decompiled: frink.graphics.BasicGraphicsViewFactory.AnonymousClass6.create(frink.expr.Environment, java.lang.String, java.lang.Object, frink.graphics.GraphicsViewFactory):frink.graphics.GraphicsView");
                }
            });
            addConstructor("DeferredWindow", new GraphicsViewConstructor() {
                public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                    environment.getSecurityHelper().checkOpenGraphicsWindow();
                    try {
                        getClass();
                        if (Class.forName("javax.swing.JFrame") != null) {
                            graphicsViewFactory.setDefaultConstructorName("ScalingTranslatingJComponentFrame");
                            return graphicsViewFactory.createDefault(environment, str, obj);
                        }
                    } catch (Exception e) {
                    }
                    graphicsViewFactory.setDefaultConstructorName("ScalingTranslatingCanvasFrame");
                    return graphicsViewFactory.createDefault(environment, str, obj);
                }
            });
            this.initialized = true;
        }
    }

    public synchronized void addConstructor(String str, GraphicsViewConstructor graphicsViewConstructor) {
        if (this.constructors == null) {
            this.constructors = new Hashtable<>();
        }
        this.constructors.put(str, graphicsViewConstructor);
    }

    public GraphicsView createDefault(Environment environment, String str, Object obj) throws FrinkSecurityException {
        return create(this.defaultConstructorName, environment, str, obj);
    }

    public GraphicsView create(String str, Environment environment, String str2, Object obj) throws FrinkSecurityException {
        if (!this.initialized) {
            initializeConstructors();
        }
        GraphicsViewConstructor graphicsViewConstructor = this.constructors.get(str);
        if (graphicsViewConstructor != null) {
            return graphicsViewConstructor.create(environment, str2, obj, this);
        }
        return null;
    }

    public PaintController createDefaultPaintController(Drawable drawable, Environment environment, String str, Object obj) throws FrinkSecurityException {
        return createPaintController(this.defaultConstructorName, drawable, environment, str, obj);
    }

    public PaintController createPaintController(String str, Drawable drawable, Environment environment, String str2, Object obj) throws FrinkSecurityException {
        GraphicsView create = create(str, environment, str2, obj);
        if (create == null) {
            return null;
        }
        PaintController paintController = new PaintController(drawable, create, environment);
        create.setPaintRequestListener(paintController);
        create.setPrintRequestListener(paintController);
        create.drawableModified();
        return paintController;
    }
}
