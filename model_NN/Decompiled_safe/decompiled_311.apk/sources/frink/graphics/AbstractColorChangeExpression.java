package frink.graphics;

import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidChildException;
import frink.symbolic.MatchingContext;

public abstract class AbstractColorChangeExpression implements Drawable, GraphicsExpression {
    protected FrinkColor color;

    protected AbstractColorChangeExpression(FrinkColor frinkColor) {
        this.color = frinkColor;
    }

    public FrinkColor getColor() {
        return this.color;
    }

    public BoundingBox getBoundingBox() {
        return null;
    }

    public Drawable getDrawable() {
        return this;
    }

    public int getChildCount() {
        return 4;
    }

    public Expression getChild(int i) throws InvalidChildException {
        switch (i) {
            case 0:
                return DimensionlessUnitExpression.construct(((double) this.color.getRed()) / 255.0d);
            case 1:
                return DimensionlessUnitExpression.construct(((double) this.color.getGreen()) / 255.0d);
            case 2:
                return DimensionlessUnitExpression.construct(((double) this.color.getBlue()) / 255.0d);
            case 3:
                return DimensionlessUnitExpression.construct(((double) this.color.getAlpha()) / 255.0d);
            default:
                throw new InvalidChildException("Invalid child for color.", this);
        }
    }

    public boolean isConstant() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression.getExpressionType() == getExpressionType()) {
            int i = 0;
            while (i <= 3) {
                try {
                    if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                        return false;
                    }
                    i++;
                } catch (InvalidChildException e) {
                }
            }
            return true;
        }
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }
}
