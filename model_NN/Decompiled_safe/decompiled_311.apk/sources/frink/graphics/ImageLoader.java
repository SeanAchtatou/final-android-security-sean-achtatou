package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import java.io.IOException;
import java.net.URL;

public interface ImageLoader<T> {
    T getImage(URL url, Environment environment) throws IOException, FrinkSecurityException;
}
