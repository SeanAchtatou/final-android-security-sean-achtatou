package frink.graphics;

import frink.expr.BasicStringExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidChildException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;

public class FontChangeExpression implements Drawable, GraphicsExpression {
    public static final String TYPE = "FontChangeExpression";
    private Unit fontHeight;
    private String fontName;
    private int style;

    public FontChangeExpression(String str, int i, Unit unit) {
        this.fontName = str;
        this.fontHeight = unit;
        this.style = i;
    }

    public BoundingBox getBoundingBox() {
        return null;
    }

    public Drawable getDrawable() {
        return this;
    }

    public int getChildCount() {
        return 3;
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.setFont(this.fontName, this.style, this.fontHeight);
    }

    public Expression getChild(int i) throws InvalidChildException {
        switch (i) {
            case 0:
                return new BasicStringExpression(this.fontName);
            case 1:
                return DimensionlessUnitExpression.construct(this.style);
            case 2:
                return BasicUnitExpression.construct(this.fontHeight);
            default:
                throw new InvalidChildException("Invalid child for color.", this);
        }
    }

    public boolean isConstant() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression.getExpressionType() == getExpressionType()) {
            int i = 0;
            while (i <= 3) {
                try {
                    if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                        return false;
                    }
                    i++;
                } catch (InvalidChildException e) {
                }
            }
            return true;
        }
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public static int parseStyle(String str) {
        String lowerCase = str.toLowerCase();
        int i = 0;
        if (lowerCase.indexOf("bold") != -1) {
            i = 0 + 1;
        }
        if (lowerCase.indexOf("italic") != -1) {
            return i + 2;
        }
        return i;
    }
}
