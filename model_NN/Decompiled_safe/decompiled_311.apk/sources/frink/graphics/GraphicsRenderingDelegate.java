package frink.graphics;

import frink.numeric.NumericException;
import frink.units.Unit;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

public class GraphicsRenderingDelegate implements RenderingDelegate {
    private AWTGraphicsView awtgv;
    private Graphics g;

    public GraphicsRenderingDelegate(AWTGraphicsView aWTGraphicsView, Graphics graphics) {
        this.awtgv = aWTGraphicsView;
        setGraphics(graphics);
    }

    public void setGraphics(Graphics graphics) {
        this.g = graphics;
    }

    public void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                this.g.drawLine(Coord.getIntCoord(unit, width, deviceResolution), Coord.getIntCoord(unit2, height, deviceResolution), Coord.getIntCoord(unit3, width, deviceResolution), Coord.getIntCoord(unit4, height, deviceResolution));
            } catch (NumericException e) {
                System.err.println("GraphicsRenderingDelegate.drawLine:  NumericException:\n  " + e);
            }
        }
    }

    public void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                int intCoord = Coord.getIntCoord(unit, width, deviceResolution);
                int intCoord2 = Coord.getIntCoord(unit2, height, deviceResolution);
                int intCoord3 = Coord.getIntCoord(unit3, width, deviceResolution);
                int intCoord4 = Coord.getIntCoord(unit4, height, deviceResolution);
                if (z) {
                    this.g.fillRect(intCoord, intCoord2, intCoord3, intCoord4);
                } else {
                    this.g.drawRect(intCoord, intCoord2, intCoord3, intCoord4);
                }
            } catch (NumericException e) {
                System.err.println("GraphicsRenderingDelegate.drawRectangle:  NumericException:\n  " + e);
            }
        }
    }

    public void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                int intCoord = Coord.getIntCoord(unit, width, deviceResolution);
                int intCoord2 = Coord.getIntCoord(unit2, height, deviceResolution);
                int intCoord3 = Coord.getIntCoord(unit3, width, deviceResolution);
                int intCoord4 = Coord.getIntCoord(unit4, height, deviceResolution);
                if (z) {
                    this.g.fillOval(intCoord, intCoord2, intCoord3, intCoord4);
                } else {
                    this.g.drawOval(intCoord, intCoord2, intCoord3, intCoord4);
                }
            } catch (NumericException e) {
                System.err.println("GraphicsRenderingDelegate.drawEllipse:  NumericException:\n  " + e);
            }
        }
    }

    public void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                int pointCount = frinkPointList.getPointCount();
                int[] iArr = new int[pointCount];
                int[] iArr2 = new int[pointCount];
                for (int i = 0; i < pointCount; i++) {
                    FrinkPoint point = frinkPointList.getPoint(i);
                    iArr[i] = Coord.getIntCoord(point.getX(), width, deviceResolution);
                    iArr2[i] = Coord.getIntCoord(point.getY(), height, deviceResolution);
                }
                if (!z) {
                    this.g.drawPolyline(iArr, iArr2, pointCount);
                } else if (z2) {
                    this.g.fillPolygon(iArr, iArr2, pointCount);
                } else {
                    this.g.drawPolygon(iArr, iArr2, pointCount);
                }
            } catch (NumericException e) {
                System.err.println("GraphicsRenderingDelegate.drawPoly:  NumericException:\n  " + e);
            }
        }
    }

    public void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z) {
        System.err.println("General path not implemented in this version of Java.");
    }

    public void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        ImageObserver imageObserver;
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                if (frinkImage instanceof ImageObserver) {
                    imageObserver = (ImageObserver) frinkImage;
                } else {
                    imageObserver = null;
                }
                this.g.drawImage((Image) frinkImage.getImage(), Coord.getIntCoord(unit, width, deviceResolution), Coord.getIntCoord(unit2, height, deviceResolution), Coord.getIntCoord(unit3, width, deviceResolution), Coord.getIntCoord(unit4, height, deviceResolution), imageObserver);
            } catch (NumericException e) {
                System.err.println("GraphicsRenderingDelegate.drawImage:  NumericException:\n  " + e);
            }
        }
    }

    public void drawText(String str, Unit unit, Unit unit2, int i, int i2) {
        BoundingBox rendererBoundingBox = this.awtgv.getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = this.awtgv.getDeviceResolution();
            try {
                int intCoord = Coord.getIntCoord(unit, width, deviceResolution);
                int intCoord2 = Coord.getIntCoord(unit2, height, deviceResolution);
                FontMetrics fontMetrics = this.g.getFontMetrics();
                int stringWidth = fontMetrics.stringWidth(str);
                int ascent = fontMetrics.getAscent();
                int descent = fontMetrics.getDescent();
                switch (i) {
                    case 1:
                        break;
                    case 2:
                        intCoord -= stringWidth;
                        break;
                    default:
                        intCoord -= stringWidth / 2;
                        break;
                }
                switch (i2) {
                    case 1:
                        intCoord2 += ascent;
                        break;
                    case 2:
                        intCoord2 -= descent;
                        break;
                    case 3:
                        break;
                    default:
                        intCoord2 += (ascent - descent) / 2;
                        break;
                }
                this.g.drawString(str, intCoord, intCoord2);
            } catch (NumericException e) {
                System.err.println("GraphicsRenderingDelegate.drawText:  NumericException:\n  " + e);
            }
        }
    }

    public void setStroke(Unit unit) {
    }
}
