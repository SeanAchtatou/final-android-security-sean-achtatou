package frink.graphics;

import frink.errors.NotAnIntegerException;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.ListExpression;
import frink.expr.StringExpression;
import frink.function.BuiltinFunctionSource;
import frink.object.ObjectSource;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class GraphicsObjectSource implements ObjectSource {
    public static final GraphicsObjectSource INSTANCE = new GraphicsObjectSource();

    private GraphicsObjectSource() {
    }

    public String getName() {
        return "GraphicsObjectSource";
    }

    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException {
        if (str.equals("graphics")) {
            return constructGraphics(listExpression, environment);
        }
        if (str.equals("filledPolygon")) {
            return constructFilledPolygon(listExpression, environment);
        }
        if (str.equals("polygon")) {
            return constructPolygon(listExpression, environment);
        }
        if (str.equals("polyline")) {
            return constructPolyline(listExpression, environment);
        }
        if (str.equals(GeneralPathExpression.TYPE)) {
            return constructGeneralPath(listExpression, environment);
        }
        if (str.equals("filledGeneralPath")) {
            return constructFilledGeneralPath(listExpression, environment);
        }
        if (str.equals("image")) {
            return constructImage(listExpression, environment);
        }
        if (str.equals("color")) {
            return constructColor(listExpression, environment);
        }
        return null;
    }

    public Expression constructFilledPolygon(ListExpression listExpression, Environment environment) {
        return new PolyShapeExpression(true, true);
    }

    public Expression constructPolygon(ListExpression listExpression, Environment environment) {
        return new PolyShapeExpression(true, false);
    }

    public Expression constructPolyline(ListExpression listExpression, Environment environment) {
        return new PolyShapeExpression(false, false);
    }

    public Expression constructFilledGeneralPath(ListExpression listExpression, Environment environment) {
        return new GeneralPathExpression(true);
    }

    public Expression constructGeneralPath(ListExpression listExpression, Environment environment) {
        return new GeneralPathExpression(false);
    }

    public Expression constructImage(ListExpression listExpression, Environment environment) throws EvaluationException {
        if (listExpression != null && listExpression.getChildCount() == 1) {
            Expression child = listExpression.getChild(0);
            if (child instanceof StringExpression) {
                String string = ((StringExpression) child).getString();
                try {
                    return new FrinkImageExpression(environment.getFrinkImageLoader().getFrinkImage(new URL(string), environment));
                } catch (MalformedURLException e) {
                    throw new InvalidArgumentException("Invalid URL in image constructor:\n  " + e, listExpression.getChild(0));
                } catch (IOException e2) {
                    throw new InvalidArgumentException("Error in opening URL for image: " + string + "\n  " + e2, listExpression);
                }
            } else {
                throw new InvalidArgumentException("The argument to \"new image[str]\" should be a string containing a URL.", listExpression);
            }
        } else if (listExpression == null || listExpression.getChildCount() != 2) {
            throw new InvalidArgumentException("Invalid arguments to \"new image[...]\".  Arguments must be [URL] or [width,height]", listExpression);
        } else {
            try {
                return new FrinkImageExpression(GraphicsUtils.constructFrinkImage(GraphicsUtils.constructWritableImage(BuiltinFunctionSource.getIntegerValue(listExpression.getChild(0)), BuiltinFunctionSource.getIntegerValue(listExpression.getChild(1))), "<<Constructed in memory>>", environment));
            } catch (NotAnIntegerException e3) {
                throw new InvalidArgumentException("The arguments to \"new image[width, height]\" should both be integers.", listExpression);
            }
        }
    }

    public Expression constructGraphics(ListExpression listExpression, Environment environment) throws EvaluationException {
        return new GraphicsExpressionList();
    }

    public Expression constructColor(ListExpression listExpression, Environment environment) throws EvaluationException {
        FrinkColor constructColorFromExpressions;
        try {
            int childCount = listExpression.getChildCount();
            if (childCount >= 3 && childCount <= 4) {
                Expression child = listExpression.getChild(0);
                Expression child2 = listExpression.getChild(1);
                Expression child3 = listExpression.getChild(2);
                if (childCount == 3) {
                    constructColorFromExpressions = GraphicsExpressionList.constructColorFromExpressions(environment, child, child2, child3);
                } else {
                    constructColorFromExpressions = GraphicsExpressionList.constructColorFromExpressions(environment, child, child2, child3, listExpression.getChild(3));
                }
                return ColorChangeExpression.construct(constructColorFromExpressions);
            }
        } catch (InvalidArgumentException e) {
        }
        throw new InvalidArgumentException("Arguments to new color should be 3 or 4 values dimensionless integers between 0 and 1 inclusive.", listExpression);
    }
}
