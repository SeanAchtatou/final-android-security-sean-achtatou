package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import java.util.Vector;

public class DrawableList implements Drawable {
    private BoundingBox bbox = null;
    private Vector<Drawable> drawables = null;

    public void add(Drawable drawable) {
        if (this.drawables == null) {
            this.drawables = new Vector<>();
            this.bbox = drawable.getBoundingBox();
        } else {
            try {
                this.bbox = BoundingBox.union(this.bbox, drawable.getBoundingBox());
            } catch (ConformanceException | NumericException | OverlapException e) {
            }
        }
        this.drawables.addElement(drawable);
    }

    public void draw(GraphicsView graphicsView) {
        if (this.drawables != null) {
            int size = this.drawables.size();
            for (int i = 0; i < size; i++) {
                this.drawables.elementAt(i).draw(graphicsView);
            }
        }
    }

    public BoundingBox getBoundingBox() {
        return this.bbox;
    }
}
