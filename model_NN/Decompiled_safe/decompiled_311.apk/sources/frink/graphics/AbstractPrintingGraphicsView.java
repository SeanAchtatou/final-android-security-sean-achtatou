package frink.graphics;

import frink.expr.Environment;
import frink.units.Unit;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

public abstract class AbstractPrintingGraphicsView extends AWTGraphicsView implements Printable {
    private static PageFormat lastFormat = null;
    protected BoundingBox bbox;
    protected boolean boundingBoxNeedsRecalculation;
    protected Environment env;
    protected PageFormat pf = null;
    private Unit resolution;

    public abstract int print(Graphics graphics, PageFormat pageFormat, int i) throws PrinterException;

    /* access modifiers changed from: protected */
    public abstract void recalculateBoundingBox();

    public AbstractPrintingGraphicsView(Environment environment) {
        this.env = environment;
        this.bbox = null;
        this.boundingBoxNeedsRecalculation = true;
        this.resolution = GraphicsUtils.get72PerInch(this.env);
    }

    public Unit getDeviceResolution() {
        return this.resolution;
    }

    public BoundingBox getRendererBoundingBox() {
        if (this.boundingBoxNeedsRecalculation) {
            recalculateBoundingBox();
        }
        return this.bbox;
    }

    public void drawableModified() {
        if (this.child != null) {
            this.child.drawableModified();
        }
        requestPrint();
    }

    private void requestPrint() {
        PageFormat pageFormat;
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        if (lastFormat == null) {
            pageFormat = printerJob.defaultPage();
        } else {
            pageFormat = lastFormat;
        }
        PageFormat pageDialog = printerJob.pageDialog(pageFormat);
        lastFormat = pageDialog;
        printerJob.setPrintable(this, pageDialog);
        if (printerJob.printDialog()) {
            try {
                printerJob.print();
            } catch (PrinterException e) {
                System.out.println(e);
            }
        }
    }

    public void rendererResized() {
        if (this.parent != null) {
            this.parent.rendererResized();
        }
    }
}
