package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;

public class RectangleExpression extends ShapeExpression {
    public static final String TYPE = "RectangleExpression";

    public RectangleExpression(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z, Unit unit5) throws ConformanceException, NumericException, OverlapException {
        super(unit, unit2, unit3, unit4, z, unit5);
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.drawRectangle(this.shapeBox.getLeft(), this.shapeBox.getTop(), this.shapeBox.getWidth(), this.shapeBox.getHeight(), isFilled());
    }

    public String getExpressionType() {
        return TYPE;
    }
}
