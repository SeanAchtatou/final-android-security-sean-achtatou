package frink.graphics;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidChildException;
import frink.symbolic.MatchingContext;

public abstract class SingleGraphicsExpression implements GraphicsExpression, Drawable {
    public abstract void draw(GraphicsView graphicsView);

    public abstract String getExpressionType();

    public abstract boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z);

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public int getChildCount() {
        return 0;
    }

    public Expression getChild(int i) throws InvalidChildException {
        throw new InvalidChildException("SingleGraphicsExpression has no children", this);
    }

    public boolean isConstant() {
        return false;
    }
}
