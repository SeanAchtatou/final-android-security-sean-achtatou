package frink.graphics;

import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.SelfDisplayingExpression;
import frink.expr.TerminalExpression;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.FunctionSource;
import frink.function.SingleArgMethod;
import frink.function.ZeroArgMethod;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;

public class PaintController extends TerminalExpression implements PaintRequestListener, PrintRequestListener, FrinkObject, SelfDisplayingExpression {
    public static final String TYPE = "PaintController";
    private static final PaintControllerFunctionSource methods = new PaintControllerFunctionSource();
    private EmptyObjectContextFrame contextFrame = null;
    /* access modifiers changed from: private */
    public Drawable drawable;
    private Environment env;
    /* access modifiers changed from: private */
    public GraphicsView gv;

    public PaintController(Drawable drawable2, GraphicsView graphicsView, Environment environment) {
        this.drawable = drawable2;
        this.gv = graphicsView;
        this.env = environment;
    }

    public BoundingBox getDrawableBoundingBox() {
        return this.drawable.getBoundingBox();
    }

    public void paintRequested(GraphicsView graphicsView) {
        this.drawable.draw(graphicsView);
    }

    public void printRequested() {
        try {
            this.env.getSecurityHelper().checkPrint();
            this.env.getGraphicsViewFactory().createPaintController("ScalingTranslatingPrinter", this.drawable, this.env, "Print Job", new PrintingArguments());
        } catch (FrinkSecurityException e) {
            this.env.outputln("Printing denied due to security policy.");
        }
    }

    public void repaint() {
        if (this.gv != null) {
            this.gv.drawableModified();
        }
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return methods;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public boolean isA(String str) {
        return TYPE.equals(str);
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String toString(Environment environment, boolean z) {
        return "";
    }

    private static class PaintControllerFunctionSource extends BasicFunctionSource {
        PaintControllerFunctionSource() {
            super(PaintController.TYPE);
            addFunctionDefinition("repaint", new ZeroArgMethod<PaintController>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, PaintController paintController) {
                    paintController.repaint();
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("replaceGraphics", new SingleArgMethod<PaintController>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, PaintController paintController, Expression expression) throws InvalidArgumentException {
                    if (expression instanceof GraphicsExpression) {
                        Drawable unused = paintController.drawable = ((GraphicsExpression) expression).getDrawable();
                        paintController.gv.drawableModified();
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("PaintController.replaceGraphics[g] requires a graphics object.", expression);
                }
            });
        }
    }
}
