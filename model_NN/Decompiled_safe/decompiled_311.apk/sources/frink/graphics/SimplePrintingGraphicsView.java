package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.DimensionlessUnit;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public class SimplePrintingGraphicsView extends AbstractPrintingGraphicsView implements Printable, BackgroundChangedListener {
    public SimplePrintingGraphicsView(Environment environment) {
        super(environment);
        setBackgroundChangedListener(this);
    }

    public void backgroundChanged(FrinkColor frinkColor) {
        if (this.g != null) {
            Color color = this.g.getColor();
            this.g.setColor(new Color(frinkColor.getPacked(), true));
            this.g.fillRect(0, 0, (int) this.pf.getWidth(), (int) this.pf.getHeight());
            this.g.setColor(color);
        }
    }

    public int print(Graphics graphics, PageFormat pageFormat, int i) throws PrinterException {
        if (i > 0) {
            return 1;
        }
        setGraphics(graphics);
        this.pf = pageFormat;
        this.boundingBoxNeedsRecalculation = true;
        paintRequested();
        return 0;
    }

    /* access modifiers changed from: protected */
    public synchronized void recalculateBoundingBox() {
        if (this.boundingBoxNeedsRecalculation) {
            if (this.pf == null) {
                System.out.println("SimplePrintingGraphicsView.recalculateBoundingBox: no PageFormat object.");
            } else {
                try {
                    this.bbox = new BoundingBox(DimensionlessUnit.construct(this.pf.getImageableX()), DimensionlessUnit.construct(this.pf.getImageableY()), DimensionlessUnit.construct(this.pf.getImageableWidth() + this.pf.getImageableX()), DimensionlessUnit.construct(this.pf.getImageableHeight() + this.pf.getImageableY()));
                } catch (ConformanceException e) {
                    this.bbox = null;
                } catch (NumericException e2) {
                    this.bbox = null;
                } catch (OverlapException e3) {
                    this.bbox = null;
                }
                this.boundingBoxNeedsRecalculation = false;
            }
        }
    }
}
