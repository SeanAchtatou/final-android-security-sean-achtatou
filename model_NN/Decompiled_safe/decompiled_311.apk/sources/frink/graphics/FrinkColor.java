package frink.graphics;

public interface FrinkColor {
    int getAlpha();

    int getBlue();

    int getGreen();

    int getPacked();

    int getRed();
}
