package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.expr.NotSupportedException;
import java.io.File;
import java.io.IOException;

public interface FrinkImage<T> {
    int getHeight();

    T getImage();

    int getPixelPacked(int i, int i2) throws NotSupportedException;

    String getSource();

    int getWidth();

    void makeARGB();

    void setPixelPacked(int i, int i2, int i3) throws NotSupportedException;

    void write(File file, String str, Environment environment) throws IOException, NotSupportedException, FrinkSecurityException;
}
