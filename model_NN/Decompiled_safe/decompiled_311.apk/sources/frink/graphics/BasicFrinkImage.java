package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.expr.NotSupportedException;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class BasicFrinkImage implements FrinkImage, ImageObserver {
    private boolean error = false;
    private Image image;
    private String location;

    public BasicFrinkImage(Image image2, String str) {
        this.image = image2;
        this.location = str;
    }

    public int getWidth() {
        if (this.error || this.image == null) {
            return 0;
        }
        synchronized (this.image) {
            while (this.image.getWidth(this) == -1) {
                try {
                    this.image.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        return this.image.getWidth(this);
    }

    public int getHeight() {
        if (this.error || this.image == null) {
            return 0;
        }
        synchronized (this.image) {
            while (this.image.getHeight(this) == -1) {
                try {
                    this.image.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        return this.image.getHeight(this);
    }

    public Image getImage() {
        return this.image;
    }

    public int getPixelPacked(int i, int i2) throws NotSupportedException {
        throw new NotSupportedException("BasicFrinkImage:  This image (and your Java platform) does not seem to allow manipulation of individual pixels in an image.", null);
    }

    public void setPixelPacked(int i, int i2, int i3) throws NotSupportedException {
        throw new NotSupportedException("BasicFrinkImage:  This image (and your Java platform) does not seem to allow manipulation of individual pixels in an image.", null);
    }

    public void makeARGB() {
    }

    public void write(File file, String str, Environment environment) throws IOException, NotSupportedException, FrinkSecurityException {
        throw new NotSupportedException("BasicFrinkImage:  Your Java implementation does not seem to support writing of images.  Image '" + file + "' will not be written.", null);
    }

    public ImageObserver getImageObserver() {
        return this;
    }

    public boolean imageUpdate(Image image2, int i, int i2, int i3, int i4, int i5) {
        if ((i & 64) != 0) {
            this.error = true;
            System.err.println("Error when loading image.");
            synchronized (this.image) {
                this.image.notifyAll();
            }
            return false;
        } else if ((i & 32) != 0) {
            return false;
        } else {
            if (!(i4 == -1 && i5 == -1)) {
                synchronized (this.image) {
                    this.image.notifyAll();
                }
            }
            return true;
        }
    }

    public String getSource() {
        return this.location;
    }

    public static void main(String[] strArr) {
        String str = "http://futureboy.us/images/futureboydomethumb4.gif";
        if (strArr.length > 0) {
            str = strArr[0];
        }
        try {
            URL url = new URL(str);
            System.out.println("Loading " + url);
            BasicFrinkImage basicFrinkImage = new BasicFrinkImage(Toolkit.getDefaultToolkit().getImage(url), str);
            System.out.println(basicFrinkImage.getWidth() + " x " + basicFrinkImage.getHeight());
        } catch (MalformedURLException e) {
            System.out.println(e);
        }
    }
}
