package frink.graphics;

public interface BackgroundChangedListener {
    void backgroundChanged(FrinkColor frinkColor);
}
