package frink.graphics;

import frink.units.Unit;

public interface GraphicsView {
    void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z);

    void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z);

    void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4);

    void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4);

    void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2);

    void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z);

    void drawText(String str, Unit unit, Unit unit2, int i, int i2);

    void drawableModified();

    FrinkColor getBackgroundColor();

    GraphicsView getChildView();

    FrinkColor getColor();

    Unit getDeviceResolution();

    BoundingBox getDrawableBoundingBox();

    PaintRequestListener getPaintRequestListener();

    GraphicsView getParentView();

    PrintRequestListener getPrintRequestListener();

    BoundingBox getRendererBoundingBox();

    void paintRequested();

    void printRequested();

    void rendererResized();

    void setBackgroundColor(FrinkColor frinkColor);

    void setChildView(GraphicsView graphicsView);

    void setColor(FrinkColor frinkColor);

    void setFont(String str, int i, Unit unit);

    void setPaintRequestListener(PaintRequestListener paintRequestListener);

    void setParentView(GraphicsView graphicsView);

    void setPrintRequestListener(PrintRequestListener printRequestListener);

    void setStroke(Unit unit);
}
