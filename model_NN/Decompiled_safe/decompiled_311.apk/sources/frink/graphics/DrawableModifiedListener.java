package frink.graphics;

public interface DrawableModifiedListener {
    void drawableModified();
}
