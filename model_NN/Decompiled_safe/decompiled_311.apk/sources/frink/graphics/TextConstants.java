package frink.graphics;

public class TextConstants {
    public static final int HORIZ_CENTER = 0;
    public static final int HORIZ_LEFT = 1;
    public static final int HORIZ_RIGHT = 2;
    public static final int VERT_BASELINE = 3;
    public static final int VERT_BOTTOM = 2;
    public static final int VERT_CENTER = 0;
    public static final int VERT_TOP = 1;

    public static int parseHorizontalAlignment(String str) {
        if (str.equalsIgnoreCase("left")) {
            return 1;
        }
        if (str.equalsIgnoreCase("right")) {
            return 2;
        }
        return 0;
    }

    public static int parseVerticalAlignment(String str) {
        if (str.equalsIgnoreCase("top")) {
            return 1;
        }
        if (str.equalsIgnoreCase("bottom")) {
            return 2;
        }
        if (str.equalsIgnoreCase("baseline")) {
            return 3;
        }
        return 0;
    }
}
