package frink.graphics;

import frink.units.DimensionlessUnit;
import frink.units.Unit;

public class PrintingArguments {
    public Unit insets;

    public PrintingArguments() {
        this(DimensionlessUnit.ONE);
    }

    public PrintingArguments(Unit unit) {
        this.insets = unit;
    }
}
