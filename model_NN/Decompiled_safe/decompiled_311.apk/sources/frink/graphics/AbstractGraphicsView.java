package frink.graphics;

public abstract class AbstractGraphicsView implements GraphicsView {
    protected GraphicsView child = null;
    private PaintRequestListener paintListener = null;
    protected GraphicsView parent = null;
    private PrintRequestListener printListener = null;

    public void setPaintRequestListener(PaintRequestListener paintRequestListener) {
        this.paintListener = paintRequestListener;
    }

    public PaintRequestListener getPaintRequestListener() {
        return this.paintListener;
    }

    public void setPrintRequestListener(PrintRequestListener printRequestListener) {
        this.printListener = printRequestListener;
    }

    public PrintRequestListener getPrintRequestListener() {
        return this.printListener;
    }

    public GraphicsView getParentView() {
        return this.parent;
    }

    public GraphicsView getChildView() {
        return this.child;
    }

    public void setParentView(GraphicsView graphicsView) {
        this.parent = graphicsView;
        if (this.parent.getChildView() != this) {
            this.parent.setChildView(this);
        }
    }

    public void setChildView(GraphicsView graphicsView) {
        this.child = graphicsView;
        if (this.child.getParentView() != this) {
            this.child.setParentView(this);
        }
    }

    public BoundingBox getDrawableBoundingBox() {
        if (this.parent != null) {
            return this.parent.getDrawableBoundingBox();
        }
        if (this.paintListener != null) {
            return this.paintListener.getDrawableBoundingBox();
        }
        System.err.println("AbstractGraphicsView.getDrawableBoundingBox: error: No parent object!");
        return null;
    }

    /* access modifiers changed from: protected */
    public void callPaintListeners() {
        if (this.paintListener != null) {
            this.paintListener.paintRequested(this);
        }
    }

    /* access modifiers changed from: protected */
    public void callPrintListeners() {
        if (this.printListener != null) {
            this.printListener.printRequested();
        }
    }
}
