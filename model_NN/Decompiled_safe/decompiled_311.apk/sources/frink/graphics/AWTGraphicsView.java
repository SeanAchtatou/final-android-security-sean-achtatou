package frink.graphics;

import frink.expr.Environment;
import frink.numeric.FrinkInt;
import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;

public abstract class AWTGraphicsView extends AbstractGraphicsView {
    protected FrinkColor background = null;
    private BackgroundChangedListener backgroundChangedListener = null;
    private FrinkColor color = null;
    private Font font = null;
    protected Graphics g = null;
    private RenderingDelegate renderer = null;

    public abstract BoundingBox getRendererBoundingBox();

    public void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        if (this.renderer == null) {
            System.err.println("AWTGraphicsView.drawLine:  no delegate!");
        } else {
            this.renderer.drawLine(unit, unit2, unit3, unit4);
        }
    }

    public void setBackgroundChangedListener(BackgroundChangedListener backgroundChangedListener2) {
        this.backgroundChangedListener = backgroundChangedListener2;
    }

    public void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        if (this.renderer == null) {
            System.err.println("AWTGraphicsView.drawRectangle:  no delegate!");
        } else {
            this.renderer.drawRectangle(unit, unit2, unit3, unit4, z);
        }
    }

    public void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        if (this.renderer == null) {
            System.err.println("AWTGraphicsView.drawEllipse:  no delegate!");
        } else {
            this.renderer.drawEllipse(unit, unit2, unit3, unit4, z);
        }
    }

    public void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2) {
        if (this.renderer == null) {
            System.err.println("AWTGraphicsView.drawEllipse:  no delegate!");
        } else {
            this.renderer.drawPoly(frinkPointList, z, z2);
        }
    }

    public void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z) {
        if (this.renderer == null) {
            System.err.println("AWTGraphicsView.drawGeneralPath:  no delegate!");
        } else {
            this.renderer.drawGeneralPath(frinkGeneralPath, z);
        }
    }

    public void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        if (this.renderer == null) {
            System.err.println("AWTGraphicsView.drawImage:  no delegate!");
        } else {
            this.renderer.drawImage(frinkImage, unit, unit2, unit3, unit4);
        }
    }

    public FrinkColor getColor() {
        if (this.color != null) {
            return this.color;
        }
        if (this.g != null) {
            return new BasicFrinkColor(this.g.getColor().getRGB());
        }
        return BasicFrinkColor.BLACK;
    }

    public void setColor(FrinkColor frinkColor) {
        this.color = frinkColor;
        this.g.setColor(new Color(frinkColor.getPacked(), true));
    }

    public FrinkColor getBackgroundColor() {
        return this.background;
    }

    public void setBackgroundColor(FrinkColor frinkColor) {
        this.background = frinkColor;
        if (this.backgroundChangedListener != null) {
            this.backgroundChangedListener.backgroundChanged(frinkColor);
        }
    }

    public void setFont(String str, int i, Unit unit) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            try {
                this.font = FontFactory.construct(str, i, Coord.getIntCoord(unit, rendererBoundingBox.getHeight(), getDeviceResolution()));
                if (this.g != null) {
                    this.g.setFont(this.font);
                }
            } catch (NumericException e) {
                System.err.println("AWTGraphicsView.setFont:  NumericException:\n  " + e);
            }
        }
    }

    public void setStroke(Unit unit) {
        this.renderer.setStroke(unit);
    }

    public void drawText(String str, Unit unit, Unit unit2, int i, int i2) {
        this.renderer.drawText(str, unit, unit2, i, i2);
    }

    public void setGraphics(Graphics graphics) {
        graphics.setColor(Color.black);
        if (this.g != graphics) {
            this.g = graphics;
            if (GraphicsUtils.isGraphics2D(graphics)) {
                try {
                    getClass();
                    Class<?> cls = Class.forName("frink.graphics.Graphics2DRenderingDelegate");
                    if (cls != null) {
                        this.renderer = (RenderingDelegate) cls.getConstructor(AWTGraphicsView.class, Graphics.class).newInstance(this, graphics);
                        return;
                    }
                } catch (Exception e) {
                    System.err.println("Couldn't find frink.graphics.Graphics2DRenderingDelegate:\n  " + e);
                }
            }
            this.renderer = new GraphicsRenderingDelegate(this, graphics);
        }
    }

    public void paintRequested() {
        if (this.parent != null) {
            this.parent.paintRequested();
        }
        callPaintListeners();
    }

    public void printRequested() {
        if (this.parent != null) {
            this.parent.printRequested();
        }
        callPrintListeners();
    }

    public static Unit getScreenResolution(Environment environment) {
        Unit inch = GraphicsUtils.getInch(environment);
        if (inch != null) {
            Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
            if (defaultToolkit == null) {
                environment.outputln("Could not get default toolkit!");
                return null;
            }
            try {
                return UnitMath.divide(FrinkInt.construct(defaultToolkit.getScreenResolution()), inch);
            } catch (NumericException e) {
                environment.outputln("AWTComponentGraphicsView:  weird NumericException\n   " + e);
            }
        }
        return null;
    }
}
