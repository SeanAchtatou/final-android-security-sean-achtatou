package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import java.awt.Color;
import java.awt.Component;

public class AWTComponentGraphicsView extends AWTGraphicsView implements BackgroundChangedListener {
    private BoundingBox bbox = null;
    private final Component component;
    private int lastHeight = -1;
    private int lastWidth = -1;
    private Unit resolution;

    public AWTComponentGraphicsView(Component component2, Environment environment) {
        this.component = component2;
        component2.setBackground(Color.white);
        this.resolution = getScreenResolution(environment);
    }

    private synchronized void recalculateBoundingBox(int i, int i2) {
        try {
            if (!(i == this.lastWidth && i2 == this.lastHeight && this.bbox != null)) {
                this.lastWidth = i;
                this.lastHeight = i2;
                this.bbox = new BoundingBox(0, 0, i, i2);
                rendererResized();
            }
        } catch (ConformanceException e) {
            this.bbox = null;
        } catch (NumericException e2) {
            this.bbox = null;
        } catch (OverlapException e3) {
            this.bbox = null;
        }
        return;
    }

    public BoundingBox getRendererBoundingBox() {
        if (this.bbox == null) {
            recalculateBoundingBox(this.component.getWidth(), this.component.getHeight());
        }
        return this.bbox;
    }

    public Unit getDeviceResolution() {
        return this.resolution;
    }

    public void drawableModified() {
        if (this.child != null) {
            this.child.drawableModified();
        }
        this.component.repaint();
    }

    public void backgroundChanged(FrinkColor frinkColor) {
        Color color = new Color(frinkColor.getPacked(), true);
        this.component.setBackground(color);
        if (this.g != null) {
            Color color2 = this.g.getColor();
            this.g.setColor(color);
            this.g.fillRect(0, 0, this.component.getWidth(), this.component.getHeight());
            this.g.setColor(color2);
        }
    }

    public void rendererResized() {
        if (this.parent != null) {
            this.parent.rendererResized();
        }
    }

    public void paintRequested() {
        recalculateBoundingBox(this.component.getWidth(), this.component.getHeight());
        super.paintRequested();
    }
}
