package frink.graphics;

import frink.units.Unit;

public interface DeferredDrawable {
    void setStrokeWidth(Unit unit);
}
