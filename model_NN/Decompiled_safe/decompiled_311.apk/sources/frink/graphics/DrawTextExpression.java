package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;

public class DrawTextExpression extends SingleGraphicsExpression implements Drawable {
    private BoundingBox bb = null;
    private int horizAlign;
    private String text;
    private int vertAlign;
    private Unit x;
    private Unit y;

    public DrawTextExpression(String str, Unit unit, Unit unit2, Unit unit3, String str2, int i, int i2, Environment environment) throws ConformanceException, NumericException, OverlapException {
        this.text = str;
        this.x = unit;
        this.y = unit2;
        this.horizAlign = i;
        this.vertAlign = i2;
        if (unit3 != null) {
            guessBoundingBox(unit3, str2, environment);
        }
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.drawText(this.text, this.x, this.y, this.horizAlign, this.vertAlign);
    }

    public BoundingBox getBoundingBox() {
        if (this.bb == null) {
            guessBoundingBox(null, null, null);
        }
        return this.bb;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00cb A[Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0111 A[Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x011a A[Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void guessBoundingBox(frink.units.Unit r7, java.lang.String r8, frink.expr.Environment r9) {
        /*
            r6 = this;
            java.lang.String r0 = "DrawTextExpression.guessBoundingBox:  "
            monitor-enter(r6)
            frink.graphics.BoundingBox r0 = r6.bb     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r6)
            return
        L_0x0009:
            if (r7 != 0) goto L_0x0126
            if (r9 != 0) goto L_0x003f
            java.io.PrintStream r0 = java.lang.System.err     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            java.lang.String r1 = "DrawTextExpression: Don't have environment!"
            r0.println(r1)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.graphics.BoundingBox r0 = new frink.graphics.BoundingBox     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r1 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r4 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r0.<init>(r1, r2, r3, r4)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r6.bb = r0     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            goto L_0x0007
        L_0x0024:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x003c }
            r1.<init>()     // Catch:{ all -> 0x003c }
            java.lang.String r2 = "DrawTextExpression.guessBoundingBox:  "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x003c }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x003c }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x003c }
            r9.outputln(r0)     // Catch:{ all -> 0x003c }
            goto L_0x0007
        L_0x003c:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x003f:
            frink.units.Unit r0 = frink.graphics.GraphicsUtils.getInch(r9)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            if (r0 != 0) goto L_0x0072
            java.lang.String r0 = "DrawTextExpression: Don't have inch!"
            r9.outputln(r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.graphics.BoundingBox r0 = new frink.graphics.BoundingBox     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r1 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r4 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r0.<init>(r1, r2, r3, r4)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r6.bb = r0     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            goto L_0x0007
        L_0x005a:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x003c }
            r1.<init>()     // Catch:{ all -> 0x003c }
            java.lang.String r2 = "DrawTextExpression.guessBoundingBox:  "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x003c }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x003c }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x003c }
            r9.outputln(r0)     // Catch:{ all -> 0x003c }
            goto L_0x0007
        L_0x0072:
            r1 = 6
            frink.units.DimensionlessUnit r1 = frink.units.DimensionlessUnit.construct(r1)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r0 = frink.units.UnitMath.divide(r0, r1)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
        L_0x007b:
            frink.units.Unit r1 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            boolean r1 = frink.units.UnitMath.areConformal(r1, r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            if (r1 != 0) goto L_0x008b
            frink.units.DimensionlessUnit r0 = frink.units.DimensionlessUnit.ZERO     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r1 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r0 = frink.units.UnitMath.multiply(r0, r1)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
        L_0x008b:
            frink.units.Unit r1 = frink.units.UnitMath.halve(r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            java.lang.String r2 = r6.text     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            float r2 = frink.graphics.FontWidthEstimator.guessWidthFactor(r2, r8)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            double r2 = (double) r2     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.DimensionlessUnit r2 = frink.units.DimensionlessUnit.construct(r2)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = frink.units.UnitMath.multiply(r2, r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            boolean r3 = frink.units.UnitMath.areConformal(r3, r2)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            if (r3 != 0) goto L_0x00ae
            frink.units.DimensionlessUnit r2 = frink.units.DimensionlessUnit.ZERO     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = frink.units.UnitMath.multiply(r2, r3)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
        L_0x00ae:
            frink.units.Unit r3 = frink.units.UnitMath.halve(r2)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            int r4 = r6.horizAlign     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            switch(r4) {
                case 1: goto L_0x00fc;
                case 2: goto L_0x0105;
                default: goto L_0x00b7;
            }     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
        L_0x00b7:
            frink.units.Unit r2 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = frink.units.UnitMath.subtract(r2, r3)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r4 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r3 = frink.units.UnitMath.add(r4, r3)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r5 = r3
            r3 = r2
            r2 = r5
        L_0x00c6:
            int r4 = r6.vertAlign     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            switch(r4) {
                case 1: goto L_0x0111;
                case 2: goto L_0x011a;
                default: goto L_0x00cb;
            }     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
        L_0x00cb:
            frink.units.Unit r0 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r0 = frink.units.UnitMath.subtract(r0, r1)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r4 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r1 = frink.units.UnitMath.add(r4, r1)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00da:
            frink.graphics.BoundingBox r4 = new frink.graphics.BoundingBox     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r4.<init>(r3, r1, r2, r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r6.bb = r4     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            goto L_0x0007
        L_0x00e3:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x003c }
            r1.<init>()     // Catch:{ all -> 0x003c }
            java.lang.String r2 = "DrawTextExpression.guessBoundingBox:  "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x003c }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x003c }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x003c }
            r9.outputln(r0)     // Catch:{ all -> 0x003c }
            goto L_0x0007
        L_0x00fc:
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r4 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = frink.units.UnitMath.add(r4, r2)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            goto L_0x00c6
        L_0x0105:
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r2 = frink.units.UnitMath.subtract(r3, r2)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r3 = r6.x     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r5 = r3
            r3 = r2
            r2 = r5
            goto L_0x00c6
        L_0x0111:
            frink.units.Unit r1 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r4 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r0 = frink.units.UnitMath.add(r4, r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            goto L_0x00da
        L_0x011a:
            frink.units.Unit r1 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r0 = frink.units.UnitMath.subtract(r1, r0)     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            frink.units.Unit r1 = r6.y     // Catch:{ ConformanceException -> 0x0024, NumericException -> 0x005a, OverlapException -> 0x00e3 }
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00da
        L_0x0126:
            r0 = r7
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.graphics.DrawTextExpression.guessBoundingBox(frink.units.Unit, java.lang.String, frink.expr.Environment):void");
    }

    public void setFontHeight(Unit unit, String str, Environment environment) {
        if (this.bb != null) {
            System.err.println("DrawTextExpression:  bounding box already reported.");
        } else {
            guessBoundingBox(unit, str, environment);
        }
    }

    public String getExpressionType() {
        return "DrawTextExpression";
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        return false;
    }

    public Drawable getDrawable() {
        return this;
    }
}
