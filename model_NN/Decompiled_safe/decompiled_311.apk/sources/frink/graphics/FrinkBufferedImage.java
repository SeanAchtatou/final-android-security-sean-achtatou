package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.expr.NotSupportedException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class FrinkBufferedImage implements FrinkImage, ImageObserver {
    private BufferedImage bImage;
    private String location;

    public FrinkBufferedImage(BufferedImage bufferedImage, String str) {
        this.bImage = bufferedImage;
        this.location = str;
    }

    public int getWidth() {
        return this.bImage.getWidth();
    }

    public int getHeight() {
        return this.bImage.getHeight();
    }

    public BufferedImage getImage() {
        return this.bImage;
    }

    public int getPixelPacked(int i, int i2) {
        return this.bImage.getRGB(i, i2);
    }

    public void setPixelPacked(int i, int i2, int i3) {
        this.bImage.setRGB(i, i2, i3);
    }

    public void makeARGB() {
        this.bImage = changeColorModel(this.bImage, 2);
    }

    private static BufferedImage changeColorModel(BufferedImage bufferedImage, int i) {
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        int[] rgb = bufferedImage.getRGB(0, 0, width, height, (int[]) null, 0, width);
        BufferedImage bufferedImage2 = new BufferedImage(width, height, i);
        bufferedImage2.setRGB(0, 0, width, height, rgb, 0, width);
        return bufferedImage2;
    }

    public void write(File file, String str, Environment environment) throws IOException, NotSupportedException, FrinkSecurityException {
        BufferedImage bufferedImage = this.bImage;
        if (str.toLowerCase().startsWith("jp") && this.bImage.getColorModel().hasAlpha()) {
            bufferedImage = changeColorModel(this.bImage, 1);
        }
        doWrite(file, bufferedImage, str, environment);
    }

    private void doWrite(File file, BufferedImage bufferedImage, String str, Environment environment) throws IOException, NotSupportedException, FrinkSecurityException {
        environment.getSecurityHelper().checkWrite(file);
        if (str == null) {
            environment.outputln("FrinkBufferedImage.write:  No format specified for writing '" + file + "'.  File was not written.");
        } else if (!ImageIO.write(bufferedImage, str, file)) {
            environment.outputln("FrinkBufferedImage.write:  Could not find appropriate writer for format '" + str + "'");
        }
    }

    public ImageObserver getImageObserver() {
        return this;
    }

    public String getSource() {
        return this.location;
    }

    public boolean imageUpdate(Image image, int i, int i2, int i3, int i4, int i5) {
        if ((i & 64) != 0) {
            System.err.println("Error when loading image.");
            return false;
        } else if ((i & 32) != 0) {
            return false;
        } else {
            return true;
        }
    }
}
