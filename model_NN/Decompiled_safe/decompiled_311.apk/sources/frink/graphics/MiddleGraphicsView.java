package frink.graphics;

import frink.units.Unit;

public abstract class MiddleGraphicsView extends AbstractGraphicsView {
    public FrinkColor getColor() {
        if (this.child != null) {
            return this.child.getColor();
        }
        return BasicFrinkColor.BLACK;
    }

    public void setColor(FrinkColor frinkColor) {
        if (this.child != null) {
            this.child.setColor(frinkColor);
        }
    }

    public FrinkColor getBackgroundColor() {
        if (this.child != null) {
            return this.child.getColor();
        }
        return null;
    }

    public void setBackgroundColor(FrinkColor frinkColor) {
        if (this.child != null) {
            this.child.setBackgroundColor(frinkColor);
        }
    }

    public BoundingBox getRendererBoundingBox() {
        if (this.child != null) {
            return this.child.getRendererBoundingBox();
        }
        return null;
    }

    public Unit getDeviceResolution() {
        if (this.child != null) {
            return this.child.getDeviceResolution();
        }
        System.out.println("MiddleGraphicsView:  getDeviceResolution:  no child!");
        return null;
    }

    public void paintRequested() {
        if (this.parent != null) {
            this.parent.paintRequested();
        }
        callPaintListeners();
    }

    public void printRequested() {
        if (this.parent != null) {
            this.parent.printRequested();
        }
        callPrintListeners();
    }

    public void drawableModified() {
        if (this.child != null) {
            this.child.drawableModified();
        }
    }

    public void rendererResized() {
        if (this.parent != null) {
            this.parent.rendererResized();
        }
    }
}
