package frink.graphics;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

public class SwingPopupMenu extends JPopupMenu implements MouseListener {
    /* access modifiers changed from: private */
    public final GraphicsView gv;
    private Component parent;
    private PaintController pc;
    private JMenuItem printMenuItem;

    public SwingPopupMenu(Component component, GraphicsView graphicsView) {
        super("Graphics");
        this.parent = component;
        this.gv = graphicsView;
        initGUI();
    }

    private void initGUI() {
        this.printMenuItem = new JMenuItem("Print...", 80);
        this.printMenuItem.setAccelerator(KeyStroke.getKeyStroke(80, 128));
        this.printMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingPopupMenu.this.gv.printRequested();
            }
        });
        add(this.printMenuItem);
        this.parent.addMouseListener(this);
    }

    public void mousePressed(MouseEvent mouseEvent) {
        maybeShowPopup(mouseEvent);
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        maybeShowPopup(mouseEvent);
    }

    public void mouseClicked(MouseEvent mouseEvent) {
    }

    public void mouseEntered(MouseEvent mouseEvent) {
    }

    public void mouseExited(MouseEvent mouseEvent) {
    }

    private void maybeShowPopup(MouseEvent mouseEvent) {
        if (mouseEvent.isPopupTrigger()) {
            show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }
    }
}
