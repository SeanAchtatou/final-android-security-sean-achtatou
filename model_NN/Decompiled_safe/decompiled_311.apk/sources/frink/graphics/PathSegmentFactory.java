package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import frink.units.UnitMath;

public class PathSegmentFactory {
    private static final BasicPathSegment CLOSE_INSTANCE = new BasicPathSegment(PathSegment.TYPE_CLOSE, null);

    public static PathSegment create(String str, FrinkPoint[] frinkPointArr, Object obj) {
        if (obj == null) {
            return new BasicPathSegment(str, frinkPointArr);
        }
        if (str.equals(PathSegment.TYPE_ELLIPTICAL_ARC)) {
            return new EllipticalArcPathSegment(frinkPointArr, obj);
        }
        System.err.println("PathSegmentFactory.create: Unhandled type " + str);
        return null;
    }

    public static PathSegment createLine(FrinkPoint frinkPoint) {
        return new BasicPathSegment(PathSegment.TYPE_LINE, new FrinkPoint[]{frinkPoint});
    }

    public static PathSegment createMoveTo(FrinkPoint frinkPoint) {
        return new BasicPathSegment(PathSegment.TYPE_MOVE_TO, new FrinkPoint[]{frinkPoint});
    }

    public static PathSegment createQuadratic(FrinkPoint frinkPoint, FrinkPoint frinkPoint2) {
        return new BasicPathSegment(PathSegment.TYPE_QUADRATIC, new FrinkPoint[]{frinkPoint, frinkPoint2});
    }

    public static PathSegment createCubic(FrinkPoint frinkPoint, FrinkPoint frinkPoint2, FrinkPoint frinkPoint3) {
        return new BasicPathSegment(PathSegment.TYPE_CUBIC, new FrinkPoint[]{frinkPoint, frinkPoint2, frinkPoint3});
    }

    private static PathSegment createEllipse(FrinkPoint frinkPoint, FrinkPoint frinkPoint2) {
        return new BasicPathSegment(PathSegment.TYPE_ELLIPSE, new FrinkPoint[]{frinkPoint, frinkPoint2});
    }

    public static PathSegment createEllipseSides(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        Unit unit5;
        Unit unit6;
        Unit unit7;
        Unit unit8;
        if (UnitMath.compare(unit, unit3) > 0) {
            unit5 = unit;
            unit6 = unit3;
        } else {
            unit5 = unit3;
            unit6 = unit;
        }
        if (UnitMath.compare(unit2, unit4) > 0) {
            unit7 = unit2;
            unit8 = unit4;
        } else {
            unit7 = unit4;
            unit8 = unit2;
        }
        return createEllipse(new FrinkPoint(unit6, unit8), new FrinkPoint(unit5, unit7));
    }

    public static PathSegment createEllipseSize(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        return createEllipseSides(unit, unit2, UnitMath.add(unit, unit3), UnitMath.add(unit2, unit4));
    }

    public static PathSegment createEllipseCenter(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        Unit halve = UnitMath.halve(unit3);
        Unit halve2 = UnitMath.halve(unit4);
        return createEllipseSides(UnitMath.subtract(unit, halve), UnitMath.subtract(unit2, halve2), UnitMath.add(unit, halve), UnitMath.add(unit2, halve2));
    }

    public static PathSegment createEllipticalArc(FrinkPoint frinkPoint, Unit unit, Unit unit2, Unit unit3, Unit unit4, Unit unit5, Environment environment) throws EvaluationException {
        return EllipticalArcPathSegment.construct(frinkPoint, unit, unit2, unit3, unit4, unit5, environment);
    }

    public static PathSegment createCircularArcCenterAngle(FrinkPoint frinkPoint, Unit unit, Unit unit2, Unit unit3, Environment environment) throws ConformanceException, NumericException, OverlapException, EvaluationException {
        Unit sqrt = UnitMath.sqrt(UnitMath.add(UnitMath.square(UnitMath.subtract(unit, frinkPoint.getX())), UnitMath.square(UnitMath.subtract(unit2, frinkPoint.getY()))));
        return createEllipticalArc(frinkPoint, unit, unit2, sqrt, sqrt, unit3, environment);
    }

    public static PathSegment createClose() {
        return CLOSE_INSTANCE;
    }
}
