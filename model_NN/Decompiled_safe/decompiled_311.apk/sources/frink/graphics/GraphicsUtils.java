package frink.graphics;

import frink.expr.BasicListExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.ListExpression;
import frink.expr.NotSupportedException;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkRational;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.NumericException;
import frink.units.DimensionlessUnit;
import frink.units.Unit;
import frink.units.UnitMath;
import java.io.File;

public class GraphicsUtils {
    private static Class biClass = null;
    private static Class bitmapClass = null;
    private static boolean checkedBitmapClass = false;
    private static boolean checkedBufferedImageClass = false;
    private static boolean checkedFrameClass = false;
    private static boolean checkedG2Class = false;
    private static boolean checkedIOClass = false;
    private static boolean checkedImageClass = false;
    private static boolean checkedJFrameClass = false;
    private static boolean checkedPrintableClass = false;
    private static Class frameClass = null;
    private static Class g2Class = null;
    private static Class imageClass = null;
    private static Class imageIOClass = null;
    private static Unit inch = null;
    private static boolean inchChecked;
    private static Class jFrameClass = null;
    private static Class printableClass = null;
    private static Unit radian = null;
    private static boolean radianChecked;

    public static boolean isGraphics2D(Object obj) {
        if (checkedG2Class) {
            return false;
        }
        try {
            g2Class = Class.forName("java.awt.Graphics2D");
            if (g2Class == null || !g2Class.isInstance(obj)) {
                return false;
            }
            return true;
        } catch (ClassNotFoundException e) {
            checkedG2Class = true;
            return false;
        }
    }

    public static boolean isBufferedImage(Object obj) {
        if (checkedBufferedImageClass) {
            return false;
        }
        try {
            biClass = Class.forName("java.awt.image.BufferedImage");
            if (biClass == null || !biClass.isInstance(obj)) {
                return false;
            }
            return true;
        } catch (ClassNotFoundException e) {
            checkedBufferedImageClass = true;
            return false;
        }
    }

    public static Class getBufferedImageClass() {
        if (checkedBufferedImageClass && biClass == null) {
            return null;
        }
        if (!checkedBufferedImageClass || biClass == null) {
            try {
                biClass = Class.forName("java.awt.image.BufferedImage");
            } catch (ClassNotFoundException e) {
                checkedBufferedImageClass = true;
                return null;
            }
        }
        return biClass;
    }

    public static Object constructWritableImage(int i, int i2) throws NotSupportedException {
        if (hasAndroidBitmap()) {
            return constructAndroidBitmap(i, i2);
        }
        return constructBufferedImage(i, i2);
    }

    private static Object constructAndroidBitmap(int i, int i2) throws NotSupportedException {
        try {
            Class cls = bitmapClass;
            Class<?> cls2 = Class.forName("android.graphics.Bitmap$Config");
            Object obj = cls2.getField("ARGB_8888").get(null);
            Class[] clsArr = {Integer.TYPE, Integer.TYPE, cls2};
            return bitmapClass.getMethod("createBitmap", clsArr).invoke(null, new Integer(i), new Integer(i2), obj);
        } catch (Exception e) {
            throw new NotSupportedException("Error when trying to construct Android bitmap:\n  " + e, null);
        }
    }

    private static Object constructBufferedImage(int i, int i2) throws NotSupportedException {
        Class bufferedImageClass = getBufferedImageClass();
        if (bufferedImageClass == null) {
            throw new NotSupportedException("Your Java platform does not support the BufferedImage class.", null);
        }
        try {
            return bufferedImageClass.getConstructor(Integer.TYPE, Integer.TYPE, Integer.TYPE).newInstance(new Integer(i), new Integer(i2), (Integer) bufferedImageClass.getField("TYPE_INT_ARGB").get(null));
        } catch (Exception e) {
            throw new NotSupportedException("Error when constructing buffered image:\n  " + e, null);
        }
    }

    public static synchronized boolean hasImageIO() {
        boolean z;
        synchronized (GraphicsUtils.class) {
            if (!checkedIOClass) {
                try {
                    imageIOClass = Class.forName("javax.imageio.ImageIO");
                    checkedIOClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedIOClass = true;
                    z = false;
                }
            } else if (imageIOClass != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean hasImage() {
        boolean z;
        synchronized (GraphicsUtils.class) {
            if (!checkedImageClass) {
                try {
                    imageClass = Class.forName("java.awt.Image");
                    checkedImageClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedImageClass = true;
                    z = false;
                }
            } else if (imageClass != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean hasAndroidBitmap() {
        boolean z;
        synchronized (GraphicsUtils.class) {
            if (!checkedBitmapClass) {
                try {
                    bitmapClass = Class.forName("android.graphics.Bitmap");
                    checkedBitmapClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedBitmapClass = true;
                    z = false;
                }
            } else if (bitmapClass != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean hasFrame() {
        boolean z;
        synchronized (GraphicsUtils.class) {
            if (!checkedFrameClass) {
                try {
                    frameClass = Class.forName("java.awt.Frame");
                    checkedFrameClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedFrameClass = true;
                    z = false;
                }
            } else if (frameClass != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean hasPrintable() {
        boolean z;
        synchronized (GraphicsUtils.class) {
            if (!checkedPrintableClass) {
                try {
                    printableClass = Class.forName("java.awt.print.Printable");
                    checkedPrintableClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedPrintableClass = true;
                    z = false;
                }
            } else if (printableClass != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean hasJFrame() {
        boolean z;
        synchronized (GraphicsUtils.class) {
            if (!checkedJFrameClass) {
                try {
                    jFrameClass = Class.forName("javax.swing.JFrame");
                    checkedJFrameClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedJFrameClass = true;
                    z = false;
                }
            } else if (jFrameClass != null) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static Unit getInch(Environment environment) {
        if (!inchChecked) {
            fetchInch(environment);
        }
        return inch;
    }

    private static synchronized void fetchInch(Environment environment) {
        synchronized (GraphicsUtils.class) {
            if (!inchChecked) {
                inch = environment.getUnitManager().getUnit("inch");
                if (inch == null) {
                    Unit unit = environment.getUnitManager().getUnit("m");
                    if (unit == null) {
                        environment.outputln("GraphicsUtils.getInch:  Can't get unit 'inch' or 'm'.  Graphics may not work correctly.");
                    } else {
                        try {
                            inch = UnitMath.multiply(FrinkRational.construct(254, 10000), unit);
                        } catch (InvalidDenominatorException | NumericException e) {
                        }
                    }
                }
                inchChecked = true;
            }
        }
    }

    public static Unit getRadian(Environment environment) {
        if (!radianChecked) {
            fetchRadian(environment);
        }
        return radian;
    }

    private static synchronized void fetchRadian(Environment environment) {
        synchronized (GraphicsUtils.class) {
            if (!radianChecked) {
                radian = environment.getUnitManager().getUnit("radian");
                if (radian == null) {
                    environment.outputln("GraphicsUtils.getRadian:  Can't get unit 'radian' or 'm'.  Graphics may not work correctly.");
                }
                radianChecked = true;
            }
        }
    }

    public static Unit getZeroLength(Environment environment) {
        try {
            return UnitMath.multiply(FrinkInt.ZERO, getInch(environment));
        } catch (NumericException e) {
            return null;
        }
    }

    public static Unit getSeventySecondInch(Environment environment) {
        try {
            return UnitMath.divide(getInch(environment), DimensionlessUnit.construct(72));
        } catch (NumericException e) {
            environment.outputln("GraphicsUtils.getSeventySecondInch: problem dividing:\n  " + e);
            return null;
        }
    }

    public static Unit get72PerInch(Environment environment) {
        try {
            return UnitMath.divide(DimensionlessUnit.construct(72), getInch(environment));
        } catch (NumericException e) {
            environment.outputln("GraphicsUtils.get72PerInch: problem dividing:\n  " + e);
            return null;
        }
    }

    public static int packColor(int i, int i2, int i3, int i4) {
        return (i4 << 24) | (i << 16) | (i2 << 8) | i3;
    }

    public static int packColor(int i, int i2, int i3) {
        return -16777216 | (i << 16) | (i2 << 8) | i3;
    }

    public static int packColor(double d, double d2, double d3, double d4) {
        return packColor((int) (d * 255.0d), (int) (d2 * 255.0d), (int) (d3 * 255.0d), (int) (d4 * 255.0d));
    }

    public static double getAlphaDouble(FrinkColor frinkColor) {
        return ((double) frinkColor.getAlpha()) / 255.0d;
    }

    public static ListExpression ARGBToIntListExpression(int i) {
        BasicListExpression basicListExpression = new BasicListExpression(4);
        basicListExpression.appendChild(DimensionlessUnitExpression.construct((i >>> 16) & 255));
        basicListExpression.appendChild(DimensionlessUnitExpression.construct((i >>> 8) & 255));
        basicListExpression.appendChild(DimensionlessUnitExpression.construct(i & 255));
        basicListExpression.appendChild(DimensionlessUnitExpression.construct((i >>> 24) & 255));
        return basicListExpression;
    }

    public static ListExpression ARGBToRationalListExpression(int i) {
        BasicListExpression basicListExpression = new BasicListExpression(4);
        try {
            basicListExpression.appendChild(DimensionlessUnitExpression.construct(FrinkRational.construct((i >>> 16) & 255, 255)));
            basicListExpression.appendChild(DimensionlessUnitExpression.construct(FrinkRational.construct((i >>> 8) & 255, 255)));
            basicListExpression.appendChild(DimensionlessUnitExpression.construct(FrinkRational.construct(i & 255, 255)));
            basicListExpression.appendChild(DimensionlessUnitExpression.construct(FrinkRational.construct((i >>> 24) & 255, 255)));
        } catch (InvalidDenominatorException e) {
        }
        return basicListExpression;
    }

    private static String[] getWriterFormatNames(Environment environment) {
        if (hasImageIO()) {
            try {
                return (String[]) imageIOClass.getMethod("getWriterFormatNames", null).invoke(null, null);
            } catch (Exception e) {
                environment.outputln("GraphicsUtils:getWriterFormatNames:\n  " + e);
            }
        }
        if (!hasAndroidBitmap()) {
            return null;
        }
        return new String[]{"jpeg", "jpg", "png"};
    }

    public static String guessFormat(Object obj, Environment environment) {
        if (obj instanceof File) {
            String name = ((File) obj).getName();
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf == -1) {
                environment.outputln("Warning:  No extension specified on image file '" + name + "'.  Image file will probably not render correctly.");
                printRecognizedFormats(environment);
                return null;
            }
            String substring = name.substring(lastIndexOf + 1);
            String[] writerFormatNames = getWriterFormatNames(environment);
            if (writerFormatNames != null) {
                for (String equalsIgnoreCase : writerFormatNames) {
                    if (equalsIgnoreCase.equalsIgnoreCase(substring)) {
                        return substring;
                    }
                }
            }
            environment.outputln("Warning:  unknown extension '" + substring + "' when writing image file '" + ((File) obj).getName() + "'.  Image file will probably not render correctly.");
            printRecognizedFormats(environment);
            return substring;
        }
        environment.outputln("Error:  No format specified when writing image.  Image file will probably not render correctly.");
        printRecognizedFormats(environment);
        return null;
    }

    private static void printRecognizedFormats(Environment environment) {
        environment.outputln("The file formats supported for writing by your Java platform are: ");
        String[] writerFormatNames = getWriterFormatNames(environment);
        if (writerFormatNames != null) {
            int length = writerFormatNames.length;
            for (int i = 0; i < length; i++) {
                environment.output("\"" + writerFormatNames[i] + "\" ");
            }
        }
        environment.outputln(".");
        environment.outputln("To force writing to a file without a recognized extension, instead use the method:");
        environment.outputln("   g.writeFormat[\"filename\", \"format\" ... ]");
        environment.outputln("      where \"format\" is one of the format strings listed above.");
    }

    public static FrinkImage constructFrinkImage(Object obj, String str, Environment environment) {
        Class<String> cls = String.class;
        if (obj == null) {
            return null;
        }
        if (isBufferedImage(obj)) {
            try {
                obj.getClass();
                Class<?> cls2 = Class.forName("frink.graphics.FrinkBufferedImage");
                if (cls2 != null) {
                    return (FrinkImage) cls2.getConstructor(getBufferedImageClass(), String.class).newInstance(obj, str);
                }
            } catch (Exception e) {
                environment.outputln("Couldn't find frink.graphics.ImageIOImageLoader:\n  " + e);
            }
        }
        if (hasAndroidBitmap()) {
            try {
                obj.getClass();
                Class<?> cls3 = Class.forName("frink.android.FrinkBitmap");
                if (cls3 != null) {
                    return (FrinkImage) cls3.getConstructor(bitmapClass, String.class).newInstance(obj, str);
                }
            } catch (Exception e2) {
                environment.outputln("Couldn't find frink.android.FrinkBitmap:\n  " + e2);
            }
        }
        try {
            obj.getClass();
            Class<?> cls4 = Class.forName("frink.graphics.BasicFrinkImage");
            obj.getClass();
            Class<?> cls5 = Class.forName("java.awt.Image");
            if (cls4 != null) {
                return (FrinkImage) cls4.getConstructor(cls5, String.class).newInstance(obj, str);
            }
        } catch (Exception e3) {
            environment.outputln("Couldn't find frink.graphics.FrinkBitmap:\n  " + e3);
        }
        return null;
    }

    public static File possiblyFixRelativePath(File file) {
        if (hasAndroidBitmap() && !file.isAbsolute()) {
            try {
                file.getClass();
                Class<?> cls = Class.forName("android.os.Environment");
                if (cls != null) {
                    return new File(new File((File) cls.getMethod("getExternalStorageDirectory", null).invoke(null, null), "frink"), file.toString());
                }
            } catch (Exception e) {
                return file;
            }
        }
        return file;
    }
}
