package frink.graphics;

import frink.units.Unit;

public class ImageFileArguments {
    public boolean backgroundTransparent;
    public String format;
    public Unit height;
    public Unit insets;
    public Object outTo;
    public Unit width;

    public ImageFileArguments(Object obj, String str, Unit unit, Unit unit2, boolean z) {
        this(obj, str, unit, unit2, null, z);
    }

    public ImageFileArguments(Object obj, String str, Unit unit, Unit unit2, Unit unit3, boolean z) {
        this.outTo = obj;
        this.format = str;
        this.width = unit;
        this.height = unit2;
        this.insets = unit3;
        this.backgroundTransparent = z;
    }
}
