package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import java.io.File;
import java.io.OutputStream;
import java.util.Hashtable;

public class InternalRendererManager {
    private static Hashtable<String, GraphicsViewConstructor> constructors = null;
    private static boolean initialized = false;

    private static synchronized void initializeConstructors() {
        synchronized (InternalRendererManager.class) {
            if (!initialized) {
                constructors = new Hashtable<>();
                SVGRendererConstructor sVGRendererConstructor = new SVGRendererConstructor();
                constructors.put("svg", sVGRendererConstructor);
                constructors.put("SVG", sVGRendererConstructor);
                initialized = true;
            }
        }
    }

    public static GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
        GraphicsViewConstructor graphicsViewConstructor;
        GraphicsViewConstructor graphicsViewConstructor2;
        if (!initialized) {
            initializeConstructors();
        }
        if (obj instanceof ImageFileArguments) {
            ImageFileArguments imageFileArguments = (ImageFileArguments) obj;
            if (imageFileArguments.outTo instanceof File) {
                File file = (File) imageFileArguments.outTo;
                String str2 = imageFileArguments.format;
                if (str2 == null) {
                    str2 = guessFormat(file);
                }
                if (!(str2 == null || (graphicsViewConstructor2 = constructors.get(str2)) == null)) {
                    return graphicsViewConstructor2.create(environment, str, obj, graphicsViewFactory);
                }
            } else if (!(!(imageFileArguments.outTo instanceof OutputStream) || imageFileArguments.format == null || (graphicsViewConstructor = constructors.get(imageFileArguments.format)) == null)) {
                return graphicsViewConstructor.create(environment, str, obj, graphicsViewFactory);
            }
        }
        return null;
    }

    private static String guessFormat(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf != -1) {
            return name.substring(lastIndexOf + 1);
        }
        System.err.println("Warning:  No extension specified on image file '" + name + "'.  Image file will probably not render correctly.");
        return null;
    }

    private static class SVGRendererConstructor implements GraphicsViewConstructor {
        private SVGRendererConstructor() {
        }

        public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
            return new SVGRenderer(environment, (ImageFileArguments) obj);
        }
    }
}
