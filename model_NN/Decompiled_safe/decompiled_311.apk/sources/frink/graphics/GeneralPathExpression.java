package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.BasicListExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationConformanceException;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.FourArgMethod;
import frink.function.FunctionSource;
import frink.function.SixArgMethod;
import frink.function.ThreeArgMethod;
import frink.function.TwoArgMethod;
import frink.function.ZeroArgMethod;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import frink.units.Unit;

public class GeneralPathExpression implements Drawable, GraphicsExpression, FrinkObject, DeferredDrawable {
    public static final String TYPE = "GeneralPath";
    private static final GeneralPathFunctionSource methods = new GeneralPathFunctionSource();
    private EmptyObjectContextFrame contextFrame = null;
    private boolean filled;
    private FrinkGeneralPath path = new FrinkGeneralPath();

    public GeneralPathExpression(boolean z) {
        this.filled = z;
    }

    public void close() throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createClose());
    }

    public void lineTo(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createLine(new FrinkPoint(unit, unit2)));
    }

    public void moveTo(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createMoveTo(new FrinkPoint(unit, unit2)));
    }

    public void quadratic(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createQuadratic(new FrinkPoint(unit, unit2), new FrinkPoint(unit3, unit4)));
    }

    public void addEllipseSides(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createEllipseSides(unit, unit2, unit3, unit4));
    }

    public void addEllipseSize(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createEllipseSize(unit, unit2, unit3, unit4));
    }

    public void addEllipseCenter(Unit unit, Unit unit2, Unit unit3, Unit unit4) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createEllipseCenter(unit, unit2, unit3, unit4));
    }

    public void addCubicSegment(Unit unit, Unit unit2, Unit unit3, Unit unit4, Unit unit5, Unit unit6) throws ConformanceException, NumericException, OverlapException {
        this.path.addSegment(PathSegmentFactory.createCubic(new FrinkPoint(unit, unit2), new FrinkPoint(unit3, unit4), new FrinkPoint(unit5, unit6)));
    }

    public void addCircularArc(Unit unit, Unit unit2, Unit unit3, Environment environment) throws ConformanceException, NumericException, OverlapException, EvaluationException {
        this.path.addSegment(PathSegmentFactory.createCircularArcCenterAngle(getLastPoint(), unit, unit2, unit3, environment));
    }

    public int getChildCount() {
        return this.path.getSegmentCount();
    }

    public FrinkPoint getLastPoint() {
        return this.path.getLastPoint();
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i >= this.path.getSegmentCount()) {
            throw new InvalidChildException("Invalid child " + i + " requested for GeneralPathExpression.", this);
        }
        int segmentCount = this.path.getSegmentCount();
        BasicListExpression basicListExpression = new BasicListExpression(segmentCount);
        for (int i2 = 0; i2 < segmentCount; i2++) {
            PathSegment segment = this.path.getSegment(i);
            int pointCount = segment.getPointCount();
            for (int i3 = 0; i3 < pointCount; i3++) {
                FrinkPoint point = segment.getPoint(i3);
                basicListExpression.appendChild(BasicUnitExpression.construct(point.getX()));
                basicListExpression.appendChild(BasicUnitExpression.construct(point.getY()));
            }
        }
        return basicListExpression;
    }

    public void setStrokeWidth(Unit unit) {
        this.path.setStrokeWidth(unit, this.filled);
    }

    public BoundingBox getBoundingBox() {
        return this.path.getBoundingBox();
    }

    public Drawable getDrawable() {
        return this;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression == this) {
            return true;
        }
        if (expression.getExpressionType() == getExpressionType()) {
            try {
                int segmentCount = this.path.getSegmentCount();
                for (int i = 0; i < segmentCount; i++) {
                    if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                        return false;
                    }
                }
                return true;
            } catch (InvalidChildException e) {
            }
        }
        return false;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return methods;
    }

    public boolean isA(String str) {
        return str.equals(TYPE);
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.drawGeneralPath(this.path, this.filled);
    }

    public String getExpressionType() {
        return TYPE;
    }

    private static class GeneralPathFunctionSource extends BasicFunctionSource {
        GeneralPathFunctionSource() {
            super("GeneralPathExpression");
            AnonymousClass1 r0 = new TwoArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2) throws EvaluationException {
                    try {
                        generalPathExpression.lineTo(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            };
            addFunctionDefinition("addPoint", r0);
            addFunctionDefinition("lineTo", r0);
            addFunctionDefinition(PathSegment.TYPE_MOVE_TO, new TwoArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2) throws EvaluationException {
                    try {
                        generalPathExpression.moveTo(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition(PathSegment.TYPE_QUADRATIC, new FourArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        generalPathExpression.quadratic(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("cubicCurve", new SixArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4, Expression expression5, Expression expression6) throws EvaluationException {
                    try {
                        generalPathExpression.addCubicSegment(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4), BuiltinFunctionSource.getUnitValue(expression5), BuiltinFunctionSource.getUnitValue(expression6));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition(PathSegment.TYPE_CLOSE, new ZeroArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression) throws EvaluationException {
                    try {
                        generalPathExpression.close();
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("ellipseSides", new FourArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        generalPathExpression.addEllipseSides(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("ellipseSize", new FourArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        generalPathExpression.addEllipseSize(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("ellipseCenter", new FourArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws EvaluationException {
                    try {
                        generalPathExpression.addEllipseCenter(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), BuiltinFunctionSource.getUnitValue(expression4));
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
            addFunctionDefinition("circularArc", new ThreeArgMethod<GeneralPathExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, GeneralPathExpression generalPathExpression, Expression expression, Expression expression2, Expression expression3) throws EvaluationException {
                    try {
                        generalPathExpression.addCircularArc(BuiltinFunctionSource.getUnitValue(expression), BuiltinFunctionSource.getUnitValue(expression2), BuiltinFunctionSource.getUnitValue(expression3), environment);
                        return VoidExpression.VOID;
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException(e.toString(), this);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException(e2.toString(), this);
                    } catch (OverlapException e3) {
                        throw new InvalidArgumentException(e3.toString(), this);
                    }
                }
            });
        }
    }
}
