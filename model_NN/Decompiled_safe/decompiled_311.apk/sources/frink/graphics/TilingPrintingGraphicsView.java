package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.DimensionlessUnit;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public class TilingPrintingGraphicsView extends AbstractPrintingGraphicsView implements Printable {
    private int hPages;
    private double usedPercent = 0.949999988079071d;
    private int vPages;

    public TilingPrintingGraphicsView(Environment environment, int i, int i2) {
        super(environment);
        this.hPages = i;
        this.vPages = i2;
    }

    public int print(Graphics graphics, PageFormat pageFormat, int i) throws PrinterException {
        if (i >= this.hPages * this.vPages) {
            return 1;
        }
        this.boundingBoxNeedsRecalculation = true;
        setGraphics(graphics);
        this.pf = pageFormat;
        int i2 = i / this.hPages;
        int i3 = i % this.hPages;
        FrinkColor backgroundColor = getBackgroundColor();
        if (backgroundColor != null) {
            Color color = graphics.getColor();
            graphics.setColor(new Color(backgroundColor.getPacked(), true));
            graphics.fillRect(0, 0, (int) pageFormat.getWidth(), (int) pageFormat.getHeight());
            graphics.setColor(color);
        }
        graphics.translate((int) ((-pageFormat.getImageableWidth()) * ((double) i3) * this.usedPercent), (int) ((-pageFormat.getImageableHeight()) * ((double) i2) * this.usedPercent));
        paintRequested();
        return 0;
    }

    /* access modifiers changed from: protected */
    public synchronized void recalculateBoundingBox() {
        if (this.boundingBoxNeedsRecalculation) {
            if (this.pf == null) {
                System.out.println("TilingPrintingGraphicsView.recalculateBoundingBox: no PageFormat object.");
            } else {
                try {
                    double imageableWidth = this.pf.getImageableWidth();
                    double imageableHeight = this.pf.getImageableHeight();
                    this.bbox = new BoundingBox(DimensionlessUnit.construct(this.pf.getImageableX()), DimensionlessUnit.construct(this.pf.getImageableY()), DimensionlessUnit.construct(imageableWidth + (((double) (this.hPages - 1)) * imageableWidth * this.usedPercent)), DimensionlessUnit.construct((((double) (this.vPages - 1)) * imageableHeight * this.usedPercent) + imageableHeight));
                } catch (ConformanceException e) {
                    this.bbox = null;
                } catch (NumericException e2) {
                    this.bbox = null;
                } catch (OverlapException e3) {
                    this.bbox = null;
                }
                this.boundingBoxNeedsRecalculation = false;
            }
        }
    }
}
