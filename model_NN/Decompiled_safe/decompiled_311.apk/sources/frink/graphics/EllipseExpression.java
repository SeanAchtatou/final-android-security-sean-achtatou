package frink.graphics;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;

public class EllipseExpression extends ShapeExpression {
    public static final String TYPE = "EllipseExpression";

    public EllipseExpression(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z, Unit unit5, Environment environment) throws ConformanceException, NumericException, OverlapException {
        super(unit, unit2, unit3, unit4, z, unit5);
    }

    public void draw(GraphicsView graphicsView) {
        graphicsView.drawEllipse(this.shapeBox.getLeft(), this.shapeBox.getTop(), this.shapeBox.getWidth(), this.shapeBox.getHeight(), isFilled());
    }

    public String getExpressionType() {
        return TYPE;
    }
}
