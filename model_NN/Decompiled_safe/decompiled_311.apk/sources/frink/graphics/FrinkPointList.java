package frink.graphics;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import frink.units.UnitMath;
import java.util.Vector;

public class FrinkPointList {
    private boolean boundingBoxNeedsRecalculation = false;
    private BoundingBox boxWithStroke = null;
    private boolean filled = false;
    private Vector<FrinkPoint> points = new Vector<>();
    private int recalculateFrom = 0;
    private BoundingBox shapeBox = null;
    private Unit strokeWidth = null;

    public void addPoint(Unit unit, Unit unit2) throws ConformanceException, NumericException, OverlapException {
        this.boundingBoxNeedsRecalculation = true;
        this.points.addElement(new FrinkPoint(unit, unit2));
    }

    public int getPointCount() {
        return this.points.size();
    }

    public FrinkPoint getPoint(int i) {
        return this.points.elementAt(i);
    }

    public void setStrokeWidth(Unit unit, boolean z) {
        this.strokeWidth = unit;
        this.filled = z;
        this.boundingBoxNeedsRecalculation = true;
    }

    public BoundingBox getBoundingBox() {
        if (this.boundingBoxNeedsRecalculation) {
            recalculateBoundingBox();
        }
        if (this.boxWithStroke != null) {
            return this.boxWithStroke;
        }
        return this.shapeBox;
    }

    private synchronized void recalculateBoundingBox() {
        Unit right;
        Unit top;
        Unit unit;
        Unit bottom;
        Unit unit2;
        Unit unit3;
        if (this.boundingBoxNeedsRecalculation) {
            int size = this.points.size();
            if (size != 0) {
                if (this.recalculateFrom == 0) {
                    FrinkPoint elementAt = this.points.elementAt(0);
                    right = elementAt.getX();
                    bottom = elementAt.getY();
                    this.recalculateFrom = 1;
                    top = bottom;
                    unit = right;
                } else {
                    Unit left = this.shapeBox.getLeft();
                    right = this.shapeBox.getRight();
                    top = this.shapeBox.getTop();
                    unit = left;
                    bottom = this.shapeBox.getBottom();
                }
                try {
                    int i = this.recalculateFrom;
                    Unit unit4 = bottom;
                    Unit unit5 = right;
                    Unit unit6 = unit;
                    Unit unit7 = unit5;
                    while (i < size) {
                        FrinkPoint elementAt2 = this.points.elementAt(i);
                        Unit x = elementAt2.getX();
                        Unit y = elementAt2.getY();
                        if (UnitMath.compare(x, unit6) < 0) {
                            unit3 = unit7;
                            unit2 = x;
                        } else if (UnitMath.compare(x, unit7) > 0) {
                            unit2 = unit6;
                            unit3 = x;
                        } else {
                            Unit unit8 = unit7;
                            unit2 = unit6;
                            unit3 = unit8;
                        }
                        if (UnitMath.compare(y, top) < 0) {
                            top = y;
                            y = unit4;
                        } else if (UnitMath.compare(y, unit4) <= 0) {
                            y = unit4;
                        }
                        i++;
                        unit4 = y;
                        Unit unit9 = unit3;
                        unit6 = unit2;
                        unit7 = unit9;
                    }
                    this.recalculateFrom = size;
                    this.shapeBox = new BoundingBox(unit6, top, unit7, unit4);
                    if (!this.filled && this.strokeWidth != null) {
                        this.boxWithStroke = new BoundingBox(unit6, top, unit7, unit4, this.strokeWidth, this.filled);
                    }
                } catch (ConformanceException e) {
                    System.err.println("FrinkPointList: Error in calculating bounding box:\n " + e);
                } catch (NumericException e2) {
                    System.err.println("FrinkPointList: Error in calculating bounding box:\n " + e2);
                } catch (OverlapException e3) {
                    System.err.println("FrinkPointList: Error in calculating bounding box:\n " + e3);
                }
                this.boundingBoxNeedsRecalculation = false;
            }
        }
    }
}
