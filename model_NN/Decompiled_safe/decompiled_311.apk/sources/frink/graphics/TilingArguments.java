package frink.graphics;

import frink.units.DimensionlessUnit;
import frink.units.Unit;

public class TilingArguments extends PrintingArguments {
    public int pagesHigh;
    public int pagesWide;

    public TilingArguments(int i, int i2) {
        this(i, i2, DimensionlessUnit.ONE);
    }

    public TilingArguments(int i, int i2, Unit unit) {
        super(unit);
        this.pagesWide = i;
        this.pagesHigh = i2;
    }
}
