package frink.graphics;

public interface PrintRequestListener {
    void printRequested();
}
