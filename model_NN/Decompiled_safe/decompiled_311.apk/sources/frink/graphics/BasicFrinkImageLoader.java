package frink.graphics;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import java.io.IOException;
import java.net.URL;

public class BasicFrinkImageLoader implements FrinkImageLoader {
    private static final boolean DEBUG = false;
    public static final BasicFrinkImageLoader INSTANCE = new BasicFrinkImageLoader();
    private ImageLoader delegate = null;

    private BasicFrinkImageLoader() {
    }

    public FrinkImage getFrinkImage(URL url, Environment environment) throws IOException, FrinkSecurityException {
        environment.getSecurityHelper().checkRead(url);
        return GraphicsUtils.constructFrinkImage(getDelegate(environment).getImage(url, environment), url.toString(), environment);
    }

    private synchronized ImageLoader getDelegate(Environment environment) {
        ImageLoader imageLoader;
        if (this.delegate != null) {
            imageLoader = this.delegate;
        } else {
            if (GraphicsUtils.hasImageIO()) {
                try {
                    getClass();
                    Class<?> cls = Class.forName("frink.graphics.ImageIOLoaderDelegate");
                    if (cls != null) {
                        this.delegate = (ImageLoader) cls.getConstructor(new Class[0]).newInstance(new Object[0]);
                        imageLoader = this.delegate;
                    }
                } catch (Exception e) {
                    environment.outputln("Couldn't find frink.graphics.ImageIOLoaderDelegate:\n  " + e);
                }
            }
            if (GraphicsUtils.hasImage()) {
                try {
                    getClass();
                    Class<?> cls2 = Class.forName("frink.graphics.BasicImageLoaderDelegate");
                    if (cls2 != null) {
                        this.delegate = (ImageLoader) cls2.getConstructor(new Class[0]).newInstance(new Object[0]);
                        imageLoader = this.delegate;
                    }
                } catch (Exception e2) {
                    environment.outputln("Couldn't find frink.graphics.BasicImageLoaderDelegate:\n  " + e2);
                }
            }
            if (GraphicsUtils.hasAndroidBitmap()) {
                try {
                    getClass();
                    Class<?> cls3 = Class.forName("frink.android.AndroidImageLoader");
                    if (cls3 != null) {
                        this.delegate = (ImageLoader) cls3.getConstructor(new Class[0]).newInstance(new Object[0]);
                        imageLoader = this.delegate;
                    }
                } catch (Exception e3) {
                    environment.outputln("Couldn't find frink.android.AndroidImageLoader:\n  " + e3);
                }
            }
            imageLoader = null;
        }
        return imageLoader;
    }
}
