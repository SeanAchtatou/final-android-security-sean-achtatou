package frink.android;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.graphics.BasicFrinkColor;
import frink.graphics.BoundingBox;
import frink.graphics.DrawableModifiedListener;
import frink.graphics.GraphicsUtils;
import frink.graphics.GraphicsView;
import frink.numeric.FrinkFloat;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import frink.units.UnitMath;

public class AndroidGraphicsView extends View implements DrawableModifiedListener {
    public static final int MESSAGE_DRAWABLE_MODIFIED = 100;
    private BoundingBox bbox;
    private Handler handler;
    private int lastHeight = -1;
    private int lastWidth = -1;
    private AndroidGraphicsRenderer renderer;

    public AndroidGraphicsView(Context context, Environment environment) {
        super(context);
        initHandler();
        this.renderer = new AndroidGraphicsRenderer(context, environment, calcResolution(context, environment), this);
    }

    private synchronized void initHandler() {
        if (this.handler == null) {
            this.handler = new Handler() {
                public void handleMessage(Message message) {
                    switch (message.what) {
                        case 100:
                            AndroidGraphicsView.this.doDrawableModified();
                            return;
                        default:
                            return;
                    }
                }
            };
        }
    }

    public GraphicsView getGraphicsView() {
        return this.renderer;
    }

    public BoundingBox getRendererBoundingBox() {
        recalculateBoundingBox(getWidth(), getHeight());
        return this.bbox;
    }

    private synchronized void recalculateBoundingBox(int i, int i2) {
        try {
            if (!(this.bbox != null && i == this.lastWidth && i2 == this.lastHeight)) {
                this.lastWidth = i;
                this.lastHeight = i2;
                this.bbox = new BoundingBox(0, 0, i, i2);
                this.renderer.setBoundingBox(this.bbox);
                this.renderer.rendererResized();
            }
        } catch (ConformanceException e) {
            this.bbox = null;
        } catch (NumericException e2) {
            this.bbox = null;
        } catch (OverlapException e3) {
            this.bbox = null;
        }
        return;
    }

    public void drawableModified() {
        this.handler.sendMessage(Message.obtain(this.handler, 100));
    }

    /* access modifiers changed from: private */
    public void doDrawableModified() {
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.renderer.setCanvas(canvas);
        recalculateBoundingBox(getWidth(), getHeight());
        canvas.drawColor(BasicFrinkColor.WHITE.getPacked());
        this.renderer.paintRequested();
    }

    private Unit calcResolution(Context context, Environment environment) {
        Unit inch = GraphicsUtils.getInch(environment);
        if (inch != null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            try {
                return UnitMath.divide(new FrinkFloat(((double) (displayMetrics.ydpi + displayMetrics.xdpi)) / 2.0d), inch);
            } catch (NumericException e) {
                environment.outputln("AndroidGraphicsView:  weird NumericException\n   " + e);
            }
        }
        return null;
    }
}
