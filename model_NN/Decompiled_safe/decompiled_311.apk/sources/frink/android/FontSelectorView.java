package frink.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class FontSelectorView extends Spinner {
    private static final int MAX_SIZE = 30;
    private static final int MIN_SIZE = 1;
    private int size = 5;

    public FontSelectorView(Context context) {
        super(context);
        initGUI(context);
    }

    private void initGUI(Context context) {
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        ArrayAdapter arrayAdapter = new ArrayAdapter(context, 17367048);
        for (int i = 1; i <= 30; i++) {
            arrayAdapter.add(Integer.toString(i));
        }
        setAdapter((SpinnerAdapter) arrayAdapter);
    }

    private void setFontSize(int i) {
        int i2 = 30;
        if (i <= 30) {
            i2 = i;
        }
        if (i2 < 1) {
            i2 = 1;
        }
        this.size = i2;
        setSelection(i2 - 1);
    }

    public static void showInAlert(int i, final FontChangedListener fontChangedListener, Context context) {
        final FontSelectorView fontSelectorView = new FontSelectorView(context);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Font size");
        fontSelectorView.setFontSize(i);
        builder.setView(fontSelectorView);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                fontChangedListener.fontSizeChanged(fontSelectorView.getSelectedItemPosition() + 1);
            }
        });
        builder.show();
    }
}
