package frink.android;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.UndefExpression;
import java.util.Vector;

public class AndroidServiceManager implements LocationServiceListener {
    private LocationService GPSService = null;
    private Activity activity;
    private boolean isShutdown;
    private Vector<AndroidService> loadedServices;
    private Vector<SensorService> sensorServices;
    private SpeechService speaker = null;

    public AndroidServiceManager(Activity activity2) {
        this.activity = activity2;
        this.loadedServices = new Vector<>();
        this.sensorServices = new Vector<>();
        this.isShutdown = false;
    }

    private void addService(AndroidService service) {
        if (this.isShutdown) {
            service.shutdown();
            return;
        }
        synchronized (this.loadedServices) {
            this.loadedServices.addElement(service);
        }
    }

    public SpeechService getSpeechService() {
        if (this.speaker == null) {
            startTTS();
        }
        return this.speaker;
    }

    public LocationService getGPSService() {
        if (this.GPSService == null) {
            startGPS();
        }
        return this.GPSService;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        if (frink.android.AndroidUtils.hasSensor() == false) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        getClass();
        r1 = java.lang.Class.forName("frink.android.BasicSensorService");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        if (r1 == null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        r7 = (frink.android.SensorService) r1.getConstructor(java.lang.Integer.TYPE, android.hardware.SensorManager.class).newInstance(new java.lang.Integer(r12), (android.hardware.SensorManager) r11.activity.getSystemService("sensor"));
        addService(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0063, code lost:
        r8 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0065, code lost:
        r8 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized frink.android.SensorService getSensorService(int r12) throws java.lang.Exception {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.Vector<frink.android.SensorService> r8 = r11.sensorServices     // Catch:{ all -> 0x0067 }
            int r6 = r8.size()     // Catch:{ all -> 0x0067 }
            r4 = 0
        L_0x0008:
            if (r4 >= r6) goto L_0x001e
            java.util.Vector<frink.android.SensorService> r8 = r11.sensorServices     // Catch:{ all -> 0x0067 }
            java.lang.Object r5 = r8.elementAt(r4)     // Catch:{ all -> 0x0067 }
            frink.android.SensorService r5 = (frink.android.SensorService) r5     // Catch:{ all -> 0x0067 }
            int r8 = r5.getType()     // Catch:{ all -> 0x0067 }
            if (r12 != r8) goto L_0x001b
            r8 = r5
        L_0x0019:
            monitor-exit(r11)
            return r8
        L_0x001b:
            int r4 = r4 + 1
            goto L_0x0008
        L_0x001e:
            boolean r8 = frink.android.AndroidUtils.hasSensor()     // Catch:{ all -> 0x0067 }
            if (r8 == 0) goto L_0x0065
            r11.getClass()     // Catch:{ all -> 0x0067 }
            java.lang.String r8 = "frink.android.BasicSensorService"
            java.lang.Class r1 = java.lang.Class.forName(r8)     // Catch:{ all -> 0x0067 }
            if (r1 == 0) goto L_0x0065
            r8 = 2
            java.lang.Class[] r0 = new java.lang.Class[r8]     // Catch:{ all -> 0x0067 }
            r8 = 0
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0067 }
            r0[r8] = r9     // Catch:{ all -> 0x0067 }
            r8 = 1
            java.lang.Class<android.hardware.SensorManager> r9 = android.hardware.SensorManager.class
            r0[r8] = r9     // Catch:{ all -> 0x0067 }
            java.lang.reflect.Constructor r3 = r1.getConstructor(r0)     // Catch:{ all -> 0x0067 }
            r8 = 2
            java.lang.Object[] r2 = new java.lang.Object[r8]     // Catch:{ all -> 0x0067 }
            r8 = 0
            java.lang.Integer r9 = new java.lang.Integer     // Catch:{ all -> 0x0067 }
            r9.<init>(r12)     // Catch:{ all -> 0x0067 }
            r2[r8] = r9     // Catch:{ all -> 0x0067 }
            r9 = 1
            android.app.Activity r8 = r11.activity     // Catch:{ all -> 0x0067 }
            java.lang.String r10 = "sensor"
            java.lang.Object r8 = r8.getSystemService(r10)     // Catch:{ all -> 0x0067 }
            android.hardware.SensorManager r8 = (android.hardware.SensorManager) r8     // Catch:{ all -> 0x0067 }
            android.hardware.SensorManager r8 = (android.hardware.SensorManager) r8     // Catch:{ all -> 0x0067 }
            r2[r9] = r8     // Catch:{ all -> 0x0067 }
            java.lang.Object r7 = r3.newInstance(r2)     // Catch:{ all -> 0x0067 }
            frink.android.SensorService r7 = (frink.android.SensorService) r7     // Catch:{ all -> 0x0067 }
            r11.addService(r7)     // Catch:{ all -> 0x0067 }
            r8 = r7
            goto L_0x0019
        L_0x0065:
            r8 = 0
            goto L_0x0019
        L_0x0067:
            r8 = move-exception
            monitor-exit(r11)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.android.AndroidServiceManager.getSensorService(int):frink.android.SensorService");
    }

    public synchronized void startTTS() {
        if (this.speaker == null) {
            if (AndroidUtils.hasTTS()) {
                Log.d("Frink Speech", "Has TextToSpeech class!");
                try {
                    getClass();
                    Class c = Class.forName("frink.android.TextToSpeechService");
                    if (c != null) {
                        this.speaker = (SpeechService) c.getConstructor(Context.class).newInstance(this.activity);
                        addService(this.speaker);
                    }
                } catch (Exception e) {
                    Log.w("Frink Speech", e.toString());
                }
            }
        }
        return;
    }

    public synchronized void startGPS() {
        if (this.GPSService == null) {
            LocationService.construct((LocationManager) this.activity.getSystemService("location"), "gps", this);
        }
    }

    public synchronized void stopGPS() {
        if (this.GPSService != null) {
            ((LocationManager) this.activity.getSystemService("location")).removeUpdates(this.GPSService);
        }
    }

    public synchronized Location getGPSLocationJava() {
        Location location;
        if (this.GPSService != null) {
            location = this.GPSService.getLocationJava();
        } else {
            location = null;
        }
        return location;
    }

    public void locationServiceEnabled(LocationService ls, String provider) {
        if (provider.equals("gps")) {
            this.GPSService = ls;
            addService(ls);
        }
    }

    public Expression doUI(final Environment env, final Expression arg) {
        final ExpressionHolder holder = new ExpressionHolder();
        Runnable r = new Runnable() {
            public void run() {
                synchronized (holder) {
                    try {
                        holder.value = arg.evaluate(env);
                        holder.notifyAll();
                    } catch (EvaluationException e) {
                        env.outputln("Error in doUI:\n" + e);
                        holder.value = UndefExpression.UNDEF;
                        holder.notifyAll();
                    } catch (Throwable th) {
                        holder.notifyAll();
                        throw th;
                    }
                }
            }
        };
        synchronized (holder) {
            try {
                this.activity.runOnUiThread(r);
                holder.wait();
            } catch (InterruptedException e) {
                env.outputln("Interrupted when waiting for input:\n" + e);
            }
        }
        return holder.value;
    }

    public synchronized void shutdown() {
        if (!this.isShutdown) {
            if (this.loadedServices != null) {
                synchronized (this.loadedServices) {
                    int size = this.loadedServices.size();
                    for (int i = 0; i < size; i++) {
                        this.loadedServices.elementAt(i).shutdown();
                    }
                }
                this.isShutdown = true;
                this.loadedServices = null;
                this.speaker = null;
                this.GPSService = null;
            }
        }
    }
}
