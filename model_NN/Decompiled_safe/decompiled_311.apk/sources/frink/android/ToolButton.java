package frink.android;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ToolButton extends Button {
    /* access modifiers changed from: private */
    public String after;
    /* access modifiers changed from: private */
    public String afterSelected;
    /* access modifiers changed from: private */
    public String before;
    /* access modifiers changed from: private */
    public String beforeSelected;
    /* access modifiers changed from: private */
    public ActiveTextProvider fieldProv;
    /* access modifiers changed from: private */
    public int selectedOffset;
    /* access modifiers changed from: private */
    public int unselectedOffset;

    public ToolButton(Context context, String label, String before2, String after2, int unselectedOffset2, ActiveTextProvider fieldProv2) {
        this(context, label, before2, after2, unselectedOffset2, fieldProv2, 0);
    }

    public ToolButton(Context context, String label, String before2, String after2, int unselectedOffset2, ActiveTextProvider fieldProv2, int offset) {
        super(context);
        setText(label);
        this.before = before2;
        this.after = after2;
        this.unselectedOffset = unselectedOffset2;
        this.beforeSelected = null;
        this.afterSelected = null;
        this.fieldProv = fieldProv2;
        this.selectedOffset = this.selectedOffset;
        init();
    }

    public ToolButton(Context context, String label, String before2, String after2, int unselectedOffset2, String beforeSelected2, String afterSelected2, int selectedOffset2, ActiveTextProvider fieldProv2) {
        super(context);
        setText(label);
        this.before = before2;
        this.after = after2;
        this.unselectedOffset = unselectedOffset2;
        this.beforeSelected = beforeSelected2;
        this.afterSelected = afterSelected2;
        this.fieldProv = fieldProv2;
        this.selectedOffset = selectedOffset2;
        init();
    }

    private void init() {
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText tf = ToolButton.this.fieldProv.getActiveField();
                String text = tf.getText().toString();
                int start = tf.getSelectionStart();
                int end = tf.getSelectionEnd();
                if (start > end) {
                    int temp = start;
                    start = end;
                    end = temp;
                }
                if (start - end == 0 || ToolButton.this.beforeSelected == null) {
                    tf.setText(text.substring(0, start) + ToolButton.this.before + text.substring(start, end) + ToolButton.this.after + text.substring(end));
                    tf.setSelection(ToolButton.this.before.length() + end + ToolButton.this.after.length() + ToolButton.this.unselectedOffset);
                } else {
                    tf.setText(text.substring(0, start) + ToolButton.this.beforeSelected + text.substring(start, end) + ToolButton.this.afterSelected + text.substring(end));
                    int newPos = start + ToolButton.this.beforeSelected.length();
                    if (ToolButton.this.selectedOffset != 0) {
                        newPos = ToolButton.this.beforeSelected.length() + end + ToolButton.this.afterSelected.length() + ToolButton.this.selectedOffset;
                    }
                    tf.setSelection(newPos);
                }
                tf.requestFocus();
            }
        });
    }
}
