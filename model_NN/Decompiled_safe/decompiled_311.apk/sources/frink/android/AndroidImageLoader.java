package frink.android;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.graphics.ImageLoader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class AndroidImageLoader implements ImageLoader<Bitmap> {
    public Bitmap getImage(URL url, Environment environment) throws IOException, FrinkSecurityException {
        InputStream inputStream;
        Throwable th;
        try {
            InputStream openStream = url.openStream();
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(openStream);
                if (openStream != null) {
                    openStream.close();
                }
                return decodeStream;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                inputStream = openStream;
                th = th3;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            inputStream = null;
            th = th5;
        }
        if (inputStream != null) {
            inputStream.close();
        }
        throw th;
    }
}
