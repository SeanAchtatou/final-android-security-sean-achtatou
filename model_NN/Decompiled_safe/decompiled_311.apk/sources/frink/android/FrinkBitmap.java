package frink.android;

import android.graphics.Bitmap;
import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.expr.NotSupportedException;
import frink.graphics.FrinkImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FrinkBitmap implements FrinkImage<Bitmap> {
    private Bitmap bImage;
    private String location;

    public FrinkBitmap(Bitmap bitmap, String str) {
        this.bImage = bitmap;
        this.location = str;
    }

    public int getWidth() {
        return this.bImage.getWidth();
    }

    public int getHeight() {
        return this.bImage.getHeight();
    }

    public Bitmap getImage() {
        return this.bImage;
    }

    public int getPixelPacked(int i, int i2) {
        return this.bImage.getPixel(i, i2);
    }

    public void setPixelPacked(int i, int i2, int i3) {
        this.bImage.setPixel(i, i2, i3);
    }

    public void makeARGB() {
        this.bImage = changeColorModel(this.bImage, Bitmap.Config.ARGB_8888);
    }

    private static Bitmap changeColorModel(Bitmap bitmap, Bitmap.Config config) {
        if (bitmap.getConfig() == config && bitmap.isMutable()) {
            return bitmap;
        }
        Bitmap copy = bitmap.copy(config, true);
        if (copy == null) {
            return bitmap;
        }
        return copy;
    }

    public void write(File file, String str, Environment environment) throws IOException, NotSupportedException, FrinkSecurityException {
        doWrite(file, this.bImage, str, environment);
    }

    private void doWrite(File file, Bitmap bitmap, String str, Environment environment) throws IOException, NotSupportedException, FrinkSecurityException {
        environment.getSecurityHelper().checkWrite(file);
        if (str != null) {
            Bitmap.CompressFormat valueOf = Bitmap.CompressFormat.valueOf(str);
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    boolean compress = bitmap.compress(valueOf, 90, fileOutputStream2);
                    if (fileOutputStream2 != null) {
                        fileOutputStream2.close();
                    }
                    if (!compress) {
                        environment.outputln("FrinkBitmap.write:  Could not find appropriate writer for format '" + str + "'");
                    }
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                }
            } catch (Throwable th2) {
                th = th2;
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                throw th;
            }
        } else {
            environment.outputln("FrinkBitmap.write:  No format specified for writing '" + file + "'.  File was not written.");
        }
    }

    public String getSource() {
        return this.location;
    }
}
