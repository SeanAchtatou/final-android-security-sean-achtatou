package frink.android;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

public class LocationService implements AndroidService, LocationListener {
    private Location lastLocation;
    private LocationManager lm;
    private String provider;

    private LocationService(LocationManager locationManager, String str) {
        this.lastLocation = null;
        this.lm = locationManager;
        this.provider = str;
        this.lastLocation = locationManager.getLastKnownLocation(str);
        locationManager.requestLocationUpdates(str, 60000, 1.0f, this);
    }

    public static void construct(final LocationManager locationManager, final String str, final LocationServiceListener locationServiceListener) {
        new Thread(new Runnable() {
            public void run() {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }
                locationServiceListener.locationServiceEnabled(new LocationService(locationManager, str), str);
                Looper.loop();
                Looper.myLooper().quit();
            }
        }, "LocationService").start();
    }

    public Location getLocationJava() {
        return this.lastLocation;
    }

    public void onLocationChanged(Location location) {
        this.lastLocation = location;
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public void shutdown() {
        this.lm.removeUpdates(this);
    }
}
