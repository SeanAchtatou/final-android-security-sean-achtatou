package frink.android;

public final class R {

    public static final class array {
        public static final int fontSize = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int input = 2131099650;
        public static final int output = 2131099649;
        public static final int scroll = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int programmingpreferences = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }
}
