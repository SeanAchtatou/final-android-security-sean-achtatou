package frink.android;

public interface SpeechService extends AndroidService {
    void speak(String str);
}
