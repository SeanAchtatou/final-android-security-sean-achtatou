package frink.android;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

public class FileChooser extends ListActivity {
    public static final String FILE_KEY = "FILE_KEY";
    public static final int LOAD_FILE = 1001;
    public static final int SAVE_FILE = 1002;
    private static final Comparator<File> fileComparator = new Comparator<File>() {
        public int compare(File a, File b) {
            return a.getName().compareTo(b.getName());
        }
    };
    private File currentDirectory = null;
    private File[] list;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        initDirectory();
        populateList();
        setSelection(0);
    }

    private void initDirectory() {
        File extdir = Environment.getExternalStorageDirectory();
        this.currentDirectory = new File(extdir, "frink");
        if (!this.currentDirectory.exists()) {
            this.currentDirectory = extdir;
            if (!this.currentDirectory.exists()) {
                this.currentDirectory = new File("/");
            }
        }
    }

    private void populateList() {
        this.list = this.currentDirectory.listFiles();
        Arrays.sort(this.list, fileComparator);
        setListAdapter(new ArrayAdapter(this, 17367043, this.list));
        setTitle(this.currentDirectory.toString());
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        File f = this.list[position];
        if (f.isDirectory()) {
            this.currentDirectory = f;
            populateList();
        } else if (f.isFile()) {
            Intent result = new Intent();
            try {
                result.putExtra(FILE_KEY, f.getCanonicalPath());
                setResult(-1, result);
            } catch (IOException e) {
                setResult(0, result);
            }
            finish();
        }
    }
}
