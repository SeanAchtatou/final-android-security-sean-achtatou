package frink.android;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class BasicSensorService implements SensorService, SensorEventListener {
    public float[] rawValues = null;
    public Sensor sensor;
    public SensorManager sensorMgr;

    public BasicSensorService(int i, SensorManager sensorManager) {
        this.sensor = sensorManager.getDefaultSensor(i);
        this.sensorMgr = sensorManager;
        sensorManager.registerListener(this, this.sensor, 3);
    }

    public String getSensorName() {
        return this.sensor.getName();
    }

    public int getType() {
        return this.sensor.getType();
    }

    public float[] getRawValues() {
        return this.rawValues;
    }

    public void onAccuracyChanged(Sensor sensor2, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        this.rawValues = sensorEvent.values;
    }

    public void shutdown() {
        this.sensorMgr.unregisterListener(this);
    }
}
