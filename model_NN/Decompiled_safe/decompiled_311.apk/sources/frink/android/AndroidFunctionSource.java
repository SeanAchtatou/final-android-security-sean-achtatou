package frink.android;

import android.app.Activity;
import android.hardware.SensorManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import frink.errors.NotAnIntegerException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.StringExpression;
import frink.expr.UndefExpression;
import frink.expr.VoidExpression;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.SingleArgFunction;
import frink.function.ZeroArgFunction;
import frink.java.JavaObjectFactory;
import frink.text.StringUtils;

public class AndroidFunctionSource extends BasicFunctionSource {
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public AndroidServiceManager asm;
    private Geocoder geocoder = null;
    private LocationManager lm = null;
    private SensorManager sensorMgr = null;

    public AndroidFunctionSource(Activity activity2, AndroidServiceManager asm2) {
        super("AndroidFunctionSource");
        this.activity = activity2;
        this.asm = asm2;
        initializeFunctions();
    }

    private void initializeFunctions() {
        addFunctionDefinition("getActivity", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                return JavaObjectFactory.create(AndroidFunctionSource.this.activity);
            }
        });
        addFunctionDefinition("speak", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env, Expression arg) {
                String outtext;
                if (arg instanceof StringExpression) {
                    outtext = ((StringExpression) arg).getString();
                } else {
                    outtext = env.format(arg);
                }
                SpeechService ss = AndroidFunctionSource.this.asm.getSpeechService();
                if (ss != null) {
                    ss.speak(outtext);
                } else {
                    env.outputln("No speech service.");
                }
                return arg;
            }
        });
        addFunctionDefinition("speakSAMPA", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env, Expression arg) {
                String outtext;
                if (arg instanceof StringExpression) {
                    outtext = ((StringExpression) arg).getString();
                } else {
                    outtext = env.format(arg);
                }
                String XMLtext = "<speak xml:lang=\"en-US\"><phoneme alphabet=\"xsampa\" ph=\"" + StringUtils.replace(outtext, '\"', "&quot;") + "\"/>.</speak>";
                SpeechService ss = AndroidFunctionSource.this.asm.getSpeechService();
                if (ss != null) {
                    ss.speak(XMLtext);
                } else {
                    env.outputln("No speech service.");
                }
                return arg;
            }
        });
        addFunctionDefinition("speakln", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env, Expression arg) {
                String outtext;
                if (arg instanceof StringExpression) {
                    outtext = ((StringExpression) arg).getString();
                } else {
                    outtext = env.format(arg);
                }
                env.outputln(outtext);
                SpeechService ss = AndroidFunctionSource.this.asm.getSpeechService();
                if (ss != null) {
                    ss.speak(outtext);
                } else {
                    env.outputln("No speech service.");
                }
                return arg;
            }
        });
        addFunctionDefinition("getLocationManager", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                return JavaObjectFactory.createNullAllowed(AndroidFunctionSource.this.getLocationManager());
            }
        });
        addFunctionDefinition("getLastKnownGPSLocationJava", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                return JavaObjectFactory.createNullAllowed(AndroidFunctionSource.this.getLastKnownGPSLocation());
            }
        });
        addFunctionDefinition("getLastKnownNetworkLocationJava", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                return JavaObjectFactory.createNullAllowed(AndroidFunctionSource.this.getLastKnownNetworkLocation());
            }
        });
        addFunctionDefinition("getGeocoder", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                return JavaObjectFactory.createNullAllowed(AndroidFunctionSource.this.getGeocoder());
            }
        });
        addFunctionDefinition("startGPS", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                AndroidFunctionSource.this.asm.startGPS();
                return VoidExpression.VOID;
            }
        });
        addFunctionDefinition("stopGPS", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                AndroidFunctionSource.this.asm.stopGPS();
                return VoidExpression.VOID;
            }
        });
        addFunctionDefinition("getGPSLocationJava", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                Location loc = AndroidFunctionSource.this.asm.getGPSLocationJava();
                if (loc != null) {
                    return JavaObjectFactory.createNullAllowed(loc);
                }
                return UndefExpression.UNDEF;
            }
        });
        addFunctionDefinition("getSensorManager", new ZeroArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env) {
                return JavaObjectFactory.createNullAllowed(AndroidFunctionSource.this.getSensorManager());
            }
        });
        addFunctionDefinition("getSensorService", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env, Expression arg) throws InvalidArgumentException {
                int type = -1;
                try {
                    type = BuiltinFunctionSource.getIntegerValue(arg);
                    SensorService ss = AndroidFunctionSource.this.asm.getSensorService(type);
                    if (ss != null) {
                        return JavaObjectFactory.create(ss);
                    }
                    throw new InvalidArgumentException("Couldn't create SensorService for type " + type, this);
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("getSensorService[]: Type of sensor should be an integer.", this);
                } catch (Exception e2) {
                    throw new InvalidArgumentException("Couldn't create SensorService for type " + type + ", got exception\n:" + e2, this);
                }
            }
        });
        addFunctionDefinition("doUI", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment env, Expression arg) {
                return AndroidFunctionSource.this.asm.doUI(env, arg);
            }

            public boolean shouldEvaluateFirst() {
                return false;
            }
        });
    }

    public LocationManager getLocationManager() {
        if (this.lm == null) {
            this.lm = (LocationManager) this.activity.getSystemService("location");
        }
        return this.lm;
    }

    public SensorManager getSensorManager() {
        if (this.sensorMgr == null) {
            this.sensorMgr = (SensorManager) this.activity.getSystemService("sensor");
        }
        return this.sensorMgr;
    }

    public Location getLastKnownGPSLocation() {
        return getLocationManager().getLastKnownLocation("gps");
    }

    public Location getLastKnownNetworkLocation() {
        return getLocationManager().getLastKnownLocation("network");
    }

    public synchronized Geocoder getGeocoder() {
        if (this.geocoder == null) {
            this.geocoder = new Geocoder(this.activity);
        }
        return this.geocoder;
    }
}
