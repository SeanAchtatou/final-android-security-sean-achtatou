package frink.android;

import android.app.Activity;
import android.app.AlertDialog;
import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.graphics.GraphicsView;
import frink.graphics.GraphicsViewConstructor;
import frink.graphics.GraphicsViewFactory;
import frink.graphics.ScalingTranslatingGraphicsView;

public class AndroidViewFactory {
    public static final String OPEN_IN_ALERT = "AndroidGraphicsViewInAlertDialog";
    /* access modifiers changed from: private */
    public Activity activity;

    public AndroidViewFactory(Activity activity2, Environment environment) {
        this.activity = activity2;
        registerConstructors(environment);
    }

    public void registerConstructors(Environment environment) {
        environment.getGraphicsViewFactory().addConstructor(OPEN_IN_ALERT, new GraphicsViewConstructor() {
            public GraphicsView create(Environment environment, String str, Object obj, GraphicsViewFactory graphicsViewFactory) throws FrinkSecurityException {
                return AndroidViewFactory.this.createInAlertDialog(environment, str, obj, AndroidViewFactory.this.activity);
            }
        });
    }

    public AndroidGraphicsView createView(Environment environment, String str, Object obj) {
        return new AndroidGraphicsView(this.activity, environment);
    }

    public GraphicsView createInAlertDialog(Environment environment, String str, Object obj, Activity activity2) throws FrinkSecurityException {
        environment.getSecurityHelper().checkOpenGraphicsWindow();
        final GraphicsViewHolder graphicsViewHolder = new GraphicsViewHolder();
        final Activity activity3 = activity2;
        final String str2 = str;
        final Environment environment2 = environment;
        final Object obj2 = obj;
        AnonymousClass2 r0 = new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity3);
                if (str2 == null) {
                    builder.setTitle("Frink Graphics");
                } else {
                    builder.setTitle(str2);
                }
                ScalingTranslatingGraphicsView scalingTranslatingGraphicsView = new ScalingTranslatingGraphicsView(environment2);
                AndroidGraphicsView createView = AndroidViewFactory.this.createView(environment2, str2, obj2);
                scalingTranslatingGraphicsView.setChildView(createView.getGraphicsView());
                builder.setView(createView);
                builder.show();
                synchronized (graphicsViewHolder) {
                    graphicsViewHolder.value = scalingTranslatingGraphicsView;
                    graphicsViewHolder.notifyAll();
                }
            }
        };
        synchronized (graphicsViewHolder) {
            activity2.runOnUiThread(r0);
            try {
                graphicsViewHolder.wait();
            } catch (InterruptedException e) {
                environment.outputln("AndroidViewFactory:  Interrupted while creating.");
            }
        }
        return graphicsViewHolder.value;
    }
}
