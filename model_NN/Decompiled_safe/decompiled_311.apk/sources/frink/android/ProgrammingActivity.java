package frink.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.ViewSwitcher;
import frink.io.InputItem;
import frink.io.InputManager;
import frink.parser.Frink;
import frink.parser.sym;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ProgrammingActivity extends Activity implements Runnable, OutputManagerListener, InputManager, ActiveTextProvider, FontChangedListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String FILENAME_KEY = "FILENAME_KEY";
    private static final int LOAD_FILE_ACTION = 3;
    private static final int LOAD_SPECIFIC_FILE_ACTION = 5;
    private static final int MENU_GROUP_FILE = 0;
    private static final int MENU_GROUP_HELP = 2;
    private static final int MENU_GROUP_MODE = 1;
    private static final int MENU_ID_ANDROID_DOCS = 10;
    private static final int MENU_ID_CONVERT = 3;
    private static final int MENU_ID_DOCS = 6;
    private static final int MENU_ID_DONATE = 11;
    private static final int MENU_ID_EXIT = 1;
    private static final int MENU_ID_HELP = 5;
    private static final int MENU_ID_LOAD = 7;
    private static final int MENU_ID_MODE = 2;
    private static final int MENU_ID_NEW = 9;
    private static final int MENU_ID_PREFERENCES = 14;
    private static final int MENU_ID_PROGRAMMING = 4;
    private static final int MENU_ID_RUN = 15;
    private static final int MENU_ID_SAVE = 8;
    private static final int MENU_ID_WHATS_NEW = 12;
    public static final int MESSAGE_FONT_SIZE_CHANGED = 203;
    public static final int MESSAGE_OUTPUT_MANAGER_CLOSED = 202;
    public static final int MESSAGE_OUTPUT_PRODUCED = 201;
    public static final int MESSAGE_OUT_OF_MEMORY = 911;
    public static final int MESSAGE_SET_PROGRAM_RUNNING = 200;
    public static final int MESSAGE_TEXT_SHOULD_CHANGE = 205;
    public static final int MESSAGE_UPDATE_TITLE = 204;
    private static final int NEW_FILE_ACTION = 4;
    private static final int NOT_SAVED_DIALOG_ID = 2;
    private static final String PROGRAMMING_FONT_SIZE_KEY = "PROGRAMMING_FONT_SIZE_KEY";
    private static final String PROGRAM_AREA_KEY = "PROGRAM_AREA_KEY";
    private static final String PROGRAM_CHANGED_KEY = "PROGRAM_CHANGED_KEY";
    private static final int SAVE_AS_DIALOG_ID = 1;
    private static final String SAVE_ROOT = "frink";
    private static final String SELECTION_END_KEY = "SELECTION_END_KEY";
    private static final String SELECTION_START_KEY = "SELECTION_START_KEY";
    private static final String SHOW_RUN_KEY = "SHOW_RUN_KEY";
    private static final String STACK_SIZE_KEY = "STACK_SIZE_KEY";
    private MenuItem androidDocsMenuItem;
    private TableLayout buttonLayout;
    private TableRow buttonRow;
    private MenuItem convertMenuItem;
    private File currFile = null;
    private MenuItem docsMenuItem;
    private MenuItem donateMenuItem;
    private MenuItem exitMenuItem;
    private int fontSize = 8;
    private final Handler handler = new Handler() {
        public void handleMessage(Message m) {
            switch (m.what) {
                case 200:
                    ProgrammingActivity.this.doSetProgramRunning(m.arg1 != 0);
                    return;
                case ProgrammingActivity.MESSAGE_OUTPUT_PRODUCED /*201*/:
                    ProgrammingActivity.this.doOutputProduced();
                    return;
                case ProgrammingActivity.MESSAGE_OUTPUT_MANAGER_CLOSED /*202*/:
                    ProgrammingActivity.this.doOutputManagerClosed();
                    return;
                case ProgrammingActivity.MESSAGE_FONT_SIZE_CHANGED /*203*/:
                    ProgrammingActivity.this.doFontSizeChanged(m.arg1);
                    return;
                case ProgrammingActivity.MESSAGE_UPDATE_TITLE /*204*/:
                    ProgrammingActivity.this.doUpdateTitle();
                    return;
                case ProgrammingActivity.MESSAGE_TEXT_SHOULD_CHANGE /*205*/:
                    ProgrammingActivity.this.setTextChanged(false);
                    return;
                case ProgrammingActivity.MESSAGE_OUT_OF_MEMORY /*911*/:
                    ProgrammingActivity.this.outOfMemoryDialog.show();
                    return;
                default:
                    ProgrammingActivity.this.outputMgr.output("Unhandled message " + m);
                    return;
            }
        }
    };
    private MenuItem helpMenuItem;
    /* access modifiers changed from: private */
    public AndroidInputManager inputMgr;
    /* access modifiers changed from: private */
    public Frink interp;
    private boolean isRunning = false;
    private MenuItem loadMenuItem;
    private MenuItem modeMenuItem;
    private MenuItem newMenuItem;
    private int nextAction = 0;
    private String nextFile = null;
    /* access modifiers changed from: private */
    public AlertDialog outOfMemoryDialog = null;
    private Message outOfMemoryMessage = null;
    /* access modifiers changed from: private */
    public AndroidOutputManager outputMgr;
    private MenuItem preferencesMenuItem;
    private boolean prime = true;
    private EditText programArea;
    private MenuItem programmingMenuItem;
    private Button runButton;
    private Object runLocker;
    private MenuItem runMenuItem;
    private boolean runRequested;
    private Thread runner = null;
    /* access modifiers changed from: private */
    public EditText saveFilenameView;
    private MenuItem saveMenuItem;
    private Button showOutputButton;
    private ScrollView sv;
    /* access modifiers changed from: private */
    public ViewSwitcher switcher;
    /* access modifiers changed from: private */
    public boolean textChanged = false;
    private LinearLayout topLayout;
    private MenuItem whatsNewMenuItem;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.interp = null;
        initGUI();
        this.runRequested = false;
        this.runLocker = new Object();
        createOutOfMemoryDialog();
        initRunnerThread();
        Intent intent = getIntent();
        restoreUI();
        setTitle();
        if (intent != null && "android.intent.action.VIEW".equals(intent.getAction())) {
            queryThenLoadFile(intent.getData().getPath());
        }
    }

    private void restoreUI() {
        SharedPreferences prefs = getPreferences(0);
        String filename = prefs.getString(FILENAME_KEY, null);
        if (filename != null) {
            this.currFile = new File(filename);
        }
        String prog = prefs.getString(PROGRAM_AREA_KEY, null);
        if (prog == null || (filename == null && prog.indexOf("Your estimated address") != -1)) {
            prog = "\n// Simple program to draw spirals.\n// Change the step or the equation\n// to make vastly different graphs.\n\nincr=eval[input[\"Enter degrees between sides: \", 20]] degrees\ng = new graphics\np = new polyline\ng.stroke[2]\n\nextent = 10.1 circles\n\nfor theta = 0 degrees to extent step incr\n{\n    r = theta                   // Change this to make different graphs\n    x = r cos[theta]            // Convert polar to rectangular coordinates\n    y = r sin[theta]\n    p.addPoint[x, y]\n}\n\ng.add[p]    // Add polyline to the graphics object\ng.show[]\n";
        }
        this.programArea.setText(prog);
        boolean changed = prefs.getBoolean(PROGRAM_CHANGED_KEY, true);
        setTextChanged(changed);
        if (!changed) {
            sendTextChangingFuture();
        }
    }

    private void initGUI() {
        this.topLayout = new LinearLayout(this);
        this.topLayout.setOrientation(1);
        this.topLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.sv = new ScrollView(this);
        this.sv.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.fontSize = getPreferences(0).getInt(PROGRAMMING_FONT_SIZE_KEY, 8);
        this.programArea = new EditText(this);
        this.programArea.setId(123);
        this.programArea.setTypeface(Typeface.MONOSPACE);
        this.programArea.setTextSize(2, (float) this.fontSize);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPrefs.registerOnSharedPreferenceChangeListener(this);
        if (sharedPrefs.contains(PROGRAMMING_FONT_SIZE_KEY)) {
            this.fontSize = Integer.decode(sharedPrefs.getString(PROGRAMMING_FONT_SIZE_KEY, "8")).intValue();
        }
        boolean showRun = sharedPrefs.getBoolean(SHOW_RUN_KEY, true);
        this.programArea.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.programArea.setHorizontallyScrolling(true);
        this.programArea.setFreezesText(true);
        this.programArea.setCursorVisible(true);
        this.programArea.selectAll();
        this.sv.addView(this.programArea);
        this.programArea.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!ProgrammingActivity.this.textChanged) {
                    ProgrammingActivity.this.setTextChanged(true);
                }
            }
        });
        this.runButton = new Button(this);
        this.runButton.setText("Run");
        this.runButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProgrammingActivity.this.runProgram();
            }
        });
        this.showOutputButton = new Button(this);
        this.showOutputButton.setText("Show Output");
        this.showOutputButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProgrammingActivity.this.switcher.showNext();
            }
        });
        this.buttonLayout = new TableLayout(this);
        this.buttonLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.buttonRow = new TableRow(this);
        this.buttonRow.addView(this.runButton);
        this.buttonRow.addView(this.showOutputButton);
        this.buttonLayout.setColumnStretchable(0, true);
        this.buttonLayout.setColumnStretchable(1, true);
        this.buttonLayout.addView(this.buttonRow);
        if (showRun) {
            this.buttonLayout.setVisibility(0);
        } else {
            this.buttonLayout.setVisibility(8);
        }
        this.outputMgr = new AndroidOutputManager(this, this);
        this.outputMgr.fontSizeChanged(this.fontSize);
        this.inputMgr = new AndroidInputManager(this);
        this.switcher = new ViewSwitcher(this);
        this.switcher.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.topLayout.addView(this.sv);
        this.topLayout.addView(this.buttonLayout);
        HorizontalScrollView hScroll = new HorizontalScrollView(this);
        hScroll.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        hScroll.addView(new FrinkButtons(this, this, null));
        hScroll.setFillViewport(true);
        this.topLayout.addView(hScroll);
        this.switcher.addView(this.topLayout);
        this.switcher.addView(this.outputMgr);
        setContentView(this.switcher);
    }

    private void initRunnerThread() {
        ThreadGroup tg = new ThreadGroup("ProgrammingActivity runner threads");
        int stackSize = 0;
        try {
            stackSize = Integer.decode(PreferenceManager.getDefaultSharedPreferences(this).getString(STACK_SIZE_KEY, "0").trim()).intValue();
            if (stackSize < 8000) {
                stackSize = 8000;
            }
        } catch (NumberFormatException e) {
        }
        if (stackSize <= 0) {
            this.runner = new Thread(tg, this, "ProgrammingActivity runner");
        } else {
            this.runner = new Thread(tg, this, "ProgrammingActivity runner", (long) stackSize);
        }
        this.runner.setDaemon(true);
        this.runner.setPriority(4);
        this.runner.start();
    }

    public void runProgram() {
        savePrefs();
        this.runRequested = true;
        setProgramRunning(true);
        synchronized (this.runLocker) {
            this.runLocker.notifyAll();
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(PROGRAM_AREA_KEY)) {
                this.programArea.setText(savedInstanceState.getString(PROGRAM_AREA_KEY));
            }
            if (savedInstanceState.containsKey(SELECTION_START_KEY)) {
                this.programArea.setSelection(savedInstanceState.getInt(SELECTION_START_KEY), savedInstanceState.getInt(SELECTION_END_KEY));
            }
            if (savedInstanceState.containsKey(PROGRAM_CHANGED_KEY)) {
                boolean changed = savedInstanceState.getBoolean(PROGRAM_CHANGED_KEY);
                setTextChanged(changed);
                if (!changed) {
                    sendTextChangingFuture();
                }
            }
        }
    }

    public void stopProgram(boolean resume) {
        if (this.runner != null) {
            try {
                this.runner.stop();
            } catch (Throwable th) {
            } finally {
                this.runner = null;
                this.outputMgr.outputln("\n-- Program stopped by user. --");
            }
        }
        setProgramRunning(false);
        if (resume) {
            initRunnerThread();
        }
    }

    public void onPause() {
        super.onPause();
        savePrefs();
        try {
            if (this.runner != null) {
                this.runner.suspend();
            }
        } catch (Throwable th) {
        }
    }

    private void savePrefs() {
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putString(PROGRAM_AREA_KEY, this.programArea.getText().toString());
        if (this.currFile != null) {
            editor.putString(FILENAME_KEY, this.currFile.toString());
        } else {
            editor.putString(FILENAME_KEY, null);
        }
        editor.putInt(SELECTION_START_KEY, this.programArea.getSelectionStart());
        editor.putInt(SELECTION_END_KEY, this.programArea.getSelectionEnd());
        editor.putInt(PROGRAMMING_FONT_SIZE_KEY, this.fontSize);
        editor.putBoolean(PROGRAM_CHANGED_KEY, this.textChanged);
        editor.commit();
    }

    public void onStop() {
        super.onStop();
        try {
            if (this.runner != null) {
                this.runner.suspend();
            }
        } catch (Throwable th) {
        }
    }

    public void onResume() {
        super.onResume();
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            if (this.runner != null) {
                this.runner.resume();
            }
        } catch (Throwable th) {
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(PROGRAM_AREA_KEY, this.programArea.getText().toString());
        savedInstanceState.putInt(SELECTION_START_KEY, this.programArea.getSelectionStart());
        savedInstanceState.putInt(SELECTION_END_KEY, this.programArea.getSelectionEnd());
        savedInstanceState.putInt(PROGRAMMING_FONT_SIZE_KEY, this.fontSize);
        savedInstanceState.putBoolean(PROGRAM_CHANGED_KEY, this.textChanged);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onDestroy() {
        super.onDestroy();
        stopProgram(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c5 A[SYNTHETIC, Splitter:B:32:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00f5 A[SYNTHETIC, Splitter:B:53:0x00f5] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x012e A[SYNTHETIC, Splitter:B:66:0x012e] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0144 A[SYNTHETIC, Splitter:B:75:0x0144] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r15 = this;
            r14 = 0
            r13 = 0
            r0 = 0
        L_0x0003:
            frink.parser.Frink r10 = r15.interp
            if (r10 != 0) goto L_0x0015
            frink.parser.Frink r10 = new frink.parser.Frink
            r10.<init>()
            r15.interp = r10
            frink.parser.Frink r10 = r15.interp     // Catch:{ FrinkEvaluationException -> 0x007b }
            java.lang.String r11 = "2+2"
            r10.parseString(r11)     // Catch:{ FrinkEvaluationException -> 0x007b }
        L_0x0015:
            boolean r10 = r15.runRequested
            if (r10 == 0) goto L_0x0154
            java.io.File r10 = r15.currFile     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            if (r10 == 0) goto L_0x0096
            frink.parser.Frink r10 = r15.interp     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.expr.Environment r10 = r10.getEnvironment()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.parser.IncludeManager r10 = r10.getIncludeManager()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            java.io.File r11 = r15.currFile     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            java.lang.String r11 = r11.getParent()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            r10.appendPath(r11)     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
        L_0x0030:
            frink.parser.Frink r10 = r15.interp     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.expr.Environment r3 = r10.getEnvironment()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            r3.setInputManager(r15)     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.android.AndroidOutputManager r10 = r15.outputMgr     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            r3.setOutputManager(r10)     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.android.AndroidServiceManager r1 = new frink.android.AndroidServiceManager     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            r1.<init>(r15)     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.function.FunctionManager r10 = r3.getFunctionManager()     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            frink.android.AndroidFunctionSource r11 = new frink.android.AndroidFunctionSource     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            r11.<init>(r15, r1)     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            r12 = 0
            r10.addFunctionSource(r11, r12)     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            frink.android.AndroidViewFactory r2 = new frink.android.AndroidViewFactory     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            r2.<init>(r15, r3)     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            frink.graphics.GraphicsViewFactory r10 = r3.getGraphicsViewFactory()     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            java.lang.String r11 = "AndroidGraphicsViewInAlertDialog"
            r10.setDefaultConstructorName(r11)     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            frink.parser.Frink r10 = r15.interp     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            android.widget.EditText r11 = r15.programArea     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            android.text.Editable r11 = r11.getText()     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            java.lang.String r11 = r11.toString()     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            r10.parseString(r11)     // Catch:{ FrinkEvaluationException -> 0x0180, OutOfMemoryError -> 0x017b, Throwable -> 0x0177, all -> 0x0174 }
            r15.interp = r14
            if (r1 == 0) goto L_0x0074
            r1.shutdown()     // Catch:{ all -> 0x00cf }
        L_0x0074:
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            r0 = r1
            goto L_0x0003
        L_0x007b:
            r10 = move-exception
            r6 = r10
            frink.android.AndroidOutputManager r10 = r15.outputMgr
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Error when priming interpreter:\n "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r6)
            java.lang.String r11 = r11.toString()
            r10.outputln(r11)
            goto L_0x0015
        L_0x0096:
            java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            if (r4 == 0) goto L_0x0030
            java.io.File r5 = new java.io.File     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            java.lang.String r10 = "frink"
            r5.<init>(r4, r10)     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.parser.Frink r10 = r15.interp     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.expr.Environment r10 = r10.getEnvironment()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            frink.parser.IncludeManager r10 = r10.getIncludeManager()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            java.lang.String r11 = r5.toString()     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            r10.appendPath(r11)     // Catch:{ FrinkEvaluationException -> 0x00b6, OutOfMemoryError -> 0x00dd, Throwable -> 0x0105 }
            goto L_0x0030
        L_0x00b6:
            r10 = move-exception
            r6 = r10
        L_0x00b8:
            frink.android.AndroidOutputManager r10 = r15.outputMgr     // Catch:{ all -> 0x013f }
            java.lang.String r11 = r6.toString()     // Catch:{ all -> 0x013f }
            r10.outputln(r11)     // Catch:{ all -> 0x013f }
            r15.interp = r14
            if (r0 == 0) goto L_0x00c8
            r0.shutdown()     // Catch:{ all -> 0x00d6 }
        L_0x00c8:
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            goto L_0x0003
        L_0x00cf:
            r10 = move-exception
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            throw r10
        L_0x00d6:
            r10 = move-exception
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            throw r10
        L_0x00dd:
            r10 = move-exception
            r8 = r10
        L_0x00df:
            r10 = 0
            r15.interp = r10     // Catch:{ all -> 0x013f }
            java.lang.System.gc()     // Catch:{ all -> 0x013f }
            r10 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r10)     // Catch:{ InterruptedException -> 0x0171 }
        L_0x00ea:
            android.os.Handler r10 = r15.handler     // Catch:{ all -> 0x013f }
            android.os.Message r11 = r15.outOfMemoryMessage     // Catch:{ all -> 0x013f }
            r10.sendMessage(r11)     // Catch:{ all -> 0x013f }
            r15.interp = r14
            if (r0 == 0) goto L_0x00f8
            r0.shutdown()     // Catch:{ all -> 0x00fe }
        L_0x00f8:
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            return
        L_0x00fe:
            r10 = move-exception
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            throw r10
        L_0x0105:
            r10 = move-exception
            r9 = r10
        L_0x0107:
            frink.android.AndroidOutputManager r10 = r15.outputMgr     // Catch:{ all -> 0x013f }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x013f }
            r11.<init>()     // Catch:{ all -> 0x013f }
            java.lang.String r12 = "Your device threw a low-level error from Java:\n  "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x013f }
            java.lang.String r12 = r9.getMessage()     // Catch:{ all -> 0x013f }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x013f }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x013f }
            r10.outputln(r11)     // Catch:{ all -> 0x013f }
            frink.android.AndroidOutputManager r10 = r15.outputMgr     // Catch:{ all -> 0x013f }
            java.lang.String r11 = "If this is unexpected, please report it to eliasen@mindspring.com so it can be fixed."
            r10.outputln(r11)     // Catch:{ all -> 0x013f }
            r15.interp = r14
            if (r0 == 0) goto L_0x0131
            r0.shutdown()     // Catch:{ all -> 0x0138 }
        L_0x0131:
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            goto L_0x0003
        L_0x0138:
            r10 = move-exception
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            throw r10
        L_0x013f:
            r10 = move-exception
        L_0x0140:
            r15.interp = r14
            if (r0 == 0) goto L_0x0147
            r0.shutdown()     // Catch:{ all -> 0x014d }
        L_0x0147:
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            throw r10
        L_0x014d:
            r10 = move-exception
            r15.setProgramRunning(r13)
            r15.runRequested = r13
            throw r10
        L_0x0154:
            java.lang.Object r10 = r15.runLocker     // Catch:{ InterruptedException -> 0x0162 }
            monitor-enter(r10)     // Catch:{ InterruptedException -> 0x0162 }
            java.lang.Object r11 = r15.runLocker     // Catch:{ all -> 0x015f }
            r11.wait()     // Catch:{ all -> 0x015f }
            monitor-exit(r10)     // Catch:{ all -> 0x015f }
            goto L_0x0003
        L_0x015f:
            r11 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x015f }
            throw r11     // Catch:{ InterruptedException -> 0x0162 }
        L_0x0162:
            r10 = move-exception
            r7 = r10
            frink.parser.Frink r10 = r15.interp
            frink.expr.Environment r10 = r10.getEnvironment()
            java.lang.String r11 = "Calculation interrupted in wait."
            r10.outputln(r11)
            goto L_0x0003
        L_0x0171:
            r10 = move-exception
            goto L_0x00ea
        L_0x0174:
            r10 = move-exception
            r0 = r1
            goto L_0x0140
        L_0x0177:
            r10 = move-exception
            r9 = r10
            r0 = r1
            goto L_0x0107
        L_0x017b:
            r10 = move-exception
            r8 = r10
            r0 = r1
            goto L_0x00df
        L_0x0180:
            r10 = move-exception
            r6 = r10
            r0 = r1
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.android.ProgrammingActivity.run():void");
    }

    private void setProgramRunning(boolean flag) {
        Message m = Message.obtain(this.handler, 200);
        m.arg1 = flag ? 1 : 0;
        this.handler.sendMessage(m);
    }

    /* access modifiers changed from: private */
    public void doSetProgramRunning(boolean flag) {
        boolean z;
        this.runButton.setEnabled(!flag);
        if (this.runMenuItem != null) {
            MenuItem menuItem = this.runMenuItem;
            if (!flag) {
                z = true;
            } else {
                z = false;
            }
            menuItem.setEnabled(z);
        }
        if (flag) {
            this.outputMgr.clear();
        }
        this.isRunning = flag;
        updateTitle();
    }

    private void updateTitle() {
        this.handler.sendMessage(Message.obtain(this.handler, (int) MESSAGE_UPDATE_TITLE));
    }

    /* access modifiers changed from: private */
    public void doUpdateTitle() {
        String str;
        String str2;
        StringBuilder append = new StringBuilder().append(this.currFile != null ? this.currFile.getName() : "Frink Program");
        if (this.textChanged) {
            str = " *";
        } else {
            str = "";
        }
        StringBuilder append2 = append.append(str);
        if (this.isRunning) {
            str2 = " (running)";
        } else {
            str2 = "";
        }
        setTitle(append2.append(str2).toString());
    }

    public void outputProduced() {
        this.handler.sendMessage(Message.obtain(this.handler, (int) MESSAGE_OUTPUT_PRODUCED));
    }

    /* access modifiers changed from: private */
    public void doOutputProduced() {
        if (this.switcher.getDisplayedChild() != 1) {
            this.switcher.setDisplayedChild(1);
        }
    }

    public void outputManagerClosed() {
        this.handler.sendMessage(Message.obtain(this.handler, (int) MESSAGE_OUTPUT_MANAGER_CLOSED));
    }

    /* access modifiers changed from: private */
    public void doOutputManagerClosed() {
        this.switcher.setDisplayedChild(0);
    }

    public EditText getActiveField() {
        return this.programArea;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.saveMenuItem = menu.add(0, 8, 0, "Save as...");
        this.saveMenuItem.setAlphabeticShortcut('s');
        this.saveMenuItem.setIcon(17301582);
        this.runMenuItem = menu.add(0, 15, 0, "Run");
        this.runMenuItem.setAlphabeticShortcut('r');
        this.preferencesMenuItem = menu.add(0, 14, 0, "Settings...");
        this.preferencesMenuItem.setIcon(17301593);
        this.preferencesMenuItem.setIcon(17301577);
        this.convertMenuItem = menu.add(1, 3, 0, "Convert Mode");
        SubMenu helpMenu = menu.addSubMenu(2, 5, 0, "Docs...");
        helpMenu.setIcon(17301568);
        this.docsMenuItem = helpMenu.add(2, 6, 0, "Frink Documentation");
        this.androidDocsMenuItem = helpMenu.add(2, 10, 0, "Frink on Android");
        this.donateMenuItem = helpMenu.add(2, 11, 0, "Donate");
        this.whatsNewMenuItem = helpMenu.add(2, 12, 0, "What's New in Frink");
        this.newMenuItem = menu.add(0, 9, 0, "New program...");
        this.newMenuItem.setIcon(17301566);
        this.loadMenuItem = menu.add(0, 7, 0, "Load File...");
        this.loadMenuItem.setAlphabeticShortcut('l');
        this.exitMenuItem = menu.add(0, 1, 0, "Exit Frink");
        this.exitMenuItem.setIcon(17301560);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                finish();
                return true;
            case 2:
            case 5:
            case sym.NOEVAL /*13*/:
            default:
                return false;
            case 3:
                startConvertMode();
                return true;
            case 4:
                return true;
            case 6:
                launchDocs();
                return true;
            case 7:
                queryThenLoadFile();
                return true;
            case 8:
                saveFileAs();
                return true;
            case 9:
                queryThenNew();
                return true;
            case 10:
                launchAndroidDocs();
                return true;
            case 11:
                launchDonate();
                return true;
            case 12:
                launchWhatsNew();
                return true;
            case 14:
                launchPreferences();
                return true;
            case 15:
                runProgram();
                return true;
        }
    }

    private void launchDocs() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/")));
    }

    private void launchAndroidDocs() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/android.html")));
    }

    private void launchDonate() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/donate.html")));
    }

    private void launchWhatsNew() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/whatsnew.html")));
    }

    private void startConvertMode() {
        startActivity(new Intent(this, Frink.class));
    }

    private void queryThenLoadFile() {
        this.nextAction = 3;
        if (this.textChanged) {
            showDialog(2);
        } else {
            doNextThing();
        }
    }

    private void queryThenLoadFile(String filename) {
        this.nextFile = filename;
        this.nextAction = 5;
        if (this.textChanged) {
            showDialog(2);
        } else {
            doNextThing();
        }
    }

    private void queryThenNew() {
        this.nextAction = 4;
        if (this.textChanged) {
            showDialog(2);
        } else {
            doNextThing();
        }
    }

    private void loadFile() {
        startActivityForResult(new Intent(this, FileChooser.class), FileChooser.LOAD_FILE);
    }

    private void newFile() {
        this.programArea.setText("");
        this.currFile = null;
        setTextChanged(false);
        sendTextChangingFuture();
    }

    /* access modifiers changed from: private */
    public void saveFileAs() {
        showDialog(1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FileChooser.LOAD_FILE:
                if (resultCode == -1) {
                    doLoadFile(data.getStringExtra(FileChooser.FILE_KEY));
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void doLoadFile(String filename) {
        File f = new File(filename);
        String file = f.getPath();
        try {
            FileReader fr = new FileReader(file);
            StringBuffer sb = new StringBuffer();
            char[] buf = new char[1024];
            while (true) {
                try {
                    int chars = fr.read(buf, 0, 1024);
                    if (chars != -1) {
                        sb.append(buf, 0, chars);
                    } else {
                        fr.close();
                        this.programArea.setText(new String(sb));
                        sendTextChangingFuture();
                        this.currFile = f;
                        setTitle();
                        return;
                    }
                } catch (IOException e) {
                    this.outputMgr.outputln("Error on reading " + f.getPath() + ":\n" + e);
                    return;
                }
            }
        } catch (FileNotFoundException e2) {
            this.outputMgr.outputln("File '" + file + "' not found:\n" + e2);
        }
    }

    private void sendTextChangingFuture() {
        this.handler.sendMessageDelayed(Message.obtain(this.handler, (int) MESSAGE_TEXT_SHOULD_CHANGE), 1500);
    }

    /* access modifiers changed from: private */
    public void doSaveFileRelative(String relative) {
        File extdir = new File(Environment.getExternalStorageDirectory(), SAVE_ROOT);
        if (!extdir.isDirectory()) {
            extdir.mkdir();
        }
        doSaveFile(new File(extdir, relative));
    }

    private void doSaveFile(File f) {
        File path = f.getParentFile();
        if (!path.isDirectory()) {
            path.mkdirs();
        }
        try {
            FileWriter fw = new FileWriter(f);
            fw.write(this.programArea.getText().toString());
            fw.close();
            this.currFile = f;
            setTextChanged(false);
            doNextThing();
        } catch (IOException e) {
            this.outputMgr.outputln("Error on writing " + f + ":\n" + e);
        }
    }

    /* access modifiers changed from: private */
    public void doNextThing() {
        switch (this.nextAction) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                showDialog(this.nextAction);
                this.nextAction = 0;
                return;
            case 3:
                this.nextAction = 0;
                loadFile();
                return;
            case 4:
                this.nextAction = 0;
                newFile();
                return;
            case 5:
                this.nextAction = 0;
                doLoadFile(this.nextFile);
                this.nextFile = null;
                return;
        }
    }

    private void setTitle() {
        if (this.currFile != null) {
            setTitle(this.currFile.getName());
        } else {
            setTitle("Frink Program");
        }
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                AlertDialog.Builder saveAsDialog = new AlertDialog.Builder(this);
                saveAsDialog.setTitle("Save program as...");
                saveAsDialog.setMessage("Enter filename.  File will be saved to your removable media in the directory\n" + new File(Environment.getExternalStorageDirectory(), SAVE_ROOT));
                this.saveFilenameView = new EditText(this);
                this.saveFilenameView.setSingleLine(true);
                saveAsDialog.setView(this.saveFilenameView);
                saveAsDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        ProgrammingActivity.this.doSaveFileRelative(ProgrammingActivity.this.saveFilenameView.getText().toString());
                    }
                });
                saveAsDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    }
                });
                return saveAsDialog.create();
            case 2:
                AlertDialog.Builder notSavedDialog = new AlertDialog.Builder(this);
                notSavedDialog.setTitle("Save program first?");
                notSavedDialog.setMessage("The current file " + (this.currFile != null ? "\"" + this.currFile.toString() + "\"" : "") + " has not been saved.  Save it before continuing?");
                notSavedDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        ProgrammingActivity.this.saveFileAs();
                    }
                });
                notSavedDialog.setNeutralButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        ProgrammingActivity.this.doNextThing();
                    }
                });
                notSavedDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    }
                });
                return notSavedDialog.create();
            default:
                return null;
        }
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        int startpos = 0;
        int endpos = 0;
        switch (id) {
            case 1:
                String text = ".frink";
                if (this.currFile != null) {
                    String fullPath = this.currFile.getPath();
                    int pos = fullPath.indexOf(SAVE_ROOT);
                    if (pos == -1) {
                        text = this.currFile.getName();
                    } else {
                        int newpos = SAVE_ROOT.length() + pos + 1;
                        if (newpos > fullPath.length()) {
                            text = ".frink";
                        } else {
                            text = fullPath.substring(newpos);
                        }
                    }
                    startpos = text.lastIndexOf(File.separatorChar) + 1;
                    if (startpos == -1) {
                        startpos = 0;
                    }
                    endpos = text.lastIndexOf(46);
                    if (endpos == -1) {
                        endpos = 0;
                    }
                }
                this.saveFilenameView.setText(text);
                if (startpos == 0 && endpos == 0) {
                    this.saveFilenameView.setSelection(1);
                    this.saveFilenameView.extendSelection(0);
                    this.saveFilenameView.setSelection(0);
                    return;
                } else if (endpos != -1) {
                    this.saveFilenameView.setSelection(startpos, endpos);
                    return;
                } else {
                    return;
                }
            case 2:
                ((AlertDialog) dialog).setMessage("The current file" + (this.currFile != null ? " \"" + this.currFile.toString() + "\"" : "") + " has not been saved.  Save it before continuing?");
                return;
            default:
                return;
        }
    }

    public String input(String prompt, String defaultValue, frink.expr.Environment env) {
        final StringHolder sh = new StringHolder();
        final String str = prompt;
        final String str2 = defaultValue;
        final frink.expr.Environment environment = env;
        Runnable r = new Runnable() {
            public void run() {
                ProgrammingActivity.this.inputMgr.input(str, str2, environment, ProgrammingActivity.this.interp, sh);
            }
        };
        synchronized (this.interp) {
            try {
                runOnUiThread(r);
                this.interp.wait();
            } catch (InterruptedException e) {
                this.outputMgr.outputln("Interrupted when waiting for input:\n" + e);
            }
        }
        return sh.value;
    }

    public String[] input(String mainPrompt, InputItem[] fields, frink.expr.Environment env) {
        final MultiStringHolder msh = new MultiStringHolder();
        final String str = mainPrompt;
        final InputItem[] inputItemArr = fields;
        final frink.expr.Environment environment = env;
        Runnable r = new Runnable() {
            public void run() {
                ProgrammingActivity.this.inputMgr.input(str, inputItemArr, environment, ProgrammingActivity.this.interp, msh);
            }
        };
        synchronized (this.interp) {
            try {
                runOnUiThread(r);
                this.interp.wait();
            } catch (InterruptedException e) {
                this.outputMgr.outputln("Interrupted when waiting for input:\n" + e);
            }
        }
        return msh.values;
    }

    public void fontSizeChanged(int newSize) {
        Message m = Message.obtain(this.handler, (int) MESSAGE_FONT_SIZE_CHANGED);
        m.arg1 = newSize;
        this.handler.sendMessage(m);
    }

    /* access modifiers changed from: private */
    public void doFontSizeChanged(int newSize) {
        if (this.fontSize != newSize) {
            this.programArea.setTextSize(2, (float) newSize);
            this.outputMgr.fontSizeChanged(newSize);
            this.fontSize = newSize;
        }
    }

    public void launchPreferences() {
        startActivity(new Intent(this, ProgrammingPreferences.class));
    }

    private void createOutOfMemoryDialog() {
        this.outOfMemoryMessage = Message.obtain(this.handler, (int) MESSAGE_OUT_OF_MEMORY);
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Out of memory!");
        b.setMessage("Your device threw an Out of Memory error.  You may need to make more RAM available to execute this program.\n\nNote that RAM is *not* the same as free space on your slow SD card, but is the small, fast memory available to run programs (usually 256 MB or less,) which must be shared between *all* installed applications, running programs, and running services on your device.\n\nThe operating system may also limit the memory available to each process (usually to 16 MB or less, which is a fundamental limit for any Android program.)  If you're hitting this limit, you may be unable to run this program on any Android device.\n\nYou may need to stop some running applications or background services, or uninstall unused applications to free up RAM.\n\nThe program state has been saved.  Frink will now exit, as results will be unreliable after OutOfMemory is thrown.\n");
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                ProgrammingActivity.this.finish();
            }
        });
        this.outOfMemoryDialog = b.create();
        this.outOfMemoryDialog.show();
        this.outOfMemoryDialog.hide();
    }

    /* access modifiers changed from: private */
    public void setTextChanged(boolean flag) {
        this.textChanged = flag;
        updateTitle();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.switcher.getDisplayedChild() != 1) {
            return super.onKeyDown(keyCode, event);
        }
        outputManagerClosed();
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(this.switcher);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPrefs, String key) {
        if (PROGRAMMING_FONT_SIZE_KEY.equals(key)) {
            fontSizeChanged(Integer.decode(sharedPrefs.getString(PROGRAMMING_FONT_SIZE_KEY, "8")).intValue());
        }
        if (SHOW_RUN_KEY.equals(key)) {
            if (sharedPrefs.getBoolean(SHOW_RUN_KEY, true)) {
                this.buttonLayout.setVisibility(0);
            } else {
                this.buttonLayout.setVisibility(8);
            }
        }
        if (STACK_SIZE_KEY.equals(key) && !"0".equals(sharedPrefs.getString(STACK_SIZE_KEY, "0"))) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Stack size changed");
            alertDialog.setMessage("Stack size for running programs has been changed.  This change will not take effect until you completely exit Frink by using the \"Exit Frink\" menu item and restarting.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                }
            });
            alertDialog.setNegativeButton("Exit Now", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    ProgrammingActivity.this.finish();
                }
            });
            alertDialog.show();
        }
    }
}
