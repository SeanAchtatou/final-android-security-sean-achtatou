package frink.android;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import frink.gui.InteractiveController;

public class FrinkButtons extends LinearLayout {
    /* access modifiers changed from: private */
    public InteractiveController controller;
    private ActiveTextProvider fieldProv;

    public FrinkButtons(Context context, ActiveTextProvider fieldProv2, InteractiveController controller2) {
        super(context);
        this.fieldProv = fieldProv2;
        this.controller = controller2;
        initGUI(context);
    }

    private void initGUI(Context context) {
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        addView(new ToolButton(context, "\" \"", "\"", "\"", -1, this.fieldProv));
        addView(new ToolButton(context, "[ ]", "[", "]", -1, this.fieldProv));
        addView(new ToolButton(context, "[", "[", "", 0, this.fieldProv));
        addView(new ToolButton(context, "]", "]", "", 0, this.fieldProv));
        addView(new ToolButton(context, "( )", "(", ")", -1, this.fieldProv));
        addView(new ToolButton(context, "/", "/ (", ")", -1, this.fieldProv));
        addView(new ToolButton(context, "->", "->", "", 0, this.fieldProv));
        addView(new ToolButton(context, "^", "^", "", 0, "(", ")^", 0, this.fieldProv));
        addView(new ToolButton(context, "^( )", "", "^()", -1, "(", ")^()", -1, this.fieldProv));
        addView(new ToolButton(context, "{", "{\n", "\n}", -2, "{\n", "\n}", -2, this.fieldProv));
        if (this.controller != null) {
            Button b = new Button(context);
            b.setText("↑");
            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FrinkButtons.this.controller.historyBack();
                }
            });
            addView(b);
            Button b2 = new Button(context);
            b2.setText("↓");
            b2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FrinkButtons.this.controller.historyForward();
                }
            });
            addView(b2);
        }
    }
}
