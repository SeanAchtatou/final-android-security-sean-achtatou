package frink.android;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ProgrammingPreferences extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.programmingpreferences);
    }
}
