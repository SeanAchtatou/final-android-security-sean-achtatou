package frink.android;

public interface VoiceRecognizerService extends AndroidService {
    String listen();
}
