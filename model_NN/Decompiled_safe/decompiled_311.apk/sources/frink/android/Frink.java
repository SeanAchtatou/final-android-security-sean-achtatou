package frink.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Selection;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import frink.expr.Environment;
import frink.gui.InteractiveController;
import frink.gui.InteractiveFields;
import frink.gui.ModeNotImplementedException;
import frink.io.InputItem;
import frink.io.InputManager;
import frink.io.OutputManager;
import java.io.File;
import java.io.OutputStream;

public class Frink extends Activity implements InteractiveFields, OutputManager, InputManager, ActiveTextProvider, View.OnFocusChangeListener, FontChangedListener {
    private static final String FRINK_ROOT = "frink";
    private static final String INTERACTIVE_FONT_SIZE_KEY = "INTERACTIVE_FONT_SIZE_KEY";
    private static final int MENU_GROUP_FILE = 0;
    private static final int MENU_GROUP_HELP = 2;
    private static final int MENU_GROUP_MODE = 1;
    private static final int MENU_ID_ANDROID_DOCS = 7;
    private static final int MENU_ID_CONVERT = 3;
    private static final int MENU_ID_DOCS = 6;
    private static final int MENU_ID_DONATE = 9;
    private static final int MENU_ID_EXIT = 1;
    private static final int MENU_ID_FONT = 10;
    private static final int MENU_ID_HELP = 5;
    private static final int MENU_ID_MODE = 2;
    private static final int MENU_ID_PROGRAMMING = 4;
    private static final int MENU_ID_WHATS_NEW = 8;
    public static final int MESSAGE_FONT_SIZE_CHANGED = 101;
    public static final int MESSAGE_OUTPUT = 100;
    private EditText activeField;
    private MenuItem androidDocsMenuItem;
    /* access modifiers changed from: private */
    public InteractiveController controller;
    private MenuItem convertMenuItem;
    private MenuItem docsMenuItem;
    private MenuItem donateMenuItem;
    private MenuItem exitMenuItem;
    private MenuItem fontMenuItem;
    private int fontSize = 10;
    private EditText fromField;
    private TextView fromLabel;
    private TableRow fromRow;
    private final Handler handler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public void handleMessage(Message m) {
            switch (m.what) {
                case 0:
                    Frink.this.doAppendInput((String) m.obj);
                    return;
                case 1:
                    Frink.this.doAppendOutput((String) m.obj);
                    return;
                case 3:
                    Frink.this.doSetFromText((String) m.obj);
                    return;
                case 4:
                    Frink.this.doSetToText((String) m.obj);
                    return;
                case 100:
                    Frink.this.doOutput((String) m.obj);
                    return;
                case 101:
                    Frink.this.doFontSizeChanged(m.arg1);
                    return;
                default:
                    Frink.this.doAppendOutput("Unhandled message " + m.what);
                    return;
            }
        }
    };
    private MenuItem helpMenuItem;
    /* access modifiers changed from: private */
    public AndroidInputManager inputMgr;
    private TableLayout inputTableLayout;
    private MenuItem modeMenuItem;
    /* access modifiers changed from: private */
    public TextView output;
    private MenuItem programmingMenuItem;
    /* access modifiers changed from: private */
    public ScrollView sv;
    private EditText toField;
    private TextView toLabel;
    private TableRow toRow;
    private LinearLayout topLayout;
    private MenuItem whatsNewMenuItem;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.controller = new InteractiveController(0, this, true);
        initGUI();
        Environment env = this.controller.getInterpreter().getEnvironment();
        File extdir = android.os.Environment.getExternalStorageDirectory();
        if (extdir != null) {
            env.getIncludeManager().appendPath(new File(extdir, FRINK_ROOT).toString());
        }
        env.setOutputManager(this);
        env.setInputManager(this);
        env.getFunctionManager().addFunctionSource(new AndroidFunctionSource(this, new AndroidServiceManager(this)), false);
        new AndroidViewFactory(this, env);
        env.getGraphicsViewFactory().setDefaultConstructorName(AndroidViewFactory.OPEN_IN_ALERT);
        this.controller.parseArguments(new String[0]);
    }

    private void initGUI() {
        this.topLayout = new LinearLayout(this);
        this.topLayout.setOrientation(1);
        this.topLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.sv = new ScrollView(this);
        this.sv.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.output = new TextView(this);
        this.output.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.fontSize = getPreferences(0).getInt(INTERACTIVE_FONT_SIZE_KEY, 10);
        this.output.setTypeface(Typeface.MONOSPACE);
        this.output.setTextSize(2, (float) this.fontSize);
        this.output.setText("Frink\nCopyright 2000-2011\nAlan Eliasen, eliasen@mindspring.com\nhttp://futureboy.us/frinkdocs/\nUse up/down arrows to repeat or modify calculations.\n");
        Linkify.addLinks(this.output, 1);
        this.sv.addView(this.output);
        this.fromField = new EditText(this);
        this.activeField = this.fromField;
        this.fromField.setTypeface(Typeface.MONOSPACE);
        this.fromField.setTextSize(2, (float) this.fontSize);
        this.fromField.setHorizontallyScrolling(true);
        this.toField = new EditText(this);
        this.toField.setTypeface(Typeface.MONOSPACE);
        this.toField.setTextSize(2, (float) this.fontSize);
        this.toField.setHorizontallyScrolling(true);
        View.OnKeyListener keyListener = new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == 0 && keyCode == 66) {
                    Frink.this.controller.performCalculation(Frink.this.getFromText(), Frink.this.getToText());
                    return true;
                } else if (event.getAction() == 0 && keyCode == 19) {
                    Frink.this.controller.historyBack();
                    return true;
                } else if (event.getAction() == 0 && keyCode == 20) {
                    Frink.this.controller.historyForward();
                    return true;
                } else if (event.getAction() != 0 || keyCode != 23) {
                    return false;
                } else {
                    Frink.this.controller.performCalculation(Frink.this.getFromText(), Frink.this.getToText());
                    return true;
                }
            }
        };
        this.fromField.setOnKeyListener(keyListener);
        this.toField.setOnKeyListener(keyListener);
        this.fromField.setOnFocusChangeListener(this);
        this.toField.setOnFocusChangeListener(this);
        this.fromLabel = new TextView(this);
        this.fromLabel.setText("From:");
        this.toLabel = new TextView(this);
        this.toLabel.setText("To:");
        this.inputTableLayout = new TableLayout(this);
        this.fromRow = new TableRow(this);
        this.fromRow.addView(this.fromLabel);
        this.fromRow.addView(this.fromField);
        this.toRow = new TableRow(this);
        this.toRow.addView(this.toLabel);
        this.toRow.addView(this.toField);
        this.inputTableLayout.addView(this.fromRow);
        this.inputTableLayout.addView(this.toRow);
        this.inputTableLayout.setColumnShrinkable(0, true);
        this.inputTableLayout.setColumnStretchable(1, true);
        this.topLayout.addView(this.sv);
        this.topLayout.addView(this.inputTableLayout);
        HorizontalScrollView hScroll = new HorizontalScrollView(this);
        hScroll.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        hScroll.addView(new FrinkButtons(this, this, this.controller));
        hScroll.setFillViewport(true);
        this.topLayout.addView(hScroll);
        setContentView(this.topLayout);
        this.inputMgr = new AndroidInputManager(this);
    }

    public void appendInput(String str) {
        Message m = Message.obtain(this.handler, 0);
        m.obj = str;
        this.handler.sendMessage(m);
    }

    /* access modifiers changed from: private */
    public void doAppendInput(String str) {
        this.output.append(str);
        scrollOutput();
    }

    public void appendOutput(String str) {
        Message m = Message.obtain(this.handler, 1);
        m.obj = str;
        this.handler.sendMessage(m);
    }

    /* access modifiers changed from: private */
    public void doAppendOutput(String str) {
        this.output.append(str);
        scrollOutput();
    }

    public void output(String str) {
        Message m = Message.obtain(this.handler, 100);
        m.obj = str;
        this.handler.sendMessage(m);
    }

    public void outputln(String str) {
        output(str + "\n");
    }

    /* access modifiers changed from: private */
    public void doOutput(String str) {
        this.output.append(str);
        scrollOutput();
    }

    public OutputStream getRawOutputStream() {
        return null;
    }

    private void scrollOutput() {
        this.sv.post(new Runnable() {
            public void run() {
                Frink.this.sv.smoothScrollTo(0, Frink.this.output.getHeight());
            }
        });
    }

    public void setInteractiveRunning(boolean runStatus) {
    }

    public EditText getActiveFromField() {
        return this.fromField;
    }

    public void setFromText(String text) {
        Message m = Message.obtain(this.handler, 3);
        m.obj = text;
        this.handler.sendMessage(m);
    }

    public void doSetFromText(String text) {
        getActiveFromField().setText(text);
        Editable e = getActiveFromField().getText();
        Selection.setSelection(e, e.length());
    }

    public void setToText(String text) {
        Message m = Message.obtain(this.handler, 4);
        m.obj = text;
        this.handler.sendMessage(m);
    }

    public void doSetToText(String text) {
        this.toField.setText(text);
        Editable e = this.toField.getText();
        Selection.setSelection(e, e.length());
    }

    public String getFromText() {
        return getActiveFromField().getText().toString();
    }

    public String getToText() {
        return this.toField.getText().toString();
    }

    public void setWidth(int width) {
    }

    public void setHeight(int height) {
    }

    public void setFocus() {
        this.fromField.requestFocus();
    }

    public void doLoadFile(File f) {
    }

    public void modeChangeRequested(int prevMode, int newMode) throws ModeNotImplementedException {
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.exitMenuItem = menu.add(0, 1, 0, "Exit Frink");
        this.exitMenuItem.setIcon(17301560);
        this.fontMenuItem = menu.add(0, 10, 0, "Font size...");
        this.fontMenuItem.setIcon(17301593);
        SubMenu moreMenu = menu.addSubMenu(1, 2, 0, "Mode...");
        this.convertMenuItem = moreMenu.add(1, 3, 0, "Convert");
        this.programmingMenuItem = moreMenu.add(1, 4, 0, "Programming");
        SubMenu helpMenu = menu.addSubMenu(2, 5, 0, "Docs...");
        helpMenu.setIcon(17301568);
        this.docsMenuItem = helpMenu.add(0, 6, 0, "Frink Documentation");
        this.androidDocsMenuItem = helpMenu.add(0, 7, 0, "Frink on Android");
        this.donateMenuItem = helpMenu.add(0, 9, 0, "Donate");
        this.whatsNewMenuItem = helpMenu.add(0, 8, 0, "What's New in Frink");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                finish();
                return true;
            case 2:
            case 5:
            default:
                return false;
            case 3:
                this.controller.requestModeChange(0);
                return true;
            case 4:
                startProgrammingMode();
                return true;
            case 6:
                launchDocs();
                return true;
            case 7:
                launchAndroidDocs();
                return true;
            case 8:
                launchWhatsNew();
                return true;
            case 9:
                launchDonate();
                return true;
            case 10:
                selectFont();
                return true;
        }
    }

    private void startProgrammingMode() {
        startActivity(new Intent(this, ProgrammingActivity.class));
    }

    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putInt(INTERACTIVE_FONT_SIZE_KEY, this.fontSize);
        editor.commit();
        this.controller.suspend();
    }

    public void onStop() {
        super.onStop();
        this.controller.suspend();
    }

    public void onResume() {
        super.onResume();
        this.controller.resume();
    }

    public void onDestroy() {
        super.onDestroy();
        this.controller.interrupt(false);
    }

    public EditText getActiveField() {
        return this.activeField;
    }

    public void onFocusChange(View v, boolean hasFocus) {
        if (1 != 0 && (v instanceof EditText)) {
            this.activeField = (EditText) v;
        }
    }

    private void launchDocs() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/")));
    }

    private void launchAndroidDocs() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/android.html")));
    }

    private void launchDonate() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/donate.html")));
    }

    private void launchWhatsNew() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://futureboy.us/frinkdocs/whatsnew.html")));
    }

    public String input(String prompt, String defaultValue, Environment env) {
        final StringHolder sh = new StringHolder();
        final String str = prompt;
        final String str2 = defaultValue;
        final Environment environment = env;
        Runnable r = new Runnable() {
            public void run() {
                Frink.this.inputMgr.input(str, str2, environment, Frink.this.controller.getInterpreter(), sh);
            }
        };
        synchronized (this.controller.getInterpreter()) {
            try {
                runOnUiThread(r);
                this.controller.getInterpreter().wait();
            } catch (InterruptedException e) {
                outputln("Interrupted when waiting for input:\n" + e);
            }
        }
        return sh.value;
    }

    public String[] input(String mainPrompt, InputItem[] fields, Environment env) {
        final MultiStringHolder msh = new MultiStringHolder();
        final String str = mainPrompt;
        final InputItem[] inputItemArr = fields;
        final Environment environment = env;
        Runnable r = new Runnable() {
            public void run() {
                Frink.this.inputMgr.input(str, inputItemArr, environment, Frink.this.controller.getInterpreter(), msh);
            }
        };
        synchronized (this.controller.getInterpreter()) {
            try {
                runOnUiThread(r);
                this.controller.getInterpreter().wait();
            } catch (InterruptedException e) {
                outputln("Interrupted when waiting for input:\n" + e);
            }
        }
        return msh.values;
    }

    public void fontSizeChanged(int newSize) {
        Message m = Message.obtain(this.handler, 101);
        m.arg1 = newSize;
        this.handler.sendMessage(m);
    }

    /* access modifiers changed from: private */
    public void doFontSizeChanged(int newSize) {
        if (this.fontSize != newSize) {
            this.output.setTextSize(2, (float) newSize);
            this.fromField.setTextSize(2, (float) newSize);
            this.toField.setTextSize(2, (float) newSize);
            this.fontSize = newSize;
        }
    }

    public void selectFont() {
        FontSelectorView.showInAlert(this.fontSize, this, this);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(INTERACTIVE_FONT_SIZE_KEY)) {
            fontSizeChanged(savedInstanceState.getInt(INTERACTIVE_FONT_SIZE_KEY));
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(INTERACTIVE_FONT_SIZE_KEY, this.fontSize);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(this.topLayout);
    }
}
