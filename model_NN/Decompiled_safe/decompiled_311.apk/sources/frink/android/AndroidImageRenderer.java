package frink.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import frink.graphics.BasicFrinkColor;
import frink.graphics.BoundingBox;
import frink.graphics.DrawableModifiedListener;
import frink.graphics.GraphicsUtils;
import frink.graphics.GraphicsView;
import frink.graphics.ImageFileArguments;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.units.Unit;
import frink.units.UnitMath;
import java.io.File;

public class AndroidImageRenderer implements DrawableModifiedListener {
    private Environment env;
    private String format;
    private int height;
    private Bitmap image;
    private Object outTo;
    private boolean renderBackgroundTransparent;
    private AndroidGraphicsRenderer renderer;
    private int width;

    public AndroidImageRenderer(Environment environment, ImageFileArguments imageFileArguments) throws FrinkSecurityException {
        this(environment, imageFileArguments.width, imageFileArguments.height, imageFileArguments.outTo, imageFileArguments.format, imageFileArguments.backgroundTransparent);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private AndroidImageRenderer(Environment environment, Unit unit, Unit unit2, Object obj, String str, boolean z) throws FrinkSecurityException {
        File file;
        this.env = environment;
        this.renderBackgroundTransparent = z;
        try {
            this.width = UnitMath.getIntegerValue(unit);
            this.height = UnitMath.getIntegerValue(unit2);
        } catch (NotAnIntegerException e) {
            environment.outputln("ImageRenderer:  Invalid dimensions passed in.  Defaulting.");
            this.width = 640;
            this.height = 480;
        }
        Unit unit3 = GraphicsUtils.get72PerInch(environment);
        this.image = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(this.image);
        this.renderer = new AndroidGraphicsRenderer(null, environment, unit3, this);
        this.renderer.setCanvas(canvas);
        try {
            this.renderer.setBoundingBox(new BoundingBox(0, 0, this.width, this.height));
        } catch (ConformanceException e2) {
            environment.outputln("AndroidImageRenderer:\n  " + e2);
        } catch (NumericException e3) {
            environment.outputln("AndroidImageRenderer:\n  " + e3);
        } catch (OverlapException e4) {
            environment.outputln("AndroidImageRenderer:\n  " + e4);
        }
        if (obj instanceof File) {
            File possiblyFixRelativePath = GraphicsUtils.possiblyFixRelativePath((File) obj);
            environment.getSecurityHelper().checkWrite(possiblyFixRelativePath);
            file = possiblyFixRelativePath;
        } else {
            file = obj;
        }
        this.outTo = file;
        this.format = str;
        if (str == null) {
            this.format = GraphicsUtils.guessFormat(file, environment);
        }
        if (!z) {
            canvas.drawColor(BasicFrinkColor.WHITE.getPacked());
        }
    }

    public GraphicsView getGraphicsView() {
        return this.renderer;
    }

    public void drawableModified() {
        doRender();
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a7 A[SYNTHETIC, Splitter:B:32:0x00a7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doRender() {
        /*
            r7 = this;
            java.lang.String r5 = "AndroidImageRenderer.doRender:  IO error on close:\n  "
            frink.android.AndroidGraphicsRenderer r0 = r7.renderer
            r0.paintRequested()
            r0 = 0
            java.lang.Object r1 = r7.outTo
            boolean r1 = r1 instanceof java.io.OutputStream
            if (r1 == 0) goto L_0x00cf
            r1 = r0
        L_0x000f:
            java.lang.Object r0 = r7.outTo     // Catch:{ IOException -> 0x00ca }
            boolean r0 = r0 instanceof java.io.File     // Catch:{ IOException -> 0x00ca }
            if (r0 == 0) goto L_0x00cc
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00ca }
            java.lang.Object r0 = r7.outTo     // Catch:{ IOException -> 0x00ca }
            java.io.File r0 = (java.io.File) r0     // Catch:{ IOException -> 0x00ca }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00ca }
            r0 = r2
        L_0x001f:
            java.lang.String r1 = r7.format     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            java.lang.String r2 = "jpg"
            boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            if (r1 == 0) goto L_0x002d
            java.lang.String r1 = "JPEG"
            r7.format = r1     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
        L_0x002d:
            if (r0 != 0) goto L_0x003c
            frink.expr.Environment r1 = r7.env     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            java.lang.String r2 = "ImageRenderer: Got null output stream."
            r1.outputln(r2)     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
        L_0x0036:
            if (r0 == 0) goto L_0x003b
            r0.close()     // Catch:{ IOException -> 0x008a }
        L_0x003b:
            return
        L_0x003c:
            android.graphics.Bitmap r1 = r7.image     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            java.lang.String r2 = r7.format     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            java.lang.String r2 = r2.toUpperCase()     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.valueOf(r2)     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            r3 = 90
            r1.compress(r2, r3, r0)     // Catch:{ IOException -> 0x004e, all -> 0x00c5 }
            goto L_0x0036
        L_0x004e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0052:
            frink.expr.Environment r2 = r7.env     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a4 }
            r3.<init>()     // Catch:{ all -> 0x00a4 }
            java.lang.String r4 = "AndroidImageRenderer.doRender:  IO error:\n  "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00a4 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a4 }
            r2.outputln(r0)     // Catch:{ all -> 0x00a4 }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x003b
        L_0x0070:
            r0 = move-exception
            frink.expr.Environment r1 = r7.env
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "AndroidImageRenderer.doRender:  IO error on close:\n  "
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x003b
        L_0x008a:
            r0 = move-exception
            frink.expr.Environment r1 = r7.env
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "AndroidImageRenderer.doRender:  IO error on close:\n  "
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x003b
        L_0x00a4:
            r0 = move-exception
        L_0x00a5:
            if (r1 == 0) goto L_0x00aa
            r1.close()     // Catch:{ IOException -> 0x00ab }
        L_0x00aa:
            throw r0
        L_0x00ab:
            r1 = move-exception
            frink.expr.Environment r2 = r7.env
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "AndroidImageRenderer.doRender:  IO error on close:\n  "
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.outputln(r1)
            goto L_0x00aa
        L_0x00c5:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00a5
        L_0x00ca:
            r0 = move-exception
            goto L_0x0052
        L_0x00cc:
            r0 = r1
            goto L_0x001f
        L_0x00cf:
            r1 = r0
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.android.AndroidImageRenderer.doRender():void");
    }
}
