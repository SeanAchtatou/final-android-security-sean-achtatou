package frink.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.graphics.BasicFrinkColor;
import frink.graphics.BoundingBox;
import frink.graphics.Coord;
import frink.graphics.DrawableModifiedListener;
import frink.graphics.EllipticalArcPathSegment;
import frink.graphics.FrinkColor;
import frink.graphics.FrinkGeneralPath;
import frink.graphics.FrinkImage;
import frink.graphics.FrinkPoint;
import frink.graphics.FrinkPointList;
import frink.graphics.GraphicsView;
import frink.graphics.PaintRequestListener;
import frink.graphics.PathSegment;
import frink.graphics.PrintRequestListener;
import frink.numeric.NumericException;
import frink.units.Unit;
import frink.units.UnitMath;

public class AndroidGraphicsRenderer implements GraphicsView {
    private FrinkColor background = null;
    private BoundingBox bbox = null;
    private Canvas canvas;
    private GraphicsView child = null;
    private FrinkColor color = null;
    private DrawableModifiedListener dmListener;
    private Paint drawPaint;
    private RectF ellipse;
    private Paint fillPaint;
    private PaintRequestListener paintListener = null;
    private GraphicsView parent = null;
    private PrintRequestListener printListener = null;
    private Unit resolution;

    public AndroidGraphicsRenderer(Context context, Environment environment, Unit unit, DrawableModifiedListener drawableModifiedListener) {
        this.dmListener = drawableModifiedListener;
        this.ellipse = new RectF();
        this.drawPaint = new Paint(1);
        this.drawPaint.setStyle(Paint.Style.STROKE);
        this.drawPaint.setColor(-16777216);
        this.drawPaint.setStrokeCap(Paint.Cap.BUTT);
        this.drawPaint.setStrokeJoin(Paint.Join.MITER);
        this.fillPaint = new Paint(1);
        this.fillPaint.setStyle(Paint.Style.FILL);
        this.fillPaint.setColor(-16777216);
        this.resolution = unit;
    }

    public void setCanvas(Canvas canvas2) {
        this.canvas = canvas2;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.bbox = boundingBox;
    }

    public void setPaintRequestListener(PaintRequestListener paintRequestListener) {
        this.paintListener = paintRequestListener;
    }

    public PaintRequestListener getPaintRequestListener() {
        return this.paintListener;
    }

    public void setPrintRequestListener(PrintRequestListener printRequestListener) {
        this.printListener = printRequestListener;
    }

    public PrintRequestListener getPrintRequestListener() {
        return this.printListener;
    }

    public GraphicsView getParentView() {
        return this.parent;
    }

    public GraphicsView getChildView() {
        return this.child;
    }

    public void setParentView(GraphicsView graphicsView) {
        this.parent = graphicsView;
        if (this.parent.getChildView() != this) {
            this.parent.setChildView(this);
        }
    }

    public void setChildView(GraphicsView graphicsView) {
        this.child = graphicsView;
        if (this.child.getParentView() != this) {
            this.child.setParentView(this);
        }
    }

    public void drawLine(Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                this.canvas.drawLine(Coord.getFloatCoord(unit, width, deviceResolution), Coord.getFloatCoord(unit2, height, deviceResolution), Coord.getFloatCoord(unit3, width, deviceResolution), Coord.getFloatCoord(unit4, height, deviceResolution), this.drawPaint);
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.drawLine:  NumericException:\n  " + e);
            }
        }
    }

    public void drawRectangle(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                Unit add = UnitMath.add(unit, unit3);
                Unit add2 = UnitMath.add(unit2, unit4);
                float floatCoord = Coord.getFloatCoord(unit, width, deviceResolution);
                float floatCoord2 = Coord.getFloatCoord(unit2, height, deviceResolution);
                float floatCoord3 = Coord.getFloatCoord(add, width, deviceResolution);
                float floatCoord4 = Coord.getFloatCoord(add2, height, deviceResolution);
                if (z) {
                    this.canvas.drawRect(floatCoord, floatCoord2, floatCoord3, floatCoord4, this.fillPaint);
                } else {
                    this.canvas.drawRect(floatCoord, floatCoord2, floatCoord3, floatCoord4, this.drawPaint);
                }
            } catch (ConformanceException e) {
                System.err.println("AndroidGraphicsView.drawRectangle:  ConformanceException:\n  " + e);
            } catch (NumericException e2) {
                System.err.println("AndroidGraphicsView.drawRectangle:  NumericException:\n  " + e2);
            }
        }
    }

    public void drawEllipse(Unit unit, Unit unit2, Unit unit3, Unit unit4, boolean z) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                synchronized (this.ellipse) {
                    this.ellipse.set(Coord.getFloatCoord(unit, width, deviceResolution), Coord.getFloatCoord(unit2, height, deviceResolution), Coord.getFloatCoord(UnitMath.add(unit, unit3), width, deviceResolution), Coord.getFloatCoord(UnitMath.add(unit2, unit4), height, deviceResolution));
                    if (z) {
                        this.canvas.drawOval(this.ellipse, this.fillPaint);
                    } else {
                        this.canvas.drawOval(this.ellipse, this.drawPaint);
                    }
                }
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.drawEllipse:  NumericException:\n  " + e);
            } catch (ConformanceException e2) {
                System.err.println("AndroidGraphicsView.drawEllipse:  ConformanceException:\n  " + e2);
            }
        }
    }

    public void drawPoly(FrinkPointList frinkPointList, boolean z, boolean z2) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                int pointCount = frinkPointList.getPointCount();
                if (pointCount >= 2) {
                    Path path = new Path();
                    FrinkPoint point = frinkPointList.getPoint(0);
                    path.moveTo(Coord.getFloatCoord(point.getX(), width, deviceResolution), Coord.getFloatCoord(point.getY(), height, deviceResolution));
                    for (int i = 1; i < pointCount; i++) {
                        FrinkPoint point2 = frinkPointList.getPoint(i);
                        path.lineTo(Coord.getFloatCoord(point2.getX(), width, deviceResolution), Coord.getFloatCoord(point2.getY(), height, deviceResolution));
                    }
                    if (z) {
                        path.close();
                    }
                    if (z2) {
                        this.canvas.drawPath(path, this.fillPaint);
                    } else {
                        this.canvas.drawPath(path, this.drawPaint);
                    }
                }
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.drawPoly:  NumericException:\n  " + e);
            }
        }
    }

    public void drawGeneralPath(FrinkGeneralPath frinkGeneralPath, boolean z) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                int segmentCount = frinkGeneralPath.getSegmentCount();
                Path path = new Path();
                path.setFillType(Path.FillType.EVEN_ODD);
                for (int i = 0; i < segmentCount; i++) {
                    PathSegment segment = frinkGeneralPath.getSegment(i);
                    String segmentType = segment.getSegmentType();
                    if (segmentType == PathSegment.TYPE_LINE) {
                        FrinkPoint point = segment.getPoint(0);
                        path.lineTo(Coord.getFloatCoord(point.getX(), width, deviceResolution), Coord.getFloatCoord(point.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_MOVE_TO) {
                        FrinkPoint point2 = segment.getPoint(0);
                        path.moveTo(Coord.getFloatCoord(point2.getX(), width, deviceResolution), Coord.getFloatCoord(point2.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_QUADRATIC) {
                        FrinkPoint point3 = segment.getPoint(0);
                        FrinkPoint point4 = segment.getPoint(1);
                        path.quadTo(Coord.getFloatCoord(point3.getX(), width, deviceResolution), Coord.getFloatCoord(point3.getY(), height, deviceResolution), Coord.getFloatCoord(point4.getX(), width, deviceResolution), Coord.getFloatCoord(point4.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_CUBIC) {
                        FrinkPoint point5 = segment.getPoint(0);
                        FrinkPoint point6 = segment.getPoint(1);
                        FrinkPoint point7 = segment.getPoint(2);
                        path.cubicTo(Coord.getFloatCoord(point5.getX(), width, deviceResolution), Coord.getFloatCoord(point5.getY(), height, deviceResolution), Coord.getFloatCoord(point6.getX(), width, deviceResolution), Coord.getFloatCoord(point6.getY(), height, deviceResolution), Coord.getFloatCoord(point7.getX(), width, deviceResolution), Coord.getFloatCoord(point7.getY(), height, deviceResolution));
                    } else if (segmentType == PathSegment.TYPE_ELLIPSE) {
                        FrinkPoint point8 = segment.getPoint(0);
                        FrinkPoint point9 = segment.getPoint(1);
                        path.addOval(new RectF(Coord.getFloatCoord(point8.getX(), width, deviceResolution), Coord.getFloatCoord(point8.getY(), height, deviceResolution), Coord.getFloatCoord(point9.getX(), width, deviceResolution), Coord.getFloatCoord(point9.getY(), height, deviceResolution)), Path.Direction.CCW);
                    } else if (segmentType == PathSegment.TYPE_ELLIPTICAL_ARC) {
                        FrinkPoint point10 = segment.getPoint(2);
                        FrinkPoint point11 = segment.getPoint(3);
                        Unit x = point10.getX();
                        Unit y = point10.getY();
                        Unit x2 = point11.getX();
                        Unit y2 = point11.getY();
                        EllipticalArcPathSegment.ArcData arcData = (EllipticalArcPathSegment.ArcData) segment.getExtraData();
                        path.arcTo(new RectF(Coord.getFloatCoord(x, width, deviceResolution), Coord.getFloatCoord(y, height, deviceResolution), Coord.getFloatCoord(x2, width, deviceResolution), Coord.getFloatCoord(y2, height, deviceResolution)), -((float) Math.toDegrees(arcData.startAngle)), -((float) Math.toDegrees(arcData.angularExtent)), false);
                    } else if (segmentType == PathSegment.TYPE_CLOSE) {
                        path.close();
                    } else {
                        System.err.println("AndroidGraphicsView.drawGeneralPath:  unhandled segment type " + segmentType);
                    }
                }
                if (z) {
                    this.canvas.drawPath(path, this.fillPaint);
                } else {
                    this.canvas.drawPath(path, this.drawPaint);
                }
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.drawGeneralPath:  NumericException:\n  " + e);
            }
        }
    }

    public void drawImage(FrinkImage frinkImage, Unit unit, Unit unit2, Unit unit3, Unit unit4) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                this.canvas.drawBitmap((Bitmap) frinkImage.getImage(), new Rect(0, 0, frinkImage.getWidth(), frinkImage.getHeight()), new RectF(Coord.getFloatCoord(unit, width, deviceResolution), Coord.getFloatCoord(unit2, height, deviceResolution), Coord.getFloatCoord(UnitMath.add(unit, unit3), width, deviceResolution), Coord.getFloatCoord(UnitMath.add(unit2, unit4), height, deviceResolution)), this.fillPaint);
            } catch (ConformanceException e) {
                System.err.println("AndroidGraphicsView.drawImage:  ConformanceException:\n  " + e);
            } catch (NumericException e2) {
                System.err.println("AndroidGraphicsView.drawImage:  NumericException:\n  " + e2);
            }
        }
    }

    public FrinkColor getColor() {
        if (this.color != null) {
            return this.color;
        }
        if (this.canvas != null) {
            return new BasicFrinkColor(this.fillPaint.getColor());
        }
        return BasicFrinkColor.BLACK;
    }

    public void setColor(FrinkColor frinkColor) {
        this.color = frinkColor;
        this.fillPaint.setColor(frinkColor.getPacked());
        this.drawPaint.setColor(frinkColor.getPacked());
    }

    public FrinkColor getBackgroundColor() {
        return this.background;
    }

    public void setBackgroundColor(FrinkColor frinkColor) {
        this.background = frinkColor;
        if (this.canvas != null) {
            this.canvas.drawColor(frinkColor.getPacked());
        }
    }

    public void setFont(String str, int i, Unit unit) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            try {
                float floatCoord = Coord.getFloatCoord(unit, rendererBoundingBox.getHeight(), getDeviceResolution());
                this.fillPaint.setTypeface(Typeface.create(str, i));
                this.fillPaint.setTextSize(floatCoord);
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.setFont:  NumericException:\n  " + e);
            }
        }
    }

    public void setStroke(Unit unit) {
        Unit unit2;
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            if (!UnitMath.areConformal(height, unit)) {
                unit2 = rendererBoundingBox.getWidth();
            } else {
                unit2 = height;
            }
            try {
                this.drawPaint.setStrokeWidth(Coord.getFloatCoord(unit, unit2, getDeviceResolution()));
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.setStroke:  NumericException:\n  " + e);
            }
        }
    }

    public void drawText(String str, Unit unit, Unit unit2, int i, int i2) {
        BoundingBox rendererBoundingBox = getRendererBoundingBox();
        if (rendererBoundingBox != null) {
            Unit height = rendererBoundingBox.getHeight();
            Unit width = rendererBoundingBox.getWidth();
            Unit deviceResolution = getDeviceResolution();
            try {
                float floatCoord = Coord.getFloatCoord(unit, width, deviceResolution);
                float floatCoord2 = Coord.getFloatCoord(unit2, height, deviceResolution);
                float measureText = this.fillPaint.measureText(str);
                Paint.FontMetrics fontMetrics = this.fillPaint.getFontMetrics();
                float f = fontMetrics.ascent;
                float f2 = fontMetrics.descent;
                switch (i) {
                    case 1:
                        break;
                    case 2:
                        floatCoord -= measureText;
                        break;
                    default:
                        floatCoord -= measureText / 2.0f;
                        break;
                }
                switch (i2) {
                    case 1:
                        floatCoord2 = (floatCoord2 - f) - f2;
                        break;
                    case 2:
                        floatCoord2 -= f2;
                        break;
                    case 3:
                        break;
                    default:
                        floatCoord2 += ((-f) - f2) / 2.0f;
                        break;
                }
                this.canvas.drawText(str, floatCoord, floatCoord2, this.fillPaint);
            } catch (NumericException e) {
                System.err.println("AndroidGraphicsView.drawText:  NumericException:\n  " + e);
            }
        }
    }

    public BoundingBox getRendererBoundingBox() {
        return this.bbox;
    }

    public BoundingBox getDrawableBoundingBox() {
        if (this.parent != null) {
            return this.parent.getDrawableBoundingBox();
        }
        if (this.paintListener != null) {
            return this.paintListener.getDrawableBoundingBox();
        }
        System.err.println("AndroidGraphicsView.getDrawableBoundingBox: error: No parent object!");
        return null;
    }

    public Unit getDeviceResolution() {
        return this.resolution;
    }

    public void paintRequested() {
        if (this.parent != null) {
            this.parent.paintRequested();
        }
        callPaintListeners();
    }

    public void printRequested() {
        if (this.parent != null) {
            this.parent.printRequested();
        }
        callPrintListeners();
    }

    public void drawableModified() {
        if (this.child != null) {
            this.child.drawableModified();
        }
        this.dmListener.drawableModified();
    }

    public void rendererResized() {
        if (this.parent != null) {
            this.parent.rendererResized();
        }
    }

    private void callPaintListeners() {
        if (this.paintListener != null) {
            this.paintListener.paintRequested(this);
        }
    }

    private void callPrintListeners() {
        if (this.printListener != null) {
            this.printListener.printRequested();
        }
    }
}
