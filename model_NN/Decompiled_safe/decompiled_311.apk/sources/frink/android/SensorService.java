package frink.android;

public interface SensorService extends AndroidService {
    float[] getRawValues();

    String getSensorName();

    int getType();
}
