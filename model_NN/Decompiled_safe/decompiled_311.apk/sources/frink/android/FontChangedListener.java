package frink.android;

public interface FontChangedListener {
    void fontSizeChanged(int i);
}
