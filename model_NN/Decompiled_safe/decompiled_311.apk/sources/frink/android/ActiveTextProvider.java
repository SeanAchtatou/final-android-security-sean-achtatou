package frink.android;

import android.widget.EditText;

public interface ActiveTextProvider {
    EditText getActiveField();
}
