package frink.android;

public interface OutputManagerListener {
    void outputManagerClosed();

    void outputProduced();
}
