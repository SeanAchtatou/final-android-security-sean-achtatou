package frink.android;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import java.util.HashMap;

public class TextToSpeechService implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener, AndroidService, SpeechService {
    private Context context;
    private Object initLocker = new Object();
    private boolean initialized;
    private boolean speechFailed;
    private Object statusLocker = new Object();
    private TextToSpeech tts;
    private int utteranceCount;
    private int utteranceID;

    public TextToSpeechService(Context context2) {
        this.context = context2;
        this.initialized = false;
        this.speechFailed = false;
        this.utteranceCount = 0;
        this.utteranceID = 0;
        this.tts = new TextToSpeech(context2, this);
    }

    public void onInit(int i) {
        synchronized (this.initLocker) {
            if (i == 0) {
                this.initialized = true;
            }
            if (i == -1) {
                this.speechFailed = true;
            }
            this.initLocker.notifyAll();
        }
    }

    public synchronized void speak(String str) {
        if (!this.speechFailed) {
            int i = 0;
            while (i < 10) {
                if (!this.initialized) {
                    i++;
                    try {
                        synchronized (this.initLocker) {
                            this.initLocker.wait(1000);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            }
            if (this.initialized) {
                this.tts.setOnUtteranceCompletedListener(this);
                doSpeak(str, 1);
            }
        }
    }

    public void shutdown() {
        if (this.initialized && this.tts != null) {
            while (true) {
                try {
                    if (getUtteranceCount() > 0 || this.tts.isSpeaking()) {
                        Log.i("Frink Speech", "Shutdown, utterance count = " + getUtteranceCount() + ", isSpeaking = " + this.tts.isSpeaking());
                        synchronized (this.statusLocker) {
                            this.statusLocker.wait(1000);
                        }
                    } else {
                        return;
                    }
                } catch (InterruptedException e) {
                    return;
                } finally {
                    this.tts.shutdown();
                    this.tts = null;
                }
            }
        }
    }

    private synchronized void doSpeak(String str, int i) {
        String addUtterance = addUtterance();
        HashMap hashMap = new HashMap();
        hashMap.put("utteranceId", addUtterance);
        this.tts.speak(str, i, hashMap);
    }

    public void onUtteranceCompleted(String str) {
        synchronized (this.statusLocker) {
            this.utteranceCount--;
            Log.i("Frink Speech", "Finished id " + str);
            if (this.utteranceCount == 0) {
                this.statusLocker.notifyAll();
            }
        }
    }

    public synchronized String addUtterance() {
        this.utteranceCount++;
        this.utteranceID++;
        return Integer.toString(this.utteranceID);
    }

    public synchronized int getUtteranceCount() {
        return this.utteranceCount;
    }
}
