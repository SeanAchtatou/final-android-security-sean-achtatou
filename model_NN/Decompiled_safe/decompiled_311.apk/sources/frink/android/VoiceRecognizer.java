package frink.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import frink.expr.Environment;
import java.util.ArrayList;

public class VoiceRecognizer {
    private static final int VOICE_RECOGNITION_COMPLETE = 1234;
    public Activity activity;
    /* access modifiers changed from: private */
    public Object locker = null;
    /* access modifiers changed from: private */
    public ListView mList;
    /* access modifiers changed from: private */
    public ArrayList<String> matches;
    /* access modifiers changed from: private */
    public StringHolder resultHolder = null;

    public VoiceRecognizer(Activity activity2) {
        this.activity = activity2;
        this.mList = null;
        this.matches = null;
    }

    public void input(String str, String str2, Environment environment, Object obj, StringHolder stringHolder) {
        this.locker = obj;
        this.resultHolder = stringHolder;
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.PROMPT", str);
        this.activity.startActivityForResult(intent, VOICE_RECOGNITION_COMPLETE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.app.AlertDialog.Builder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):android.app.AlertDialog$Builder}
     arg types: [java.lang.String, frink.android.VoiceRecognizer$1]
     candidates:
      ClspMth{android.app.AlertDialog.Builder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):android.app.AlertDialog$Builder}
      ClspMth{android.app.AlertDialog.Builder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):android.app.AlertDialog$Builder} */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != VOICE_RECOGNITION_COMPLETE) {
            return;
        }
        if (i2 == -1) {
            this.matches = intent.getStringArrayListExtra("android.speech.extra.RESULTS");
            this.mList.setAdapter((ListAdapter) new ArrayAdapter(this.activity, 17367043, this.matches));
            AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
            builder.setTitle("Select phrase");
            builder.setCancelable(true);
            builder.setView(this.mList);
            builder.setPositiveButton((CharSequence) "Ok", (DialogInterface.OnClickListener) new 1(this));
            builder.setOnCancelListener(new 2(this));
            builder.show();
            return;
        }
        this.resultHolder.value = "";
        synchronized (this.locker) {
            this.locker.notifyAll();
        }
    }
}
