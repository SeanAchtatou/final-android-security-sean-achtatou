package frink.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.ScrollView;
import frink.expr.Environment;
import frink.io.InputItem;

public class AndroidInputManager {
    public Activity activity;

    public AndroidInputManager(Activity activity2) {
        this.activity = activity2;
    }

    public void input(String prompt, String defaultValue, Environment env, final Object locker, final StringHolder resultHolder) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.activity);
        alert.setTitle("Frink Input");
        if (prompt != null) {
            alert.setMessage(prompt);
        }
        final EditText input = new EditText(this.activity);
        input.setSingleLine();
        if (defaultValue != null) {
            input.setText(defaultValue);
        }
        input.selectAll();
        alert.setCancelable(true);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                synchronized (locker) {
                    resultHolder.value = input.getText().toString();
                    locker.notifyAll();
                }
            }
        });
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                synchronized (locker) {
                    resultHolder.value = "";
                    locker.notifyAll();
                }
            }
        });
        alert.show();
    }

    public void input(String mainPrompt, InputItem[] fields, Environment env, final Object locker, final MultiStringHolder resultHolder) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.activity);
        alert.setTitle("Frink Input");
        final MultiInputList list = new MultiInputList(this.activity, fields, mainPrompt);
        ScrollView scroll = new ScrollView(this.activity);
        scroll.addView(list);
        alert.setCancelable(true);
        alert.setView(scroll);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                synchronized (locker) {
                    resultHolder.values = list.getValues();
                    locker.notifyAll();
                }
            }
        });
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                synchronized (locker) {
                    resultHolder.values = list.getValues();
                    locker.notifyAll();
                }
            }
        });
        alert.show();
    }
}
