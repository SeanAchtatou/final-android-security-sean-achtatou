package frink.android;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import frink.io.OutputManager;
import java.io.OutputStream;

public class AndroidOutputManager extends LinearLayout implements OutputManager, Runnable {
    private static final int MAX_DELAY_TIME = 500;
    public static final int MESSAGE_FONT_SIZE_CHANGED = 101;
    public static final int MESSAGE_OUTPUT = 100;
    private StringBuffer appendBuffer;
    private TableLayout buttonLayout;
    private TableRow buttonRow;
    private Button clearButton;
    private Button closeButton;
    private int delayTime = 30;
    private Handler handler;
    /* access modifiers changed from: private */
    public OutputManagerListener listener = null;
    /* access modifiers changed from: private */
    public ScrollView osv;
    /* access modifiers changed from: private */
    public TextView outputView;

    public AndroidOutputManager(Activity context, OutputManagerListener listener2) {
        super(context);
        this.listener = listener2;
        this.appendBuffer = new StringBuffer();
        initGUI(context);
        Thread t = new Thread(this, "AndroidOutputManager dispatcher");
        t.setPriority(7);
        t.start();
    }

    private void initGUI(Activity context) {
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.outputView = new TextView(context);
        this.outputView.setTypeface(Typeface.MONOSPACE);
        this.outputView.setTextSize(2, 8.0f);
        this.outputView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.outputView.setFreezesText(true);
        this.outputView.setFocusable(true);
        this.osv = new ScrollView(context);
        this.osv.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.osv.addView(this.outputView);
        this.clearButton = new Button(context);
        this.clearButton.setText("Clear");
        this.clearButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AndroidOutputManager.this.outputView.setText("");
            }
        });
        this.closeButton = new Button(context);
        this.closeButton.setText("Close");
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AndroidOutputManager.this.listener != null) {
                    AndroidOutputManager.this.listener.outputManagerClosed();
                }
            }
        });
        this.buttonLayout = new TableLayout(context);
        this.buttonLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.0f));
        this.buttonRow = new TableRow(context);
        this.buttonRow.addView(this.closeButton);
        this.buttonLayout.setColumnStretchable(0, true);
        this.buttonLayout.addView(this.buttonRow);
        addView(this.osv);
        addView(this.buttonLayout);
        this.handler = new Handler() {
            /* Debug info: failed to restart local var, previous not found, register: 3 */
            public void handleMessage(Message m) {
                switch (m.what) {
                    case 100:
                        AndroidOutputManager.this.doOutput((String) m.obj);
                        return;
                    case 101:
                        AndroidOutputManager.this.doFontSizeChanged(m.arg1);
                        return;
                    default:
                        AndroidOutputManager.this.doOutput("Unhandled message " + m.what);
                        return;
                }
            }
        };
    }

    public void output(String str) {
        appendText(str);
    }

    public void outputln(String str) {
        appendText(str + "\n");
    }

    /* access modifiers changed from: private */
    public void doOutput(String str) {
        this.outputView.append(str);
        scrollOutput();
        if (this.listener != null) {
            this.listener.outputProduced();
        }
    }

    public OutputStream getRawOutputStream() {
        return null;
    }

    private void appendText(String str) {
        synchronized (this.appendBuffer) {
            this.appendBuffer.append(str);
            this.appendBuffer.notifyAll();
        }
    }

    private void scrollOutput() {
        this.osv.post(new Runnable() {
            public void run() {
                AndroidOutputManager.this.osv.smoothScrollTo(0, AndroidOutputManager.this.outputView.getHeight());
            }
        });
    }

    public void setOutputManagerListener(OutputManagerListener listener2) {
        this.listener = listener2;
    }

    public void clear() {
        this.outputView.setText("");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        if (r2 == null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        r1 = android.os.Message.obtain(r9.handler, 100);
        r1.obj = r2;
        r9.handler.sendMessage(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        java.lang.Thread.sleep((long) r9.delayTime);
        r9.delayTime *= 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        if (r9.delayTime < 500) goto L_0x0004;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        r9.delayTime = 500;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
        monitor-enter(r9.appendBuffer);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r1 = android.os.Message.obtain(r9.handler, 100);
        r1.obj = r2;
        r9.handler.sendMessage(r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            r8 = 500(0x1f4, float:7.0E-43)
            r7 = 100
        L_0x0004:
            r2 = 0
            java.lang.StringBuffer r4 = r9.appendBuffer
            monitor-enter(r4)
            java.lang.StringBuffer r5 = r9.appendBuffer     // Catch:{ all -> 0x0073 }
            int r5 = r5.length()     // Catch:{ all -> 0x0073 }
            if (r5 != 0) goto L_0x0064
            java.lang.StringBuffer r5 = r9.appendBuffer     // Catch:{ InterruptedException -> 0x0053 }
            r5.wait()     // Catch:{ InterruptedException -> 0x0053 }
        L_0x0015:
            monitor-exit(r4)     // Catch:{ all -> 0x0073 }
            if (r2 == 0) goto L_0x0025
            android.os.Handler r4 = r9.handler
            android.os.Message r1 = android.os.Message.obtain(r4, r7)
            r1.obj = r2
            android.os.Handler r4 = r9.handler
            r4.sendMessage(r1)
        L_0x0025:
            int r4 = r9.delayTime     // Catch:{ InterruptedException -> 0x003a }
            long r4 = (long) r4     // Catch:{ InterruptedException -> 0x003a }
            java.lang.Thread.sleep(r4)     // Catch:{ InterruptedException -> 0x003a }
            int r4 = r9.delayTime     // Catch:{ InterruptedException -> 0x003a }
            int r4 = r4 * 2
            r9.delayTime = r4     // Catch:{ InterruptedException -> 0x003a }
            int r4 = r9.delayTime     // Catch:{ InterruptedException -> 0x003a }
            if (r4 < r8) goto L_0x0004
            r4 = 500(0x1f4, float:7.0E-43)
            r9.delayTime = r4     // Catch:{ InterruptedException -> 0x003a }
            goto L_0x0004
        L_0x003a:
            r4 = move-exception
            r0 = r4
            java.lang.StringBuffer r4 = r9.appendBuffer
            monitor-enter(r4)
            android.os.Handler r5 = r9.handler     // Catch:{ all -> 0x0050 }
            r6 = 100
            android.os.Message r1 = android.os.Message.obtain(r5, r6)     // Catch:{ all -> 0x0050 }
            r1.obj = r2     // Catch:{ all -> 0x0050 }
            android.os.Handler r5 = r9.handler     // Catch:{ all -> 0x0050 }
            r5.sendMessage(r1)     // Catch:{ all -> 0x0050 }
            monitor-exit(r4)     // Catch:{ all -> 0x0050 }
            goto L_0x0004
        L_0x0050:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0050 }
            throw r5
        L_0x0053:
            r5 = move-exception
            r0 = r5
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x0073 }
            java.lang.StringBuffer r5 = r9.appendBuffer     // Catch:{ all -> 0x0073 }
            r3.<init>(r5)     // Catch:{ all -> 0x0073 }
            java.lang.StringBuffer r5 = r9.appendBuffer     // Catch:{ all -> 0x0076 }
            r6 = 0
            r5.setLength(r6)     // Catch:{ all -> 0x0076 }
            r2 = r3
            goto L_0x0015
        L_0x0064:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x0073 }
            java.lang.StringBuffer r5 = r9.appendBuffer     // Catch:{ all -> 0x0073 }
            r3.<init>(r5)     // Catch:{ all -> 0x0073 }
            java.lang.StringBuffer r5 = r9.appendBuffer     // Catch:{ all -> 0x0076 }
            r6 = 0
            r5.setLength(r6)     // Catch:{ all -> 0x0076 }
            r2 = r3
            goto L_0x0015
        L_0x0073:
            r5 = move-exception
        L_0x0074:
            monitor-exit(r4)     // Catch:{ all -> 0x0073 }
            throw r5
        L_0x0076:
            r5 = move-exception
            r2 = r3
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.android.AndroidOutputManager.run():void");
    }

    public void fontSizeChanged(int newSize) {
        Message m = Message.obtain(this.handler, 101);
        m.arg1 = newSize;
        this.handler.sendMessage(m);
    }

    /* access modifiers changed from: private */
    public void doFontSizeChanged(int newSize) {
        this.outputView.setTextSize(2, (float) newSize);
    }
}
