package frink.android;

import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import frink.io.InputItem;

public class MultiInputList extends LinearLayout {
    private Context context;
    private EditText[] inputFields;
    private InputItem[] items;

    public MultiInputList(Context context2, InputItem[] items2, String prompt) {
        super(context2);
        this.context = context2;
        this.items = items2;
        initGUI(context2, items2, prompt);
    }

    private void initGUI(Context context2, InputItem[] items2, String prompt) {
        setOrientation(1);
        int size = items2.length;
        this.inputFields = new EditText[size];
        TextView label = new TextView(context2);
        label.setText(prompt);
        label.setGravity(1);
        addView(label);
        for (int i = 0; i < size; i++) {
            InputItem item = items2[i];
            TextView label2 = new TextView(context2);
            label2.setPadding(4, 2, 0, 0);
            if (item.label != null) {
                label2.setText(item.label);
            }
            label2.setFocusable(false);
            addView(label2);
            EditText inputField = new EditText(context2);
            inputField.setFocusable(true);
            if (item.defaultValue != null) {
                inputField.setText(item.defaultValue);
            }
            addView(inputField);
            this.inputFields[i] = inputField;
        }
    }

    public String[] getValues() {
        int size = this.items.length;
        String[] result = new String[size];
        for (int i = 0; i < size; i++) {
            result[i] = this.inputFields[i].getText().toString();
        }
        return result;
    }
}
