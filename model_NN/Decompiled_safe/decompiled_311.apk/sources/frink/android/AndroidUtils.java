package frink.android;

import android.util.Log;

public class AndroidUtils {
    private static boolean checkedRecognizerClass = false;
    private static boolean checkedSensorClass = false;
    private static boolean checkedTTSClass = false;
    private static Class recognizerClass = null;
    private static Class sensorClass = null;
    private static Class ttsClass = null;

    public static synchronized boolean hasTTS() {
        boolean z;
        synchronized (AndroidUtils.class) {
            if (ttsClass != null) {
                z = true;
            } else if (checkedTTSClass) {
                z = false;
            } else {
                try {
                    ttsClass = Class.forName("android.speech.tts.TextToSpeech");
                    checkedTTSClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    Log.i("Speech", e.toString());
                    checkedTTSClass = true;
                    z = false;
                }
            }
        }
        return z;
    }

    public static synchronized boolean hasRecognizer() {
        boolean z;
        synchronized (AndroidUtils.class) {
            if (recognizerClass != null) {
                z = true;
            } else if (checkedRecognizerClass) {
                z = false;
            } else {
                try {
                    recognizerClass = Class.forName("android.speech.RecognizerIntent");
                    checkedRecognizerClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    Log.i("Speech", e.toString());
                    checkedRecognizerClass = true;
                    z = false;
                }
            }
        }
        return z;
    }

    public static synchronized boolean hasSensor() {
        boolean z;
        synchronized (AndroidUtils.class) {
            if (sensorClass != null) {
                z = true;
            } else if (checkedSensorClass) {
                z = false;
            } else {
                try {
                    sensorClass = Class.forName("android.hardware.Sensor");
                    checkedSensorClass = true;
                    z = true;
                } catch (ClassNotFoundException e) {
                    checkedSensorClass = true;
                    z = false;
                }
            }
        }
        return z;
    }
}
