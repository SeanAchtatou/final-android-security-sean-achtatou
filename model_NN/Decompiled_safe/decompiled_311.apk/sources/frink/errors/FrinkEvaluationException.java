package frink.errors;

public class FrinkEvaluationException extends FrinkException {
    public FrinkEvaluationException(String str) {
        super(str);
    }
}
