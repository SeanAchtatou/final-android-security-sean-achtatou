package frink.errors;

public class Cat {
    public static final ErrorCategory ERROR_HANDLER = new ErrorCategory("ErrorHandler");
    public static final ErrorCategory SecurityCode = new ErrorCategory("SecurityCode");
    public static final ErrorCategory Units = new ErrorCategory("Units");
}
