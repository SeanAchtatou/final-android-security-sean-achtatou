package frink.errors;

import frink.numeric.NumericException;

public class NotComparableException extends NumericException {
    public NotComparableException(String str) {
        super(str);
    }
}
