package frink.errors;

import java.util.Vector;

public class ErrorHandler {
    private Vector<ErrorLogger> loggers = new Vector<>(4);

    public void message(String str, ErrorSeverity errorSeverity, ErrorCategory errorCategory, String str2) {
        ErrorMessage errorMessage = new ErrorMessage(str, errorSeverity, errorCategory, str2);
        synchronized (this.loggers) {
            int size = this.loggers.size();
            for (int i = 0; i < size; i++) {
                this.loggers.elementAt(i).log(errorMessage);
            }
        }
    }

    public void addErrorLogger(ErrorLogger errorLogger) {
        synchronized (this.loggers) {
            this.loggers.addElement(errorLogger);
        }
    }

    public void removeErrorLogger(ErrorLogger errorLogger) {
        synchronized (this.loggers) {
            this.loggers.removeElement(errorLogger);
        }
        errorLogger.cleanup();
    }
}
