package frink.errors;

import frink.units.DimensionList;

public class ConformanceException extends FrinkException {
    private DimensionList dimension1;
    private DimensionList dimension2;

    public ConformanceException(String str) {
        super("Conformance Exception:\n " + str);
        this.dimension2 = null;
        this.dimension1 = null;
    }

    public ConformanceException(String str, DimensionList dimensionList) {
        super("Conformance Exception:\n " + str);
        this.dimension1 = dimensionList;
        this.dimension2 = null;
    }

    public ConformanceException(String str, DimensionList dimensionList, DimensionList dimensionList2) {
        super("Conformance Exception:\n " + str);
        this.dimension1 = dimensionList;
        this.dimension2 = dimensionList2;
    }

    public DimensionList getDimension1() {
        return this.dimension1;
    }

    public DimensionList getDimension2() {
        return this.dimension2;
    }
}
