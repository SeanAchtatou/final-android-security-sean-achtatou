package frink.errors;

import frink.numeric.NumericException;

public class NotRealException extends NumericException {
    public static final NotRealException INSTANCE = new NotRealException("Arguments were not real.");

    private NotRealException() {
        super("NotRealException");
    }

    public NotRealException(String str) {
        super(str);
    }
}
