package frink.errors;

public abstract class FrinkException extends Exception {
    public FrinkException(String str) {
        super(str);
    }

    public String toString() {
        return getMessage();
    }
}
