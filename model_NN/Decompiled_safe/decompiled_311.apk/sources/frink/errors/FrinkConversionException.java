package frink.errors;

public abstract class FrinkConversionException extends FrinkException {
    public FrinkConversionException(String str) {
        super(str);
    }
}
