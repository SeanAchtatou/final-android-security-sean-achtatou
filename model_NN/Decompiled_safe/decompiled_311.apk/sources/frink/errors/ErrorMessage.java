package frink.errors;

public class ErrorMessage {
    private ErrorCategory category;
    private String message;
    private ErrorSeverity severity;
    private String uniqueID;

    public ErrorMessage(String str, ErrorSeverity errorSeverity, ErrorCategory errorCategory, String str2) {
        this.uniqueID = str;
        this.severity = errorSeverity;
        this.category = errorCategory;
        this.message = str2;
    }

    public String getUniqueID() {
        return this.uniqueID;
    }

    public ErrorSeverity getSeverity() {
        return this.severity;
    }

    public ErrorCategory getCategory() {
        return this.category;
    }

    public String getMessage() {
        return this.message;
    }
}
