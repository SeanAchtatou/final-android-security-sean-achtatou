package frink.errors;

public class UnknownVariableException extends FrinkConversionException {
    public UnknownVariableException(String str) {
        super("Variable \"" + str + "\" not found.");
    }
}
