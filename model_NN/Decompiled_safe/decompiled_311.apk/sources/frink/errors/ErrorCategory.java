package frink.errors;

public class ErrorCategory {
    private String name;

    public ErrorCategory(String str) {
        this.name = str;
    }

    public boolean implies(ErrorCategory errorCategory) {
        return equals(errorCategory);
    }

    public String getName() {
        return this.name;
    }
}
