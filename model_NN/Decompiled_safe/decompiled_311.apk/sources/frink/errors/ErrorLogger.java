package frink.errors;

public interface ErrorLogger {
    void cleanup();

    String getName();

    void log(ErrorMessage errorMessage);
}
