package frink.errors;

public class Log {
    private static final ErrorHandler handler = new ErrorHandler();

    static {
        handler.addErrorLogger(new PrintStreamLogger("Standard Error Logger", System.err));
    }

    public static void message(String str, ErrorSeverity errorSeverity, ErrorCategory errorCategory, String str2) {
        handler.message(str, errorSeverity, errorCategory, str2);
    }
}
