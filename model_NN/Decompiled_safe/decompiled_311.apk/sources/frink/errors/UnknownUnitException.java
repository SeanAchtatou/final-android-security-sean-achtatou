package frink.errors;

public class UnknownUnitException extends FrinkConversionException {
    public UnknownUnitException(String str, String str2) {
        super("Unit \"" + str2 + "\" not found when attempting to set variable \"" + str + "\".");
    }
}
