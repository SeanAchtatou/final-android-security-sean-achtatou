package frink.errors;

import frink.numeric.NumericException;

public class NotAnIntegerException extends NumericException {
    public static final NotAnIntegerException INSTANCE = new NotAnIntegerException();

    private NotAnIntegerException() {
        super("NotAnIntegerException");
    }
}
