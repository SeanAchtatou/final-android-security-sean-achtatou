package frink.errors;

import frink.expr.Environment;
import frink.expr.Expression;
import frink.numeric.Numeric;
import frink.numeric.NumericException;

public class UnexpectedTypeException extends FrinkConversionException {
    public UnexpectedTypeException(String str, String str2, Expression expression, Environment environment) {
        super("When trying to get variable " + str + " as " + str2 + ", actual value was " + environment.format(expression));
    }

    public UnexpectedTypeException(String str, Numeric numeric, String str2, NumericException numericException) {
        super("Got a numeric exception when trying to set variable " + str + " to " + numeric.toString() + " using unit " + str2 + ":\n " + numericException.getMessage());
    }
}
