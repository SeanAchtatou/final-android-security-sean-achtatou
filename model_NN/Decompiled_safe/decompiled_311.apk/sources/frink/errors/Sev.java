package frink.errors;

public class Sev {
    public static final ErrorSeverity CRITICAL_ERROR = new ErrorSeverity("Critical_Error", 64);
    public static final ErrorSeverity DEBUG = new ErrorSeverity("Debug", 2);
    public static final ErrorSeverity DEBUG_VERBOSE = new ErrorSeverity("Debug_Verbose", 1);
    public static final ErrorSeverity INFO = new ErrorSeverity("Info", 4);
    public static final ErrorSeverity RECOVERABLE_ERROR = new ErrorSeverity("Recoverable_Error", 16);
    public static final ErrorSeverity UNRECOVERABLE_ERROR = new ErrorSeverity("Unrecoverable_Error", 32);
    public static final ErrorSeverity WARNING = new ErrorSeverity("Warning", 8);
    public static final int iCriticalError = 64;
    public static final int iDebug = 2;
    public static final int iDebugVerbose = 1;
    public static final int iInfo = 4;
    public static final int iRecoverableError = 16;
    public static final int iUnrecoverableError = 32;
    public static final int iWarning = 8;
}
