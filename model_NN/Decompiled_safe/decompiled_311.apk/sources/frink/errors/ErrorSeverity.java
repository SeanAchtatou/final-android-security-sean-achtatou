package frink.errors;

public class ErrorSeverity {
    private String name;
    private int severity;

    public ErrorSeverity(String str, int i) {
        this.name = str;
        this.severity = i;
    }

    public int getIntValue() {
        return this.severity;
    }

    public String getName() {
        return this.name;
    }
}
