package frink.errors;

import java.io.PrintStream;

public class PrintStreamLogger implements ErrorLogger {
    private String name;
    private PrintStream stream;

    public PrintStreamLogger(String str, PrintStream printStream) {
        this.name = str;
        this.stream = printStream;
    }

    public void log(ErrorMessage errorMessage) {
        this.stream.println(errorMessage.getSeverity().getName() + ": " + errorMessage.getMessage());
    }

    public String getName() {
        return this.name;
    }

    public void cleanup() {
    }
}
