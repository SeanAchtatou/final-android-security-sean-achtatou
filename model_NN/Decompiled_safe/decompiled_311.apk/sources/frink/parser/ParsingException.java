package frink.parser;

import frink.errors.FrinkException;

public abstract class ParsingException extends FrinkException {
    public ParsingException(String str) {
        super(str);
    }
}
