package frink.parser;

public class CannotIncludeException extends IncludeException {
    private String fileName;
    private String reason;

    CannotIncludeException(String str, String str2) {
        super("CannotIncludeException");
        this.fileName = str;
        this.reason = str2;
    }

    public String toString() {
        return "CannotIncludeException: Cannot include '" + this.fileName + "':\n " + this.reason;
    }
}
