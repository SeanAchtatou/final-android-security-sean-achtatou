package frink.parser;

public class LexerException extends ParsingException {
    LexerException(String str) {
        super(str);
    }
}
