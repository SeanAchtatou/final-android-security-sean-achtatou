package frink.parser;

import java.io.Reader;
import java.io.StringReader;
import java.net.URL;

public class StringCodeSource implements CodeSource {
    private String description;
    private String str;

    public StringCodeSource(String str2, String str3) {
        this.str = str2;
        this.description = str3;
    }

    public Reader getReader() {
        return new StringReader(this.str);
    }

    public URL getURL() {
        return null;
    }

    public String toString() {
        return this.description;
    }
}
