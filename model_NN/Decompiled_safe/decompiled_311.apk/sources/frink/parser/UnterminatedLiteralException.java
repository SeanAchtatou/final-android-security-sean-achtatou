package frink.parser;

public class UnterminatedLiteralException extends LexerException {
    UnterminatedLiteralException(String str) {
        super(str);
    }
}
