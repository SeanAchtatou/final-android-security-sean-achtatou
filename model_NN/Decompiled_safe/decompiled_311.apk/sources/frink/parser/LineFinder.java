package frink.parser;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;

public class LineFinder {
    private LineNumberReader reader;

    public LineFinder(Reader reader2) {
        this.reader = new LineNumberReader(reader2);
        this.reader.setLineNumber(1);
    }

    public String getLine(int i) {
        String str = null;
        while (this.reader.getLineNumber() <= i) {
            try {
                str = this.reader.readLine();
            } catch (IOException e) {
            }
        }
        this.reader.close();
        return str;
    }

    /* access modifiers changed from: package-private */
    public String getLineWithPointer(int i, int i2) {
        String line = getLine(i);
        if (line == null) {
            return line;
        }
        StringBuffer stringBuffer = new StringBuffer(i2 + 1);
        for (int i3 = 0; i3 < i2; i3++) {
            stringBuffer.append(' ');
        }
        stringBuffer.append('^');
        return line + "\n" + ((Object) stringBuffer);
    }
}
