package frink.parser;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;

public interface CodeSource {
    Reader getReader() throws IOException;

    URL getURL();

    String toString();
}
