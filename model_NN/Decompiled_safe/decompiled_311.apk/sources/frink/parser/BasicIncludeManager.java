package frink.parser;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.oro.text.regex.Perl5Compiler;

public class BasicIncludeManager implements IncludeManager {
    private static final boolean DEBUG = false;
    private static final String SAMPLES_LIBRARY = "/samples/";
    private static final String STANDARD_LIBRARY = "/stdlib/";
    private Vector<File> includePath = new Vector<>(1);
    private Hashtable<String, String> included = new Hashtable<>();

    public BasicIncludeManager() {
        try {
            prependPath(System.getProperty("user.dir"));
        } catch (SecurityException e) {
        }
    }

    public CodeSource getCodeSource(String str, String str2, CodeSource codeSource, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException {
        CodeSource codeSourceAsResource;
        environment.getSecurityHelper().checkUse();
        if (this.included.get(str) != null) {
            throw new AlreadyIncludedException();
        }
        if (str.startsWith("/")) {
            codeSourceAsResource = getCodeSourceAsResource(str, str2, environment);
        } else {
            codeSourceAsResource = getCodeSourceAsResource("/" + str, str2, environment);
        }
        if (codeSourceAsResource != null) {
            return codeSourceAsResource;
        }
        CodeSource codeSourceAsURL = getCodeSourceAsURL(str, str2, environment);
        if (codeSourceAsURL != null) {
            return codeSourceAsURL;
        }
        if (codeSource != null) {
            codeSourceAsURL = getCodeSourceRelativeToCurrent(str, str2, codeSource, environment);
        }
        if (codeSourceAsURL != null) {
            return codeSourceAsURL;
        }
        CodeSource codeSourceAsRelativePath = getCodeSourceAsRelativePath(str, str2, environment);
        if (codeSourceAsRelativePath != null) {
            return codeSourceAsRelativePath;
        }
        CodeSource codeSourceAsResource2 = getCodeSourceAsResource(STANDARD_LIBRARY + str, str2, environment);
        if (codeSourceAsResource2 == null) {
            return getCodeSourceAsResource(SAMPLES_LIBRARY + str, str2, environment);
        }
        return codeSourceAsResource2;
    }

    private CodeSource getCodeSourceAsResource(String str, String str2, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException {
        URL resource = getClass().getResource(str);
        if (resource != null) {
            if (this.included.get(resource.toString()) != null) {
                throw new AlreadyIncludedException();
            }
            CodeSource codeSourceAsURL = getCodeSourceAsURL(resource, str2, environment);
            if (codeSourceAsURL != null) {
                this.included.put(str, resource.toString());
                return codeSourceAsURL;
            }
        }
        return null;
    }

    private CodeSource getCodeSourceAsURL(URL url, String str, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException {
        String str2;
        InputStreamReader inputStreamReader;
        String headerField;
        int indexOf;
        if (this.included.get(url.toString()) != null) {
            throw new AlreadyIncludedException();
        }
        environment.getSecurityHelper().checkRead(url);
        try {
            URLConnection openConnection = url.openConnection();
            this.included.put(url.toString(), url.toString());
            if (str != null || (headerField = openConnection.getHeaderField("Content-Type")) == null || (indexOf = headerField.indexOf("charset=")) == -1) {
                str2 = str;
            } else {
                str2 = headerField.substring(indexOf + 8).trim();
                if (str2.length() == 0) {
                    str2 = null;
                }
            }
            if (str2 == null) {
                inputStreamReader = new InputStreamReader(openConnection.getInputStream());
            } else {
                inputStreamReader = new InputStreamReader(openConnection.getInputStream(), str2);
            }
            return new URLCodeSource(url, new BufferedReader(inputStreamReader, Perl5Compiler.READ_ONLY_MASK), str2);
        } catch (IOException e) {
            throw new CannotIncludeException(url.toString(), e.toString());
        }
    }

    private CodeSource getCodeSourceAsURL(String str, String str2, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException {
        try {
            URL url = new URL(str);
            CodeSource codeSourceAsURL = getCodeSourceAsURL(url, str2, environment);
            if (codeSourceAsURL != null) {
                this.included.put(str, url.toString());
            }
            return codeSourceAsURL;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    private CodeSource getCodeSourceAsRelativePath(String str, String str2, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException {
        int size = this.includePath.size();
        int i = 0;
        while (i < size) {
            try {
                URL url = new File(this.includePath.elementAt(i), str).toURL();
                try {
                    CodeSource codeSourceAsURL = getCodeSourceAsURL(url, str2, environment);
                    if (codeSourceAsURL != null) {
                        this.included.put(str, url.toString());
                        return codeSourceAsURL;
                    }
                    i++;
                } catch (CannotIncludeException e) {
                }
            } catch (MalformedURLException e2) {
            }
        }
        return null;
    }

    private CodeSource getCodeSourceRelativeToCurrent(String str, String str2, CodeSource codeSource, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException {
        URL url = codeSource.getURL();
        if (url == null) {
            return null;
        }
        try {
            return getCodeSourceAsURL(new URL(url, str), str2, environment);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public void prependPath(String str) {
        this.includePath.insertElementAt(new File(str), 0);
    }

    public void appendPath(String str) {
        this.includePath.addElement(new File(str));
    }
}
