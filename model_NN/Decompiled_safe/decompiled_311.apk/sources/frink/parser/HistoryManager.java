package frink.parser;

import java.util.Vector;

public class HistoryManager {
    private Vector<HistoryPair> history = new Vector<>();

    public String getFrom(int i) {
        return this.history.elementAt(i).getFrom();
    }

    public String getTo(int i) {
        return this.history.elementAt(i).getTo();
    }

    public String getToString(int i) {
        String to = this.history.elementAt(i).getTo();
        return to == null ? "" : to;
    }

    public String getEntireString(int i) {
        HistoryPair elementAt = this.history.elementAt(i);
        String to = elementAt.getTo();
        if (to == null || to.length() <= 0) {
            return elementAt.getFrom();
        }
        return elementAt.getFrom() + " -> " + to;
    }

    public void append(String str, String str2) {
        this.history.addElement(new HistoryPair(str, str2));
    }

    public int getSize() {
        return this.history.size();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save(java.io.File r6) throws java.io.IOException {
        /*
            r5 = this;
            java.lang.String r0 = r6.getPath()
            r1 = 0
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ all -> 0x0036 }
            r2.<init>(r0)     // Catch:{ all -> 0x0036 }
            java.util.Vector<frink.parser.HistoryManager$HistoryPair> r0 = r5.history     // Catch:{ all -> 0x003d }
            int r0 = r0.size()     // Catch:{ all -> 0x003d }
            r1 = 0
        L_0x0011:
            if (r1 >= r0) goto L_0x0030
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x003d }
            r3.<init>()     // Catch:{ all -> 0x003d }
            java.lang.String r4 = r5.getEntireString(r1)     // Catch:{ all -> 0x003d }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x003d }
            java.lang.String r4 = "\n\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x003d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x003d }
            r2.write(r3)     // Catch:{ all -> 0x003d }
            int r1 = r1 + 1
            goto L_0x0011
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()
        L_0x0035:
            return
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            throw r0
        L_0x003d:
            r0 = move-exception
            r1 = r2
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.parser.HistoryManager.save(java.io.File):void");
    }

    private static class HistoryPair {
        private String from;
        private String to;

        public HistoryPair(String str, String str2) {
            this.from = str;
            this.to = str2;
        }

        public String getFrom() {
            return this.from;
        }

        public String getTo() {
            return this.to;
        }
    }
}
