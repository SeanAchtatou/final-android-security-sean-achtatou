package frink.parser;

import frink.expr.Environment;
import frink.expr.FrinkSecurityException;

public interface IncludeManager {
    void appendPath(String str);

    CodeSource getCodeSource(String str, String str2, CodeSource codeSource, Environment environment) throws AlreadyIncludedException, CannotIncludeException, FrinkSecurityException;

    void prependPath(String str);
}
