package frink.parser;

import frink.date.BasicDateFormatterManager;
import frink.date.BasicDateParserManager;
import frink.date.DateFormatterManager;
import frink.date.DateParserManager;
import frink.date.FrinkDateFormatter;
import frink.date.JulianDateFormatterSource;
import frink.date.JulianDateParser;
import frink.date.SimpleFrinkDateFormatter;
import frink.date.TimeZoneDateFormatterSource;
import frink.date.TimeZoneSource;
import frink.expr.AbstractEnvironment;
import frink.expr.BasicConstraintFactory;
import frink.expr.BasicContext;
import frink.expr.BasicContextFrame;
import frink.expr.BasicDateFormatterExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.BuiltinConstraintSource;
import frink.expr.BuiltinObjectSource;
import frink.expr.CannotAssignException;
import frink.expr.ConstraintFactory;
import frink.expr.Context;
import frink.expr.ContextFrame;
import frink.expr.DimensionListConstraintSource;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.FunctionConstraintSource;
import frink.expr.SymbolDefinition;
import frink.expr.UnitExpression;
import frink.expr.UnknownConstraintException;
import frink.expr.UnmodifiableConstantSymbolDefinition;
import frink.expr.VariableExistsException;
import frink.format.BasicUnitFormatterManager;
import frink.format.ExpressionFormatter;
import frink.format.PreferredExpressionFormatter;
import frink.format.UnitFormatterManager;
import frink.function.BasicFunctionManager;
import frink.function.FunctionManager;
import frink.graphics.BasicFrinkImageLoader;
import frink.graphics.BasicGraphicsViewFactory;
import frink.graphics.FrinkImageLoader;
import frink.graphics.GraphicsObjectSource;
import frink.graphics.GraphicsViewFactory;
import frink.io.InputManager;
import frink.io.OutputManager;
import frink.io.SystemInputManager;
import frink.io.SystemOutputManager;
import frink.object.BasicClassManager;
import frink.object.BasicInterfaceManager;
import frink.object.BasicObjectManager;
import frink.object.ClassManager;
import frink.object.FrinkClassConstraintSource;
import frink.object.InterfaceManager;
import frink.object.ObjectManager;
import frink.security.NoSecurityHelper;
import frink.security.RestrictiveSecurityHelper;
import frink.security.SecurityHelper;
import frink.symbolic.BasicTransformationRuleManager;
import frink.symbolic.TransformationRuleManager;
import frink.units.BasicDimensionListManager;
import frink.units.BasicDimensionManager;
import frink.units.DimensionListManager;
import frink.units.DimensionManager;
import frink.units.EnglishUnitManager;
import frink.units.Unit;
import frink.units.UnitManager;
import java.text.DateFormat;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.Vector;

public class BasicEnvironment extends AbstractEnvironment {
    private BasicClassManager classManager = null;
    private ConstraintFactory constraintFactory = null;
    private Context context = new BasicContext();
    private DimensionListManager dListMgr = new BasicDimensionListManager();
    private DimensionManager dMgr = new BasicDimensionManager();
    private DateFormatterManager dateFormatMgr = null;
    private DateParserManager dateParserMgr = null;
    private ExpressionFormatter formatter = new PreferredExpressionFormatter();
    private FrinkImageLoader frinkImageLoader = null;
    private FunctionManager funcManager = new BasicFunctionManager();
    private ContextFrame globals = new BasicContextFrame();
    private GraphicsViewFactory graphicsViewFactory = null;
    private IncludeManager includeManager = null;
    private InputManager inputManager = SystemInputManager.INSTANCE;
    private InterfaceManager interfaceManager = null;
    private ObjectManager objectManager = null;
    private FrinkDateFormatter outputDateFormatter = null;
    private OutputManager outputManager = SystemOutputManager.INSTANCE;
    private UnitCUPParser parser = new UnitCUPParser(this);
    private UnitLexer scanner = null;
    private SecurityHelper securityHelper = null;
    private boolean symbolicMode = false;
    private TimeZone timezone = null;
    private TransformationRuleManager transformationRuleManager = null;
    private UnitManager uMgr = new EnglishUnitManager();
    private Hashtable<String, UnmodifiableConstantSymbolDefinition> unitCache = new Hashtable<>();
    private UnitFormatterManager unitFormatterMgr = null;

    public boolean isVariableDefined(String str) {
        try {
            return this.context.getSymbolDefinition(str, false, this) != null;
        } catch (CannotAssignException e) {
            System.err.println("BasicEnvironment.isVariableDefined:  Unexpected CannotAssignException:\n  " + e);
            return false;
        } catch (FrinkSecurityException e2) {
            System.err.println("BasicEnvironment.isVariableDefined:  Unexpected FrinkSecurityException:\n  " + e2);
            return false;
        }
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z) throws CannotAssignException, FrinkSecurityException {
        SymbolDefinition symbolDefinition = this.context.getSymbolDefinition(str, z, this);
        if (symbolDefinition != null) {
            return symbolDefinition;
        }
        SymbolDefinition symbolDefinition2 = this.globals.getSymbolDefinition(str, false, this);
        if (symbolDefinition2 != null) {
            return symbolDefinition2;
        }
        Unit unit = getUnitManager().getUnit(str);
        if (unit != null) {
            UnmodifiableConstantSymbolDefinition unmodifiableConstantSymbolDefinition = this.unitCache.get(str);
            if (unmodifiableConstantSymbolDefinition != null && ((UnitExpression) unmodifiableConstantSymbolDefinition.getValue()).getUnit() == unit) {
                return unmodifiableConstantSymbolDefinition;
            }
            UnmodifiableConstantSymbolDefinition unmodifiableConstantSymbolDefinition2 = new UnmodifiableConstantSymbolDefinition(BasicUnitExpression.construct(unit));
            this.unitCache.put(str, unmodifiableConstantSymbolDefinition2);
            return unmodifiableConstantSymbolDefinition2;
        }
        FrinkDateFormatter formatter2 = getDateFormatterManager().getFormatter(str);
        if (formatter2 != null) {
            return new UnmodifiableConstantSymbolDefinition(new BasicDateFormatterExpression(formatter2));
        }
        return null;
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression) throws CannotAssignException, FrinkSecurityException {
        try {
            return this.context.setSymbol(str, expression, this);
        } catch (CannotAssignException e) {
            throw e;
        }
    }

    public SymbolDefinition declareVariable(String str, Vector<String> vector, Expression expression) throws VariableExistsException, CannotAssignException, UnknownConstraintException, FrinkSecurityException {
        return this.context.declareVariable(str, getConstraintFactory().createConstraints(vector), expression, this);
    }

    public SymbolDefinition declareGlobalVariable(String str, Vector<String> vector, Expression expression) throws VariableExistsException, CannotAssignException, UnknownConstraintException, FrinkSecurityException {
        return this.globals.declareVariable(str, getConstraintFactory().createConstraints(vector), expression, this);
    }

    public DimensionManager getDimensionManager() {
        return this.dMgr;
    }

    public DimensionListManager getDimensionListManager() {
        return this.dListMgr;
    }

    public UnitManager getUnitManager() {
        return this.uMgr;
    }

    public synchronized DateParserManager getDateParserManager() {
        if (this.dateParserMgr == null) {
            this.dateParserMgr = new BasicDateParserManager();
            this.dateParserMgr.addParser(new JulianDateParser());
        }
        return this.dateParserMgr;
    }

    public synchronized FrinkDateFormatter getOutputDateFormatter() {
        if (this.outputDateFormatter == null) {
            DateFormat dateTimeInstance = DateFormat.getDateTimeInstance();
            if (dateTimeInstance == null) {
                System.out.println("DateFormat.getDateTimeInstance() returned null!");
            }
            this.outputDateFormatter = new SimpleFrinkDateFormatter(dateTimeInstance);
        }
        return this.outputDateFormatter;
    }

    public synchronized void setOutputDateFormatter(FrinkDateFormatter frinkDateFormatter) {
        this.outputDateFormatter = frinkDateFormatter;
    }

    public synchronized DateFormatterManager getDateFormatterManager() {
        if (this.dateFormatMgr == null) {
            this.dateFormatMgr = new BasicDateFormatterManager();
            this.dateFormatMgr.addFormatterSource(new JulianDateFormatterSource());
            this.dateFormatMgr.addFormatterSource(new TimeZoneDateFormatterSource());
        }
        return this.dateFormatMgr;
    }

    public void addContextFrame(ContextFrame contextFrame, boolean z) {
        this.context.addContextFrame(contextFrame, z);
    }

    public ContextFrame removeContextFrame() {
        return this.context.removeContextFrame();
    }

    public String format(Expression expression) {
        return this.formatter.format(expression, this, true);
    }

    public String format(Expression expression, boolean z) {
        return this.formatter.format(expression, this, z);
    }

    public FunctionManager getFunctionManager() {
        return this.funcManager;
    }

    public synchronized GraphicsViewFactory getGraphicsViewFactory() {
        if (this.graphicsViewFactory == null) {
            this.graphicsViewFactory = new BasicGraphicsViewFactory("DeferredWindow");
        }
        return this.graphicsViewFactory;
    }

    public void setGraphicsViewFactory(GraphicsViewFactory graphicsViewFactory2) {
        this.graphicsViewFactory = graphicsViewFactory2;
    }

    public Expression eval(String str) throws Exception {
        if (this.scanner == null) {
            this.scanner = new UnitLexer(new StringCodeSource(str + ";", "eval[] function"), this);
            this.parser.setScanner(this.scanner);
        } else {
            this.scanner.reset(new StringCodeSource(str + ";", "eval[] function"));
        }
        this.parser.parse();
        return this.parser.program;
    }

    public synchronized TimeZone getDefaultTimeZone() {
        if (this.timezone == null) {
            this.timezone = TimeZone.getDefault();
        }
        return this.timezone;
    }

    public void setDefaultTimeZone(String str) {
        TimeZone timeZone = TimeZoneSource.getTimeZone(str);
        if (timeZone == null) {
            outputln("Could not find timezone named \"" + str + "\".  Default timezone unchanged.");
        } else {
            this.timezone = timeZone;
        }
    }

    public OutputManager getOutputManager() {
        return this.outputManager;
    }

    public void setOutputManager(OutputManager outputManager2) {
        this.outputManager = outputManager2;
    }

    public InputManager getInputManager() {
        return this.inputManager;
    }

    public void setInputManager(InputManager inputManager2) {
        this.inputManager = inputManager2;
    }

    public synchronized SecurityHelper getSecurityHelper() {
        if (this.securityHelper == null) {
            this.securityHelper = NoSecurityHelper.INSTANCE;
        }
        return this.securityHelper;
    }

    public synchronized void setSecurityHelper(SecurityHelper securityHelper2) {
        this.securityHelper = securityHelper2;
    }

    public synchronized UnitFormatterManager getUnitFormatterManager() {
        if (this.unitFormatterMgr == null) {
            this.unitFormatterMgr = new BasicUnitFormatterManager();
        }
        return this.unitFormatterMgr;
    }

    public synchronized IncludeManager getIncludeManager() {
        if (this.includeManager == null) {
            this.includeManager = new BasicIncludeManager();
        }
        return this.includeManager;
    }

    public synchronized ConstraintFactory getConstraintFactory() {
        if (this.constraintFactory == null) {
            this.constraintFactory = new BasicConstraintFactory();
            this.constraintFactory.addConstraintSource(BuiltinConstraintSource.INSTANCE);
            this.constraintFactory.addConstraintSource(new DimensionListConstraintSource(this.dListMgr));
            this.constraintFactory.addConstraintSource(new FunctionConstraintSource(this));
            if (this.classManager == null) {
                getClassManager();
            }
            this.constraintFactory.addConstraintSource(new FrinkClassConstraintSource(this.classManager, this.interfaceManager));
        }
        return this.constraintFactory;
    }

    public synchronized ObjectManager getObjectManager() {
        if (this.objectManager == null) {
            this.objectManager = new BasicObjectManager();
            this.objectManager.addSource(BuiltinObjectSource.INSTANCE);
            this.objectManager.addSource(GraphicsObjectSource.INSTANCE);
            if (this.classManager == null) {
                getClassManager();
            }
            this.objectManager.addSource(this.classManager);
        }
        return this.objectManager;
    }

    public synchronized InterfaceManager getInterfaceManager() {
        if (this.interfaceManager == null) {
            this.interfaceManager = new BasicInterfaceManager();
        }
        return this.interfaceManager;
    }

    public synchronized ClassManager getClassManager() {
        if (this.classManager == null) {
            this.classManager = new BasicClassManager();
        }
        return this.classManager;
    }

    public synchronized TransformationRuleManager getTransformationRuleManager() {
        if (this.transformationRuleManager == null) {
            this.transformationRuleManager = new BasicTransformationRuleManager();
            this.transformationRuleManager.setParsingTransformationRuleSet(BasicTransformationRuleManager.DEFAULT_RULESET_NAME);
        }
        return this.transformationRuleManager;
    }

    public synchronized FrinkImageLoader getFrinkImageLoader() {
        if (this.frinkImageLoader == null) {
            this.frinkImageLoader = BasicFrinkImageLoader.INSTANCE;
        }
        return this.frinkImageLoader;
    }

    public void setSymbolicMode(boolean z) {
        this.symbolicMode = z;
    }

    public boolean getSymbolicMode() {
        return this.symbolicMode;
    }

    public void setRestrictiveSecurity(boolean z) {
        if (z) {
            setSecurityHelper(RestrictiveSecurityHelper.INSTANCE);
        } else {
            setSecurityHelper(NoSecurityHelper.INSTANCE);
        }
    }
}
