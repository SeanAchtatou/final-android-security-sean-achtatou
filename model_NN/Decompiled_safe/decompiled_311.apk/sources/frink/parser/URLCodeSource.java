package frink.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

public class URLCodeSource implements CodeSource {
    private static final int BUFSIZE = 16384;
    private String encoding;
    private Reader reader;
    private URL url;
    private String urlString;

    public URLCodeSource(URL url2) throws MalformedURLException {
        this.url = url2;
        this.urlString = url2.toString();
        this.encoding = null;
        this.reader = null;
    }

    public URLCodeSource(String str) throws MalformedURLException {
        this.urlString = str;
        this.encoding = null;
        this.url = new URL(str);
        this.reader = null;
    }

    public URLCodeSource(String str, String str2) throws MalformedURLException {
        this.urlString = str;
        this.encoding = str2;
        this.url = new URL(str);
        this.reader = null;
    }

    public URLCodeSource(URL url2, String str) throws MalformedURLException {
        this.url = url2;
        this.urlString = url2.toString();
        this.encoding = str;
        this.reader = null;
    }

    public URLCodeSource(URL url2, Reader reader2, String str) throws MalformedURLException {
        this.url = url2;
        this.urlString = url2.toString();
        this.encoding = str;
        this.reader = reader2;
    }

    public Reader getReader() throws IOException {
        if (this.reader != null) {
            Reader reader2 = this.reader;
            this.reader = null;
            return reader2;
        } else if (this.encoding != null) {
            return new BufferedReader(new InputStreamReader(this.url.openStream(), this.encoding), BUFSIZE);
        } else {
            return new BufferedReader(new InputStreamReader(this.url.openStream()), BUFSIZE);
        }
    }

    public URL getURL() {
        return this.url;
    }

    public String toString() {
        return this.urlString;
    }
}
