package frink.parser;

public abstract class IncludeException extends ParsingException {
    IncludeException(String str) {
        super(str);
    }
}
