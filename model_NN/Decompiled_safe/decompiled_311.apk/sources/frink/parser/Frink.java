package frink.parser;

import frink.date.CalendarDate;
import frink.date.DateFunctionSource;
import frink.errors.FrinkEvaluationException;
import frink.errors.NotRealException;
import frink.errors.UnexpectedTypeException;
import frink.errors.UnknownUnitException;
import frink.errors.UnknownVariableException;
import frink.expr.BasicContextFrame;
import frink.expr.BasicDateExpression;
import frink.expr.BasicListExpression;
import frink.expr.BasicStringExpression;
import frink.expr.BasicUnitExpression;
import frink.expr.CannotAssignException;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.ExitException;
import frink.expr.Expression;
import frink.expr.FrinkBoolean;
import frink.expr.FrinkSecurityException;
import frink.expr.SymbolDefinition;
import frink.expr.UnitExpression;
import frink.expr.UnknownConstraintException;
import frink.expr.VariableExistsException;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.MathFunctionSource;
import frink.io.IOFunctionSource;
import frink.java.JavaFunctionSource;
import frink.java.JavaMapper;
import frink.java.JavaObjectFactory;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInteger;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.security.NoSecurityHelper;
import frink.security.RestrictiveSecurityHelper;
import frink.source.CPISource;
import frink.source.CurrencySource;
import frink.source.PoundSource;
import frink.text.TranslatorFunctionSource;
import frink.units.Dimension;
import frink.units.Unit;
import frink.units.UnitMath;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class Frink {
    private static final String ENCODING_MARKER = "--encoding";
    private boolean continueAfterParse = false;
    private String encoding = null;
    private Environment env = new BasicEnvironment();
    private boolean filesParsed = false;
    private boolean ignoreErrors = false;
    private boolean initialized = false;
    private boolean noUnits = false;
    private String nonStandardUnits = null;
    private UnitCUPParser parser = new UnitCUPParser(this.env);
    private boolean sandbox = false;
    private UnitLexer scanner = null;
    private BasicContextFrame userVars = null;

    public String parseString(String str) throws FrinkEvaluationException {
        if (!this.initialized) {
            initializeDefaultSources();
        }
        if (str == null) {
            return null;
        }
        try {
            if (this.scanner == null) {
                this.scanner = new UnitLexer(new StringCodeSource(str, "<String>"), this.env);
                this.parser.setScanner(this.scanner);
            } else {
                this.scanner.reset(new StringCodeSource(str, "<String>"));
            }
            this.parser.parse();
            int errorCount = this.parser.getErrorCount();
            if (errorCount == 0 || this.ignoreErrors) {
                try {
                    return this.env.format(this.parser.program.evaluate(this.env));
                } catch (FrinkSecurityException e) {
                    StringBuffer stringBuffer = new StringBuffer("Operation denied due to security policy: " + e);
                    if (this.sandbox) {
                        stringBuffer.append("\nFrink was started with the \"--sandbox\" command-line option which is probably the reason that this operation was denied.");
                    } else {
                        stringBuffer.append("\nYou appear to be running Frink on a computer that's not your own.  Some operations are denied due to security concerns.  To use this feature, please download Frink and run it on your own system, or use the Frink Applet, which runs entirely on your own system.");
                    }
                    throw new FrinkEvaluationException(new String(stringBuffer));
                } catch (Exception e2) {
                    e2.printStackTrace();
                    throw new FrinkEvaluationException(e2.toString());
                }
            } else {
                throw new FrinkEvaluationException(errorCount + " error(s) occurred during parsing.");
            }
        } catch (FrinkSecurityException e3) {
            StringBuffer stringBuffer2 = new StringBuffer("Operation denied due to security policy: " + e3);
            if (this.sandbox) {
                stringBuffer2.append("\nFrink was started with the \"--sandbox\" command-line option which is probably the reason that this operation was denied.");
            } else {
                stringBuffer2.append("\nYou appear to be running Frink on a computer that's not your own.  Some operations are denied due to security concerns.  To use this feature, please download Frink and run it on your own system, or use the Frink Applet, which runs entirely on your own system.");
            }
            throw new FrinkEvaluationException(new String(stringBuffer2));
        } catch (Throwable th) {
            th.printStackTrace();
            throw new FrinkEvaluationException(th.toString());
        }
    }

    public Expression parseStringToExpression(String str) {
        if (!this.initialized) {
            initializeDefaultSources();
        }
        try {
            if (this.scanner == null) {
                this.scanner = new UnitLexer(new StringCodeSource(str, "<String>"), this.env);
                this.parser.setScanner(this.scanner);
            } else {
                this.scanner.reset(new StringCodeSource(str, "<String>"));
            }
            this.parser.parse();
            int errorCount = this.parser.getErrorCount();
            if (errorCount <= 0 || this.ignoreErrors) {
                return this.parser.program;
            }
            this.env.outputln(errorCount + " errors when parsing expression.");
            return null;
        } catch (Exception e) {
            this.env.outputln("Error in parse: " + e);
            return null;
        }
    }

    public void parseStrings(String[] strArr) throws FrinkEvaluationException {
        if (!this.initialized) {
            initializeDefaultSources();
        }
        for (String parseString : strArr) {
            this.env.outputln(parseString(parseString));
        }
    }

    private void parseStdInput() {
        String str;
        if (!this.initialized) {
            initializeDefaultSources();
        }
        try {
            this.env.outputln("Frink - Copyright 2000-2011 Alan Eliasen, eliasen@mindspring.com.");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            str = null;
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    str = parseString(readLine);
                    this.env.outputln(str);
                } else {
                    this.env.outputln("Last result was " + str);
                    return;
                }
            }
        } catch (FrinkEvaluationException e) {
            FrinkEvaluationException frinkEvaluationException = e;
            String str2 = str;
            this.env.outputln("Error in evaluation: " + frinkEvaluationException.toString());
            str = str2;
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void parseFilename(String str) {
        if (!this.initialized) {
            initializeDefaultSources();
        }
        try {
            URLCodeSource uRLCodeSource = new URLCodeSource("file:" + str, this.encoding);
            if (this.scanner == null) {
                this.scanner = new UnitLexer(uRLCodeSource, this.env);
                this.parser.setScanner(this.scanner);
            } else {
                this.scanner.reset(uRLCodeSource);
            }
            try {
                this.parser.parse();
                try {
                    int errorCount = this.parser.getErrorCount();
                    if (errorCount == 0 || this.ignoreErrors) {
                        this.env.outputln(this.env.format(this.parser.program.evaluate(this.env)));
                    } else {
                        this.env.outputln(errorCount + " error(s) occured during parsing.  Program will not be run.");
                    }
                } catch (ExitException e) {
                } catch (EvaluationException e2) {
                    this.env.outputln("Error: " + e2 + " in " + this.env.format(e2.getExpression()));
                }
            } catch (Exception e3) {
                this.env.outputln("Error in parsing " + str + " " + e3);
            }
        } catch (IOException e4) {
            this.env.outputln("Can't open file " + str);
        }
    }

    public String[] parseArguments(String[] strArr) {
        BasicListExpression basicListExpression;
        BasicListExpression basicListExpression2 = new BasicListExpression(0);
        try {
            this.env.declareGlobalVariable("ARGS", null, basicListExpression2);
            basicListExpression = basicListExpression2;
        } catch (VariableExistsException e) {
            try {
                System.err.println("Warning:  parseArguments called multiple times or ARGS variable already exists.");
                basicListExpression = (BasicListExpression) this.env.getSymbolDefinition("ARGS", false).getValue();
            } catch (CannotAssignException e2) {
                System.err.println("Warning:  could not assign to variable ARGS in Frink.parseArguments.");
                basicListExpression = basicListExpression2;
            } catch (FrinkSecurityException e3) {
                basicListExpression = basicListExpression2;
            }
        } catch (UnknownConstraintException e4) {
            basicListExpression = basicListExpression2;
        }
        Vector vector = new Vector(strArr.length);
        int i = 0;
        String str = null;
        while (i < strArr.length) {
            if (strArr[i].equals("-f")) {
                i++;
                this.filesParsed = true;
                parseFilename(strArr[i]);
            } else if (strArr[i].equals("-k")) {
                this.continueAfterParse = true;
            } else if (strArr[i].equals("-u")) {
                i++;
                this.nonStandardUnits = strArr[i];
            } else if (strArr[i].equals("-nu") || strArr[i].equals("--nounits")) {
                this.noUnits = true;
            } else if (strArr[i].equals("--ignore-errors")) {
                this.ignoreErrors = true;
            } else if (strArr[i].equals("-v") || strArr[i].equals("--version")) {
                i++;
                try {
                    System.err.println(Class.forName("frink.parser.FrinkVersion").getField("VERSION").get(null));
                } catch (ClassNotFoundException e5) {
                    System.err.println("Could not find version " + e5);
                } catch (NoSuchFieldException e6) {
                    System.err.println("Could not find version " + e6);
                } catch (IllegalAccessException e7) {
                    System.err.println("Could not find version " + e7);
                }
                System.exit(0);
            } else if (strArr[i].equals("-I")) {
                i++;
                this.env.getIncludeManager().appendPath(strArr[i]);
            } else if (strArr[i].equals("--sandbox")) {
                this.sandbox = true;
                if (this.initialized) {
                    setRestrictiveSecurity(true);
                }
            } else if (strArr[i].startsWith(ENCODING_MARKER)) {
                int length = ENCODING_MARKER.length();
                if (strArr[i].length() <= length) {
                    i++;
                    if (i < strArr.length) {
                        this.encoding = strArr[i];
                    } else {
                        this.encoding = null;
                    }
                } else if (strArr[i].charAt(length) == '=') {
                    this.encoding = strArr[i].substring(length + 1);
                }
                if (this.encoding.length() == 0) {
                    this.encoding = null;
                }
            } else if (strArr[i].equals("-e")) {
                i++;
                vector.addElement(strArr[i]);
            } else if (str != null) {
                basicListExpression.appendChild(new BasicStringExpression(strArr[i]));
            } else {
                str = strArr[i];
                this.filesParsed = true;
            }
            i++;
        }
        if (str != null) {
            parseFilename(str);
        }
        String[] strArr2 = new String[vector.size()];
        for (int i2 = 0; i2 < vector.size(); i2++) {
            strArr2[i2] = (String) vector.elementAt(i2);
        }
        return strArr2;
    }

    public void setUnitsFile(String str) {
        this.nonStandardUnits = str;
    }

    public void setRestrictiveSecurity(boolean z) {
        this.env.setRestrictiveSecurity(z);
        if (z) {
            this.env.setSecurityHelper(RestrictiveSecurityHelper.INSTANCE);
        } else {
            this.env.setSecurityHelper(NoSecurityHelper.INSTANCE);
        }
    }

    private synchronized void initializeDefaultSources() {
        String str;
        String str2;
        Unit unit;
        URLCodeSource uRLCodeSource;
        if (!this.initialized) {
            try {
                Class<?> cls = getClass();
                if (!this.noUnits) {
                    if (this.nonStandardUnits == null) {
                        URL resource = cls.getResource("/data/units.txt");
                        if (resource == null) {
                            System.err.println("Failed to find Frink's standard data file, which usually resides inside Frink's jar file (in \"/data/units.txt\".)  If your jar file correctly contains this file, there is probably a bug in your Java implementation's classloader and its implementation of getResource(String) which is failing to look inside the same frink.jar file for resources.\nThe classloader used is the one that your system provides to '" + cls.getName() + "' which is '" + cls.getClassLoader().getClass().getName() + "'.\nAttempting to continue without *any* units, but lots of things will probably break.\n\nTHE PLATFORM OR FRINK DISTRIBUTION YOU ARE RUNNING ON SHOULD PROBABLY BE CONSIDERED BROKEN AND SHOULD NOT BE USED TO RUN FRINK.\n");
                            uRLCodeSource = null;
                        } else {
                            uRLCodeSource = new URLCodeSource(resource);
                        }
                    } else {
                        uRLCodeSource = new URLCodeSource("file:" + this.nonStandardUnits, this.encoding);
                    }
                    if (uRLCodeSource != null) {
                        this.scanner = new UnitLexer(uRLCodeSource, this.env);
                        this.parser.setScanner(this.scanner);
                        this.parser.parse();
                    }
                }
                URL resource2 = cls.getResource("/data/dateformats.txt");
                if (resource2 == null) {
                    System.err.println("Failed to find Frink's standard date formats file, which usually resides inside Frink's jar file (in \"/data/dateformats.txt\".)  If your jar file correctly contains this file, there is probably a bug in your Java implementation's classloader and its implementation of getResource(String) which is failing to look inside the same frink.jar file for resources.\nThe classloader used is the one that your system provides to '" + cls.getName() + "' which is '" + cls.getClassLoader().getClass().getName() + "'.\nAttempting to continue without *any* date formats, so any date processing will probably break.\n\nPlease report the details of the platform you're running on and the text of this error message to eliasen@mindspring.com\n\nTHE PLATFORM OR FRINK DISTRIBUTION YOU ARE RUNNING ON SHOULD PROBABLY BE CONSIDERED BROKEN AND SHOULD NOT BE USED TO RUN FRINK.\n");
                } else {
                    URLCodeSource uRLCodeSource2 = new URLCodeSource(resource2);
                    if (this.scanner == null) {
                        this.scanner = new UnitLexer(uRLCodeSource2, this.env);
                        this.parser.setScanner(this.scanner);
                    } else {
                        this.scanner.reset(uRLCodeSource2);
                    }
                    this.parser.parse();
                }
                Dimension dimensionByName = this.env.getDimensionManager().getDimensionByName("currency");
                if (dimensionByName != null) {
                    String fundamentalUnitName = dimensionByName.getFundamentalUnitName();
                    Unit unit2 = this.env.getUnitManager().getUnit(fundamentalUnitName);
                    if (fundamentalUnitName.equals("dollar")) {
                        unit = this.env.getUnitManager().getUnit(fundamentalUnitName);
                        str2 = fundamentalUnitName;
                        str = "USD";
                    } else if (fundamentalUnitName.equals("Euro") || fundamentalUnitName.equals("euro") || fundamentalUnitName.equals("€")) {
                        unit = this.env.getUnitManager().getUnit(fundamentalUnitName);
                        str2 = fundamentalUnitName;
                        str = "EUR";
                    } else if (fundamentalUnitName.equals("ţ")) {
                        unit = this.env.getUnitManager().getUnit(fundamentalUnitName);
                        str2 = fundamentalUnitName;
                        str = "GBP";
                    } else if (fundamentalUnitName.equals("¥")) {
                        unit = this.env.getUnitManager().getUnit(fundamentalUnitName);
                        str2 = fundamentalUnitName;
                        str = "JPY";
                    } else {
                        unit = unit2;
                        str2 = fundamentalUnitName;
                        str = null;
                    }
                } else {
                    str = null;
                    str2 = null;
                    unit = null;
                }
                Unit unit3 = this.env.getUnitManager().getUnit("troyounce");
                if (!(unit == null || unit3 == null)) {
                    if (str == null) {
                        str = str2;
                    }
                    this.env.getUnitManager().addUnitSource(new CurrencySource(str, unit, unit3, this.env));
                }
                Unit unit4 = this.env.getUnitManager().getUnit("USD");
                if (unit4 != null) {
                    this.env.getUnitManager().addUnitSource(new CPISource(unit4));
                }
                Unit unit5 = this.env.getUnitManager().getUnit("GBP");
                if (unit5 != null) {
                    this.env.getUnitManager().addUnitSource(new PoundSource(unit5));
                }
                this.env.getFunctionManager().addFunctionSource(BuiltinFunctionSource.INSTANCE, false);
                this.env.getFunctionManager().addFunctionSource(new MathFunctionSource(), false);
                this.env.getFunctionManager().addFunctionSource(new TranslatorFunctionSource(), false);
                this.env.getFunctionManager().addFunctionSource(new IOFunctionSource(), false);
                this.env.getFunctionManager().addFunctionSource(new DateFunctionSource(), false);
                this.env.getFunctionManager().addFunctionSource(JavaFunctionSource.INSTANCE, false);
                CodeSource codeSource = this.env.getIncludeManager().getCodeSource("/stdlib/standardinclude.frink", "UTF8", null, this.env);
                if (this.scanner == null) {
                    this.scanner = new UnitLexer(codeSource, this.env);
                    this.parser.setScanner(this.scanner);
                } else {
                    this.scanner.reset(codeSource);
                }
                this.parser.parse();
                this.parser.setFunctionSource(new BasicFunctionSource("UserFunctionSource"));
            } catch (Throwable th) {
                System.err.println(th);
                th.printStackTrace();
            }
            if (this.sandbox) {
                setRestrictiveSecurity(true);
            }
            this.initialized = true;
        }
        return;
    }

    public Environment getEnvironment() {
        return this.env;
    }

    public void setVariable(String str, String str2) {
        doSetExpression(str, new BasicStringExpression(str2));
    }

    public void setVariable(String str, boolean z) {
        doSetExpression(str, FrinkBoolean.create(z));
    }

    public void setVariable(String str, Calendar calendar) {
        doSetExpression(str, new BasicDateExpression(new CalendarDate((Calendar) calendar.clone())));
    }

    public void setVariable(String str, Date date) throws UnknownUnitException {
        doSetExpression(str, new BasicDateExpression(new CalendarDate((Calendar) date.clone())));
    }

    public void setVariable(String str, double d, String str2) throws UnknownUnitException, UnexpectedTypeException {
        doSetVariable(str, new FrinkFloat(d), str2);
    }

    public void setVariable(String str, int i, String str2) throws UnknownUnitException, UnexpectedTypeException {
        doSetVariable(str, FrinkInteger.construct(i), str2);
    }

    public void setVariable(String str, long j, String str2) throws UnknownUnitException, UnexpectedTypeException {
        doSetVariable(str, FrinkInteger.construct(j), str2);
    }

    public void setVariable(String str, BigInteger bigInteger, String str2) throws UnknownUnitException, UnexpectedTypeException {
        doSetVariable(str, FrinkInteger.construct(bigInteger), str2);
    }

    public void setVariable(String str, Object obj) {
        doSetExpression(str, JavaObjectFactory.create(obj));
    }

    public void setVariableMapped(String str, Object obj) {
        doSetExpression(str, JavaMapper.map(obj));
    }

    private void doSetVariable(String str, Numeric numeric, String str2) throws UnknownUnitException, UnexpectedTypeException {
        Expression construct;
        if (this.userVars == null) {
            allocateVars();
        }
        Unit unit = null;
        if (str2 == null || str2.equals("") || (unit = this.env.getUnitManager().getUnit(str2)) != null) {
            if (unit == null) {
                try {
                    construct = DimensionlessUnitExpression.construct(numeric);
                } catch (NumericException e) {
                    throw new UnexpectedTypeException(str, numeric, str2, e);
                }
            } else {
                construct = BasicUnitExpression.construct(UnitMath.multiply(numeric, unit));
            }
            doSetExpression(str, construct);
            return;
        }
        throw new UnknownUnitException(str, str2);
    }

    private void doSetExpression(String str, Expression expression) {
        try {
            this.userVars.setSymbolDefinition(str, expression, this.env);
        } catch (CannotAssignException e) {
            this.env.outputln("Unexpected CannotAssignException in doSetExpression:\n  " + e);
        } catch (FrinkSecurityException e2) {
            this.env.outputln("Unexpected FrinkSecurityException in doSetExpression:\n  " + e2);
        }
    }

    public double getVariableAsDouble(String str, String str2) throws UnknownVariableException, UnexpectedTypeException, UnknownUnitException {
        Numeric variableDividedByUnit = getVariableDividedByUnit(str, str2);
        if (variableDividedByUnit == null) {
            throw new UnknownVariableException(str);
        }
        try {
            return variableDividedByUnit.doubleValue();
        } catch (NotRealException e) {
            throw new UnexpectedTypeException(str, str2, getVariableAsExpression(str), this.env);
        }
    }

    private Numeric getVariableDividedByUnit(String str, String str2) throws UnknownUnitException, UnexpectedTypeException {
        Unit unit;
        Unit variableAsUnit = getVariableAsUnit(str);
        if (variableAsUnit == null) {
            return null;
        }
        if (str2 == null || str2.length() == 0) {
            unit = null;
        } else {
            unit = this.env.getUnitManager().getUnit(str2);
            if (unit == null) {
                throw new UnknownUnitException(str, str2);
            }
        }
        if (unit != null) {
            try {
                variableAsUnit = UnitMath.divide(variableAsUnit, unit);
            } catch (NumericException e) {
                throw new UnexpectedTypeException(str, str2, getVariableAsExpression(str), this.env);
            }
        }
        if (UnitMath.isDimensionless(variableAsUnit)) {
            return variableAsUnit.getScale();
        }
        throw new UnexpectedTypeException(str, str2, getVariableAsExpression(str), this.env);
    }

    public Unit getVariableAsUnit(String str) throws UnexpectedTypeException {
        Expression variableAsExpression = getVariableAsExpression(str);
        if (variableAsExpression == null) {
            return null;
        }
        if (variableAsExpression instanceof UnitExpression) {
            return ((UnitExpression) variableAsExpression).getUnit();
        }
        throw new UnexpectedTypeException(str, UnitExpression.TYPE, variableAsExpression, this.env);
    }

    public Expression getVariableAsExpression(String str) {
        SymbolDefinition symbolDefinition;
        try {
            symbolDefinition = this.env.getSymbolDefinition(str, false);
        } catch (CannotAssignException e) {
            symbolDefinition = null;
        } catch (FrinkSecurityException e2) {
            symbolDefinition = null;
        }
        if (symbolDefinition == null) {
            return null;
        }
        return symbolDefinition.getValue();
    }

    private void allocateVars() {
        if (this.userVars == null) {
            if (!this.initialized) {
                initializeDefaultSources();
            }
            this.userVars = new BasicContextFrame();
            this.env.addContextFrame(this.userVars, false);
        }
    }

    public static void main(String[] strArr) {
        Frink frink2 = new Frink();
        String[] parseArguments = frink2.parseArguments(strArr);
        try {
            if (parseArguments.length > 0) {
                frink2.parseStrings(parseArguments);
            }
            if ((parseArguments.length == 0 && !frink2.filesParsed) || frink2.continueAfterParse) {
                frink2.parseStdInput();
            }
        } catch (FrinkEvaluationException e) {
            System.err.println(e.toString());
        }
    }
}
