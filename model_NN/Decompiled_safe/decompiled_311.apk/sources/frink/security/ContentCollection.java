package frink.security;

import java.util.Enumeration;

public interface ContentCollection extends Content, ContainerNode {
    Enumeration<ContentCollection> getChildCollections();

    Enumeration<Resource> getResources();
}
