package frink.security;

public class ClassVariableResource extends BasicNode implements Content {
    public static final String PREFIX = "ClassVariable:";

    public ClassVariableResource(String str, String str2) {
        super(PREFIX + str + "." + str2);
    }
}
