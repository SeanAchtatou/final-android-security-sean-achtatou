package frink.security;

public class GraphicsWindowResource extends BasicNode implements Content {
    public static final GraphicsWindowResource INSTANCE = new GraphicsWindowResource();
    public static final String PREFIX = "GraphicsWindow";

    private GraphicsWindowResource() {
        super(PREFIX);
    }
}
