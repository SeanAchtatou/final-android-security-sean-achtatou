package frink.security;

import java.util.Enumeration;
import java.util.Hashtable;

public class BasicSource<T> implements Source<T> {
    public String name;
    private Hashtable<String, T> nameTable = new Hashtable<>();

    public BasicSource(String str) {
        this.name = str;
    }

    public T get(String str) {
        return this.nameTable.get(str);
    }

    public Enumeration<String> getNames() {
        return this.nameTable.keys();
    }

    public String getName() {
        return this.name;
    }
}
