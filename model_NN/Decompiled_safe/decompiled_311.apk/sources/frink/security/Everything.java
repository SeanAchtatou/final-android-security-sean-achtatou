package frink.security;

import java.util.Enumeration;

public class Everything extends Every implements ContentCollection {
    public static final Everything INSTANCE = new Everything();
    private static final String NAME = "EVERYTHING";

    protected Everything() {
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public String getName() {
        return NAME;
    }

    public Enumeration<ContentCollection> getChildCollections() {
        return null;
    }

    public Enumeration<Resource> getResources() {
        return null;
    }
}
