package frink.security;

import java.io.File;
import java.net.URL;

public class NoSecurityHelper implements SecurityHelper {
    public static final NoSecurityHelper INSTANCE = new NoSecurityHelper();

    private NoSecurityHelper() {
    }

    public void checkRead(URL url) {
    }

    public void checkUnsafeEval() {
    }

    public void checkUse() {
    }

    public void checkNewJava(String str) {
    }

    public void checkCallJava(String str) {
    }

    public void checkStaticJava(String str) {
    }

    public void checkDefineFunction() {
    }

    public void checkSetGlobalFlag() {
    }

    public void checkPrint() {
    }

    public void checkWrite(File file) {
    }

    public void checkOpenGraphicsWindow() {
    }

    public void checkConstructExpression() {
    }

    public void checkTransformExpression() {
    }

    public void checkCreateTransformationRule() {
    }

    public void checkSetClassLevelVariable(String str, String str2) {
    }
}
