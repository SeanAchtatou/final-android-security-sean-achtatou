package frink.security;

public class DefineFunctionResource extends BasicNode implements Resource {
    public static final DefineFunctionResource INSTANCE = new DefineFunctionResource();
    private static final String NAME = "DefineFunction";

    private DefineFunctionResource() {
        super(NAME);
    }
}
