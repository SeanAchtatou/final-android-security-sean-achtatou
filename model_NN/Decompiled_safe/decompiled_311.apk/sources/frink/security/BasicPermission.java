package frink.security;

public class BasicPermission implements Permission {
    private boolean allow;
    private Content content;
    private PermissionType permissionType;
    private Principal principal;

    public BasicPermission(Content content2, Principal principal2, PermissionType permissionType2, boolean z) {
        this.content = content2;
        this.principal = principal2;
        this.permissionType = permissionType2;
        this.allow = z;
    }

    public Principal getPrincipal() {
        return this.principal;
    }

    public Content getContent() {
        return this.content;
    }

    public PermissionType getPermissionType() {
        return this.permissionType;
    }

    public boolean getAllow() {
        return this.allow;
    }

    public boolean matches(Principal principal2) {
        return this.principal.equals(principal2);
    }

    public boolean matches(Content content2) {
        return this.content.equals(content2);
    }

    public boolean matches(PermissionType permissionType2) {
        return this.permissionType.equals(permissionType2);
    }

    public boolean matches(boolean z) {
        return this.allow == z;
    }

    public boolean equals(Permission permission) {
        return matches(permission.getPrincipal()) && matches(permission.getContent()) && matches(permission.getPermissionType()) && matches(permission.getAllow());
    }

    public boolean representsPermission(Principal principal2, Content content2, PermissionType permissionType2) throws DeniesAccessException {
        if (!this.principal.implies(principal2) || !this.content.implies(content2) || !this.permissionType.implies(permissionType2)) {
            return false;
        }
        if (this.allow) {
            return true;
        }
        throw new DeniesAccessException("Permission Denied, Principal=" + principal2.getName() + " Content=" + content2.getName() + " PermissionType=" + this.permissionType.getName());
    }
}
