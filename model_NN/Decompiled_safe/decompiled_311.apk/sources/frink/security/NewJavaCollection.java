package frink.security;

public class NewJavaCollection extends Everything implements ContentCollection {
    public static final NewJavaCollection INSTANCE = new NewJavaCollection();
    private static final String NAME = "NewJavaCollection";

    private NewJavaCollection() {
    }

    public boolean implies(Node node) {
        if (node.getName().startsWith(NewJavaResource.PREFIX)) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NAME;
    }
}
