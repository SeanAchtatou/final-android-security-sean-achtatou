package frink.security;

public interface ContentCollectionFactory {
    ContentCollection createContentCollection(String str) throws ExistsException, CannotCreateException;
}
