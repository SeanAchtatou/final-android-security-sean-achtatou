package frink.security;

import frink.errors.Cat;
import frink.errors.Log;
import frink.errors.Sev;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class NonTerminalNode extends BasicNode implements ContainerNode {
    private Vector<ContainerNode> containerChildren = null;
    private Hashtable<String, Node> terminalChildren = null;

    NonTerminalNode(String str) {
        super(str);
    }

    public boolean implies(Node node) {
        if (super.implies(node)) {
            return true;
        }
        if (node.isContainer()) {
            if (this.containerChildren == null) {
                return false;
            }
            if (this.containerChildren.contains(node)) {
                return true;
            }
        } else if (this.terminalChildren != null && this.terminalChildren.containsKey(node.getName())) {
            return true;
        }
        if (this.containerChildren == null) {
            return false;
        }
        int size = this.containerChildren.size();
        int i = 0;
        while (i < size) {
            try {
                if (this.containerChildren.elementAt(i).implies(node)) {
                    return true;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.message("NonTerminalNode.implies", Sev.WARNING, Cat.SecurityCode, "NonTerminalNode.implies: Concurrent modification of " + getName());
            }
        }
        return false;
    }

    public void addChild(Node node) throws CannotAlterException {
        testForCycle(node);
        if (node.isContainer()) {
            addToContainerChildren((ContainerNode) node);
        } else {
            addToTerminalChildren(node);
        }
    }

    /* access modifiers changed from: protected */
    public void testForCycle(Node node) throws CycleExistsException {
        if (node.isContainer() && node.implies(this)) {
            String str = "NonTerminalNode.addChild: attempted to add group " + getName() + " to a group which would cause " + node.getName() + " to contain itself.";
            Log.message("NonTerminalNode.addChild", Sev.WARNING, Cat.SecurityCode, str);
            throw new CycleExistsException(str);
        }
    }

    public void removeChild(Node node) throws CannotAlterException {
        if (node.isContainer()) {
            removeFromContainerChildren((ContainerNode) node);
        } else {
            removeFromTerminalChildren(node);
        }
    }

    public Enumeration<ContainerNode> getContainerChildren() {
        if (this.containerChildren == null) {
            return null;
        }
        return this.containerChildren.elements();
    }

    public Enumeration<Node> getTerminalChildren() {
        if (this.terminalChildren == null) {
            return null;
        }
        return this.terminalChildren.elements();
    }

    private void addToContainerChildren(ContainerNode containerNode) {
        if (this.containerChildren == null) {
            instantiateContainerChildren();
        }
        int indexOf = this.containerChildren.indexOf(containerNode);
        if (indexOf == -1) {
            this.containerChildren.addElement(containerNode);
        } else if (this.containerChildren.elementAt(indexOf) != containerNode) {
            Log.message("NonTerminalNode.addToContainerChildren", Sev.WARNING, Cat.SecurityCode, "NonTerminalNode.addToContainerChildren: replacing child of " + getName() + " with different child " + containerNode.getName() + ".");
            this.containerChildren.setElementAt(containerNode, indexOf);
        }
    }

    private void removeFromContainerChildren(ContainerNode containerNode) throws CannotAlterException {
        if (this.containerChildren == null || !this.containerChildren.removeElement(containerNode)) {
            String str = "NonTerminalNode.removeFromContainerChildren: Attempted to remove nonexistent child node " + containerNode.getName() + " from " + getName();
            Log.message("NonTerminalNode.removeFromContainerChildren", Sev.WARNING, Cat.SecurityCode, str);
            throw new CannotAlterException(str);
        }
    }

    private void removeFromTerminalChildren(Node node) throws CannotAlterException {
        if (this.terminalChildren == null || this.terminalChildren.remove(node.getName()) == null) {
            String str = "NonTerminalNode.removeFromTerminalChildren: Attempted to remove nonexistent child node " + node.getName() + " from " + getName();
            Log.message("NonTerminalNode.removeFromTerminalChildren", Sev.WARNING, Cat.SecurityCode, str);
            throw new CannotAlterException(str);
        }
    }

    private void instantiateContainerChildren() {
        this.containerChildren = new Vector<>(1);
    }

    private void addToTerminalChildren(Node node) {
        Node node2 = null;
        if (this.terminalChildren == null) {
            instantiateTerminalChildren();
        } else {
            node2 = this.terminalChildren.get(node.getName());
        }
        if (node != node2) {
            if (node2 != null) {
                Log.message("NonTerminalNode.addToTerminalChildren", Sev.WARNING, Cat.SecurityCode, "NonTerminalNode.addToTerminalChildren: replacing child of " + getName() + " with different child " + node.getName() + ".");
            }
            this.terminalChildren.put(node.getName(), node);
        }
    }

    private void instantiateTerminalChildren() {
        this.terminalChildren = new Hashtable<>(5);
    }

    public boolean isContainer() {
        return true;
    }
}
