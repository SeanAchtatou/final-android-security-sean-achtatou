package frink.security;

public class CycleExistsException extends CannotAlterException {
    public CycleExistsException(String str) {
        super(str);
    }
}
