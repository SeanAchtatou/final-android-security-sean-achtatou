package frink.security;

public interface Authenticator {
    String getName();

    boolean matches(String str, String str2);
}
