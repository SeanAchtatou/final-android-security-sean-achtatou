package frink.security;

public class BasicPermissionType implements PermissionType {
    private String fullName;
    private String shortName;

    BasicPermissionType(String str, String str2) {
        this.fullName = str;
        this.shortName = str2;
    }

    public String getName() {
        return this.fullName;
    }

    public String getShortName() {
        return this.shortName;
    }

    public boolean implies(PermissionType permissionType) {
        return equals(permissionType);
    }
}
