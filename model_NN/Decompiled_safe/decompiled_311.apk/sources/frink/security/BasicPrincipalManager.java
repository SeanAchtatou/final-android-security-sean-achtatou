package frink.security;

public class BasicPrincipalManager implements PrincipalManager {
    private Manager<Group> groupManager;
    private Manager<User> userManager;

    BasicPrincipalManager(Manager<Group> manager, Manager<User> manager2) {
        this.groupManager = manager;
        this.userManager = manager2;
    }

    public Principal get(String str) {
        Principal principal = this.groupManager.get(str);
        return principal != null ? principal : this.userManager.get(str);
    }
}
