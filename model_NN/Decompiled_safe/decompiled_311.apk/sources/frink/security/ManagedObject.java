package frink.security;

public interface ManagedObject {
    boolean equals(Object obj);

    String getName();

    int hashCode();
}
