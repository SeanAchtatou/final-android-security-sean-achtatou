package frink.security;

public class CallJavaResource extends BasicNode implements Content {
    public static final String PREFIX = "callJava:";

    public CallJavaResource(String str) {
        super(PREFIX + str);
    }
}
