package frink.security;

public class CannotAlterException extends Exception {
    public CannotAlterException(String str) {
        super(str);
    }
}
