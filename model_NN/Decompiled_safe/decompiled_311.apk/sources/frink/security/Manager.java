package frink.security;

import java.util.Enumeration;

public interface Manager<T> {
    void addSource(Source<T> source);

    T get(String str);

    Enumeration<String> getNames();

    void removeSource(String str);
}
