package frink.security;

import frink.expr.FrinkSecurityException;
import java.io.File;
import java.net.URL;

public class FrinkSecurityHelper implements SecurityHelper {
    private PermissionManager permissionManager;
    private User user;

    public FrinkSecurityHelper(PermissionManager permissionManager2, User user2) {
        this.permissionManager = permissionManager2;
        this.user = user2;
    }

    public PermissionManager getPermissionManager() {
        return this.permissionManager;
    }

    public void checkRead(URL url) throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, new URLResource(url), PermissionTypeManager.READ)) {
            throw new FrinkSecurityException("Cannot read URL: " + url.toString(), null);
        }
    }

    public void checkUnsafeEval() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, UnsafeEvalResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot perform unsafeEval", null);
        }
    }

    public void checkUse() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, UseResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot include file.", null);
        }
    }

    public void checkNewJava(String str) throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, new NewJavaResource(str), PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot create Java object: " + str, null);
        }
    }

    public void checkCallJava(String str) throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, new CallJavaResource(str), PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot call methods on Java class: " + str, null);
        }
    }

    public void checkStaticJava(String str) throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, new StaticJavaResource(str), PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot access static fields in class: " + str, null);
        }
    }

    public void checkDefineFunction() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, DefineFunctionResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot define functions due to security policy.", null);
        }
    }

    public void checkSetGlobalFlag() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, SetGlobalFlagResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot set global flags due to security policy.", null);
        }
    }

    public void checkPrint() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, PrintResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot print to printers due to security policy.", null);
        }
    }

    public void checkWrite(File file) throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, new FileResource(file), PermissionTypeManager.WRITE)) {
            throw new FrinkSecurityException("Cannot write to '" + file.getAbsolutePath() + "' due to security policy.", null);
        }
    }

    public void checkOpenGraphicsWindow() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, GraphicsWindowResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot create new graphics window due to security policy.", null);
        }
    }

    public void checkConstructExpression() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, ConstructExpressionResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot construct a new expression due to security policy.", null);
        }
    }

    public void checkTransformExpression() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, TransformExpressionResource.INSTANCE, PermissionTypeManager.EXEC)) {
            throw new FrinkSecurityException("Cannot transform expression due to security policy.", null);
        }
    }

    public void checkCreateTransformationRule() throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, TransformationRuleResource.INSTANCE, PermissionTypeManager.WRITE)) {
            throw new FrinkSecurityException("Cannot create a new transformation rule due to security policy.", null);
        }
    }

    public void checkSetClassLevelVariable(String str, String str2) throws FrinkSecurityException {
        if (!this.permissionManager.hasPermission(this.user, new ClassVariableResource(str, str2), PermissionTypeManager.WRITE)) {
            throw new FrinkSecurityException("Cannot access class-level variable " + str + "." + str2 + " due to security policy.", null);
        }
    }
}
