package frink.security;

public class TransformExpressionResource extends BasicNode implements Content {
    public static final TransformExpressionResource INSTANCE = new TransformExpressionResource();
    public static final String PREFIX = "TransformExpression";

    private TransformExpressionResource() {
        super(PREFIX);
    }
}
