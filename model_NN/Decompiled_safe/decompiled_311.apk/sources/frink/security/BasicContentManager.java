package frink.security;

public class BasicContentManager implements ContentManager {
    private Manager<ContentCollection> contentCollectionManager;
    private Manager<Resource> resourceManager;

    BasicContentManager(Manager<ContentCollection> manager, Manager<Resource> manager2) {
        this.contentCollectionManager = manager;
        this.resourceManager = manager2;
    }

    public Content get(String str) {
        Content content = this.contentCollectionManager.get(str);
        return content != null ? content : this.resourceManager.get(str);
    }
}
