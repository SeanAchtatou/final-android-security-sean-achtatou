package frink.security;

import java.util.Enumeration;
import java.util.Hashtable;

public class PermissionTypeManager {
    public static final PermissionType ALL = AllPermissions.getInstance();
    public static ImpliedPermissionType EDIT = new ImpliedPermissionType("EDIT", "E");
    public static final PermissionType EXEC = new BasicPermissionType("EXEC", "X");
    public static final PermissionType READ = new BasicPermissionType("READ", "R");
    public static final PermissionType WRITE = new BasicPermissionType("WRITE", "W");
    private static Hashtable<String, PermissionType> fullNameHash = new Hashtable<>();
    private static Hashtable<String, PermissionType> shortNameHash = new Hashtable<>();

    static {
        addPermissionType(READ);
        EDIT.addImpliedPermission(READ);
        addPermissionType(EDIT);
        addPermissionType(ALL);
    }

    private static void addPermissionType(PermissionType permissionType) {
        fullNameHash.put(permissionType.getName(), permissionType);
        shortNameHash.put(permissionType.getShortName(), permissionType);
    }

    public static Enumeration<PermissionType> getPermissionTypes() {
        return fullNameHash.elements();
    }

    public static PermissionType getByFullName(String str) {
        return fullNameHash.get(str);
    }

    public static PermissionType getByShortName(String str) {
        return shortNameHash.get(str);
    }
}
