package frink.security;

public abstract class BasicNode implements Node {
    private String name;

    BasicNode(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    public boolean isContainer() {
        return false;
    }

    public boolean implies(Node node) {
        return equals(node);
    }
}
