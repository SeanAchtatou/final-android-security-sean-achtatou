package frink.security;

import frink.errors.Cat;
import frink.errors.Log;
import frink.errors.Sev;
import java.util.Enumeration;

public abstract class Every implements ContainerNode {
    public void addChild(Node node) throws CannotAlterException {
        Log.message("Every.addChild", Sev.WARNING, Cat.SecurityCode, "Every.addChild: Attempted to add " + node.getName() + " to magical group.");
        throw new CannotAlterException("Every.addChild: cannot alter.");
    }

    public void removeChild(Node node) throws CannotAlterException {
        Log.message("Every.removeChild", Sev.WARNING, Cat.SecurityCode, "Every.removeChild: Attempted to remove " + node.getName() + " from magical group.");
        throw new CannotAlterException("Every.removeChild: cannot alter.");
    }

    public Enumeration<ContainerNode> getContainerChildren() {
        return null;
    }

    public Enumeration<Node> getTerminalChildren() {
        return null;
    }

    public boolean isContainer() {
        return true;
    }

    public boolean implies(Node node) {
        return true;
    }
}
