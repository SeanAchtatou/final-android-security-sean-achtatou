package frink.security;

import java.util.Enumeration;

public class Everyone extends Every implements Group {
    public static final Everyone INSTANCE = new Everyone();
    private static final String NAME = "EVERYONE";

    private Everyone() {
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public String getName() {
        return NAME;
    }

    public Enumeration<Group> getChildGroups() {
        return null;
    }

    public Enumeration<User> getUsers() {
        return null;
    }
}
