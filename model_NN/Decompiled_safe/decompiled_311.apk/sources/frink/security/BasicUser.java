package frink.security;

import frink.errors.Cat;
import frink.errors.Log;
import frink.errors.Sev;
import java.util.Enumeration;
import java.util.Hashtable;

public class BasicUser extends BasicNode implements User {
    private Hashtable<String, String> properties = null;

    public BasicUser(String str) {
        super(str);
    }

    public boolean equals(Object obj) {
        if (obj instanceof User) {
            return ((User) obj).getName().equals(getName());
        }
        return false;
    }

    public int hashCode() {
        return getName().hashCode();
    }

    public void setProperty(String str, String str2) {
        if (str == null) {
            Log.message("BasicUser.setProperty", Sev.WARNING, Cat.SecurityCode, "BasicUser.setProperty: key is null!");
            return;
        }
        if (this.properties == null) {
            instantiateProperties();
        }
        if (str2 == null) {
            this.properties.remove(str);
        } else {
            this.properties.put(str, str2);
        }
    }

    public String getProperty(String str) {
        if (this.properties == null) {
            return null;
        }
        return this.properties.get(str);
    }

    public Enumeration<String> getProperties() {
        if (this.properties == null) {
            return null;
        }
        return this.properties.keys();
    }

    private void instantiateProperties() {
        this.properties = new Hashtable<>(7);
    }
}
