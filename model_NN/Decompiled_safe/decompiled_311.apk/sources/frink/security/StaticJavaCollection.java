package frink.security;

public class StaticJavaCollection extends Everything implements ContentCollection {
    public static final StaticJavaCollection INSTANCE = new StaticJavaCollection();
    private static final String NAME = "StaticJavaCollection";

    private StaticJavaCollection() {
    }

    public boolean implies(Node node) {
        if (node.getName().startsWith(StaticJavaResource.PREFIX)) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NAME;
    }
}
