package frink.security;

import frink.errors.Cat;
import frink.errors.Log;
import frink.errors.Sev;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Vector;

public class BasicManager<T> implements Manager<T> {
    private Vector<Source<T>> sources = new Vector<>(4);

    BasicManager() {
    }

    public T get(String str) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                T t = this.sources.elementAt(i).get(str);
                if (t != null) {
                    return t;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.message("AbstractManager.get.concurrent", Sev.RECOVERABLE_ERROR, Cat.SecurityCode, "AbstractManager.get: Concurrent modification of sources.");
                return null;
            }
        }
        return null;
    }

    public Enumeration<String> getNames() {
        return new ObjectEnumeration();
    }

    /* access modifiers changed from: private */
    public Enumeration<Source<T>> getSourceEnumeration() {
        return this.sources.elements();
    }

    public void addSource(Source<T> source) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                if (this.sources.elementAt(i).getName().equals(source.getName())) {
                    Log.message("AbstractManager.addSource.exists", Sev.RECOVERABLE_ERROR, Cat.SecurityCode, "AbstractManager.addSource: source " + source.getName() + " already exists in manager.");
                    return;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.message("AbstractManager.addSource.concurrent", Sev.RECOVERABLE_ERROR, Cat.SecurityCode, "AbstractManager.addSource: Concurrent modification of sources.");
            }
        }
        this.sources.addElement(source);
    }

    public void removeSource(String str) {
        int size = this.sources.size();
        int i = 0;
        while (i < size) {
            try {
                if (this.sources.elementAt(i).getName().equals(str)) {
                    this.sources.removeElementAt(i);
                    return;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.message("AbstractManager.removeSource.concurrent", Sev.RECOVERABLE_ERROR, Cat.SecurityCode, "AbstractManager.removeSource: Concurrent modification of sources.");
            }
        }
        Log.message("AbstractManager.removeSource.nonexistent", Sev.RECOVERABLE_ERROR, Cat.SecurityCode, "AbstractManager.removeSource: source " + str + " does not exist in manager.");
    }

    protected class ObjectEnumeration implements Enumeration<String> {
        private Enumeration<String> objIter;
        private Enumeration<Source<T>> sourceIter;

        protected ObjectEnumeration() {
            this.sourceIter = BasicManager.this.getSourceEnumeration();
            getNextSource();
        }

        public boolean hasMoreElements() {
            if (this.objIter != null && this.objIter.hasMoreElements()) {
                return true;
            }
            getNextSource();
            return this.sourceIter != null;
        }

        public String nextElement() {
            if (this.objIter != null && this.objIter.hasMoreElements()) {
                return this.objIter.nextElement();
            }
            getNextSource();
            if (this.sourceIter != null) {
                return this.objIter.nextElement();
            }
            Log.message("AbstractManager.ObjectEnumeration.nextElement", Sev.WARNING, Cat.SecurityCode, "AbstractManager.ObjectEnumeration.nextElement:  no more objects exist. ");
            throw new NoSuchElementException("AbstractManager.ObjectEnumeration.nextElement:  no more objects exist. ");
        }

        private void getNextSource() {
            if (this.sourceIter != null) {
                while (this.sourceIter.hasMoreElements()) {
                    Source nextElement = this.sourceIter.nextElement();
                    if (nextElement != null) {
                        this.objIter = nextElement.getNames();
                        if (this.objIter != null && this.objIter.hasMoreElements()) {
                            return;
                        }
                    }
                }
                this.sourceIter = null;
                this.objIter = null;
            }
        }
    }
}
