package frink.security;

public class StaticJavaResource extends BasicNode implements Content {
    public static final String PREFIX = "staticJava:";

    public StaticJavaResource(String str) {
        super(PREFIX + str);
    }
}
