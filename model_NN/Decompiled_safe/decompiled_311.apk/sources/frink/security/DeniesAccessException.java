package frink.security;

public class DeniesAccessException extends Exception {
    public DeniesAccessException(String str) {
        super(str);
    }
}
