package frink.security;

import frink.expr.EnumerationStacker;
import java.util.Enumeration;

public class BasicPermissionManager implements PermissionManager {
    private FrinkSubPermissionManager allowTable = new FrinkSubPermissionManager();
    private boolean allowThenDeny;
    private FrinkSubPermissionManager denyTable = new FrinkSubPermissionManager();

    public BasicPermissionManager(boolean z) {
        this.allowThenDeny = z;
    }

    public Permission exists(Principal principal, Content content, PermissionType permissionType, boolean z) {
        if (z) {
            return this.allowTable.exists(principal, content, permissionType, z);
        }
        return this.denyTable.exists(principal, content, permissionType, z);
    }

    public boolean hasPermission(Principal principal, Content content, PermissionType permissionType) {
        if (this.allowThenDeny) {
            return hasPermissionAllowThenDeny(principal, content, permissionType);
        }
        return hasPermissionDenyThenAllow(principal, content, permissionType);
    }

    private boolean hasPermissionAllowThenDeny(Principal principal, Content content, PermissionType permissionType) {
        try {
            if (!this.allowTable.hasPermission(principal, content, permissionType)) {
                return false;
            }
            this.denyTable.hasPermission(principal, content, permissionType);
            return true;
        } catch (DeniesAccessException e) {
            return false;
        }
    }

    private boolean hasPermissionDenyThenAllow(Principal principal, Content content, PermissionType permissionType) {
        try {
            this.denyTable.hasPermission(principal, content, permissionType);
            return this.allowTable.hasPermission(principal, content, permissionType);
        } catch (DeniesAccessException e) {
            return false;
        }
    }

    public Enumeration<Permission> getPermissions(Principal principal) {
        EnumerationStacker enumerationStacker = new EnumerationStacker();
        enumerationStacker.addEnumeration(this.allowTable.getPermissions(principal));
        enumerationStacker.addEnumeration(this.denyTable.getPermissions(principal));
        return enumerationStacker;
    }

    public Enumeration<Permission> getPermissions(Principal principal, PermissionType permissionType) {
        return new PermissionTypeFilter(getPermissions(principal), permissionType);
    }

    public void addPermission(Permission permission) {
        if (permission.getAllow()) {
            this.allowTable.addPermission(permission);
        } else {
            this.denyTable.addPermission(permission);
        }
    }

    public void removePermission(Permission permission) {
        if (permission.getAllow()) {
            this.allowTable.removePermission(permission);
        } else {
            this.denyTable.removePermission(permission);
        }
    }
}
