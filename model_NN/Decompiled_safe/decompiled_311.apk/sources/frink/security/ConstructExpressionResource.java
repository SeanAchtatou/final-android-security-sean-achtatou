package frink.security;

public class ConstructExpressionResource extends BasicNode implements Content {
    public static final ConstructExpressionResource INSTANCE = new ConstructExpressionResource();
    public static final String PREFIX = "ConstructExpression";

    private ConstructExpressionResource() {
        super(PREFIX);
    }
}
