package frink.security;

public class NewJavaResource extends BasicNode implements Content {
    public static final String PREFIX = "newJava:";

    public NewJavaResource(String str) {
        super(PREFIX + str);
    }
}
