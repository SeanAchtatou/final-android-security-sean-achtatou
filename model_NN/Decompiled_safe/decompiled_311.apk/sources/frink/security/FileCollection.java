package frink.security;

public class FileCollection extends Everything implements ContentCollection {
    public static final FileCollection INSTANCE = new FileCollection();
    private static final String NAME = "FileCollection";

    private FileCollection() {
    }

    public boolean implies(Node node) {
        if (node.getName().startsWith(FileResource.PREFIX)) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NAME;
    }
}
