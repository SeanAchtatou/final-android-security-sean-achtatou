package frink.security;

import java.util.Hashtable;

public class ImpliedPermissionType extends BasicPermissionType {
    private Hashtable<PermissionType, PermissionType> impliedChildren = null;

    ImpliedPermissionType(String str, String str2) {
        super(str, str2);
    }

    /* access modifiers changed from: package-private */
    public void addImpliedPermission(PermissionType permissionType) {
        if (this.impliedChildren == null) {
            this.impliedChildren = new Hashtable<>(5, 0.75f);
        }
        this.impliedChildren.put(permissionType, permissionType);
    }

    public boolean implies(PermissionType permissionType) {
        if (super.implies(permissionType)) {
            return true;
        }
        if (this.impliedChildren != null) {
            return this.impliedChildren.containsKey(permissionType);
        }
        return false;
    }
}
