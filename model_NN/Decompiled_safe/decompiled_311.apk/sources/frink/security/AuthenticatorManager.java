package frink.security;

import java.util.Enumeration;

public interface AuthenticatorManager {
    void addAuthenticator(Authenticator authenticator);

    Enumeration<Authenticator> getEnumeration();

    boolean matches(String str, String str2);

    void removeAuthenticator(String str);
}
