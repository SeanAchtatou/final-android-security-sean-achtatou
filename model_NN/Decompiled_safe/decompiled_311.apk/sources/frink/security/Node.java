package frink.security;

public interface Node extends ManagedObject {
    boolean implies(Node node);

    boolean isContainer();
}
