package frink.security;

public class FileURLCollection extends Everything implements ContentCollection {
    public static final FileURLCollection INSTANCE = new FileURLCollection();
    private static final String NAME = "FileURLCollection";

    private FileURLCollection() {
    }

    public boolean implies(Node node) {
        if (node.getName().startsWith("URL:file")) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NAME;
    }
}
