package frink.security;

public class JavaContentCollection extends BasicContentCollection {
    public static final JavaContentCollection INSTANCE = new JavaContentCollection();

    static {
        try {
            INSTANCE.addChild(NewJavaCollection.INSTANCE);
            INSTANCE.addChild(CallJavaCollection.INSTANCE);
            INSTANCE.addChild(StaticJavaCollection.INSTANCE);
        } catch (CannotAlterException e) {
            System.out.println("Huh? " + e);
        }
    }

    private JavaContentCollection() {
        super("JavaContentCollection");
    }
}
