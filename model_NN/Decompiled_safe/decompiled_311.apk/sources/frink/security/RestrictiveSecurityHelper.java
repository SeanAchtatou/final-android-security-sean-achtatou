package frink.security;

public class RestrictiveSecurityHelper extends FrinkSecurityHelper {
    public static final RestrictiveSecurityHelper INSTANCE = new RestrictiveSecurityHelper();

    private RestrictiveSecurityHelper() {
        super(new BasicPermissionManager(true), new BasicUser("WebUser"));
        PermissionManager permissionManager = getPermissionManager();
        permissionManager.addPermission(new BasicPermission(Everything.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, true));
        permissionManager.addPermission(new BasicPermission(FileURLCollection.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.READ, false));
        permissionManager.addPermission(new BasicPermission(UnsafeEvalResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.EXEC, false));
        permissionManager.addPermission(new BasicPermission(UseResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.EXEC, false));
        permissionManager.addPermission(new BasicPermission(JavaContentCollection.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(DefineFunctionResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(SetGlobalFlagResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(PrintResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(FileCollection.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.WRITE, false));
        permissionManager.addPermission(new BasicPermission(GraphicsWindowResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(ConstructExpressionResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(TransformationRuleResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(TransformExpressionResource.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.ALL, false));
        permissionManager.addPermission(new BasicPermission(ClassVariableCollection.INSTANCE, Everyone.INSTANCE, PermissionTypeManager.WRITE, false));
    }
}
