package frink.security;

public class CallJavaCollection extends Everything implements ContentCollection {
    public static final CallJavaCollection INSTANCE = new CallJavaCollection();
    private static final String NAME = "CallJavaCollection";

    private CallJavaCollection() {
    }

    public boolean implies(Node node) {
        if (node.getName().startsWith(CallJavaResource.PREFIX)) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NAME;
    }
}
