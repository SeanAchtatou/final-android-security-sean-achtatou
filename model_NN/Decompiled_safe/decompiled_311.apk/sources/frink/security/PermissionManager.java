package frink.security;

import java.util.Enumeration;

public interface PermissionManager {
    void addPermission(Permission permission);

    Permission exists(Principal principal, Content content, PermissionType permissionType, boolean z);

    Enumeration<Permission> getPermissions(Principal principal);

    Enumeration<Permission> getPermissions(Principal principal, PermissionType permissionType);

    boolean hasPermission(Principal principal, Content content, PermissionType permissionType);

    void removePermission(Permission permission);
}
