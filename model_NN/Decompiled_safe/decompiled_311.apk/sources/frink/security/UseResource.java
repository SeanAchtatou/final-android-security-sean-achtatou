package frink.security;

public class UseResource extends BasicNode implements Resource {
    public static final UseResource INSTANCE = new UseResource();
    private static final String NAME = "Use";

    private UseResource() {
        super(NAME);
    }
}
