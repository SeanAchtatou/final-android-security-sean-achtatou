package frink.security;

import java.util.Enumeration;
import java.util.Hashtable;

class MagicGroupSource implements Source<Group> {
    private static final String NAME = "MagicGroupSource";
    private Hashtable<String, Group> groups = new Hashtable<>(5);

    public MagicGroupSource() {
        Everyone everyone = Everyone.INSTANCE;
        this.groups.put(everyone.getName(), everyone);
    }

    public Group get(String str) {
        return this.groups.get(str);
    }

    public Enumeration<String> getNames() {
        return this.groups.keys();
    }

    public String getName() {
        return NAME;
    }
}
