package frink.security;

public class SetGlobalFlagResource extends BasicNode implements Resource {
    public static final SetGlobalFlagResource INSTANCE = new SetGlobalFlagResource();
    private static final String NAME = "SetGlobalFlag";

    private SetGlobalFlagResource() {
        super(NAME);
    }
}
