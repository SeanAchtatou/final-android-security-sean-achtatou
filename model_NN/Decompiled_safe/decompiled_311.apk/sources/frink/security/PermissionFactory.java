package frink.security;

public interface PermissionFactory {
    Permission createPermission(Content content, Principal principal, PermissionType permissionType, boolean z) throws ExistsException;
}
