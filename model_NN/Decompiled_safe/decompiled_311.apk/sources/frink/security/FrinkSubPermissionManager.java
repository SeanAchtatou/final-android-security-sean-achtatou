package frink.security;

import frink.expr.EnumerationStacker;
import java.util.Enumeration;

public class FrinkSubPermissionManager {
    private PermissionTable contentGroups = new PermissionTable();
    private PermissionTable contentTerminals = new PermissionTable();
    private PermissionTable principalGroups = new PermissionTable();
    private PermissionTable principalTerminals = new PermissionTable();

    FrinkSubPermissionManager() {
    }

    public Permission exists(Principal principal, Content content, PermissionType permissionType, boolean z) {
        if (content.isContainer()) {
            return this.contentGroups.getPermission(content.getName(), principal, content, permissionType, z);
        }
        return this.contentTerminals.getPermission(content.getName(), principal, content, permissionType, z);
    }

    public boolean hasPermission(Principal principal, Content content, PermissionType permissionType) throws DeniesAccessException {
        if (!content.isContainer()) {
            if (this.contentTerminals.hasPermission(content.getName(), principal, content, permissionType)) {
                return true;
            }
        } else if (this.contentGroups.hasPermission(content.getName(), principal, content, permissionType)) {
            return true;
        }
        return this.contentGroups.hasPermissionAnywhere(principal, content, permissionType);
    }

    public Enumeration<Permission> getPermissions(Principal principal) {
        EnumerationStacker enumerationStacker = new EnumerationStacker();
        if (!principal.isContainer()) {
            enumerationStacker.addEnumeration(this.principalTerminals.getObjectEnumeration(principal.getName()));
        }
        enumerationStacker.addEnumeration(this.principalGroups.createPrincipalEnumeration(principal));
        return enumerationStacker;
    }

    public Enumeration<Permission> getPermissions(Principal principal, PermissionType permissionType) {
        return new PermissionTypeFilter(getPermissions(principal), permissionType);
    }

    public void addPermission(Permission permission) {
        Principal principal = permission.getPrincipal();
        if (principal.isContainer()) {
            this.principalGroups.addObject(principal.getName(), permission);
        } else {
            this.principalTerminals.addObject(principal.getName(), permission);
        }
        Content content = permission.getContent();
        if (content.isContainer()) {
            this.contentGroups.addObject(content.getName(), permission);
        } else {
            this.contentTerminals.addObject(content.getName(), permission);
        }
    }

    public void removePermission(Permission permission) {
        Principal principal = permission.getPrincipal();
        if (principal.isContainer()) {
            this.principalGroups.removeObject(principal.getName(), permission);
        } else {
            this.principalTerminals.removeObject(principal.getName(), permission);
        }
        Content content = permission.getContent();
        if (content.isContainer()) {
            this.contentGroups.removeObject(content.getName(), permission);
        } else {
            this.contentTerminals.removeObject(content.getName(), permission);
        }
    }
}
