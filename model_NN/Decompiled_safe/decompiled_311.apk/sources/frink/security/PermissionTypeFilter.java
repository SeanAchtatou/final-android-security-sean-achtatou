package frink.security;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class PermissionTypeFilter implements Enumeration<Permission> {
    private PermissionType filtPT;
    private Permission nextPermission;
    private Enumeration<Permission> origEnum;

    public PermissionTypeFilter(Enumeration<Permission> enumeration, PermissionType permissionType) {
        this.origEnum = enumeration;
        this.filtPT = permissionType;
        advanceToNext();
    }

    public boolean hasMoreElements() {
        return this.nextPermission != null;
    }

    public Permission nextElement() {
        if (this.nextPermission == null) {
            throw new NoSuchElementException("PermissionTypeFilter.nextElement: Requested nonexistent element");
        }
        Permission permission = this.nextPermission;
        advanceToNext();
        return permission;
    }

    private void advanceToNext() {
        while (this.origEnum != null && this.origEnum.hasMoreElements()) {
            Permission nextElement = this.origEnum.nextElement();
            if (nextElement.getPermissionType().implies(this.filtPT)) {
                this.nextPermission = nextElement;
                return;
            }
        }
        this.nextPermission = null;
        this.origEnum = null;
    }
}
