package frink.security;

import java.net.URL;

public class URLResource extends BasicNode implements Content {
    public static final String PREFIX = "URL:";

    public URLResource(URL url) {
        super(PREFIX + url.toString());
    }
}
