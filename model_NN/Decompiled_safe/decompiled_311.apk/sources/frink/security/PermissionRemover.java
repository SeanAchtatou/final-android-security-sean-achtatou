package frink.security;

public interface PermissionRemover {
    void removePermission(Content content, Principal principal, PermissionType permissionType, boolean z) throws ExistsException;
}
