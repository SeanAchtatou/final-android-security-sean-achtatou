package frink.security;

import frink.errors.Cat;
import frink.errors.Log;
import frink.errors.Sev;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;

class PermissionTable {
    /* access modifiers changed from: private */
    public Hashtable<String, Vector<Permission>> theHash = new Hashtable<>(100, 0.75f);

    public void addObject(String str, Permission permission) {
        Vector vector = this.theHash.get(str);
        if (vector == null) {
            Vector vector2 = new Vector(1);
            vector2.addElement(permission);
            this.theHash.put(str, vector2);
        } else if (!vector.contains(permission)) {
            vector.addElement(permission);
        }
    }

    public Permission getPermission(String str, Principal principal, Content content, PermissionType permissionType, boolean z) {
        Vector vector = this.theHash.get(str);
        if (vector == null) {
            return null;
        }
        int size = vector.size();
        int i = 0;
        while (i < size) {
            try {
                Permission permission = (Permission) vector.elementAt(i);
                if (permission.matches(principal) && permission.matches(content) && permission.matches(permissionType) && permission.matches(z)) {
                    return permission;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.message("FrinkPermissionManager.PermissionTable.getPermission", Sev.WARNING, Cat.SecurityCode, "FrinkPermissionManager.PermissionTable.getPermission: Concurrent modification.");
            }
        }
        return null;
    }

    public boolean removeObject(String str, Permission permission) {
        if (!this.theHash.containsKey(str)) {
            return false;
        }
        Vector vector = this.theHash.get(str);
        vector.removeElement(permission);
        if (vector.size() == 0) {
            this.theHash.remove(str);
        }
        return true;
    }

    public Enumeration<Permission> getObjectEnumeration(String str) {
        Vector vector = this.theHash.get(str);
        if (vector != null) {
            return vector.elements();
        }
        return null;
    }

    public boolean hasPermission(String str, Principal principal, Content content, PermissionType permissionType) throws DeniesAccessException {
        Vector vector = this.theHash.get(str);
        if (vector == null) {
            return false;
        }
        int size = vector.size();
        int i = 0;
        while (i < size) {
            try {
                if (((Permission) vector.elementAt(i)).representsPermission(principal, content, permissionType)) {
                    return true;
                }
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.message("FrinkPermissionManager.PermissionTable.hasPermission", Sev.WARNING, Cat.SecurityCode, "FrinkPermissionManager.PermissionTable.hasPermission: Concurrent modification.");
            }
        }
        return false;
    }

    public boolean hasPermissionAnywhere(Principal principal, Content content, PermissionType permissionType) throws DeniesAccessException {
        Content content2;
        Enumeration<Vector<Permission>> elements = this.theHash.elements();
        while (elements.hasMoreElements()) {
            Vector nextElement = elements.nextElement();
            Permission permission = (Permission) nextElement.elementAt(0);
            if (!(permission == null || (content2 = permission.getContent()) == null || !content2.implies(content))) {
                int size = nextElement.size();
                int i = 0;
                while (i < size) {
                    try {
                        if (((Permission) nextElement.elementAt(i)).representsPermission(principal, content, permissionType)) {
                            return true;
                        }
                        i++;
                    } catch (ArrayIndexOutOfBoundsException e) {
                        Log.message("FrinkPermissionManager.PermissionTable.hasPermissionAnywhere", Sev.WARNING, Cat.SecurityCode, "FrinkPermissionManager.PermissionTable.hasPermissionAnywhere: Concurrent modification.");
                    }
                }
                continue;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Enumeration<Permission> createPrincipalEnumeration(Principal principal) {
        return new PrincipalEnumeration(principal);
    }

    private class PrincipalEnumeration implements Enumeration<Permission> {
        private Principal applicant;
        private Enumeration<Vector<Permission>> hashElems;
        private Permission nextPermission;
        private Vector<Permission> vec = null;
        private int vecIndex = -1;

        public PrincipalEnumeration(Principal principal) {
            this.applicant = principal;
            this.hashElems = PermissionTable.this.theHash.elements();
            advanceToNextPermission();
        }

        public boolean hasMoreElements() {
            return this.nextPermission != null;
        }

        public Permission nextElement() {
            if (this.nextPermission == null) {
                throw new NoSuchElementException("FrinkPermissionManager.PermissionTable.PrincipalEnumeration: Requested nonexistent element");
            }
            Permission permission = this.nextPermission;
            advanceToNextPermission();
            return permission;
        }

        private void advanceToNextPermission() {
            if (this.hashElems == null) {
                this.nextPermission = null;
                this.vec = null;
                return;
            }
            do {
                if (this.vec != null) {
                    if (this.vecIndex < this.vec.size()) {
                        Vector<Permission> vector = this.vec;
                        int i = this.vecIndex;
                        this.vecIndex = i + 1;
                        this.nextPermission = vector.elementAt(i);
                        return;
                    }
                    this.vec = null;
                }
                if (!this.hashElems.hasMoreElements()) {
                    this.vec = null;
                    this.hashElems = null;
                    this.nextPermission = null;
                    return;
                }
                this.vec = this.hashElems.nextElement();
                if (this.vec == null || this.vec.size() < 1) {
                    this.vec = null;
                    continue;
                } else if (this.vec.elementAt(0).getPrincipal().implies(this.applicant)) {
                    this.vecIndex = 0;
                    continue;
                } else {
                    this.vec = null;
                    continue;
                }
            } while (0 == 0);
        }
    }
}
