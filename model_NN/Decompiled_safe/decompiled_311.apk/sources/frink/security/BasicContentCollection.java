package frink.security;

import java.util.Enumeration;

public class BasicContentCollection extends NonTerminalNode implements ContentCollection {
    public BasicContentCollection(String str) {
        super(str);
    }

    public Enumeration<ContentCollection> getChildCollections() {
        return null;
    }

    public Enumeration<Resource> getResources() {
        return null;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ContentCollection) {
            return ((ContentCollection) obj).getName().equals(getName());
        }
        return false;
    }
}
