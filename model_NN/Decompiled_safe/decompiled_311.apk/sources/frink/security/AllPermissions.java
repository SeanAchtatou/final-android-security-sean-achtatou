package frink.security;

public class AllPermissions implements PermissionType {
    private static String NAME = "ALL";
    private static String SHORT_NAME = "*";
    private static final AllPermissions instance = new AllPermissions();

    private AllPermissions() {
    }

    public static PermissionType getInstance() {
        return instance;
    }

    public String getName() {
        return NAME;
    }

    public String getShortName() {
        return SHORT_NAME;
    }

    public boolean implies(PermissionType permissionType) {
        return true;
    }
}
