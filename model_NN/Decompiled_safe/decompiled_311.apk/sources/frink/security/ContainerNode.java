package frink.security;

import java.util.Enumeration;

public interface ContainerNode extends Node {
    void addChild(Node node) throws CannotAlterException;

    Enumeration<ContainerNode> getContainerChildren();

    Enumeration<Node> getTerminalChildren();

    void removeChild(Node node) throws CannotAlterException;
}
