package frink.security;

public interface ContentManager {
    Content get(String str);
}
