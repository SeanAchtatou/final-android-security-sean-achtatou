package frink.security;

import java.util.Enumeration;

public interface Group extends Principal {
    Enumeration<Group> getChildGroups();

    Enumeration<User> getUsers();
}
