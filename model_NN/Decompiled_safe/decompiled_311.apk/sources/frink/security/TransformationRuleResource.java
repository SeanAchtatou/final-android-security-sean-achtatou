package frink.security;

public class TransformationRuleResource extends BasicNode implements Content {
    public static final TransformationRuleResource INSTANCE = new TransformationRuleResource();
    public static final String PREFIX = "TransformationRule";

    private TransformationRuleResource() {
        super("TransformationRule");
    }
}
