package frink.security;

import java.util.Enumeration;
import java.util.Hashtable;

class MagicCollectionSource implements Source<ContentCollection> {
    private static final String NAME = "MagicCollectionSource";
    private Hashtable<String, ContentCollection> collections = new Hashtable<>(5);

    public MagicCollectionSource() {
        Everything everything = Everything.INSTANCE;
        this.collections.put(everything.getName(), everything);
    }

    public ContentCollection get(String str) {
        return this.collections.get(str);
    }

    public Enumeration<String> getNames() {
        return this.collections.keys();
    }

    public String getName() {
        return NAME;
    }
}
