package frink.security;

public class CannotCreateException extends Exception {
    public CannotCreateException(String str) {
        super(str);
    }
}
