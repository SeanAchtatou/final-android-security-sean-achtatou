package frink.security;

public interface PermissionType {
    String getName();

    String getShortName();

    boolean implies(PermissionType permissionType);
}
