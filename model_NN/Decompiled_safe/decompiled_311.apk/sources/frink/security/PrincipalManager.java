package frink.security;

public interface PrincipalManager {
    Principal get(String str);
}
