package frink.security;

public class UnsafeEvalResource extends BasicNode implements Resource {
    public static final UnsafeEvalResource INSTANCE = new UnsafeEvalResource();
    private static final String NAME = "UnsafeEval";

    private UnsafeEvalResource() {
        super(NAME);
    }
}
