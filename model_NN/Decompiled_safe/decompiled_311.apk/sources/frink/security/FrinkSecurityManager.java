package frink.security;

public interface FrinkSecurityManager {
    ContentCollectionFactory getContentCollectionFactory();

    Manager<ContentCollection> getContentCollectionManager();

    ContentManager getContentManager();

    Manager<Group> getGroupManager();

    PermissionFactory getPermissionFactory();

    PermissionManager getPermissionManager();

    PermissionRemover getPermissionRemover();

    PrincipalManager getPrincipalManager();

    Manager<Resource> getResourceManager();

    Manager<User> getUserManager();
}
