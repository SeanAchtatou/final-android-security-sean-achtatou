package frink.security;

import frink.expr.FrinkSecurityException;
import java.io.File;
import java.net.URL;

public interface SecurityHelper {
    void checkCallJava(String str) throws FrinkSecurityException;

    void checkConstructExpression() throws FrinkSecurityException;

    void checkCreateTransformationRule() throws FrinkSecurityException;

    void checkDefineFunction() throws FrinkSecurityException;

    void checkNewJava(String str) throws FrinkSecurityException;

    void checkOpenGraphicsWindow() throws FrinkSecurityException;

    void checkPrint() throws FrinkSecurityException;

    void checkRead(URL url) throws FrinkSecurityException;

    void checkSetClassLevelVariable(String str, String str2) throws FrinkSecurityException;

    void checkSetGlobalFlag() throws FrinkSecurityException;

    void checkStaticJava(String str) throws FrinkSecurityException;

    void checkTransformExpression() throws FrinkSecurityException;

    void checkUnsafeEval() throws FrinkSecurityException;

    void checkUse() throws FrinkSecurityException;

    void checkWrite(File file) throws FrinkSecurityException;
}
