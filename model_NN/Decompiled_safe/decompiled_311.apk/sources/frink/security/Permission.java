package frink.security;

public interface Permission {
    boolean equals(Permission permission);

    boolean getAllow();

    Content getContent();

    PermissionType getPermissionType();

    Principal getPrincipal();

    boolean matches(Content content);

    boolean matches(PermissionType permissionType);

    boolean matches(Principal principal);

    boolean matches(boolean z);

    boolean representsPermission(Principal principal, Content content, PermissionType permissionType) throws DeniesAccessException;
}
