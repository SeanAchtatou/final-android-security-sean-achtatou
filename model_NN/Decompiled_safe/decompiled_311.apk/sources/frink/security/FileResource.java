package frink.security;

import java.io.File;

public class FileResource extends BasicNode implements Content {
    public static final String PREFIX = "File:";

    public FileResource(File file) {
        super(PREFIX + file.getAbsolutePath());
    }
}
