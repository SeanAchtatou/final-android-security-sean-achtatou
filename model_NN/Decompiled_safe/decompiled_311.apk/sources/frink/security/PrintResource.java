package frink.security;

public class PrintResource extends BasicNode implements Content {
    public static final PrintResource INSTANCE = new PrintResource();
    public static final String PREFIX = "Print";

    private PrintResource() {
        super(PREFIX);
    }
}
