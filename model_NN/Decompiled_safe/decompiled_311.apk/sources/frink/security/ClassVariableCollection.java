package frink.security;

public class ClassVariableCollection extends Everything implements ContentCollection {
    public static final ClassVariableCollection INSTANCE = new ClassVariableCollection();
    private static final String NAME = "ClassVariableCollection";

    private ClassVariableCollection() {
    }

    public boolean implies(Node node) {
        if (node.getName().startsWith(ClassVariableResource.PREFIX)) {
            return true;
        }
        return false;
    }

    public String getName() {
        return NAME;
    }
}
