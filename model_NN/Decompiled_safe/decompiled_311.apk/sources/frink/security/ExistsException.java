package frink.security;

public class ExistsException extends Exception {
    public ExistsException(String str) {
        super(str);
    }
}
