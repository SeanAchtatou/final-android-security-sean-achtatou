package frink.security;

import java.util.Enumeration;

public interface User extends Principal {
    Enumeration<String> getProperties();

    String getProperty(String str);
}
