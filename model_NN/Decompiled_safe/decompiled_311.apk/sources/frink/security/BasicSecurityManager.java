package frink.security;

public class BasicSecurityManager implements FrinkSecurityManager {
    private ContentCollectionFactory contentCollectionFactory = null;
    private Manager<ContentCollection> contentCollectionManager;
    private ContentManager contentManager;
    private Manager<Group> groupManager = new BasicManager();
    private PermissionFactory permissionFactory = null;
    private PermissionManager permissionManager;
    private PermissionRemover permissionRemover = null;
    private PrincipalManager principalManager;
    private Manager<Resource> resourceManager;
    private Manager<User> userManager = new BasicManager();

    public BasicSecurityManager() {
        this.groupManager.addSource(new MagicGroupSource());
        this.principalManager = new BasicPrincipalManager(this.groupManager, this.userManager);
        this.resourceManager = new BasicManager();
        this.contentCollectionManager = new BasicManager();
        this.contentCollectionManager.addSource(new MagicCollectionSource());
        this.contentManager = new BasicContentManager(this.contentCollectionManager, this.resourceManager);
        this.permissionManager = new BasicPermissionManager(true);
    }

    public Manager<User> getUserManager() {
        return this.userManager;
    }

    public Manager<Group> getGroupManager() {
        return this.groupManager;
    }

    public PrincipalManager getPrincipalManager() {
        return this.principalManager;
    }

    public Manager<Resource> getResourceManager() {
        return this.resourceManager;
    }

    public Manager<ContentCollection> getContentCollectionManager() {
        return this.contentCollectionManager;
    }

    public ContentCollectionFactory getContentCollectionFactory() {
        return this.contentCollectionFactory;
    }

    public ContentManager getContentManager() {
        return this.contentManager;
    }

    public PermissionManager getPermissionManager() {
        return this.permissionManager;
    }

    public PermissionFactory getPermissionFactory() {
        return this.permissionFactory;
    }

    public PermissionRemover getPermissionRemover() {
        return this.permissionRemover;
    }
}
