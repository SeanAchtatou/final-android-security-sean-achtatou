package frink.numeric;

import frink.errors.NotAnIntegerException;
import java.math.BigInteger;
import java.util.Random;

public class RealMath {
    public static final double LN_2 = Math.log(2.0d);
    public static FrinkRational half;

    static {
        try {
            half = (FrinkRational) FrinkRational.construct(FrinkInt.ONE, FrinkInt.TWO);
        } catch (InvalidDenominatorException e) {
        }
    }

    public static FrinkReal add(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        if (frinkReal.isFrinkInteger()) {
            if (frinkReal2.isFrinkInteger()) {
                return addInts((FrinkInteger) frinkReal, (FrinkInteger) frinkReal2);
            }
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(new FrinkBigDecimal(((FrinkInteger) frinkReal).getBigInt()).add(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational = (FrinkRational) frinkReal2;
                return FrinkRational.construct(addInts(multiplyInts((FrinkInteger) frinkReal, frinkRational.getDenominator()), frinkRational.getNumerator()), frinkRational.getDenominator());
            }
        }
        if (frinkReal.isFloat()) {
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().add(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isFrinkInteger()) {
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().add(new FrinkBigDecimal(((FrinkInteger) frinkReal2).getBigInt()), mathContext));
            }
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational2 = (FrinkRational) frinkReal2;
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().add(div(FrinkBigDecimal.construct(frinkRational2.getNumerator()), FrinkBigDecimal.construct(frinkRational2.getDenominator()), mathContext), mathContext));
            }
        }
        if (frinkReal.isRational()) {
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational3 = (FrinkRational) frinkReal;
                FrinkRational frinkRational4 = (FrinkRational) frinkReal2;
                return FrinkRational.construct(addInts(multiplyInts(frinkRational3.getNumerator(), frinkRational4.getDenominator()), multiplyInts(frinkRational4.getNumerator(), frinkRational3.getDenominator())), multiplyInts(frinkRational3.getDenominator(), frinkRational4.getDenominator()));
            } else if (frinkReal2.isFrinkInteger()) {
                FrinkRational frinkRational5 = (FrinkRational) frinkReal;
                return FrinkRational.construct(addInts(frinkRational5.getNumerator(), multiplyInts((FrinkInteger) frinkReal2, frinkRational5.getDenominator())), frinkRational5.getDenominator());
            } else if (frinkReal2.isFloat()) {
                FrinkRational frinkRational6 = (FrinkRational) frinkReal;
                return new FrinkFloat(div(FrinkBigDecimal.construct(frinkRational6.getNumerator()), FrinkBigDecimal.construct(frinkRational6.getDenominator()), mathContext).add(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
        }
        throw new NotImplementedException("Unknown types in RealMath.add: " + frinkReal.toString() + "+" + frinkReal2.toString(), true);
    }

    public static FrinkReal subtract(FrinkReal frinkReal, FrinkReal frinkReal2) throws NumericException {
        return subtract(frinkReal, frinkReal2, NumericMath.mc);
    }

    public static FrinkReal subtract(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        if (frinkReal.isFrinkInteger()) {
            FrinkInteger frinkInteger = (FrinkInteger) frinkReal;
            if (frinkReal2.isFrinkInteger()) {
                return subtractInts(frinkInteger, (FrinkInteger) frinkReal2);
            }
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(FrinkBigDecimal.construct(frinkInteger).subtract(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational = (FrinkRational) frinkReal2;
                return FrinkRational.construct(subtractInts(multiplyInts(frinkInteger, frinkRational.getDenominator()), frinkRational.getNumerator()), frinkRational.getDenominator());
            }
        }
        if (frinkReal.isFloat()) {
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().subtract(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isFrinkInteger()) {
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().subtract(FrinkBigDecimal.construct((FrinkInteger) frinkReal2), mathContext));
            }
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational2 = (FrinkRational) frinkReal2;
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().subtract(div(FrinkBigDecimal.construct(frinkRational2.getNumerator()), FrinkBigDecimal.construct(frinkRational2.getDenominator()), mathContext), mathContext));
            }
        }
        if (frinkReal.isRational()) {
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational3 = (FrinkRational) frinkReal;
                FrinkRational frinkRational4 = (FrinkRational) frinkReal2;
                return FrinkRational.construct(subtractInts(multiplyInts(frinkRational3.getNumerator(), frinkRational4.getDenominator()), multiplyInts(frinkRational4.getNumerator(), frinkRational3.getDenominator())), multiplyInts(frinkRational3.getDenominator(), frinkRational4.getDenominator()));
            } else if (frinkReal2.isFrinkInteger()) {
                FrinkRational frinkRational5 = (FrinkRational) frinkReal;
                return FrinkRational.construct(subtractInts(frinkRational5.getNumerator(), multiplyInts((FrinkInteger) frinkReal2, frinkRational5.getDenominator())), frinkRational5.getDenominator());
            } else if (frinkReal2.isFloat()) {
                FrinkRational frinkRational6 = (FrinkRational) frinkReal;
                return new FrinkFloat(div(FrinkBigDecimal.construct(frinkRational6.getNumerator()), FrinkBigDecimal.construct(frinkRational6.getDenominator()), mathContext).subtract(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
        }
        throw new NotImplementedException("Unknown types in RealMath.subtract: " + frinkReal.toString() + "-" + frinkReal2.toString(), true);
    }

    public static FrinkReal multiply(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        if (frinkReal == FrinkInt.ZERO || frinkReal2 == FrinkInt.ZERO) {
            return FrinkInt.ZERO;
        }
        if (frinkReal.isFrinkInteger()) {
            FrinkInteger frinkInteger = (FrinkInteger) frinkReal;
            if (frinkReal2.isFrinkInteger()) {
                return multiplyInts(frinkInteger, (FrinkInteger) frinkReal2);
            }
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(FrinkBigDecimal.construct(frinkInteger).multiply(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational = (FrinkRational) frinkReal2;
                return FrinkRational.construct(multiplyInts(frinkInteger, frinkRational.getNumerator()), frinkRational.getDenominator());
            }
        }
        if (frinkReal.isFloat()) {
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().multiply(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isFrinkInteger()) {
                try {
                    if (((FrinkInteger) frinkReal2).getInt() == 1) {
                        return frinkReal;
                    }
                } catch (NotAnIntegerException e) {
                }
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().multiply(FrinkBigDecimal.construct((FrinkInteger) frinkReal2), mathContext));
            } else if (frinkReal2.isRational()) {
                FrinkRational frinkRational2 = (FrinkRational) frinkReal2;
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().multiply(div(FrinkBigDecimal.construct(frinkRational2.getNumerator()), FrinkBigDecimal.construct(frinkRational2.getDenominator()), mathContext), mathContext));
            }
        }
        if (frinkReal.isRational()) {
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational3 = (FrinkRational) frinkReal;
                FrinkRational frinkRational4 = (FrinkRational) frinkReal2;
                return FrinkRational.construct(multiplyInts(frinkRational3.getNumerator(), frinkRational4.getNumerator()), multiplyInts(frinkRational3.getDenominator(), frinkRational4.getDenominator()));
            } else if (frinkReal2.isFrinkInteger()) {
                try {
                    if (((FrinkInteger) frinkReal2).getInt() == 1) {
                        return frinkReal;
                    }
                } catch (NotAnIntegerException e2) {
                }
                FrinkRational frinkRational5 = (FrinkRational) frinkReal;
                return FrinkRational.construct(multiplyInts(frinkRational5.getNumerator(), (FrinkInteger) frinkReal2), frinkRational5.getDenominator());
            } else if (frinkReal2.isFloat()) {
                FrinkRational frinkRational6 = (FrinkRational) frinkReal;
                return new FrinkFloat(div(FrinkBigDecimal.construct(frinkRational6.getNumerator()), FrinkBigDecimal.construct(frinkRational6.getDenominator()), mathContext).multiply(((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
        }
        throw new NotImplementedException("Unknown types in RealMath.multiply: " + frinkReal.toString() + "*" + frinkReal2.toString(), true);
    }

    public static FrinkReal divide(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        if (frinkReal == frinkReal2) {
            return FrinkInt.ONE;
        }
        if (frinkReal.isFrinkInteger()) {
            FrinkInteger frinkInteger = (FrinkInteger) frinkReal;
            if (frinkReal2.isFrinkInteger()) {
                try {
                    if (((FrinkInteger) frinkReal2).getInt() == 1) {
                        return frinkReal;
                    }
                } catch (NotAnIntegerException e) {
                }
                return FrinkRational.construct(frinkInteger, (FrinkInteger) frinkReal2);
            } else if (frinkReal2.isFloat()) {
                return new FrinkFloat(div(new FrinkBigDecimal(((FrinkInteger) frinkReal).getBigInt()), ((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            } else {
                if (frinkReal2.isRational()) {
                    FrinkRational frinkRational = (FrinkRational) frinkReal2;
                    return FrinkRational.construct(multiplyInts(frinkInteger, frinkRational.getDenominator()), frinkRational.getNumerator());
                }
            }
        }
        if (frinkReal.isFloat()) {
            if (frinkReal2.isFloat()) {
                return new FrinkFloat(div(((FrinkFloat) frinkReal).getBigDec(), ((FrinkFloat) frinkReal2).getBigDec(), mathContext));
            }
            if (frinkReal2.isFrinkInteger()) {
                try {
                    if (((FrinkInteger) frinkReal2).getInt() == 1) {
                        return frinkReal;
                    }
                } catch (NotAnIntegerException e2) {
                }
                return new FrinkFloat(div(((FrinkFloat) frinkReal).getBigDec(), FrinkBigDecimal.construct((FrinkInteger) frinkReal2), mathContext));
            } else if (frinkReal2.isRational()) {
                FrinkRational frinkRational2 = (FrinkRational) frinkReal2;
                return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().multiply(div(FrinkBigDecimal.construct(frinkRational2.getDenominator()), FrinkBigDecimal.construct(frinkRational2.getNumerator()), mathContext), mathContext));
            }
        }
        if (frinkReal.isRational()) {
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational3 = (FrinkRational) frinkReal;
                FrinkRational frinkRational4 = (FrinkRational) frinkReal2;
                return FrinkRational.construct(multiplyInts(frinkRational3.getNumerator(), frinkRational4.getDenominator()), multiplyInts(frinkRational3.getDenominator(), frinkRational4.getNumerator()));
            } else if (frinkReal2.isFrinkInteger()) {
                try {
                    if (((FrinkInteger) frinkReal2).getInt() == 1) {
                        return frinkReal;
                    }
                } catch (NotAnIntegerException e3) {
                }
                FrinkRational frinkRational5 = (FrinkRational) frinkReal;
                return FrinkRational.construct(frinkRational5.getNumerator(), multiplyInts(frinkRational5.getDenominator(), (FrinkInteger) frinkReal2));
            } else if (frinkReal2.isFloat()) {
                FrinkRational frinkRational6 = (FrinkRational) frinkReal;
                return new FrinkFloat(div(FrinkBigDecimal.construct(frinkRational6.getNumerator()), FrinkBigDecimal.construct(frinkRational6.getDenominator()).multiply(((FrinkFloat) frinkReal2).getBigDec(), mathContext), mathContext));
            }
        }
        throw new NotImplementedException("Unknown types in RealMath.divide: " + frinkReal.toString() + "/" + frinkReal2.toString(), true);
    }

    public static Numeric power(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        int i;
        FrinkInteger frinkInteger;
        int lowestSetBit;
        if (frinkReal.isInt() && ((FrinkInt) frinkReal).getInt() == 1) {
            return FrinkInt.ONE;
        }
        if (frinkReal2.isFrinkInteger()) {
            try {
                int i2 = ((FrinkInteger) frinkReal2).getInt();
                if (i2 == 1) {
                    return frinkReal;
                }
                if (i2 == 0) {
                    return FrinkInt.ONE;
                }
                if (frinkReal.isFrinkInteger()) {
                    FrinkInteger frinkInteger2 = (FrinkInteger) frinkReal;
                    if (i2 < 0) {
                        return FrinkRational.construct(FrinkInt.ONE, (FrinkInteger) power(frinkReal, FrinkInt.construct(-i2), mathContext));
                    }
                    BigInteger bigInt = frinkInteger2.getBigInt();
                    if (bigInt.signum() <= 0 || (lowestSetBit = bigInt.getLowestSetBit()) <= 0) {
                        return FrinkInteger.construct(bigInt.pow(i2));
                    }
                    BigInteger shiftRight = bigInt.shiftRight(lowestSetBit);
                    BigInteger shiftLeft = FrinkBigInteger.ONE.shiftLeft(lowestSetBit * i2);
                    if (shiftRight.equals(FrinkBigInteger.ONE)) {
                        return FrinkInteger.construct(shiftLeft);
                    }
                    return FrinkInteger.construct(shiftRight.pow(i2).multiply(shiftLeft));
                } else if (frinkReal.isRational()) {
                    FrinkRational frinkRational = (FrinkRational) frinkReal;
                    FrinkInteger numerator = frinkRational.getNumerator();
                    FrinkInteger denominator = frinkRational.getDenominator();
                    if (i2 < 0) {
                        FrinkInteger frinkInteger3 = denominator;
                        i = -i2;
                        frinkInteger = frinkInteger3;
                    } else {
                        FrinkInteger frinkInteger4 = denominator;
                        i = i2;
                        frinkInteger = numerator;
                        numerator = frinkInteger4;
                    }
                    return FrinkRational.construct(FrinkInteger.construct(frinkInteger.getBigInt().pow(i)), FrinkInteger.construct(numerator.getBigInt().pow(i)));
                } else if (frinkReal.isFloat()) {
                    try {
                        return new FrinkFloat(((FrinkFloat) frinkReal).getBigDec().pow(new FrinkBigDecimal(((FrinkInteger) frinkReal2).getBigInt()), mathContext));
                    } catch (ArithmeticException e) {
                    }
                }
            } catch (NotAnIntegerException e2) {
                throw new NotImplementedException("RealMath:power: exponent " + frinkReal2.toString() + " is too large for my little head to handle.", false);
            }
        }
        if (frinkReal2.isRational()) {
            FrinkRational frinkRational2 = (FrinkRational) frinkReal2;
            if (frinkReal.isFrinkInteger()) {
                try {
                    int i3 = frinkRational2.getNumerator().getInt();
                    int i4 = frinkRational2.getDenominator().getInt();
                    if (i3 == 1) {
                        return integerRoot((FrinkInteger) frinkReal, i4);
                    }
                    Numeric power = power(frinkReal, frinkRational2.getNumerator(), mathContext);
                    if (power.isFrinkInteger()) {
                        return integerRoot((FrinkInteger) power, i4);
                    }
                } catch (NotAnIntegerException e3) {
                }
            }
            if (frinkReal.isRational()) {
                FrinkRational frinkRational3 = (FrinkRational) frinkReal;
                return NumericMath.divide(power(frinkRational3.getNumerator(), frinkReal2, mathContext), power(frinkRational3.getDenominator(), frinkReal2, mathContext), mathContext);
            }
            FrinkInteger numerator2 = frinkRational2.getNumerator();
            FrinkInteger denominator2 = frinkRational2.getDenominator();
            try {
                if (numerator2.getInt() != 1) {
                    Numeric power2 = power(frinkReal, numerator2, mathContext);
                    if (power2.isReal()) {
                        try {
                            if (((FrinkReal) power2).doubleValue() != 0.0d) {
                                return power((FrinkReal) power2, FrinkRational.construct(1, denominator2.getInt()), mathContext);
                            }
                        } catch (NumberFormatException e4) {
                            Numeric power3 = power(frinkReal, FrinkRational.construct(1, denominator2.getInt()), mathContext);
                            if (power3.isReal()) {
                                return power((FrinkReal) power3, numerator2, mathContext);
                            }
                        }
                    }
                }
            } catch (NotAnIntegerException e5) {
            }
            try {
                if ((frinkRational2.getDenominator().getInt() & 1) == 1) {
                    double d = -Math.pow(-frinkReal.doubleValue(), frinkReal2.doubleValue());
                    if (Double.isNaN(d)) {
                        return ComplexMath.power(frinkReal, frinkReal2, mathContext);
                    }
                    return new FrinkFloat(new FrinkBigDecimal(d));
                }
            } catch (NotAnIntegerException e6) {
            }
        }
        double doubleValue = frinkReal.doubleValue();
        double pow = Math.pow(doubleValue, frinkReal2.doubleValue());
        if (Double.isNaN(pow)) {
            return ComplexMath.power(frinkReal, frinkReal2, mathContext);
        }
        if (!Double.isInfinite(pow) && pow != 0.0d) {
            return new FrinkFloat(new FrinkBigDecimal(pow));
        }
        FrinkBigDecimal bigDec = frinkReal2.getFrinkFloatValue(mathContext).getBigDec();
        FrinkBigDecimal divideInteger = bigDec.divideInteger(FrinkBigDecimal.ONE);
        return new FrinkFloat(frinkReal.getFrinkFloatValue(mathContext).getBigDec().pow(divideInteger, mathContext).multiply(new FrinkBigDecimal(Math.pow(doubleValue, bigDec.subtract(divideInteger, mathContext).doubleValue())), mathContext));
    }

    public static FrinkReal sin(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.sin(frinkReal.doubleValue()));
    }

    public static FrinkReal arcsin(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.asin(frinkReal.doubleValue()));
    }

    public static FrinkReal cos(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.cos(frinkReal.doubleValue()));
    }

    public static FrinkReal arccos(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.acos(frinkReal.doubleValue()));
    }

    public static FrinkReal tan(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.tan(frinkReal.doubleValue()));
    }

    public static FrinkReal arctan(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.atan(frinkReal.doubleValue()));
    }

    public static FrinkReal arctan(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) {
        return new FrinkFloat(Math.atan2(frinkReal.doubleValue(), frinkReal2.doubleValue()));
    }

    public static FrinkReal arcsec(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.acos(1.0d / frinkReal.doubleValue()));
    }

    public static FrinkReal arccsc(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.asin(1.0d / frinkReal.doubleValue()));
    }

    public static FrinkReal arccot(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.atan(1.0d / frinkReal.doubleValue()));
    }

    public static FrinkReal sinh(FrinkReal frinkReal, MathContext mathContext) {
        double doubleValue = frinkReal.doubleValue();
        return new FrinkFloat((Math.exp(doubleValue) - Math.exp(-doubleValue)) / 2.0d);
    }

    public static FrinkReal cosh(FrinkReal frinkReal, MathContext mathContext) {
        double doubleValue = frinkReal.doubleValue();
        return new FrinkFloat((Math.exp(-doubleValue) + Math.exp(doubleValue)) / 2.0d);
    }

    public static FrinkBigDecimal div(FrinkBigDecimal frinkBigDecimal, FrinkBigDecimal frinkBigDecimal2, MathContext mathContext) {
        return frinkBigDecimal.divide(frinkBigDecimal2, mathContext);
    }

    public static FrinkReal abs(FrinkReal frinkReal, MathContext mathContext) {
        if (frinkReal.realSignum() < 0) {
            return frinkReal.negate();
        }
        return frinkReal;
    }

    public static int getIntegerValue(FrinkReal frinkReal) throws NotAnIntegerException {
        if (frinkReal.isFrinkInteger()) {
            return ((FrinkInteger) frinkReal).getInt();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static long getLongValue(FrinkReal frinkReal) throws NotAnIntegerException {
        if (frinkReal.isFrinkInteger()) {
            return ((FrinkInteger) frinkReal).getLong();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static BigInteger getBigIntegerValue(FrinkReal frinkReal) throws NotAnIntegerException {
        if (frinkReal.isFrinkInteger()) {
            return ((FrinkInteger) frinkReal).getBigInt();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkInteger addInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(((long) frinkInteger.getInt()) + ((long) frinkInteger2.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().add(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger subtractInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(((long) frinkInteger.getInt()) - ((long) frinkInteger2.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().subtract(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger multiplyInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            int i = frinkInteger.getInt();
            int i2 = frinkInteger2.getInt();
            if (i == 1) {
                return frinkInteger2;
            }
            if (i2 == 1) {
                return frinkInteger;
            }
            return FrinkInteger.construct(((long) i2) * ((long) i));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().multiply(frinkInteger2.getBigInt()));
        }
    }

    public static double approxLog2(FrinkReal frinkReal) throws NumericException {
        if (frinkReal.isInt()) {
            int i = ((FrinkInt) frinkReal).getInt();
            if (i == 0) {
                return 0.0d;
            }
            return Math.log((double) i) / LN_2;
        } else if (frinkReal.isFrinkInteger()) {
            double doubleValue = ((FrinkInteger) frinkReal).getBigInt().doubleValue();
            if (Double.isInfinite(doubleValue)) {
                return (double) (((FrinkInteger) frinkReal).getBigInt().bitLength() - 1);
            }
            return Math.log(doubleValue) / LN_2;
        } else if (frinkReal.isFloat()) {
            return (double) ((FrinkFloat) frinkReal).getBigDec().approxLog2();
        } else {
            if (frinkReal.isRational()) {
                FrinkRational frinkRational = (FrinkRational) frinkReal;
                return approxLog2(frinkRational.getNumerator()) - approxLog2(frinkRational.getDenominator());
            }
            throw new NotImplementedException("RealMath.approxLog2 passed invalid argument " + frinkReal.toString(), true);
        }
    }

    private static Numeric integerSquareRoot(FrinkInteger frinkInteger) throws NotAnIntegerException, NumericException {
        if (frinkInteger.isInt()) {
            int i = frinkInteger.getInt();
            if (i >= 0) {
                return integerSquareRoot(i);
            }
            return FrinkComplex.construct(FrinkInt.ZERO, integerSquareRoot(-i));
        }
        BigInteger bigInt = frinkInteger.getBigInt();
        if (bigInt.compareTo(FrinkBigInteger.ZERO) >= 0) {
            return integerSquareRoot(bigInt);
        }
        return FrinkComplex.construct(FrinkInt.ZERO, integerSquareRoot(bigInt.negate()));
    }

    private static FrinkInteger integerSquareRoot(BigInteger bigInteger) throws NotAnIntegerException {
        BigInteger nearestIntegerSquareRoot = nearestIntegerSquareRoot(bigInteger);
        if (nearestIntegerSquareRoot.multiply(nearestIntegerSquareRoot).compareTo(bigInteger) == 0) {
            return FrinkInteger.construct(nearestIntegerSquareRoot);
        }
        throw NotAnIntegerException.INSTANCE;
    }

    private static BigInteger nearestIntegerSquareRoot(BigInteger bigInteger) {
        BigInteger shiftLeft = FrinkBigInteger.ONE.shiftLeft((bigInteger.bitLength() + 1) / 2);
        BigInteger subtract = shiftLeft.subtract(FrinkBigInteger.ONE);
        while (true) {
            BigInteger bigInteger2 = subtract;
            BigInteger bigInteger3 = shiftLeft;
            shiftLeft = bigInteger2;
            if (shiftLeft.compareTo(bigInteger3) >= 0) {
                return bigInteger3;
            }
            subtract = shiftLeft.multiply(shiftLeft).add(bigInteger).divide(shiftLeft.shiftLeft(1));
        }
    }

    private static int nearestIntegerSquareRoot(int i) {
        long j = (long) i;
        long j2 = (((long) i) + 1) / 2;
        while (true) {
            long j3 = j2;
            long j4 = j;
            j = j3;
            if (j >= j4) {
                return (int) j4;
            }
            j2 = ((j * j) + ((long) i)) / (2 * j);
        }
    }

    public static FrinkInteger integerSquareRoot(int i) throws NotAnIntegerException {
        int nearestIntegerSquareRoot = nearestIntegerSquareRoot(i);
        if (nearestIntegerSquareRoot * nearestIntegerSquareRoot == i) {
            return FrinkInteger.construct(nearestIntegerSquareRoot);
        }
        throw NotAnIntegerException.INSTANCE;
    }

    private static Numeric integerRoot(FrinkInteger frinkInteger, int i) throws NotAnIntegerException, NumericException {
        if (i == 2) {
            return integerSquareRoot(frinkInteger);
        }
        BigInteger bigInt = frinkInteger.getBigInt();
        if (bigInt.compareTo(FrinkBigInteger.ZERO) >= 0) {
            return integerRoot(bigInt, i);
        }
        if (i % 2 == 1) {
            return integerRoot(bigInt.negate(), i).negate();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    private static FrinkInteger integerRoot(BigInteger bigInteger, int i) throws NotAnIntegerException {
        BigInteger nearestIntegerRoot = nearestIntegerRoot(bigInteger, i);
        if (nearestIntegerRoot.pow(i).compareTo(bigInteger) == 0) {
            return FrinkInteger.construct(nearestIntegerRoot);
        }
        throw NotAnIntegerException.INSTANCE;
    }

    private static BigInteger nearestIntegerRoot(BigInteger bigInteger, int i) {
        BigInteger bigInteger2;
        BigInteger valueOf = BigInteger.valueOf((long) i);
        BigInteger.valueOf((long) (i - 1));
        BigInteger shiftLeft = BigInteger.ONE.shiftLeft((bigInteger.bitLength() + (i - 1)) / i);
        BigInteger subtract = shiftLeft.subtract(FrinkBigInteger.ONE);
        while (true) {
            BigInteger bigInteger3 = subtract;
            bigInteger2 = shiftLeft;
            shiftLeft = bigInteger3;
            if (shiftLeft.compareTo(bigInteger2) >= 0) {
                break;
            }
            subtract = shiftLeft.subtract(shiftLeft.pow(i).subtract(bigInteger).divide(shiftLeft.pow(i - 1).multiply(valueOf)));
        }
        BigInteger bigInteger4 = bigInteger2;
        while (bigInteger4.pow(i).compareTo(bigInteger) > 0) {
            bigInteger4 = bigInteger4.subtract(BigInteger.ONE);
        }
        return bigInteger4;
    }

    public static int compare(FrinkReal frinkReal, FrinkReal frinkReal2) {
        return compare(frinkReal, frinkReal2, NumericMath.mc);
    }

    public static int compare(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) {
        if (frinkReal.isFrinkInteger()) {
            FrinkInteger frinkInteger = (FrinkInteger) frinkReal;
            if (frinkReal2.isFrinkInteger()) {
                return FrinkInteger.compare((FrinkInteger) frinkReal, (FrinkInteger) frinkReal2);
            }
            if (frinkReal2.isRational()) {
                FrinkRational frinkRational = (FrinkRational) frinkReal2;
                return FrinkInteger.compare(multiplyInts(frinkInteger, frinkRational.getDenominator()), frinkRational.getNumerator());
            }
        } else if (frinkReal.isRational()) {
            FrinkRational frinkRational2 = (FrinkRational) frinkReal;
            if (frinkReal2.isFrinkInteger()) {
                return FrinkInteger.compare(frinkRational2.getNumerator(), multiplyInts(frinkRational2.getDenominator(), (FrinkInteger) frinkReal2));
            } else if (frinkReal2.isRational()) {
                FrinkRational frinkRational3 = (FrinkRational) frinkReal2;
                return FrinkInteger.compare(multiplyInts(frinkRational2.getNumerator(), frinkRational3.getDenominator()), multiplyInts(frinkRational3.getNumerator(), frinkRational2.getDenominator()));
            }
        } else if (frinkReal.isFloat() && frinkReal2.isFloat()) {
            return ((FrinkFloat) frinkReal).getBigDec().compareTo(((FrinkFloat) frinkReal2).getBigDec());
        }
        return compare(frinkReal.getFrinkFloatValue(mathContext), frinkReal2.getFrinkFloatValue(mathContext), mathContext);
    }

    public static FrinkReal min(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) {
        if (compare(frinkReal, frinkReal2, mathContext) <= 0) {
            return frinkReal;
        }
        return frinkReal2;
    }

    public static FrinkReal max(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) {
        if (compare(frinkReal, frinkReal2, mathContext) >= 0) {
            return frinkReal;
        }
        return frinkReal2;
    }

    public static FrinkReal reciprocal(FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        if (frinkReal.isRational()) {
            return ((FrinkRational) frinkReal).reciprocal();
        }
        return divide(FrinkInt.ONE, frinkReal, mathContext);
    }

    public static Numeric ln(FrinkReal frinkReal, MathContext mathContext) {
        if (frinkReal.realSignum() >= 0) {
            return new FrinkFloat(Math.log(frinkReal.doubleValue()));
        }
        return ComplexMath.ln(frinkReal, mathContext);
    }

    public static FrinkReal exp(FrinkReal frinkReal, MathContext mathContext) {
        return new FrinkFloat(Math.exp(frinkReal.doubleValue()));
    }

    public static FrinkInteger floor(FrinkReal frinkReal) throws NumericException {
        return floor(frinkReal, NumericMath.mc);
    }

    public static FrinkInteger floor(FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        if (frinkReal.isFrinkInteger()) {
            return (FrinkInteger) frinkReal;
        }
        if (frinkReal.isRational()) {
            FrinkRational frinkRational = (FrinkRational) frinkReal;
            try {
                int i = frinkRational.getNumerator().getInt();
                int i2 = frinkRational.getDenominator().getInt();
                if (i < 0) {
                    return FrinkInteger.construct((i / i2) - 1);
                }
                return FrinkInteger.construct(i / i2);
            } catch (NotAnIntegerException e) {
                BigInteger bigInt = frinkRational.getNumerator().getBigInt();
                BigInteger bigInt2 = frinkRational.getDenominator().getBigInt();
                if (bigInt.signum() == -1) {
                    return FrinkInteger.construct(bigInt.divide(bigInt2).subtract(FrinkBigInteger.ONE));
                }
                return FrinkInteger.construct(bigInt.divide(bigInt2));
            }
        } else if (frinkReal.isFloat()) {
            FrinkBigDecimal bigDec = ((FrinkFloat) frinkReal).getBigDec();
            try {
                return FrinkInteger.construct(bigDec.toBigIntegerExact());
            } catch (ArithmeticException e2) {
                BigInteger bigInteger = bigDec.toBigInteger();
                if (bigDec.signum() == -1) {
                    return FrinkInteger.construct(bigInteger.subtract(FrinkBigInteger.ONE));
                }
                return FrinkInteger.construct(bigInteger);
            }
        } else {
            throw new NotImplementedException("Unsupported type in RealMath.floor():" + frinkReal.getClass().getName(), false);
        }
    }

    public static FrinkInteger ceil(FrinkReal frinkReal) throws NumericException {
        return ceil(frinkReal, NumericMath.mc);
    }

    public static FrinkInteger ceil(FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        if (frinkReal.isFrinkInteger()) {
            return (FrinkInteger) frinkReal;
        }
        if (frinkReal.isRational()) {
            FrinkRational frinkRational = (FrinkRational) frinkReal;
            try {
                int i = frinkRational.getNumerator().getInt();
                int i2 = frinkRational.getDenominator().getInt();
                if (i < 0) {
                    return FrinkInteger.construct(i / i2);
                }
                return FrinkInteger.construct((i / i2) + 1);
            } catch (NotAnIntegerException e) {
                BigInteger bigInt = frinkRational.getNumerator().getBigInt();
                BigInteger bigInt2 = frinkRational.getDenominator().getBigInt();
                if (bigInt.signum() == -1) {
                    return FrinkInteger.construct(bigInt.divide(bigInt2));
                }
                return FrinkInteger.construct(bigInt.divide(bigInt2).add(FrinkBigInteger.ONE));
            }
        } else if (frinkReal.isFloat()) {
            FrinkBigDecimal bigDec = ((FrinkFloat) frinkReal).getBigDec();
            try {
                return FrinkInteger.construct(bigDec.toBigIntegerExact());
            } catch (ArithmeticException e2) {
                BigInteger bigInteger = bigDec.toBigInteger();
                if (bigDec.signum() == -1) {
                    return FrinkInteger.construct(bigInteger);
                }
                return FrinkInteger.construct(bigInteger.add(FrinkBigInteger.ONE));
            }
        } else {
            throw new NotImplementedException("Unsupported type in RealMath.ceil(): " + frinkReal.getClass().getName(), false);
        }
    }

    public static FrinkReal mod(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        if (!frinkReal.isInt() || !frinkReal2.isInt()) {
            if (frinkReal.isFrinkInteger() && frinkReal2.isFrinkInteger()) {
                BigInteger bigInt = ((FrinkInteger) frinkReal2).getBigInt();
                if (bigInt.signum() > 0) {
                    return FrinkInteger.construct(((FrinkInteger) frinkReal).getBigInt().mod(bigInt));
                }
            }
            return subtract(frinkReal, multiply(frinkReal2, floor(divide(frinkReal, frinkReal2, mathContext), mathContext), mathContext), mathContext);
        }
        int i = ((FrinkInt) frinkReal).getInt();
        int i2 = ((FrinkInt) frinkReal2).getInt();
        if (i >= 0) {
            if (i2 > 0) {
                return FrinkInt.construct(i % i2);
            }
            int i3 = i % i2;
            if (i3 != 0) {
                i3 += i2;
            }
            return FrinkInt.construct(i3);
        } else if (i2 < 0) {
            return FrinkInt.construct(i % i2);
        } else {
            int i4 = i % i2;
            if (i4 != 0) {
                i4 += i2;
            }
            return FrinkInt.construct(i4);
        }
    }

    public static void main(String[] strArr) {
        for (int i = 1; i < 2000; i++) {
            BigInteger valueOf = BigInteger.valueOf((long) i);
            BigInteger nearestIntegerRoot = nearestIntegerRoot(valueOf.pow(99), 99);
            if (!nearestIntegerRoot.equals(valueOf)) {
                System.out.println(i + ": " + nearestIntegerRoot + " " + valueOf.subtract(nearestIntegerRoot));
            }
        }
        Random random = new Random();
        for (int i2 = 1; i2 < 2000000; i2++) {
            BigInteger bigInteger = new BigInteger(100, random);
            int nextInt = random.nextInt(20) + 1;
            BigInteger nearestIntegerRoot2 = nearestIntegerRoot(bigInteger.pow(nextInt), nextInt);
            if (nearestIntegerRoot2.compareTo(bigInteger) > 0 || nearestIntegerRoot2.add(BigInteger.ONE).compareTo(bigInteger) <= 0) {
                System.out.println(bigInteger + "^" + nextInt + ": " + nearestIntegerRoot2 + " " + bigInteger.subtract(nearestIntegerRoot2));
            }
        }
    }
}
