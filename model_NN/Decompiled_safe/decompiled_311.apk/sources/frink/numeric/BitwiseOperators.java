package frink.numeric;

import frink.errors.NotAnIntegerException;

public class BitwiseOperators {
    public static FrinkInteger bitOr(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(frinkInteger.getInt() | frinkInteger2.getInt());
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().or(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger bitAnd(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(frinkInteger.getInt() & frinkInteger2.getInt());
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().and(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger bitXor(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(frinkInteger.getInt() ^ frinkInteger2.getInt());
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().xor(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger bitNot(FrinkInteger frinkInteger) {
        try {
            return FrinkInteger.construct(frinkInteger.getInt() ^ -1);
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().not());
        }
    }

    public static FrinkInteger bitNand(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct((frinkInteger.getInt() & frinkInteger2.getInt()) ^ -1);
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().and(frinkInteger2.getBigInt()).not());
        }
    }

    public static FrinkInteger bitNor(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct((frinkInteger.getInt() | frinkInteger2.getInt()) ^ -1);
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().or(frinkInteger2.getBigInt()).not());
        }
    }
}
