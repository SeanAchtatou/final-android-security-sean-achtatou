package frink.numeric;

import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import java.math.BigInteger;

public class NumericMath {
    public static final FrinkFloat HALF_PI = new FrinkFloat(1.5707963267948966d);
    public static final double HALF_PI_DOUBLE = 1.5707963267948966d;
    static MathContext MACHINE_ROUND = new MathContext(17, 1, false, 4);
    static MathContext MACHINE_ROUND_DOWN = new MathContext(14, 1, false, 3);
    static final MathContext MACHINE_ROUND_UP = new MathContext(14, 1, false, 2);
    private static final int MC_FORM = 1;
    static MathContext ROUND_DOWN;
    static MathContext ROUND_UP;
    static MathContext mc;

    static {
        setPrecision(20);
    }

    public static void setPrecision(int i) {
        mc = new MathContext(i, 1, false, 4);
        ROUND_DOWN = new MathContext(i, 1, false, 3);
        ROUND_UP = new MathContext(i, 1, false, 2);
    }

    public static int getPrecision() {
        return mc.getDigits();
    }

    public static Numeric add(Numeric numeric, Numeric numeric2) throws NumericException {
        return add(numeric, numeric2, mc);
    }

    public static Numeric add(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.add((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.add(numeric, numeric2, mathContext);
        }
        return ComplexMath.add(numeric, numeric2, mathContext);
    }

    public static Numeric subtract(Numeric numeric, Numeric numeric2) throws NumericException {
        return subtract(numeric, numeric2, mc);
    }

    public static Numeric subtract(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.subtract((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.subtract(numeric, numeric2, mathContext);
        }
        return ComplexMath.subtract(numeric, numeric2, mathContext);
    }

    public static Numeric multiply(Numeric numeric, Numeric numeric2) throws NumericException {
        return multiply(numeric, numeric2, mc);
    }

    public static Numeric multiply(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.multiply((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.multiply(numeric, numeric2, mathContext);
        }
        return ComplexMath.multiply(numeric, numeric2, mathContext);
    }

    public static Numeric divide(Numeric numeric, Numeric numeric2) throws NumericException {
        return divide(numeric, numeric2, mc);
    }

    public static Numeric divide(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.divide((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.divide(numeric, numeric2, mathContext);
        }
        return ComplexMath.divide(numeric, numeric2, mathContext);
    }

    public static Numeric power(Numeric numeric, Numeric numeric2) throws NumericException {
        return power(numeric, numeric2, mc);
    }

    public static Numeric power(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric2.isReal() && numeric.isReal()) {
            return RealMath.power((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric2.isInterval() || numeric.isInterval()) {
            return RealIntervalMath.power(numeric, numeric2, mathContext);
        }
        return ComplexMath.power(numeric, numeric2, mathContext);
    }

    public static boolean isNegative(Numeric numeric) throws NotRealException, NotImplementedException, OverlapException {
        if (numeric.isReal()) {
            return ((FrinkReal) numeric).realSignum() < 0;
        }
        throw NotRealException.INSTANCE;
    }

    public static boolean isPositive(Numeric numeric) throws NotRealException, NotImplementedException, OverlapException {
        if (numeric.isReal()) {
            return ((FrinkReal) numeric).realSignum() > 0;
        }
        throw NotRealException.INSTANCE;
    }

    public static int compare(Numeric numeric, Numeric numeric2) throws NotRealException, NotImplementedException, OverlapException {
        return compare(numeric, numeric2, mc);
    }

    public static int compare(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NotRealException, NotImplementedException, OverlapException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.compare((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.compare(numeric, numeric2, mathContext);
        }
        if (!numeric.isComplex() || !numeric2.isComplex()) {
            throw new NotRealException("Arguments to compare were not real.");
        }
        FrinkComplex frinkComplex = (FrinkComplex) numeric;
        FrinkComplex frinkComplex2 = (FrinkComplex) numeric2;
        if (RealMath.compare(frinkComplex.getReal(), frinkComplex2.getReal(), mc) == 0 && RealMath.compare(frinkComplex.getImag(), frinkComplex2.getImag(), mc) == 0) {
            return 0;
        }
        throw new NotRealException("Arguments to compare were unequal complex numbers.");
    }

    public static Numeric floor(Numeric numeric) throws NumericException {
        return floor(numeric, mc);
    }

    public static Numeric floor(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.floor((FrinkReal) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.floor((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("Unsupported type in NumericMath.floor():" + numeric.getClass().getName(), false);
    }

    public static Numeric mod(Numeric numeric, Numeric numeric2) throws NumericException {
        return mod(numeric, numeric2, mc);
    }

    public static Numeric mod(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.mod((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.mod(numeric, numeric2, mathContext);
        }
        return subtract(numeric, multiply(numeric2, floor(divide(numeric, numeric2, mathContext), mathContext), mathContext), mathContext);
    }

    public static Numeric ceil(Numeric numeric) throws NumericException {
        return ceil(numeric, mc);
    }

    public static Numeric ceil(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.ceil((FrinkReal) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.ceil((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("Unsupported type in NumericMath.ceil(): " + numeric.getClass().getName(), false);
    }

    public static FrinkInteger truncate(Numeric numeric) throws NumericException {
        return truncate(numeric, mc);
    }

    public static FrinkInteger truncate(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isFrinkInteger()) {
            return (FrinkInteger) numeric;
        }
        if (numeric.isRational()) {
            FrinkRational frinkRational = (FrinkRational) numeric;
            try {
                return FrinkInteger.construct(frinkRational.getNumerator().getInt() / frinkRational.getDenominator().getInt());
            } catch (NotAnIntegerException e) {
                return FrinkInteger.construct(frinkRational.getNumerator().getBigInt().divide(frinkRational.getDenominator().getBigInt()));
            }
        } else if (numeric.isFloat()) {
            FrinkBigDecimal bigDec = ((FrinkFloat) numeric).getBigDec();
            try {
                return FrinkInteger.construct(bigDec.toBigIntegerExact());
            } catch (ArithmeticException e2) {
                return FrinkInteger.construct(bigDec.toBigInteger());
            }
        } else {
            throw new NotImplementedException("Unsupported type in NumericMath.truncate():" + numeric.getClass().getName(), true);
        }
    }

    public static Numeric round(Numeric numeric) throws NumericException {
        return round(numeric, mc);
    }

    public static Numeric round(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isFrinkInteger()) {
            return (FrinkInteger) numeric;
        }
        return floor(add(numeric, RealMath.half, mathContext), mathContext);
    }

    public static Numeric abs(Numeric numeric) throws NumericException {
        return abs(numeric, mc);
    }

    public static Numeric abs(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.abs((FrinkReal) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.abs((RealInterval) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.abs((FrinkComplex) numeric, mathContext);
        }
        throw new NotImplementedException("Can't take absolute value of " + numeric.toString(), true);
    }

    public static Numeric magnitude(Numeric numeric) throws NumericException {
        return magnitude(numeric, mc);
    }

    public static Numeric magnitude(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return numeric;
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.magnitude((RealInterval) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.abs((FrinkComplex) numeric, mathContext);
        }
        throw new NotImplementedException("Can't take magnitude of " + numeric.toString(), true);
    }

    public static Numeric mignitude(Numeric numeric) throws NumericException {
        return mignitude(numeric, mc);
    }

    public static Numeric mignitude(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return numeric;
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.mignitude((RealInterval) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.abs((FrinkComplex) numeric, mathContext);
        }
        throw new NotImplementedException("Can't take mignitude of " + numeric.toString(), true);
    }

    public static long getLongValue(Numeric numeric) throws NotAnIntegerException {
        if (numeric.isFrinkInteger()) {
            return ((FrinkInteger) numeric).getLong();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static int getIntegerValue(Numeric numeric) throws NotAnIntegerException {
        if (numeric.isFrinkInteger()) {
            return ((FrinkInteger) numeric).getInt();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static short getShortValue(Numeric numeric) throws NotAnIntegerException {
        if (numeric.isFrinkInteger()) {
            return ((FrinkInteger) numeric).getShort();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static byte getByteValue(Numeric numeric) throws NotAnIntegerException {
        if (numeric.isFrinkInteger()) {
            return ((FrinkInteger) numeric).getByte();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static BigInteger getBigIntegerValue(Numeric numeric) throws NotAnIntegerException {
        if (numeric.isFrinkInteger()) {
            return ((FrinkInteger) numeric).getBigInt();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkInteger getFrinkIntegerValue(Numeric numeric) throws NotAnIntegerException {
        if (numeric.isFrinkInteger()) {
            return (FrinkInteger) numeric;
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public static FrinkReal getFrinkRealValue(Numeric numeric) throws NotRealException {
        if (numeric.isReal()) {
            return (FrinkReal) numeric;
        }
        throw NotRealException.INSTANCE;
    }

    public static FrinkInteger addInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(((long) frinkInteger.getInt()) + ((long) frinkInteger2.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().add(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger subtractInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(((long) frinkInteger.getInt()) - ((long) frinkInteger2.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().subtract(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger multiplyInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            int i = frinkInteger.getInt();
            int i2 = frinkInteger2.getInt();
            if (i == 1) {
                return frinkInteger2;
            }
            if (i2 == 1) {
                return frinkInteger;
            }
            return FrinkInteger.construct(((long) i2) * ((long) i));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().multiply(frinkInteger2.getBigInt()));
        }
    }

    public static FrinkInteger divInts(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return FrinkInteger.construct(((long) frinkInteger.getInt()) / ((long) frinkInteger2.getInt()));
        } catch (NotAnIntegerException e) {
            return FrinkInteger.construct(frinkInteger.getBigInt().divide(frinkInteger2.getBigInt()));
        }
    }

    public static Numeric ln(Numeric numeric) throws NumericException {
        return ln(numeric, mc);
    }

    public static Numeric ln(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.ln((FrinkReal) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.ln((FrinkComplex) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.ln((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to ln[" + numeric + "]", false);
    }

    public static Numeric exp(Numeric numeric) throws NumericException {
        return exp(numeric, mc);
    }

    public static Numeric exp(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.exp((FrinkReal) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.exp((FrinkComplex) numeric);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.exp((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to exp[" + numeric + "]", false);
    }

    public static Numeric sin(Numeric numeric) throws NumericException {
        return sin(numeric, mc);
    }

    public static Numeric sin(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.sin((FrinkReal) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.sin((FrinkComplex) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.sin((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to sin[" + numeric + "]", false);
    }

    public static Numeric cos(Numeric numeric) throws NumericException {
        return cos(numeric, mc);
    }

    public static Numeric cos(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.cos((FrinkReal) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.cos((FrinkComplex) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.cos((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to cos[" + numeric + "]", false);
    }

    public static Numeric tan(Numeric numeric) throws NumericException {
        return tan(numeric, mc);
    }

    public static Numeric tan(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.tan((FrinkReal) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.tan((FrinkComplex) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.tan((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to tan[" + numeric + "]", false);
    }

    public static Numeric arctan(Numeric numeric) throws NumericException {
        return arctan(numeric, mc);
    }

    public static Numeric arctan(Numeric numeric, Numeric numeric2) throws NumericException {
        return arctan(numeric, numeric2, mc);
    }

    public static Numeric arctan(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.arctan((FrinkReal) numeric, mathContext);
        }
        if (numeric.isComplex()) {
            return ComplexMath.arctan((FrinkComplex) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.arctan((RealInterval) numeric, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to arctan[" + numeric + "]", false);
    }

    public static Numeric arctan(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return RealMath.arctan((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        if (numeric.isInterval() || numeric2.isInterval()) {
            return RealIntervalMath.arctan(numeric, numeric2, mathContext);
        }
        throw new NotImplementedException("NumericMath: invalid argument to arctan[" + numeric + ", " + numeric2 + "]", false);
    }

    public static Numeric arcsin(Numeric numeric) throws NumericException {
        return arcsin(numeric, mc);
    }

    public static Numeric arcsin(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            if (RealMath.abs((FrinkReal) numeric, mathContext).doubleValue() <= 1.0d) {
                return RealMath.arcsin((FrinkReal) numeric, mathContext);
            }
            return ComplexMath.arcsin(numeric, mathContext);
        } else if (numeric.isComplex()) {
            return ComplexMath.arcsin((FrinkComplex) numeric, mathContext);
        } else {
            if (numeric.isInterval()) {
                return RealIntervalMath.arcsin((RealInterval) numeric, mathContext);
            }
            throw new NotImplementedException("NumericMath: invalid argument to arcsin[" + numeric + "]", false);
        }
    }

    public static Numeric arccos(Numeric numeric) throws NumericException {
        return arccos(numeric, mc);
    }

    public static Numeric arccos(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            if (RealMath.abs((FrinkReal) numeric, mathContext).doubleValue() <= 1.0d) {
                return RealMath.arccos((FrinkReal) numeric, mathContext);
            }
            return ComplexMath.arccos(numeric, mathContext);
        } else if (numeric.isComplex()) {
            return ComplexMath.arccos((FrinkComplex) numeric, mathContext);
        } else {
            if (numeric.isInterval()) {
                return RealIntervalMath.arccos((RealInterval) numeric, mathContext);
            }
            throw new NotImplementedException("NumericMath: invalid argument to arccos[" + numeric + "]", false);
        }
    }

    public static Numeric arccsc(Numeric numeric) throws NumericException {
        return arccsc(numeric, mc);
    }

    public static Numeric arccsc(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            if (RealMath.abs((FrinkReal) numeric, mathContext).doubleValue() >= 1.0d) {
                return RealMath.arccsc((FrinkReal) numeric, mathContext);
            }
            return ComplexMath.arccsc(numeric, mathContext);
        } else if (numeric.isComplex()) {
            return ComplexMath.arccsc((FrinkComplex) numeric, mathContext);
        } else {
            throw new NotImplementedException("NumericMath: invalid argument to arccsc[" + numeric + "]", false);
        }
    }

    public static Numeric arcsec(Numeric numeric) throws NumericException {
        return arcsec(numeric, mc);
    }

    public static Numeric arcsec(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            if (RealMath.abs((FrinkReal) numeric, mathContext).doubleValue() >= 1.0d) {
                return RealMath.arcsec((FrinkReal) numeric, mathContext);
            }
            return ComplexMath.arcsec(numeric, mathContext);
        } else if (numeric.isComplex()) {
            return ComplexMath.arcsec(numeric, mathContext);
        } else {
            throw new NotImplementedException("NumericMath: invalid argument to arcsec[" + numeric + "]", false);
        }
    }

    public static Numeric arccot(Numeric numeric) throws NumericException {
        return arccot(numeric, mc);
    }

    public static Numeric arccot(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            if (RealMath.abs((FrinkReal) numeric, mathContext).doubleValue() <= 1.5707963267948966d) {
                return RealMath.arccot((FrinkReal) numeric, mathContext);
            }
            return ComplexMath.arccot(numeric, mathContext);
        } else if (numeric.isComplex()) {
            return ComplexMath.arccot((FrinkComplex) numeric, mathContext);
        } else {
            throw new NotImplementedException("NumericMath: invalid argument to arccot[" + numeric + "]", false);
        }
    }

    public static Numeric sinh(Numeric numeric) throws NumericException {
        return sinh(numeric, mc);
    }

    public static Numeric sinh(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.sinh((FrinkReal) numeric, mathContext);
        }
        return divide(subtract(exp(numeric, mathContext), exp(negate(numeric, mathContext), mathContext), mathContext), FrinkInt.TWO, mathContext);
    }

    public static Numeric arcsinh(Numeric numeric) throws NumericException {
        return arcsinh(numeric, mc);
    }

    public static Numeric arcsinh(Numeric numeric, MathContext mathContext) throws NumericException {
        return ln(add(numeric, power(add(FrinkInt.ONE, power(numeric, FrinkInt.TWO, mathContext), mathContext), FrinkRational.ONE_HALF, mathContext), mathContext), mathContext);
    }

    public static Numeric arccsch(Numeric numeric) throws NumericException {
        return arccsch(numeric, mc);
    }

    public static Numeric arccsch(Numeric numeric, MathContext mathContext) throws NumericException {
        Numeric reciprocal = reciprocal(numeric, mathContext);
        return ln(add(power(add(FrinkInt.ONE, power(reciprocal, FrinkInt.TWO, mathContext), mathContext), FrinkRational.ONE_HALF, mathContext), reciprocal, mathContext), mathContext);
    }

    public static Numeric cosh(Numeric numeric) throws NumericException {
        return cosh(numeric, mc);
    }

    public static Numeric cosh(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.cosh((FrinkReal) numeric, mathContext);
        }
        return divide(add(exp(numeric, mathContext), exp(negate(numeric, mathContext), mathContext), mathContext), FrinkInt.TWO, mathContext);
    }

    public static Numeric arccosh(Numeric numeric) throws NumericException {
        return arccosh(numeric, mc);
    }

    public static Numeric arccosh(Numeric numeric, MathContext mathContext) throws NumericException {
        return ln(add(numeric, multiply(power(subtract(numeric, FrinkInt.ONE, mathContext), FrinkRational.ONE_HALF, mathContext), power(add(numeric, FrinkInt.ONE, mathContext), FrinkRational.ONE_HALF, mathContext), mathContext), mathContext), mathContext);
    }

    public static Numeric tanh(Numeric numeric) throws NumericException {
        return tanh(numeric, mc);
    }

    public static Numeric tanh(Numeric numeric, MathContext mathContext) throws NumericException {
        return divide(sinh(numeric, mathContext), cosh(numeric, mathContext), mathContext);
    }

    public static Numeric arctanh(Numeric numeric) throws NumericException {
        return arctanh(numeric, mc);
    }

    public static Numeric arctanh(Numeric numeric, MathContext mathContext) throws NumericException {
        return multiply(FrinkRational.ONE_HALF, subtract(ln(add(FrinkInt.ONE, numeric, mathContext), mathContext), ln(subtract(FrinkInt.ONE, numeric, mathContext), mathContext)), mathContext);
    }

    public static Numeric csc(Numeric numeric) throws NumericException {
        return csc(numeric, mc);
    }

    public static Numeric csc(Numeric numeric, MathContext mathContext) throws NumericException {
        return divide(FrinkInt.ONE, sin(numeric, mathContext), mathContext);
    }

    public static Numeric sec(Numeric numeric) throws NumericException {
        return sec(numeric, mc);
    }

    public static Numeric sec(Numeric numeric, MathContext mathContext) throws NumericException {
        return divide(FrinkInt.ONE, cos(numeric, mathContext), mathContext);
    }

    public static Numeric cot(Numeric numeric) throws NumericException {
        return cot(numeric, mc);
    }

    public static Numeric cot(Numeric numeric, MathContext mathContext) throws NumericException {
        return divide(FrinkInt.ONE, tan(numeric, mathContext), mathContext);
    }

    public static Numeric negate(Numeric numeric) throws NumericException {
        return negate(numeric, mc);
    }

    public static Numeric negate(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return ((FrinkReal) numeric).negate();
        }
        if (numeric.isComplex()) {
            return ((FrinkComplex) numeric).negate();
        }
        if (numeric.isInterval()) {
            return ((RealInterval) numeric).negate();
        }
        throw new NotImplementedException("NumericMath: invalid argument to negate[" + numeric + "]", true);
    }

    public static Numeric reciprocal(Numeric numeric) throws NumericException {
        return reciprocal(numeric, mc);
    }

    public static Numeric reciprocal(Numeric numeric, MathContext mathContext) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.reciprocal((FrinkReal) numeric, mathContext);
        }
        if (numeric.isInterval()) {
            return RealIntervalMath.reciprocal((RealInterval) numeric, mathContext);
        }
        return divide(FrinkInt.ONE, numeric, mathContext);
    }

    public static Numeric realPart(Numeric numeric) throws NumericException {
        if (numeric.isReal()) {
            return numeric;
        }
        if (numeric.isComplex()) {
            return ((FrinkComplex) numeric).getReal();
        }
        throw new NotImplementedException("NumericMath: invalid argument to negate[" + numeric + "]", false);
    }

    public static Numeric imaginaryPart(Numeric numeric) throws NumericException {
        if (numeric.isReal()) {
            return FrinkInt.ZERO;
        }
        if (numeric.isComplex()) {
            return ((FrinkComplex) numeric).getImag();
        }
        throw new NotImplementedException("NumericMath: invalid argument to Im[" + numeric + "]", false);
    }

    public static double approxLog2(Numeric numeric) throws NumericException {
        if (numeric.isReal()) {
            return RealMath.approxLog2((FrinkReal) numeric);
        }
        throw new NotImplementedException("RealMath.approxLog2 passed non-real argument " + numeric.toString(), true);
    }

    public static Numeric infimum(Numeric numeric) {
        if (numeric.isInterval()) {
            return ((RealInterval) numeric).getLower();
        }
        return numeric;
    }

    public static Numeric supremum(Numeric numeric) {
        if (numeric.isInterval()) {
            return ((RealInterval) numeric).getUpper();
        }
        return numeric;
    }

    public static Numeric main(Numeric numeric) {
        if (numeric.isInterval()) {
            return ((RealInterval) numeric).getMain();
        }
        return numeric;
    }

    public static Numeric signum(Numeric numeric) throws NumericException {
        if (numeric.isReal()) {
            return FrinkInt.construct(((FrinkReal) numeric).realSignum());
        }
        if (numeric.isComplex()) {
            return ComplexMath.signum((FrinkComplex) numeric);
        }
        if (numeric.isInterval()) {
            RealInterval realInterval = (RealInterval) numeric;
            FrinkReal main = realInterval.getMain();
            if (main == null) {
                return RealInterval.construct(signum(realInterval.getLower()), signum(realInterval.getUpper()));
            }
            return RealInterval.construct(signum(realInterval.getLower()), signum(main), signum(realInterval.getUpper()));
        }
        throw new NotImplementedException("Unsupported type in NumericMath.signum():" + numeric.getClass().getName(), false);
    }
}
