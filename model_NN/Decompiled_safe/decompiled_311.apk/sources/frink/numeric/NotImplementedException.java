package frink.numeric;

public class NotImplementedException extends NumericException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NotImplementedException(java.lang.String r3, boolean r4) {
        /*
            r2 = this;
            if (r4 == 0) goto L_0x0019
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r1 = "\n  This is unexpected.  Please send the full text of this error message to frinksupport@mindspring.com ."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0015:
            r2.<init>(r0)
            return
        L_0x0019:
            r0 = r3
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.NotImplementedException.<init>(java.lang.String, boolean):void");
    }
}
