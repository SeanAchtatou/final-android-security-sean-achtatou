package frink.numeric;

import frink.errors.FrinkException;

public class OverlapException extends FrinkException {
    public OverlapException(Numeric numeric, Numeric numeric2) {
        super("Overlap when comparing intervals:  " + numeric.toString() + "," + numeric2.toString());
    }
}
