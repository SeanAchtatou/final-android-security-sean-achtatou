package frink.numeric;

import java.math.BigInteger;
import java.util.Vector;

public class BaseConverter {
    private static final double LOG_TWO = Math.log(2.0d);
    private static final BigInteger TEN = BigInteger.valueOf(10);
    private static boolean hasFastToString;
    private static Vector<BigInteger> powerCache = new Vector<>();

    static {
        hasFastToString = false;
        powerCache.addElement(TEN);
        String property = System.getProperty("java.vm.name");
        if (property == null) {
            return;
        }
        if (property.indexOf("Kaffe") != -1 || property.indexOf("kaffe") != -1) {
            hasFastToString = true;
        }
    }

    public static String toString(BigInteger bigInteger) {
        if (hasFastToString) {
            return bigInteger.toString();
        }
        return recursiveToString(bigInteger);
    }

    public static String recursiveToString(BigInteger bigInteger) {
        BigInteger bigInteger2;
        if (bigInteger.bitLength() <= 319) {
            return bigInteger.toString();
        }
        StringBuffer stringBuffer = new StringBuffer();
        int signum = bigInteger.signum();
        if (signum == -1) {
            bigInteger2 = bigInteger.negate();
        } else {
            bigInteger2 = bigInteger;
        }
        toString(bigInteger2, stringBuffer, 0);
        if (signum == -1) {
            return "-" + ((Object) stringBuffer);
        }
        return new String(stringBuffer);
    }

    private static void toString(BigInteger bigInteger, StringBuffer stringBuffer, int i) {
        int bitLength = bigInteger.bitLength();
        if (bitLength <= 319) {
            String bigInteger2 = bigInteger.toString();
            if (bigInteger2.length() < i && stringBuffer.length() > 0) {
                for (int length = bigInteger2.length(); length < i; length++) {
                    stringBuffer.append("0");
                }
            }
            stringBuffer.append(bigInteger2);
            return;
        }
        int ceil = ((int) Math.ceil(Math.log((double) bitLength) / LOG_TWO)) - 3;
        BigInteger[] divideAndRemainder = bigInteger.divideAndRemainder(getCache(ceil));
        int i2 = 1 << ceil;
        toString(divideAndRemainder[0], stringBuffer, i2);
        toString(divideAndRemainder[1], stringBuffer, i2);
    }

    private static synchronized BigInteger getCache(int i) {
        BigInteger bigInteger;
        synchronized (BaseConverter.class) {
            bigInteger = null;
            int size = powerCache.size();
            if (i >= size) {
                powerCache.setSize(i + 1);
                while (size <= i) {
                    bigInteger = powerCache.elementAt(size - 1).pow(2);
                    powerCache.setElementAt(bigInteger, size);
                    size++;
                }
            } else {
                bigInteger = powerCache.elementAt(i);
            }
        }
        return bigInteger;
    }
}
