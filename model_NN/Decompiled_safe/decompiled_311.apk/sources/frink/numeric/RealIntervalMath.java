package frink.numeric;

import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;

public class RealIntervalMath {
    private static final int ABOVE_ZERO = 1;
    private static final int BELOW_ZERO = -1;
    private static final int CONTAINS_ZERO = 0;
    private static final int ERROR_CASE = -2;
    private static final FrinkFloat HALF_PI = new FrinkFloat(1.5707963267948966d);
    private static final FrinkFloat NEGATIVE_HALF_PI = new FrinkFloat(-1.5707963267948966d);
    private static final FrinkFloat NEGATIVE_PI = new FrinkFloat(-3.141592653589793d);
    private static final FrinkFloat PI = new FrinkFloat(3.141592653589793d);
    private static final FrinkFloat TWO_PI = new FrinkFloat(6.283185307179586d);

    public static Numeric add(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isInterval()) {
            RealInterval realInterval = (RealInterval) numeric;
            if (numeric2.isInterval()) {
                return addIntervals(realInterval, (RealInterval) numeric2, mathContext);
            }
            if (numeric2.isReal()) {
                return addIntervalToReal(realInterval, (FrinkReal) numeric2, mathContext);
            }
        } else if (numeric2.isInterval()) {
            RealInterval realInterval2 = (RealInterval) numeric2;
            if (numeric.isReal()) {
                return addIntervalToReal(realInterval2, (FrinkReal) numeric, mathContext);
            }
        }
        throw new NotImplementedException("Unexpected types in RealIntervalMath.add", true);
    }

    public static Numeric multiply(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isInterval()) {
            RealInterval realInterval = (RealInterval) numeric;
            if (numeric2.isInterval()) {
                return multiplyIntervals(realInterval, (RealInterval) numeric2, mathContext);
            }
            if (numeric2.isReal()) {
                return multiplyIntervalByReal(realInterval, (FrinkReal) numeric2, mathContext);
            }
        } else if (numeric2.isInterval()) {
            RealInterval realInterval2 = (RealInterval) numeric2;
            if (numeric.isReal()) {
                return multiplyIntervalByReal(realInterval2, (FrinkReal) numeric, mathContext);
            }
        }
        throw new NotImplementedException("Unexpected types in RealIntervalMath.multiply", true);
    }

    public static Numeric power(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isInterval()) {
            RealInterval realInterval = (RealInterval) numeric;
            if (numeric2.isInterval()) {
                return powerIntervals(realInterval, (RealInterval) numeric2, mathContext);
            }
            if (numeric2.isReal()) {
                return powerIntervalByReal(realInterval, (FrinkReal) numeric2, mathContext);
            }
        } else if (numeric2.isInterval()) {
            RealInterval realInterval2 = (RealInterval) numeric2;
            if (numeric.isReal()) {
                return powerRealByInterval((FrinkReal) numeric, realInterval2, mathContext);
            }
        }
        throw new NotImplementedException("Unexpected types in RealIntervalMath.power", true);
    }

    public static Numeric addIntervals(RealInterval realInterval, RealInterval realInterval2, MathContext mathContext) throws NumericException {
        FrinkReal add = RealMath.add(realInterval.getLower(), realInterval2.getLower(), NumericMath.ROUND_DOWN);
        FrinkReal add2 = RealMath.add(realInterval.getUpper(), realInterval2.getUpper(), NumericMath.ROUND_UP);
        FrinkReal main = realInterval.getMain();
        FrinkReal main2 = realInterval2.getMain();
        if (main == null || main2 == null) {
            return RealInterval.construct(add, add2, mathContext);
        }
        return RealInterval.construct(add, RealMath.add(main, main2, mathContext), add2, mathContext);
    }

    public static Numeric subtract(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        return add(numeric, NumericMath.negate(numeric2, mathContext), mathContext);
    }

    public static Numeric divide(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        return multiply(numeric, NumericMath.reciprocal(numeric2, mathContext), mathContext);
    }

    public static Numeric reciprocal(RealInterval realInterval, MathContext mathContext) throws NumericException {
        if (getBounds(realInterval) == 0) {
            throw new NotImplementedException("Dividing by interval " + realInterval.toString() + " containing zero.", false);
        }
        FrinkReal main = realInterval.getMain();
        FrinkReal reciprocal = RealMath.reciprocal(realInterval.getUpper(), NumericMath.ROUND_DOWN);
        FrinkReal reciprocal2 = RealMath.reciprocal(realInterval.getLower(), NumericMath.ROUND_UP);
        if (main == null) {
            return RealInterval.construct(reciprocal, reciprocal2, mathContext);
        }
        return RealInterval.construct(reciprocal, RealMath.reciprocal(main, mathContext), reciprocal2, mathContext);
    }

    public static Numeric powerIntervalByReal(RealInterval realInterval, FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        Numeric power;
        Numeric power2;
        Numeric numeric;
        try {
            if (frinkReal.isFrinkInteger()) {
                FrinkInt frinkInt = null;
                int i = ((FrinkInteger) frinkReal).getInt();
                if (i < 0) {
                    return NumericMath.reciprocal(power(realInterval, FrinkInteger.construct(-i), mathContext), mathContext);
                }
                if (i >= 0) {
                    boolean z = (i & 1) == 0;
                    if (!z || realInterval.getLower().realSignum() >= 0) {
                        frinkInt = RealMath.power(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_DOWN);
                        numeric = RealMath.power(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_UP);
                    } else if (!z || realInterval.getUpper().realSignum() > 0) {
                        int bounds = getBounds(realInterval);
                        if (!z || bounds != 0) {
                            numeric = null;
                        } else {
                            frinkInt = FrinkInt.ZERO;
                            numeric = RealMath.power(magnitude(realInterval, NumericMath.MACHINE_ROUND_UP), frinkReal, NumericMath.MACHINE_ROUND_UP);
                        }
                    } else {
                        frinkInt = RealMath.power(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_DOWN);
                        numeric = RealMath.power(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_UP);
                    }
                    if (frinkInt != null) {
                        FrinkReal main = realInterval.getMain();
                        if (main != null) {
                            return RealInterval.constructMachine(frinkInt, RealMath.power(main, frinkReal, mathContext), numeric, mathContext);
                        }
                        return RealInterval.constructMachine(frinkInt, numeric, mathContext);
                    }
                }
            }
        } catch (NotAnIntegerException e) {
        }
        try {
            FrinkReal main2 = realInterval.getMain();
            if (RealMath.compare(realInterval.getLower(), FrinkInt.ZERO, mathContext) < 0) {
                if (frinkReal.isFrinkInteger()) {
                }
                throw new NotImplementedException("RealIntervalMath.powerIntervalByReal would return complex number for this range: " + realInterval.toString() + "^" + frinkReal.toString(), false);
            }
            if (frinkReal.realSignum() >= 0) {
                power = RealMath.power(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_DOWN);
                power2 = RealMath.power(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_UP);
            } else {
                power = RealMath.power(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_DOWN);
                power2 = RealMath.power(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_UP);
            }
            if (main2 == null) {
                return RealInterval.constructMachine(power, power2, mathContext);
            }
            return RealInterval.constructMachine(power, RealMath.power(main2, frinkReal, mathContext), power2, mathContext);
        } catch (NotRealException e2) {
            throw new NotImplementedException("RealIntervalMath.power not implemented for this range:" + realInterval.toString() + "^" + frinkReal.toString(), false);
        }
    }

    public static Numeric powerRealByInterval(FrinkReal frinkReal, RealInterval realInterval, MathContext mathContext) throws NumericException {
        Numeric numeric;
        Numeric power;
        try {
            if (RealMath.compare(frinkReal, FrinkInt.ONE, mathContext) >= 0) {
                Numeric power2 = RealMath.power(frinkReal, realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN);
                numeric = power2;
                power = RealMath.power(frinkReal, realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP);
            } else if (RealMath.compare(frinkReal, FrinkInt.ZERO, mathContext) <= 0) {
                throw new NotImplementedException("RealIntervalMath.powerRealByInterval would return complex number for this range: " + frinkReal.toString() + "^" + realInterval.toString(), false);
            } else {
                Numeric power3 = RealMath.power(frinkReal, realInterval.getUpper(), NumericMath.MACHINE_ROUND_DOWN);
                numeric = power3;
                power = RealMath.power(frinkReal, realInterval.getLower(), NumericMath.MACHINE_ROUND_UP);
            }
            FrinkReal main = realInterval.getMain();
            if (main != null) {
                return RealInterval.construct(numeric, RealMath.power(frinkReal, main, mathContext), power, mathContext);
            }
            return RealInterval.construct(numeric, power, mathContext);
        } catch (NotRealException e) {
            throw new NotImplementedException("RealIntervalMath.power not implemented for this range: " + frinkReal.toString() + "^" + realInterval.toString(), false);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static frink.numeric.Numeric multiplyIntervals(frink.numeric.RealInterval r9, frink.numeric.RealInterval r10, frink.numeric.MathContext r11) throws frink.numeric.NumericException {
        /*
            r6 = 0
            int r0 = getBounds(r9)
            int r1 = getBounds(r10)
            r2 = 0
            switch(r0) {
                case -1: goto L_0x008e;
                case 0: goto L_0x00e4;
                case 1: goto L_0x0039;
                default: goto L_0x000d;
            }
        L_0x000d:
            r0 = r2
            r1 = r6
            r3 = r6
            r4 = r6
            r5 = r6
            r2 = r6
        L_0x0013:
            if (r0 != 0) goto L_0x016e
            frink.numeric.MathContext r0 = frink.numeric.NumericMath.ROUND_DOWN
            frink.numeric.FrinkReal r0 = frink.numeric.RealMath.multiply(r6, r5, r0)
            frink.numeric.MathContext r1 = frink.numeric.NumericMath.ROUND_UP
            frink.numeric.FrinkReal r1 = frink.numeric.RealMath.multiply(r4, r3, r1)
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0024:
            frink.numeric.FrinkReal r2 = r9.getMain()
            frink.numeric.FrinkReal r3 = r10.getMain()
            if (r2 == 0) goto L_0x0168
            if (r3 == 0) goto L_0x0168
            frink.numeric.FrinkReal r2 = frink.numeric.RealMath.multiply(r2, r3, r11)
            frink.numeric.Numeric r0 = frink.numeric.RealInterval.construct(r1, r2, r0, r11)
        L_0x0038:
            return r0
        L_0x0039:
            switch(r1) {
                case -1: goto L_0x0077;
                case 0: goto L_0x0060;
                case 1: goto L_0x0049;
                default: goto L_0x003c;
            }
        L_0x003c:
            r0 = r6
            r1 = r6
            r3 = r6
            r4 = r6
        L_0x0040:
            r5 = r3
            r3 = r0
            r0 = r2
            r2 = r6
            r7 = r1
            r1 = r6
            r6 = r4
            r4 = r7
            goto L_0x0013
        L_0x0049:
            frink.numeric.FrinkReal r0 = r9.getLower()
            frink.numeric.FrinkReal r1 = r10.getLower()
            frink.numeric.FrinkReal r3 = r9.getUpper()
            frink.numeric.FrinkReal r4 = r10.getUpper()
            r7 = r4
            r4 = r0
            r0 = r7
            r8 = r1
            r1 = r3
            r3 = r8
            goto L_0x0040
        L_0x0060:
            frink.numeric.FrinkReal r0 = r9.getUpper()
            frink.numeric.FrinkReal r1 = r10.getLower()
            frink.numeric.FrinkReal r3 = r9.getUpper()
            frink.numeric.FrinkReal r4 = r10.getUpper()
            r7 = r4
            r4 = r0
            r0 = r7
            r8 = r1
            r1 = r3
            r3 = r8
            goto L_0x0040
        L_0x0077:
            frink.numeric.FrinkReal r0 = r9.getUpper()
            frink.numeric.FrinkReal r1 = r10.getLower()
            frink.numeric.FrinkReal r3 = r9.getLower()
            frink.numeric.FrinkReal r4 = r10.getUpper()
            r7 = r4
            r4 = r0
            r0 = r7
            r8 = r1
            r1 = r3
            r3 = r8
            goto L_0x0040
        L_0x008e:
            switch(r1) {
                case -1: goto L_0x00cd;
                case 0: goto L_0x00b6;
                case 1: goto L_0x009f;
                default: goto L_0x0091;
            }
        L_0x0091:
            r0 = r6
            r1 = r6
            r3 = r6
            r4 = r6
        L_0x0095:
            r5 = r3
            r3 = r0
            r0 = r2
            r2 = r6
            r7 = r1
            r1 = r6
            r6 = r4
            r4 = r7
            goto L_0x0013
        L_0x009f:
            frink.numeric.FrinkReal r0 = r9.getLower()
            frink.numeric.FrinkReal r1 = r10.getUpper()
            frink.numeric.FrinkReal r3 = r9.getUpper()
            frink.numeric.FrinkReal r4 = r10.getLower()
            r7 = r4
            r4 = r0
            r0 = r7
            r8 = r1
            r1 = r3
            r3 = r8
            goto L_0x0095
        L_0x00b6:
            frink.numeric.FrinkReal r0 = r9.getLower()
            frink.numeric.FrinkReal r1 = r10.getUpper()
            frink.numeric.FrinkReal r3 = r9.getLower()
            frink.numeric.FrinkReal r4 = r10.getLower()
            r7 = r4
            r4 = r0
            r0 = r7
            r8 = r1
            r1 = r3
            r3 = r8
            goto L_0x0095
        L_0x00cd:
            frink.numeric.FrinkReal r0 = r9.getUpper()
            frink.numeric.FrinkReal r1 = r10.getUpper()
            frink.numeric.FrinkReal r3 = r9.getLower()
            frink.numeric.FrinkReal r4 = r10.getLower()
            r7 = r4
            r4 = r0
            r0 = r7
            r8 = r1
            r1 = r3
            r3 = r8
            goto L_0x0095
        L_0x00e4:
            switch(r1) {
                case -1: goto L_0x00e9;
                case 0: goto L_0x011f;
                case 1: goto L_0x0104;
                default: goto L_0x00e7;
            }
        L_0x00e7:
            goto L_0x000d
        L_0x00e9:
            frink.numeric.FrinkReal r0 = r9.getUpper()
            frink.numeric.FrinkReal r1 = r10.getLower()
            frink.numeric.FrinkReal r3 = r9.getLower()
            frink.numeric.FrinkReal r4 = r10.getLower()
            r5 = r1
            r1 = r6
            r7 = r6
            r6 = r0
            r0 = r2
            r2 = r7
            r8 = r4
            r4 = r3
            r3 = r8
            goto L_0x0013
        L_0x0104:
            frink.numeric.FrinkReal r0 = r9.getLower()
            frink.numeric.FrinkReal r1 = r10.getUpper()
            frink.numeric.FrinkReal r3 = r9.getUpper()
            frink.numeric.FrinkReal r4 = r10.getUpper()
            r5 = r1
            r1 = r6
            r7 = r6
            r6 = r0
            r0 = r2
            r2 = r7
            r8 = r4
            r4 = r3
            r3 = r8
            goto L_0x0013
        L_0x011f:
            r0 = 1
            frink.numeric.FrinkReal r1 = r9.getLower()
            frink.numeric.FrinkReal r2 = r10.getUpper()
            frink.numeric.MathContext r3 = frink.numeric.NumericMath.ROUND_DOWN
            frink.numeric.FrinkReal r1 = frink.numeric.RealMath.multiply(r1, r2, r3)
            frink.numeric.FrinkReal r2 = r9.getUpper()
            frink.numeric.FrinkReal r3 = r10.getLower()
            frink.numeric.MathContext r4 = frink.numeric.NumericMath.ROUND_DOWN
            frink.numeric.FrinkReal r2 = frink.numeric.RealMath.multiply(r2, r3, r4)
            frink.numeric.FrinkReal r1 = frink.numeric.RealMath.min(r1, r2, r11)
            frink.numeric.FrinkReal r2 = r9.getLower()
            frink.numeric.FrinkReal r3 = r10.getLower()
            frink.numeric.MathContext r4 = frink.numeric.NumericMath.ROUND_UP
            frink.numeric.FrinkReal r2 = frink.numeric.RealMath.multiply(r2, r3, r4)
            frink.numeric.FrinkReal r3 = r9.getUpper()
            frink.numeric.FrinkReal r4 = r10.getUpper()
            frink.numeric.MathContext r5 = frink.numeric.NumericMath.ROUND_UP
            frink.numeric.FrinkReal r3 = frink.numeric.RealMath.multiply(r3, r4, r5)
            frink.numeric.FrinkReal r2 = frink.numeric.RealMath.max(r2, r3, r11)
            r3 = r6
            r4 = r6
            r5 = r6
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0013
        L_0x0168:
            frink.numeric.Numeric r0 = frink.numeric.RealInterval.construct(r1, r0, r11)
            goto L_0x0038
        L_0x016e:
            r0 = r1
            r1 = r2
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.RealIntervalMath.multiplyIntervals(frink.numeric.RealInterval, frink.numeric.RealInterval, frink.numeric.MathContext):frink.numeric.Numeric");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static frink.numeric.Numeric powerIntervals(frink.numeric.RealInterval r11, frink.numeric.RealInterval r12, frink.numeric.MathContext r13) throws frink.numeric.NumericException {
        /*
            r6 = 0
            r7 = 0
            java.lang.String r8 = "^"
            int r0 = getPowerBounds(r11, r13)
            int r1 = getBounds(r12)
            frink.numeric.FrinkReal r2 = r11.getLower()
            int r2 = r2.realSignum()
            if (r2 >= 0) goto L_0x0041
            frink.numeric.NotImplementedException r0 = new frink.numeric.NotImplementedException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "RealIntervalMath.powerIntervals would return complex number for this range: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r11.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "^"
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = r12.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1, r7)
            throw r0
        L_0x0041:
            switch(r0) {
                case -1: goto L_0x00c4;
                case 0: goto L_0x0119;
                case 1: goto L_0x0070;
                default: goto L_0x0044;
            }
        L_0x0044:
            r0 = r7
            r1 = r6
            r2 = r6
            r3 = r6
            r4 = r6
            r5 = r6
        L_0x004a:
            if (r0 != 0) goto L_0x01df
            frink.numeric.MathContext r0 = frink.numeric.NumericMath.MACHINE_ROUND_DOWN
            frink.numeric.Numeric r0 = frink.numeric.RealMath.power(r6, r5, r0)
            frink.numeric.MathContext r1 = frink.numeric.NumericMath.MACHINE_ROUND_UP
            frink.numeric.Numeric r1 = frink.numeric.RealMath.power(r4, r3, r1)
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x005b:
            frink.numeric.FrinkReal r2 = r11.getMain()
            frink.numeric.FrinkReal r3 = r12.getMain()
            if (r2 == 0) goto L_0x019f
            if (r3 == 0) goto L_0x019f
            frink.numeric.Numeric r2 = frink.numeric.RealMath.power(r2, r3, r13)     // Catch:{ NotRealException -> 0x01a5 }
            frink.numeric.Numeric r0 = frink.numeric.RealInterval.construct(r1, r2, r0, r13)     // Catch:{ NotRealException -> 0x01a5 }
        L_0x006f:
            return r0
        L_0x0070:
            switch(r1) {
                case -1: goto L_0x00ad;
                case 0: goto L_0x0096;
                case 1: goto L_0x007f;
                default: goto L_0x0073;
            }
        L_0x0073:
            r0 = r6
            r1 = r6
            r2 = r6
            r3 = r6
        L_0x0077:
            r4 = r1
            r5 = r2
            r2 = r6
            r1 = r6
            r6 = r3
            r3 = r0
            r0 = r7
            goto L_0x004a
        L_0x007f:
            frink.numeric.FrinkReal r0 = r11.getLower()
            frink.numeric.FrinkReal r1 = r12.getLower()
            frink.numeric.FrinkReal r2 = r11.getUpper()
            frink.numeric.FrinkReal r3 = r12.getUpper()
            r9 = r3
            r3 = r0
            r0 = r9
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x0077
        L_0x0096:
            frink.numeric.FrinkReal r0 = r11.getUpper()
            frink.numeric.FrinkReal r1 = r12.getLower()
            frink.numeric.FrinkReal r2 = r11.getUpper()
            frink.numeric.FrinkReal r3 = r12.getUpper()
            r9 = r3
            r3 = r0
            r0 = r9
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x0077
        L_0x00ad:
            frink.numeric.FrinkReal r0 = r11.getUpper()
            frink.numeric.FrinkReal r1 = r12.getLower()
            frink.numeric.FrinkReal r2 = r11.getLower()
            frink.numeric.FrinkReal r3 = r12.getUpper()
            r9 = r3
            r3 = r0
            r0 = r9
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x0077
        L_0x00c4:
            switch(r1) {
                case -1: goto L_0x0102;
                case 0: goto L_0x00eb;
                case 1: goto L_0x00d4;
                default: goto L_0x00c7;
            }
        L_0x00c7:
            r0 = r6
            r1 = r6
            r2 = r6
            r3 = r6
        L_0x00cb:
            r4 = r1
            r5 = r2
            r2 = r6
            r1 = r6
            r6 = r3
            r3 = r0
            r0 = r7
            goto L_0x004a
        L_0x00d4:
            frink.numeric.FrinkReal r0 = r11.getLower()
            frink.numeric.FrinkReal r1 = r12.getUpper()
            frink.numeric.FrinkReal r2 = r11.getUpper()
            frink.numeric.FrinkReal r3 = r12.getLower()
            r9 = r3
            r3 = r0
            r0 = r9
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x00cb
        L_0x00eb:
            frink.numeric.FrinkReal r0 = r11.getLower()
            frink.numeric.FrinkReal r1 = r12.getUpper()
            frink.numeric.FrinkReal r2 = r11.getLower()
            frink.numeric.FrinkReal r3 = r12.getLower()
            r9 = r3
            r3 = r0
            r0 = r9
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x00cb
        L_0x0102:
            frink.numeric.FrinkReal r0 = r11.getUpper()
            frink.numeric.FrinkReal r1 = r12.getUpper()
            frink.numeric.FrinkReal r2 = r11.getLower()
            frink.numeric.FrinkReal r3 = r12.getLower()
            r9 = r3
            r3 = r0
            r0 = r9
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x00cb
        L_0x0119:
            switch(r1) {
                case -1: goto L_0x011e;
                case 0: goto L_0x014e;
                case 1: goto L_0x0136;
                default: goto L_0x011c;
            }
        L_0x011c:
            goto L_0x0044
        L_0x011e:
            frink.numeric.FrinkReal r0 = r11.getUpper()
            frink.numeric.FrinkReal r1 = r12.getLower()
            frink.numeric.FrinkReal r2 = r11.getLower()
            frink.numeric.FrinkReal r3 = r12.getLower()
            r4 = r2
            r5 = r1
            r2 = r6
            r1 = r6
            r6 = r0
            r0 = r7
            goto L_0x004a
        L_0x0136:
            frink.numeric.FrinkReal r0 = r11.getLower()
            frink.numeric.FrinkReal r1 = r12.getUpper()
            frink.numeric.FrinkReal r2 = r11.getUpper()
            frink.numeric.FrinkReal r3 = r12.getUpper()
            r4 = r2
            r5 = r1
            r2 = r6
            r1 = r6
            r6 = r0
            r0 = r7
            goto L_0x004a
        L_0x014e:
            r2 = 1
            frink.numeric.FrinkReal r0 = r11.getLower()
            frink.numeric.FrinkReal r1 = r12.getUpper()
            frink.numeric.MathContext r3 = frink.numeric.NumericMath.MACHINE_ROUND_DOWN
            frink.numeric.Numeric r0 = frink.numeric.RealMath.power(r0, r1, r3)
            frink.numeric.FrinkReal r0 = (frink.numeric.FrinkReal) r0
            frink.numeric.FrinkReal r1 = r11.getUpper()
            frink.numeric.FrinkReal r3 = r12.getLower()
            frink.numeric.MathContext r4 = frink.numeric.NumericMath.MACHINE_ROUND_DOWN
            frink.numeric.Numeric r1 = frink.numeric.RealMath.power(r1, r3, r4)
            frink.numeric.FrinkReal r1 = (frink.numeric.FrinkReal) r1
            frink.numeric.FrinkReal r3 = frink.numeric.RealMath.min(r0, r1, r13)
            frink.numeric.FrinkReal r0 = r11.getLower()
            frink.numeric.FrinkReal r1 = r12.getLower()
            frink.numeric.MathContext r4 = frink.numeric.NumericMath.MACHINE_ROUND_UP
            frink.numeric.Numeric r0 = frink.numeric.RealMath.power(r0, r1, r4)
            frink.numeric.FrinkReal r0 = (frink.numeric.FrinkReal) r0
            frink.numeric.FrinkReal r1 = r11.getUpper()
            frink.numeric.FrinkReal r4 = r12.getUpper()
            frink.numeric.MathContext r5 = frink.numeric.NumericMath.MACHINE_ROUND_UP
            frink.numeric.Numeric r1 = frink.numeric.RealMath.power(r1, r4, r5)
            frink.numeric.FrinkReal r1 = (frink.numeric.FrinkReal) r1
            frink.numeric.FrinkReal r0 = frink.numeric.RealMath.max(r0, r1, r13)
            r1 = r0
            r4 = r6
            r5 = r6
            r0 = r2
            r2 = r3
            r3 = r6
            goto L_0x004a
        L_0x019f:
            frink.numeric.Numeric r0 = frink.numeric.RealInterval.construct(r1, r0, r13)     // Catch:{ NotRealException -> 0x01a5 }
            goto L_0x006f
        L_0x01a5:
            r0 = move-exception
            frink.numeric.NotImplementedException r1 = new frink.numeric.NotImplementedException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "RealIntervalMath.power threw NotRealException for this range: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r11.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "^"
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r3 = r12.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "\n "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0, r7)
            throw r1
        L_0x01df:
            r0 = r1
            r1 = r2
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.RealIntervalMath.powerIntervals(frink.numeric.RealInterval, frink.numeric.RealInterval, frink.numeric.MathContext):frink.numeric.Numeric");
    }

    public static Numeric addIntervalToReal(RealInterval realInterval, FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        FrinkReal add = RealMath.add(realInterval.getLower(), frinkReal, NumericMath.ROUND_DOWN);
        FrinkReal add2 = RealMath.add(realInterval.getUpper(), frinkReal, NumericMath.ROUND_UP);
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.construct(add, add2, mathContext);
        }
        return RealInterval.construct(add, RealMath.add(main, frinkReal, mathContext), add2, mathContext);
    }

    public static Numeric multiplyIntervalByReal(RealInterval realInterval, FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        FrinkReal multiply = RealMath.multiply(realInterval.getLower(), frinkReal, NumericMath.ROUND_DOWN);
        FrinkReal multiply2 = RealMath.multiply(realInterval.getUpper(), frinkReal, NumericMath.ROUND_UP);
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.construct(multiply, multiply2, mathContext);
        }
        return RealInterval.construct(multiply, RealMath.multiply(main, frinkReal, mathContext), multiply2, mathContext);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkInt, frink.numeric.FrinkReal, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkInt, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric */
    public static Numeric abs(RealInterval realInterval, MathContext mathContext) throws NumericException {
        switch (getBounds(realInterval)) {
            case -1:
                return realInterval.negate();
            case 0:
                FrinkReal main = realInterval.getMain();
                FrinkInt frinkInt = FrinkInt.ZERO;
                FrinkReal max = RealMath.max(realInterval.getLower().negate(), realInterval.getUpper(), mathContext);
                if (main == null) {
                    return RealInterval.construct((FrinkReal) frinkInt, max, mathContext);
                }
                return RealInterval.construct((FrinkReal) frinkInt, RealMath.abs(main, mathContext), max, mathContext);
            case 1:
                return realInterval;
            default:
                System.err.println("Error in RealIntervalMath.abs");
                return null;
        }
    }

    public static FrinkReal magnitude(RealInterval realInterval, MathContext mathContext) {
        return RealMath.max(RealMath.abs(realInterval.getLower(), NumericMath.ROUND_UP), RealMath.abs(realInterval.getUpper(), NumericMath.ROUND_UP), mathContext);
    }

    public static FrinkReal mignitude(RealInterval realInterval, MathContext mathContext) {
        return RealMath.min(RealMath.abs(realInterval.getLower(), NumericMath.ROUND_DOWN), RealMath.abs(realInterval.getUpper(), NumericMath.ROUND_DOWN), mathContext);
    }

    private static final boolean containsZero(RealInterval realInterval) {
        if (realInterval.getLower().realSignum() > 0) {
            return false;
        }
        return realInterval.getUpper().realSignum() >= 0;
    }

    private static final int getBounds(RealInterval realInterval) {
        switch (realInterval.getLower().realSignum()) {
            case 0:
            case 1:
                return 1;
            case -1:
                switch (realInterval.getUpper().realSignum()) {
                    case -1:
                    case 0:
                        return -1;
                    case 1:
                        return 0;
                }
        }
        return ERROR_CASE;
    }

    private static final int getPowerBounds(RealInterval realInterval, MathContext mathContext) {
        switch (RealMath.compare(realInterval.getLower(), FrinkInt.ONE, mathContext)) {
            case 0:
            case 1:
                return 1;
            case -1:
                switch (RealMath.compare(realInterval.getUpper(), FrinkInt.ONE, mathContext)) {
                    case -1:
                    case 0:
                        return -1;
                    case 1:
                        return 0;
                }
        }
        return ERROR_CASE;
    }

    public static Numeric ln(RealInterval realInterval, MathContext mathContext) throws NumericException {
        Numeric ln = RealMath.ln(realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN);
        Numeric ln2 = RealMath.ln(realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP);
        FrinkReal main = realInterval.getMain();
        if (main != null) {
            return RealInterval.constructMachine(ln, RealMath.ln(main, mathContext), ln2, mathContext);
        }
        try {
            return RealInterval.constructMachine(ln, ln2, mathContext);
        } catch (NotRealException e) {
            throw new NotImplementedException("RealIntervalMath.ln:  Passed interval that produced non-real values: " + realInterval.toString(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.constructMachine(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.constructMachine(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.constructMachine(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.constructMachine(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.constructMachine(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.constructMachine(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric */
    public static Numeric exp(RealInterval realInterval, MathContext mathContext) throws NumericException {
        FrinkReal exp = RealMath.exp(realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN);
        FrinkReal exp2 = RealMath.exp(realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP);
        FrinkReal main = realInterval.getMain();
        if (main != null) {
            return RealInterval.constructMachine((Numeric) exp, (Numeric) RealMath.exp(main, mathContext), (Numeric) exp2, mathContext);
        }
        try {
            return RealInterval.constructMachine((Numeric) exp, (Numeric) exp2, mathContext);
        } catch (NotRealException e) {
            throw new NotImplementedException("RealIntervalMath.exp:  Passed interval that produced non-real values: " + realInterval.toString(), false);
        }
    }

    public static int compare(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NotImplementedException, OverlapException {
        if (numeric.isInterval()) {
            if (numeric2.isInterval()) {
                return compare((RealInterval) numeric, (RealInterval) numeric2, mathContext);
            }
            if (numeric2.isReal()) {
                return compare((RealInterval) numeric, (FrinkReal) numeric2, mathContext);
            }
        } else if (numeric2.isInterval() && numeric.isReal()) {
            return compare((FrinkReal) numeric, (RealInterval) numeric2, mathContext);
        }
        throw new NotImplementedException("RealIntervalMath.compare: Unexpected arguments to compare: " + numeric.toString() + ", " + numeric2.toString(), true);
    }

    public static int compare(RealInterval realInterval, FrinkReal frinkReal, MathContext mathContext) throws OverlapException {
        if (RealMath.compare(realInterval.getUpper(), frinkReal, mathContext) < 0) {
            return -1;
        }
        if (RealMath.compare(realInterval.getLower(), frinkReal, mathContext) > 0) {
            return 1;
        }
        throw new OverlapException(realInterval, frinkReal);
    }

    public static int compare(FrinkReal frinkReal, RealInterval realInterval, MathContext mathContext) throws OverlapException {
        if (RealMath.compare(frinkReal, realInterval.getLower(), mathContext) < 0) {
            return -1;
        }
        if (RealMath.compare(frinkReal, realInterval.getUpper(), mathContext) > 0) {
            return 1;
        }
        throw new OverlapException(frinkReal, realInterval);
    }

    public static int compare(RealInterval realInterval, RealInterval realInterval2, MathContext mathContext) throws OverlapException {
        if (RealMath.compare(realInterval.getUpper(), realInterval2.getLower(), mathContext) < 0) {
            return -1;
        }
        if (RealMath.compare(realInterval.getLower(), realInterval2.getUpper(), mathContext) > 0) {
            return 1;
        }
        throw new OverlapException(realInterval, realInterval2);
    }

    public static Numeric sin(RealInterval realInterval, MathContext mathContext) throws NumericException {
        int i;
        FrinkInt frinkInt;
        FrinkReal frinkReal;
        FrinkInt frinkInt2;
        FrinkReal frinkReal2;
        FrinkReal lower = realInterval.getLower();
        FrinkReal upper = realInterval.getUpper();
        FrinkReal frinkReal3 = null;
        if (RealMath.compare(RealMath.subtract(upper, lower, mathContext), TWO_PI, mathContext) > 0) {
            frinkReal2 = FrinkInt.NEGATIVE_ONE;
            frinkInt2 = FrinkInt.ONE;
        } else {
            int quadrant = getQuadrant(lower, HALF_PI, mathContext);
            int quadrant2 = getQuadrant(upper, HALF_PI, mathContext);
            int i2 = quadrant % 4;
            if (i2 < 0) {
                i2 += 4;
            }
            int i3 = quadrant2 % 4;
            if (i3 < 0) {
                i3 += 4;
            }
            if (quadrant == quadrant2 || i2 < i3) {
                i = i3;
            } else {
                i = i3 + 4;
            }
            if ((i2 != 0 || i < 1) && (i2 < 1 || i < 5)) {
                frinkInt = null;
            } else {
                frinkInt = FrinkInt.ONE;
            }
            if (i2 <= 2 && i >= 3) {
                frinkReal3 = FrinkInt.NEGATIVE_ONE;
            }
            FrinkReal sin = RealMath.sin(lower, mathContext);
            FrinkReal sin2 = RealMath.sin(upper, mathContext);
            if (frinkInt == null) {
                frinkReal = RealMath.max(sin, sin2, mathContext);
            } else {
                frinkReal = frinkInt;
            }
            if (frinkReal3 == null) {
                frinkReal2 = RealMath.min(sin, sin2, mathContext);
                frinkInt2 = frinkReal;
            } else {
                frinkInt2 = frinkReal;
                frinkReal2 = frinkReal3;
            }
        }
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.constructMachine(frinkReal2, frinkInt2, mathContext);
        }
        return RealInterval.constructMachine(frinkReal2, RealMath.sin(main, mathContext), frinkInt2, mathContext);
    }

    public static Numeric cos(RealInterval realInterval, MathContext mathContext) throws NumericException {
        int i;
        FrinkInt frinkInt;
        FrinkReal frinkReal;
        FrinkInt frinkInt2;
        FrinkReal frinkReal2;
        FrinkReal lower = realInterval.getLower();
        FrinkReal upper = realInterval.getUpper();
        FrinkReal frinkReal3 = null;
        if (RealMath.compare(RealMath.subtract(upper, lower, mathContext), TWO_PI, mathContext) > 0) {
            frinkReal2 = FrinkInt.NEGATIVE_ONE;
            frinkInt2 = FrinkInt.ONE;
        } else {
            int quadrant = getQuadrant(lower, HALF_PI, mathContext);
            int quadrant2 = getQuadrant(upper, HALF_PI, mathContext);
            int i2 = quadrant % 4;
            if (i2 < 0) {
                i2 += 4;
            }
            int i3 = quadrant2 % 4;
            if (i3 < 0) {
                i3 += 4;
            }
            if (quadrant == quadrant2 || i2 < i3) {
                i = i3;
            } else {
                i = i3 + 4;
            }
            if (i >= 4) {
                frinkInt = FrinkInt.ONE;
            } else {
                frinkInt = null;
            }
            if ((i2 <= 1 && i >= 2) || (i2 >= 2 && i >= 6)) {
                frinkReal3 = FrinkInt.NEGATIVE_ONE;
            }
            FrinkReal cos = RealMath.cos(lower, mathContext);
            FrinkReal cos2 = RealMath.cos(upper, mathContext);
            if (frinkInt == null) {
                frinkReal = RealMath.max(cos, cos2, mathContext);
            } else {
                frinkReal = frinkInt;
            }
            if (frinkReal3 == null) {
                frinkReal2 = RealMath.min(cos, cos2, mathContext);
                frinkInt2 = frinkReal;
            } else {
                frinkInt2 = frinkReal;
                frinkReal2 = frinkReal3;
            }
        }
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.constructMachine(frinkReal2, frinkInt2, mathContext);
        }
        return RealInterval.constructMachine(frinkReal2, RealMath.cos(main, mathContext), frinkInt2, mathContext);
    }

    public static Numeric tan(RealInterval realInterval, MathContext mathContext) throws NumericException {
        return divide(sin(realInterval, mathContext), cos(realInterval, mathContext), mathContext);
    }

    private static int getQuadrant(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        FrinkInteger floor = RealMath.floor(RealMath.divide(frinkReal, frinkReal2, mathContext), mathContext);
        if (floor.isInt()) {
            return ((FrinkInt) floor).getInt();
        }
        throw new NotImplementedException("Unexpected return value from RealIntervalMath.getQuadrant: " + floor, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkInteger, frink.numeric.FrinkInteger, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkInteger, frink.numeric.FrinkInteger, frink.numeric.FrinkInteger, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric */
    public static Numeric floor(RealInterval realInterval, MathContext mathContext) throws NumericException {
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.construct((FrinkReal) RealMath.floor(realInterval.getLower(), mathContext), (FrinkReal) RealMath.floor(realInterval.getUpper(), mathContext), mathContext);
        }
        return RealInterval.construct((FrinkReal) RealMath.floor(realInterval.getLower(), mathContext), (FrinkReal) RealMath.floor(main, mathContext), (FrinkReal) RealMath.floor(realInterval.getUpper(), mathContext), mathContext);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkInteger, frink.numeric.FrinkInteger, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric
     arg types: [frink.numeric.FrinkInteger, frink.numeric.FrinkInteger, frink.numeric.FrinkInteger, frink.numeric.MathContext]
     candidates:
      frink.numeric.RealInterval.construct(frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.Numeric, frink.numeric.MathContext):frink.numeric.Numeric
      frink.numeric.RealInterval.construct(frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.FrinkReal, frink.numeric.MathContext):frink.numeric.Numeric */
    public static Numeric ceil(RealInterval realInterval, MathContext mathContext) throws NumericException {
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.construct((FrinkReal) RealMath.ceil(realInterval.getLower(), mathContext), (FrinkReal) RealMath.ceil(realInterval.getUpper(), mathContext), mathContext);
        }
        return RealInterval.construct((FrinkReal) RealMath.ceil(realInterval.getLower(), mathContext), (FrinkReal) RealMath.ceil(main, mathContext), (FrinkReal) RealMath.ceil(realInterval.getUpper(), mathContext), mathContext);
    }

    public static Numeric mod(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isInterval() && numeric2.isReal()) {
            return mod((RealInterval) numeric, (FrinkReal) numeric2, mathContext);
        }
        throw new NotImplementedException("mod is not defined for this combination of types: " + numeric + " mod " + numeric2, false);
    }

    public static Numeric mod(RealInterval realInterval, FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        FrinkInt frinkInt;
        FrinkReal mod;
        if (getQuadrant(realInterval.getLower(), frinkReal, mathContext) != getQuadrant(realInterval.getUpper(), frinkReal, mathContext)) {
            frinkInt = FrinkInt.ZERO;
            mod = frinkReal;
        } else {
            FrinkReal mod2 = RealMath.mod(realInterval.getLower(), frinkReal, NumericMath.ROUND_DOWN);
            frinkInt = mod2;
            mod = RealMath.mod(realInterval.getUpper(), frinkReal, NumericMath.ROUND_UP);
        }
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.construct(frinkInt, mod, mathContext);
        }
        return RealInterval.construct(frinkInt, RealMath.mod(main, frinkReal, mathContext), mod, mathContext);
    }

    public static Numeric arcsin(RealInterval realInterval, MathContext mathContext) throws NumericException {
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.constructMachine(RealMath.arcsin(realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arcsin(realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP), mathContext);
        }
        return RealInterval.constructMachine(RealMath.arcsin(realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arcsin(main, mathContext), RealMath.arcsin(realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP), mathContext);
    }

    public static Numeric arccos(RealInterval realInterval, MathContext mathContext) throws NumericException {
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.constructMachine(RealMath.arccos(realInterval.getUpper(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arccos(realInterval.getLower(), NumericMath.MACHINE_ROUND_UP), mathContext);
        }
        return RealInterval.constructMachine(RealMath.arccos(realInterval.getUpper(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arccos(main, mathContext), RealMath.arccos(realInterval.getLower(), NumericMath.MACHINE_ROUND_UP), mathContext);
    }

    public static Numeric arctan(RealInterval realInterval, MathContext mathContext) throws NumericException {
        FrinkReal main = realInterval.getMain();
        if (main == null) {
            return RealInterval.constructMachine(RealMath.arctan(realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP), mathContext);
        }
        return RealInterval.constructMachine(RealMath.arctan(realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(main, mathContext), RealMath.arctan(realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP), mathContext);
    }

    public static Numeric arctan(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isInterval()) {
            if (numeric2.isReal()) {
                return arctan((RealInterval) numeric, (FrinkReal) numeric2, mathContext);
            }
            if (numeric2.isInterval()) {
                return arctan((RealInterval) numeric, (RealInterval) numeric2, mathContext);
            }
        } else if (numeric.isReal() && numeric2.isInterval()) {
            return arctan((FrinkReal) numeric, (RealInterval) numeric2, mathContext);
        }
        throw new NotImplementedException("RealIntervalMath: arctan[x,y] not yet implemented for this combination of arguments: " + numeric + ", " + numeric2, false);
    }

    public static Numeric arctan(RealInterval realInterval, FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        FrinkReal arctan;
        int realSignum = frinkReal.realSignum();
        FrinkReal main = realInterval.getMain();
        switch (realSignum) {
            case -1:
                if (getBounds(realInterval) == 0) {
                    FrinkReal lower = realInterval.getLower();
                    FrinkReal negate = lower.negate();
                    FrinkReal upper = realInterval.getUpper();
                    int compare = RealMath.compare(negate, upper, mathContext);
                    FrinkReal arctan2 = RealMath.arctan(upper, frinkReal, NumericMath.MACHINE_ROUND_DOWN);
                    if (compare > 0) {
                        arctan2 = RealMath.subtract(arctan2, TWO_PI, NumericMath.MACHINE_ROUND_DOWN);
                    }
                    if (compare == 0) {
                        arctan = RealMath.subtract(TWO_PI, arctan2, NumericMath.MACHINE_ROUND_UP);
                    } else {
                        arctan = RealMath.arctan(lower, frinkReal, NumericMath.MACHINE_ROUND_UP);
                        if (compare == -1) {
                            arctan = RealMath.add(arctan, TWO_PI, NumericMath.MACHINE_ROUND_UP);
                        }
                    }
                    if (main == null) {
                        return RealInterval.construct(arctan2, arctan, mathContext);
                    }
                    FrinkReal arctan3 = RealMath.arctan(main, frinkReal, mathContext);
                    if (compare <= 0) {
                        if (RealMath.compare(arctan3, arctan2) < 0) {
                            arctan3 = RealMath.add(arctan3, TWO_PI, mathContext);
                        }
                    } else if (RealMath.compare(arctan3, arctan) > 0) {
                        arctan3 = RealMath.subtract(arctan3, TWO_PI, mathContext);
                    }
                    return RealInterval.construct(arctan2, arctan3, arctan, mathContext);
                } else if (main == null) {
                    return RealInterval.construct(RealMath.arctan(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_UP));
                } else {
                    return RealInterval.construct(RealMath.arctan(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(main, frinkReal, mathContext), RealMath.arctan(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_UP));
                }
            case 0:
                if (realInterval.getLower().realSignum() <= 0 && realInterval.getUpper().realSignum() >= 0) {
                    throw new NotImplementedException("RealIntervalMath:  arctan[x,y] passed x=" + realInterval + ", y=" + frinkReal + " which contains a discontinuity at x=0.", false);
                } else if (realInterval.getLower().realSignum() > 1) {
                    return HALF_PI;
                } else {
                    return NEGATIVE_HALF_PI;
                }
            case 1:
                if (main == null) {
                    return RealInterval.construct(RealMath.arctan(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_UP));
                }
                return RealInterval.construct(RealMath.arctan(realInterval.getLower(), frinkReal, NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(main, frinkReal, mathContext), RealMath.arctan(realInterval.getUpper(), frinkReal, NumericMath.MACHINE_ROUND_UP));
            default:
                throw new NotImplementedException("RealIntervalMath: arctan[x,y] not yet implemented for this combination of arguments: " + realInterval + ", " + frinkReal, true);
        }
    }

    public static Numeric arctan(FrinkReal frinkReal, RealInterval realInterval, MathContext mathContext) throws NumericException {
        int realSignum = frinkReal.realSignum();
        FrinkReal main = realInterval.getMain();
        switch (realSignum) {
            case -1:
                if (main == null) {
                    return RealInterval.construct(RealMath.arctan(frinkReal, realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(frinkReal, realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP));
                }
                return RealInterval.construct(RealMath.arctan(frinkReal, realInterval.getLower(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(frinkReal, main, mathContext), RealMath.arctan(frinkReal, realInterval.getUpper(), NumericMath.MACHINE_ROUND_UP));
            case 0:
                if (realInterval.getLower().realSignum() <= 0 && realInterval.getUpper().realSignum() >= 0) {
                    throw new NotImplementedException("RealIntervalMath:  arctan[x,y] passed x=" + frinkReal + ", y=" + realInterval + " which contains a discontinuity at [0,0].", false);
                } else if (realInterval.getLower().realSignum() > 1) {
                    return FrinkInt.ZERO;
                } else {
                    return PI;
                }
            case 1:
                if (main == null) {
                    return RealInterval.construct(RealMath.arctan(frinkReal, realInterval.getUpper(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(frinkReal, realInterval.getLower(), NumericMath.MACHINE_ROUND_UP));
                }
                return RealInterval.construct(RealMath.arctan(frinkReal, realInterval.getUpper(), NumericMath.MACHINE_ROUND_DOWN), RealMath.arctan(frinkReal, main, mathContext), RealMath.arctan(frinkReal, realInterval.getLower(), NumericMath.MACHINE_ROUND_UP));
            default:
                throw new NotImplementedException("RealIntervalMath: arctan[x,y] not yet implemented for this combination of arguments: " + frinkReal + ", " + realInterval, true);
        }
    }

    public static Numeric arctan(RealInterval realInterval, RealInterval realInterval2, MathContext mathContext) throws NumericException {
        FrinkReal frinkReal;
        FrinkReal frinkReal2;
        FrinkReal arctan;
        FrinkReal frinkReal3;
        FrinkReal arctan2;
        FrinkReal arctan3;
        boolean containsZero = containsZero(realInterval);
        boolean containsZero2 = containsZero(realInterval2);
        if (!containsZero || !containsZero2) {
            int bounds = getBounds(realInterval);
            int bounds2 = getBounds(realInterval2);
            FrinkReal lower = realInterval.getLower();
            FrinkReal upper = realInterval.getUpper();
            FrinkReal lower2 = realInterval2.getLower();
            FrinkReal upper2 = realInterval2.getUpper();
            FrinkReal main = realInterval.getMain();
            FrinkReal main2 = realInterval2.getMain();
            if (main == null || main2 == null) {
                frinkReal = null;
            } else {
                frinkReal = RealMath.arctan(main, main2, mathContext);
            }
            switch (bounds) {
                case -1:
                    switch (bounds2) {
                        case -1:
                            arctan2 = RealMath.arctan(upper, lower2, NumericMath.MACHINE_ROUND_DOWN);
                            FrinkReal arctan4 = RealMath.arctan(lower, upper2, NumericMath.MACHINE_ROUND_UP);
                            if (!containsZero) {
                                arctan = arctan4;
                                break;
                            } else {
                                arctan2 = NEGATIVE_PI;
                                arctan = arctan4;
                                break;
                            }
                        case 0:
                            arctan2 = RealMath.arctan(upper, lower2, NumericMath.MACHINE_ROUND_DOWN);
                            arctan = RealMath.arctan(upper, upper2, NumericMath.MACHINE_ROUND_UP);
                            break;
                        case 1:
                            arctan2 = RealMath.arctan(lower, lower2, NumericMath.MACHINE_ROUND_DOWN);
                            arctan = RealMath.arctan(upper, upper2, NumericMath.MACHINE_ROUND_UP);
                            break;
                        default:
                            arctan = null;
                            arctan2 = null;
                            break;
                    }
                    frinkReal2 = frinkReal;
                    break;
                case 0:
                    switch (bounds2) {
                        case -1:
                            int compare = RealMath.compare(lower.negate(), upper, mathContext);
                            frinkReal3 = RealMath.arctan(upper, upper2, NumericMath.MACHINE_ROUND_DOWN);
                            if (compare > 0) {
                                frinkReal3 = RealMath.subtract(frinkReal3, TWO_PI, NumericMath.MACHINE_ROUND_DOWN);
                            }
                            if (compare == 0) {
                                arctan3 = RealMath.subtract(TWO_PI, frinkReal3, NumericMath.MACHINE_ROUND_UP);
                            } else {
                                arctan3 = RealMath.arctan(lower, upper2, NumericMath.MACHINE_ROUND_UP);
                                if (compare == -1) {
                                    arctan3 = RealMath.add(arctan3, TWO_PI, NumericMath.MACHINE_ROUND_UP);
                                }
                            }
                            if (frinkReal != null) {
                                if (compare > 0) {
                                    if (RealMath.compare(frinkReal, arctan3) > 0) {
                                        FrinkReal frinkReal4 = arctan3;
                                        frinkReal2 = RealMath.subtract(frinkReal, TWO_PI, mathContext);
                                        arctan = frinkReal4;
                                        break;
                                    }
                                } else if (RealMath.compare(frinkReal, frinkReal3) < 0) {
                                    FrinkReal frinkReal5 = arctan3;
                                    frinkReal2 = RealMath.add(frinkReal, TWO_PI, mathContext);
                                    arctan = frinkReal5;
                                    break;
                                }
                            }
                            arctan = arctan3;
                            frinkReal2 = frinkReal;
                            break;
                        case 0:
                        default:
                            arctan = null;
                            frinkReal3 = null;
                            frinkReal2 = frinkReal;
                            break;
                        case 1:
                            frinkReal2 = frinkReal;
                            FrinkReal arctan5 = RealMath.arctan(lower, lower2, NumericMath.MACHINE_ROUND_DOWN);
                            arctan = RealMath.arctan(upper, lower2, NumericMath.MACHINE_ROUND_UP);
                            frinkReal3 = arctan5;
                            break;
                    }
                case 1:
                    switch (bounds2) {
                        case -1:
                            frinkReal3 = RealMath.arctan(upper, upper2, NumericMath.MACHINE_ROUND_DOWN);
                            FrinkReal arctan6 = RealMath.arctan(lower, lower2, NumericMath.MACHINE_ROUND_UP);
                            if (!containsZero) {
                                arctan = arctan6;
                                frinkReal2 = frinkReal;
                                break;
                            } else {
                                arctan = PI;
                                frinkReal2 = frinkReal;
                                break;
                            }
                        case 0:
                            frinkReal2 = frinkReal;
                            FrinkReal arctan7 = RealMath.arctan(lower, upper2, NumericMath.MACHINE_ROUND_DOWN);
                            arctan = RealMath.arctan(lower, lower2, NumericMath.MACHINE_ROUND_UP);
                            frinkReal3 = arctan7;
                            break;
                        case 1:
                            frinkReal2 = frinkReal;
                            FrinkReal arctan8 = RealMath.arctan(lower, upper2, NumericMath.MACHINE_ROUND_DOWN);
                            arctan = RealMath.arctan(upper, lower2, NumericMath.MACHINE_ROUND_UP);
                            frinkReal3 = arctan8;
                            break;
                    }
                default:
                    arctan = null;
                    frinkReal3 = null;
                    frinkReal2 = frinkReal;
                    break;
            }
            if (frinkReal3 == null || arctan == null) {
                throw new NotImplementedException("RealIntervalMath:  arctan[x,y] not implemented correctly for x=" + realInterval + ", y=" + realInterval2, true);
            } else if (frinkReal2 == null) {
                return RealInterval.construct(frinkReal3, arctan, mathContext);
            } else {
                return RealInterval.construct(frinkReal3, frinkReal2, arctan, mathContext);
            }
        } else {
            throw new NotImplementedException("RealIntervalMath:  arctan[x,y] passed x=" + realInterval + ", y=" + realInterval2 + " which contains a discontinuity at [0,0].", false);
        }
    }
}
