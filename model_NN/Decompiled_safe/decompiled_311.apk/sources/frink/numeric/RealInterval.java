package frink.numeric;

import frink.errors.NotRealException;

public class RealInterval implements Interval<FrinkReal>, Numeric {
    private static boolean collapseIntervals = true;
    private FrinkReal lower;
    private FrinkReal main;
    private FrinkReal upper;

    private RealInterval(FrinkReal frinkReal, FrinkReal frinkReal2) {
        this.lower = frinkReal;
        this.upper = frinkReal2;
        this.main = null;
    }

    private RealInterval(FrinkReal frinkReal, FrinkReal frinkReal2, FrinkReal frinkReal3) {
        this.lower = frinkReal;
        this.upper = frinkReal3;
        this.main = frinkReal2;
    }

    public static Numeric construct(Numeric numeric, Numeric numeric2) throws NumericException {
        return construct(numeric, numeric2, NumericMath.mc);
    }

    public static Numeric construct(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric2.isReal()) {
            return construct((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
        }
        throw new NotRealException("RealInterval.construct:  Arguments were not real.");
    }

    public static Numeric constructMachine(Numeric numeric, Numeric numeric2) throws NumericException {
        return constructMachine(numeric, (Numeric) null, numeric2, NumericMath.mc);
    }

    public static Numeric constructMachine(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        return constructMachine(numeric, (Numeric) null, numeric2, mathContext);
    }

    public static Numeric constructMachine(Numeric numeric, Numeric numeric2, Numeric numeric3, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric3.isReal() && (numeric2 == null || numeric2.isReal())) {
            return constructMachine((FrinkReal) numeric, (FrinkReal) numeric2, (FrinkReal) numeric3, mathContext);
        }
        throw new NotRealException("RealInterval.constructMachine:  Arguments were not real.");
    }

    public static Numeric constructMachine(FrinkReal frinkReal, FrinkReal frinkReal2, FrinkReal frinkReal3, MathContext mathContext) throws NumericException {
        Numeric constructMachineOrdered;
        if (frinkReal2 == null) {
            return constructMachine(frinkReal, frinkReal3, mathContext);
        }
        int compare = RealMath.compare(frinkReal, frinkReal3, mathContext);
        switch (compare) {
            case -1:
                constructMachineOrdered = constructMachineOrdered(frinkReal, frinkReal2, frinkReal3);
                break;
            case 0:
                if (RealMath.compare(frinkReal, frinkReal2, mathContext) == 0) {
                    if (!collapseIntervals) {
                        constructMachineOrdered = constructMachineOrdered(frinkReal, frinkReal2, frinkReal3);
                        break;
                    } else {
                        return frinkReal;
                    }
                } else {
                    throw new NotImplementedException("RealInterval::construct: (1) main value does not lie between bounds.  Values are [" + frinkReal + ", " + frinkReal2 + ", " + frinkReal3 + "].", true);
                }
            case 1:
                constructMachineOrdered = constructMachineOrdered(frinkReal3, frinkReal2, frinkReal);
                break;
            default:
                throw new NotImplementedException("RealInterval::construct: compare returned invalid value " + compare, true);
        }
        if (!(constructMachineOrdered instanceof RealInterval)) {
            return constructMachineOrdered;
        }
        RealInterval realInterval = (RealInterval) constructMachineOrdered;
        if (RealMath.compare(realInterval.getLower(), frinkReal2, mathContext) > 0 || RealMath.compare(frinkReal2, realInterval.getUpper(), mathContext) > 0) {
            throw new NotImplementedException("RealInterval::construct: (2) main value does not lie between bounds.  Values are [" + frinkReal + ", " + frinkReal2 + ", " + frinkReal3 + "].", true);
        }
        realInterval.main = frinkReal2;
        return constructMachineOrdered;
    }

    public static Numeric construct(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        int compare = RealMath.compare(frinkReal, frinkReal2, mathContext);
        switch (compare) {
            case -1:
                return new RealInterval(frinkReal, frinkReal2);
            case 0:
                if (collapseIntervals) {
                    return frinkReal;
                }
                return new RealInterval(frinkReal, frinkReal2);
            case 1:
                return new RealInterval(frinkReal2, frinkReal);
            default:
                throw new NotImplementedException("RealInterval::construct: compare returned invalid value " + compare, true);
        }
    }

    public static Numeric constructMachine(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        int compare = RealMath.compare(frinkReal, frinkReal2, mathContext);
        switch (compare) {
            case -1:
                return constructMachineOrdered(frinkReal, null, frinkReal2);
            case 0:
                if (collapseIntervals) {
                    return frinkReal;
                }
                return constructMachineOrdered(frinkReal, null, frinkReal2);
            case 1:
                return constructMachineOrdered(frinkReal2, null, frinkReal);
            default:
                throw new NotImplementedException("RealInterval::construct: compare returned invalid value " + compare, true);
        }
    }

    private static Numeric constructMachineOrdered(FrinkReal frinkReal, FrinkReal frinkReal2, FrinkReal frinkReal3) throws NumericException {
        FrinkReal frinkReal4;
        FrinkReal frinkReal5;
        if (frinkReal.isFloat()) {
            frinkReal4 = ((FrinkFloat) frinkReal).round(NumericMath.MACHINE_ROUND_DOWN);
        } else {
            frinkReal4 = frinkReal;
        }
        if (frinkReal3.isFloat()) {
            frinkReal5 = ((FrinkFloat) frinkReal3).round(NumericMath.MACHINE_ROUND_UP);
        } else {
            frinkReal5 = frinkReal3;
        }
        if (frinkReal2 == null) {
            return new RealInterval(frinkReal4, frinkReal5);
        }
        return new RealInterval(frinkReal4, frinkReal2, frinkReal5);
    }

    public static Numeric construct(Numeric numeric, Numeric numeric2, Numeric numeric3) throws NumericException {
        return construct(numeric, numeric2, numeric3, NumericMath.mc);
    }

    public static Numeric construct(Numeric numeric, Numeric numeric2, Numeric numeric3, MathContext mathContext) throws NumericException {
        if (numeric.isReal() && numeric3.isReal() && (numeric2 == null || numeric2.isReal())) {
            return construct((FrinkReal) numeric, (FrinkReal) numeric2, (FrinkReal) numeric3, mathContext);
        }
        throw new NotRealException("RealInterval.construct:  Arguments were not real.");
    }

    public static Numeric construct(FrinkReal frinkReal, FrinkReal frinkReal2, FrinkReal frinkReal3, MathContext mathContext) throws NumericException {
        RealInterval realInterval;
        if (frinkReal2 == null) {
            return construct(frinkReal, frinkReal3, mathContext);
        }
        int compare = RealMath.compare(frinkReal, frinkReal3, mathContext);
        switch (compare) {
            case -1:
                realInterval = new RealInterval(frinkReal, frinkReal3);
                break;
            case 0:
                if (RealMath.compare(frinkReal, frinkReal2, mathContext) == 0) {
                    if (!collapseIntervals) {
                        realInterval = new RealInterval(frinkReal, frinkReal3);
                        break;
                    } else {
                        return frinkReal;
                    }
                } else {
                    throw new NotImplementedException("RealInterval::construct: (3) main value does not lie between bounds.  Values are [" + frinkReal + ", " + frinkReal2 + ", " + frinkReal3 + "].", true);
                }
            case 1:
                realInterval = new RealInterval(frinkReal3, frinkReal);
                break;
            default:
                throw new NotImplementedException("RealInterval::construct: compare returned invalid value " + compare, true);
        }
        if (RealMath.compare(realInterval.getLower(), frinkReal2, mathContext) > 0 || RealMath.compare(frinkReal2, realInterval.getUpper(), mathContext) > 0) {
            throw new NotImplementedException("RealInterval::construct: (4) main value does not lie between bounds.  Values are [" + frinkReal + ", " + frinkReal2 + ", " + frinkReal3 + "].", true);
        }
        realInterval.main = frinkReal2;
        return realInterval;
    }

    public FrinkReal getLower() {
        return this.lower;
    }

    public FrinkReal getUpper() {
        return this.upper;
    }

    public FrinkReal getMain() {
        return this.main;
    }

    public String toString() {
        return "[" + this.lower.toString() + ", " + (this.main != null ? this.main.toString() + ", " : "") + this.upper.toString() + "]";
    }

    public String toString(int i) {
        return "[" + format(this.lower, i) + ", " + (this.main != null ? format(this.main, i) + ", " : "") + format(this.upper, i) + "]";
    }

    private static String format(Numeric numeric, int i) {
        FrinkBigDecimal frinkBigDecimal;
        if (numeric.isFloat()) {
            frinkBigDecimal = ((FrinkFloat) numeric).getBigDec();
        } else {
            try {
                frinkBigDecimal = new FrinkBigDecimal(numeric.doubleValue());
            } catch (NotRealException e) {
                return numeric.toString();
            }
        }
        return frinkBigDecimal.format(-1, i, -1, -1, 2, 4);
    }

    public boolean isFrinkInteger() {
        return false;
    }

    public boolean isBigInteger() {
        return false;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isRational() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isReal() {
        return false;
    }

    public boolean isComplex() {
        return false;
    }

    public boolean isInterval() {
        return true;
    }

    public double doubleValue() throws NotRealException {
        throw new NotRealException("Interval " + toString() + " cannot be converted to a double value.");
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) throws NotRealException {
        throw new NotRealException("Interval " + toString() + " cannot be converted to FrinkFloat.");
    }

    public RealInterval negate() {
        if (this.main == null) {
            return new RealInterval(this.upper.negate(), this.lower.negate());
        }
        return new RealInterval(this.upper.negate(), this.main.negate(), this.lower.negate());
    }

    public int hashCode() {
        return (this.upper.hashCode() + this.lower.hashCode()) | 1073741824;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof RealInterval) {
            RealInterval realInterval = (RealInterval) obj;
            if (realInterval.upper.equals(this.upper) && realInterval.lower.equals(this.lower)) {
                return true;
            }
        }
        return false;
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }

    public static void setCollapseIntervals(boolean z) {
        collapseIntervals = z;
    }
}
