package frink.numeric;

import frink.errors.NotAnIntegerException;

public class ComplexMath {
    public static Numeric add(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        boolean isComplex = numeric.isComplex();
        boolean isComplex2 = numeric2.isComplex();
        if (isComplex && isComplex2) {
            FrinkComplex frinkComplex = (FrinkComplex) numeric;
            FrinkComplex frinkComplex2 = (FrinkComplex) numeric2;
            return FrinkComplex.construct(RealMath.add(frinkComplex.getReal(), frinkComplex2.getReal(), mathContext), RealMath.add(frinkComplex.getImag(), frinkComplex2.getImag(), mathContext));
        } else if (isComplex && numeric2.isReal()) {
            FrinkComplex frinkComplex3 = (FrinkComplex) numeric;
            return FrinkComplex.construct(RealMath.add(frinkComplex3.getReal(), (FrinkReal) numeric2, mathContext), frinkComplex3.getImag());
        } else if (!isComplex2 || !numeric.isReal()) {
            throw new NotImplementedException("ComplexMath.add: No valid combination of arguments passed in: [" + numeric.toString() + "," + numeric2.toString() + "]", false);
        } else {
            FrinkComplex frinkComplex4 = (FrinkComplex) numeric2;
            return FrinkComplex.construct(RealMath.add(frinkComplex4.getReal(), (FrinkReal) numeric, mathContext), frinkComplex4.getImag());
        }
    }

    public static Numeric subtract(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        boolean isComplex = numeric.isComplex();
        boolean isComplex2 = numeric2.isComplex();
        if (isComplex && isComplex2) {
            FrinkComplex frinkComplex = (FrinkComplex) numeric;
            FrinkComplex frinkComplex2 = (FrinkComplex) numeric2;
            return FrinkComplex.construct(RealMath.subtract(frinkComplex.getReal(), frinkComplex2.getReal(), mathContext), RealMath.subtract(frinkComplex.getImag(), frinkComplex2.getImag(), mathContext));
        } else if (isComplex && numeric2.isReal()) {
            FrinkComplex frinkComplex3 = (FrinkComplex) numeric;
            return FrinkComplex.construct(RealMath.subtract(frinkComplex3.getReal(), (FrinkReal) numeric2, mathContext), frinkComplex3.getImag());
        } else if (!numeric.isReal() || !isComplex2) {
            throw new NotImplementedException("ComplexMath.subtract: No appropriate combination of arguments passed in: [" + numeric.toString() + "," + numeric2.toString() + "]", false);
        } else {
            FrinkComplex frinkComplex4 = (FrinkComplex) numeric2;
            return FrinkComplex.construct(RealMath.subtract((FrinkReal) numeric, frinkComplex4.getReal(), mathContext), frinkComplex4.getImag().negate());
        }
    }

    public static Numeric multiply(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        boolean isComplex = numeric.isComplex();
        boolean isComplex2 = numeric2.isComplex();
        if (isComplex && isComplex2) {
            FrinkComplex frinkComplex = (FrinkComplex) numeric;
            FrinkComplex frinkComplex2 = (FrinkComplex) numeric2;
            FrinkReal real = frinkComplex.getReal();
            FrinkReal imag = frinkComplex.getImag();
            FrinkReal real2 = frinkComplex2.getReal();
            FrinkReal imag2 = frinkComplex2.getImag();
            return FrinkComplex.construct(RealMath.subtract(RealMath.multiply(real, real2, mathContext), RealMath.multiply(imag, imag2, mathContext), mathContext), RealMath.add(RealMath.multiply(real, imag2, mathContext), RealMath.multiply(imag, real2, mathContext), mathContext));
        } else if (isComplex && numeric2.isReal()) {
            FrinkComplex frinkComplex3 = (FrinkComplex) numeric;
            FrinkReal frinkReal = (FrinkReal) numeric2;
            return FrinkComplex.construct(RealMath.multiply(frinkReal, frinkComplex3.getReal(), mathContext), RealMath.multiply(frinkReal, frinkComplex3.getImag(), mathContext));
        } else if (!numeric.isReal() || !isComplex2) {
            throw new NotImplementedException("ComplexMath.multiply: No complex arguments passed in: [" + numeric.toString() + "," + numeric2.toString() + "]", false);
        } else {
            FrinkComplex frinkComplex4 = (FrinkComplex) numeric2;
            FrinkReal frinkReal2 = (FrinkReal) numeric;
            return FrinkComplex.construct(RealMath.multiply(frinkReal2, frinkComplex4.getReal(), mathContext), RealMath.multiply(frinkReal2, frinkComplex4.getImag(), mathContext));
        }
    }

    public static Numeric divide(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        boolean isComplex = numeric.isComplex();
        boolean isComplex2 = numeric2.isComplex();
        if (isComplex && isComplex2) {
            FrinkComplex frinkComplex = (FrinkComplex) numeric;
            FrinkComplex frinkComplex2 = (FrinkComplex) numeric2;
            FrinkReal real = frinkComplex.getReal();
            FrinkReal imag = frinkComplex.getImag();
            FrinkReal real2 = frinkComplex2.getReal();
            FrinkReal imag2 = frinkComplex2.getImag();
            FrinkReal add = RealMath.add(RealMath.multiply(real2, real2, mathContext), RealMath.multiply(imag2, imag2, mathContext), mathContext);
            return FrinkComplex.construct(RealMath.divide(RealMath.add(RealMath.multiply(real, real2, mathContext), RealMath.multiply(imag, imag2, mathContext), mathContext), add, mathContext), RealMath.divide(RealMath.subtract(RealMath.multiply(imag, real2, mathContext), RealMath.multiply(real, imag2, mathContext), mathContext), add, mathContext));
        } else if (isComplex && numeric2.isReal()) {
            FrinkComplex frinkComplex3 = (FrinkComplex) numeric;
            FrinkReal frinkReal = (FrinkReal) numeric2;
            return FrinkComplex.construct(RealMath.divide(frinkComplex3.getReal(), frinkReal, mathContext), RealMath.divide(frinkComplex3.getImag(), frinkReal, mathContext));
        } else if (!isComplex2 || !numeric.isReal()) {
            throw new NotImplementedException("ComplexMath.divide: No appropriate arguments passed in: [" + numeric.toString() + "," + numeric2.toString() + "]", false);
        } else {
            FrinkComplex frinkComplex4 = (FrinkComplex) numeric2;
            FrinkReal frinkReal2 = (FrinkReal) numeric;
            FrinkReal real3 = frinkComplex4.getReal();
            FrinkReal imag3 = frinkComplex4.getImag();
            FrinkReal add2 = RealMath.add(RealMath.multiply(real3, real3, mathContext), RealMath.multiply(imag3, imag3, mathContext), mathContext);
            return FrinkComplex.construct(RealMath.divide(RealMath.multiply(frinkReal2, real3, mathContext), add2, mathContext), RealMath.divide(RealMath.multiply(frinkReal2, imag3, mathContext).negate(), add2, mathContext));
        }
    }

    public static Numeric power(Numeric numeric, Numeric numeric2, MathContext mathContext) throws NumericException {
        if (numeric.isComplex()) {
            if (numeric2.isComplex()) {
                return power((FrinkComplex) numeric, (FrinkComplex) numeric2, mathContext);
            }
            if (numeric2.isReal()) {
                return power((FrinkComplex) numeric, (FrinkReal) numeric2, mathContext);
            }
        }
        if (numeric.isReal()) {
            if (numeric2.isReal()) {
                return power((FrinkReal) numeric, (FrinkReal) numeric2, mathContext);
            }
            if (numeric2.isComplex()) {
                return power((FrinkReal) numeric, (FrinkComplex) numeric2, mathContext);
            }
        }
        throw new NotImplementedException("ComplexMath.power not implemented for these args: " + numeric.toString() + "^" + numeric2.toString(), false);
    }

    public static Numeric power(FrinkReal frinkReal, FrinkReal frinkReal2, MathContext mathContext) throws NumericException {
        if (frinkReal2.isRational() && frinkReal.realSignum() < 0) {
            try {
                FrinkRational frinkRational = (FrinkRational) frinkReal2;
                if (NumericMath.getIntegerValue(frinkRational.getNumerator()) == 1 && NumericMath.getIntegerValue(frinkRational.getDenominator()) == 2) {
                    return FrinkComplex.construct(FrinkInt.ZERO, new FrinkFloat(Math.sqrt(-frinkReal.doubleValue())));
                }
            } catch (NotAnIntegerException e) {
            }
        }
        try {
            if (NumericMath.getIntegerValue(frinkReal2) == -1) {
                return RealMath.divide(FrinkInt.ONE, frinkReal, mathContext);
            }
        } catch (NotAnIntegerException e2) {
        }
        Numeric multiply = NumericMath.multiply(ln(frinkReal, mathContext), frinkReal2, mathContext);
        if (multiply.isReal()) {
            return new FrinkFloat(Math.exp(((FrinkReal) multiply).doubleValue()));
        }
        if (multiply.isComplex()) {
            return exp((FrinkComplex) multiply);
        }
        throw new NotImplementedException("ComplexMath.power(complex, real) got weird answer for [" + frinkReal.toString() + ", " + frinkReal2.toString() + "]", false);
    }

    public static Numeric power(FrinkComplex frinkComplex, FrinkReal frinkReal, MathContext mathContext) throws NumericException {
        try {
            int integerValue = NumericMath.getIntegerValue(frinkReal);
            if (integerValue == -1) {
                return divide(FrinkInt.ONE, frinkComplex, mathContext);
            }
            if (integerValue == 2) {
                return multiply(frinkComplex, frinkComplex, mathContext);
            }
            if (integerValue == 1) {
                return frinkComplex;
            }
            if (integerValue == 0) {
                return FrinkInt.ONE;
            }
            if (NumericMath.getIntegerValue(frinkComplex.getReal()) == 0) {
                FrinkReal frinkReal2 = (FrinkReal) RealMath.power(frinkComplex.getImag(), frinkReal, mathContext);
                int i = integerValue & 3;
                if (i == 2 || i == 3) {
                    frinkReal2 = frinkReal2.negate();
                }
                if ((integerValue & 1) == 1) {
                    return FrinkComplex.construct(FrinkInt.ZERO, frinkReal2);
                }
                return frinkReal2;
            }
            Numeric multiply = multiply(ln(frinkComplex, mathContext), frinkReal, mathContext);
            if (multiply.isReal()) {
                return new FrinkFloat(Math.exp(((FrinkReal) multiply).doubleValue()));
            }
            if (multiply.isComplex()) {
                return exp((FrinkComplex) multiply);
            }
            throw new NotImplementedException("ComplexMath.power(complex, real) got weird answer for [" + frinkComplex.toString() + ", " + frinkReal.toString() + "]", false);
        } catch (NotAnIntegerException e) {
        }
    }

    public static Numeric power(FrinkComplex frinkComplex, FrinkComplex frinkComplex2, MathContext mathContext) throws NumericException {
        Numeric multiply = multiply(ln(frinkComplex, mathContext), frinkComplex2, mathContext);
        if (multiply.isReal()) {
            return new FrinkFloat(Math.exp(((FrinkReal) multiply).doubleValue()));
        }
        if (multiply.isComplex()) {
            return exp((FrinkComplex) multiply);
        }
        throw new NotImplementedException("ComplexMath.power(complex, complex) got weird answer for [" + frinkComplex.toString() + ", " + frinkComplex2.toString() + "]", false);
    }

    public static Numeric power(FrinkReal frinkReal, FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        Numeric multiply = multiply(ln(frinkReal, mathContext), frinkComplex, mathContext);
        if (multiply.isReal()) {
            return new FrinkFloat(Math.exp(((FrinkReal) multiply).doubleValue()));
        }
        if (multiply.isComplex()) {
            return exp((FrinkComplex) multiply);
        }
        throw new NotImplementedException("ComplexMath.power(real, complex) got weird answer for [" + frinkReal.toString() + ", " + frinkComplex.toString() + "]", false);
    }

    public static Numeric exp(FrinkComplex frinkComplex) {
        double exp = Math.exp(frinkComplex.getReal().doubleValue());
        double doubleValue = frinkComplex.getImag().doubleValue();
        return FrinkComplex.construct(Math.cos(doubleValue) * exp, exp * Math.sin(doubleValue));
    }

    public static Numeric ln(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        double modulus = modulus(frinkComplex, mathContext);
        double arg = arg(frinkComplex);
        if (arg > 3.141592653589793d) {
            arg -= 6.283185307179586d;
        }
        return FrinkComplex.construct(Math.log(modulus), arg);
    }

    public static Numeric ln(FrinkReal frinkReal, MathContext mathContext) {
        if (frinkReal.realSignum() > 0) {
            return new FrinkFloat(Math.log(frinkReal.doubleValue()));
        }
        return FrinkComplex.construct(Math.log(Math.abs(frinkReal.doubleValue())), 3.141592653589793d);
    }

    public static Numeric sin(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        FrinkReal real = frinkComplex.getReal();
        FrinkReal imag = frinkComplex.getImag();
        return FrinkComplex.construct(RealMath.multiply(RealMath.sin(real, mathContext), RealMath.cosh(imag, mathContext), mathContext), RealMath.multiply(RealMath.cos(real, mathContext), RealMath.sinh(imag, mathContext), mathContext));
    }

    public static Numeric cos(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        FrinkReal real = frinkComplex.getReal();
        FrinkReal imag = frinkComplex.getImag();
        return FrinkComplex.construct(RealMath.multiply(RealMath.cos(real, mathContext), RealMath.cosh(imag, mathContext), mathContext), RealMath.multiply(RealMath.sin(real, mathContext), RealMath.sinh(imag, mathContext), mathContext).negate());
    }

    public static Numeric tan(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        return divide(sin(frinkComplex, mathContext), cos(frinkComplex, mathContext), mathContext);
    }

    public static Numeric arctan(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        Numeric multiply = multiply(FrinkComplex.I, frinkComplex, mathContext);
        return NumericMath.multiply(FrinkComplex.I_2, NumericMath.subtract(NumericMath.ln(NumericMath.subtract(FrinkInt.ONE, multiply, mathContext), mathContext), NumericMath.ln(NumericMath.add(multiply, FrinkInt.ONE, mathContext), mathContext), mathContext), mathContext);
    }

    public static Numeric arcsin(Numeric numeric, MathContext mathContext) throws NumericException {
        return NumericMath.multiply(FrinkComplex.NEGATIVE_I, NumericMath.ln(NumericMath.add(NumericMath.multiply(FrinkComplex.I, numeric, mathContext), NumericMath.power(NumericMath.subtract(FrinkInt.ONE, NumericMath.multiply(numeric, numeric, mathContext), mathContext), FrinkRational.ONE_HALF, mathContext), mathContext), mathContext), mathContext);
    }

    public static Numeric arccos(Numeric numeric, MathContext mathContext) throws NumericException {
        return NumericMath.subtract(NumericMath.HALF_PI, arcsin(numeric, mathContext), mathContext);
    }

    public static Numeric arcsec(Numeric numeric, MathContext mathContext) throws NumericException {
        return NumericMath.arccos(NumericMath.divide(FrinkInt.ONE, numeric, mathContext), mathContext);
    }

    public static Numeric arccsc(Numeric numeric, MathContext mathContext) throws NumericException {
        return NumericMath.arcsin(NumericMath.divide(FrinkInt.ONE, numeric, mathContext), mathContext);
    }

    public static Numeric arccot(Numeric numeric, MathContext mathContext) throws NumericException {
        return NumericMath.arctan(NumericMath.divide(FrinkInt.ONE, numeric, mathContext), mathContext);
    }

    private static double arg(FrinkComplex frinkComplex) {
        return Math.atan2(frinkComplex.getImag().doubleValue(), frinkComplex.getReal().doubleValue());
    }

    public static double modulus(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        FrinkReal real = frinkComplex.getReal();
        FrinkReal imag = frinkComplex.getImag();
        return Math.sqrt(RealMath.add(RealMath.multiply(real, real, mathContext), RealMath.multiply(imag, imag, mathContext), mathContext).doubleValue());
    }

    public static Numeric abs(FrinkComplex frinkComplex, MathContext mathContext) throws NumericException {
        return new FrinkFloat(modulus(frinkComplex, mathContext));
    }

    public static Numeric signum(FrinkComplex frinkComplex) throws NumericException {
        return NumericMath.divide(frinkComplex, abs(frinkComplex, NumericMath.mc));
    }
}
