package frink.numeric;

import java.math.BigInteger;

public class NumberSystems {
    public static final char DEVANAGARI_ZERO = '०';
    private static final BigInteger TEN = BigInteger.valueOf(10);

    public static FrinkInteger parseDevanagariInt(String str) {
        if (str.length() >= 10) {
            return FrinkInteger.construct(parseDevanagariIntBig(str));
        }
        return FrinkInteger.construct(parseDevanagariIntSmall(str));
    }

    private static int parseDevanagariIntSmall(String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i = (i * 10) + (str.charAt(i2) - DEVANAGARI_ZERO);
        }
        return i;
    }

    private static BigInteger parseDevanagariIntBig(String str) {
        int length = str.length();
        BigInteger bigInteger = BigInteger.ZERO;
        for (int i = 0; i < length; i++) {
            bigInteger = bigInteger.multiply(TEN).add(BigInteger.valueOf((long) (str.charAt(i) - DEVANAGARI_ZERO)));
        }
        return bigInteger;
    }
}
