package frink.numeric;

import java.math.BigInteger;

public final class FrinkFloat implements FrinkReal {
    private static int outputFormat = 1;
    private FrinkBigDecimal bigDec;

    public FrinkFloat(String str) {
        this.bigDec = new FrinkBigDecimal(str);
    }

    public FrinkFloat(double d) {
        this.bigDec = new FrinkBigDecimal(d);
    }

    public FrinkFloat(FrinkBigDecimal frinkBigDecimal) {
        this.bigDec = frinkBigDecimal;
    }

    public FrinkFloat(int i) {
        this.bigDec = new FrinkBigDecimal(i);
    }

    public FrinkFloat(BigInteger bigInteger) {
        this.bigDec = new FrinkBigDecimal(bigInteger);
    }

    public FrinkBigDecimal getBigDec() {
        return this.bigDec;
    }

    public String toString() {
        return toString(this.bigDec);
    }

    public static String toString(FrinkBigDecimal frinkBigDecimal) {
        boolean z;
        String format = frinkBigDecimal.format(-1, -1, -1, 6, outputFormat, 4);
        int indexOf = format.indexOf(".");
        if (indexOf == -1) {
            z = true;
        } else {
            z = false;
        }
        if (format.indexOf(69, indexOf + 1) != -1 || format.indexOf(101, indexOf + 1) != -1) {
            return format;
        }
        if (z) {
            return format + ".";
        }
        int length = format.length();
        int i = indexOf + 1;
        for (int i2 = indexOf + 1; i2 < length; i2++) {
            if (format.charAt(i2) != '0') {
                i = i2;
            }
        }
        if (i <= length - 1) {
            return format.substring(0, i + 1);
        }
        return format;
    }

    public FrinkFloat round(MathContext mathContext) {
        return new FrinkFloat(this.bigDec.multiply(FrinkBigDecimal.ONE, mathContext));
    }

    public double doubleValue() {
        return this.bigDec.doubleValue();
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) {
        return this;
    }

    public int realSignum() {
        return this.bigDec.signum();
    }

    public FrinkReal negate() {
        return new FrinkFloat(this.bigDec.negate());
    }

    public final boolean isFrinkInteger() {
        return false;
    }

    public final boolean isBigInteger() {
        return false;
    }

    public final boolean isInt() {
        return false;
    }

    public final boolean isRational() {
        return false;
    }

    public final boolean isFloat() {
        return true;
    }

    public final boolean isReal() {
        return true;
    }

    public boolean isComplex() {
        return false;
    }

    public boolean isInterval() {
        return false;
    }

    public int hashCode() {
        return this.bigDec.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FrinkFloat) || !((FrinkFloat) obj).bigDec.equals(this.bigDec)) {
            return false;
        }
        return true;
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }

    public static void setEngineering(boolean z) {
        if (z) {
            outputFormat = 2;
        } else {
            outputFormat = 1;
        }
    }
}
