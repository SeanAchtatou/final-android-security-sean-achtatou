package frink.numeric;

import frink.errors.NotAnIntegerException;
import frink.parser.sym;
import java.math.BigInteger;

public final class FrinkBigInteger extends FrinkInteger {
    public static final BigInteger EIGHT = BigInteger.valueOf(8);
    public static final BigInteger ELEVEN = BigInteger.valueOf(11);
    public static final BigInteger FIVE = BigInteger.valueOf(5);
    public static final BigInteger FOUR = BigInteger.valueOf(4);
    public static final BigInteger MAXBYTE = BigInteger.valueOf(127);
    public static final BigInteger MAXINT = BigInteger.valueOf(2147483647L);
    public static final BigInteger MAXLONG = BigInteger.valueOf(Long.MAX_VALUE);
    public static final BigInteger MAXSHORT = BigInteger.valueOf(32767);
    public static final BigInteger MINBYTE = BigInteger.valueOf(-128);
    public static final BigInteger MININT = BigInteger.valueOf(-2147483648L);
    public static final BigInteger MINLONG = BigInteger.valueOf(Long.MIN_VALUE);
    public static final BigInteger MINSHORT = BigInteger.valueOf(-32768);
    public static final BigInteger ONE = BigInteger.valueOf(1);
    public static final BigInteger SEVEN = BigInteger.valueOf(7);
    public static final BigInteger SEVENTEEN = BigInteger.valueOf(17);
    public static final BigInteger THIRTEEN = BigInteger.valueOf(13);
    public static final BigInteger THREE = BigInteger.valueOf(3);
    public static final BigInteger TWO = BigInteger.valueOf(2);
    public static final BigInteger ZERO = BigInteger.valueOf(0);
    private BigInteger bigInt;

    FrinkBigInteger(BigInteger bigInteger) {
        this.bigInt = bigInteger;
    }

    public static BigInteger makeBigInt(int i) {
        switch (i) {
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case 4:
                return FOUR;
            case 5:
                return FIVE;
            case 6:
            case 9:
            case 10:
            case 12:
            case 14:
            case sym.PERCENT /*15*/:
            case 16:
            default:
                return BigInteger.valueOf((long) i);
            case 7:
                return SEVEN;
            case 8:
                return EIGHT;
            case sym.EVAL /*11*/:
                return ELEVEN;
            case sym.NOEVAL /*13*/:
                return THIRTEEN;
            case sym.PIPE /*17*/:
                return SEVENTEEN;
        }
    }

    FrinkBigInteger(long j) {
        this.bigInt = BigInteger.valueOf(j);
    }

    public BigInteger getBigInt() {
        return this.bigInt;
    }

    public long getLong() throws NotAnIntegerException {
        if (this.bigInt.compareTo(MAXLONG) == 1) {
            throw NotAnIntegerException.INSTANCE;
        } else if (this.bigInt.compareTo(MINLONG) != -1) {
            return this.bigInt.longValue();
        } else {
            throw NotAnIntegerException.INSTANCE;
        }
    }

    public int getInt() throws NotAnIntegerException {
        if (this.bigInt.compareTo(MAXINT) == 1) {
            throw NotAnIntegerException.INSTANCE;
        } else if (this.bigInt.compareTo(MININT) != -1) {
            return this.bigInt.intValue();
        } else {
            throw NotAnIntegerException.INSTANCE;
        }
    }

    public short getShort() throws NotAnIntegerException {
        if (this.bigInt.compareTo(MAXSHORT) == 1) {
            throw NotAnIntegerException.INSTANCE;
        } else if (this.bigInt.compareTo(MINSHORT) != -1) {
            return (short) this.bigInt.intValue();
        } else {
            throw NotAnIntegerException.INSTANCE;
        }
    }

    public byte getByte() throws NotAnIntegerException {
        if (this.bigInt.compareTo(MAXBYTE) == 1) {
            throw NotAnIntegerException.INSTANCE;
        } else if (this.bigInt.compareTo(MINBYTE) != -1) {
            return (byte) this.bigInt.intValue();
        } else {
            throw NotAnIntegerException.INSTANCE;
        }
    }

    public String toString() {
        return BaseConverter.toString(this.bigInt);
    }

    public String toString(int i) {
        if (i == 10) {
            return toString();
        }
        return this.bigInt.toString(i);
    }

    public double doubleValue() {
        return this.bigInt.doubleValue();
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) {
        return new FrinkFloat(this.bigInt);
    }

    public final boolean isBigInteger() {
        return true;
    }

    public final boolean isInt() {
        return false;
    }

    public int realSignum() {
        return this.bigInt.signum();
    }

    public FrinkReal negate() {
        return FrinkInteger.construct(this.bigInt.negate());
    }

    public int getIntegerValue() throws NotAnIntegerException {
        if (this.bigInt.compareTo(MAXINT) <= 0 && this.bigInt.compareTo(MININT) >= 0) {
            return this.bigInt.intValue();
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public int hashCode() {
        return this.bigInt.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof FrinkBigInteger) && ((FrinkBigInteger) obj).bigInt.equals(this.bigInt);
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }
}
