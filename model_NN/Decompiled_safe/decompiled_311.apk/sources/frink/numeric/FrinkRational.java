package frink.numeric;

import frink.errors.NotAnIntegerException;
import frink.expr.OperatorExpression;
import java.math.BigInteger;

public final class FrinkRational implements FrinkReal {
    private static final int MAX_IEEE_DIGITS = 15;
    public static final FrinkRational ONE_HALF = new FrinkRational(FrinkInt.ONE, FrinkInt.TWO);
    private static MathContext mc = new MathContext(20, 0);
    public static boolean rationalAsFloat = false;
    public static boolean showApprox = true;
    private FrinkInteger denominator;
    private FrinkInteger numerator;

    public static FrinkReal construct(int i, int i2) throws InvalidDenominatorException {
        return construct(i, i2, null, null);
    }

    public static FrinkReal construct(int i, int i2, FrinkInteger frinkInteger, FrinkInteger frinkInteger2) throws InvalidDenominatorException {
        int i3;
        int i4;
        if (i == i2) {
            return FrinkInt.ONE;
        }
        if (i2 == 0) {
            throw new InvalidDenominatorException();
        }
        boolean z = false;
        if (i2 < 0) {
            i4 = -i;
            i3 = -i2;
            z = true;
        } else {
            i3 = i2;
            i4 = i;
        }
        int gcd = FrinkInteger.gcd(i4, i3);
        if (gcd == i3) {
            return FrinkInt.construct(i4 / gcd);
        }
        if (gcd != 1) {
            return construct(FrinkInt.construct(i4 / gcd), FrinkInt.construct(i3 / gcd));
        }
        if (z) {
            return new FrinkRational(FrinkInt.construct(i4), FrinkInt.construct(i3));
        }
        if (frinkInteger == null || frinkInteger2 == null) {
            return new FrinkRational(FrinkInt.construct(i4), FrinkInt.construct(i3));
        }
        return new FrinkRational(frinkInteger, frinkInteger2);
    }

    public static FrinkReal construct(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) throws InvalidDenominatorException {
        BigInteger bigInteger;
        BigInteger bigInteger2;
        try {
            return construct(frinkInteger.getInt(), frinkInteger2.getInt(), frinkInteger, frinkInteger2);
        } catch (NotAnIntegerException e) {
            BigInteger bigInt = frinkInteger.getBigInt();
            BigInteger bigInt2 = frinkInteger2.getBigInt();
            if (bigInt2.signum() == -1) {
                BigInteger abs = bigInt2.abs();
                bigInteger = bigInt.negate();
                bigInteger2 = abs;
            } else {
                BigInteger bigInteger3 = bigInt2;
                bigInteger = bigInt;
                bigInteger2 = bigInteger3;
            }
            BigInteger gcd = bigInteger.gcd(bigInteger2);
            if (gcd.equals(bigInteger2)) {
                return FrinkInteger.construct(bigInteger.divide(gcd));
            }
            if (!gcd.equals(FrinkBigInteger.ONE)) {
                return new FrinkRational(FrinkInteger.construct(bigInteger.divide(gcd)), FrinkInteger.construct(bigInteger2.divide(gcd)));
            }
            return new FrinkRational(FrinkInteger.construct(bigInteger), FrinkInteger.construct(bigInteger2));
        }
    }

    private FrinkRational(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        this.numerator = frinkInteger;
        this.denominator = frinkInteger2;
    }

    public FrinkInteger getNumerator() {
        return this.numerator;
    }

    public FrinkInteger getDenominator() {
        return this.denominator;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String toString() {
        String str;
        if (!showApprox && !rationalAsFloat) {
            return this.numerator.toString() + "/" + this.denominator.toString();
        }
        double doubleValue = doubleValue();
        if (Double.isInfinite(doubleValue) || Double.isNaN(doubleValue) || doubleValue == 0.0d) {
            str = FrinkFloat.toString(FrinkBigDecimal.construct(this.numerator).divide(FrinkBigDecimal.construct(this.denominator), mc));
        } else {
            str = Double.toString(doubleValue);
        }
        String replace = str.replace('E', 'e');
        if (rationalAsFloat) {
            return replace;
        }
        String str2 = "approx.";
        int maxFactorsOfTwoAndFive = getMaxFactorsOfTwoAndFive(this.denominator);
        if (maxFactorsOfTwoAndFive > 0 && maxFactorsOfTwoAndFive <= getDigitsAfterDecimal(replace)) {
            str2 = "exactly";
        }
        return this.numerator.toString() + "/" + this.denominator.toString() + " (" + str2 + " " + replace + ")";
    }

    private static int getDigitsAfterDecimal(String str) {
        int indexOf = str.indexOf(".");
        if (indexOf == -1) {
            return 0;
        }
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < indexOf; i2++) {
            char charAt = str.charAt(i2);
            if (charAt >= '0' && charAt <= '9') {
                i++;
            }
        }
        int i3 = indexOf + 1;
        int i4 = 0;
        while (i3 < length) {
            char charAt2 = str.charAt(i3);
            if (charAt2 >= '0' && charAt2 <= '9') {
                i4++;
                i3++;
            } else if (charAt2 != 'e' && charAt2 != 'E') {
                return 0;
            } else {
                try {
                    int i5 = -Integer.parseInt(str.substring(i3 + 1));
                    if (i4 + i <= 15) {
                        return i5 + i4;
                    }
                    return 0;
                } catch (NumberFormatException e) {
                    return 0;
                }
            }
        }
        if (i4 + i <= 15) {
            return i4;
        }
        return 0;
    }

    private static int getMaxFactorsOfTwoAndFive(FrinkInteger frinkInteger) {
        BigInteger bigInt = frinkInteger.getBigInt();
        if (bigInt.signum() == -1) {
            bigInt = bigInt.negate();
        }
        int lowestSetBit = bigInt.getLowestSetBit();
        if (lowestSetBit > 0) {
            bigInt = bigInt.shiftRight(lowestSetBit);
        }
        BigInteger bigInteger = bigInt;
        int i = 0;
        while (bigInteger.mod(FrinkBigInteger.FIVE).compareTo(FrinkBigInteger.ZERO) == 0) {
            i++;
            bigInteger = bigInteger.divide(FrinkBigInteger.FIVE);
        }
        if (bigInteger.compareTo(FrinkBigInteger.ONE) != 0) {
            return 0;
        }
        if (lowestSetBit > i) {
            return lowestSetBit;
        }
        return i;
    }

    public FrinkReal reciprocal() throws InvalidDenominatorException {
        return construct(this.denominator, this.numerator);
    }

    public double doubleValue() {
        return this.numerator.doubleValue() / this.denominator.doubleValue();
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) {
        return new FrinkFloat(RealMath.div(this.numerator.getFrinkFloatValue(mathContext).getBigDec(), this.denominator.getFrinkFloatValue(mathContext).getBigDec(), mathContext));
    }

    public final boolean isFrinkInteger() {
        return false;
    }

    public final boolean isBigInteger() {
        return false;
    }

    public final boolean isInt() {
        return false;
    }

    public final boolean isRational() {
        return true;
    }

    public final boolean isFloat() {
        return false;
    }

    public boolean isReal() {
        return true;
    }

    public boolean isComplex() {
        return false;
    }

    public boolean isInterval() {
        return false;
    }

    public int realSignum() {
        return this.numerator.realSignum();
    }

    public FrinkReal negate() {
        return new FrinkRational((FrinkInteger) this.numerator.negate(), this.denominator);
    }

    public int hashCode() {
        return (this.numerator.hashCode() + this.denominator.hashCode()) | OperatorExpression.PREC_LOWEST;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof FrinkRational) {
            FrinkRational frinkRational = (FrinkRational) obj;
            if (frinkRational.numerator.equals(this.numerator) && frinkRational.denominator.equals(this.denominator)) {
                return true;
            }
        }
        return false;
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }
}
