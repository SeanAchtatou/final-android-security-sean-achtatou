package frink.numeric;

public interface FrinkReal extends Numeric {
    double doubleValue();

    FrinkFloat getFrinkFloatValue(MathContext mathContext);

    FrinkReal negate();

    int realSignum();
}
