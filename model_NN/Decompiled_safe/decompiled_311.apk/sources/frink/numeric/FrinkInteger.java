package frink.numeric;

import frink.errors.NotAnIntegerException;
import java.math.BigInteger;

public abstract class FrinkInteger implements FrinkReal {
    public abstract BigInteger getBigInt();

    public abstract byte getByte() throws NotAnIntegerException;

    public abstract FrinkFloat getFrinkFloatValue(MathContext mathContext);

    public abstract int getInt() throws NotAnIntegerException;

    public abstract long getLong() throws NotAnIntegerException;

    public abstract short getShort() throws NotAnIntegerException;

    public abstract String toString(int i);

    public static FrinkInteger construct(String str, int i) {
        if (str.length() > 6) {
            return construct(new BigInteger(str, i));
        }
        return FrinkInt.construct(Integer.parseInt(str, i));
    }

    public static FrinkInteger construct(BigInteger bigInteger) {
        if (bigInteger.compareTo(FrinkBigInteger.MAXINT) > 0) {
            return new FrinkBigInteger(bigInteger);
        }
        if (bigInteger.compareTo(FrinkBigInteger.MININT) < 0) {
            return new FrinkBigInteger(bigInteger);
        }
        return FrinkInt.construct(bigInteger.intValue());
    }

    public static FrinkInteger construct(long j) {
        int i = (int) j;
        if (((long) i) != j) {
            return new FrinkBigInteger(j);
        }
        return FrinkInt.construct(i);
    }

    public static FrinkInteger construct(int i) {
        return FrinkInt.construct(i);
    }

    public boolean isFrinkInteger() {
        return true;
    }

    public boolean isRational() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isReal() {
        return true;
    }

    public boolean isComplex() {
        return false;
    }

    public boolean isInterval() {
        return false;
    }

    public static FrinkInteger gcd(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            return construct(gcd(frinkInteger.getInt(), frinkInteger2.getInt()));
        } catch (NotAnIntegerException e) {
            return construct(frinkInteger.getBigInt().gcd(frinkInteger2.getBigInt()));
        }
    }

    public static int gcd(int i, int i2) {
        int i3;
        int i4;
        if (i == 0) {
            return i2;
        }
        if (i2 == 0) {
            return i;
        }
        if (i < 0) {
            i3 = -i;
        } else {
            i3 = i;
        }
        if (i2 < 0) {
            i4 = -i2;
        } else {
            i4 = i2;
        }
        int i5 = i3 % i4;
        while (i5 > 0) {
            int i6 = i4 % i5;
            i4 = i5;
            i5 = i6;
        }
        return i4;
    }

    public static FrinkReal lcm(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        FrinkInteger multiplyInts = NumericMath.multiplyInts(NumericMath.divInts(frinkInteger, gcd(frinkInteger, frinkInteger2)), frinkInteger2);
        if (multiplyInts.realSignum() < 0) {
            return multiplyInts.negate();
        }
        return multiplyInts;
    }

    public static int compare(FrinkInteger frinkInteger, FrinkInteger frinkInteger2) {
        try {
            int i = frinkInteger.getInt();
            int i2 = frinkInteger2.getInt();
            if (i < i2) {
                return -1;
            }
            if (i == i2) {
                return 0;
            }
            return 1;
        } catch (NotAnIntegerException e) {
            return frinkInteger.getBigInt().compareTo(frinkInteger2.getBigInt());
        }
    }
}
