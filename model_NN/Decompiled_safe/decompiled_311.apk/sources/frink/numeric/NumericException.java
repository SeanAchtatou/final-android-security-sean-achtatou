package frink.numeric;

import frink.errors.FrinkException;

public abstract class NumericException extends FrinkException {
    public NumericException(String str) {
        super(str);
    }
}
