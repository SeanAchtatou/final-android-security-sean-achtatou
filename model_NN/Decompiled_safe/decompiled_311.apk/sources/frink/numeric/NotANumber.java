package frink.numeric;

import frink.errors.NotRealException;

public class NotANumber implements Numeric {
    public static final NotANumber NaN = new NotANumber();

    private NotANumber() {
    }

    public String toString() {
        return "NaN";
    }

    public double doubleValue() {
        return Double.NaN;
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) throws NotRealException {
        throw new NotRealException("NaN cannot be converted to FrinkFloat.");
    }

    public boolean isFrinkInteger() {
        return false;
    }

    public boolean isBigInteger() {
        return false;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isRational() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isReal() {
        return false;
    }

    public boolean isComplex() {
        return false;
    }

    public boolean isInterval() {
        return false;
    }

    public int hashCode() {
        return -1612613422;
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }
}
