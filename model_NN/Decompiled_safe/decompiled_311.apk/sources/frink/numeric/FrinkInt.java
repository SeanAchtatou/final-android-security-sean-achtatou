package frink.numeric;

import frink.errors.NotAnIntegerException;
import java.math.BigInteger;

public class FrinkInt extends FrinkInteger {
    public static final FrinkInt FIVE = cache[105];
    public static final FrinkInt FOUR = cache[104];
    public static final int MAX_CACHE = 200;
    public static final int MIN_CACHE = -100;
    public static final FrinkInt NEGATIVE_ONE = cache[99];
    public static final FrinkInt ONE = cache[101];
    public static final FrinkInt TEN = cache[110];
    public static final FrinkInt THREE = cache[103];
    public static final FrinkInt TWO = cache[102];
    public static final FrinkInt ZERO = cache[100];
    private static final FrinkInt[] cache = new FrinkInt[301];
    private int value;

    static {
        for (int i = -100; i <= 200; i++) {
            cache[i - -100] = new FrinkInt(i);
        }
    }

    public static FrinkInt construct(int i) {
        if (i > 200 || i < -100) {
            return new FrinkInt(i);
        }
        return cache[i - -100];
    }

    private FrinkInt(int i) {
        this.value = i;
    }

    public double doubleValue() {
        return (double) this.value;
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) {
        return new FrinkFloat(this.value);
    }

    public String toString() {
        return Integer.toString(this.value);
    }

    public String toString(int i) {
        return Integer.toString(this.value, i);
    }

    public boolean isBigInteger() {
        return false;
    }

    public boolean isInt() {
        return true;
    }

    public BigInteger getBigInt() {
        return FrinkBigInteger.makeBigInt(this.value);
    }

    public long getLong() {
        return (long) this.value;
    }

    public int getInt() {
        return this.value;
    }

    public short getShort() throws NotAnIntegerException {
        if (this.value >= -32768 && this.value <= 32767) {
            return (short) this.value;
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public byte getByte() throws NotAnIntegerException {
        if (this.value >= -128 && this.value <= 127) {
            return (byte) this.value;
        }
        throw NotAnIntegerException.INSTANCE;
    }

    public int realSignum() {
        if (this.value < 0) {
            return -1;
        }
        if (this.value > 0) {
            return 1;
        }
        return 0;
    }

    public FrinkReal negate() {
        if (this.value == Integer.MIN_VALUE) {
            return FrinkInteger.construct(-((long) this.value));
        }
        return FrinkInteger.construct(-this.value);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FrinkInt) || ((FrinkInt) obj).value != this.value) {
            return false;
        }
        return true;
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }

    public int hashCode() {
        return this.value;
    }
}
