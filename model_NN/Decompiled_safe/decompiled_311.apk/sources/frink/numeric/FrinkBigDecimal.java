package frink.numeric;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class FrinkBigDecimal extends Number implements Serializable {
    private static final String $0 = "FrinkBigDecimal.nrx";
    private static final int MaxArg = 999999999;
    private static final int MaxExp = 999999999;
    private static final int MinArg = -999999999;
    private static final int MinExp = -999999999;
    public static final FrinkBigDecimal ONE = new FrinkBigDecimal(1L);
    public static final int ROUND_CEILING = 2;
    public static final int ROUND_DOWN = 1;
    public static final int ROUND_FLOOR = 3;
    public static final int ROUND_HALF_DOWN = 5;
    public static final int ROUND_HALF_EVEN = 6;
    public static final int ROUND_HALF_UP = 4;
    public static final int ROUND_UNNECESSARY = 7;
    public static final int ROUND_UP = 0;
    public static final FrinkBigDecimal TEN = new FrinkBigDecimal(10);
    public static final FrinkBigDecimal ZERO = new FrinkBigDecimal(0L);
    private static byte[] bytecar = new byte[190];
    private static byte[] bytedig = diginit();
    private static final String copyright = " Copyright (c) IBM Corporation 1996, 2000.  All rights reserved. ";
    private static final byte isneg = -1;
    private static final byte ispos = 1;
    private static final byte iszero = 0;
    private static final MathContext plainMC = new MathContext(0, 0);
    private static final long serialVersionUID = 8245355804974198832L;
    private int exp;
    private byte form;
    private byte ind;
    private byte[] mant;

    public FrinkBigDecimal(BigDecimal bigDecimal) {
        this(bigDecimal.toString());
    }

    public FrinkBigDecimal(BigInteger bigInteger) {
        this(bigInteger.toString(10));
    }

    public static FrinkBigDecimal construct(FrinkInteger frinkInteger) {
        if (frinkInteger.isInt()) {
            return new FrinkBigDecimal(((FrinkInt) frinkInteger).getInt());
        }
        return new FrinkBigDecimal(frinkInteger.getBigInt());
    }

    public FrinkBigDecimal(BigInteger bigInteger, int i) {
        this(bigInteger.toString(10));
        if (i < 0) {
            throw new NumberFormatException("Negative scale: " + i);
        }
        this.exp = -i;
    }

    public FrinkBigDecimal(char[] cArr) {
        this(cArr, 0, cArr.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007b, code lost:
        if ((r4 - r1) <= (r0 - 2)) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007d, code lost:
        bad(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0087, code lost:
        if (r13[r4 + 1] != '-') goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0089, code lost:
        r3 = true;
        r2 = r4 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008f, code lost:
        r0 = r0 - (r2 - r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0092, code lost:
        if (r0 != 0) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0094, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0097, code lost:
        if (r0 <= 9) goto L_0x00e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0099, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x009b, code lost:
        if ((r4 | r9) == false) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009d, code lost:
        bad(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a0, code lost:
        if (r0 <= 0) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00a2, code lost:
        r4 = r13[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00a6, code lost:
        if (r4 >= '0') goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a8, code lost:
        bad(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ad, code lost:
        if (r4 <= '9') goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b3, code lost:
        if (java.lang.Character.isDigit(r4) != false) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b5, code lost:
        bad(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b8, code lost:
        r4 = java.lang.Character.digit(r4, 10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00be, code lost:
        if (r4 >= 0) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c0, code lost:
        bad(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c3, code lost:
        r12.exp = r4 + (r12.exp * 10);
        r0 = r0 - 1;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00d5, code lost:
        if (r13[r4 + 1] != '+') goto L_0x00dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00d7, code lost:
        r3 = false;
        r2 = r4 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00dd, code lost:
        r3 = false;
        r2 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00e3, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00e5, code lost:
        r9 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00e7, code lost:
        r4 = r4 - '0';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00eb, code lost:
        if (r3 == false) goto L_0x00f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00ed, code lost:
        r12.exp = -r12.exp;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00f2, code lost:
        r0 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FrinkBigDecimal(char[] r13, int r14, int r15) {
        /*
            r12 = this;
            r12.<init>()
            r0 = 0
            r12.form = r0
            if (r15 > 0) goto L_0x000b
            r12.bad(r13)
        L_0x000b:
            r0 = 1
            r12.ind = r0
            r0 = 0
            char r0 = r13[r0]
            r1 = 45
            if (r0 != r1) goto L_0x0042
            int r0 = r15 + -1
            if (r0 != 0) goto L_0x001c
            r12.bad(r13)
        L_0x001c:
            r1 = -1
            r12.ind = r1
            int r1 = r14 + 1
        L_0x0021:
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = -1
            r6 = -1
            r7 = r4
            r8 = r2
            r2 = r0
            r4 = r1
            r11 = r6
            r6 = r5
            r5 = r11
        L_0x002d:
            if (r2 <= 0) goto L_0x01c7
            char r9 = r13[r4]
            r10 = 48
            if (r9 < r10) goto L_0x0053
            r10 = 57
            if (r9 > r10) goto L_0x0053
            int r5 = r7 + 1
            r7 = r5
            r5 = r4
        L_0x003d:
            int r2 = r2 + -1
            int r4 = r4 + 1
            goto L_0x002d
        L_0x0042:
            r0 = 0
            char r0 = r13[r0]
            r1 = 43
            if (r0 != r1) goto L_0x01ca
            int r0 = r15 + -1
            if (r0 != 0) goto L_0x0050
            r12.bad(r13)
        L_0x0050:
            int r1 = r14 + 1
            goto L_0x0021
        L_0x0053:
            r10 = 46
            if (r9 != r10) goto L_0x005f
            if (r6 < 0) goto L_0x005c
            r12.bad(r13)
        L_0x005c:
            int r6 = r4 - r1
            goto L_0x003d
        L_0x005f:
            r10 = 101(0x65, float:1.42E-43)
            if (r9 == r10) goto L_0x0076
            r10 = 69
            if (r9 == r10) goto L_0x0076
            boolean r5 = java.lang.Character.isDigit(r9)
            if (r5 != 0) goto L_0x0070
            r12.bad(r13)
        L_0x0070:
            r5 = 1
            int r7 = r7 + 1
            r8 = r5
            r5 = r4
            goto L_0x003d
        L_0x0076:
            int r2 = r4 - r1
            r3 = 2
            int r3 = r0 - r3
            if (r2 <= r3) goto L_0x0080
            r12.bad(r13)
        L_0x0080:
            r2 = 0
            int r3 = r4 + 1
            char r3 = r13[r3]
            r9 = 45
            if (r3 != r9) goto L_0x00cf
            r2 = 1
            int r3 = r4 + 2
            r11 = r3
            r3 = r2
            r2 = r11
        L_0x008f:
            int r4 = r2 - r1
            int r0 = r0 - r4
            if (r0 != 0) goto L_0x00e3
            r4 = 1
        L_0x0095:
            r9 = 9
            if (r0 <= r9) goto L_0x00e5
            r9 = 1
        L_0x009a:
            r4 = r4 | r9
            if (r4 == 0) goto L_0x00a0
            r12.bad(r13)
        L_0x00a0:
            if (r0 <= 0) goto L_0x00eb
            char r4 = r13[r2]
            r9 = 48
            if (r4 >= r9) goto L_0x00ab
            r12.bad(r13)
        L_0x00ab:
            r9 = 57
            if (r4 <= r9) goto L_0x00e7
            boolean r9 = java.lang.Character.isDigit(r4)
            if (r9 != 0) goto L_0x00b8
            r12.bad(r13)
        L_0x00b8:
            r9 = 10
            int r4 = java.lang.Character.digit(r4, r9)
            if (r4 >= 0) goto L_0x00c3
            r12.bad(r13)
        L_0x00c3:
            int r9 = r12.exp
            int r9 = r9 * 10
            int r4 = r4 + r9
            r12.exp = r4
            int r0 = r0 + -1
            int r2 = r2 + 1
            goto L_0x00a0
        L_0x00cf:
            int r3 = r4 + 1
            char r3 = r13[r3]
            r9 = 43
            if (r3 != r9) goto L_0x00dd
            int r3 = r4 + 2
            r11 = r3
            r3 = r2
            r2 = r11
            goto L_0x008f
        L_0x00dd:
            int r3 = r4 + 1
            r11 = r3
            r3 = r2
            r2 = r11
            goto L_0x008f
        L_0x00e3:
            r4 = 0
            goto L_0x0095
        L_0x00e5:
            r9 = 0
            goto L_0x009a
        L_0x00e7:
            r9 = 48
            int r4 = r4 - r9
            goto L_0x00c3
        L_0x00eb:
            if (r3 == 0) goto L_0x00f2
            int r0 = r12.exp
            int r0 = -r0
            r12.exp = r0
        L_0x00f2:
            r0 = 1
        L_0x00f3:
            if (r7 != 0) goto L_0x00f8
            r12.bad(r13)
        L_0x00f8:
            if (r6 < 0) goto L_0x0100
            int r2 = r12.exp
            int r2 = r2 + r6
            int r2 = r2 - r7
            r12.exp = r2
        L_0x0100:
            r2 = 1
            int r2 = r5 - r2
            r3 = r6
            r4 = r7
            r5 = r1
        L_0x0106:
            if (r1 > r2) goto L_0x0124
            char r6 = r13[r1]
            r7 = 48
            if (r6 != r7) goto L_0x0117
            int r5 = r5 + 1
            int r3 = r3 + -1
            int r4 = r4 + -1
        L_0x0114:
            int r1 = r1 + 1
            goto L_0x0106
        L_0x0117:
            r7 = 46
            if (r6 != r7) goto L_0x0120
            int r5 = r5 + 1
            int r3 = r3 + -1
            goto L_0x0114
        L_0x0120:
            r7 = 57
            if (r6 > r7) goto L_0x014a
        L_0x0124:
            byte[] r1 = new byte[r4]
            r12.mant = r1
            if (r8 == 0) goto L_0x016a
            r1 = 0
            r2 = r5
            r11 = r4
            r4 = r1
            r1 = r11
        L_0x012f:
            if (r1 <= 0) goto L_0x0186
            if (r4 != r3) goto L_0x0135
            int r2 = r2 + 1
        L_0x0135:
            char r5 = r13[r2]
            r6 = 57
            if (r5 > r6) goto L_0x0159
            byte[] r6 = r12.mant
            r7 = 48
            int r5 = r5 - r7
            byte r5 = (byte) r5
            r6[r4] = r5
        L_0x0143:
            int r2 = r2 + 1
            int r1 = r1 + -1
            int r4 = r4 + 1
            goto L_0x012f
        L_0x014a:
            r7 = 10
            int r6 = java.lang.Character.digit(r6, r7)
            if (r6 != 0) goto L_0x0124
            int r5 = r5 + 1
            int r3 = r3 + -1
            int r4 = r4 + -1
            goto L_0x0114
        L_0x0159:
            r6 = 10
            int r5 = java.lang.Character.digit(r5, r6)
            if (r5 >= 0) goto L_0x0164
            r12.bad(r13)
        L_0x0164:
            byte[] r6 = r12.mant
            byte r5 = (byte) r5
            r6[r4] = r5
            goto L_0x0143
        L_0x016a:
            r1 = 0
            r2 = r5
            r11 = r4
            r4 = r1
            r1 = r11
        L_0x016f:
            if (r1 <= 0) goto L_0x0186
            if (r4 != r3) goto L_0x0175
            int r2 = r2 + 1
        L_0x0175:
            byte[] r5 = r12.mant
            char r6 = r13[r2]
            r7 = 48
            int r6 = r6 - r7
            byte r6 = (byte) r6
            r5[r4] = r6
            int r2 = r2 + 1
            int r1 = r1 + -1
            int r4 = r4 + 1
            goto L_0x016f
        L_0x0186:
            byte[] r1 = r12.mant
            r2 = 0
            byte r1 = r1[r2]
            if (r1 != 0) goto L_0x01a3
            r1 = 0
            r12.ind = r1
            int r1 = r12.exp
            if (r1 <= 0) goto L_0x0197
            r1 = 0
            r12.exp = r1
        L_0x0197:
            if (r0 == 0) goto L_0x01a2
            frink.numeric.FrinkBigDecimal r0 = frink.numeric.FrinkBigDecimal.ZERO
            byte[] r0 = r0.mant
            r12.mant = r0
            r0 = 0
            r12.exp = r0
        L_0x01a2:
            return
        L_0x01a3:
            if (r0 == 0) goto L_0x01a2
            r0 = 1
            r12.form = r0
            int r0 = r12.exp
            byte[] r1 = r12.mant
            int r1 = r1.length
            int r0 = r0 + r1
            r1 = 1
            int r0 = r0 - r1
            r1 = -999999999(0xffffffffc4653601, float:-916.8438)
            if (r0 >= r1) goto L_0x01c3
            r1 = 1
        L_0x01b6:
            r2 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r0 <= r2) goto L_0x01c5
            r0 = 1
        L_0x01bc:
            r0 = r0 | r1
            if (r0 == 0) goto L_0x01a2
            r12.bad(r13)
            goto L_0x01a2
        L_0x01c3:
            r1 = 0
            goto L_0x01b6
        L_0x01c5:
            r0 = 0
            goto L_0x01bc
        L_0x01c7:
            r0 = r3
            goto L_0x00f3
        L_0x01ca:
            r0 = r15
            r1 = r14
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.FrinkBigDecimal.<init>(char[], int, int):void");
    }

    public FrinkBigDecimal(double d) {
        this(Double.toString(d));
    }

    public FrinkBigDecimal(int i) {
        int i2;
        int i3 = 9;
        this.form = iszero;
        if (i > 9 || i < -9) {
            if (i > 0) {
                this.ind = ispos;
                i2 = -i;
            } else {
                this.ind = isneg;
                i2 = i;
            }
            int i4 = i2;
            while (true) {
                i4 /= 10;
                if (i4 == 0) {
                    break;
                }
                i3--;
            }
            this.mant = new byte[(10 - i3)];
            int i5 = (10 - i3) - 1;
            int i6 = i2;
            int i7 = i5;
            while (true) {
                this.mant[i7] = (byte) (-((byte) (i6 % 10)));
                i6 /= 10;
                if (i6 != 0) {
                    i7--;
                } else {
                    return;
                }
            }
        } else if (i == 0) {
            this.mant = ZERO.mant;
            this.ind = iszero;
        } else if (i == 1) {
            this.mant = ONE.mant;
            this.ind = ispos;
        } else if (i == -1) {
            this.mant = ONE.mant;
            this.ind = isneg;
        } else {
            this.mant = new byte[1];
            if (i > 0) {
                this.mant[0] = (byte) i;
                this.ind = ispos;
                return;
            }
            this.mant[0] = (byte) (-i);
            this.ind = isneg;
        }
    }

    public FrinkBigDecimal(long j) {
        long j2;
        this.form = iszero;
        if (j > 0) {
            this.ind = ispos;
            j2 = -j;
        } else if (j == 0) {
            this.ind = iszero;
            j2 = j;
        } else {
            this.ind = isneg;
            j2 = j;
        }
        int i = 18;
        long j3 = j2;
        while (true) {
            j3 /= 10;
            if (j3 == 0) {
                break;
            }
            i--;
        }
        this.mant = new byte[(19 - i)];
        long j4 = j2;
        int i2 = (19 - i) - 1;
        while (true) {
            this.mant[i2] = (byte) (-((byte) ((int) (j4 % 10))));
            j4 /= 10;
            if (j4 != 0) {
                i2--;
            } else {
                return;
            }
        }
    }

    public FrinkBigDecimal(String str) {
        this(str.toCharArray(), 0, str.length());
    }

    private FrinkBigDecimal() {
        this.form = iszero;
    }

    public FrinkBigDecimal abs() {
        return abs(plainMC);
    }

    public FrinkBigDecimal abs(MathContext mathContext) {
        if (this.ind == -1) {
            return negate(mathContext);
        }
        return plus(mathContext);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (r3 <= r10) goto L_0x0156;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public frink.numeric.FrinkBigDecimal add(frink.numeric.FrinkBigDecimal r18, frink.numeric.MathContext r19) {
        /*
            r17 = this;
            r0 = r19
            boolean r0 = r0.lostDigits
            r3 = r0
            if (r3 == 0) goto L_0x0014
            r0 = r19
            int r0 = r0.digits
            r3 = r0
            r0 = r17
            r1 = r18
            r2 = r3
            r0.checkdigits(r1, r2)
        L_0x0014:
            r0 = r17
            byte r0 = r0.ind
            r3 = r0
            if (r3 != 0) goto L_0x0027
            r0 = r19
            int r0 = r0.form
            r3 = r0
            if (r3 == 0) goto L_0x0027
            frink.numeric.FrinkBigDecimal r3 = r18.plus(r19)
        L_0x0026:
            return r3
        L_0x0027:
            r0 = r18
            byte r0 = r0.ind
            r3 = r0
            if (r3 != 0) goto L_0x003e
            r0 = r19
            int r0 = r0.form
            r3 = r0
            if (r3 == 0) goto L_0x003e
            r0 = r17
            r1 = r19
            frink.numeric.FrinkBigDecimal r3 = r0.plus(r1)
            goto L_0x0026
        L_0x003e:
            r0 = r19
            int r0 = r0.digits
            r3 = r0
            if (r3 <= 0) goto L_0x01e8
            r0 = r17
            byte[] r0 = r0.mant
            r4 = r0
            int r4 = r4.length
            if (r4 <= r3) goto L_0x01e4
            frink.numeric.FrinkBigDecimal r4 = clone(r17)
            r0 = r4
            r1 = r19
            frink.numeric.FrinkBigDecimal r4 = r0.round(r1)
        L_0x0058:
            r0 = r18
            byte[] r0 = r0.mant
            r5 = r0
            int r5 = r5.length
            if (r5 <= r3) goto L_0x01e0
            frink.numeric.FrinkBigDecimal r5 = clone(r18)
            r0 = r5
            r1 = r19
            frink.numeric.FrinkBigDecimal r5 = r0.round(r1)
        L_0x006b:
            frink.numeric.FrinkBigDecimal r9 = new frink.numeric.FrinkBigDecimal
            r9.<init>()
            byte[] r6 = r4.mant
            byte[] r7 = r4.mant
            int r7 = r7.length
            byte[] r8 = r5.mant
            byte[] r10 = r5.mant
            int r10 = r10.length
            int r11 = r4.exp
            int r12 = r5.exp
            if (r11 != r12) goto L_0x00b6
            int r3 = r4.exp
            r9.exp = r3
            r3 = r10
        L_0x0085:
            byte r10 = r4.ind
            if (r10 != 0) goto L_0x0159
            r10 = 1
            r9.ind = r10
        L_0x008c:
            byte r10 = r4.ind
            r11 = -1
            if (r10 != r11) goto L_0x015f
            r10 = 1
        L_0x0092:
            byte r11 = r5.ind
            r12 = -1
            if (r11 != r12) goto L_0x0162
            r11 = 1
        L_0x0098:
            if (r10 != r11) goto L_0x0165
            r4 = 1
            r5 = r8
            r15 = r3
            r3 = r6
            r6 = r15
            r16 = r7
            r7 = r4
            r4 = r16
        L_0x00a4:
            r8 = 0
            byte[] r3 = byteaddsub(r3, r4, r5, r6, r7, r8)
            r9.mant = r3
            r3 = 0
            r0 = r9
            r1 = r19
            r2 = r3
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0026
        L_0x00b6:
            int r11 = r4.exp
            int r12 = r5.exp
            if (r11 <= r12) goto L_0x010b
            int r11 = r4.exp
            int r11 = r11 + r7
            int r12 = r5.exp
            int r11 = r11 - r12
            int r12 = r10 + r3
            int r12 = r12 + 1
            if (r11 < r12) goto L_0x00f0
            if (r3 <= 0) goto L_0x00f0
            r9.mant = r6
            int r5 = r4.exp
            r9.exp = r5
            byte r5 = r4.ind
            r9.ind = r5
            if (r7 >= r3) goto L_0x00e5
            byte[] r4 = r4.mant
            byte[] r4 = extend(r4, r3)
            r9.mant = r4
            int r4 = r9.exp
            int r3 = r3 - r7
            int r3 = r4 - r3
            r9.exp = r3
        L_0x00e5:
            r3 = 0
            r0 = r9
            r1 = r19
            r2 = r3
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0026
        L_0x00f0:
            int r12 = r5.exp
            r9.exp = r12
            int r12 = r3 + 1
            if (r11 <= r12) goto L_0x01dd
            if (r3 <= 0) goto L_0x01dd
            int r11 = r11 - r3
            r12 = 1
            int r11 = r11 - r12
            int r10 = r10 - r11
            int r12 = r9.exp
            int r11 = r11 + r12
            r9.exp = r11
            int r3 = r3 + 1
        L_0x0105:
            if (r3 <= r7) goto L_0x0156
            r7 = r3
            r3 = r10
            goto L_0x0085
        L_0x010b:
            int r11 = r5.exp
            int r11 = r11 + r10
            int r12 = r4.exp
            int r11 = r11 - r12
            int r12 = r7 + r3
            int r12 = r12 + 1
            if (r11 < r12) goto L_0x013f
            if (r3 <= 0) goto L_0x013f
            r9.mant = r8
            int r4 = r5.exp
            r9.exp = r4
            byte r4 = r5.ind
            r9.ind = r4
            if (r10 >= r3) goto L_0x0134
            byte[] r4 = r5.mant
            byte[] r4 = extend(r4, r3)
            r9.mant = r4
            int r4 = r9.exp
            int r3 = r3 - r10
            int r3 = r4 - r3
            r9.exp = r3
        L_0x0134:
            r3 = 0
            r0 = r9
            r1 = r19
            r2 = r3
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0026
        L_0x013f:
            int r12 = r4.exp
            r9.exp = r12
            int r12 = r3 + 1
            if (r11 <= r12) goto L_0x01da
            if (r3 <= 0) goto L_0x01da
            int r11 = r11 - r3
            r12 = 1
            int r11 = r11 - r12
            int r7 = r7 - r11
            int r12 = r9.exp
            int r11 = r11 + r12
            r9.exp = r11
            int r3 = r3 + 1
        L_0x0154:
            if (r3 > r10) goto L_0x0085
        L_0x0156:
            r3 = r10
            goto L_0x0085
        L_0x0159:
            byte r10 = r4.ind
            r9.ind = r10
            goto L_0x008c
        L_0x015f:
            r10 = 0
            goto L_0x0092
        L_0x0162:
            r11 = 0
            goto L_0x0098
        L_0x0165:
            r10 = -1
            byte r5 = r5.ind
            if (r5 != 0) goto L_0x0172
            r5 = r8
            r4 = r7
            r7 = r10
            r15 = r3
            r3 = r6
            r6 = r15
            goto L_0x00a4
        L_0x0172:
            if (r7 >= r3) goto L_0x018a
            r5 = 1
        L_0x0175:
            byte r4 = r4.ind
            if (r4 != 0) goto L_0x018c
            r4 = 1
        L_0x017a:
            r4 = r4 | r5
            if (r4 == 0) goto L_0x018e
            byte r4 = r9.ind
            int r4 = -r4
            byte r4 = (byte) r4
            r9.ind = r4
            r5 = r6
            r4 = r3
            r6 = r7
            r3 = r8
            r7 = r10
            goto L_0x00a4
        L_0x018a:
            r5 = 0
            goto L_0x0175
        L_0x018c:
            r4 = 0
            goto L_0x017a
        L_0x018e:
            if (r7 <= r3) goto L_0x0198
            r5 = r8
            r4 = r7
            r7 = r10
            r15 = r3
            r3 = r6
            r6 = r15
            goto L_0x00a4
        L_0x0198:
            r4 = 0
            r5 = 0
            int r11 = r6.length
            r12 = 1
            int r11 = r11 - r12
            int r12 = r8.length
            r13 = 1
            int r12 = r12 - r13
            r15 = r5
            r5 = r4
            r4 = r15
        L_0x01a3:
            if (r5 > r11) goto L_0x01bc
            byte r13 = r6[r5]
        L_0x01a7:
            if (r4 > r12) goto L_0x01cb
            byte r14 = r8[r4]
        L_0x01ab:
            if (r13 == r14) goto L_0x01cd
            if (r13 >= r14) goto L_0x01d2
            byte r4 = r9.ind
            int r4 = -r4
            byte r4 = (byte) r4
            r9.ind = r4
            r5 = r6
            r4 = r3
            r6 = r7
            r3 = r8
            r7 = r10
            goto L_0x00a4
        L_0x01bc:
            if (r4 <= r12) goto L_0x01c9
            r0 = r19
            int r0 = r0.form
            r4 = r0
            if (r4 == 0) goto L_0x01d2
            frink.numeric.FrinkBigDecimal r3 = frink.numeric.FrinkBigDecimal.ZERO
            goto L_0x0026
        L_0x01c9:
            r13 = 0
            goto L_0x01a7
        L_0x01cb:
            r14 = 0
            goto L_0x01ab
        L_0x01cd:
            int r5 = r5 + 1
            int r4 = r4 + 1
            goto L_0x01a3
        L_0x01d2:
            r5 = r8
            r4 = r7
            r7 = r10
            r15 = r3
            r3 = r6
            r6 = r15
            goto L_0x00a4
        L_0x01da:
            r3 = r11
            goto L_0x0154
        L_0x01dd:
            r3 = r11
            goto L_0x0105
        L_0x01e0:
            r5 = r18
            goto L_0x006b
        L_0x01e4:
            r4 = r17
            goto L_0x0058
        L_0x01e8:
            r4 = r17
            r5 = r18
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.FrinkBigDecimal.add(frink.numeric.FrinkBigDecimal, frink.numeric.MathContext):frink.numeric.FrinkBigDecimal");
    }

    public int compareTo(FrinkBigDecimal frinkBigDecimal) {
        return compareTo(frinkBigDecimal, plainMC);
    }

    public int compareTo(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        boolean z;
        if (mathContext.lostDigits) {
            checkdigits(frinkBigDecimal, mathContext.digits);
        }
        if ((this.ind == frinkBigDecimal.ind) && (this.exp == frinkBigDecimal.exp)) {
            int length = this.mant.length;
            if (length < frinkBigDecimal.mant.length) {
                return (byte) (-this.ind);
            }
            if (length > frinkBigDecimal.mant.length) {
                return this.ind;
            }
            if (length <= mathContext.digits) {
                z = true;
            } else {
                z = false;
            }
            if (z || (mathContext.digits == 0)) {
                int i = 0;
                while (length > 0) {
                    if (this.mant[i] < frinkBigDecimal.mant[i]) {
                        return (byte) (-this.ind);
                    }
                    if (this.mant[i] > frinkBigDecimal.mant[i]) {
                        return this.ind;
                    }
                    length--;
                    i++;
                }
                return 0;
            }
        } else if (this.ind < frinkBigDecimal.ind) {
            return -1;
        } else {
            if (this.ind > frinkBigDecimal.ind) {
                return 1;
            }
        }
        FrinkBigDecimal clone = clone(frinkBigDecimal);
        clone.ind = (byte) (-clone.ind);
        return add(clone, mathContext).ind;
    }

    public FrinkBigDecimal divide(FrinkBigDecimal frinkBigDecimal, int i) {
        return dodivide('D', frinkBigDecimal, new MathContext(0, 0, false, i), -1);
    }

    public FrinkBigDecimal divide(FrinkBigDecimal frinkBigDecimal, int i, int i2) {
        if (i >= 0) {
            return dodivide('D', frinkBigDecimal, new MathContext(0, 0, false, i2), i);
        }
        throw new ArithmeticException("Negative scale: " + i);
    }

    public FrinkBigDecimal divide(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        return dodivide('D', frinkBigDecimal, mathContext, -1);
    }

    public FrinkBigDecimal divideInteger(FrinkBigDecimal frinkBigDecimal) {
        return dodivide('I', frinkBigDecimal, plainMC, 0);
    }

    public FrinkBigDecimal divideInteger(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        return dodivide('I', frinkBigDecimal, mathContext, 0);
    }

    public FrinkBigDecimal max(FrinkBigDecimal frinkBigDecimal) {
        return max(frinkBigDecimal, plainMC);
    }

    public FrinkBigDecimal max(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        if (compareTo(frinkBigDecimal, mathContext) >= 0) {
            return plus(mathContext);
        }
        return frinkBigDecimal.plus(mathContext);
    }

    public FrinkBigDecimal min(FrinkBigDecimal frinkBigDecimal) {
        return min(frinkBigDecimal, plainMC);
    }

    public FrinkBigDecimal min(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        if (compareTo(frinkBigDecimal, mathContext) <= 0) {
            return plus(mathContext);
        }
        return frinkBigDecimal.plus(mathContext);
    }

    public FrinkBigDecimal multiply(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        int i;
        FrinkBigDecimal frinkBigDecimal2;
        FrinkBigDecimal frinkBigDecimal3;
        byte[] bArr;
        byte[] bArr2;
        int i2;
        FrinkBigDecimal frinkBigDecimal4;
        if (mathContext.lostDigits) {
            checkdigits(frinkBigDecimal, mathContext.digits);
        }
        int i3 = 0;
        int i4 = mathContext.digits;
        if (i4 > 0) {
            if (this.mant.length > i4) {
                frinkBigDecimal4 = clone(this).round(mathContext);
            } else {
                frinkBigDecimal4 = this;
            }
            if (frinkBigDecimal.mant.length > i4) {
                i = 0;
                frinkBigDecimal2 = frinkBigDecimal4;
                frinkBigDecimal3 = clone(frinkBigDecimal).round(mathContext);
            } else {
                i = 0;
                frinkBigDecimal2 = frinkBigDecimal4;
                frinkBigDecimal3 = frinkBigDecimal;
            }
        } else {
            if (this.exp > 0) {
                i3 = 0 + this.exp;
            }
            if (frinkBigDecimal.exp > 0) {
                i = i3 + frinkBigDecimal.exp;
                frinkBigDecimal2 = this;
                frinkBigDecimal3 = frinkBigDecimal;
            } else {
                i = i3;
                frinkBigDecimal2 = this;
                frinkBigDecimal3 = frinkBigDecimal;
            }
        }
        if (frinkBigDecimal2.mant.length < frinkBigDecimal3.mant.length) {
            byte[] bArr3 = frinkBigDecimal2.mant;
            bArr = frinkBigDecimal3.mant;
            bArr2 = bArr3;
        } else {
            byte[] bArr4 = frinkBigDecimal3.mant;
            bArr = frinkBigDecimal2.mant;
            bArr2 = bArr4;
        }
        int length = (bArr2.length + bArr.length) - 1;
        if (bArr2[0] * bArr[0] > 9) {
            i2 = length + 1;
        } else {
            i2 = length;
        }
        FrinkBigDecimal frinkBigDecimal5 = new FrinkBigDecimal();
        int length2 = bArr2.length;
        int i5 = 0;
        int i6 = length;
        byte[] bArr5 = new byte[i2];
        while (length2 > 0) {
            byte b = bArr2[i5];
            if (b != 0) {
                bArr5 = byteaddsub(bArr5, bArr5.length, bArr, i6, b, true);
            }
            length2--;
            i5++;
            i6--;
        }
        frinkBigDecimal5.ind = (byte) (frinkBigDecimal2.ind * frinkBigDecimal3.ind);
        frinkBigDecimal5.exp = (frinkBigDecimal2.exp + frinkBigDecimal3.exp) - i;
        if (i == 0) {
            frinkBigDecimal5.mant = bArr5;
        } else {
            frinkBigDecimal5.mant = extend(bArr5, bArr5.length + i);
        }
        return frinkBigDecimal5.finish(mathContext, false);
    }

    public FrinkBigDecimal negate() {
        return negate(plainMC);
    }

    public FrinkBigDecimal negate(MathContext mathContext) {
        if (mathContext.lostDigits) {
            checkdigits(null, mathContext.digits);
        }
        FrinkBigDecimal clone = clone(this);
        clone.ind = (byte) (-clone.ind);
        return clone.finish(mathContext, false);
    }

    public FrinkBigDecimal plus() {
        return plus(plainMC);
    }

    public FrinkBigDecimal plus(MathContext mathContext) {
        if (mathContext.lostDigits) {
            checkdigits(null, mathContext.digits);
        }
        if (mathContext.form == 0 && this.form == 0) {
            if (this.mant.length <= mathContext.digits) {
                return this;
            }
            if (mathContext.digits == 0) {
                return this;
            }
        }
        return clone(this).finish(mathContext, false);
    }

    public FrinkBigDecimal pow(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        FrinkBigDecimal frinkBigDecimal2;
        int length;
        FrinkBigDecimal frinkBigDecimal3;
        if (mathContext.lostDigits) {
            checkdigits(frinkBigDecimal, mathContext.digits);
        }
        int intcheck = frinkBigDecimal.intcheck(-999999999, 999999999);
        int i = mathContext.digits;
        if (i == 0) {
            if (frinkBigDecimal.ind == -1) {
                throw new ArithmeticException("Negative power: " + frinkBigDecimal.toString());
            }
            length = 0;
            frinkBigDecimal2 = this;
        } else if (frinkBigDecimal.mant.length + frinkBigDecimal.exp > i) {
            throw new ArithmeticException("Too many digits: " + frinkBigDecimal.toString());
        } else {
            if (this.mant.length > i) {
                frinkBigDecimal2 = clone(this).round(mathContext);
            } else {
                frinkBigDecimal2 = this;
            }
            length = i + frinkBigDecimal.mant.length + frinkBigDecimal.exp + 1;
        }
        MathContext mathContext2 = new MathContext(length, mathContext.form, false, mathContext.roundingMode);
        FrinkBigDecimal frinkBigDecimal4 = ONE;
        if (intcheck == 0) {
            return frinkBigDecimal4;
        }
        if (intcheck < 0) {
            intcheck = -intcheck;
        }
        FrinkBigDecimal frinkBigDecimal5 = frinkBigDecimal4;
        int i2 = intcheck;
        int i3 = 1;
        boolean z = false;
        while (true) {
            i2 += i2;
            if (i2 < 0) {
                frinkBigDecimal5 = frinkBigDecimal5.multiply(frinkBigDecimal2, mathContext2);
                z = true;
            }
            if (i3 == 31) {
                break;
            }
            if (z) {
                frinkBigDecimal5 = frinkBigDecimal5.multiply(frinkBigDecimal5, mathContext2);
            }
            i3++;
        }
        if (frinkBigDecimal.ind < 0) {
            frinkBigDecimal3 = ONE.divide(frinkBigDecimal5, mathContext2);
        } else {
            frinkBigDecimal3 = frinkBigDecimal5;
        }
        return frinkBigDecimal3.finish(mathContext, true);
    }

    public FrinkBigDecimal remainder(FrinkBigDecimal frinkBigDecimal) {
        return dodivide('R', frinkBigDecimal, plainMC, -1);
    }

    public FrinkBigDecimal remainder(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        return dodivide('R', frinkBigDecimal, mathContext, -1);
    }

    public FrinkBigDecimal subtract(FrinkBigDecimal frinkBigDecimal, MathContext mathContext) {
        if (mathContext.lostDigits) {
            checkdigits(frinkBigDecimal, mathContext.digits);
        }
        FrinkBigDecimal clone = clone(frinkBigDecimal);
        clone.ind = (byte) (-clone.ind);
        return add(clone, mathContext);
    }

    public byte byteValueExact() {
        boolean z;
        int intValueExact = intValueExact();
        if (intValueExact > 127) {
            z = true;
        } else {
            z = false;
        }
        if (!z && !(intValueExact < -128)) {
            return (byte) intValueExact;
        }
        throw new ArithmeticException("Conversion overflow: " + toString());
    }

    public int compareTo(Object obj) {
        return compareTo((FrinkBigDecimal) obj, plainMC);
    }

    public double doubleValue() {
        return Double.valueOf(toString()).doubleValue();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FrinkBigDecimal)) {
            return false;
        }
        FrinkBigDecimal frinkBigDecimal = (FrinkBigDecimal) obj;
        if (this.ind != frinkBigDecimal.ind) {
            return false;
        }
        if (((this.mant.length == frinkBigDecimal.mant.length) & (this.exp == frinkBigDecimal.exp)) && (this.form == frinkBigDecimal.form)) {
            int length = this.mant.length;
            int i = 0;
            while (length > 0) {
                if (this.mant[i] != frinkBigDecimal.mant[i]) {
                    return false;
                }
                length--;
                i++;
            }
        } else {
            char[] layout = layout();
            char[] layout2 = frinkBigDecimal.layout();
            if (layout.length != layout2.length) {
                return false;
            }
            int length2 = layout.length;
            int i2 = 0;
            while (length2 > 0) {
                if (layout[i2] != layout2[i2]) {
                    return false;
                }
                length2--;
                i2++;
            }
        }
        return true;
    }

    public float floatValue() {
        return Float.valueOf(toString()).floatValue();
    }

    public String format(int i, int i2) {
        return format(i, i2, -1, -1, 1, 4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0059 A[LOOP:0: B:34:0x0059->B:125:0x0059, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String format(int r9, int r10, int r11, int r12, int r13, int r14) {
        /*
            r8 = this;
            r0 = -1
            if (r9 >= r0) goto L_0x009b
            r0 = 1
        L_0x0004:
            if (r9 != 0) goto L_0x009e
            r1 = 1
        L_0x0007:
            r0 = r0 | r1
            if (r0 == 0) goto L_0x0014
            java.lang.String r0 = "format"
            r1 = 1
            java.lang.String r2 = java.lang.String.valueOf(r9)
            r8.badarg(r0, r1, r2)
        L_0x0014:
            r0 = -1
            if (r10 >= r0) goto L_0x0021
            java.lang.String r0 = "format"
            r1 = 2
            java.lang.String r2 = java.lang.String.valueOf(r10)
            r8.badarg(r0, r1, r2)
        L_0x0021:
            r0 = -1
            if (r11 >= r0) goto L_0x00a1
            r0 = 1
        L_0x0025:
            if (r11 != 0) goto L_0x00a3
            r1 = 1
        L_0x0028:
            r0 = r0 | r1
            if (r0 == 0) goto L_0x0035
            java.lang.String r0 = "format"
            r1 = 3
            java.lang.String r2 = java.lang.String.valueOf(r11)
            r8.badarg(r0, r1, r2)
        L_0x0035:
            r0 = -1
            if (r12 >= r0) goto L_0x0042
            java.lang.String r0 = "format"
            r1 = 4
            java.lang.String r2 = java.lang.String.valueOf(r11)
            r8.badarg(r0, r1, r2)
        L_0x0042:
            r0 = 1
            if (r13 != r0) goto L_0x00a5
            r0 = r13
        L_0x0046:
            r1 = 4
            if (r14 == r1) goto L_0x00c4
            r1 = -1
            if (r14 != r1) goto L_0x00bb
            r1 = 4
        L_0x004d:
            frink.numeric.FrinkBigDecimal r2 = clone(r8)
            r3 = -1
            if (r12 != r3) goto L_0x00d2
            r0 = 0
            r2.form = r0
        L_0x0057:
            if (r10 < 0) goto L_0x0062
        L_0x0059:
            byte r0 = r2.form
            if (r0 != 0) goto L_0x00f5
            int r0 = r2.exp
            int r0 = -r0
        L_0x0060:
            if (r0 != r10) goto L_0x0120
        L_0x0062:
            char[] r0 = r2.layout()
            if (r9 <= 0) goto L_0x0189
            int r1 = r0.length
            r2 = 0
        L_0x006a:
            if (r1 <= 0) goto L_0x007a
            char r3 = r0[r2]
            r4 = 46
            if (r3 == r4) goto L_0x007a
            r4 = 69
            if (r3 == r4) goto L_0x007a
            r4 = 101(0x65, float:1.42E-43)
            if (r3 != r4) goto L_0x017d
        L_0x007a:
            if (r2 <= r9) goto L_0x0086
            java.lang.String r1 = "format"
            r3 = 1
            java.lang.String r4 = java.lang.String.valueOf(r9)
            r8.badarg(r1, r3, r4)
        L_0x0086:
            if (r2 >= r9) goto L_0x0189
            int r1 = r0.length
            int r1 = r1 + r9
            int r1 = r1 - r2
            char[] r1 = new char[r1]
            int r2 = r9 - r2
            r3 = 0
        L_0x0090:
            if (r2 <= 0) goto L_0x0183
            r4 = 32
            r1[r3] = r4
            int r2 = r2 + -1
            int r3 = r3 + 1
            goto L_0x0090
        L_0x009b:
            r0 = 0
            goto L_0x0004
        L_0x009e:
            r1 = 0
            goto L_0x0007
        L_0x00a1:
            r0 = 0
            goto L_0x0025
        L_0x00a3:
            r1 = 0
            goto L_0x0028
        L_0x00a5:
            r0 = 2
            if (r13 != r0) goto L_0x00aa
            r0 = r13
            goto L_0x0046
        L_0x00aa:
            r0 = -1
            if (r13 != r0) goto L_0x00af
            r0 = 1
            goto L_0x0046
        L_0x00af:
            java.lang.String r0 = "format"
            r1 = 5
            java.lang.String r2 = java.lang.String.valueOf(r13)
            r8.badarg(r0, r1, r2)
            r0 = r13
            goto L_0x0046
        L_0x00bb:
            frink.numeric.MathContext r1 = new frink.numeric.MathContext     // Catch:{ IllegalArgumentException -> 0x00c6 }
            r2 = 9
            r3 = 1
            r4 = 0
            r1.<init>(r2, r3, r4, r14)     // Catch:{ IllegalArgumentException -> 0x00c6 }
        L_0x00c4:
            r1 = r14
            goto L_0x004d
        L_0x00c6:
            r1 = move-exception
            java.lang.String r1 = "format"
            r2 = 6
            java.lang.String r3 = java.lang.String.valueOf(r14)
            r8.badarg(r1, r2, r3)
            goto L_0x00c4
        L_0x00d2:
            byte r3 = r2.ind
            if (r3 != 0) goto L_0x00db
            r0 = 0
            r2.form = r0
            goto L_0x0057
        L_0x00db:
            int r3 = r2.exp
            byte[] r4 = r2.mant
            int r4 = r4.length
            int r3 = r3 + r4
            if (r3 <= r12) goto L_0x00e8
            byte r0 = (byte) r0
            r2.form = r0
            goto L_0x0057
        L_0x00e8:
            r4 = -5
            if (r3 >= r4) goto L_0x00f0
            byte r0 = (byte) r0
            r2.form = r0
            goto L_0x0057
        L_0x00f0:
            r0 = 0
            r2.form = r0
            goto L_0x0057
        L_0x00f5:
            byte r0 = r2.form
            r3 = 1
            if (r0 != r3) goto L_0x0101
            byte[] r0 = r2.mant
            int r0 = r0.length
            r3 = 1
            int r0 = r0 - r3
            goto L_0x0060
        L_0x0101:
            int r0 = r2.exp
            byte[] r3 = r2.mant
            int r3 = r3.length
            int r0 = r0 + r3
            r3 = 1
            int r0 = r0 - r3
            int r0 = r0 % 3
            if (r0 >= 0) goto L_0x010f
            int r0 = r0 + 3
        L_0x010f:
            int r0 = r0 + 1
            byte[] r3 = r2.mant
            int r3 = r3.length
            if (r0 < r3) goto L_0x0119
            r0 = 0
            goto L_0x0060
        L_0x0119:
            byte[] r3 = r2.mant
            int r3 = r3.length
            int r0 = r3 - r0
            goto L_0x0060
        L_0x0120:
            if (r0 >= r10) goto L_0x0159
            byte[] r1 = r2.mant
            byte[] r3 = r2.mant
            int r3 = r3.length
            int r3 = r3 + r10
            int r3 = r3 - r0
            byte[] r1 = extend(r1, r3)
            r2.mant = r1
            int r1 = r2.exp
            int r0 = r10 - r0
            int r0 = r1 - r0
            r2.exp = r0
            int r0 = r2.exp
            r1 = -999999999(0xffffffffc4653601, float:-916.8438)
            if (r0 >= r1) goto L_0x0062
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Exponent Overflow: "
            java.lang.StringBuilder r1 = r1.append(r3)
            int r2 = r2.exp
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0159:
            int r0 = r0 - r10
            byte[] r3 = r2.mant
            int r3 = r3.length
            if (r0 <= r3) goto L_0x016d
            frink.numeric.FrinkBigDecimal r0 = frink.numeric.FrinkBigDecimal.ZERO
            byte[] r0 = r0.mant
            r2.mant = r0
            r0 = 0
            r2.ind = r0
            r0 = 0
            r2.exp = r0
            goto L_0x0059
        L_0x016d:
            byte[] r3 = r2.mant
            int r3 = r3.length
            int r3 = r3 - r0
            int r4 = r2.exp
            r2.round(r3, r1)
            int r3 = r2.exp
            int r3 = r3 - r4
            if (r3 != r0) goto L_0x0059
            goto L_0x0062
        L_0x017d:
            int r1 = r1 + -1
            int r2 = r2 + 1
            goto L_0x006a
        L_0x0183:
            r2 = 0
            int r4 = r0.length
            java.lang.System.arraycopy(r0, r2, r1, r3, r4)
            r0 = r1
        L_0x0189:
            if (r11 <= 0) goto L_0x01c2
            int r1 = r0.length
            r2 = 1
            int r1 = r1 - r2
            int r2 = r0.length
            r3 = 1
            int r2 = r2 - r3
        L_0x0191:
            if (r1 <= 0) goto L_0x019d
            char r3 = r0[r2]
            r4 = 69
            if (r3 == r4) goto L_0x019d
            r4 = 101(0x65, float:1.42E-43)
            if (r3 != r4) goto L_0x01bc
        L_0x019d:
            if (r2 != 0) goto L_0x01c8
            int r1 = r0.length
            int r1 = r1 + r11
            int r1 = r1 + 2
            char[] r1 = new char[r1]
            r2 = 0
            r3 = 0
            int r4 = r0.length
            java.lang.System.arraycopy(r0, r2, r1, r3, r4)
            int r2 = r11 + 2
            int r0 = r0.length
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x01b1:
            if (r0 <= 0) goto L_0x01c1
            r3 = 32
            r1[r2] = r3
            int r0 = r0 + -1
            int r2 = r2 + 1
            goto L_0x01b1
        L_0x01bc:
            int r1 = r1 + -1
            int r2 = r2 + -1
            goto L_0x0191
        L_0x01c1:
            r0 = r1
        L_0x01c2:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r0)
            return r1
        L_0x01c8:
            int r1 = r0.length
            int r1 = r1 - r2
            r3 = 2
            int r1 = r1 - r3
            if (r1 <= r11) goto L_0x01d8
            java.lang.String r3 = "format"
            r4 = 3
            java.lang.String r5 = java.lang.String.valueOf(r11)
            r8.badarg(r3, r4, r5)
        L_0x01d8:
            if (r1 >= r11) goto L_0x01c2
            int r3 = r0.length
            int r3 = r3 + r11
            int r3 = r3 - r1
            char[] r3 = new char[r3]
            r4 = 0
            r5 = 0
            int r6 = r2 + 2
            java.lang.System.arraycopy(r0, r4, r3, r5, r6)
            int r4 = r11 - r1
            int r5 = r2 + 2
        L_0x01ea:
            if (r4 <= 0) goto L_0x01f5
            r6 = 48
            r3[r5] = r6
            int r4 = r4 + -1
            int r5 = r5 + 1
            goto L_0x01ea
        L_0x01f5:
            int r2 = r2 + 2
            java.lang.System.arraycopy(r0, r2, r3, r5, r1)
            r0 = r3
            goto L_0x01c2
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.FrinkBigDecimal.format(int, int, int, int, int, int):java.lang.String");
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public int intValue() {
        return toBigInteger().intValue();
    }

    public int intValueExact() {
        int i;
        int i2;
        if (this.ind == 0) {
            return 0;
        }
        int length = this.mant.length - 1;
        if (this.exp < 0) {
            int i3 = length + this.exp;
            if (!allzero(this.mant, i3 + 1)) {
                throw new ArithmeticException("Decimal part non-zero: " + toString());
            } else if (i3 < 0) {
                return 0;
            } else {
                i = i3;
                i2 = 0;
            }
        } else if (this.exp + length > 9) {
            throw new ArithmeticException("Conversion overflow: " + toString());
        } else {
            i = length;
            i2 = this.exp;
        }
        int i4 = i + i2;
        int i5 = 0;
        for (int i6 = 0; i6 <= i4; i6++) {
            i5 *= 10;
            if (i6 <= i) {
                i5 += this.mant[i6];
            }
        }
        if (i2 + i != 9 || i5 / 1000000000 == this.mant[0]) {
            if (this.ind == 1) {
                return i5;
            }
            return -i5;
        } else if (i5 == Integer.MIN_VALUE && this.ind == -1 && this.mant[0] == 2) {
            return i5;
        } else {
            throw new ArithmeticException("Conversion overflow: " + toString());
        }
    }

    public long longValue() {
        return toBigInteger().longValue();
    }

    public long longValueExact() {
        int i;
        int i2;
        int i3;
        if (this.ind == 0) {
            return 0;
        }
        int length = this.mant.length - 1;
        if (this.exp < 0) {
            int i4 = length + this.exp;
            if (i4 < 0) {
                i3 = 0;
            } else {
                i3 = i4 + 1;
            }
            if (!allzero(this.mant, i3)) {
                throw new ArithmeticException("Decimal part non-zero: " + toString());
            } else if (i4 < 0) {
                return 0;
            } else {
                i = i4;
                i2 = 0;
            }
        } else if (this.exp + this.mant.length > 18) {
            throw new ArithmeticException("Conversion overflow: " + toString());
        } else {
            i = length;
            i2 = this.exp;
        }
        int i5 = i + i2;
        long j = 0;
        for (int i6 = 0; i6 <= i5; i6++) {
            j *= 10;
            if (i6 <= i) {
                j += (long) this.mant[i6];
            }
        }
        if (i2 + i != 18 || j / 1000000000000000000L == ((long) this.mant[0])) {
            if (this.ind == 1) {
                return j;
            }
            return -j;
        } else if (j == Long.MIN_VALUE && this.ind == -1 && this.mant[0] == 9) {
            return j;
        } else {
            throw new ArithmeticException("Conversion overflow: " + toString());
        }
    }

    public FrinkBigDecimal movePointLeft(int i) {
        FrinkBigDecimal clone = clone(this);
        clone.exp -= i;
        return clone.finish(plainMC, false);
    }

    public FrinkBigDecimal movePointRight(int i) {
        FrinkBigDecimal clone = clone(this);
        clone.exp += i;
        return clone.finish(plainMC, false);
    }

    public int scale() {
        if (this.exp >= 0) {
            return 0;
        }
        return -this.exp;
    }

    public FrinkBigDecimal setScale(int i) {
        return setScale(i, 7);
    }

    public FrinkBigDecimal setScale(int i, int i2) {
        FrinkBigDecimal round;
        int i3;
        int scale = scale();
        if (scale == i && this.form == 0) {
            return this;
        }
        FrinkBigDecimal clone = clone(this);
        if (scale <= i) {
            if (scale == 0) {
                i3 = clone.exp + i;
            } else {
                i3 = i - scale;
            }
            clone.mant = extend(clone.mant, i3 + clone.mant.length);
            clone.exp = -i;
            round = clone;
        } else if (i < 0) {
            throw new ArithmeticException("Negative scale: " + i);
        } else {
            round = clone.round(clone.mant.length - (scale - i), i2);
            if (round.exp != (-i)) {
                round.mant = extend(round.mant, round.mant.length + 1);
                round.exp--;
            }
        }
        round.form = iszero;
        return round;
    }

    public short shortValueExact() {
        boolean z;
        int intValueExact = intValueExact();
        if (intValueExact > 32767) {
            z = true;
        } else {
            z = false;
        }
        if (!z && !(intValueExact < -32768)) {
            return (short) intValueExact;
        }
        throw new ArithmeticException("Conversion overflow: " + toString());
    }

    public int signum() {
        return this.ind;
    }

    public BigDecimal toBigDecimal() {
        return new BigDecimal(unscaledValue(), scale());
    }

    public BigInteger toBigInteger() {
        boolean z;
        boolean z2;
        FrinkBigDecimal clone;
        if (this.exp >= 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.form == 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z && z2) {
            clone = this;
        } else if (this.exp >= 0) {
            clone = clone(this);
            clone.form = iszero;
        } else if ((-this.exp) >= this.mant.length) {
            clone = ZERO;
        } else {
            clone = clone(this);
            int length = clone.mant.length + clone.exp;
            byte[] bArr = new byte[length];
            System.arraycopy(clone.mant, 0, bArr, 0, length);
            clone.mant = bArr;
            clone.form = iszero;
            clone.exp = 0;
        }
        return new BigInteger(new String(clone.layout()));
    }

    public BigInteger toBigIntegerExact() {
        if (this.exp >= 0 || allzero(this.mant, this.mant.length + this.exp)) {
            return toBigInteger();
        }
        throw new ArithmeticException("Decimal part non-zero: " + toString());
    }

    public char[] toCharArray() {
        return layout();
    }

    public String toString() {
        return new String(layout());
    }

    public BigInteger unscaledValue() {
        FrinkBigDecimal clone;
        if (this.exp >= 0) {
            clone = this;
        } else {
            clone = clone(this);
            clone.exp = 0;
        }
        return clone.toBigInteger();
    }

    public static FrinkBigDecimal valueOf(double d) {
        return new FrinkBigDecimal(new Double(d).toString());
    }

    public static FrinkBigDecimal valueOf(long j) {
        return valueOf(j, 0);
    }

    public static FrinkBigDecimal valueOf(long j, int i) {
        FrinkBigDecimal frinkBigDecimal;
        if (j == 0) {
            frinkBigDecimal = ZERO;
        } else if (j == 1) {
            frinkBigDecimal = ONE;
        } else if (j == 10) {
            frinkBigDecimal = TEN;
        } else {
            frinkBigDecimal = new FrinkBigDecimal(j);
        }
        if (i == 0) {
            return frinkBigDecimal;
        }
        if (i < 0) {
            throw new NumberFormatException("Negative scale: " + i);
        }
        FrinkBigDecimal clone = clone(frinkBigDecimal);
        clone.exp = -i;
        return clone;
    }

    private char[] layout() {
        int i;
        int i2;
        char c;
        char[] cArr = new char[this.mant.length];
        int length = this.mant.length;
        int i3 = 0;
        while (length > 0) {
            cArr[i3] = (char) (this.mant[i3] + 48);
            length--;
            i3++;
        }
        if (this.form != 0) {
            StringBuffer stringBuffer = new StringBuffer(cArr.length + 15);
            if (this.ind == -1) {
                stringBuffer.append('-');
            }
            int length2 = (this.exp + cArr.length) - 1;
            if (this.form == 1) {
                stringBuffer.append(cArr[0]);
                if (cArr.length > 1) {
                    stringBuffer.append('.').append(cArr, 1, cArr.length - 1);
                    i = length2;
                }
                i = length2;
            } else {
                int i4 = length2 % 3;
                if (i4 < 0) {
                    i4 += 3;
                }
                length2 -= i4;
                int i5 = i4 + 1;
                if (i5 >= cArr.length) {
                    stringBuffer.append(cArr, 0, cArr.length);
                    for (int length3 = i5 - cArr.length; length3 > 0; length3--) {
                        stringBuffer.append('0');
                    }
                    i = length2;
                } else {
                    stringBuffer.append(cArr, 0, i5).append('.').append(cArr, i5, cArr.length - i5);
                    i = length2;
                }
            }
            if (i != 0) {
                if (i < 0) {
                    i2 = -i;
                    c = '-';
                } else {
                    i2 = i;
                    c = '+';
                }
                stringBuffer.append('e').append(c).append(i2);
            }
            char[] cArr2 = new char[stringBuffer.length()];
            stringBuffer.getChars(0, stringBuffer.length(), cArr2, 0);
            return cArr2;
        } else if (this.exp != 0) {
            int i6 = this.ind == -1 ? 1 : 0;
            int length4 = this.exp + cArr.length;
            if (length4 < 1) {
                char[] cArr3 = new char[((i6 + 2) - this.exp)];
                if (i6 != 0) {
                    cArr3[0] = '-';
                }
                cArr3[i6] = '0';
                cArr3[i6 + 1] = '.';
                int i7 = -length4;
                int i8 = i6 + 2;
                while (i7 > 0) {
                    cArr3[i8] = '0';
                    i7--;
                    i8++;
                }
                System.arraycopy(cArr, 0, cArr3, (i6 + 2) - length4, cArr.length);
                return cArr3;
            } else if (length4 > cArr.length) {
                char[] cArr4 = new char[(i6 + length4)];
                if (i6 != 0) {
                    cArr4[0] = '-';
                }
                System.arraycopy(cArr, 0, cArr4, i6, cArr.length);
                int length5 = cArr.length + i6;
                int length6 = length4 - cArr.length;
                while (length6 > 0) {
                    cArr4[length5] = '0';
                    length6--;
                    length5++;
                }
                return cArr4;
            } else {
                char[] cArr5 = new char[(i6 + 1 + cArr.length)];
                if (i6 != 0) {
                    cArr5[0] = '-';
                }
                System.arraycopy(cArr, 0, cArr5, i6, length4);
                cArr5[i6 + length4] = '.';
                System.arraycopy(cArr, length4, cArr5, i6 + length4 + 1, cArr.length - length4);
                return cArr5;
            }
        } else if (this.ind >= 0) {
            return cArr;
        } else {
            char[] cArr6 = new char[(cArr.length + 1)];
            cArr6[0] = '-';
            System.arraycopy(cArr, 0, cArr6, 1, cArr.length);
            return cArr6;
        }
    }

    private int intcheck(int i, int i2) {
        boolean z;
        boolean z2 = false;
        int intValueExact = intValueExact();
        if (intValueExact < i) {
            z = true;
        } else {
            z = false;
        }
        if (intValueExact > i2) {
            z2 = true;
        }
        if (!z && !z2) {
            return intValueExact;
        }
        throw new ArithmeticException("Conversion overflow: " + intValueExact);
    }

    /* JADX WARN: Type inference failed for: r8v19, types: [byte] */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01f8, code lost:
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01fb, code lost:
        r16 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0202, code lost:
        if (r3[0] != 0) goto L_0x0208;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0204, code lost:
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0207, code lost:
        r7 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0208, code lost:
        if (r11 < 0) goto L_0x0212;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x020d, code lost:
        if ((-r13.exp) <= r11) goto L_0x0212;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x020f, code lost:
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0217, code lost:
        if (r21 == 'D') goto L_0x0220;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x021b, code lost:
        if (r13.exp > 0) goto L_0x0220;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x021d, code lost:
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0134, code lost:
        if (r15 == 0) goto L_0x01f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0136, code lost:
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0137, code lost:
        if (r7 == 0) goto L_0x01fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0139, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x013d, code lost:
        if ((r8 | r16) == false) goto L_0x0207;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x013f, code lost:
        r13.mant[r15] = (byte) r7;
        r7 = r15 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0148, code lost:
        if (r7 != (r9 + 1)) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x014a, code lost:
        r5 = r7;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r8v19, types: [byte] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private frink.numeric.FrinkBigDecimal dodivide(char r21, frink.numeric.FrinkBigDecimal r22, frink.numeric.MathContext r23, int r24) {
        /*
            r20 = this;
            r0 = r23
            boolean r0 = r0.lostDigits
            r3 = r0
            if (r3 == 0) goto L_0x0014
            r0 = r23
            int r0 = r0.digits
            r3 = r0
            r0 = r20
            r1 = r22
            r2 = r3
            r0.checkdigits(r1, r2)
        L_0x0014:
            r0 = r22
            byte r0 = r0.ind
            r3 = r0
            if (r3 != 0) goto L_0x0023
            java.lang.ArithmeticException r3 = new java.lang.ArithmeticException
            java.lang.String r4 = "Divide by 0"
            r3.<init>(r4)
            throw r3
        L_0x0023:
            r0 = r20
            byte r0 = r0.ind
            r3 = r0
            if (r3 != 0) goto L_0x0046
            r0 = r23
            int r0 = r0.form
            r3 = r0
            if (r3 == 0) goto L_0x0034
            frink.numeric.FrinkBigDecimal r3 = frink.numeric.FrinkBigDecimal.ZERO
        L_0x0033:
            return r3
        L_0x0034:
            r3 = -1
            r0 = r24
            r1 = r3
            if (r0 != r1) goto L_0x003d
            r3 = r20
            goto L_0x0033
        L_0x003d:
            r0 = r20
            r1 = r24
            frink.numeric.FrinkBigDecimal r3 = r0.setScale(r1)
            goto L_0x0033
        L_0x0046:
            r0 = r23
            int r0 = r0.digits
            r3 = r0
            if (r3 <= 0) goto L_0x0098
            r0 = r20
            byte[] r0 = r0.mant
            r4 = r0
            int r4 = r4.length
            if (r4 <= r3) goto L_0x0354
            frink.numeric.FrinkBigDecimal r4 = clone(r20)
            r0 = r4
            r1 = r23
            frink.numeric.FrinkBigDecimal r4 = r0.round(r1)
        L_0x0060:
            r0 = r22
            byte[] r0 = r0.mant
            r5 = r0
            int r5 = r5.length
            if (r5 <= r3) goto L_0x034c
            frink.numeric.FrinkBigDecimal r5 = clone(r22)
            r0 = r5
            r1 = r23
            frink.numeric.FrinkBigDecimal r5 = r0.round(r1)
            r9 = r3
            r10 = r4
            r11 = r24
            r12 = r5
        L_0x0078:
            int r3 = r10.exp
            int r4 = r12.exp
            int r3 = r3 - r4
            byte[] r4 = r10.mant
            int r4 = r4.length
            int r3 = r3 + r4
            byte[] r4 = r12.mant
            int r4 = r4.length
            int r3 = r3 - r4
            if (r3 >= 0) goto L_0x00f8
            r4 = 68
            r0 = r21
            r1 = r4
            if (r0 == r1) goto L_0x00f8
            r3 = 73
            r0 = r21
            r1 = r3
            if (r0 != r1) goto L_0x00e9
            frink.numeric.FrinkBigDecimal r3 = frink.numeric.FrinkBigDecimal.ZERO
            goto L_0x0033
        L_0x0098:
            r3 = -1
            r0 = r24
            r1 = r3
            if (r0 != r1) goto L_0x0348
            int r3 = r20.scale()
        L_0x00a2:
            r0 = r20
            byte[] r0 = r0.mant
            r4 = r0
            int r4 = r4.length
            r0 = r20
            int r0 = r0.exp
            r5 = r0
            int r5 = -r5
            if (r3 == r5) goto L_0x00b7
            int r4 = r4 + r3
            r0 = r20
            int r0 = r0.exp
            r5 = r0
            int r4 = r4 + r5
        L_0x00b7:
            r0 = r22
            byte[] r0 = r0.mant
            r5 = r0
            int r5 = r5.length
            r6 = 1
            int r5 = r5 - r6
            int r4 = r4 - r5
            r0 = r22
            int r0 = r0.exp
            r5 = r0
            int r4 = r4 - r5
            r0 = r20
            byte[] r0 = r0.mant
            r5 = r0
            int r5 = r5.length
            if (r4 >= r5) goto L_0x00d4
            r0 = r20
            byte[] r0 = r0.mant
            r4 = r0
            int r4 = r4.length
        L_0x00d4:
            r0 = r22
            byte[] r0 = r0.mant
            r5 = r0
            int r5 = r5.length
            if (r4 >= r5) goto L_0x0340
            r0 = r22
            byte[] r0 = r0.mant
            r4 = r0
            int r4 = r4.length
            r9 = r4
            r10 = r20
            r11 = r3
            r12 = r22
            goto L_0x0078
        L_0x00e9:
            frink.numeric.FrinkBigDecimal r3 = clone(r10)
            r4 = 0
            r0 = r3
            r1 = r23
            r2 = r4
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0033
        L_0x00f8:
            frink.numeric.FrinkBigDecimal r13 = new frink.numeric.FrinkBigDecimal
            r13.<init>()
            byte r4 = r10.ind
            byte r5 = r12.ind
            int r4 = r4 * r5
            byte r4 = (byte) r4
            r13.ind = r4
            r13.exp = r3
            int r3 = r9 + 1
            byte[] r3 = new byte[r3]
            r13.mant = r3
            int r3 = r9 + r9
            int r3 = r3 + 1
            byte[] r4 = r10.mant
            byte[] r4 = extend(r4, r3)
            byte[] r5 = r12.mant
            r6 = 0
            byte r6 = r5[r6]
            int r6 = r6 * 10
            int r6 = r6 + 1
            int r7 = r5.length
            r8 = 1
            if (r7 <= r8) goto L_0x033d
            r7 = 1
            byte r7 = r5[r7]
            int r6 = r6 + r7
            r14 = r6
        L_0x0129:
            r6 = 0
            r15 = r6
            r6 = r3
        L_0x012c:
            r7 = 0
            r19 = r3
            r3 = r4
            r4 = r19
        L_0x0132:
            if (r4 >= r6) goto L_0x016e
        L_0x0134:
            if (r15 == 0) goto L_0x01f8
            r8 = 1
        L_0x0137:
            if (r7 == 0) goto L_0x01fb
            r16 = 1
        L_0x013b:
            r8 = r8 | r16
            if (r8 == 0) goto L_0x0207
            byte[] r8 = r13.mant
            byte r7 = (byte) r7
            r8[r15] = r7
            int r7 = r15 + 1
            int r8 = r9 + 1
            if (r7 != r8) goto L_0x01ff
            r5 = r7
        L_0x014b:
            if (r5 != 0) goto L_0x014e
            r5 = 1
        L_0x014e:
            r6 = 73
            r0 = r21
            r1 = r6
            if (r0 != r1) goto L_0x0230
            r6 = 1
        L_0x0156:
            r7 = 82
            r0 = r21
            r1 = r7
            if (r0 != r1) goto L_0x0233
            r7 = 1
        L_0x015e:
            r6 = r6 | r7
            if (r6 == 0) goto L_0x02b3
            int r6 = r13.exp
            int r6 = r6 + r5
            if (r6 <= r9) goto L_0x0236
            java.lang.ArithmeticException r3 = new java.lang.ArithmeticException
            java.lang.String r4 = "Integer overflow"
            r3.<init>(r4)
            throw r3
        L_0x016e:
            if (r4 != r6) goto L_0x01c1
            r8 = 0
            r16 = r8
            r8 = r4
        L_0x0174:
            if (r8 <= 0) goto L_0x01b3
            r0 = r5
            int r0 = r0.length
            r17 = r0
            r0 = r16
            r1 = r17
            if (r0 >= r1) goto L_0x01ab
            byte r17 = r5[r16]
        L_0x0182:
            byte r18 = r3[r16]
            r0 = r18
            r1 = r17
            if (r0 < r1) goto L_0x0134
            byte r18 = r3[r16]
            r0 = r18
            r1 = r17
            if (r0 <= r1) goto L_0x01ae
            r8 = 0
            byte r8 = r3[r8]
        L_0x0195:
            int r8 = r8 * 10
            int r8 = r8 / r14
            if (r8 != 0) goto L_0x019b
            r8 = 1
        L_0x019b:
            int r16 = r7 + r8
            int r7 = -r8
            r8 = 1
            byte[] r3 = byteaddsub(r3, r4, r5, r6, r7, r8)
            r7 = 0
            byte r7 = r3[r7]
            if (r7 == 0) goto L_0x01d4
            r7 = r16
            goto L_0x0132
        L_0x01ab:
            r17 = 0
            goto L_0x0182
        L_0x01ae:
            int r8 = r8 + -1
            int r16 = r16 + 1
            goto L_0x0174
        L_0x01b3:
            int r5 = r7 + 1
            byte[] r6 = r13.mant
            byte r5 = (byte) r5
            r6[r15] = r5
            int r5 = r15 + 1
            r6 = 0
            r7 = 0
            r3[r6] = r7
            goto L_0x014b
        L_0x01c1:
            r8 = 0
            byte r8 = r3[r8]
            int r8 = r8 * 10
            r16 = 1
            r0 = r4
            r1 = r16
            if (r0 <= r1) goto L_0x0195
            r16 = 1
            byte r16 = r3[r16]
            int r8 = r8 + r16
            goto L_0x0195
        L_0x01d4:
            r7 = 2
            int r7 = r4 - r7
            r8 = 0
            r19 = r8
            r8 = r4
            r4 = r19
        L_0x01dd:
            if (r4 > r7) goto L_0x01e3
            byte r17 = r3[r4]
            if (r17 == 0) goto L_0x01ea
        L_0x01e3:
            if (r4 != 0) goto L_0x01ef
            r7 = r16
            r4 = r8
            goto L_0x0132
        L_0x01ea:
            int r8 = r8 + -1
            int r4 = r4 + 1
            goto L_0x01dd
        L_0x01ef:
            r7 = 0
            java.lang.System.arraycopy(r3, r4, r3, r7, r8)
            r7 = r16
            r4 = r8
            goto L_0x0132
        L_0x01f8:
            r8 = 0
            goto L_0x0137
        L_0x01fb:
            r16 = 0
            goto L_0x013b
        L_0x01ff:
            r8 = 0
            byte r8 = r3[r8]
            if (r8 != 0) goto L_0x0208
            r5 = r7
            goto L_0x014b
        L_0x0207:
            r7 = r15
        L_0x0208:
            if (r11 < 0) goto L_0x0212
            int r8 = r13.exp
            int r8 = -r8
            if (r8 <= r11) goto L_0x0212
            r5 = r7
            goto L_0x014b
        L_0x0212:
            r8 = 68
            r0 = r21
            r1 = r8
            if (r0 == r1) goto L_0x0220
            int r8 = r13.exp
            if (r8 > 0) goto L_0x0220
            r5 = r7
            goto L_0x014b
        L_0x0220:
            int r8 = r13.exp
            r15 = 1
            int r8 = r8 - r15
            r13.exp = r8
            int r6 = r6 + -1
            r15 = r7
            r19 = r4
            r4 = r3
            r3 = r19
            goto L_0x012c
        L_0x0230:
            r6 = 0
            goto L_0x0156
        L_0x0233:
            r7 = 0
            goto L_0x015e
        L_0x0236:
            r6 = 82
            r0 = r21
            r1 = r6
            if (r0 != r1) goto L_0x02cd
            byte[] r5 = r13.mant
            r6 = 0
            byte r5 = r5[r6]
            if (r5 != 0) goto L_0x0253
            frink.numeric.FrinkBigDecimal r3 = clone(r10)
            r4 = 0
            r0 = r3
            r1 = r23
            r2 = r4
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0033
        L_0x0253:
            r5 = 0
            byte r5 = r3[r5]
            if (r5 != 0) goto L_0x025c
            frink.numeric.FrinkBigDecimal r3 = frink.numeric.FrinkBigDecimal.ZERO
            goto L_0x0033
        L_0x025c:
            byte r5 = r10.ind
            r13.ind = r5
            int r5 = r9 + r9
            int r5 = r5 + 1
            byte[] r6 = r10.mant
            int r6 = r6.length
            int r5 = r5 - r6
            int r6 = r13.exp
            int r5 = r6 - r5
            int r6 = r10.exp
            int r5 = r5 + r6
            r13.exp = r5
            r5 = 1
            int r5 = r4 - r5
        L_0x0274:
            r6 = 1
            if (r5 < r6) goto L_0x0288
            int r6 = r13.exp
            int r7 = r10.exp
            if (r6 >= r7) goto L_0x02a0
            r6 = 1
        L_0x027e:
            int r7 = r13.exp
            int r8 = r12.exp
            if (r7 >= r8) goto L_0x02a2
            r7 = 1
        L_0x0285:
            r6 = r6 & r7
            if (r6 != 0) goto L_0x02a4
        L_0x0288:
            int r5 = r3.length
            if (r4 >= r5) goto L_0x0293
            byte[] r5 = new byte[r4]
            r6 = 0
            r7 = 0
            java.lang.System.arraycopy(r3, r6, r5, r7, r4)
            r3 = r5
        L_0x0293:
            r13.mant = r3
            r3 = 0
            r0 = r13
            r1 = r23
            r2 = r3
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0033
        L_0x02a0:
            r6 = 0
            goto L_0x027e
        L_0x02a2:
            r7 = 0
            goto L_0x0285
        L_0x02a4:
            byte r6 = r3[r5]
            if (r6 != 0) goto L_0x0288
            int r4 = r4 + -1
            int r6 = r13.exp
            int r6 = r6 + 1
            r13.exp = r6
            int r5 = r5 + -1
            goto L_0x0274
        L_0x02b3:
            r4 = 0
            byte r3 = r3[r4]
            if (r3 == 0) goto L_0x02cd
            byte[] r3 = r13.mant
            r4 = 1
            int r4 = r5 - r4
            byte r3 = r3[r4]
            int r4 = r3 % 5
            if (r4 != 0) goto L_0x02cd
            byte[] r4 = r13.mant
            r6 = 1
            int r6 = r5 - r6
            int r3 = r3 + 1
            byte r3 = (byte) r3
            r4[r6] = r3
        L_0x02cd:
            if (r11 < 0) goto L_0x0310
            byte[] r3 = r13.mant
            int r3 = r3.length
            if (r5 == r3) goto L_0x02dd
            int r3 = r13.exp
            byte[] r4 = r13.mant
            int r4 = r4.length
            int r4 = r4 - r5
            int r3 = r3 - r4
            r13.exp = r3
        L_0x02dd:
            byte[] r3 = r13.mant
            int r3 = r3.length
            int r4 = r13.exp
            int r4 = -r4
            int r4 = r4 - r11
            int r3 = r3 - r4
            r0 = r23
            int r0 = r0.roundingMode
            r4 = r0
            r13.round(r3, r4)
            int r3 = r13.exp
            int r4 = -r11
            if (r3 == r4) goto L_0x0305
            byte[] r3 = r13.mant
            byte[] r4 = r13.mant
            int r4 = r4.length
            int r4 = r4 + 1
            byte[] r3 = extend(r3, r4)
            r13.mant = r3
            int r3 = r13.exp
            r4 = 1
            int r3 = r3 - r4
            r13.exp = r3
        L_0x0305:
            r3 = 1
            r0 = r13
            r1 = r23
            r2 = r3
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0033
        L_0x0310:
            byte[] r3 = r13.mant
            int r3 = r3.length
            if (r5 != r3) goto L_0x0326
            r0 = r13
            r1 = r23
            r0.round(r1)
        L_0x031b:
            r3 = 1
            r0 = r13
            r1 = r23
            r2 = r3
            frink.numeric.FrinkBigDecimal r3 = r0.finish(r1, r2)
            goto L_0x0033
        L_0x0326:
            byte[] r3 = r13.mant
            r4 = 0
            byte r3 = r3[r4]
            if (r3 != 0) goto L_0x0331
            frink.numeric.FrinkBigDecimal r3 = frink.numeric.FrinkBigDecimal.ZERO
            goto L_0x0033
        L_0x0331:
            byte[] r3 = new byte[r5]
            byte[] r4 = r13.mant
            r6 = 0
            r7 = 0
            java.lang.System.arraycopy(r4, r6, r3, r7, r5)
            r13.mant = r3
            goto L_0x031b
        L_0x033d:
            r14 = r6
            goto L_0x0129
        L_0x0340:
            r9 = r4
            r10 = r20
            r11 = r3
            r12 = r22
            goto L_0x0078
        L_0x0348:
            r3 = r24
            goto L_0x00a2
        L_0x034c:
            r9 = r3
            r10 = r4
            r11 = r24
            r12 = r22
            goto L_0x0078
        L_0x0354:
            r4 = r20
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.FrinkBigDecimal.dodivide(char, frink.numeric.FrinkBigDecimal, frink.numeric.MathContext, int):frink.numeric.FrinkBigDecimal");
    }

    private void bad(char[] cArr) {
        throw new NumberFormatException("Not a number: " + String.valueOf(cArr));
    }

    private void badarg(String str, int i, String str2) {
        throw new IllegalArgumentException("Bad argument " + i + " " + "to" + " " + str + ":" + " " + str2);
    }

    private static final byte[] extend(byte[] bArr, int i) {
        if (bArr.length == i) {
            return bArr;
        }
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:55:0x0047 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    private static final byte[] byteaddsub(byte[] bArr, int i, byte[] bArr2, int i2, int i3, boolean z) {
        int i4;
        byte[] bArr3;
        byte b;
        int length = bArr.length;
        int length2 = bArr2.length;
        int i5 = i - 1;
        int i6 = i2 - 1;
        if (i6 < i5) {
            i4 = i5;
        } else {
            i4 = i6;
        }
        byte[] bArr4 = null;
        if (z && i4 + 1 == length) {
            bArr4 = bArr;
        }
        if (bArr4 == null) {
            bArr3 = new byte[(i4 + 1)];
        } else {
            bArr3 = bArr4;
        }
        boolean z2 = false;
        if (i3 == 1) {
            z2 = true;
        } else if (i3 == -1) {
            z2 = true;
        }
        int i7 = i5;
        int i8 = 0;
        int i9 = i6;
        int i10 = i4;
        while (i10 >= 0) {
            if (i7 >= 0) {
                if (i7 < length) {
                    i8 += bArr[i7];
                }
                i7--;
            }
            if (i9 >= 0) {
                if (i9 < length2) {
                    if (!z2) {
                        i8 += bArr2[i9] * i3;
                    } else if (i3 > 0) {
                        i8 += bArr2[i9];
                    } else {
                        i8 -= bArr2[i9];
                    }
                }
                i9--;
            }
            if (i8 >= 10 || i8 < 0) {
                int i11 = i8 + 90;
                bArr3[i10] = bytedig[i11];
                b = bytecar[i11];
            } else {
                bArr3[i10] = (byte) i8;
                b = iszero;
            }
            i10--;
            i8 = b;
        }
        if (i8 == 0) {
            return bArr3;
        }
        byte[] bArr5 = null;
        if (!z || i4 + 2 != bArr.length) {
            bArr = bArr5;
        }
        if (bArr == null) {
            bArr = new byte[(i4 + 2)];
        }
        bArr[0] = (byte) i8;
        if (i4 < 10) {
            int i12 = i4 + 1;
            int i13 = 0;
            while (i12 > 0) {
                bArr[i13 + 1] = bArr3[i13];
                i12--;
                i13++;
            }
            return bArr;
        }
        System.arraycopy(bArr3, 0, bArr, 1, i4 + 1);
        return bArr;
    }

    private static final byte[] diginit() {
        byte[] bArr = new byte[190];
        for (int i = 0; i <= 189; i++) {
            int i2 = i - 90;
            if (i2 >= 0) {
                bArr[i] = (byte) (i2 % 10);
                bytecar[i] = (byte) (i2 / 10);
            } else {
                int i3 = i2 + 100;
                bArr[i] = (byte) (i3 % 10);
                bytecar[i] = (byte) ((i3 / 10) - 10);
            }
        }
        return bArr;
    }

    private static final FrinkBigDecimal clone(FrinkBigDecimal frinkBigDecimal) {
        FrinkBigDecimal frinkBigDecimal2 = new FrinkBigDecimal();
        frinkBigDecimal2.ind = frinkBigDecimal.ind;
        frinkBigDecimal2.exp = frinkBigDecimal.exp;
        frinkBigDecimal2.form = frinkBigDecimal.form;
        frinkBigDecimal2.mant = frinkBigDecimal.mant;
        return frinkBigDecimal2;
    }

    private void checkdigits(FrinkBigDecimal frinkBigDecimal, int i) {
        if (i != 0) {
            if (this.mant.length > i && !allzero(this.mant, i)) {
                throw new ArithmeticException("Too many digits: " + toString());
            } else if (frinkBigDecimal != null && frinkBigDecimal.mant.length > i && !allzero(frinkBigDecimal.mant, i)) {
                throw new ArithmeticException("Too many digits: " + frinkBigDecimal.toString());
            }
        }
    }

    private FrinkBigDecimal round(MathContext mathContext) {
        return round(mathContext.digits, mathContext.roundingMode);
    }

    /* JADX WARN: Type inference failed for: r0v16, types: [int] */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0086, code lost:
        if (allzero(r1, r9 + 1) != false) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a3, code lost:
        if ((r8.mant[r8.mant.length - 1] % 2) != 1) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b1, code lost:
        if (allzero(r1, r9) == false) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00be, code lost:
        if (allzero(r1, r9) == false) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00cb, code lost:
        if (allzero(r1, r9) == false) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        if (r2 >= 5) goto L_0x0027;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private frink.numeric.FrinkBigDecimal round(int r9, int r10) {
        /*
            r8 = this;
            r3 = 1
            r7 = 5
            r6 = 0
            byte[] r0 = r8.mant
            int r0 = r0.length
            int r0 = r0 - r9
            if (r0 > 0) goto L_0x000b
            r0 = r8
        L_0x000a:
            return r0
        L_0x000b:
            int r1 = r8.exp
            int r0 = r0 + r1
            r8.exp = r0
            byte r0 = r8.ind
            byte[] r1 = r8.mant
            if (r9 <= 0) goto L_0x0058
            byte[] r2 = new byte[r9]
            r8.mant = r2
            byte[] r2 = r8.mant
            java.lang.System.arraycopy(r1, r6, r2, r6, r9)
            byte r2 = r1[r9]
            r5 = r3
        L_0x0022:
            r4 = 4
            if (r10 != r4) goto L_0x0069
            if (r2 < r7) goto L_0x0088
        L_0x0027:
            if (r0 == 0) goto L_0x0036
            byte r1 = r8.ind
            if (r1 != 0) goto L_0x00e8
            frink.numeric.FrinkBigDecimal r1 = frink.numeric.FrinkBigDecimal.ONE
            byte[] r1 = r1.mant
            r8.mant = r1
            byte r0 = (byte) r0
            r8.ind = r0
        L_0x0036:
            int r0 = r8.exp
            r1 = 999999999(0x3b9ac9ff, float:0.004723787)
            if (r0 <= r1) goto L_0x0116
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Exponent Overflow: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r8.exp
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0058:
            frink.numeric.FrinkBigDecimal r2 = frink.numeric.FrinkBigDecimal.ZERO
            byte[] r2 = r2.mant
            r8.mant = r2
            r8.ind = r6
            if (r9 != 0) goto L_0x0066
            byte r2 = r1[r6]
            r5 = r6
            goto L_0x0022
        L_0x0066:
            r2 = r6
            r5 = r6
            goto L_0x0022
        L_0x0069:
            r4 = 7
            if (r10 != r4) goto L_0x007a
            boolean r0 = allzero(r1, r9)
            if (r0 != 0) goto L_0x0088
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            java.lang.String r1 = "Rounding necessary"
            r0.<init>(r1)
            throw r0
        L_0x007a:
            if (r10 != r7) goto L_0x008a
            if (r2 > r7) goto L_0x0027
            if (r2 != r7) goto L_0x0088
            int r2 = r9 + 1
            boolean r1 = allzero(r1, r2)
            if (r1 == 0) goto L_0x0027
        L_0x0088:
            r0 = r6
            goto L_0x0027
        L_0x008a:
            r4 = 6
            if (r10 != r4) goto L_0x00a6
            if (r2 > r7) goto L_0x0027
            if (r2 != r7) goto L_0x0088
            int r2 = r9 + 1
            boolean r1 = allzero(r1, r2)
            if (r1 == 0) goto L_0x0027
            byte[] r1 = r8.mant
            byte[] r2 = r8.mant
            int r2 = r2.length
            int r2 = r2 - r3
            byte r1 = r1[r2]
            int r1 = r1 % 2
            if (r1 != r3) goto L_0x0088
            goto L_0x0027
        L_0x00a6:
            if (r10 != r3) goto L_0x00ab
            r0 = r6
            goto L_0x0027
        L_0x00ab:
            if (r10 != 0) goto L_0x00b5
            boolean r1 = allzero(r1, r9)
            if (r1 != 0) goto L_0x0088
            goto L_0x0027
        L_0x00b5:
            r2 = 2
            if (r10 != r2) goto L_0x00c2
            if (r0 <= 0) goto L_0x0088
            boolean r1 = allzero(r1, r9)
            if (r1 != 0) goto L_0x0088
            goto L_0x0027
        L_0x00c2:
            r2 = 3
            if (r10 != r2) goto L_0x00cf
            if (r0 >= 0) goto L_0x0088
            boolean r1 = allzero(r1, r9)
            if (r1 != 0) goto L_0x0088
            goto L_0x0027
        L_0x00cf:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Bad round value: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00e8:
            byte r1 = r8.ind
            r2 = -1
            if (r1 != r2) goto L_0x0119
            int r0 = -r0
            r4 = r0
        L_0x00ef:
            byte[] r0 = r8.mant
            byte[] r1 = r8.mant
            int r1 = r1.length
            frink.numeric.FrinkBigDecimal r2 = frink.numeric.FrinkBigDecimal.ONE
            byte[] r2 = r2.mant
            byte[] r0 = byteaddsub(r0, r1, r2, r3, r4, r5)
            int r1 = r0.length
            byte[] r2 = r8.mant
            int r2 = r2.length
            if (r1 <= r2) goto L_0x0112
            int r1 = r8.exp
            int r1 = r1 + 1
            r8.exp = r1
            byte[] r1 = r8.mant
            byte[] r2 = r8.mant
            int r2 = r2.length
            java.lang.System.arraycopy(r0, r6, r1, r6, r2)
            goto L_0x0036
        L_0x0112:
            r8.mant = r0
            goto L_0x0036
        L_0x0116:
            r0 = r8
            goto L_0x000a
        L_0x0119:
            r4 = r0
            goto L_0x00ef
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.FrinkBigDecimal.round(int, int):frink.numeric.FrinkBigDecimal");
    }

    private static final boolean allzero(byte[] bArr, int i) {
        int i2;
        if (i < 0) {
            i2 = 0;
        } else {
            i2 = i;
        }
        int length = bArr.length - 1;
        while (i2 <= length) {
            if (bArr[i2] != 0) {
                return false;
            }
            i2++;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a4, code lost:
        if (r0 <= 999999999) goto L_0x00a6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private frink.numeric.FrinkBigDecimal finish(frink.numeric.MathContext r11, boolean r12) {
        /*
            r10 = this;
            r7 = 999999999(0x3b9ac9ff, float:0.004723787)
            r6 = -999999999(0xffffffffc4653601, float:-916.8438)
            r5 = 1
            r4 = 0
            java.lang.String r8 = "Exponent Overflow: "
            int r0 = r11.digits
            if (r0 == 0) goto L_0x0018
            byte[] r0 = r10.mant
            int r0 = r0.length
            int r1 = r11.digits
            if (r0 <= r1) goto L_0x0018
            r10.round(r11)
        L_0x0018:
            if (r12 == 0) goto L_0x003c
            int r0 = r11.form
            if (r0 == 0) goto L_0x003c
            byte[] r0 = r10.mant
            int r0 = r0.length
            int r1 = r0 - r5
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0026:
            if (r0 < r5) goto L_0x002e
            byte[] r2 = r10.mant
            byte r2 = r2[r0]
            if (r2 == 0) goto L_0x0078
        L_0x002e:
            byte[] r0 = r10.mant
            int r0 = r0.length
            if (r1 >= r0) goto L_0x003c
            byte[] r0 = new byte[r1]
            byte[] r2 = r10.mant
            java.lang.System.arraycopy(r2, r4, r0, r4, r1)
            r10.mant = r0
        L_0x003c:
            r10.form = r4
            byte[] r0 = r10.mant
            int r0 = r0.length
            r1 = r4
        L_0x0042:
            if (r0 <= 0) goto L_0x00cb
            byte[] r2 = r10.mant
            byte r2 = r2[r1]
            if (r2 == 0) goto L_0x00c5
            if (r1 <= 0) goto L_0x005d
            byte[] r0 = r10.mant
            int r0 = r0.length
            int r0 = r0 - r1
            byte[] r0 = new byte[r0]
            byte[] r2 = r10.mant
            byte[] r3 = r10.mant
            int r3 = r3.length
            int r3 = r3 - r1
            java.lang.System.arraycopy(r2, r1, r0, r4, r3)
            r10.mant = r0
        L_0x005d:
            int r0 = r10.exp
            byte[] r1 = r10.mant
            int r1 = r1.length
            int r0 = r0 + r1
            if (r0 <= 0) goto L_0x0083
            int r1 = r11.digits
            if (r0 <= r1) goto L_0x0072
            int r1 = r11.digits
            if (r1 == 0) goto L_0x0072
            int r1 = r11.form
            byte r1 = (byte) r1
            r10.form = r1
        L_0x0072:
            int r1 = r0 - r5
            if (r1 > r7) goto L_0x008b
            r0 = r10
        L_0x0077:
            return r0
        L_0x0078:
            int r1 = r1 + -1
            int r2 = r10.exp
            int r2 = r2 + 1
            r10.exp = r2
            int r0 = r0 + -1
            goto L_0x0026
        L_0x0083:
            r1 = -5
            if (r0 >= r1) goto L_0x008b
            int r1 = r11.form
            byte r1 = (byte) r1
            r10.form = r1
        L_0x008b:
            int r0 = r0 + -1
            if (r0 >= r6) goto L_0x00a8
            r1 = r5
        L_0x0090:
            if (r0 <= r7) goto L_0x00aa
            r2 = r5
        L_0x0093:
            r1 = r1 | r2
            if (r1 == 0) goto L_0x00a6
            byte r1 = r10.form
            r2 = 2
            if (r1 != r2) goto L_0x00ac
            int r1 = r0 % 3
            if (r1 >= 0) goto L_0x00a1
            int r1 = r1 + 3
        L_0x00a1:
            int r0 = r0 - r1
            if (r0 < r6) goto L_0x00ac
            if (r0 > r7) goto L_0x00ac
        L_0x00a6:
            r0 = r10
            goto L_0x0077
        L_0x00a8:
            r1 = r4
            goto L_0x0090
        L_0x00aa:
            r2 = r4
            goto L_0x0093
        L_0x00ac:
            java.lang.ArithmeticException r1 = new java.lang.ArithmeticException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Exponent Overflow: "
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00c5:
            int r0 = r0 + -1
            int r1 = r1 + 1
            goto L_0x0042
        L_0x00cb:
            r10.ind = r4
            int r0 = r11.form
            if (r0 == 0) goto L_0x00db
            r10.exp = r4
        L_0x00d3:
            frink.numeric.FrinkBigDecimal r0 = frink.numeric.FrinkBigDecimal.ZERO
            byte[] r0 = r0.mant
            r10.mant = r0
            r0 = r10
            goto L_0x0077
        L_0x00db:
            int r0 = r10.exp
            if (r0 <= 0) goto L_0x00e2
            r10.exp = r4
            goto L_0x00d3
        L_0x00e2:
            int r0 = r10.exp
            if (r0 >= r6) goto L_0x00d3
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Exponent Overflow: "
            java.lang.StringBuilder r1 = r1.append(r8)
            int r2 = r10.exp
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.numeric.FrinkBigDecimal.finish(frink.numeric.MathContext, boolean):frink.numeric.FrinkBigDecimal");
    }

    public int approxLog2() {
        return ((int) (((float) ((this.exp + this.mant.length) - 1)) * 3.321928f)) + 1;
    }
}
