package frink.numeric;

import frink.errors.NotRealException;

public interface Numeric {
    double doubleValue() throws NotRealException;

    void equalsDummy();

    FrinkFloat getFrinkFloatValue(MathContext mathContext) throws NotRealException;

    void hashCodeDummy();

    boolean isBigInteger();

    boolean isComplex();

    boolean isFloat();

    boolean isFrinkInteger();

    boolean isInt();

    boolean isInterval();

    boolean isRational();

    boolean isReal();

    String toString();
}
