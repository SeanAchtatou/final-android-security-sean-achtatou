package frink.numeric;

public interface Interval<T> {
    T getLower();

    T getMain();

    T getUpper();
}
