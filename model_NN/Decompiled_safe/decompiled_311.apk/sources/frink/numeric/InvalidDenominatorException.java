package frink.numeric;

public class InvalidDenominatorException extends NumericException {
    public InvalidDenominatorException() {
        super("Attempt to divide by zero.");
    }
}
