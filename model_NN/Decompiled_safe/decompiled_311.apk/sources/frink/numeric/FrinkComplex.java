package frink.numeric;

import frink.errors.NotRealException;

public class FrinkComplex implements Numeric {
    public static final FrinkComplex I = new FrinkComplex(FrinkInt.ZERO, FrinkInt.ONE);
    public static final FrinkComplex I_2 = new FrinkComplex(FrinkInt.ZERO, FrinkRational.ONE_HALF);
    public static final FrinkComplex NEGATIVE_I = new FrinkComplex(FrinkInt.ZERO, FrinkInt.NEGATIVE_ONE);
    private FrinkReal imag;
    private FrinkReal real;

    private FrinkComplex(FrinkReal frinkReal, FrinkReal frinkReal2) {
        this.real = frinkReal;
        this.imag = frinkReal2;
    }

    public FrinkReal getReal() {
        return this.real;
    }

    public FrinkReal getImag() {
        return this.imag;
    }

    public static Numeric construct(FrinkReal frinkReal, FrinkReal frinkReal2) throws NumericException {
        if (frinkReal.isComplex() || frinkReal2.isComplex()) {
            throw new NotImplementedException("FrinkComplex.construct(real, imag) passed non-real parts.", false);
        } else if (frinkReal2.realSignum() == 0) {
            return frinkReal;
        } else {
            return new FrinkComplex(frinkReal, frinkReal2);
        }
    }

    public static Numeric construct(double d, double d2) {
        if (d2 == 0.0d) {
            return new FrinkFloat(d);
        }
        return new FrinkComplex(new FrinkFloat(d), new FrinkFloat(d2));
    }

    public double doubleValue() throws NotRealException {
        throw new NotRealException("Value is complex.");
    }

    public FrinkFloat getFrinkFloatValue(MathContext mathContext) throws NotRealException {
        throw new NotRealException("Value is complex.");
    }

    public String toString() {
        if (this.real.realSignum() == 0) {
            return this.imag.toString() + " i";
        }
        if (this.imag.realSignum() < 0) {
            return "( " + this.real.toString() + " - " + this.imag.negate().toString() + " i )";
        }
        return "( " + this.real.toString() + " + " + this.imag.toString() + " i )";
    }

    public boolean isFrinkInteger() {
        return false;
    }

    public boolean isBigInteger() {
        return false;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isRational() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isReal() {
        return false;
    }

    public boolean isComplex() {
        return true;
    }

    public boolean isInterval() {
        return false;
    }

    public FrinkComplex negate() {
        return new FrinkComplex(this.real.negate(), this.imag.negate());
    }

    public int hashCode() {
        return (this.real.hashCode() + this.imag.hashCode()) | 536870912;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof FrinkComplex) {
            FrinkComplex frinkComplex = (FrinkComplex) obj;
            if (frinkComplex.real.equals(this.real) && frinkComplex.imag.equals(this.imag)) {
                return true;
            }
        }
        return false;
    }

    public void hashCodeDummy() {
    }

    public void equalsDummy() {
    }
}
