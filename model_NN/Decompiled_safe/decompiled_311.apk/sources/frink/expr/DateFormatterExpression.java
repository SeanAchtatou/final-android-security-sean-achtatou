package frink.expr;

import frink.date.FrinkDateFormatter;

public interface DateFormatterExpression {
    public static final String TYPE = "DateFormatter";

    FrinkDateFormatter getFormatter();
}
