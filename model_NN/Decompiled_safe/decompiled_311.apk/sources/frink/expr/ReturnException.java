package frink.expr;

public class ReturnException extends ControlFlowException {
    private Expression returnValue;

    public ReturnException(Expression expression, Expression expression2) {
        super("Return statement used outside a function", expression);
        this.returnValue = expression2;
    }

    public Expression getReturnValue() {
        return this.returnValue;
    }
}
