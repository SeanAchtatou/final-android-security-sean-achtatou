package frink.expr;

import frink.errors.NotAnIntegerException;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.DefaultOrderer;
import frink.function.FunctionSource;
import frink.function.Permutation;
import frink.function.SingleArgMethod;
import frink.function.TwoArgMethod;
import frink.function.ZeroArgMethod;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.object.InvalidMethodException;
import frink.object.InvalidPropertyException;
import frink.symbolic.MatchingContext;
import java.util.Vector;

public class BasicListExpression extends NonTerminalExpression implements ListExpression, AssignableExpression, EnumeratingExpression, FrinkObject, DeepCopyable, HashingExpression {
    private static final BasicListFunctionSource methods = new BasicListFunctionSource();
    private boolean allConstant;
    private EmptyObjectContextFrame contextFrame;

    public BasicListExpression(int i) {
        super(i);
        this.allConstant = false;
        this.contextFrame = null;
    }

    public BasicListExpression(BasicListExpression basicListExpression) {
        super(basicListExpression);
        synchronized (basicListExpression) {
            this.allConstant = basicListExpression.allConstant;
            this.contextFrame = null;
        }
    }

    public BasicListExpression(BasicListExpression basicListExpression, int i) {
        super(basicListExpression, i);
        this.allConstant = basicListExpression.allConstant;
        this.contextFrame = null;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        if (this.allConstant) {
            return this;
        }
        int childCount = getChildCount();
        BasicListExpression basicListExpression = null;
        boolean z = true;
        for (int i = 0; i < childCount; i++) {
            Expression child = getChild(i);
            if (!z || !child.isConstant()) {
                if (z) {
                    basicListExpression = new BasicListExpression(childCount);
                    for (int i2 = 0; i2 < i; i2++) {
                        basicListExpression.setChild(i2, getChild(i2));
                    }
                    z = false;
                }
                basicListExpression.setChild(i, child.evaluate(environment));
            } else {
                setChild(i, child.evaluate(environment));
            }
        }
        if (!z) {
            return basicListExpression;
        }
        this.allConstant = true;
        return this;
    }

    public boolean isConstant() {
        return this.allConstant;
    }

    public void setChild(int i, Expression expression) throws CannotAssignException {
        int childCount = getChildCount();
        if (i < 0) {
            throw new CannotAssignException("Cannot assign to negative index " + i, this);
        }
        if (i >= childCount) {
            if (this.children == null) {
                this.children = new Vector(i + 1);
            }
            this.children.setSize(i + 1);
            while (childCount < i) {
                this.children.setElementAt(UndefExpression.UNDEF, childCount);
                childCount++;
            }
        }
        this.children.setElementAt(expression, i);
        this.allConstant = this.allConstant && expression.isConstant();
    }

    public void insertChild(int i, Expression expression) throws CannotAssignException {
        if (this.children == null) {
            this.children = new Vector(i + 1);
        }
        this.children.insertElementAt(expression, i);
        this.allConstant = this.allConstant && expression.isConstant();
    }

    public void removeChild(int i) {
        if (i < getChildCount()) {
            this.children.removeElementAt(i);
        }
    }

    public void assign(Expression expression, Environment environment) throws EvaluationException {
        Expression child;
        int childCount = getChildCount();
        int childCount2 = expression.getChildCount();
        for (int i = 0; i < childCount; i++) {
            Expression child2 = getChild(i);
            if (!(child2 instanceof SymbolExpression)) {
                throw new CannotAssignException("Could not assign to " + child2, this);
            }
            String name = ((SymbolExpression) child2).getName();
            if (i >= childCount2) {
                child = UndefExpression.UNDEF;
            } else {
                child = expression.getChild(i);
            }
            environment.setSymbolDefinition(name, child);
        }
    }

    public FrinkEnumeration getEnumeration(Environment environment) {
        return new ListEnumerator(this);
    }

    public Expression getProperty(String str) throws InvalidPropertyException {
        throw new InvalidPropertyException(str, this);
    }

    public Expression executeMethod(String str, Expression expression) throws InvalidMethodException, CannotAssignException {
        if (str.equals("append")) {
            setChild(getChildCount(), expression);
            return this;
        }
        throw new InvalidMethodException(str, this);
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return methods;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public boolean isA(String str) {
        return str.equals(ListExpression.TYPE);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ListExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    private static class BasicListFunctionSource extends BasicFunctionSource {
        BasicListFunctionSource() {
            super("BasicListExpression");
            addFunctionDefinition("push", new SingleArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression, Expression expression) throws EvaluationException {
                    basicListExpression.setChild(basicListExpression.getChildCount(), expression);
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("pop", new ZeroArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression) throws EvaluationException {
                    int childCount = basicListExpression.getChildCount() - 1;
                    if (childCount >= 0) {
                        Expression child = basicListExpression.getChild(childCount);
                        basicListExpression.removeChild(childCount);
                        return child;
                    }
                    environment.outputln("Tried to pop from empty list.");
                    throw new InvalidArgumentException("pop", basicListExpression);
                }
            });
            addFunctionDefinition("insert", new TwoArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression, Expression expression, Expression expression2) throws EvaluationException {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                        if (integerValue >= 0) {
                            if (integerValue >= basicListExpression.getChildCount()) {
                                basicListExpression.setChild(integerValue, expression2);
                            } else {
                                basicListExpression.insertChild(integerValue, expression2);
                            }
                            return VoidExpression.VOID;
                        }
                        throw new InvalidArgumentException("BasicListExpression.insert passed negative index: " + integerValue, basicListExpression);
                    } catch (NotAnIntegerException e) {
                        throw new InvalidArgumentException("First argument to insert[pos, value] must be an integer, value was " + environment.format(expression), expression);
                    }
                }
            });
            addFunctionDefinition("removeValue", new SingleArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression, Expression expression) throws EvaluationException {
                    int childCount = basicListExpression.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        if (ComparisonExpression.doComparison(basicListExpression.getChild(i), expression, ComparisonType.EQUALS, environment)) {
                            basicListExpression.removeChild(i);
                            return FrinkBoolean.TRUE;
                        }
                    }
                    return FrinkBoolean.FALSE;
                }
            });
            addFunctionDefinition("remove", new SingleArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression, Expression expression) throws EvaluationException {
                    try {
                        int integerValue = BuiltinFunctionSource.getIntegerValue(expression);
                        if (integerValue < 0 || integerValue >= basicListExpression.getChildCount()) {
                            throw new InvalidArgumentException("Argument to array.remove[pos] was out of range. Value was " + environment.format(expression) + ", size is " + basicListExpression.getChildCount() + ".", expression);
                        }
                        Expression child = basicListExpression.getChild(integerValue);
                        basicListExpression.removeChild(integerValue);
                        return child;
                    } catch (NotAnIntegerException e) {
                        throw new InvalidArgumentException("Argument to remove[pos] must be an integer, value was " + environment.format(expression), expression);
                    }
                }
            });
            addFunctionDefinition("removeRandom", new ZeroArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression) throws EvaluationException {
                    int childCount = basicListExpression.getChildCount();
                    if (childCount == 0) {
                        environment.outputln("Error.  Doing removeRandom from an empty list.");
                        return VoidExpression.VOID;
                    }
                    int randomInt = BuiltinFunctionSource.randomInt(childCount);
                    Expression child = basicListExpression.getChild(randomInt);
                    basicListExpression.removeChild(randomInt);
                    return child;
                }
            });
            addFunctionDefinition("shallowCopy", new ZeroArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression) throws EvaluationException {
                    return new BasicListExpression(basicListExpression);
                }
            });
            addFunctionDefinition("permute", new ZeroArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression) throws EvaluationException {
                    return new Permutation(basicListExpression);
                }
            });
            addFunctionDefinition("lexicographicPermute", new ZeroArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression) throws EvaluationException {
                    return new Permutation(basicListExpression, DefaultOrderer.INSTANCE);
                }
            });
            addFunctionDefinition("pushFirst", new SingleArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression, Expression expression) throws EvaluationException {
                    basicListExpression.insertChild(0, expression);
                    return VoidExpression.VOID;
                }
            });
            addFunctionDefinition("popFirst", new ZeroArgMethod<BasicListExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicListExpression basicListExpression) throws EvaluationException {
                    if (basicListExpression.getChildCount() == 0) {
                        environment.outputln("Tried to pop from front of empty list.");
                        throw new InvalidArgumentException("popFirst", basicListExpression);
                    }
                    Expression child = basicListExpression.getChild(0);
                    basicListExpression.removeChild(0);
                    return child;
                }
            });
        }
    }

    public String getExpressionType() {
        return ListExpression.TYPE;
    }

    public Expression deepCopy(int i) {
        return new BasicListExpression(this, i);
    }

    public int hashCode() {
        int i;
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 2069549463;
        while (i2 < childCount) {
            try {
                Expression child = getChild(i2);
                if (child instanceof HashingExpression) {
                    i = (((HashingExpression) child).hashCode() * (i2 + 1)) ^ i3;
                } else {
                    System.err.println("Attempting to use non-hashable value " + child + " in array as hashcode.");
                    i = i3;
                }
                i2++;
                i3 = i;
            } catch (InvalidChildException e) {
                System.err.println("BasicListExpression.hashCode:  Unexpected InvalidChildException:\n  " + e);
            }
        }
        return i3;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ListExpression)) {
            return false;
        }
        ListExpression listExpression = (ListExpression) obj;
        int childCount = getChildCount();
        if (childCount != listExpression.getChildCount()) {
            return false;
        }
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = getChild(i);
                Expression child2 = listExpression.getChild(i);
                if (!(child instanceof HashingExpression) || !(child2 instanceof HashingExpression)) {
                    System.err.println("Trying to use non-hashable value in array as hashcode: " + child + "," + child2);
                    return false;
                } else if (!child.equals(child2)) {
                    return false;
                } else {
                    i++;
                }
            } catch (InvalidChildException e) {
                System.err.println("BasicListExpression.equals:  Unexpected InvalidChildException\n  " + e);
            }
        }
        return true;
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }
}
