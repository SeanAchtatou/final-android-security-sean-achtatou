package frink.expr;

public class InvalidArgumentException extends EvaluationException {
    public InvalidArgumentException(String str, Expression expression) {
        super(str, expression);
    }
}
