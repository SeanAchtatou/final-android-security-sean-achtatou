package frink.expr;

public interface FrinkEnumeration {
    void dispose();

    Expression getNext(Environment environment) throws EvaluationException;
}
