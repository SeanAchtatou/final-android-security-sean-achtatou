package frink.expr;

public class StringExpressionFactory implements ExpressionFactory<String> {
    public static final StringExpressionFactory INSTANCE = new StringExpressionFactory();

    public Expression makeExpression(String str, Environment environment) {
        return new BasicStringExpression(str);
    }
}
