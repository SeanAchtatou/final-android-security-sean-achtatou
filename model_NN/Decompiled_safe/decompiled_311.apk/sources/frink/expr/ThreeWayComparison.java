package frink.expr;

import frink.errors.ConformanceException;
import frink.errors.NotComparableException;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public class ThreeWayComparison extends CachingExpression {
    public static final String TYPE = "ThreeWayComparison";

    public ThreeWayComparison(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        Expression evaluate = getChild(0).evaluate(environment);
        Expression evaluate2 = getChild(1).evaluate(environment);
        try {
            switch (compare(evaluate, evaluate2, environment)) {
                case -1:
                    return DimensionlessUnitExpression.NEGATIVE_ONE;
                case 0:
                    return DimensionlessUnitExpression.ZERO;
                case 1:
                    return DimensionlessUnitExpression.ONE;
                default:
                    throw new TypeException("Could not compare " + environment.format(evaluate) + " and " + environment.format(evaluate2), this);
            }
        } catch (NumericException e) {
            throw new TypeException("Could not compare " + environment.format(evaluate) + " and " + environment.format(evaluate2) + ":\n " + e.getMessage(), this);
        } catch (OverlapException e2) {
            throw new TypeException("Could not compare overlapping intervals: " + environment.format(evaluate) + " and " + environment.format(evaluate2) + ":\n " + e2.getMessage(), this);
        }
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ThreeWayComparison) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public static int compare(Expression expression, Expression expression2, Environment environment) throws NotComparableException, OverlapException {
        if ((expression instanceof UnitExpression) && (expression2 instanceof UnitExpression)) {
            return compareUnits(((UnitExpression) expression).getUnit(), ((UnitExpression) expression2).getUnit());
        }
        if ((expression instanceof DateExpression) && (expression2 instanceof DateExpression)) {
            try {
                return NumericMath.compare(((DateExpression) expression).getFrinkDate().getJulian(), ((DateExpression) expression2).getFrinkDate().getJulian());
            } catch (NumericException e) {
                throw new NotComparableException("ThreeWayComparison: cannot compare values.");
            }
        } else if (!(expression instanceof StringExpression) || !(expression2 instanceof StringExpression)) {
            throw new NotComparableException("ThreeWayComparison: arguments " + environment.format(expression) + " and " + environment.format(expression2) + " are not comparable.");
        } else {
            int compareTo = ((StringExpression) expression).getString().compareTo(((StringExpression) expression2).getString());
            if (compareTo < 0) {
                return -1;
            }
            if (compareTo > 0) {
                return 1;
            }
            return 0;
        }
    }

    public static int compareUnits(Unit unit, Unit unit2) throws NotComparableException, OverlapException {
        try {
            return UnitMath.compare(unit, unit2);
        } catch (ConformanceException e) {
            throw new NotComparableException("Units are not conformal in comparison:\n " + e);
        } catch (NumericException e2) {
            throw new NotComparableException("Units could not be compared:\n " + e2);
        }
    }

    public String getExpressionType() {
        return TYPE;
    }
}
