package frink.expr;

public class VariableExistsException extends EvaluationException {
    public VariableExistsException(String str, Expression expression) {
        super(str, expression);
    }
}
