package frink.expr;

public class EvaluationNumericException extends EvaluationException {
    public EvaluationNumericException(String str, Expression expression) {
        super(str, expression);
    }
}
