package frink.expr;

public class Truth {
    public static boolean isTrue(Environment environment, Expression expression) throws EvaluationException {
        if (expression instanceof SymbolExpression) {
            return false;
        }
        if (expression instanceof BooleanExpression) {
            return ((BooleanExpression) expression).getBoolean();
        }
        if (expression == UndefExpression.UNDEF) {
            return false;
        }
        if (expression == VoidExpression.VOID) {
            return false;
        }
        if (expression instanceof ListExpression) {
            return true;
        }
        if (expression instanceof StringExpression) {
            return ((StringExpression) expression).getString().length() > 0;
        }
        throw new TypeException("Unsupported condition in conditional, condition is " + environment.format(expression), expression);
    }
}
