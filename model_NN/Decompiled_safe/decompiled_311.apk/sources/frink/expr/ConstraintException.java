package frink.expr;

public class ConstraintException extends CannotAssignException {
    public ConstraintException(String str, Expression expression) {
        super(str, expression);
    }
}
