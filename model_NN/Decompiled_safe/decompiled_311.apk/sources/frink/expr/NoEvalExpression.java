package frink.expr;

import frink.symbolic.MatchingContext;

public class NoEvalExpression extends NonTerminalExpression {
    public static final String TYPE = "noEval";

    public NoEvalExpression(Expression expression) {
        super(1);
        appendChild(expression);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return getChild(0);
    }

    public boolean isConstant() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof NoEvalExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
