package frink.expr;

public interface OperatorExpression extends Expression {
    public static final int ASSOC_LEFT = -1;
    public static final int ASSOC_NONE = 0;
    public static final int ASSOC_RIGHT = 1;
    public static final int PREC_ADD = 400;
    public static final int PREC_ASSIGN = 30;
    public static final int PREC_COMPARISON = 300;
    public static final int PREC_CONVERT = 50;
    public static final int PREC_FACTORIAL = 2000;
    public static final int PREC_LOGICAL_AND = 200;
    public static final int PREC_LOGICAL_NOT = 3000;
    public static final int PREC_LOGICAL_OR = 100;
    public static final int PREC_LOWEST = Integer.MIN_VALUE;
    public static final int PREC_MULTIPLY = 500;
    public static final int PREC_POWER = 1000;
    public static final int PREC_SOLVE = 40;

    int getAssociativity();

    int getPrecedence();

    String getSymbol();
}
