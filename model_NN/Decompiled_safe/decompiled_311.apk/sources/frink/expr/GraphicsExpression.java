package frink.expr;

import frink.graphics.Drawable;

public interface GraphicsExpression extends Expression {
    Drawable getDrawable();
}
