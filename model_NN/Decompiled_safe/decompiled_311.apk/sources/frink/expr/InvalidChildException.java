package frink.expr;

public class InvalidChildException extends EvaluationException {
    public InvalidChildException(String str, Expression expression) {
        super(str, expression);
    }
}
