package frink.expr;

import frink.symbolic.MatchingContext;

public class ReturnExpression extends CachingExpression {
    public static final ReturnExpression NO_RETURN = new ReturnExpression();
    public static final String TYPE = "return";
    private ReturnException ret;

    private ReturnExpression() {
        super(1);
        appendChild(VoidExpression.VOID);
        this.ret = new ReturnException(this, VoidExpression.VOID);
    }

    public ReturnExpression(Expression expression) {
        super(1);
        appendChild(expression);
        this.ret = null;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        if (this.ret == null) {
            throw new ReturnException(this, getChild(0).evaluate(environment));
        }
        throw this.ret;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
