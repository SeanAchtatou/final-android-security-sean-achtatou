package frink.expr;

import frink.symbolic.MatchingContext;

public final class ArrayStatementList implements ListExpression {
    private Expression[] elements;

    public ArrayStatementList(StatementList statementList) {
        int childCount = statementList.getChildCount();
        this.elements = new Expression[childCount];
        int i = 0;
        while (i < childCount) {
            try {
                this.elements[i] = statementList.getChild(i);
                i++;
            } catch (InvalidChildException e) {
                System.out.println("Invalid child exception when creating ArrayStatementList");
                return;
            }
        }
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        VoidExpression voidExpression = VoidExpression.VOID;
        Expression expression = voidExpression;
        for (Expression evaluate : this.elements) {
            expression = evaluate.evaluate(environment);
        }
        return expression;
    }

    public int getChildCount() {
        return this.elements.length;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i >= 0 && i < this.elements.length) {
            return this.elements[i];
        }
        throw new InvalidChildException("ArrayStatementList asked for child " + i + ", highest index is " + (this.elements.length - 1), this);
    }

    public void setChild(int i, Expression expression) throws CannotAssignException {
        if (i < 0 || i >= this.elements.length) {
            throw new CannotAssignException("ArrayStatementList.setChild asked for child " + i + ", highest index is " + (this.elements.length - 1), this);
        }
        this.elements[i] = expression;
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return ListExpression.TYPE;
    }
}
