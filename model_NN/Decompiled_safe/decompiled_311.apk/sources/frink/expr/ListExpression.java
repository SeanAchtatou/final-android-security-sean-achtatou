package frink.expr;

public interface ListExpression extends Expression {
    public static final String TYPE = "array";

    void setChild(int i, Expression expression) throws CannotAssignException;
}
