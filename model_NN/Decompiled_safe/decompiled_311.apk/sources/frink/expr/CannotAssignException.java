package frink.expr;

public class CannotAssignException extends EvaluationException {
    public CannotAssignException(String str, Expression expression) {
        super(str, expression);
    }
}
