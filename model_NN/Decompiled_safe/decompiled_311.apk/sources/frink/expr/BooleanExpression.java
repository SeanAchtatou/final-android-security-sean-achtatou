package frink.expr;

public interface BooleanExpression {
    boolean getBoolean();
}
