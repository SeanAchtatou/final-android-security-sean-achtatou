package frink.expr;

public interface HashingExpression extends Expression {
    boolean equals(Object obj);

    int hashCode();

    void iHaveOverriddenHashCodeAndEqualsDummyMethod();
}
