package frink.expr;

import frink.units.Source;
import java.util.Enumeration;
import java.util.Hashtable;

public class BuiltinConstraintSource implements Source<Constraint> {
    public static final BuiltinConstraintSource INSTANCE = new BuiltinConstraintSource();
    private static Hashtable<String, Constraint> constraints = new Hashtable<>();

    static {
        constraints.put(ListExpression.TYPE, new ArrayConstraint(ListExpression.TYPE));
        constraints.put("boolean", new BooleanConstraint("boolean"));
        constraints.put("date", new DateConstraint("date"));
        constraints.put(DictionaryExpression.TYPE, new DictionaryConstraint(DictionaryExpression.TYPE));
        constraints.put(SetExpression.TYPE, new SetConstraint(SetExpression.TYPE));
        constraints.put("regexp", new RegexpConstraint("regexp"));
        constraints.put("string", new StringConstraint("string"));
        constraints.put("subst", new SubstitutionConstraint("subst"));
        constraints.put("unit", new UnitConstraint("unit"));
        constraints.put("graphics", new GraphicsConstraint("graphics"));
    }

    public String getName() {
        return "BuiltinConstraintSource";
    }

    public Constraint get(String str) {
        return constraints.get(str);
    }

    public Enumeration<String> getNames() {
        return constraints.keys();
    }

    private static abstract class TypeConstraint implements Constraint {
        private String name;

        public abstract void checkConstraint(Expression expression) throws ConstraintException;

        TypeConstraint(String str) {
            this.name = str;
        }

        public String getDescription() {
            return "Object must be of the built-in type " + this.name;
        }

        /* access modifiers changed from: protected */
        public void throwException(Expression expression) throws ConstraintException {
            throw new ConstraintException("Constraint not met--item must be of the built-in type " + this.name, expression);
        }
    }

    private static class ArrayConstraint extends TypeConstraint {
        private ArrayConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof ListExpression;
        }
    }

    private static class StringConstraint extends TypeConstraint {
        private StringConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof StringExpression;
        }
    }

    private static class DictionaryConstraint extends TypeConstraint {
        private DictionaryConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof DictionaryExpression;
        }
    }

    private static class SetConstraint extends TypeConstraint {
        private SetConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof SetExpression;
        }
    }

    private static class BooleanConstraint extends TypeConstraint {
        private BooleanConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof BooleanExpression;
        }
    }

    private static class UnitConstraint extends TypeConstraint {
        private UnitConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof UnitExpression;
        }
    }

    private static class DateConstraint extends TypeConstraint {
        private DateConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof DateExpression;
        }
    }

    private static class RegexpConstraint extends TypeConstraint {
        private RegexpConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof RegexpExpression;
        }
    }

    private static class SubstitutionConstraint extends TypeConstraint {
        private SubstitutionConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof SubstitutionExpression;
        }
    }

    private static class GraphicsConstraint extends TypeConstraint {
        private GraphicsConstraint(String str) {
            super(str);
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throwException(expression);
            }
        }

        public boolean meetsConstraint(Expression expression) {
            return expression instanceof GraphicsExpression;
        }
    }
}
