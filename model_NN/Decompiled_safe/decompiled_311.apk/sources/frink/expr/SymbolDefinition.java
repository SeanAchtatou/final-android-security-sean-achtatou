package frink.expr;

public interface SymbolDefinition {
    Expression getValue();

    boolean isConstant();

    void setValue(Expression expression) throws CannotAssignException;
}
