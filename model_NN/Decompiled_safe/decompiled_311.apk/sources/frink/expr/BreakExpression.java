package frink.expr;

import frink.symbolic.MatchingContext;

public class BreakExpression extends TerminalExpression {
    public static final String TYPE = "break";
    private BreakException be;

    public BreakExpression() {
        this.be = new BreakException(this);
    }

    public BreakExpression(String str) {
        this.be = new BreakException(str, this);
    }

    public Expression evaluate(Environment environment) throws BreakException {
        throw this.be;
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
