package frink.expr;

public interface Constraint {
    void checkConstraint(Expression expression) throws ConstraintException;

    String getDescription();

    boolean meetsConstraint(Expression expression);
}
