package frink.expr;

public interface ExpressionType {
    String getName();
}
