package frink.expr;

public class ListEnumerator implements FrinkEnumeration {
    private int index = 0;
    private ListExpression list;

    public ListEnumerator(ListExpression listExpression) {
        this.list = listExpression;
    }

    public Expression getNext(Environment environment) throws EvaluationException {
        if (this.list == null) {
            return null;
        }
        if (this.index >= this.list.getChildCount()) {
            return null;
        }
        ListExpression listExpression = this.list;
        int i = this.index;
        this.index = i + 1;
        return listExpression.getChild(i);
    }

    public void dispose() {
        this.list = null;
    }
}
