package frink.expr;

import frink.date.DateMath;
import frink.date.FrinkDate;
import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public class RangeExpression extends NonTerminalExpression implements EnumeratingExpression {
    public static final String TYPE = "range";

    public RangeExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    public RangeExpression(Expression expression, Expression expression2, Expression expression3) {
        super(3);
        appendChild(expression);
        appendChild(expression2);
        appendChild(expression3);
    }

    public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
        return evaluate(environment).getEnumeration(environment);
    }

    public EnumeratingExpression evaluate(Environment environment) throws EvaluationException {
        Expression evaluate = getChild(0).evaluate(environment);
        Expression evaluate2 = getChild(1).evaluate(environment);
        if ((evaluate instanceof UnitExpression) && (evaluate2 instanceof UnitExpression)) {
            return evaluateUnit(evaluate, evaluate2, environment);
        }
        if ((evaluate instanceof BooleanExpression) && (evaluate2 instanceof BooleanExpression)) {
            return new BooleanRangeEnumerator(((BooleanExpression) evaluate).getBoolean(), ((BooleanExpression) evaluate2).getBoolean(), environment);
        }
        if ((evaluate instanceof DateExpression) && (evaluate2 instanceof DateExpression)) {
            return evaluateDate(((DateExpression) evaluate).getFrinkDate(), ((DateExpression) evaluate2).getFrinkDate(), environment);
        }
        throw new InvalidArgumentException("Bounds in range expression are of unsupported types: (" + environment.format(evaluate) + ", " + environment.format(evaluate2) + ")", this);
    }

    public EnumeratingExpression evaluateDate(FrinkDate frinkDate, FrinkDate frinkDate2, Environment environment) throws EvaluationException {
        if (getChildCount() < 3) {
            throw new InvalidArgumentException("Range expression: if boundaries are dates, step must be specified.", this);
        }
        Expression evaluate = getChild(2).evaluate(environment);
        if (!(evaluate instanceof UnitExpression)) {
            throw new InvalidArgumentException("Step in range expression is not a unit.", evaluate);
        }
        try {
            return new DateRangeEnumerator(frinkDate, frinkDate2, ((UnitExpression) evaluate).getUnit());
        } catch (NumericException e) {
            throw new InvalidArgumentException("Expressions not valid in range operator: " + e, this);
        } catch (OverlapException e2) {
            throw new InvalidArgumentException("Expressions not valid in range operator: " + e2, this);
        }
    }

    private EnumeratingExpression evaluateUnit(Expression expression, Expression expression2, Environment environment) throws EvaluationException {
        Unit unit;
        int i;
        boolean z = false;
        Unit unit2 = ((UnitExpression) expression).getUnit();
        Unit unit3 = ((UnitExpression) expression2).getUnit();
        if (getChildCount() == 3) {
            Expression evaluate = getChild(2).evaluate(environment);
            if (!(evaluate instanceof UnitExpression)) {
                throw new InvalidArgumentException("Step in range expression is not a unit.", evaluate);
            }
            unit = ((UnitExpression) evaluate).getUnit();
            z = true;
        } else {
            unit = DimensionlessUnitExpression.ONE;
        }
        try {
            int integerValue = UnitMath.getIntegerValue(unit2);
            int integerValue2 = UnitMath.getIntegerValue(unit3);
            if (z) {
                i = UnitMath.getIntegerValue(unit);
            } else {
                i = 1;
            }
            return new IntegerRangeEnumerator(integerValue, integerValue2, i);
        } catch (NotAnIntegerException e) {
            try {
                return new RangeEnumerator(unit2, unit3, unit);
            } catch (NumericException e2) {
                throw new InvalidArgumentException("Expressions not valid in range operator: " + e2, this);
            } catch (OverlapException e3) {
                throw new InvalidArgumentException("Expressions not valid in range operator: " + e3, this);
            }
        }
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof RangeExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    private static class RangeEnumerator extends TerminalExpression implements EnumeratingExpression, FrinkEnumeration {
        public static final String TYPE = "RangeEnumerator";
        private boolean countsUp;
        private Unit current;
        private Unit step;
        private Unit to;

        public String getExpressionType() {
            return TYPE;
        }

        public RangeEnumerator(Unit unit, Unit unit2, Unit unit3) throws NumericException, OverlapException {
            this.current = unit;
            this.to = unit2;
            this.step = unit3;
            this.countsUp = UnitMath.isPositive(unit3);
        }

        public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
            return this;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            try {
                if (this.countsUp) {
                    if (UnitMath.compare(this.current, this.to) > 0) {
                        return null;
                    }
                } else if (UnitMath.compare(this.current, this.to) < 0) {
                    return null;
                }
                UnitExpression construct = BasicUnitExpression.construct(this.current);
                this.current = UnitMath.add(this.current, this.step);
                return construct;
            } catch (ConformanceException e) {
                if (!UnitMath.areConformal(this.current, this.to)) {
                    throw new EvaluationConformanceException("Expressions at endpoints not conformal in range operator.", this);
                }
                throw new EvaluationConformanceException("Range operator endpoints are conformal, but require you to specify an conformal step.", this);
            } catch (NumericException e2) {
                throw new InvalidArgumentException("Expressions not valid in range operator: " + e2, this);
            } catch (OverlapException e3) {
                throw new InvalidArgumentException("Expressions not valid in range operator: " + e3, this);
            }
        }

        public void dispose() {
            this.current = null;
            this.to = null;
            this.step = null;
        }

        public Expression evaluate(Environment environment) {
            System.out.println("Evaluating RangeEnumerator... this is probably unexpected.");
            System.out.println("RangeEnumerator should be used in a context that expects an enumerating expression.");
            return this;
        }

        public boolean isConstant() {
            return false;
        }

        public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
            return this == expression;
        }
    }

    private static class IntegerRangeEnumerator extends TerminalExpression implements EnumeratingExpression, FrinkEnumeration {
        public static final String TYPE = "IntegerRangeEnumerator";
        private boolean countsUp;
        private int current;
        private int step;
        private int to;

        public String getExpressionType() {
            return TYPE;
        }

        public IntegerRangeEnumerator(int i, int i2, int i3) {
            this.current = i;
            this.to = i2;
            this.step = i3;
            this.countsUp = i3 > 0;
        }

        public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
            return this;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.countsUp) {
                if (this.current > this.to) {
                    return null;
                }
            } else if (this.current < this.to) {
                return null;
            }
            DimensionlessUnitExpression construct = DimensionlessUnitExpression.construct(this.current);
            this.current += this.step;
            return construct;
        }

        public void dispose() {
        }

        public Expression evaluate(Environment environment) {
            System.out.println("Evaluating IntegerRangeEnumerator... this is probably unexpected.");
            System.out.println("IntegerRangeEnumerator should be used in a context that expects an enumerating expression.");
            return this;
        }

        public boolean isConstant() {
            return false;
        }

        public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
            return this == expression;
        }
    }

    private static class DateRangeEnumerator extends TerminalExpression implements EnumeratingExpression, FrinkEnumeration {
        public static final String TYPE = "DateRangeEnumerator";
        private boolean countsUp;
        private FrinkDate current;
        private Unit step;
        private Numeric to;

        public String getExpressionType() {
            return TYPE;
        }

        public DateRangeEnumerator(FrinkDate frinkDate, FrinkDate frinkDate2, Unit unit) throws NumericException, OverlapException {
            this.current = frinkDate;
            this.to = frinkDate2.getJulian();
            this.step = unit;
            this.countsUp = UnitMath.isPositive(unit);
        }

        public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
            return this;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            try {
                if (this.countsUp) {
                    if (NumericMath.compare(this.current.getJulian(), this.to) > 0) {
                        return null;
                    }
                } else if (NumericMath.compare(this.current.getJulian(), this.to) < 0) {
                    return null;
                }
                BasicDateExpression basicDateExpression = new BasicDateExpression(this.current);
                this.current = DateMath.add(this.current, this.step, environment.getUnitManager());
                return basicDateExpression;
            } catch (ConformanceException e) {
                throw new EvaluationConformanceException("Expressions not conformal in range operator.", this);
            } catch (NumericException e2) {
                throw new InvalidArgumentException("Numeric error in range operator:\n " + e2, this);
            } catch (OverlapException e3) {
                throw new InvalidArgumentException("Invalid arguments to range operator:\n " + e3, this);
            }
        }

        public void dispose() {
            this.current = null;
            this.to = null;
            this.step = null;
        }

        public Expression evaluate(Environment environment) {
            System.out.println("Evaluating DateRangeEnumerator... this is probably unexpected.");
            System.out.println("DateRangeEnumerator should be used in a context that expects an enumerating expression.");
            return this;
        }

        public boolean isConstant() {
            return false;
        }

        public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
            return this == expression;
        }
    }

    public String getExpressionType() {
        return TYPE;
    }

    public static Expression construct(Expression expression, Expression expression2, Expression expression3, Environment environment) {
        if (expression3 == null) {
            return new RangeExpression(expression, expression2);
        }
        return new RangeExpression(expression, expression2, expression3);
    }

    private static class BooleanRangeEnumerator extends TerminalExpression implements EnumeratingExpression, FrinkEnumeration {
        public static final String TYPE = "BooleanRangeEnumerator";
        private int currentState;
        private int numStates;
        private boolean[] states;

        public String getExpressionType() {
            return TYPE;
        }

        public BooleanRangeEnumerator(boolean z, boolean z2, Environment environment) {
            if (z == z2) {
                this.numStates = 1;
            } else {
                this.numStates = 2;
            }
            this.states = new boolean[this.numStates];
            this.states[0] = z;
            if (this.numStates == 2) {
                this.states[1] = z2;
            }
            this.currentState = 0;
        }

        public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
            return this;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.currentState == this.numStates) {
                return null;
            }
            boolean[] zArr = this.states;
            int i = this.currentState;
            this.currentState = i + 1;
            return FrinkBoolean.create(zArr[i]);
        }

        public void dispose() {
            this.states = null;
        }

        public Expression evaluate(Environment environment) {
            System.out.println("Evaluating BooleanRangeEnumerator... this is probably unexpected.");
            System.out.println("BooleanRangeEnumerator should be used in a context that expects an enumerating expression.");
            return this;
        }

        public boolean isConstant() {
            return false;
        }

        public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
            return this == expression;
        }
    }
}
