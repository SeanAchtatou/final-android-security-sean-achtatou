package frink.expr;

import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Substitution;

public interface SubstitutionExpression {
    public static final String TYPE = "Substitution";

    int getNumSubs();

    Pattern getPattern();

    Substitution getSubstitution();

    boolean isRightExpression();
}
