package frink.expr;

import frink.date.DateInterval;
import frink.date.FrinkDate;
import frink.errors.NotAnIntegerException;
import frink.function.BuiltinFunctionSource;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.RealInterval;
import frink.object.ObjectSource;
import frink.units.Unit;
import frink.units.UnitMath;

public class BuiltinObjectSource implements ObjectSource {
    public static final BuiltinObjectSource INSTANCE = new BuiltinObjectSource();

    private BuiltinObjectSource() {
    }

    public String getName() {
        return "BuiltinObjectSource";
    }

    public Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException {
        if (str.equals(DictionaryExpression.TYPE)) {
            return constructDict(listExpression, environment);
        }
        if (str.equals(SetExpression.TYPE)) {
            return constructSet(listExpression, environment);
        }
        if (str.equals(ListExpression.TYPE)) {
            return constructArray(listExpression, environment);
        }
        if (str.equals("interval")) {
            return constructInterval(listExpression, environment);
        }
        if (str.equals(RangeExpression.TYPE)) {
            return constructRange(listExpression, environment);
        }
        return null;
    }

    public Expression constructDict(ListExpression listExpression, Environment environment) {
        return new BasicDictionaryExpression();
    }

    public Expression constructSet(ListExpression listExpression, Environment environment) throws EvaluationException {
        if (listExpression == null || listExpression.getChildCount() < 1) {
            return new BasicSetExpression();
        }
        return SetUtils.makeSet(listExpression, environment);
    }

    public Expression constructArray(ListExpression listExpression, Environment environment) throws EvaluationException {
        if (listExpression == null || listExpression.getChildCount() < 1) {
            return new BasicListExpression(0);
        }
        Expression child = listExpression.getChild(0);
        if (child instanceof ListExpression) {
            Expression expression = null;
            if (listExpression.getChildCount() >= 2) {
                expression = listExpression.getChild(1);
            }
            return ArrayUtils.makeArray((ListExpression) child, expression, environment);
        }
        try {
            int integerValue = BuiltinFunctionSource.getIntegerValue(child);
            BasicListExpression basicListExpression = new BasicListExpression(integerValue);
            if (listExpression.getChildCount() >= 2) {
                Expression child2 = listExpression.getChild(1);
                for (int i = 0; i < integerValue; i++) {
                    basicListExpression.setChild(i, child2);
                }
            }
            return basicListExpression;
        } catch (NotAnIntegerException e) {
            throw new InvalidArgumentException("Arguments to new array[" + environment.format(listExpression.getChild(0)) + "] are invalid.  First argument must be an integer.", listExpression.getChild(0));
        }
    }

    public Expression constructInterval(ListExpression listExpression, Environment environment) throws EvaluationException {
        Expression child = listExpression.getChild(0);
        if (child instanceof UnitExpression) {
            return constructUnitInterval(listExpression, environment);
        }
        if (child instanceof DateExpression) {
            return constructDateInterval(listExpression, environment);
        }
        throw new InvalidArgumentException("BuiltinObjectSource: unsupported arguments.", listExpression);
    }

    public Expression constructUnitInterval(ListExpression listExpression, Environment environment) throws EvaluationException {
        Expression child;
        Expression expression;
        Numeric construct;
        if (listExpression == null) {
            throw new InvalidArgumentException("Constructor for interval must take 2 or 3 arguments.", listExpression);
        }
        int childCount = listExpression.getChildCount();
        if (childCount < 2 || childCount > 3) {
            throw new InvalidArgumentException("Constructor for interval must take 2 or 3 arguments.", listExpression);
        }
        Expression child2 = listExpression.getChild(0);
        if (child2 instanceof UnitExpression) {
            Unit unit = ((UnitExpression) child2).getUnit();
            if (childCount == 2) {
                child = listExpression.getChild(1);
            } else {
                child = listExpression.getChild(2);
            }
            if (child instanceof UnitExpression) {
                Unit unit2 = ((UnitExpression) child).getUnit();
                if (childCount == 3) {
                    expression = listExpression.getChild(1);
                } else {
                    expression = child;
                }
                if (expression instanceof UnitExpression) {
                    Unit unit3 = ((UnitExpression) expression).getUnit();
                    if (UnitMath.areConformal(unit, unit2)) {
                        if (childCount == 2) {
                            try {
                                construct = RealInterval.construct(unit.getScale(), unit2.getScale());
                            } catch (NumericException e) {
                                throw new EvaluationNumericException("Non-real arguments to constructInterval:\n " + e.getMessage(), listExpression);
                            }
                        } else if (UnitMath.areConformal(unit, unit3)) {
                            construct = RealInterval.construct(unit.getScale(), unit3.getScale(), unit2.getScale());
                        } else {
                            throw new InvalidArgumentException("Arguments to new interval must be real, conformal units.", listExpression);
                        }
                        if (construct != null) {
                            return new CondensedUnitExpression(construct, unit.getDimensionList());
                        }
                        throw new InvalidArgumentException("Invalid arguments to constructInterval", listExpression);
                    }
                    throw new InvalidArgumentException("Arguments to new interval must be real, conformal units.", listExpression);
                }
                throw new InvalidArgumentException("Arguments to new interval must be real, conformal units.", expression);
            }
            throw new InvalidArgumentException("Arguments to new interval must be real, conformal units.", child);
        }
        throw new InvalidArgumentException("Arguments to new interval must be real, conformal units.", child2);
    }

    public Expression constructDateInterval(ListExpression listExpression, Environment environment) throws EvaluationException {
        Expression child;
        if (listExpression == null) {
            throw new InvalidArgumentException("Constructor for interval must take 2 or 3 arguments.", listExpression);
        }
        int childCount = listExpression.getChildCount();
        if (childCount < 2 || childCount > 3) {
            throw new InvalidArgumentException("Constructor for interval must take 2 or 3 arguments.", listExpression);
        }
        Expression child2 = listExpression.getChild(0);
        if (child2 instanceof DateExpression) {
            FrinkDate frinkDate = ((DateExpression) child2).getFrinkDate();
            if (childCount == 2) {
                child = listExpression.getChild(1);
            } else {
                child = listExpression.getChild(2);
            }
            if (child instanceof DateExpression) {
                FrinkDate frinkDate2 = ((DateExpression) child).getFrinkDate();
                if (childCount == 3) {
                    child = listExpression.getChild(1);
                }
                if (child instanceof DateExpression) {
                    FrinkDate frinkDate3 = ((DateExpression) child).getFrinkDate();
                    if (childCount != 2) {
                        return new BasicDateExpression(DateInterval.construct(frinkDate.getJulian(), frinkDate3.getJulian(), frinkDate2.getJulian()));
                    }
                    try {
                        return new BasicDateExpression(DateInterval.construct(frinkDate.getJulian(), frinkDate2.getJulian()));
                    } catch (NumericException e) {
                        throw new EvaluationNumericException("Arguments to new interval must be dates:\n " + e, child);
                    }
                } else {
                    throw new InvalidArgumentException("Arguments to new interval must be dates.", child);
                }
            } else {
                throw new InvalidArgumentException("Arguments to new interval must be dates.", child);
            }
        } else {
            throw new InvalidArgumentException("Arguments to new interval must be dates.", child2);
        }
    }

    public static Expression constructRange(ListExpression listExpression, Environment environment) throws EvaluationException {
        Expression expression;
        if (listExpression == null) {
            throw new InvalidArgumentException("Constructor for range must take 2 or 3 arguments.", listExpression);
        }
        int childCount = listExpression.getChildCount();
        if (childCount < 2 || childCount > 3) {
            throw new InvalidArgumentException("Constructor for range must take 2 or 3 arguments.", listExpression);
        }
        Expression child = listExpression.getChild(0);
        Expression child2 = listExpression.getChild(1);
        if (childCount >= 3) {
            expression = listExpression.getChild(2);
        } else {
            expression = null;
        }
        return RangeExpression.construct(child, child2, expression, environment);
    }
}
