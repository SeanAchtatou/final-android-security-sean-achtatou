package frink.expr;

public interface SetExpression extends Expression {
    public static final String TYPE = "set";

    void clear();

    boolean contains(HashingExpression hashingExpression);

    int getSize();

    EnumeratingExpression getValues();

    void put(HashingExpression hashingExpression);

    void remove(HashingExpression hashingExpression);
}
