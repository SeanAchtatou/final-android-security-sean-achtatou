package frink.expr;

import frink.numeric.FrinkBigInteger;
import frink.numeric.FrinkComplex;
import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.Numeric;
import frink.symbolic.MatchingContext;
import frink.units.DimensionList;
import frink.units.DimensionlessList;
import frink.units.Unit;
import frink.units.UnitMath;
import java.math.BigInteger;

public final class DimensionlessUnitExpression extends TerminalExpression implements UnitExpression, Unit {
    public static final DimensionlessUnitExpression I = new DimensionlessUnitExpression(FrinkComplex.I);
    public static final DimensionlessUnitExpression NEGATIVE_ONE = intCache[99];
    public static final DimensionlessUnitExpression ONE = intCache[101];
    public static final DimensionlessUnitExpression ONE_HALF = new DimensionlessUnitExpression(FrinkRational.ONE_HALF);
    public static final DimensionlessUnitExpression THREE = intCache[103];
    public static final DimensionlessUnitExpression TWO = intCache[102];
    public static final DimensionlessUnitExpression ZERO = intCache[100];
    private static final DimensionlessUnitExpression[] intCache = new DimensionlessUnitExpression[301];
    private Numeric scale;

    static {
        for (int i = -100; i <= 200; i++) {
            intCache[i - -100] = new DimensionlessUnitExpression(i);
        }
    }

    public static DimensionlessUnitExpression construct(int i) {
        if (i > 200 || i < -100) {
            return new DimensionlessUnitExpression(i);
        }
        return intCache[i - -100];
    }

    public static DimensionlessUnitExpression construct(Integer num) {
        return construct(num.intValue());
    }

    private DimensionlessUnitExpression(int i) {
        this.scale = FrinkInt.construct(i);
    }

    private DimensionlessUnitExpression(Integer num) {
        this.scale = FrinkInt.construct(num.intValue());
    }

    public static DimensionlessUnitExpression construct(Long l) {
        long longValue = l.longValue();
        int i = (int) longValue;
        if (((long) i) == longValue) {
            return construct(i);
        }
        return new DimensionlessUnitExpression(FrinkInteger.construct(longValue));
    }

    private DimensionlessUnitExpression(Long l) {
        this.scale = FrinkInteger.construct(l.longValue());
    }

    public static DimensionlessUnitExpression construct(BigInteger bigInteger) {
        if (bigInteger.compareTo(FrinkBigInteger.MAXINT) > 0 || bigInteger.compareTo(FrinkBigInteger.MININT) < 0) {
            return new DimensionlessUnitExpression(bigInteger);
        }
        return construct(bigInteger.intValue());
    }

    private DimensionlessUnitExpression(BigInteger bigInteger) {
        this.scale = FrinkInteger.construct(bigInteger);
    }

    private DimensionlessUnitExpression(double d) {
        this.scale = new FrinkFloat(d);
    }

    public static DimensionlessUnitExpression construct(double d) {
        return new DimensionlessUnitExpression(d);
    }

    public static DimensionlessUnitExpression construct(Double d) {
        return new DimensionlessUnitExpression(d);
    }

    private DimensionlessUnitExpression(Double d) {
        this.scale = new FrinkFloat(d.doubleValue());
    }

    public static DimensionlessUnitExpression construct(Float f) {
        return new DimensionlessUnitExpression(f);
    }

    private DimensionlessUnitExpression(Float f) {
        this.scale = new FrinkFloat((double) f.floatValue());
    }

    private DimensionlessUnitExpression(FrinkFloat frinkFloat) {
        this.scale = frinkFloat;
    }

    public static DimensionlessUnitExpression construct(Numeric numeric) {
        if (numeric.isInt()) {
            return construct(((FrinkInt) numeric).getInt());
        }
        return new DimensionlessUnitExpression(numeric);
    }

    private DimensionlessUnitExpression(Numeric numeric) {
        this.scale = numeric;
    }

    public final boolean isConstant() {
        return true;
    }

    public final Expression evaluate(Environment environment) {
        return this;
    }

    public final Numeric getScale() {
        return this.scale;
    }

    public final DimensionList getDimensionList() {
        return DimensionlessList.INSTANCE;
    }

    public final Unit getUnit() {
        return this;
    }

    public int hashCode() {
        return this.scale.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof UnitExpression) {
            Unit unit = ((UnitExpression) obj).getUnit();
            if (!unit.getScale().equals(this.scale) || !UnitMath.areConformal(this, unit)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return equals(expression);
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }

    public String getExpressionType() {
        return UnitExpression.TYPE;
    }
}
