package frink.expr;

public interface EnumeratingExpression extends Expression {
    public static final String TYPE = "Enumeration";

    FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException;
}
