package frink.expr;

public class EvaluationConformanceException extends EvaluationException {
    public EvaluationConformanceException(String str, Expression expression) {
        super(str, expression);
    }
}
