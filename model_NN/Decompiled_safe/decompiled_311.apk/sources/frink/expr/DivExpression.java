package frink.expr;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public class DivExpression extends CachingExpression {
    public static final String TYPE = "div";

    public DivExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        Expression evaluate = getChild(0).evaluate(environment);
        Expression evaluate2 = getChild(1).evaluate(environment);
        if ((evaluate instanceof UnitExpression) && (evaluate2 instanceof UnitExpression)) {
            Unit unit = ((UnitExpression) evaluate).getUnit();
            Unit unit2 = ((UnitExpression) evaluate2).getUnit();
            if (UnitMath.areConformal(unit, unit2)) {
                try {
                    return BasicUnitExpression.construct(UnitMath.floor(UnitMath.divide(unit, unit2)));
                } catch (ConformanceException e) {
                } catch (NumericException e2) {
                    throw new InvalidArgumentException("Error in div: " + e2, this);
                }
            }
        }
        throw new InvalidArgumentException("Arguments to div must be conformal units.", this);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof DivExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
