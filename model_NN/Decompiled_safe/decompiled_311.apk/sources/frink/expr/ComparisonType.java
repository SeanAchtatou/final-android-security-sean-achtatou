package frink.expr;

import java.util.Enumeration;
import java.util.Hashtable;

public class ComparisonType {
    public static final ComparisonType CERTAINLY_EQUALS = new ComparisonType(false, true, false, "CEQ", true, false);
    public static final ComparisonType CERTAINLY_GREATER = new ComparisonType(false, false, true, "CGT", true, false);
    public static final ComparisonType CERTAINLY_GREATER_OR_EQUAL = new ComparisonType(false, true, true, "CGE", true, false);
    public static final ComparisonType CERTAINLY_LESS = new ComparisonType(true, false, false, "CLT", true, false);
    public static final ComparisonType CERTAINLY_LESS_OR_EQUAL = new ComparisonType(true, true, false, "CLE", true, false);
    public static final ComparisonType CERTAINLY_NOT_EQUALS = new ComparisonType(true, false, true, "CNE", true, false);
    public static final ComparisonType EQUALS = new ComparisonType(false, true, false, "==");
    public static final ComparisonType GREATER = new ComparisonType(false, false, true, ">");
    public static final ComparisonType GREATER_OR_EQUAL = new ComparisonType(false, true, true, ">=");
    public static final ComparisonType LESS = new ComparisonType(true, false, false, "<");
    public static final ComparisonType LESS_OR_EQUAL = new ComparisonType(true, true, false, "<=");
    public static final ComparisonType NOT_EQUALS = new ComparisonType(true, false, true, "!=");
    public static final ComparisonType POSSIBLY_EQUALS = new ComparisonType(false, true, false, "PEQ", true, true);
    public static final ComparisonType POSSIBLY_GREATER = new ComparisonType(false, false, true, "PGT", true, true);
    public static final ComparisonType POSSIBLY_GREATER_OR_EQUAL = new ComparisonType(false, true, true, "PGE", true, true);
    public static final ComparisonType POSSIBLY_LESS = new ComparisonType(true, false, false, "PLT", true, true);
    public static final ComparisonType POSSIBLY_LESS_OR_EQUAL = new ComparisonType(true, true, false, "PLE", true, true);
    public static final ComparisonType POSSIBLY_NOT_EQUALS = new ComparisonType(true, false, true, "PNE", true, true);
    private static final Hashtable<String, ComparisonType> opTable = new Hashtable<>();
    private boolean equalOkay;
    private boolean greaterOkay;
    private boolean lessOkay;
    private boolean overlapDefined;
    private boolean overlapValue;
    private String representation;

    private ComparisonType(boolean z, boolean z2, boolean z3, String str) {
        this(z, z2, z3, str, false, false);
    }

    private ComparisonType(boolean z, boolean z2, boolean z3, String str, boolean z4, boolean z5) {
        this.lessOkay = z;
        this.equalOkay = z2;
        this.greaterOkay = z3;
        this.representation = str;
        this.overlapDefined = z4;
        this.overlapValue = z5;
        opTable.put(str, this);
    }

    public boolean comparesCorrectly(int i) {
        switch (i) {
            case -1:
                return this.lessOkay;
            case 0:
                return this.equalOkay;
            case 1:
                return this.greaterOkay;
            default:
                System.out.println("ComparisonType: bad constant " + i);
                return false;
        }
    }

    public String getRepresentation() {
        return this.representation;
    }

    public boolean isOverlapDefined() {
        return this.overlapDefined;
    }

    public boolean getOverlapValue() {
        return this.overlapValue;
    }

    public static ComparisonType getComparisonType(String str) {
        return opTable.get(str);
    }

    public static Enumeration<String> getKeys() {
        return opTable.keys();
    }
}
