package frink.expr;

import frink.numeric.NumericMath;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public final class BasicUnitExpression extends TerminalExpression implements UnitExpression {
    private Unit unit;

    private BasicUnitExpression(Unit unit2) {
        this.unit = unit2;
    }

    public static UnitExpression construct(Unit unit2) {
        if (UnitMath.isDimensionless(unit2)) {
            return DimensionlessUnitExpression.construct(unit2.getScale());
        }
        return new BasicUnitExpression(unit2);
    }

    public final Expression evaluate(Environment environment) {
        return this;
    }

    public final boolean isConstant() {
        return true;
    }

    public final Unit getUnit() {
        return this.unit;
    }

    public int hashCode() {
        return this.unit.getScale().hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof UnitExpression) {
            Unit unit2 = ((UnitExpression) obj).getUnit();
            if (UnitMath.areConformal(this.unit, unit2)) {
                try {
                    return NumericMath.compare(unit2.getScale(), this.unit.getScale()) == 0;
                } catch (Exception e) {
                    return unit2.getScale().equals(this.unit.getScale());
                }
            }
        }
        return false;
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return equals(expression);
    }

    public String getExpressionType() {
        return UnitExpression.TYPE;
    }
}
