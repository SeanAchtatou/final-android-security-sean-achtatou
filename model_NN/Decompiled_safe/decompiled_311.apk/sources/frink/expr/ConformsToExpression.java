package frink.expr;

import frink.symbolic.MatchingContext;
import frink.units.DimensionList;
import frink.units.DimensionListMath;
import frink.units.UnitMath;

public class ConformsToExpression extends CachingExpression {
    public static final String TYPE = "conforms";

    public ConformsToExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        DimensionList dimensionList;
        Expression evaluate = getChild(0).evaluate(environment);
        Expression child = getChild(1);
        if (evaluate instanceof UnitExpression) {
            String str = null;
            if (child instanceof SymbolExpression) {
                str = ((SymbolExpression) child).getName();
            } else if (child instanceof StringExpression) {
                str = ((StringExpression) child).getString();
            }
            if (str != null && (dimensionList = environment.getDimensionListManager().getDimensionList(str)) != null) {
                return FrinkBoolean.create(DimensionListMath.equals(((UnitExpression) evaluate).getUnit().getDimensionList(), dimensionList));
            }
            Expression evaluate2 = child.evaluate(environment);
            if (evaluate2 instanceof UnitExpression) {
                return FrinkBoolean.create(UnitMath.areConformal(((UnitExpression) evaluate).getUnit(), ((UnitExpression) evaluate2).getUnit()));
            }
        }
        return FrinkBoolean.FALSE;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ConformsToExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
