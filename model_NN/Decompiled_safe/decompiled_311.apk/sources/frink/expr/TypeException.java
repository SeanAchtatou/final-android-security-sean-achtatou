package frink.expr;

public class TypeException extends EvaluationException {
    public TypeException(String str, Expression expression) {
        super(str, expression);
    }
}
