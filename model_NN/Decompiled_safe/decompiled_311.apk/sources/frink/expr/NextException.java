package frink.expr;

public class NextException extends ControlFlowException {
    public NextException(Expression expression) {
        super("Next statement used outside a loop", expression);
    }
}
