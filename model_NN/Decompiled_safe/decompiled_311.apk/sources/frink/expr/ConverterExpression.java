package frink.expr;

import frink.date.FrinkDateFormatter;
import frink.date.TimeZoneSource;
import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.function.FunctionCacher;
import frink.function.NoSuchFunctionException;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.NumericException;
import frink.numeric.RealInterval;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import frink.units.DimensionListMath;
import frink.units.DimensionlessList;
import frink.units.Unit;
import frink.units.UnitMath;
import java.util.TimeZone;

public class ConverterExpression extends CachingExpression implements OperatorExpression {
    public static final String TYPE = "convert";

    public ConverterExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
     arg types: [java.lang.String, frink.expr.Environment, frink.expr.BasicListExpression, ?[OBJECT, ARRAY], int, ?[OBJECT, ARRAY]]
     candidates:
      frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
      frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression */
    public static String convertPart(Expression expression, Expression expression2, Environment environment) throws NumericException, EvaluationException {
        Expression expression3;
        String str;
        boolean z;
        String string;
        Unit unit;
        FrinkDateFormatter frinkDateFormatter;
        TimeZone timeZone;
        String str2;
        Expression expression4;
        String str3;
        boolean z2;
        Unit unit2;
        StringBuffer stringBuffer = new StringBuffer();
        Expression evaluate = expression2.evaluate(environment);
        if (!(evaluate instanceof ListExpression) || !(expression instanceof UnitExpression)) {
            if (expression instanceof UnitExpression) {
                if (evaluate instanceof StringExpression) {
                    String string2 = ((StringExpression) evaluate).getString();
                    try {
                        str2 = string2;
                        expression4 = environment.eval(string2).evaluate(environment);
                    } catch (FrinkSecurityException e) {
                        throw e;
                    } catch (Exception e2) {
                        throw new InvalidArgumentException("ConverterExpression:  error in parsing string: " + string2 + ":\n " + e2, evaluate);
                    }
                } else {
                    str2 = null;
                    expression4 = evaluate;
                }
                if (expression4 instanceof UnitExpression) {
                    try {
                        Expression evaluate2 = MultiplyExpression.divide(expression, expression4, environment).evaluate(environment);
                        if (evaluate2 instanceof UnitExpression) {
                            Unit unit3 = ((UnitExpression) evaluate2).getUnit();
                            if (UnitMath.isDimensionless(unit3)) {
                                stringBuffer.append(environment.format(evaluate2));
                                if (str2 != null) {
                                    stringBuffer.append(" " + str2);
                                }
                                return new String(stringBuffer);
                            }
                            try {
                                Expression evaluate3 = MultiplyExpression.divide(PowerExpression.reciprocal(expression, environment), expression4, environment).evaluate(environment);
                                if (UnitMath.isDimensionless(((UnitExpression) evaluate3).getUnit())) {
                                    stringBuffer.append(" Warning: reciprocal conversion\n");
                                    stringBuffer.append(environment.format(evaluate3));
                                    if (str2 != null) {
                                        stringBuffer.append(" " + str2);
                                    }
                                    return new String(stringBuffer);
                                }
                            } catch (ConformanceException e3) {
                                throw new EvaluationConformanceException("Conformance error: " + e3, expression4);
                            } catch (EvaluationNumericException | InvalidArgumentException | InvalidDenominatorException e4) {
                            }
                            stringBuffer.append(" Conformance error\n");
                            Unit unit4 = ((UnitExpression) expression).getUnit();
                            stringBuffer.append("   Left side is: " + UnitMath.toString(unit4, environment.getDimensionManager()));
                            String name = environment.getDimensionListManager().getName(unit4.getDimensionList());
                            if (name != null) {
                                stringBuffer.append(" (" + name + ")\n");
                            } else {
                                stringBuffer.append(" (unknown unit type)\n");
                            }
                            Unit unit5 = ((UnitExpression) expression4).getUnit();
                            stringBuffer.append("  Right side is: " + UnitMath.toString(unit5, environment.getDimensionManager()));
                            String name2 = environment.getDimensionListManager().getName(unit5.getDimensionList());
                            if (name2 != null) {
                                stringBuffer.append(" (" + name2 + ")\n");
                            } else {
                                stringBuffer.append(" (unknown unit type)\n");
                            }
                            String makeSuggestions = makeSuggestions(environment, unit3);
                            if (makeSuggestions != null) {
                                stringBuffer.append(makeSuggestions);
                            }
                        }
                    } catch (InvalidDenominatorException e5) {
                        throw new InvalidArgumentException("Divide by zero", expression4);
                    } catch (ConformanceException e6) {
                        throw new EvaluationConformanceException("Conformance exception: " + e6, expression4);
                    } catch (EvaluationNumericException e7) {
                        throw new EvaluationConformanceException("Conversion would require dividing by zero: ", expression4);
                    }
                }
                expression3 = expression4;
            } else {
                expression3 = evaluate;
            }
            if (expression instanceof DateExpression) {
                if (expression3 instanceof DateFormatterExpression) {
                    frinkDateFormatter = ((DateFormatterExpression) expression3).getFormatter();
                    timeZone = null;
                } else if (expression3 instanceof StringExpression) {
                    frinkDateFormatter = environment.getDateFormatterManager().getFormatter(((StringExpression) expression3).getString());
                    timeZone = null;
                } else if ((expression3 instanceof ListExpression) && expression3.getChildCount() == 2) {
                    Expression child = expression3.getChild(0);
                    if (child instanceof DateFormatterExpression) {
                        FrinkDateFormatter formatter = ((DateFormatterExpression) child).getFormatter();
                        Expression child2 = expression3.getChild(1);
                        if (child2 instanceof StringExpression) {
                            TimeZone timeZone2 = TimeZoneSource.getTimeZone(((StringExpression) child2).getString());
                            if (timeZone2 == null) {
                                environment.outputln("Warning: Converting time " + environment.format(expression) + " failed.  No timezone named '" + environment.format(child2) + "' found.");
                                timeZone = timeZone2;
                                frinkDateFormatter = null;
                            } else {
                                timeZone = timeZone2;
                                frinkDateFormatter = formatter;
                            }
                        } else {
                            frinkDateFormatter = null;
                            timeZone = null;
                        }
                    } else {
                        frinkDateFormatter = null;
                        timeZone = null;
                    }
                } else if (expression2 instanceof SymbolExpression) {
                    frinkDateFormatter = environment.getDateFormatterManager().getFormatter(((SymbolExpression) expression2).getName());
                    timeZone = null;
                } else {
                    frinkDateFormatter = null;
                    timeZone = null;
                }
                if (frinkDateFormatter != null) {
                    return frinkDateFormatter.format(((DateExpression) expression).getFrinkDate(), timeZone, environment);
                }
            }
            if (expression instanceof ListExpression) {
                ListExpression listExpression = (ListExpression) expression;
                int childCount = expression.getChildCount();
                stringBuffer.append("[");
                for (int i = 0; i < childCount; i++) {
                    if (i > 0) {
                        stringBuffer.append(", \n ");
                    }
                    Expression child3 = listExpression.getChild(i);
                    if (!(child3 instanceof StringExpression) || (unit = environment.getUnitManager().getUnit((string = ((StringExpression) child3).getString()))) == null) {
                        z = false;
                    } else {
                        String convertPart = convertPart(BasicUnitExpression.construct(unit), expression3, environment);
                        if (convertPart == null) {
                            stringBuffer.append(string);
                        } else {
                            stringBuffer.append(string + " = " + convertPart);
                        }
                        z = true;
                    }
                    if (!z) {
                        String convertPart2 = convertPart(listExpression.getChild(i), expression3, environment);
                        if (convertPart2 != null) {
                            stringBuffer.append(convertPart2);
                        } else {
                            stringBuffer.append(environment.format(listExpression.getChild(i)));
                        }
                    }
                }
                stringBuffer.append("]");
                return new String(stringBuffer);
            }
            if (expression3 instanceof SymbolExpression) {
                str = ((SymbolExpression) expression3).getName();
            } else if (expression2 instanceof SymbolExpression) {
                str = ((SymbolExpression) expression2).getName();
            } else {
                str = null;
            }
            if (str != null) {
                BasicListExpression basicListExpression = new BasicListExpression(1);
                basicListExpression.appendChild(expression);
                try {
                    return environment.format(environment.getFunctionManager().execute(str, environment, (ListExpression) basicListExpression, (FrinkObject) null, true, (FunctionCacher) null));
                } catch (NoSuchFunctionException e8) {
                } catch (ReturnException e9) {
                    return environment.format(e9.getReturnValue());
                } catch (EvaluationException e10) {
                    environment.outputln("Warning: Tried to call " + str + "[] but it didn't work.");
                    environment.outputln(e10.toString());
                }
            }
            if (stringBuffer.length() > 0) {
                return new String(stringBuffer);
            }
            if ((expression3 instanceof SymbolExpression) && !environment.getSymbolicMode()) {
                environment.outputln("Unknown symbol \"" + ((SymbolExpression) expression3).getName() + "\"");
            }
            try {
                Expression evaluate4 = MultiplyExpression.divide(expression, expression3, environment).evaluate(environment);
                if (ExpressionUtils.isDimensionless(evaluate4)) {
                    return environment.format(evaluate4);
                }
            } catch (ConformanceException e11) {
            }
            return null;
        }
        int childCount2 = evaluate.getChildCount();
        Unit unit6 = ((UnitExpression) expression).getUnit();
        if (unit6.getScale().isInterval()) {
            RealInterval realInterval = (RealInterval) unit6.getScale();
            return "[(" + convertPart(new CondensedUnitExpression(realInterval.getLower(), unit6.getDimensionList()), evaluate, environment) + "), (" + convertPart(new CondensedUnitExpression(realInterval.getUpper(), unit6.getDimensionList()), evaluate, environment) + ")]";
        }
        int i2 = 0;
        boolean z3 = false;
        Unit unit7 = unit6;
        while (i2 < childCount2) {
            Expression child4 = evaluate.getChild(i2);
            if (child4 instanceof StringExpression) {
                str3 = ((StringExpression) child4).getString();
                SymbolDefinition symbolDefinition = environment.getSymbolDefinition(str3, false);
                if (symbolDefinition != null) {
                    child4 = symbolDefinition.getValue();
                } else {
                    throw new InvalidArgumentException("ConverterExpression: Error: unknown string \"" + str3 + "\" in conversion.", child4);
                }
            } else {
                str3 = null;
            }
            if (!(child4 instanceof UnitExpression)) {
                return null;
            }
            Unit unit8 = ((UnitExpression) child4).getUnit();
            if (i2 == 0) {
                try {
                    if (UnitMath.getIntegerValue(unit8) == 0) {
                        z2 = true;
                        unit2 = unit7;
                        i2++;
                        unit7 = unit2;
                        z3 = z2;
                    }
                } catch (NotAnIntegerException e12) {
                }
            }
            if (i2 < childCount2 - 1) {
                if (i2 > 0 && !z3) {
                    try {
                        stringBuffer.append(", ");
                    } catch (ConformanceException e13) {
                        throw new EvaluationConformanceException("Arguments not conformal in conversion.", expression2);
                    }
                }
                Unit floor = UnitMath.floor(UnitMath.divide(unit7, unit8));
                Unit subtract = UnitMath.subtract(unit7, UnitMath.multiply(floor, unit8));
                if (z3) {
                    try {
                        if (UnitMath.getIntegerValue(floor) == 0) {
                            z2 = z3;
                            unit2 = subtract;
                            i2++;
                            unit7 = unit2;
                            z3 = z2;
                        }
                    } catch (NotAnIntegerException e14) {
                    }
                }
                stringBuffer.append(environment.format(BasicUnitExpression.construct(floor)));
                z2 = false;
                unit2 = subtract;
            } else {
                try {
                    if (UnitMath.getIntegerValue(unit8) == 0) {
                        return new String(stringBuffer);
                    }
                } catch (NotAnIntegerException e15) {
                }
                if (i2 > 0 && !z3) {
                    stringBuffer.append(", ");
                }
                stringBuffer.append(convertPart(BasicUnitExpression.construct(unit7), child4, environment));
                z2 = z3;
                unit2 = unit7;
            }
            if (str3 != null) {
                stringBuffer.append(" ");
                stringBuffer.append(str3);
            }
            i2++;
            unit7 = unit2;
            z3 = z2;
        }
        return new String(stringBuffer);
    }

    private static String makeSuggestions(Environment environment, Unit unit) {
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        addSuggestion(environment.getDimensionListManager().getName(DimensionListMath.divide(DimensionlessList.INSTANCE, unit.getDimensionList())), "multiply", "", stringBuffer, stringBuffer2);
        addSuggestion(environment.getDimensionListManager().getName(DimensionListMath.divide(DimensionlessList.INSTANCE, DimensionListMath.power(unit.getDimensionList(), 1, 2))), "multiply", "^2", stringBuffer, stringBuffer2);
        addSuggestion(environment.getDimensionListManager().getName(DimensionListMath.divide(DimensionlessList.INSTANCE, DimensionListMath.power(unit.getDimensionList(), 2))), "multiply", "^(1/2)", stringBuffer, stringBuffer2);
        addSuggestion(environment.getDimensionListManager().getName(unit.getDimensionList()), "divide", "", stringBuffer, stringBuffer2);
        addSuggestion(environment.getDimensionListManager().getName(DimensionListMath.power(unit.getDimensionList(), 1, 2)), "divide", "^2", stringBuffer, stringBuffer2);
        addSuggestion(environment.getDimensionListManager().getName(DimensionListMath.power(unit.getDimensionList(), 2)), "divide", "^(1/2)", stringBuffer, stringBuffer2);
        if (stringBuffer2.length() != 0) {
            stringBuffer2.append("\n                   to list known units with these dimensions.");
            stringBuffer.append(stringBuffer2);
        }
        if (stringBuffer.length() != 0) {
            return new String(stringBuffer);
        }
        return null;
    }

    private static void addSuggestion(String str, String str2, String str3, StringBuffer stringBuffer, StringBuffer stringBuffer2) {
        if (str != null) {
            if (stringBuffer.length() != 0) {
                stringBuffer.append("              or ");
            } else {
                stringBuffer.append("     Suggestion: ");
            }
            stringBuffer.append(str2 + " left side by " + str + str3 + "\n");
            if (stringBuffer2.length() == 0) {
                stringBuffer2.append("\n For help, type: ");
            } else {
                stringBuffer2.append("\n                   or\n                 ");
            }
            stringBuffer2.append("units[" + str + "]");
        }
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        if (getChildCount() != 2) {
            throw new InvalidArgumentException("ConverterExpression requires exactly two arguments.", this);
        }
        try {
            Expression evaluate = getChild(0).evaluate(environment);
            Expression child = getChild(1);
            try {
                String convertPart = convertPart(evaluate, child, environment);
                if (convertPart != null) {
                    return new BasicStringExpression(convertPart);
                }
                if (environment.getSymbolicMode()) {
                    return new ConverterExpression(evaluate, child.evaluate(environment));
                }
                return new BasicStringExpression("Unconvertable expression:\n  " + environment.format(new ConverterExpression(evaluate, child.evaluate(environment)), true));
            } catch (NumericException e) {
                throw new EvaluationNumericException(e.toString(), this);
            }
        } catch (InvalidChildException e2) {
            environment.outputln(e2.toString());
            return this;
        }
    }

    public String getSymbol() {
        return " -> ";
    }

    public int getPrecedence() {
        return 50;
    }

    public int getAssociativity() {
        return -1;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ConverterExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
