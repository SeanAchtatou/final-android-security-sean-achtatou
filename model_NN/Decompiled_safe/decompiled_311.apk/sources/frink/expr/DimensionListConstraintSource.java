package frink.expr;

import frink.units.DimensionList;
import frink.units.DimensionListManager;
import frink.units.DimensionListMath;
import frink.units.Source;
import java.util.Enumeration;
import java.util.Hashtable;

public class DimensionListConstraintSource implements Source<Constraint> {
    private Hashtable<String, DimensionListConstraint> constraints = new Hashtable<>();
    private DimensionListManager dlm;

    public DimensionListConstraintSource(DimensionListManager dimensionListManager) {
        this.dlm = dimensionListManager;
    }

    public String getName() {
        return "DimensionListConstraintSource";
    }

    public Constraint get(String str) {
        DimensionListConstraint dimensionListConstraint = this.constraints.get(str);
        if (dimensionListConstraint != null) {
            return dimensionListConstraint;
        }
        DimensionList dimensionList = this.dlm.getDimensionList(str);
        if (dimensionList == null) {
            return null;
        }
        DimensionListConstraint dimensionListConstraint2 = new DimensionListConstraint(str, dimensionList);
        this.constraints.put(str, dimensionListConstraint2);
        return dimensionListConstraint2;
    }

    public Enumeration<String> getNames() {
        return this.dlm.getNames();
    }

    private static class DimensionListConstraint implements Constraint {
        private DimensionList dl;
        private String name;

        DimensionListConstraint(String str, DimensionList dimensionList) {
            this.name = str;
            this.dl = dimensionList;
        }

        public boolean meetsConstraint(Expression expression) {
            if (expression instanceof UnitExpression) {
                return DimensionListMath.equals(((UnitExpression) expression).getUnit().getDimensionList(), this.dl);
            }
            return false;
        }

        public void checkConstraint(Expression expression) throws ConstraintException {
            if (!meetsConstraint(expression)) {
                throw new ConstraintException("Constraint not met--value must have dimensions of " + this.name, expression);
            }
        }

        public String getDescription() {
            return "Unit must have dimensions of " + this.name;
        }
    }
}
