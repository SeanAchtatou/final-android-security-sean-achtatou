package frink.expr;

public interface AssignableExpression extends Expression {
    void assign(Expression expression, Environment environment) throws EvaluationException;
}
