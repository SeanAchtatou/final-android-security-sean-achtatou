package frink.expr;

public class BreakException extends ControlFlowException {
    private String label;

    public BreakException(Expression expression) {
        super("Break statement used outside a loop", expression);
        this.label = null;
    }

    public BreakException(String str, Expression expression) {
        super("No corresponding loop for \"break " + str + "\" statement.", expression);
        this.label = str;
    }

    public String getLabel() {
        return this.label;
    }
}
