package frink.expr;

import frink.symbolic.MatchingContext;

public class SymbolExpression extends TerminalExpression implements AssignableExpression, HashingExpression {
    public static final String TYPE = "Symbol";
    private Expression constantValue = null;
    private String name;

    public SymbolExpression(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        if (this.constantValue != null) {
            return this.constantValue;
        }
        SymbolDefinition symbolDefinition = environment.getSymbolDefinition(this.name, false);
        if (symbolDefinition == null) {
            return this;
        }
        if (symbolDefinition.isConstant()) {
            this.constantValue = symbolDefinition.getValue();
        }
        return symbolDefinition.getValue();
    }

    public boolean isConstant() {
        return this.constantValue != null;
    }

    public void assign(Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException {
        environment.setSymbolDefinition(this.name, expression);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (expression instanceof SymbolExpression) {
            return this.name.equals(((SymbolExpression) expression).getName());
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public int hashCode() {
        return 2044383639 ^ this.name.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof SymbolExpression) {
            return this.name.equals(((SymbolExpression) obj).name);
        }
        return false;
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }
}
