package frink.expr;

public interface SelfDisplayingExpression {
    String toString(Environment environment, boolean z);
}
