package frink.expr;

import frink.date.DateFormatterManager;
import frink.date.DateParserManager;
import frink.date.FrinkDateFormatter;
import frink.format.UnitFormatterManager;
import frink.function.FunctionManager;
import frink.graphics.FrinkImageLoader;
import frink.graphics.GraphicsViewFactory;
import frink.io.InputItem;
import frink.io.InputManager;
import frink.io.OutputManager;
import frink.object.ClassManager;
import frink.object.InterfaceManager;
import frink.object.ObjectManager;
import frink.parser.IncludeManager;
import frink.security.SecurityHelper;
import frink.symbolic.TransformationRuleManager;
import frink.units.DimensionListManager;
import frink.units.DimensionManager;
import frink.units.UnitManager;
import java.util.TimeZone;
import java.util.Vector;

public interface Environment {
    void addContextFrame(ContextFrame contextFrame, boolean z);

    SymbolDefinition declareGlobalVariable(String str, Vector<String> vector, Expression expression) throws VariableExistsException, CannotAssignException, UnknownConstraintException, FrinkSecurityException;

    SymbolDefinition declareVariable(String str, Vector<String> vector, Expression expression) throws VariableExistsException, CannotAssignException, UnknownConstraintException, FrinkSecurityException;

    Expression eval(String str) throws Exception;

    String format(Expression expression);

    String format(Expression expression, boolean z);

    ClassManager getClassManager();

    ConstraintFactory getConstraintFactory();

    DateFormatterManager getDateFormatterManager();

    DateParserManager getDateParserManager();

    TimeZone getDefaultTimeZone();

    DimensionListManager getDimensionListManager();

    DimensionManager getDimensionManager();

    FrinkImageLoader getFrinkImageLoader();

    FunctionManager getFunctionManager();

    GraphicsViewFactory getGraphicsViewFactory();

    IncludeManager getIncludeManager();

    InputManager getInputManager();

    InterfaceManager getInterfaceManager();

    ObjectManager getObjectManager();

    FrinkDateFormatter getOutputDateFormatter();

    OutputManager getOutputManager();

    SecurityHelper getSecurityHelper();

    SymbolDefinition getSymbolDefinition(String str, boolean z) throws CannotAssignException, FrinkSecurityException;

    boolean getSymbolicMode();

    TransformationRuleManager getTransformationRuleManager();

    UnitFormatterManager getUnitFormatterManager();

    UnitManager getUnitManager();

    String input(String str, String str2);

    String[] input(String str, InputItem[] inputItemArr);

    boolean isVariableDefined(String str);

    void output(Expression expression);

    void output(String str);

    void outputln(Expression expression);

    void outputln(String str);

    ContextFrame removeContextFrame();

    void setDefaultTimeZone(String str);

    void setGraphicsViewFactory(GraphicsViewFactory graphicsViewFactory);

    void setInputManager(InputManager inputManager);

    void setOutputDateFormatter(FrinkDateFormatter frinkDateFormatter);

    void setOutputManager(OutputManager outputManager);

    void setRestrictiveSecurity(boolean z);

    void setSecurityHelper(SecurityHelper securityHelper);

    SymbolDefinition setSymbolDefinition(String str, Expression expression) throws CannotAssignException, FrinkSecurityException;

    void setSymbolicMode(boolean z);
}
