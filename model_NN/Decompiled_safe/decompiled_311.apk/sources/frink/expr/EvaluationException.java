package frink.expr;

import frink.errors.FrinkException;

public abstract class EvaluationException extends FrinkException {
    private Expression expression;

    public EvaluationException(String str, Expression expression2) {
        super(str);
        this.expression = expression2;
    }

    public Expression getExpression() {
        return this.expression;
    }
}
