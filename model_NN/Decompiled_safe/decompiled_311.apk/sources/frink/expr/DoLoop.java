package frink.expr;

import frink.symbolic.MatchingContext;

public class DoLoop extends NonTerminalExpression {
    public static final String TYPE = "do";
    private String label;

    public DoLoop(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
        this.label = null;
    }

    public DoLoop(Expression expression, Expression expression2, String str) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
        this.label = str;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        try {
            Expression child = getChild(0);
            Expression child2 = getChild(1);
            do {
                try {
                    child.evaluate(environment);
                } catch (NextException e) {
                } catch (BreakException e2) {
                    String label2 = e2.getLabel();
                    if (label2 == null) {
                        return VoidExpression.VOID;
                    }
                    if (label2.equals(this.label)) {
                        return VoidExpression.VOID;
                    }
                    throw e2;
                }
            } while (Truth.isTrue(environment, child2.evaluate(environment)));
            return VoidExpression.VOID;
        } catch (InvalidChildException e3) {
            throw new InvalidArgumentException("DoLoop: bad child", this);
        }
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
