package frink.expr;

public interface ExpressionFactory<T> {
    Expression makeExpression(Object obj, Environment environment);
}
