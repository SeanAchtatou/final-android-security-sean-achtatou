package frink.expr;

public class NotSupportedException extends EvaluationException {
    public NotSupportedException(String str, Expression expression) {
        super(str, expression);
    }
}
