package frink.expr;

import frink.symbolic.MatchingContext;

public class UnsafeEvalExpression extends NonTerminalExpression {
    public static final String TYPE = "unsafeEval";

    public UnsafeEvalExpression(Expression expression) {
        super(1);
        if (expression instanceof ListExpression) {
            try {
                appendChild(expression.getChild(0));
            } catch (InvalidChildException e) {
                System.out.println("Cannot construct UnsafeEvalExpression with empty list");
            }
        } else {
            appendChild(expression);
        }
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        environment.getSecurityHelper().checkUnsafeEval();
        try {
            Expression evaluate = getChild(0).evaluate(environment);
            if (!(evaluate instanceof ListExpression)) {
                return evaluatePart(evaluate, environment);
            }
            ListExpression listExpression = (ListExpression) evaluate;
            int childCount = listExpression.getChildCount();
            BasicListExpression basicListExpression = new BasicListExpression(childCount);
            for (int i = 0; i < childCount; i++) {
                basicListExpression.appendChild(evaluatePart(listExpression.getChild(i), environment));
            }
            return basicListExpression;
        } catch (InvalidChildException e) {
            environment.outputln(e.toString());
            return this;
        } catch (EvaluationException e2) {
            return UndefExpression.UNDEF;
        } catch (Throwable th) {
            return UndefExpression.UNDEF;
        }
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof UnsafeEvalExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    private Expression evaluatePart(Expression expression, Environment environment) throws EvaluationException, Exception {
        if (!(expression instanceof StringExpression)) {
            return expression.evaluate(environment);
        }
        String string = ((StringExpression) expression).getString();
        if (string == null) {
            return VoidExpression.VOID;
        }
        return environment.eval(string).evaluate(environment);
    }

    public boolean isConstant() {
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
