package frink.expr;

import java.util.Vector;

public interface Context {
    void addContextFrame(ContextFrame contextFrame, boolean z);

    SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException, FrinkSecurityException;

    SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws CannotAssignException, FrinkSecurityException;

    ContextFrame removeContextFrame();

    SymbolDefinition setSymbol(String str, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException;
}
