package frink.expr;

public class UnmodifiableSymbolDefinition implements SymbolDefinition {
    private Expression value;

    public UnmodifiableSymbolDefinition(Expression expression) {
        this.value = expression;
    }

    public void setValue(Expression expression) throws CannotAssignException {
        throw new CannotAssignException("Cannot modify the value of this symbol.", expression);
    }

    public Expression getValue() {
        return this.value;
    }

    public boolean isConstant() {
        return false;
    }
}
