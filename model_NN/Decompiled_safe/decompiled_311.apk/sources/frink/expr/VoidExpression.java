package frink.expr;

import frink.symbolic.MatchingContext;

public final class VoidExpression extends TerminalExpression {
    public static final String TYPE = "Void";
    public static final VoidExpression VOID = new VoidExpression();

    private VoidExpression() {
    }

    public final boolean isConstant() {
        return true;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
