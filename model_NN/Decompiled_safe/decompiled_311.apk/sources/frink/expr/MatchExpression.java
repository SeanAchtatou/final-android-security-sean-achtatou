package frink.expr;

import frink.symbolic.MatchingContext;
import frink.text.FrinkExpressionSubstitution;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Substitution;
import org.apache.oro.text.regex.Util;

public class MatchExpression extends NonTerminalExpression {
    public static final String TYPE = "Match";
    private Perl5Matcher matcher = new Perl5Matcher();

    public MatchExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        Expression child = getChild(0);
        Expression evaluate = child.evaluate(environment);
        if (!(evaluate instanceof StringExpression)) {
            throw new InvalidArgumentException("MatchExpression: left must be string.  Expression was " + environment.format(evaluate), this);
        }
        Expression doOperation = doOperation(child, ((StringExpression) evaluate).getString(), environment);
        if (doOperation == null) {
            return UndefExpression.UNDEF;
        }
        return doOperation;
    }

    public Expression doOperation(Expression expression, String str, Environment environment) throws EvaluationException {
        Expression evaluate = getChild(1).evaluate(environment);
        if (evaluate instanceof RegexpExpression) {
            RegexpExpression regexpExpression = (RegexpExpression) evaluate;
            if (regexpExpression.isMultiple()) {
                return new MatchEnumeration(str, regexpExpression);
            }
            return doMatch(regexpExpression.getPattern(), new PatternMatcherInput(str), environment);
        } else if (evaluate instanceof SubstitutionExpression) {
            String doSubstitution = doSubstitution((SubstitutionExpression) evaluate, str, environment);
            BasicStringExpression basicStringExpression = new BasicStringExpression(doSubstitution);
            if (doSubstitution != str && (expression instanceof AssignableExpression)) {
                ((AssignableExpression) expression).assign(basicStringExpression, environment);
            }
            return basicStringExpression;
        } else {
            throw new InvalidArgumentException("MatchExpression: right must pattern match or substitution... type is " + environment.format(evaluate), this);
        }
    }

    /* access modifiers changed from: private */
    public Expression doMatch(Pattern pattern, PatternMatcherInput patternMatcherInput, Environment environment) throws EvaluationException {
        MatchResult matchResult;
        synchronized (this.matcher) {
            if (this.matcher.contains(patternMatcherInput, pattern)) {
                matchResult = this.matcher.getMatch();
            } else {
                matchResult = null;
            }
        }
        if (matchResult == null) {
            return null;
        }
        int groups = matchResult.groups();
        BasicListExpression basicListExpression = new BasicListExpression(groups - 1);
        for (int i = 1; i < groups; i++) {
            if (matchResult.group(i) == null) {
                basicListExpression.appendChild(UndefExpression.UNDEF);
            } else {
                basicListExpression.appendChild(new BasicStringExpression(matchResult.group(i)));
            }
        }
        return basicListExpression;
    }

    private String doSubstitution(SubstitutionExpression substitutionExpression, String str, Environment environment) throws EvaluationException {
        Substitution substitution = substitutionExpression.getSubstitution();
        if (substitution instanceof FrinkExpressionSubstitution) {
            ((FrinkExpressionSubstitution) substitution).setEnvironment(environment);
        }
        return Util.substitute(this.matcher, substitutionExpression.getPattern(), substitution, str, substitutionExpression.getNumSubs());
    }

    public boolean isConstant() {
        return false;
    }

    private class MatchEnumeration extends NonTerminalExpression implements FrinkEnumeration, EnumeratingExpression {
        public static final String TYPE = "MatchEnumeration";
        private PatternMatcherInput input;
        private Pattern pat;
        private BasicListExpression retVal;

        private MatchEnumeration(String str, RegexpExpression regexpExpression) throws EvaluationException {
            super(0);
            this.pat = regexpExpression.getPattern();
            this.input = new PatternMatcherInput(str);
            this.retVal = null;
        }

        public Expression evaluate(Environment environment) throws EvaluationException {
            if (this.retVal != null) {
                return this.retVal;
            }
            this.retVal = new BasicListExpression(0);
            while (true) {
                Expression next = getNext(environment);
                if (next == null) {
                    return this.retVal;
                }
                this.retVal.appendChild(next);
            }
        }

        public boolean isConstant() {
            return false;
        }

        public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
            if (this == expression) {
                return true;
            }
            if (expression instanceof MatchEnumeration) {
                return childrenEqual(expression, matchingContext, environment, z);
            }
            return false;
        }

        public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
            if (this.retVal != null) {
                return this.retVal.getEnumeration(environment);
            }
            this.retVal = new BasicListExpression(0);
            return this;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            Expression access$100 = MatchExpression.this.doMatch(this.pat, this.input, environment);
            this.retVal.appendChild(access$100);
            return access$100;
        }

        public void dispose() {
            this.input = null;
            this.pat = null;
        }

        public String getExpressionType() {
            return TYPE;
        }
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof MatchExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
