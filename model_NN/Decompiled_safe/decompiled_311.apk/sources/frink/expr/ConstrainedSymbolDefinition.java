package frink.expr;

import java.util.Vector;

public class ConstrainedSymbolDefinition implements SymbolDefinition {
    private Vector<Constraint> constraints;
    private Expression value;

    public ConstrainedSymbolDefinition(Vector<Constraint> vector, Expression expression) throws ConstraintException {
        this.constraints = vector;
        setValue(expression);
    }

    public void setValue(Expression expression) throws ConstraintException {
        if (this.constraints != null) {
            int size = this.constraints.size();
            for (int i = 0; i < size; i++) {
                this.constraints.elementAt(i).checkConstraint(expression);
            }
        }
        this.value = expression;
    }

    public Expression getValue() {
        return this.value;
    }

    public boolean isConstant() {
        return false;
    }
}
