package frink.expr;

public interface StringExpression extends Expression {
    String getString();
}
