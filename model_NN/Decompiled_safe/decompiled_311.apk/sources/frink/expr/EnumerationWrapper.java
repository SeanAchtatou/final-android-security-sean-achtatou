package frink.expr;

import frink.symbolic.MatchingContext;
import java.util.Enumeration;

public class EnumerationWrapper<T> extends TerminalExpression implements EnumeratingExpression {
    private EnumerationWrapper<T>.Wrapper<T> wrap;

    public EnumerationWrapper(Enumeration<T> enumeration, ExpressionFactory<T> expressionFactory) {
        this.wrap = new Wrapper<>(enumeration, expressionFactory);
    }

    public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
        return this.wrap;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    private class Wrapper<S> implements FrinkEnumeration {
        private Enumeration<S> enumerator;
        private ExpressionFactory<S> factory;

        private Wrapper(Enumeration<S> enumeration, ExpressionFactory<S> expressionFactory) {
            this.enumerator = enumeration;
            this.factory = expressionFactory;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.enumerator != null) {
                if (this.enumerator.hasMoreElements()) {
                    return this.factory.makeExpression(this.enumerator.nextElement(), environment);
                }
                this.enumerator = null;
            }
            return null;
        }

        public void dispose() {
            this.enumerator = null;
        }
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return EnumeratingExpression.TYPE;
    }
}
