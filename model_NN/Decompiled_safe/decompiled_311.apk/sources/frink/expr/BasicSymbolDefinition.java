package frink.expr;

public class BasicSymbolDefinition implements SymbolDefinition {
    private Expression value;

    public BasicSymbolDefinition(Expression expression) {
        this.value = expression;
    }

    public void setValue(Expression expression) {
        this.value = expression;
    }

    public Expression getValue() {
        return this.value;
    }

    public boolean isConstant() {
        return false;
    }
}
