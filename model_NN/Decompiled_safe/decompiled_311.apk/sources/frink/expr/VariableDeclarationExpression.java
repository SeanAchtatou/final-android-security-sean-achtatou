package frink.expr;

import frink.symbolic.MatchingContext;
import java.util.Vector;

public class VariableDeclarationExpression extends NonTerminalExpression {
    private static final String TYPE = "VariableDeclaration";
    private Vector<String> constraints;
    private boolean hasInitialValue;
    private String name;

    public VariableDeclarationExpression(String str, Vector<String> vector, Expression expression) {
        super(1);
        if (expression != null) {
            this.hasInitialValue = true;
            appendChild(expression);
        } else {
            this.hasInitialValue = false;
            appendChild(UndefExpression.UNDEF);
        }
        this.name = str;
        this.constraints = vector;
    }

    public VariableDeclarationExpression(ConstraintDefinition constraintDefinition) {
        this(constraintDefinition.getName(), constraintDefinition.getConstraintNames(), constraintDefinition.getInitialValue());
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return environment.declareVariable(this.name, this.constraints, getInitialValue(environment)).getValue();
    }

    public boolean isConstant() {
        return false;
    }

    public String getName() {
        return this.name;
    }

    public Vector<String> getConstraints() {
        return this.constraints;
    }

    public static Vector<String> constructVector(String str, Expression expression) throws InvalidArgumentException {
        if (expression instanceof ListExpression) {
            ListExpression listExpression = (ListExpression) expression;
            int childCount = listExpression.getChildCount();
            Vector<String> vector = new Vector<>(childCount);
            int i = 0;
            while (i < childCount) {
                try {
                    Expression child = listExpression.getChild(i);
                    if (child instanceof SymbolExpression) {
                        vector.addElement(((SymbolExpression) child).getName());
                        i++;
                    } else {
                        throw new InvalidArgumentException("Constraint in declaration for variable " + str + " is not a constraint name.", child);
                    }
                } catch (InvalidChildException e) {
                    throw new InvalidArgumentException("When declaring constraints for variable " + str + ", got a weird InvalidChildException: \n" + e, expression);
                }
            }
            return vector;
        }
        throw new InvalidArgumentException("Constraints in declaration of variable " + str + " is not a constraint name.", expression);
    }

    public Expression getInitialValue(Environment environment) throws EvaluationException {
        if (this.hasInitialValue) {
            return getChild(0).evaluate(environment);
        }
        return UndefExpression.UNDEF;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
