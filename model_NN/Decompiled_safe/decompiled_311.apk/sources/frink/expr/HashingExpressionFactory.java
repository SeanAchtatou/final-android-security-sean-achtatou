package frink.expr;

public class HashingExpressionFactory implements ExpressionFactory<HashingExpression> {
    public static final HashingExpressionFactory INSTANCE = new HashingExpressionFactory();

    public Expression makeExpression(HashingExpression hashingExpression, Environment environment) {
        return hashingExpression;
    }
}
