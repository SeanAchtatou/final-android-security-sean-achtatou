package frink.expr;

import frink.units.BasicManager;
import frink.units.Manager;
import frink.units.Source;
import java.util.Vector;

public class BasicConstraintFactory implements ConstraintFactory {
    private Manager<Constraint> mgr = new BasicManager();

    public Constraint createConstraint(String str) {
        return this.mgr.get(str);
    }

    public void addConstraintSource(Source<Constraint> source) {
        this.mgr.addSource(source);
    }

    public void removeConstraintSource(String str) {
        this.mgr.removeSource(str);
    }

    public Vector<Constraint> createConstraints(Vector<String> vector) throws UnknownConstraintException {
        if (vector == null || vector.size() <= 0) {
            return null;
        }
        int size = vector.size();
        Vector<Constraint> vector2 = new Vector<>(size);
        for (int i = 0; i < size; i++) {
            String elementAt = vector.elementAt(i);
            Constraint createConstraint = createConstraint(elementAt);
            if (createConstraint == null) {
                throw new UnknownConstraintException(elementAt, null);
            }
            vector2.addElement(createConstraint);
        }
        return vector2;
    }
}
