package frink.expr;

public abstract class ControlFlowException extends EvaluationException {
    public ControlFlowException(String str, Expression expression) {
        super(str, expression);
    }
}
