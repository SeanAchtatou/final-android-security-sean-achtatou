package frink.expr;

import frink.object.MetaclassObject;
import frink.object.NoSuchObjectException;
import frink.symbolic.MatchingContext;

public class MetaNewExpression extends NonTerminalExpression {
    public static final String TYPE = "new";

    public MetaNewExpression(Expression expression) {
        super(1);
        appendChild(expression);
    }

    public MetaNewExpression(Expression expression, ListExpression listExpression) {
        super(2);
        appendChild(expression);
        appendChild(listExpression);
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        ListExpression listExpression;
        Expression createInstance;
        Expression evaluate = getChild(0).evaluate(environment);
        if (getChildCount() >= 2) {
            listExpression = getChild(1).evaluate(environment);
        } else {
            listExpression = null;
        }
        if (evaluate instanceof StringExpression) {
            createInstance = environment.getObjectManager().construct(((StringExpression) evaluate).getString(), listExpression, environment);
        } else {
            createInstance = evaluate instanceof MetaclassObject ? ((MetaclassObject) evaluate).getFrinkClass().createInstance(listExpression, environment) : null;
        }
        if (createInstance != null) {
            return createInstance;
        }
        StringBuffer stringBuffer = new StringBuffer("Could not construct: " + environment.format(evaluate));
        if (listExpression != null) {
            stringBuffer.append(environment.format(listExpression));
        }
        throw new NoSuchObjectException(new String(stringBuffer), this);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof MetaNewExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return "new";
    }
}
