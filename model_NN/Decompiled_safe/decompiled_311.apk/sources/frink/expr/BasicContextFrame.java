package frink.expr;

import frink.object.ThisContextFrame;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class BasicContextFrame implements ContextFrame, ThisContextFrame {
    private boolean canCreate;
    private boolean canModify;
    private Hashtable<String, SymbolDefinition> symbols;

    public BasicContextFrame() {
        this.symbols = new Hashtable<>(5);
        this.canCreate = true;
        this.canModify = true;
    }

    protected BasicContextFrame(BasicContextFrame basicContextFrame, int i) {
        this.symbols = new Hashtable<>(basicContextFrame.symbols.size());
        Enumeration<String> keys = basicContextFrame.symbols.keys();
        while (keys.hasMoreElements()) {
            String nextElement = keys.nextElement();
            Expression value = basicContextFrame.symbols.get(nextElement).getValue();
            int i2 = i < 0 ? i : i == 0 ? 0 : i - 1;
            if (i != 0 && (value instanceof DeepCopyable)) {
                value = ((DeepCopyable) value).deepCopy(i2);
            }
            this.symbols.put(nextElement, new BasicSymbolDefinition(value));
        }
        this.canCreate = basicContextFrame.canCreate;
        this.canModify = basicContextFrame.canModify;
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws FrinkSecurityException {
        SymbolDefinition symbolDefinition = this.symbols.get(str);
        if (symbolDefinition != null) {
            return symbolDefinition;
        }
        if (!z || !this.canCreate) {
            return null;
        }
        BasicSymbolDefinition basicSymbolDefinition = new BasicSymbolDefinition(UndefExpression.UNDEF);
        this.symbols.put(str, basicSymbolDefinition);
        return basicSymbolDefinition;
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException {
        if (this.canCreate) {
            if (this.canModify) {
                return doSetSymbol(str, expression);
            }
            if (this.symbols.get(str) == null) {
                return doSetSymbol(str, expression);
            }
            throw new CannotAssignException("Can create new variables but not modify existing symbol " + str + " in the BasicContextFrame it's defined in.  That's odd.", expression);
        } else if (!this.canModify) {
            throw new CannotAssignException("Cannot set symbol " + str + " in an unassignable BasicContextFrame.", expression);
        } else if (this.symbols.get(str) != null) {
            return doSetSymbol(str, expression);
        } else {
            throw new CannotAssignException("Cannot create nonexistent variable " + str + " in defining BasicContextFrame.  I could modify it if it existed, though.", expression);
        }
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException, FrinkSecurityException {
        if (this.symbols.get(str) != null) {
            throw new VariableExistsException("Variable " + str + " already exists", expression);
        } else if (vector == null || vector.size() == 0) {
            return doSetSymbol(str, expression);
        } else {
            if (this.canCreate) {
                ConstrainedSymbolDefinition constrainedSymbolDefinition = new ConstrainedSymbolDefinition(vector, expression);
                this.symbols.put(str, constrainedSymbolDefinition);
                return constrainedSymbolDefinition;
            }
            throw new CannotAssignException("Cannot declare variable " + str + " in non-modifiable context frame.", expression);
        }
    }

    private SymbolDefinition doSetSymbol(String str, Expression expression) throws CannotAssignException {
        if (expression == null) {
            this.symbols.remove(str);
            return null;
        }
        SymbolDefinition symbolDefinition = this.symbols.get(str);
        if (symbolDefinition == null) {
            BasicSymbolDefinition basicSymbolDefinition = new BasicSymbolDefinition(expression);
            this.symbols.put(str, basicSymbolDefinition);
            return basicSymbolDefinition;
        }
        symbolDefinition.setValue(expression);
        return symbolDefinition;
    }

    public boolean canCreateNewVariables() {
        return this.canCreate;
    }

    public void setCanCreateNewVariables(boolean z) {
        this.canCreate = z;
    }

    public boolean canModifyVariables() {
        return this.canModify;
    }

    public void setCanModifyVariables(boolean z) {
        this.canModify = z;
    }

    public void setThis(Expression expression) {
        try {
            doSetSymbol("this", expression);
        } catch (CannotAssignException e) {
            System.out.println("BasicContextFrame:  error:  could not set 'this' object:\n  " + e);
            e.printStackTrace();
        }
    }

    public BasicContextFrame deepCopy(int i) {
        return new BasicContextFrame(this, i);
    }
}
