package frink.expr;

import frink.security.RestrictiveSecurityHelper;
import frink.security.SecurityHelper;
import frink.symbolic.MatchingContext;

public class EvalExpression extends NonTerminalExpression {
    public static final String TYPE = "eval";
    public boolean hideLocals;
    public boolean rethrow;

    public EvalExpression(Expression expression, boolean z, boolean z2) {
        super(1);
        this.rethrow = z;
        this.hideLocals = z2;
        if (expression instanceof ListExpression) {
            try {
                appendChild(expression.getChild(0));
            } catch (InvalidChildException e) {
                System.out.println("Cannot construct EvalExpression with empty list");
            }
        } else {
            appendChild(expression);
        }
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return evaluate(getChild(0).evaluate(environment), environment, this.rethrow, this.hideLocals);
    }

    public static Expression evaluate(Expression expression, Environment environment, boolean z, boolean z2) throws EvaluationException {
        SecurityHelper securityHelper = environment.getSecurityHelper();
        try {
            environment.setSecurityHelper(RestrictiveSecurityHelper.INSTANCE);
            if (z2) {
                environment.addContextFrame(new BasicContextFrame(), true);
            }
            if (expression instanceof ListExpression) {
                ListExpression listExpression = (ListExpression) expression;
                int childCount = listExpression.getChildCount();
                BasicListExpression basicListExpression = new BasicListExpression(childCount);
                for (int i = 0; i < childCount; i++) {
                    basicListExpression.appendChild(evaluatePart(listExpression.getChild(i), environment));
                }
                environment.setSecurityHelper(securityHelper);
                if (z2) {
                    environment.removeContextFrame();
                }
                return basicListExpression;
            }
            Expression evaluatePart = evaluatePart(expression, environment);
            environment.setSecurityHelper(securityHelper);
            if (!z2) {
                return evaluatePart;
            }
            environment.removeContextFrame();
            return evaluatePart;
        } catch (InvalidChildException e) {
            environment.outputln(e.toString());
            environment.setSecurityHelper(securityHelper);
            if (z2) {
                environment.removeContextFrame();
            }
            return expression;
        } catch (EvaluationException e2) {
            if (z) {
                throw e2;
            }
            UndefExpression undefExpression = UndefExpression.UNDEF;
            environment.setSecurityHelper(securityHelper);
            if (!z2) {
                return undefExpression;
            }
            environment.removeContextFrame();
            return undefExpression;
        } catch (Throwable th) {
            environment.setSecurityHelper(securityHelper);
            if (z2) {
                environment.removeContextFrame();
            }
            throw th;
        }
    }

    private static Expression evaluatePart(Expression expression, Environment environment) throws EvaluationException, Exception {
        if (!(expression instanceof StringExpression)) {
            return expression.evaluate(environment);
        }
        String string = ((StringExpression) expression).getString();
        if (string == null) {
            return VoidExpression.VOID;
        }
        return environment.eval(string).evaluate(environment);
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof EvalExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
