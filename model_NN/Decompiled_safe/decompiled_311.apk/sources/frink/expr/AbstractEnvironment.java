package frink.expr;

import frink.io.InputItem;

public abstract class AbstractEnvironment implements Environment {
    public void output(String str) {
        getOutputManager().output(str);
    }

    public void output(Expression expression) {
        getOutputManager().output(format(expression));
    }

    public void outputln(String str) {
        getOutputManager().outputln(str);
    }

    public void outputln(Expression expression) {
        getOutputManager().outputln(format(expression));
    }

    public String input(String str, String str2) {
        return getInputManager().input(str, str2, this);
    }

    public String[] input(String str, InputItem[] inputItemArr) {
        return getInputManager().input(str, inputItemArr, this);
    }
}
