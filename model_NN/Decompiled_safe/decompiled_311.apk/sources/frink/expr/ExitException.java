package frink.expr;

public class ExitException extends ControlFlowException {
    public ExitException(Expression expression) {
        super("Uncaught exit statement.", expression);
    }
}
