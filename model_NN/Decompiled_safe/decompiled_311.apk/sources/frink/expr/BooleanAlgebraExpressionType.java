package frink.expr;

import java.util.Enumeration;
import java.util.Hashtable;

public class BooleanAlgebraExpressionType {
    public static final BooleanAlgebraExpressionType AND = new BooleanAlgebraExpressionType("AND", " && ", 1, 200, 2, -1, -1, true);
    public static final BooleanAlgebraExpressionType IMPLIES = new BooleanAlgebraExpressionType("IMPLIES", " IMPLIES ", 7, 100, 2, 2, -1, false);
    public static final BooleanAlgebraExpressionType NAND = new BooleanAlgebraExpressionType("NAND", " NAND ", 6, 200, 2, -1, -1, true);
    public static final BooleanAlgebraExpressionType NOR = new BooleanAlgebraExpressionType("NOR", " NOR ", 5, 100, 2, -1, -1, true);
    public static final BooleanAlgebraExpressionType NOT = new BooleanAlgebraExpressionType("NOT", "!", 4, OperatorExpression.PREC_LOGICAL_NOT, 1, 1, 1, true);
    public static final BooleanAlgebraExpressionType OR = new BooleanAlgebraExpressionType("OR", " || ", 2, 100, 2, -1, -1, true);
    public static final BooleanAlgebraExpressionType XOR = new BooleanAlgebraExpressionType("XOR", " XOR ", 3, 100, 2, -1, -1, true);
    private static final Hashtable<String, BooleanAlgebraExpressionType> opTable = new Hashtable<>();
    private int associativity;
    private int intValue;
    private boolean isCommutative;
    private int maxChildren;
    private int minChildren;
    private int precedence;
    private String symbol;
    private String text;

    private BooleanAlgebraExpressionType(String str, String str2, int i, int i2, int i3, int i4, int i5, boolean z) {
        this.text = str;
        this.symbol = str2;
        this.intValue = i;
        this.precedence = i2;
        this.minChildren = i3;
        this.maxChildren = i4;
        this.associativity = i5;
        this.isCommutative = z;
        opTable.put(str, this);
    }

    public String getText() {
        return this.text;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public static BooleanAlgebraExpressionType getBooleanAlgebraExpressionType(String str) {
        return opTable.get(str);
    }

    public int getIntType() {
        return this.intValue;
    }

    public int getPrecedence() {
        return this.precedence;
    }

    public int getMinChildren() {
        return this.minChildren;
    }

    public int getMaxChildren() {
        return this.maxChildren;
    }

    public int getAssociativity() {
        return this.associativity;
    }

    public boolean isCommutative() {
        return this.isCommutative;
    }

    public static Enumeration<String> getKeys() {
        return opTable.keys();
    }
}
