package frink.expr;

import frink.symbolic.MatchingContext;

public class TryExpression implements Expression {
    public static final String TYPE = "TryExpression";
    private Expression body;
    private Expression finallyBlock;

    public TryExpression(Expression expression, Expression expression2) {
        this.body = expression;
        this.finallyBlock = expression2;
    }

    public int getChildCount() {
        return 2;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.body;
        }
        if (i == 1) {
            return this.finallyBlock;
        }
        throw new InvalidChildException("TryExpression: requested invalid child " + i, this);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        try {
            environment.addContextFrame(null, false);
            return this.body.evaluate(environment);
        } finally {
            environment.removeContextFrame();
            this.finallyBlock.evaluate(environment);
        }
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
