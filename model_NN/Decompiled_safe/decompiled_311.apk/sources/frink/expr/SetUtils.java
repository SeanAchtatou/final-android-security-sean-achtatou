package frink.expr;

public class SetUtils {
    public static SetExpression union(SetExpression setExpression, SetExpression setExpression2, Environment environment) throws EvaluationException {
        BasicSetExpression basicSetExpression = new BasicSetExpression();
        addValues(basicSetExpression, setExpression, environment);
        addValues(basicSetExpression, setExpression2, environment);
        return basicSetExpression;
    }

    private static void addValues(SetExpression setExpression, SetExpression setExpression2, Environment environment) throws EvaluationException {
        FrinkEnumeration enumeration;
        EnumeratingExpression values = setExpression2.getValues();
        if (values != null && (enumeration = values.getEnumeration(environment)) != null) {
            while (true) {
                try {
                    Expression next = enumeration.getNext(environment);
                    if (next != null) {
                        setExpression.put((HashingExpression) next);
                    } else {
                        return;
                    }
                } finally {
                    enumeration.dispose();
                }
            }
        }
    }

    public static void removeValues(SetExpression setExpression, SetExpression setExpression2, Environment environment) throws EvaluationException {
        FrinkEnumeration enumeration;
        EnumeratingExpression values = setExpression2.getValues();
        if (values != null && (enumeration = values.getEnumeration(environment)) != null) {
            while (true) {
                try {
                    Expression next = enumeration.getNext(environment);
                    if (next != null) {
                        setExpression.remove((HashingExpression) next);
                    } else {
                        return;
                    }
                } finally {
                    enumeration.dispose();
                }
            }
        }
    }

    public static SetExpression difference(SetExpression setExpression, SetExpression setExpression2, Environment environment) throws EvaluationException {
        FrinkEnumeration enumeration;
        BasicSetExpression basicSetExpression = new BasicSetExpression();
        EnumeratingExpression values = setExpression.getValues();
        if (!(values == null || (enumeration = values.getEnumeration(environment)) == null)) {
            while (true) {
                try {
                    HashingExpression hashingExpression = (HashingExpression) enumeration.getNext(environment);
                    if (hashingExpression == null) {
                        break;
                    } else if (!setExpression2.contains(hashingExpression)) {
                        basicSetExpression.put(hashingExpression);
                    }
                } finally {
                    enumeration.dispose();
                }
            }
        }
        return basicSetExpression;
    }

    public static SetExpression intersection(SetExpression setExpression, SetExpression setExpression2, Environment environment) throws EvaluationException {
        SetExpression setExpression3;
        SetExpression setExpression4;
        FrinkEnumeration enumeration;
        BasicSetExpression basicSetExpression = new BasicSetExpression();
        if (setExpression.getSize() > setExpression2.getSize()) {
            setExpression3 = setExpression;
            setExpression4 = setExpression2;
        } else {
            setExpression3 = setExpression2;
            setExpression4 = setExpression;
        }
        EnumeratingExpression values = setExpression4.getValues();
        if (!(values == null || (enumeration = values.getEnumeration(environment)) == null)) {
            while (true) {
                try {
                    HashingExpression hashingExpression = (HashingExpression) enumeration.getNext(environment);
                    if (hashingExpression == null) {
                        break;
                    } else if (setExpression3.contains(hashingExpression)) {
                        basicSetExpression.put(hashingExpression);
                    }
                } finally {
                    enumeration.dispose();
                }
            }
        }
        return basicSetExpression;
    }

    public static boolean setsIntersect(SetExpression setExpression, SetExpression setExpression2, Environment environment) throws EvaluationException {
        SetExpression setExpression3;
        SetExpression setExpression4;
        HashingExpression hashingExpression;
        if (setExpression.getSize() == 0) {
            return false;
        }
        if (setExpression2.getSize() == 0) {
            return false;
        }
        if (setExpression.getSize() > setExpression2.getSize()) {
            setExpression3 = setExpression;
            setExpression4 = setExpression2;
        } else {
            setExpression3 = setExpression2;
            setExpression4 = setExpression;
        }
        EnumeratingExpression values = setExpression4.getValues();
        if (values == null) {
            return false;
        }
        FrinkEnumeration enumeration = values.getEnumeration(environment);
        if (enumeration == null) {
            return false;
        }
        do {
            try {
                hashingExpression = (HashingExpression) enumeration.getNext(environment);
                if (hashingExpression == null) {
                    enumeration.dispose();
                    return false;
                }
            } finally {
                enumeration.dispose();
            }
        } while (!setExpression3.contains(hashingExpression));
        return true;
    }

    /* JADX INFO: finally extract failed */
    public static boolean isSubset(SetExpression setExpression, SetExpression setExpression2, boolean z, Environment environment) throws EvaluationException {
        Expression next;
        if (z) {
            if (setExpression.getSize() >= setExpression2.getSize()) {
                return false;
            }
            if (setExpression.getSize() > setExpression2.getSize()) {
                return false;
            }
        }
        EnumeratingExpression values = setExpression.getValues();
        if (values == null) {
            return true;
        }
        FrinkEnumeration enumeration = values.getEnumeration(environment);
        if (enumeration == null) {
            return true;
        }
        do {
            try {
                next = enumeration.getNext(environment);
                if (next == null) {
                    enumeration.dispose();
                    return true;
                }
            } catch (Throwable th) {
                enumeration.dispose();
                throw th;
            }
        } while (setExpression2.contains((HashingExpression) next));
        enumeration.dispose();
        return false;
    }

    public static SetExpression makeSet(Expression expression, Environment environment) throws EvaluationException, InvalidArgumentException {
        if (expression instanceof SetExpression) {
            return (SetExpression) expression;
        }
        if (expression instanceof ListExpression) {
            return makeSetFromList((ListExpression) expression, environment);
        }
        if (expression instanceof EnumeratingExpression) {
            return makeSetFromEnum((EnumeratingExpression) expression, environment);
        }
        if (expression instanceof HashingExpression) {
            BasicSetExpression basicSetExpression = new BasicSetExpression();
            basicSetExpression.put((HashingExpression) expression);
            return basicSetExpression;
        }
        throw new InvalidArgumentException("Cannot yet convert expression of type " + expression.getExpressionType() + " to set.", expression);
    }

    public static SetExpression makeSetFromList(ListExpression listExpression, Environment environment) {
        BasicSetExpression basicSetExpression = new BasicSetExpression();
        int childCount = listExpression.getChildCount();
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = listExpression.getChild(i);
                if (child instanceof HashingExpression) {
                    basicSetExpression.put((HashingExpression) child);
                } else {
                    environment.outputln("Cannot insert " + environment.format(child) + " (" + child.getExpressionType() + ") into a set, as it's not a HashingExpression.");
                }
                i++;
            } catch (InvalidChildException e) {
                environment.outputln("SetUtils.makeSet got InvalidChildException:\n " + e);
                return null;
            }
        }
        return basicSetExpression;
    }

    public static SetExpression makeSetFromEnum(EnumeratingExpression enumeratingExpression, Environment environment) throws EvaluationException {
        BasicSetExpression basicSetExpression = new BasicSetExpression();
        FrinkEnumeration frinkEnumeration = null;
        try {
            frinkEnumeration = enumeratingExpression.getEnumeration(environment);
            if (frinkEnumeration != null) {
                while (true) {
                    Expression next = frinkEnumeration.getNext(environment);
                    if (next == null) {
                        break;
                    } else if (next instanceof HashingExpression) {
                        basicSetExpression.put((HashingExpression) next);
                    } else {
                        environment.outputln("Cannot insert " + environment.format(next) + " (" + next.getExpressionType() + ") into a set, as it's not a HashingExpression.");
                    }
                }
                if (frinkEnumeration != null) {
                    frinkEnumeration.dispose();
                }
            }
            return basicSetExpression;
        } finally {
            if (frinkEnumeration != null) {
                frinkEnumeration.dispose();
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public static boolean areEqual(SetExpression setExpression, SetExpression setExpression2) throws EvaluationException {
        Expression next;
        if (setExpression == setExpression2) {
            return true;
        }
        int size = setExpression.getSize();
        if (size != setExpression2.getSize()) {
            return false;
        }
        if (size == 0) {
            return true;
        }
        EnumeratingExpression values = setExpression.getValues();
        if (values == null) {
            return true;
        }
        FrinkEnumeration enumeration = values.getEnumeration(null);
        if (enumeration == null) {
            return true;
        }
        do {
            try {
                next = enumeration.getNext(null);
                if (next == null) {
                    enumeration.dispose();
                    return true;
                }
            } catch (Throwable th) {
                enumeration.dispose();
                throw th;
            }
        } while (setExpression2.contains((HashingExpression) next));
        enumeration.dispose();
        return false;
    }
}
