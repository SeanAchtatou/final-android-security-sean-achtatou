package frink.expr;

import frink.numeric.Numeric;
import frink.symbolic.MatchingContext;
import frink.units.DimensionList;
import frink.units.Unit;
import frink.units.UnitMath;

public final class CondensedUnitExpression extends TerminalExpression implements UnitExpression, Unit {
    private DimensionList dimensions;
    private Numeric scale;

    public CondensedUnitExpression(Numeric numeric, DimensionList dimensionList) {
        this.scale = numeric;
        this.dimensions = dimensionList;
    }

    public final boolean isConstant() {
        return true;
    }

    public final Expression evaluate(Environment environment) {
        return this;
    }

    public final Numeric getScale() {
        return this.scale;
    }

    public final DimensionList getDimensionList() {
        return this.dimensions;
    }

    public final Unit getUnit() {
        return this;
    }

    public int hashCode() {
        return this.scale.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof UnitExpression) {
            Unit unit = ((UnitExpression) obj).getUnit();
            if (!unit.getScale().equals(this.scale) || !UnitMath.areConformal(this, unit)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return equals(expression);
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }

    public String getExpressionType() {
        return UnitExpression.TYPE;
    }
}
