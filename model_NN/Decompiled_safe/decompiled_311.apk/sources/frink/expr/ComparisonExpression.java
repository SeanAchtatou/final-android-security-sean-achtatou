package frink.expr;

import frink.errors.ConformanceException;
import frink.errors.NotRealException;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;
import frink.units.UnitMath;

public class ComparisonExpression extends CachingExpression implements OperatorExpression {
    private ComparisonType cType;

    public ComparisonExpression(ComparisonType comparisonType) {
        super(2);
        this.cType = comparisonType;
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        return FrinkBoolean.create(doComparison(getChild(0).evaluate(environment), getChild(1).evaluate(environment), this.cType, environment));
    }

    public static boolean doComparison(Expression expression, Expression expression2, ComparisonType comparisonType, Environment environment) throws EvaluationException {
        if (!(expression instanceof UnitExpression) || !(expression2 instanceof UnitExpression)) {
            if (!(expression instanceof BooleanExpression) || !(expression2 instanceof BooleanExpression)) {
                if ((expression instanceof DateExpression) && (expression2 instanceof DateExpression)) {
                    try {
                        return comparisonType.comparesCorrectly(NumericMath.compare(((DateExpression) expression).getFrinkDate().getJulian(), ((DateExpression) expression2).getFrinkDate().getJulian()));
                    } catch (OverlapException e) {
                        if (comparisonType.isOverlapDefined()) {
                            return comparisonType.getOverlapValue();
                        }
                        throw new InvalidArgumentException("Comparison expression: Using operator " + comparisonType.getRepresentation() + " to compare intervals.  This operator is only defined if there is no overlap between intervals.  Please modify your program to use interval-aware comparison operators.", expression);
                    } catch (NumericException e2) {
                        throw new InvalidArgumentException("ComparisonExpression:  Cannot compare arguments:\n " + e2.toString(), expression);
                    }
                } else if ((expression instanceof StringExpression) && (expression2 instanceof StringExpression)) {
                    int compareTo = ((StringExpression) expression).getString().compareTo(((StringExpression) expression2).getString());
                    if (compareTo < 0) {
                        compareTo = -1;
                    } else if (compareTo > 0) {
                        compareTo = 1;
                    }
                    return comparisonType.comparesCorrectly(compareTo);
                } else if (!(expression instanceof SetExpression) || !(expression2 instanceof SetExpression)) {
                    if (!(expression instanceof UndefExpression) || !(expression2 instanceof UndefExpression)) {
                        if (comparisonType == ComparisonType.EQUALS) {
                            return expression.equals(expression2);
                        }
                        if (comparisonType == ComparisonType.NOT_EQUALS) {
                            return !expression.equals(expression2);
                        }
                    } else if (comparisonType == ComparisonType.EQUALS) {
                        return true;
                    } else {
                        if (comparisonType == ComparisonType.NOT_EQUALS) {
                            return false;
                        }
                    }
                } else if (comparisonType == ComparisonType.EQUALS) {
                    return SetUtils.areEqual((SetExpression) expression, (SetExpression) expression2);
                } else {
                    if (comparisonType == ComparisonType.NOT_EQUALS) {
                        return !SetUtils.areEqual((SetExpression) expression, (SetExpression) expression2);
                    }
                }
            } else if (comparisonType == ComparisonType.EQUALS) {
                return ((BooleanExpression) expression).getBoolean() == ((BooleanExpression) expression2).getBoolean();
            } else {
                if (comparisonType == ComparisonType.NOT_EQUALS) {
                    return ((BooleanExpression) expression).getBoolean() != ((BooleanExpression) expression2).getBoolean();
                }
            }
            throw new TypeException("Unsupported type in comparison ( " + comparisonType.getRepresentation() + " ): \n" + "Left is:  " + environment.format(expression) + " (" + expression.getClass().getName() + ")\n" + "Right is: " + environment.format(expression2) + " (" + expression2.getClass().getName() + ")", expression);
        }
        try {
            return comparisonType.comparesCorrectly(UnitMath.compare(((UnitExpression) expression).getUnit(), ((UnitExpression) expression2).getUnit()));
        } catch (OverlapException e3) {
            if (comparisonType.isOverlapDefined()) {
                return comparisonType.getOverlapValue();
            }
            throw new InvalidArgumentException("Comparison expression: Using operator " + comparisonType.getRepresentation() + " to compare intervals " + environment.format(expression) + " and " + environment.format(expression2) + ".\n  This operator is only defined if there is no overlap between intervals.\n  Please modify your program to use interval-aware comparison operators.", expression);
        } catch (ConformanceException e4) {
            throw new EvaluationConformanceException("ComparisonExpression.units not conformal: " + expression.toString() + ", " + expression2.toString(), expression);
        } catch (NotRealException e5) {
            if (comparisonType == ComparisonType.EQUALS) {
                return false;
            }
            if (comparisonType == ComparisonType.NOT_EQUALS) {
                return true;
            }
            throw new InvalidArgumentException("ComparisonExpression:  Cannot compare arguments:\n " + e5.toString(), expression);
        } catch (NumericException e6) {
            throw new InvalidArgumentException("ComparisonExpression:  Cannot compare arguments:\n " + e6.toString(), expression);
        }
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ComparisonExpression) {
            return this.cType == ((ComparisonExpression) expression).cType && childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return this.cType.getRepresentation();
    }

    public String getSymbol() {
        return " " + this.cType.getRepresentation() + " ";
    }

    public int getPrecedence() {
        return OperatorExpression.PREC_COMPARISON;
    }

    public int getAssociativity() {
        return -1;
    }
}
