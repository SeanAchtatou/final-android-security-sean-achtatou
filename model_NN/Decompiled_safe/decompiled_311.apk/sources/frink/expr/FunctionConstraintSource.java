package frink.expr;

import frink.function.FunctionDefinition;
import frink.function.RequiresArgumentsException;
import frink.object.FrinkObject;
import frink.units.Source;
import java.util.Enumeration;

public class FunctionConstraintSource implements Source<Constraint> {
    Environment env;

    public FunctionConstraintSource(Environment environment) {
        this.env = environment;
    }

    public String getName() {
        return "FunctionConstraintSource";
    }

    public Enumeration<String> getNames() {
        return null;
    }

    public Constraint get(String str) {
        FunctionDefinition functionDefinition;
        try {
            functionDefinition = this.env.getFunctionManager().getBestMatch(str, 1, null);
        } catch (RequiresArgumentsException e) {
            System.out.println("FunctionConstraintSource.get(String): got RequiresArgumentException when asking for " + str);
            functionDefinition = null;
        }
        if (functionDefinition == null) {
            return null;
        }
        return new FunctionConstraint(str, functionDefinition);
    }

    public class FunctionConstraint implements Constraint {
        private FunctionDefinition fd;
        private String name;

        private FunctionConstraint(String str, FunctionDefinition functionDefinition) {
            this.name = str;
            this.fd = functionDefinition;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
         arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.BasicListExpression, int, ?[OBJECT, ARRAY], int]
         candidates:
          frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
          frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
        public boolean meetsConstraint(Expression expression) {
            BasicListExpression basicListExpression = new BasicListExpression(1);
            basicListExpression.appendChild(expression);
            try {
                Expression execute = FunctionConstraintSource.this.env.getFunctionManager().execute(this.fd, FunctionConstraintSource.this.env, (Expression) basicListExpression, true, (FrinkObject) null, true);
                if (execute instanceof BooleanExpression) {
                    return ((BooleanExpression) execute).getBoolean();
                }
                return false;
            } catch (EvaluationException e) {
                return false;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
         arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.BasicListExpression, int, ?[OBJECT, ARRAY], int]
         candidates:
          frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
          frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
        public void checkConstraint(Expression expression) throws ConstraintException {
            BasicListExpression basicListExpression = new BasicListExpression(1);
            basicListExpression.appendChild(expression);
            try {
                Expression execute = FunctionConstraintSource.this.env.getFunctionManager().execute(this.fd, FunctionConstraintSource.this.env, (Expression) basicListExpression, true, (FrinkObject) null, true);
                if ((execute instanceof BooleanExpression) && !((BooleanExpression) execute).getBoolean()) {
                    throw new ConstraintException("Constraint not met: function " + this.name + "[" + FunctionConstraintSource.this.env.format(expression) + "] returned false.", expression);
                }
            } catch (ConstraintException e) {
                throw e;
            } catch (EvaluationException e2) {
                throw new ConstraintException("FunctionConstraintSource: Tried to call " + this.name + "[" + FunctionConstraintSource.this.env.format(expression) + "] but it didn't work.\n  " + e2, expression);
            }
        }

        public String getDescription() {
            return "Unit must have dimensions of " + this.name;
        }
    }
}
