package frink.expr;

public interface ConstructableExpression {
    Expression construct(String str, ListExpression listExpression, Environment environment) throws EvaluationException, InvalidChildException;
}
