package frink.expr;

import frink.symbolic.MatchingContext;

public class UndefExpression extends TerminalExpression {
    public static final String TYPE = "undef";
    public static final UndefExpression UNDEF = new UndefExpression();

    private UndefExpression() {
    }

    public boolean isConstant() {
        return true;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
