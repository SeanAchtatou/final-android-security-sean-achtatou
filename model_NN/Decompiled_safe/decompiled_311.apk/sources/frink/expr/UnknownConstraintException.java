package frink.expr;

public class UnknownConstraintException extends EvaluationException {
    public UnknownConstraintException(String str, Expression expression) {
        super("Constraint \"" + str + "\" was not found.", expression);
    }
}
