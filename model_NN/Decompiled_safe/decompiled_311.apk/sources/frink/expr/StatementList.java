package frink.expr;

import frink.symbolic.MatchingContext;

public class StatementList extends NonTerminalExpression {
    public StatementList() {
        super(0);
    }

    public void append(Expression expression) {
        appendChild(expression);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        VoidExpression voidExpression = VoidExpression.VOID;
        int childCount = getChildCount();
        Expression expression = voidExpression;
        for (int i = 0; i < childCount; i++) {
            expression = getChild(i).evaluate(environment);
        }
        return expression;
    }

    public boolean isConstant() {
        return false;
    }

    public Expression optimize() {
        if (getChildCount() != 1) {
            return new ArrayStatementList(this);
        }
        try {
            return getChild(0);
        } catch (InvalidChildException e) {
            System.out.println("Invalid child exception when creating ArrayStatementList");
            return null;
        }
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return ListExpression.TYPE;
    }
}
