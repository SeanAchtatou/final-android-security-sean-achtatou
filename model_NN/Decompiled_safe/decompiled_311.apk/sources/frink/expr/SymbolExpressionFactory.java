package frink.expr;

public class SymbolExpressionFactory implements ExpressionFactory<String> {
    public static final SymbolExpressionFactory INSTANCE = new SymbolExpressionFactory();

    public Expression makeExpression(String str, Environment environment) {
        return new SymbolExpression(str);
    }
}
