package frink.expr;

public interface DictionaryExpression {
    public static final String TYPE = "dict";

    boolean containsKey(HashingExpression hashingExpression);

    Expression get(HashingExpression hashingExpression);

    int getSize();

    Expression keys();

    void put(HashingExpression hashingExpression, Expression expression);
}
