package frink.expr;

import frink.symbolic.MatchingContext;

public class ConditionalExpression extends CachingExpression {
    public static final String TYPE = "Conditional";

    public ConditionalExpression(Expression expression, Expression expression2, Expression expression3) {
        super(3);
        appendChild(expression);
        appendChild(expression2);
        appendChild(expression3);
    }

    public ConditionalExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        if (Truth.isTrue(environment, getChild(0).evaluate(environment))) {
            return getChild(1).evaluate(environment);
        }
        if (getChildCount() == 3) {
            return getChild(2).evaluate(environment);
        }
        return VoidExpression.VOID;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ConditionalExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
