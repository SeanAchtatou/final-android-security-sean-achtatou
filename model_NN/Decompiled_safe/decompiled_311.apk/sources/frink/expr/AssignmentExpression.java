package frink.expr;

import frink.symbolic.MatchingContext;

public class AssignmentExpression implements Expression, OperatorExpression {
    public static final String TYPE = "Assignment";
    private AssignableExpression left;
    private Expression right;

    public AssignmentExpression(AssignableExpression assignableExpression, Expression expression) {
        this.left = assignableExpression;
        this.right = expression;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        Expression evaluate = this.right.evaluate(environment);
        try {
            this.left.assign(evaluate, environment);
            return evaluate;
        } catch (CannotAssignException e) {
            throw new CannotAssignException(e.getMessage(), this);
        }
    }

    public int getChildCount() {
        return 2;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.left;
        }
        if (i == 1) {
            return this.right;
        }
        throw new InvalidChildException("AssignmentExpression: bad child " + i, this);
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        int i;
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof AssignmentExpression)) {
            return false;
        }
        AssignmentExpression assignmentExpression = (AssignmentExpression) expression;
        if (z) {
            i = matchingContext.beginMatch();
        } else {
            i = 0;
        }
        try {
            return this.left.structureEquals(assignmentExpression.left, matchingContext, environment, z) && this.right.structureEquals(assignmentExpression.right, matchingContext, environment, z);
        } finally {
            if (z) {
                matchingContext.rollbackMatch(i);
            }
        }
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String getSymbol() {
        return " = ";
    }

    public int getPrecedence() {
        return 30;
    }

    public int getAssociativity() {
        return -1;
    }
}
