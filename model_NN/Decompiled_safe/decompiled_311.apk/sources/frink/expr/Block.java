package frink.expr;

import frink.symbolic.MatchingContext;

public class Block implements Expression {
    public static final String TYPE = "Block";
    private Expression child;

    public Block(Expression expression) {
        this.child = expression;
    }

    public int getChildCount() {
        return 1;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.child;
        }
        throw new InvalidChildException("Block: requested invalid child " + i, this);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this.child.evaluate(environment);
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
