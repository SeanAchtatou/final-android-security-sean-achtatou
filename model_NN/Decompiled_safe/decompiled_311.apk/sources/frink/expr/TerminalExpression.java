package frink.expr;

public abstract class TerminalExpression implements Expression {
    public final int getChildCount() {
        return 0;
    }

    public Expression getChild(int i) throws InvalidChildException {
        throw new InvalidChildException("Terminal expression never has children", this);
    }
}
