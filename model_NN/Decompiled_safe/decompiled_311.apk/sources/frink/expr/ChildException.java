package frink.expr;

public class ChildException extends EvaluationException {
    public ChildException(String str, Expression expression) {
        super(str, expression);
    }
}
