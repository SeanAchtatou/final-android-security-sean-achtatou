package frink.expr;

public interface DeepCopyable {
    Expression deepCopy(int i);
}
