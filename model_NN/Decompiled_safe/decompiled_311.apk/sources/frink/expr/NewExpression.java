package frink.expr;

import frink.object.NoSuchObjectException;
import frink.symbolic.MatchingContext;

public class NewExpression extends NonTerminalExpression {
    public static final String TYPE = "new";
    private String name;

    public NewExpression(String str) {
        super(1);
        this.name = str;
        appendChild(new BasicStringExpression(str));
    }

    public NewExpression(String str, ListExpression listExpression) {
        super(2);
        this.name = str;
        appendChild(new BasicStringExpression(str));
        appendChild(listExpression);
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        Expression expression = null;
        if (getChildCount() >= 2) {
            expression = getChild(1).evaluate(environment);
        }
        Expression construct = environment.getObjectManager().construct(this.name, expression, environment);
        if (construct != null) {
            return construct;
        }
        StringBuffer stringBuffer = new StringBuffer("Could not construct object: " + this.name);
        if (expression != null) {
            stringBuffer.append(environment.format(expression));
        } else {
            stringBuffer.append("[]");
        }
        stringBuffer.append("\n  No matching constructor.");
        throw new NoSuchObjectException(new String(stringBuffer), this);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return "new";
    }
}
