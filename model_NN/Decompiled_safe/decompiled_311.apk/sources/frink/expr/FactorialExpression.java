package frink.expr;

import frink.errors.NotAnIntegerException;
import frink.function.BuiltinFunctionSource;
import frink.function.Factorial;
import frink.symbolic.MatchingContext;

public class FactorialExpression extends CachingExpression implements OperatorExpression {
    public static final String TYPE = "Factorial";

    public FactorialExpression(Expression expression) {
        super(1);
        appendChild(expression);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        if (getChildCount() != 1) {
            throw new InvalidArgumentException("FactorialExpression requires 1 argument", this);
        }
        try {
            Expression evaluate = getChild(0).evaluate(environment);
            if ((evaluate instanceof SymbolExpression) && !environment.getSymbolicMode()) {
                environment.outputln("Warning: undefined symbol \"" + ((SymbolExpression) evaluate).getName() + "\".");
            }
            if (!(evaluate instanceof UnitExpression)) {
                return this;
            }
            try {
                int integerValue = BuiltinFunctionSource.getIntegerValue(evaluate);
                if (integerValue >= 0) {
                    return DimensionlessUnitExpression.construct(Factorial.factorial(integerValue));
                }
                throw new InvalidArgumentException("Argument to factorial must be a dimensionless, non-negative integer.  Argument was " + integerValue, evaluate);
            } catch (NotAnIntegerException e) {
                throw new InvalidArgumentException("Argument to factorial must be a dimensionless, non-negative integer", evaluate);
            }
        } catch (InvalidChildException e2) {
            throw new InvalidArgumentException("FactorialExpression: trouble getting child", this);
        }
    }

    public String getSymbol() {
        return "!";
    }

    public int getPrecedence() {
        return OperatorExpression.PREC_FACTORIAL;
    }

    public int getAssociativity() {
        return -1;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof FactorialExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
