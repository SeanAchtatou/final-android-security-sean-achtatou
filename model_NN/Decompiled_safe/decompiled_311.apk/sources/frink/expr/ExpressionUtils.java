package frink.expr;

import frink.units.UnitMath;

public class ExpressionUtils {
    public static boolean isDimensionless(Expression expression) {
        if (!(expression instanceof UnitExpression) || !UnitMath.isDimensionless(((UnitExpression) expression).getUnit())) {
            return false;
        }
        return true;
    }
}
