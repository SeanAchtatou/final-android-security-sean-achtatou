package frink.expr;

public class FrinkSecurityException extends EvaluationException {
    public FrinkSecurityException(String str, Expression expression) {
        super(str, expression);
    }
}
