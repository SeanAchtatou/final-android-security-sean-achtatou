package frink.expr;

import frink.date.DateMath;
import frink.date.FrinkDate;
import frink.date.NegativeJulianDate;
import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.numeric.NumericException;
import frink.symbolic.BasicMatchingContext;
import frink.symbolic.MatchingContext;
import frink.symbolic.SymbolicUtils;
import frink.units.Unit;
import frink.units.UnitMath;

public class AddExpression extends CachingExpression implements OperatorExpression {
    private static final boolean DEBUG = false;
    public static final String TYPE = "Add";
    private boolean canonicalized;
    private int nextToFlatten;

    public static final Expression subtract(Expression expression, Expression expression2, Environment environment) throws ConformanceException, NumericException, InvalidChildException {
        return construct(expression, MultiplyExpression.negate(expression2, environment), environment);
    }

    public static final Expression construct(Expression expression, Expression expression2, Environment environment) throws InvalidChildException, ConformanceException, NumericException {
        BasicListExpression basicListExpression = new BasicListExpression(2);
        basicListExpression.appendChild(expression);
        basicListExpression.appendChild(expression2);
        return construct(basicListExpression, environment);
    }

    public static final Expression constructConservatively(Expression expression, Expression expression2, Environment environment) throws InvalidChildException, ConformanceException, NumericException {
        if (!(expression instanceof UnitExpression) || !(expression2 instanceof UnitExpression)) {
            return new AddExpression(expression, expression2);
        }
        BasicListExpression basicListExpression = new BasicListExpression(2);
        basicListExpression.appendChild(expression);
        basicListExpression.appendChild(expression2);
        return construct(basicListExpression, environment);
    }

    public static final Expression construct(ListExpression listExpression, Environment environment) throws InvalidChildException, ConformanceException, NumericException {
        BasicListExpression basicListExpression;
        int i;
        BasicListExpression basicListExpression2;
        BasicListExpression basicListExpression3;
        Unit unit;
        BasicListExpression basicListExpression4;
        Unit unit2;
        int childCount = listExpression.getChildCount();
        if (childCount == 1) {
            return listExpression.getChild(0);
        }
        int i2 = 0;
        BasicListExpression basicListExpression5 = null;
        Unit unit3 = null;
        while (i2 < childCount) {
            Expression child = listExpression.getChild(i2);
            if (child instanceof AddExpression) {
                ((AddExpression) child).flatten();
                int childCount2 = child.getChildCount();
                Unit unit4 = unit3;
                BasicListExpression basicListExpression6 = basicListExpression5;
                int i3 = 0;
                while (i3 < childCount2) {
                    Expression child2 = child.getChild(i3);
                    if (!(child2 instanceof UnitExpression)) {
                        if (basicListExpression6 == null) {
                            basicListExpression6 = new BasicListExpression(1);
                        }
                        basicListExpression6.appendChild(child2);
                        basicListExpression4 = basicListExpression6;
                        unit2 = unit4;
                    } else if (unit4 == null) {
                        BasicListExpression basicListExpression7 = basicListExpression6;
                        unit2 = ((UnitExpression) child2).getUnit();
                        basicListExpression4 = basicListExpression7;
                    } else {
                        BasicListExpression basicListExpression8 = basicListExpression6;
                        unit2 = UnitMath.add(unit4, ((UnitExpression) child2).getUnit());
                        basicListExpression4 = basicListExpression8;
                    }
                    i3++;
                    unit4 = unit2;
                    basicListExpression6 = basicListExpression4;
                }
                basicListExpression3 = basicListExpression6;
                unit = unit4;
            } else if (!(child instanceof UnitExpression)) {
                if (basicListExpression5 == null) {
                    basicListExpression3 = new BasicListExpression(1);
                } else {
                    basicListExpression3 = basicListExpression5;
                }
                basicListExpression3.appendChild(child);
                unit = unit3;
            } else if (unit3 == null) {
                unit = ((UnitExpression) child).getUnit();
                basicListExpression3 = basicListExpression5;
            } else {
                unit = UnitMath.add(unit3, ((UnitExpression) child).getUnit());
                basicListExpression3 = basicListExpression5;
            }
            i2++;
            basicListExpression5 = basicListExpression3;
            unit3 = unit;
        }
        if (unit3 != null) {
            UnitExpression construct = BasicUnitExpression.construct(unit3);
            if (basicListExpression5 == null) {
                return construct;
            }
            try {
                if (UnitMath.getIntegerValue(unit3) == 0) {
                    i = 0;
                } else {
                    i = 1;
                }
            } catch (NotAnIntegerException e) {
                i = 1;
            }
            int childCount3 = basicListExpression5.getChildCount();
            if (childCount3 == 1 && i == 0) {
                return basicListExpression5.getChild(0);
            }
            if (childCount3 > 1) {
                SymbolicUtils.sortOperands(basicListExpression5, environment);
                BasicListExpression combineOperands = combineOperands(basicListExpression5, environment);
                basicListExpression2 = combineOperands;
                childCount3 = combineOperands.getChildCount();
            } else {
                basicListExpression2 = basicListExpression5;
            }
            if (childCount3 == 1 && (basicListExpression2.getChild(0) instanceof UnitExpression)) {
                return construct(construct, basicListExpression2.getChild(0), environment);
            }
            AddExpression addExpression = new AddExpression(i + childCount3);
            if (i == 1) {
                addExpression.appendChild(construct);
            }
            for (int i4 = 0; i4 < childCount3; i4++) {
                addExpression.appendChild(basicListExpression2.getChild(i4));
            }
            addExpression.nextToFlatten = addExpression.getChildCount();
            addExpression.canonicalized = true;
            return addExpression;
        }
        int childCount4 = basicListExpression5.getChildCount();
        if (childCount4 > 1) {
            SymbolicUtils.sortOperands(basicListExpression5, environment);
            BasicListExpression combineOperands2 = combineOperands(basicListExpression5, environment);
            basicListExpression = combineOperands2;
            childCount4 = combineOperands2.getChildCount();
        } else {
            basicListExpression = basicListExpression5;
        }
        if (childCount4 == 1) {
            return basicListExpression.getChild(0);
        }
        AddExpression addExpression2 = new AddExpression(childCount4);
        for (int i5 = 0; i5 < childCount4; i5++) {
            addExpression2.appendChild(basicListExpression.getChild(i5));
        }
        addExpression2.nextToFlatten = addExpression2.getChildCount();
        addExpression2.canonicalized = true;
        return addExpression2;
    }

    private AddExpression(Expression expression, Expression expression2) {
        this(2);
        flattenAndAppend(expression);
        flattenAndAppend(expression2);
        this.nextToFlatten = getChildCount();
    }

    private AddExpression(int i) {
        super(i);
        this.nextToFlatten = 0;
        this.canonicalized = DEBUG;
        this.nextToFlatten = 0;
    }

    private static BasicListExpression combineOperands(BasicListExpression basicListExpression, Environment environment) throws InvalidChildException, NumericException, ConformanceException {
        BasicMatchingContext basicMatchingContext;
        int i;
        Expression expression;
        boolean z;
        Expression expression2;
        int i2;
        Expression expression3;
        boolean z2;
        Expression expression4;
        boolean structureEquals;
        BasicListExpression basicListExpression2;
        int i3;
        int i4;
        BasicListExpression basicListExpression3;
        BasicListExpression basicListExpression4;
        BasicListExpression basicListExpression5;
        if (SymbolicUtils.containsPattern(basicListExpression)) {
            return basicListExpression;
        }
        int childCount = basicListExpression.getChildCount() - 1;
        BasicListExpression basicListExpression6 = null;
        boolean z3 = false;
        int i5 = 1;
        int i6 = -1;
        BasicListExpression basicListExpression7 = null;
        Expression expression5 = null;
        int i7 = -1;
        BasicMatchingContext basicMatchingContext2 = null;
        Expression expression6 = null;
        int i8 = 0;
        while (i8 < childCount && i5 <= childCount) {
            if (basicMatchingContext2 == null) {
                basicMatchingContext = new BasicMatchingContext();
            } else {
                basicMatchingContext = basicMatchingContext2;
            }
            Expression child = basicListExpression.getChild(i8);
            Expression child2 = basicListExpression.getChild(i5);
            int childCount2 = child.getChildCount();
            int childCount3 = child2.getChildCount();
            if (!(child instanceof MultiplyExpression) || childCount2 < 2) {
                i = 0;
                expression = DimensionlessUnitExpression.ONE;
                z = false;
                expression2 = child;
            } else {
                z = true;
                if (child.getChild(0) instanceof UnitExpression) {
                    i = 1;
                    expression = child.getChild(0);
                    expression2 = child.getChild(1);
                } else {
                    i = 0;
                    expression = DimensionlessUnitExpression.ONE;
                    expression2 = child.getChild(0);
                }
            }
            if (!(child2 instanceof MultiplyExpression) || childCount3 < 2) {
                i2 = 0;
                expression3 = DimensionlessUnitExpression.ONE;
                z2 = false;
                expression4 = child2;
            } else {
                z2 = true;
                if (child2.getChild(0) instanceof UnitExpression) {
                    i2 = 1;
                    expression3 = child2.getChild(0);
                    expression4 = child2.getChild(1);
                } else {
                    i2 = 0;
                    expression3 = DimensionlessUnitExpression.ONE;
                    expression4 = child2.getChild(0);
                }
            }
            if (!z) {
                structureEquals = z2 ? (childCount3 - i2 != 1 || !expression2.structureEquals(expression4, basicMatchingContext, environment, true)) ? DEBUG : true : expression2.structureEquals(expression4, basicMatchingContext, environment, true);
            } else if (!z2) {
                structureEquals = (childCount2 - i != 1 || !expression2.structureEquals(expression4, basicMatchingContext, environment, true)) ? DEBUG : true;
            } else if (childCount2 - i == childCount3 - i2) {
                int i9 = 0;
                while (true) {
                    if (i9 >= childCount2 - i) {
                        structureEquals = true;
                        break;
                    }
                    if (!child.getChild(i + i9).structureEquals(child2.getChild(i2 + i9), basicMatchingContext, environment, true)) {
                        structureEquals = DEBUG;
                        break;
                    }
                    i9++;
                }
            } else {
                structureEquals = DEBUG;
            }
            if (structureEquals) {
                if (basicListExpression6 == null) {
                    basicListExpression4 = new BasicListExpression(i8);
                    for (int i10 = 0; i10 < i8; i10++) {
                        basicListExpression4.appendChild(basicListExpression.getChild(i10));
                    }
                } else {
                    basicListExpression4 = basicListExpression6;
                }
                if (basicListExpression7 == null) {
                    basicListExpression5 = new BasicListExpression(2);
                    basicListExpression5.appendChild(expression);
                } else {
                    basicListExpression5 = basicListExpression7;
                }
                basicListExpression5.appendChild(expression3);
                int i11 = i5 + 1;
                basicListExpression3 = basicListExpression4;
                basicListExpression2 = basicListExpression5;
                i3 = i11;
                i4 = i8;
            } else {
                if (basicListExpression6 != null) {
                    if (basicListExpression7 != null) {
                        if (z) {
                            BasicListExpression basicListExpression8 = new BasicListExpression(childCount2);
                            basicListExpression8.appendChild(construct(basicListExpression7, environment));
                            for (int i12 = i; i12 < childCount2; i12++) {
                                basicListExpression8.appendChild(child.getChild(i12));
                            }
                            basicListExpression6.appendChild(MultiplyExpression.construct(basicListExpression8, environment));
                        } else {
                            basicListExpression6.appendChild(MultiplyExpression.construct(construct(basicListExpression7, environment), expression2, environment));
                        }
                        basicListExpression2 = null;
                        i3 = i5 + 1;
                        i4 = i5;
                        basicListExpression3 = basicListExpression6;
                    } else {
                        basicListExpression6.appendChild(child);
                    }
                }
                basicListExpression2 = basicListExpression7;
                i3 = i5 + 1;
                i4 = i5;
                basicListExpression3 = basicListExpression6;
            }
            i8 = i4;
            basicListExpression6 = basicListExpression3;
            basicListExpression7 = basicListExpression2;
            i5 = i3;
            expression5 = child;
            i6 = childCount2;
            i7 = i;
            expression6 = expression2;
            boolean z4 = z;
            basicMatchingContext2 = basicMatchingContext;
            z3 = z4;
        }
        if (basicListExpression6 == null) {
            return basicListExpression;
        }
        if (basicListExpression7 == null) {
            basicListExpression6.appendChild(basicListExpression.getChild(childCount));
        } else if (z3) {
            BasicListExpression basicListExpression9 = new BasicListExpression(i6);
            basicListExpression9.appendChild(construct(basicListExpression7, environment));
            for (int i13 = i7; i13 < i6; i13++) {
                basicListExpression9.appendChild(expression5.getChild(i13));
            }
            basicListExpression6.appendChild(MultiplyExpression.construct(basicListExpression9, environment));
        } else {
            basicListExpression6.appendChild(MultiplyExpression.construct(construct(basicListExpression7, environment), expression6, environment));
        }
        return basicListExpression6;
    }

    private void flattenAndAppend(Expression expression) {
        if (expression instanceof AddExpression) {
            try {
                ((AddExpression) expression).flatten();
                int childCount = expression.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    appendChild(expression.getChild(i));
                }
            } catch (InvalidChildException e) {
                System.out.println("AddExpression.flattenAndAppend got bad child.");
            }
        } else {
            appendChild(expression);
        }
        this.canonicalized = DEBUG;
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        Unit unit;
        StringBuffer stringBuffer;
        FrinkDate frinkDate;
        StringBuffer stringBuffer2;
        StringBuffer stringBuffer3;
        BasicListExpression basicListExpression;
        Unit unit2;
        FrinkDate frinkDate2;
        BasicListExpression basicListExpression2;
        StringBuffer stringBuffer4;
        Unit unit3;
        int childCount = getChildCount();
        if (this.nextToFlatten < childCount) {
            flatten();
        }
        int i = 0;
        Unit unit4 = null;
        StringBuffer stringBuffer5 = null;
        FrinkDate frinkDate3 = null;
        BasicListExpression basicListExpression3 = null;
        while (i < childCount) {
            try {
                Expression evaluate = getChild(i).evaluate(environment);
                if (evaluate instanceof UnitExpression) {
                    unit2 = ((UnitExpression) evaluate).getUnit();
                    if (unit4 != null) {
                        try {
                            unit2 = UnitMath.add(unit4, unit2);
                            if (childCount == 2) {
                                return BasicUnitExpression.construct(unit2);
                            }
                        } catch (ConformanceException e) {
                            throw new EvaluationConformanceException(e.getMessage(), this);
                        } catch (NumericException e2) {
                            throw new EvaluationNumericException(e2.getMessage(), this);
                        }
                    }
                    stringBuffer4 = stringBuffer5;
                    frinkDate2 = frinkDate3;
                    basicListExpression2 = basicListExpression3;
                } else if (evaluate instanceof StringExpression) {
                    if (stringBuffer5 == null) {
                        stringBuffer4 = new StringBuffer();
                    } else {
                        stringBuffer4 = stringBuffer5;
                    }
                    if (basicListExpression3 != null) {
                        if (basicListExpression3.getChildCount() == 1) {
                            stringBuffer4.append(environment.format(basicListExpression3.getChild(0)));
                        } else {
                            stringBuffer4.append(environment.format(basicListExpression3));
                        }
                        basicListExpression2 = null;
                    } else {
                        basicListExpression2 = basicListExpression3;
                    }
                    if (unit4 != null) {
                        if (frinkDate3 != null) {
                            try {
                                frinkDate3 = DateMath.add(frinkDate3, unit4, environment.getUnitManager());
                                unit4 = null;
                            } catch (ConformanceException e3) {
                                stringBuffer4.append(environment.format(BasicUnitExpression.construct(unit4)));
                                stringBuffer4.append(environment.format(new BasicDateExpression(frinkDate3)));
                                unit4 = null;
                                frinkDate3 = null;
                            } catch (NumericException e4) {
                                stringBuffer4.append(environment.format(BasicUnitExpression.construct(unit4)));
                                stringBuffer4.append(environment.format(new BasicDateExpression(frinkDate3)));
                                unit4 = null;
                                frinkDate3 = null;
                            }
                        } else {
                            stringBuffer4.append(environment.format(BasicUnitExpression.construct(unit4)));
                            unit4 = null;
                        }
                    }
                    if (frinkDate3 != null) {
                        stringBuffer4.append(environment.format(new BasicDateExpression(frinkDate3)));
                        frinkDate3 = null;
                    }
                    stringBuffer4.append(((StringExpression) evaluate).getString());
                    unit2 = unit4;
                    frinkDate2 = frinkDate3;
                } else if (!(evaluate instanceof DateExpression)) {
                    if ((evaluate instanceof SymbolExpression) && !environment.getSymbolicMode()) {
                        environment.outputln("Warning: undefined symbol \"" + ((SymbolExpression) evaluate).getName() + "\".");
                    }
                    if (basicListExpression3 == null) {
                        basicListExpression = new BasicListExpression(1);
                    } else {
                        basicListExpression = basicListExpression3;
                    }
                    basicListExpression.appendChild(evaluate);
                    unit2 = unit4;
                    frinkDate2 = frinkDate3;
                    StringBuffer stringBuffer6 = stringBuffer5;
                    basicListExpression2 = basicListExpression;
                    stringBuffer4 = stringBuffer6;
                } else if (frinkDate3 == null) {
                    stringBuffer4 = stringBuffer5;
                    basicListExpression2 = basicListExpression3;
                    FrinkDate frinkDate4 = ((DateExpression) evaluate).getFrinkDate();
                    unit2 = unit4;
                    frinkDate2 = frinkDate4;
                } else {
                    FrinkDate frinkDate5 = ((DateExpression) evaluate).getFrinkDate();
                    if ((frinkDate5 instanceof NegativeJulianDate) || (frinkDate3 instanceof NegativeJulianDate)) {
                        try {
                            if ((frinkDate5 instanceof NegativeJulianDate) && !(frinkDate3 instanceof NegativeJulianDate)) {
                                unit3 = DateMath.subtract(frinkDate3, frinkDate5, environment.getUnitManager());
                            } else if (!(frinkDate5 instanceof NegativeJulianDate)) {
                                unit3 = DateMath.subtract(frinkDate5, frinkDate3, environment.getUnitManager());
                            } else {
                                unit3 = null;
                            }
                            if (unit3 == null) {
                                throw new InvalidArgumentException("Could not add multiple negative dates in " + environment.format(this), this);
                            }
                            if (unit4 != null) {
                                unit3 = UnitMath.add(unit4, unit3);
                            }
                            stringBuffer4 = stringBuffer5;
                            frinkDate2 = null;
                            basicListExpression2 = basicListExpression3;
                        } catch (ConformanceException e5) {
                            throw new EvaluationConformanceException(e5.getMessage(), this);
                        } catch (NumericException e6) {
                            throw new EvaluationNumericException(e6.getMessage(), this);
                        }
                    } else {
                        throw new InvalidArgumentException("Cannot add a date to a date in " + environment.format(this), this);
                    }
                }
                i++;
                frinkDate3 = frinkDate2;
                basicListExpression3 = basicListExpression2;
                unit4 = unit2;
                stringBuffer5 = stringBuffer4;
            } catch (InvalidChildException e7) {
                throw new InvalidArgumentException("Error getting child " + i, this);
            }
        }
        if (frinkDate3 == null || unit4 == null) {
            unit = unit4;
            stringBuffer = stringBuffer5;
            frinkDate = frinkDate3;
        } else {
            try {
                stringBuffer = stringBuffer5;
                frinkDate = DateMath.add(frinkDate3, unit4, environment.getUnitManager());
                unit = null;
            } catch (ConformanceException e8) {
                if (stringBuffer5 == null) {
                    stringBuffer3 = new StringBuffer();
                } else {
                    stringBuffer3 = stringBuffer5;
                }
                stringBuffer3.append(environment.format(BasicUnitExpression.construct(unit4)));
                stringBuffer3.append(environment.format(new BasicDateExpression(frinkDate3)));
                stringBuffer = stringBuffer3;
                frinkDate = null;
                unit = null;
            } catch (NumericException e9) {
                if (stringBuffer5 == null) {
                    stringBuffer2 = new StringBuffer();
                } else {
                    stringBuffer2 = stringBuffer5;
                }
                stringBuffer2.append(environment.format(BasicUnitExpression.construct(unit4)));
                stringBuffer2.append(environment.format(new BasicDateExpression(frinkDate3)));
                stringBuffer = stringBuffer2;
                frinkDate = null;
                unit = null;
            }
        }
        if (stringBuffer != null) {
            if (frinkDate != null && unit == null) {
                stringBuffer.append(environment.format(new BasicDateExpression(frinkDate)));
            }
            if (basicListExpression3 != null) {
                if (basicListExpression3.getChildCount() == 1) {
                    stringBuffer.append(environment.format(basicListExpression3.getChild(0)));
                } else {
                    stringBuffer.append(environment.format(basicListExpression3));
                }
            }
            if (unit != null) {
                stringBuffer.append(environment.format(BasicUnitExpression.construct(unit)));
            }
            return new BasicStringExpression(new String(stringBuffer));
        } else if (basicListExpression3 == null) {
            if (frinkDate == null) {
                return BasicUnitExpression.construct(unit);
            }
            return new BasicDateExpression(frinkDate);
        } else if (frinkDate == null) {
            if (unit != null) {
                try {
                    if (UnitMath.getIntegerValue(unit) == 0) {
                        if (basicListExpression3.getChildCount() == 1) {
                            return basicListExpression3.getChild(0);
                        }
                        return construct(basicListExpression3, environment);
                    }
                } catch (ConformanceException e10) {
                    throw new EvaluationConformanceException(e10.getMessage(), this);
                } catch (NumericException e11) {
                    throw new EvaluationNumericException(e11.getMessage(), this);
                } catch (NotAnIntegerException e12) {
                }
                basicListExpression3.insertBefore(0, BasicUnitExpression.construct(unit));
            }
            try {
                return construct(basicListExpression3, environment);
            } catch (ConformanceException e13) {
                throw new EvaluationConformanceException(e13.getMessage(), this);
            } catch (NumericException e14) {
                throw new EvaluationNumericException(e14.getMessage(), this);
            }
        } else {
            basicListExpression3.appendChild(new BasicDateExpression(frinkDate));
            try {
                return construct(basicListExpression3, environment);
            } catch (ConformanceException e15) {
                throw new EvaluationConformanceException(e15.getMessage(), this);
            } catch (NumericException e16) {
                throw new EvaluationNumericException(e16.getMessage(), this);
            }
        }
    }

    private void flatten() throws InvalidChildException {
        int childCount = getChildCount();
        if (this.nextToFlatten < childCount) {
            this.canonicalized = DEBUG;
            clearCache();
            for (int i = this.nextToFlatten; i < childCount; i++) {
                Expression child = getChild(i);
                if (child instanceof AddExpression) {
                    AddExpression addExpression = (AddExpression) child;
                    addExpression.flatten();
                    int childCount2 = addExpression.getChildCount();
                    replaceChild(i, addExpression.getChild(0));
                    for (int i2 = 1; i2 < childCount2; i2++) {
                        insertBefore(i + i2, addExpression.getChild(i2));
                    }
                }
            }
            this.nextToFlatten = getChildCount();
        }
    }

    private void canonicalize(Environment environment) throws InvalidChildException {
        int childCount = getChildCount();
        if (this.nextToFlatten < childCount) {
            flatten();
        }
        BasicListExpression basicListExpression = new BasicListExpression(childCount);
        for (int i = 0; i < childCount; i++) {
            basicListExpression.appendChild(getChild(i));
        }
        try {
            SymbolicUtils.sortOperands(basicListExpression, environment);
            replaceChildren(combineOperands(basicListExpression, environment));
            this.canonicalized = true;
        } catch (NumericException e) {
            System.err.println("AddExpression:  NumericException in canonicalize:\n  " + e);
        } catch (ConformanceException e2) {
            System.err.println("AddExpression:  Conformance in canonicalize:\n  " + e2);
        }
    }

    public String getSymbol() {
        return " + ";
    }

    public int getPrecedence() {
        return OperatorExpression.PREC_ADD;
    }

    public int getAssociativity() {
        return -1;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        try {
            if (this.nextToFlatten < getChildCount()) {
                flatten();
                canonicalize(environment);
            } else if (!this.canonicalized) {
                canonicalize(environment);
            }
            if (!(expression instanceof AddExpression)) {
                return DEBUG;
            }
            AddExpression addExpression = (AddExpression) expression;
            if (addExpression.nextToFlatten < addExpression.getChildCount()) {
                addExpression.flatten();
                addExpression.canonicalize(environment);
            } else if (!addExpression.canonicalized) {
                addExpression.canonicalize(environment);
            }
            return childrenEqualPermuted(addExpression, matchingContext, environment, z);
        } catch (InvalidChildException e) {
            System.err.println("InvalidChildException in AddExpression.structureEquals:\n  " + e);
            return DEBUG;
        }
    }

    public String getExpressionType() {
        return TYPE;
    }
}
