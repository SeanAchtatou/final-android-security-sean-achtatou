package frink.expr;

import frink.date.FrinkDateFormatter;
import frink.symbolic.MatchingContext;

public final class BasicDateFormatterExpression extends TerminalExpression implements DateFormatterExpression {
    private FrinkDateFormatter formatter;

    public BasicDateFormatterExpression(FrinkDateFormatter frinkDateFormatter) {
        this.formatter = frinkDateFormatter;
    }

    public final Expression evaluate(Environment environment) {
        return this;
    }

    public final boolean isConstant() {
        return true;
    }

    public FrinkDateFormatter getFormatter() {
        return this.formatter;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof DateFormatterExpression) {
            return this.formatter.equals(((DateFormatterExpression) expression).getFormatter());
        }
        return false;
    }

    public String getExpressionType() {
        return DateFormatterExpression.TYPE;
    }
}
