package frink.expr;

import frink.symbolic.MatchingContext;

public class SolveExpression implements Expression, OperatorExpression {
    public static final String TYPE = "Solve";
    private Expression left;
    private Expression right;

    public SolveExpression(Expression expression, Expression expression2) {
        this.left = expression;
        this.right = expression2;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public int getChildCount() {
        return 2;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i == 0) {
            return this.left;
        }
        if (i == 1) {
            return this.right;
        }
        throw new InvalidChildException("SolveExpression: bad child " + i, this);
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        int i;
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof SolveExpression)) {
            return false;
        }
        SolveExpression solveExpression = (SolveExpression) expression;
        if (z) {
            i = matchingContext.beginMatch();
        } else {
            i = 0;
        }
        try {
            return this.left.structureEquals(solveExpression.left, matchingContext, environment, z) && this.right.structureEquals(solveExpression.right, matchingContext, environment, z);
        } finally {
            if (z) {
                matchingContext.rollbackMatch(i);
            }
        }
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String getSymbol() {
        return " === ";
    }

    public int getPrecedence() {
        return 40;
    }

    public int getAssociativity() {
        return -1;
    }
}
