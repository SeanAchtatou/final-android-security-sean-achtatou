package frink.expr;

import frink.errors.ConformanceException;
import frink.numeric.NumericException;
import frink.symbolic.MatchingContext;
import frink.units.UnitMath;

public class PowerExpression extends CachingExpression implements OperatorExpression {
    public static final String TYPE = "Power";

    public static Expression reciprocal(Expression expression, Environment environment) throws NumericException {
        return construct(expression, DimensionlessUnitExpression.NEGATIVE_ONE, environment);
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static frink.expr.Expression construct(frink.expr.Expression r10, frink.expr.Expression r11, frink.expr.Environment r12) throws frink.numeric.NumericException {
        /*
            r2 = 1
            int r1 = frink.function.BuiltinFunctionSource.getIntegerValue(r11)     // Catch:{ NotAnIntegerException -> 0x002c }
            if (r1 != r2) goto L_0x0009
            r1 = r10
        L_0x0008:
            return r1
        L_0x0009:
            if (r1 != 0) goto L_0x000e
            frink.expr.DimensionlessUnitExpression r1 = frink.expr.DimensionlessUnitExpression.ONE     // Catch:{ NotAnIntegerException -> 0x002c }
            goto L_0x0008
        L_0x000e:
            r2 = -1
            if (r1 != r2) goto L_0x002d
            boolean r1 = r10 instanceof frink.expr.UnitExpression     // Catch:{ NotAnIntegerException -> 0x002c }
            if (r1 == 0) goto L_0x002d
            int r1 = frink.function.BuiltinFunctionSource.getIntegerValue(r10)     // Catch:{ NotAnIntegerException -> 0x002c }
            if (r1 == 0) goto L_0x002d
            r0 = r10
            frink.expr.UnitExpression r0 = (frink.expr.UnitExpression) r0     // Catch:{ NotAnIntegerException -> 0x002c }
            r1 = r0
            frink.units.Unit r1 = r1.getUnit()     // Catch:{ NotAnIntegerException -> 0x002c }
            frink.units.Unit r1 = frink.units.UnitMath.reciprocal(r1)     // Catch:{ NotAnIntegerException -> 0x002c }
            frink.expr.UnitExpression r1 = frink.expr.BasicUnitExpression.construct(r1)     // Catch:{ NotAnIntegerException -> 0x002c }
            goto L_0x0008
        L_0x002c:
            r1 = move-exception
        L_0x002d:
            boolean r1 = r10 instanceof frink.expr.PowerExpression
            if (r1 == 0) goto L_0x005b
            r1 = 0
            frink.expr.Expression r1 = r10.getChild(r1)     // Catch:{ InvalidChildException -> 0x0044, ConformanceException -> 0x00e1 }
            r2 = 1
            frink.expr.Expression r2 = r10.getChild(r2)     // Catch:{ InvalidChildException -> 0x0044, ConformanceException -> 0x00e1 }
            frink.expr.Expression r2 = frink.expr.MultiplyExpression.construct(r2, r11, r12)     // Catch:{ InvalidChildException -> 0x0044, ConformanceException -> 0x00e1 }
            frink.expr.Expression r1 = construct(r1, r2, r12)     // Catch:{ InvalidChildException -> 0x0044, ConformanceException -> 0x00e1 }
            goto L_0x0008
        L_0x0044:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "PowerExpression.construct:  Unexpected InvalidChildException:\n  "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            r12.outputln(r1)
        L_0x005b:
            boolean r1 = r11 instanceof frink.expr.UnitExpression
            if (r1 == 0) goto L_0x0150
            boolean r1 = r10 instanceof frink.expr.UnitExpression
            if (r1 == 0) goto L_0x0150
            r0 = r10
            frink.expr.UnitExpression r0 = (frink.expr.UnitExpression) r0
            r1 = r0
            frink.units.Unit r2 = r1.getUnit()
            r0 = r11
            frink.expr.UnitExpression r0 = (frink.expr.UnitExpression) r0
            r1 = r0
            frink.units.Unit r1 = r1.getUnit()
            frink.numeric.Numeric r3 = r2.getScale()
            frink.numeric.Numeric r4 = r1.getScale()
            boolean r5 = r3.isRational()     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            if (r5 == 0) goto L_0x0147
            boolean r5 = frink.numeric.NumericMath.isPositive(r3)     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            if (r5 == 0) goto L_0x0147
            boolean r5 = frink.numeric.NumericMath.isNegative(r4)     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            if (r5 == 0) goto L_0x0147
            frink.units.Unit r2 = frink.units.UnitMath.reciprocal(r2)     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.units.Unit r1 = frink.units.UnitMath.negate(r1)     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.numeric.Numeric r3 = r2.getScale()     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.numeric.Numeric r4 = r1.getScale()     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.units.DimensionList r5 = r2.getDimensionList()     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.units.Unit r5 = frink.units.BasicUnit.construct(r3, r5)     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.expr.UnitExpression r5 = frink.expr.BasicUnitExpression.construct(r5)     // Catch:{ NotRealException -> 0x00fa, NotImplementedException -> 0x0104, OverlapException -> 0x010e }
            frink.units.DimensionList r6 = r1.getDimensionList()     // Catch:{ NotRealException -> 0x0140, NotImplementedException -> 0x0139, OverlapException -> 0x0132 }
            frink.units.Unit r6 = frink.units.BasicUnit.construct(r4, r6)     // Catch:{ NotRealException -> 0x0140, NotImplementedException -> 0x0139, OverlapException -> 0x0132 }
            frink.expr.UnitExpression r6 = frink.expr.BasicUnitExpression.construct(r6)     // Catch:{ NotRealException -> 0x0140, NotImplementedException -> 0x0139, OverlapException -> 0x0132 }
            r8 = r4
            r4 = r2
            r2 = r3
            r3 = r1
            r1 = r8
            r9 = r6
            r6 = r5
            r5 = r9
        L_0x00bd:
            boolean r7 = r1.isFrinkInteger()
            if (r7 != 0) goto L_0x00d7
            boolean r1 = r1.isFloat()
            if (r1 != 0) goto L_0x00d7
            frink.numeric.FrinkInt r1 = frink.numeric.FrinkInt.ONE
            if (r2 == r1) goto L_0x00d7
            frink.numeric.FrinkInt r1 = frink.numeric.FrinkInt.NEGATIVE_ONE
            if (r2 == r1) goto L_0x00d7
            boolean r1 = r2.isFloat()
            if (r1 == 0) goto L_0x0128
        L_0x00d7:
            frink.units.Unit r1 = frink.units.UnitMath.power(r4, r3)     // Catch:{ ConformanceException -> 0x0118, NumericException -> 0x0120 }
            frink.expr.UnitExpression r1 = frink.expr.BasicUnitExpression.construct(r1)     // Catch:{ ConformanceException -> 0x0118, NumericException -> 0x0120 }
            goto L_0x0008
        L_0x00e1:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "PowerExpression.construct:  Got ConformanceException:\n  "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            r12.outputln(r1)
            goto L_0x005b
        L_0x00fa:
            r5 = move-exception
            r5 = r10
            r8 = r3
            r3 = r1
            r1 = r4
            r4 = r2
            r2 = r8
        L_0x0101:
            r6 = r5
            r5 = r11
            goto L_0x00bd
        L_0x0104:
            r5 = move-exception
            r5 = r10
            r8 = r3
            r3 = r1
            r1 = r4
            r4 = r2
            r2 = r8
        L_0x010b:
            r6 = r5
            r5 = r11
            goto L_0x00bd
        L_0x010e:
            r5 = move-exception
            r5 = r10
            r8 = r3
            r3 = r1
            r1 = r4
            r4 = r2
            r2 = r8
        L_0x0115:
            r6 = r5
            r5 = r11
            goto L_0x00bd
        L_0x0118:
            r1 = move-exception
            frink.expr.PowerExpression r1 = new frink.expr.PowerExpression
            r1.<init>(r6, r5)
            goto L_0x0008
        L_0x0120:
            r1 = move-exception
            frink.expr.PowerExpression r1 = new frink.expr.PowerExpression
            r1.<init>(r6, r5)
            goto L_0x0008
        L_0x0128:
            r1 = r5
            r2 = r6
        L_0x012a:
            frink.expr.PowerExpression r3 = new frink.expr.PowerExpression
            r3.<init>(r2, r1)
            r1 = r3
            goto L_0x0008
        L_0x0132:
            r6 = move-exception
            r8 = r4
            r4 = r2
            r2 = r3
            r3 = r1
            r1 = r8
            goto L_0x0115
        L_0x0139:
            r6 = move-exception
            r8 = r4
            r4 = r2
            r2 = r3
            r3 = r1
            r1 = r8
            goto L_0x010b
        L_0x0140:
            r6 = move-exception
            r8 = r4
            r4 = r2
            r2 = r3
            r3 = r1
            r1 = r8
            goto L_0x0101
        L_0x0147:
            r5 = r11
            r6 = r10
            r8 = r1
            r1 = r4
            r4 = r2
            r2 = r3
            r3 = r8
            goto L_0x00bd
        L_0x0150:
            r1 = r11
            r2 = r10
            goto L_0x012a
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.expr.PowerExpression.construct(frink.expr.Expression, frink.expr.Expression, frink.expr.Environment):frink.expr.Expression");
    }

    private PowerExpression(Expression expression, Expression expression2) {
        this(2);
        appendChild(expression);
        appendChild(expression2);
    }

    public PowerExpression(int i) {
        super(i);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        if (getChildCount() != 2) {
            throw new InvalidArgumentException("PowerExpression requires 2 arguments", this);
        }
        try {
            Expression evaluate = getChild(0).evaluate(environment);
            Expression evaluate2 = getChild(1).evaluate(environment);
            if (!environment.getSymbolicMode()) {
                if (evaluate instanceof SymbolExpression) {
                    environment.outputln("Warning: undefined symbol \"" + ((SymbolExpression) evaluate).getName() + "\".");
                }
                if (evaluate2 instanceof SymbolExpression) {
                    environment.outputln("Warning: undefined symbol \"" + ((SymbolExpression) evaluate2).getName() + "\".");
                }
            }
            if (!(evaluate instanceof UnitExpression) || !(evaluate2 instanceof UnitExpression)) {
                if (evaluate instanceof MultiplyExpression) {
                    try {
                        int childCount = evaluate.getChildCount();
                        BasicListExpression basicListExpression = new BasicListExpression(childCount);
                        for (int i = 0; i < childCount; i++) {
                            basicListExpression.appendChild(construct(evaluate.getChild(i), evaluate2, environment));
                        }
                        return MultiplyExpression.construct(basicListExpression, environment);
                    } catch (ConformanceException e) {
                        throw new EvaluationConformanceException("PowerExpression:  Conformance error:\n  " + e, this);
                    } catch (NumericException e2) {
                    }
                }
                try {
                    return construct(evaluate, evaluate2, environment);
                } catch (NumericException e3) {
                    return new PowerExpression(evaluate, evaluate2);
                }
            } else {
                try {
                    return BasicUnitExpression.construct(UnitMath.power(((UnitExpression) evaluate).getUnit(), ((UnitExpression) evaluate2).getUnit()));
                } catch (ConformanceException e4) {
                    throw new EvaluationConformanceException(e4.getMessage(), this);
                } catch (NumericException e5) {
                    throw new EvaluationNumericException("Error in power: " + e5, this);
                }
            }
        } catch (InvalidChildException e6) {
            throw new InvalidArgumentException("PowerExpression: trouble getting child", this);
        }
    }

    public String getSymbol() {
        return "^";
    }

    public int getPrecedence() {
        return 1000;
    }

    public int getAssociativity() {
        return 1;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof PowerExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
