package frink.expr;

import frink.function.BasicFunctionSource;
import frink.function.FunctionSource;
import frink.function.SingleArgMethod;
import frink.function.ZeroArgMethod;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import java.util.Enumeration;
import java.util.Hashtable;

public class BasicSetExpression extends TerminalExpression implements SetExpression, EnumeratingExpression, FrinkObject, DeepCopyable, HashingExpression {
    private static final BasicSetFunctionSource methods = new BasicSetFunctionSource();
    private EmptyObjectContextFrame contextFrame;
    /* access modifiers changed from: private */
    public Hashtable<HashingExpression, Boolean> hash;

    public BasicSetExpression() {
        this.hash = new Hashtable<>(5);
        this.contextFrame = null;
    }

    public BasicSetExpression(BasicSetExpression basicSetExpression, int i) {
        this.contextFrame = null;
        if (i == 0) {
            this.hash = (Hashtable) basicSetExpression.hash.clone();
            return;
        }
        this.hash = new Hashtable<>(basicSetExpression.hash.size());
        Enumeration<HashingExpression> keys = basicSetExpression.hash.keys();
        int i2 = i < 0 ? i : i - 1;
        while (keys.hasMoreElements()) {
            HashingExpression nextElement = keys.nextElement();
            Boolean bool = basicSetExpression.hash.get(nextElement);
            if (nextElement instanceof DeepCopyable) {
                nextElement = (HashingExpression) ((DeepCopyable) nextElement).deepCopy(i2);
            }
            this.hash.put(nextElement, bool);
        }
    }

    public boolean contains(HashingExpression hashingExpression) {
        return this.hash.get(hashingExpression) != null;
    }

    public void put(HashingExpression hashingExpression) {
        this.hash.put(hashingExpression, Boolean.TRUE);
    }

    public void remove(HashingExpression hashingExpression) {
        this.hash.remove(hashingExpression);
    }

    public EnumeratingExpression getValues() {
        return new EnumerationWrapper(this.hash.keys(), HashingExpressionFactory.INSTANCE);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isA(String str) {
        return str.equals(SetExpression.TYPE);
    }

    public boolean isConstant() {
        return false;
    }

    public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
        return new SetEnumerator();
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return methods;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return SetExpression.TYPE;
    }

    public int getSize() {
        return this.hash.size();
    }

    public void clear() {
        this.hash.clear();
    }

    public Expression deepCopy(int i) {
        return new BasicSetExpression(this, i);
    }

    public int hashCode() {
        Enumeration<HashingExpression> keys = this.hash.keys();
        int i = 2044383639;
        while (keys.hasMoreElements()) {
            i ^= keys.nextElement().hashCode();
        }
        return i;
    }

    public boolean equals(Object obj) {
        try {
            if (obj instanceof SetExpression) {
                return SetUtils.areEqual(this, (SetExpression) obj);
            }
        } catch (EvaluationException e) {
            System.err.println("BasicSetExpression.equals:  Evaluation exception:\n  " + e);
        }
        return false;
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }

    private class SetEnumerator implements FrinkEnumeration {
        private Enumeration<HashingExpression> keyEnum;

        public SetEnumerator() {
            this.keyEnum = BasicSetExpression.this.hash.keys();
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.keyEnum.hasMoreElements()) {
                return this.keyEnum.nextElement();
            }
            return null;
        }

        public void dispose() {
            this.keyEnum = null;
        }
    }

    private static class BasicSetFunctionSource extends BasicFunctionSource {
        BasicSetFunctionSource() {
            super("BasicSetExpression");
            addFunctionDefinition("clear", new ZeroArgMethod<BasicSetExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicSetExpression basicSetExpression) throws EvaluationException {
                    basicSetExpression.clear();
                    return basicSetExpression;
                }
            });
            addFunctionDefinition("put", new SingleArgMethod<BasicSetExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicSetExpression basicSetExpression, Expression expression) throws EvaluationException {
                    if (expression instanceof HashingExpression) {
                        basicSetExpression.put((HashingExpression) expression);
                        return basicSetExpression;
                    }
                    throw new InvalidArgumentException("BasicSetExpression.put: argument " + environment.format(expression) + " is not a hashing expression and cannot yet be put into a set.", expression);
                }
            });
            addFunctionDefinition("contains", new SingleArgMethod<BasicSetExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicSetExpression basicSetExpression, Expression expression) throws EvaluationException {
                    if (expression instanceof HashingExpression) {
                        return FrinkBoolean.create(basicSetExpression.contains((HashingExpression) expression));
                    }
                    throw new InvalidArgumentException("BasicSetExpression.contains: argument " + environment.format(expression) + " is not a hashing expression and cannot yet be contains into a set.", expression);
                }
            });
            addFunctionDefinition("remove", new SingleArgMethod<BasicSetExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicSetExpression basicSetExpression, Expression expression) throws EvaluationException {
                    if (expression instanceof HashingExpression) {
                        basicSetExpression.remove((HashingExpression) expression);
                        return VoidExpression.VOID;
                    }
                    throw new InvalidArgumentException("BasicSetExpression.remove: argument " + environment.format(expression) + " is not a hashing expression and cannot yet be remove into a set.", expression);
                }
            });
            addFunctionDefinition("shallowCopy", new ZeroArgMethod<BasicSetExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicSetExpression basicSetExpression) throws EvaluationException {
                    return new BasicSetExpression(basicSetExpression, 0);
                }
            });
        }
    }
}
