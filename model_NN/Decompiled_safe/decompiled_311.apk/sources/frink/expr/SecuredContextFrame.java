package frink.expr;

import java.util.Vector;

public class SecuredContextFrame extends BasicContextFrame {
    private String classname;

    public SecuredContextFrame(String str) {
        this.classname = str;
    }

    protected SecuredContextFrame(SecuredContextFrame securedContextFrame, int i) {
        super(securedContextFrame, i);
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws FrinkSecurityException {
        if (z) {
            environment.getSecurityHelper().checkSetClassLevelVariable(this.classname, str);
        }
        return super.getSymbolDefinition(str, z, environment);
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException {
        environment.getSecurityHelper().checkSetClassLevelVariable(this.classname, str);
        return super.setSymbolDefinition(str, expression, environment);
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException, FrinkSecurityException {
        environment.getSecurityHelper().checkSetClassLevelVariable(this.classname, str);
        return super.declareVariable(str, vector, expression, environment);
    }

    public BasicContextFrame deepCopy(int i) {
        return new SecuredContextFrame(this, i);
    }
}
