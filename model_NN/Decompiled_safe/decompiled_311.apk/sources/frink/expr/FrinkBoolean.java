package frink.expr;

import frink.symbolic.MatchingContext;

public class FrinkBoolean extends TerminalExpression implements BooleanExpression {
    public static final FrinkBoolean FALSE = new FrinkBoolean(false);
    public static final FrinkBoolean TRUE = new FrinkBoolean(true);
    public static final String TYPE = "Boolean";
    private boolean boolVal;

    private FrinkBoolean(boolean z) {
        this.boolVal = z;
    }

    public static FrinkBoolean create(boolean z) {
        if (z) {
            return TRUE;
        }
        return FALSE;
    }

    public boolean getBoolean() {
        return this.boolVal;
    }

    public boolean isConstant() {
        return true;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (!(expression instanceof BooleanExpression) || ((BooleanExpression) expression).getBoolean() != this.boolVal) {
            return false;
        }
        return true;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
