package frink.expr;

import frink.date.FrinkDate;
import frink.date.NegativeJulianDate;
import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import frink.function.BuiltinFunctionSource;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkInteger;
import frink.numeric.FrinkRational;
import frink.numeric.NumericException;
import frink.numeric.OverlapException;
import frink.symbolic.BasicMatchingContext;
import frink.symbolic.MatchingContext;
import frink.symbolic.SymbolicUtils;
import frink.units.Unit;
import frink.units.UnitMath;

public class MultiplyExpression extends CachingExpression implements OperatorExpression {
    private static final boolean DEBUG = false;
    public static final String TYPE = "Multiply";
    private int nextToFlatten;

    public static Expression negate(Expression expression, Environment environment) throws NumericException, InvalidChildException, ConformanceException {
        if (expression instanceof UnitExpression) {
            return BasicUnitExpression.construct(UnitMath.negate(((UnitExpression) expression).getUnit()));
        }
        return construct(DimensionlessUnitExpression.NEGATIVE_ONE, expression, environment);
    }

    public static Expression divide(Expression expression, Expression expression2, Environment environment) throws NumericException, InvalidChildException, ConformanceException {
        FrinkInteger frinkInteger = null;
        try {
            return DimensionlessUnitExpression.construct(FrinkRational.construct(BuiltinFunctionSource.getFrinkIntegerValue(expression), BuiltinFunctionSource.getFrinkIntegerValue(expression2)));
        } catch (NotAnIntegerException e) {
            if (frinkInteger != null) {
                try {
                    if (frinkInteger.getInt() == 1) {
                        return PowerExpression.reciprocal(expression2, environment);
                    }
                } catch (NotAnIntegerException e2) {
                }
            }
            return construct(expression, PowerExpression.reciprocal(expression2, environment), environment);
        }
    }

    public static final Expression construct(Expression expression, Expression expression2, Environment environment) throws InvalidChildException, NumericException, ConformanceException {
        BasicListExpression basicListExpression = new BasicListExpression(2);
        basicListExpression.appendChild(expression);
        basicListExpression.appendChild(expression2);
        return construct(basicListExpression, environment);
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ef  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final frink.expr.Expression construct(frink.expr.ListExpression r14, frink.expr.Environment r15) throws frink.expr.InvalidChildException, frink.numeric.NumericException, frink.errors.ConformanceException {
        /*
            r12 = 0
            r11 = 1
            r10 = 0
            int r3 = r14.getChildCount()
            if (r3 != r11) goto L_0x000e
            frink.expr.Expression r1 = r14.getChild(r10)
        L_0x000d:
            return r1
        L_0x000e:
            r4 = r10
            r5 = r12
            r6 = r12
        L_0x0011:
            if (r4 >= r3) goto L_0x008d
            frink.expr.Expression r2 = r14.getChild(r4)
            boolean r1 = r2 instanceof frink.expr.MultiplyExpression
            if (r1 == 0) goto L_0x0065
            r0 = r2
            frink.expr.MultiplyExpression r0 = (frink.expr.MultiplyExpression) r0
            r1 = r0
            r1.flatten()
            int r7 = r2.getChildCount()
            r8 = r6
            r6 = r5
            r5 = r10
        L_0x0029:
            if (r5 >= r7) goto L_0x005e
            frink.expr.Expression r1 = r2.getChild(r5)
            boolean r9 = r1 instanceof frink.expr.UnitExpression
            if (r9 == 0) goto L_0x0051
            if (r8 != 0) goto L_0x0043
            frink.expr.UnitExpression r1 = (frink.expr.UnitExpression) r1
            frink.units.Unit r1 = r1.getUnit()
            r13 = r6
            r6 = r1
            r1 = r13
        L_0x003e:
            int r5 = r5 + 1
            r8 = r6
            r6 = r1
            goto L_0x0029
        L_0x0043:
            frink.expr.UnitExpression r1 = (frink.expr.UnitExpression) r1
            frink.units.Unit r1 = r1.getUnit()
            frink.units.Unit r1 = frink.units.UnitMath.multiply(r8, r1)
            r13 = r6
            r6 = r1
            r1 = r13
            goto L_0x003e
        L_0x0051:
            if (r6 != 0) goto L_0x0058
            frink.expr.BasicListExpression r6 = new frink.expr.BasicListExpression
            r6.<init>(r11)
        L_0x0058:
            r6.appendChild(r1)
            r1 = r6
            r6 = r8
            goto L_0x003e
        L_0x005e:
            r1 = r6
            r2 = r8
        L_0x0060:
            int r4 = r4 + 1
            r5 = r1
            r6 = r2
            goto L_0x0011
        L_0x0065:
            boolean r1 = r2 instanceof frink.expr.UnitExpression
            if (r1 == 0) goto L_0x0081
            if (r6 != 0) goto L_0x0074
            frink.expr.UnitExpression r2 = (frink.expr.UnitExpression) r2
            frink.units.Unit r1 = r2.getUnit()
            r2 = r1
            r1 = r5
            goto L_0x0060
        L_0x0074:
            frink.expr.UnitExpression r2 = (frink.expr.UnitExpression) r2
            frink.units.Unit r1 = r2.getUnit()
            frink.units.Unit r1 = frink.units.UnitMath.multiply(r6, r1)
            r2 = r1
            r1 = r5
            goto L_0x0060
        L_0x0081:
            if (r5 != 0) goto L_0x0135
            frink.expr.BasicListExpression r1 = new frink.expr.BasicListExpression
            r1.<init>(r11)
        L_0x0088:
            r1.appendChild(r2)
            r2 = r6
            goto L_0x0060
        L_0x008d:
            if (r6 == 0) goto L_0x0132
            int r1 = frink.units.UnitMath.getIntegerValue(r6)     // Catch:{ NotAnIntegerException -> 0x00cc }
            if (r1 != r11) goto L_0x0132
            if (r5 != 0) goto L_0x009b
            frink.expr.DimensionlessUnitExpression r1 = frink.expr.DimensionlessUnitExpression.ONE     // Catch:{ NotAnIntegerException -> 0x00cc }
            goto L_0x000d
        L_0x009b:
            r1 = r12
        L_0x009c:
            if (r1 == 0) goto L_0x00ef
            frink.expr.UnitExpression r1 = frink.expr.BasicUnitExpression.construct(r1)
            if (r5 == 0) goto L_0x000d
            int r2 = r5.getChildCount()
            if (r2 <= r11) goto L_0x0130
            frink.symbolic.SymbolicUtils.sortOperands(r5, r15)
            frink.expr.BasicListExpression r2 = combineOperands(r5, r15)
            int r3 = r2.getChildCount()
            r13 = r3
            r3 = r2
            r2 = r13
        L_0x00b8:
            if (r2 != r11) goto L_0x00cf
            frink.expr.Expression r4 = r3.getChild(r10)
            boolean r4 = r4 instanceof frink.expr.UnitExpression
            if (r4 == 0) goto L_0x00cf
            frink.expr.Expression r2 = r3.getChild(r10)
            frink.expr.Expression r1 = construct(r1, r2, r15)
            goto L_0x000d
        L_0x00cc:
            r1 = move-exception
            r1 = r6
            goto L_0x009c
        L_0x00cf:
            frink.expr.MultiplyExpression r4 = new frink.expr.MultiplyExpression
            int r5 = r2 + 1
            r4.<init>(r5)
            r4.appendChild(r1)
            r1 = r10
        L_0x00da:
            if (r1 >= r2) goto L_0x00e6
            frink.expr.Expression r5 = r3.getChild(r1)
            r4.appendChild(r5)
            int r1 = r1 + 1
            goto L_0x00da
        L_0x00e6:
            int r1 = r4.getChildCount()
            r4.nextToFlatten = r1
            r1 = r4
            goto L_0x000d
        L_0x00ef:
            int r1 = r5.getChildCount()
            if (r1 != r11) goto L_0x00fb
            frink.expr.Expression r1 = r5.getChild(r10)
            goto L_0x000d
        L_0x00fb:
            if (r1 <= r11) goto L_0x012e
            frink.symbolic.SymbolicUtils.sortOperands(r5, r15)
            frink.expr.BasicListExpression r1 = combineOperands(r5, r15)
            int r2 = r1.getChildCount()
            r13 = r2
            r2 = r1
            r1 = r13
        L_0x010b:
            if (r1 != r11) goto L_0x0113
            frink.expr.Expression r1 = r2.getChild(r10)
            goto L_0x000d
        L_0x0113:
            frink.expr.MultiplyExpression r3 = new frink.expr.MultiplyExpression
            r3.<init>(r1)
            r4 = r10
        L_0x0119:
            if (r4 >= r1) goto L_0x0125
            frink.expr.Expression r5 = r2.getChild(r4)
            r3.appendChild(r5)
            int r4 = r4 + 1
            goto L_0x0119
        L_0x0125:
            int r1 = r3.getChildCount()
            r3.nextToFlatten = r1
            r1 = r3
            goto L_0x000d
        L_0x012e:
            r2 = r5
            goto L_0x010b
        L_0x0130:
            r3 = r5
            goto L_0x00b8
        L_0x0132:
            r1 = r6
            goto L_0x009c
        L_0x0135:
            r1 = r5
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.expr.MultiplyExpression.construct(frink.expr.ListExpression, frink.expr.Environment):frink.expr.Expression");
    }

    private static BasicListExpression combineOperands(BasicListExpression basicListExpression, Environment environment) throws InvalidChildException, NumericException, ConformanceException {
        BasicMatchingContext basicMatchingContext;
        Expression expression;
        Expression expression2;
        Expression expression3;
        Expression expression4;
        int i;
        BasicListExpression basicListExpression2;
        BasicListExpression basicListExpression3;
        if (SymbolicUtils.containsPattern(basicListExpression)) {
            return basicListExpression;
        }
        int childCount = basicListExpression.getChildCount() - 1;
        basicListExpression.getChild(0);
        BasicListExpression basicListExpression4 = null;
        Expression expression5 = null;
        int i2 = 1;
        BasicMatchingContext basicMatchingContext2 = null;
        BasicListExpression basicListExpression5 = null;
        int i3 = 0;
        while (i3 < childCount && i2 <= childCount) {
            if (basicMatchingContext2 == null) {
                basicMatchingContext = new BasicMatchingContext();
            } else {
                basicMatchingContext = basicMatchingContext2;
            }
            Expression child = basicListExpression.getChild(i3);
            Expression child2 = basicListExpression.getChild(i2);
            if (child instanceof PowerExpression) {
                expression2 = child.getChild(0);
                expression = child.getChild(1);
            } else {
                expression = DimensionlessUnitExpression.ONE;
                expression2 = child;
            }
            if (child2 instanceof PowerExpression) {
                expression3 = child2.getChild(0);
                expression4 = child2.getChild(1);
            } else {
                expression3 = child2;
                expression4 = DimensionlessUnitExpression.ONE;
            }
            int beginMatch = basicMatchingContext.beginMatch();
            if (expression2.structureEquals(expression3, basicMatchingContext, environment, true)) {
                if (basicListExpression4 == null) {
                    basicListExpression3 = new BasicListExpression(i3);
                    for (int i4 = 0; i4 < i3; i4++) {
                        basicListExpression3.appendChild(basicListExpression.getChild(i4));
                    }
                } else {
                    basicListExpression3 = basicListExpression4;
                }
                if (basicListExpression5 == null) {
                    basicListExpression5 = new BasicListExpression(2);
                    basicListExpression5.appendChild(expression);
                }
                basicListExpression5.appendChild(expression4);
                int i5 = i2 + 1;
                i2 = i3;
                basicListExpression2 = basicListExpression3;
                i = i5;
            } else {
                if (basicListExpression4 != null) {
                    if (basicListExpression5 != null) {
                        Expression construct = PowerExpression.construct(expression2, AddExpression.construct(basicListExpression5, environment), environment);
                        if (construct != DimensionlessUnitExpression.ONE) {
                            basicListExpression4.appendChild(construct);
                        }
                        basicListExpression5 = null;
                    } else {
                        basicListExpression4.appendChild(child);
                    }
                }
                i = i2 + 1;
                basicListExpression2 = basicListExpression4;
            }
            basicMatchingContext.rollbackMatch(beginMatch);
            basicListExpression4 = basicListExpression2;
            i3 = i2;
            i2 = i;
            basicMatchingContext2 = basicMatchingContext;
            expression5 = expression2;
        }
        if (basicListExpression4 == null) {
            return basicListExpression;
        }
        if (basicListExpression5 != null) {
            Expression construct2 = PowerExpression.construct(expression5, AddExpression.construct(basicListExpression5, environment), environment);
            if (basicListExpression4.getChildCount() == 0 || construct2 != DimensionlessUnitExpression.ONE) {
                basicListExpression4.appendChild(construct2);
            }
        } else {
            basicListExpression4.appendChild(basicListExpression.getChild(childCount));
        }
        return basicListExpression4;
    }

    private MultiplyExpression(Expression expression, Expression expression2) {
        this(2);
        flattenAndAppend(expression);
        flattenAndAppend(expression2);
        this.nextToFlatten = getChildCount();
    }

    private MultiplyExpression(int i) {
        super(i);
        this.nextToFlatten = 0;
        this.nextToFlatten = 0;
    }

    public void flattenAndAppend(Expression expression) {
        if (expression instanceof MultiplyExpression) {
            try {
                ((MultiplyExpression) expression).flatten();
                int childCount = expression.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    appendChild(expression.getChild(i));
                }
            } catch (InvalidChildException e) {
                System.out.println("MultiplyExpression.flattenAndAppend got bad child.");
            }
        } else {
            appendChild(expression);
        }
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        BasicListExpression basicListExpression;
        FrinkDate frinkDate;
        BasicListExpression basicListExpression2;
        Unit unit;
        int childCount = getChildCount();
        if (this.nextToFlatten < childCount) {
            flatten();
        }
        Unit unit2 = null;
        FrinkDate frinkDate2 = null;
        BasicListExpression basicListExpression3 = null;
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = getChild(i);
                if (child.getChildCount() == 2 && (child instanceof PowerExpression) && child.getChild(1) == DimensionlessUnitExpression.NEGATIVE_ONE && unit2 != null) {
                    Expression evaluate = child.getChild(0).evaluate(environment);
                    if (evaluate instanceof UnitExpression) {
                        unit = UnitMath.divide(unit2, ((UnitExpression) evaluate).getUnit());
                        frinkDate = frinkDate2;
                        basicListExpression2 = basicListExpression3;
                        i++;
                        frinkDate2 = frinkDate;
                        basicListExpression3 = basicListExpression2;
                        unit2 = unit;
                    }
                }
                Expression evaluate2 = child.evaluate(environment);
                if ((evaluate2 instanceof SymbolExpression) && !environment.getSymbolicMode()) {
                    environment.outputln("Warning: undefined symbol \"" + ((SymbolExpression) evaluate2).getName() + "\".");
                }
                if (evaluate2 instanceof UnitExpression) {
                    unit = ((UnitExpression) evaluate2).getUnit();
                    if (unit2 == null) {
                        frinkDate = frinkDate2;
                        basicListExpression2 = basicListExpression3;
                    } else if (unit == DimensionlessUnitExpression.NEGATIVE_ONE) {
                        unit = UnitMath.negate(unit2);
                        frinkDate = frinkDate2;
                        basicListExpression2 = basicListExpression3;
                    } else {
                        unit = UnitMath.multiply(unit2, unit);
                        frinkDate = frinkDate2;
                        basicListExpression2 = basicListExpression3;
                    }
                } else if (evaluate2 instanceof DateExpression) {
                    frinkDate = ((DateExpression) evaluate2).getFrinkDate();
                    unit = unit2;
                    basicListExpression2 = basicListExpression3;
                } else {
                    if (basicListExpression3 == null) {
                        basicListExpression = new BasicListExpression(1);
                    } else {
                        basicListExpression = basicListExpression3;
                    }
                    basicListExpression.appendChild(evaluate2);
                    frinkDate = frinkDate2;
                    Unit unit3 = unit2;
                    basicListExpression2 = basicListExpression;
                    unit = unit3;
                }
                i++;
                frinkDate2 = frinkDate;
                basicListExpression3 = basicListExpression2;
                unit2 = unit;
            } catch (ConformanceException e) {
                throw new EvaluationConformanceException("MultiplyExpression: \n  " + e, this);
            } catch (InvalidChildException e2) {
                throw new InvalidArgumentException("Error getting child " + i, this);
            } catch (NumericException e3) {
                throw new EvaluationNumericException("MultiplyExpression: \n" + e3, this);
            }
        }
        if (basicListExpression3 == null) {
            if (frinkDate2 == null) {
                return BasicUnitExpression.construct(unit2);
            }
            try {
                if (UnitMath.compare(unit2, DimensionlessUnitExpression.NEGATIVE_ONE) == 0) {
                    return new BasicDateExpression(new NegativeJulianDate(frinkDate2.getJulian()));
                }
                if (UnitMath.compare(unit2, DimensionlessUnitExpression.ZERO) == 0) {
                    return BasicUnitExpression.construct(UnitMath.multiply(FrinkInt.ZERO, environment.getUnitManager().getUnit("s")));
                }
                throw new InvalidArgumentException("Cannot multiply a date by anything but 1, -1, or 0.", this);
            } catch (ConformanceException | NotRealException | OverlapException e4) {
            }
        } else if (frinkDate2 == null) {
            if (unit2 != null) {
                try {
                    if (UnitMath.getIntegerValue(unit2) == 0) {
                        return DimensionlessUnitExpression.ZERO;
                    }
                } catch (NotAnIntegerException e5) {
                }
                basicListExpression3.insertBefore(0, BasicUnitExpression.construct(unit2));
            }
            return construct(basicListExpression3, environment);
        } else {
            throw new InvalidArgumentException("Cannot multiply a date by anything but 1 or -1", this);
        }
    }

    private void flatten() throws InvalidChildException {
        int childCount = getChildCount();
        if (this.nextToFlatten < childCount) {
            clearCache();
            for (int i = this.nextToFlatten; i < childCount; i++) {
                Expression child = getChild(i);
                if (child instanceof MultiplyExpression) {
                    MultiplyExpression multiplyExpression = (MultiplyExpression) child;
                    multiplyExpression.flatten();
                    int childCount2 = multiplyExpression.getChildCount();
                    replaceChild(i, multiplyExpression.getChild(0));
                    for (int i2 = 1; i2 < childCount2; i2++) {
                        insertBefore(i + i2, multiplyExpression.getChild(i2));
                    }
                }
            }
            this.nextToFlatten = getChildCount();
        }
    }

    public String getSymbol() {
        return " ";
    }

    public int getPrecedence() {
        return 500;
    }

    public int getAssociativity() {
        return -1;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof MultiplyExpression) {
            return childrenEqualPermuted(expression, matchingContext, environment, z);
        }
        return DEBUG;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
