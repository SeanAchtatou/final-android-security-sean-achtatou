package frink.expr;

import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public class ModulusExpression extends CachingExpression {
    public static final String TYPE = "mod";

    public ModulusExpression(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        Expression evaluate = getChild(0).evaluate(environment);
        Expression evaluate2 = getChild(1).evaluate(environment);
        if ((evaluate instanceof UnitExpression) && (evaluate2 instanceof UnitExpression)) {
            Unit unit = ((UnitExpression) evaluate).getUnit();
            Unit unit2 = ((UnitExpression) evaluate2).getUnit();
            try {
                if (UnitMath.areConformal(unit, unit2)) {
                    Numeric mod = NumericMath.mod(unit.getScale(), unit2.getScale());
                    if (UnitMath.isDimensionless(unit)) {
                        return DimensionlessUnitExpression.construct(mod);
                    }
                    return new CondensedUnitExpression(mod, unit.getDimensionList());
                }
            } catch (NumericException e) {
                throw new InvalidArgumentException("Arguments to modulus must be conformal units:\n " + e, this);
            }
        }
        throw new InvalidArgumentException("Arguments to modulus must be conformal units.", this);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof ModulusExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
