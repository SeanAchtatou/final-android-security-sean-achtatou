package frink.expr;

public class ValueNotFoundException extends EvaluationException {
    public ValueNotFoundException(String str, Expression expression) {
        super(str, expression);
    }
}
