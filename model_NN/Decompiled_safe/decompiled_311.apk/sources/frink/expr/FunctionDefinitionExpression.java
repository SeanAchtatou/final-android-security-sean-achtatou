package frink.expr;

import frink.function.FunctionDefinition;

public interface FunctionDefinitionExpression {
    FunctionDefinition getFunctionDefinition();
}
