package frink.expr;

public abstract class CachingExpression extends NonTerminalExpression {
    private static final byte CONSTANT = 2;
    private static final byte NON_CONSTANT = 1;
    private static final byte UNKNOWN = 0;
    private byte constantFlag = 0;
    private Expression result = null;

    /* access modifiers changed from: protected */
    public abstract Expression doOperation(Environment environment) throws EvaluationException;

    public CachingExpression(int i) {
        super(i);
    }

    public void appendChild(Expression expression) {
        super.appendChild(expression);
        clearCache();
    }

    /* access modifiers changed from: protected */
    public void replaceChild(int i, Expression expression) {
        super.replaceChild(i, expression);
        clearCache();
    }

    /* access modifiers changed from: protected */
    public void insertBefore(int i, Expression expression) {
        super.insertBefore(i, expression);
        clearCache();
    }

    /* access modifiers changed from: protected */
    public void clearCache() {
        this.result = null;
        this.constantFlag = 0;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        if (this.constantFlag == 2 && this.result != null) {
            return this.result;
        }
        this.result = doOperation(environment);
        if (this.constantFlag == 0) {
            resolveConstantFlag();
        }
        return this.result;
    }

    private void resolveConstantFlag() {
        if (this.constantFlag == 0) {
            int childCount = getChildCount();
            int i = 0;
            while (i < childCount) {
                try {
                    Expression child = getChild(i);
                    if (child == null || !child.isConstant()) {
                        this.constantFlag = NON_CONSTANT;
                        return;
                    }
                    i++;
                } catch (InvalidChildException e) {
                    System.out.println(e);
                    return;
                }
            }
            this.constantFlag = CONSTANT;
        }
    }

    public boolean isConstant() {
        if (this.constantFlag == 0) {
            resolveConstantFlag();
        }
        if (this.constantFlag == 2) {
            return true;
        }
        return false;
    }
}
