package frink.expr;

import frink.function.FunctionDescriptor;
import frink.source.CurrencySource;
import frink.symbolic.MatchingContext;
import java.util.Enumeration;

public class HelpExpression extends CachingExpression {
    public static final String TYPE = "Help";
    boolean verbose;

    public HelpExpression(SymbolExpression symbolExpression, boolean z) {
        super(1);
        appendChild(symbolExpression);
        this.verbose = z;
    }

    /* JADX INFO: finally extract failed */
    public ListExpression listSymbolsContaining(String str, Environment environment) {
        String lowerCase = str.toLowerCase();
        int length = lowerCase.length();
        Enumeration<String> names = environment.getUnitManager().getNames();
        try {
            CurrencySource.setUseCacheOnly(true);
            boolean z = true;
            BasicListExpression basicListExpression = null;
            while (names.hasMoreElements()) {
                String nextElement = names.nextElement();
                if (length <= nextElement.length() && nextElement.toLowerCase().indexOf(lowerCase) != -1) {
                    if (basicListExpression == null) {
                        basicListExpression = new BasicListExpression(1);
                    }
                    if (this.verbose) {
                        basicListExpression.appendChild(new BasicStringExpression((z ? "" : "\n") + nextElement + " = " + environment.format(BasicUnitExpression.construct(environment.getUnitManager().getUnit(nextElement)))));
                        z = false;
                    } else {
                        basicListExpression.appendChild(new BasicStringExpression(nextElement));
                    }
                }
            }
            CurrencySource.setUseCacheOnly(false);
            Enumeration<FunctionDescriptor> functionDescriptors = environment.getFunctionManager().getFunctionDescriptors();
            boolean z2 = z;
            BasicListExpression basicListExpression2 = basicListExpression;
            while (functionDescriptors.hasMoreElements()) {
                FunctionDescriptor nextElement2 = functionDescriptors.nextElement();
                if (nextElement2.getName().toLowerCase().indexOf(lowerCase) != -1) {
                    if (basicListExpression2 == null) {
                        basicListExpression2 = new BasicListExpression(1);
                    }
                    if (this.verbose) {
                        basicListExpression2.appendChild(new BasicStringExpression((z2 ? "" : "\n") + nextElement2.toString(environment)));
                    } else {
                        basicListExpression2.appendChild(new BasicStringExpression(nextElement2.toString(environment)));
                    }
                    z2 = false;
                }
            }
            return basicListExpression2;
        } catch (Throwable th) {
            CurrencySource.setUseCacheOnly(false);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        Expression child = getChild(0);
        if (child instanceof SymbolExpression) {
            ListExpression listSymbolsContaining = listSymbolsContaining(((SymbolExpression) child).getName(), environment);
            if (listSymbolsContaining == null) {
                return new BasicStringExpression("No matches found.");
            }
            return listSymbolsContaining;
        }
        throw new InvalidArgumentException("Help expression requires identifier.", child);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof HelpExpression) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
