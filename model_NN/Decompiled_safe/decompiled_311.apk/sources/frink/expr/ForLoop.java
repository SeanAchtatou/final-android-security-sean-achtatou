package frink.expr;

import frink.symbolic.MatchingContext;

public class ForLoop extends NonTerminalExpression {
    public static final String TYPE = "for";
    private String[] argNames;
    private boolean assignToArray;
    private String label;

    public ForLoop(String str, Expression expression, Expression expression2, String str2) {
        super(2);
        appendChild(expression2);
        appendChild(expression);
        this.argNames = new String[1];
        this.argNames[0] = str;
        this.assignToArray = false;
        this.label = str2;
    }

    public ForLoop(ListExpression listExpression, Expression expression, Expression expression2, String str) {
        super(2);
        appendChild(expression2);
        appendChild(expression);
        this.label = str;
        this.assignToArray = true;
        int childCount = listExpression.getChildCount();
        this.argNames = new String[childCount];
        int i = 0;
        while (i < childCount) {
            try {
                Expression child = listExpression.getChild(i);
                if (child instanceof SymbolExpression) {
                    this.argNames[i] = ((SymbolExpression) child).getName();
                } else {
                    System.out.println("ForLoop:  argument not a symbol");
                }
                i++;
            } catch (InvalidChildException e) {
                System.out.println("ForLoop:  Unexpected child count.");
                return;
            }
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 166 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public frink.expr.Expression evaluate(frink.expr.Environment r12) throws frink.expr.EvaluationException {
        /*
            r11 = this;
            r10 = 1
            r9 = 0
            frink.expr.Expression r0 = r11.getChild(r10)
            frink.expr.Expression r0 = r0.evaluate(r12)
            frink.expr.UndefExpression r1 = frink.expr.UndefExpression.UNDEF
            if (r0 != r1) goto L_0x0011
            frink.expr.VoidExpression r0 = frink.expr.VoidExpression.VOID
        L_0x0010:
            return r0
        L_0x0011:
            boolean r1 = r0 instanceof frink.expr.EnumeratingExpression
            if (r1 != 0) goto L_0x0032
            frink.expr.InvalidArgumentException r1 = new frink.expr.InvalidArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ForLoop:  not a EnumeratingExpression: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r12.format(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0, r11)
            throw r1
        L_0x0032:
            frink.expr.EnumeratingExpression r0 = (frink.expr.EnumeratingExpression) r0
            frink.expr.FrinkEnumeration r0 = r0.getEnumeration(r12)
            frink.expr.Expression r1 = r11.getChild(r9)
            boolean r2 = r11.assignToArray
            if (r2 != 0) goto L_0x0045
            frink.expr.Expression r0 = r11.evaluateSingleCase(r12, r0, r1)
            goto L_0x0010
        L_0x0045:
            java.lang.String[] r2 = r11.argNames
            int r2 = r2.length
            r3 = 0
            if (r2 <= 0) goto L_0x004d
            frink.expr.SymbolDefinition[] r3 = new frink.expr.SymbolDefinition[r2]
        L_0x004d:
            frink.expr.BasicContextFrame r4 = new frink.expr.BasicContextFrame
            r4.<init>()
            r12.addContextFrame(r4, r9)
            r4 = r9
        L_0x0056:
            if (r4 >= r2) goto L_0x006a
            java.lang.String[] r5 = r11.argNames     // Catch:{ all -> 0x0091 }
            r5 = r5[r4]     // Catch:{ all -> 0x0091 }
            frink.expr.UndefExpression r6 = frink.expr.UndefExpression.UNDEF     // Catch:{ all -> 0x0091 }
            frink.expr.SymbolDefinition r5 = r12.setSymbolDefinition(r5, r6)     // Catch:{ all -> 0x0091 }
            r3[r4] = r5     // Catch:{ all -> 0x0091 }
            int r4 = r4 + 1
            goto L_0x0056
        L_0x0067:
            r1.evaluate(r12)     // Catch:{ NextException -> 0x00e4, BreakException -> 0x00ae }
        L_0x006a:
            frink.expr.Expression r4 = r0.getNext(r12)     // Catch:{ all -> 0x0091 }
            if (r4 == 0) goto L_0x00d8
            boolean r5 = r4 instanceof frink.expr.ListExpression     // Catch:{ all -> 0x0091 }
            if (r5 == 0) goto L_0x009b
            int r5 = r4.getChildCount()     // Catch:{ all -> 0x0091 }
            r6 = r9
        L_0x0079:
            if (r6 >= r2) goto L_0x0067
            if (r6 >= r5) goto L_0x0089
            r7 = r3[r6]     // Catch:{ all -> 0x0091 }
            frink.expr.Expression r8 = r4.getChild(r6)     // Catch:{ all -> 0x0091 }
            r7.setValue(r8)     // Catch:{ all -> 0x0091 }
        L_0x0086:
            int r6 = r6 + 1
            goto L_0x0079
        L_0x0089:
            r7 = r3[r6]     // Catch:{ all -> 0x0091 }
            frink.expr.UndefExpression r8 = frink.expr.UndefExpression.UNDEF     // Catch:{ all -> 0x0091 }
            r7.setValue(r8)     // Catch:{ all -> 0x0091 }
            goto L_0x0086
        L_0x0091:
            r1 = move-exception
            r12.removeContextFrame()
            if (r0 == 0) goto L_0x009a
            r0.dispose()
        L_0x009a:
            throw r1
        L_0x009b:
            r5 = 0
            r5 = r3[r5]     // Catch:{ all -> 0x0091 }
            r5.setValue(r4)     // Catch:{ all -> 0x0091 }
            r4 = r10
        L_0x00a2:
            if (r4 >= r2) goto L_0x0067
            r5 = r3[r4]     // Catch:{ all -> 0x0091 }
            frink.expr.UndefExpression r6 = frink.expr.UndefExpression.UNDEF     // Catch:{ all -> 0x0091 }
            r5.setValue(r6)     // Catch:{ all -> 0x0091 }
            int r4 = r4 + 1
            goto L_0x00a2
        L_0x00ae:
            r1 = move-exception
            java.lang.String r2 = r1.getLabel()     // Catch:{ all -> 0x0091 }
            if (r2 == 0) goto L_0x00cb
            java.lang.String r3 = r11.label     // Catch:{ all -> 0x0091 }
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0091 }
            if (r2 == 0) goto L_0x00ca
            frink.expr.VoidExpression r1 = frink.expr.VoidExpression.VOID     // Catch:{ all -> 0x0091 }
            r12.removeContextFrame()
            if (r0 == 0) goto L_0x00c7
            r0.dispose()
        L_0x00c7:
            r0 = r1
            goto L_0x0010
        L_0x00ca:
            throw r1     // Catch:{ all -> 0x0091 }
        L_0x00cb:
            frink.expr.VoidExpression r1 = frink.expr.VoidExpression.VOID     // Catch:{ all -> 0x0091 }
            r12.removeContextFrame()
            if (r0 == 0) goto L_0x00d5
            r0.dispose()
        L_0x00d5:
            r0 = r1
            goto L_0x0010
        L_0x00d8:
            r12.removeContextFrame()
            if (r0 == 0) goto L_0x00e0
            r0.dispose()
        L_0x00e0:
            frink.expr.VoidExpression r0 = frink.expr.VoidExpression.VOID
            goto L_0x0010
        L_0x00e4:
            r4 = move-exception
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.expr.ForLoop.evaluate(frink.expr.Environment):frink.expr.Expression");
    }

    private Expression evaluateSingleCase(Environment environment, FrinkEnumeration frinkEnumeration, Expression expression) throws EvaluationException {
        environment.addContextFrame(new BasicContextFrame(), false);
        try {
            SymbolDefinition symbolDefinition = environment.setSymbolDefinition(this.argNames[0], UndefExpression.UNDEF);
            while (true) {
                Expression next = frinkEnumeration.getNext(environment);
                if (next == null) {
                    break;
                }
                symbolDefinition.setValue(next);
                expression.evaluate(environment);
            }
            environment.removeContextFrame();
            if (frinkEnumeration != null) {
                frinkEnumeration.dispose();
            }
            return VoidExpression.VOID;
        } catch (NextException e) {
        } catch (BreakException e2) {
            String label2 = e2.getLabel();
            if (label2 == null) {
                VoidExpression voidExpression = VoidExpression.VOID;
                environment.removeContextFrame();
                if (frinkEnumeration == null) {
                    return voidExpression;
                }
                frinkEnumeration.dispose();
                return voidExpression;
            } else if (label2.equals(this.label)) {
                VoidExpression voidExpression2 = VoidExpression.VOID;
                environment.removeContextFrame();
                if (frinkEnumeration == null) {
                    return voidExpression2;
                }
                frinkEnumeration.dispose();
                return voidExpression2;
            } else {
                throw e2;
            }
        } catch (Throwable th) {
            environment.removeContextFrame();
            if (frinkEnumeration != null) {
                frinkEnumeration.dispose();
            }
            throw th;
        }
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
