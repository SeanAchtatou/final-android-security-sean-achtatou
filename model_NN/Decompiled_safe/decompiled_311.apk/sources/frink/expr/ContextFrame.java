package frink.expr;

import frink.object.ThisContextFrame;
import java.util.Vector;

public interface ContextFrame {
    boolean canCreateNewVariables();

    boolean canModifyVariables();

    SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException, FrinkSecurityException;

    ThisContextFrame deepCopy(int i);

    SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws CannotAssignException, FrinkSecurityException;

    SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException;
}
