package frink.expr;

import org.apache.oro.text.regex.Pattern;

public interface RegexpExpression {
    public static final String TYPE = "Regexp";

    Pattern getPattern();

    boolean isMultiple();
}
