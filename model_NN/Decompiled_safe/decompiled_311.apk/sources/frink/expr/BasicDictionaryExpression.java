package frink.expr;

import frink.function.BasicFunctionSource;
import frink.function.FunctionSource;
import frink.function.SingleArgMethod;
import frink.function.ZeroArgMethod;
import frink.object.EmptyObjectContextFrame;
import frink.object.FrinkObject;
import frink.symbolic.MatchingContext;
import java.util.Enumeration;
import java.util.Hashtable;

public class BasicDictionaryExpression extends TerminalExpression implements DictionaryExpression, EnumeratingExpression, FrinkObject, DeepCopyable {
    private static final BasicDictionaryFunctionSource methods = new BasicDictionaryFunctionSource();
    private EmptyObjectContextFrame contextFrame;
    /* access modifiers changed from: private */
    public Hashtable<HashingExpression, Expression> hash;

    public BasicDictionaryExpression() {
        this.hash = new Hashtable<>(5);
        this.contextFrame = null;
    }

    public BasicDictionaryExpression(BasicDictionaryExpression basicDictionaryExpression, int i) {
        this.contextFrame = null;
        if (i == 0) {
            this.hash = (Hashtable) basicDictionaryExpression.hash.clone();
            return;
        }
        this.hash = new Hashtable<>(basicDictionaryExpression.hash.size());
        Enumeration<HashingExpression> keys = basicDictionaryExpression.hash.keys();
        int i2 = i < 0 ? i : i - 1;
        while (keys.hasMoreElements()) {
            HashingExpression nextElement = keys.nextElement();
            Expression expression = basicDictionaryExpression.hash.get(nextElement);
            expression = expression instanceof DeepCopyable ? ((DeepCopyable) expression).deepCopy(i2) : expression;
            if (nextElement instanceof DeepCopyable) {
                nextElement = (HashingExpression) ((DeepCopyable) nextElement).deepCopy(i2);
            }
            this.hash.put(nextElement, expression);
        }
    }

    public Expression get(HashingExpression hashingExpression) {
        return this.hash.get(hashingExpression);
    }

    public void put(HashingExpression hashingExpression, Expression expression) {
        this.hash.put(hashingExpression, expression);
    }

    public boolean containsKey(HashingExpression hashingExpression) {
        return this.hash.containsKey(hashingExpression);
    }

    public Expression keys() {
        return new EnumerationWrapper(this.hash.keys(), HashingExpressionFactory.INSTANCE);
    }

    public int getSize() {
        return this.hash.size();
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isConstant() {
        return false;
    }

    public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
        return new DictEnumerator();
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return methods;
    }

    public ContextFrame getContextFrame(Environment environment) {
        if (this.contextFrame == null) {
            this.contextFrame = new EmptyObjectContextFrame(this);
        }
        return this.contextFrame;
    }

    public Expression deepCopy(int i) {
        return new BasicDictionaryExpression(this, i);
    }

    private class DictEnumerator implements FrinkEnumeration {
        private Enumeration<HashingExpression> keyEnum;

        public DictEnumerator() {
            this.keyEnum = BasicDictionaryExpression.this.hash.keys();
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (!this.keyEnum.hasMoreElements()) {
                return null;
            }
            HashingExpression nextElement = this.keyEnum.nextElement();
            BasicListExpression basicListExpression = new BasicListExpression(2);
            basicListExpression.appendChild(nextElement);
            basicListExpression.appendChild((Expression) BasicDictionaryExpression.this.hash.get(nextElement));
            return basicListExpression;
        }

        public void dispose() {
            this.keyEnum = null;
        }
    }

    public boolean isA(String str) {
        return str.equals(DictionaryExpression.TYPE);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        return false;
    }

    public String getExpressionType() {
        return DictionaryExpression.TYPE;
    }

    private static class BasicDictionaryFunctionSource extends BasicFunctionSource {
        BasicDictionaryFunctionSource() {
            super("BasicDictionaryExpression");
            addFunctionDefinition("clear", new ZeroArgMethod<BasicDictionaryExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicDictionaryExpression basicDictionaryExpression) {
                    basicDictionaryExpression.hash.clear();
                    return basicDictionaryExpression;
                }
            });
            addFunctionDefinition("containsKey", new SingleArgMethod<BasicDictionaryExpression>(false) {
                /* access modifiers changed from: protected */
                public Expression doMethod(Environment environment, BasicDictionaryExpression basicDictionaryExpression, Expression expression) throws InvalidArgumentException {
                    if (expression instanceof HashingExpression) {
                        return FrinkBoolean.create(basicDictionaryExpression.containsKey((HashingExpression) expression));
                    }
                    throw new InvalidArgumentException("containsKey: Dictionary can never contain value " + environment.format(expression) + " because it's not a hashable expression.", expression);
                }
            });
        }
    }
}
