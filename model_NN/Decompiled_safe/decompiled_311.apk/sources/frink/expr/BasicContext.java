package frink.expr;

import java.util.Stack;
import java.util.Vector;

public class BasicContext implements Context {
    private Stack<ContextFrame> frames = new Stack<>();
    private int highestFrame;
    private int[] lowestArray;
    private int lowestFrame;

    public BasicContext() {
        this.frames.push(null);
        this.lowestArray = new int[16];
        this.lowestArray[0] = 0;
        this.lowestFrame = 0;
        this.highestFrame = 0;
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws CannotAssignException, FrinkSecurityException {
        SymbolDefinition symbolDefinition;
        int i = this.highestFrame;
        while (true) {
            int i2 = i;
            if (i2 >= this.lowestFrame) {
                ContextFrame elementAt = this.frames.elementAt(i2);
                if (elementAt != null && (symbolDefinition = elementAt.getSymbolDefinition(str, z, environment)) != null) {
                    return symbolDefinition;
                }
                i = i2 - 1;
            } else {
                if (z) {
                    createInLowest(str, null, null, environment);
                }
                return null;
            }
        }
    }

    public SymbolDefinition setSymbol(String str, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException {
        int i = this.highestFrame;
        while (true) {
            int i2 = i;
            if (i2 < this.lowestFrame) {
                return createInLowest(str, null, expression, environment);
            }
            ContextFrame elementAt = this.frames.elementAt(i2);
            if (elementAt == null || elementAt.getSymbolDefinition(str, false, environment) == null) {
                i = i2 - 1;
            } else if (elementAt.canModifyVariables()) {
                try {
                    return elementAt.setSymbolDefinition(str, expression, environment);
                } catch (CannotAssignException e) {
                    throw new CannotAssignException("BasicContext:  Cannot set symbol " + str + ", ContextFrame threw exception:\n  " + e, expression);
                }
            } else {
                throw new CannotAssignException("BasicContext:  Cannot modify symbol " + str + ", ContextFrame definining it is unmodifiable.", expression);
            }
        }
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws VariableExistsException, CannotAssignException, FrinkSecurityException {
        try {
            if (getSymbolDefinition(str, false, environment) != null) {
                throw new VariableExistsException("Variable \"" + str + "\" is already defined.", expression);
            }
        } catch (CannotAssignException e) {
            System.out.println("BasicContext.declareVariable: Unexpected exception.  This probably shouldn't happen.\n");
            System.out.println(e);
        }
        return createInLowest(str, vector, expression, environment);
    }

    private SymbolDefinition createInLowest(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws CannotAssignException, FrinkSecurityException {
        SymbolDefinition declareVariable;
        int i = this.highestFrame;
        while (true) {
            int i2 = i;
            if (i2 >= this.lowestFrame) {
                ContextFrame elementAt = this.frames.elementAt(i2);
                if (elementAt == null) {
                    elementAt = new BasicContextFrame();
                    this.frames.setElementAt(elementAt, i2);
                }
                if (!elementAt.canCreateNewVariables()) {
                    i = i2 - 1;
                } else if (vector == null) {
                    try {
                        declareVariable = elementAt.setSymbolDefinition(str, expression, environment);
                        break;
                    } catch (ConstraintException e) {
                        throw e;
                    } catch (CannotAssignException e2) {
                    }
                } else {
                    try {
                        declareVariable = elementAt.declareVariable(str, vector, expression, environment);
                        break;
                    } catch (VariableExistsException e3) {
                        System.out.println("BasicContext.createInLowest got VariableExistsException:\n" + e3);
                    }
                }
            } else {
                throw new CannotAssignException("BasicContext:  Cannot assign " + str + " to any assignable ContextFrame.", expression);
            }
        }
        return declareVariable;
    }

    public void addContextFrame(ContextFrame contextFrame, boolean z) {
        this.highestFrame++;
        if (z) {
            this.lowestFrame = this.highestFrame;
        }
        if (this.highestFrame >= this.lowestArray.length) {
            enlargeLowestArray();
        }
        this.lowestArray[this.highestFrame] = this.lowestFrame;
        this.frames.push(contextFrame);
    }

    public ContextFrame removeContextFrame() {
        this.highestFrame--;
        ContextFrame pop = this.frames.pop();
        this.lowestFrame = this.lowestArray[this.highestFrame];
        return pop;
    }

    private void enlargeLowestArray() {
        int length = this.lowestArray.length;
        int[] iArr = new int[(length * 2)];
        System.arraycopy(this.lowestArray, 0, iArr, 0, length);
        this.lowestArray = iArr;
    }
}
