package frink.expr;

import frink.symbolic.MatchingContext;

public class NextExpression extends TerminalExpression {
    public static final String TYPE = "next";
    private NextException ne = new NextException(this);

    public Expression evaluate(Environment environment) throws NextException {
        throw this.ne;
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
