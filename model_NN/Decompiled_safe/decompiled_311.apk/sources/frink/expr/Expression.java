package frink.expr;

import frink.symbolic.MatchingContext;

public interface Expression {
    Expression evaluate(Environment environment) throws EvaluationException;

    Expression getChild(int i) throws InvalidChildException;

    int getChildCount();

    String getExpressionType();

    boolean isConstant();

    boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z);
}
