package frink.expr;

import frink.errors.NotAnIntegerException;
import frink.function.BuiltinFunctionSource;

public class ArrayUtils {
    public static ListExpression makeArray(ListExpression listExpression, Expression expression, Environment environment) throws InvalidArgumentException {
        return makeArray(getIntArray(listExpression, environment), 0, expression, environment);
    }

    public static ListExpression makeArray(int[] iArr, Expression expression, Environment environment) throws InvalidArgumentException {
        return makeArray(iArr, 0, expression, environment);
    }

    private static ListExpression makeArray(int[] iArr, int i, Expression expression, Environment environment) throws InvalidArgumentException {
        int i2 = 0;
        int length = iArr.length;
        if (length < 1) {
            throw new InvalidArgumentException("Attempted to make an array with " + length + " dimensions.  Number of dimensions must be at least 1.", null);
        }
        int i3 = iArr[i];
        if (i != length - 1) {
            BasicListExpression basicListExpression = new BasicListExpression(i3);
            while (i2 < i3) {
                basicListExpression.appendChild(makeArray(iArr, i + 1, expression, environment));
                i2++;
            }
            return basicListExpression;
        } else if (expression == null) {
            return new BasicListExpression(0);
        } else {
            BasicListExpression basicListExpression2 = new BasicListExpression(i3);
            while (i2 < i3) {
                basicListExpression2.appendChild(expression);
                i2++;
            }
            return basicListExpression2;
        }
    }

    public static ListExpression toArray(Expression expression, Environment environment) throws EvaluationException {
        if (expression instanceof ListExpression) {
            return (ListExpression) expression;
        }
        BasicListExpression basicListExpression = new BasicListExpression(1);
        if (expression instanceof EnumeratingExpression) {
            FrinkEnumeration enumeration = ((EnumeratingExpression) expression).getEnumeration(environment);
            while (true) {
                try {
                    Expression next = enumeration.getNext(environment);
                    if (next == null) {
                        return basicListExpression;
                    }
                    basicListExpression.appendChild(next);
                } finally {
                    enumeration.dispose();
                }
            }
        } else {
            basicListExpression.appendChild(expression);
            return basicListExpression;
        }
    }

    public static int[] getIntArray(ListExpression listExpression, Environment environment) throws InvalidArgumentException {
        int childCount = listExpression.getChildCount();
        int[] iArr = new int[childCount];
        int i = 0;
        while (i < childCount) {
            try {
                iArr[i] = BuiltinFunctionSource.getIntegerValue(listExpression.getChild(i));
                i++;
            } catch (InvalidChildException e) {
                throw new InvalidArgumentException("ArrayUtils.getIntArray got unexpected InvalidChildException.", listExpression);
            } catch (NotAnIntegerException e2) {
                throw new InvalidArgumentException("ArrayUtils.getIntArray passed something that wasn't a list of integers: " + environment.format(listExpression), listExpression);
            }
        }
        return iArr;
    }
}
