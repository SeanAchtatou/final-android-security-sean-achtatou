package frink.expr;

import frink.symbolic.MatchingContext;

public class MultiForLoop extends NonTerminalExpression {
    public static final String TYPE = "multifor";
    private AssignableExpression[] assignTo = null;
    private boolean assignToArray;
    private String label;
    private AssignableExpression singleAssign = null;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MultiForLoop(AssignableExpression assignableExpression, Expression expression, Expression expression2, String str) {
        super(2);
        appendChild(expression2);
        appendChild(expression);
        this.label = str;
        try {
            if (assignableExpression instanceof ListExpression) {
                this.assignToArray = true;
                int childCount = assignableExpression.getChildCount();
                this.assignTo = new AssignableExpression[childCount];
                for (int i = 0; i < childCount; i++) {
                    Expression child = assignableExpression.getChild(i);
                    if (child instanceof AssignableExpression) {
                        this.assignTo[i] = (AssignableExpression) child;
                    } else {
                        System.out.println("MultiForLoop:  argument not assignable.");
                    }
                }
                return;
            }
            this.assignToArray = false;
            this.singleAssign = assignableExpression;
        } catch (InvalidChildException e) {
            System.out.println("MultiForLoop:  Unexpected child count.");
        }
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public frink.expr.Expression evaluate(frink.expr.Environment r15) throws frink.expr.EvaluationException {
        /*
            r14 = this;
            r13 = 0
            r12 = 1
            r11 = 0
            frink.expr.Expression r0 = r14.getChild(r12)
            boolean r1 = r0 instanceof frink.expr.ListExpression
            if (r1 != 0) goto L_0x000f
            frink.expr.Expression r0 = r0.evaluate(r15)
        L_0x000f:
            boolean r1 = r0 instanceof frink.expr.ListExpression
            if (r1 == 0) goto L_0x0025
            int r1 = r0.getChildCount()
            frink.expr.Expression[] r2 = new frink.expr.Expression[r1]
            r3 = r11
        L_0x001a:
            if (r3 >= r1) goto L_0x0042
            frink.expr.Expression r4 = r0.getChild(r3)
            r2[r3] = r4
            int r3 = r3 + 1
            goto L_0x001a
        L_0x0025:
            frink.expr.InvalidArgumentException r1 = new frink.expr.InvalidArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "MultiForLoop:  right-hand-side is not a list of enumerating expressions "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r15.format(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0, r14)
            throw r1
        L_0x0042:
            frink.expr.FrinkEnumeration[] r0 = new frink.expr.FrinkEnumeration[r1]
            r3 = r11
        L_0x0045:
            if (r3 >= r1) goto L_0x0052
            r4 = r2[r3]
            frink.expr.FrinkEnumeration r4 = r14.getNextEnumeration(r4, r15)
            r0[r3] = r4
            int r3 = r3 + 1
            goto L_0x0045
        L_0x0052:
            frink.expr.Expression r3 = r14.getChild(r11)
            boolean r4 = r14.assignToArray
            if (r4 == 0) goto L_0x0081
            frink.expr.AssignableExpression[] r4 = r14.assignTo
            int r4 = r4.length
        L_0x005d:
            frink.expr.BasicContextFrame r5 = new frink.expr.BasicContextFrame
            r5.<init>()
            r15.addContextFrame(r5, r11)
            boolean r5 = r14.assignToArray
            if (r5 != 0) goto L_0x018a
            frink.expr.BasicListExpression r5 = new frink.expr.BasicListExpression
            r5.<init>(r1)
        L_0x006e:
            r6 = r1
        L_0x006f:
            if (r6 >= r4) goto L_0x009e
            boolean r7 = r14.assignToArray     // Catch:{ all -> 0x0089 }
            if (r7 == 0) goto L_0x0083
            frink.expr.AssignableExpression[] r7 = r14.assignTo     // Catch:{ all -> 0x0089 }
            r7 = r7[r6]     // Catch:{ all -> 0x0089 }
            frink.expr.UndefExpression r8 = frink.expr.UndefExpression.UNDEF     // Catch:{ all -> 0x0089 }
            r7.assign(r8, r15)     // Catch:{ all -> 0x0089 }
        L_0x007e:
            int r6 = r6 + 1
            goto L_0x006f
        L_0x0081:
            r4 = r1
            goto L_0x005d
        L_0x0083:
            frink.expr.UndefExpression r7 = frink.expr.UndefExpression.UNDEF     // Catch:{ all -> 0x0089 }
            r5.setChild(r6, r7)     // Catch:{ all -> 0x0089 }
            goto L_0x007e
        L_0x0089:
            r2 = move-exception
            r15.removeContextFrame()
            r3 = r11
        L_0x008e:
            if (r3 >= r1) goto L_0x0183
            r4 = r0[r3]
            if (r4 == 0) goto L_0x009b
            r4 = r0[r3]
            r4.dispose()
            r0[r3] = r13
        L_0x009b:
            int r3 = r3 + 1
            goto L_0x008e
        L_0x009e:
            r6 = r11
            r7 = r11
        L_0x00a0:
            int r8 = r1 - r12
            if (r6 >= r8) goto L_0x00c0
            r8 = r0[r6]     // Catch:{ all -> 0x0089 }
            frink.expr.Expression r8 = r8.getNext(r15)     // Catch:{ all -> 0x0089 }
            if (r8 == 0) goto L_0x00be
            boolean r9 = r14.assignToArray     // Catch:{ all -> 0x0089 }
            if (r9 == 0) goto L_0x00ba
            frink.expr.AssignableExpression[] r9 = r14.assignTo     // Catch:{ all -> 0x0089 }
            r9 = r9[r6]     // Catch:{ all -> 0x0089 }
            r9.assign(r8, r15)     // Catch:{ all -> 0x0089 }
        L_0x00b7:
            int r6 = r6 + 1
            goto L_0x00a0
        L_0x00ba:
            r5.setChild(r6, r8)     // Catch:{ all -> 0x0089 }
            goto L_0x00b7
        L_0x00be:
            r7 = r12
            goto L_0x00b7
        L_0x00c0:
            int r6 = r1 - r12
        L_0x00c2:
            r8 = r0[r6]     // Catch:{ all -> 0x0089 }
            frink.expr.Expression r8 = r8.getNext(r15)     // Catch:{ all -> 0x0089 }
            if (r8 == 0) goto L_0x0106
            if (r6 >= r4) goto L_0x00d7
            boolean r9 = r14.assignToArray     // Catch:{ all -> 0x0089 }
            if (r9 == 0) goto L_0x0102
            frink.expr.AssignableExpression[] r9 = r14.assignTo     // Catch:{ all -> 0x0089 }
            r6 = r9[r6]     // Catch:{ all -> 0x0089 }
            r6.assign(r8, r15)     // Catch:{ all -> 0x0089 }
        L_0x00d7:
            int r6 = r1 - r12
        L_0x00d9:
            if (r7 != 0) goto L_0x00ec
            boolean r8 = r14.assignToArray     // Catch:{ NextException -> 0x0187, BreakException -> 0x0143 }
            if (r8 != 0) goto L_0x00e9
            frink.expr.AssignableExpression r8 = r14.singleAssign     // Catch:{ NextException -> 0x0187, BreakException -> 0x0143 }
            frink.expr.BasicListExpression r9 = new frink.expr.BasicListExpression     // Catch:{ NextException -> 0x0187, BreakException -> 0x0143 }
            r9.<init>(r5)     // Catch:{ NextException -> 0x0187, BreakException -> 0x0143 }
            r8.assign(r9, r15)     // Catch:{ NextException -> 0x0187, BreakException -> 0x0143 }
        L_0x00e9:
            r3.evaluate(r15)     // Catch:{ NextException -> 0x0187, BreakException -> 0x0143 }
        L_0x00ec:
            if (r7 == 0) goto L_0x00c2
            r15.removeContextFrame()
            r2 = r11
        L_0x00f2:
            if (r2 >= r1) goto L_0x0184
            r3 = r0[r2]
            if (r3 == 0) goto L_0x00ff
            r3 = r0[r2]
            r3.dispose()
            r0[r2] = r13
        L_0x00ff:
            int r2 = r2 + 1
            goto L_0x00f2
        L_0x0102:
            r5.setChild(r6, r8)     // Catch:{ all -> 0x0089 }
            goto L_0x00d7
        L_0x0106:
            r8 = r0[r6]     // Catch:{ all -> 0x0089 }
            r8.dispose()     // Catch:{ all -> 0x0089 }
            r8 = 0
            r0[r6] = r8     // Catch:{ all -> 0x0089 }
            if (r6 != 0) goto L_0x0112
            r7 = r12
            goto L_0x00d9
        L_0x0112:
            r8 = r6
        L_0x0113:
            if (r8 >= r1) goto L_0x013f
            r9 = r0[r8]     // Catch:{ all -> 0x0089 }
            if (r9 != 0) goto L_0x013f
            r9 = r2[r8]     // Catch:{ all -> 0x0089 }
            frink.expr.FrinkEnumeration r9 = r14.getNextEnumeration(r9, r15)     // Catch:{ all -> 0x0089 }
            r0[r8] = r9     // Catch:{ all -> 0x0089 }
            r9 = r0[r8]     // Catch:{ all -> 0x0089 }
            frink.expr.Expression r9 = r9.getNext(r15)     // Catch:{ all -> 0x0089 }
            if (r8 >= r4) goto L_0x0136
            if (r9 == 0) goto L_0x013d
            boolean r10 = r14.assignToArray     // Catch:{ all -> 0x0089 }
            if (r10 == 0) goto L_0x0139
            frink.expr.AssignableExpression[] r10 = r14.assignTo     // Catch:{ all -> 0x0089 }
            r10 = r10[r8]     // Catch:{ all -> 0x0089 }
            r10.assign(r9, r15)     // Catch:{ all -> 0x0089 }
        L_0x0136:
            int r8 = r8 + 1
            goto L_0x0113
        L_0x0139:
            r5.setChild(r8, r9)     // Catch:{ all -> 0x0089 }
            goto L_0x0136
        L_0x013d:
            r7 = r12
            goto L_0x00d9
        L_0x013f:
            int r6 = r6 - r12
            if (r7 == 0) goto L_0x00c2
            goto L_0x00d9
        L_0x0143:
            r2 = move-exception
            java.lang.String r3 = r2.getLabel()     // Catch:{ all -> 0x0089 }
            if (r3 == 0) goto L_0x016b
            java.lang.String r4 = r14.label     // Catch:{ all -> 0x0089 }
            boolean r3 = r3.equals(r4)     // Catch:{ all -> 0x0089 }
            if (r3 == 0) goto L_0x016a
            frink.expr.VoidExpression r2 = frink.expr.VoidExpression.VOID     // Catch:{ all -> 0x0089 }
            r15.removeContextFrame()
            r3 = r11
        L_0x0158:
            if (r3 >= r1) goto L_0x0168
            r4 = r0[r3]
            if (r4 == 0) goto L_0x0165
            r4 = r0[r3]
            r4.dispose()
            r0[r3] = r13
        L_0x0165:
            int r3 = r3 + 1
            goto L_0x0158
        L_0x0168:
            r0 = r2
        L_0x0169:
            return r0
        L_0x016a:
            throw r2     // Catch:{ all -> 0x0089 }
        L_0x016b:
            frink.expr.VoidExpression r2 = frink.expr.VoidExpression.VOID     // Catch:{ all -> 0x0089 }
            r15.removeContextFrame()
            r3 = r11
        L_0x0171:
            if (r3 >= r1) goto L_0x0181
            r4 = r0[r3]
            if (r4 == 0) goto L_0x017e
            r4 = r0[r3]
            r4.dispose()
            r0[r3] = r13
        L_0x017e:
            int r3 = r3 + 1
            goto L_0x0171
        L_0x0181:
            r0 = r2
            goto L_0x0169
        L_0x0183:
            throw r2
        L_0x0184:
            frink.expr.VoidExpression r0 = frink.expr.VoidExpression.VOID
            goto L_0x0169
        L_0x0187:
            r8 = move-exception
            goto L_0x00ec
        L_0x018a:
            r5 = r13
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.expr.MultiForLoop.evaluate(frink.expr.Environment):frink.expr.Expression");
    }

    private FrinkEnumeration getNextEnumeration(Expression expression, Environment environment) throws InvalidArgumentException, EvaluationException {
        Expression evaluate = expression.evaluate(environment);
        if (evaluate instanceof EnumeratingExpression) {
            return ((EnumeratingExpression) evaluate).getEnumeration(environment);
        }
        throw new InvalidArgumentException("Argument to multifor is not an enumerating expression.  Argument was: " + environment.format(evaluate), evaluate);
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
