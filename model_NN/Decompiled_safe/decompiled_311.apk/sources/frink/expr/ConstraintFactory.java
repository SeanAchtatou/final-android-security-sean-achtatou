package frink.expr;

import frink.units.Source;
import java.util.Vector;

public interface ConstraintFactory {
    void addConstraintSource(Source<Constraint> source);

    Constraint createConstraint(String str);

    Vector<Constraint> createConstraints(Vector<String> vector) throws UnknownConstraintException;

    void removeConstraintSource(String str);
}
