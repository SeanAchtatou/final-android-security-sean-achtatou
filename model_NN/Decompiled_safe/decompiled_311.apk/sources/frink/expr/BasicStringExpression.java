package frink.expr;

import frink.symbolic.MatchingContext;

public final class BasicStringExpression extends TerminalExpression implements StringExpression, HashingExpression {
    public static final BasicStringExpression EMPTY_STRING = new BasicStringExpression("");
    public static final String TYPE = "String";
    private String str;

    public BasicStringExpression(String str2) {
        this.str = str2;
    }

    public BasicStringExpression(StringBuffer stringBuffer) {
        this.str = new String(stringBuffer);
    }

    public final Expression evaluate(Environment environment) {
        return this;
    }

    public final boolean isConstant() {
        return true;
    }

    public String getString() {
        return this.str;
    }

    public int hashCode() {
        return this.str.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof StringExpression) {
            return ((StringExpression) obj).getString().equals(this.str);
        }
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return equals(expression);
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }

    public String getExpressionType() {
        return TYPE;
    }
}
