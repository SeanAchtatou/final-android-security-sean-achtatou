package frink.expr;

import frink.symbolic.Backtrackable;
import frink.symbolic.MatchingContext;
import frink.symbolic.SymbolicUtils;
import java.util.Vector;

public abstract class NonTerminalExpression implements Expression {
    private static final boolean DEBUG = false;
    protected Vector<Expression> children;

    public NonTerminalExpression(int i) {
        if (i > 0) {
            this.children = new Vector<>(i);
        } else {
            this.children = null;
        }
    }

    public NonTerminalExpression(NonTerminalExpression nonTerminalExpression) {
        if (nonTerminalExpression.children == null) {
            this.children = null;
        } else {
            this.children = (Vector) nonTerminalExpression.children.clone();
        }
    }

    public NonTerminalExpression(NonTerminalExpression nonTerminalExpression, int i) {
        int i2;
        if (nonTerminalExpression.children == null) {
            this.children = null;
        } else if (i == 0) {
            this.children = (Vector) nonTerminalExpression.children.clone();
        } else {
            int size = nonTerminalExpression.children.size();
            if (i < 0) {
                i2 = i;
            } else {
                i2 = i - 1;
            }
            this.children = new Vector<>(size);
            int i3 = 0;
            while (i3 < size) {
                try {
                    Expression child = nonTerminalExpression.getChild(i3);
                    this.children.addElement(child instanceof DeepCopyable ? ((DeepCopyable) child).deepCopy(i2) : child);
                    i3++;
                } catch (InvalidChildException e) {
                    System.err.println("NonterminalExpression:  Unexpected InvalidChildException:\n  " + e);
                    return;
                }
            }
        }
    }

    public void appendChild(Expression expression) {
        if (this.children == null) {
            this.children = new Vector<>(1);
        }
        this.children.addElement(expression);
    }

    /* access modifiers changed from: protected */
    public void replaceChild(int i, Expression expression) {
        this.children.setElementAt(expression, i);
    }

    /* access modifiers changed from: protected */
    public void replaceChildren(ListExpression listExpression) throws InvalidChildException {
        int childCount = listExpression.getChildCount();
        if (this.children == null) {
            this.children = new Vector<>(childCount);
        } else {
            this.children.setSize(childCount);
        }
        for (int i = 0; i < childCount; i++) {
            this.children.setElementAt(listExpression.getChild(i), i);
        }
    }

    /* access modifiers changed from: protected */
    public void insertBefore(int i, Expression expression) {
        if (this.children == null) {
            this.children = new Vector<>(1);
        }
        this.children.insertElementAt(expression, i);
    }

    public int getChildCount() {
        if (this.children == null) {
            return 0;
        }
        return this.children.size();
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (this.children == null) {
            throw new InvalidChildException("Bad child " + i, this);
        }
        try {
            return this.children.elementAt(i);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new InvalidChildException("NonTerminalExpression: bad child " + i, this);
        }
    }

    /* access modifiers changed from: protected */
    public boolean childrenEqual(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        int childCount = getChildCount();
        if (expression.getChildCount() != childCount) {
            return false;
        }
        int i = 0;
        while (i < childCount) {
            try {
                if (!getChild(i).structureEquals(expression.getChild(i), matchingContext, environment, z)) {
                    return false;
                }
                i++;
            } catch (InvalidChildException e) {
                System.err.println("NonTerminalExpression:  Unexpected InvalidChildException.\n  " + e);
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean childrenEqualPermuted(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        try {
            if (!SymbolicUtils.containsPattern(this) && !SymbolicUtils.containsPattern(expression)) {
                return childrenEqual(expression, matchingContext, environment, z);
            }
        } catch (InvalidChildException e) {
            System.err.println("InvalidChildException in childrenEqualPermuted.");
        }
        if (expression.getChildCount() > getChildCount()) {
            return childrenEqualPermuted(expression, this, matchingContext, environment, z);
        }
        return childrenEqualPermuted(this, expression, matchingContext, environment, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r14 = frink.symbolic.ExpressionConstructor.construct(r22.getExpressionType(), r16, r25);
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00f8 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean childrenEqualPermuted(frink.expr.Expression r22, frink.expr.Expression r23, frink.symbolic.MatchingContext r24, frink.expr.Environment r25, boolean r26) {
        /*
            int r6 = r22.getChildCount()
            int r7 = r23.getChildCount()
            r0 = r24
            r1 = r22
            r2 = r23
            r3 = r25
            frink.symbolic.Backtrackable r5 = r0.getState(r1, r2, r3)
            frink.expr.NonTerminalExpression$MatchState r5 = (frink.expr.NonTerminalExpression.MatchState) r5
            frink.symbolic.Backtrackable r8 = r24.getParentState()
            if (r5 == 0) goto L_0x0030
            r9 = 1
        L_0x001d:
            if (r9 != 0) goto L_0x0032
            frink.expr.NonTerminalExpression$MatchState r5 = new frink.expr.NonTerminalExpression$MatchState
            r5.<init>(r6, r7)
            r0 = r5
            r1 = r25
            boolean r10 = r0.nextState(r1)
            if (r10 != 0) goto L_0x0032
            r22 = 0
        L_0x002f:
            return r22
        L_0x0030:
            r9 = 0
            goto L_0x001d
        L_0x0032:
            r10 = 1
        L_0x0033:
            if (r10 != 0) goto L_0x0110
            r11 = 0
            r0 = r24
            r1 = r5
            r2 = r11
            r3 = r25
            r0.clearState(r1, r2, r3)
            r0 = r24
            r1 = r22
            r2 = r23
            r3 = r5
            r4 = r8
            r0.pushState(r1, r2, r3, r4)
        L_0x004a:
            int r11 = r24.beginMatch()
            r12 = 1
            r13 = 0
        L_0x0050:
            if (r13 >= r7) goto L_0x01af
            r14 = 0
            int[] r15 = r5.slotUsed
            r15 = r15[r13]
            r16 = 1
            r0 = r15
            r1 = r16
            if (r0 <= r1) goto L_0x0120
            frink.expr.BasicListExpression r14 = new frink.expr.BasicListExpression
            int[] r16 = r5.slotUsed
            r16 = r16[r13]
            r0 = r14
            r1 = r16
            r0.<init>(r1)
            r16 = 1
            r21 = r16
            r16 = r14
            r14 = r21
        L_0x0076:
            r0 = r23
            r1 = r13
            frink.expr.Expression r17 = r0.getChild(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r18 = 0
            r19 = 0
            r21 = r19
            r19 = r18
            r18 = r21
        L_0x0087:
            r0 = r18
            r1 = r6
            if (r0 >= r1) goto L_0x01ab
            int[] r20 = r5.usedInSlot     // Catch:{ InvalidChildException -> 0x01a3 }
            r20 = r20[r18]     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r20
            r1 = r13
            if (r0 != r1) goto L_0x019f
            if (r14 == 0) goto L_0x0149
            r0 = r22
            r1 = r18
            frink.expr.Expression r20 = r0.getChild(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r16
            r1 = r20
            r0.appendChild(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            int r19 = r19 + 1
            r0 = r19
            r1 = r15
            if (r0 != r1) goto L_0x019f
            java.lang.String r14 = r22.getExpressionType()     // Catch:{ EvaluationException -> 0x012a }
            r0 = r14
            r1 = r16
            r2 = r25
            frink.expr.Expression r14 = frink.symbolic.ExpressionConstructor.construct(r0, r1, r2)     // Catch:{ EvaluationException -> 0x012a }
        L_0x00bc:
            boolean r15 = frink.symbolic.SymbolicUtils.containsPattern(r17)     // Catch:{ InvalidChildException -> 0x01a3 }
            if (r15 == 0) goto L_0x0135
            r0 = r24
            r1 = r5
            r0.setParentState(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r17
            r1 = r14
            r2 = r24
            r3 = r25
            r4 = r26
            boolean r14 = r0.structureEquals(r1, r2, r3, r4)     // Catch:{ InvalidChildException -> 0x01a3 }
        L_0x00d5:
            if (r14 != 0) goto L_0x01ab
            r0 = r24
            r1 = r11
            r0.rollbackMatch(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r12 = 0
        L_0x00de:
            boolean r13 = r5.isFinished()
            if (r13 != 0) goto L_0x00ef
            r0 = r24
            r1 = r5
            r2 = r25
            boolean r13 = r0.nextState(r1, r2)
            if (r13 != 0) goto L_0x004a
        L_0x00ef:
            r0 = r5
            r1 = r25
            boolean r13 = r0.nextState(r1)
            if (r13 != 0) goto L_0x0033
            if (r12 == 0) goto L_0x0100
            r0 = r24
            r1 = r11
            r0.rollbackMatch(r1)
        L_0x0100:
            r22 = 0
            r0 = r24
            r1 = r5
            r2 = r22
            r3 = r25
            r0.clearState(r1, r2, r3)
            r22 = 0
            goto L_0x002f
        L_0x0110:
            if (r9 != 0) goto L_0x011d
            r0 = r24
            r1 = r22
            r2 = r23
            r3 = r5
            r4 = r8
            r0.pushState(r1, r2, r3, r4)
        L_0x011d:
            r10 = 0
            goto L_0x004a
        L_0x0120:
            r16 = 0
            r21 = r16
            r16 = r14
            r14 = r21
            goto L_0x0076
        L_0x012a:
            r14 = move-exception
            java.lang.String r14 = "NonTerminalExpression.childrenEqualPermuted: Evaluation Exception in constructing list."
            r0 = r25
            r1 = r14
            r0.outputln(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r14 = 0
            goto L_0x00bc
        L_0x0135:
            r0 = r24
            r1 = r5
            r0.setParentState(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r14
            r1 = r17
            r2 = r24
            r3 = r25
            r4 = r26
            boolean r14 = r0.structureEquals(r1, r2, r3, r4)     // Catch:{ InvalidChildException -> 0x01a3 }
            goto L_0x00d5
        L_0x0149:
            if (r15 != 0) goto L_0x0161
            java.io.PrintStream r14 = java.lang.System.err     // Catch:{ InvalidChildException -> 0x01a3 }
            java.lang.String r15 = "AAAAAAAAAAAAAAGH!  Invalid state!"
            r14.println(r15)     // Catch:{ InvalidChildException -> 0x01a3 }
            r24.dumpState(r25)     // Catch:{ InvalidChildException -> 0x01a3 }
            r14 = 0
        L_0x0156:
            if (r14 != 0) goto L_0x01ab
            r0 = r24
            r1 = r11
            r0.rollbackMatch(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r12 = 0
            goto L_0x00de
        L_0x0161:
            boolean r14 = frink.symbolic.SymbolicUtils.containsPattern(r17)     // Catch:{ InvalidChildException -> 0x01a3 }
            if (r14 == 0) goto L_0x0183
            r0 = r24
            r1 = r5
            r0.setParentState(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r22
            r1 = r18
            frink.expr.Expression r14 = r0.getChild(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r17
            r1 = r14
            r2 = r24
            r3 = r25
            r4 = r26
            boolean r14 = r0.structureEquals(r1, r2, r3, r4)     // Catch:{ InvalidChildException -> 0x01a3 }
            goto L_0x0156
        L_0x0183:
            r0 = r24
            r1 = r5
            r0.setParentState(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r22
            r1 = r18
            frink.expr.Expression r14 = r0.getChild(r1)     // Catch:{ InvalidChildException -> 0x01a3 }
            r0 = r14
            r1 = r17
            r2 = r24
            r3 = r25
            r4 = r26
            boolean r14 = r0.structureEquals(r1, r2, r3, r4)     // Catch:{ InvalidChildException -> 0x01a3 }
            goto L_0x0156
        L_0x019f:
            int r18 = r18 + 1
            goto L_0x0087
        L_0x01a3:
            r14 = move-exception
            java.io.PrintStream r14 = java.lang.System.err
            java.lang.String r15 = "InvalidChild in something."
            r14.println(r15)
        L_0x01ab:
            int r13 = r13 + 1
            goto L_0x0050
        L_0x01af:
            if (r26 == 0) goto L_0x01b9
            if (r12 == 0) goto L_0x01b9
            r0 = r24
            r1 = r11
            r0.rollbackMatch(r1)
        L_0x01b9:
            r22 = 1
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.expr.NonTerminalExpression.childrenEqualPermuted(frink.expr.Expression, frink.expr.Expression, frink.symbolic.MatchingContext, frink.expr.Environment, boolean):boolean");
    }

    private static class MatchState implements Backtrackable {
        private int elements;
        private boolean finished = false;
        /* access modifiers changed from: private */
        public int[] slotUsed;
        private int slots;
        /* access modifiers changed from: private */
        public int[] usedInSlot;

        public MatchState(int i, int i2) {
            this.elements = i;
            this.slots = i2;
            this.usedInSlot = new int[i];
            this.slotUsed = new int[i2];
            for (int i3 = 0; i3 < i - 1; i3++) {
                this.usedInSlot[i3] = 0;
            }
            if (i > 0) {
                this.usedInSlot[i - 1] = -1;
            }
        }

        public boolean nextState(Environment environment) {
            int i;
            do {
                int i2 = this.elements - 1;
                while (i2 >= 0) {
                    int[] iArr = this.usedInSlot;
                    int i3 = iArr[i2] + 1;
                    iArr[i2] = i3;
                    if (i3 < this.slots) {
                        break;
                    }
                    this.usedInSlot[i2] = 0;
                    i2--;
                }
                if (i2 < 0) {
                    this.finished = true;
                    return false;
                }
                for (int i4 = 0; i4 < this.slots; i4++) {
                    this.slotUsed[i4] = 0;
                }
                i = 0;
                for (int i5 = 0; i5 < this.elements; i5++) {
                    int i6 = this.usedInSlot[i5];
                    if (this.slotUsed[i6] == 0) {
                        i++;
                    }
                    int[] iArr2 = this.slotUsed;
                    iArr2[i6] = iArr2[i6] + 1;
                }
            } while (i != this.slots);
            return true;
        }

        public String getStateAsString() {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < this.elements; i++) {
                stringBuffer.append(this.usedInSlot[i]);
            }
            return new String(stringBuffer);
        }

        public boolean isFinished() {
            return this.finished;
        }
    }
}
