package frink.expr;

import frink.units.Unit;

public interface UnitExpression extends HashingExpression {
    public static final String TYPE = "Unit";

    Unit getUnit();
}
