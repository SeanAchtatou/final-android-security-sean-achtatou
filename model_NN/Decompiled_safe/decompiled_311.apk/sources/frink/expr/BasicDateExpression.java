package frink.expr;

import frink.date.DateMath;
import frink.date.FrinkDate;
import frink.errors.NotRealException;
import frink.numeric.NotImplementedException;
import frink.numeric.NumericMath;
import frink.numeric.OverlapException;
import frink.symbolic.MatchingContext;

public class BasicDateExpression extends TerminalExpression implements DateExpression {
    public static final String TYPE = "Date";
    private FrinkDate date;

    public BasicDateExpression(FrinkDate frinkDate) {
        this.date = frinkDate;
    }

    public FrinkDate getFrinkDate() {
        return this.date;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isConstant() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof DateExpression) {
            return DateMath.compare(((DateExpression) expression).getFrinkDate(), this.date) == 0;
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public int hashCode() {
        return this.date.getJulian().hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DateExpression)) {
            return false;
        }
        try {
            if (NumericMath.compare(((DateExpression) obj).getFrinkDate().getJulian(), this.date.getJulian()) == 0) {
                return true;
            }
            return false;
        } catch (NotRealException e) {
            return false;
        } catch (NotImplementedException e2) {
            return false;
        } catch (OverlapException e3) {
            return false;
        }
    }

    public void iHaveOverriddenHashCodeAndEqualsDummyMethod() {
    }
}
