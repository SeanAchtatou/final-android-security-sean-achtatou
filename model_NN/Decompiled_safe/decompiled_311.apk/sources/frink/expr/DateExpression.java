package frink.expr;

import frink.date.FrinkDate;

public interface DateExpression extends HashingExpression {
    FrinkDate getFrinkDate();
}
