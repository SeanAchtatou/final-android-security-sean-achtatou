package frink.expr;

import frink.errors.NotAnIntegerException;
import frink.numeric.FrinkInteger;
import frink.numeric.Numeric;
import frink.symbolic.MatchingContext;
import frink.units.Unit;
import frink.units.UnitMath;

public class DereferenceOperator extends NonTerminalExpression implements AssignableExpression, SelfDisplayingExpression {
    public static final String TYPE = "Dereference";

    public DereferenceOperator(Expression expression, Expression expression2) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        Expression evaluate = getChild(0).evaluate(environment);
        Expression evaluate2 = getChild(1).evaluate(environment);
        if ((evaluate instanceof ListExpression) && (evaluate2 instanceof UnitExpression)) {
            Unit unit = ((UnitExpression) evaluate2).getUnit();
            if (UnitMath.isDimensionless(unit)) {
                Numeric scale = unit.getScale();
                if (scale.isFrinkInteger()) {
                    try {
                        return evaluate.getChild(((FrinkInteger) scale).getInt());
                    } catch (NotAnIntegerException e) {
                        throw new InvalidArgumentException("Dereference operator: Index is out of range: " + scale.toString(), this);
                    }
                }
            }
        }
        if (!(evaluate instanceof DictionaryExpression)) {
            return this;
        }
        if (evaluate2 instanceof HashingExpression) {
            Expression expression = ((DictionaryExpression) evaluate).get((HashingExpression) evaluate2);
            if (expression == null) {
                return UndefExpression.UNDEF;
            }
            return expression;
        }
        throw new TypeException("Right-hand side (" + environment.format(evaluate2) + ") is not hashable (1)", evaluate2);
    }

    public boolean isConstant() {
        return false;
    }

    private Expression getReference(Environment environment, boolean z) throws EvaluationException {
        Expression expression;
        Expression child = getChild(0);
        if (child instanceof SymbolExpression) {
            SymbolDefinition symbolDefinition = environment.getSymbolDefinition(((SymbolExpression) child).getName(), false);
            if (symbolDefinition == null) {
                throw new InvalidArgumentException("In expression '" + environment.format(this) + "' the symbol '" + environment.format(child, false) + "' is undefined.", this);
            }
            expression = symbolDefinition.getValue();
        } else if (child instanceof DereferenceOperator) {
            expression = ((DereferenceOperator) child).getReference(environment, true);
        } else {
            expression = null;
        }
        if (!z) {
            return expression;
        }
        if (expression instanceof ListExpression) {
            Expression evaluate = getChild(1).evaluate(environment);
            if (evaluate instanceof UnitExpression) {
                Unit unit = ((UnitExpression) evaluate).getUnit();
                if (UnitMath.isDimensionless(unit)) {
                    Numeric scale = unit.getScale();
                    if (scale.isFrinkInteger()) {
                        try {
                            return expression.getChild(((FrinkInteger) scale).getInt());
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("DereferenceOperator.getReference: Index is out of range: " + scale.toString(), this);
                        }
                    }
                }
            }
        }
        if (!(expression instanceof DictionaryExpression)) {
            return null;
        }
        Expression evaluate2 = getChild(1).evaluate(environment);
        if (evaluate2 instanceof HashingExpression) {
            Expression expression2 = ((DictionaryExpression) expression).get((HashingExpression) evaluate2);
            if (expression2 == null) {
                return UndefExpression.UNDEF;
            }
            return expression2;
        }
        throw new TypeException("Right-hand side (" + environment.format(evaluate2) + ") is not hashable (2)", evaluate2);
    }

    public void assign(Expression expression, Environment environment) throws EvaluationException {
        Expression reference = getReference(environment, false);
        if (reference == null) {
            throw new CannotAssignException("Cannot assign to left-hand side of " + environment.format(this), this);
        }
        if (reference instanceof ListExpression) {
            Expression evaluate = getChild(1).evaluate(environment);
            if (evaluate instanceof UnitExpression) {
                Unit unit = ((UnitExpression) evaluate).getUnit();
                if (UnitMath.isDimensionless(unit)) {
                    Numeric scale = unit.getScale();
                    if (scale.isFrinkInteger()) {
                        try {
                            ((ListExpression) reference).setChild(((FrinkInteger) scale).getInt(), expression);
                            return;
                        } catch (NotAnIntegerException e) {
                            throw new InvalidArgumentException("DereferenceOperator.assign: Index is out of range: " + scale.toString() + " in expression " + environment.format(this), this);
                        }
                    }
                }
            }
        }
        if (reference instanceof DictionaryExpression) {
            Expression evaluate2 = getChild(1).evaluate(environment);
            if (evaluate2 instanceof HashingExpression) {
                ((DictionaryExpression) reference).put((HashingExpression) evaluate2, expression);
                return;
            }
            throw new TypeException("Right-hand side (" + environment.format(evaluate2) + ") is not hashable (3) in " + environment.format(this), evaluate2);
        }
        throw new CannotAssignException("Cannot assign in " + environment.format(this), this);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (expression instanceof DereferenceOperator) {
            return childrenEqual(expression, matchingContext, environment, z);
        }
        return false;
    }

    public String getExpressionType() {
        return TYPE;
    }

    public String toString(Environment environment, boolean z) {
        try {
            return environment.format(getChild(0), false) + "@(" + environment.format(getChild(1), false) + ")";
        } catch (InvalidChildException e) {
            return "Unexpected InvalidChildException in DereferenceOperator.toString:\n  " + e;
        }
    }
}
