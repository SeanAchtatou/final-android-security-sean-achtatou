package frink.expr;

import frink.symbolic.MatchingContext;

public class BooleanAlgebraExpression extends CachingExpression implements OperatorExpression {
    public static final int AND = 1;
    public static final int IMPLIES = 7;
    public static final int NAND = 6;
    public static final int NOR = 5;
    public static final int NOT = 4;
    public static final int OR = 2;
    public static final int XOR = 3;
    private BooleanAlgebraExpressionType opType;

    private BooleanAlgebraExpression(BooleanAlgebraExpressionType booleanAlgebraExpressionType, int i) {
        super(i);
        this.opType = booleanAlgebraExpressionType;
    }

    private BooleanAlgebraExpression(Expression expression, Expression expression2, BooleanAlgebraExpressionType booleanAlgebraExpressionType) {
        super(2);
        appendChild(expression);
        appendChild(expression2);
        this.opType = booleanAlgebraExpressionType;
    }

    public static Expression construct(Expression expression, Expression expression2, BooleanAlgebraExpressionType booleanAlgebraExpressionType, Environment environment) throws InvalidChildException {
        BasicListExpression basicListExpression = new BasicListExpression(2);
        basicListExpression.appendChild(expression);
        basicListExpression.appendChild(expression2);
        return construct(booleanAlgebraExpressionType, basicListExpression, environment);
    }

    public static Expression constructNot(Expression expression) {
        boolean z;
        try {
            if (expression instanceof FrinkBoolean) {
                if (!((FrinkBoolean) expression).getBoolean()) {
                    z = true;
                } else {
                    z = false;
                }
                return FrinkBoolean.create(z);
            }
            if ((expression instanceof BooleanAlgebraExpression) && ((BooleanAlgebraExpression) expression).getExpressionTypeAsInteger() == 4) {
                return expression.getChild(0);
            }
            return new BooleanAlgebraExpression(expression);
        } catch (InvalidChildException e) {
            System.err.println("Unexpected invalid child exception in BooleanAlgebraException.constructNot");
        }
    }

    public static Expression construct(BooleanAlgebraExpressionType booleanAlgebraExpressionType, ListExpression listExpression, Environment environment) throws InvalidChildException {
        int childCount = listExpression.getChildCount();
        BooleanAlgebraExpression booleanAlgebraExpression = new BooleanAlgebraExpression(booleanAlgebraExpressionType, childCount);
        int maxChildren = booleanAlgebraExpressionType.getMaxChildren();
        if (booleanAlgebraExpressionType == BooleanAlgebraExpressionType.NOT && childCount == 1) {
            return constructNot(listExpression.getChild(0));
        }
        for (int i = 0; i < childCount; i++) {
            Expression child = listExpression.getChild(i);
            if (!(child instanceof BooleanAlgebraExpression) || booleanAlgebraExpressionType != ((BooleanAlgebraExpression) child).getBooleanAlgebraExpressionType() || maxChildren >= 0) {
                booleanAlgebraExpression.appendChild(child);
            } else {
                int childCount2 = child.getChildCount();
                for (int i2 = 0; i2 < childCount2; i2++) {
                    booleanAlgebraExpression.appendChild(child.getChild(i2));
                }
            }
        }
        return booleanAlgebraExpression;
    }

    private BooleanAlgebraExpression(Expression expression) {
        super(1);
        appendChild(expression);
        this.opType = BooleanAlgebraExpressionType.NOT;
    }

    /* access modifiers changed from: protected */
    public Expression doOperation(Environment environment) throws EvaluationException {
        boolean z;
        boolean z2;
        int childCount = getChildCount();
        int intType = this.opType.getIntType();
        switch (intType) {
            case 1:
            case 5:
                z = true;
                break;
            case 2:
            case 3:
            case 6:
                z = false;
                break;
            case 4:
            case 7:
                z = false;
                break;
            default:
                try {
                    environment.outputln("BooleanAlgebraExpression:  Unhandled type in first case: " + this.opType.getText());
                    z = false;
                    break;
                } catch (TypeException e) {
                    throw new InvalidArgumentException("BooleanAlgebraException: arguments must be boolean: " + e, this);
                }
        }
        boolean z3 = z;
        for (int i = 0; i < childCount; i++) {
            boolean isTrue = Truth.isTrue(environment, getChild(i).evaluate(environment));
            switch (intType) {
                case 1:
                    if (isTrue) {
                        break;
                    } else {
                        return FrinkBoolean.FALSE;
                    }
                case 2:
                    if (!isTrue) {
                        break;
                    } else {
                        return FrinkBoolean.TRUE;
                    }
                case 3:
                    z3 ^= isTrue;
                    break;
                case 4:
                    if (!isTrue) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    return FrinkBoolean.create(z2);
                case 5:
                    if (!isTrue) {
                        break;
                    } else {
                        return FrinkBoolean.FALSE;
                    }
                case 6:
                    if (isTrue) {
                        break;
                    } else {
                        return FrinkBoolean.TRUE;
                    }
                case 7:
                    if (i != 0) {
                        if (i != 1) {
                            environment.outputln("Fell through IMPLIES.");
                            break;
                        } else {
                            return FrinkBoolean.create(isTrue);
                        }
                    } else if (isTrue) {
                        break;
                    } else {
                        return FrinkBoolean.TRUE;
                    }
                default:
                    throw new InvalidArgumentException("BooleanAlgebraExpression: unimplemented comparison type " + this.opType, this);
            }
        }
        return FrinkBoolean.create(z3);
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof BooleanAlgebraExpression)) {
            return false;
        }
        if (this.opType != ((BooleanAlgebraExpression) expression).opType) {
            return false;
        }
        if (this.opType.isCommutative()) {
            return childrenEqualPermuted(expression, matchingContext, environment, z);
        }
        return childrenEqual(expression, matchingContext, environment, z);
    }

    public int getExpressionTypeAsInteger() {
        return this.opType.getIntType();
    }

    public String getExpressionType() {
        return this.opType.getText();
    }

    public BooleanAlgebraExpressionType getBooleanAlgebraExpressionType() {
        return this.opType;
    }

    public String getText() {
        return this.opType.getText();
    }

    public String getSymbol() {
        return this.opType.getSymbol();
    }

    public int getPrecedence() {
        return this.opType.getPrecedence();
    }

    public int getAssociativity() {
        return this.opType.getAssociativity();
    }
}
