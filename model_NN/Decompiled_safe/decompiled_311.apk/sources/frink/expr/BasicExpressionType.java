package frink.expr;

public class BasicExpressionType implements ExpressionType {
    private String name;

    public BasicExpressionType(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }
}
