package frink.expr;

import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Vector;

public class EnumerationStacker<T> implements Enumeration<T> {
    private Enumeration<T> currentEnum = null;
    private int currentIdx = 0;
    private Vector<Enumeration<T>> vec = null;

    public void addEnumeration(Enumeration<T> enumeration) {
        if (enumeration != null) {
            if (this.vec == null) {
                this.currentEnum = enumeration;
                this.currentIdx = 0;
                this.vec = new Vector<>(2);
            }
            this.vec.addElement(enumeration);
        }
    }

    public boolean hasMoreElements() {
        if (this.currentEnum == null) {
            return false;
        }
        if (this.currentEnum.hasMoreElements()) {
            return true;
        }
        nextEnumeration();
        return this.currentEnum != null && this.currentEnum.hasMoreElements();
    }

    public T nextElement() {
        if (this.currentEnum == null) {
            throw new NoSuchElementException("EnumerationStacker: Requested nonexistent element");
        } else if (this.currentEnum.hasMoreElements()) {
            return this.currentEnum.nextElement();
        } else {
            nextEnumeration();
            if (this.currentEnum != null) {
                return this.currentEnum.nextElement();
            }
            throw new NoSuchElementException("EnumerationStacker: Requested nonexistent element");
        }
    }

    private void nextEnumeration() {
        while (this.currentEnum != null && !this.currentEnum.hasMoreElements()) {
            this.vec.setElementAt(null, this.currentIdx);
            this.currentIdx++;
            if (this.currentIdx >= this.vec.size()) {
                this.currentEnum = null;
                this.vec = null;
                return;
            }
            this.currentEnum = this.vec.elementAt(this.currentIdx);
        }
    }
}
