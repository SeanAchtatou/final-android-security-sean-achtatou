package frink.expr;

import java.util.Vector;

public class ConstraintDefinition {
    private Vector<String> constraintNames;
    private Expression initVal;
    private String name;

    public ConstraintDefinition(String str, Vector<String> vector, Expression expression) {
        this.name = str;
        this.constraintNames = vector;
        this.initVal = expression;
    }

    public String getName() {
        return this.name;
    }

    public Vector<String> getConstraintNames() {
        return this.constraintNames;
    }

    public Expression getInitialValue() {
        return this.initVal;
    }
}
