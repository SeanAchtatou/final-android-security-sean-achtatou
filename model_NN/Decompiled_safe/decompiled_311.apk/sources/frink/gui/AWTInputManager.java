package frink.gui;

import frink.expr.Environment;
import frink.io.InputItem;
import frink.io.InputManager;
import java.awt.Button;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.ScrollPane;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

public class AWTInputManager extends Dialog implements InputManager {
    private GridBagConstraints fieldConstraints;
    /* access modifiers changed from: private */
    public Vector<TextComponent> fieldList;
    private Panel inputPanel;
    private GridBagConstraints labelConstraints;
    private Label promptLabel = new Label();
    private ScrollPane scroll;

    public AWTInputManager(Frame frame) {
        super(frame, "Frink Input", true);
        this.promptLabel.setAlignment(1);
        Button button = new Button("OK");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                AWTInputManager.this.hide();
            }
        });
        this.inputPanel = new Panel(new GridBagLayout());
        this.fieldList = new Vector<>();
        add(this.promptLabel, "North");
        add(this.inputPanel, "Center");
        add(button, "South");
        addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent windowEvent) {
                TextComponent textComponent = (TextComponent) AWTInputManager.this.fieldList.elementAt(0);
                textComponent.setCaretPosition(textComponent.getText().length());
                textComponent.requestFocus();
            }

            public void windowGainedFocus(WindowEvent windowEvent) {
            }
        });
        setResizable(true);
        this.labelConstraints = new GridBagConstraints();
        this.labelConstraints.gridx = 0;
        this.labelConstraints.anchor = 13;
        this.fieldConstraints = new GridBagConstraints();
        this.fieldConstraints.gridx = 1;
        this.fieldConstraints.weightx = 1.0d;
        this.fieldConstraints.gridwidth = 0;
        this.fieldConstraints.fill = 2;
    }

    private void packAndDisplay() {
        pack();
        validate();
        Container parent = getParent();
        Point locationOnScreen = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        Dimension preferredSize = getPreferredSize();
        int i = preferredSize.width;
        if (i < size.width) {
            i = size.width;
        }
        int i2 = preferredSize.height;
        setSize(i, i2);
        setLocation(((size.width - i) / 2) + locationOnScreen.x, ((size.height - i2) / 2) + locationOnScreen.y);
        show();
    }

    public String input(String str, String str2, Environment environment) {
        clearGUI();
        if (str == null) {
            this.promptLabel.setText("");
        } else {
            this.promptLabel.setText(str);
        }
        TextField addField = addField(null, str2);
        this.fieldList.addElement(addField);
        if (addField instanceof TextField) {
            addField.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    AWTInputManager.this.hide();
                }
            });
        }
        packAndDisplay();
        String text = this.fieldList.elementAt(0).getText();
        if (text == null) {
            return "";
        }
        return text;
    }

    public String[] input(String str, InputItem[] inputItemArr, Environment environment) {
        clearGUI();
        if (str == null) {
            this.promptLabel.setText("");
        } else {
            this.promptLabel.setText(str);
        }
        TextComponent textComponent = null;
        for (InputItem addInputItem : inputItemArr) {
            textComponent = addInputItem(addInputItem);
            this.fieldList.addElement(textComponent);
        }
        if (textComponent instanceof TextField) {
            ((TextField) textComponent).addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    AWTInputManager.this.hide();
                }
            });
        }
        packAndDisplay();
        int size = this.fieldList.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            String text = this.fieldList.elementAt(i).getText();
            if (text == null) {
                text = "";
            }
            strArr[i] = text;
        }
        return strArr;
    }

    private void clearGUI() {
        this.inputPanel.removeAll();
        this.fieldList.clear();
    }

    private TextComponent addInputItem(InputItem inputItem) {
        return addField(inputItem.label, inputItem.defaultValue);
    }

    private TextComponent addField(String str, String str2) {
        if (str != null) {
            Label label = new Label();
            label.setAlignment(2);
            label.setText(str);
            this.inputPanel.add(label, this.labelConstraints);
        }
        TextField textField = new TextField();
        textField.setColumns(40);
        if (str2 != null) {
            textField.setText(str2);
        }
        this.inputPanel.add(textField, this.fieldConstraints);
        return textField;
    }
}
