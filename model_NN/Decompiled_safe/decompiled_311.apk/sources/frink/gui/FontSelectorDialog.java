package frink.gui;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FontSelectorDialog extends Dialog {
    private Checkbox bold;
    private Button cancelButton;
    private Font font;
    private TextField fontSizeField;
    private Button okButton;

    public FontSelectorDialog(Frame frame, Font font2) {
        super(frame, "Font Selector", true);
        this.font = font2;
        initGUI();
    }

    private void initGUI() {
        Panel panel = new Panel(new GridLayout(3, 1, 0, 5));
        panel.add(new Label("Font Size (points)"));
        AnonymousClass1 r1 = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                FontSelectorDialog.this.changeFont();
                FontSelectorDialog.this.hide();
            }
        };
        this.fontSizeField = new TextField(3);
        this.fontSizeField.addActionListener(r1);
        panel.add(this.fontSizeField);
        this.bold = new Checkbox("Bold", false);
        panel.add(this.bold);
        add(panel, "Center");
        Panel panel2 = new Panel(new FlowLayout(2));
        this.okButton = new Button("OK");
        this.okButton.addActionListener(r1);
        this.cancelButton = new Button("Cancel");
        this.cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                FontSelectorDialog.this.hide();
            }
        });
        panel2.add(this.okButton);
        panel2.add(this.cancelButton);
        add(panel2, "South");
        pack();
    }

    /* access modifiers changed from: private */
    public void changeFont() {
        int i = 0;
        if (this.bold.getState()) {
            i = 1;
        }
        this.font = new Font(this.font.getName(), i, getFontSize());
    }

    public int getFontSize() {
        try {
            return Integer.parseInt(this.fontSizeField.getText().trim());
        } catch (NumberFormatException e) {
            return 12;
        }
    }

    public Font getFont() {
        return this.font;
    }

    public void initializeFieldsFromFont() {
        int i = 12;
        if (this.font != null) {
            i = this.font.getSize();
            if ((this.font.getStyle() & 1) == 0) {
                this.bold.setState(false);
            } else {
                this.bold.setState(true);
            }
        }
        this.fontSizeField.setText(Integer.toString(i));
    }

    public void showCentered() {
        initializeFieldsFromFont();
        Container parent = getParent();
        Point locationOnScreen = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        Dimension size2 = getSize();
        setLocation(locationOnScreen.x + ((size.width - size2.width) / 2), ((size.height - size2.height) / 2) + locationOnScreen.y);
        this.fontSizeField.selectAll();
        show();
    }
}
