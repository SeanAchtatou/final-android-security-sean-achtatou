package frink.gui;

import java.io.File;

public interface InteractiveFields {
    public static final int MESSAGE_APPEND_INPUT = 0;
    public static final int MESSAGE_APPEND_OUTPUT = 1;
    public static final int MESSAGE_DO_LOAD_FILE = 8;
    public static final int MESSAGE_MODE_CHANGE_REQUESTED = 9;
    public static final int MESSAGE_SET_FOCUS = 7;
    public static final int MESSAGE_SET_FROM_TEXT = 3;
    public static final int MESSAGE_SET_HEIGHT = 6;
    public static final int MESSAGE_SET_INTERACTIVE_RUNNING = 2;
    public static final int MESSAGE_SET_TO_TEXT = 4;
    public static final int MESSAGE_SET_WIDTH = 5;

    void appendInput(String str);

    void appendOutput(String str);

    void doLoadFile(File file);

    void modeChangeRequested(int i, int i2) throws ModeNotImplementedException;

    void setFocus();

    void setFromText(String str);

    void setHeight(int i);

    void setInteractiveRunning(boolean z);

    void setToText(String str);

    void setWidth(int i);
}
