package frink.gui;

import frink.errors.FrinkException;

public class ModeNotImplementedException extends FrinkException {
    public ModeNotImplementedException() {
        super("GUI mode not implemented.");
    }
}
