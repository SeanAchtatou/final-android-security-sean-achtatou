package frink.gui;

import frink.io.OutputManager;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.OutputStream;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ThreadedSwingOutputManager extends JPanel implements OutputManager, Runnable {
    private StringBuffer appendBuffer = new StringBuffer();
    private JButton clearButton;
    private JButton closeButton;
    /* access modifiers changed from: private */
    public JFrame dialog;
    private boolean firstOutput = true;
    /* access modifiers changed from: private */
    public JTextArea output;
    private JFrame parent;
    /* access modifiers changed from: private */
    public SwingProgrammingPanel progPanel;
    private JButton stopButton;

    public ThreadedSwingOutputManager(JFrame jFrame, SwingProgrammingPanel swingProgrammingPanel) {
        this.parent = jFrame;
        this.progPanel = swingProgrammingPanel;
        initGUI();
        Thread thread = new Thread(this, "ThreadedSwingOutputManager dispatcher");
        thread.setPriority(7);
        thread.start();
    }

    private void initGUI() {
        this.dialog = null;
        setLayout(new BorderLayout());
        this.output = new JTextArea("", 0, 0);
        this.output.setFont(this.progPanel.getCurrentFont());
        this.output.setEditable(false);
        this.output.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == 9) {
                    ThreadedSwingOutputManager.this.output.transferFocus();
                }
            }
        });
        JPanel jPanel = new JPanel(new FlowLayout());
        this.clearButton = new JButton("Clear");
        this.clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ThreadedSwingOutputManager.this.clear();
            }
        });
        jPanel.add(this.clearButton);
        this.closeButton = new JButton("Close");
        this.closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ThreadedSwingOutputManager.this.close();
            }
        });
        jPanel.add(this.closeButton);
        this.stopButton = new JButton("Stop");
        enableStopButton(false);
        this.stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (ThreadedSwingOutputManager.this.progPanel != null) {
                    ThreadedSwingOutputManager.this.progPanel.stopProgram();
                }
            }
        });
        jPanel.add(this.stopButton);
        add(new JScrollPane(this.output), "Center");
        add(jPanel, "South");
    }

    public void output(String str) {
        appendText(str);
    }

    public void outputln(String str) {
        appendText(str + "\n");
    }

    public OutputStream getRawOutputStream() {
        return null;
    }

    private void appendText(String str) {
        synchronized (this.appendBuffer) {
            this.appendBuffer.append(str);
            this.appendBuffer.notifyAll();
        }
    }

    public void close() {
        this.dialog.setVisible(false);
        clear();
    }

    public void clear() {
        this.output.setText("");
    }

    public void enableStopButton(boolean z) {
        this.stopButton.setEnabled(z);
    }

    private void showDialog() {
        if (this.dialog == null) {
            this.dialog = new JFrame();
            this.dialog.setTitle("Frink Output Window");
            this.dialog.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent windowEvent) {
                    ThreadedSwingOutputManager.this.dialog.setVisible(false);
                    ThreadedSwingOutputManager.this.clear();
                }
            });
            this.dialog.getContentPane().setLayout(new BorderLayout());
            this.dialog.getContentPane().add(this, "Center");
            this.dialog.setSize(this.parent.getSize());
        }
        if (!this.dialog.isVisible()) {
            this.dialog.setVisible(true);
        }
        if (this.firstOutput) {
            this.dialog.setState(0);
            this.dialog.setVisible(true);
            this.dialog.toFront();
            this.firstOutput = false;
        }
    }

    public void run() {
        while (true) {
            String str = null;
            synchronized (this.appendBuffer) {
                if (this.appendBuffer.length() == 0) {
                    try {
                        this.appendBuffer.wait();
                    } catch (InterruptedException e) {
                        str = new String(this.appendBuffer);
                        this.appendBuffer.setLength(0);
                    }
                } else {
                    str = new String(this.appendBuffer);
                    this.appendBuffer.setLength(0);
                }
            }
            if (str != null) {
                this.output.append(str);
                this.output.setCaretPosition(this.output.getText().length());
                showDialog();
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e2) {
                synchronized (this.appendBuffer) {
                    this.output.append(new String(this.appendBuffer));
                    this.output.setCaretPosition(this.output.getText().length());
                    this.appendBuffer.setLength(0);
                    showDialog();
                }
            }
        }
        while (true) {
        }
    }

    public void reset() {
        this.firstOutput = true;
    }

    public void setCurrentFont(Font font) {
        this.output.setFont(font);
        validate();
    }
}
