package frink.gui;

import frink.expr.HelpExpression;
import frink.io.BrowserHelper;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleMenuBar extends MenuBar {
    private boolean addAccelerators;
    private MenuItem alternateData;
    private MenuItem clearMenu;
    private MenuItem convert;
    private Menu dataMenu;
    private MenuItem defaultData;
    private MenuItem documentationMenu;
    private MenuItem donateMenu;
    /* access modifiers changed from: private */
    public ConfirmDialog exitDialog;
    private MenuItem fontMenu;
    private Menu frinkMenu;
    private Menu helpMenu;
    /* access modifiers changed from: private */
    public InteractivePanel iPanel;
    private MenuItem interruptMenuItem;
    private MenuItem loadMenuItem;
    private Menu modeMenu;
    private MenuItem multiLine;
    private MenuItem oneLine;
    /* access modifiers changed from: private */
    public Frame parentFrame;
    private MenuItem program;
    private boolean programRunning = false;
    private MenuItem runMenuItem;
    private MenuItem saveHistoryMenuItem;
    private MenuItem saveMenuItem;
    private MenuItem stopMenuItem;
    private MenuItem useMenuItem;
    private MenuItem versionMenu;
    private Menu viewMenu;
    private MenuItem whatsNewMenu;

    public SimpleMenuBar(Frame frame, InteractivePanel interactivePanel, boolean z) {
        this.parentFrame = frame;
        this.iPanel = interactivePanel;
        this.exitDialog = null;
        this.addAccelerators = z;
        init();
    }

    private void init() {
        this.frinkMenu = new Menu("Frink");
        this.loadMenuItem = new MenuItem("Load");
        if (this.addAccelerators) {
            this.loadMenuItem.setShortcut(new MenuShortcut(76));
        }
        this.loadMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel programmingPanel = SimpleMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.loadProgram();
                }
            }
        });
        this.frinkMenu.add(this.loadMenuItem);
        this.saveMenuItem = new MenuItem("Save");
        if (this.addAccelerators) {
            this.saveMenuItem.setShortcut(new MenuShortcut(83));
        }
        this.saveMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel programmingPanel = SimpleMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.saveProgram();
                }
            }
        });
        this.frinkMenu.add(this.saveMenuItem);
        this.saveHistoryMenuItem = new MenuItem("Save History");
        if (this.addAccelerators) {
            this.saveHistoryMenuItem.setShortcut(new MenuShortcut(83));
        }
        this.saveHistoryMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.saveHistory();
            }
        });
        this.frinkMenu.add(this.saveHistoryMenuItem);
        this.runMenuItem = new MenuItem("Run");
        if (this.addAccelerators) {
            this.runMenuItem.setShortcut(new MenuShortcut(82));
        }
        this.runMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel programmingPanel = SimpleMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.runProgram();
                }
            }
        });
        this.frinkMenu.add(this.runMenuItem);
        this.stopMenuItem = new MenuItem("Stop");
        if (this.addAccelerators) {
            this.stopMenuItem.setShortcut(new MenuShortcut(67));
        }
        this.stopMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel programmingPanel = SimpleMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.stopProgram();
                }
            }
        });
        this.frinkMenu.add(this.stopMenuItem);
        this.useMenuItem = new MenuItem("Use File...");
        if (this.addAccelerators) {
            this.useMenuItem.setShortcut(new MenuShortcut(85));
        }
        this.useMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.useFile();
            }
        });
        this.frinkMenu.add(this.useMenuItem);
        this.interruptMenuItem = new MenuItem("Interrupt calculation");
        if (this.addAccelerators) {
            this.interruptMenuItem.setShortcut(new MenuShortcut(73));
        }
        this.interruptMenuItem.setEnabled(false);
        this.interruptMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.getController().interrupt(true);
            }
        });
        this.frinkMenu.add(this.interruptMenuItem);
        MenuItem menuItem = new MenuItem("Exit");
        if (this.addAccelerators) {
            menuItem.setShortcut(new MenuShortcut(81));
        }
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (SimpleMenuBar.this.exitDialog == null) {
                    ConfirmDialog unused = SimpleMenuBar.this.exitDialog = new ConfirmDialog(SimpleMenuBar.this.parentFrame);
                }
                if (SimpleMenuBar.this.exitDialog.display("Do you really want to exit Frink?", "Frink exit confirmation") == 1) {
                    System.exit(0);
                }
            }
        });
        this.frinkMenu.add(menuItem);
        add(this.frinkMenu);
        this.modeMenu = new Menu("Mode");
        if (this.addAccelerators) {
            this.modeMenu.setShortcut(new MenuShortcut(85));
        }
        this.oneLine = new MenuItem("One-Line");
        if (this.addAccelerators) {
            this.oneLine.setShortcut(new MenuShortcut(49));
        }
        this.oneLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.getController().requestModeChange(1);
            }
        });
        this.modeMenu.add(this.oneLine);
        this.convert = new MenuItem("Convert");
        if (this.addAccelerators) {
            this.convert.setShortcut(new MenuShortcut(50));
        }
        this.convert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.getController().requestModeChange(0);
            }
        });
        this.modeMenu.add(this.convert);
        this.multiLine = new MenuItem("Multi-Line");
        if (this.addAccelerators) {
            this.multiLine.setShortcut(new MenuShortcut(51));
        }
        this.multiLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.getController().requestModeChange(3);
            }
        });
        this.modeMenu.add(this.multiLine);
        this.program = new MenuItem("Programming");
        if (this.addAccelerators) {
            this.program.setShortcut(new MenuShortcut(80));
        }
        this.program.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.getController().requestModeChange(2);
            }
        });
        this.modeMenu.add(this.program);
        add(this.modeMenu);
        this.dataMenu = new Menu("Data");
        this.defaultData = new MenuItem("Use default units file");
        this.defaultData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel programmingPanel = SimpleMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.useDefaultUnits();
                }
            }
        });
        this.dataMenu.add(this.defaultData);
        this.alternateData = new MenuItem("Select alternate units file...");
        this.alternateData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel programmingPanel = SimpleMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.selectUnitsFile();
                }
            }
        });
        this.dataMenu.add(this.alternateData);
        add(this.dataMenu);
        this.viewMenu = new Menu("View");
        this.fontMenu = new MenuItem("Font...");
        if (this.addAccelerators) {
            this.fontMenu.setShortcut(new MenuShortcut(70));
        }
        this.fontMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (SimpleMenuBar.this.iPanel.getMode() == 2) {
                    SimpleMenuBar.this.iPanel.getProgrammingPanel().selectFont();
                } else {
                    SimpleMenuBar.this.iPanel.selectFont();
                }
            }
        });
        this.viewMenu.add(this.fontMenu);
        this.clearMenu = new MenuItem("Clear");
        this.clearMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SimpleMenuBar.this.iPanel.clearOutput();
            }
        });
        this.viewMenu.add(this.clearMenu);
        add(this.viewMenu);
        this.helpMenu = new Menu(HelpExpression.TYPE);
        if (BrowserHelper.canOpenURLs()) {
            if (this.addAccelerators) {
                this.helpMenu.setShortcut(new MenuShortcut(72));
            }
            this.documentationMenu = new MenuItem("Frink Documentation");
            this.documentationMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    BrowserHelper.openURL("http://futureboy.us/frinkdocs/");
                }
            });
            this.helpMenu.add(this.documentationMenu);
            this.whatsNewMenu = new MenuItem("What's New");
            this.whatsNewMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    BrowserHelper.openURL("http://futureboy.us/frinkdocs/whatsnew.html");
                }
            });
            this.helpMenu.add(this.whatsNewMenu);
            this.donateMenu = new MenuItem("Donate");
            this.donateMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    BrowserHelper.openURL("http://futureboy.us/frinkdocs/donate.html");
                }
            });
            this.helpMenu.add(this.donateMenu);
        }
        try {
            this.versionMenu = new MenuItem((String) Class.forName("frink.parser.FrinkVersion").getField("VERSION").get(null));
            this.versionMenu.setEnabled(false);
            this.helpMenu.add(this.versionMenu);
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e) {
        }
        add(this.helpMenu);
        changeMode(0);
    }

    public void setProgramRunning(boolean z) {
        this.programRunning = z;
        this.runMenuItem.setEnabled(!z);
        this.stopMenuItem.setEnabled(z);
    }

    public void setInteractiveRunning(boolean z) {
        this.interruptMenuItem.setEnabled(z);
    }

    public void changeMode(int i) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (i == 2) {
            z = true;
        } else {
            z = false;
        }
        this.runMenuItem.setEnabled(z && !this.programRunning);
        MenuItem menuItem = this.stopMenuItem;
        if (!z || !this.programRunning) {
            z2 = false;
        } else {
            z2 = true;
        }
        menuItem.setEnabled(z2);
        this.loadMenuItem.setEnabled(z);
        this.saveMenuItem.setEnabled(z);
        MenuItem menuItem2 = this.saveHistoryMenuItem;
        if (!z) {
            z3 = true;
        } else {
            z3 = false;
        }
        menuItem2.setEnabled(z3);
        MenuItem menuItem3 = this.useMenuItem;
        if (!z) {
            z4 = true;
        } else {
            z4 = false;
        }
        menuItem3.setEnabled(z4);
        this.defaultData.setEnabled(z);
        this.alternateData.setEnabled(z);
        this.dataMenu.setEnabled(z);
        this.oneLine.setEnabled(true);
        this.multiLine.setEnabled(true);
        this.convert.setEnabled(true);
        this.program.setEnabled(true);
        switch (i) {
            case 0:
                this.convert.setEnabled(false);
                return;
            case 1:
                this.oneLine.setEnabled(false);
                return;
            case 2:
                this.program.setEnabled(false);
                return;
            case 3:
                this.multiLine.setEnabled(false);
                return;
            default:
                return;
        }
    }
}
