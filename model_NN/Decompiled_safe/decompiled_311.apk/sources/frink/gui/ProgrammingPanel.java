package frink.gui;

import frink.errors.FrinkEvaluationException;
import frink.expr.OperatorExpression;
import frink.parser.Frink;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

public class ProgrammingPanel extends Panel implements Runnable {
    private static final String SUFFIX = ".frink";
    private Panel buttonPanel;
    /* access modifiers changed from: private */
    public boolean changed;
    private File currFile;
    private FileDialog fileDialog;
    private FontSelectorDialog fontDialog;
    private Frame frame;
    private AWTInputManager inputMgr;
    private Button loadButton;
    private SimpleMenuBar menuBar;
    private ThreadedAWTOutputManager outputMgr;
    private TextArea programArea;
    private Button runButton;
    private Thread runner;
    private Button saveButton;
    private Button stopButton;
    private File unitsFile;
    private boolean useSystemColors;

    public ProgrammingPanel(Frame frame2) {
        this(frame2, false);
    }

    public ProgrammingPanel(Frame frame2, boolean z) {
        this.useSystemColors = z;
        this.frame = frame2;
        this.menuBar = null;
        this.currFile = null;
        this.unitsFile = null;
        this.fileDialog = null;
        this.changed = false;
        this.runner = null;
        initGUI();
        this.inputMgr = new AWTInputManager(frame2);
        this.outputMgr = new ThreadedAWTOutputManager(frame2, this);
    }

    private void initGUI() {
        setLayout(new BorderLayout());
        this.programArea = new TextArea("// Frink programming mode\n// Enter program in this box.\n\n", 40, 80, 0);
        this.programArea.setFont(new Font("Monospaced", 0, 12));
        this.programArea.selectAll();
        this.programArea.addTextListener(new TextListener() {
            public void textValueChanged(TextEvent textEvent) {
                if (!ProgrammingPanel.this.changed) {
                    boolean unused = ProgrammingPanel.this.changed = true;
                    ProgrammingPanel.this.setTitle();
                }
            }
        });
        setTitle();
        if (!this.useSystemColors) {
            this.programArea.setBackground(new Color(0, 0, 128));
            this.programArea.setForeground(Color.white);
        }
        add(this.programArea, "Center");
        this.buttonPanel = new Panel(new FlowLayout(1, 0, 0));
        this.runButton = new Button("Run");
        this.runButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel.this.runProgram();
            }
        });
        this.buttonPanel.add(this.runButton);
        this.stopButton = new Button("Stop");
        this.stopButton.setEnabled(false);
        this.stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel.this.stopProgram();
            }
        });
        this.buttonPanel.add(this.stopButton);
        this.loadButton = new Button("Load");
        this.loadButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel.this.loadProgram();
            }
        });
        this.buttonPanel.add(this.loadButton);
        this.saveButton = new Button("Save");
        this.saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                ProgrammingPanel.this.saveProgram();
            }
        });
        this.buttonPanel.add(this.saveButton);
        add(this.buttonPanel, "South");
        setFocus();
    }

    public void runProgram() {
        if (this.runner == null) {
            this.runner = new Thread(this);
            this.runner.setPriority(4);
            this.runner.start();
        }
    }

    public void stopProgram() {
        if (this.runner != null) {
            this.runner.stop();
            this.runner = null;
            this.outputMgr.outputln("\n-- Program stopped by user. --");
        }
        setProgramRunning(false);
    }

    public void loadProgram() {
        if (this.fileDialog == null) {
            this.fileDialog = new FileDialog(this.frame);
        }
        this.fileDialog.setMode(0);
        if (this.currFile == null) {
            this.fileDialog.setDirectory(System.getProperty("user.dir"));
        }
        this.fileDialog.show();
        String file = this.fileDialog.getFile();
        setFocus();
        if (file != null) {
            doLoadFile(new File(this.fileDialog.getDirectory(), file));
        }
    }

    public void doLoadFile(File file) {
        String path = file.getPath();
        try {
            FileReader fileReader = new FileReader(path);
            StringBuffer stringBuffer = new StringBuffer();
            char[] cArr = new char[1024];
            while (true) {
                try {
                    int read = fileReader.read(cArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    stringBuffer.append(cArr, 0, read);
                } catch (IOException e) {
                    this.outputMgr.outputln("Error on reading " + file.getPath() + ":\n" + e);
                    if (fileReader != null) {
                        try {
                            fileReader.close();
                            return;
                        } catch (IOException e2) {
                            this.outputMgr.outputln("Error on closing " + file.getPath() + ":\n" + e2);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    if (fileReader != null) {
                        try {
                            fileReader.close();
                        } catch (IOException e3) {
                            this.outputMgr.outputln("Error on closing " + file.getPath() + ":\n" + e3);
                        }
                    }
                    throw th;
                }
            }
            fileReader.close();
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e4) {
                    this.outputMgr.outputln("Error on closing " + file.getPath() + ":\n" + e4);
                }
            }
            this.programArea.setText(new String(stringBuffer));
            this.currFile = file;
            this.changed = false;
            setFocus();
            setCaret();
            setTitle();
        } catch (FileNotFoundException e5) {
            this.outputMgr.outputln("File '" + path + "' not found:\n" + e5);
        }
    }

    public void setMenuBar(SimpleMenuBar simpleMenuBar) {
        this.menuBar = simpleMenuBar;
    }

    public void setTitle() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        if (this.runner != null) {
            str = "Running - ";
        } else {
            str = "";
        }
        if (this.currFile != null) {
            Frame frame2 = this.frame;
            StringBuilder append = new StringBuilder().append(str).append("Frink - ").append(this.currFile.getName());
            if (this.changed) {
                str4 = "*";
            } else {
                str4 = "";
            }
            StringBuilder append2 = append.append(str4);
            if (this.unitsFile != null) {
                str5 = " (using " + this.unitsFile.getName() + ")";
            } else {
                str5 = "";
            }
            frame2.setTitle(append2.append(str5).toString());
            return;
        }
        Frame frame3 = this.frame;
        StringBuilder append3 = new StringBuilder().append(str).append("Frink Programming Mode");
        if (this.changed) {
            str2 = " *";
        } else {
            str2 = "";
        }
        StringBuilder append4 = append3.append(str2);
        if (this.unitsFile != null) {
            str3 = " (using " + this.unitsFile.getName() + ")";
        } else {
            str3 = "";
        }
        frame3.setTitle(append4.append(str3).toString());
    }

    public void saveProgram() {
        if (this.fileDialog == null) {
            this.fileDialog = new FileDialog(this.frame);
        }
        if (this.currFile != null) {
            this.fileDialog.setFile(this.currFile.getName());
            this.fileDialog.setDirectory(this.currFile.getParent());
        } else {
            this.fileDialog.setDirectory(System.getProperty("user.dir"));
            this.fileDialog.setFile("untitled.frink");
        }
        this.fileDialog.setMode(1);
        this.fileDialog.show();
        String file = this.fileDialog.getFile();
        setFocus();
        if (file != null) {
            doSaveFile(new File(this.fileDialog.getDirectory(), file));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x007c A[SYNTHETIC, Splitter:B:15:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ab A[SYNTHETIC, Splitter:B:21:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doSaveFile(java.io.File r8) {
        /*
            r7 = this;
            java.lang.String r6 = "Error on closing "
            java.lang.String r5 = ":\n"
            java.lang.String r0 = r8.getPath()
            r1 = 0
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ IOException -> 0x0053 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0053 }
            java.awt.TextArea r0 = r7.programArea     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            java.lang.String r0 = r0.getText()     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            r2.write(r0)     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            r2.close()     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x002b }
        L_0x001f:
            r7.currFile = r8
            r0 = 0
            r7.changed = r0
            r7.setFocus()
            r7.setTitle()
        L_0x002a:
            return
        L_0x002b:
            r0 = move-exception
            frink.gui.ThreadedAWTOutputManager r1 = r7.outputMgr
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error on closing "
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = r8.getPath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ":\n"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x001f
        L_0x0053:
            r0 = move-exception
        L_0x0054:
            frink.gui.ThreadedAWTOutputManager r2 = r7.outputMgr     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r3.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = "Error on writing "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = r8.getPath()     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = ":\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a8 }
            r2.outputln(r0)     // Catch:{ all -> 0x00a8 }
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x0080 }
            goto L_0x002a
        L_0x0080:
            r0 = move-exception
            frink.gui.ThreadedAWTOutputManager r1 = r7.outputMgr
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error on closing "
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = r8.getPath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ":\n"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x002a
        L_0x00a8:
            r0 = move-exception
        L_0x00a9:
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ IOException -> 0x00af }
        L_0x00ae:
            throw r0
        L_0x00af:
            r1 = move-exception
            frink.gui.ThreadedAWTOutputManager r2 = r7.outputMgr
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error on closing "
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r4 = r8.getPath()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = ":\n"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.outputln(r1)
            goto L_0x00ae
        L_0x00d7:
            r0 = move-exception
            r1 = r2
            goto L_0x00a9
        L_0x00da:
            r0 = move-exception
            r1 = r2
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.gui.ProgrammingPanel.doSaveFile(java.io.File):void");
    }

    public void setFocus() {
        this.programArea.requestFocus();
    }

    public void setCaret() {
        this.programArea.setCaretPosition(this.programArea.getText().length());
    }

    public void selectUnitsFile() {
        if (this.fileDialog == null) {
            this.fileDialog = new FileDialog(this.frame);
        }
        this.fileDialog.setMode(0);
        if (this.unitsFile == null) {
            this.fileDialog.setDirectory(System.getProperty("user.dir"));
        } else {
            this.fileDialog.setDirectory(this.unitsFile.getParent());
            this.fileDialog.setFile(this.unitsFile.getName());
        }
        this.fileDialog.show();
        String file = this.fileDialog.getFile();
        if (file != null) {
            this.unitsFile = new File(this.fileDialog.getDirectory(), file);
            setTitle();
            setFocus();
        }
    }

    public void useDefaultUnits() {
        this.unitsFile = null;
        setTitle();
    }

    public void run() {
        try {
            Frink frink2 = new Frink();
            if (this.unitsFile != null) {
                frink2.setUnitsFile(this.unitsFile.getPath());
            }
            if (this.currFile != null) {
                frink2.getEnvironment().getIncludeManager().appendPath(this.currFile.getParent());
            }
            frink2.getEnvironment().setInputManager(this.inputMgr);
            frink2.getEnvironment().setOutputManager(this.outputMgr);
            frink2.getEnvironment().getGraphicsViewFactory().setDefaultConstructorName("ScalingTranslatingCanvasFrame");
            setProgramRunning(true);
            frink2.parseString(this.programArea.getText());
        } catch (FrinkEvaluationException e) {
            this.outputMgr.outputln(e.toString());
        } catch (Throwable th) {
            this.runner = null;
            setProgramRunning(false);
            throw th;
        }
        this.runner = null;
        setProgramRunning(false);
    }

    private void setProgramRunning(boolean z) {
        this.runButton.setEnabled(!z);
        this.stopButton.setEnabled(z);
        this.outputMgr.enableStopButton(z);
        if (this.menuBar != null) {
            this.menuBar.setProgramRunning(z);
        }
        setTitle();
    }

    public boolean isRunning() {
        return this.runner != null;
    }

    public void selectFont() {
        if (this.fontDialog == null) {
            this.fontDialog = new FontSelectorDialog(this.frame, this.programArea.getFont());
        }
        this.fontDialog.showCentered();
        Font font = this.fontDialog.getFont();
        if (font != null) {
            this.programArea.setFont(font);
            this.outputMgr.setCurrentFont(font);
            validate();
        }
        setFocus();
    }

    public Font getCurrentFont() {
        return this.programArea.getFont();
    }

    public static void main(String[] strArr) {
        Toolkit toolkit;
        Image image;
        Frame frame2 = new Frame();
        frame2.setLayout(new BorderLayout());
        frame2.setSize(500, (int) OperatorExpression.PREC_ADD);
        ProgrammingPanel programmingPanel = new ProgrammingPanel(frame2);
        frame2.add(programmingPanel, "Center");
        frame2.addWindowListener(new WindowAdapter(programmingPanel) {
            final /* synthetic */ ProgrammingPanel val$p;

            {
                this.val$p = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }

            public void windowOpened(WindowEvent windowEvent) {
                this.val$p.setFocus();
                this.val$p.setCaret();
            }
        });
        URL resource = programmingPanel.getClass().getResource("/data/icon.gif");
        if (!(resource == null || (toolkit = frame2.getToolkit()) == null || (image = toolkit.getImage(resource)) == null)) {
            frame2.setIconImage(image);
        }
        frame2.show();
        if (strArr.length == 1) {
            programmingPanel.doLoadFile(new File(strArr[0]));
        }
    }
}
