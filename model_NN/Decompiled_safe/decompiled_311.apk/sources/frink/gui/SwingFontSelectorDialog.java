package frink.gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SwingFontSelectorDialog extends JDialog {
    private JCheckBox bold;
    private JButton cancelButton;
    private Font font;
    private JTextField fontSizeField;
    private JButton okButton;

    public SwingFontSelectorDialog(JFrame jFrame, Font font2) {
        super(jFrame, "Font Selector", true);
        this.font = font2;
        initGUI();
    }

    private void initGUI() {
        JPanel jPanel = new JPanel(new GridLayout(3, 1, 0, 5));
        jPanel.add(new Label("Font Size (points)"));
        AnonymousClass1 r1 = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingFontSelectorDialog.this.changeFont();
                SwingFontSelectorDialog.this.hide();
            }
        };
        this.fontSizeField = new JTextField(3);
        this.fontSizeField.addActionListener(r1);
        jPanel.add(this.fontSizeField);
        this.bold = new JCheckBox("Bold", false);
        jPanel.add(this.bold);
        getContentPane().add(jPanel, "Center");
        JPanel jPanel2 = new JPanel(new FlowLayout(2));
        this.okButton = new JButton("OK");
        this.okButton.addActionListener(r1);
        this.cancelButton = new JButton("Cancel");
        this.cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingFontSelectorDialog.this.hide();
            }
        });
        jPanel2.add(this.okButton);
        jPanel2.add(this.cancelButton);
        getContentPane().add(jPanel2, "South");
        pack();
    }

    /* access modifiers changed from: private */
    public void changeFont() {
        int i = 0;
        if (this.bold.isSelected()) {
            i = 1;
        }
        this.font = new Font(this.font.getName(), i, getFontSize());
    }

    public int getFontSize() {
        try {
            return Integer.parseInt(this.fontSizeField.getText().trim());
        } catch (NumberFormatException e) {
            return 12;
        }
    }

    public Font getFont() {
        return this.font;
    }

    public void initializeFieldsFromFont() {
        int i = 12;
        if (this.font != null) {
            i = this.font.getSize();
            this.bold.setSelected((this.font.getStyle() & 1) != 0);
        }
        this.fontSizeField.setText(Integer.toString(i));
    }

    public void showCentered() {
        initializeFieldsFromFont();
        Container parent = getParent();
        Point locationOnScreen = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        Dimension size2 = getSize();
        setLocation(locationOnScreen.x + ((size.width - size2.width) / 2), ((size.height - size2.height) / 2) + locationOnScreen.y);
        this.fontSizeField.selectAll();
        show();
    }
}
