package frink.gui;

import java.awt.Button;
import java.awt.TextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextShortcutButton extends Button {
    /* access modifiers changed from: private */
    public String after;
    /* access modifiers changed from: private */
    public String afterSelected;
    /* access modifiers changed from: private */
    public String before;
    /* access modifiers changed from: private */
    public String beforeSelected;
    /* access modifiers changed from: private */
    public ActiveFieldProvider fieldProv;
    /* access modifiers changed from: private */
    public int offset;

    public TextShortcutButton(String str, String str2, String str3, ActiveFieldProvider activeFieldProvider) {
        super(str);
        this.before = str2;
        this.after = str3;
        this.beforeSelected = null;
        this.afterSelected = null;
        this.fieldProv = activeFieldProvider;
        this.offset = 0;
        init();
    }

    public TextShortcutButton(String str, String str2, String str3, String str4, String str5, int i, ActiveFieldProvider activeFieldProvider) {
        super(str);
        this.before = str2;
        this.after = str3;
        this.beforeSelected = str4;
        this.afterSelected = str5;
        this.fieldProv = activeFieldProvider;
        this.offset = i;
        init();
    }

    private void init() {
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                TextComponent activeField = TextShortcutButton.this.fieldProv.getActiveField();
                String text = activeField.getText();
                int selectionStart = activeField.getSelectionStart();
                int selectionEnd = activeField.getSelectionEnd();
                if (selectionStart - selectionEnd == 0 || TextShortcutButton.this.beforeSelected == null) {
                    activeField.setText(text.substring(0, selectionStart) + TextShortcutButton.this.before + text.substring(selectionStart, selectionEnd) + TextShortcutButton.this.after + text.substring(selectionEnd));
                    int length = TextShortcutButton.this.before.length() + selectionStart;
                    if (TextShortcutButton.this.offset != 0) {
                        length = TextShortcutButton.this.after.length() + selectionEnd + TextShortcutButton.this.before.length() + TextShortcutButton.this.offset;
                    }
                    activeField.setCaretPosition(length);
                    activeField.setSelectionStart(length);
                    activeField.setSelectionEnd(length);
                } else {
                    activeField.setText(text.substring(0, selectionStart) + TextShortcutButton.this.beforeSelected + text.substring(selectionStart, selectionEnd) + TextShortcutButton.this.afterSelected + text.substring(selectionEnd));
                    int length2 = TextShortcutButton.this.beforeSelected.length() + selectionStart;
                    if (TextShortcutButton.this.offset != 0) {
                        length2 = TextShortcutButton.this.beforeSelected.length() + selectionEnd + TextShortcutButton.this.afterSelected.length() + TextShortcutButton.this.offset;
                    }
                    activeField.setCaretPosition(length2);
                    activeField.setSelectionStart(length2);
                    activeField.setSelectionEnd(length2);
                }
                activeField.requestFocus();
            }
        });
    }
}
