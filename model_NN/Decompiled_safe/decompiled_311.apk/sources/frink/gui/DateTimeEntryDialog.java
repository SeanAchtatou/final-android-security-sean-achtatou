package frink.gui;

import java.awt.Button;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DateTimeEntryDialog extends Dialog {
    private Button cancelButton;
    /* access modifiers changed from: private */
    public DateEntryPanel datePanel;
    private Button okButton;
    /* access modifiers changed from: private */
    public String returnValue = null;
    /* access modifiers changed from: private */
    public TimeEntryPanel timePanel;

    public DateTimeEntryDialog(Frame frame) {
        super(frame, "Date/Time Input", true);
        initGUI();
    }

    private void initGUI() {
        this.datePanel = new DateEntryPanel();
        this.timePanel = new TimeEntryPanel();
        Panel panel = new Panel(new GridLayout(2, 1));
        panel.add(this.datePanel);
        panel.add(this.timePanel);
        add(panel, "Center");
        Panel panel2 = new Panel(new FlowLayout(2));
        this.okButton = new Button("OK");
        this.okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                DateTimeEntryDialog.this.setVisible(false);
                String unused = DateTimeEntryDialog.this.returnValue = " # " + DateTimeEntryDialog.this.datePanel.getValue() + " " + DateTimeEntryDialog.this.timePanel.getValue() + " # ";
            }
        });
        this.cancelButton = new Button("Cancel");
        this.cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                DateTimeEntryDialog.this.setVisible(false);
                String unused = DateTimeEntryDialog.this.returnValue = null;
            }
        });
        panel2.add(this.okButton);
        panel2.add(this.cancelButton);
        add(panel2, "South");
        pack();
    }

    public String getValue() {
        return this.returnValue;
    }

    public void showCentered() {
        Container parent = getParent();
        Point locationOnScreen = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        Dimension size2 = getSize();
        setLocation(locationOnScreen.x + ((size.width - size2.width) / 2), ((size.height - size2.height) / 2) + locationOnScreen.y);
        setVisible(true);
    }
}
