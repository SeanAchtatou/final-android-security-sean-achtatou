package frink.gui;

import java.applet.Applet;
import java.awt.BorderLayout;

public class FrinkApplet extends Applet {
    private SwingInteractivePanel p;

    public void init() {
        this.p = new SwingInteractivePanel(0);
        setLayout(new BorderLayout());
        add(this.p, "Center");
        validate();
    }

    public void start() {
        this.p.setFocus();
    }
}
