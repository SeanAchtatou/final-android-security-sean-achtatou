package frink.gui;

import frink.errors.FrinkEvaluationException;
import frink.parser.Frink;
import frink.parser.HistoryManager;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

public class InteractiveController implements Runnable {
    public static final int MODE_CONVERT = 0;
    public static final int MODE_MULTILINE = 3;
    public static final int MODE_ONELINE = 1;
    public static final int MODE_PROGRAM = 2;
    private Vector<String> calcQueue;
    int currentHist;
    private String fileToRun = null;
    private InteractiveFields gui;
    private HistoryManager history;
    private File historyFile;
    private Frink interp;
    private int mode;
    private boolean prime;
    private Thread runner;
    private boolean running;
    private File useFile;

    public InteractiveController(int i, InteractiveFields interactiveFields, boolean z) {
        this.mode = i;
        this.gui = interactiveFields;
        this.interp = new Frink();
        this.history = new HistoryManager();
        this.running = false;
        this.currentHist = 0;
        this.useFile = null;
        this.historyFile = null;
        this.calcQueue = new Vector<>(1);
        this.prime = z;
        initCalcThread();
    }

    public void initCalcThread() {
        this.runner = new Thread(this, "InteractivePanel runner");
        this.runner.setPriority(3);
        this.runner.start();
    }

    public int getMode() {
        return this.mode;
    }

    public void run() {
        if (this.prime) {
            try {
                this.gui.appendOutput("Version: " + this.interp.parseString("FrinkVersion[]"));
            } catch (FrinkEvaluationException e) {
            }
        }
        while (true) {
            String str = null;
            synchronized (this.calcQueue) {
                if (this.calcQueue.size() == 0) {
                    try {
                        changeRunningStatus(false);
                        this.calcQueue.wait();
                    } catch (InterruptedException e2) {
                        this.interp.getEnvironment().outputln("Calculation interrupted in wait.");
                    }
                } else {
                    str = this.calcQueue.elementAt(0);
                    this.calcQueue.removeElementAt(0);
                }
            }
            if (str != null) {
                try {
                    changeRunningStatus(true);
                    this.gui.appendInput("\n\n" + str + "\n");
                    String parseString = this.interp.parseString(str);
                    if (parseString.length() > 0) {
                        this.gui.appendOutput(parseString);
                    }
                } catch (FrinkEvaluationException e3) {
                    this.interp.getEnvironment().outputln(e3.toString());
                } catch (ThreadDeath e4) {
                    this.interp.getEnvironment().outputln("Calculation interrupted.");
                    return;
                } catch (Throwable th) {
                    this.interp.getEnvironment().outputln("Got serious error: " + th);
                    if (th instanceof ExceptionInInitializerError) {
                        this.interp.getEnvironment().getOutputManager().outputln("Initial exception was: " + ((ExceptionInInitializerError) th).getException());
                    }
                }
            }
        }
    }

    public void addCalc(String str) {
        synchronized (this.calcQueue) {
            this.calcQueue.addElement(str);
            this.calcQueue.notifyAll();
        }
    }

    public void changeRunningStatus(boolean z) {
        this.running = z;
        this.gui.setInteractiveRunning(z);
    }

    public void interrupt(boolean z) {
        if (this.running) {
            try {
                this.runner.stop();
            } catch (Throwable th) {
            }
            changeRunningStatus(false);
            if (z) {
                initCalcThread();
            }
        }
    }

    public Frink getInterpreter() {
        return this.interp;
    }

    public void doUseFile(File file) {
        try {
            performCalculation("use " + new URL("file", "", file.getPath()).toString(), null);
            this.useFile = file;
        } catch (MalformedURLException e) {
            this.gui.appendOutput(e.toString());
        }
    }

    public File getUseFile() {
        return this.useFile;
    }

    public File getHistoryFile() {
        return this.historyFile;
    }

    public void saveHistory(File file) {
        try {
            this.history.save(file);
            this.historyFile = file;
        } catch (IOException e) {
            this.gui.appendOutput("\nError on writing " + file.getPath() + ":\n" + e);
        }
    }

    public void historyBack() {
        this.currentHist--;
        if (this.currentHist < 0) {
            this.currentHist = 0;
        } else if (this.mode == 0) {
            this.gui.setFromText(this.history.getFrom(this.currentHist));
            this.gui.setToText(this.history.getToString(this.currentHist));
        } else if (this.mode == 1 || this.mode == 3) {
            String to = this.history.getTo(this.currentHist);
            if (to == null || to.length() <= 0) {
                this.gui.setFromText(this.history.getFrom(this.currentHist));
            } else {
                this.gui.setFromText(this.history.getFrom(this.currentHist) + " -> " + to);
            }
        }
    }

    public void historyForward() {
        this.currentHist++;
        if (this.currentHist >= this.history.getSize()) {
            this.currentHist = this.history.getSize();
            this.gui.setFromText("");
            if (this.mode == 0) {
                this.gui.setToText("");
            }
        } else if (this.mode == 0) {
            this.gui.setFromText(this.history.getFrom(this.currentHist));
            this.gui.setToText(this.history.getToString(this.currentHist));
        } else if (this.mode == 1 || this.mode == 3) {
            String to = this.history.getTo(this.currentHist);
            if (to == null || to.length() <= 0) {
                this.gui.setFromText(this.history.getFrom(this.currentHist));
            } else {
                this.gui.setFromText(this.history.getFrom(this.currentHist) + " -> " + to);
            }
        }
    }

    public void parseArguments(String[] strArr) {
        this.interp.parseArguments(parseGUIArguments(strArr));
        if (this.fileToRun != null) {
            requestModeChange(2);
            if (this.mode == 2) {
                this.gui.doLoadFile(new File(this.fileToRun));
            }
        }
    }

    public void requestModeChange(int i) {
        if (this.mode != i) {
            try {
                this.gui.modeChangeRequested(this.mode, i);
                this.mode = i;
            } catch (ModeNotImplementedException e) {
                System.err.println("GUI mode not implemented.");
            }
        }
    }

    public void performCalculation(String str, String str2) {
        String str3;
        String str4;
        if (this.mode != 0) {
            str3 = null;
        } else {
            str3 = str2;
        }
        if (str.length() != 0) {
            this.history.append(str, str3);
            if (this.mode != 0 || str3 == null || str3.length() == 0) {
                str4 = str;
            } else {
                str4 = str + " -> " + str3;
            }
            addCalc(str4);
            this.currentHist = this.history.getSize();
            this.gui.setFromText("");
            if (this.mode == 0) {
                this.gui.setToText("");
            }
        }
        this.gui.setFocus();
    }

    private String[] parseGUIArguments(String[] strArr) {
        int length = strArr.length;
        Vector vector = new Vector(strArr.length);
        int i = 0;
        while (i < length) {
            if (strArr[i].equals("-width")) {
                i++;
                this.gui.setWidth(Integer.parseInt(strArr[i]));
            } else if (strArr[i].equals("-height")) {
                i++;
                this.gui.setHeight(Integer.parseInt(strArr[i]));
            } else if (strArr[i].equals("-open")) {
                i++;
                this.fileToRun = strArr[i];
            } else {
                vector.addElement(strArr[i]);
            }
            i++;
        }
        String[] strArr2 = new String[vector.size()];
        for (int i2 = 0; i2 < vector.size(); i2++) {
            strArr2[i2] = (String) vector.elementAt(i2);
        }
        return strArr2;
    }

    public void setFileToRun(String str) {
        this.fileToRun = str;
    }

    public void suspend() {
        try {
            if (this.running && this.runner != null) {
                this.runner.suspend();
            }
        } catch (Throwable th) {
        }
    }

    public void resume() {
        try {
            if (this.running && this.runner != null) {
                this.runner.resume();
            }
        } catch (Throwable th) {
        }
    }
}
