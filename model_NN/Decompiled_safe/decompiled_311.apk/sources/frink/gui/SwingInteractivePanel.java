package frink.gui;

import frink.expr.OperatorExpression;
import frink.io.OutputManager;
import frink.parser.Frink;
import frink.symbolic.BasicTransformationRuleManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.OutputStream;
import java.net.URL;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

public class SwingInteractivePanel extends JPanel implements OutputManager, InteractiveFields {
    /* access modifiers changed from: private */
    public JTextComponent activeField;
    private JTextComponent activeFromField;
    /* access modifiers changed from: private */
    public InteractiveController controller;
    private JPanel convertPanel;
    private SwingFontSelectorDialog fontDialog;
    private JFrame frame;
    /* access modifiers changed from: private */
    public JTextArea fromArea;
    private boolean fromAreaInitialized = false;
    /* access modifiers changed from: private */
    public JTextField fromField;
    private JButton goButton;
    private int height = OperatorExpression.PREC_ADD;
    private JFileChooser historyDialog = null;
    private JPanel inputAreaPanel;
    private JPanel legendPanel;
    private JPanel mainPanel;
    private SwingMenuBar menuBar = null;
    private JPanel noConvertPanel;
    /* access modifiers changed from: private */
    public JTextPane output;
    private SwingProgrammingPanel programPanel;
    private JPanel subInputPanel;
    /* access modifiers changed from: private */
    public JTextField toField;
    private JFileChooser useDialog = null;
    private int width = 500;

    public SwingInteractivePanel(int i) {
        this.controller = new InteractiveController(i, this, false);
        this.controller.getInterpreter().getEnvironment().setOutputManager(this);
        this.controller.getInterpreter().getEnvironment().getGraphicsViewFactory().setDefaultConstructorName("ScalingTranslatingJComponentFrame");
        initGUI();
    }

    private void initGUI() {
        setLayout(new BorderLayout());
        this.mainPanel = new JPanel(new BorderLayout());
        setLayout(new BorderLayout());
        JLabel jLabel = new JLabel("From:");
        JLabel jLabel2 = new JLabel("To:");
        this.output = new JTextPane();
        JScrollPane jScrollPane = new JScrollPane(this.output);
        initStyles();
        append("Frink\n", "banner");
        append("Copyright 2000-2011 Alan Eliasen, eliasen@mindspring.com\nhttp://futureboy.us/frinkdocs/\nEnter calculations in the text fields at bottom.\nUse up/down arrows to repeat/modify previous calculations.\n", "regular");
        this.output.setEditable(false);
        this.output.setBackground(Color.black);
        this.output.setForeground(Color.white);
        this.mainPanel.add(jScrollPane, "Center");
        this.goButton = new JButton("Go");
        this.goButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingInteractivePanel.this.controller.performCalculation(SwingInteractivePanel.this.getFromText(), SwingInteractivePanel.this.getToText());
            }
        });
        this.output.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent keyEvent) {
                if (!keyEvent.isActionKey() && (keyEvent.getModifiers() & -66) == 0) {
                    SwingInteractivePanel.this.setFromText(SwingInteractivePanel.this.getFromText() + keyEvent.getKeyChar());
                    SwingInteractivePanel.this.setFocus();
                }
            }
        });
        AnonymousClass3 r2 = new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                int keyCode = keyEvent.getKeyCode();
                if (keyCode == 38 || keyCode == 224) {
                    SwingInteractivePanel.this.controller.historyBack();
                    keyEvent.consume();
                }
                if (keyCode == 40 || keyCode == 225) {
                    SwingInteractivePanel.this.controller.historyForward();
                    keyEvent.consume();
                }
            }
        };
        AnonymousClass4 r3 = new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                int keyCode = keyEvent.getKeyCode();
                if ((keyCode == 38 || keyCode == 224) && (keyEvent.getModifiers() & 2) != 0) {
                    SwingInteractivePanel.this.controller.historyBack();
                    keyEvent.consume();
                }
                if ((keyCode == 40 || keyCode == 225) && (keyEvent.getModifiers() & 2) != 0) {
                    SwingInteractivePanel.this.controller.historyForward();
                    keyEvent.consume();
                }
                if (keyCode == 10 && (keyEvent.getModifiers() & 2) != 0) {
                    SwingInteractivePanel.this.controller.performCalculation(SwingInteractivePanel.this.getFromText(), SwingInteractivePanel.this.getToText());
                    keyEvent.consume();
                }
            }
        };
        this.fromField = new JTextField();
        this.fromArea = new JTextArea(4, 80);
        this.toField = new JTextField();
        this.activeFromField = this.fromField;
        this.activeField = this.fromField;
        AnonymousClass5 r4 = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingInteractivePanel.this.controller.performCalculation(SwingInteractivePanel.this.getFromText(), SwingInteractivePanel.this.getToText());
            }
        };
        this.fromField.addActionListener(r4);
        this.toField.addActionListener(r4);
        this.fromField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent focusEvent) {
                JTextComponent unused = SwingInteractivePanel.this.activeField = SwingInteractivePanel.this.fromField;
            }
        });
        this.fromArea.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent focusEvent) {
                JTextComponent unused = SwingInteractivePanel.this.activeField = SwingInteractivePanel.this.fromArea;
            }
        });
        this.toField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent focusEvent) {
                JTextComponent unused = SwingInteractivePanel.this.activeField = SwingInteractivePanel.this.toField;
            }
        });
        this.fromField.addKeyListener(r2);
        this.fromArea.addKeyListener(r3);
        this.toField.addKeyListener(r2);
        this.inputAreaPanel = new JPanel(new BorderLayout());
        this.mainPanel.add(this.inputAreaPanel, "South");
        this.subInputPanel = new JPanel(new GridLayout(2, 1));
        this.convertPanel = new JPanel(new BorderLayout());
        this.noConvertPanel = new JPanel(new BorderLayout());
        this.legendPanel = new JPanel(new GridLayout(2, 1));
        this.legendPanel.add(jLabel);
        this.legendPanel.add(jLabel2);
        add(this.mainPanel, "Center");
        setupInputPanel(this.controller.getMode());
    }

    public void useFile() {
        if (this.useDialog == null) {
            this.useDialog = new JFileChooser(System.getProperty("user.dir"));
            this.useDialog.setDialogTitle("Use File");
        }
        int showOpenDialog = this.useDialog.showOpenDialog(this.frame);
        setFocus();
        if (showOpenDialog == 0) {
            this.controller.doUseFile(this.useDialog.getSelectedFile());
        }
    }

    public void saveHistory() {
        if (this.historyDialog == null) {
            this.historyDialog = new JFileChooser(System.getProperty("user.dir"));
            this.historyDialog.setDialogTitle("Save History");
        }
        int showSaveDialog = this.historyDialog.showSaveDialog(this.frame);
        setFocus();
        if (showSaveDialog == 0) {
            this.controller.saveHistory(this.historyDialog.getSelectedFile());
        }
    }

    private void setupInputPanel(int i) {
        this.inputAreaPanel.removeAll();
        this.noConvertPanel.removeAll();
        this.convertPanel.removeAll();
        this.subInputPanel.removeAll();
        if (i == 0) {
            this.subInputPanel.add(this.fromField);
            this.subInputPanel.add(this.toField);
            this.convertPanel.add(this.legendPanel, "West");
            this.convertPanel.add(this.subInputPanel, "Center");
            this.convertPanel.add(this.goButton, "East");
            this.inputAreaPanel.add(this.convertPanel, "Center");
        } else if (i == 1 || i == 3) {
            if (i == 1) {
                this.noConvertPanel.add(this.fromField, "Center");
            } else {
                this.noConvertPanel.add(this.fromArea, "Center");
            }
            this.noConvertPanel.add(this.goButton, "East");
            this.inputAreaPanel.add(this.noConvertPanel, "Center");
        }
        validate();
    }

    public JTextComponent getActiveFromField() {
        return this.activeFromField;
    }

    private void adjustCaret() {
        getActiveFromField().setCaretPosition(getActiveFromField().getText().length());
        if (this.controller.getMode() == 0) {
            this.toField.setCaretPosition(this.toField.getText().length());
        }
    }

    public void setFocus() {
        getActiveFromField().requestFocus();
    }

    public Frink getInterpreter() {
        return this.controller.getInterpreter();
    }

    public void output(String str) {
        append(str, "print");
    }

    public void outputln(String str) {
        append(str + "\n", "print");
    }

    public OutputStream getRawOutputStream() {
        return null;
    }

    private void initStyles() {
        Style style = StyleContext.getDefaultStyleContext().getStyle(BasicTransformationRuleManager.DEFAULT_RULESET_NAME);
        AnonymousClass9 r1 = new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
                SwingInteractivePanel.reapplyStyles(SwingInteractivePanel.this.output, (Style) changeEvent.getSource());
            }
        };
        Style addStyle = this.output.addStyle("regular", style);
        StyleConstants.setFontFamily(addStyle, "SansSerif");
        StyleConstants.setFontSize(addStyle, 14);
        addStyle.addChangeListener(r1);
        this.output.addStyle("input", addStyle).addChangeListener(r1);
        Style addStyle2 = this.output.addStyle("output", addStyle);
        StyleConstants.setBold(addStyle2, true);
        addStyle2.addChangeListener(r1);
        Style addStyle3 = this.output.addStyle("print", addStyle);
        StyleConstants.setForeground(addStyle3, Color.yellow);
        addStyle3.addChangeListener(r1);
        Style addStyle4 = this.output.addStyle("banner", addStyle);
        StyleConstants.setBold(addStyle4, true);
        StyleConstants.setFontSize(addStyle4, 20);
        addStyle4.addChangeListener(r1);
    }

    private void append(String str, String str2) {
        Document document = this.output.getDocument();
        try {
            document.insertString(document.getLength(), str, this.output.getStyle(str2));
            this.output.setCaretPosition(document.getLength());
        } catch (BadLocationException e) {
            System.out.println("Couldn't insert text.");
        }
    }

    public void setInteractiveRunning(boolean z) {
        if (this.menuBar != null) {
            this.menuBar.setInteractiveRunning(z);
        }
    }

    public void setFromText(String str) {
        getActiveFromField().setText(str);
        adjustCaret();
    }

    public String getFromText() {
        return getActiveFromField().getText();
    }

    public String getToText() {
        if (this.toField != null) {
            return this.toField.getText();
        }
        return null;
    }

    public void setToText(String str) {
        this.toField.setText(str);
        adjustCaret();
    }

    public void appendInput(String str) {
        append(str, "input");
        this.output.setCaretPosition(this.output.getDocument().getLength());
    }

    public void appendOutput(String str) {
        append(str, "output");
        this.output.setCaretPosition(this.output.getDocument().getLength());
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void doLoadFile(File file) {
        initProgrammingPanel();
        this.programPanel.doLoadFile(file);
    }

    public void selectFont() {
        if (this.fontDialog == null) {
            this.fontDialog = new SwingFontSelectorDialog(this.frame, this.output.getFont());
        }
        this.fontDialog.showCentered();
        Font font = this.fontDialog.getFont();
        if (font != null) {
            Style style = this.output.getStyle("regular");
            StyleConstants.setFontSize(style, font.getSize());
            StyleConstants.setBold(style, font.isBold());
            StyleConstants.setFontSize(this.output.getStyle("banner"), (int) (((double) font.getSize()) * 1.8d));
            this.fromField.setFont(font);
            setFromAreaFont();
            this.toField.setFont(font);
            validate();
        }
        setFocus();
    }

    public static void reapplyStyles(JTextPane jTextPane, Style style) {
        Element defaultRootElement = jTextPane.getDocument().getDefaultRootElement();
        int elementCount = defaultRootElement.getElementCount();
        for (int i = 0; i < elementCount; i++) {
            Element element = defaultRootElement.getElement(i);
            if (style.getName().equals((String) element.getAttributes().getAttribute(StyleConstants.NameAttribute))) {
                int startOffset = element.getStartOffset();
                jTextPane.getStyledDocument().setParagraphAttributes(startOffset, element.getEndOffset() - startOffset, style, true);
            }
            for (int i2 = 0; i2 < element.getElementCount(); i2++) {
                Element element2 = element.getElement(i2);
                if (style.getName().equals((String) element2.getAttributes().getAttribute(StyleConstants.NameAttribute))) {
                    int startOffset2 = element2.getStartOffset();
                    jTextPane.getStyledDocument().setCharacterAttributes(startOffset2, element2.getEndOffset() - startOffset2, style, true);
                }
            }
        }
    }

    private void setFromAreaFont() {
        Font font = this.fromField.getFont();
        if (font != null) {
            this.fromArea.setFont(new Font("Monospaced", font.getStyle(), font.getSize()));
        }
    }

    public void modeChangeRequested(int i, int i2) throws ModeNotImplementedException {
        if (i2 == 3) {
            if (!this.fromAreaInitialized) {
                setFromAreaFont();
                this.fromAreaInitialized = true;
            }
            this.activeFromField = this.fromArea;
        } else {
            this.activeFromField = this.fromField;
        }
        if (i2 == 2) {
            removeAll();
            initProgrammingPanel();
            add(this.programPanel, "Center");
            this.programPanel.setTitle();
            if (!(this.frame == null || this.menuBar == null)) {
                this.frame.setJMenuBar(this.menuBar);
            }
            this.programPanel.setFocus();
            validate();
            repaint();
        } else {
            if (!(i == 0 || i == 1 || i == 3)) {
                removeAll();
                add(this.mainPanel, "Center");
            }
            setupInputPanel(i2);
            if (i2 == 0) {
                this.toField.setText("");
            }
            if (this.frame != null) {
                this.frame.setTitle("Frink");
            }
            if (!(this.frame == null || this.menuBar == null)) {
                this.frame.setJMenuBar(this.menuBar);
            }
            validate();
            repaint();
            this.output.setCaretPosition(this.output.getDocument().getLength());
            setFocus();
        }
        if (this.menuBar != null) {
            this.menuBar.changeMode(i2);
        }
    }

    public InteractiveController getController() {
        return this.controller;
    }

    public void setMenuBar(SwingMenuBar swingMenuBar) {
        this.menuBar = swingMenuBar;
        if (this.programPanel != null) {
            this.programPanel.setMenuBar(this.menuBar);
        }
    }

    public void setFrame(JFrame jFrame) {
        this.frame = jFrame;
        if (this.controller.getMode() == 2 && this.programPanel != null) {
            this.programPanel.setTitle();
        }
    }

    public static JFrame initializeFrame(SwingInteractivePanel swingInteractivePanel, boolean z) {
        Toolkit toolkit;
        Image image;
        JFrame jFrame = new JFrame("Frink");
        jFrame.getContentPane().setLayout(new BorderLayout());
        jFrame.getContentPane().add(swingInteractivePanel, "Center");
        if (z) {
            SwingMenuBar swingMenuBar = new SwingMenuBar(jFrame, swingInteractivePanel);
            swingInteractivePanel.setMenuBar(swingMenuBar);
            jFrame.setJMenuBar(swingMenuBar);
        }
        swingInteractivePanel.setFrame(jFrame);
        swingInteractivePanel.getInterpreter().getEnvironment().setInputManager(new SwingInputManager(jFrame));
        jFrame.setSize(swingInteractivePanel.width, swingInteractivePanel.height);
        jFrame.addWindowListener(new WindowAdapter(swingInteractivePanel) {
            final /* synthetic */ SwingInteractivePanel val$p;

            {
                this.val$p = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }

            public void windowOpened(WindowEvent windowEvent) {
                this.val$p.setFocus();
            }
        });
        URL resource = swingInteractivePanel.getClass().getResource("/data/icon.gif");
        if (!(resource == null || (toolkit = jFrame.getToolkit()) == null || (image = toolkit.getImage(resource)) == null)) {
            jFrame.setIconImage(image);
        }
        jFrame.setVisible(true);
        return jFrame;
    }

    private void initProgrammingPanel() {
        if (this.programPanel == null) {
            this.programPanel = new SwingProgrammingPanel(this.frame);
        }
    }

    public SwingProgrammingPanel getProgrammingPanel() {
        initProgrammingPanel();
        return this.programPanel;
    }

    public static void main(String[] strArr) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }
        SwingInteractivePanel swingInteractivePanel = new SwingInteractivePanel(0);
        initializeFrame(swingInteractivePanel, true);
        swingInteractivePanel.getController().parseArguments(strArr);
    }
}
