package frink.gui;

import frink.graphics.GraphicsViewFactory;
import java.awt.Dimension;
import java.awt.Toolkit;

public class FullScreenAWTStarter {
    public void init(String[] strArr, boolean z, boolean z2, boolean z3) {
        InteractivePanel interactivePanel = new InteractivePanel(0, z);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        interactivePanel.setWidth(screenSize.width);
        interactivePanel.setHeight(screenSize.height);
        GraphicsViewFactory graphicsViewFactory = interactivePanel.getInterpreter().getEnvironment().getGraphicsViewFactory();
        graphicsViewFactory.setNewWindowSize(screenSize.width, screenSize.height);
        graphicsViewFactory.setShowControlsOnWindows(true);
        InteractivePanel.initializeFrame(interactivePanel, z2, z3);
        interactivePanel.getController().parseArguments(strArr);
    }

    public static void main(String[] strArr) {
        new FullScreenAWTStarter().init(strArr, true, true, true);
    }
}
