package frink.gui;

import frink.errors.FrinkEvaluationException;
import frink.expr.OperatorExpression;
import frink.parser.Frink;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SwingProgrammingPanel extends JPanel implements Runnable {
    private static final String SUFFIX = ".frink";
    private JPanel buttonPanel;
    private boolean changed = false;
    private File currFile = null;
    private JFileChooser fileDialog = null;
    private SwingFontSelectorDialog fontDialog;
    private JFrame frame;
    private SwingInputManager inputMgr;
    private JButton loadButton;
    private SwingMenuBar menuBar = null;
    private ThreadedSwingOutputManager outputMgr;
    private JTextArea programArea;
    private JButton runButton;
    private Thread runner = null;
    private JButton saveButton;
    private JButton stopButton;
    private File unitsFile = null;

    public SwingProgrammingPanel(JFrame jFrame) {
        this.frame = jFrame;
        initGUI();
        this.inputMgr = new SwingInputManager(jFrame);
        this.outputMgr = new ThreadedSwingOutputManager(jFrame, this);
    }

    private void initGUI() {
        setLayout(new BorderLayout());
        this.programArea = new JTextArea("", 0, 0);
        JScrollPane jScrollPane = new JScrollPane(this.programArea);
        this.programArea.setText("// Frink programming mode\n// Enter program in this box.\n\n");
        this.programArea.setFont(new Font("Monospaced", 0, 12));
        this.programArea.selectAll();
        setTitle();
        this.programArea.setCaretColor(Color.yellow);
        this.programArea.setBackground(new Color(0, 0, 90));
        this.programArea.setForeground(Color.white);
        add(jScrollPane, "Center");
        this.buttonPanel = new JPanel(new FlowLayout(1, 0, 0));
        this.runButton = new JButton("Run");
        this.runButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel.this.runProgram();
            }
        });
        this.buttonPanel.add(this.runButton);
        this.stopButton = new JButton("Stop");
        this.stopButton.setEnabled(false);
        this.stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel.this.stopProgram();
            }
        });
        this.buttonPanel.add(this.stopButton);
        this.loadButton = new JButton("Load");
        this.loadButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel.this.loadProgram();
            }
        });
        this.buttonPanel.add(this.loadButton);
        this.saveButton = new JButton("Save");
        this.saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel.this.saveProgram();
            }
        });
        this.buttonPanel.add(this.saveButton);
        add(this.buttonPanel, "South");
        setFocus();
    }

    public void runProgram() {
        if (this.runner == null) {
            this.runner = new Thread(this);
            this.runner.setPriority(4);
            this.runner.start();
        }
    }

    public void stopProgram() {
        if (this.runner != null) {
            this.runner.stop();
            this.runner = null;
            this.outputMgr.outputln("\n-- Program stopped by user. --");
        }
        setProgramRunning(false);
    }

    public void loadProgram() {
        if (this.fileDialog == null) {
            this.fileDialog = new JFileChooser(System.getProperty("user.dir"));
        }
        this.fileDialog.setSelectedFile((File) null);
        this.fileDialog.setDialogTitle("Load program");
        int showOpenDialog = this.fileDialog.showOpenDialog(this.programArea);
        setFocus();
        if (showOpenDialog == 0) {
            doLoadFile(this.fileDialog.getSelectedFile());
        }
    }

    public void doLoadFile(File file) {
        String path = file.getPath();
        try {
            FileReader fileReader = new FileReader(path);
            StringBuffer stringBuffer = new StringBuffer();
            char[] cArr = new char[1024];
            while (true) {
                try {
                    int read = fileReader.read(cArr, 0, 1024);
                    if (read != -1) {
                        stringBuffer.append(cArr, 0, read);
                    } else {
                        fileReader.close();
                        try {
                            fileReader.close();
                            this.programArea.setText(new String(stringBuffer));
                            this.currFile = file;
                            this.changed = false;
                            setFocus();
                            setCaret();
                            setTitle();
                            return;
                        } catch (IOException e) {
                            this.outputMgr.outputln("Error on closing " + file.getPath() + ":\n" + e);
                            return;
                        }
                    }
                } catch (IOException e2) {
                    this.outputMgr.outputln("Error on reading " + file.getPath() + ":\n" + e2);
                    try {
                        fileReader.close();
                        return;
                    } catch (IOException e3) {
                        this.outputMgr.outputln("Error on closing " + file.getPath() + ":\n" + e3);
                        return;
                    }
                } catch (Throwable th) {
                    try {
                        fileReader.close();
                        throw th;
                    } catch (IOException e4) {
                        this.outputMgr.outputln("Error on closing " + file.getPath() + ":\n" + e4);
                        return;
                    }
                }
            }
        } catch (FileNotFoundException e5) {
            this.outputMgr.outputln("File '" + path + "' not found:\n" + e5);
        }
    }

    public void setMenuBar(SwingMenuBar swingMenuBar) {
        this.menuBar = swingMenuBar;
    }

    public void setTitle() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        if (this.runner != null) {
            str = "Running - ";
        } else {
            str = "";
        }
        if (this.currFile == null) {
            return;
        }
        if (this.frame != null) {
            JFrame jFrame = this.frame;
            StringBuilder append = new StringBuilder().append(str).append("Frink - ").append(this.currFile.getName());
            if (this.changed) {
                str4 = "*";
            } else {
                str4 = "";
            }
            StringBuilder append2 = append.append(str4);
            if (this.unitsFile != null) {
                str5 = " (using " + this.unitsFile.getName() + ")";
            } else {
                str5 = "";
            }
            jFrame.setTitle(append2.append(str5).toString());
        } else if (this.frame != null) {
            JFrame jFrame2 = this.frame;
            StringBuilder append3 = new StringBuilder().append(str).append("Frink Programming Mode");
            if (this.changed) {
                str2 = " *";
            } else {
                str2 = "";
            }
            StringBuilder append4 = append3.append(str2);
            if (this.unitsFile != null) {
                str3 = " (using " + this.unitsFile.getName() + ")";
            } else {
                str3 = "";
            }
            jFrame2.setTitle(append4.append(str3).toString());
        }
    }

    public void saveProgram() {
        if (this.fileDialog == null) {
            if (this.currFile == null) {
                this.fileDialog = new JFileChooser(System.getProperty("user.dir"));
                this.fileDialog.setSelectedFile(new File("untitled.frink"));
            } else {
                this.fileDialog = new JFileChooser(this.currFile);
            }
        }
        if (this.currFile != null) {
            this.fileDialog.setCurrentDirectory(this.currFile.getParentFile());
            this.fileDialog.setSelectedFile(this.currFile);
        }
        this.fileDialog.setDialogTitle("Save program");
        int showSaveDialog = this.fileDialog.showSaveDialog(this.programArea);
        setFocus();
        if (showSaveDialog == 0) {
            doSaveFile(this.fileDialog.getSelectedFile());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x007c A[SYNTHETIC, Splitter:B:15:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ab A[SYNTHETIC, Splitter:B:21:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doSaveFile(java.io.File r8) {
        /*
            r7 = this;
            java.lang.String r6 = "Error on closing "
            java.lang.String r5 = ":\n"
            java.lang.String r0 = r8.getPath()
            r1 = 0
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ IOException -> 0x0053 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0053 }
            javax.swing.JTextArea r0 = r7.programArea     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            java.lang.String r0 = r0.getText()     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            r2.write(r0)     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            r2.close()     // Catch:{ IOException -> 0x00da, all -> 0x00d7 }
            if (r2 == 0) goto L_0x001f
            r2.close()     // Catch:{ IOException -> 0x002b }
        L_0x001f:
            r7.currFile = r8
            r0 = 0
            r7.changed = r0
            r7.setFocus()
            r7.setTitle()
        L_0x002a:
            return
        L_0x002b:
            r0 = move-exception
            frink.gui.ThreadedSwingOutputManager r1 = r7.outputMgr
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error on closing "
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = r8.getPath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ":\n"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x001f
        L_0x0053:
            r0 = move-exception
        L_0x0054:
            frink.gui.ThreadedSwingOutputManager r2 = r7.outputMgr     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r3.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = "Error on writing "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = r8.getPath()     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a8 }
            java.lang.String r4 = ":\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a8 }
            r2.outputln(r0)     // Catch:{ all -> 0x00a8 }
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x0080 }
            goto L_0x002a
        L_0x0080:
            r0 = move-exception
            frink.gui.ThreadedSwingOutputManager r1 = r7.outputMgr
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Error on closing "
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r3 = r8.getPath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ":\n"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.outputln(r0)
            goto L_0x002a
        L_0x00a8:
            r0 = move-exception
        L_0x00a9:
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ IOException -> 0x00af }
        L_0x00ae:
            throw r0
        L_0x00af:
            r1 = move-exception
            frink.gui.ThreadedSwingOutputManager r2 = r7.outputMgr
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error on closing "
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r4 = r8.getPath()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = ":\n"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.outputln(r1)
            goto L_0x00ae
        L_0x00d7:
            r0 = move-exception
            r1 = r2
            goto L_0x00a9
        L_0x00da:
            r0 = move-exception
            r1 = r2
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.gui.SwingProgrammingPanel.doSaveFile(java.io.File):void");
    }

    public void setFocus() {
        this.programArea.requestFocus();
    }

    public void setCaret() {
        this.programArea.setCaretPosition(this.programArea.getText().length());
    }

    public void selectUnitsFile() {
        if (this.fileDialog == null) {
            this.fileDialog = new JFileChooser(System.getProperty("user.dir"));
        }
        this.fileDialog.setDialogTitle("Select units file");
        if (this.unitsFile != null) {
            this.fileDialog.setCurrentDirectory(this.unitsFile.getParentFile());
            this.fileDialog.setSelectedFile(this.unitsFile);
        }
        int showOpenDialog = this.fileDialog.showOpenDialog(this.programArea);
        setFocus();
        if (showOpenDialog == 0) {
            this.unitsFile = this.fileDialog.getSelectedFile();
        }
        setTitle();
    }

    public void useDefaultUnits() {
        this.unitsFile = null;
        setTitle();
    }

    public void run() {
        try {
            Frink frink2 = new Frink();
            if (this.unitsFile != null) {
                frink2.setUnitsFile(this.unitsFile.getPath());
            }
            if (this.currFile != null) {
                frink2.getEnvironment().getIncludeManager().appendPath(this.currFile.getParent());
            }
            frink2.getEnvironment().setInputManager(this.inputMgr);
            this.outputMgr.reset();
            frink2.getEnvironment().setOutputManager(this.outputMgr);
            frink2.getEnvironment().getGraphicsViewFactory().setDefaultConstructorName("ScalingTranslatingJComponentFrame");
            setProgramRunning(true);
            frink2.parseString(this.programArea.getText());
        } catch (FrinkEvaluationException e) {
            this.outputMgr.outputln(e.toString());
        } catch (Throwable th) {
            this.runner = null;
            setProgramRunning(false);
            throw th;
        }
        this.runner = null;
        setProgramRunning(false);
    }

    private void setProgramRunning(boolean z) {
        this.runButton.setEnabled(!z);
        this.stopButton.setEnabled(z);
        this.outputMgr.enableStopButton(z);
        if (this.menuBar != null) {
            this.menuBar.setProgramRunning(z);
        }
        setTitle();
    }

    public boolean isRunning() {
        return this.runner != null;
    }

    public void selectFont() {
        if (this.fontDialog == null) {
            this.fontDialog = new SwingFontSelectorDialog(this.frame, getCurrentFont());
        }
        this.fontDialog.showCentered();
        setFocus();
        Font font = this.fontDialog.getFont();
        if (font != null) {
            this.programArea.setFont(font);
            this.outputMgr.setCurrentFont(font);
            validate();
        }
    }

    public Font getCurrentFont() {
        return this.programArea.getFont();
    }

    public static void main(String[] strArr) {
        Toolkit toolkit;
        Image image;
        JFrame jFrame = new JFrame();
        jFrame.getContentPane().setLayout(new BorderLayout());
        jFrame.setSize(500, (int) OperatorExpression.PREC_ADD);
        SwingProgrammingPanel swingProgrammingPanel = new SwingProgrammingPanel(jFrame);
        jFrame.getContentPane().add(swingProgrammingPanel, "Center");
        jFrame.addWindowListener(new WindowAdapter(swingProgrammingPanel) {
            final /* synthetic */ SwingProgrammingPanel val$p;

            {
                this.val$p = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }

            public void windowOpened(WindowEvent windowEvent) {
                this.val$p.setFocus();
                this.val$p.setCaret();
            }
        });
        URL resource = swingProgrammingPanel.getClass().getResource("/data/icon.gif");
        if (!(resource == null || (toolkit = jFrame.getToolkit()) == null || (image = toolkit.getImage(resource)) == null)) {
            jFrame.setIconImage(image);
        }
        jFrame.show();
        if (strArr.length == 1) {
            swingProgrammingPanel.doLoadFile(new File(strArr[0]));
        }
    }
}
