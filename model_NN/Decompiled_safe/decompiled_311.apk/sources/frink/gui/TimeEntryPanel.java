package frink.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;

public class TimeEntryPanel extends Panel {
    private TextField hourField;
    private TextField minuteField;
    private TextField secondField;

    public TimeEntryPanel() {
        initGUI();
    }

    private void initGUI() {
        setLayout(new FlowLayout());
        Calendar instance = Calendar.getInstance();
        this.hourField = new TextField(2);
        this.hourField.setText(pad(Integer.toString(instance.get(11)), 2));
        this.minuteField = new TextField(2);
        this.minuteField.setText(pad(Integer.toString(instance.get(12)), 2));
        this.secondField = new TextField(2);
        this.secondField.setText(pad(Integer.toString(instance.get(13)), 2));
        Panel panel = new Panel(new GridLayout(2, 1));
        panel.add(new Label("Hour"));
        panel.add(this.hourField);
        add(panel);
        Panel panel2 = new Panel(new GridLayout(2, 1));
        panel2.add(new Label("Min"));
        panel2.add(this.minuteField);
        add(panel2);
        Panel panel3 = new Panel(new GridLayout(2, 1));
        panel3.add(new Label("Sec"));
        panel3.add(this.secondField);
        add(panel3);
    }

    public String getValue() {
        return pad(this.hourField.getText(), 2) + ":" + pad(this.minuteField.getText(), 2) + ":" + pad(this.secondField.getText(), 2);
    }

    private String pad(String str, int i) {
        int length = str.length();
        if (length >= i) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i - length; i2++) {
            stringBuffer.append("0");
        }
        stringBuffer.append(str);
        return new String(stringBuffer);
    }

    public static void main(String[] strArr) {
        TimeEntryPanel timeEntryPanel = new TimeEntryPanel();
        Frame frame = new Frame("TimeEntryPanel");
        frame.setLayout(new BorderLayout());
        frame.add(timeEntryPanel, "Center");
        frame.pack();
        frame.addWindowListener(new WindowAdapter(timeEntryPanel) {
            final /* synthetic */ TimeEntryPanel val$dp;

            {
                this.val$dp = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                System.out.println(this.val$dp.getValue());
                System.exit(0);
            }
        });
        frame.setVisible(true);
    }
}
