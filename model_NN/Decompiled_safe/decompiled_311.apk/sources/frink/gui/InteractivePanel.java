package frink.gui;

import frink.expr.OperatorExpression;
import frink.io.OutputManager;
import frink.parser.Frink;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.OutputStream;
import java.net.URL;

public class InteractivePanel extends Panel implements OutputManager, ActiveFieldProvider, InteractiveFields {
    /* access modifiers changed from: private */
    public TextComponent activeField;
    private TextComponent activeFromField;
    /* access modifiers changed from: private */
    public InteractiveController controller;
    private Panel convertPanel;
    private FontSelectorDialog fontDialog;
    private Frame frame;
    /* access modifiers changed from: private */
    public TextArea fromArea;
    private boolean fromAreaInitialized;
    /* access modifiers changed from: private */
    public TextField fromField;
    private Button goButton;
    private int height;
    private FileDialog historyDialog;
    private Panel inputAreaPanel;
    private Panel legendPanel;
    private Panel mainPanel;
    private SimpleMenuBar menuBar;
    private Panel noConvertPanel;
    private TextArea output;
    private ProgrammingPanel programPanel;
    private Panel subInputPanel;
    /* access modifiers changed from: private */
    public TextField toField;
    private Toolbar toolbar;
    private FileDialog useDialog;
    private boolean useSystemColors;
    private int width;

    public InteractivePanel(int i) {
        this(i, false);
    }

    public InteractivePanel(int i, boolean z) {
        this.controller = new InteractiveController(i, this, false);
        this.controller.getInterpreter().getEnvironment().setOutputManager(this);
        this.controller.getInterpreter().getEnvironment().getGraphicsViewFactory().setDefaultConstructorName("ScalingTranslatingCanvasFrame");
        this.useSystemColors = z;
        this.frame = null;
        this.programPanel = null;
        this.menuBar = null;
        this.useDialog = null;
        this.historyDialog = null;
        this.width = 500;
        this.height = OperatorExpression.PREC_ADD;
        this.fontDialog = null;
        this.fromAreaInitialized = false;
        initGUI();
    }

    private void initGUI() {
        this.toolbar = null;
        this.mainPanel = new Panel(new BorderLayout());
        setLayout(new BorderLayout());
        Label label = new Label("From:");
        Label label2 = new Label("To:");
        this.output = new TextArea("", 0, 0, 1);
        this.output.append("Frink - Copyright 2000-2011 Alan Eliasen, eliasen@mindspring.com\n");
        this.output.append(" http://futureboy.us/frinkdocs/\n");
        this.output.append("Enter calculations in the text fields at bottom.\n");
        this.output.append("Use up/down arrows to repeat/modify previous calculations.");
        this.output.setEditable(false);
        if (!this.useSystemColors) {
            setOutputBackground(Color.black);
            setOutputForeground(Color.white);
        }
        this.mainPanel.add(this.output, "Center");
        this.goButton = new Button("Go");
        this.goButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                InteractivePanel.this.controller.performCalculation(InteractivePanel.this.getFromText(), InteractivePanel.this.getToText());
            }
        });
        AnonymousClass2 r2 = new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                int keyCode = keyEvent.getKeyCode();
                if (keyCode == 38 || keyCode == 224) {
                    InteractivePanel.this.controller.historyBack();
                    keyEvent.consume();
                }
                if (keyCode == 40 || keyCode == 225) {
                    InteractivePanel.this.controller.historyForward();
                    keyEvent.consume();
                }
            }
        };
        AnonymousClass3 r3 = new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                int keyCode = keyEvent.getKeyCode();
                if ((keyCode == 38 || keyCode == 224) && (keyEvent.getModifiers() & 2) != 0) {
                    InteractivePanel.this.controller.historyBack();
                    keyEvent.consume();
                }
                if ((keyCode == 40 || keyCode == 225) && (keyEvent.getModifiers() & 2) != 0) {
                    InteractivePanel.this.controller.historyForward();
                    keyEvent.consume();
                }
                if (keyCode == 10 && (keyEvent.getModifiers() & 2) != 0) {
                    InteractivePanel.this.controller.performCalculation(InteractivePanel.this.getFromText(), InteractivePanel.this.getToText());
                    keyEvent.consume();
                }
            }
        };
        this.fromField = new TextField();
        this.fromArea = new TextArea(4, 80);
        this.toField = new TextField();
        this.activeFromField = this.fromField;
        this.activeField = this.fromField;
        AnonymousClass4 r4 = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                InteractivePanel.this.controller.performCalculation(InteractivePanel.this.getFromText(), InteractivePanel.this.getToText());
            }
        };
        this.fromField.addActionListener(r4);
        this.toField.addActionListener(r4);
        this.fromField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent focusEvent) {
                TextComponent unused = InteractivePanel.this.activeField = InteractivePanel.this.fromField;
            }
        });
        this.fromArea.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent focusEvent) {
                TextComponent unused = InteractivePanel.this.activeField = InteractivePanel.this.fromArea;
            }
        });
        this.toField.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent focusEvent) {
                TextComponent unused = InteractivePanel.this.activeField = InteractivePanel.this.toField;
            }
        });
        this.fromField.addKeyListener(r2);
        this.fromArea.addKeyListener(r3);
        this.toField.addKeyListener(r2);
        this.inputAreaPanel = new Panel(new BorderLayout());
        this.mainPanel.add(this.inputAreaPanel, "South");
        this.subInputPanel = new Panel(new GridLayout(2, 1));
        this.convertPanel = new Panel(new BorderLayout());
        this.noConvertPanel = new Panel(new BorderLayout());
        this.legendPanel = new Panel(new GridLayout(2, 1));
        this.legendPanel.add(label);
        this.legendPanel.add(label2);
        add(this.mainPanel, "Center");
        setupInputPanel(this.controller.getMode());
    }

    public TextComponent getActiveFromField() {
        return this.activeFromField;
    }

    public int getMode() {
        return this.controller.getMode();
    }

    public void setFrame(Frame frame2) {
        this.frame = frame2;
    }

    public void selectFont() {
        if (this.fontDialog == null) {
            this.fontDialog = new FontSelectorDialog(this.frame, this.output.getFont());
        }
        this.fontDialog.showCentered();
        Font font = this.fontDialog.getFont();
        if (font != null) {
            this.output.setFont(font);
            this.fromField.setFont(font);
            setFromAreaFont();
            this.toField.setFont(font);
            validate();
        }
        setFocus();
    }

    private void setFromAreaFont() {
        Font font = this.fromField.getFont();
        if (font != null) {
            this.fromArea.setFont(new Font("Monospaced", font.getStyle(), font.getSize()));
        }
    }

    public void modeChangeRequested(int i, int i2) {
        if (i2 == 3) {
            if (!this.fromAreaInitialized) {
                setFromAreaFont();
                this.fromAreaInitialized = true;
            }
            this.activeFromField = this.fromArea;
        } else {
            this.activeFromField = this.fromField;
        }
        if (i2 == 2) {
            removeAll();
            initProgrammingPanel();
            add(this.programPanel, "Center");
            this.programPanel.setTitle();
            validate();
            this.programPanel.setFocus();
        } else {
            if (!(i == 0 || i == 1 || i == 3)) {
                removeAll();
                add(this.mainPanel, "Center");
            }
            setupInputPanel(i2);
            if (i2 == 0) {
                this.toField.setText("");
            }
            if (this.frame != null) {
                this.frame.setTitle("Frink");
            }
            validate();
            this.output.setCaretPosition(this.output.getText().length());
            setFocus();
        }
        if (this.menuBar != null) {
            this.menuBar.changeMode(i2);
        }
    }

    private void initProgrammingPanel() {
        if (this.programPanel == null) {
            this.programPanel = new ProgrammingPanel(this.frame, this.useSystemColors);
        }
        this.programPanel.setMenuBar(this.menuBar);
    }

    public void setInteractiveRunning(boolean z) {
        if (this.menuBar != null) {
            this.menuBar.setInteractiveRunning(z);
        }
    }

    public void clearOutput() {
        this.output.setText("");
    }

    public void useFile() {
        if (this.useDialog == null) {
            this.useDialog = new FileDialog(this.frame, "Use File");
        }
        this.useDialog.setMode(0);
        if (this.controller.getUseFile() == null) {
            this.useDialog.setDirectory(System.getProperty("user.dir"));
        }
        this.useDialog.show();
        String file = this.useDialog.getFile();
        setFocus();
        if (file != null) {
            this.controller.doUseFile(new File(this.useDialog.getDirectory(), file));
        }
    }

    public void saveHistory() {
        if (this.historyDialog == null) {
            this.historyDialog = new FileDialog(this.frame, "Save History");
        }
        this.historyDialog.setMode(1);
        if (this.controller.getHistoryFile() == null) {
            this.historyDialog.setDirectory(System.getProperty("user.dir"));
        }
        this.historyDialog.show();
        String file = this.historyDialog.getFile();
        setFocus();
        if (file != null) {
            this.controller.saveHistory(new File(this.historyDialog.getDirectory(), file));
        }
    }

    private void setupInputPanel(int i) {
        this.inputAreaPanel.removeAll();
        this.noConvertPanel.removeAll();
        this.convertPanel.removeAll();
        this.subInputPanel.removeAll();
        if (this.toolbar != null) {
            this.inputAreaPanel.add(this.toolbar, "North");
        }
        if (i == 0) {
            this.subInputPanel.add(this.fromField);
            this.subInputPanel.add(this.toField);
            this.convertPanel.add(this.legendPanel, "West");
            this.convertPanel.add(this.subInputPanel, "Center");
            this.convertPanel.add(this.goButton, "East");
            this.inputAreaPanel.add(this.convertPanel, "Center");
        } else if (i == 1 || i == 3) {
            if (i == 1) {
                this.noConvertPanel.add(this.fromField, "Center");
            } else {
                this.noConvertPanel.add(this.fromArea, "Center");
            }
            this.noConvertPanel.add(this.goButton, "East");
            this.inputAreaPanel.add(this.noConvertPanel, "Center");
        }
        validate();
    }

    private void adjustCaret() {
        getActiveFromField().setCaretPosition(getActiveFromField().getText().length());
        if (this.controller.getMode() == 0) {
            this.toField.setCaretPosition(this.toField.getText().length());
        }
    }

    public void setFocus() {
        getActiveFromField().requestFocus();
    }

    public Frink getInterpreter() {
        return this.controller.getInterpreter();
    }

    public void output(String str) {
        this.output.append(str);
        this.output.setCaretPosition(this.output.getText().length());
    }

    public void outputln(String str) {
        this.output.append(str + "\n");
        this.output.setCaretPosition(this.output.getText().length());
    }

    public OutputStream getRawOutputStream() {
        return null;
    }

    public TextComponent getActiveField() {
        return this.activeField;
    }

    public void appendInput(String str) {
        this.output.append(str);
        this.output.setCaretPosition(this.output.getText().length());
    }

    public void appendOutput(String str) {
        this.output.append(str);
        this.output.setCaretPosition(this.output.getText().length());
    }

    public void setToolbar(Toolbar toolbar2, int i) {
        this.toolbar = toolbar2;
        setupInputPanel(i);
    }

    public static Frame initializeFrame(InteractivePanel interactivePanel, boolean z, boolean z2) {
        Toolkit toolkit;
        Image image;
        Frame frame2 = new Frame("Frink");
        frame2.setLayout(new BorderLayout());
        frame2.add(interactivePanel, "Center");
        interactivePanel.setFrame(frame2);
        if (z) {
            SimpleMenuBar simpleMenuBar = new SimpleMenuBar(frame2, interactivePanel, z2);
            frame2.setMenuBar(simpleMenuBar);
            interactivePanel.setMenuBar(simpleMenuBar);
        }
        interactivePanel.setToolbar(new Toolbar(interactivePanel, frame2), interactivePanel.getController().getMode());
        interactivePanel.getInterpreter().getEnvironment().setInputManager(new AWTInputManager(frame2));
        frame2.setSize(interactivePanel.width, interactivePanel.height);
        frame2.addWindowListener(new WindowAdapter(interactivePanel) {
            final /* synthetic */ InteractivePanel val$p;

            {
                this.val$p = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }

            public void windowOpened(WindowEvent windowEvent) {
                this.val$p.setFocus();
            }
        });
        URL resource = interactivePanel.getClass().getResource("/data/icon.gif");
        if (!(resource == null || (toolkit = frame2.getToolkit()) == null || (image = toolkit.getImage(resource)) == null)) {
            frame2.setIconImage(image);
        }
        frame2.setVisible(true);
        return frame2;
    }

    public void setOutputBackground(Color color) {
        this.output.setBackground(color);
    }

    public void setOutputForeground(Color color) {
        this.output.setForeground(color);
    }

    public ProgrammingPanel getProgrammingPanel() {
        initProgrammingPanel();
        return this.programPanel;
    }

    public void setMenuBar(SimpleMenuBar simpleMenuBar) {
        this.menuBar = simpleMenuBar;
        if (this.programPanel != null) {
            this.programPanel.setMenuBar(this.menuBar);
        }
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public InteractiveController getController() {
        return this.controller;
    }

    public void setFromText(String str) {
        getActiveFromField().setText(str);
        adjustCaret();
    }

    public String getFromText() {
        return getActiveFromField().getText();
    }

    public String getToText() {
        if (this.toField != null) {
            return this.toField.getText();
        }
        return null;
    }

    public void setToText(String str) {
        this.toField.setText(str);
        adjustCaret();
    }

    public void doLoadFile(File file) {
        initProgrammingPanel();
        this.programPanel.doLoadFile(file);
    }

    public static void main(String[] strArr) {
        InteractivePanel interactivePanel = new InteractivePanel(0);
        initializeFrame(interactivePanel, true, true);
        interactivePanel.getController().parseArguments(strArr);
    }
}
