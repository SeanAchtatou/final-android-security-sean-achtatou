package frink.gui;

import frink.parser.Frink;
import java.awt.HeadlessException;
import java.util.Vector;

public class FrinkStarter {
    private static boolean startAWT = false;
    private static boolean startFullScreen = false;
    private static boolean startSwing = false;

    private static String[] parseArguments(String[] strArr) {
        int length = strArr.length;
        Vector vector = new Vector(strArr.length);
        int i = 0;
        while (i < length) {
            if (strArr[i].equals("--awt")) {
                startAWT = true;
            } else if (strArr[i].equals("--gui") || strArr[i].equals("--swing")) {
                startSwing = true;
            } else if (strArr[i].equals("--fullscreen")) {
                startFullScreen = true;
            } else if (strArr[i].equals("-open")) {
                startSwing = true;
                vector.addElement(strArr[i]);
                i++;
                vector.addElement(strArr[i]);
            } else {
                vector.addElement(strArr[i]);
            }
            i++;
        }
        String[] strArr2 = new String[vector.size()];
        for (int i2 = 0; i2 < vector.size(); i2++) {
            strArr2[i2] = (String) vector.elementAt(i2);
        }
        return strArr2;
    }

    public static void main(String[] strArr) {
        String[] parseArguments = parseArguments(strArr);
        try {
            if (startAWT) {
                InteractivePanel.main(parseArguments);
            } else if (startFullScreen) {
                FullScreenAWTStarter.main(parseArguments);
            } else if (startSwing) {
                SwingInteractivePanel.main(parseArguments);
            } else {
                Frink.main(parseArguments);
            }
        } catch (HeadlessException e) {
            System.err.println("Headless exception caught:\n  " + e);
            System.err.println("Starting command-line mode.");
            Frink.main(parseArguments);
        }
    }
}
