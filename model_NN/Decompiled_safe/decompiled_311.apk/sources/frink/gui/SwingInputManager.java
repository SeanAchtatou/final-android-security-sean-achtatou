package frink.gui;

import frink.expr.Environment;
import frink.io.InputItem;
import frink.io.InputManager;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

public class SwingInputManager extends JDialog implements InputManager {
    private static final Color FIELD_BACKGROUND = new Color(0, 60, 0);
    private GridBagConstraints fieldConstraints;
    /* access modifiers changed from: private */
    public Vector<JTextComponent> fieldList;
    private JPanel inputPanel;
    private GridBagConstraints labelConstraints;
    private JFrame parentFrame;
    private JLabel promptLabel = new JLabel("", 0);
    private JScrollPane scroll;

    public SwingInputManager(JFrame jFrame) {
        super(jFrame, "Frink Input", true);
        this.parentFrame = jFrame;
        setBackground(Color.black);
        this.promptLabel.setOpaque(true);
        this.promptLabel.setBackground(Color.black);
        this.promptLabel.setForeground(Color.green);
        this.promptLabel.setBorder(BorderFactory.createEmptyBorder(7, 0, 4, 0));
        Button button = new Button("OK");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingInputManager.this.hide();
            }
        });
        Container contentPane = getContentPane();
        this.inputPanel = new JPanel(new GridBagLayout());
        this.inputPanel.setBorder(BorderFactory.createMatteBorder(10, 0, 10, 0, Color.black));
        this.inputPanel.setBackground(Color.black);
        this.scroll = new JScrollPane(this.inputPanel, 20, 30);
        this.scroll.setBorder(BorderFactory.createEmptyBorder());
        this.fieldList = new Vector<>();
        contentPane.add(this.promptLabel, "North");
        contentPane.add(this.scroll, "Center");
        contentPane.add(button, "South");
        addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent windowEvent) {
                JTextComponent jTextComponent = (JTextComponent) SwingInputManager.this.fieldList.elementAt(0);
                jTextComponent.requestFocus();
                jTextComponent.setCaretPosition(jTextComponent.getText().length());
            }

            public void windowGainedFocus(WindowEvent windowEvent) {
                JTextComponent jTextComponent = (JTextComponent) SwingInputManager.this.fieldList.elementAt(0);
                jTextComponent.setCaretPosition(jTextComponent.getText().length());
                jTextComponent.requestFocus();
            }
        });
        setResizable(true);
        this.labelConstraints = new GridBagConstraints();
        this.labelConstraints.gridx = 0;
        this.labelConstraints.anchor = 13;
        this.fieldConstraints = new GridBagConstraints();
        this.fieldConstraints.gridx = 1;
        this.fieldConstraints.weightx = 1.0d;
        this.fieldConstraints.gridwidth = 0;
        this.fieldConstraints.fill = 2;
    }

    private void packAndDisplay() {
        pack();
        validate();
        Container parent = getParent();
        Point locationOnScreen = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        Dimension preferredSize = getPreferredSize();
        int i = preferredSize.width;
        if (i < size.width) {
            i = size.width;
        }
        int i2 = preferredSize.height;
        setSize(i, i2);
        setLocation(((size.width - i) / 2) + locationOnScreen.x, ((size.height - i2) / 2) + locationOnScreen.y);
        show();
    }

    public String input(String str, String str2, Environment environment) {
        String showInputDialog = JOptionPane.showInputDialog(this.parentFrame, str, str2);
        if (showInputDialog == null) {
            return "";
        }
        return showInputDialog;
    }

    public String[] input(String str, InputItem[] inputItemArr, Environment environment) {
        clearGUI();
        if (str == null) {
            this.promptLabel.setText("");
        } else {
            this.promptLabel.setText(str);
        }
        JTextComponent jTextComponent = null;
        for (InputItem addInputItem : inputItemArr) {
            jTextComponent = addInputItem(addInputItem);
            this.fieldList.addElement(jTextComponent);
        }
        if (jTextComponent instanceof JTextField) {
            ((JTextField) jTextComponent).addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    SwingInputManager.this.hide();
                }
            });
        }
        packAndDisplay();
        int size = this.fieldList.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            String text = this.fieldList.elementAt(i).getText();
            if (text == null) {
                text = "";
            }
            strArr[i] = text;
        }
        return strArr;
    }

    private void clearGUI() {
        this.inputPanel.removeAll();
        this.fieldList.clear();
    }

    private JTextComponent addInputItem(InputItem inputItem) {
        return addField(inputItem.label, inputItem.defaultValue);
    }

    private JTextComponent addField(String str, String str2) {
        if (str != null) {
            JLabel jLabel = new JLabel(str, 4);
            jLabel.setBorder(BorderFactory.createMatteBorder(3, 7, 3, 7, Color.black));
            jLabel.setOpaque(true);
            jLabel.setBackground(Color.black);
            jLabel.setForeground(Color.green);
            this.inputPanel.add(jLabel, this.labelConstraints);
        }
        JTextField jTextField = new JTextField();
        jTextField.setBackground(FIELD_BACKGROUND);
        jTextField.setOpaque(true);
        jTextField.setForeground(Color.green);
        jTextField.setCaretColor(Color.yellow);
        jTextField.setBorder(BorderFactory.createMatteBorder(3, 0, 3, 7, Color.black));
        jTextField.setColumns(20);
        if (str2 != null) {
            jTextField.setText(str2);
        }
        this.inputPanel.add(jTextField, this.fieldConstraints);
        return jTextField;
    }
}
