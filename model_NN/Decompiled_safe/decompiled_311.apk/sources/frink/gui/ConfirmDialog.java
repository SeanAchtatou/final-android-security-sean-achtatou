package frink.gui;

import java.awt.Button;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConfirmDialog extends Dialog {
    public static final int VALUE_CANCEL = 0;
    public static final int VALUE_OK = 1;
    private Button cancelButton;
    private Button okButton;
    private Label questionLabel;
    /* access modifiers changed from: private */
    public int value = 0;

    public ConfirmDialog(Frame frame) {
        super(frame, true);
        initGUI();
    }

    private void initGUI() {
        this.questionLabel = new Label();
        this.questionLabel.setAlignment(1);
        this.okButton = new Button("OK");
        this.okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                int unused = ConfirmDialog.this.value = 1;
                ConfirmDialog.this.setVisible(false);
            }
        });
        this.cancelButton = new Button("Cancel");
        this.cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                int unused = ConfirmDialog.this.value = 0;
                ConfirmDialog.this.setVisible(false);
            }
        });
        add(this.questionLabel, "Center");
        Panel panel = new Panel(new FlowLayout(1));
        panel.add(this.okButton);
        panel.add(this.cancelButton);
        add(panel, "South");
    }

    public int display(String str, String str2) {
        this.questionLabel.setText(str);
        setTitle(str2);
        pack();
        Container parent = getParent();
        Point locationOnScreen = parent.getLocationOnScreen();
        Dimension size = parent.getSize();
        Dimension size2 = getSize();
        setLocation(locationOnScreen.x + ((size.width - size2.width) / 2), ((size.height - size2.height) / 2) + locationOnScreen.y);
        setVisible(true);
        return this.value;
    }
}
