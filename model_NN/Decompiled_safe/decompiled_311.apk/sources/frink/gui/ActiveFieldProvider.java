package frink.gui;

import java.awt.TextComponent;

public interface ActiveFieldProvider {
    TextComponent getActiveField();
}
