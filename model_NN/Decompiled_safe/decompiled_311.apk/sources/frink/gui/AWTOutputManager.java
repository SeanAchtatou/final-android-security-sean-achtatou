package frink.gui;

import frink.io.OutputManager;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.OutputStream;

public class AWTOutputManager extends Panel implements OutputManager {
    private Button clearButton;
    private Button closeButton;
    /* access modifiers changed from: private */
    public Dialog dialog;
    /* access modifiers changed from: private */
    public TextArea output;
    private Frame parent;
    /* access modifiers changed from: private */
    public ProgrammingPanel progPanel;
    private Button stopButton;

    public AWTOutputManager(Frame frame, ProgrammingPanel programmingPanel) {
        this.parent = frame;
        this.progPanel = programmingPanel;
        initGUI();
    }

    private void initGUI() {
        this.dialog = null;
        setLayout(new BorderLayout());
        this.output = new TextArea("", 10, 80, 0);
        this.output.setFont(new Font("Monospaced", 0, 12));
        this.output.setEditable(false);
        this.output.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == 9) {
                    AWTOutputManager.this.output.transferFocus();
                }
            }
        });
        Panel panel = new Panel(new FlowLayout());
        this.clearButton = new Button("Clear");
        this.clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                AWTOutputManager.this.clear();
            }
        });
        panel.add(this.clearButton);
        this.closeButton = new Button("Close");
        this.closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                AWTOutputManager.this.close();
            }
        });
        panel.add(this.closeButton);
        this.stopButton = new Button("Stop");
        enableStopButton(false);
        this.stopButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (AWTOutputManager.this.progPanel != null) {
                    AWTOutputManager.this.progPanel.stopProgram();
                }
            }
        });
        panel.add(this.stopButton);
        add(this.output, "Center");
        add(panel, "South");
    }

    public void output(String str) {
        this.output.append(str);
        showDialog();
    }

    public void outputln(String str) {
        this.output.append(str + "\n");
        showDialog();
    }

    public void close() {
        this.dialog.setVisible(false);
        clear();
    }

    public void clear() {
        this.output.setText("");
    }

    public void enableStopButton(boolean z) {
        this.stopButton.setEnabled(z);
    }

    private void showDialog() {
        if (this.dialog == null) {
            this.dialog = new Dialog(this.parent, "Frink Output Window");
            this.dialog.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent windowEvent) {
                    AWTOutputManager.this.dialog.setVisible(false);
                    AWTOutputManager.this.clear();
                }
            });
            this.dialog.setLayout(new BorderLayout());
            this.dialog.add(this, "Center");
            this.dialog.setSize(this.parent.getSize());
        }
        this.dialog.setVisible(true);
        this.dialog.toFront();
    }

    public OutputStream getRawOutputStream() {
        return null;
    }
}
