package frink.gui;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Toolbar extends Panel {
    /* access modifiers changed from: private */
    public DateTimeEntryDialog dateDialog;
    /* access modifiers changed from: private */
    public ActiveFieldProvider fieldProv;
    /* access modifiers changed from: private */
    public Frame parent;

    public Toolbar(ActiveFieldProvider activeFieldProvider, Frame frame) {
        FlowLayout flowLayout = new FlowLayout(0);
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);
        setLayout(flowLayout);
        this.fieldProv = activeFieldProvider;
        this.parent = frame;
        initButtons();
    }

    private void initButtons() {
        this.dateDialog = null;
        Button button = new Button("Time");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (Toolbar.this.dateDialog == null) {
                    DateTimeEntryDialog unused = Toolbar.this.dateDialog = new DateTimeEntryDialog(Toolbar.this.parent);
                }
                Toolbar.this.dateDialog.showCentered();
                String value = Toolbar.this.dateDialog.getValue();
                if (value != null) {
                    TextComponent activeField = Toolbar.this.fieldProv.getActiveField();
                    StringBuffer stringBuffer = new StringBuffer(activeField.getText());
                    int caretPosition = activeField.getCaretPosition();
                    stringBuffer.insert(caretPosition, value);
                    String str = new String(stringBuffer);
                    activeField.setText(str);
                    activeField.setCaretPosition(str.length() + caretPosition);
                    activeField.requestFocus();
                }
            }
        });
        add(button);
        add(new TextShortcutButton("\" \"", "\"", "\"", this.fieldProv));
        add(new TextShortcutButton("[ ]", "[", "]", this.fieldProv));
        add(new TextShortcutButton("( )", "(", ")", this.fieldProv));
        add(new TextShortcutButton("/", "/ (", ")", this.fieldProv));
        add(new TextShortcutButton("^", "", "^", "(", ")^", -1, this.fieldProv));
        add(new TextShortcutButton("^( )", "", "^()", "(", ")^()", -1, this.fieldProv));
    }
}
