package frink.gui;

import frink.expr.HelpExpression;
import frink.io.BrowserHelper;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class SwingMenuBar extends JMenuBar {
    private JMenuItem alternateData;
    private JMenuItem clearMenu;
    private JMenuItem convert;
    private JMenu dataMenu;
    private JMenuItem defaultData;
    private JMenuItem documentationMenu;
    private JMenuItem donateMenu;
    private JMenuItem fontMenu;
    private JMenu frinkMenu;
    private JMenu helpMenu;
    /* access modifiers changed from: private */
    public SwingInteractivePanel iPanel;
    private JMenuItem interruptMenuItem;
    private JMenuItem loadMenuItem;
    private JMenu modeMenu;
    private JMenuItem multiLine;
    private JMenuItem oneLine;
    private JFrame parentFrame;
    private JMenuItem program;
    private boolean programRunning = false;
    private JMenuItem runMenuItem;
    private JMenuItem saveHistoryMenuItem;
    private JMenuItem saveMenuItem;
    private JMenuItem stopMenuItem;
    private JMenuItem useMenuItem;
    private JMenuItem versionMenu;
    private JMenu viewMenu;
    private JMenuItem whatsNewMenu;

    public SwingMenuBar(JFrame jFrame, SwingInteractivePanel swingInteractivePanel) {
        this.parentFrame = jFrame;
        this.iPanel = swingInteractivePanel;
        init();
    }

    private void init() {
        this.frinkMenu = new JMenu("Frink");
        this.frinkMenu.setMnemonic(70);
        this.loadMenuItem = new JMenuItem("Load", 76);
        this.loadMenuItem.setAccelerator(KeyStroke.getKeyStroke(76, 128));
        this.loadMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel programmingPanel = SwingMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.loadProgram();
                }
            }
        });
        this.frinkMenu.add(this.loadMenuItem);
        this.saveMenuItem = new JMenuItem("Save", 83);
        this.saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(83, 128));
        this.saveMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel programmingPanel = SwingMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.saveProgram();
                }
            }
        });
        this.frinkMenu.add(this.saveMenuItem);
        this.saveHistoryMenuItem = new JMenuItem("Save History", 83);
        this.saveHistoryMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.saveHistory();
            }
        });
        this.frinkMenu.add(this.saveHistoryMenuItem);
        this.runMenuItem = new JMenuItem("Run", 82);
        this.runMenuItem.setAccelerator(KeyStroke.getKeyStroke(82, 128));
        this.runMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel programmingPanel = SwingMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.runProgram();
                }
            }
        });
        this.frinkMenu.add(this.runMenuItem);
        this.stopMenuItem = new JMenuItem("Stop", 67);
        this.stopMenuItem.setAccelerator(KeyStroke.getKeyStroke(67, 128));
        this.stopMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel programmingPanel = SwingMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.stopProgram();
                }
            }
        });
        this.frinkMenu.add(this.stopMenuItem);
        this.useMenuItem = new JMenuItem("Use File...", 85);
        this.useMenuItem.setAccelerator(KeyStroke.getKeyStroke(85, 128));
        this.useMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.useFile();
            }
        });
        this.frinkMenu.add(this.useMenuItem);
        this.interruptMenuItem = new JMenuItem("Interrupt calculation", 73);
        this.interruptMenuItem.setAccelerator(KeyStroke.getKeyStroke(73, 128));
        this.interruptMenuItem.setEnabled(false);
        this.interruptMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.getController().interrupt(true);
            }
        });
        this.frinkMenu.add(this.interruptMenuItem);
        JMenuItem jMenuItem = new JMenuItem("Exit", 81);
        jMenuItem.setAccelerator(KeyStroke.getKeyStroke(81, 128));
        jMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (JOptionPane.showConfirmDialog((Component) null, "Do you really want to exit Frink?", "Frink Exit Confirmation", 2) == 0) {
                    System.exit(0);
                }
            }
        });
        this.frinkMenu.add(jMenuItem);
        add(this.frinkMenu);
        this.modeMenu = new JMenu("Mode");
        this.modeMenu.setMnemonic(77);
        this.oneLine = new JMenuItem("One-Line", 49);
        this.oneLine.setAccelerator(KeyStroke.getKeyStroke(49, 128));
        this.oneLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.getController().requestModeChange(1);
            }
        });
        this.modeMenu.add(this.oneLine);
        this.convert = new JMenuItem("Convert", 50);
        this.convert.setAccelerator(KeyStroke.getKeyStroke(50, 128));
        this.convert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.getController().requestModeChange(0);
            }
        });
        this.modeMenu.add(this.convert);
        this.multiLine = new JMenuItem("Multi-Line", 51);
        this.multiLine.setAccelerator(KeyStroke.getKeyStroke(51, 128));
        this.multiLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.getController().requestModeChange(3);
            }
        });
        this.modeMenu.add(this.multiLine);
        this.program = new JMenuItem("Programming", 80);
        this.program.setAccelerator(KeyStroke.getKeyStroke(80, 128));
        this.program.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingMenuBar.this.iPanel.getController().requestModeChange(2);
            }
        });
        this.modeMenu.add(this.program);
        add(this.modeMenu);
        this.dataMenu = new JMenu("Data");
        this.defaultData = new JMenuItem("Use default units file");
        this.defaultData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel programmingPanel = SwingMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.useDefaultUnits();
                }
            }
        });
        this.dataMenu.add(this.defaultData);
        this.alternateData = new JMenuItem("Select alternate units file...");
        this.alternateData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                SwingProgrammingPanel programmingPanel = SwingMenuBar.this.iPanel.getProgrammingPanel();
                if (programmingPanel != null) {
                    programmingPanel.selectUnitsFile();
                }
            }
        });
        this.dataMenu.add(this.alternateData);
        add(this.dataMenu);
        this.viewMenu = new JMenu("View");
        this.fontMenu = new JMenuItem("Font...", 70);
        this.fontMenu.setAccelerator(KeyStroke.getKeyStroke(70, 128));
        this.fontMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if (SwingMenuBar.this.iPanel.getController().getMode() == 2) {
                    SwingMenuBar.this.iPanel.getProgrammingPanel().selectFont();
                } else {
                    SwingMenuBar.this.iPanel.selectFont();
                }
            }
        });
        this.viewMenu.add(this.fontMenu);
        add(this.viewMenu);
        this.helpMenu = new JMenu(HelpExpression.TYPE);
        this.helpMenu.setMnemonic(72);
        if (BrowserHelper.canOpenURLs()) {
            this.documentationMenu = new JMenuItem("Frink Documentation");
            this.documentationMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    BrowserHelper.openURL("http://futureboy.us/frinkdocs/");
                }
            });
            this.helpMenu.add(this.documentationMenu);
            this.whatsNewMenu = new JMenuItem("What's New");
            this.whatsNewMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    BrowserHelper.openURL("http://futureboy.us/frinkdocs/whatsnew.html");
                }
            });
            this.helpMenu.add(this.whatsNewMenu);
            this.donateMenu = new JMenuItem("Donate");
            this.donateMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    BrowserHelper.openURL("http://futureboy.us/frinkdocs/donate.html");
                }
            });
            this.helpMenu.add(this.donateMenu);
        }
        try {
            this.versionMenu = new JMenuItem((String) Class.forName("frink.parser.FrinkVersion").getField("VERSION").get(null));
            this.versionMenu.setEnabled(false);
            this.helpMenu.add(this.versionMenu);
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e) {
        }
        add(this.helpMenu);
        changeMode(0);
    }

    public void setProgramRunning(boolean z) {
        this.programRunning = z;
        this.runMenuItem.setEnabled(!z);
        this.stopMenuItem.setEnabled(z);
    }

    public void setInteractiveRunning(boolean z) {
        this.interruptMenuItem.setEnabled(z);
    }

    public void changeMode(int i) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (i == 2) {
            z = true;
        } else {
            z = false;
        }
        this.runMenuItem.setEnabled(z && !this.programRunning);
        JMenuItem jMenuItem = this.stopMenuItem;
        if (!z || !this.programRunning) {
            z2 = false;
        } else {
            z2 = true;
        }
        jMenuItem.setEnabled(z2);
        this.loadMenuItem.setEnabled(z);
        this.saveMenuItem.setEnabled(z);
        JMenuItem jMenuItem2 = this.saveHistoryMenuItem;
        if (!z) {
            z3 = true;
        } else {
            z3 = false;
        }
        jMenuItem2.setEnabled(z3);
        JMenuItem jMenuItem3 = this.useMenuItem;
        if (!z) {
            z4 = true;
        } else {
            z4 = false;
        }
        jMenuItem3.setEnabled(z4);
        this.defaultData.setEnabled(z);
        this.alternateData.setEnabled(z);
        this.dataMenu.setEnabled(z);
        this.oneLine.setEnabled(true);
        this.multiLine.setEnabled(true);
        this.convert.setEnabled(true);
        this.program.setEnabled(true);
        switch (i) {
            case 0:
                this.convert.setEnabled(false);
                return;
            case 1:
                this.oneLine.setEnabled(false);
                return;
            case 2:
                this.program.setEnabled(false);
                return;
            case 3:
                this.multiLine.setEnabled(false);
                return;
            default:
                return;
        }
    }
}
