package frink.gui;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;

public class DateEntryPanel extends Panel {
    private Choice dayField;
    private Choice monthField;
    private TextField yearField;

    public DateEntryPanel() {
        initGUI();
    }

    private void initGUI() {
        setLayout(new FlowLayout());
        Calendar instance = Calendar.getInstance();
        this.yearField = new TextField(4);
        this.yearField.setText(Integer.toString(instance.get(1)));
        this.monthField = new Choice();
        for (int i = 1; i <= 12; i++) {
            this.monthField.add(Integer.toString(i));
        }
        this.monthField.select(instance.get(2));
        this.dayField = new Choice();
        for (int i2 = 1; i2 <= 31; i2++) {
            this.dayField.add(Integer.toString(i2));
        }
        this.dayField.select(instance.get(5) - 1);
        Panel panel = new Panel(new GridLayout(2, 1));
        panel.add(new Label("Year"));
        panel.add(this.yearField);
        add(panel);
        Panel panel2 = new Panel(new GridLayout(2, 1));
        panel2.add(new Label("Month"));
        panel2.add(this.monthField);
        add(panel2);
        Panel panel3 = new Panel(new GridLayout(2, 1));
        panel3.add(new Label("Day"));
        panel3.add(this.dayField);
        add(panel3);
    }

    public String getValue() {
        return pad(this.yearField.getText(), 4) + "-" + pad(this.monthField.getSelectedItem(), 2) + "-" + pad(this.dayField.getSelectedItem(), 2);
    }

    private String pad(String str, int i) {
        int length = str.length();
        if (length >= i) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i - length; i2++) {
            stringBuffer.append("0");
        }
        stringBuffer.append(str);
        return new String(stringBuffer);
    }

    public static void main(String[] strArr) {
        DateEntryPanel dateEntryPanel = new DateEntryPanel();
        Frame frame = new Frame("DateEntryPanel");
        frame.setLayout(new BorderLayout());
        frame.add(dateEntryPanel, "Center");
        frame.pack();
        frame.addWindowListener(new WindowAdapter(dateEntryPanel) {
            final /* synthetic */ DateEntryPanel val$dp;

            {
                this.val$dp = r1;
            }

            public void windowClosing(WindowEvent windowEvent) {
                System.out.println(this.val$dp.getValue());
                System.exit(0);
            }
        });
        frame.setVisible(true);
    }
}
