package frink.text;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.SubstitutionExpression;
import frink.expr.TerminalExpression;
import frink.symbolic.MatchingContext;
import org.apache.oro.text.PatternCacheLRU;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.Perl5Substitution;
import org.apache.oro.text.regex.Substitution;

public class BasicSubstitutionExpression extends TerminalExpression implements SubstitutionExpression {
    private static PatternCacheLRU cache = new PatternCacheLRU(100);
    private int numsubs = 1;
    private Pattern pat;
    private boolean rightExpression = false;
    private Substitution sub;

    public BasicSubstitutionExpression(String str, String str2) {
        this.pat = cache.getPattern(str);
        this.sub = new Perl5Substitution(str2);
    }

    public BasicSubstitutionExpression(String str, String str2, String str3) {
        this.pat = cache.getPattern(str, parseModifiers(str3));
        if (this.rightExpression) {
            this.sub = new FrinkExpressionSubstitution(str2);
        } else {
            this.sub = new Perl5Substitution(str2);
        }
    }

    public Pattern getPattern() {
        return this.pat;
    }

    public Substitution getSubstitution() {
        return this.sub;
    }

    public int getNumSubs() {
        return this.numsubs;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isConstant() {
        return true;
    }

    private int parseModifiers(String str) {
        int length = str.length();
        int i = 32768;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            switch (charAt) {
                case 'e':
                    this.rightExpression = true;
                    break;
                case 'g':
                    this.numsubs = -1;
                    break;
                case 'i':
                    i |= 1;
                    break;
                case 'm':
                    i |= 8;
                    break;
                case 's':
                    i |= 16;
                    break;
                case 'x':
                    i |= 32;
                    break;
                default:
                    System.out.println("Unknown regex modifier '" + charAt + "'");
                    break;
            }
        }
        return i;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    public boolean isRightExpression() {
        return this.rightExpression;
    }

    public String getExpressionType() {
        return SubstitutionExpression.TYPE;
    }
}
