package frink.text;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Translator {
    private static final boolean DEBUG = false;
    private static final int IN_BUFFER_SIZE = 1024;
    private static final String SYSTRAN_AJAX_URL_STRING = "http://www.systranet.com/sai";
    private static URL SYSTRAN_URL = null;
    private static final String SYSTRAN_URL_STRING = "http://www.systranbox.com/systran/box";
    private static boolean cookieHandlerRemoved = DEBUG;
    private String fromLanguage;
    private String toLanguage;

    static {
        try {
            SYSTRAN_URL = new URL(SYSTRAN_URL_STRING);
        } catch (MalformedURLException e) {
            System.err.println("Malformed URL exception: " + e);
        }
    }

    public Translator(String str, String str2) {
        this.fromLanguage = str;
        this.toLanguage = str2;
    }

    public String translate(String str) {
        if (this.fromLanguage.equals(this.toLanguage)) {
            return str;
        }
        return translateSystranAjax(str);
    }

    public String translateBabelfish(String str) {
        try {
            try {
                URLConnection openConnection = new URL("http://babelfish.altavista.com/cgi-bin/translate").openConnection();
                openConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.6) Gecko/20091216 Fedora/3.5.6-1.fc12 Firefox/3.5.6");
                openConnection.setDoOutput(true);
                openConnection.setDoInput(true);
                openConnection.setUseCaches(DEBUG);
                String str2 = (("urltext=" + URLEncoder.encode(str)) + "&lp=" + URLEncoder.encode(this.fromLanguage) + '_' + URLEncoder.encode(this.toLanguage)) + "&doit=done";
                try {
                    DataOutputStream dataOutputStream = new DataOutputStream(openConnection.getOutputStream());
                    try {
                        dataOutputStream.writeBytes(str2);
                        dataOutputStream.flush();
                        try {
                            dataOutputStream.close();
                        } catch (IOException e) {
                            System.err.println("Could not close output stream.  Now that's weird. " + e);
                        }
                        try {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
                            boolean z = false;
                            boolean z2 = false;
                            boolean z3 = false;
                            StringBuffer stringBuffer = null;
                            while (true) {
                                try {
                                    String readLine = bufferedReader.readLine();
                                    if (readLine == null) {
                                        try {
                                            break;
                                        } catch (IOException e2) {
                                            System.err.println("Could not close input stream.  Now that's weird. " + e2);
                                        }
                                    } else if (!z2) {
                                        if (z3) {
                                            int indexOf = readLine.indexOf("helvetica\">");
                                            if (indexOf >= 0) {
                                                readLine = readLine.substring(indexOf + 11);
                                                z = true;
                                            }
                                        } else if (readLine.indexOf("/gifs/clear.gif") >= 0) {
                                            z3 = true;
                                        }
                                        if (z) {
                                            if (readLine.indexOf(60) >= 0) {
                                                z = false;
                                                z2 = true;
                                            } else if (stringBuffer == null) {
                                                stringBuffer = new StringBuffer(readLine);
                                            } else {
                                                stringBuffer.append(readLine);
                                            }
                                        }
                                    }
                                } catch (IOException e3) {
                                    System.err.println("Something went wrong in the readLine " + e3);
                                    if (stringBuffer == null) {
                                        return null;
                                    }
                                    return new String(stringBuffer);
                                }
                            }
                            bufferedReader.close();
                            if (stringBuffer == null) {
                                return null;
                            }
                            return new String(stringBuffer);
                        } catch (IOException e4) {
                            System.err.println("Could not get input stream " + e4);
                            return null;
                        }
                    } catch (IOException e5) {
                        System.err.println("Could not write data to output stream " + e5);
                        return "";
                    }
                } catch (IOException e6) {
                    System.err.println("Could not get output stream " + e6);
                    return null;
                }
            } catch (IOException e7) {
                System.err.println("Could not open connection. " + e7);
                return null;
            }
        } catch (MalformedURLException e8) {
            System.err.println("Malformed URL exception: " + e8);
            return null;
        }
    }

    private static void removeCookieHandler() {
        if (!cookieHandlerRemoved) {
            try {
                Class<?> cls = Class.forName("java.net.CookieHandler");
                cls.getMethod("setDefault", cls).invoke(cls, null);
            } catch (ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            }
            cookieHandlerRemoved = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:79:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x021b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String translateSystran(java.lang.String r13) {
        /*
            r12 = this;
            r10 = 1
            r9 = 0
            r8 = 0
            java.lang.String r5 = "UTF8"
            removeCookieHandler()
            java.net.URL r0 = frink.text.Translator.SYSTRAN_URL     // Catch:{ IOException -> 0x0122 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0122 }
            r0.setDoOutput(r10)
            r0.setDoInput(r10)
            r0.setUseCaches(r9)
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.6) Gecko/20091216 Fedora/3.5.6-1.fc12 Firefox/3.5.6"
            r0.setRequestProperty(r1, r2)
            java.lang.String r1 = "Referer"
            java.lang.String r2 = "http://www.systranbox.com/systran/box"
            r0.setRequestProperty(r1, r2)
            java.lang.String r1 = "UTF8"
            java.lang.String r1 = frink.text.TempURLEncoder.encode(r13, r1)     // Catch:{ UnsupportedEncodingException -> 0x013d }
        L_0x002b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "ttype=text&systran_text="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "&systran_lp="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r12.fromLanguage
            java.lang.StringBuilder r1 = r1.append(r2)
            r2 = 95
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r12.toLanguage
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "&systran_id=SystranSoft-en"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "&systran_charset=utf-8"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x015c }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ IOException -> 0x015c }
            r2.<init>(r3)     // Catch:{ IOException -> 0x015c }
            r2.writeBytes(r1)     // Catch:{ IOException -> 0x0177 }
            r2.flush()     // Catch:{ IOException -> 0x0177 }
            r2.close()     // Catch:{ IOException -> 0x0193 }
        L_0x009b:
            java.lang.String r1 = "Content-Type"
            java.lang.String r1 = r0.getHeaderField(r1)
            if (r1 == 0) goto L_0x025d
            java.lang.String r2 = "charset="
            int r2 = r1.indexOf(r2)
            r3 = -1
            if (r2 == r3) goto L_0x025d
            int r2 = r2 + 8
            java.lang.String r1 = r1.substring(r2)
            java.lang.String r1 = r1.trim()
            int r2 = r1.length()
            if (r2 != 0) goto L_0x00bd
            r1 = r8
        L_0x00bd:
            if (r1 != 0) goto L_0x00c2
            java.lang.String r1 = "UTF8"
            r1 = r5
        L_0x00c2:
            if (r1 != 0) goto L_0x01ae
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01bf }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01bf }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x01bf }
            r2.<init>(r0)     // Catch:{ IOException -> 0x01bf }
            r1.<init>(r2)     // Catch:{ IOException -> 0x01bf }
            r0 = r1
        L_0x00d3:
            r1 = r9
            r2 = r9
            r3 = r9
            r4 = r8
        L_0x00d7:
            java.lang.String r5 = r0.readLine()     // Catch:{ IOException -> 0x024b }
            if (r5 == 0) goto L_0x0222
            java.lang.String r5 = r5.trim()     // Catch:{ IOException -> 0x024b }
            int r6 = r5.length()     // Catch:{ IOException -> 0x024b }
            if (r6 == 0) goto L_0x00d7
            if (r2 != 0) goto L_0x00d7
            if (r3 != 0) goto L_0x0259
            java.lang.String r6 = "\"translation\""
            int r6 = r5.indexOf(r6)     // Catch:{ IOException -> 0x024b }
            if (r6 < 0) goto L_0x0254
            r3 = r6
            r6 = r10
        L_0x00f5:
            if (r6 == 0) goto L_0x0251
            java.lang.String r7 = ">"
            int r3 = r5.indexOf(r7, r3)     // Catch:{ IOException -> 0x024b }
            if (r3 < 0) goto L_0x0251
            int r1 = r3 + 1
            java.lang.String r1 = r5.substring(r1)     // Catch:{ IOException -> 0x024b }
            r3 = r1
            r1 = r10
        L_0x0107:
            if (r1 == 0) goto L_0x024e
            r5 = 60
            int r5 = r3.indexOf(r5)     // Catch:{ IOException -> 0x024b }
            if (r5 < 0) goto L_0x01e6
            if (r4 != 0) goto L_0x01db
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x024b }
            r2 = 0
            java.lang.String r2 = r3.substring(r2, r5)     // Catch:{ IOException -> 0x024b }
            r1.<init>(r2)     // Catch:{ IOException -> 0x024b }
        L_0x011d:
            r2 = r10
            r3 = r6
            r4 = r1
            r1 = r9
            goto L_0x00d7
        L_0x0122:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not open connection. "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r8
        L_0x013c:
            return r0
        L_0x013d:
            r1 = move-exception
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Language translations won't work.  This port of Java doesn't support UTF-8 encoding: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.println(r1)
            java.lang.String r1 = frink.text.TempURLEncoder.encode(r13)
            goto L_0x002b
        L_0x015c:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not get output stream "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r8
            goto L_0x013c
        L_0x0177:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not write data to output stream "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            java.lang.String r0 = ""
            goto L_0x013c
        L_0x0193:
            r1 = move-exception
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Could not close output stream.  Now that's weird. "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.println(r1)
            goto L_0x009b
        L_0x01ae:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x01bf }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x01bf }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x01bf }
            r3.<init>(r0, r1)     // Catch:{ IOException -> 0x01bf }
            r2.<init>(r3)     // Catch:{ IOException -> 0x01bf }
            r0 = r2
            goto L_0x00d3
        L_0x01bf:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not get input stream "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r8
            goto L_0x013c
        L_0x01db:
            r1 = 0
            java.lang.String r1 = r3.substring(r1, r5)     // Catch:{ IOException -> 0x024b }
            r4.append(r1)     // Catch:{ IOException -> 0x024b }
            r1 = r4
            goto L_0x011d
        L_0x01e6:
            if (r4 != 0) goto L_0x01f7
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x024b }
            r5.<init>(r3)     // Catch:{ IOException -> 0x024b }
            r3 = r5
        L_0x01ee:
            r4 = 32
            r3.append(r4)     // Catch:{ IOException -> 0x01fc }
            r4 = r3
            r3 = r6
            goto L_0x00d7
        L_0x01f7:
            r4.append(r3)     // Catch:{ IOException -> 0x024b }
            r3 = r4
            goto L_0x01ee
        L_0x01fc:
            r0 = move-exception
            r1 = r3
        L_0x01fe:
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Something went wrong in the readLine "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            r2.println(r0)
            if (r1 != 0) goto L_0x021b
            r0 = r8
            goto L_0x013c
        L_0x021b:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
            goto L_0x013c
        L_0x0222:
            r0.close()     // Catch:{ IOException -> 0x022a }
        L_0x0225:
            if (r4 != 0) goto L_0x0244
            r0 = r8
            goto L_0x013c
        L_0x022a:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not close input stream.  Now that's weird. "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            goto L_0x0225
        L_0x0244:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r4)
            goto L_0x013c
        L_0x024b:
            r0 = move-exception
            r1 = r4
            goto L_0x01fe
        L_0x024e:
            r3 = r6
            goto L_0x00d7
        L_0x0251:
            r3 = r5
            goto L_0x0107
        L_0x0254:
            r11 = r6
            r6 = r3
            r3 = r11
            goto L_0x00f5
        L_0x0259:
            r6 = r3
            r3 = r9
            goto L_0x00f5
        L_0x025d:
            r1 = r8
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.text.Translator.translateSystran(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0188  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String translateSystranAjax(java.lang.String r10) {
        /*
            r9 = this;
            r3 = 1
            r7 = 0
            r6 = -1
            r5 = 0
            java.lang.String r8 = "Content-Type"
            removeCookieHandler()
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x00b4 }
            r1.<init>()     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.String r2 = "http://www.systranet.com/sai?gui=text&service=translate&lp="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.String r2 = r9.fromLanguage     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x00b4 }
            r2 = 95
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.String r2 = r9.toLanguage     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x00b4 }
            java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x00b4 }
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x00b4 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x00cf }
            r0.setDoOutput(r3)
            r0.setDoInput(r3)
            r0.setUseCaches(r7)
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6"
            r0.setRequestProperty(r1, r2)
            java.lang.String r1 = "Referer"
            java.lang.String r2 = "http://www.systranet.com/sai"
            r0.setRequestProperty(r1, r2)
            java.lang.String r1 = "Content-Type"
            java.lang.String r1 = "application/x-www-form-urlencoded; charset=UTF-8"
            r0.setRequestProperty(r8, r1)
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x00ea }
            java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ IOException -> 0x00ea }
            java.lang.String r3 = "UTF8"
            r1.<init>(r2, r3)     // Catch:{ IOException -> 0x00ea }
            r1.write(r10)     // Catch:{ IOException -> 0x0105 }
            r1.flush()     // Catch:{ IOException -> 0x0105 }
            r1.close()     // Catch:{ IOException -> 0x0121 }
        L_0x0065:
            java.lang.String r1 = "Content-Type"
            java.lang.String r1 = r0.getHeaderField(r8)
            if (r1 == 0) goto L_0x01c2
            java.lang.String r2 = "charset="
            int r2 = r1.indexOf(r2)
            if (r2 == r6) goto L_0x01c2
            int r2 = r2 + 8
            java.lang.String r1 = r1.substring(r2)
            java.lang.String r1 = r1.trim()
            int r2 = r1.length()
            if (r2 != 0) goto L_0x0086
            r1 = r5
        L_0x0086:
            if (r1 != 0) goto L_0x008a
            java.lang.String r1 = "UTF8"
        L_0x008a:
            if (r1 != 0) goto L_0x013c
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x014d }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x014d }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x014d }
            r2.<init>(r0)     // Catch:{ IOException -> 0x014d }
            r1.<init>(r2)     // Catch:{ IOException -> 0x014d }
            r0 = r1
        L_0x009b:
            r1 = 1024(0x400, float:1.435E-42)
            char[] r1 = new char[r1]     // Catch:{ IOException -> 0x0169 }
            r2 = r5
        L_0x00a0:
            int r3 = r0.read(r1)     // Catch:{ IOException -> 0x01bb }
            if (r3 == r6) goto L_0x018f
            if (r2 != 0) goto L_0x01be
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x01bb }
            r4.<init>()     // Catch:{ IOException -> 0x01bb }
            r2 = 6
        L_0x00ae:
            int r3 = r3 - r2
            r4.append(r1, r2, r3)     // Catch:{ IOException -> 0x01b8 }
            r2 = r4
            goto L_0x00a0
        L_0x00b4:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Malformed URL exception: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r5
        L_0x00ce:
            return r0
        L_0x00cf:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not open connection. "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r5
            goto L_0x00ce
        L_0x00ea:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not get output stream "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r5
            goto L_0x00ce
        L_0x0105:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not write data to output stream "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            java.lang.String r0 = ""
            goto L_0x00ce
        L_0x0121:
            r1 = move-exception
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Could not close output stream.  Now that's weird. "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.println(r1)
            goto L_0x0065
        L_0x013c:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x014d }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x014d }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x014d }
            r3.<init>(r0, r1)     // Catch:{ IOException -> 0x014d }
            r2.<init>(r3)     // Catch:{ IOException -> 0x014d }
            r0 = r2
            goto L_0x009b
        L_0x014d:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not get input stream "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            r0 = r5
            goto L_0x00ce
        L_0x0169:
            r0 = move-exception
            r1 = r5
        L_0x016b:
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Something went wrong in the readLine "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            r2.println(r0)
            if (r1 != 0) goto L_0x0188
            r0 = r5
            goto L_0x00ce
        L_0x0188:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
            goto L_0x00ce
        L_0x018f:
            r0.close()     // Catch:{ IOException -> 0x0197 }
        L_0x0192:
            if (r2 != 0) goto L_0x01b1
            r0 = r5
            goto L_0x00ce
        L_0x0197:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.err
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Could not close input stream.  Now that's weird. "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
            goto L_0x0192
        L_0x01b1:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r2)
            goto L_0x00ce
        L_0x01b8:
            r0 = move-exception
            r1 = r4
            goto L_0x016b
        L_0x01bb:
            r0 = move-exception
            r1 = r2
            goto L_0x016b
        L_0x01be:
            r4 = r2
            r2 = r7
            goto L_0x00ae
        L_0x01c2:
            r1 = r5
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.text.Translator.translateSystranAjax(java.lang.String):java.lang.String");
    }
}
