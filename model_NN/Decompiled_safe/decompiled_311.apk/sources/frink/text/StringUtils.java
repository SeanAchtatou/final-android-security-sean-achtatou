package frink.text;

public class StringUtils {
    public static String replace(String str, char c, String str2) {
        int i = 0;
        int indexOf = str.indexOf(c);
        if (indexOf == -1) {
            return str;
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int i2 = indexOf;
            int i3 = i;
            int i4 = i2;
            stringBuffer.append(str.substring(i3, i4));
            stringBuffer.append(str2);
            i = i4 + 1;
            if (i >= length || (indexOf = str.indexOf(c, i)) == -1) {
            }
        }
        if (i <= length - 1) {
            stringBuffer.append(str.substring(i));
        }
        return new String(stringBuffer);
    }

    public static String safeTrim(String str) {
        if (str == null) {
            return null;
        }
        return str.trim();
    }

    public static String repeat(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer(str.length() * i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(str);
        }
        return new String(stringBuffer);
    }

    public static String repeat(char c, int i) {
        StringBuffer stringBuffer = new StringBuffer(i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(c);
        }
        return new String(stringBuffer);
    }

    public static String padLeft(String str, int i, char c) {
        int length = str.length();
        if (length >= i) {
            return str;
        }
        return repeat(c, i - length) + str;
    }

    public static String padRight(String str, int i, char c) {
        int length = str.length();
        if (length >= i) {
            return str;
        }
        return str + repeat(c, i - length);
    }
}
