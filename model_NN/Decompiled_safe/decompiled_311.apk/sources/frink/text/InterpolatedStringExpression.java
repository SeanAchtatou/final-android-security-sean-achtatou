package frink.text;

import frink.expr.BasicStringExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.TerminalExpression;
import frink.symbolic.MatchingContext;

public class InterpolatedStringExpression extends TerminalExpression {
    public static final String TYPE = "InterpolatedString";
    private String preString;

    private InterpolatedStringExpression(String str) {
        this.preString = str;
    }

    public static Expression create(String str) {
        if (StringInterpolator.requiresInterpolation(str)) {
            return new InterpolatedStringExpression(str);
        }
        return new BasicStringExpression(str);
    }

    public Expression evaluate(Environment environment) {
        return new BasicStringExpression(StringInterpolator.interpolate(this.preString, environment));
    }

    public boolean isConstant() {
        return false;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof InterpolatedStringExpression) || !((InterpolatedStringExpression) expression).preString.equals(this.preString)) {
            return false;
        }
        return true;
    }

    public String getExpressionType() {
        return TYPE;
    }
}
