package frink.text;

public class SGMLUtils {
    public static String XMLEncode(String str) {
        if (str == null) {
            return "";
        }
        return StringUtils.replace(StringUtils.replace(StringUtils.replace(StringUtils.replace(str, '&', "&amp;"), '<', "&lt;"), '>', "&gt;"), '\"', "&quot;");
    }

    public static String emptyIf(int i, int i2) {
        if (i == i2) {
            return "";
        }
        return Integer.toString(i);
    }

    public static String replaceNewlines(String str) {
        return StringUtils.replace(str, 10, "<BR>");
    }
}
