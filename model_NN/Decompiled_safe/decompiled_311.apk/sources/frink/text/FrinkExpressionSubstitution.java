package frink.text;

import frink.expr.BasicContextFrame;
import frink.expr.BasicStringExpression;
import frink.expr.CannotAssignException;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Substitution;

public class FrinkExpressionSubstitution implements Substitution {
    private Environment env;
    private Expression parsedProgram = null;
    private String programString;

    public FrinkExpressionSubstitution(String str) {
        this.programString = str;
    }

    public void setEnvironment(Environment environment) {
        this.env = environment;
    }

    public void appendSubstitution(StringBuffer stringBuffer, MatchResult matchResult, int i, PatternMatcherInput patternMatcherInput, PatternMatcher patternMatcher, Pattern pattern) {
        int i2 = 0;
        int groups = matchResult.groups();
        BasicContextFrame basicContextFrame = new BasicContextFrame();
        while (i2 < groups) {
            try {
                basicContextFrame.setSymbolDefinition(((char) StringInterpolator.INDICATOR) + Integer.toString(i2), new BasicStringExpression(matchResult.group(i2)), this.env);
                i2++;
            } catch (CannotAssignException e) {
                this.env.outputln("FrinkSubstitutionExpression: " + e);
            } catch (FrinkSecurityException e2) {
                this.env.outputln("FrinkSubstitutionExpression: " + e2);
            }
        }
        try {
            if (this.parsedProgram == null) {
                this.parsedProgram = this.env.eval(this.programString);
            }
        } catch (Exception e3) {
        }
        try {
            this.env.addContextFrame(basicContextFrame, false);
            stringBuffer.append(this.env.format(this.parsedProgram.evaluate(this.env)));
        } catch (EvaluationException e4) {
            this.env.outputln("FrinkExpressionSubstitution: " + e4);
        } finally {
            this.env.removeContextFrame();
        }
    }
}
