package frink.text;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.RegexpExpression;
import frink.expr.TerminalExpression;
import frink.symbolic.MatchingContext;
import org.apache.oro.text.PatternCacheLRU;
import org.apache.oro.text.regex.Pattern;

public class BasicRegexpExpression extends TerminalExpression implements RegexpExpression {
    private static PatternCacheLRU cache = new PatternCacheLRU(100);
    private boolean multiple = false;
    private Pattern pat;

    public BasicRegexpExpression(String str) {
        this.pat = cache.getPattern(str);
    }

    public BasicRegexpExpression(String str, String str2) {
        this.pat = cache.getPattern(str, parseModifiers(str2));
    }

    public static Pattern getPattern(String str) {
        return cache.getPattern(str);
    }

    public Pattern getPattern() {
        return this.pat;
    }

    public boolean isMultiple() {
        return this.multiple;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public boolean isConstant() {
        return true;
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        return this == expression;
    }

    private int parseModifiers(String str) {
        int length = str.length();
        int i = 32768;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            switch (charAt) {
                case 'g':
                    this.multiple = true;
                    break;
                case 'i':
                    i |= 1;
                    break;
                case 'm':
                    i |= 8;
                    break;
                case 's':
                    i |= 16;
                    break;
                case 'x':
                    i |= 32;
                    break;
                default:
                    System.out.println("Unknown regex modifier " + charAt);
                    break;
            }
        }
        return i;
    }

    public String getExpressionType() {
        return RegexpExpression.TYPE;
    }
}
