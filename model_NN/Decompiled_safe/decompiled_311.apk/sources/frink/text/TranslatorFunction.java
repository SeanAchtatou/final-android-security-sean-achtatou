package frink.text;

import frink.function.StringFunction;

public class TranslatorFunction extends StringFunction {
    private Translator translator;

    public TranslatorFunction(String str, String str2) {
        super(true);
        this.translator = new Translator(str, str2);
    }

    /* access modifiers changed from: protected */
    public String doFunction(String str) {
        return this.translator.translate(str);
    }
}
