package frink.text;

import frink.expr.BasicContextFrame;
import frink.expr.CannotAssignException;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.FrinkSecurityException;
import frink.parser.BasicEnvironment;

public class StringInterpolator {
    public static final char END_BRACKET = '}';
    public static final char INDICATOR = '$';
    public static final char START_BRACKET = '{';

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x006e, code lost:
        r2 = r11.substring(r2, r4);
        r3 = r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String interpolate(java.lang.String r11, frink.expr.Environment r12) {
        /*
            r9 = 0
            r8 = 0
            r7 = 36
            int r0 = r11.length()
            r1 = r8
            r2 = r9
        L_0x000a:
            int r3 = r11.indexOf(r7, r2)
            r4 = -1
            if (r3 == r4) goto L_0x00b7
            if (r1 != 0) goto L_0x0037
            if (r3 <= 0) goto L_0x0031
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = r11.substring(r9, r3)
            r1.<init>(r2)
        L_0x001e:
            r2 = 2
            int r2 = r0 - r2
            if (r3 > r2) goto L_0x00b0
            int r2 = r3 + 1
            char r2 = r11.charAt(r2)
            if (r2 != r7) goto L_0x0041
            r1.append(r7)
            int r2 = r3 + 2
            goto L_0x000a
        L_0x0031:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            goto L_0x001e
        L_0x0037:
            if (r3 <= r2) goto L_0x001e
            java.lang.String r2 = r11.substring(r2, r3)
            r1.append(r2)
            goto L_0x001e
        L_0x0041:
            r4 = 123(0x7b, float:1.72E-43)
            if (r2 != r4) goto L_0x00cd
            int r3 = r3 + 1
            r4 = 1
            r10 = r4
            r4 = r3
            r3 = r10
        L_0x004b:
            if (r3 != 0) goto L_0x0053
            boolean r2 = java.lang.Character.isLetter(r2)
            if (r2 == 0) goto L_0x00a9
        L_0x0053:
            int r2 = r4 + 1
            r4 = r2
        L_0x0056:
            if (r4 >= r0) goto L_0x00ca
            char r5 = r11.charAt(r4)
            if (r4 != r2) goto L_0x008e
        L_0x005e:
            r6 = 95
            if (r5 == r6) goto L_0x009b
            boolean r6 = java.lang.Character.isSpaceChar(r5)
            if (r6 != 0) goto L_0x006e
            boolean r5 = java.lang.Character.isLetterOrDigit(r5)
            if (r5 != 0) goto L_0x009b
        L_0x006e:
            java.lang.String r2 = r11.substring(r2, r4)
            r3 = r4
        L_0x0073:
            if (r2 != 0) goto L_0x007a
            java.lang.String r2 = r11.substring(r3)
            r3 = r0
        L_0x007a:
            r4 = 0
            frink.expr.SymbolDefinition r2 = r12.getSymbolDefinition(r2, r4)     // Catch:{ CannotAssignException -> 0x00a3, FrinkSecurityException -> 0x00a6 }
            if (r2 != 0) goto L_0x009e
            r2 = r8
        L_0x0082:
            if (r2 == 0) goto L_0x008b
            java.lang.String r2 = r12.format(r2)
            r1.append(r2)
        L_0x008b:
            r2 = r3
            goto L_0x000a
        L_0x008e:
            if (r3 == 0) goto L_0x005e
            r6 = 125(0x7d, float:1.75E-43)
            if (r5 != r6) goto L_0x005e
            java.lang.String r2 = r11.substring(r2, r4)
            int r3 = r4 + 1
            goto L_0x0073
        L_0x009b:
            int r4 = r4 + 1
            goto L_0x0056
        L_0x009e:
            frink.expr.Expression r2 = r2.getValue()     // Catch:{ CannotAssignException -> 0x00a3, FrinkSecurityException -> 0x00a6 }
            goto L_0x0082
        L_0x00a3:
            r2 = move-exception
            r2 = r8
            goto L_0x0082
        L_0x00a6:
            r2 = move-exception
            r2 = r8
            goto L_0x0082
        L_0x00a9:
            r1.append(r7)
            int r2 = r4 + 1
            goto L_0x000a
        L_0x00b0:
            r1.append(r7)
            int r2 = r3 + 1
            goto L_0x000a
        L_0x00b7:
            if (r1 != 0) goto L_0x00bb
            r0 = r11
        L_0x00ba:
            return r0
        L_0x00bb:
            if (r2 >= r0) goto L_0x00c4
            java.lang.String r0 = r11.substring(r2)
            r1.append(r0)
        L_0x00c4:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
            goto L_0x00ba
        L_0x00ca:
            r3 = r2
            r2 = r8
            goto L_0x0073
        L_0x00cd:
            r4 = r3
            r3 = r9
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.text.StringInterpolator.interpolate(java.lang.String, frink.expr.Environment):java.lang.String");
    }

    public static boolean requiresInterpolation(String str) {
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(36, i);
            if (indexOf == -1) {
                return false;
            }
            if (indexOf > str.length() - 2) {
                return false;
            }
            char charAt = str.charAt(indexOf + 1);
            if (charAt == '$') {
                return true;
            }
            if (charAt != '{' && !Character.isLetter(charAt)) {
                i = indexOf + 2;
            }
        }
        return true;
    }

    public static void main(String[] strArr) {
        BasicEnvironment basicEnvironment = new BasicEnvironment();
        basicEnvironment.addContextFrame(new BasicContextFrame(), false);
        try {
            basicEnvironment.setSymbolDefinition("one", DimensionlessUnitExpression.ONE);
            basicEnvironment.setSymbolDefinition("two", DimensionlessUnitExpression.TWO);
            basicEnvironment.setSymbolDefinition("three", DimensionlessUnitExpression.THREE);
        } catch (CannotAssignException | FrinkSecurityException e) {
        }
        System.out.println(interpolate("Escaped: $$one", basicEnvironment));
        System.out.println(interpolate("Unescaped: $one", basicEnvironment));
        System.out.println(interpolate("Unescaped: $1.99", basicEnvironment));
        System.out.println(interpolate("Escaped before num: $$1.99", basicEnvironment));
        System.out.println(interpolate("Escaped before var: $$one", basicEnvironment));
        System.out.println(interpolate("", basicEnvironment));
        System.out.println(interpolate("nothing", basicEnvironment));
        System.out.println(interpolate("$one", basicEnvironment));
        System.out.println(interpolate("one: $one", basicEnvironment));
        System.out.println(interpolate("bracketed: ${one}two", basicEnvironment));
        System.out.println(interpolate("bracketed: ${one}", basicEnvironment));
        System.out.println(interpolate("$one $two", basicEnvironment));
        System.out.println(interpolate("$one$two$three", basicEnvironment));
        System.out.println(interpolate("one: $one two: $two", basicEnvironment));
        System.out.println(interpolate("one: $one two: $two nothree", basicEnvironment));
        System.out.println(interpolate("one:$one two:$two three:$three", basicEnvironment));
        System.out.println(interpolate("Bare dollar: $", basicEnvironment));
        System.out.println(interpolate("Dollars at end: $$", basicEnvironment));
        System.out.println(interpolate("Dollar Space: $ ", basicEnvironment));
        System.out.println(interpolate("$undefined", basicEnvironment));
    }
}
