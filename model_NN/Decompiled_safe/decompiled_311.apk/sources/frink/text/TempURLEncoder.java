package frink.text;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.BitSet;

public class TempURLEncoder {
    static final int caseDiff = 32;
    static String dfltEncName;
    static BitSet dontNeedEncoding = new BitSet(256);

    static {
        dfltEncName = null;
        for (int i = 97; i <= 122; i++) {
            dontNeedEncoding.set(i);
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            dontNeedEncoding.set(i2);
        }
        for (int i3 = 48; i3 <= 57; i3++) {
            dontNeedEncoding.set(i3);
        }
        dontNeedEncoding.set(32);
        dontNeedEncoding.set(45);
        dontNeedEncoding.set(95);
        dontNeedEncoding.set(46);
        dontNeedEncoding.set(42);
        dfltEncName = System.getProperty("file.encoding");
    }

    private TempURLEncoder() {
    }

    public static String encode(String str) {
        try {
            return encode(str, dfltEncName);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String encode(String str, String str2) throws UnsupportedEncodingException {
        char charAt;
        char c;
        StringBuffer stringBuffer = new StringBuffer(str.length());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(10);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(byteArrayOutputStream, str2));
        boolean z = false;
        boolean z2 = false;
        int i = 0;
        while (i < str.length()) {
            char charAt2 = str.charAt(i);
            if (dontNeedEncoding.get(charAt2)) {
                if (charAt2 == ' ') {
                    c = '+';
                    z2 = true;
                } else {
                    c = charAt2;
                }
                stringBuffer.append((char) c);
                z = true;
            } else {
                if (z) {
                    try {
                        bufferedWriter = new BufferedWriter(new OutputStreamWriter(byteArrayOutputStream, str2));
                        z = false;
                    } catch (IOException e) {
                        byteArrayOutputStream.reset();
                    }
                }
                bufferedWriter.write(charAt2);
                if (charAt2 >= 55296 && charAt2 <= 56319 && i + 1 < str.length() && (charAt = str.charAt(i + 1)) >= 56320 && charAt <= 57343) {
                    bufferedWriter.write(charAt);
                    i++;
                }
                bufferedWriter.flush();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                for (int i2 = 0; i2 < byteArray.length; i2++) {
                    stringBuffer.append('%');
                    char forDigit = Character.forDigit((byteArray[i2] >> 4) & 15, 16);
                    if (Character.isLetter(forDigit)) {
                        forDigit = (char) (forDigit - ' ');
                    }
                    stringBuffer.append(forDigit);
                    char forDigit2 = Character.forDigit(byteArray[i2] & 15, 16);
                    if (Character.isLetter(forDigit2)) {
                        forDigit2 = (char) (forDigit2 - ' ');
                    }
                    stringBuffer.append(forDigit2);
                }
                byteArrayOutputStream.reset();
                z2 = true;
            }
            i++;
        }
        return z2 ? stringBuffer.toString() : str;
    }
}
