package frink.text;

import java.lang.reflect.Array;

public class EditDistance {
    private static int LD(String str, String str2) {
        int i;
        int length = str.length();
        int length2 = str2.length();
        if (length == 0) {
            return length2;
        }
        if (length2 == 0) {
            return length;
        }
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, length + 1, length2 + 1);
        for (int i2 = 0; i2 <= length; i2++) {
            iArr[i2][0] = i2;
        }
        for (int i3 = 0; i3 <= length2; i3++) {
            iArr[0][i3] = i3;
        }
        for (int i4 = 1; i4 <= length; i4++) {
            char charAt = str.charAt(i4 - 1);
            for (int i5 = 1; i5 <= length2; i5++) {
                if (charAt == str2.charAt(i5 - 1)) {
                    i = 0;
                } else {
                    i = 1;
                }
                iArr[i4][i5] = minimum(iArr[i4 - 1][i5] + 1, iArr[i4][i5 - 1] + 1, i + iArr[i4 - 1][i5 - 1]);
            }
        }
        return iArr[length][length2];
    }

    public static int getEditDistance(String str, String str2) {
        String str3;
        int i;
        String str4;
        int i2;
        int length = str.length();
        int length2 = str2.length();
        if (length == 0) {
            return length2;
        }
        if (length2 == 0) {
            return length;
        }
        if (length < length2) {
            str3 = str2;
            i = length;
            length = length2;
            str4 = str;
        } else {
            str3 = str;
            String str5 = str2;
            i = length2;
            str4 = str5;
        }
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 2, i + 1);
        for (int i3 = 0; i3 <= i; i3++) {
            iArr[0][i3] = i3;
        }
        int i4 = 1;
        int i5 = 0;
        int i6 = 1;
        while (i4 <= length) {
            iArr[i6][0] = i4;
            char charAt = str3.charAt(i4 - 1);
            for (int i7 = 1; i7 <= i; i7++) {
                int i8 = i7 - 1;
                if (charAt == str4.charAt(i8)) {
                    i2 = 0;
                } else {
                    i2 = 1;
                }
                iArr[i6][i7] = minimum(iArr[i5][i7] + 1, iArr[i6][i8] + 1, iArr[i5][i8] + i2);
            }
            i4++;
            int i9 = i5;
            i5 = 1 - i5;
            i6 = i9;
        }
        return iArr[i5][i];
    }

    private static int minimum(int i, int i2, int i3) {
        int i4;
        if (i2 < i) {
            i4 = i2;
        } else {
            i4 = i;
        }
        if (i3 < i4) {
            return i3;
        }
        return i4;
    }

    public static void main(String[] strArr) {
        System.out.println(LD(strArr[0], strArr[1]));
        System.out.println(getEditDistance(strArr[0], strArr[1]));
    }
}
