package frink.format;

import frink.expr.Environment;
import frink.expr.Expression;

public interface ExpressionFormatter {
    String format(Expression expression, Environment environment, boolean z);

    String getDescription();

    String getName();
}
