package frink.format;

import frink.expr.ConverterExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.UnitExpression;
import frink.numeric.NumericException;

public class PreferredExpressionFormatter extends BasicExpressionFormatter {
    public String formatUnit(UnitExpression unitExpression, Environment environment, boolean z) {
        Expression formatter = environment.getUnitFormatterManager().getFormatter(unitExpression.getUnit().getDimensionList());
        if (formatter != null) {
            try {
                String convertPart = ConverterExpression.convertPart(unitExpression, formatter, environment);
                if (convertPart != null) {
                    return convertPart;
                }
                environment.outputln("Warning:  Error in default converter--returned null.");
            } catch (EvaluationException e) {
                environment.outputln("Warning:  Error in default converter.");
            } catch (NumericException e2) {
                environment.outputln("Warning:  Error in default converter:\n " + e2);
            }
        }
        return super.formatUnit(unitExpression, environment, z);
    }
}
