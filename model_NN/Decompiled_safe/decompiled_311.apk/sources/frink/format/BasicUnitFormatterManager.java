package frink.format;

import frink.errors.ConformanceException;
import frink.errors.NotAnIntegerException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.StringExpression;
import frink.expr.UnitExpression;
import frink.function.BuiltinFunctionSource;
import frink.units.DimensionList;
import frink.units.DimensionListMath;
import frink.units.UnitMath;
import java.util.Vector;

public class BasicUnitFormatterManager implements UnitFormatterManager {
    private Vector<Pair> dimensionMap = new Vector<>();

    public Expression getFormatter(DimensionList dimensionList) {
        int size = this.dimensionMap.size();
        for (int i = 0; i < size; i++) {
            Pair elementAt = this.dimensionMap.elementAt(i);
            if (DimensionListMath.equals(dimensionList, elementAt.getDimensionList())) {
                return elementAt.getExpression();
            }
        }
        return null;
    }

    public void setFormatter(DimensionList dimensionList, Expression expression, Environment environment) throws ConformanceException {
        int size = this.dimensionMap.size();
        testConformance(dimensionList, expression, environment);
        for (int i = 0; i < size; i++) {
            if (DimensionListMath.equals(dimensionList, this.dimensionMap.elementAt(i).getDimensionList())) {
                this.dimensionMap.setElementAt(new Pair(dimensionList, expression), i);
                return;
            }
        }
        this.dimensionMap.addElement(new Pair(dimensionList, expression));
    }

    private void testConformance(DimensionList dimensionList, Expression expression, Environment environment) throws ConformanceException {
        if (expression instanceof UnitExpression) {
            if (!DimensionListMath.equals(dimensionList, ((UnitExpression) expression).getUnit().getDimensionList())) {
                throw new ConformanceException("Formatting error: unit does not have appropriate dimensions for formatter.", dimensionList);
            }
        } else if (expression instanceof StringExpression) {
            String string = ((StringExpression) expression).getString();
            try {
                if (!DimensionListMath.equals(dimensionList, BuiltinFunctionSource.getUnitValue(environment.eval(string).evaluate(environment)).getDimensionList())) {
                    throw new ConformanceException("Formatting error: unit \"" + string + "\" does not have appropriate dimensions.", dimensionList);
                }
            } catch (Exception e) {
                environment.outputln("BasicUnitFormatterManager:  Warning: cannot find unit '" + string + "' -- you may get errors when displaying units of this type later.");
            }
        } else if (expression instanceof ListExpression) {
            ListExpression listExpression = (ListExpression) expression;
            int childCount = listExpression.getChildCount();
            for (int i = 0; i < childCount; i++) {
                try {
                    testConformance(dimensionList, listExpression.getChild(i), environment);
                } catch (ConformanceException e2) {
                    ConformanceException conformanceException = e2;
                    if (i == 0 || i == childCount - 1) {
                        Expression child = listExpression.getChild(i);
                        if (child instanceof UnitExpression) {
                            try {
                                if (UnitMath.getIntegerValue(((UnitExpression) child).getUnit()) == 0) {
                                }
                            } catch (NotAnIntegerException e3) {
                            }
                        }
                    }
                    throw conformanceException;
                } catch (InvalidChildException e4) {
                    environment.outputln("BasicUnitFormatterManager:  Weird InvalidChildException: " + e4);
                }
            }
        }
    }

    private static class Pair {
        private DimensionList dimensionList;
        private Expression expr;

        public Pair(DimensionList dimensionList2, Expression expression) {
            this.dimensionList = dimensionList2;
            this.expr = expression;
        }

        public DimensionList getDimensionList() {
            return this.dimensionList;
        }

        public Expression getExpression() {
            return this.expr;
        }
    }
}
