package frink.format;

import frink.expr.BooleanExpression;
import frink.expr.DateExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.GraphicsExpression;
import frink.expr.ListExpression;
import frink.expr.OperatorExpression;
import frink.expr.SelfDisplayingExpression;
import frink.expr.StringExpression;
import frink.expr.SymbolExpression;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.VoidExpression;
import frink.function.FunctionCallExpression;

public abstract class AbstractExpressionFormatter implements ExpressionFormatter, DetailedExpressionFormatter {
    public abstract String formatBoolean(BooleanExpression booleanExpression, Environment environment, boolean z);

    public abstract String formatDate(DateExpression dateExpression, Environment environment, boolean z);

    public abstract String formatEnumerating(EnumeratingExpression enumeratingExpression, Environment environment, boolean z);

    public abstract String formatFunctionCall(FunctionCallExpression functionCallExpression, Environment environment, boolean z);

    public abstract String formatGraphics(GraphicsExpression graphicsExpression, Environment environment, boolean z);

    public abstract String formatList(ListExpression listExpression, Environment environment, boolean z);

    public abstract String formatOperator(OperatorExpression operatorExpression, Environment environment, boolean z);

    public abstract String formatString(StringExpression stringExpression, Environment environment, boolean z);

    public abstract String formatSymbol(SymbolExpression symbolExpression, Environment environment, boolean z);

    public abstract String formatUndef(UndefExpression undefExpression, Environment environment, boolean z);

    public abstract String formatUnit(UnitExpression unitExpression, Environment environment, boolean z);

    public abstract String formatVoid(VoidExpression voidExpression, Environment environment, boolean z);

    public String format(Expression expression, Environment environment, boolean z) {
        if (expression instanceof SelfDisplayingExpression) {
            return ((SelfDisplayingExpression) expression).toString(environment, z);
        }
        if (expression instanceof StringExpression) {
            return formatString((StringExpression) expression, environment, z);
        }
        if (expression instanceof UnitExpression) {
            return formatUnit((UnitExpression) expression, environment, z);
        }
        if (expression instanceof DateExpression) {
            return formatDate((DateExpression) expression, environment, z);
        }
        if (expression instanceof FunctionCallExpression) {
            return formatFunctionCall((FunctionCallExpression) expression, environment, z);
        }
        if (expression instanceof VoidExpression) {
            return formatVoid((VoidExpression) expression, environment, z);
        }
        if (expression instanceof UndefExpression) {
            return formatUndef((UndefExpression) expression, environment, z);
        }
        if (expression instanceof SymbolExpression) {
            return formatSymbol((SymbolExpression) expression, environment, z);
        }
        if (expression instanceof BooleanExpression) {
            return formatBoolean((BooleanExpression) expression, environment, z);
        }
        if (expression instanceof OperatorExpression) {
            return formatOperator((OperatorExpression) expression, environment, z);
        }
        if (expression instanceof EnumeratingExpression) {
            return formatEnumerating((EnumeratingExpression) expression, environment, z);
        }
        if (expression instanceof ListExpression) {
            return formatList((ListExpression) expression, environment, z);
        }
        if (expression instanceof GraphicsExpression) {
            return formatGraphics((GraphicsExpression) expression, environment, z);
        }
        return formatUnknown(expression, environment, z);
    }

    public String formatUnknown(Expression expression, Environment environment, boolean z) {
        if (expression == null) {
            environment.outputln("BasicExpressionFormatter.formatUnknown passed null expression!");
            return "BasicExpressionFormatter.formatUnknown passed null expression!";
        }
        StringBuffer stringBuffer = new StringBuffer(expression.getClass().getName() + "(");
        int childCount = expression.getChildCount();
        int i = 0;
        while (i < childCount) {
            if (i > 0) {
                stringBuffer.append(", ");
            }
            try {
                stringBuffer.append(format(expression.getChild(i), environment, true));
                i++;
            } catch (EvaluationException e) {
                environment.outputln("Error in BasicExpressionFormatter.formatUnknown");
                return "Error in BasicExpressionFormatter.formatUnknown " + e;
            }
        }
        stringBuffer.append(")");
        return new String(stringBuffer);
    }
}
