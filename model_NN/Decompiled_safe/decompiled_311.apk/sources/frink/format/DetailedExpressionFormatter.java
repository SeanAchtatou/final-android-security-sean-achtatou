package frink.format;

import frink.expr.BooleanExpression;
import frink.expr.DateExpression;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.GraphicsExpression;
import frink.expr.ListExpression;
import frink.expr.OperatorExpression;
import frink.expr.StringExpression;
import frink.expr.SymbolExpression;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.VoidExpression;
import frink.function.FunctionCallExpression;

public interface DetailedExpressionFormatter {
    String formatBoolean(BooleanExpression booleanExpression, Environment environment, boolean z);

    String formatDate(DateExpression dateExpression, Environment environment, boolean z);

    String formatEnumerating(EnumeratingExpression enumeratingExpression, Environment environment, boolean z);

    String formatFunctionCall(FunctionCallExpression functionCallExpression, Environment environment, boolean z);

    String formatGraphics(GraphicsExpression graphicsExpression, Environment environment, boolean z);

    String formatList(ListExpression listExpression, Environment environment, boolean z);

    String formatOperator(OperatorExpression operatorExpression, Environment environment, boolean z);

    String formatString(StringExpression stringExpression, Environment environment, boolean z);

    String formatSymbol(SymbolExpression symbolExpression, Environment environment, boolean z);

    String formatUndef(UndefExpression undefExpression, Environment environment, boolean z);

    String formatUnit(UnitExpression unitExpression, Environment environment, boolean z);

    String formatVoid(VoidExpression voidExpression, Environment environment, boolean z);
}
