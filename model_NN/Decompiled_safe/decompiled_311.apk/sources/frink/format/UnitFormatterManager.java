package frink.format;

import frink.errors.ConformanceException;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.units.DimensionList;

public interface UnitFormatterManager {
    Expression getFormatter(DimensionList dimensionList);

    void setFormatter(DimensionList dimensionList, Expression expression, Environment environment) throws ConformanceException;
}
