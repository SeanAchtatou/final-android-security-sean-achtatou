package frink.format;

import frink.expr.BooleanExpression;
import frink.expr.DateExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkBoolean;
import frink.expr.GraphicsExpression;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.OperatorExpression;
import frink.expr.StringExpression;
import frink.expr.SymbolExpression;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.VoidExpression;
import frink.function.BuiltinFunctionSource;
import frink.function.FunctionCallExpression;
import frink.units.Unit;
import frink.units.UnitMath;

public class BasicExpressionFormatter extends AbstractExpressionFormatter {
    private static String dimensionPostamble = ")";
    private static String dimensionPreamble = "(";
    private static boolean showDimensionName = true;
    private static boolean showUndefinedSymbols = true;

    public String getName() {
        return "Output";
    }

    public String getDescription() {
        return "Formats expressions in Frink's default output format.";
    }

    public String formatUnit(UnitExpression unitExpression, Environment environment, boolean z) {
        Unit unit = unitExpression.getUnit();
        StringBuffer stringBuffer = new StringBuffer(UnitMath.toString(unit, environment.getDimensionManager()));
        if (showDimensionName && !UnitMath.isDimensionless(unit)) {
            String name = environment.getDimensionListManager().getName(unit.getDimensionList());
            if (name != null) {
                stringBuffer.append(" " + dimensionPreamble + name + dimensionPostamble);
            } else {
                stringBuffer.append(" " + dimensionPreamble + "unknown unit type" + dimensionPostamble);
            }
        }
        return new String(stringBuffer);
    }

    public String formatDate(DateExpression dateExpression, Environment environment, boolean z) {
        return environment.getOutputDateFormatter().format(dateExpression.getFrinkDate(), null, environment);
    }

    public String formatFunctionCall(FunctionCallExpression functionCallExpression, Environment environment, boolean z) {
        StringBuffer stringBuffer = new StringBuffer(functionCallExpression.getName() + "[");
        int childCount = functionCallExpression.getChildCount();
        int i = 1;
        while (i < childCount) {
            if (i > 1) {
                stringBuffer.append(", ");
            }
            try {
                stringBuffer.append(format(functionCallExpression.getChild(i), environment, false));
                i++;
            } catch (InvalidChildException e) {
                environment.outputln("Bad child in BasicExpressionFormatter.formatFunctionCall");
                return "Error in BasicExpressionFormatter.formatFunctionCall";
            }
        }
        stringBuffer.append("]");
        return new String(stringBuffer);
    }

    public String formatString(StringExpression stringExpression, Environment environment, boolean z) {
        return stringExpression.getString();
    }

    public String formatVoid(VoidExpression voidExpression, Environment environment, boolean z) {
        return "";
    }

    public String formatUndef(UndefExpression undefExpression, Environment environment, boolean z) {
        return UndefExpression.TYPE;
    }

    public String formatSymbol(SymbolExpression symbolExpression, Environment environment, boolean z) {
        if (!z || !showUndefinedSymbols) {
            return symbolExpression.getName();
        }
        return symbolExpression.getName() + " (undefined symbol)";
    }

    public String formatBoolean(BooleanExpression booleanExpression, Environment environment, boolean z) {
        if (booleanExpression.equals(FrinkBoolean.TRUE)) {
            return "true";
        }
        if (booleanExpression.equals(FrinkBoolean.FALSE)) {
            return "false";
        }
        return "BooleanExpression is not true or false?!";
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String formatEnumerating(frink.expr.EnumeratingExpression r7, frink.expr.Environment r8, boolean r9) {
        /*
            r6 = this;
            r3 = 0
            r0 = 0
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "["
            r1.<init>(r2)
            frink.expr.FrinkEnumeration r0 = r7.getEnumeration(r8)     // Catch:{ EvaluationException -> 0x0037, all -> 0x0059 }
            r2 = r3
        L_0x000e:
            frink.expr.Expression r3 = r0.getNext(r8)     // Catch:{ EvaluationException -> 0x006a, all -> 0x0063 }
            if (r3 == 0) goto L_0x0025
            if (r2 <= 0) goto L_0x001b
            java.lang.String r4 = ", "
            r1.append(r4)     // Catch:{ EvaluationException -> 0x006a, all -> 0x0063 }
        L_0x001b:
            int r2 = r2 + 1
            r4 = 0
            java.lang.String r4 = r6.format(r3, r8, r4)     // Catch:{ EvaluationException -> 0x006a, all -> 0x0063 }
            r1.append(r4)     // Catch:{ EvaluationException -> 0x006a, all -> 0x0063 }
        L_0x0025:
            if (r3 != 0) goto L_0x000e
            if (r0 == 0) goto L_0x002c
            r0.dispose()
        L_0x002c:
            java.lang.String r0 = "]"
            r1.append(r0)
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
        L_0x0036:
            return r0
        L_0x0037:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x003b:
            java.lang.String r2 = "Error in BasicExpressionFormatter.formatEnumerating in evaluation"
            r8.outputln(r2)     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r2.<init>()     // Catch:{ all -> 0x0068 }
            java.lang.String r3 = "Error in BasicExpressionFormatter.formatEnumerating in evaluation "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0068 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0068 }
            if (r1 == 0) goto L_0x0036
            r1.dispose()
            goto L_0x0036
        L_0x0059:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x005d:
            if (r1 == 0) goto L_0x0062
            r1.dispose()
        L_0x0062:
            throw r0
        L_0x0063:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005d
        L_0x0068:
            r0 = move-exception
            goto L_0x005d
        L_0x006a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.format.BasicExpressionFormatter.formatEnumerating(frink.expr.EnumeratingExpression, frink.expr.Environment, boolean):java.lang.String");
    }

    public String formatList(ListExpression listExpression, Environment environment, boolean z) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer("[");
        int childCount = listExpression.getChildCount();
        while (i < childCount) {
            if (i > 0) {
                stringBuffer.append(", ");
            }
            try {
                stringBuffer.append(format(listExpression.getChild(i), environment, false));
                i++;
            } catch (EvaluationException e) {
                environment.outputln("Error in BasicExpressionFormatter.formatList");
                return "Error in BasicExpressionFormatter.formatList " + e;
            }
        }
        stringBuffer.append("]");
        return new String(stringBuffer);
    }

    public String formatOperator(OperatorExpression operatorExpression, Environment environment, boolean z) {
        int i;
        boolean z2;
        String symbol = operatorExpression.getSymbol();
        StringBuffer stringBuffer = new StringBuffer();
        int childCount = operatorExpression.getChildCount();
        if (childCount == 1 && operatorExpression.getAssociativity() == 1) {
            stringBuffer.append(symbol);
        }
        int i2 = 0;
        while (i2 < childCount) {
            if (i2 > 0) {
                stringBuffer.append(symbol);
            }
            try {
                Expression child = operatorExpression.getChild(i2);
                if (child instanceof OperatorExpression) {
                    i = ((OperatorExpression) child).getPrecedence();
                    z2 = true;
                } else {
                    i = Integer.MIN_VALUE;
                    z2 = false;
                }
                if (BuiltinFunctionSource.isRational(child)) {
                    i = 500;
                }
                if (i2 == 0 && BuiltinFunctionSource.isNegativeUnit(child)) {
                    i = 500;
                }
                if ((!z2 || operatorExpression.getAssociativity() == -1) && (i == Integer.MIN_VALUE || operatorExpression.getPrecedence() <= i)) {
                    stringBuffer.append(format(child, environment, z));
                } else {
                    stringBuffer.append("(" + format(child, environment, z) + ")");
                }
                i2++;
            } catch (EvaluationException e) {
                environment.outputln("Error in BasicExpressionFormatter.formatOperator");
                return "Error in BasicExpressionFormatter.formatOperator " + e;
            }
        }
        if (childCount == 1 && operatorExpression.getAssociativity() == -1) {
            stringBuffer.append(symbol);
        }
        return new String(stringBuffer);
    }

    public String formatGraphics(GraphicsExpression graphicsExpression, Environment environment, boolean z) {
        return "-Graphics Expression: use its .show[] method to display when finished.-";
    }

    public static void setShowUndefined(boolean z) {
        showUndefinedSymbols = z;
    }

    public static void setShowDimensionName(boolean z) {
        showDimensionName = z;
    }

    public static void setDimensionNameDelimiters(String str, String str2) {
        dimensionPreamble = str;
        dimensionPostamble = str2;
    }
}
