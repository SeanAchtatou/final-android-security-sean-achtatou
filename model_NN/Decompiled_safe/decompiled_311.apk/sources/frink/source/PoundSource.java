package frink.source;

import frink.numeric.FrinkInt;
import frink.numeric.FrinkRational;
import frink.numeric.FrinkReal;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.units.BasicUnit;
import frink.units.DimensionList;
import frink.units.Source;
import frink.units.Unit;
import java.util.Enumeration;
import java.util.Hashtable;

public class PoundSource implements Source<Unit> {
    private static final boolean DEBUG = false;
    private String MARKER = " is worth:<br><br><p><b>&pound;";
    private Hashtable<String, BasicUnit> finalHash;
    private Hashtable<String, Numeric> multHashPost1970;
    private Hashtable<String, Numeric> multHashPre1970;
    /* access modifiers changed from: private */
    public Unit pound;
    private Hashtable<Integer, PoundUnit> yearHash;

    public PoundSource(Unit unit) {
        this.pound = unit;
        this.yearHash = new Hashtable<>();
        this.multHashPre1970 = new Hashtable<>();
        this.multHashPost1970 = new Hashtable<>();
        this.finalHash = new Hashtable<>();
        initMultHashes();
    }

    public void initMultHashes() {
        this.multHashPre1970.put("pound", FrinkInt.ONE);
        this.multHashPre1970.put("pounds", FrinkInt.ONE);
        this.multHashPre1970.put("GBP", FrinkInt.ONE);
        this.multHashPre1970.put("sovereign", FrinkInt.ONE);
        this.multHashPre1970.put("sovereigns", FrinkInt.ONE);
        try {
            FrinkReal construct = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(20));
            this.multHashPre1970.put("shilling", construct);
            this.multHashPre1970.put("shillings", construct);
            FrinkReal construct2 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(240));
            this.multHashPre1970.put("penny", construct2);
            this.multHashPre1970.put("pence", construct2);
            FrinkReal construct3 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(960));
            this.multHashPre1970.put("farthing", construct3);
            this.multHashPre1970.put("farthings", construct3);
            FrinkReal construct4 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(60));
            this.multHashPre1970.put("groat", construct4);
            this.multHashPre1970.put("groats", construct4);
            FrinkReal construct5 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(10));
            this.multHashPre1970.put("florin", construct5);
            this.multHashPre1970.put("florins", construct5);
            FrinkReal construct6 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(3));
            this.multHashPre1970.put("noble", construct6);
            this.multHashPre1970.put("nobles", construct6);
            FrinkReal construct7 = FrinkRational.construct(FrinkInt.construct(162), FrinkInt.construct(240));
            this.multHashPre1970.put("merk", construct7);
            this.multHashPre1970.put("merks", construct7);
            FrinkReal construct8 = FrinkRational.construct(FrinkInt.construct(160), FrinkInt.construct(240));
            this.multHashPre1970.put("mark", construct8);
            this.multHashPre1970.put("marks", construct8);
            FrinkReal construct9 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(4));
            this.multHashPre1970.put("crown", construct9);
            this.multHashPre1970.put("crowns", construct9);
            FrinkReal construct10 = FrinkRational.construct(FrinkInt.construct(21), FrinkInt.construct(20));
            this.multHashPre1970.put("guinea", construct10);
            this.multHashPre1970.put("guineas", construct10);
            this.multHashPost1970.put("pound", FrinkInt.ONE);
            this.multHashPost1970.put("pounds", FrinkInt.ONE);
            this.multHashPost1970.put("GBP", FrinkInt.ONE);
            FrinkReal construct11 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(100));
            this.multHashPost1970.put("penny", construct11);
            this.multHashPost1970.put("pence", construct11);
            FrinkReal construct12 = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(20));
            this.multHashPost1970.put("shilling", construct12);
            this.multHashPost1970.put("shillings", construct12);
        } catch (InvalidDenominatorException e) {
        }
    }

    public String getName() {
        return "PoundSource";
    }

    public Enumeration<String> getNames() {
        return this.finalHash.keys();
    }

    /* JADX WARNING: Removed duplicated region for block: B:69:0x013f A[SYNTHETIC, Splitter:B:69:0x013f] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0149 A[SYNTHETIC, Splitter:B:75:0x0149] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public frink.units.Unit get(java.lang.String r11) {
        /*
            r10 = this;
            r3 = 2005(0x7d5, float:2.81E-42)
            r9 = -1
            r8 = 0
            java.util.Hashtable<java.lang.String, frink.units.BasicUnit> r0 = r10.finalHash
            java.lang.Object r0 = r0.get(r11)
            frink.units.Unit r0 = (frink.units.Unit) r0
            if (r0 == 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.lang.String r0 = "_"
            int r0 = r11.lastIndexOf(r0)
            if (r0 != r9) goto L_0x0019
            r0 = r8
            goto L_0x000e
        L_0x0019:
            int r1 = r0 + 1
            java.lang.String r1 = r11.substring(r1)     // Catch:{ NumberFormatException -> 0x007e }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x007e }
            if (r1 <= r3) goto L_0x002a
            r2 = 2011(0x7db, float:2.818E-42)
            if (r1 > r2) goto L_0x002a
            r1 = r3
        L_0x002a:
            java.lang.Integer r2 = new java.lang.Integer     // Catch:{ NumberFormatException -> 0x007e }
            r2.<init>(r1)     // Catch:{ NumberFormatException -> 0x007e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "http://www.measuringworth.com/ppoweruk/result.php?year_result=2005&amount=1&use%5B%5D=CPI&year_source="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            r4 = 0
            java.lang.String r0 = r11.substring(r4, r0)
            r4 = 1970(0x7b2, float:2.76E-42)
            if (r1 > r4) goto L_0x0081
            java.util.Hashtable<java.lang.String, frink.numeric.Numeric> r1 = r10.multHashPre1970
            java.lang.Object r0 = r1.get(r0)
            frink.numeric.Numeric r0 = (frink.numeric.Numeric) r0
            r1 = r0
        L_0x0054:
            r4 = 0
            if (r1 == 0) goto L_0x0168
            java.util.Hashtable<java.lang.Integer, frink.source.PoundSource$PoundUnit> r0 = r10.yearHash     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            frink.source.PoundSource$PoundUnit r0 = (frink.source.PoundSource.PoundUnit) r0     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            if (r0 == 0) goto L_0x008b
            frink.units.BasicUnit r2 = new frink.units.BasicUnit     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            frink.numeric.Numeric r3 = r0.getScale()     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            frink.numeric.Numeric r1 = frink.numeric.NumericMath.multiply(r1, r3)     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            frink.units.DimensionList r0 = r0.getDimensionList()     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            r2.<init>(r1, r0)     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            java.util.Hashtable<java.lang.String, frink.units.BasicUnit> r0 = r10.finalHash     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            r0.put(r11, r2)     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            if (r8 == 0) goto L_0x007c
            r4.close()     // Catch:{ IOException -> 0x014d }
        L_0x007c:
            r0 = r2
            goto L_0x000e
        L_0x007e:
            r0 = move-exception
            r0 = r8
            goto L_0x000e
        L_0x0081:
            java.util.Hashtable<java.lang.String, frink.numeric.Numeric> r1 = r10.multHashPost1970
            java.lang.Object r0 = r1.get(r0)
            frink.numeric.Numeric r0 = (frink.numeric.Numeric) r0
            r1 = r0
            goto L_0x0054
        L_0x008b:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0102 }
            r0.<init>(r3)     // Catch:{ MalformedURLException -> 0x0102 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0119 }
            java.lang.String r3 = "User-Agent"
            java.lang.String r4 = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3"
            r0.setRequestProperty(r3, r4)     // Catch:{ IOException -> 0x0119 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0119 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0119 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0119 }
            r4.<init>(r0)     // Catch:{ IOException -> 0x0119 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0119 }
        L_0x00a9:
            java.lang.String r0 = r3.readLine()     // Catch:{ IOException -> 0x0162, NumericException -> 0x015f, all -> 0x015a }
            if (r0 == 0) goto L_0x0110
            java.lang.String r4 = r10.MARKER     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            int r4 = r0.indexOf(r4)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            if (r4 == r9) goto L_0x00a9
            java.lang.String r5 = r10.MARKER     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            int r5 = r5.length()     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            int r4 = r4 + r5
            java.lang.String r5 = "<"
            int r5 = r0.indexOf(r5, r4)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            if (r5 == r9) goto L_0x00a9
            frink.source.PoundSource$PoundUnit r6 = new frink.source.PoundSource$PoundUnit     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            frink.numeric.FrinkFloat r7 = new frink.numeric.FrinkFloat     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            java.lang.String r0 = r0.substring(r4, r5)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            r4 = 44
            java.lang.String r5 = ""
            java.lang.String r0 = frink.text.StringUtils.replace(r0, r4, r5)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            r7.<init>(r0)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            r0 = 0
            r6.<init>(r7)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            java.util.Hashtable<java.lang.Integer, frink.source.PoundSource$PoundUnit> r0 = r10.yearHash     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            r0.put(r2, r6)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            frink.units.BasicUnit r0 = new frink.units.BasicUnit     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            frink.numeric.Numeric r4 = r6.getScale()     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            frink.numeric.Numeric r4 = frink.numeric.NumericMath.multiply(r1, r4)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            frink.units.DimensionList r5 = r6.getDimensionList()     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            r0.<init>(r4, r5)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            java.util.Hashtable<java.lang.String, frink.units.BasicUnit> r4 = r10.finalHash     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            r4.put(r11, r0)     // Catch:{ StringIndexOutOfBoundsException -> 0x0165 }
            if (r3 == 0) goto L_0x000e
            r3.close()     // Catch:{ IOException -> 0x00ff }
            goto L_0x000e
        L_0x00ff:
            r1 = move-exception
            goto L_0x000e
        L_0x0102:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            r1.println(r0)     // Catch:{ NumericException -> 0x0123, all -> 0x0145 }
            if (r8 == 0) goto L_0x010d
            r4.close()     // Catch:{ IOException -> 0x0150 }
        L_0x010d:
            r0 = r8
            goto L_0x000e
        L_0x0110:
            r0 = r3
        L_0x0111:
            if (r0 == 0) goto L_0x0116
            r0.close()     // Catch:{ IOException -> 0x0154 }
        L_0x0116:
            r0 = r8
            goto L_0x000e
        L_0x0119:
            r0 = move-exception
            r0 = r8
        L_0x011b:
            if (r0 == 0) goto L_0x0120
            r0.close()     // Catch:{ IOException -> 0x0152 }
        L_0x0120:
            r0 = r8
            goto L_0x000e
        L_0x0123:
            r0 = move-exception
            r1 = r8
        L_0x0125:
            java.io.PrintStream r2 = java.lang.System.err     // Catch:{ all -> 0x015d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x015d }
            r3.<init>()     // Catch:{ all -> 0x015d }
            java.lang.String r4 = "Unexpected numeric exception in PoundSource.get: \n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x015d }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x015d }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x015d }
            r2.println(r0)     // Catch:{ all -> 0x015d }
            if (r1 == 0) goto L_0x0142
            r1.close()     // Catch:{ IOException -> 0x0156 }
        L_0x0142:
            r0 = r8
            goto L_0x000e
        L_0x0145:
            r0 = move-exception
            r1 = r8
        L_0x0147:
            if (r1 == 0) goto L_0x014c
            r1.close()     // Catch:{ IOException -> 0x0158 }
        L_0x014c:
            throw r0
        L_0x014d:
            r0 = move-exception
            goto L_0x007c
        L_0x0150:
            r0 = move-exception
            goto L_0x010d
        L_0x0152:
            r0 = move-exception
            goto L_0x0120
        L_0x0154:
            r0 = move-exception
            goto L_0x0116
        L_0x0156:
            r0 = move-exception
            goto L_0x0142
        L_0x0158:
            r1 = move-exception
            goto L_0x014c
        L_0x015a:
            r0 = move-exception
            r1 = r3
            goto L_0x0147
        L_0x015d:
            r0 = move-exception
            goto L_0x0147
        L_0x015f:
            r0 = move-exception
            r1 = r3
            goto L_0x0125
        L_0x0162:
            r0 = move-exception
            r0 = r3
            goto L_0x011b
        L_0x0165:
            r0 = move-exception
            goto L_0x00a9
        L_0x0168:
            r0 = r8
            goto L_0x0111
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.source.PoundSource.get(java.lang.String):frink.units.Unit");
    }

    private class PoundUnit implements Unit {
        private Numeric scale;

        private PoundUnit(Numeric numeric) throws NumericException {
            this.scale = NumericMath.multiply(numeric, PoundSource.this.pound.getScale());
        }

        public DimensionList getDimensionList() {
            return PoundSource.this.pound.getDimensionList();
        }

        public Numeric getScale() {
            return this.scale;
        }
    }
}
