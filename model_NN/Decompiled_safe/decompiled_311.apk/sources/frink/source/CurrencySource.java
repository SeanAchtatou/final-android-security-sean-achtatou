package frink.source;

import frink.expr.DimensionlessUnitExpression;
import frink.expr.EnumerationStacker;
import frink.expr.Environment;
import frink.numeric.FrinkFloat;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.units.DimensionList;
import frink.units.DimensionListMath;
import frink.units.Source;
import frink.units.Unit;
import frink.units.UnitManager;
import frink.units.UnitMath;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;

public class CurrencySource implements Source<Unit> {
    private static final boolean DEBUG = false;
    private static Hashtable<String, Numeric> cache = null;
    private static String cacheBaseCurrency = null;
    private static String cacheDate = null;
    /* access modifiers changed from: private */
    public static boolean useCacheOnly = false;
    /* access modifiers changed from: private */
    public String baseCurrency;
    private Unit baseUnit;
    private Hashtable<String, CurrencyUnit> codeHash;
    private Hashtable<String, UsedCurrencyPair> currencyHash;
    private Environment env;
    private Hashtable<String, String> nameHash;
    /* access modifiers changed from: private */
    public long refreshInterval = 3600000;
    private Unit troyOunce;

    public CurrencySource(String str, Unit unit, Unit unit2, Environment environment) {
        this.baseCurrency = str;
        this.baseUnit = unit;
        this.troyOunce = unit2;
        this.env = environment;
        this.codeHash = new Hashtable<>();
        this.nameHash = new Hashtable<>();
        initUnits();
        initScaledUnits();
    }

    public void setRefreshInterval(long j) {
        this.refreshInterval = j;
    }

    public String getName() {
        return "CurrencySource";
    }

    public Enumeration<String> getNames() {
        EnumerationStacker enumerationStacker = new EnumerationStacker();
        enumerationStacker.addEnumeration(this.codeHash.keys());
        enumerationStacker.addEnumeration(this.nameHash.keys());
        return enumerationStacker;
    }

    public Unit get(String str) {
        String str2 = this.nameHash.get(str);
        if (str2 != null) {
            return this.codeHash.get(str2);
        }
        return this.codeHash.get(str);
    }

    private void addScaledUnit(String str, String str2, Unit unit) {
        String canonicalName = getCanonicalName(str);
        if (this.nameHash.get(canonicalName) == null) {
            this.nameHash.put(canonicalName, str2);
        } else {
            System.out.println("Duplicate units for " + canonicalName);
        }
        if (this.codeHash.get(str2) == null) {
            this.codeHash.put(str2, new CurrencyUnit(str2, this.baseUnit.getDimensionList(), unit));
        }
    }

    private void addUnit(String str, String str2, String str3) {
        String canonicalName = getCanonicalName(str);
        boolean z = false;
        if (str.indexOf("OBSOLETE") != -1) {
            z = true;
        }
        if (!z) {
            if (this.nameHash.get(canonicalName) == null) {
                this.nameHash.put(canonicalName, str3);
            } else {
                System.out.println("Duplicate units for " + canonicalName);
            }
        }
        if (!(str == null || str2 == null)) {
            String canonicalName2 = getCanonicalName(str + "_" + str2);
            if (this.nameHash.get(canonicalName2) == null) {
                this.nameHash.put(canonicalName2, str3);
            } else {
                System.out.println("Duplicate units for " + canonicalName2);
            }
            if (!z) {
                String canonicalName3 = getCanonicalName(str + "_currency");
                if (this.nameHash.get(canonicalName3) == null) {
                    this.nameHash.put(canonicalName3, str3);
                } else {
                    System.out.println("Duplicate units for " + canonicalName3);
                }
            }
        }
        if (!(this.currencyHash == null || str2 == null)) {
            UsedCurrencyPair usedCurrencyPair = this.currencyHash.get(str2);
            if (usedCurrencyPair == null) {
                this.currencyHash.put(str2, new UsedCurrencyPair(str3));
            } else {
                usedCurrencyPair.incrementCount();
            }
        }
        if (this.codeHash.get(str3) == null) {
            this.codeHash.put(str3, new CurrencyUnit(str3, this.baseUnit.getDimensionList()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private String getCanonicalName(String str) {
        return str.replace(' ', '_').replace('/', '_').replace('-', '_').replace('\'', '_');
    }

    private void initUnits() {
        this.currencyHash = new Hashtable<>();
        addUnit("Abkhazia", "Ruble", "RUB");
        addUnit("Afghanistan", "Afghani", "AFN");
        addUnit("Åland Islands", "Euro", "EUR");
        addUnit("Albania", "Lek", "ALL");
        addUnit("Algeria", "Dinar", "DZD");
        addUnit("America", "Dollar", "USD");
        addUnit("American Samoa", "United States Dollar", "USD");
        addUnit("Andorra", "Euro", "EUR");
        addUnit("Angola", "Kwanza", "AOA");
        addUnit("Anguilla", "East Caribbean Dollar", "XCD");
        addUnit("Antigua and Barbuda", "East Caribbean Dollar", "XCD");
        addUnit("Argentina", "Peso", "ARS");
        addUnit("Armenia", "Dram", "AMD");
        addUnit("Aruba", "Guilder", "AWG");
        addUnit("Ashmore and Cartier Islands", "Australia Dollar", "AUD");
        addUnit("Australia", "Dollar", "AUD");
        addUnit("Australian Antarctic Territory", "Dollar", "AUD");
        addUnit("Austria OBSOLETE", "Schilling", "ATS");
        addUnit("Austria", "Euro", "EUR");
        addUnit("Azerbaijan", "Manat", "AZN");
        addUnit("Azores", "Euro", "EUR");
        addUnit("Bahamas", "Dollar", "BSD");
        addUnit("Bahrain", "Dinar", "BHD");
        addUnit("Bajan", "Dollar", "BBD");
        addUnit("Balearic Islands", "Euro", "EUR");
        addUnit("Bangladesh", "Taka", "BDT");
        addUnit("Barbados", "Dollar", "BBD");
        addUnit("Belarus", "Ruble", "BYR");
        addUnit("Belgium OBSOLETE", "Franc", "BEF");
        addUnit("Belgium", "Euro", "EUR");
        addUnit("Belize", "Dollar", "BZD");
        addUnit("Benin", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Bermuda", "Dollar", "BMD");
        addUnit("Bhutan", "Ngultrum", "BTN");
        addUnit("Bolivia", "Boliviano", "BOB");
        addUnit("Bonaire", "Netherlands Antilles Guilder", "ANG");
        addUnit("Bosnia and Herzegovina", "Convertible Mark", "BAM");
        addUnit("Bosnia", "Convertible Mark", "BAM");
        addUnit("Botswana", "Pula", "BWP");
        addUnit("Bouvet Island", "Krone", "NOK");
        addUnit("Brazil", "Real", "BRL");
        addUnit("Britain", "Pound", "GBP");
        addUnit("British Antarctic Territory", "Pound", "GBP");
        addUnit("British Indian Ocean Territory", "Dollar", "USD");
        addUnit("British Virgin Islands", "United States Dollar", "USD");
        addUnit("Brunei", "Dollar", "BND");
        addUnit("Bulgaria", "Lev", "BGN");
        addUnit("Burkina Faso", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Burma", "Kyat", "MMK");
        addUnit("Burundi", "Franc", "BIF");
        addUnit("Cambodia", "Riel", "KHR");
        addUnit("Cameroon", "Coopération financière en Afrique centrale Franc", "XAF");
        addUnit("Canada", "Dollar", "CAD");
        addUnit("Canary Islands", "Euro", "EUR");
        addUnit("Cape Verde", "Escudo", "CVE");
        addUnit("Cayman Islands", "Dollar", "KYD");
        addUnit("Central African Republic", "Coopération financière en Afrique centrale Franc", "XAF");
        addUnit("Chad", "Coopération financière en Afrique centrale Franc", "XAF");
        addUnit("Channel Islands", "Pound", "GBP");
        addUnit("Chile", "Peso", "CLP");
        addUnit("China", "Yuan Renminbi", "CNY");
        addUnit("Christmas Island", "Australia Dollar", "AUD");
        addUnit("Cocos", "Australia Dollar", "AUD");
        addUnit("Colombia", "Peso", "COP");
        addUnit("Coopération financière en Afrique centrale Franc", null, "XAF");
        addUnit("Communauté Financière d'Afrique Franc", null, "XOF");
        addUnit("Comoros", "Franc", "KMF");
        addUnit("Comptoirs Français du Pacifique Franc", null, "XPF");
        addUnit("Congo/Brazzaville", "Coopération financière en Afrique centrale Franc", "XAF");
        addUnit("Congo/Kinshasa", "Franc", "CDF");
        addUnit("Cook Islands", "New Zealand Dollar", "NZD");
        addUnit("Coral Sea Islands", "Australia Dollar", "AUD");
        addUnit("Costa Rica", "Colon", "CRC");
        addUnit("Côte d'Ivoire", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Ivory Coast", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Croatia", "Kuna", "HRK");
        addUnit("Cuba", "Peso", "CUP");
        addUnit("Curaço", "Netherlands Antilles Guilder", "ANG");
        addUnit("Cyprus OBSOLETE", "Pound", "CYP");
        addUnit("Cyprus", "Euro", "EUR");
        addUnit("Czech Republic", "Koruna", "CZK");
        addUnit("Denmark", "Krone", "DKK");
        addUnit("Djibouti", "Franc", "DJF");
        addUnit("Dominica", "East Caribbean Dollar", "XCD");
        addUnit("Dominican Republic", "Peso", "DOP");
        addUnit("Dutch OBSOLETE", "Guilder", "NLG");
        addUnit("Dutch", "Euro", "EUR");
        addUnit("East Caribbean Dollar", null, "XCD");
        addUnit("East Timor", "Dollar", "USD");
        addUnit("Ecuador", "United States Dollar", "USD");
        addUnit("Egypt", "Pound", "EGP");
        addUnit("El Salvador", "Colon", "SVC");
        addUnit("England", "Pound", "GBP");
        addUnit("Equatorial Guinea", "Coopération financière en Afrique centrale Franc", "XAF");
        addUnit("Eritrea", "Nakfa", "ERN");
        addUnit("Estonia", "Kroon", "EEK");
        addUnit("Ethiopia", "Birr", "ETB");
        addUnit("Euro", null, "EUR");
        addUnit("euro", null, "EUR");
        addUnit("€", null, "EUR");
        addUnit("Europa Island", "Euro", "EUR");
        addUnit("European Union", "Euro", "EUR");
        addUnit("Falkland Islands", "Pound", "FKP");
        addUnit("Faroe Islands", "Denmark Krone", "DKK");
        addUnit("Fiji", "Dollar", "FJD");
        addUnit("Finland OBSOLETE", "Markka", "FIM");
        addUnit("Finland", "Euro", "EUR");
        addUnit("France OBSOLETE", "Franc", "FRF");
        addUnit("France", "Euro", "EUR");
        addUnit("French Guiana OBSOLETE", "France Franc", "FRF");
        addUnit("French Guiana", "Euro", "EUR");
        addUnit("French Polynesia", "Comptoirs Français du Pacifique Franc", "XPF");
        addUnit("French Southern and Antarctic Lands", "Euro", "EUR");
        addUnit("Gabon", "Coopération financière en Afrique centrale Franc", "XAF");
        addUnit("Gambia", "Dalasi", "GMD");
        addUnit("Gaza Strip", "Israel New Sheqel", "ILS");
        addUnit("Georgia", "Lari", "GEL");
        addUnit("Germany", "Euro", "EUR");
        addUnit("Germany OBSOLETE", "Deutsche Mark", "DEM");
        addUnit("Ghana", "Cedi", "GHS");
        addUnit("Gibraltar", "Pound", "GIP");
        addUnit("Great Britain", "Pound", "GBP");
        addUnit("Greece OBSOLETE", "Drachma", "GRD");
        addUnit("Greece", "Euro", "EUR");
        addUnit("Greenland", "Denmark Krone", "DKK");
        addUnit("Grenada", "East Caribbean Dollar", "XCD");
        addUnit("Guadeloupe OBSOLETE", "France Franc", "FRF");
        addUnit("Guadeloupe", "Euro", "EUR");
        addUnit("Guam", "United States Dollar", "USD");
        addUnit("Guatemala", "Quetzal", "GTQ");
        addUnit("Guernsey", "Pound", "GBP");
        addUnit("Guinea", "Franc", "GNF");
        addUnit("Guinea-Bissau", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Guyana", "Dollar", "GYD");
        addUnit("Haiti", "Gourde", "HTG");
        addUnit("Heard Island and McDonald Islands", "Dollar", "AUD");
        addUnit("Herzegovina", "Convertible Mark", "BAM");
        addUnit("Holy See OBSOLETE", "Lira", "VAL");
        addUnit("Holy See", "Euro", "EUR");
        addUnit("Honduras", "Lempira", "HNL");
        addUnit("Hong Kong", "Dollar", "HKD");
        addUnit("Hungary", "Forint", "HUF");
        addUnit("Iceland", "Krona", "ISK");
        addUnit("India", "Rupee", "INR");
        addUnit("Indonesia", "Rupiah", "IDR");
        addUnit("International Monetary Fund Special Drawing Right", null, "XDR");
        addUnit("Iran", "Rial", "IRR");
        addUnit("Iraq", "Dinar", "IQD");
        addUnit("Ireland", "Euro", "EUR");
        addUnit("Ireland OBSOLETE", "Punt", "IEP");
        addUnit("Islas Malvinas", "Pound", "FKP");
        addUnit("Isle of Man", "Pound", "GBP");
        addUnit("Israel", "New Sheqel", "ILS");
        addUnit("Italy", "Euro", "EUR");
        addUnit("Italy OBSOLETE", "Lira", "ITL");
        addUnit("Jamaica", "Dollar", "JMD");
        addUnit("Japan", "Yen", "JPY");
        addUnit("¥", null, "JPY");
        addUnit("Jersey", "Pound", "GBP");
        addUnit("Jordan", "Dinar", "JOD");
        addUnit("Juan de Nova", "Euro", "EUR");
        addUnit("Kazakhstan", "Tenge", "KZT");
        addUnit("Kenya", "Shilling", "KES");
        addUnit("Kiribati", "Australia Dollar", "AUD");
        addUnit("Kosovo", "Euro", "EUR");
        addUnit("Kuwait", "Dinar", "KWD");
        addUnit("Kyrgyzstan", "Som", "KGS");
        addUnit("Laos", "Kip", "LAK");
        addUnit("Latvia", "Lats", "LVL");
        addUnit("Lebanon", "Pound", "LBP");
        addUnit("Lesotho", "Loti", "LSL");
        addUnit("Liberia", "Dollar", "LRD");
        addUnit("Libya", "Dinar", "LYD");
        addUnit("Liechtenstein", "Switzerland Franc", "CHF");
        addUnit("Lithuania", "Litas", "LTL");
        addUnit("Luxembourg", "Euro", "EUR");
        addUnit("Luxembourg OBSOLETE", "Franc", "LUF");
        addUnit("Macau", "Pataca", "MOP");
        addUnit("Macedonia", "Denar", "MKD");
        addUnit("Madagascar", "Ariary", "MGA");
        addUnit("Madeira Islands", "Euro", "EUR");
        addUnit("Malawi", "Kwacha", "MWK");
        addUnit("Malaysia", "Ringgit", "MYR");
        addUnit("Maldives", "Rufiyaa", "MVR");
        addUnit("Mali", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Malta", "Euro", "EUR");
        addUnit("Malta OBSOLETE", "Lira", "MTL");
        addUnit("Malvinas", "Pound", "FKP");
        addUnit("Marshall Islands", "United States Dollar", "USD");
        addUnit("Martinique", "Euro", "EUR");
        addUnit("Mauritania", "Ouguiya", "MRO");
        addUnit("Mauritius", "Rupee", "MUR");
        addUnit("Mayotte", "Euro", "EUR");
        addUnit("Mexico", "Peso", "MXN");
        addUnit("Micronesia", "United States Dollar", "USD");
        addUnit("Midway Islands", "United States Dollar", "USD");
        addUnit("Moldova", "Leu", "MDL");
        addUnit("Monaco", "Euro", "EUR");
        addUnit("Mongolia", "Tugrik", "MNT");
        addUnit("Montenegro", "Euro", "EUR");
        addUnit("Montserrat", "East Caribbean Dollar", "XCD");
        addUnit("Morocco", "Dirham", "MAD");
        addUnit("Mozambique", "Metical", "MZN");
        addUnit("Myanmar", "Kyat", "MMK");
        addUnit("Namibia", "Dollar", "NAD");
        addUnit("Nauru", "Australia Dollar", "AUD");
        addUnit("Nepal", "Rupee", "NPR");
        addUnit("Netherlands Antilles", "Guilder", "ANG");
        addUnit("Netherlands", "Euro", "EUR");
        addUnit("Netherlands OBSOLETE", "Guilder", "NLG");
        addUnit("New Caledonia", "Comptoirs Français du Pacifique Franc", "XPF");
        addUnit("New Zealand", "Dollar", "NZD");
        addUnit("Nicaragua", "Gold Cordoba", "NIO");
        addUnit("Niger", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Nigeria", "Naira", "NGN");
        addUnit("Niue", "New Zealand Dollar", "NZD");
        addUnit("Norfolk Island", "Australia Dollar", "AUD");
        addUnit("North Korea", "Won", "KPW");
        addUnit("Northern Cyprus", "New Turkish Lira", "TRY");
        addUnit("Northern Mariana Islands", "United States Dollar", "USD");
        addUnit("Norway", "Krone", "NOK");
        addUnit("Oman", "Rial", "OMR");
        addUnit("Pakistan", "Rupee", "PKR");
        addUnit("Palau", "United States Dollar", "USD");
        addUnit("Panama", "Balboa", "PAB");
        addUnit("Papua New Guinea", "Kina", "PGK");
        addUnit("Paraguay", "Guarani", "PYG");
        addUnit("Peru", "Nuevo Sol", "PEN");
        addUnit("Philippines", "Peso", "PHP");
        addUnit("Pitcairn", "New Zealand Dollar", "NZD");
        addUnit("Poland", "Zloty", "PLN");
        addUnit("Portugal", "Euro", "EUR");
        addUnit("Portugal OBSOLETE", "Escudo", "PTE");
        addUnit("Puerto Rico", "United States Dollar", "USD");
        addUnit("Qatar", "Rial", "QAR");
        addUnit("Reunion", "Euro", "EUR");
        addUnit("Romania", "New Leu", "RON");
        addUnit("Russia", "Ruble", "RUB");
        addUnit("Rwanda", "Franc", "RWF");
        addUnit("Saba", "Netherlands Antilles Guilder", "ANG");
        addUnit("Saint-Barthélemy", "Euro", "EUR");
        addUnit("Saint Helena", "Pound", "SHP");
        addUnit("Saint Kitts and Nevis", "East Caribbean Dollar", "XCD");
        addUnit("Saint Lucia", "East Caribbean Dollar", "XCD");
        addUnit("Saint Pierre and Miquelon", "Euro", "EUR");
        addUnit("Saint Vincent and The Grenadines", "East Caribbean Dollar", "XCD");
        addUnit("Saint Martin", "Euro", "EUR");
        addUnit("Samoa", "Tala", "WST");
        addUnit("San Marino", "Euro", "EUR");
        addUnit("São Tome and Principe", "Dobra", "STD");
        addUnit("Saudi Arabia", "Riyal", "SAR");
        addUnit("Scotland", "Pound", "GBP");
        addUnit("Seborga", "Euro", "EUR");
        addUnit("Senegal", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Serbia", "Dinar", "RSD");
        addUnit("Seychelles", "Rupee", "SCR");
        addUnit("Sierra Leone", "Leone", "SLL");
        addUnit("Singapore", "Dollar", "SGD");
        addUnit("Sint Eustatius", "Netherlands Antilles Guilder", "ANG");
        addUnit("Sint Maarten", "Netherlands Antilles Guilder", "ANG");
        addUnit("Slovakia OBSOLETE", "Koruna", "SKK");
        addUnit("Slovakia", "Euro", "EUR");
        addUnit("Slovenia OBSOLETE", "Tolar", "SIT");
        addUnit("Slovenia", "Euro", "EUR");
        addUnit("Solomon Islands", "Dollar", "SBD");
        addUnit("Somalia", "Shilling", "SOS");
        addUnit("South Africa", "Rand", "ZAR");
        addUnit("South Georgia", "United Kingdom Pound", "GBP");
        addUnit("South Korea", "Won", "KRW");
        addUnit("South Ossetia", "Ruble", "RUB");
        addUnit("South Sandwich Islands", "United Kingdom Pound", "GBP");
        addUnit("Spain", "Euro", "EUR");
        addUnit("Spain OBSOLETE", "Peseta", "ESP");
        addUnit("Sri Lanka", "Rupee", "LKR");
        addUnit("Sudan", "Pound", "SDG");
        addUnit("Suriname", "Dollar", "SRD");
        addUnit("Svalbard and Jan Mayen", "Norway Krone", "NOK");
        addUnit("Swaziland", "Lilangeni", "SZL");
        addUnit("Sweden", "Krona", "SEK");
        addUnit("Switzerland", "Franc", "CHF");
        addUnit("Syria", "Pound", "SYP");
        addUnit("Taiwan", "New Dollar", "TWD");
        addUnit("Tahiti", "Comptoirs Français du Pacifique Franc", "XPF");
        addUnit("Tajikistan", "Somoni", "TJS");
        addUnit("Tanzania", "Shilling", "TZS");
        addUnit("Thailand", "Baht", "THB");
        addUnit("Timor-Leste", "Dollar", "USD");
        addUnit("Togo", "Communauté Financière d'Afrique Franc", "XOF");
        addUnit("Tokelau", "New Zealand Dollar", "NZD");
        addUnit("Tonga", "Pa'anga", "TOP");
        addUnit("Transnistria", "Moldova Leu", "MDL");
        addUnit("Trinidad and Tobago", "Dollar", "TTD");
        addUnit("Tunisia", "Dinar", "TND");
        addUnit("Turkey", "New Turkish Lira", "TRY");
        addUnit("Turkmenistan", "Manat", "TMM");
        addUnit("Turks and Caicos Islands", "United States Dollar", "USD");
        addUnit("Tuvalu", "Dollar", "TVD");
        addUnit("Uganda", "Shilling", "UGX");
        addUnit("Ukraine", "Hryvna", "UAH");
        addUnit("United Arab Emirates", "Dirham", "AED");
        addUnit("United Kingdom", "Pound", "GBP");
        addUnit("£", null, "GBP");
        addUnit("United States", "Dollar", "USD");
        addUnit("dollar", null, "USD");
        addUnit("United States Minor Outlying Islands", "Dollar", "USD");
        addUnit("Uruguay", "Peso", "UYU");
        addUnit("Uzbekistan", "Som", "UZS");
        addUnit("Vanuatu", "Vatu", "VUV");
        addUnit("Vatican City", "Euro", "EUR");
        addUnit("Vatican City OBSOLETE", "Lira", "VAL");
        addUnit("Venezuela", "Bolivar Fuerte", "VEF");
        addUnit("Vietnam", "Dong", "VND");
        addUnit("Virgin Islands", "United States Dollar", "USD");
        addUnit("Wake Island", "United States Dollar", "USD");
        addUnit("Wallis and Futuna Islands", "Comptoirs Français du Pacifique Franc", "XPF");
        addUnit("Western Sahara", "Morocco Dirham", "MAD");
        addUnit("Western Samoa", "Tala", "WST");
        addUnit("Yemen", "Rial", "YER");
        addUnit("Yugoslavia", "New Dinar", "YUN");
        addUnit("Zambia", "Kwacha", "ZMK");
        addUnit("Zimbabwe", "Dollar", "ZWD");
        findUniqueNames();
    }

    private void initScaledUnits() {
        if (get("USD") != null) {
            try {
                Unit divide = UnitMath.divide(DimensionlessUnitExpression.ONE, this.troyOunce);
                addScaledUnit("Gold", "XAU", divide);
                addScaledUnit("Palladium", "XPD", divide);
                addScaledUnit("Platinum", "XPT", divide);
                addScaledUnit("Silver", "XAG", divide);
            } catch (NumericException e) {
                System.err.println("Unexpected exception in CurrencySource.initScaledUnits: " + e);
            }
        }
    }

    /* access modifiers changed from: private */
    public String removeCommas(String str) {
        if (str.indexOf(44) == -1) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(44, i);
            if (indexOf == -1) {
                break;
            }
            stringBuffer.append(str.substring(i, indexOf));
            i = indexOf + 1;
        }
        if (i != str.length() - 1) {
            stringBuffer.append(str.substring(i));
        }
        return new String(stringBuffer);
    }

    private class CurrencyUnit implements Unit {
        private Numeric baseScale;
        private String code;
        private DimensionList dimensions;
        private long lastFetched;
        private Numeric scale;

        public CurrencyUnit(String str, DimensionList dimensionList) {
            this.code = str;
            this.dimensions = dimensionList;
            this.baseScale = null;
            this.scale = null;
            this.lastFetched = 0;
        }

        public CurrencyUnit(String str, DimensionList dimensionList, Unit unit) {
            this.code = str;
            this.dimensions = DimensionListMath.multiply(dimensionList, unit.getDimensionList());
            this.baseScale = unit.getScale();
            this.scale = null;
            this.lastFetched = 0;
        }

        /* JADX WARNING: Removed duplicated region for block: B:56:0x0164 A[SYNTHETIC, Splitter:B:56:0x0164] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x0169 A[Catch:{ IOException -> 0x018d }] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0174 A[SYNTHETIC, Splitter:B:64:0x0174] */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x0179 A[Catch:{ IOException -> 0x017d }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public frink.numeric.Numeric getScale() {
            /*
                r9 = this;
                r7 = 0
                frink.numeric.Numeric r0 = r9.scale
                if (r0 == 0) goto L_0x0019
                long r0 = java.lang.System.currentTimeMillis()
                long r2 = r9.lastFetched
                long r0 = r0 - r2
                frink.source.CurrencySource r2 = frink.source.CurrencySource.this
                long r2 = r2.refreshInterval
                int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r0 > 0) goto L_0x0019
                frink.numeric.Numeric r0 = r9.scale
            L_0x0018:
                return r0
            L_0x0019:
                java.lang.String r0 = r9.code
                frink.source.CurrencySource r1 = frink.source.CurrencySource.this
                java.lang.String r1 = r1.baseCurrency
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x002e
                frink.numeric.FrinkInt r0 = frink.numeric.FrinkInt.ONE
                r9.scale = r0
                frink.numeric.Numeric r0 = r9.scale
                goto L_0x0018
            L_0x002e:
                boolean r0 = frink.source.CurrencySource.useCacheOnly
                if (r0 == 0) goto L_0x003f
                frink.source.CurrencySource r0 = frink.source.CurrencySource.this
                java.lang.String r1 = r9.code
                frink.numeric.Numeric r2 = r9.baseScale
                frink.numeric.Numeric r0 = r0.getValueFromCache(r1, r2)
                goto L_0x0018
            L_0x003f:
                java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0128 }
                java.lang.String r1 = "http://www.xe.com/ucc/convert/"
                r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0128 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                r1.<init>()     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.String r2 = "1&nbsp;"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.String r2 = r9.code     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.String r2 = "&nbsp;=&nbsp;"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                r2 = 1
                r0.setDoOutput(r2)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                r2 = 1
                r0.setDoInput(r2)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                r2 = 0
                r0.setUseCaches(r2)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.String r2 = "User-Agent"
                java.lang.String r3 = "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0"
                r0.setRequestProperty(r2, r3)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                r2.<init>(r3)     // Catch:{ IOException -> 0x0155, all -> 0x016f }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                r3.<init>()     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r4 = "Amount=1&From="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                frink.source.CurrencySource r4 = frink.source.CurrencySource.this     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r4 = r4.baseCurrency     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r4 = java.net.URLEncoder.encode(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r4 = "&To="
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r4 = r9.code     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r4 = java.net.URLEncoder.encode(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                r2.writeBytes(r3)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                r2.flush()     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                r4.<init>(r0)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
                r3.<init>(r4)     // Catch:{ IOException -> 0x018f, all -> 0x017f }
            L_0x00c2:
                java.lang.String r0 = r3.readLine()     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                if (r0 == 0) goto L_0x013f
                int r4 = r0.indexOf(r1)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                if (r4 < 0) goto L_0x00c2
                int r5 = r1.length()     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                int r4 = r4 + r5
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                r5.<init>()     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                java.lang.String r6 = "&nbsp;"
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                frink.source.CurrencySource r6 = frink.source.CurrencySource.this     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                java.lang.String r6 = r6.baseCurrency     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                int r5 = r0.indexOf(r5, r4)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                if (r5 < 0) goto L_0x00c2
                java.lang.String r0 = r0.substring(r4, r5)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                frink.source.CurrencySource r1 = frink.source.CurrencySource.this     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                java.lang.String r0 = r1.removeCommas(r0)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                frink.numeric.FrinkFloat r1 = new frink.numeric.FrinkFloat     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                r1.<init>(r0)     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                r9.scale = r1     // Catch:{ IOException -> 0x0193, all -> 0x0183 }
                frink.numeric.Numeric r0 = r9.baseScale     // Catch:{ NumericException -> 0x0131 }
                if (r0 == 0) goto L_0x0111
                frink.numeric.Numeric r0 = r9.scale     // Catch:{ NumericException -> 0x0131 }
                frink.numeric.Numeric r1 = r9.baseScale     // Catch:{ NumericException -> 0x0131 }
                frink.numeric.Numeric r0 = frink.numeric.NumericMath.multiply(r0, r1)     // Catch:{ NumericException -> 0x0131 }
                r9.scale = r0     // Catch:{ NumericException -> 0x0131 }
            L_0x0111:
                long r0 = java.lang.System.currentTimeMillis()     // Catch:{ NumericException -> 0x0131 }
                r9.lastFetched = r0     // Catch:{ NumericException -> 0x0131 }
                frink.numeric.Numeric r0 = r9.scale     // Catch:{ NumericException -> 0x0131 }
                if (r2 == 0) goto L_0x011e
                r2.close()     // Catch:{ IOException -> 0x0125 }
            L_0x011e:
                if (r3 == 0) goto L_0x0018
                r3.close()     // Catch:{ IOException -> 0x0125 }
                goto L_0x0018
            L_0x0125:
                r1 = move-exception
                goto L_0x0018
            L_0x0128:
                r0 = move-exception
                java.io.PrintStream r1 = java.lang.System.out
                r1.println(r0)
                r0 = r7
                goto L_0x0018
            L_0x0131:
                r0 = move-exception
                if (r2 == 0) goto L_0x0137
                r2.close()     // Catch:{ IOException -> 0x0199 }
            L_0x0137:
                if (r3 == 0) goto L_0x013c
                r3.close()     // Catch:{ IOException -> 0x0199 }
            L_0x013c:
                r0 = r7
                goto L_0x0018
            L_0x013f:
                if (r2 == 0) goto L_0x0144
                r2.close()     // Catch:{ IOException -> 0x0197 }
            L_0x0144:
                if (r3 == 0) goto L_0x0149
                r3.close()     // Catch:{ IOException -> 0x0197 }
            L_0x0149:
                frink.source.CurrencySource r0 = frink.source.CurrencySource.this
                java.lang.String r1 = r9.code
                frink.numeric.Numeric r2 = r9.baseScale
                frink.numeric.Numeric r0 = r0.getValueFromCache(r1, r2)
                goto L_0x0018
            L_0x0155:
                r0 = move-exception
                r0 = r7
                r1 = r7
            L_0x0158:
                frink.source.CurrencySource r2 = frink.source.CurrencySource.this     // Catch:{ all -> 0x0187 }
                java.lang.String r3 = r9.code     // Catch:{ all -> 0x0187 }
                frink.numeric.Numeric r4 = r9.baseScale     // Catch:{ all -> 0x0187 }
                frink.numeric.Numeric r2 = r2.getValueFromCache(r3, r4)     // Catch:{ all -> 0x0187 }
                if (r0 == 0) goto L_0x0167
                r0.close()     // Catch:{ IOException -> 0x018d }
            L_0x0167:
                if (r1 == 0) goto L_0x016c
                r1.close()     // Catch:{ IOException -> 0x018d }
            L_0x016c:
                r0 = r2
                goto L_0x0018
            L_0x016f:
                r0 = move-exception
                r1 = r7
                r2 = r7
            L_0x0172:
                if (r1 == 0) goto L_0x0177
                r1.close()     // Catch:{ IOException -> 0x017d }
            L_0x0177:
                if (r2 == 0) goto L_0x017c
                r2.close()     // Catch:{ IOException -> 0x017d }
            L_0x017c:
                throw r0
            L_0x017d:
                r1 = move-exception
                goto L_0x017c
            L_0x017f:
                r0 = move-exception
                r1 = r2
                r2 = r7
                goto L_0x0172
            L_0x0183:
                r0 = move-exception
                r1 = r2
                r2 = r3
                goto L_0x0172
            L_0x0187:
                r2 = move-exception
                r8 = r2
                r2 = r1
                r1 = r0
                r0 = r8
                goto L_0x0172
            L_0x018d:
                r0 = move-exception
                goto L_0x016c
            L_0x018f:
                r0 = move-exception
                r0 = r2
                r1 = r7
                goto L_0x0158
            L_0x0193:
                r0 = move-exception
                r0 = r2
                r1 = r3
                goto L_0x0158
            L_0x0197:
                r0 = move-exception
                goto L_0x0149
            L_0x0199:
                r0 = move-exception
                goto L_0x013c
            */
            throw new UnsupportedOperationException("Method not decompiled: frink.source.CurrencySource.CurrencyUnit.getScale():frink.numeric.Numeric");
        }

        public DimensionList getDimensionList() {
            return this.dimensions;
        }
    }

    public static void setUseCacheOnly(boolean z) {
        useCacheOnly = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void findUniqueNames() {
        if (this.currencyHash != null) {
            Enumeration<String> keys = this.currencyHash.keys();
            UnitManager unitManager = this.env.getUnitManager();
            while (keys.hasMoreElements()) {
                String nextElement = keys.nextElement();
                UsedCurrencyPair usedCurrencyPair = this.currencyHash.get(nextElement);
                if (usedCurrencyPair.getCount() == 1) {
                    String replace = nextElement.replace(' ', '_');
                    if (unitManager.getUnit(replace) == null) {
                        this.nameHash.put(replace, usedCurrencyPair.getCode());
                    }
                    String lowerCase = replace.toLowerCase();
                    if (unitManager.getUnit(lowerCase) == null) {
                        this.nameHash.put(lowerCase, usedCurrencyPair.getCode());
                    }
                }
            }
            this.currencyHash = null;
        }
    }

    /* access modifiers changed from: private */
    public Numeric getValueFromCache(String str, Numeric numeric) {
        readCache();
        Numeric numeric2 = cache.get(str);
        if (numeric2 == null) {
            return null;
        }
        try {
            if (!str.equals(this.baseCurrency)) {
                numeric2 = NumericMath.divide(numeric2, cache.get(this.baseCurrency));
            }
            if (numeric != null) {
                return NumericMath.multiply(numeric2, numeric);
            }
            return numeric2;
        } catch (NumericException e) {
            return null;
        }
    }

    private synchronized void readCache() {
        if (cache == null) {
            cache = new Hashtable<>();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/data/currencycache.txt")));
            try {
                cacheDate = bufferedReader.readLine();
                cacheBaseCurrency = bufferedReader.readLine();
                System.out.println("Warning: reading currency values from cache dated " + cacheDate);
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (readLine.length() > 4) {
                        cache.put(readLine.substring(0, 3), new FrinkFloat(readLine.substring(4)));
                    }
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        System.out.println("Error in closing currency cache.  That's weird.");
                    }
                }
            } catch (IOException e2) {
                System.out.println("Could not read currency cache: " + e2);
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e3) {
                        System.out.println("Error in closing currency cache.  That's weird.");
                    }
                }
            } catch (Throwable th) {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e4) {
                        System.out.println("Error in closing currency cache.  That's weird.");
                    }
                }
                throw th;
            }
        }
        return;
    }

    private static class UsedCurrencyPair {
        private String code;
        private int count = 1;

        public UsedCurrencyPair(String str) {
            this.code = str;
        }

        public void incrementCount() {
            this.count++;
        }

        public int getCount() {
            return this.count;
        }

        public String getCode() {
            return this.code;
        }
    }
}
