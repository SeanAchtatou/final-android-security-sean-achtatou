package frink.source;

import frink.numeric.FrinkFloat;
import frink.numeric.FrinkInt;
import frink.numeric.FrinkRational;
import frink.numeric.InvalidDenominatorException;
import frink.numeric.Numeric;
import frink.numeric.NumericException;
import frink.numeric.NumericMath;
import frink.units.BasicUnit;
import frink.units.DimensionList;
import frink.units.Source;
import frink.units.Unit;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

public class CPISource implements Source<Unit> {
    private static final boolean DEBUG = false;
    private static final String altCentPrefix = "cents_";
    private static final String altPrefix = "dollars_";
    private static Numeric centFactor = null;
    private static final String centPrefix = "cent_";
    private static final String intlPrefix = "USD_";
    private static final String prefix = "dollar_";
    private static final String staticFile = "/data/cpiai.txt";
    /* access modifiers changed from: private */
    public Numeric currRate;
    /* access modifiers changed from: private */
    public Unit dollar;
    private boolean fetched = DEBUG;
    private Hashtable<String, Unit> unitHash = new Hashtable<>();

    static {
        try {
            centFactor = FrinkRational.construct(FrinkInt.ONE, FrinkInt.construct(100));
        } catch (InvalidDenominatorException e) {
        }
    }

    public CPISource(Unit unit) {
        this.dollar = unit;
    }

    public String getName() {
        return "CPISource";
    }

    public Enumeration<String> getNames() {
        return this.unitHash.keys();
    }

    public Unit get(String str) {
        if (!this.fetched) {
            if (!str.startsWith(prefix) && !str.startsWith(altPrefix) && !str.startsWith(intlPrefix) && !str.startsWith("usd_") && !str.startsWith(centPrefix) && !str.startsWith(altCentPrefix)) {
                return null;
            }
            initRates();
        }
        return this.unitHash.get(str);
    }

    public synchronized void initRates() {
        if (!this.fetched) {
            BufferedReader openFTP = openFTP();
            if (openFTP != null) {
                parseFile(openFTP);
            } else {
                BufferedReader openResource = openResource();
                if (openResource != null) {
                    parseFile(openResource);
                }
            }
        }
    }

    private BufferedReader openResource() {
        URL resource = getClass().getResource(staticFile);
        if (resource == null) {
            return null;
        }
        try {
            return new BufferedReader(new InputStreamReader(resource.openConnection().getInputStream()));
        } catch (IOException e) {
            System.out.println("Error when reading CPI file from resource: " + e);
            return null;
        }
    }

    private BufferedReader openFTP() {
        if (this.fetched) {
            return null;
        }
        try {
            return new BufferedReader(new InputStreamReader(new URL("ftp://ftp.bls.gov/pub/special.requests/cpi/cpiai.txt").openConnection().getInputStream()));
        } catch (MalformedURLException e) {
            System.err.println("Malformed URL when retrieving CPI data: " + e);
            return null;
        } catch (IOException e2) {
            System.err.println("Error when reading Consumer Price Index file from FTP site.\nWill use static file for historical U.S. currency conversions.");
            return null;
        }
    }

    private void parseFile(BufferedReader bufferedReader) {
        String num;
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    try {
                        int parseInt = Integer.parseInt(readLine.substring(5, 9));
                        if (parseInt >= 1913) {
                            for (int i = 1; i <= 12; i++) {
                                FrinkFloat frinkFloat = new FrinkFloat(readLine.substring((i * 9) + 1, (i + 1) * 9).trim());
                                CPIUnit cPIUnit = new CPIUnit(frinkFloat);
                                if (i < 10) {
                                    num = "0" + i;
                                } else {
                                    num = Integer.toString(i);
                                }
                                this.unitHash.put(prefix + parseInt + "_" + num, cPIUnit);
                                this.unitHash.put(altPrefix + parseInt + "_" + num, cPIUnit);
                                this.unitHash.put(intlPrefix + parseInt + "_" + num, cPIUnit);
                                CPIUnit cPIUnit2 = new CPIUnit(NumericMath.divide(frinkFloat, centFactor));
                                this.unitHash.put(centPrefix + parseInt + "_" + num, cPIUnit2);
                                this.unitHash.put(altCentPrefix + parseInt + "_" + num, cPIUnit2);
                                this.currRate = frinkFloat;
                            }
                            FrinkFloat frinkFloat2 = new FrinkFloat(readLine.substring(118, 126).trim());
                            CPIUnit cPIUnit3 = new CPIUnit(frinkFloat2);
                            this.unitHash.put(prefix + parseInt, cPIUnit3);
                            this.unitHash.put(altPrefix + parseInt, cPIUnit3);
                            CPIUnit cPIUnit4 = new CPIUnit(NumericMath.divide(frinkFloat2, centFactor));
                            this.unitHash.put(centPrefix + parseInt, cPIUnit4);
                            this.unitHash.put(altCentPrefix + parseInt, cPIUnit4);
                        }
                    } catch (NumericException | NumberFormatException | StringIndexOutOfBoundsException e) {
                    }
                } else {
                    try {
                        break;
                    } catch (IOException e2) {
                    }
                }
            } catch (IOException e3) {
                System.out.println("IO Exception: " + e3);
                try {
                    bufferedReader.close();
                    return;
                } catch (IOException e4) {
                    return;
                }
            } catch (Throwable th) {
                try {
                    bufferedReader.close();
                } catch (IOException e5) {
                }
                throw th;
            }
        }
        bufferedReader.close();
        this.fetched = true;
        addPreCPIRates();
    }

    private void addRate(Unit unit, int i, double d) {
        try {
            BasicUnit basicUnit = new BasicUnit(NumericMath.divide(unit.getScale(), new FrinkFloat(d)), this.dollar.getDimensionList());
            this.unitHash.put(prefix + i, basicUnit);
            this.unitHash.put(altPrefix + i, basicUnit);
            BasicUnit basicUnit2 = new BasicUnit(NumericMath.multiply(basicUnit.getScale(), centFactor), basicUnit.getDimensionList());
            this.unitHash.put(centPrefix + i, basicUnit2);
            this.unitHash.put(altCentPrefix + i, basicUnit2);
        } catch (NumericException e) {
            System.err.println("Unexpected exception in CPISource.addRate: " + e);
        }
    }

    private void addPreCPIRates() {
        Unit unit = this.unitHash.get("dollar_2000");
        addRate(unit, 1700, 0.063d);
        addRate(unit, 1701, 0.068d);
        addRate(unit, 1702, 0.066d);
        addRate(unit, 1703, 0.057d);
        addRate(unit, 1704, 0.052d);
        addRate(unit, 1705, 0.05d);
        addRate(unit, 1706, 0.054d);
        addRate(unit, 1707, 0.058d);
        addRate(unit, 1708, 0.061d);
        addRate(unit, 1709, 0.056d);
        addRate(unit, 1710, 0.049d);
        addRate(unit, 1711, 0.051d);
        addRate(unit, 1712, 0.058d);
        addRate(unit, 1713, 0.062d);
        addRate(unit, 1714, 0.062d);
        addRate(unit, 1715, 0.043d);
        addRate(unit, 1716, 0.035d);
        addRate(unit, 1717, 0.037d);
        addRate(unit, 1718, 0.044d);
        addRate(unit, 1719, 0.045d);
        addRate(unit, 1720, 0.037d);
        addRate(unit, 1721, 0.034d);
        addRate(unit, 1722, 0.036d);
        addRate(unit, 1723, 0.037d);
        addRate(unit, 1724, 0.039d);
        addRate(unit, 1725, 0.046d);
        addRate(unit, 1726, 0.045d);
        addRate(unit, 1727, 0.042d);
        addRate(unit, 1728, 0.039d);
        addRate(unit, 1729, 0.039d);
        addRate(unit, 1730, 0.039d);
        addRate(unit, 1731, 0.034d);
        addRate(unit, 1732, 0.033d);
        addRate(unit, 1733, 0.032d);
        addRate(unit, 1734, 0.033d);
        addRate(unit, 1735, 0.033d);
        addRate(unit, 1736, 0.032d);
        addRate(unit, 1737, 0.032d);
        addRate(unit, 1738, 0.034d);
        addRate(unit, 1739, 0.031d);
        addRate(unit, 1740, 0.032d);
        addRate(unit, 1741, 0.044d);
        addRate(unit, 1742, 0.039d);
        addRate(unit, 1743, 0.034d);
        addRate(unit, 1744, 0.032d);
        addRate(unit, 1745, 0.031d);
        addRate(unit, 1746, 0.032d);
        addRate(unit, 1747, 0.034d);
        addRate(unit, 1748, 0.04d);
        addRate(unit, 1749, 0.041d);
        addRate(unit, 1750, 0.041d);
        addRate(unit, 1751, 0.041d);
        addRate(unit, 1752, 0.042d);
        addRate(unit, 1753, 0.041d);
        addRate(unit, 1754, 0.039d);
        addRate(unit, 1755, 0.038d);
        addRate(unit, 1756, 0.037d);
        addRate(unit, 1757, 0.039d);
        addRate(unit, 1758, 0.042d);
        addRate(unit, 1759, 0.048d);
        addRate(unit, 1760, 0.047d);
        addRate(unit, 1761, 0.044d);
        addRate(unit, 1762, 0.046d);
        addRate(unit, 1763, 0.046d);
        addRate(unit, 1764, 0.043d);
        addRate(unit, 1765, 0.043d);
        addRate(unit, 1766, 0.048d);
        addRate(unit, 1767, 0.046d);
        addRate(unit, 1768, 0.044d);
        addRate(unit, 1769, 0.045d);
        addRate(unit, 1770, 0.049d);
        addRate(unit, 1771, 0.047d);
        addRate(unit, 1772, 0.053d);
        addRate(unit, 1773, 0.049d);
        addRate(unit, 1774, 0.047d);
        addRate(unit, 1775, 0.045d);
        addRate(unit, 1776, 0.051d);
        addRate(unit, 1777, 0.062d);
        addRate(unit, 1778, 0.081d);
        addRate(unit, 1779, 0.071d);
        addRate(unit, 1780, 0.08d);
        addRate(unit, 1781, 0.065d);
        addRate(unit, 1782, 0.071d);
        addRate(unit, 1783, 0.062d);
        addRate(unit, 1784, 0.06d);
        addRate(unit, 1785, 0.057d);
        addRate(unit, 1786, 0.055d);
        addRate(unit, 1787, 0.054d);
        addRate(unit, 1788, 0.052d);
        addRate(unit, 1789, 0.051d);
        addRate(unit, 1790, 0.053d);
        addRate(unit, 1791, 0.055d);
        addRate(unit, 1792, 0.056d);
        addRate(unit, 1793, 0.058d);
        addRate(unit, 1794, 0.064d);
        addRate(unit, 1795, 0.073d);
        addRate(unit, 1796, 0.077d);
        addRate(unit, 1797, 0.074d);
        addRate(unit, 1798, 0.072d);
        addRate(unit, 1799, 0.072d);
        addRate(unit, 1800, 0.073d);
        addRate(unit, 1801, 0.074d);
        addRate(unit, 1802, 0.063d);
        addRate(unit, 1803, 0.066d);
        addRate(unit, 1804, 0.069d);
        addRate(unit, 1805, 0.068d);
        addRate(unit, 1806, 0.071d);
        addRate(unit, 1807, 0.067d);
        addRate(unit, 1808, 0.073d);
        addRate(unit, 1809, 0.072d);
        addRate(unit, 1810, 0.072d);
        addRate(unit, 1811, 0.077d);
        addRate(unit, 1812, 0.078d);
        addRate(unit, 1813, 0.093d);
        addRate(unit, 1814, 0.102d);
        addRate(unit, 1815, 0.09d);
        addRate(unit, 1816, 0.082d);
        addRate(unit, 1817, 0.078d);
        addRate(unit, 1818, 0.074d);
        addRate(unit, 1819, 0.074d);
        addRate(unit, 1820, 0.068d);
        addRate(unit, 1821, 0.066d);
        addRate(unit, 1822, 0.068d);
        addRate(unit, 1823, 0.061d);
        addRate(unit, 1824, 0.056d);
        addRate(unit, 1825, 0.058d);
        addRate(unit, 1826, 0.058d);
        addRate(unit, 1827, 0.058d);
        addRate(unit, 1828, 0.055d);
        addRate(unit, 1829, 0.054d);
        addRate(unit, 1830, 0.054d);
        addRate(unit, 1831, 0.05d);
        addRate(unit, 1832, 0.05d);
        addRate(unit, 1833, 0.049d);
        addRate(unit, 1834, 0.05d);
        addRate(unit, 1835, 0.051d);
        addRate(unit, 1836, 0.054d);
        addRate(unit, 1837, 0.056d);
        addRate(unit, 1838, 0.054d);
        addRate(unit, 1839, 0.054d);
        addRate(unit, 1840, 0.05d);
        addRate(unit, 1841, 0.051d);
        addRate(unit, 1842, 0.048d);
        addRate(unit, 1843, 0.043d);
        addRate(unit, 1844, 0.044d);
        addRate(unit, 1845, 0.044d);
        addRate(unit, 1846, 0.045d);
        addRate(unit, 1847, 0.048d);
        addRate(unit, 1848, 0.046d);
        addRate(unit, 1849, 0.045d);
        addRate(unit, 1850, 0.046d);
        addRate(unit, 1851, 0.045d);
        addRate(unit, 1852, 0.045d);
        addRate(unit, 1853, 0.045d);
        addRate(unit, 1854, 0.049d);
        addRate(unit, 1855, 0.05d);
        addRate(unit, 1856, 0.049d);
        addRate(unit, 1857, 0.051d);
        addRate(unit, 1858, 0.048d);
        addRate(unit, 1859, 0.049d);
        addRate(unit, 1860, 0.049d);
        addRate(unit, 1861, 0.051d);
        addRate(unit, 1862, 0.059d);
        addRate(unit, 1863, 0.073d);
        addRate(unit, 1864, 0.092d);
        addRate(unit, 1865, 0.095d);
        addRate(unit, 1866, 0.093d);
        addRate(unit, 1867, 0.086d);
        addRate(unit, 1868, 0.083d);
        addRate(unit, 1869, 0.08d);
        addRate(unit, 1870, 0.076d);
        addRate(unit, 1871, 0.071d);
        addRate(unit, 1872, 0.071d);
        addRate(unit, 1873, 0.07d);
        addRate(unit, 1874, 0.066d);
        addRate(unit, 1875, 0.064d);
        addRate(unit, 1876, 0.063d);
        addRate(unit, 1877, 0.061d);
        addRate(unit, 1878, 0.058d);
        addRate(unit, 1879, 0.058d);
        addRate(unit, 1880, 0.06d);
        addRate(unit, 1881, 0.06d);
        addRate(unit, 1882, 0.06d);
        addRate(unit, 1883, 0.059d);
        addRate(unit, 1884, 0.057d);
        addRate(unit, 1885, 0.056d);
        addRate(unit, 1886, 0.055d);
        addRate(unit, 1887, 0.055d);
        addRate(unit, 1888, 0.055d);
        addRate(unit, 1889, 0.054d);
        addRate(unit, 1890, 0.053d);
        addRate(unit, 1891, 0.053d);
        addRate(unit, 1892, 0.053d);
        addRate(unit, 1893, 0.052d);
        addRate(unit, 1894, 0.05d);
        addRate(unit, 1895, 0.049d);
        addRate(unit, 1896, 0.049d);
        addRate(unit, 1897, 0.049d);
        addRate(unit, 1898, 0.049d);
        addRate(unit, 1899, 0.049d);
        addRate(unit, 1900, 0.049d);
        addRate(unit, 1901, 0.046d);
        addRate(unit, 1902, 0.052d);
        addRate(unit, 1903, 0.052d);
        addRate(unit, 1904, 0.052d);
        addRate(unit, 1905, 0.052d);
        addRate(unit, 1906, 0.052d);
        addRate(unit, 1907, 0.052d);
        addRate(unit, 1908, 0.052d);
        addRate(unit, 1909, 0.052d);
        addRate(unit, 1910, 0.052d);
        addRate(unit, 1911, 0.052d);
        addRate(unit, 1912, 0.058d);
    }

    private class CPIUnit implements Unit {
        private Numeric scaled;
        private Numeric unscaled;

        private CPIUnit(Numeric numeric) {
            this.unscaled = numeric;
        }

        public DimensionList getDimensionList() {
            return CPISource.this.dollar.getDimensionList();
        }

        public Numeric getScale() {
            if (this.scaled != null) {
                return this.scaled;
            }
            try {
                this.scaled = NumericMath.multiply(CPISource.this.dollar.getScale(), NumericMath.divide(CPISource.this.currRate, this.unscaled));
                return this.scaled;
            } catch (NumericException e) {
                System.err.println("Unexpected exception in CPISource.getScale: " + e);
                return null;
            }
        }
    }
}
