package frink.java;

import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.TerminalExpression;
import frink.function.FunctionSource;
import frink.symbolic.MatchingContext;

public class SingleJavaObject extends TerminalExpression implements JavaObject {
    private JavaObjectFieldSource contextFrame;
    private FunctionSource functionSrc;
    private Object obj;

    public SingleJavaObject(Object obj2) {
        this.obj = obj2;
        this.functionSrc = JavaObjectFunctionSource.getFunctionSource(obj2.getClass());
        this.contextFrame = new JavaObjectFieldSource(obj2, this);
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return this.functionSrc;
    }

    public ContextFrame getContextFrame(Environment environment) {
        return this.contextFrame;
    }

    public Object getObject() {
        return this.obj;
    }

    public boolean isA(String str) {
        return str.equals(getObject().getClass().getName());
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public String toString(Environment environment, boolean z) {
        return "JavaObject:" + getObject().getClass().getName();
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof SingleJavaObject) || this.obj != ((SingleJavaObject) expression).obj) {
            return false;
        }
        return true;
    }

    public String getExpressionType() {
        return "SingleJavaObject:" + this.obj.getClass().getName();
    }
}
