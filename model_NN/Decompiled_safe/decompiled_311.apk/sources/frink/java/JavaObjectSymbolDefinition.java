package frink.java;

import frink.expr.CannotAssignException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.SymbolDefinition;
import java.lang.reflect.Field;

public class JavaObjectSymbolDefinition implements SymbolDefinition {
    private Field field;
    private Object object;

    public JavaObjectSymbolDefinition(Object obj, Field field2) {
        this.object = obj;
        this.field = field2;
    }

    public void setValue(Expression expression) throws CannotAssignException {
        Object obj;
        try {
            obj = JavaMapper.map(expression, this.field.getType(), null);
        } catch (InvalidArgumentException e) {
            obj = null;
        }
        if (obj == null) {
            throw new CannotAssignException("JavaObjectSymbolDefinition: Could not convert types.", expression);
        }
        try {
            this.field.set(this.object, obj);
        } catch (IllegalAccessException e2) {
            throw new CannotAssignException("JavaObjectSymbolDefintion.  Cannot assign to value:" + e2, expression);
        }
    }

    public Expression getValue() {
        try {
            return JavaMapper.map(this.field.get(this.object));
        } catch (IllegalAccessException e) {
            System.err.println("JavaObjectSymbolDefintion.getValue():  Cannot read value:" + e);
            return null;
        }
    }

    public boolean isConstant() {
        return false;
    }
}
