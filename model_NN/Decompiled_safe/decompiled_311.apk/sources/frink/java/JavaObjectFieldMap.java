package frink.java;

import java.lang.reflect.Field;
import java.util.Hashtable;

public class JavaObjectFieldMap {
    private static Hashtable<Class, JavaObjectFieldMap> classMap = new Hashtable<>();
    private Class classObj;
    private Hashtable<String, Field> fieldMap;

    private JavaObjectFieldMap(Class cls) {
        this.classObj = cls;
        populateFields();
    }

    private void populateFields() {
        this.fieldMap = new Hashtable<>();
        for (Field field : this.classObj.getFields()) {
            this.fieldMap.put(field.getName(), field);
        }
    }

    public Field getField(String str) {
        return this.fieldMap.get(str);
    }

    public static JavaObjectFieldMap getFieldMap(Class cls) {
        JavaObjectFieldMap javaObjectFieldMap = classMap.get(cls);
        if (javaObjectFieldMap != null) {
            return javaObjectFieldMap;
        }
        JavaObjectFieldMap javaObjectFieldMap2 = new JavaObjectFieldMap(cls);
        classMap.put(cls, javaObjectFieldMap2);
        return javaObjectFieldMap2;
    }
}
