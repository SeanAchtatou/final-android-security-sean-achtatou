package frink.java;

import java.lang.reflect.Method;

public class MethodWrapper {
    private JavaFunctionDefinition functionDef = null;
    private Method method;

    public MethodWrapper(Method method2) {
        this.method = method2;
    }

    public void reset(Method method2, JavaFunctionDefinition javaFunctionDefinition) {
        this.method = method2;
        this.functionDef = javaFunctionDefinition;
    }

    public Method getMethod() {
        return this.method;
    }

    public JavaFunctionDefinition getFunctionDefinition() {
        if (this.functionDef == null) {
            this.functionDef = new JavaFunctionDefinition(this);
        }
        return this.functionDef;
    }
}
