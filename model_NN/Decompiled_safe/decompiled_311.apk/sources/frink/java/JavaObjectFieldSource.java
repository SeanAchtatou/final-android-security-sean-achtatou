package frink.java;

import frink.expr.BasicSymbolDefinition;
import frink.expr.CannotAssignException;
import frink.expr.Constraint;
import frink.expr.ContextFrame;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.SymbolDefinition;
import frink.object.ThisContextFrame;
import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.Vector;

public class JavaObjectFieldSource implements ContextFrame, ThisContextFrame {
    private static final boolean DEBUG = false;
    private JavaObjectFieldMap fieldMap;
    private BasicSymbolDefinition joThis;
    private Object object;
    private Hashtable<String, JavaObjectSymbolDefinition> symbolMap = null;

    public JavaObjectFieldSource(Object obj, JavaObject javaObject) {
        this.object = obj;
        this.joThis = new BasicSymbolDefinition(javaObject);
        this.fieldMap = JavaObjectFieldMap.getFieldMap(obj.getClass());
    }

    public SymbolDefinition getSymbolDefinition(String str, boolean z, Environment environment) throws CannotAssignException {
        JavaObjectSymbolDefinition javaObjectSymbolDefinition;
        if (str.equals("this")) {
            return this.joThis;
        }
        if (this.symbolMap != null && (javaObjectSymbolDefinition = this.symbolMap.get(str)) != null) {
            return javaObjectSymbolDefinition;
        }
        Field field = this.fieldMap.getField(str);
        if (field != null) {
            JavaObjectSymbolDefinition javaObjectSymbolDefinition2 = new JavaObjectSymbolDefinition(this.object, field);
            if (this.symbolMap == null) {
                this.symbolMap = new Hashtable<>();
            }
            this.symbolMap.put(str, javaObjectSymbolDefinition2);
            return javaObjectSymbolDefinition2;
        } else if (!z) {
            return null;
        } else {
            throw new CannotAssignException("JavaObjectFieldSource: Cannot create field " + str + " in object.", null);
        }
    }

    public SymbolDefinition setSymbolDefinition(String str, Expression expression, Environment environment) throws CannotAssignException {
        JavaObjectSymbolDefinition javaObjectSymbolDefinition;
        if (this.symbolMap == null || (javaObjectSymbolDefinition = this.symbolMap.get(str)) == null) {
            Field field = this.fieldMap.getField(str);
            if (field == null) {
                throw new CannotAssignException("JavaObjectFieldSource: Cannot set nonexistent field " + str + " in object.", null);
            }
            JavaObjectSymbolDefinition javaObjectSymbolDefinition2 = new JavaObjectSymbolDefinition(this.object, field);
            if (this.symbolMap == null) {
                this.symbolMap = new Hashtable<>();
            }
            javaObjectSymbolDefinition2.setValue(expression);
            this.symbolMap.put(str, javaObjectSymbolDefinition2);
            return javaObjectSymbolDefinition2;
        }
        javaObjectSymbolDefinition.setValue(expression);
        return javaObjectSymbolDefinition;
    }

    public SymbolDefinition declareVariable(String str, Vector<Constraint> vector, Expression expression, Environment environment) throws CannotAssignException {
        throw new CannotAssignException("JavaObjectFieldSource: cannot declare new variables in Java objects.  Field was " + str, null);
    }

    public boolean canCreateNewVariables() {
        return false;
    }

    public boolean canModifyVariables() {
        return true;
    }

    public JavaObjectFieldSource deepCopy(int i) {
        return this;
    }

    public void setThis(Expression expression) {
    }
}
