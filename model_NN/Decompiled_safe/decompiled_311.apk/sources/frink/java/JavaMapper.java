package frink.java;

import frink.errors.NotAnIntegerException;
import frink.errors.NotRealException;
import frink.expr.BasicStringExpression;
import frink.expr.BooleanExpression;
import frink.expr.DimensionlessUnitExpression;
import frink.expr.Environment;
import frink.expr.Expression;
import frink.expr.FrinkBoolean;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.expr.StringExpression;
import frink.expr.UndefExpression;
import frink.expr.UnitExpression;
import frink.expr.VoidExpression;
import frink.function.BuiltinFunctionSource;
import frink.units.Unit;
import frink.units.UnitMath;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigInteger;

public class JavaMapper {
    private static final boolean DEBUG = false;

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009c, code lost:
        if (r8.equals(r2) != false) goto L_0x009e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean canConvert(frink.expr.Expression r7, java.lang.Class r8, frink.expr.Environment r9) {
        /*
            r4 = 0
            r3 = 1
            java.lang.Class<java.lang.Double> r6 = java.lang.Double.class
            java.lang.Class<java.lang.Comparable> r2 = java.lang.Comparable.class
            java.lang.Class<java.lang.Byte> r5 = java.lang.Byte.class
            frink.expr.UndefExpression r1 = frink.expr.UndefExpression.UNDEF
            if (r7 != r1) goto L_0x000e
            r1 = r3
        L_0x000d:
            return r1
        L_0x000e:
            boolean r1 = r7 instanceof frink.expr.UnitExpression
            if (r1 == 0) goto L_0x008e
            java.lang.Class r1 = java.lang.Integer.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Integer> r1 = java.lang.Integer.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class r1 = java.lang.Double.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Double> r1 = java.lang.Double.class
            boolean r1 = r8.equals(r6)
            if (r1 != 0) goto L_0x008a
            java.lang.Class r1 = java.lang.Float.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Float> r1 = java.lang.Float.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class r1 = java.lang.Long.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Long> r1 = java.lang.Long.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class r1 = java.lang.Short.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Short> r1 = java.lang.Short.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class r1 = java.lang.Byte.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Byte> r1 = java.lang.Byte.class
            boolean r1 = r8.equals(r5)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.math.BigInteger> r1 = java.math.BigInteger.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Number> r1 = java.lang.Number.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x008a
            java.lang.Class<java.lang.Comparable> r1 = java.lang.Comparable.class
            boolean r1 = r8.equals(r2)
            if (r1 == 0) goto L_0x008c
        L_0x008a:
            r1 = r3
            goto L_0x000d
        L_0x008c:
            r1 = r4
            goto L_0x000d
        L_0x008e:
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            boolean r1 = r8.equals(r1)
            if (r1 != 0) goto L_0x009e
            java.lang.Class<java.lang.Comparable> r1 = java.lang.Comparable.class
            boolean r1 = r8.equals(r2)
            if (r1 == 0) goto L_0x00a5
        L_0x009e:
            boolean r1 = r7 instanceof frink.expr.StringExpression
            if (r1 == 0) goto L_0x00a5
            r1 = r3
            goto L_0x000d
        L_0x00a5:
            java.lang.Class r1 = java.lang.Boolean.TYPE
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x00b7
            boolean r1 = r7 instanceof frink.expr.BooleanExpression
            if (r1 == 0) goto L_0x00b4
            r1 = r3
            goto L_0x000d
        L_0x00b4:
            r1 = r4
            goto L_0x000d
        L_0x00b7:
            boolean r1 = r8.isArray()
            if (r1 == 0) goto L_0x00c8
            boolean r1 = r7 instanceof frink.expr.ListExpression
            if (r1 != 0) goto L_0x00c5
            boolean r1 = r7 instanceof frink.expr.EnumeratingExpression
            if (r1 == 0) goto L_0x00c8
        L_0x00c5:
            r1 = r3
            goto L_0x000d
        L_0x00c8:
            boolean r1 = r7 instanceof frink.java.JavaObject
            if (r1 == 0) goto L_0x017e
            r0 = r7
            frink.java.JavaObject r0 = (frink.java.JavaObject) r0
            r1 = r0
            java.lang.Object r1 = r1.getObject()
            if (r8 != 0) goto L_0x00d9
            r1 = r3
            goto L_0x000d
        L_0x00d9:
            java.lang.Class r1 = r1.getClass()
            boolean r2 = r8.isAssignableFrom(r1)
            if (r2 == 0) goto L_0x00e6
            r1 = r3
            goto L_0x000d
        L_0x00e6:
            java.lang.Class r2 = java.lang.Integer.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x00f9
            java.lang.Class<java.lang.Integer> r2 = java.lang.Integer.class
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x00f9
            r1 = r3
            goto L_0x000d
        L_0x00f9:
            java.lang.Class r2 = java.lang.Double.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x010c
            java.lang.Class<java.lang.Double> r2 = java.lang.Double.class
            boolean r2 = r1.equals(r6)
            if (r2 == 0) goto L_0x010c
            r1 = r3
            goto L_0x000d
        L_0x010c:
            java.lang.Class r2 = java.lang.Float.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x011f
            java.lang.Class<java.lang.Float> r2 = java.lang.Float.class
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x011f
            r1 = r3
            goto L_0x000d
        L_0x011f:
            java.lang.Class r2 = java.lang.Long.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x0132
            java.lang.Class<java.lang.Long> r2 = java.lang.Long.class
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0132
            r1 = r3
            goto L_0x000d
        L_0x0132:
            java.lang.Class r2 = java.lang.Character.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x0145
            java.lang.Class<java.lang.Character> r2 = java.lang.Character.class
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0145
            r1 = r3
            goto L_0x000d
        L_0x0145:
            java.lang.Class r2 = java.lang.Short.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x0158
            java.lang.Class<java.lang.Short> r2 = java.lang.Short.class
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0158
            r1 = r3
            goto L_0x000d
        L_0x0158:
            java.lang.Class r2 = java.lang.Byte.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x016b
            java.lang.Class<java.lang.Byte> r2 = java.lang.Byte.class
            boolean r2 = r1.equals(r5)
            if (r2 == 0) goto L_0x016b
            r1 = r3
            goto L_0x000d
        L_0x016b:
            java.lang.Class r2 = java.lang.Boolean.TYPE
            boolean r2 = r8.equals(r2)
            if (r2 == 0) goto L_0x018b
            java.lang.Class<java.lang.Boolean> r2 = java.lang.Boolean.class
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x018b
            r1 = r3
            goto L_0x000d
        L_0x017e:
            java.lang.Class r1 = r7.getClass()
            boolean r1 = r8.isAssignableFrom(r1)
            if (r1 == 0) goto L_0x018b
            r1 = r3
            goto L_0x000d
        L_0x018b:
            boolean r1 = r7 instanceof frink.expr.StringExpression
            if (r1 == 0) goto L_0x019a
            java.lang.Class<java.lang.String> r1 = java.lang.String.class
            boolean r1 = r8.isAssignableFrom(r1)
            if (r1 == 0) goto L_0x019a
            r1 = r3
            goto L_0x000d
        L_0x019a:
            r1 = r4
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: frink.java.JavaMapper.canConvert(frink.expr.Expression, java.lang.Class, frink.expr.Environment):boolean");
    }

    public static Object map(Expression expression, Class cls, Environment environment) throws InvalidArgumentException {
        if (expression == UndefExpression.UNDEF) {
            return null;
        }
        if (cls.equals(Integer.TYPE) || cls.equals(Integer.class)) {
            if (expression instanceof UnitExpression) {
                try {
                    return new Integer(BuiltinFunctionSource.getIntegerValue(expression));
                } catch (NotAnIntegerException e) {
                    throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".\n " + e, expression);
                }
            } else if (expression instanceof JavaObject) {
                return convertTo((JavaObject) expression, cls);
            } else {
                throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
            }
        } else if (cls.equals(Long.TYPE) || cls.equals(Long.class)) {
            if (expression instanceof UnitExpression) {
                try {
                    return new Long(BuiltinFunctionSource.getLongValue(expression));
                } catch (NotAnIntegerException e2) {
                    throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".\n " + e2, expression);
                }
            } else if (expression instanceof JavaObject) {
                return convertTo((JavaObject) expression, cls);
            } else {
                throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
            }
        } else if (cls.equals(Double.TYPE) || cls.equals(Double.class)) {
            if (expression instanceof UnitExpression) {
                Unit unit = ((UnitExpression) expression).getUnit();
                if (!UnitMath.isDimensionless(unit)) {
                    throw new InvalidArgumentException("Cannot convert non-dimensionless value to " + cls.getName() + ".", expression);
                }
                try {
                    return new Double(unit.getScale().doubleValue());
                } catch (NotRealException e3) {
                    throw new InvalidArgumentException("Cannot convert non-real number to " + cls.getName() + ".", expression);
                }
            } else if (expression instanceof JavaObject) {
                return convertTo((JavaObject) expression, cls);
            } else {
                throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
            }
        } else if (cls.equals(Float.TYPE) || cls.equals(Float.class)) {
            if (expression instanceof UnitExpression) {
                Unit unit2 = ((UnitExpression) expression).getUnit();
                if (!UnitMath.isDimensionless(unit2)) {
                    throw new InvalidArgumentException("Cannot convert non-dimensionless value to " + cls.getName() + ".", expression);
                }
                try {
                    double doubleValue = unit2.getScale().doubleValue();
                    if (doubleValue <= 3.4028234663852886E38d && doubleValue >= 1.401298464324817E-45d) {
                        return new Float(doubleValue);
                    }
                    throw new InvalidArgumentException("Value " + doubleValue + " cannot fit in a float. " + cls.getName() + ".", expression);
                } catch (NotRealException e4) {
                    throw new InvalidArgumentException("Cannot convert non-real number to " + cls.getName() + ".", expression);
                }
            } else if (expression instanceof JavaObject) {
                return convertTo((JavaObject) expression, cls);
            } else {
                throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
            }
        } else if (cls.equals(String.class) && (expression instanceof StringExpression)) {
            return ((StringExpression) expression).getString();
        } else {
            if (cls.equals(Short.TYPE) || cls.equals(Short.class)) {
                if (expression instanceof UnitExpression) {
                    try {
                        return new Short(BuiltinFunctionSource.getShortValue(expression));
                    } catch (NotAnIntegerException e5) {
                        throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".\n " + e5, expression);
                    }
                } else if (expression instanceof JavaObject) {
                    return convertTo((JavaObject) expression, cls);
                } else {
                    throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
                }
            } else if (cls.equals(Byte.TYPE) || cls.equals(Byte.class)) {
                if (expression instanceof UnitExpression) {
                    try {
                        return new Byte(BuiltinFunctionSource.getByteValue(expression));
                    } catch (NotAnIntegerException e6) {
                        throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".\n " + e6, expression);
                    }
                } else if (expression instanceof JavaObject) {
                    return convertTo((JavaObject) expression, cls);
                } else {
                    throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
                }
            } else if (cls.equals(Boolean.TYPE) || cls.equals(Boolean.class)) {
                if (expression instanceof BooleanExpression) {
                    return Boolean.valueOf(((BooleanExpression) expression).getBoolean());
                }
                if (expression instanceof JavaObject) {
                    return convertTo((JavaObject) expression, cls);
                }
                throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".", expression);
            } else if (!cls.equals(BigInteger.class) || !(expression instanceof UnitExpression)) {
                if (expression instanceof JavaObject) {
                    Object object = ((JavaObject) expression).getObject();
                    if (cls.isAssignableFrom(object.getClass())) {
                        return object;
                    }
                } else if (cls.isAssignableFrom(expression.getClass())) {
                    return expression;
                }
                if (cls.isArray() && (expression instanceof ListExpression)) {
                    Class<?> componentType = cls.getComponentType();
                    int childCount = expression.getChildCount();
                    Object newInstance = Array.newInstance(componentType, childCount);
                    int i = 0;
                    while (i < childCount) {
                        try {
                            Array.set(newInstance, i, map(expression.getChild(i), componentType, environment));
                            i++;
                        } catch (InvalidChildException e7) {
                            throw new InvalidArgumentException("JavaMapper: when mapping Frink list to Java array, got unexpected InvalidChildException", expression);
                        }
                    }
                    return newInstance;
                } else if ((expression instanceof StringExpression) && cls.isAssignableFrom(String.class)) {
                    return ((StringExpression) expression).getString();
                } else {
                    throw new InvalidArgumentException("JavaMapper.map:  Cannot convert value to " + cls.getName() + ".", expression);
                }
            } else {
                try {
                    return BuiltinFunctionSource.getBigIntegerValue(expression);
                } catch (NotAnIntegerException e8) {
                    throw new InvalidArgumentException("Cannot convert value " + environment.format(expression) + " (" + expression + ") to " + cls.getName() + ".\n " + e8, expression);
                }
            }
        }
    }

    public static Expression map(Object obj) {
        if (obj == null) {
            return UndefExpression.UNDEF;
        }
        if (obj instanceof Expression) {
            return (Expression) obj;
        }
        if (obj instanceof Integer) {
            return DimensionlessUnitExpression.construct((Integer) obj);
        }
        if (obj instanceof Long) {
            return DimensionlessUnitExpression.construct((Long) obj);
        }
        if (obj instanceof Double) {
            return DimensionlessUnitExpression.construct((Double) obj);
        }
        if (obj instanceof Float) {
            return DimensionlessUnitExpression.construct((Float) obj);
        }
        if (obj instanceof Character) {
            return new BasicStringExpression(((Character) obj).toString());
        }
        if (obj instanceof String) {
            return new BasicStringExpression((String) obj);
        }
        if (obj instanceof Boolean) {
            return FrinkBoolean.create(((Boolean) obj).booleanValue());
        }
        if (obj instanceof Short) {
            return DimensionlessUnitExpression.construct(((Short) obj).intValue());
        }
        if (obj instanceof Byte) {
            return DimensionlessUnitExpression.construct(((Byte) obj).intValue());
        }
        if (obj instanceof Void) {
            return VoidExpression.VOID;
        }
        if (obj instanceof BigInteger) {
            return DimensionlessUnitExpression.construct((BigInteger) obj);
        }
        return JavaObjectFactory.create(obj);
    }

    public static boolean canMatch(Method method, ListExpression listExpression, Environment environment) {
        return canMatch(method.getParameterTypes(), listExpression, environment);
    }

    public static boolean canMatch(Constructor constructor, ListExpression listExpression, Environment environment) {
        return canMatch(constructor.getParameterTypes(), listExpression, environment);
    }

    public static boolean canMatch(Class[] clsArr, ListExpression listExpression, Environment environment) {
        int length = clsArr.length;
        if (length != listExpression.getChildCount()) {
            return false;
        }
        int i = 0;
        while (i < length) {
            try {
                if (!canConvert(listExpression.getChild(i), clsArr[i], environment)) {
                    return false;
                }
                i++;
            } catch (InvalidChildException e) {
                environment.outputln("JavaObjectFunctionSource: Got unexpected error in getChild argument number " + i + ":\n" + e);
            }
        }
        return true;
    }

    private static Object convertTo(JavaObject javaObject, Class cls) {
        return javaObject.getObject();
    }
}
