package frink.java;

import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.InvalidArgumentException;
import frink.expr.ListExpression;
import frink.expr.SymbolDefinition;
import frink.expr.VoidExpression;
import frink.function.AbstractFunctionDefinition;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JavaFunctionDefinition extends AbstractFunctionDefinition {
    private static final boolean DEBUG = false;
    private MethodWrapper methodWrapper;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: frink.function.AbstractFunctionDefinition.<init>(java.lang.reflect.Method, boolean):void
     arg types: [java.lang.reflect.Method, int]
     candidates:
      frink.function.AbstractFunctionDefinition.<init>(int, boolean):void
      frink.function.AbstractFunctionDefinition.<init>(frink.expr.ListExpression, boolean):void
      frink.function.AbstractFunctionDefinition.<init>(java.lang.reflect.Method, boolean):void */
    public JavaFunctionDefinition(MethodWrapper methodWrapper2) {
        super(methodWrapper2.getMethod(), false);
        this.methodWrapper = methodWrapper2;
    }

    public Expression doEvaluation(Environment environment, Expression expression) throws EvaluationException {
        JavaObject javaObject;
        Object obj;
        String str;
        Method method = this.methodWrapper.getMethod();
        SymbolDefinition symbolDefinition = environment.getSymbolDefinition("this", false);
        if (symbolDefinition != null) {
            JavaObject javaObject2 = (JavaObject) symbolDefinition.getValue();
            if (javaObject2 != null) {
                javaObject = javaObject2;
                obj = javaObject2.getObject();
            } else {
                environment.outputln("JavaFunctionDefinition.doEvaluation: obj is null for 'this'");
                javaObject = javaObject2;
                obj = null;
            }
        } else {
            javaObject = null;
            obj = null;
        }
        int childCount = expression.getChildCount();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] objArr = new Object[childCount];
        for (int i = 0; i < childCount; i++) {
            objArr[i] = JavaMapper.map(expression.getChild(i), parameterTypes[i], environment);
        }
        try {
            Object invoke = method.invoke(obj, objArr);
            if (method.getReturnType().equals(Void.TYPE)) {
                return VoidExpression.VOID;
            }
            return JavaMapper.map(invoke);
        } catch (IllegalAccessException e) {
            String str2 = "";
            if (!(javaObject == null || javaObject.getObject() == null)) {
                Class<?> cls = javaObject.getObject().getClass();
                String name = cls.getName();
                for (Class<? super Object> superclass = cls.getSuperclass(); superclass != null; superclass = superclass.getSuperclass()) {
                    MethodWrapper bestMatchWrapper = JavaObjectFunctionSource.getFunctionSource(superclass).getBestMatchWrapper(method.getName(), (ListExpression) expression, environment);
                    if (bestMatchWrapper != null) {
                        try {
                            JavaFunctionDefinition functionDefinition = bestMatchWrapper.getFunctionDefinition();
                            if (bestMatchWrapper.getMethod() != this.methodWrapper.getMethod()) {
                                this.methodWrapper.reset(bestMatchWrapper.getMethod(), functionDefinition);
                                return functionDefinition.doEvaluation(environment, expression);
                            }
                        } catch (InvalidArgumentException e2) {
                        }
                    }
                }
                str2 = name;
            }
            throw new InvalidArgumentException("Could not call Java method " + str2 + "." + method.getName() + ":\n " + e, null);
        } catch (InvocationTargetException e3) {
            if (javaObject == null || javaObject.getObject() == null) {
                str = "";
            } else {
                str = javaObject.getObject().getClass().getName();
            }
            throw new InvalidArgumentException("Could not call method " + str + "." + method.getName() + ":\n " + e3 + "\n" + e3.getTargetException(), null);
        }
    }
}
