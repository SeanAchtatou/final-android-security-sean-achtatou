package frink.java;

import frink.expr.Environment;
import frink.expr.ListExpression;
import frink.function.FunctionDefinition;
import frink.function.FunctionDescriptor;
import frink.function.FunctionSource;
import frink.function.RequiresArgumentsException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class JavaObjectFunctionSource implements FunctionSource {
    private static final boolean DEBUG = false;
    private static Hashtable<Class, JavaObjectFunctionSource> classMap = new Hashtable<>();
    private Class classObj;
    private Hashtable<String, Vector<MethodWrapper>> methodMap;

    private JavaObjectFunctionSource(Class cls) {
        this.classObj = cls;
        populateMethods();
    }

    public String getName() {
        return this.classObj.getName();
    }

    public MethodWrapper getBestMatchWrapper(String str, ListExpression listExpression, Environment environment) {
        int i;
        Vector vector = this.methodMap.get(str);
        if (vector == null) {
            return null;
        }
        int size = vector.size();
        int i2 = 0;
        MethodWrapper methodWrapper = null;
        int i3 = 0;
        while (i2 < size) {
            MethodWrapper methodWrapper2 = (MethodWrapper) vector.elementAt(i2);
            Method method = methodWrapper2.getMethod();
            if (JavaMapper.canMatch(method, listExpression, environment)) {
                if (i3 == 1) {
                    methodWrapper2 = getMostSpecific(methodWrapper, methodWrapper2, environment);
                    if (methodWrapper2 == null) {
                        environment.outputln("Can match " + methodWrapper.getMethod());
                        MethodWrapper methodWrapper3 = methodWrapper;
                        i = i3 + 1;
                        methodWrapper2 = methodWrapper3;
                    } else {
                        i = i3;
                    }
                } else {
                    i = i3 + 1;
                }
                if (i > 1) {
                    environment.outputln("Can match " + method);
                }
            } else {
                methodWrapper2 = methodWrapper;
                i = i3;
            }
            i2++;
            i3 = i;
            methodWrapper = methodWrapper2;
        }
        if (i3 == 1) {
            return methodWrapper;
        }
        if (i3 == 0) {
            return null;
        }
        environment.outputln("Multiple matches for \"" + str + "\" in class " + getName() + ".  None will be called.");
        return null;
    }

    private MethodWrapper getMostSpecific(MethodWrapper methodWrapper, MethodWrapper methodWrapper2, Environment environment) {
        Method method = methodWrapper.getMethod();
        Method method2 = methodWrapper2.getMethod();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Class<?>[] parameterTypes2 = method2.getParameterTypes();
        int length = parameterTypes.length;
        if (length != parameterTypes2.length) {
            environment.outputln("JavaObjectFunctionSource.getMostSpecific:  Unexpected parameter length mismatch:\n  " + method.toString() + "\n  " + method2.toString());
            return null;
        }
        MethodWrapper methodWrapper3 = null;
        for (int i = 0; i < length; i++) {
            if (!parameterTypes[i].isAssignableFrom(parameterTypes2[i]) || parameterTypes2[i].isAssignableFrom(parameterTypes[i])) {
                if (parameterTypes2[i].isAssignableFrom(parameterTypes[i]) && !parameterTypes[i].isAssignableFrom(parameterTypes2[i])) {
                    if (methodWrapper3 == null) {
                        methodWrapper3 = methodWrapper;
                    } else if (methodWrapper3 != methodWrapper) {
                        return null;
                    }
                }
            } else if (methodWrapper3 == null) {
                methodWrapper3 = methodWrapper2;
            } else if (methodWrapper3 != methodWrapper2) {
                return null;
            }
        }
        return methodWrapper3;
    }

    public FunctionDefinition getBestMatch(String str, ListExpression listExpression, Environment environment) {
        MethodWrapper bestMatchWrapper = getBestMatchWrapper(str, listExpression, environment);
        if (bestMatchWrapper == null) {
            return null;
        }
        return bestMatchWrapper.getFunctionDefinition();
    }

    public FunctionDefinition getBestMatch(String str, int i, Environment environment) throws RequiresArgumentsException {
        throw RequiresArgumentsException.INSTANCE;
    }

    private void populateMethods() {
        Method[] methods = this.classObj.getMethods();
        this.methodMap = new Hashtable<>();
        for (Method method : methods) {
            Vector vector = this.methodMap.get(method.getName());
            if (vector == null) {
                vector = new Vector(1);
                this.methodMap.put(method.getName(), vector);
            }
            vector.addElement(new MethodWrapper(method));
        }
    }

    public static JavaObjectFunctionSource getFunctionSource(Class cls) {
        JavaObjectFunctionSource javaObjectFunctionSource = classMap.get(cls);
        if (javaObjectFunctionSource != null) {
            return javaObjectFunctionSource;
        }
        JavaObjectFunctionSource javaObjectFunctionSource2 = new JavaObjectFunctionSource(cls);
        classMap.put(cls, javaObjectFunctionSource2);
        return javaObjectFunctionSource2;
    }

    public Enumeration<FunctionDescriptor> getFunctionDescriptors() {
        return new MethodEnumeration(this.classObj);
    }

    private static class MethodEnumeration implements Enumeration<FunctionDescriptor> {
        private Method[] methods;
        private int nextElement = 0;

        public MethodEnumeration(Class cls) {
            this.methods = cls.getMethods();
        }

        public boolean hasMoreElements() {
            return this.nextElement < this.methods.length;
        }

        public FunctionDescriptor nextElement() {
            Method method = this.methods[this.nextElement];
            this.nextElement++;
            return new FunctionDescriptor(method.getName(), new MethodWrapper(method).getFunctionDefinition());
        }
    }
}
