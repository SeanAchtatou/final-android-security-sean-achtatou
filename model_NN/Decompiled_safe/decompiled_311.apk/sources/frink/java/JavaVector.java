package frink.java;

import frink.expr.ContextFrame;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.InvalidChildException;
import frink.expr.ListExpression;
import frink.function.FunctionSource;
import frink.symbolic.MatchingContext;
import java.util.Vector;

public class JavaVector implements JavaObject, EnumeratingExpression {
    public static final String TYPE = "JavaVector";
    private JavaObjectFieldSource contextFrame;
    private FunctionSource functionSrc;
    /* access modifiers changed from: private */
    public Vector vec;

    public JavaVector(Vector vector) {
        this.vec = vector;
        this.functionSrc = JavaObjectFunctionSource.getFunctionSource(vector.getClass());
        this.contextFrame = new JavaObjectFieldSource(vector, this);
    }

    public Object getObject() {
        return this.vec;
    }

    public int getChildCount() {
        return this.vec.size();
    }

    public Expression getChild(int i) throws InvalidChildException {
        int size = this.vec.size();
        if (i >= 0 && i < size) {
            return JavaMapper.map(this.vec.elementAt(i));
        }
        throw new InvalidChildException("JavaVector: Bad child " + i + ", length is " + size, this);
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public FrinkEnumeration getEnumeration(Environment environment) {
        return new NonTypeSafeEnumeration();
    }

    public boolean isConstant() {
        return false;
    }

    public boolean isA(String str) {
        return str.equals("java.util.Vector") || str.equals(ListExpression.TYPE);
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return this.functionSrc;
    }

    public ContextFrame getContextFrame(Environment environment) {
        return this.contextFrame;
    }

    public String toString(Environment environment, boolean z) {
        return "JavaVector:" + getObject().getClass().getName() + "[" + this.vec.size() + "]";
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof JavaVector) || this.vec != ((JavaVector) expression).vec) {
            return false;
        }
        return true;
    }

    private class NonTypeSafeEnumeration implements FrinkEnumeration {
        int index;

        private NonTypeSafeEnumeration() {
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.index >= JavaVector.this.vec.size()) {
                return null;
            }
            Vector access$100 = JavaVector.this.vec;
            int i = this.index;
            this.index = i + 1;
            return JavaMapper.map(access$100.elementAt(i));
        }

        public void dispose() {
        }
    }

    public String getExpressionType() {
        return TYPE;
    }
}
