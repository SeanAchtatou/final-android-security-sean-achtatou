package frink.java;

import frink.expr.Expression;
import frink.expr.UndefExpression;
import java.lang.reflect.Array;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class JavaObjectFactory {
    private static final Hashtable<String, Class> primitives = new Hashtable<>();

    static {
        primitives.put("boolean", Boolean.TYPE);
        primitives.put("byte", Byte.TYPE);
        primitives.put("char", Character.TYPE);
        primitives.put("short", Short.TYPE);
        primitives.put("int", Integer.TYPE);
        primitives.put("long", Long.TYPE);
        primitives.put("float", Float.TYPE);
        primitives.put("double", Double.TYPE);
    }

    private JavaObjectFactory() {
    }

    public static JavaObject create(Object obj) {
        if (obj.getClass().isArray()) {
            return new JavaArray(obj);
        }
        if (obj instanceof Enumeration) {
            return new JavaEnumeration((Enumeration) obj);
        }
        if (obj instanceof Vector) {
            return new JavaVector((Vector) obj);
        }
        return new SingleJavaObject(obj);
    }

    public static Expression createNullAllowed(Object obj) {
        if (obj == null) {
            return UndefExpression.UNDEF;
        }
        return create(obj);
    }

    public static JavaArray createArray(String str, int i) throws ClassNotFoundException {
        Class<?> cls = primitives.get(str);
        if (cls == null) {
            cls = Class.forName(str);
        }
        return new JavaArray(Array.newInstance(cls, i));
    }
}
