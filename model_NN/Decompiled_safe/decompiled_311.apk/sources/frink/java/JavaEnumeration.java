package frink.java;

import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import java.util.Enumeration;

public class JavaEnumeration extends SingleJavaObject implements EnumeratingExpression {
    public JavaEnumeration(Enumeration enumeration) {
        super(enumeration);
    }

    public FrinkEnumeration getEnumeration(Environment environment) throws EvaluationException {
        return new EnumWrapper((Enumeration) getObject());
    }

    private static class EnumWrapper implements FrinkEnumeration {
        private Enumeration enumerator;

        public EnumWrapper(Enumeration enumeration) {
            this.enumerator = enumeration;
        }

        public Expression getNext(Environment environment) throws EvaluationException {
            if (this.enumerator == null) {
                return null;
            }
            if (this.enumerator.hasMoreElements()) {
                return JavaMapper.map(this.enumerator.nextElement());
            }
            this.enumerator = null;
            return null;
        }

        public void dispose() {
            this.enumerator = null;
        }
    }
}
