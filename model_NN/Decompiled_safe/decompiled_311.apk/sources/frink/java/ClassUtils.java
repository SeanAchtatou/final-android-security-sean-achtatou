package frink.java;

public class ClassUtils {
    public static String getCanonicalName(Class cls) {
        try {
            try {
                return (String) cls.getClass().getMethod("getCanonicalName", new Class[0]).invoke(cls, new Object[0]);
            } catch (Exception e) {
                return cls.getName();
            }
        } catch (NoSuchMethodException e2) {
        }
    }
}
