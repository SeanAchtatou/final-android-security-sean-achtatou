package frink.java;

import frink.expr.CannotAssignException;
import frink.expr.ContextFrame;
import frink.expr.EnumeratingExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkEnumeration;
import frink.expr.InvalidArgumentException;
import frink.expr.InvalidChildException;
import frink.expr.ListEnumerator;
import frink.expr.ListExpression;
import frink.function.FunctionSource;
import frink.symbolic.MatchingContext;
import java.lang.reflect.Array;

public class JavaArray implements JavaObject, ListExpression, EnumeratingExpression {
    private JavaObjectFieldSource contextFrame;
    private FunctionSource functionSrc;
    private int length;
    private Object obj;
    private Class objClass;

    public JavaArray(Object obj2) {
        this.obj = obj2;
        this.length = Array.getLength(obj2);
        this.objClass = obj2.getClass().getComponentType();
        this.functionSrc = JavaObjectFunctionSource.getFunctionSource(obj2.getClass());
        this.contextFrame = new JavaObjectFieldSource(obj2, this);
    }

    public FunctionSource getFunctionSource(Environment environment) {
        return this.functionSrc;
    }

    public ContextFrame getContextFrame(Environment environment) {
        return this.contextFrame;
    }

    public Object getObject() {
        return this.obj;
    }

    public boolean isA(String str) {
        return str.equals(ListExpression.TYPE);
    }

    public boolean isConstant() {
        return false;
    }

    public Expression evaluate(Environment environment) throws EvaluationException {
        return this;
    }

    public int getChildCount() {
        return this.length;
    }

    public Expression getChild(int i) throws InvalidChildException {
        if (i >= 0 && i < this.length) {
            return JavaMapper.map(Array.get(this.obj, i));
        }
        throw new InvalidChildException("JavaArray: Bad child " + i + ", length is " + this.length, this);
    }

    public void setChild(int i, Expression expression) throws CannotAssignException {
        try {
            Array.set(this.obj, i, JavaMapper.map(expression, this.objClass, null));
        } catch (InvalidArgumentException e) {
            throw new CannotAssignException("JavaArray.setChild: cannot assign object of type " + expression.getExpressionType() + " into array of " + this.objClass.getName(), expression);
        }
    }

    public FrinkEnumeration getEnumeration(Environment environment) {
        return new ListEnumerator(this);
    }

    public String toString(Environment environment, boolean z) {
        return "JavaArray:" + this.objClass.getName() + "[]";
    }

    public boolean structureEquals(Expression expression, MatchingContext matchingContext, Environment environment, boolean z) {
        if (this == expression) {
            return true;
        }
        if (!(expression instanceof JavaArray) || this.obj != ((JavaArray) expression).obj) {
            return false;
        }
        return true;
    }

    public String getExpressionType() {
        return "JavaArray:" + this.objClass.getName();
    }
}
