package frink.java;

import frink.errors.NotAnIntegerException;
import frink.expr.BasicListExpression;
import frink.expr.Environment;
import frink.expr.EvaluationException;
import frink.expr.Expression;
import frink.expr.FrinkSecurityException;
import frink.expr.InvalidArgumentException;
import frink.expr.ListExpression;
import frink.expr.StringExpression;
import frink.expr.UndefExpression;
import frink.function.BasicFunctionSource;
import frink.function.BuiltinFunctionSource;
import frink.function.DoubleArgFunction;
import frink.function.FunctionDefinition;
import frink.function.SingleArgFunction;
import frink.function.TripleArgFunction;
import frink.function.TwoArgFunction;
import frink.object.FrinkObject;
import java.lang.reflect.Field;

public class JavaFunctionSource extends BasicFunctionSource {
    public static final JavaFunctionSource INSTANCE = new JavaFunctionSource();

    private JavaFunctionSource() {
        super("JavaFunctionSource");
        initializeFunctions();
    }

    private void initializeFunctions() {
        addFunctionDefinition("newJava", new SingleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression) throws InvalidArgumentException, FrinkSecurityException {
                if (expression instanceof StringExpression) {
                    try {
                        String string = ((StringExpression) expression).getString();
                        environment.getSecurityHelper().checkNewJava(string);
                        try {
                            return JavaObjectFactory.create(Class.forName(string).newInstance());
                        } catch (InstantiationException e) {
                            throw new InvalidArgumentException("newJava: could not instantiate " + ((StringExpression) expression).getString() + " could not be instantiated with no arguments:\n" + e, expression);
                        } catch (IllegalAccessException e2) {
                            throw new InvalidArgumentException("newJava: could not instantiate " + ((StringExpression) expression).getString() + " could not be instantiated due to access restrictions:\n" + e2, expression);
                        }
                    } catch (ClassNotFoundException e3) {
                        throw new InvalidArgumentException("newJava: class " + ((StringExpression) expression).getString() + " not found in classpath.\n" + e3, expression);
                    }
                } else {
                    throw new InvalidArgumentException("newJava[str]: argument must be the name of a class in your classpath.  Argument was " + environment.format(expression), this);
                }
            }
        });
        addFunctionDefinition("newJava", new TwoArgFunction(false) {
            /* JADX WARN: Type inference failed for: r14v1, types: [frink.expr.ListExpression] */
            /* access modifiers changed from: protected */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public frink.expr.Expression doFunction(frink.expr.Environment r12, frink.expr.Expression r13, frink.expr.Expression r14) throws frink.expr.EvaluationException, frink.expr.FrinkSecurityException {
                /*
                    r11 = this;
                    boolean r1 = r13 instanceof frink.expr.StringExpression
                    if (r1 == 0) goto L_0x015f
                    r0 = r13
                    frink.expr.StringExpression r0 = (frink.expr.StringExpression) r0
                    r1 = r0
                    java.lang.String r1 = r1.getString()
                    frink.security.SecurityHelper r2 = r12.getSecurityHelper()     // Catch:{ ClassNotFoundException -> 0x003d }
                    r2.checkNewJava(r1)     // Catch:{ ClassNotFoundException -> 0x003d }
                    java.lang.Class r2 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x003d }
                    java.lang.reflect.Constructor[] r2 = r2.getConstructors()
                    r3 = 0
                    r4 = 0
                    boolean r5 = r14 instanceof frink.expr.ListExpression
                    if (r5 == 0) goto L_0x0061
                    frink.expr.ListExpression r14 = (frink.expr.ListExpression) r14
                    r5 = r14
                L_0x0024:
                    int r6 = r5.getChildCount()
                    r7 = 0
                    r10 = r7
                    r7 = r3
                    r3 = r10
                L_0x002c:
                    int r8 = r2.length
                    if (r3 >= r8) goto L_0x006b
                    r8 = r2[r3]
                    boolean r9 = frink.java.JavaMapper.canMatch(r8, r5, r12)
                    if (r9 == 0) goto L_0x003a
                    int r4 = r4 + 1
                    r7 = r8
                L_0x003a:
                    int r3 = r3 + 1
                    goto L_0x002c
                L_0x003d:
                    r2 = move-exception
                    frink.expr.InvalidArgumentException r3 = new frink.expr.InvalidArgumentException
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "newJava: class "
                    java.lang.StringBuilder r4 = r4.append(r5)
                    java.lang.StringBuilder r1 = r4.append(r1)
                    java.lang.String r4 = " not found in classpath.\n"
                    java.lang.StringBuilder r1 = r1.append(r4)
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    r3.<init>(r1, r13)
                    throw r3
                L_0x0061:
                    frink.expr.BasicListExpression r5 = new frink.expr.BasicListExpression
                    r6 = 1
                    r5.<init>(r6)
                    r5.appendChild(r14)
                    goto L_0x0024
                L_0x006b:
                    r2 = 1
                    if (r4 != r2) goto L_0x012e
                    java.lang.Class[] r2 = r7.getParameterTypes()
                    java.lang.Object[] r3 = new java.lang.Object[r6]
                    r4 = 0
                L_0x0075:
                    if (r4 >= r6) goto L_0x0086
                    frink.expr.Expression r8 = r5.getChild(r4)
                    r9 = r2[r4]
                    java.lang.Object r8 = frink.java.JavaMapper.map(r8, r9, r12)
                    r3[r4] = r8
                    int r4 = r4 + 1
                    goto L_0x0075
                L_0x0086:
                    java.lang.Object r2 = r7.newInstance(r3)     // Catch:{ InstantiationException -> 0x008f, IllegalAccessException -> 0x00c1, InvocationTargetException -> 0x00e5 }
                    frink.java.JavaObject r1 = frink.java.JavaObjectFactory.create(r2)     // Catch:{ InstantiationException -> 0x008f, IllegalAccessException -> 0x00c1, InvocationTargetException -> 0x00e5 }
                L_0x008e:
                    return r1
                L_0x008f:
                    r2 = move-exception
                    frink.expr.InvalidArgumentException r3 = new frink.expr.InvalidArgumentException
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r6 = "newJava: could not instantiate "
                    java.lang.StringBuilder r4 = r4.append(r6)
                    java.lang.StringBuilder r1 = r4.append(r1)
                    java.lang.String r4 = " could not be instantiated with arguments "
                    java.lang.StringBuilder r1 = r1.append(r4)
                    java.lang.String r4 = r12.format(r5)
                    java.lang.StringBuilder r1 = r1.append(r4)
                    java.lang.String r4 = "\n"
                    java.lang.StringBuilder r1 = r1.append(r4)
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    r3.<init>(r1, r11)
                    throw r3
                L_0x00c1:
                    r2 = move-exception
                    frink.expr.InvalidArgumentException r3 = new frink.expr.InvalidArgumentException
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "newJava: could not instantiate "
                    java.lang.StringBuilder r4 = r4.append(r5)
                    java.lang.StringBuilder r1 = r4.append(r1)
                    java.lang.String r4 = " could not be instantiated due to access restrictions:\n"
                    java.lang.StringBuilder r1 = r1.append(r4)
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    r3.<init>(r1, r13)
                    throw r3
                L_0x00e5:
                    r2 = move-exception
                    java.lang.StringBuffer r3 = new java.lang.StringBuffer
                    r3.<init>()
                    java.lang.Throwable r4 = r2.getTargetException()
                    if (r4 == 0) goto L_0x0107
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "\nCause:\n  "
                    java.lang.StringBuilder r5 = r5.append(r6)
                    java.lang.StringBuilder r4 = r5.append(r4)
                    java.lang.String r4 = r4.toString()
                    r3.append(r4)
                L_0x0107:
                    frink.expr.InvalidArgumentException r4 = new frink.expr.InvalidArgumentException
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "newJava: could not instantiate "
                    java.lang.StringBuilder r5 = r5.append(r6)
                    java.lang.StringBuilder r1 = r5.append(r1)
                    java.lang.String r5 = " could not be instantiated:\n"
                    java.lang.StringBuilder r1 = r1.append(r5)
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.StringBuilder r1 = r1.append(r3)
                    java.lang.String r1 = r1.toString()
                    r4.<init>(r1, r13)
                    throw r4
                L_0x012e:
                    if (r4 != 0) goto L_0x0133
                    r1 = 0
                    goto L_0x008e
                L_0x0133:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "Multiple matches for constructor "
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.StringBuilder r1 = r2.append(r1)
                    java.lang.String r2 = "\" in class"
                    java.lang.StringBuilder r1 = r1.append(r2)
                    frink.java.JavaFunctionSource r2 = frink.java.JavaFunctionSource.this
                    java.lang.String r2 = r2.getName()
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r2 = ".  None will be called."
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    r12.outputln(r1)
                L_0x015f:
                    frink.expr.InvalidArgumentException r1 = new frink.expr.InvalidArgumentException
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r3 = "newJava[str, args]: argument must be the name of a class in your classpath.  Argument was "
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r3 = r12.format(r13)
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    r1.<init>(r2, r11)
                    throw r1
                */
                throw new UnsupportedOperationException("Method not decompiled: frink.java.JavaFunctionSource.AnonymousClass2.doFunction(frink.expr.Environment, frink.expr.Expression, frink.expr.Expression):frink.expr.Expression");
            }
        });
        addFunctionDefinition("newJavaArray", new TwoArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws EvaluationException, FrinkSecurityException {
                if (expression instanceof StringExpression) {
                    String string = ((StringExpression) expression).getString();
                    environment.getSecurityHelper().checkNewJava(string);
                    try {
                        try {
                            return JavaObjectFactory.createArray(string, BuiltinFunctionSource.getIntegerValue(expression2));
                        } catch (ClassNotFoundException e) {
                            throw new InvalidArgumentException("newJavaArray: class " + string + " not found in classpath.\n" + e, expression);
                        }
                    } catch (NotAnIntegerException e2) {
                        throw new InvalidArgumentException("newJavaArray: Argument 1 should be a positive integer containing the length when constructing " + string + ".\n  Argument was " + environment.format(expression2), this);
                    }
                } else {
                    throw new InvalidArgumentException("newJavaArray[str, len]: argument must be the name of a class in your classpath.  Argument was " + environment.format(expression), this);
                }
            }
        });
        addFunctionDefinition("staticJava", new DoubleArgFunction(false) {
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2) throws InvalidArgumentException, FrinkSecurityException {
                if (!(expression instanceof StringExpression) || !(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("staticJava[className, field] passed invalid arguments: [" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
                String string = ((StringExpression) expression).getString();
                String string2 = ((StringExpression) expression2).getString();
                environment.getSecurityHelper().checkStaticJava(string);
                try {
                    JavaObjectFieldMap fieldMap = JavaObjectFieldMap.getFieldMap(Class.forName(string));
                    if (fieldMap == null) {
                        throw new InvalidArgumentException("staticJava: No class map for " + string, this);
                    }
                    Field field = fieldMap.getField(string2);
                    if (field == null) {
                        throw new InvalidArgumentException("staticJava: Class " + string + " has no field called " + string2, this);
                    }
                    try {
                        Object obj = field.get(null);
                        if (obj == null) {
                            return UndefExpression.UNDEF;
                        }
                        return JavaObjectFactory.create(obj);
                    } catch (IllegalAccessException e) {
                        throw new InvalidArgumentException("staticJava: Access exception in " + string + " getting field " + string2 + "\n " + e, this);
                    }
                } catch (ClassNotFoundException e2) {
                    throw new InvalidArgumentException("staticJava: No class named " + string + "\n " + e2, this);
                }
            }
        });
        addFunctionDefinition("callJava", new TripleArgFunction(false) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression
             arg types: [frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, int, ?[OBJECT, ARRAY], int]
             candidates:
              frink.function.FunctionManager.execute(java.lang.String, frink.expr.Environment, frink.expr.ListExpression, frink.object.FrinkObject, boolean, frink.function.FunctionCacher):frink.expr.Expression
              frink.function.FunctionManager.execute(frink.function.FunctionDefinition, frink.expr.Environment, frink.expr.Expression, boolean, frink.object.FrinkObject, boolean):frink.expr.Expression */
            /* access modifiers changed from: protected */
            public Expression doFunction(Environment environment, Expression expression, Expression expression2, Expression expression3) throws InvalidArgumentException, FrinkSecurityException, EvaluationException {
                FunctionDefinition bestMatch;
                if (!(expression instanceof StringExpression) || !(expression2 instanceof StringExpression)) {
                    throw new InvalidArgumentException("callJava[className, methodName, args] passed invalid arguments: [" + environment.format(expression) + ", " + environment.format(expression2) + "]", this);
                }
                String string = ((StringExpression) expression).getString();
                environment.getSecurityHelper().checkCallJava(string);
                String string2 = ((StringExpression) expression2).getString();
                try {
                    JavaObjectFunctionSource functionSource = JavaObjectFunctionSource.getFunctionSource(Class.forName(string));
                    if (functionSource == null) {
                        throw new InvalidArgumentException("callJava: No function map for " + string, this);
                    }
                    if (expression3 instanceof ListExpression) {
                        bestMatch = functionSource.getBestMatch(string2, (ListExpression) expression3, environment);
                    } else {
                        BasicListExpression basicListExpression = new BasicListExpression(1);
                        basicListExpression.appendChild(expression3);
                        bestMatch = functionSource.getBestMatch(string2, basicListExpression, environment);
                    }
                    if (bestMatch != null) {
                        return environment.getFunctionManager().execute(bestMatch, environment, expression3, false, (FrinkObject) null, false);
                    }
                    throw new InvalidArgumentException("callJava[" + string + ", " + string2 + ", " + environment.format(expression3) + ": could not find function match.", this);
                } catch (ClassNotFoundException e) {
                    throw new InvalidArgumentException("callJava: No class named " + string + "\n " + e, this);
                }
            }
        });
    }
}
