package frink.java;

import frink.expr.Expression;
import frink.expr.SelfDisplayingExpression;
import frink.object.FrinkObject;

public interface JavaObject extends FrinkObject, Expression, SelfDisplayingExpression {
    Object getObject();
}
