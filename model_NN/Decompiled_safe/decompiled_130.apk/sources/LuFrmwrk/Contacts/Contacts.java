package LuFrmwrk.Contacts;

import android.app.Activity;
import android.database.Cursor;
import android.provider.ContactsContract;
import java.util.Vector;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class Contacts {
    private Activity mActivity;
    private Vector<Contact> mCheckedContacts;
    private Vector<Contact> mContacts;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: LuFrmwrk.Contacts.Contacts.<init>(android.app.Activity):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public Contacts(android.app.Activity r1) {
        /*
            r1 = this;
            r1.<init>()
            r1.mActivity = r2
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            r1.mContacts = r0
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            r1.mCheckedContacts = r0
            r1.listContacts()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: LuFrmwrk.Contacts.Contacts.<init>(android.app.Activity):void");
    }

    public Contact getContact(int p_indice) {
        return this.mContacts.elementAt(p_indice);
    }

    public Contact getCheckedContact(int p_indice) {
        return this.mCheckedContacts.elementAt(p_indice);
    }

    public int getCheckedContactsCount() {
        return this.mCheckedContacts.size();
    }

    public int getContactsCount() {
        return this.mContacts.size();
    }

    public void checkContact(Contact p_contact) {
        setCheckedContactState(true, p_contact);
    }

    public void uncheckContact(Contact p_contact) {
        setCheckedContactState(false, p_contact);
    }

    private void setCheckedContactState(boolean p_checked, Contact p_contact) {
        p_contact.setChecked(p_checked);
        if (p_checked) {
            this.mCheckedContacts.add(p_contact);
        } else {
            this.mCheckedContacts.remove(p_contact);
        }
    }

    public void unCheckAll() {
        for (int i = 0; i < this.mContacts.size(); i++) {
            setCheckedContactState(false, getContact(i));
        }
    }

    private void listContacts() {
        Cursor cur = this.mActivity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"display_name", "data1", "_id"}, null, null, "display_name");
        this.mActivity.startManagingCursor(cur);
        if (cur.moveToFirst()) {
            do {
                if (cur.getString(cur.getColumnIndex("display_name")).charAt(0) != '.') {
                    this.mContacts.add(new Contact(cur.getString(cur.getColumnIndex("display_name")), cur.getString(cur.getColumnIndex("data1")), cur.getString(cur.getColumnIndex("_id"))));
                }
            } while (cur.moveToNext());
        }
    }
}
