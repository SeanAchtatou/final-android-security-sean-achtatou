package LuFrmwrk.Contacts;

public class Contact {
    private boolean checked;
    private String id;
    private String name;
    private String num;

    public Contact(String p_name, String p_num, String p_id) {
        this.name = p_name;
        this.num = p_num;
        this.id = p_id;
    }

    public String getName() {
        return this.name;
    }

    public String getNumber() {
        return this.num;
    }

    public String getId() {
        return this.id;
    }

    public boolean getChecked() {
        return this.checked;
    }

    public void setChecked(boolean p_state) {
        this.checked = p_state;
    }
}
