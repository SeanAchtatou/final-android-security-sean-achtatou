package LuFrmwrk.Lists;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class listItem {
    private boolean checked;
    private String mainText;
    private Paint paint;
    private listItems parent;
    private Rect rect;
    private boolean selected;
    private String subText;

    public listItem(String p_mainText, String p_subText, Rect p_rect, boolean p_checked, listItems p_parent, Paint p_paint) {
        this.mainText = p_mainText;
        this.subText = p_subText;
        this.checked = p_checked;
        this.paint = p_paint;
        this.parent = p_parent;
        this.rect = p_rect;
    }

    public void setSelected(boolean p_selected) {
        this.selected = p_selected;
    }

    public void setChecked(boolean p_checked) {
        this.checked = p_checked;
    }

    public void Draw(Canvas canvas) {
    }

    public boolean isIn(float p_x, float p_y) {
        if ((p_x > ((float) this.rect.left)) && (p_x < ((float) this.rect.right))) {
            if ((p_y > ((float) this.rect.top)) && (p_y < ((float) this.rect.bottom))) {
                return true;
            }
        }
        return false;
    }
}
