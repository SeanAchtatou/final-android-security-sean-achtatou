package com.storybird.bubblebuster;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.facebook.android.Facebook;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class BubbleBuster extends Activity implements AdListener {
    private String MY_AD_UNIT_ID = "a14d93459877d71";
    private AdView advertising;
    public Facebook facebook = new Facebook("138396016233863");
    private ImageView mImage;
    private MainView mMainView;
    public Menu mMenu;
    PowerManager pm;
    private LinearLayout rl;
    PowerManager.WakeLock wl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView((int) R.layout.main);
        setRequestedOrientation(1);
        StartVersusPanel(getIntent().getExtras());
        this.pm = (PowerManager) getSystemService("power");
        this.wl = this.pm.newWakeLock(6, "My Tag");
        this.wl.acquire();
    }

    public void onDestroy() {
        super.onDestroy();
        this.wl.release();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.facebook.authorizeCallback(requestCode, resultCode, data);
    }

    public void setFullScreen() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setWindowAnimations(16973828);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.mMainView == null) {
            return false;
        }
        this.mMainView.RefreshOptionsMenu();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.mMainView == null) {
            return false;
        }
        this.mMainView.OptionsItemSelected(item);
        return true;
    }

    public void onNewIntent(Intent newIntent) {
        super.onNewIntent(newIntent);
        setIntent(newIntent);
        StartVersusPanel(getIntent().getExtras());
    }

    public void StartVersusPanel(Bundle levelBundle) {
        this.advertising = new AdView(this, AdSize.BANNER, this.MY_AD_UNIT_ID);
        this.advertising.setAdListener(this);
        this.mImage = new ImageView(this);
        this.mImage.setImageResource(R.drawable.bandeau_sb);
        this.mMainView = new MainView(this);
        this.mMainView.setActivity(this);
        this.rl = new LinearLayout(this);
        this.rl.setOrientation(1);
        this.advertising.loadAd(new AdRequest());
        this.mImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                BubbleBuster.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.storybird.spacebusterlite&feature=search_result")));
            }
        });
        this.rl.addView(this.mImage);
        this.rl.addView(this.advertising);
        this.rl.addView(this.mMainView);
        setContentView(this.rl);
    }

    public void ShowAd() {
        Log.i("ShowAd", "in da place");
        this.advertising.setVisibility(0);
    }

    public void onReceiveAd(Ad ad) {
        Log.e("-- onReceiveAd --", "onReceiveAd");
        this.rl.removeView(this.mImage);
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
        Log.e("-- onFailedToReceiveAd --", "onFailedToReceiveAd");
    }

    public void onPresentScreen(Ad ad) {
        Log.e("-- onPresentScreen --", "onPresentScreen");
    }

    public void onDismissScreen(Ad ad) {
        Log.e("-- onDismissScreen --", "onDismissScreen");
    }

    public void onLeaveApplication(Ad ad) {
        Log.e("-- onLeaveApplication --", "onLeaveApplication");
    }
}
