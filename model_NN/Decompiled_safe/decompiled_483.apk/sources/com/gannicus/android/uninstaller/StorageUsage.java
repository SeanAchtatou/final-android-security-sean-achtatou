package com.gannicus.android.uninstaller;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.StatFs;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.gannicus.android.api.Utils;

public class StorageUsage extends Activity {
    /* access modifiers changed from: private */
    public Handler handler;
    private TextView internalTotalView;
    /* access modifiers changed from: private */
    public ProgressBar internalUsageBar;
    private TextView internalUsedView;
    private TextView internalavailableView;
    private View sdCardPanel;
    private TextView sdCardTotalView;
    /* access modifiers changed from: private */
    public ProgressBar sdCardUsageBar;
    private TextView sdCardUsedView;
    private TextView sdCardavailableView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.storage_usage);
        init();
        updateInternalBeanToUI(loadUsage(Utils.getInternalStoragePath()));
        if (Utils.isAPI8AndAbove()) {
            updateSDCardBeanToUI(loadUsage(Utils.getSDCardPath()));
        } else {
            this.sdCardPanel.setVisibility(8);
        }
    }

    private Bean loadUsage(String path) {
        Bean bean = new Bean(this, null);
        StatFs statFs = new StatFs(path);
        bean.totalValue = statFs.getBlockCount();
        bean.availableValue = statFs.getAvailableBlocks();
        bean.usedValue = bean.totalValue - bean.availableValue;
        int sizePerBlock = statFs.getBlockSize();
        long available = ((long) bean.availableValue) * ((long) sizePerBlock);
        long total = ((long) bean.totalValue) * ((long) sizePerBlock);
        bean.total = format(total);
        bean.available = format(available);
        bean.used = format(total - available);
        return bean;
    }

    private static final String format(long size) {
        return (((float) ((int) ((((((float) size) / 1024.0f) / 1024.0f) * 100.0f) + 0.5f))) / 100.0f) + " MB";
    }

    private void init() {
        this.handler = new Handler();
        this.sdCardTotalView = (TextView) findViewById(R.id.sdcard_total);
        this.sdCardavailableView = (TextView) findViewById(R.id.sdcard_available);
        this.sdCardUsedView = (TextView) findViewById(R.id.sdcard_used);
        this.sdCardUsageBar = (ProgressBar) findViewById(R.id.sdcard_usage_bar);
        this.sdCardPanel = findViewById(R.id.sdcard_panel);
        this.internalTotalView = (TextView) findViewById(R.id.internal_total);
        this.internalavailableView = (TextView) findViewById(R.id.internal_available);
        this.internalUsedView = (TextView) findViewById(R.id.internal_used);
        this.internalUsageBar = (ProgressBar) findViewById(R.id.internal_usage_bar);
    }

    private void updateSDCardBeanToUI(Bean bean) {
        this.sdCardTotalView.setText(bean.total);
        this.sdCardavailableView.setText(bean.available);
        this.sdCardUsedView.setText(bean.used);
        this.sdCardUsageBar.setMax(bean.totalValue);
        final int used = bean.usedValue;
        new Thread(new Runnable() {
            public void run() {
                int progress = 0;
                int increment = used / 20;
                if (increment <= 10) {
                    increment = 1;
                }
                while (progress <= used) {
                    final int temp = progress;
                    progress += increment;
                    StorageUsage.this.handler.post(new Runnable() {
                        public void run() {
                            StorageUsage.this.sdCardUsageBar.setProgress(temp);
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        progress = used;
                        Handler access$0 = StorageUsage.this.handler;
                        final int i = used;
                        access$0.post(new Runnable() {
                            public void run() {
                                StorageUsage.this.sdCardUsageBar.setProgress(i);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    private void updateInternalBeanToUI(Bean bean) {
        this.internalTotalView.setText(bean.total);
        this.internalavailableView.setText(bean.available);
        this.internalUsedView.setText(bean.used);
        this.internalUsageBar.setMax(bean.totalValue);
        final int used = bean.usedValue;
        new Thread(new Runnable() {
            public void run() {
                int progress = 0;
                int increment = used / 20;
                if (increment <= 10) {
                    increment = 1;
                }
                while (progress <= used) {
                    final int temp = progress;
                    progress += increment;
                    StorageUsage.this.handler.post(new Runnable() {
                        public void run() {
                            StorageUsage.this.internalUsageBar.setProgress(temp);
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        progress = used;
                        Handler access$0 = StorageUsage.this.handler;
                        final int i = used;
                        access$0.post(new Runnable() {
                            public void run() {
                                StorageUsage.this.internalUsageBar.setProgress(i);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    private class Bean {
        String available;
        int availableValue;
        String total;
        int totalValue;
        String used;
        int usedValue;

        private Bean() {
        }

        /* synthetic */ Bean(StorageUsage storageUsage, Bean bean) {
            this();
        }
    }
}
