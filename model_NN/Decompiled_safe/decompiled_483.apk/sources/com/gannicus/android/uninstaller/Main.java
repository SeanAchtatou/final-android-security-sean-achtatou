package com.gannicus.android.uninstaller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.gannicus.android.api.AdmobUtils;
import com.gannicus.android.api.AlertUtils;
import com.gannicus.android.api.DBTools;
import com.gannicus.android.api.Utils;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class Main extends Activity {
    private static final CharSequence[] LONG_TOUCH_OPTION_ITEMS = {"Launch", "Rate It", "Details"};
    private static final CharSequence[] LONG_TOUCH_OPTION_ITEMS_API8_MOVE_BACK = {"Launch", "Rate It", "Move to Phone"};
    private static final CharSequence[] LONG_TOUCH_OPTION_ITEMS_API8_MOVE_TO = {"Launch", "Rate It", "Move to SD card"};
    private static final int REQUEST_CODE = 484893;
    public static final String[] SORT_ITEMS = {"Last Installed", "Size", "Name"};
    public static final String[] SORT_ITEMS_API8 = {"Last Installed", "Size", "Name", "Install Location"};
    /* access modifiers changed from: private */
    public ListView appList;
    /* access modifiers changed from: private */
    public ArrayList<App> apps;
    /* access modifiers changed from: private */
    public int curPosition;
    /* access modifiers changed from: private */
    public ArrayList<App> filteredApps;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public View infoPanel;
    /* access modifiers changed from: private */
    public boolean isCached;
    private boolean isloaded;
    /* access modifiers changed from: private */
    public View loadingPanel;
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            String packageName;
            Main.this.curPosition = position;
            if (Main.this.searchBar.getVisibility() == 0) {
                packageName = ((App) Main.this.filteredApps.get(position)).packageName;
            } else {
                packageName = ((App) Main.this.apps.get(position)).packageName;
            }
            Main.this.startActivityForResult(new Intent("android.intent.action.DELETE", Uri.parse("package:" + packageName)), Main.REQUEST_CODE);
        }
    };
    private AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            App me;
            if (Main.this.searchBar.getVisibility() == 0) {
                me = (App) Main.this.filteredApps.get(position);
            } else {
                me = (App) Main.this.apps.get(position);
            }
            Main.this.showOptionDialog(me, position);
            return false;
        }
    };
    /* access modifiers changed from: private */
    public HashSet<String> onSDCardSet = new HashSet<>();
    /* access modifiers changed from: private */
    public ProgressDialog progress;
    /* access modifiers changed from: private */
    public View searchBar;
    /* access modifiers changed from: private */
    public EditText searchEditor;
    private TextWatcher watcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            AppListAdapter adapter;
            String key = s.toString();
            if (Main.this.appList != null && (adapter = (AppListAdapter) Main.this.appList.getAdapter()) != null) {
                if (key == null || key.trim().equals("")) {
                    Main.this.filteredApps = Main.this.apps;
                } else {
                    Main.this.filteredApps = Utils.filter(Main.this.apps, key);
                }
                adapter.refresh(Main.this.filteredApps);
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("full_screen", false)) {
            getWindow().setFlags(1024, 1024);
        }
        boolean isTablet = Utils.isXLagerScreen(this);
        boolean autoRotate = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("auto_rotate_screen", isTablet);
        if (autoRotate) {
            setRequestedOrientation(-1);
        } else if (!autoRotate && !isTablet) {
            setRequestedOrientation(1);
        } else if (!autoRotate && isTablet) {
            setRequestedOrientation(0);
        }
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        init();
        startLogoAnim();
        loadApps();
        AdmobUtils.showAd(this, (LinearLayout) findViewById(R.id.id_hold));
    }

    private void init() {
        setLoaded(false);
        this.appList = (ListView) findViewById(R.id.app_list);
        this.appList.setOnItemClickListener(this.onItemClickListener);
        this.appList.setOnItemLongClickListener(this.onItemLongClickListener);
        this.searchBar = findViewById(R.id.search_bar);
        this.searchEditor = (EditText) findViewById(R.id.search_editor);
        this.searchEditor.addTextChangedListener(this.watcher);
        this.infoPanel = findViewById(R.id.info_label);
        this.loadingPanel = findViewById(R.id.loading_panel);
        SharedPreferences pref = getPreferences(0);
        this.isCached = pref.getBoolean("isCached", false);
        if (!this.isCached) {
            pref.edit().putBoolean("isCached", true).commit();
        }
    }

    private void startLogoAnim() {
        this.loadingPanel.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
    }

    private void loadApps() {
        if (!this.isCached) {
            this.progress = createProgressDialog(this);
            this.progress.show();
        }
        new Thread() {
            public void run() {
                try {
                    Main.this.apps = Main.this.getApps();
                    if (!Main.this.isCached && Main.this.progress != null) {
                        Main.this.progress.dismiss();
                    }
                    Main.this.handler.post(new Runnable() {
                        public void run() {
                            Main.this.filteredApps = Main.this.apps;
                            if (Main.this.apps.size() == 0) {
                                Main.this.infoPanel.setVisibility(0);
                            } else {
                                Main.this.infoPanel.setVisibility(8);
                            }
                            Main.this.loadingPanel.setVisibility(8);
                            Main.this.appList.setAdapter((ListAdapter) new AppListAdapter(Main.this, Main.this.apps, Main.this.onSDCardSet));
                            Main.this.setLoaded(true);
                            if (!Main.this.isCached) {
                                Toast.makeText(Main.this, Utils.getTheFirstTip(), 1).show();
                            }
                        }
                    });
                } catch (Exception e) {
                    Main.this.handler.post(new Runnable() {
                        public void run() {
                            Utils.showCrashedDialog(Main.this);
                        }
                    });
                }
            }
        }.start();
    }

    private static final ProgressDialog createProgressDialog(Context ctx) {
        ProgressDialog progress2 = new ProgressDialog(ctx);
        progress2.setIcon((int) R.drawable.login_logo);
        progress2.setTitle("Making Cache");
        progress2.setMessage("Only Once!\n\nNext Time, There Will be No Loading Time\n\nThanks for using me");
        progress2.setProgressStyle(1);
        progress2.setCancelable(false);
        return progress2;
    }

    /* access modifiers changed from: private */
    public ArrayList<App> getApps() {
        List<ApplicationInfo> appInfos = getPackageManager().getInstalledApplications(0);
        if (!this.isCached && this.progress != null) {
            int length = 0;
            for (ApplicationInfo e : appInfos) {
                if ((e.flags & 1) != 1) {
                    length++;
                }
            }
            this.progress.setMax(length - 1);
        }
        ArrayList<String>[] cachedAppInfo = DBTools.getInstance(this).getCachedAppInfo();
        ArrayList<String> cachedAppPackageNames = cachedAppInfo[0];
        ArrayList<String> arrayList = cachedAppInfo[1];
        for (ApplicationInfo appInfo : appInfos) {
            if (!appInfo.packageName.equals("com.gannicus.android.uninstaller") && (appInfo.flags & 1) != 1) {
                int cachedAt = cachedAt(cachedAppPackageNames, appInfo.packageName);
                if (cachedAt != -1) {
                    if ((appInfo.flags & 262144) == 262144) {
                        this.onSDCardSet.add(appInfo.packageName);
                    }
                    File file = new File(appInfo.sourceDir);
                    long installedAt = file.lastModified();
                    if (((Long) arrayList.get(cachedAt)).longValue() != installedAt) {
                        App app = new App();
                        app.packageName = appInfo.packageName;
                        app.installedAt = installedAt;
                        app.size = (int) (file.length() / 1024);
                        DBTools.getInstance(this).update(app);
                    }
                    cachedAppPackageNames.remove(cachedAt);
                    arrayList.remove(cachedAt);
                } else {
                    if ((appInfo.flags & 262144) == 262144) {
                        this.onSDCardSet.add(appInfo.packageName);
                    }
                    App app2 = new App();
                    app2.icon = appInfo.loadIcon(getPackageManager());
                    app2.name = appInfo.loadLabel(getPackageManager()).toString();
                    app2.packageName = appInfo.packageName;
                    File file2 = new File(appInfo.sourceDir);
                    app2.installedAt = file2.lastModified();
                    app2.size = (int) (file2.length() / 1024);
                    DBTools.getInstance(this).save(app2);
                    if (!this.isCached && this.progress != null) {
                        this.progress.setProgress(this.progress.getProgress() + 1);
                    }
                }
            }
        }
        ArrayList<String> uninstalledApps = new ArrayList<>();
        Iterator it = cachedAppPackageNames.iterator();
        while (it.hasNext()) {
            String e2 = (String) it.next();
            boolean uninstalled = true;
            Iterator<ApplicationInfo> it2 = appInfos.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (e2.equals(it2.next().packageName)) {
                        uninstalled = false;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (uninstalled) {
                uninstalledApps.add(e2);
            }
        }
        if (uninstalledApps.size() > 0) {
            DBTools.getInstance(this).removeAllByPackageNames(uninstalledApps);
        }
        ArrayList<App> ret = DBTools.getInstance(this).find();
        int sortType = getPreferences(0).getInt("sort_by", 0);
        Utils.sort(ret, this.onSDCardSet, sortType);
        DBTools.getInstance(this).close();
        return ret;
    }

    private static final int cachedAt(ArrayList<String> cachedAppPackageNames, String packageName) {
        int length = cachedAppPackageNames.size();
        for (int i = 0; i != length; i++) {
            if (cachedAppPackageNames.get(i).equals(packageName)) {
                return i;
            }
        }
        return -1;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.searchBar.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.searchEditor.setText("");
        this.searchBar.setVisibility(8);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            check();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.isCached && this.progress != null) {
            this.progress.dismiss();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0091, code lost:
        if ((r3.applicationInfo.flags & 262144) != 262144) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0093, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009b, code lost:
        if (r2 != r12.onSDCardSet.contains(r4)) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009d, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009e, code lost:
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a0, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a2, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a3, code lost:
        if (r2 == false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a5, code lost:
        r12.onSDCardSet.add(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r12.onSDCardSet.remove(r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void check() {
        /*
            r12 = this;
            r11 = 262144(0x40000, float:3.67342E-40)
            r10 = 0
            android.content.pm.PackageManager r8 = r12.getPackageManager()     // Catch:{ Exception -> 0x00ab }
            r9 = 0
            java.util.List r5 = r8.getInstalledPackages(r9)     // Catch:{ Exception -> 0x00ab }
            r4 = 0
            android.view.View r8 = r12.searchBar     // Catch:{ Exception -> 0x00ab }
            int r8 = r8.getVisibility()     // Catch:{ Exception -> 0x00ab }
            if (r8 != 0) goto L_0x006f
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.filteredApps     // Catch:{ Exception -> 0x00ab }
            int r9 = r12.curPosition     // Catch:{ Exception -> 0x00ab }
            java.lang.Object r8 = r8.get(r9)     // Catch:{ Exception -> 0x00ab }
            com.gannicus.android.uninstaller.App r8 = (com.gannicus.android.uninstaller.App) r8     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = r8.packageName     // Catch:{ Exception -> 0x00ab }
        L_0x0021:
            r7 = 1
            r1 = 0
            java.util.Iterator r8 = r5.iterator()     // Catch:{ Exception -> 0x00ab }
        L_0x0027:
            boolean r9 = r8.hasNext()     // Catch:{ Exception -> 0x00ab }
            if (r9 != 0) goto L_0x007c
        L_0x002d:
            if (r7 == 0) goto L_0x00d4
            r6 = 0
            android.view.View r8 = r12.searchBar     // Catch:{ Exception -> 0x00ab }
            int r8 = r8.getVisibility()     // Catch:{ Exception -> 0x00ab }
            if (r8 != 0) goto L_0x00b7
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.filteredApps     // Catch:{ Exception -> 0x00ab }
            int r9 = r12.curPosition     // Catch:{ Exception -> 0x00ab }
            java.lang.Object r6 = r8.remove(r9)     // Catch:{ Exception -> 0x00ab }
            com.gannicus.android.uninstaller.App r6 = (com.gannicus.android.uninstaller.App) r6     // Catch:{ Exception -> 0x00ab }
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.apps     // Catch:{ Exception -> 0x00ab }
            r8.remove(r6)     // Catch:{ Exception -> 0x00ab }
        L_0x0047:
            android.widget.ListView r8 = r12.appList     // Catch:{ Exception -> 0x00ab }
            android.widget.ListAdapter r8 = r8.getAdapter()     // Catch:{ Exception -> 0x00ab }
            android.widget.BaseAdapter r8 = (android.widget.BaseAdapter) r8     // Catch:{ Exception -> 0x00ab }
            r8.notifyDataSetChanged()     // Catch:{ Exception -> 0x00ab }
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.apps     // Catch:{ Exception -> 0x00ab }
            int r8 = r8.size()     // Catch:{ Exception -> 0x00ab }
            if (r8 != 0) goto L_0x00cc
            android.view.View r8 = r12.infoPanel     // Catch:{ Exception -> 0x00ab }
            r9 = 0
            r8.setVisibility(r9)     // Catch:{ Exception -> 0x00ab }
        L_0x0060:
            com.gannicus.android.api.DBTools r8 = com.gannicus.android.api.DBTools.getInstance(r12)     // Catch:{ Exception -> 0x00ab }
            r8.remove(r6)     // Catch:{ Exception -> 0x00ab }
            com.gannicus.android.api.DBTools r8 = com.gannicus.android.api.DBTools.getInstance(r12)     // Catch:{ Exception -> 0x00ab }
            r8.close()     // Catch:{ Exception -> 0x00ab }
        L_0x006e:
            return
        L_0x006f:
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.apps     // Catch:{ Exception -> 0x00ab }
            int r9 = r12.curPosition     // Catch:{ Exception -> 0x00ab }
            java.lang.Object r8 = r8.get(r9)     // Catch:{ Exception -> 0x00ab }
            com.gannicus.android.uninstaller.App r8 = (com.gannicus.android.uninstaller.App) r8     // Catch:{ Exception -> 0x00ab }
            java.lang.String r4 = r8.packageName     // Catch:{ Exception -> 0x00ab }
            goto L_0x0021
        L_0x007c:
            java.lang.Object r3 = r8.next()     // Catch:{ Exception -> 0x00ab }
            android.content.pm.PackageInfo r3 = (android.content.pm.PackageInfo) r3     // Catch:{ Exception -> 0x00ab }
            android.content.pm.ApplicationInfo r9 = r3.applicationInfo     // Catch:{ Exception -> 0x00ab }
            java.lang.String r9 = r9.packageName     // Catch:{ Exception -> 0x00ab }
            boolean r9 = r9.equals(r4)     // Catch:{ Exception -> 0x00ab }
            if (r9 == 0) goto L_0x0027
            android.content.pm.ApplicationInfo r8 = r3.applicationInfo     // Catch:{ Exception -> 0x00ab }
            int r8 = r8.flags     // Catch:{ Exception -> 0x00ab }
            r8 = r8 & r11
            if (r8 != r11) goto L_0x00a0
            r8 = 1
            r2 = r8
        L_0x0095:
            java.util.HashSet<java.lang.String> r8 = r12.onSDCardSet     // Catch:{ Exception -> 0x00ab }
            boolean r8 = r8.contains(r4)     // Catch:{ Exception -> 0x00ab }
            if (r2 != r8) goto L_0x00a2
            r1 = 0
        L_0x009e:
            r7 = 0
            goto L_0x002d
        L_0x00a0:
            r2 = r10
            goto L_0x0095
        L_0x00a2:
            r1 = 1
            if (r2 == 0) goto L_0x00b1
            java.util.HashSet<java.lang.String> r8 = r12.onSDCardSet     // Catch:{ Exception -> 0x00ab }
            r8.add(r4)     // Catch:{ Exception -> 0x00ab }
            goto L_0x009e
        L_0x00ab:
            r8 = move-exception
            r0 = r8
            com.gannicus.android.api.AlertUtils.alertError(r12)
            goto L_0x006e
        L_0x00b1:
            java.util.HashSet<java.lang.String> r8 = r12.onSDCardSet     // Catch:{ Exception -> 0x00ab }
            r8.remove(r4)     // Catch:{ Exception -> 0x00ab }
            goto L_0x009e
        L_0x00b7:
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.apps     // Catch:{ Exception -> 0x00ab }
            int r9 = r12.curPosition     // Catch:{ Exception -> 0x00ab }
            java.lang.Object r6 = r8.remove(r9)     // Catch:{ Exception -> 0x00ab }
            com.gannicus.android.uninstaller.App r6 = (com.gannicus.android.uninstaller.App) r6     // Catch:{ Exception -> 0x00ab }
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.filteredApps     // Catch:{ Exception -> 0x00ab }
            if (r8 == 0) goto L_0x0047
            java.util.ArrayList<com.gannicus.android.uninstaller.App> r8 = r12.filteredApps     // Catch:{ Exception -> 0x00ab }
            r8.remove(r6)     // Catch:{ Exception -> 0x00ab }
            goto L_0x0047
        L_0x00cc:
            android.view.View r8 = r12.infoPanel     // Catch:{ Exception -> 0x00ab }
            r9 = 8
            r8.setVisibility(r9)     // Catch:{ Exception -> 0x00ab }
            goto L_0x0060
        L_0x00d4:
            if (r1 == 0) goto L_0x006e
            android.widget.ListView r8 = r12.appList     // Catch:{ Exception -> 0x00ab }
            android.widget.ListAdapter r8 = r8.getAdapter()     // Catch:{ Exception -> 0x00ab }
            android.widget.BaseAdapter r8 = (android.widget.BaseAdapter) r8     // Catch:{ Exception -> 0x00ab }
            r8.notifyDataSetChanged()     // Catch:{ Exception -> 0x00ab }
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gannicus.android.uninstaller.Main.check():void");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sort_by:
                showSortByDialog();
                break;
            case R.id.menu_storage_info:
                showStorageDialog();
                break;
            case R.id.menu_search:
                showSearchBar();
                break;
            case R.id.menu_settings:
                Utils.showSettingsDialog(this);
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void showStorageDialog() {
        startActivity(new Intent(this, StorageUsage.class));
    }

    private void showSearchBar() {
        this.searchEditor.setText("");
        if (this.searchBar.getVisibility() == 8) {
            this.searchBar.setVisibility(0);
        } else {
            this.searchBar.setVisibility(8);
        }
    }

    public void reloadThemeToUI() {
        if (isLoaded()) {
            AppListAdapter adapter = (AppListAdapter) this.appList.getAdapter();
            adapter.loadThemeStyle();
            adapter.notifyDataSetChanged();
        }
    }

    private final void showSortByDialog() {
        String[] items;
        if (Utils.isAPI8AndAbove()) {
            items = SORT_ITEMS_API8;
        } else {
            items = SORT_ITEMS;
        }
        int sortId = getPreferences(0).getInt("sort_by", 0);
        final int settingIndex = Utils.sortIdToSettingIndex(sortId);
        final int oldSortId = sortId;
        new AlertDialog.Builder(this).setSingleChoiceItems(items, settingIndex, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int sortId = Utils.settingIndexToSortId(whichButton, settingIndex, oldSortId);
                Main.this.getPreferences(0).edit().putInt("sort_by", sortId).commit();
                if (!Main.this.isLoaded()) {
                    dialog.dismiss();
                    return;
                }
                Utils.sort(Main.this.apps, Main.this.onSDCardSet, sortId);
                if (Main.this.searchBar.getVisibility() == 0) {
                    String key = Main.this.searchEditor.getText().toString();
                    if (key == null || key.trim().equals("")) {
                        Main.this.filteredApps = Main.this.apps;
                    } else {
                        Main.this.filteredApps = Utils.filter(Main.this.apps, key);
                    }
                    Main.this.appList.setAdapter((ListAdapter) new AppListAdapter(Main.this, Main.this.filteredApps, Main.this.onSDCardSet));
                    Main.this.appList.startLayoutAnimation();
                } else {
                    Main.this.appList.setAdapter((ListAdapter) new AppListAdapter(Main.this, Main.this.apps, Main.this.onSDCardSet));
                    Main.this.appList.startLayoutAnimation();
                }
                dialog.dismiss();
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void showOptionDialog(final App app, final int position) {
        CharSequence[] item;
        if (!Utils.isAPI8AndAbove()) {
            item = LONG_TOUCH_OPTION_ITEMS;
        } else if (this.onSDCardSet.contains(app.packageName)) {
            item = LONG_TOUCH_OPTION_ITEMS_API8_MOVE_BACK;
        } else {
            item = LONG_TOUCH_OPTION_ITEMS_API8_MOVE_TO;
        }
        new AlertDialog.Builder(this).setItems(item, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    try {
                        Main.this.lanuch(app);
                    } catch (Exception e) {
                        Log.d("Debug", "", e);
                        AlertUtils.alertError(Main.this);
                    }
                } else if (which == 1) {
                    Main.this.rateIt(app, position);
                } else if (which == 2) {
                    Main.this.showDetails(app.packageName, position);
                }
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void lanuch(App app) {
        Intent switchTo = getPackageManager().getLaunchIntentForPackage(app.packageName);
        if (switchTo == null) {
            AlertUtils.alertError(this);
        } else {
            startActivity(switchTo);
        }
    }

    /* access modifiers changed from: private */
    public void rateIt(App app, int position) {
        this.curPosition = position;
        startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + app.packageName)), REQUEST_CODE);
    }

    /* access modifiers changed from: private */
    public void showDetails(String packageName, int position) {
        this.curPosition = position;
        Intent intent = new Intent();
        if (Utils.isAPI9AndAbove()) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", packageName, null));
        } else {
            intent.setAction("android.intent.action.VIEW");
            intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            intent.putExtra("com.android.settings.ApplicationPkgName", packageName);
            intent.putExtra("pkg", packageName);
        }
        startActivityForResult(intent, REQUEST_CODE);
    }

    /* access modifiers changed from: private */
    public synchronized boolean isLoaded() {
        return this.isloaded;
    }

    /* access modifiers changed from: private */
    public synchronized void setLoaded(boolean value) {
        this.isloaded = value;
    }
}
