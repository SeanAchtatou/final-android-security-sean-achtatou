package com.gannicus.android.uninstaller;

public final class R {

    public static final class anim {
        public static final int fade = 2130968576;
        public static final int layout_top_to_bottom_slide = 2130968577;
        public static final int slide_top_to_bottom = 2130968578;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int class_green = 2131034113;
        public static final int class_red = 2131034114;
        public static final int text_color = 2131034115;
        public static final int white = 2131034112;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int login_logo = 2130837505;
        public static final int logo = 2130837506;
        public static final int menu_search = 2130837507;
        public static final int menu_settings = 2130837508;
        public static final int menu_sort = 2130837509;
        public static final int menu_storage = 2130837510;
        public static final int row_selector = 2130837511;
        public static final int sort = 2130837512;
    }

    public static final class id {
        public static final int app_list = 2131230727;
        public static final int auto_orientation = 2131230736;
        public static final int icon = 2131230721;
        public static final int id_hold = 2131230728;
        public static final int info_label = 2131230729;
        public static final int installed_at = 2131230723;
        public static final int internal_available = 2131230741;
        public static final int internal_total = 2131230739;
        public static final int internal_usage_bar = 2131230737;
        public static final int internal_used = 2131230740;
        public static final int large_row_panel = 2131230720;
        public static final int large_ui = 2131230734;
        public static final int loading_panel = 2131230730;
        public static final int logo = 2131230731;
        public static final int memory_usage_text_view = 2131230738;
        public static final int menu_search = 2131230749;
        public static final int menu_settings = 2131230750;
        public static final int menu_sort_by = 2131230747;
        public static final int menu_storage_info = 2131230748;
        public static final int name = 2131230722;
        public static final int on_sdcard = 2131230725;
        public static final int sdcard_available = 2131230746;
        public static final int sdcard_panel = 2131230742;
        public static final int sdcard_total = 2131230744;
        public static final int sdcard_usage_bar = 2131230743;
        public static final int sdcard_used = 2131230745;
        public static final int search_bar = 2131230732;
        public static final int search_editor = 2131230733;
        public static final int size = 2131230724;
        public static final int small_row_panel = 2131230726;
        public static final int small_ui = 2131230735;
    }

    public static final class layout {
        public static final int app_item_large = 2130903040;
        public static final int app_item_small = 2130903041;
        public static final int main = 2130903042;
        public static final int search_bar = 2130903043;
        public static final int settings = 2130903044;
        public static final int storage_usage = 2130903045;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class string {
        public static final int app_name = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
