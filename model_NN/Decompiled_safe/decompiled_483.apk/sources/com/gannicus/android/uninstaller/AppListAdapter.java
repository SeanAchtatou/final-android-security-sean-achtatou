package com.gannicus.android.uninstaller;

import android.content.Context;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class AppListAdapter extends BaseAdapter {
    private static final int COLOR1 = Color.parseColor("#eaeaea");
    private static final int COLOR2 = Color.parseColor("#ffffff");
    private ArrayList<App> apps;
    private Context ctx;
    private int currentLayoutId;
    private int currentViewId;
    private LayoutInflater inflater;
    private HashSet<String> onSDCardSet;

    public AppListAdapter(Context ctx2, ArrayList<App> apps2, HashSet<String> onSDCardSet2) {
        this.ctx = ctx2;
        this.apps = apps2;
        this.onSDCardSet = onSDCardSet2;
        this.inflater = LayoutInflater.from(ctx2);
        loadThemeStyle();
    }

    public void loadThemeStyle() {
        int style = PreferenceManager.getDefaultSharedPreferences(this.ctx).getInt("theme_style", 0);
        if (style == 0) {
            this.currentLayoutId = R.layout.app_item_large;
            this.currentViewId = R.id.large_row_panel;
        } else if (style == 1) {
            this.currentLayoutId = R.layout.app_item_small;
            this.currentViewId = R.id.small_row_panel;
        }
    }

    public View getView(int position, View ret, ViewGroup parent) {
        if (ret == null || ret.getId() != this.currentViewId) {
            ret = this.inflater.inflate(this.currentLayoutId, (ViewGroup) null);
        }
        View onSDCardView = ret.findViewById(R.id.on_sdcard);
        App app = this.apps.get(position);
        ((TextView) ret.findViewById(R.id.name)).setText(app.name);
        ((ImageView) ret.findViewById(R.id.icon)).setBackgroundDrawable(app.icon);
        ((TextView) ret.findViewById(R.id.installed_at)).setText(formatDate(app.installedAt));
        ((TextView) ret.findViewById(R.id.size)).setText(formatSize(app.size));
        if (this.onSDCardSet.contains(app.packageName)) {
            onSDCardView.setVisibility(0);
        } else {
            onSDCardView.setVisibility(8);
        }
        if (position % 2 == 0) {
            ret.setBackgroundColor(COLOR1);
        } else {
            ret.setBackgroundColor(COLOR2);
        }
        return ret;
    }

    public void refresh(ArrayList<App> apps2) {
        this.apps = apps2;
        notifyDataSetChanged();
    }

    private static final String formatSize(int size) {
        StringBuilder buf = new StringBuilder();
        if (size < 1024) {
            buf.append(size).append("KB");
        } else if (size < 1048576) {
            buf.append(formatFloat(((float) size) / 1024.0f)).append("MB");
        } else {
            buf.append(formatFloat((((float) size) / 1024.0f) / 1024.0f)).append("GB");
        }
        return buf.toString();
    }

    private static final float formatFloat(float value) {
        return ((float) ((int) ((value * 100.0f) + 0.5f))) / 100.0f;
    }

    private static final CharSequence formatDate(long installedAt) {
        return DateFormat.format("MM/dd/yyyy", new Date(installedAt));
    }

    public int getCount() {
        return this.apps.size();
    }

    public Object getItem(int position) {
        return this.apps.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }
}
