package com.gannicus.android.uninstaller;

import android.graphics.drawable.Drawable;

public class App {
    public static String COLUMN_NAME_ICON = "icon";
    public static String COLUMN_NAME_INSTALLED_AT = "installedat";
    public static String COLUMN_NAME_NAME = "name";
    public static String COLUMN_NAME_PACKAGE_NAME = "packagename";
    public static String COLUMN_NAME_SIZE = "size";
    public Drawable icon;
    public long installedAt;
    public String name;
    public String packageName;
    public int size;
}
