package com.gannicus.android.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.gannicus.android.uninstaller.App;
import com.gannicus.android.uninstaller.Main;
import com.gannicus.android.uninstaller.R;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

public class Utils {
    public static final int BY_DATE_ASC = 6;
    public static final int BY_DATE_DESC = 0;
    public static final int BY_INSTALL_LOCATION_ASC = 4;
    public static final int BY_INSTALL_LOCATION_DESC = 5;
    public static final int BY_NAME_ASC = 2;
    public static final int BY_NAME_DESC = 3;
    public static final int BY_SIZE_ASC = 7;
    public static final int BY_SIZE_DESC = 1;
    private static final CharSequence[] THEMES = {"Small UI", "Auto-rotate Screen", "Full Screen"};
    private static final CharSequence[] THEMES_FOR_TABLET = {"Small UI", "Auto-rotate Screen"};
    public static final int THEME_STYLE_LARGE = 0;
    public static final int THEME_STYLE_SMALL = 1;

    public static final void showCrashedDialog(final Activity activity) {
        String name = activity.getResources().getString(R.string.app_name);
        StringBuilder buf = new StringBuilder();
        buf.append("\nWhoa! Sorry!\n\n").append(name).append(" has crashed:(\n ");
        new AlertDialog.Builder(activity).setMessage(buf.toString()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                activity.finish();
            }
        }).create().show();
    }

    public static final void showSettingsDialog(final Main main) {
        boolean isSmallUI;
        boolean[] values;
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(main);
        int themeStyle = pref.getInt("theme_style", 0);
        final boolean isTablet = isXLagerScreen(main);
        if (themeStyle == 1) {
            isSmallUI = true;
        } else {
            isSmallUI = false;
        }
        boolean autoOrientation = pref.getBoolean("auto_rotate_screen", isTablet);
        boolean[] tableValues = {isSmallUI, autoOrientation};
        boolean[] phoneValue = {isSmallUI, autoOrientation, pref.getBoolean("full_screen", false)};
        if (isTablet) {
            values = tableValues;
        } else {
            values = phoneValue;
        }
        new AlertDialog.Builder(main).setMultiChoiceItems(isTablet ? THEMES_FOR_TABLET : THEMES, values, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (which == 0) {
                    Utils.changeToSmallUI(isChecked, Main.this);
                } else if (which == 1) {
                    Utils.autoScreenOrientation(isChecked, Main.this);
                } else if (which == 2 && !isTablet) {
                    Utils.fullScreen(isChecked, Main.this);
                }
            }
        }).setNegativeButton("Close", (DialogInterface.OnClickListener) null).create().show();
    }

    public static boolean isXLagerScreen(Activity activity) {
        int w = activity.getResources().getDisplayMetrics().widthPixels;
        int h = activity.getResources().getDisplayMetrics().heightPixels;
        int orientation = activity.getResources().getConfiguration().orientation;
        if (orientation == 2) {
            return w >= 1024 && h >= 768;
        }
        if (orientation == 1) {
            return w >= 768 && h >= 1024;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static void fullScreen(boolean isChecked, Main main) {
        PreferenceManager.getDefaultSharedPreferences(main).edit().putBoolean("full_screen", isChecked).commit();
        Toast.makeText(main, "Setting \"Full Screen\" will take effect until next restart", 0).show();
    }

    /* access modifiers changed from: private */
    public static void autoScreenOrientation(boolean autoRotate, Main main) {
        PreferenceManager.getDefaultSharedPreferences(main).edit().putBoolean("auto_rotate_screen", autoRotate).commit();
        boolean isTablet = isXLagerScreen(main);
        if (autoRotate) {
            main.setRequestedOrientation(-1);
        } else if (!autoRotate && !isTablet) {
            main.setRequestedOrientation(1);
        } else if (!autoRotate && isTablet) {
            main.setRequestedOrientation(0);
        }
    }

    /* access modifiers changed from: private */
    public static void changeToSmallUI(boolean isChecked, Main main) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(main).edit();
        if (isChecked) {
            editor.putInt("theme_style", 1).commit();
        } else {
            editor.putInt("theme_style", 0).commit();
        }
        main.reloadThemeToUI();
    }

    public static final void sort(ArrayList<App> apps, HashSet<String> onSDCardSet, int sortType) {
        if (sortType == 4 || sortType == 5) {
            sortByInstallLocation(apps, onSDCardSet, sortType);
        } else {
            sort(apps, sortType);
        }
    }

    private static final void sort(ArrayList<App> apps, int sortType) {
        Comparator<App> comparator;
        if (sortType == 0) {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app2.installedAt >= app1.installedAt ? 1 : -1;
                }
            };
        } else if (sortType == 6) {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app2.installedAt < app1.installedAt ? 1 : -1;
                }
            };
        } else if (sortType == 1) {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app2.size - app1.size;
                }
            };
        } else if (sortType == 7) {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app1.size - app2.size;
                }
            };
        } else if (sortType == 2) {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app1.name.compareToIgnoreCase(app2.name);
                }
            };
        } else if (sortType == 3) {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app2.name.compareToIgnoreCase(app1.name);
                }
            };
        } else {
            comparator = new Comparator<App>() {
                public int compare(App app1, App app2) {
                    return app2.installedAt >= app1.installedAt ? 1 : -1;
                }
            };
        }
        SortUtils.sort(apps, comparator);
    }

    private static final void sortByInstallLocation(ArrayList<App> apps, HashSet<String> onCardSet, int sortType) {
        boolean isAsc;
        if (sortType == 4) {
            isAsc = true;
        } else if (sortType == 5) {
            isAsc = false;
        } else {
            return;
        }
        ArrayList<App> onSdCard = new ArrayList<>();
        ArrayList<App> others = new ArrayList<>();
        Iterator<App> it = apps.iterator();
        while (it.hasNext()) {
            App e = it.next();
            if (onCardSet.contains(e.packageName)) {
                onSdCard.add(e);
            } else {
                others.add(e);
            }
        }
        sort(onSdCard, 2);
        sort(others, 2);
        apps.clear();
        if (isAsc) {
            Iterator it2 = others.iterator();
            while (it2.hasNext()) {
                apps.add((App) it2.next());
            }
            Iterator it3 = onSdCard.iterator();
            while (it3.hasNext()) {
                apps.add((App) it3.next());
            }
            return;
        }
        Iterator it4 = onSdCard.iterator();
        while (it4.hasNext()) {
            apps.add((App) it4.next());
        }
        Iterator it5 = others.iterator();
        while (it5.hasNext()) {
            apps.add((App) it5.next());
        }
    }

    public static final ArrayList<App> filter(ArrayList<App> apps, String key) {
        ArrayList<App> filtered = new ArrayList<>();
        Iterator<App> it = apps.iterator();
        while (it.hasNext()) {
            App e = it.next();
            if (e.name.toLowerCase().contains(key.toLowerCase())) {
                filtered.add(e);
            }
        }
        return filtered;
    }

    public static final boolean isAPI8AndAbove() {
        int apiLevel = 0;
        try {
            apiLevel = Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (Exception e) {
        }
        return apiLevel >= 8;
    }

    public static final boolean isAPI9AndAbove() {
        int apiLevel = 0;
        try {
            apiLevel = Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (Exception e) {
        }
        return apiLevel >= 9;
    }

    public static final String getSDCardPath() {
        return Environment.getExternalStorageDirectory().getPath();
    }

    public static final String getInternalStoragePath() {
        return Environment.getDataDirectory().getAbsolutePath();
    }

    public static final int sortIdToSettingIndex(int sortId) {
        switch (sortId) {
            case 0:
            case BY_DATE_ASC /*6*/:
            default:
                return 0;
            case 1:
            case BY_SIZE_ASC /*7*/:
                return 1;
            case 2:
            case 3:
                return 2;
            case 4:
            case BY_INSTALL_LOCATION_DESC /*5*/:
                return 3;
        }
    }

    public static int settingIndexToSortId(int whichButton, int oldWhichButton, int oldSortId) {
        if (whichButton == oldWhichButton) {
            if (oldSortId == 6) {
                return 0;
            }
            if (oldSortId == 0) {
                return 6;
            }
            if (oldSortId == 7) {
                return 1;
            }
            if (oldSortId == 1) {
                return 7;
            }
            if (oldSortId == 2) {
                return 3;
            }
            if (oldSortId == 3) {
                return 2;
            }
            if (oldSortId == 4) {
                return 5;
            }
            if (oldSortId == 5) {
                return 4;
            }
        } else if (whichButton == 0) {
            return 0;
        } else {
            if (whichButton == 1) {
                return 1;
            }
            if (whichButton == 2) {
                return 2;
            }
            if (whichButton == 3) {
                return 5;
            }
        }
        return 0;
    }

    public static final String getTheFirstTip() {
        if (isAPI8AndAbove()) {
            return "Tap item to uninstall application!\n\nLong click item to launch, rate, comment application and move it to SD card or move it back";
        }
        return "Tap item to uninstall application!\n\nLong click item to launch, rate, comment application and view details about it";
    }
}
