package com.gannicus.android.api;

import android.content.Context;
import android.widget.Toast;

public class AlertUtils {
    private static final String ERROR = "Opps !  I got error.  Sorry:(";

    public static final void alert(Context ctx, String content) {
        Toast.makeText(ctx, content, 0).show();
    }

    public static final void alertError(Context ctx) {
        alert(ctx, ERROR);
    }
}
