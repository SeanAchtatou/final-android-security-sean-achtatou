package com.gannicus.android.api;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import com.gannicus.android.uninstaller.R;

public class SupporterUtils {
    private static final String DEVELOPER = "\"Mini Guy\"";
    private static final CharSequence[] LIKE_ME_OPTION_ITEMS = {"Rate it 5 stars", "More apps", "Email us"};
    private static final String MARKET_URL = "market://details?id=com.mini.android.uninstaller";
    private static final CharSequence[] NOT_THAT_GREAT_OPTION_ITEMS = {"Email us"};
    private static final String SEARCH_BY_DEVELOPER_URI = "market://search?q=pub:\"Mini Guy\"";
    private static final String SUPPORTER_EMIAL = "bauermini@gmail.com";

    public static final void showLikeMeDialog(final Context ctx) {
        String appName = ctx.getResources().getString(R.string.app_name);
        StringBuilder buf = new StringBuilder();
        buf.append("I like this app (").append(appName).append(")");
        final String subject = buf.toString();
        new AlertDialog.Builder(ctx).setItems(LIKE_ME_OPTION_ITEMS, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    ctx.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(SupporterUtils.MARKET_URL)));
                } else if (which == 1) {
                    ctx.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(SupporterUtils.SEARCH_BY_DEVELOPER_URI)));
                } else if (which == 2) {
                    String body = SupporterUtils.createBody(ctx);
                    Intent sendIntent = new Intent("android.intent.action.SEND");
                    sendIntent.putExtra("android.intent.extra.SUBJECT", subject);
                    sendIntent.putExtra("android.intent.extra.TEXT", body);
                    sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{SupporterUtils.SUPPORTER_EMIAL});
                    sendIntent.setType("message/rfc822");
                    ctx.startActivity(sendIntent);
                }
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public static final String createBody(Context ctx) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;
        StringBuilder buf = new StringBuilder();
        buf.append("\n\n\n\n").append("----------\nMy phone:\n").append(Build.MODEL).append("\n").append(w).append("x").append(h).append("\nAndroid ").append(Build.VERSION.RELEASE);
        return buf.toString();
    }

    public static final void showNotThatGreat(final Context ctx) {
        String appName = ctx.getResources().getString(R.string.app_name);
        StringBuilder buf = new StringBuilder();
        buf.append("Not that great (").append(appName).append(")");
        final String subject = buf.toString();
        new AlertDialog.Builder(ctx).setItems(NOT_THAT_GREAT_OPTION_ITEMS, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    String body = SupporterUtils.createBody(ctx);
                    Intent sendIntent = new Intent("android.intent.action.SEND");
                    sendIntent.putExtra("android.intent.extra.SUBJECT", subject);
                    sendIntent.putExtra("android.intent.extra.TEXT", body);
                    sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{SupporterUtils.SUPPORTER_EMIAL});
                    sendIntent.setType("message/rfc822");
                    ctx.startActivity(sendIntent);
                }
            }
        }).create().show();
    }
}
