package com.gannicus.android.api;

import android.app.Activity;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class AdmobUtils {
    public static final void showAd(Activity activity, LinearLayout holder) {
        AdView adView = new AdView(activity, AdSize.BANNER, "a14c4b44371d0bf");
        holder.addView(adView);
        adView.loadAd(new AdRequest());
    }
}
