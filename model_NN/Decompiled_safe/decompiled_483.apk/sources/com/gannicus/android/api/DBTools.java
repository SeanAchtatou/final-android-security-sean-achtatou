package com.gannicus.android.api;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.gannicus.android.uninstaller.App;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class DBTools {
    public static final String DATABASE_NAME = "mini_uninstaller_2010722.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "appcache";
    private static DBTools instance;
    private DatabaseHelper helper;

    private DBTools(Context ctx) {
        this.helper = new DatabaseHelper(ctx);
    }

    public static DBTools getInstance(Context ctx) {
        if (instance == null) {
            instance = new DBTools(ctx);
        }
        return instance;
    }

    public final void close() {
        this.helper.close();
    }

    public final void save(App app) {
        ContentValues values = new ContentValues();
        values.put(App.COLUMN_NAME_NAME, app.name);
        values.put(App.COLUMN_NAME_INSTALLED_AT, Long.valueOf(app.installedAt));
        values.put(App.COLUMN_NAME_PACKAGE_NAME, app.packageName);
        values.put(App.COLUMN_NAME_SIZE, Integer.valueOf(app.size));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = drawableToBitmap(app.icon);
        if (!bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)) {
            baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] b = baos.toByteArray();
        try {
            baos.close();
        } catch (IOException e) {
        }
        values.put(App.COLUMN_NAME_ICON, b);
        this.helper.getWritableDatabase().insert(TABLE_NAME, null, values);
    }

    private static final Bitmap drawableToBitmap(Drawable icon) {
        if (icon instanceof BitmapDrawable) {
            return ((BitmapDrawable) icon).getBitmap();
        }
        int width = icon.getIntrinsicWidth();
        int height = icon.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        icon.setBounds(0, 0, width, height);
        icon.draw(canvas);
        return bitmap;
    }

    public final void update(App app) {
        ContentValues values = new ContentValues();
        values.put(App.COLUMN_NAME_INSTALLED_AT, Long.valueOf(app.installedAt));
        values.put(App.COLUMN_NAME_PACKAGE_NAME, app.packageName);
        values.put(App.COLUMN_NAME_SIZE, Integer.valueOf(app.size));
        StringBuffer buf = new StringBuffer();
        buf.append(App.COLUMN_NAME_PACKAGE_NAME).append(" == \"").append(app.packageName).append("\"");
        this.helper.getWritableDatabase().update(TABLE_NAME, values, buf.toString(), null);
    }

    public final ArrayList[] getCachedAppInfo() {
        Cursor c = this.helper.getWritableDatabase().query(TABLE_NAME, new String[]{App.COLUMN_NAME_PACKAGE_NAME, App.COLUMN_NAME_INSTALLED_AT}, null, null, null, null, String.valueOf(App.COLUMN_NAME_INSTALLED_AT) + " DESC");
        int length = c.getCount();
        ArrayList<String> packageNames = new ArrayList<>();
        ArrayList<Long> installedAts = new ArrayList<>();
        c.moveToFirst();
        for (int i = 0; i != length; i++) {
            packageNames.add(c.getString(c.getColumnIndex(App.COLUMN_NAME_PACKAGE_NAME)));
            installedAts.add(Long.valueOf(c.getLong(c.getColumnIndex(App.COLUMN_NAME_INSTALLED_AT))));
            c.moveToNext();
        }
        c.close();
        return new ArrayList[]{packageNames, installedAts};
    }

    public final ArrayList<App> find() {
        Cursor c = this.helper.getWritableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<App> ret = new ArrayList<>();
        int length = c.getCount();
        c.moveToFirst();
        for (int i = 0; i != length; i++) {
            App app = new App();
            app.name = c.getString(c.getColumnIndex(App.COLUMN_NAME_NAME));
            byte[] b = c.getBlob(c.getColumnIndex(App.COLUMN_NAME_ICON));
            app.icon = new BitmapDrawable(BitmapFactory.decodeByteArray(b, 0, b.length));
            app.packageName = c.getString(c.getColumnIndex(App.COLUMN_NAME_PACKAGE_NAME));
            app.installedAt = c.getLong(c.getColumnIndex(App.COLUMN_NAME_INSTALLED_AT));
            app.size = c.getInt(c.getColumnIndex(App.COLUMN_NAME_SIZE));
            ret.add(app);
            c.moveToNext();
        }
        c.close();
        return ret;
    }

    public final int remove(App app) {
        StringBuffer buf = new StringBuffer();
        buf.append(App.COLUMN_NAME_PACKAGE_NAME).append(" = \"").append(app.packageName).append("\"");
        return this.helper.getWritableDatabase().delete(TABLE_NAME, buf.toString(), null);
    }

    public final int removeAllByPackageNames(ArrayList<String> packagenames) {
        if (packagenames.size() == 0) {
            return 0;
        }
        StringBuffer buf = new StringBuffer();
        Iterator<String> it = packagenames.iterator();
        while (it.hasNext()) {
            buf.append(" OR ").append(App.COLUMN_NAME_PACKAGE_NAME).append(" = \"").append(it.next()).append("\"");
        }
        if (buf.length() > 0) {
            buf.delete(0, 4);
        }
        return this.helper.getWritableDatabase().delete(TABLE_NAME, buf.toString(), null);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DBTools.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(new StringBuffer().append("CREATE TABLE ").append(DBTools.TABLE_NAME).append(" (").append(App.COLUMN_NAME_PACKAGE_NAME).append(" TEXT PRIMARY KEY,").append(App.COLUMN_NAME_NAME).append(" TEXT,").append(App.COLUMN_NAME_INSTALLED_AT).append(" INTEGER,").append(App.COLUMN_NAME_ICON).append(" BLOB,").append(App.COLUMN_NAME_SIZE).append(" INTEGER").append(");").toString());
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }
}
