package com.gannicus.android.api;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class SortUtils {
    private static final int INSERTIONSORT_THRESHOLD = 7;

    public static <T> void sort(List<T> list, Comparator<? super T> c) {
        Object[] a = list.toArray();
        sort(a, c);
        ListIterator i = list.listIterator();
        for (Object obj : a) {
            i.next();
            i.set(obj);
        }
    }

    public static <T> void sort(T[] a, Comparator<? super T> c) {
        Object[] aux = (Object[]) a.clone();
        if (c == null) {
            mergeSort(aux, a, 0, a.length, 0);
            return;
        }
        mergeSort(aux, a, 0, a.length, 0, c);
    }

    /* JADX INFO: Multiple debug info for r6v4 int: [D('p' int), D('q' int)] */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00a0, code lost:
        if (r21.compare(r16[r6], r16[r7]) <= 0) goto L_0x00a2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void mergeSort(java.lang.Object[] r16, java.lang.Object[] r17, int r18, int r19, int r20, java.util.Comparator r21) {
        /*
            int r14 = r19 - r18
            r5 = 7
            if (r14 >= r5) goto L_0x0038
            r16 = r18
        L_0x0007:
            r0 = r16
            r1 = r19
            if (r0 < r1) goto L_0x000e
        L_0x000d:
            return
        L_0x000e:
            r20 = r16
        L_0x0010:
            r0 = r20
            r1 = r18
            if (r0 <= r1) goto L_0x0027
            r5 = 1
            int r5 = r20 - r5
            r5 = r17[r5]
            r6 = r17[r20]
            r0 = r21
            r1 = r5
            r2 = r6
            int r5 = r0.compare(r1, r2)
            if (r5 > 0) goto L_0x002a
        L_0x0027:
            int r16 = r16 + 1
            goto L_0x0007
        L_0x002a:
            r5 = 1
            int r5 = r20 - r5
            r0 = r17
            r1 = r20
            r2 = r5
            swap(r0, r1, r2)
            int r20 = r20 + -1
            goto L_0x0010
        L_0x0038:
            r13 = r18
            r12 = r19
            int r18 = r18 + r20
            int r19 = r19 + r20
            int r5 = r18 + r19
            int r8 = r5 >>> 1
            r0 = r20
            int r0 = -r0
            r9 = r0
            r5 = r17
            r6 = r16
            r7 = r18
            r10 = r21
            mergeSort(r5, r6, r7, r8, r9, r10)
            r0 = r20
            int r0 = -r0
            r10 = r0
            r6 = r17
            r7 = r16
            r9 = r19
            r11 = r21
            mergeSort(r6, r7, r8, r9, r10, r11)
            r20 = 1
            int r20 = r8 - r20
            r20 = r16[r20]
            r5 = r16[r8]
            r0 = r21
            r1 = r20
            r2 = r5
            int r20 = r0.compare(r1, r2)
            if (r20 > 0) goto L_0x0081
            r0 = r16
            r1 = r18
            r2 = r17
            r3 = r13
            r4 = r14
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            goto L_0x000d
        L_0x0081:
            r20 = r13
            r5 = r18
            r6 = r8
            r7 = r6
            r6 = r5
        L_0x0088:
            r0 = r20
            r1 = r12
            if (r0 >= r1) goto L_0x000d
            r0 = r7
            r1 = r19
            if (r0 >= r1) goto L_0x00a2
            if (r6 >= r8) goto L_0x00ae
            r5 = r16[r6]
            r9 = r16[r7]
            r0 = r21
            r1 = r5
            r2 = r9
            int r5 = r0.compare(r1, r2)
            if (r5 > 0) goto L_0x00ae
        L_0x00a2:
            int r5 = r6 + 1
            r6 = r16[r6]
            r17[r20] = r6
            r6 = r7
        L_0x00a9:
            int r20 = r20 + 1
            r7 = r6
            r6 = r5
            goto L_0x0088
        L_0x00ae:
            int r5 = r7 + 1
            r7 = r16[r7]
            r17[r20] = r7
            r15 = r5
            r5 = r6
            r6 = r15
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gannicus.android.api.SortUtils.mergeSort(java.lang.Object[], java.lang.Object[], int, int, int, java.util.Comparator):void");
    }

    /* JADX INFO: Multiple debug info for r1v1 int: [D('p' int), D('destLow' int)] */
    /* JADX INFO: Multiple debug info for r2v1 int: [D('length' int), D('q' int)] */
    private static void mergeSort(Object[] src, Object[] dest, int low, int high, int off) {
        int p;
        int q;
        int length = high - low;
        if (length < 7) {
            for (int i = low; i < high; i++) {
                int j = i;
                while (j > low && ((Comparable) dest[j - 1]).compareTo(dest[j]) > 0) {
                    swap(dest, j, j - 1);
                    j--;
                }
            }
            return;
        }
        int destLow = low;
        int destHigh = high;
        int low2 = low + off;
        int high2 = high + off;
        int mid = (low2 + high2) >>> 1;
        mergeSort(dest, src, low2, mid, -off);
        mergeSort(dest, src, mid, high2, -off);
        if (((Comparable) src[mid - 1]).compareTo(src[mid]) <= 0) {
            System.arraycopy(src, low2, dest, destLow, length);
            return;
        }
        int i2 = destLow;
        int q2 = mid;
        int q3 = low2;
        while (i2 < destHigh) {
            if (q2 >= high2 || (q3 < mid && ((Comparable) src[q3]).compareTo(src[q2]) <= 0)) {
                q = q3 + 1;
                dest[i2] = src[q3];
                p = q2;
            } else {
                dest[i2] = src[q2];
                q = q3;
                p = q2 + 1;
            }
            i2++;
            q2 = p;
            q3 = q;
        }
    }

    private static void swap(Object[] x, int a, int b) {
        Object t = x[a];
        x[a] = x[b];
        x[b] = t;
    }
}
