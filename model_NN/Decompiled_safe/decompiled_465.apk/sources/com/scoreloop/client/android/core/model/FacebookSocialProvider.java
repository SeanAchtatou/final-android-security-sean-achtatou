package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.a.a.g;
import com.a.a.k;
import com.scoreloop.client.android.core.controller.FacebookSocialProviderController;
import org.json.JSONException;
import org.json.JSONObject;

public class FacebookSocialProvider extends SocialProvider {
    public static final String IDENTIFIER = "com.facebook.v1";

    /* renamed from: a  reason: collision with root package name */
    private static g f92a;

    private void a(User user, JSONObject jSONObject, Context context) {
        if (user == null) {
            throw new IllegalArgumentException();
        }
        boolean isUserConnected = isUserConnected(user);
        try {
            user.a(jSONObject.getJSONObject("facebook"), getName());
            boolean isUserConnected2 = isUserConnected(user);
            if (isUserConnected != isUserConnected2 && isUserConnected2 && d().c()) {
                d().b(context);
            }
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public static g c() {
        if (f92a == null) {
            f92a = g.a("95076661cafb5979188d7a687991d339", "f78b158bfc03c4859b7076bd66e9cd3c", (k) null);
        }
        return f92a;
    }

    public Class a() {
        return FacebookSocialProviderController.class;
    }

    public void a(User user, Long l, Context context) {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject.put("uid", l);
            jSONObject2.put("facebook", jSONObject);
            a(user, jSONObject2, context);
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public g d() {
        return c();
    }

    /* access modifiers changed from: protected */
    public void d(User user, JSONObject jSONObject) {
        if (user == null || jSONObject == null) {
            throw new IllegalArgumentException();
        } else if (user.e() != null) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                try {
                    jSONObject2.put("uid", Long.valueOf(user.e().getLong("uid")));
                    try {
                        jSONObject.put("facebook", jSONObject2);
                    } catch (JSONException e) {
                        throw new IllegalStateException(e);
                    }
                } catch (JSONException e2) {
                    throw new IllegalStateException(e2);
                }
            } catch (JSONException e3) {
                throw new IllegalStateException(e3);
            }
        }
    }

    public String getIdentifier() {
        return IDENTIFIER;
    }

    public boolean isUserConnected(User user) {
        return user.e() != null;
    }
}
