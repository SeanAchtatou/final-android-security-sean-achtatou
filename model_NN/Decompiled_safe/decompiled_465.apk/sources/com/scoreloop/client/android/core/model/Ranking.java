package com.scoreloop.client.android.core.model;

import org.json.JSONObject;

public class Ranking {

    /* renamed from: a  reason: collision with root package name */
    private Integer f100a;
    private Integer b;

    public void a(JSONObject jSONObject) {
        this.f100a = Integer.valueOf(jSONObject.getInt("rank"));
        this.b = Integer.valueOf(jSONObject.getInt("total"));
        if (this.f100a.intValue() == 0) {
            this.f100a = null;
        }
    }

    public Integer getRank() {
        return this.f100a;
    }
}
