package com.scoreloop.client.android.core.model;

import java.util.List;
import org.json.JSONObject;

public class SearchList {

    /* renamed from: a  reason: collision with root package name */
    private final String f102a;
    private String b;

    public SearchList(String str) {
        this.f102a = str;
    }

    SearchList(String str, String str2) {
        this(str);
        this.b = str2;
    }

    public SearchList(JSONObject jSONObject) {
        this.f102a = jSONObject.getString("id");
        this.b = jSONObject.getString("name");
    }

    static SearchList a(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        List<SearchList> c = Session.getCurrentSession().getUser().c();
        if (c != null) {
            for (SearchList searchList : c) {
                if (str.equalsIgnoreCase(searchList.getIdentifier())) {
                    return searchList;
                }
            }
        }
        return new SearchList(str, "nameless");
    }

    public static SearchList buddiesScoreSearchList() {
        return a("701bb990-80d8-11de-8a39-0800200c9a66");
    }

    public static SearchList defaultScoreSearchList() {
        List c = Session.getCurrentSession().getUser().c();
        return (c == null || c.size() <= 0) ? globalScoreSearchList() : (SearchList) c.get(0);
    }

    public static SearchList globalScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a1");
    }

    public static SearchList localScoreSearchList() {
        return a("#local");
    }

    public static SearchList twentyFourHourScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a2");
    }

    public static SearchList userCountryScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a3");
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SearchList)) {
            return super.equals(obj);
        }
        SearchList searchList = (SearchList) obj;
        if (getIdentifier() != null && searchList.getIdentifier() != null) {
            return getIdentifier().equalsIgnoreCase(searchList.getIdentifier());
        }
        throw new IllegalStateException();
    }

    public String getIdentifier() {
        return this.f102a;
    }

    public String getName() {
        return this.b;
    }

    public int hashCode() {
        if (getIdentifier() != null) {
            return getIdentifier().hashCode();
        }
        throw new IllegalStateException();
    }

    public void setName(String str) {
        this.b = str;
    }

    public String toString() {
        return getName();
    }
}
