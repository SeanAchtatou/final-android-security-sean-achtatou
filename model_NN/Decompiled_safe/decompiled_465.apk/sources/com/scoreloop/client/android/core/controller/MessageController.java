package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Message;
import com.scoreloop.client.android.core.model.MessageControllerEmailReceiver;
import com.scoreloop.client.android.core.model.MessageReceiver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import java.util.List;

public class MessageController extends RequestController {
    private Message c;

    public MessageController(RequestControllerObserver requestControllerObserver) {
        this(Session.getCurrentSession(), requestControllerObserver);
    }

    public MessageController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    /* access modifiers changed from: private */
    public Message i() {
        if (this.c == null) {
            this.c = new Message();
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        return true;
    }

    public void addReceiverWithUsers(Object obj, List list) {
        if (obj == null) {
            throw new IllegalArgumentException("null argument");
        }
        if (obj instanceof MessageControllerEmailReceiver) {
            if (list == null || list.size() == 0) {
                throw new IllegalArgumentException("some users should be passed");
            }
        } else if (!SocialProvider.a(obj.getClass())) {
            throw new IllegalArgumentException("we don't support such provider");
        }
        i().a(new MessageReceiver(obj, list));
    }

    public int countReceivers() {
        return i().c().size();
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public Object getTarget() {
        return i().a();
    }

    public String getText() {
        return i().b();
    }

    public boolean isPostAllowed() {
        if (getTarget() == null) {
            return false;
        }
        if (i().c().isEmpty()) {
            return false;
        }
        for (MessageReceiver a2 : i().c()) {
            Object a3 = a2.a();
            if ((a3 instanceof SocialProvider) && !((SocialProvider) a3).isUserConnected(f())) {
                return false;
            }
        }
        return true;
    }

    public void postMessage() {
        if (!isPostAllowed()) {
            throw new IllegalStateException("posting is not allowed");
        }
        h();
        a(new T(this, d()));
    }

    public void removeAllReceiversOfType(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException();
        }
        Logger.a("removeAll invoked");
        for (MessageReceiver messageReceiver : i().c()) {
            Logger.a("checking " + messageReceiver.a() + " against " + cls);
            if (messageReceiver.a().getClass().equals(cls)) {
                Logger.a("got it, removing");
                i().b(messageReceiver);
                return;
            }
            Logger.a("nooope");
        }
    }

    public void setTarget(Object obj) {
        i().a(obj);
    }

    public void setText(String str) {
        i().a(str);
    }
}
