package com.scoreloop.client.android.core.server;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.utils.Base64;
import com.scoreloop.client.android.core.utils.Logger;
import java.net.URL;
import java.nio.channels.IllegalSelectorException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONException;
import org.json.JSONObject;

public class Server {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f116a = (!Server.class.desiredAssertionStatus());
    private final URL b;
    private final e c = new e(this.d);
    private final j d = new j(this);
    private c e;
    /* access modifiers changed from: private */
    public Request f;
    private final LinkedList g = new LinkedList();

    public Server(URL url) {
        this.b = url;
        this.c.setPriority(1);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            instance.reset();
            instance.update("https://api.scoreloop.com/bayeux/android/v2".getBytes());
            byte[] digest = instance.digest();
            instance.reset();
            instance.update("https://www.scoreloop.com/android/updates".getBytes());
            byte[] digest2 = instance.digest();
            byte[] bArr = new byte[16];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) ((digest[(i + 6) % digest.length] ^ digest2[(i + 3) % digest2.length]) ^ 62);
            }
            this.e = new c(b(), this.c, bArr);
            for (int i2 = 0; i2 < digest.length; i2++) {
                digest[i2] = (byte) (digest[i2] ^ 26);
            }
            this.e.b(Base64.a(digest));
            for (int i3 = 0; i3 < digest2.length; i3++) {
                digest2[i3] = (byte) (digest2[i3] ^ 53);
            }
            this.e.a(Base64.a(digest2));
            this.c.a(this.e);
            this.c.start();
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException();
        }
    }

    private void c() {
        if (this.f != null) {
            Logger.a("Server", "doCancelCurrentRequest canceling request: " + this.f.toString());
            this.f.n();
            this.f.g().a(this.f);
            this.c.b();
        }
    }

    private void c(Request request) {
        Logger.a("Server", "startProcessingRequest: " + request.toString());
        if (!f116a && request == null) {
            throw new AssertionError();
        } else if (!f116a && request.m()) {
            throw new AssertionError();
        } else if (f116a || this.f == null) {
            this.f = request;
            this.f.g().b(this.f);
            this.f.p();
            this.c.a(this.f);
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        Logger.a("dumpQueue: ", "currentRequest: " + (this.f != null ? this.f.toString() : "null"));
        Iterator it = this.g.iterator();
        while (it.hasNext()) {
            Request request = (Request) it.next();
            Logger.a("dumpQueue: ", request.toString() + ", " + request.l().toString());
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        Request request;
        if (f116a || this.f == null) {
            do {
                request = (Request) this.g.poll();
                if (request != null && !request.m()) {
                    c(request);
                    return;
                }
            } while (request != null);
            return;
        }
        throw new AssertionError();
    }

    public void a() {
        this.c.a();
    }

    public void a(Game game) {
        this.c.a(game);
    }

    public void a(Request request) {
        Logger.a("Server", "addRequest: " + request.toString());
        if (request.l() == Request.State.ENQUEUED || request.l() == Request.State.EXECUTING) {
            throw new IllegalStateException("Request already enqueued or executing");
        } else if (request.c() == null) {
            throw new IllegalStateException("Request channel is not set");
        } else if (request.b() == null) {
            throw new IllegalStateException("Request method is not set");
        } else {
            if (request.i() == null) {
                request.a(new JSONObject());
            }
            try {
                request.i().put("method", request.b().toString());
                if (this.f != null || !this.g.isEmpty()) {
                    request.o();
                    this.g.add(request);
                    return;
                }
                c(request);
            } catch (JSONException e2) {
                throw new IllegalSelectorException();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public URL b() {
        return this.b;
    }

    public void b(Request request) {
        Logger.a("Server", "cancelRequest: " + request.toString());
        if (this.f == request) {
            c();
            return;
        }
        request.n();
        request.g().a(request);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        a();
        super.finalize();
    }
}
