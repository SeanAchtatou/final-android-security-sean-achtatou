package com.scoreloop.client.android.core.utils;

import java.io.File;
import java.io.FileWriter;

public abstract class Logger {

    /* renamed from: a  reason: collision with root package name */
    private static final String f150a = (File.separator + "sdcard" + File.separator + "scoreloop" + File.separator);

    public static void a(String str) {
        a("Scoreloop", str);
    }

    public static void a(String str, String str2) {
    }

    public static void a(String str, String str2, Exception exc) {
    }

    public static void b(String str) {
        c("Scoreloop", str);
    }

    public static void b(String str, String str2) {
    }

    public static void c(String str, String str2) {
    }

    public static void d(String str, String str2) {
        File file = new File(f150a);
        if (!file.exists()) {
            file.mkdirs();
        }
        String str3 = file.getAbsolutePath() + File.separator + str + "_" + System.currentTimeMillis() + ".log";
        File file2 = new File(str3);
        file2.createNewFile();
        FileWriter fileWriter = new FileWriter(file2);
        a("printTOSDCard", "printing to:" + str3);
        fileWriter.append((CharSequence) str2);
        fileWriter.flush();
        fileWriter.close();
        a("printTOSDCard", "print to:" + str3 + " finished");
    }
}
