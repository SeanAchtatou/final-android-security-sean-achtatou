package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.c  reason: case insensitive filesystem */
class C0003c extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Device f70a;
    private final Game b;

    public C0003c(RequestCompletionCallback requestCompletionCallback, Game game, Device device) {
        super(requestCompletionCallback);
        this.b = game;
        this.f70a = device;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("device_id", this.f70a.a());
            jSONObject.put("user", jSONObject2);
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid device data");
        }
    }

    public RequestMethod b() {
        return RequestMethod.POST;
    }

    public String c() {
        if (this.b == null) {
            return "/service/session";
        }
        return String.format("/service/games/%s/session", this.b.getIdentifier());
    }
}
