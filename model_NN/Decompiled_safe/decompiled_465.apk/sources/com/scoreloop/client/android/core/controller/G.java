package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.PaymentCredential;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

abstract class G extends Request {
    protected final Game c;
    protected final User e;
    protected final PaymentCredential f;
    protected final JSONObject g;
    final /* synthetic */ PaymentController h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public G(PaymentController paymentController, RequestCompletionCallback requestCompletionCallback, Game game, User user, PaymentCredential paymentCredential, JSONObject jSONObject) {
        super(requestCompletionCallback);
        this.h = paymentController;
        this.c = game;
        this.e = user;
        this.f = paymentCredential;
        this.g = jSONObject;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject d = d();
            if (this.g != null) {
                Iterator<String> keys = this.g.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    d.put(next, this.g.get(next));
                }
            }
            jSONObject.put("payment", d);
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid payment data", e2);
        }
    }

    /* access modifiers changed from: protected */
    public abstract JSONObject d();

    /* access modifiers changed from: protected */
    public abstract C0013m e();
}
