package com.scoreloop.client.android.core.utils;

import java.text.SimpleDateFormat;

public abstract class Formats {

    /* renamed from: a  reason: collision with root package name */
    public static final SimpleDateFormat f149a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd");
}
