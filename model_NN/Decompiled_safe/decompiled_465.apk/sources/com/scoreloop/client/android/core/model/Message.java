package com.scoreloop.client.android.core.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Message {

    /* renamed from: a  reason: collision with root package name */
    private Object f94a;
    private String b;
    private List c = new ArrayList();

    private JSONArray e() {
        JSONArray jSONArray = new JSONArray();
        for (MessageReceiver b2 : c()) {
            try {
                jSONArray.put(b2.b());
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
        return jSONArray;
    }

    private JSONObject f() {
        if (a() == null) {
            throw new IllegalStateException();
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("id", ((MessageTarget) a()).getIdentifier());
            jSONObject.put("target_type", ((MessageTarget) a()).b());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public Object a() {
        return this.f94a;
    }

    public void a(MessageReceiver messageReceiver) {
        if (!this.c.contains(messageReceiver)) {
            this.c.add(messageReceiver);
        }
    }

    public void a(Object obj) {
        if (!(obj instanceof MessageTarget)) {
            throw new IllegalArgumentException();
        }
        this.f94a = obj;
    }

    public void a(String str) {
        this.b = str;
    }

    public String b() {
        return this.b;
    }

    public void b(MessageReceiver messageReceiver) {
        this.c.remove(messageReceiver);
    }

    public List c() {
        return this.c;
    }

    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        if (b() != null) {
            jSONObject.put("text", b());
        }
        jSONObject.put("target", f());
        jSONObject.put("receivers", e());
        return jSONObject;
    }
}
