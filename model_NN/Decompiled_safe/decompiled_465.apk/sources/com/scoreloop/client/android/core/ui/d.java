package com.scoreloop.client.android.core.ui;

import com.scoreloop.client.android.core.ui.MyspaceCredentialsDialog;
import java.net.MalformedURLException;

class d implements MyspaceCredentialsDialog.MyspaceCredentialsListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MySpaceAuthViewController f137a;

    d(MySpaceAuthViewController mySpaceAuthViewController) {
        this.f137a = mySpaceAuthViewController;
    }

    public void a() {
        this.f137a.e.dismiss();
        this.f137a.a().userDidCancel();
    }

    public void a(String str, String str2) {
        try {
            this.f137a.a(str, str2, this.f137a.b("https://roa.myspace.com/roa/09/messaging/login"));
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }
}
