package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import java.nio.channels.IllegalSelectorException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.j  reason: case insensitive filesystem */
class C0010j extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Device f76a;
    private final J b;

    public C0010j(RequestCompletionCallback requestCompletionCallback, Device device, J j) {
        super(requestCompletionCallback);
        this.f76a = device;
        this.b = j;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            switch (C0019s.f82a[this.b.ordinal()]) {
                case 1:
                    jSONObject.put("uuid", this.f76a.f());
                    jSONObject.put("system", this.f76a.b());
                    break;
                case 2:
                    jSONObject.put("device", this.f76a.g());
                    break;
                case 3:
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("uuid", this.f76a.f());
                    jSONObject2.put("id", this.f76a.a());
                    jSONObject2.put("system", this.f76a.b());
                    jSONObject2.put("state", "freed");
                    jSONObject.put("device", jSONObject2);
                    break;
                case 4:
                    jSONObject.put("device", this.f76a.g());
                default:
                    throw new IllegalSelectorException();
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid device data", e);
        }
    }

    public RequestMethod b() {
        switch (C0019s.f82a[this.b.ordinal()]) {
            case 1:
                return RequestMethod.GET;
            case 2:
                return RequestMethod.POST;
            case 3:
            case 4:
                return RequestMethod.PUT;
            default:
                throw new IllegalSelectorException();
        }
    }

    public String c() {
        return "/service/device";
    }

    public J d() {
        return this.b;
    }
}
