package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Ranking;

/* renamed from: com.scoreloop.client.android.core.controller.k  reason: case insensitive filesystem */
class C0011k implements RequestControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScoresController f77a;

    private C0011k(ScoresController scoresController) {
        this.f77a = scoresController;
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f77a.f62a.requestControllerDidFail(this.f77a, exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        Ranking ranking = ((RankingController) requestController).getRanking();
        if (this.f77a.i != null && this.f77a.i.getRank() == null) {
            this.f77a.i.a(ranking.getRank());
        }
        this.f77a.a(ranking.getRank());
    }
}
