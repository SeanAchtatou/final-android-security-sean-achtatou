package com.scoreloop.client.android.core.ui;

import android.app.Activity;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.utils.HTTPUtils;
import com.scoreloop.client.android.core.utils.JSONUtils;
import com.scoreloop.client.android.core.utils.Logger;
import com.scoreloop.client.android.core.utils.OAuthBuilder;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class MySpaceAuthViewController extends AuthViewController {

    /* renamed from: a  reason: collision with root package name */
    private Activity f127a;
    private String b;
    private String c;
    private String d;
    /* access modifiers changed from: private */
    public MyspaceCredentialsDialog e;

    public MySpaceAuthViewController(SocialProviderControllerObserver socialProviderControllerObserver) {
        super(socialProviderControllerObserver);
    }

    private void a(Exception exc) {
        this.e.dismiss();
        a().didFail(exc);
    }

    private boolean a(JSONObject jSONObject) {
        return JSONUtils.a(jSONObject, "token") && JSONUtils.a(jSONObject, "tokenSecret") && JSONUtils.a(jSONObject, "userId");
    }

    private Activity b() {
        return this.f127a;
    }

    /* access modifiers changed from: private */
    public HttpPut b(String str) {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        oAuthBuilder.a(new URL(str));
        oAuthBuilder.a("73c6e22c33e14b56a329e7a19b40914c");
        return oAuthBuilder.b("814d340412384e27b0d480add625fb4391d5cefac08e431f9797c3c9fa94caaf", null);
    }

    private HttpPut b(String str, String str2, HttpPut httpPut) {
        httpPut.setHeader("Content-Type", "application/json");
        httpPut.setEntity(new StringEntity(a(str, str2).toString()));
        return httpPut;
    }

    private boolean b(JSONObject jSONObject) {
        if (!JSONUtils.a(jSONObject, "captcha")) {
            return false;
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject("captcha");
        return JSONUtils.a(jSONObject2, "captchaImageUri") && JSONUtils.a(jSONObject2, "captchaGuid");
    }

    private void c() {
        this.e.dismiss();
        a().didEnterInvalidCredentials();
    }

    private void c(JSONObject jSONObject) {
        Logger.a("myspace response parser", "MYSPACE: valid user credentials acquired");
        String string = jSONObject.getString("token");
        String string2 = jSONObject.getString("tokenSecret");
        String string3 = jSONObject.getString("userId");
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("uid", string3);
        jSONObject2.put("token", string);
        jSONObject2.put("token_secret", string2);
        JSONObject jSONObject3 = new JSONObject();
        try {
            jSONObject3.put("myspace", jSONObject2);
            SocialProvider.b(Session.getCurrentSession().getUser(), jSONObject3);
            this.e.dismiss();
            a().didSucceed();
        } catch (JSONException e2) {
            e2.printStackTrace();
            throw new IllegalStateException(e2);
        }
    }

    private void d(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject.getJSONObject("captcha");
        Logger.a("myspace response parser", "MYSPACE: captcha request aquired");
        String string = jSONObject2.getString("captchaImageUri");
        this.b = jSONObject2.getString("captchaGuid");
        MyspaceCaptchaDialog myspaceCaptchaDialog = new MyspaceCaptchaDialog(b(), new e(this), string);
        this.e.dismiss();
        myspaceCaptchaDialog.show();
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("username", str);
        jSONObject.put("password", str2);
        return jSONObject;
    }

    public void a(Activity activity) {
        this.f127a = activity;
        this.e = new MyspaceCredentialsDialog(b(), new d(this));
        this.e.show();
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        try {
            a(this.c, this.d, b("https://roa.myspace.com/roa/09/messaging/login/" + this.b + "/" + str));
        } catch (MalformedURLException e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, HttpPut httpPut) {
        try {
            this.c = str;
            this.d = str2;
            JSONObject jSONObject = new JSONObject(HTTPUtils.a(new DefaultHttpClient().execute(b(str, str2, httpPut))));
            Logger.a("myspace response parsed json:", jSONObject.toString(4));
            if (a(jSONObject)) {
                c(jSONObject);
            } else if (b(jSONObject)) {
                d(jSONObject);
            } else {
                c();
            }
        } catch (UnsupportedEncodingException e2) {
            a(e2);
        } catch (URISyntaxException e3) {
            a(e3);
        } catch (JSONException e4) {
            a(e4);
        } catch (ClientProtocolException e5) {
            a((Exception) e5);
        } catch (IOException e6) {
            a(e6);
        }
    }
}
