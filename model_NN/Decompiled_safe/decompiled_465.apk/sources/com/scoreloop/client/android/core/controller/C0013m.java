package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.m  reason: case insensitive filesystem */
enum C0013m {
    StartPayment,
    SubmitPayment;

    public static C0013m[] a() {
        return (C0013m[]) c.clone();
    }
}
