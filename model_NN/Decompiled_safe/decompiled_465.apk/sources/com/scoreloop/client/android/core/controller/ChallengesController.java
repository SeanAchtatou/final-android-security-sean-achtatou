package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ChallengesController extends RequestController {
    private final List c;
    private Integer d;
    private Integer e;
    private Integer f;
    private final User g;

    public ChallengesController(RequestControllerObserver requestControllerObserver) {
        this(Session.getCurrentSession(), requestControllerObserver);
    }

    public ChallengesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = new ArrayList();
        this.g = session.getUser();
        this.f = 25;
        this.e = 0;
    }

    private void a(String str) {
        Game b = b();
        User i = i();
        if (b == null) {
            throw new IllegalStateException("Set game first");
        }
        z zVar = new z(d(), b, i, str, this.d, this.f, this.e);
        h();
        a(zVar);
    }

    private User i() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f2 = response.f();
        if (f2 != 200) {
            throw new Exception("Request failed with status:" + f2);
        }
        JSONArray jSONArray = ((JSONObject) response.a()).getJSONArray("challenges");
        this.c.clear();
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            this.c.add(new Challenge(jSONArray.getJSONObject(i).getJSONObject("challenge")));
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public Challenge getChallengeForIndex(int i) {
        if (getChallenges() == null) {
            return null;
        }
        return (Challenge) getChallenges().get(i);
    }

    public List getChallenges() {
        return this.c;
    }

    public Integer getMode() {
        return this.d;
    }

    public void requestChallengeHistory() {
        a("#history");
    }

    public void requestOpenChallenges() {
        a("#open");
    }

    public void setMode(Integer num) {
        this.d = num;
    }
}
