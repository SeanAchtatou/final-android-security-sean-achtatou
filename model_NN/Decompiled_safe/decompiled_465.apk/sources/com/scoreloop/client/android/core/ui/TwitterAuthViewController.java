package com.scoreloop.client.android.core.ui;

import android.app.Activity;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.utils.OAuthBuilder;
import java.net.URL;
import java.util.HashMap;

public class TwitterAuthViewController extends AuthViewController {

    /* renamed from: a  reason: collision with root package name */
    TwitterAuthDialog f132a;
    private String b;

    public TwitterAuthViewController(SocialProviderControllerObserver socialProviderControllerObserver) {
        super(socialProviderControllerObserver);
    }

    public void a(Activity activity) {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        HashMap hashMap = new HashMap();
        hashMap.put("oauth_token", this.b);
        URL a2 = oAuthBuilder.a("http://twitter.com/oauth/authorize", hashMap);
        this.f132a = new TwitterAuthDialog(activity, 16973841, this);
        this.f132a.b();
        this.f132a.setCancelable(true);
        this.f132a.setCanceledOnTouchOutside(true);
        this.f132a.setOnDismissListener(new i(this));
        this.f132a.setOnCancelListener(new j(this));
        this.f132a.a(a2.toString());
        this.f132a.show();
    }

    public void a(String str) {
        this.b = str;
    }

    public String b() {
        return this.b;
    }
}
