package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.i  reason: case insensitive filesystem */
class C0009i {

    /* renamed from: a  reason: collision with root package name */
    private final String f75a;
    private final S b;

    public C0009i(String str, S s) {
        this.f75a = str;
        this.b = s;
    }

    public String a() {
        return this.f75a;
    }

    public String b() {
        return this.b.toString();
    }
}
