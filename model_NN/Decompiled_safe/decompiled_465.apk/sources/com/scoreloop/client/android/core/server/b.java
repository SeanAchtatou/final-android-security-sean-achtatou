package com.scoreloop.client.android.core.server;

import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.methods.HttpPost;

class b extends g {

    /* renamed from: a  reason: collision with root package name */
    private final Cipher f117a;
    private final byte[] b;

    b(URI uri, byte[] bArr) {
        super(uri);
        try {
            this.f117a = Cipher.getInstance("AES/CBC/PKCS7Padding");
            this.b = bArr;
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException();
        } catch (NoSuchPaddingException e2) {
            throw new IllegalStateException();
        }
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "x-application/sjson";
    }

    /* access modifiers changed from: package-private */
    public String a(HttpPost httpPost, String str) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.b, "AES");
        try {
            this.f117a.init(1, secretKeySpec, new IvParameterSpec(this.b));
            byte[] a2 = super.a(httpPost, this.f117a.doFinal(str.getBytes("UTF8")));
            try {
                this.f117a.init(2, secretKeySpec, new IvParameterSpec(this.b));
                return new String(this.f117a.doFinal(a2), "UTF8");
            } catch (GeneralSecurityException e) {
                throw new IllegalStateException(e);
            }
        } catch (GeneralSecurityException e2) {
            throw new IllegalStateException(e2);
        }
    }
}
