package com.scoreloop.android.coreui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import com.feasy.jewels.JungleQuest.R;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.controller.UserControllerObserver;
import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.Iterator;
import java.util.List;

public class HighscoresActivity extends BaseActivity {
    private static final int COLOR_HIGHLIGHT = -13131824;
    private static final int COLOR_NORMAL = -1;
    private static final int DEFAULT_SEARCH_LISTS_SELECTION = 0;
    private static final int FIXED_OFFSET = 3;
    private static final int RANGE_LENGTH = 20;
    private static final int RANGE_POSITION = 0;
    /* access modifiers changed from: private */
    public ListView highScoresListView;
    /* access modifiers changed from: private */
    public boolean initialLoadDone;
    /* access modifiers changed from: private */
    public LoadingType loadingType;
    /* access modifiers changed from: private */
    public LinearLayout myScoreView;
    /* access modifiers changed from: private */
    public int rankCorrection;
    /* access modifiers changed from: private */
    public ScoresController scoresController;
    private boolean shouldShowDialogs;
    /* access modifiers changed from: private */
    public TextView titleLoginView;

    private class ScoresControllerObserver implements RequestControllerObserver {
        private ScoresControllerObserver() {
        }

        /* synthetic */ ScoresControllerObserver(HighscoresActivity highscoresActivity, ScoresControllerObserver scoresControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            HighscoresActivity.this.initialLoadDone = true;
            HighscoresActivity.this.showDialog(HighscoresActivity.FIXED_OFFSET);
            HighscoresActivity.this.setProgressIndicator(false);
            HighscoresActivity.this.blockUI(false);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            List<Score> scores = HighscoresActivity.this.scoresController.getScores();
            HighscoresActivity.this.highScoresListView.setAdapter((ListAdapter) new ScoreViewAdapter(HighscoresActivity.this, R.layout.sl_highscores, scores));
            if (HighscoresActivity.this.scoresController.hasPreviousRange()) {
                scores.add(HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION, null);
                scores.add(HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION, null);
                HighscoresActivity.this.rankCorrection = -2;
            } else {
                HighscoresActivity.this.rankCorrection = HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION;
            }
            if (HighscoresActivity.this.scoresController.hasNextRange()) {
                scores.add(null);
            }
            Score myScore = null;
            User currentUser = Session.getCurrentSession().getUser();
            int idx = HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION;
            Iterator it = scores.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Score score = (Score) it.next();
                if (score != null && score.getUser().equals(currentUser)) {
                    myScore = score;
                    break;
                }
                idx++;
            }
            if (HighscoresActivity.this.loadingType == LoadingType.ME) {
                if (myScore != null) {
                    HighscoresActivity.this.highScoresListView.setSelection(HighscoresActivity.this.calculateUserScorePosition(idx));
                } else {
                    HighscoresActivity.this.showDialog(1);
                }
            }
            if (myScore != null) {
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.rank)).setText(new StringBuilder().append(((ScoresController) requestController).getLoadedRange().getLocation() + idx + 1 + HighscoresActivity.this.rankCorrection).toString());
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.login)).setText(myScore.getUser().getLogin());
                ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.score)).setText(new StringBuilder().append(myScore.getResult().intValue()).toString());
                HighscoresActivity.this.myScoreView.setVisibility(8);
            } else if (((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.rank)).getText().toString().equals("")) {
                HighscoresActivity.this.myScoreView.setVisibility(8);
            } else {
                HighscoresActivity.this.myScoreView.setVisibility(HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION);
            }
            HighscoresActivity.this.initialLoadDone = true;
            HighscoresActivity.this.setProgressIndicator(false);
            HighscoresActivity.this.blockUI(false);
        }
    }

    private class ScoreViewAdapter extends ArrayAdapter<Score> {
        public ScoreViewAdapter(Context context, int resource, List<Score> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = HighscoresActivity.this.getLayoutInflater().inflate((int) R.layout.sl_highscores_list_item, (ViewGroup) null);
            }
            Score score = (Score) getItem(position);
            int rangeStart = HighscoresActivity.this.scoresController.getLoadedRange().getLocation();
            TextView scoreRank = (TextView) view.findViewById(R.id.rank);
            TextView playerName = (TextView) view.findViewById(R.id.login);
            TextView scoreInfo = (TextView) view.findViewById(R.id.score);
            if (score == null) {
                switch (position) {
                    case HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION /*0*/:
                        scoreRank.setText("", (TextView.BufferType) null);
                        playerName.setText((int) R.string.sl_top, (TextView.BufferType) null);
                        scoreInfo.setText("", (TextView.BufferType) null);
                        break;
                    case 1:
                        scoreRank.setText("", (TextView.BufferType) null);
                        playerName.setText((int) R.string.sl_prev, (TextView.BufferType) null);
                        scoreInfo.setText("", (TextView.BufferType) null);
                        break;
                    default:
                        scoreRank.setText("", (TextView.BufferType) null);
                        playerName.setText((int) R.string.sl_next, (TextView.BufferType) null);
                        scoreInfo.setText("", (TextView.BufferType) null);
                        break;
                }
            } else {
                scoreRank.setText(new StringBuilder().append(rangeStart + position + 1 + HighscoresActivity.this.rankCorrection).toString(), (TextView.BufferType) null);
                playerName.setText(score.getUser().getLogin(), (TextView.BufferType) null);
                scoreInfo.setText(new StringBuilder().append(score.getResult().intValue()).toString(), (TextView.BufferType) null);
            }
            if (score != null) {
                HighscoresActivity.this.highlightView(view, score.getUser().equals(Session.getCurrentSession().getUser()));
            } else {
                HighscoresActivity.this.highlightView(view, false);
            }
            return view;
        }
    }

    private class UserUpdateObserver implements UserControllerObserver {
        private UserUpdateObserver() {
        }

        /* synthetic */ UserUpdateObserver(HighscoresActivity highscoresActivity, UserUpdateObserver userUpdateObserver) {
            this();
        }

        public void onEmailAlreadyTaken(UserController controller) {
            HighscoresActivity.this.onUserErrorUpdateUI(8);
        }

        public void onEmailInvalidFormat(UserController controller) {
            HighscoresActivity.this.onUserErrorUpdateUI(10);
        }

        public void onUsernameAlreadyTaken(UserController controller) {
            HighscoresActivity.this.onUserErrorUpdateUI(11);
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            HighscoresActivity.this.onUserErrorUpdateUI(HighscoresActivity.FIXED_OFFSET);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            HighscoresActivity.this.titleLoginView.setText(Session.getCurrentSession().getUser().getLogin());
            HighscoresActivity.this.setProgressIndicator(false);
            HighscoresActivity.this.blockUI(false);
            HighscoresActivity.this.onSearchListsAvailable();
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class LoadingType extends Enum<LoadingType> {
        private static final /* synthetic */ LoadingType[] ENUM$VALUES;
        public static final LoadingType ME = new LoadingType("ME", HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION);
        public static final LoadingType OTHER = new LoadingType("OTHER", 1);

        public static LoadingType valueOf(String str) {
            return (LoadingType) Enum.valueOf(LoadingType.class, str);
        }

        public static LoadingType[] values() {
            LoadingType[] loadingTypeArr = ENUM$VALUES;
            int length = loadingTypeArr.length;
            LoadingType[] loadingTypeArr2 = new LoadingType[length];
            System.arraycopy(loadingTypeArr, HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION, loadingTypeArr2, HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION, length);
            return loadingTypeArr2;
        }

        private LoadingType(String str, int i) {
        }

        static {
            LoadingType[] loadingTypeArr = new LoadingType[2];
            loadingTypeArr[HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION] = ME;
            loadingTypeArr[1] = OTHER;
            ENUM$VALUES = loadingTypeArr;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add((int) DEFAULT_SEARCH_LISTS_SELECTION, (int) DEFAULT_SEARCH_LISTS_SELECTION, (int) DEFAULT_SEARCH_LISTS_SELECTION, (int) R.string.sl_profile).setIcon((int) R.drawable.sl_menu_profile);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: private */
    public void blockUI(boolean flg) {
        ((Spinner) findViewById(R.id.game_mode_spinner)).setEnabled(flg ? DEFAULT_SEARCH_LISTS_SELECTION : true);
        ((ListView) findViewById(R.id.list_view)).setEnabled(flg ? DEFAULT_SEARCH_LISTS_SELECTION : true);
        ((LinearLayout) findViewById(R.id.myscore_view)).setEnabled(flg ? DEFAULT_SEARCH_LISTS_SELECTION : true);
    }

    /* access modifiers changed from: private */
    public int calculateUserScorePosition(int index) {
        if (index < FIXED_OFFSET) {
            return DEFAULT_SEARCH_LISTS_SELECTION;
        }
        return index - FIXED_OFFSET;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    /* access modifiers changed from: private */
    public void highlightView(View view, boolean flg) {
        int c = flg ? COLOR_HIGHLIGHT : COLOR_NORMAL;
        ((TextView) view.findViewById(R.id.rank)).setTextColor(c);
        ((TextView) view.findViewById(R.id.login)).setTextColor(c);
        ((TextView) view.findViewById(R.id.score)).setTextColor(c);
    }

    /* access modifiers changed from: private */
    public void loadRange(boolean isInitialLoad) {
        if (isInitialLoad || this.initialLoadDone) {
            this.loadingType = LoadingType.ME;
            blockUI(true);
            setProgressIndicator(true);
            this.scoresController.loadRangeForUser(Session.getCurrentSession().getUser());
        }
    }

    /* access modifiers changed from: private */
    public void onSearchListsAvailable() {
        List<SearchList> searchLists = Session.getCurrentSession().getScoreSearchLists();
        searchLists.remove(SearchList.buddiesScoreSearchList());
        new ArrayAdapter<>(this, (int) R.layout.sl_spinner_item, searchLists).setDropDownViewResource(17367049);
        this.scoresController.setRange(new Range(DEFAULT_SEARCH_LISTS_SELECTION, RANGE_LENGTH));
        blockUI(true);
        setProgressIndicator(true);
        loadRange(true);
    }

    /* access modifiers changed from: private */
    public void onUserErrorUpdateUI(int error) {
        setProgressIndicator(false);
        if (this.shouldShowDialogs) {
            showDialog(error);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_highscores);
        this.scoresController = new ScoresController(new ScoresControllerObserver(this, null));
        this.titleLoginView = (TextView) findViewById(R.id.title_login);
        if (Session.getCurrentSession().isAuthenticated()) {
            this.titleLoginView.setText(Session.getCurrentSession().getUser().getLogin());
        }
        this.myScoreView = (LinearLayout) findViewById(R.id.myscore_view);
        this.myScoreView.setVisibility(8);
        ((TextView) this.myScoreView.findViewById(R.id.rank)).setText("");
        highlightView(this.myScoreView, true);
        this.myScoreView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighscoresActivity.this.loadRange(false);
            }
        });
        this.highScoresListView = (ListView) findViewById(R.id.list_view);
        this.highScoresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                if (((Score) adapter.getItemAtPosition(position)) == null) {
                    switch (position) {
                        case HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION /*0*/:
                            HighscoresActivity.this.loadingType = LoadingType.OTHER;
                            HighscoresActivity.this.scoresController.setRange(new Range(HighscoresActivity.DEFAULT_SEARCH_LISTS_SELECTION, HighscoresActivity.RANGE_LENGTH));
                            HighscoresActivity.this.blockUI(true);
                            HighscoresActivity.this.setProgressIndicator(true);
                            HighscoresActivity.this.scoresController.loadRange();
                            return;
                        case 1:
                            HighscoresActivity.this.loadingType = LoadingType.OTHER;
                            HighscoresActivity.this.blockUI(true);
                            HighscoresActivity.this.setProgressIndicator(true);
                            HighscoresActivity.this.scoresController.loadPreviousRange();
                            return;
                        default:
                            HighscoresActivity.this.loadingType = LoadingType.OTHER;
                            HighscoresActivity.this.blockUI(true);
                            HighscoresActivity.this.setProgressIndicator(true);
                            HighscoresActivity.this.scoresController.loadNextRange();
                            return;
                    }
                }
            }
        });
        Spinner gameModeSpinner = ScoreloopManager.getGameModeChooser(this);
        if (ScoreloopManager.client.getGameModes().getLength() > 1) {
            gameModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    HighscoresActivity.this.scoresController.setMode(Integer.valueOf(position));
                    ((TextView) HighscoresActivity.this.myScoreView.findViewById(R.id.rank)).setText("");
                    HighscoresActivity.this.loadRange(false);
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        } else {
            gameModeSpinner.setVisibility(8);
        }
        if (!Session.getCurrentSession().isAuthenticated()) {
            blockUI(true);
            setProgressIndicator(true);
            new UserController(new UserUpdateObserver(this, null)).updateUser();
            return;
        }
        onSearchListsAvailable();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.shouldShowDialogs = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.shouldShowDialogs = true;
    }
}
