package com.feasy.jewels.JungleQuest;

public final class R {

    public static final class array {
        public static final int sl_game_modes = 2131034112;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int sl_selector_color = 2131230720;
    }

    public static final class drawable {
        public static final int app0 = 2130837504;
        public static final int app1 = 2130837505;
        public static final int app2 = 2130837506;
        public static final int app3 = 2130837507;
        public static final int app4 = 2130837508;
        public static final int app5 = 2130837509;
        public static final int bg = 2130837510;
        public static final int bonus = 2130837511;
        public static final int bonusbar = 2130837512;
        public static final int bonusbar_fill = 2130837513;
        public static final int btn_back = 2130837514;
        public static final int btn_exit = 2130837515;
        public static final int btn_highscore = 2130837516;
        public static final int btn_menu1 = 2130837517;
        public static final int btn_menu2 = 2130837518;
        public static final int btn_menu3 = 2130837519;
        public static final int btn_moregame = 2130837520;
        public static final int btn_newgame = 2130837521;
        public static final int btn_option = 2130837522;
        public static final int btn_resume = 2130837523;
        public static final int cursor1 = 2130837524;
        public static final int cursor2 = 2130837525;
        public static final int icon = 2130837526;
        public static final int img0 = 2130837527;
        public static final int img1 = 2130837528;
        public static final int img2 = 2130837529;
        public static final int img3 = 2130837530;
        public static final int menu_img = 2130837531;
        public static final int n1 = 2130837532;
        public static final int n2 = 2130837533;
        public static final int n3 = 2130837534;
        public static final int n4 = 2130837535;
        public static final int n5 = 2130837536;
        public static final int n6 = 2130837537;
        public static final int n7 = 2130837538;
        public static final int n8 = 2130837539;
        public static final int n9 = 2130837540;
        public static final int num01 = 2130837541;
        public static final int num02 = 2130837542;
        public static final int pane = 2130837543;
        public static final int score = 2130837544;
        public static final int score_title = 2130837545;
        public static final int sl_bg_btn = 2130837546;
        public static final int sl_bg_btn_pre = 2130837547;
        public static final int sl_bg_dropdown = 2130837548;
        public static final int sl_bg_dropdown_pre = 2130837549;
        public static final int sl_bg_h1 = 2130837550;
        public static final int sl_bg_list = 2130837551;
        public static final int sl_bg_list_pre = 2130837552;
        public static final int sl_divider = 2130837553;
        public static final int sl_divider_list = 2130837554;
        public static final int sl_logo = 2130837555;
        public static final int sl_menu_highscores = 2130837556;
        public static final int sl_menu_profile = 2130837557;
        public static final int sl_selector_btn = 2130837558;
        public static final int sl_selector_dropdown = 2130837559;
        public static final int sl_selector_list = 2130837560;
        public static final int star0 = 2130837561;
        public static final int star1 = 2130837562;
        public static final int star2 = 2130837563;
        public static final int star3 = 2130837564;
        public static final int star4 = 2130837565;
        public static final int star5 = 2130837566;
        public static final int star6 = 2130837567;
        public static final int star7 = 2130837568;
        public static final int title = 2130837569;
    }

    public static final class id {
        public static final int TextView02 = 2131296288;
        public static final int ad = 2131296257;
        public static final int ad2 = 2131296269;
        public static final int btn_back = 2131296258;
        public static final int btn_cancel = 2131296273;
        public static final int btn_exit = 2131296268;
        public static final int btn_hiscore = 2131296266;
        public static final int btn_newgame = 2131296263;
        public static final int btn_option = 2131296265;
        public static final int btn_resume = 2131296264;
        public static final int btn_save = 2131296274;
        public static final int btn_sl_profile = 2131296267;
        public static final int cb_music = 2131296270;
        public static final int cb_sound = 2131296271;
        public static final int cb_vibrate = 2131296272;
        public static final int email = 2131296287;
        public static final int game_mode_spinner = 2131296280;
        public static final int game_view = 2131296261;
        public static final int highscores_list_item = 2131296283;
        public static final int img_icon = 2131296276;
        public static final int img_order = 2131296259;
        public static final int list_view = 2131296281;
        public static final int login = 2131296285;
        public static final int lv = 2131296275;
        public static final int lv_score = 2131296256;
        public static final int myscore_view = 2131296282;
        public static final int progress_indicator = 2131296279;
        public static final int rank = 2131296284;
        public static final int score = 2131296286;
        public static final int scorebar = 2131296262;
        public static final int spinnerTarget = 2131296290;
        public static final int title_login = 2131296278;
        public static final int tv_name = 2131296277;
        public static final int txt_score = 2131296260;
        public static final int update_button = 2131296289;
    }

    public static final class layout {
        public static final int lv_score = 2130903040;
        public static final int lv_score_raw = 2130903041;
        public static final int main = 2130903042;
        public static final int menu = 2130903043;
        public static final int option = 2130903044;
        public static final int popup_lv = 2130903045;
        public static final int popup_lv_item = 2130903046;
        public static final int sl_highscores = 2130903047;
        public static final int sl_highscores_list_item = 2130903048;
        public static final int sl_profile = 2130903049;
        public static final int sl_spinner_item = 2130903050;
    }

    public static final class raw {
        public static final int bg_01 = 2130968576;
        public static final int bg_02 = 2130968577;
        public static final int bg_03 = 2130968578;
        public static final int click = 2130968579;
        public static final int drop1 = 2130968580;
        public static final int drop2 = 2130968581;
        public static final int menu_music = 2130968582;
        public static final int move = 2130968583;
        public static final int move_ok = 2130968584;
        public static final int move_ok1 = 2130968585;
        public static final int nextlevel = 2130968586;
        public static final int wrong2 = 2130968587;
    }

    public static final class string {
        public static final int about = 2131099664;
        public static final int app_name = 2131099661;
        public static final int help = 2131099663;
        public static final int sl_email = 2131099652;
        public static final int sl_error_message_email_already_taken = 2131099659;
        public static final int sl_error_message_invalid_email_format = 2131099660;
        public static final int sl_error_message_name_already_taken = 2131099658;
        public static final int sl_error_message_network = 2131099657;
        public static final int sl_error_message_not_on_highscore_list = 2131099656;
        public static final int sl_highscores = 2131099649;
        public static final int sl_login = 2131099651;
        public static final int sl_next = 2131099654;
        public static final int sl_prev = 2131099653;
        public static final int sl_profile = 2131099648;
        public static final int sl_top = 2131099655;
        public static final int sl_update_profile = 2131099650;
        public static final int start = 2131099662;
    }

    public static final class style {
        public static final int sl_heading = 2131165184;
        public static final int sl_normal = 2131165186;
        public static final int sl_title_bar = 2131165185;
    }
}
