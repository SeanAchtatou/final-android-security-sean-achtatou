package com.feasy.jewels.JungleQuest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.lang.reflect.Array;
import java.util.Random;

public class GameView extends View implements Runnable {
    public static int SCREEN_H = 455;
    public static int SCREEN_W = 320;
    static final int ST_PLAYING = 2;
    static int gameState = ST_PLAYING;
    final int BODY_H = 8;
    final int BODY_W = 8;
    int beginDrawX;
    int beginDrawY;
    private Bitmap bg;
    private Bitmap[] block = new Bitmap[8];
    final int blockCount = 8;
    private Bitmap bmTop;
    public int[][] body = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    Bomb bomb;
    final int caseWidth = 40;
    int[][] clear = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    int clearFrame;
    final int clearFrameMax = 8;
    final int clearW = 30;
    Context context;
    int curScore;
    int currentX = -1;
    int currentY = -1;
    private Bitmap cursor1;
    private Bitmap cursor2;
    final int cursorW = 36;
    private int delay = 20;
    private int drawDelay = 150;
    private boolean focus_isShow = true;
    private int focus_show_cnt = 0;
    int gameScore;
    int iDisScore;
    private Bitmap imgBonus;
    private Bitmap imgBonusbar;
    private Bitmap imgBonusbar_fill;
    private Bitmap imgNum01;
    private Bitmap imgPane;
    private Bitmap imgScore;
    boolean isClear;
    boolean isDown = true;
    boolean isExchange;
    boolean isReExchange;
    private boolean isResumeLoad;
    public boolean isRunning = true;
    boolean isSelected;
    private int[] mBkgFiles = {R.drawable.img0, R.drawable.img1, R.drawable.img2, R.drawable.img3};
    int mBonus = 1;
    private Typeface mFontFace;
    private boolean mIsShowScoreAdd = false;
    private long mLastDrawTime = (System.currentTimeMillis() - ((long) this.drawDelay));
    int mMatchCnt;
    private GameActivity mParent;
    private long mShowScoreBeginTime = 0;
    int moveFrame;
    final int moveFrameMax = 8;
    final int moveSpeed = 5;
    int nowAddScore;
    Paint paint = new Paint();
    Random random = new Random();
    ScoreBubble scoreBubble;
    int scoreSpace;
    int selectedX;
    int selectedY;
    int[][] tempMove = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));

    public GameView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        setFocusable(true);
    }

    public void setParent(GameActivity a2) {
        this.mParent = a2;
    }

    public void setScreen(int w, int h) {
        SCREEN_W = w;
        SCREEN_H = h;
        this.beginDrawX = (SCREEN_W - 320) >> 1;
        this.beginDrawY = (SCREEN_H - 320) >> 1;
        this.bomb = new Bomb(this.block, SCREEN_W, SCREEN_H);
        this.scoreBubble = new ScoreBubble(SCREEN_W, SCREEN_H);
    }

    public void init(boolean isNewGame) {
        this.isResumeLoad = true;
        if (!isNewGame) {
            this.isResumeLoad = false;
            this.mParent.loadResumeData();
        }
        loadFP();
        this.paint.setColor(-1);
        this.paint.setFlags(1);
        this.paint.setFakeBoldText(true);
        this.mMatchCnt = 0;
    }

    public Bitmap createImage(Drawable tile, int w, int h) {
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, w, h);
        tile.draw(canvas);
        return bitmap;
    }

    public void loadFP() {
        Resources r = getResources();
        changeBkg();
        this.bg = BitmapFactory.decodeResource(r, R.drawable.bg);
        this.cursor1 = BitmapFactory.decodeResource(r, R.drawable.cursor1);
        this.cursor2 = BitmapFactory.decodeResource(r, R.drawable.cursor2);
        this.imgNum01 = BitmapFactory.decodeResource(r, R.drawable.num01);
        this.imgScore = BitmapFactory.decodeResource(r, R.drawable.score);
        this.imgBonus = BitmapFactory.decodeResource(r, R.drawable.bonus);
        this.imgBonusbar_fill = BitmapFactory.decodeResource(r, R.drawable.bonusbar_fill);
        this.imgBonusbar = BitmapFactory.decodeResource(r, R.drawable.bonusbar);
        this.imgPane = BitmapFactory.decodeResource(r, R.drawable.pane);
        this.scoreSpace = this.imgScore.getWidth();
        this.block[0] = BitmapFactory.decodeResource(r, R.drawable.star0);
        this.block[1] = BitmapFactory.decodeResource(r, R.drawable.star1);
        this.block[ST_PLAYING] = BitmapFactory.decodeResource(r, R.drawable.star2);
        this.block[3] = BitmapFactory.decodeResource(r, R.drawable.star3);
        this.block[4] = BitmapFactory.decodeResource(r, R.drawable.star4);
        this.block[5] = BitmapFactory.decodeResource(r, R.drawable.star5);
        this.block[6] = BitmapFactory.decodeResource(r, R.drawable.star6);
        this.block[7] = BitmapFactory.decodeResource(r, R.drawable.star7);
    }

    private void changeBkg() {
        if (this.bmTop != null && !this.bmTop.isRecycled()) {
            this.bmTop.recycle();
        }
        int idx = this.mBonus % this.mBkgFiles.length;
        Log.v("GameView", "changeBkg(): idx=" + idx);
        this.bmTop = BitmapFactory.decodeResource(getResources(), this.mBkgFiles[idx]);
    }

    public void toState(int state) {
        gameState = state;
    }

    public void logic() {
        this.bomb.move();
        if (this.iDisScore < this.gameScore) {
            this.iDisScore += ((this.gameScore - this.iDisScore) / ST_PLAYING) + 1;
        }
        if (this.isClear) {
            this.clearFrame++;
            if (this.clearFrame >= 5) {
                this.clearFrame = 0;
                this.isClear = false;
                this.isDown = true;
            }
        }
        if (this.isDown || this.isExchange || this.isReExchange) {
            this.moveFrame++;
        }
        if (this.isDown && this.moveFrame >= 4) {
            this.moveFrame = 0;
            this.isDown = doDown();
            if (!this.isDown) {
                this.isClear = checkClear();
            }
        }
        if (this.isExchange && this.moveFrame >= 7) {
            this.moveFrame = 0;
            this.isExchange = false;
            Log.v("GameView", "logic(): new tempMove()");
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
            this.isClear = checkClear();
            if (!this.isClear) {
                this.isReExchange = true;
                doExchange();
            }
        }
        if (this.isReExchange && this.moveFrame >= 7) {
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
            this.moveFrame = 0;
            this.isReExchange = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (gameState) {
            case ST_PLAYING /*2*/:
                paintPlaying(canvas);
                drawProcessBar(canvas);
                return;
            default:
                return;
        }
    }

    private void paintSelectFocus(Canvas canvas) {
        if (!this.isSelected) {
            return;
        }
        if (this.focus_isShow) {
            int i = this.focus_show_cnt;
            this.focus_show_cnt = i + 1;
            if (i < ST_PLAYING) {
                canvas.drawBitmap(this.isSelected ? this.cursor1 : this.cursor2, (float) (this.beginDrawX + (this.currentX * 40)), (float) (this.beginDrawY + (this.currentY * 40)), this.paint);
            } else {
                this.focus_isShow = false;
            }
        } else {
            int i2 = this.focus_show_cnt;
            this.focus_show_cnt = i2 - 1;
            if (i2 <= 1) {
                this.focus_show_cnt = 0;
                this.focus_isShow = true;
            }
        }
    }

    private void paintScoreAndBonus(Canvas canvas) {
        canvas.drawBitmap(this.imgPane, 0.0f, 0.0f, this.paint);
        this.paint.setColor(-1);
        this.paint.setTextSize(16.0f);
        canvas.drawText(Integer.toString(this.gameScore), (float) (this.scoreSpace + 10), 22.0f, this.paint);
        canvas.drawText(String.valueOf(this.mBonus) + "x", 270.0f, 22.0f, this.paint);
    }

    private void paintPlaying(Canvas canvas) {
        canvas.clipRect(0, 0, SCREEN_W, SCREEN_H);
        canvas.drawBitmap(this.bmTop, 0.0f, 0.0f, this.paint);
        canvas.drawBitmap(this.bg, (float) this.beginDrawX, (float) this.beginDrawY, this.paint);
        paintSelectFocus(canvas);
        for (int i = 0; i < this.tempMove.length; i++) {
            for (int j = 0; j < this.tempMove[i].length; j++) {
                switch (this.tempMove[i][j]) {
                    case 1:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case ST_PLAYING /*2*/:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 3:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 4:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), j * 40);
                        break;
                    case 5:
                    default:
                        if (this.clear[i][j] != 0) {
                            break;
                        } else {
                            paintBlock(canvas, this.body[i][j], i * 40, j * 40);
                            break;
                        }
                    case 6:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), j * 40);
                        break;
                    case 7:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 8:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 9:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                }
            }
        }
        paintScoreAndBonus(canvas);
        this.bomb.paint(canvas, this.paint);
        this.scoreBubble.show(canvas, this.paint);
    }

    public int getRandomBlockId() {
        return Math.abs(this.random.nextInt() % 8) + 1;
    }

    private boolean doDown() {
        boolean isFull = false;
        for (int i = 0; i < this.body.length; i++) {
            int j = this.body[i].length - 1;
            while (true) {
                if (j < 0) {
                    break;
                }
                this.tempMove[i][j] = 0;
                if (this.body[i][j] == 0) {
                    isFull = true;
                    for (int k = j; k >= 0; k--) {
                        this.tempMove[i][k] = ST_PLAYING;
                        if (k != 0) {
                            this.body[i][k] = this.body[i][k - 1];
                        } else if (this.isResumeLoad) {
                            this.body[i][k] = getRandomBlockId();
                        }
                    }
                } else {
                    j--;
                }
            }
        }
        if (!this.isResumeLoad && !isFull) {
            this.isResumeLoad = true;
        }
        return isFull;
    }

    private void doExchange() {
        if (this.currentX - this.selectedX == -1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 9;
                this.tempMove[this.selectedX][this.selectedY] = 1;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 6;
                this.tempMove[this.selectedX][this.selectedY] = 4;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 3;
                this.tempMove[this.selectedX][this.selectedY] = 7;
            }
        } else if (this.currentX - this.selectedX == 0) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 8;
                this.tempMove[this.selectedX][this.selectedY] = ST_PLAYING;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = ST_PLAYING;
                this.tempMove[this.selectedX][this.selectedY] = 8;
            }
        } else if (this.currentX - this.selectedX == 1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 7;
                this.tempMove[this.selectedX][this.selectedY] = 3;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 4;
                this.tempMove[this.selectedX][this.selectedY] = 6;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 1;
                this.tempMove[this.selectedX][this.selectedY] = 9;
            }
        }
        int temp = this.body[this.selectedX][this.selectedY];
        this.body[this.selectedX][this.selectedY] = this.body[this.currentX][this.currentY];
        this.body[this.currentX][this.currentY] = temp;
    }

    private boolean checkClear() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                if (j > 0 && j < this.body[i].length - 1 && this.body[i][j] == this.body[i][j - 1] && this.body[i][j] == this.body[i][j + 1]) {
                    int[] iArr = this.clear[i];
                    this.clear[i][j + 1] = 1;
                    this.clear[i][j - 1] = 1;
                    iArr[j] = 1;
                }
                if (i > 0 && i < this.body.length - 1 && this.body[i][j] == this.body[i - 1][j] && this.body[i][j] == this.body[i + 1][j]) {
                    int[] iArr2 = this.clear[i];
                    int[] iArr3 = this.clear[i - 1];
                    this.clear[i + 1][j] = 1;
                    iArr3[j] = 1;
                    iArr2[j] = 1;
                }
            }
        }
        boolean clearBlock = false;
        int cnt = 0;
        int left = 0;
        int top = 0;
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j2 = 0; j2 < 8; j2++) {
                if (this.clear[i2][j2] == 1) {
                    clearBlock = true;
                    left = this.beginDrawX + (i2 * 40);
                    top = this.beginDrawY + (j2 * 40);
                    this.bomb.add(left, top, this.body[i2][j2] - 1);
                    int[] iArr4 = this.clear[i2];
                    this.body[i2][j2] = 0;
                    iArr4[j2] = 0;
                    cnt++;
                }
            }
        }
        if (clearBlock) {
            addScore(cnt, left, top);
            this.mParent.playVibrate();
        }
        return clearBlock;
    }

    private void addScore(int cnt, int x, int y) {
        if (cnt >= 3) {
            GameActivity gameActivity = this.mParent;
            this.mParent.mGameSound.getClass();
            gameActivity.playSP(3);
            this.mIsShowScoreAdd = true;
            this.mShowScoreBeginTime = System.currentTimeMillis();
            this.nowAddScore = (cnt - ST_PLAYING) * this.mBonus * 10;
            this.gameScore += this.nowAddScore;
            this.curScore += this.nowAddScore;
            this.mMatchCnt++;
            this.scoreBubble.Add(x, y, this.nowAddScore);
            if (this.curScore >= getCurLevelScore()) {
                this.curScore -= getCurLevelScore();
                this.mMatchCnt = 0;
                updateLevel();
                GameActivity gameActivity2 = this.mParent;
                this.mParent.mGameSound.getClass();
                gameActivity2.playSP(4);
            }
        }
    }

    private void updateLevel() {
        this.mBonus++;
        changeBkg();
        this.mParent.playMusic();
    }

    public int getCurLevelScore() {
        return this.mBonus * 10 * 50;
    }

    private void paintBlock(Canvas canvas, int index, int x, int y) {
        int index2 = index - 1;
        if (index2 >= 0 && index2 < this.block.length) {
            canvas.save();
            canvas.clipRect(this.beginDrawX, this.beginDrawY, this.beginDrawX + 320, this.beginDrawY + 320);
            canvas.drawBitmap(this.block[index2], (float) (this.beginDrawX + x), (float) (this.beginDrawY + y), this.paint);
            canvas.restore();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (!this.isDown && !this.isExchange && !this.isReExchange && this.moveFrame == 0) {
                    if (event.getX() > ((float) this.beginDrawX) && event.getX() < ((float) (this.beginDrawX + 320)) && event.getY() > ((float) this.beginDrawY) && event.getY() < ((float) (this.beginDrawY + 320))) {
                        if (!this.isSelected) {
                            this.isSelected = true;
                            this.focus_isShow = true;
                            this.focus_show_cnt = 0;
                            this.currentX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            this.currentY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            this.selectedX = this.currentX;
                            this.selectedY = this.currentY;
                            break;
                        } else {
                            GameActivity gameActivity = this.mParent;
                            this.mParent.mGameSound.getClass();
                            gameActivity.playSP(ST_PLAYING);
                            int tempX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            int tempY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            if (tempX <= this.selectedX) {
                                if (tempX != this.selectedX) {
                                    if (tempX < this.selectedX) {
                                        if (tempY <= this.selectedY) {
                                            if (tempY != this.selectedY) {
                                                if (tempY < this.selectedY) {
                                                    moveLeftUp();
                                                    break;
                                                }
                                            } else {
                                                moveLeft();
                                                break;
                                            }
                                        } else {
                                            moveLeftDown();
                                            break;
                                        }
                                    }
                                } else if (tempY <= this.selectedY) {
                                    if (tempY != this.selectedY) {
                                        if (tempY < this.selectedY) {
                                            moveUp();
                                            break;
                                        }
                                    } else {
                                        this.isSelected = false;
                                        break;
                                    }
                                } else {
                                    moveDown();
                                    break;
                                }
                            } else if (tempY <= this.selectedY) {
                                if (tempY != this.selectedY) {
                                    if (tempY < this.selectedY) {
                                        moveRightUp();
                                        break;
                                    }
                                } else {
                                    moveRight();
                                    break;
                                }
                            } else {
                                moveRightDown();
                                break;
                            }
                        }
                    }
                } else {
                    return super.onTouchEvent(event);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDown || this.isExchange || this.isReExchange || this.moveFrame != 0) {
            return super.onKeyDown(keyCode, msg);
        }
        if (keyCode == 23) {
            if (this.isDown || this.isExchange || this.isReExchange) {
                return super.onKeyDown(keyCode, msg);
            }
            this.isSelected = !this.isSelected;
            this.selectedX = this.currentX;
            this.selectedY = this.currentY;
        } else if (this.isExchange || this.isReExchange) {
            return super.onKeyDown(keyCode, msg);
        } else {
            if (keyCode == 19) {
                moveUp();
            } else if (keyCode == 20) {
                moveDown();
            } else if (keyCode == 21) {
                moveLeft();
            } else if (keyCode == 22) {
                moveRight();
            }
        }
        return super.onKeyDown(keyCode, msg);
    }

    public void setExchange() {
        if (this.isSelected) {
            this.isExchange = true;
            this.isSelected = false;
            doExchange();
        }
    }

    private void moveRightDown() {
        if (this.currentX == 7 || this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % 8;
        setExchange();
    }

    private void moveLeftDown() {
        if (this.currentX == 0 || this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % 8;
        setExchange();
    }

    private void moveRightUp() {
        if (this.currentY == 0 || this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + 8) % 8;
        setExchange();
    }

    private void moveLeftUp() {
        if (this.currentX == 0 || this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + 8) % 8;
        setExchange();
    }

    private void moveUp() {
        if (this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentY - 1;
        this.currentY = i;
        this.currentY = (i + 8) % 8;
        setExchange();
    }

    private void moveDown() {
        if (this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentY + 1;
        this.currentY = i;
        this.currentY = i % 8;
        setExchange();
    }

    private void moveLeft() {
        if (this.currentX == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        setExchange();
    }

    private void moveRight() {
        if (this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        setExchange();
    }

    public void run() {
        logic();
        if (System.currentTimeMillis() - this.mLastDrawTime >= ((long) this.drawDelay)) {
            invalidate();
            this.mLastDrawTime = System.currentTimeMillis();
        }
        if (this.isRunning) {
            postDelayed(this, (long) this.delay);
        }
    }

    private void drawProcessBar(Canvas c) {
        BitmapDrawable d1 = new BitmapDrawable(this.imgBonusbar);
        d1.setBounds(6, 45, 308, 45 + 12);
        d1.draw(c);
        BitmapDrawable d2 = new BitmapDrawable(this.imgBonusbar_fill);
        d2.setBounds(6 + 1, 45 + ST_PLAYING, (int) ((306.0f * ((float) this.curScore)) / ((float) getCurLevelScore())), 45 + 10);
        d2.draw(c);
    }

    public void restoreData(int level, int score, int curScore2) {
        this.mBonus = level;
        this.gameScore = score;
    }

    public void freeObject() {
        this.bg = null;
        this.bmTop = null;
        this.cursor1 = null;
        this.imgBonusbar_fill = null;
        this.imgBonusbar = null;
        this.imgScore = null;
        this.imgBonus = null;
        this.block = null;
    }
}
