package com.feasy.jewels.JungleQuest;

import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.Vector;

public class ScoreBubble {
    private final int CONST_SHOW_INTERVAL = 120;
    private final int CONST_SHOW_TIME = 1500;
    private int height;
    private Vector<Point> vector = new Vector<>();
    private int width;

    public ScoreBubble(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public void Add(int x, int y, int score) {
        this.vector.addElement(new Point(x, y, score, System.currentTimeMillis()));
    }

    public void show(Canvas c, Paint p) {
        c.save();
        c.clipRect(0, 0, this.width, this.height);
        int fontColor = p.getColor();
        float fontSize = p.getTextSize();
        p.setColor(-16777216);
        p.setTextSize(17.0f);
        for (int i = this.vector.size() - 1; i >= 0; i--) {
            Point point = this.vector.get(i);
            String str = "+" + Integer.toString(point.score);
            if (System.currentTimeMillis() - point.t <= 1500) {
                int h = ((int) (System.currentTimeMillis() - point.t)) / 120;
                int x = point.x + 1;
                int y = (point.y + 10) - (h * 2);
                c.drawText(str, (float) (x - 1), (float) (y - 1), p);
                c.drawText(str, (float) x, (float) (y - 1), p);
                c.drawText(str, (float) (x + 1), (float) y, p);
                c.drawText(str, (float) x, (float) (y + 1), p);
                p.setColor(-1);
                p.setTextSize(16.0f);
                c.drawText(str, (float) x, (float) y, p);
            } else {
                this.vector.remove(i);
            }
        }
        c.restore();
        p.setColor(fontColor);
        p.setTextSize(fontSize);
    }

    class Point {
        int score;
        long t;
        int x;
        int y;

        Point(int x2, int y2, int score2, long t2) {
            this.x = x2;
            this.y = y2;
            this.t = t2;
            this.score = score2;
        }
    }
}
