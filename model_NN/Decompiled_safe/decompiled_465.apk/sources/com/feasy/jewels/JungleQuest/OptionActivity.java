package com.feasy.jewels.JungleQuest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class OptionActivity extends Activity {
    /* access modifiers changed from: private */
    public CheckBox cbMusic;
    /* access modifiers changed from: private */
    public CheckBox cbSound;
    /* access modifiers changed from: private */
    public CheckBox cbVibrate;
    /* access modifiers changed from: private */
    public Intent mIntent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.option);
        setTitle("Game Option");
        this.mIntent = getIntent();
        Bundle b = getIntent().getExtras();
        boolean isMusic = b.getBoolean("isMusic");
        boolean isSound = b.getBoolean("isSound");
        boolean isVibrate = b.getBoolean("isVibrate");
        this.cbMusic = (CheckBox) findViewById(R.id.cb_music);
        this.cbMusic.setChecked(isMusic);
        this.cbSound = (CheckBox) findViewById(R.id.cb_sound);
        this.cbSound.setChecked(isSound);
        this.cbVibrate = (CheckBox) findViewById(R.id.cb_vibrate);
        this.cbVibrate.setChecked(isVibrate);
        ((Button) findViewById(R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OptionActivity.this.mIntent.putExtra("isMusic", OptionActivity.this.cbMusic.isChecked());
                OptionActivity.this.mIntent.putExtra("isSound", OptionActivity.this.cbSound.isChecked());
                OptionActivity.this.mIntent.putExtra("isVibrate", OptionActivity.this.cbVibrate.isChecked());
                Log.v("Option", "Save(), isMusic=" + (OptionActivity.this.cbMusic.isChecked() ? "true" : "false") + ", isSound=" + (OptionActivity.this.cbSound.isChecked() ? "true" : "false"));
                OptionActivity.this.setResult(-1, OptionActivity.this.mIntent);
                OptionActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OptionActivity.this.finish();
            }
        });
    }
}
