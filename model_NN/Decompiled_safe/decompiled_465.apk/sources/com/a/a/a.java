package com.a.a;

import android.graphics.Bitmap;
import android.webkit.CookieManager;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1a = a.class.getSimpleName();
    private g b;
    private e c;
    private String d = null;
    private String e = null;
    private Map f = null;
    private Object g = null;
    private Date h = null;
    private boolean i = false;

    public a(g gVar, e eVar) {
        this.b = gVar;
        this.c = eVar;
    }

    public static a a(g gVar, e eVar) {
        return new a(gVar, eVar);
    }

    private void a(Object obj) {
        if (this.c != null) {
            this.c.a(this, obj);
        }
    }

    private void a(String str, Map map, Object obj) {
        this.d = b(str);
        this.e = str;
        this.f = map != null ? new HashMap(map) : new HashMap();
        this.g = obj;
        this.f.put("method", this.e);
        this.f.put("api_key", this.b.e());
        this.f.put("v", "1.0");
        this.f.put("format", "JSON");
        if (!f()) {
            this.f.put("session_key", this.b.i());
            this.f.put("call_id", h());
            if (this.b.j() != null) {
                this.f.put("ss", "1");
            }
        }
        this.f.put("sig", i());
        this.b.a(this);
    }

    private void a(Throwable th) {
        if (this.c != null) {
            this.c.a(this, th);
        }
    }

    private String b(String str) {
        return this.b.b();
    }

    private void c(String str) {
        try {
            Object a2 = a(str);
            Object d2 = a2 == null ? d(str) : a2;
            if (d2 instanceof JSONObject) {
                JSONObject jSONObject = (JSONObject) d2;
                if (jSONObject.has("error_code")) {
                    int i2 = jSONObject.getInt("error_code");
                    String string = jSONObject.getString("error_msg");
                    JSONArray jSONArray = jSONObject.getJSONArray("request_args");
                    HashMap hashMap = new HashMap();
                    for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
                        hashMap.put(jSONObject2.getString("key"), jSONObject2.getString("value"));
                    }
                    a((Throwable) new m(i2, string, hashMap));
                    return;
                }
            }
            a(d2);
        } catch (JSONException e2) {
            a((Throwable) e2);
        }
    }

    private Object d(String str) {
        return str.startsWith("[") ? new JSONArray(str) : new JSONObject(str);
    }

    private boolean f() {
        return this.e.equals("facebook.auth.getSession") || this.e.equals("facebook.auth.createToken");
    }

    private String g() {
        String str = "&";
        try {
            if (!new URL(this.d).getPath().contains("?")) {
                str = "?";
            }
            ArrayList arrayList = new ArrayList();
            for (Map.Entry entry : this.f.entrySet()) {
                arrayList.add(((String) entry.getKey()) + "=" + ((String) entry.getValue()));
            }
            return this.d + str + a.a.a(arrayList, "&");
        } catch (MalformedURLException e2) {
            Logger.a(f1a, "Invalid URL");
            return null;
        }
    }

    private String h() {
        return String.format(Long.toString(System.currentTimeMillis()), new Object[0]);
    }

    private String i() {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> arrayList = new ArrayList<>(this.f.keySet());
        Collections.sort(arrayList, a.a.f0a);
        for (String str : arrayList) {
            sb.append(str);
            sb.append("=");
            Object obj = this.f.get(str);
            if (obj instanceof String) {
                sb.append(obj);
            }
        }
        if (f()) {
            if (this.b.f() != null) {
                sb.append(this.b.f());
            }
        } else if (this.b.j() != null) {
            sb.append(this.b.j());
        } else if (this.b.f() != null) {
            sb.append(this.b.f());
        }
        return a.a.a(sb.toString());
    }

    private byte[] j() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes("UTF-8"));
        for (Map.Entry entry : this.f.entrySet()) {
            byteArrayOutputStream.write(("Content-Disposition: form-data; name=\"" + ((String) entry.getKey()) + "\"\r\n\r\n").getBytes("UTF-8"));
            byteArrayOutputStream.write(((String) entry.getValue()).getBytes("UTF-8"));
            byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes("UTF-8"));
        }
        if (this.g != null) {
            if (this.g instanceof Bitmap) {
                byteArrayOutputStream.write("Content-Disposition: form-data; filename=\"photo\"\r\n".getBytes("UTF-8"));
                byteArrayOutputStream.write("Content-Type: image/png\r\n\r\n".getBytes("UTF-8"));
                ((Bitmap) this.g).compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
                byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes("UTF-8"));
            } else if (this.g instanceof byte[]) {
                byteArrayOutputStream.write("Content-Disposition: form-data; filename=\"data\"\r\n".getBytes("UTF-8"));
                byteArrayOutputStream.write("Content-Type: content/unknown\r\n\r\n".getBytes("UTF-8"));
                byteArrayOutputStream.write((byte[]) this.g);
                byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes("UTF-8"));
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    /* access modifiers changed from: protected */
    public Object a(String str) {
        return null;
    }

    public Date a() {
        return this.h;
    }

    public void a(String str, Map map) {
        a(str, map, null);
    }

    public void b() {
        HttpURLConnection httpURLConnection;
        OutputStream outputStream;
        InputStream inputStream;
        byte[] bArr;
        OutputStream outputStream2;
        this.i = true;
        try {
            if (this.c != null) {
                this.c.a(this);
            }
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(this.e != null ? this.d : g()).openConnection();
            try {
                httpURLConnection2.setRequestProperty("User-Agent", "FacebookConnect");
                if (this.e != null) {
                    httpURLConnection2.setRequestMethod("POST");
                    httpURLConnection2.setRequestProperty("Content-Type", "multipart/form-data; boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
                    bArr = j();
                } else {
                    bArr = null;
                }
                httpURLConnection2.setDoOutput(true);
                if (c()) {
                    httpURLConnection2.addRequestProperty("Cookie", CookieManager.getInstance().getCookie("facebook.com"));
                }
                httpURLConnection2.connect();
                if (bArr != null) {
                    outputStream = httpURLConnection2.getOutputStream();
                    try {
                        outputStream.write(bArr);
                        outputStream2 = outputStream;
                    } catch (Throwable th) {
                        httpURLConnection = httpURLConnection2;
                        th = th;
                        inputStream = null;
                        a.a.a((Closeable) inputStream);
                        a.a.a(outputStream);
                        a.a.a(httpURLConnection);
                        throw th;
                    }
                } else {
                    outputStream2 = null;
                }
                try {
                    InputStream inputStream2 = httpURLConnection2.getInputStream();
                    try {
                        String sb = a.a.a(inputStream2).toString();
                        a.a.a((Closeable) inputStream2);
                        a.a.a(outputStream2);
                        a.a.a(httpURLConnection2);
                        this.h = new Date();
                        c(sb);
                        this.i = false;
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        httpURLConnection = httpURLConnection2;
                        th = th3;
                        OutputStream outputStream3 = outputStream2;
                        inputStream = inputStream2;
                        outputStream = outputStream3;
                    }
                } catch (Throwable th4) {
                    httpURLConnection = httpURLConnection2;
                    th = th4;
                    outputStream = outputStream2;
                    inputStream = null;
                }
            } catch (Throwable th5) {
                outputStream = null;
                httpURLConnection = httpURLConnection2;
                th = th5;
                inputStream = null;
                a.a.a((Closeable) inputStream);
                a.a.a(outputStream);
                a.a.a(httpURLConnection);
                throw th;
            }
        } catch (Throwable th6) {
            this.i = false;
            throw th6;
        }
    }

    public void b(String str, Map map) {
        this.d = str;
        this.f = map != null ? new HashMap(map) : new HashMap();
        this.b.a(this);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return false;
    }

    public boolean d() {
        return this.i;
    }

    public void e() {
        if (d() && this.c != null) {
            this.c.b(this);
        }
    }

    public String toString() {
        return "<FBRequest " + (this.e != null ? this.e : this.d) + ">";
    }
}
