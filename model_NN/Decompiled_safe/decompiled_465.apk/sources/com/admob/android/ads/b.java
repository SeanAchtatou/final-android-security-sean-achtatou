package com.admob.android.ads;

import android.view.View;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ZPositionAnimation */
public final class b extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private View f22a;
    private float b;
    private float c;

    /* compiled from: ZPositionAnimation */
    public interface a {
        void f();
    }

    public b(float f, float f2, View view) {
        this.b = f;
        this.c = f2;
        this.f22a = view;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        transformation.setTransformationType(Transformation.TYPE_IDENTITY);
        if (((double) f) >= 0.0d && ((double) f) <= 1.0d) {
            float f2 = this.b + ((this.c - this.b) * f);
            View view = this.f22a;
            if (view != null) {
                f c2 = f.c(view);
                c2.f33a = f2;
                view.setTag(c2);
            }
            ViewParent parent = this.f22a.getParent();
            if (parent instanceof a) {
                ((a) parent).f();
            }
        }
    }
}
