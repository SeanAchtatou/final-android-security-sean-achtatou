package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;

public class AdManager {
    public static final String LOG = "AdMobSDK";
    public static final String SDK_VERSION = "20100331-ANDROID-3312276cc1406347";
    public static final String SDK_VERSION_DATE = "20100331";
    public static final String TEST_EMULATOR = "emulator";

    /* renamed from: a  reason: collision with root package name */
    private static String f11a;
    private static int b;
    private static String c;
    private static String d = "url";
    private static String[] e = null;
    private static String f;
    /* access modifiers changed from: private */
    public static Location g;
    /* access modifiers changed from: private */
    public static long h;
    private static String i;
    private static GregorianCalendar j;
    private static Gender k;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        Log.i(LOG, "AdMob SDK version is 20100331-ANDROID-3312276cc1406347");
    }

    private AdManager() {
    }

    protected static void clientError(String str) {
        Log.e(LOG, str);
        throw new IllegalArgumentException(str);
    }

    protected static int getScreenWidth(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getWidth();
        }
        return 0;
    }

    private static void b(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                if (applicationInfo.metaData != null) {
                    String string = applicationInfo.metaData.getString("ADMOB_PUBLISHER_ID");
                    if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, "Publisher ID read from AndroidManifest.xml is " + string);
                    }
                    if (c == null && string != null) {
                        if (!string.equals("a1496ced2842262") || (!context.getPackageName().equals("com.admob.android.ads") && !context.getPackageName().equals("com.example.admob.lunarlander"))) {
                            setPublisherId(string);
                        } else {
                            Log.i(LOG, "This is a sample application so allowing sample publisher ID.");
                            c = string;
                        }
                    }
                }
                f11a = applicationInfo.packageName;
                if (Log.isLoggable(LOG, 2)) {
                    Log.v(LOG, "Application's package name is " + f11a);
                }
            }
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
                b = packageInfo.versionCode;
                if (Log.isLoggable(LOG, 2)) {
                    Log.v(LOG, "Application's version number is " + b);
                }
            }
        } catch (Exception e2) {
        }
    }

    protected static String getApplicationPackageName(Context context) {
        if (f11a == null) {
            b(context);
        }
        return f11a;
    }

    protected static int getApplicationVersion(Context context) {
        if (f11a == null) {
            b(context);
        }
        return b;
    }

    public static String getPublisherId(Context context) {
        if (c == null) {
            b(context);
        }
        if (c == null && Log.isLoggable(LOG, 6)) {
            Log.e(LOG, "getPublisherId returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherId(String)");
        }
        return c;
    }

    public static void setPublisherId(String str) {
        if (str == null || str.length() != 15) {
            clientError("SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  " + c);
        }
        if (str.equalsIgnoreCase("a1496ced2842262")) {
            clientError("SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com.");
        }
        Log.i(LOG, "Publisher ID set to " + str);
        c = str;
    }

    public static String getTestAction() {
        return d;
    }

    public static void setTestDevices(String[] strArr) {
        if (strArr == null) {
            e = null;
            return;
        }
        String[] strArr2 = (String[]) strArr.clone();
        e = strArr2;
        Arrays.sort(strArr2);
    }

    static String[] getTestDevices() {
        return e;
    }

    public static boolean isTestDevice(Context context) {
        if (e == null) {
            return false;
        }
        String userId = getUserId(context);
        if (userId == null) {
            userId = TEST_EMULATOR;
        }
        if (Arrays.binarySearch(e, userId) >= 0) {
            return true;
        }
        return false;
    }

    public static void setTestAction(String str) {
        d = str;
    }

    public static String getUserId(Context context) {
        if (f == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null) {
                f = TEST_EMULATOR;
                Log.i(LOG, "To get test ads on the emulator use AdManager.setTestDevices( new String[] { Admanager.TEST_EMULATOR } )");
            } else {
                f = a(string);
                Log.i(LOG, "To get test ads on this device use AdManager.setTestDevices( new String[] { \"" + f + "\" } )");
            }
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, "The user ID is " + f);
            }
        }
        if (f == TEST_EMULATOR) {
            return null;
        }
        return f;
    }

    private static String a(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            Log.d(LOG, "Could not generate hash of " + str, e2);
            return str.substring(0, 32);
        }
    }

    public static Location getCoordinates(Context context) {
        String str;
        final LocationManager locationManager;
        boolean z;
        if (context != null && (g == null || System.currentTimeMillis() > h + 900000)) {
            synchronized (context) {
                if (g == null || System.currentTimeMillis() > h + 900000) {
                    h = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from the network.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria, true);
                            z = true;
                        } else {
                            str = null;
                            z = true;
                        }
                    } else {
                        str = null;
                        locationManager = null;
                        z = false;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from GPS.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            z = true;
                        }
                    }
                    if (!z) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str != null) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Location provider setup successfully.");
                        }
                        locationManager.requestLocationUpdates(str, 0, 0.0f, new LocationListener() {
                            public final void onLocationChanged(Location location) {
                                Location unused = AdManager.g = location;
                                long unused2 = AdManager.h = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                                if (Log.isLoggable(AdManager.LOG, 3)) {
                                    Log.d(AdManager.LOG, "Acquired location " + AdManager.g.getLatitude() + "," + AdManager.g.getLongitude() + " at " + new Date(AdManager.h).toString() + ".");
                                }
                            }

                            public final void onProviderDisabled(String str) {
                            }

                            public final void onProviderEnabled(String str) {
                            }

                            public final void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                    } else if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return g;
    }

    static String a(Context context) {
        String str = null;
        Location coordinates = getCoordinates(context);
        if (coordinates != null) {
            str = coordinates.getLatitude() + "," + coordinates.getLongitude();
        }
        if (Log.isLoggable(LOG, 3)) {
            Log.d(LOG, "User coordinates are " + str);
        }
        return str;
    }

    static String a() {
        return String.valueOf(h / 1000);
    }

    public static String getPostalCode() {
        return i;
    }

    public static String getOrientation(Context context) {
        if (((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation() == 1) {
            return "l";
        }
        return "p";
    }

    public static void setPostalCode(String str) {
        i = str;
    }

    public static GregorianCalendar getBirthday() {
        return j;
    }

    static String b() {
        GregorianCalendar birthday = getBirthday();
        if (birthday == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(birthday.get(1)), Integer.valueOf(birthday.get(2) + 1), Integer.valueOf(birthday.get(5)));
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        j = gregorianCalendar;
    }

    public static void setBirthday(int i2, int i3, int i4) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i2, i3 - 1, i4);
        setBirthday(gregorianCalendar);
    }

    public static Gender getGender() {
        return k;
    }

    static String c() {
        if (k == Gender.MALE) {
            return "m";
        }
        if (k == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static void setGender(Gender gender) {
        k = gender;
    }

    static void setEndpoint(String str) {
        u.a(str);
    }

    static String getEndpoint() {
        return u.a();
    }
}
