package com.admob.android.ads;

import android.graphics.PointF;
import android.view.View;

/* compiled from: ViewInfo */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    public float f33a = 0.0f;
    public PointF b = new PointF(0.5f, 0.5f);

    public static float a(View view) {
        if (view != null) {
            return c(view).f33a;
        }
        return 0.0f;
    }

    public static PointF b(View view) {
        if (view != null) {
            return c(view).b;
        }
        return null;
    }

    public static f c(View view) {
        Object tag = view.getTag();
        if (tag == null || !(tag instanceof f)) {
            return new f();
        }
        return (f) tag;
    }
}
