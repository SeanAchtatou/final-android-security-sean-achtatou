package bys.widgets.jwidget;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class AboutActivity extends Activity {
    AdView adView;
    Handler mHandler = new Handler();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        ((WebView) findViewById(R.id.WebView01)).loadUrl("file:///android_asset/about.html");
        this.adView = (AdView) findViewById(R.id.ad2);
        this.adView.loadAd(new AdRequest());
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                Log.v(TempleInfo.strIdentifier, "LordJesusPrayer :m_taskAdsWait Run");
                AboutActivity.this.adView.setVisibility(0);
            }
        }, 120000);
    }
}
