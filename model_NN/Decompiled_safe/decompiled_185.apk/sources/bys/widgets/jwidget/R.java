package bys.widgets.jwidget;

public final class R {

    public static final class array {
        public static final int LyricDisplayTime = 2131230720;
        public static final int quotes = 2131230721;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class color {
        public static final int dark_brown = 2131165184;
    }

    public static final class drawable {
        public static final int bronze_statues_temple_bell_40 = 2130837504;
        public static final int candle = 2130837505;
        public static final int classic_clock = 2130837506;
        public static final int glowing_ohm = 2130837507;
        public static final int god_icon = 2130837508;
        public static final int god_image_01 = 2130837509;
        public static final int god_image_02 = 2130837510;
        public static final int god_image_03 = 2130837511;
        public static final int god_image_04 = 2130837512;
        public static final int god_image_05 = 2130837513;
        public static final int god_image_06 = 2130837514;
        public static final int god_image_07 = 2130837515;
        public static final int god_image_08 = 2130837516;
        public static final int god_image_09 = 2130837517;
        public static final int god_image_10 = 2130837518;
        public static final int god_image_11 = 2130837519;
        public static final int god_image_12 = 2130837520;
        public static final int god_image_13 = 2130837521;
        public static final int god_image_14 = 2130837522;
        public static final int god_image_15 = 2130837523;
        public static final int god_image_16 = 2130837524;
        public static final int god_image_17 = 2130837525;
        public static final int god_image_18 = 2130837526;
        public static final int god_image_19 = 2130837527;
        public static final int god_image_20 = 2130837528;
        public static final int god_image_21 = 2130837529;
        public static final int gold_plate = 2130837530;
        public static final int gold_plate_with_flower = 2130837531;
        public static final int help_us = 2130837532;
        public static final int menu_how_to = 2130837533;
        public static final int not_glowing_ohm = 2130837534;
        public static final int picture_frame = 2130837535;
        public static final int shankh = 2130837536;
        public static final int side_curtain = 2130837537;
        public static final int sub_form_background = 2130837538;
        public static final int sub_form_dip_background = 2130837539;
        public static final int top_curtain = 2130837540;
    }

    public static final class id {
        public static final int ActivityGodImage = 2131099661;
        public static final int BANNER = 2131099648;
        public static final int FrameAndImageHolder = 2131099655;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int LayoutQoute = 2131099667;
        public static final int LinearLayout01 = 2131099654;
        public static final int LinearLayout02 = 2131099660;
        public static final int NoteDisableShanknad = 2131099691;
        public static final int TextView01 = 2131099682;
        public static final int WebView01 = 2131099652;
        public static final int WidgetGodImage1 = 2131099671;
        public static final int WidgetGodImage2 = 2131099669;
        public static final int WidgetGodImage3 = 2131099670;
        public static final int ad = 2131099666;
        public static final int ad2 = 2131099653;
        public static final int ad3 = 2131099679;
        public static final int btnChangeAlarmTriggerTime = 2131099684;
        public static final int btnCheckOurOtherApps = 2131099677;
        public static final int btnDonate = 2131099678;
        public static final int btnGiveFiveStar = 2131099674;
        public static final int btnSankh = 2131099662;
        public static final int btnSuggestImprovement = 2131099673;
        public static final int btnTellOthersEmail = 2131099676;
        public static final int btnTellOthersSMS = 2131099675;
        public static final int chkDisableShankhnad = 2131099690;
        public static final int chkEnableAlarm = 2131099681;
        public static final int chkEnableRepeat = 2131099686;
        public static final int edtRepeatCount = 2131099689;
        public static final int imgBellLeft = 2131099656;
        public static final int imgBellRight = 2131099657;
        public static final int imgDiya = 2131099658;
        public static final int imgOhm = 2131099659;
        public static final int imgSideLeftCurtain = 2131099663;
        public static final int imgSideRightCurtain = 2131099664;
        public static final int imgTopCurtain = 2131099665;
        public static final int layoutAlarmSetting = 2131099680;
        public static final int layoutRepeatAudio = 2131099685;
        public static final int mnuSetAsAlarm = 2131099692;
        public static final int mnuTellOthers = 2131099693;
        public static final int rdbRepeatContinuosly = 2131099687;
        public static final int rdbRepeatNTimes = 2131099688;
        public static final int txtAlarmTriggerTime = 2131099683;
        public static final int txtIntroduction = 2131099672;
        public static final int txtQoute = 2131099668;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int full_screen_layout = 2130903041;
        public static final int god_widget_layout_160_200 = 2130903042;
        public static final int god_widget_layout_240_300 = 2130903043;
        public static final int god_widget_layout_80_100 = 2130903044;
        public static final int help_us_layout = 2130903045;
        public static final int settings_layout = 2130903046;
    }

    public static final class menu {
        public static final int option_menu = 2131361792;
    }

    public static final class raw {
        public static final int bell = 2131034112;
        public static final int chanting = 2131034113;
    }

    public static final class string {
        public static final int CheckOurOtherApps = 2131296272;
        public static final int Continuously = 2131296258;
        public static final int DisableShankhnad = 2131296260;
        public static final int DisableShankhnadNote = 2131296259;
        public static final int Donate = 2131296273;
        public static final int EnableAlarm = 2131296262;
        public static final int HelpUs = 2131296267;
        public static final int HowTo = 2131296266;
        public static final int Introduction = 2131296274;
        public static final int Rate5Star = 2131296269;
        public static final int Repeat = 2131296257;
        public static final int RepeatMantra = 2131296261;
        public static final int SetAsAlarm = 2131296265;
        public static final int SuggestImprovement = 2131296268;
        public static final int TellOthers = 2131296264;
        public static final int TellOthersEmail = 2131296271;
        public static final int TellOthersSMS = 2131296270;
        public static final int Time = 2131296263;
        public static final int Times = 2131296256;
        public static final int app_name = 2131296275;
        public static final int widget_name_160_200 = 2131296277;
        public static final int widget_name_240_300 = 2131296278;
        public static final int widget_name_80_100 = 2131296276;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }

    public static final class xml {
        public static final int god_widget_info_160_200 = 2130968576;
        public static final int god_widget_info_240_300 = 2130968577;
        public static final int god_widget_info_80_100 = 2130968578;
    }
}
