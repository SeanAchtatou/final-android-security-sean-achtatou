package bys.widgets.jwidget;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.Calendar;

public class SettingsActivity extends Activity {
    private static final int DIALOG_TIME_PICK = 1;
    private static final String mstrAppIdentifier = "LordJesusPrayer";
    AlarmManager am = null;
    CheckBox chkDisableShankhnad = null;
    CheckBox chkEnableAlarm = null;
    CheckBox chkEnableRepeat = null;
    EditText edtRepeatCount = null;
    boolean mblnDisableShanknad = false;
    boolean mblnEnableAudioRepeat = false;
    boolean mblnRepeatContinous = false;
    int mintAudioRepeatCount = 1;
    int mintSavedAlarmHourTime;
    int mintSavedAlarmMinuteTime;
    PendingIntent pi = null;
    PowerManager pm = null;
    RadioButton rdbRepeatContinuosly = null;
    RadioButton rdbRepeatNTimes = null;
    TextView txtTime = null;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        SettingsActivity.this.mintSavedAlarmHourTime = hourOfDay;
                        SettingsActivity.this.mintSavedAlarmMinuteTime = minute;
                        SettingsActivity.this.SetUserSpecifiedAlarmTime();
                    }
                }, this.mintSavedAlarmHourTime, this.mintSavedAlarmMinuteTime, false);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mintAudioRepeatCount = new Integer(this.edtRepeatCount.getText().toString()).intValue();
        SavePreference();
        super.onDestroy();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.settings_layout);
        this.am = (AlarmManager) getSystemService("alarm");
        this.pi = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("INTENT_LordJesusPrayer"), 0);
        this.chkEnableAlarm = (CheckBox) findViewById(R.id.chkEnableAlarm);
        this.chkEnableRepeat = (CheckBox) findViewById(R.id.chkEnableRepeat);
        this.chkDisableShankhnad = (CheckBox) findViewById(R.id.chkDisableShankhnad);
        this.edtRepeatCount = (EditText) findViewById(R.id.edtRepeatCount);
        this.rdbRepeatContinuosly = (RadioButton) findViewById(R.id.rdbRepeatContinuosly);
        this.rdbRepeatNTimes = (RadioButton) findViewById(R.id.rdbRepeatNTimes);
        ReadPreference();
        ApplyPreferenceOnUI();
        this.chkEnableRepeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.mblnEnableAudioRepeat = isChecked;
                if (!isChecked) {
                    SettingsActivity.this.rdbRepeatContinuosly.setChecked(false);
                    SettingsActivity.this.rdbRepeatNTimes.setChecked(false);
                    SettingsActivity.this.rdbRepeatContinuosly.setEnabled(false);
                    SettingsActivity.this.rdbRepeatNTimes.setEnabled(false);
                    SettingsActivity.this.edtRepeatCount.setEnabled(false);
                    return;
                }
                SettingsActivity.this.rdbRepeatContinuosly.setEnabled(true);
                SettingsActivity.this.rdbRepeatNTimes.setEnabled(true);
                if (SettingsActivity.this.mblnRepeatContinous) {
                    SettingsActivity.this.rdbRepeatContinuosly.setChecked(true);
                    SettingsActivity.this.rdbRepeatNTimes.setChecked(false);
                    SettingsActivity.this.edtRepeatCount.setEnabled(false);
                    return;
                }
                SettingsActivity.this.rdbRepeatContinuosly.setChecked(false);
                SettingsActivity.this.rdbRepeatNTimes.setChecked(true);
                SettingsActivity.this.edtRepeatCount.setEnabled(true);
            }
        });
        this.chkDisableShankhnad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.mblnDisableShanknad = isChecked;
            }
        });
        this.rdbRepeatContinuosly.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnRepeatContinous = true;
                SettingsActivity.this.rdbRepeatNTimes.setChecked(false);
                SettingsActivity.this.edtRepeatCount.setEnabled(false);
            }
        });
        this.rdbRepeatNTimes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnRepeatContinous = false;
                SettingsActivity.this.rdbRepeatContinuosly.setChecked(false);
                SettingsActivity.this.edtRepeatCount.setEnabled(true);
            }
        });
        this.txtTime = (TextView) findViewById(R.id.txtAlarmTriggerTime);
        GetSavedAlarmTime();
        DisplayAlarmTime();
        ((ImageButton) findViewById(R.id.btnChangeAlarmTriggerTime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.showDialog(1);
            }
        });
        this.chkEnableAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.EnableDisableAlarm();
                SettingsActivity.this.SaveAlarmTime();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void SetUserSpecifiedAlarmTime() {
        SaveAlarmTime();
        DisplayAlarmTime();
        EnableDisableAlarm();
    }

    /* access modifiers changed from: package-private */
    public String GetAlarmTimeDisplayString() {
        String lstrMinute;
        String lstrHour = new StringBuilder().append(this.mintSavedAlarmHourTime > 11 ? this.mintSavedAlarmHourTime - 12 : this.mintSavedAlarmHourTime).toString();
        if (this.mintSavedAlarmMinuteTime > 9) {
            lstrMinute = new StringBuilder().append(this.mintSavedAlarmMinuteTime).toString();
        } else {
            lstrMinute = "0" + this.mintSavedAlarmMinuteTime;
        }
        return String.valueOf(lstrHour) + ":" + lstrMinute + " " + ((this.mintSavedAlarmHourTime * 60) + this.mintSavedAlarmMinuteTime < 720 ? "AM" : "PM");
    }

    /* access modifiers changed from: package-private */
    public void DisplayAlarmTime() {
        this.txtTime.setText(GetAlarmTimeDisplayString());
    }

    /* access modifiers changed from: package-private */
    public void EnableDisableAlarm() {
        Calendar calAlarmTrigger = Calendar.getInstance();
        if (this.chkEnableAlarm.isChecked()) {
            Calendar calNow = Calendar.getInstance();
            if ((calNow.get(11) * 60) + calNow.get(12) > (this.mintSavedAlarmHourTime * 60) + this.mintSavedAlarmMinuteTime) {
                calAlarmTrigger.set(calNow.get(1), calNow.get(2), calNow.get(5) + 1, this.mintSavedAlarmHourTime, this.mintSavedAlarmMinuteTime);
            } else {
                calAlarmTrigger.set(calNow.get(1), calNow.get(2), calNow.get(5), this.mintSavedAlarmHourTime, this.mintSavedAlarmMinuteTime);
            }
            Log.v("LordJesusPrayer:EnableDisableAlarm", "Activating Alarm " + calAlarmTrigger.toString());
            this.am.cancel(this.pi);
            this.am.setRepeating(0, calAlarmTrigger.getTimeInMillis(), 86400000, this.pi);
            Toast.makeText(getApplicationContext(), "Alarm set @ " + GetAlarmTimeDisplayString(), 1).show();
            return;
        }
        Log.v("LordJesusPrayer:EnableDisableAlarm", "Disabling Alarm ");
        this.am.cancel(this.pi);
    }

    private void ReadPreference() {
        SharedPreferences settings = getSharedPreferences("LordJesusPrayer", 0);
        this.mblnDisableShanknad = settings.getBoolean("Key_DisableShankhnad", false);
        this.mblnEnableAudioRepeat = settings.getBoolean("Key_EnableAudioRepeat", false);
        this.mblnRepeatContinous = settings.getBoolean("Key_RepeatContinous", false);
        this.mintAudioRepeatCount = settings.getInt("Key_AudioRepeatCount", 1);
    }

    private void ApplyPreferenceOnUI() {
        this.chkEnableRepeat.setChecked(this.mblnEnableAudioRepeat);
        this.chkDisableShankhnad.setChecked(this.mblnDisableShanknad);
        this.edtRepeatCount.setText(new StringBuilder().append(this.mintAudioRepeatCount).toString());
        if (this.mblnEnableAudioRepeat) {
            this.rdbRepeatContinuosly.setEnabled(true);
            this.rdbRepeatNTimes.setEnabled(true);
            if (this.mblnRepeatContinous) {
                this.rdbRepeatContinuosly.setChecked(true);
                this.rdbRepeatNTimes.setChecked(false);
                this.edtRepeatCount.setEnabled(false);
                return;
            }
            this.rdbRepeatContinuosly.setChecked(false);
            this.rdbRepeatNTimes.setChecked(true);
            this.edtRepeatCount.setEnabled(true);
            return;
        }
        this.rdbRepeatContinuosly.setChecked(false);
        this.rdbRepeatNTimes.setChecked(false);
        this.rdbRepeatContinuosly.setEnabled(false);
        this.rdbRepeatNTimes.setEnabled(false);
        this.edtRepeatCount.setEnabled(false);
    }

    private void SavePreference() {
        SharedPreferences.Editor editor = getSharedPreferences("LordJesusPrayer", 0).edit();
        editor.putBoolean("Key_DisableShankhnad", this.mblnDisableShanknad);
        editor.putBoolean("Key_EnableAudioRepeat", this.mblnEnableAudioRepeat);
        editor.putBoolean("Key_RepeatContinous", this.mblnRepeatContinous);
        editor.putInt("Key_AudioRepeatCount", this.mintAudioRepeatCount);
        editor.commit();
    }

    private void GetSavedAlarmTime() {
        SharedPreferences settings = getSharedPreferences("LordJesusPrayer", 0);
        this.mintSavedAlarmHourTime = settings.getInt("WakeUpHourTime", 6);
        this.mintSavedAlarmMinuteTime = settings.getInt("WakeUpMinuteTime", 0);
        this.chkEnableAlarm.setChecked(settings.getBoolean("AlarmEnabled", false));
    }

    /* access modifiers changed from: private */
    public void SaveAlarmTime() {
        SharedPreferences.Editor editor = getSharedPreferences("LordJesusPrayer", 0).edit();
        editor.putInt("WakeUpHourTime", this.mintSavedAlarmHourTime);
        editor.putInt("WakeUpMinuteTime", this.mintSavedAlarmMinuteTime);
        editor.putBoolean("AlarmEnabled", this.chkEnableAlarm.isChecked());
        editor.commit();
    }
}
