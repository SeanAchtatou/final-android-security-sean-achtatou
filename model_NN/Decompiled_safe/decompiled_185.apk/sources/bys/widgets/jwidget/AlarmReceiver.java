package bys.widgets.jwidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Intent startActivity = new Intent();
        Log.v("onReceive", "Starting Activity FullScreenActivity");
        startActivity.setClass(context, FullScreenActivity.class);
        startActivity.setAction(FullScreenActivity.class.getName());
        startActivity.setFlags(276824064);
        context.startActivity(startActivity);
    }
}
