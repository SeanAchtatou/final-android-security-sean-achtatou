package bys.widgets.jwidget;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.Random;

public class FullScreenActivity extends Activity implements ViewSwitcher.ViewFactory {
    private static final String mstrAppIdentifier = "LordJesusPrayer";
    AdView adView;
    int[] arrLngQouteDisplayTime = null;
    ImageSwitcher imgGod = null;
    ImageView imgOhm;
    ImageView imgSideLeftCurtain;
    ImageView imgSideRightCurtain;
    ImageView imgTopCurtain;
    LinearLayout layoutQoute;
    Handler mHandler = new Handler();
    int m_intImageCount = 21;
    int m_intLyricCount = 0;
    Runnable m_taskCurtainClose;
    Runnable m_taskCurtainOpen;
    boolean mblnCurtainOpen = false;
    boolean mblnDisableShanknad = false;
    boolean mblnEnableAudioRepeat = false;
    boolean mblnIsScreenOnWhenStarted = false;
    boolean mblnRepeatContinous = false;
    boolean mblnTextQuoteBackIsHindi = true;
    BellManager mclsBellManager = null;
    MantraManager mclsMantraManager = null;
    int mintAudioRepeatCount = 1;
    int mlintLyricIndex = 0;
    PowerManager pm = null;
    TextView txtQoute;
    PowerManager.WakeLock wl = null;

    private interface MantraManagerListeners {
        void onChangeImage(int i);

        void onMantraEnded();

        void onPauseMantra(boolean z);

        void onSetLyric(int i);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.v("LordJesusPrayer", "LordJesusPrayer:onDestroy:");
        this.mclsMantraManager.StopMantra();
        this.mclsBellManager.Destroy();
        if (this.wl.isHeld()) {
            this.wl.release();
        }
        this.mHandler.removeCallbacks(this.m_taskCurtainOpen);
        this.mHandler.removeCallbacks(this.m_taskCurtainClose);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.v("LordJesusPrayer", "LordJesusPrayer:onStop");
        if (this.mHandler != null) {
            this.mHandler.removeCallbacks(this.m_taskCurtainOpen);
            this.mHandler.removeCallbacks(this.m_taskCurtainClose);
        }
        this.mclsBellManager.Destroy();
        this.mclsMantraManager.PauseMantra();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v("LordJesusPrayer", "LordJesusPrayer:onResume:");
        if (this.mblnCurtainOpen) {
            this.mclsMantraManager.ResumeMantra();
        }
        super.onResume();
    }

    /* access modifiers changed from: private */
    public void DisplayRandomQoute() {
        String[] lstrStrings = getResources().getStringArray(R.array.quotes);
        int intQouteIndex = new RandomGenerator(this, null).nextInt(0, lstrStrings.length - 1);
        Log.v("GodWidgetProvider", "Qoute of Index: " + intQouteIndex);
        this.txtQoute.setText(lstrStrings[intQouteIndex]);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        Log.v("LordJesusPrayer", "LordJesusPrayer:onCreate:");
        setContentView((int) R.layout.full_screen_layout);
        ReadPreference();
        this.adView = (AdView) findViewById(R.id.ad);
        this.adView.loadAd(new AdRequest());
        this.arrLngQouteDisplayTime = getResources().getIntArray(R.array.LyricDisplayTime);
        this.m_intLyricCount = this.arrLngQouteDisplayTime.length;
        this.mclsBellManager = new BellManager();
        this.mclsMantraManager = new MantraManager();
        this.mclsMantraManager.setMantraManagerListeners(new MantraManagerListeners() {
            int intQouteIndex = 0;

            public void onChangeImage(int intImageIndex) {
                FullScreenActivity.this.imgGod.setImageResource(R.drawable.god_image_01 + intImageIndex);
            }

            public void onSetLyric(int intLyricIndex) {
                if (!FullScreenActivity.this.mblnDisableShanknad) {
                    String[] lstrStrings = FullScreenActivity.this.getResources().getStringArray(R.array.quotes);
                    if (this.intQouteIndex < lstrStrings.length) {
                        FullScreenActivity.this.txtQoute.setText(lstrStrings[this.intQouteIndex]);
                        this.intQouteIndex++;
                    }
                }
            }

            public void onPauseMantra(boolean blnFlag) {
                if (blnFlag) {
                    FullScreenActivity.this.imgOhm.setBackgroundResource(R.drawable.not_glowing_ohm);
                } else {
                    FullScreenActivity.this.imgOhm.setBackgroundResource(R.drawable.glowing_ohm);
                }
            }

            public void onMantraEnded() {
                FullScreenActivity.this.mHandler.postDelayed(FullScreenActivity.this.m_taskCurtainClose, 100);
            }
        });
        this.pm = (PowerManager) getSystemService("power");
        this.wl = this.pm.newWakeLock(26, "LordJesusPrayer");
        this.txtQoute = (TextView) findViewById(R.id.txtQoute);
        this.layoutQoute = (LinearLayout) findViewById(R.id.LayoutQoute);
        final View layoutMainHolder = findViewById(R.id.FrameAndImageHolder);
        this.imgGod = (ImageSwitcher) findViewById(R.id.ActivityGodImage);
        this.imgGod.setFactory(this);
        Animation in = AnimationUtils.loadAnimation(this, 17432576);
        Animation out = AnimationUtils.loadAnimation(this, 17432577);
        out.setDuration(1000);
        in.setDuration(2000);
        this.imgGod.setInAnimation(in);
        this.imgGod.setOutAnimation(out);
        Log.v("LordJesusPrayer", "Setting On Click Listener to imgGod" + this.imgGod);
        this.imgGod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RandomGenerator randGen = new RandomGenerator(FullScreenActivity.this, null);
                if (FullScreenActivity.this.m_intImageCount > 0) {
                    FullScreenActivity.this.imgGod.setImageResource(R.drawable.god_image_01 + randGen.nextInt(0, FullScreenActivity.this.m_intImageCount - 1));
                }
            }
        });
        ((ImageView) findViewById(R.id.imgBellLeft)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FullScreenActivity.this.mclsBellManager.PlayLeftBell(false);
            }
        });
        ((ImageView) findViewById(R.id.imgBellRight)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FullScreenActivity.this.mclsBellManager.PlayRightBell(false);
            }
        });
        this.imgOhm = (ImageView) findViewById(R.id.imgOhm);
        this.imgOhm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FullScreenActivity.this.mclsMantraManager.IsPlaying()) {
                    FullScreenActivity.this.mclsMantraManager.PauseMantra();
                } else {
                    FullScreenActivity.this.mclsMantraManager.ResumeMantra();
                }
            }
        });
        this.imgTopCurtain = (ImageView) findViewById(R.id.imgTopCurtain);
        this.imgSideLeftCurtain = (ImageView) findViewById(R.id.imgSideLeftCurtain);
        this.imgSideRightCurtain = (ImageView) findViewById(R.id.imgSideRightCurtain);
        final Runnable m_taskUnlockSleep = new Runnable() {
            public void run() {
                if (FullScreenActivity.this.wl.isHeld()) {
                    FullScreenActivity.this.wl.release();
                }
            }
        };
        this.m_taskCurtainClose = new Runnable() {
            public void run() {
                ViewGroup.LayoutParams paramsLeftCurtain = FullScreenActivity.this.imgSideLeftCurtain.getLayoutParams();
                ViewGroup.LayoutParams layoutParams = FullScreenActivity.this.imgSideRightCurtain.getLayoutParams();
                if (paramsLeftCurtain.width < layoutMainHolder.getWidth() / 2) {
                    ViewGroup.LayoutParams paramsLeftCurtain2 = FullScreenActivity.this.imgSideLeftCurtain.getLayoutParams();
                    ViewGroup.LayoutParams paramsRightCurtain = FullScreenActivity.this.imgSideRightCurtain.getLayoutParams();
                    paramsLeftCurtain2.width += 3;
                    paramsRightCurtain.width += 3;
                    if (paramsLeftCurtain2.width >= layoutMainHolder.getWidth() / 2) {
                        FullScreenActivity.this.adView.setVisibility(0);
                        FullScreenActivity.this.adView.loadAd(new AdRequest());
                        FullScreenActivity.this.layoutQoute.setVisibility(8);
                        FullScreenActivity.this.imgOhm.setBackgroundResource(R.drawable.not_glowing_ohm);
                        FullScreenActivity.this.mblnCurtainOpen = false;
                        FullScreenActivity.this.mclsMantraManager.StopMantra();
                        FullScreenActivity.this.mHandler.postDelayed(m_taskUnlockSleep, 30000);
                    }
                    Log.v("LordJesusPrayer", "In run " + paramsLeftCurtain2.width);
                    FullScreenActivity.this.imgSideLeftCurtain.setLayoutParams(paramsLeftCurtain2);
                    FullScreenActivity.this.imgSideRightCurtain.setLayoutParams(paramsRightCurtain);
                    if (FullScreenActivity.this.mHandler != null) {
                        FullScreenActivity.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 100);
                    }
                }
            }
        };
        this.m_taskCurtainOpen = new Runnable() {
            public void run() {
                ViewGroup.LayoutParams paramsLeftCurtain = FullScreenActivity.this.imgSideLeftCurtain.getLayoutParams();
                ViewGroup.LayoutParams layoutParams = FullScreenActivity.this.imgSideRightCurtain.getLayoutParams();
                if (paramsLeftCurtain.width > 2) {
                    ViewGroup.LayoutParams paramsLeftCurtain2 = FullScreenActivity.this.imgSideLeftCurtain.getLayoutParams();
                    ViewGroup.LayoutParams paramsRightCurtain = FullScreenActivity.this.imgSideRightCurtain.getLayoutParams();
                    paramsLeftCurtain2.width -= 3;
                    paramsRightCurtain.width = paramsLeftCurtain2.width;
                    if (paramsLeftCurtain2.width < 3 && !FullScreenActivity.this.mclsMantraManager.IsPlaying()) {
                        if (!FullScreenActivity.this.wl.isHeld()) {
                            FullScreenActivity.this.wl.acquire();
                        }
                        paramsLeftCurtain2.width = 0;
                        paramsRightCurtain.width = 0;
                        FullScreenActivity.this.mclsBellManager.PlayLeftBell(true);
                        FullScreenActivity.this.imgOhm.setBackgroundResource(R.drawable.glowing_ohm);
                        FullScreenActivity.this.txtQoute.setBackgroundResource(0);
                        FullScreenActivity.this.DisplayRandomQoute();
                        FullScreenActivity.this.mblnCurtainOpen = true;
                        FullScreenActivity.this.mclsMantraManager.StartMantra();
                    }
                    Log.v("LordJesusPrayer", "In run " + paramsLeftCurtain2.width);
                    FullScreenActivity.this.imgSideLeftCurtain.setLayoutParams(paramsLeftCurtain2);
                    FullScreenActivity.this.imgSideRightCurtain.setLayoutParams(paramsRightCurtain);
                    if (FullScreenActivity.this.mHandler != null) {
                        FullScreenActivity.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 50);
                    }
                }
            }
        };
        this.imgSideRightCurtain.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FullScreenActivity.this.mHandler.removeCallbacks(FullScreenActivity.this.m_taskCurtainClose);
                FullScreenActivity.this.mHandler.postDelayed(FullScreenActivity.this.m_taskCurtainOpen, 100);
                FullScreenActivity.this.adView.setVisibility(8);
                FullScreenActivity.this.layoutQoute.setVisibility(0);
            }
        });
        this.imgSideLeftCurtain.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FullScreenActivity.this.mHandler.removeCallbacks(FullScreenActivity.this.m_taskCurtainClose);
                FullScreenActivity.this.mHandler.postDelayed(FullScreenActivity.this.m_taskCurtainOpen, 100);
                FullScreenActivity.this.adView.setVisibility(8);
                FullScreenActivity.this.layoutQoute.setVisibility(0);
            }
        });
        ((Button) findViewById(R.id.btnSankh)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FullScreenActivity.this.DisplayRandomQoute();
            }
        });
        this.mHandler.postDelayed(this.m_taskCurtainOpen, 100);
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(FullScreenActivity fullScreenActivity, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }

    private String GetMessageBody() {
        return "~~:LordJesusPrayer:~~\n\r\n\rCheck this out.\n\r" + ("https://market.android.com/details?id=" + getPackageName()) + "\n\r" + "It is really soul enlightening application.\n\r";
    }

    private void SendAsSMS() {
        Intent sendIntent = new Intent("android.intent.action.VIEW", Uri.parse("sms:"));
        sendIntent.putExtra("sms_body", GetMessageBody());
        startActivity(sendIntent);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.imgSideLeftCurtain.getWidth() >= 10) {
            return super.onKeyDown(keyCode, event);
        }
        this.mHandler.postDelayed(this.m_taskCurtainClose, 100);
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuSetAsAlarm /*2131099692*/:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.mnuTellOthers /*2131099693*/:
                startActivity(new Intent(this, HelpUsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public View makeView() {
        ImageView i = new ImageView(this);
        i.setScaleType(ImageView.ScaleType.FIT_XY);
        i.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        return i;
    }

    private void ReadPreference() {
        SharedPreferences settings = getSharedPreferences("LordJesusPrayer", 0);
        this.mblnDisableShanknad = settings.getBoolean("Key_DisableShankhnad", false);
        this.mblnEnableAudioRepeat = settings.getBoolean("Key_EnableAudioRepeat", false);
        this.mblnRepeatContinous = settings.getBoolean("Key_RepeatContinous", false);
        this.mintAudioRepeatCount = settings.getInt("Key_AudioRepeatCount", 1);
    }

    private class BellManager {
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayerBellL = null;
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayerBellR = null;

        public BellManager() {
        }

        public void PlayRightBell(boolean flagBothSpeaker) {
            if (this.mMediaPlayerBellR != null) {
                this.mMediaPlayerBellR.release();
            }
            this.mMediaPlayerBellR = MediaPlayer.create(FullScreenActivity.this.getApplicationContext(), (int) R.raw.bell);
            if (this.mMediaPlayerBellR != null) {
                this.mMediaPlayerBellR.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        BellManager.this.mMediaPlayerBellR.release();
                        BellManager.this.mMediaPlayerBellR = null;
                    }
                });
                if (flagBothSpeaker) {
                    this.mMediaPlayerBellR.setVolume(70.0f, 70.0f);
                } else {
                    this.mMediaPlayerBellR.setVolume(100.0f, 0.0f);
                }
                this.mMediaPlayerBellR.start();
            }
        }

        public void PlayLeftBell(boolean flagBothSpeaker) {
            if (this.mMediaPlayerBellL != null) {
                this.mMediaPlayerBellL.release();
            }
            this.mMediaPlayerBellL = MediaPlayer.create(FullScreenActivity.this.getApplicationContext(), (int) R.raw.bell);
            if (this.mMediaPlayerBellL != null) {
                this.mMediaPlayerBellL.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        BellManager.this.mMediaPlayerBellL.release();
                        BellManager.this.mMediaPlayerBellL = null;
                    }
                });
                if (flagBothSpeaker) {
                    this.mMediaPlayerBellL.setVolume(70.0f, 70.0f);
                } else {
                    this.mMediaPlayerBellL.setVolume(0.0f, 100.0f);
                }
                this.mMediaPlayerBellL.start();
            }
        }

        public void Destroy() {
            if (this.mMediaPlayerBellR != null) {
                this.mMediaPlayerBellR.release();
                this.mMediaPlayerBellR = null;
            }
            if (this.mMediaPlayerBellL != null) {
                this.mMediaPlayerBellL.release();
                this.mMediaPlayerBellL = null;
            }
        }
    }

    private class MantraManager {
        /* access modifiers changed from: private */
        public int lintImageIndex;
        /* access modifiers changed from: private */
        public int lintLyricCount;
        /* access modifiers changed from: private */
        public int lintLyricIndex;
        private int lintPausedAt;
        /* access modifiers changed from: private */
        public int lintRepeatCounter;
        private MediaPlayer mMediaPlayerMantra;
        private Runnable m_taskSlideShow;
        private boolean mblnIsPlaying;
        private boolean mblnPaused;
        MantraManagerListeners onMantraManagerListener;

        public MantraManager() {
            this.onMantraManagerListener = null;
            this.mMediaPlayerMantra = null;
            this.m_taskSlideShow = null;
            this.mblnPaused = false;
            this.mblnIsPlaying = false;
            this.lintImageIndex = 0;
            this.lintLyricIndex = 0;
            this.lintLyricCount = 0;
            this.lintRepeatCounter = 0;
            this.lintPausedAt = 0;
            this.mMediaPlayerMantra = CreateChantingMediaObject();
            this.lintLyricCount = FullScreenActivity.this.arrLngQouteDisplayTime.length;
            Log.v("LordJesusPrayer", "LordJesusPrayer:MantraManager lintLyricCount = " + this.lintLyricCount);
            this.m_taskSlideShow = new Runnable() {
                public void run() {
                    if (FullScreenActivity.this.mHandler != null) {
                        if (MantraManager.this.lintLyricCount == 0) {
                            FullScreenActivity.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 25000);
                        } else if (MantraManager.this.lintLyricIndex < MantraManager.this.lintLyricCount) {
                            Log.v("LordJesusPrayer", "Lyric Lapse[" + MantraManager.this.lintLyricIndex + "] = " + FullScreenActivity.this.arrLngQouteDisplayTime[MantraManager.this.lintLyricIndex]);
                            FullScreenActivity.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + ((long) FullScreenActivity.this.arrLngQouteDisplayTime[MantraManager.this.lintLyricIndex]));
                        }
                    }
                    if (MantraManager.this.onMantraManagerListener != null) {
                        MantraManager mantraManager = MantraManager.this;
                        mantraManager.lintImageIndex = mantraManager.lintImageIndex + 1;
                        if (MantraManager.this.lintImageIndex > FullScreenActivity.this.m_intImageCount - 1) {
                            MantraManager.this.lintImageIndex = 0;
                        }
                        Log.v("LordJesusPrayer Widget: m_taskGodImageSlideShow:Run", "Setting image " + MantraManager.this.lintImageIndex);
                        MantraManager.this.onMantraManagerListener.onChangeImage(MantraManager.this.lintImageIndex);
                        Log.v("LordJesusPrayer", "Setting Lyric Index " + MantraManager.this.lintLyricIndex + "," + "Lyric Count " + MantraManager.this.lintLyricCount);
                        MantraManager.this.onMantraManagerListener.onSetLyric(MantraManager.this.lintLyricIndex);
                        if (MantraManager.this.lintLyricIndex < MantraManager.this.lintLyricCount - 1) {
                            MantraManager mantraManager2 = MantraManager.this;
                            mantraManager2.lintLyricIndex = mantraManager2.lintLyricIndex + 1;
                        }
                    }
                }
            };
        }

        public void setMantraManagerListeners(MantraManagerListeners listener) {
            this.onMantraManagerListener = listener;
        }

        public boolean IsPlaying() {
            return this.mblnIsPlaying;
        }

        /* access modifiers changed from: private */
        public void ReplayMantra() {
            this.lintLyricIndex = 0;
            FullScreenActivity.this.mHandler.removeCallbacks(this.m_taskSlideShow);
            FullScreenActivity.this.mHandler.postDelayed(this.m_taskSlideShow, 100);
            this.mMediaPlayerMantra.seekTo(0);
            this.mMediaPlayerMantra.start();
            this.mblnIsPlaying = true;
        }

        public void StartMantra() {
            this.lintLyricIndex = 0;
            this.lintRepeatCounter = 0;
            FullScreenActivity.this.mHandler.postDelayed(this.m_taskSlideShow, 100);
            if (this.mMediaPlayerMantra != null) {
                this.mMediaPlayerMantra.release();
            }
            this.mMediaPlayerMantra = CreateChantingMediaObject();
            if (this.mMediaPlayerMantra != null) {
                this.mMediaPlayerMantra.start();
                this.mblnIsPlaying = true;
            }
        }

        public void StopMantra() {
            FullScreenActivity.this.mHandler.removeCallbacks(this.m_taskSlideShow);
            if (this.mMediaPlayerMantra != null) {
                this.mMediaPlayerMantra.release();
                this.mMediaPlayerMantra = null;
                this.mblnIsPlaying = false;
            }
        }

        public void PauseMantra() {
            if (this.mMediaPlayerMantra != null) {
                this.mMediaPlayerMantra.pause();
                this.lintPausedAt = this.mMediaPlayerMantra.getCurrentPosition();
                this.mblnPaused = true;
                this.mblnIsPlaying = false;
                if (this.onMantraManagerListener != null) {
                    this.onMantraManagerListener.onPauseMantra(true);
                }
                FullScreenActivity.this.mHandler.removeCallbacks(this.m_taskSlideShow);
            }
        }

        public void ResumeMantra() {
            if (this.mMediaPlayerMantra != null && this.mblnPaused) {
                this.mMediaPlayerMantra.start();
                this.mblnPaused = false;
                this.mblnIsPlaying = true;
                if (this.onMantraManagerListener != null) {
                    this.onMantraManagerListener.onPauseMantra(false);
                }
                int lintNextLyricTime = 0;
                for (int i = 0; i < this.lintLyricIndex; i++) {
                    lintNextLyricTime += FullScreenActivity.this.arrLngQouteDisplayTime[i];
                }
                FullScreenActivity.this.mHandler.postDelayed(this.m_taskSlideShow, (long) (lintNextLyricTime - this.lintPausedAt));
            }
        }

        private MediaPlayer CreateChantingMediaObject() {
            MediaPlayer lMediaPlayer = null;
            try {
                lMediaPlayer = MediaPlayer.create(FullScreenActivity.this.getApplicationContext(), (int) R.raw.chanting);
                lMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("LordJesusPrayer", " onCompletion Counter = " + MantraManager.this.lintRepeatCounter);
                        if (FullScreenActivity.this.mblnRepeatContinous) {
                            MantraManager.this.ReplayMantra();
                            return;
                        }
                        MantraManager mantraManager = MantraManager.this;
                        mantraManager.lintRepeatCounter = mantraManager.lintRepeatCounter + 1;
                        if (MantraManager.this.lintRepeatCounter < FullScreenActivity.this.mintAudioRepeatCount) {
                            MantraManager.this.ReplayMantra();
                        } else if (MantraManager.this.onMantraManagerListener != null) {
                            MantraManager.this.onMantraManagerListener.onMantraEnded();
                        }
                    }
                });
                return lMediaPlayer;
            } catch (Exception e) {
                e.printStackTrace();
                return lMediaPlayer;
            }
        }
    }
}
