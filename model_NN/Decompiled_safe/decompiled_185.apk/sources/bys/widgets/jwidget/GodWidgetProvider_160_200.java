package bys.widgets.jwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GodWidgetProvider_160_200 extends AppWidgetProvider {
    Context m_Context = null;
    Timer timer = null;

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        this.m_Context = context;
        RemoteViews m_remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.god_widget_layout_160_200);
        AppWidgetManager m_appWidgetManager = appWidgetManager;
        for (int appWidgetId : appWidgetIds) {
            m_remoteViews.setOnClickPendingIntent(R.id.WidgetGodImage2, PendingIntent.getActivity(context, 0, new Intent(context, FullScreenActivity.class), 0));
            m_remoteViews.setImageViewResource(R.id.WidgetGodImage2, R.drawable.god_image_06);
            m_appWidgetManager.updateAppWidget(appWidgetId, m_remoteViews);
        }
        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(new ImageRefresherTimer(appWidgetManager), 1800000, 1800000);
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(GodWidgetProvider_160_200 godWidgetProvider_160_200, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }

    private class ImageRefresherTimer extends TimerTask {
        AppWidgetManager appWidgetManager;
        RandomGenerator randGen = null;
        RemoteViews remoteViews;
        ComponentName thisWidget;

        public ImageRefresherTimer(AppWidgetManager appWidgetManager2) {
            this.appWidgetManager = appWidgetManager2;
            this.remoteViews = new RemoteViews(GodWidgetProvider_160_200.this.m_Context.getPackageName(), (int) R.layout.god_widget_layout_160_200);
            this.thisWidget = new ComponentName(GodWidgetProvider_160_200.this.m_Context, GodWidgetProvider_160_200.class);
            this.randGen = new RandomGenerator(GodWidgetProvider_160_200.this, null);
        }

        public void run() {
            this.remoteViews.setImageViewResource(R.id.WidgetGodImage2, R.drawable.god_image_01 + this.randGen.nextInt(0, 20));
            this.appWidgetManager.updateAppWidget(this.thisWidget, this.remoteViews);
        }
    }
}
