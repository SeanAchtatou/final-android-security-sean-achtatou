package bys.widgets.jwidget;

public final class TempleInfo {
    public static final int intImageCount = 21;
    public static final int intResIdChanting = 2131034113;
    public static final int intResIdFirstImage = 2130837509;
    public static final int intResIdLyricDisplayTimeArray = 2131230720;
    public static final int intResIdQouteArray = 2131230721;
    public static final String strIdentifier = "LordJesusPrayer";
}
