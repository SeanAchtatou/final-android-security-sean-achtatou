package bys.widgets.jwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.widget.RemoteViews;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GodWidgetProvider_80_100 extends AppWidgetProvider {
    Context m_Context = null;
    Timer timer = null;

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        this.m_Context = context;
        RemoteViews m_remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.god_widget_layout_80_100);
        AppWidgetManager m_appWidgetManager = appWidgetManager;
        for (int appWidgetId : appWidgetIds) {
            m_remoteViews.setOnClickPendingIntent(R.id.WidgetGodImage1, PendingIntent.getActivity(context, 0, new Intent(context, FullScreenActivity.class), 0));
            m_remoteViews.setImageViewResource(R.id.WidgetGodImage1, R.drawable.god_image_05);
            m_appWidgetManager.updateAppWidget(appWidgetId, m_remoteViews);
        }
        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(new ImageRefresherTimer(appWidgetManager), 1800000, 1800000);
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(GodWidgetProvider_80_100 godWidgetProvider_80_100, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }

    private class ImageRefresherTimer extends TimerTask {
        AppWidgetManager appWidgetManager;
        int i = 0;
        RandomGenerator randGen = null;
        RemoteViews remoteViews;
        Resources res = null;
        ComponentName thisWidget;

        public ImageRefresherTimer(AppWidgetManager appWidgetManager2) {
            this.appWidgetManager = appWidgetManager2;
            this.res = GodWidgetProvider_80_100.this.m_Context.getResources();
            this.remoteViews = new RemoteViews(GodWidgetProvider_80_100.this.m_Context.getPackageName(), (int) R.layout.god_widget_layout_80_100);
            this.thisWidget = new ComponentName(GodWidgetProvider_80_100.this.m_Context, GodWidgetProvider_80_100.class);
            this.randGen = new RandomGenerator(GodWidgetProvider_80_100.this, null);
        }

        public void run() {
            this.remoteViews.setImageViewResource(R.id.WidgetGodImage1, R.drawable.god_image_01 + this.randGen.nextInt(0, 20));
            this.appWidgetManager.updateAppWidget(this.thisWidget, this.remoteViews);
        }
    }
}
