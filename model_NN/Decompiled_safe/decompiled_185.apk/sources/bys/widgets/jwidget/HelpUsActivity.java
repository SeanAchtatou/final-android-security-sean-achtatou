package bys.widgets.jwidget;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class HelpUsActivity extends Activity {
    private Button btnCheckOurOtherApps = null;
    private Button btnGiveFiveStar = null;
    private Button btnSuggestImprovement = null;
    private Button btnTellOthersEmail = null;
    private Button btnTellOthersSMS = null;
    /* access modifiers changed from: private */
    public String mstrAppIdentifier = TempleInfo.strIdentifier;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help_us_layout);
        this.btnSuggestImprovement = (Button) findViewById(R.id.btnSuggestImprovement);
        this.btnGiveFiveStar = (Button) findViewById(R.id.btnGiveFiveStar);
        this.btnTellOthersSMS = (Button) findViewById(R.id.btnTellOthersSMS);
        this.btnTellOthersEmail = (Button) findViewById(R.id.btnTellOthersEmail);
        this.btnCheckOurOtherApps = (Button) findViewById(R.id.btnCheckOurOtherApps);
        ((AdView) findViewById(R.id.ad3)).loadAd(new AdRequest());
        this.btnSuggestImprovement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpUsActivity.this.SendAsEmail(new String[]{"feedback@backyardsoft.com"}, "My Suggestions for Improvment", "");
            }
        });
        this.btnGiveFiveStar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=" + HelpUsActivity.this.getPackageName()));
                HelpUsActivity.this.startActivity(intent);
            }
        });
        this.btnTellOthersSMS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpUsActivity.this.SendAsSMS();
            }
        });
        this.btnTellOthersEmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpUsActivity.this.SendAsEmail(new String[]{""}, "~~:" + HelpUsActivity.this.mstrAppIdentifier + ":~~", HelpUsActivity.this.GetMessageBody());
            }
        });
        this.btnCheckOurOtherApps.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://search?q=pub:BackyardSoft"));
                HelpUsActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public String GetMessageBody() {
        return "Check this out.\n\r" + ("https://market.android.com/details?id=" + getPackageName()) + "\n\r" + "I found this android application very interesting.\n\r";
    }

    /* access modifiers changed from: private */
    public void SendAsSMS() {
        Intent sendIntent = new Intent("android.intent.action.VIEW", Uri.parse("sms:"));
        sendIntent.putExtra("sms_body", "~~:" + this.mstrAppIdentifier + ":~~\n\r" + GetMessageBody());
        startActivity(sendIntent);
    }

    /* access modifiers changed from: private */
    public void SendAsEmail(String[] strEmailAddress, String strSubject, String strMessage) {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("message/rfc822");
        i.putExtra("android.intent.extra.EMAIL", strEmailAddress);
        i.putExtra("android.intent.extra.SUBJECT", strSubject);
        i.putExtra("android.intent.extra.TEXT", strMessage);
        startActivity(Intent.createChooser(i, "Select email application."));
    }
}
