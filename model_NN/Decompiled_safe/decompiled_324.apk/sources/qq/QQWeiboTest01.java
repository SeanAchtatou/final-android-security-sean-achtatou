package qq;

public class QQWeiboTest01 {
    public static final int TYPE = 2;

    public String process() {
        String res = new QWeiboSyncApi().getRequestToken("7eea9d83f9014d938a8680578afee680", "e04e8f72e9f1cbd2ffbfa5fca1d61a84");
        System.out.println(res);
        String[] data = res.split("=");
        String oauth_token = data[1].split("&")[0];
        String oauth_token_secret = data[2].split("&")[0];
        System.out.println("oauth_token:" + oauth_token);
        System.out.println("oauth_token_secret:" + oauth_token_secret);
        System.out.println("https://open.t.qq.com/cgi-bin/authorize?oauth_token=" + oauth_token);
        return "https://open.t.qq.com/cgi-bin/authorize?oauth_token=" + oauth_token;
    }
}
