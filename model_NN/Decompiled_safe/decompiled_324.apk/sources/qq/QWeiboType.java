package qq;

public class QWeiboType {

    public enum PageFlag {
        PageFlag_First,
        PageFlag_Next,
        PageFlag_Last
    }

    public enum ResultType {
        ResultType_Xml,
        ResultType_Json
    }
}
