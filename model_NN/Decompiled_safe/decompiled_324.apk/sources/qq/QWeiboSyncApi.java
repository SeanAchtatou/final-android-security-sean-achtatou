package qq;

import com.kandian.videoplayer.LocalProxy;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import qq.QWeiboType;
import qq.api.OauthKey;
import qq.api.QParameter;
import qq.api.QWeiboRequest;

public class QWeiboSyncApi {
    public String getRequestToken(String customKey, String customSecret) {
        List<QParameter> parameters = new ArrayList<>();
        OauthKey oauthKey = new OauthKey();
        oauthKey.customKey = customKey;
        oauthKey.customSecrect = customSecret;
        oauthKey.callbackUrl = "null";
        try {
            return new QWeiboRequest().syncRequest("https://open.t.qq.com/cgi-bin/request_token", "GET", oauthKey, parameters, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getAccessToken(String customKey, String customSecret, String requestToken, String requestTokenSecrect, String verify) {
        List<QParameter> parameters = new ArrayList<>();
        OauthKey oauthKey = new OauthKey();
        oauthKey.customKey = customKey;
        oauthKey.customSecrect = customSecret;
        oauthKey.tokenKey = requestToken;
        oauthKey.tokenSecrect = requestTokenSecrect;
        oauthKey.verify = verify;
        try {
            return new QWeiboRequest().syncRequest("https://open.t.qq.com/cgi-bin/access_token", "GET", oauthKey, parameters, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getHomeMsg(String customKey, String customSecret, String requestToken, String requestTokenSecrect, QWeiboType.ResultType format, QWeiboType.PageFlag pageFlag, int nReqNum) {
        String strFormat;
        List<QParameter> parameters = new ArrayList<>();
        OauthKey oauthKey = new OauthKey();
        oauthKey.customKey = customKey;
        oauthKey.customSecrect = customSecret;
        oauthKey.tokenKey = requestToken;
        oauthKey.tokenSecrect = requestTokenSecrect;
        if (format == QWeiboType.ResultType.ResultType_Xml) {
            strFormat = "xml";
        } else if (format != QWeiboType.ResultType.ResultType_Json) {
            return "";
        } else {
            strFormat = "json";
        }
        parameters.add(new QParameter("format", strFormat));
        parameters.add(new QParameter("pageflag", String.valueOf(pageFlag.ordinal())));
        parameters.add(new QParameter("reqnum", String.valueOf(nReqNum)));
        String res = null;
        try {
            res = new QWeiboRequest().syncRequest("http://open.t.qq.com/api/statuses/home_timeline", "GET", oauthKey, parameters, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public String publishMsg(String customKey, String customSecret, String requestToken, String requestTokenSecrect, String content, String pic, QWeiboType.ResultType format) {
        String url;
        String strFormat;
        List<QParameter> files = new ArrayList<>();
        if (pic == null || pic.trim().equals("")) {
            url = "http://open.t.qq.com/api/t/add";
        } else {
            url = "http://open.t.qq.com/api/t/add_pic";
            files.add(new QParameter("pic", pic));
        }
        OauthKey oauthKey = new OauthKey();
        oauthKey.customKey = customKey;
        oauthKey.customSecrect = customSecret;
        oauthKey.tokenKey = requestToken;
        oauthKey.tokenSecrect = requestTokenSecrect;
        List<QParameter> parameters = new ArrayList<>();
        if (format == QWeiboType.ResultType.ResultType_Xml) {
            strFormat = "xml";
        } else if (format != QWeiboType.ResultType.ResultType_Json) {
            return "";
        } else {
            strFormat = "json";
        }
        parameters.add(new QParameter("format", strFormat));
        try {
            parameters.add(new QParameter("content", new String(content.getBytes(StringEncodings.UTF8))));
            parameters.add(new QParameter("clientip", LocalProxy.DEFAULT_PROXY_HOST));
            String res = null;
            try {
                res = new QWeiboRequest().syncRequest(url, "POST", oauthKey, parameters, files);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String createfriends(String customKey, String customSecret, String requestToken, String requestTokenSecrect, String friend, QWeiboType.ResultType format) {
        String strFormat;
        List<QParameter> files = new ArrayList<>();
        OauthKey oauthKey = new OauthKey();
        oauthKey.customKey = customKey;
        oauthKey.customSecrect = customSecret;
        oauthKey.tokenKey = requestToken;
        oauthKey.tokenSecrect = requestTokenSecrect;
        List<QParameter> parameters = new ArrayList<>();
        if (format == QWeiboType.ResultType.ResultType_Xml) {
            strFormat = "xml";
        } else if (format != QWeiboType.ResultType.ResultType_Json) {
            return "";
        } else {
            strFormat = "json";
        }
        parameters.add(new QParameter("format", strFormat));
        parameters.add(new QParameter("clientip", LocalProxy.DEFAULT_PROXY_HOST));
        parameters.add(new QParameter("name", friend));
        String res = null;
        try {
            res = new QWeiboRequest().syncRequest("http://open.t.qq.com/api/friends/add", "POST", oauthKey, parameters, files);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
