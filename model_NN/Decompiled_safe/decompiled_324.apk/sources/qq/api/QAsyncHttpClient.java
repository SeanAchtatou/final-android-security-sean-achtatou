package qq.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import qq.api.utils.QHttpUtil;

public class QAsyncHttpClient {
    private static final int CONNECTION_TIMEOUT = 20000;
    private ExecutorService mThreadPool = Executors.newFixedThreadPool(20);

    public boolean httpGet(String url, String queryString, QAsyncHandler callback, Object cookie) {
        if (url == null || url.equals("")) {
            return false;
        }
        if (queryString != null && !queryString.equals("")) {
            url = url + "?" + queryString;
        }
        GetMethod httpGet = new GetMethod(url);
        httpGet.getParams().setParameter("http.socket.timeout", new Integer(20000));
        this.mThreadPool.submit(new AsyncThread(httpGet, callback, cookie));
        return true;
    }

    public boolean httpPost(String url, String queryString, QAsyncHandler callback, Object cookie) {
        if (url == null || url.equals("")) {
            return false;
        }
        PostMethod httpPost = new PostMethod(url);
        httpPost.addParameter("Content-Type", PostMethod.FORM_URL_ENCODED_CONTENT_TYPE);
        httpPost.getParams().setParameter("http.socket.timeout", new Integer(20000));
        if (queryString != null && !queryString.equals("")) {
            httpPost.setRequestEntity(new ByteArrayRequestEntity(queryString.getBytes()));
        }
        this.mThreadPool.submit(new AsyncThread(httpPost, callback, cookie));
        return true;
    }

    public boolean httpPostWithFile(String url, String queryString, List<QParameter> files, QAsyncHandler callback, Object cookie) {
        FileNotFoundException e;
        if (url == null || url.equals("")) {
            return false;
        }
        PostMethod postMethod = new PostMethod(url + '?' + queryString);
        List<QParameter> listParams = QHttpUtil.getQueryParameters(queryString);
        Part[] parts = new Part[(listParams.size() + (files == null ? 0 : files.size()))];
        int i = 0;
        for (QParameter param : listParams) {
            parts[i] = new StringPart(param.mName, QHttpUtil.formParamDecode(param.mValue), StringEncodings.UTF8);
            i++;
        }
        try {
            Iterator i$ = files.iterator();
            while (true) {
                try {
                    int i2 = i;
                    if (i$.hasNext()) {
                        QParameter param2 = i$.next();
                        File file = new File(param2.mValue);
                        i = i2 + 1;
                        parts[i2] = new FilePart(param2.mName, file.getName(), file, QHttpUtil.getContentType(file), StringEncodings.UTF8);
                    } else {
                        postMethod.setRequestEntity(new MultipartRequestEntity(parts, postMethod.getParams()));
                        this.mThreadPool.submit(new AsyncThread(postMethod, callback, cookie));
                        return true;
                    }
                } catch (FileNotFoundException e2) {
                    e = e2;
                    e.printStackTrace();
                    return false;
                }
            }
        } catch (FileNotFoundException e3) {
            e = e3;
        }
    }

    class AsyncThread extends Thread {
        private QAsyncHandler mAsyncHandler;
        private Object mCookie;
        private HttpMethod mHttpMedthod;

        public AsyncThread(HttpMethod method, QAsyncHandler handler, Object cookie) {
            this.mHttpMedthod = method;
            this.mAsyncHandler = handler;
            this.mCookie = cookie;
        }

        public void run() {
            try {
                int statusCode = new HttpClient().executeMethod(this.mHttpMedthod);
                if (statusCode != 200) {
                    System.err.println("HttpMethod failed: " + this.mHttpMedthod.getStatusLine());
                }
                String responseData = this.mHttpMedthod.getResponseBodyAsString();
                this.mHttpMedthod.releaseConnection();
                if (this.mAsyncHandler != null) {
                    this.mAsyncHandler.onCompleted(statusCode, responseData, this.mCookie);
                }
            } catch (HttpException e) {
                HttpException e2 = e;
                e2.printStackTrace();
                if (this.mAsyncHandler != null) {
                    this.mAsyncHandler.onThrowable(e2, this.mCookie);
                }
                this.mHttpMedthod.releaseConnection();
            } catch (IOException e3) {
                IOException e4 = e3;
                e4.printStackTrace();
                if (this.mAsyncHandler != null) {
                    this.mAsyncHandler.onThrowable(e4, this.mCookie);
                }
                this.mHttpMedthod.releaseConnection();
            } catch (Throwable th) {
                this.mHttpMedthod.releaseConnection();
                throw th;
            }
        }
    }
}
