package qq.api;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import qq.api.utils.Base64Encoder;

public class OAuth implements Serializable {
    public static final String HMACSHA1SignatureType = "HmacSHA1";
    public static final String HMACSHA1SignatureType_TEXT = "HMAC-SHA1";
    public static final String OAuthCallbackKey = "oauth_callback";
    public static final String OAuthConsumerKeyKey = "oauth_consumer_key";
    public static final String OAuthNonceKey = "oauth_nonce";
    public static final String OAuthQParameterPrefix = "oauth_";
    public static final String OAuthSignatureKey = "oauth_signature";
    public static final String OAuthSignatureMethodKey = "oauth_signature_method";
    public static final String OAuthTimestampKey = "oauth_timestamp";
    public static final String OAuthTokenKey = "oauth_token";
    public static final String OAuthTokenSecretKey = "oauth_token_secret";
    public static final String OAuthVersion = "1.0";
    public static final String OAuthVersionKey = "oauth_version";
    public static final String oAauthVerifier = "oauth_verifier";
    static final long serialVersionUID = -8736851024315996L;
    public Random random = new Random();
    public String unreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

    public String getOauthUrl(String url, String httpMethod, String customKey, String customSecrect, String tokenKey, String tokenSecrect, String verify, String callbackUrl, List<QParameter> parameters, StringBuffer queryString) {
        String parameterString = normalizeRequestParameters(parameters);
        String urlWithQParameter = url;
        if (parameterString != null && !parameterString.equals("")) {
            urlWithQParameter = urlWithQParameter + "?" + parameterString;
        }
        URL aUrl = null;
        try {
            aUrl = new URL(urlWithQParameter);
        } catch (MalformedURLException e) {
            System.err.println("URL parse error:" + e.getLocalizedMessage());
        }
        String nonce = generateNonce();
        String timeStamp = generateTimeStamp();
        parameters.add(new QParameter(OAuthVersionKey, OAuthVersion));
        parameters.add(new QParameter(OAuthNonceKey, nonce));
        parameters.add(new QParameter(OAuthTimestampKey, timeStamp));
        parameters.add(new QParameter(OAuthSignatureMethodKey, HMACSHA1SignatureType_TEXT));
        parameters.add(new QParameter(OAuthConsumerKeyKey, customKey));
        if (tokenKey != null && !tokenKey.equals("")) {
            parameters.add(new QParameter(OAuthTokenKey, tokenKey));
        }
        if (verify != null && !verify.equals("")) {
            parameters.add(new QParameter(oAauthVerifier, verify));
        }
        if (callbackUrl != null && !callbackUrl.equals("")) {
            parameters.add(new QParameter(OAuthCallbackKey, callbackUrl));
        }
        StringBuffer normalizedUrl = new StringBuffer();
        String signature = generateSignature(aUrl, customSecrect, tokenSecrect, httpMethod, parameters, normalizedUrl, queryString);
        queryString.append("&oauth_signature=");
        queryString.append(encode(signature));
        return normalizedUrl.toString();
    }

    private String normalizeRequestParameters(List<QParameter> parameters) {
        StringBuffer sb = new StringBuffer();
        int size = parameters.size();
        for (int i = 0; i < size; i++) {
            QParameter p = parameters.get(i);
            sb.append(p.mName);
            sb.append("=");
            sb.append(p.mValue);
            if (i < size - 1) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    public String generateTimeStamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    public String generateNonce() {
        return String.valueOf(this.random.nextInt(9876599) + 123400);
    }

    public static String encode(String value) {
        String encoded = null;
        try {
            encoded = URLEncoder.encode(value, StringEncodings.UTF8);
        } catch (UnsupportedEncodingException e) {
        }
        StringBuffer buf = new StringBuffer(encoded.length());
        int i = 0;
        while (i < encoded.length()) {
            char focus = encoded.charAt(i);
            if (focus == '*') {
                buf.append("%2A");
            } else if (focus == '+') {
                buf.append("%20");
            } else if (focus == '%' && i + 1 < encoded.length() && encoded.charAt(i + 1) == '7' && encoded.charAt(i + 2) == 'E') {
                buf.append('~');
                i += 2;
            } else {
                buf.append(focus);
            }
            i++;
        }
        return buf.toString();
    }

    public String generateSignature(URL url, String consumerSecret, String tokenSecret, String httpMethod, List<QParameter> parameters, StringBuffer normalizedUrl, StringBuffer normalizedRequestParameters) {
        String signatureBase = generateSignatureBase(url, httpMethod, parameters, normalizedUrl, normalizedRequestParameters);
        byte[] oauthSignature = null;
        try {
            Mac mac = Mac.getInstance(HMACSHA1SignatureType);
            mac.init(new SecretKeySpec((encode(consumerSecret) + "&" + ((tokenSecret == null || tokenSecret.equals("")) ? "" : encode(tokenSecret))).getBytes("US-ASCII"), HMACSHA1SignatureType));
            oauthSignature = mac.doFinal(signatureBase.getBytes("US-ASCII"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e2) {
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
        }
        return Base64Encoder.encode(oauthSignature);
    }

    public String generateSignatureBase(URL url, String httpMethod, List<QParameter> parameters, StringBuffer normalizedUrl, StringBuffer normalizedRequestParameters) {
        Collections.sort(parameters);
        normalizedUrl.append(url.getProtocol());
        normalizedUrl.append("://");
        normalizedUrl.append(url.getHost());
        if ((url.getProtocol().equals("http") || url.getProtocol().equals("https")) && url.getPort() != -1) {
            normalizedUrl.append(":");
            normalizedUrl.append(url.getPort());
        }
        normalizedUrl.append(url.getPath());
        normalizedRequestParameters.append(formEncodeParameters(parameters));
        StringBuffer signatureBase = new StringBuffer();
        signatureBase.append(httpMethod.toUpperCase());
        signatureBase.append("&");
        signatureBase.append(encode(normalizedUrl.toString()));
        signatureBase.append("&");
        signatureBase.append(encode(normalizedRequestParameters.toString()));
        return signatureBase.toString();
    }

    private String formEncodeParameters(List<QParameter> parameters) {
        List<QParameter> encodeParams = new ArrayList<>();
        for (QParameter a : parameters) {
            encodeParams.add(new QParameter(a.mName, encode(a.mValue)));
        }
        return normalizeRequestParameters(encodeParams);
    }
}
