package qq.api;

import java.util.List;

public class QWeiboRequest {
    public String syncRequest(String url, String httpMethod, OauthKey key, List<QParameter> listParam, List<QParameter> listFile) throws Exception {
        if (url == null || url.equals("")) {
            return null;
        }
        OAuth oauth = new OAuth();
        StringBuffer sbQueryString = new StringBuffer();
        String oauthUrl = oauth.getOauthUrl(url, httpMethod, key.customKey, key.customSecrect, key.tokenKey, key.tokenSecrect, key.verify, key.callbackUrl, listParam, sbQueryString);
        String queryString = sbQueryString.toString();
        QHttpClient http = new QHttpClient();
        if ("GET".equals(httpMethod)) {
            return http.httpGet(oauthUrl, queryString);
        }
        if (listFile == null || listFile.size() == 0) {
            return http.httpPost(oauthUrl, queryString);
        }
        return http.httpPostWithFile(oauthUrl, queryString, listFile);
    }

    public boolean asyncRequest(String url, String httpMethod, OauthKey key, List<QParameter> listParam, List<QParameter> listFile, QAsyncHandler asyncHandler, Object cookie) {
        OAuth oauth = new OAuth();
        StringBuffer sbQueryString = new StringBuffer();
        String oauthUrl = oauth.getOauthUrl(url, httpMethod, key.customKey, key.customSecrect, key.tokenKey, key.tokenSecrect, key.verify, key.callbackUrl, listParam, sbQueryString);
        String queryString = sbQueryString.toString();
        QAsyncHttpClient asyncHttp = new QAsyncHttpClient();
        if ("GET".equals(httpMethod)) {
            return asyncHttp.httpGet(oauthUrl, queryString, asyncHandler, cookie);
        }
        if (listFile == null || listFile.size() == 0) {
            return asyncHttp.httpPost(oauthUrl, queryString, asyncHandler, cookie);
        }
        return asyncHttp.httpPostWithFile(oauthUrl, queryString, listFile, asyncHandler, cookie);
    }
}
