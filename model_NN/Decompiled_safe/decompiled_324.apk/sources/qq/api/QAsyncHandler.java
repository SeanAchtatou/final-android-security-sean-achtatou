package qq.api;

public interface QAsyncHandler {
    void onCompleted(int i, String str, Object obj);

    void onThrowable(Throwable th, Object obj);
}
