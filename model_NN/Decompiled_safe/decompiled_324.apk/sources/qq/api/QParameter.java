package qq.api;

import java.io.Serializable;

public class QParameter implements Serializable, Comparable {
    private static final long serialVersionUID = 5164951358145483848L;
    String mName;
    String mValue;

    public QParameter(String name, String value) {
        this.mName = name;
        this.mValue = value;
    }

    public boolean equals(Object arg0) {
        if (arg0 == null) {
            return false;
        }
        if (this == arg0) {
            return true;
        }
        if (!(arg0 instanceof QParameter)) {
            return false;
        }
        QParameter param = (QParameter) arg0;
        return this.mName.equals(param.mName) && this.mValue.equals(param.mValue);
    }

    public int compareTo(Object o) {
        QParameter param = (QParameter) o;
        int compared = this.mName.compareTo(param.mName);
        if (compared == 0) {
            return this.mValue.compareTo(param.mValue);
        }
        return compared;
    }
}
