package qq.api;

import java.io.File;
import java.util.List;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import qq.api.utils.QHttpUtil;

public class QHttpClient {
    public static final int CONNECTION_TIMEOUT = 20000;

    public String httpGet(String url, String queryString) throws Exception {
        if (queryString != null && !queryString.equals("")) {
            url = url + "?" + queryString;
        }
        System.out.println("url:" + url);
        HttpClient httpClient = new HttpClient();
        GetMethod httpGet = new GetMethod(url);
        httpGet.getParams().setParameter("http.socket.timeout", new Integer(20000));
        try {
            if (httpClient.executeMethod(httpGet) != 200) {
                System.err.println("HttpGet Method failed: " + httpGet.getStatusLine());
            }
            String responseData = httpGet.getResponseBodyAsString();
            httpGet.releaseConnection();
            return responseData;
        } catch (Exception e) {
            throw new Exception(e);
        } catch (Throwable th) {
            httpGet.releaseConnection();
            throw th;
        }
    }

    public String httpPost(String url, String queryString) throws Exception {
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        httpPost.addParameter("Content-Type", PostMethod.FORM_URL_ENCODED_CONTENT_TYPE);
        httpPost.getParams().setParameter("http.socket.timeout", new Integer(20000));
        if (queryString != null && !queryString.equals("")) {
            httpPost.setRequestEntity(new ByteArrayRequestEntity(queryString.getBytes()));
        }
        try {
            if (httpClient.executeMethod(httpPost) != 200) {
                System.err.println("HttpPost Method failed: " + httpPost.getStatusLine());
            }
            String responseData = httpPost.getResponseBodyAsString();
            httpPost.releaseConnection();
            return responseData;
        } catch (Exception e) {
            throw new Exception(e);
        } catch (Throwable th) {
            httpPost.releaseConnection();
            throw th;
        }
    }

    public String httpPostWithFile(String url, String queryString, List<QParameter> files) throws Exception {
        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = new PostMethod(url + '?' + queryString);
        try {
            List<QParameter> listParams = QHttpUtil.getQueryParameters(queryString);
            Part[] parts = new Part[(listParams.size() + (files == null ? 0 : files.size()))];
            int i = 0;
            for (QParameter param : listParams) {
                parts[i] = new StringPart(param.mName, QHttpUtil.formParamDecode(param.mValue), StringEncodings.UTF8);
                i++;
            }
            for (QParameter param2 : files) {
                File file = new File(param2.mValue);
                parts[i] = new FilePart(param2.mName, file.getName(), file, QHttpUtil.getContentType(file), StringEncodings.UTF8);
                i++;
            }
            postMethod.setRequestEntity(new MultipartRequestEntity(parts, postMethod.getParams()));
            if (httpClient.executeMethod(postMethod) != 200) {
                System.err.println("HttpPost Method failed: " + postMethod.getStatusLine());
            }
            String responseData = postMethod.getResponseBodyAsString();
            postMethod.releaseConnection();
            return responseData;
        } catch (Exception e) {
            throw new Exception(e);
        } catch (Throwable th) {
            postMethod.releaseConnection();
            throw th;
        }
    }
}
