package qq.api.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.activation.MimetypesFileTypeMap;
import qq.api.QParameter;

public class QHttpUtil {
    public static String getContentType(String fileName) {
        return new MimetypesFileTypeMap().getContentType(fileName);
    }

    public static String getContentType(File file) {
        return new MimetypesFileTypeMap().getContentType(file);
    }

    public static List<QParameter> getQueryParameters(String queryString) {
        if (queryString.startsWith("?")) {
            queryString = queryString.substring(1);
        }
        List<QParameter> result = new ArrayList<>();
        if (queryString != null && !queryString.equals("")) {
            for (String s : queryString.split("&")) {
                if (s != null && !s.equals("") && s.indexOf(61) > -1) {
                    String[] temp = s.split("=");
                    result.add(new QParameter(temp[0], temp[1]));
                }
            }
        }
        return result;
    }

    public static String formParamDecode(String value) {
        int index;
        int nCount = 0;
        int i = 0;
        while (i < value.length()) {
            if (value.charAt(i) == '%') {
                i += 2;
            }
            nCount++;
            i++;
        }
        byte[] sb = new byte[nCount];
        int i2 = 0;
        int index2 = 0;
        while (i2 < value.length()) {
            if (value.charAt(i2) != '%') {
                index = index2 + 1;
                sb[index2] = (byte) value.charAt(i2);
            } else {
                StringBuilder sChar = new StringBuilder();
                sChar.append(value.charAt(i2 + 1));
                sChar.append(value.charAt(i2 + 2));
                index = index2 + 1;
                sb[index2] = Integer.valueOf(sChar.toString(), 16).byteValue();
                i2 += 2;
            }
            index2 = index;
            i2++;
        }
        try {
            return new String(sb, StringEncodings.UTF8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
