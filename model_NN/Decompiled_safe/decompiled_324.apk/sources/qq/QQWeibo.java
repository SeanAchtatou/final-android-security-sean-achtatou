package qq;

import qq.QWeiboType;

public class QQWeibo {
    private String access_token;
    private String access_token_secret;
    private String customKey;
    private String customSecrect;
    private String oauth_token;
    private String oauth_token_secret;
    private String userId;
    private String verify;

    public QQWeibo(String customKey2, String customSecrect2) {
        this.customKey = customKey2;
        this.customSecrect = customSecrect2;
    }

    public String getRequestToken() {
        String res = new QWeiboSyncApi().getRequestToken(this.customKey, this.customSecrect);
        System.out.println("########## getRequestToken()=[" + res + "]");
        String[] data = res.split("=");
        this.oauth_token = data[1].split("&")[0];
        this.oauth_token_secret = data[2].split("&")[0];
        System.out.println("oauth_token:" + this.oauth_token);
        System.out.println("oauth_token_secret:" + this.oauth_token_secret);
        System.out.println("https://open.t.qq.com/cgi-bin/authorize?oauth_token=" + this.oauth_token);
        return "https://open.t.qq.com/cgi-bin/authorize?oauth_token=" + this.oauth_token;
    }

    public void getAccessToken(String verify2) {
        this.verify = verify2;
        String response = new QWeiboSyncApi().getAccessToken(this.customKey, this.customSecrect, this.oauth_token, this.oauth_token_secret, this.verify);
        System.out.println("########## getAccessToken()=[" + response + "]");
        String[] arr = response.split("&");
        this.access_token = arr[0].substring(arr[0].indexOf("=") + 1, arr[0].length());
        this.access_token_secret = arr[1].substring(arr[1].indexOf("=") + 1, arr[1].length());
        this.userId = arr[2].substring(arr[2].indexOf("=") + 1, arr[2].length());
        System.out.println("########## access_token=[" + this.access_token + "]");
        System.out.println("########## access_token_secret=[" + this.access_token_secret + "]");
        System.out.println("########## userId=[" + this.userId + "]");
    }

    public boolean publishMsg(String content, String pic) {
        String response = new QWeiboSyncApi().publishMsg(this.customKey, this.customSecrect, this.access_token, this.access_token_secret, content, pic, QWeiboType.ResultType.ResultType_Json);
        System.out.println(response);
        if (response == null || response.trim().length() == 0) {
            return false;
        }
        return response.indexOf("\"msg\":\"ok\"") > 0;
    }

    public boolean createfriends(String friend) {
        String response = new QWeiboSyncApi().createfriends(this.customKey, this.customSecrect, this.access_token, this.access_token_secret, friend, QWeiboType.ResultType.ResultType_Json);
        System.out.println(response);
        if (response == null || response.trim().length() == 0) {
            return false;
        }
        return response.indexOf("\"msg\":\"ok\"") > 0;
    }

    public String getCustomKey() {
        return this.customKey;
    }

    public void setCustomKey(String customKey2) {
        this.customKey = customKey2;
    }

    public String getCustomSecrect() {
        return this.customSecrect;
    }

    public void setCustomSecrect(String customSecrect2) {
        this.customSecrect = customSecrect2;
    }

    public String getOauth_token() {
        return this.oauth_token;
    }

    public void setOauth_token(String oauth_token2) {
        this.oauth_token = oauth_token2;
    }

    public String getOauth_token_secret() {
        return this.oauth_token_secret;
    }

    public void setOauth_token_secret(String oauth_token_secret2) {
        this.oauth_token_secret = oauth_token_secret2;
    }

    public String getVerify() {
        return this.verify;
    }

    public void setVerify(String verify2) {
        this.verify = verify2;
    }

    public String getAccess_token() {
        return this.access_token;
    }

    public void setAccess_token(String access_token2) {
        this.access_token = access_token2;
    }

    public String getAccess_token_secret() {
        return this.access_token_secret;
    }

    public void setAccess_token_secret(String access_token_secret2) {
        this.access_token_secret = access_token_secret2;
    }

    public String getUserCode() {
        return this.userId;
    }
}
