package javax.activation;

import java.io.File;

public abstract class FileTypeMap {
    static Class class$javax$activation$FileTypeMap;
    private static FileTypeMap defaultMap = null;

    public abstract String getContentType(File file);

    public abstract String getContentType(String str);

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public static void setDefaultFileTypeMap(FileTypeMap fileTypeMap) {
        Class class$;
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            try {
                securityManager.checkSetFactory();
            } catch (SecurityException e) {
                if (class$javax$activation$FileTypeMap != null) {
                    class$ = class$javax$activation$FileTypeMap;
                } else {
                    class$ = class$("javax.activation.FileTypeMap");
                    class$javax$activation$FileTypeMap = class$;
                }
                if (class$.getClassLoader() != fileTypeMap.getClass().getClassLoader()) {
                    throw e;
                }
            }
        }
        defaultMap = fileTypeMap;
    }

    public static FileTypeMap getDefaultFileTypeMap() {
        if (defaultMap == null) {
            defaultMap = new MimetypesFileTypeMap();
        }
        return defaultMap;
    }
}
