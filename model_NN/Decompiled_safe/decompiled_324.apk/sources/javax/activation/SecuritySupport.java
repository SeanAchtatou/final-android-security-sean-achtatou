package javax.activation;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

class SecuritySupport {
    private static final Object securitySupport;

    SecuritySupport() {
    }

    static {
        SecuritySupport securitySupport2 = null;
        try {
            Class.forName("java.security.AccessController");
            securitySupport2 = new SecuritySupport12();
        } catch (Exception e) {
        } catch (Throwable th) {
            if (0 == 0) {
                securitySupport2 = new SecuritySupport();
            }
            securitySupport = securitySupport2;
            throw th;
        }
        if (securitySupport2 == null) {
            securitySupport2 = new SecuritySupport();
        }
        securitySupport = securitySupport2;
    }

    public static SecuritySupport getInstance() {
        return (SecuritySupport) securitySupport;
    }

    public ClassLoader getContextClassLoader() {
        return null;
    }

    public InputStream getResourceAsStream(Class cls, String str) throws IOException {
        return cls.getResourceAsStream(str);
    }

    public URL[] getResources(ClassLoader classLoader, String str) {
        return null;
    }

    public URL[] getSystemResources(String str) {
        return null;
    }

    public InputStream openStream(URL url) throws IOException {
        return url.openStream();
    }
}
