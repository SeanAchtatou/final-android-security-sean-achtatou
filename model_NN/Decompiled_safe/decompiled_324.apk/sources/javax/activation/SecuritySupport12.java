package javax.activation;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Enumeration;
import java.util.Vector;

class SecuritySupport12 extends SecuritySupport {
    SecuritySupport12() {
    }

    public ClassLoader getContextClassLoader() {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                } catch (SecurityException e) {
                    return null;
                }
            }
        });
    }

    public InputStream getResourceAsStream(Class cls, String str) throws IOException {
        try {
            return (InputStream) AccessController.doPrivileged(new PrivilegedExceptionAction(cls, str) {
                private final Class val$c;
                private final String val$name;

                {
                    this.val$c = r1;
                    this.val$name = r2;
                }

                public Object run() throws IOException {
                    return this.val$c.getResourceAsStream(this.val$name);
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    public URL[] getResources(ClassLoader classLoader, String str) {
        return (URL[]) AccessController.doPrivileged(new PrivilegedAction(classLoader, str) {
            private final ClassLoader val$cl;
            private final String val$name;

            {
                this.val$cl = r1;
                this.val$name = r2;
            }

            public Object run() {
                try {
                    Vector vector = new Vector();
                    Enumeration<URL> resources = this.val$cl.getResources(this.val$name);
                    while (resources != null && resources.hasMoreElements()) {
                        URL nextElement = resources.nextElement();
                        if (nextElement != null) {
                            vector.addElement(nextElement);
                        }
                    }
                    if (vector.size() <= 0) {
                        return null;
                    }
                    URL[] urlArr = new URL[vector.size()];
                    vector.copyInto(urlArr);
                    return urlArr;
                } catch (IOException | SecurityException e) {
                    return null;
                }
            }
        });
    }

    public URL[] getSystemResources(String str) {
        return (URL[]) AccessController.doPrivileged(new PrivilegedAction(str) {
            private final String val$name;

            {
                this.val$name = r1;
            }

            public Object run() {
                try {
                    Vector vector = new Vector();
                    Enumeration<URL> systemResources = ClassLoader.getSystemResources(this.val$name);
                    while (systemResources != null && systemResources.hasMoreElements()) {
                        URL nextElement = systemResources.nextElement();
                        if (nextElement != null) {
                            vector.addElement(nextElement);
                        }
                    }
                    if (vector.size() <= 0) {
                        return null;
                    }
                    URL[] urlArr = new URL[vector.size()];
                    vector.copyInto(urlArr);
                    return urlArr;
                } catch (IOException | SecurityException e) {
                    return null;
                }
            }
        });
    }

    public InputStream openStream(URL url) throws IOException {
        try {
            return (InputStream) AccessController.doPrivileged(new PrivilegedExceptionAction(url) {
                private final URL val$url;

                {
                    this.val$url = r1;
                }

                public Object run() throws IOException {
                    return this.val$url.openStream();
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }
}
