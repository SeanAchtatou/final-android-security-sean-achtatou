package javax.activation;

import com.sun.activation.registries.MimeTypeFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public class MimetypesFileTypeMap extends FileTypeMap {
    private static final int PROG = 0;
    static Class class$javax$activation$MimetypesFileTypeMap;
    private static boolean debug;
    private static MimeTypeFile defDB = null;
    private static String defaultType = FilePart.DEFAULT_CONTENT_TYPE;
    private MimeTypeFile[] DB;

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    static {
        debug = false;
        try {
            debug = Boolean.getBoolean("javax.activation.debug");
        } catch (Throwable th) {
        }
    }

    public MimetypesFileTypeMap() {
        Class class$;
        MimeTypeFile loadFile;
        Vector vector = new Vector(5);
        vector.addElement(null);
        if (debug) {
            System.out.println("MimetypesFileTypeMap: load HOME");
        }
        try {
            String property = System.getProperty("user.home");
            if (!(property == null || (loadFile = loadFile(new StringBuffer(String.valueOf(property)).append(File.separator).append(".mime.types").toString())) == null)) {
                vector.addElement(loadFile);
            }
        } catch (SecurityException e) {
        }
        if (debug) {
            System.out.println("MimetypesFileTypeMap: load SYS");
        }
        try {
            MimeTypeFile loadFile2 = loadFile(new StringBuffer(String.valueOf(System.getProperty("java.home"))).append(File.separator).append("lib").append(File.separator).append("mime.types").toString());
            if (loadFile2 != null) {
                vector.addElement(loadFile2);
            }
        } catch (SecurityException e2) {
        }
        if (debug) {
            System.out.println("MimetypesFileTypeMap: load JAR");
        }
        loadAllResources(vector, "META-INF/mime.types");
        if (debug) {
            System.out.println("MimetypesFileTypeMap: load DEF");
        }
        if (class$javax$activation$MimetypesFileTypeMap != null) {
            class$ = class$javax$activation$MimetypesFileTypeMap;
        } else {
            class$ = class$("javax.activation.MimetypesFileTypeMap");
            class$javax$activation$MimetypesFileTypeMap = class$;
        }
        synchronized (class$) {
            if (defDB == null) {
                defDB = loadResource("/META-INF/mimetypes.default");
            }
        }
        if (defDB != null) {
            vector.addElement(defDB);
        }
        this.DB = new MimeTypeFile[vector.size()];
        vector.copyInto(this.DB);
    }

    private static final void pr(String str) {
        System.out.println(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0049 A[SYNTHETIC, Splitter:B:17:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0071 A[Catch:{ IOException -> 0x004e, SecurityException -> 0x006b, all -> 0x0085, all -> 0x0098 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0089 A[SYNTHETIC, Splitter:B:35:0x0089] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.sun.activation.registries.MimeTypeFile loadResource(java.lang.String r7) {
        /*
            r6 = this;
            r4 = 0
            javax.activation.SecuritySupport r0 = javax.activation.SecuritySupport.getInstance()     // Catch:{ IOException -> 0x004e, SecurityException -> 0x006b, all -> 0x0085 }
            java.lang.Class r1 = r6.getClass()     // Catch:{ IOException -> 0x004e, SecurityException -> 0x006b, all -> 0x0085 }
            java.io.InputStream r0 = r0.getResourceAsStream(r1, r7)     // Catch:{ IOException -> 0x004e, SecurityException -> 0x006b, all -> 0x0085 }
            if (r0 == 0) goto L_0x0031
            com.sun.activation.registries.MimeTypeFile r1 = new com.sun.activation.registries.MimeTypeFile     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            boolean r2 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            if (r2 == 0) goto L_0x002a
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            java.lang.String r3 = "MimetypesFileTypeMap: successfully loaded mime types file: "
            r2.<init>(r3)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            java.lang.StringBuffer r2 = r2.append(r7)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            pr(r2)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
        L_0x002a:
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x0091 }
        L_0x002f:
            r0 = r1
        L_0x0030:
            return r0
        L_0x0031:
            boolean r1 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            if (r1 == 0) goto L_0x0047
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            java.lang.String r2 = "MimetypesFileTypeMap: not loading mime types file: "
            r1.<init>(r2)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            java.lang.StringBuffer r1 = r1.append(r7)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
            pr(r1)     // Catch:{ IOException -> 0x009f, SecurityException -> 0x009a, all -> 0x0093 }
        L_0x0047:
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ IOException -> 0x008f }
        L_0x004c:
            r0 = r4
            goto L_0x0030
        L_0x004e:
            r0 = move-exception
            r1 = r4
        L_0x0050:
            boolean r2 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x00a4
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x0098 }
            java.lang.String r3 = "MimetypesFileTypeMap: "
            r2.<init>(r3)     // Catch:{ all -> 0x0098 }
            java.lang.StringBuffer r2 = r2.append(r0)     // Catch:{ all -> 0x0098 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0098 }
            pr(r2)     // Catch:{ all -> 0x0098 }
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            r0 = r1
            goto L_0x0047
        L_0x006b:
            r0 = move-exception
            r1 = r4
        L_0x006d:
            boolean r2 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x00a4
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x0098 }
            java.lang.String r3 = "MimetypesFileTypeMap: "
            r2.<init>(r3)     // Catch:{ all -> 0x0098 }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ all -> 0x0098 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0098 }
            pr(r0)     // Catch:{ all -> 0x0098 }
            r0 = r1
            goto L_0x0047
        L_0x0085:
            r0 = move-exception
            r1 = r4
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ IOException -> 0x008d }
        L_0x008c:
            throw r0
        L_0x008d:
            r1 = move-exception
            goto L_0x008c
        L_0x008f:
            r0 = move-exception
            goto L_0x004c
        L_0x0091:
            r0 = move-exception
            goto L_0x002f
        L_0x0093:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0087
        L_0x0098:
            r0 = move-exception
            goto L_0x0087
        L_0x009a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x006d
        L_0x009f:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0050
        L_0x00a4:
            r0 = r1
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.activation.MimetypesFileTypeMap.loadResource(java.lang.String):com.sun.activation.registries.MimeTypeFile");
    }

    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARN: Type inference failed for: r2v16 */
    /* JADX WARN: Type inference failed for: r2v18, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v10, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v22 */
    /* JADX WARN: Type inference failed for: r1v12 */
    /* JADX WARN: Type inference failed for: r2v25 */
    /* JADX WARN: Type inference failed for: r2v27 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a0 A[SYNTHETIC, Splitter:B:36:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e7 A[Catch:{ IOException -> 0x00c0, SecurityException -> 0x00de, all -> 0x00fc, all -> 0x0129 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0100 A[SYNTHETIC, Splitter:B:59:0x0100] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x00a3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:89:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadAllResources(java.util.Vector r9, java.lang.String r10) {
        /*
            r8 = this;
            r2 = 0
            javax.activation.SecuritySupport r0 = javax.activation.SecuritySupport.getInstance()     // Catch:{ Exception -> 0x0104 }
            java.lang.ClassLoader r0 = r0.getContextClassLoader()     // Catch:{ Exception -> 0x0104 }
            if (r0 != 0) goto L_0x0013
            java.lang.Class r0 = r8.getClass()     // Catch:{ Exception -> 0x0104 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ Exception -> 0x0104 }
        L_0x0013:
            if (r0 == 0) goto L_0x0051
            javax.activation.SecuritySupport r1 = javax.activation.SecuritySupport.getInstance()     // Catch:{ Exception -> 0x0104 }
            java.net.URL[] r0 = r1.getResources(r0, r10)     // Catch:{ Exception -> 0x0104 }
        L_0x001d:
            if (r0 == 0) goto L_0x0141
            boolean r1 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ Exception -> 0x0104 }
            if (r1 == 0) goto L_0x0028
            java.lang.String r1 = "MimetypesFileTypeMap: getResources"
            pr(r1)     // Catch:{ Exception -> 0x0104 }
        L_0x0028:
            r1 = r2
        L_0x0029:
            int r3 = r0.length     // Catch:{ Exception -> 0x0104 }
            if (r1 < r3) goto L_0x005a
            r0 = r2
        L_0x002d:
            if (r0 != 0) goto L_0x0050
            boolean r0 = javax.activation.MimetypesFileTypeMap.debug
            if (r0 == 0) goto L_0x0038
            java.lang.String r0 = "MimetypesFileTypeMap: !anyLoaded"
            pr(r0)
        L_0x0038:
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            java.lang.String r1 = "/"
            r0.<init>(r1)
            java.lang.StringBuffer r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            com.sun.activation.registries.MimeTypeFile r0 = r8.loadResource(r0)
            if (r0 == 0) goto L_0x0050
            r9.addElement(r0)
        L_0x0050:
            return
        L_0x0051:
            javax.activation.SecuritySupport r0 = javax.activation.SecuritySupport.getInstance()     // Catch:{ Exception -> 0x0104 }
            java.net.URL[] r0 = r0.getSystemResources(r10)     // Catch:{ Exception -> 0x0104 }
            goto L_0x001d
        L_0x005a:
            r3 = r0[r1]     // Catch:{ Exception -> 0x0104 }
            r4 = 0
            boolean r5 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ Exception -> 0x0104 }
            if (r5 == 0) goto L_0x0073
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0104 }
            java.lang.String r6 = "MimetypesFileTypeMap: URL "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0104 }
            java.lang.StringBuffer r5 = r5.append(r3)     // Catch:{ Exception -> 0x0104 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0104 }
            pr(r5)     // Catch:{ Exception -> 0x0104 }
        L_0x0073:
            javax.activation.SecuritySupport r5 = javax.activation.SecuritySupport.getInstance()     // Catch:{ IOException -> 0x00c0, SecurityException -> 0x00de, all -> 0x00fc }
            java.io.InputStream r4 = r5.openStream(r3)     // Catch:{ IOException -> 0x00c0, SecurityException -> 0x00de, all -> 0x00fc }
            if (r4 == 0) goto L_0x00a7
            com.sun.activation.registries.MimeTypeFile r5 = new com.sun.activation.registries.MimeTypeFile     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            r9.addElement(r5)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            r2 = 1
            boolean r5 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            if (r5 == 0) goto L_0x013d
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            java.lang.String r6 = "MimetypesFileTypeMap: successfully loaded mime types from URL: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            java.lang.StringBuffer r3 = r5.append(r3)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            pr(r3)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            r3 = r2
            r2 = r4
        L_0x009e:
            if (r2 == 0) goto L_0x00a3
            r2.close()     // Catch:{ IOException -> 0x0121, Exception -> 0x0123 }
        L_0x00a3:
            int r1 = r1 + 1
            r2 = r3
            goto L_0x0029
        L_0x00a7:
            boolean r5 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            if (r5 == 0) goto L_0x013d
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            java.lang.String r6 = "MimetypesFileTypeMap: not loading mime types from URL: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            java.lang.StringBuffer r3 = r5.append(r3)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            pr(r3)     // Catch:{ IOException -> 0x0133, SecurityException -> 0x012d, all -> 0x0126 }
            r3 = r2
            r2 = r4
            goto L_0x009e
        L_0x00c0:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
        L_0x00c5:
            boolean r5 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ all -> 0x0129 }
            if (r5 == 0) goto L_0x0139
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ all -> 0x0129 }
            java.lang.String r6 = "MimetypesFileTypeMap: "
            r5.<init>(r6)     // Catch:{ all -> 0x0129 }
            java.lang.StringBuffer r2 = r5.append(r2)     // Catch:{ all -> 0x0129 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0129 }
            pr(r2)     // Catch:{ all -> 0x0129 }
            r2 = r3
            r3 = r4
            goto L_0x009e
        L_0x00de:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
        L_0x00e3:
            boolean r5 = javax.activation.MimetypesFileTypeMap.debug     // Catch:{ all -> 0x0129 }
            if (r5 == 0) goto L_0x0139
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ all -> 0x0129 }
            java.lang.String r6 = "MimetypesFileTypeMap: "
            r5.<init>(r6)     // Catch:{ all -> 0x0129 }
            java.lang.StringBuffer r2 = r5.append(r2)     // Catch:{ all -> 0x0129 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0129 }
            pr(r2)     // Catch:{ all -> 0x0129 }
            r2 = r3
            r3 = r4
            goto L_0x009e
        L_0x00fc:
            r0 = move-exception
            r1 = r4
        L_0x00fe:
            if (r1 == 0) goto L_0x0103
            r1.close()     // Catch:{ IOException -> 0x011f }
        L_0x0103:
            throw r0     // Catch:{ Exception -> 0x0104 }
        L_0x0104:
            r0 = move-exception
            r1 = r2
        L_0x0106:
            boolean r2 = javax.activation.MimetypesFileTypeMap.debug
            if (r2 == 0) goto L_0x011c
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "MimetypesFileTypeMap: "
            r2.<init>(r3)
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            pr(r0)
        L_0x011c:
            r0 = r1
            goto L_0x002d
        L_0x011f:
            r1 = move-exception
            goto L_0x0103
        L_0x0121:
            r2 = move-exception
            goto L_0x00a3
        L_0x0123:
            r0 = move-exception
            r1 = r3
            goto L_0x0106
        L_0x0126:
            r0 = move-exception
            r1 = r4
            goto L_0x00fe
        L_0x0129:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x00fe
        L_0x012d:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
            goto L_0x00e3
        L_0x0133:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
            goto L_0x00c5
        L_0x0139:
            r2 = r3
            r3 = r4
            goto L_0x009e
        L_0x013d:
            r3 = r2
            r2 = r4
            goto L_0x009e
        L_0x0141:
            r0 = r2
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.activation.MimetypesFileTypeMap.loadAllResources(java.util.Vector, java.lang.String):void");
    }

    private MimeTypeFile loadFile(String str) {
        try {
            return new MimeTypeFile(str);
        } catch (IOException e) {
            return null;
        }
    }

    public MimetypesFileTypeMap(String str) throws IOException {
        this();
        this.DB[0] = new MimeTypeFile(str);
    }

    public MimetypesFileTypeMap(InputStream inputStream) {
        this();
        try {
            this.DB[0] = new MimeTypeFile(inputStream);
        } catch (IOException e) {
        }
    }

    public synchronized void addMimeTypes(String str) {
        if (this.DB[0] == null) {
            this.DB[0] = new MimeTypeFile();
        }
        this.DB[0].appendToRegistry(str);
    }

    public String getContentType(File file) {
        return getContentType(file.getName());
    }

    public synchronized String getContentType(String str) {
        String str2;
        String mIMETypeString;
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf >= 0) {
            String substring = str.substring(lastIndexOf + 1);
            if (substring.length() != 0) {
                int i = 0;
                while (true) {
                    if (i < this.DB.length) {
                        if (this.DB[i] != null && (mIMETypeString = this.DB[i].getMIMETypeString(substring)) != null) {
                            str2 = mIMETypeString;
                            break;
                        }
                        i++;
                    } else {
                        str2 = defaultType;
                        break;
                    }
                }
            } else {
                str2 = defaultType;
            }
        } else {
            str2 = defaultType;
        }
        return str2;
    }
}
