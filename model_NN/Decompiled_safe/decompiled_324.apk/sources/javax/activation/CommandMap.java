package javax.activation;

public abstract class CommandMap {
    static Class class$javax$activation$CommandMap;
    private static CommandMap defaultCommandMap = null;

    public abstract DataContentHandler createDataContentHandler(String str);

    public abstract CommandInfo[] getAllCommands(String str);

    public abstract CommandInfo getCommand(String str, String str2);

    public abstract CommandInfo[] getPreferredCommands(String str);

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public static CommandMap getDefaultCommandMap() {
        if (defaultCommandMap == null) {
            defaultCommandMap = new MailcapCommandMap();
        }
        return defaultCommandMap;
    }

    public static void setDefaultCommandMap(CommandMap commandMap) {
        Class class$;
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            try {
                securityManager.checkSetFactory();
            } catch (SecurityException e) {
                if (class$javax$activation$CommandMap != null) {
                    class$ = class$javax$activation$CommandMap;
                } else {
                    class$ = class$("javax.activation.CommandMap");
                    class$javax$activation$CommandMap = class$;
                }
                if (class$.getClassLoader() != commandMap.getClass().getClassLoader()) {
                    throw e;
                }
            }
        }
        defaultCommandMap = commandMap;
    }
}
