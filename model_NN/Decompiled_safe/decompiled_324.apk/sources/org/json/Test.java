package org.json;

import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.cookie.Cookie2;

public class Test {
    /* JADX INFO: Multiple debug info for r6v121 org.json.JSONObject: [D('i' int), D('j' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r6v168 org.json.JSONObject: [D('j' org.json.JSONObject), D('s' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v171 org.json.JSONObject: [D('j' org.json.JSONObject), D('s' java.lang.String)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.util.Collection):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.util.Map):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.util.Collection):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.util.Map):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.util.Collection):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, java.util.Map):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject
      org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject */
    public static void main(String[] args) {
        AnonymousClass1Obj obj = new JSONString("A string, a number, and a boolean", 42.0d, true) {
            public boolean aBoolean;
            public double aNumber;
            public String aString;

            {
                this.aString = string;
                this.aNumber = n;
                this.aBoolean = b;
            }

            public String toJSONString() {
                return new StringBuffer().append("{").append(JSONObject.quote(this.aString)).append(":").append(JSONObject.doubleToString(this.aNumber)).append("}").toString();
            }

            public String toString() {
                return new StringBuffer().append(this.aString).append(" ").append(this.aNumber).append(" ").append(this.aBoolean).toString();
            }
        };
        System.out.println(XML.toJSONObject("<![CDATA[This is a collection of test patterns and examples for org.json.]]>").toString());
        String s = new JSONStringer().object().key("foo").value("bar").key("baz").array().object().key("quux").value("Thanks, Josh!").endObject().endArray().endObject().toString();
        System.out.println(s);
        System.out.println(new JSONStringer().object().key("a").array().array().array().value("b").endArray().endArray().endArray().endObject().toString());
        JSONStringer jj = new JSONStringer();
        jj.array();
        jj.value(1L);
        jj.array();
        jj.value((Object) null);
        jj.array();
        jj.object();
        jj.key("empty-array").array().endArray();
        jj.key("answer").value(42L);
        jj.key("null").value((Object) null);
        jj.key(HttpState.PREEMPTIVE_DEFAULT).value(false);
        jj.key("true").value(true);
        jj.key("big").value(1.23456789E96d);
        jj.key("small").value(1.23456789E-80d);
        jj.key("empty-object").object().endObject();
        jj.key("long");
        jj.value(Long.MAX_VALUE);
        jj.endObject();
        jj.value("two");
        jj.endArray();
        jj.value(true);
        jj.endArray();
        jj.value(98.6d);
        jj.value(-100.0d);
        jj.object();
        jj.endObject();
        jj.object();
        jj.key("one");
        jj.value(1.0d);
        jj.endObject();
        jj.value(obj);
        jj.endArray();
        System.out.println(jj.toString());
        System.out.println(new JSONArray(jj.toString()).toString(4));
        JSONObject j = new JSONObject(obj, new String[]{"aString", "aNumber", "aBoolean"});
        j.put("test", obj);
        j.put(Cookie2.COMMENT, "This object contains a test object that implements JSONString");
        System.out.println(j.toString(4));
        JSONObject j2 = new JSONObject("{slashes: '///', closetag: '</script>', backslash:'\\\\', ei: {quotes: '\"\\''},eo: {a: '\"quoted\"', b:\"don't\"}, quotes: [\"'\", '\"']}");
        System.out.println(j2.toString(2));
        System.out.println(XML.toString(j2));
        System.out.println("");
        JSONObject j3 = new JSONObject("/*comment*/{foo: [true, false,9876543210,    0.0, 1.00000001,  1.000000000001, 1.00000000000000001, .00000000000000001, 2.00, 0.1, 2e100, -32,[],{}, \"string\"],   to   : null, op : 'Good',ten:10} postfix comment");
        j3.put("String", "98.6");
        j3.put("JSONObject", new JSONObject());
        j3.put("JSONArray", new JSONArray());
        j3.put("int", 57);
        j3.put("double", 1.2345678901234568E29d);
        j3.put("true", true);
        j3.put(HttpState.PREEMPTIVE_DEFAULT, false);
        j3.put("null", JSONObject.NULL);
        j3.put("bool", "true");
        j3.put("zero", -0.0d);
        JSONArray a = j3.getJSONArray("foo");
        a.put(666);
        a.put(2001.99d);
        a.put("so \"fine\".");
        a.put("so <fine>.");
        a.put(true);
        a.put(false);
        a.put(new JSONArray());
        a.put(new JSONObject());
        System.out.println(j3.toString(4));
        System.out.println(XML.toString(j3));
        System.out.println(new StringBuffer().append("String: ").append(j3.getDouble("String")).toString());
        System.out.println(new StringBuffer().append("  bool: ").append(j3.getBoolean("bool")).toString());
        System.out.println(new StringBuffer().append("    to: ").append(j3.getString("to")).toString());
        System.out.println(new StringBuffer().append("  true: ").append(j3.getString("true")).toString());
        System.out.println(new StringBuffer().append("   foo: ").append(j3.getJSONArray("foo")).toString());
        System.out.println(new StringBuffer().append("    op: ").append(j3.getString("op")).toString());
        System.out.println(new StringBuffer().append("   ten: ").append(j3.getInt("ten")).toString());
        System.out.println(new StringBuffer().append("  oops: ").append(j3.optBoolean("oops")).toString());
        JSONObject j4 = XML.toJSONObject("<xml one = 1 two=' \"2\" '><five></five>First \t&lt;content&gt;<five></five> This is \"content\". <three>  3  </three>JSON does not preserve the sequencing of elements and contents.<three>  III  </three>  <three>  T H R E E</three><four/>Content text is an implied structure in XML. <six content=\"6\"/>JSON does not have implied structure:<seven>7</seven>everything is explicit.<![CDATA[CDATA blocks<are><supported>!]]></xml>");
        System.out.println(j4.toString(2));
        System.out.println(XML.toString(j4));
        System.out.println("");
        JSONObject j5 = XML.toJSONObject("<mapping><empty/>   <class name = \"Customer\">      <field name = \"ID\" type = \"string\">         <bind-xml name=\"ID\" node=\"attribute\"/>      </field>      <field name = \"FirstName\" type = \"FirstName\"/>      <field name = \"MI\" type = \"MI\"/>      <field name = \"LastName\" type = \"LastName\"/>   </class>   <class name = \"FirstName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"MI\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"LastName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class></mapping>");
        System.out.println(j5.toString(2));
        System.out.println(XML.toString(j5));
        System.out.println("");
        JSONObject j6 = XML.toJSONObject("<?xml version=\"1.0\" ?><Book Author=\"Anonymous\"><Title>Sample Book</Title><Chapter id=\"1\">This is chapter 1. It is not very long or interesting.</Chapter><Chapter id=\"2\">This is chapter 2. Although it is longer than chapter 1, it is not any more interesting.</Chapter></Book>");
        System.out.println(j6.toString(2));
        System.out.println(XML.toString(j6));
        System.out.println("");
        JSONObject j7 = XML.toJSONObject("<!DOCTYPE bCard 'http://www.cs.caltech.edu/~adam/schemas/bCard'><bCard><?xml default bCard        firstname = ''        lastname  = '' company   = '' email = '' homepage  = ''?><bCard        firstname = 'Rohit'        lastname  = 'Khare'        company   = 'MCI'        email     = 'khare@mci.net'        homepage  = 'http://pest.w3.org/'/><bCard        firstname = 'Adam'        lastname  = 'Rifkin'        company   = 'Caltech Infospheres Project'        email     = 'adam@cs.caltech.edu'        homepage  = 'http://www.cs.caltech.edu/~adam/'/></bCard>");
        System.out.println(j7.toString(2));
        System.out.println(XML.toString(j7));
        System.out.println("");
        JSONObject j8 = XML.toJSONObject("<?xml version=\"1.0\"?><customer>    <firstName>        <text>Fred</text>    </firstName>    <ID>fbs0001</ID>    <lastName> <text>Scerbo</text>    </lastName>    <MI>        <text>B</text>    </MI></customer>");
        System.out.println(j8.toString(2));
        System.out.println(XML.toString(j8));
        System.out.println("");
        JSONObject j9 = XML.toJSONObject("<!ENTITY tp-address PUBLIC '-//ABC University::Special Collections Library//TEXT (titlepage: name and address)//EN' 'tpspcoll.sgm'><list type='simple'><head>Repository Address </head><item>Special Collections Library</item><item>ABC University</item><item>Main Library, 40 Circle Drive</item><item>Ourtown, Pennsylvania</item><item>17654 USA</item></list>");
        System.out.println(j9.toString());
        System.out.println(XML.toString(j9));
        System.out.println("");
        JSONObject j10 = XML.toJSONObject("<test intertag status=ok><empty/>deluxe<blip sweet=true>&amp;&quot;toot&quot;&toot;&#x41;</blip><x>eks</x><w>bonus</w><w>bonus2</w></test>");
        System.out.println(j10.toString(2));
        System.out.println(XML.toString(j10));
        System.out.println("");
        JSONObject j11 = HTTP.toJSONObject("GET / HTTP/1.0\nAccept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, */*\nAccept-Language: en-us\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5; Windows 98; Win 9x 4.90; T312461; Q312461)\nHost: www.nokko.com\nConnection: keep-alive\nAccept-encoding: gzip, deflate\n");
        System.out.println(j11.toString(2));
        System.out.println(HTTP.toString(j11));
        System.out.println("");
        JSONObject j12 = HTTP.toJSONObject("HTTP/1.1 200 Oki Doki\nDate: Sun, 26 May 2002 17:38:52 GMT\nServer: Apache/1.3.23 (Unix) mod_perl/1.26\nKeep-Alive: timeout=15, max=100\nConnection: Keep-Alive\nTransfer-Encoding: chunked\nContent-Type: text/html\n");
        System.out.println(j12.toString(2));
        System.out.println(HTTP.toString(j12));
        System.out.println("");
        JSONObject j13 = new JSONObject("{nix: null, nux: false, null: 'null', 'Request-URI': '/', Method: 'GET', 'HTTP-Version': 'HTTP/1.0'}");
        System.out.println(j13.toString(2));
        System.out.println(new StringBuffer().append("isNull: ").append(j13.isNull("nix")).toString());
        System.out.println(new StringBuffer().append("   has: ").append(j13.has("nix")).toString());
        System.out.println(XML.toString(j13));
        System.out.println(HTTP.toString(j13));
        System.out.println("");
        JSONObject j14 = XML.toJSONObject("<?xml version='1.0' encoding='UTF-8'?>\n\n<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\"><SOAP-ENV:Body><ns1:doGoogleSearch xmlns:ns1=\"urn:GoogleSearch\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><key xsi:type=\"xsd:string\">GOOGLEKEY</key> <q xsi:type=\"xsd:string\">'+search+'</q> <start xsi:type=\"xsd:int\">0</start> <maxResults xsi:type=\"xsd:int\">10</maxResults> <filter xsi:type=\"xsd:boolean\">true</filter> <restrict xsi:type=\"xsd:string\"></restrict> <safeSearch xsi:type=\"xsd:boolean\">false</safeSearch> <lr xsi:type=\"xsd:string\"></lr> <ie xsi:type=\"xsd:string\">latin1</ie> <oe xsi:type=\"xsd:string\">latin1</oe></ns1:doGoogleSearch></SOAP-ENV:Body></SOAP-ENV:Envelope>");
        System.out.println(j14.toString(2));
        System.out.println(XML.toString(j14));
        System.out.println("");
        JSONObject j15 = new JSONObject("{Envelope: {Body: {\"ns1:doGoogleSearch\": {oe: \"latin1\", filter: true, q: \"'+search+'\", key: \"GOOGLEKEY\", maxResults: 10, \"SOAP-ENV:encodingStyle\": \"http://schemas.xmlsoap.org/soap/encoding/\", start: 0, ie: \"latin1\", safeSearch:false, \"xmlns:ns1\": \"urn:GoogleSearch\"}}}}");
        System.out.println(j15.toString(2));
        System.out.println(XML.toString(j15));
        System.out.println("");
        JSONObject j16 = CookieList.toJSONObject("  f%oo = b+l=ah  ; o;n%40e = t.wo ");
        System.out.println(j16.toString(2));
        System.out.println(CookieList.toString(j16));
        System.out.println("");
        JSONObject j17 = Cookie.toJSONObject("f%oo=blah; secure ;expires = April 24, 2002");
        System.out.println(j17.toString(2));
        System.out.println(Cookie.toString(j17));
        System.out.println("");
        System.out.println(new JSONObject("{script: 'It is not allowed in HTML to send a close script tag in a string<script>because it confuses browsers</script>so we insert a backslash before the /'}").toString());
        System.out.println("");
        JSONTokener jt = new JSONTokener("{op:'test', to:'session', pre:1}{op:'test', to:'session', pre:2}");
        JSONObject j18 = new JSONObject(jt);
        System.out.println(j18.toString());
        System.out.println(new StringBuffer().append("pre: ").append(j18.optInt("pre")).toString());
        System.out.println(jt.skipTo('{'));
        System.out.println(new JSONObject(jt).toString());
        System.out.println("");
        JSONArray a2 = CDL.toJSONArray("No quotes, 'Single Quotes', \"Double Quotes\"\n1,'2',\"3\"\n,'It is \"good,\"', \"It works.\"\n\n");
        System.out.println(CDL.toString(a2));
        System.out.println("");
        System.out.println(a2.toString(4));
        System.out.println("");
        JSONArray a3 = new JSONArray(" [\"<escape>\", next is an implied null , , ok,] ");
        System.out.println(a3.toString());
        System.out.println("");
        System.out.println(XML.toString(a3));
        System.out.println("");
        JSONObject j19 = new JSONObject("{ fun => with non-standard forms ; forgiving => This package can be used to parse formats that are similar to but not stricting conforming to JSON; why=To make it easier to migrate existing data to JSON,one = [[1.00]]; uno=[[{1=>1}]];'+':+6e66 ;pluses=+++;empty = '' , 'double':0.666,true: TRUE, false: FALSE, null=NULL;[true] = [[!,@;*]]; string=>  o. k. ; # comment\r oct=0666; hex=0x666; dec=666; o=0999; noh=0x0x}");
        System.out.println(j19.toString(4));
        System.out.println("");
        if (j19.getBoolean("true") && !j19.getBoolean(HttpState.PREEMPTIVE_DEFAULT)) {
            System.out.println("It's all good");
        }
        System.out.println("");
        JSONObject j20 = new JSONObject(j19, new String[]{"dec", "oct", "hex", "missing"});
        System.out.println(j20.toString(4));
        System.out.println("");
        System.out.println(new JSONStringer().array().value(a3).value(j20).endArray());
        JSONObject j21 = new JSONObject("{string: \"98.6\", long: 2147483648, int: 2147483647, longer: 9223372036854775807, double: 9223372036854775808}");
        System.out.println(j21.toString(4));
        System.out.println("\ngetInt");
        System.out.println(new StringBuffer().append("int    ").append(j21.getInt("int")).toString());
        System.out.println(new StringBuffer().append("long   ").append(j21.getInt("long")).toString());
        System.out.println(new StringBuffer().append("longer ").append(j21.getInt("longer")).toString());
        System.out.println(new StringBuffer().append("double ").append(j21.getInt("double")).toString());
        System.out.println(new StringBuffer().append("string ").append(j21.getInt("string")).toString());
        System.out.println("\ngetLong");
        System.out.println(new StringBuffer().append("int    ").append(j21.getLong("int")).toString());
        System.out.println(new StringBuffer().append("long   ").append(j21.getLong("long")).toString());
        System.out.println(new StringBuffer().append("longer ").append(j21.getLong("longer")).toString());
        System.out.println(new StringBuffer().append("double ").append(j21.getLong("double")).toString());
        System.out.println(new StringBuffer().append("string ").append(j21.getLong("string")).toString());
        System.out.println("\ngetDouble");
        System.out.println(new StringBuffer().append("int    ").append(j21.getDouble("int")).toString());
        System.out.println(new StringBuffer().append("long   ").append(j21.getDouble("long")).toString());
        System.out.println(new StringBuffer().append("longer ").append(j21.getDouble("longer")).toString());
        System.out.println(new StringBuffer().append("double ").append(j21.getDouble("double")).toString());
        System.out.println(new StringBuffer().append("string ").append(j21.getDouble("string")).toString());
        j21.put("good sized", Long.MAX_VALUE);
        System.out.println(j21.toString(4));
        System.out.println(new JSONArray("[2147483647, 2147483648, 9223372036854775807, 9223372036854775808]").toString(4));
        System.out.println("\nKeys: ");
        Iterator it = j21.keys();
        String str = s;
        while (it.hasNext()) {
            String s2 = (String) it.next();
            System.out.println(new StringBuffer().append(s2).append(": ").append(j21.getString(s2)).toString());
        }
        System.out.println("\naccumulate: ");
        JSONObject j22 = new JSONObject();
        j22.accumulate("stooge", "Curly");
        j22.accumulate("stooge", "Larry");
        j22.accumulate("stooge", "Moe");
        j22.getJSONArray("stooge").put(5, "Shemp");
        System.out.println(j22.toString(4));
        System.out.println("\nwrite:");
        System.out.println(j22.write(new StringWriter()));
        JSONObject j23 = XML.toJSONObject("<xml empty><a></a><a>1</a><a>22</a><a>333</a></xml>");
        System.out.println(j23.toString(4));
        System.out.println(XML.toString(j23));
        JSONObject j24 = XML.toJSONObject("<book><chapter>Content of the first chapter</chapter><chapter>Content of the second chapter      <chapter>Content of the first subchapter</chapter>      <chapter>Content of the second subchapter</chapter></chapter><chapter>Third Chapter</chapter></book>");
        System.out.println(j24.toString(4));
        System.out.println(XML.toString(j24));
        JSONObject j25 = new JSONObject((Map) null);
        JSONArray a4 = new JSONArray((Collection) null);
        j25.append("stooge", "Joe DeRita");
        j25.put("map", (Map) null);
        j25.put("collection", (Collection) null);
        j25.put("array", a4);
        a4.put((Map) null);
        a4.put((Collection) null);
        System.out.println(j25.toString(4));
        System.out.println("\nTesting Exceptions: ");
        System.out.print("Exception: ");
        try {
            System.out.println(j25.getDouble("stooge"));
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            System.out.print("Exception: ");
            try {
                System.out.println(j25.getDouble("howard"));
            } catch (Exception e2) {
                System.out.println(e2);
            }
            System.out.print("Exception: ");
            try {
                System.out.println(j25.put((String) null, "howard"));
            } catch (Exception e3) {
                System.out.println(e3);
            }
            System.out.print("Exception: ");
            try {
                System.out.println(a4.getDouble(0));
            } catch (Exception e4) {
                System.out.println(e4);
            }
            System.out.print("Exception: ");
            try {
                System.out.println(a4.get(-1));
            } catch (Exception e5) {
                System.out.println(e5);
            }
            System.out.print("Exception: ");
            try {
                System.out.println(a4.put(Double.NaN));
            } catch (Exception e6) {
                System.out.println(e6);
            }
        } catch (Exception e7) {
            System.out.println(e7.toString());
        }
    }
}
