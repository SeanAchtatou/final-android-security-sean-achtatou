package org.json;

import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import weibo4j.AsyncWeibo;

public class JSONTokener {
    private int myIndex = 0;
    private String mySource;

    public JSONTokener(String s) {
        this.mySource = s;
    }

    public void back() {
        if (this.myIndex > 0) {
            this.myIndex--;
        }
    }

    public static int dehexchar(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return c - '7';
        }
        if (c < 'a' || c > 'f') {
            return -1;
        }
        return c - 'W';
    }

    public boolean more() {
        return this.myIndex < this.mySource.length();
    }

    public char next() {
        if (!more()) {
            return 0;
        }
        char c = this.mySource.charAt(this.myIndex);
        this.myIndex++;
        return c;
    }

    public char next(char c) throws JSONException {
        char n = next();
        if (n == c) {
            return n;
        }
        throw syntaxError(new StringBuffer().append("Expected '").append(c).append("' and instead saw '").append(n).append("'.").toString());
    }

    public String next(int n) throws JSONException {
        int i = this.myIndex;
        int j = i + n;
        if (j >= this.mySource.length()) {
            throw syntaxError("Substring bounds error");
        }
        this.myIndex += n;
        return this.mySource.substring(i, j);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: CFG modification limit reached, blocks count: 142 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public char nextClean() throws org.json.JSONException {
        /*
            r5 = this;
            r4 = 13
            r3 = 10
            r2 = 47
        L_0x0006:
            char r0 = r5.next()
            if (r0 != r2) goto L_0x003e
            char r1 = r5.next()
            switch(r1) {
                case 42: goto L_0x0026;
                case 47: goto L_0x0018;
                default: goto L_0x0013;
            }
        L_0x0013:
            r5.back()
            r1 = r2
        L_0x0017:
            return r1
        L_0x0018:
            char r0 = r5.next()
            if (r0 == r3) goto L_0x0006
            if (r0 == r4) goto L_0x0006
            if (r0 != 0) goto L_0x0018
            goto L_0x0006
        L_0x0023:
            r5.back()
        L_0x0026:
            char r0 = r5.next()
            if (r0 != 0) goto L_0x0033
            java.lang.String r1 = "Unclosed comment."
            org.json.JSONException r1 = r5.syntaxError(r1)
            throw r1
        L_0x0033:
            r1 = 42
            if (r0 != r1) goto L_0x0026
            char r1 = r5.next()
            if (r1 != r2) goto L_0x0023
            goto L_0x0006
        L_0x003e:
            r1 = 35
            if (r0 != r1) goto L_0x004d
        L_0x0042:
            char r0 = r5.next()
            if (r0 == r3) goto L_0x0006
            if (r0 == r4) goto L_0x0006
            if (r0 != 0) goto L_0x0042
            goto L_0x0006
        L_0x004d:
            if (r0 == 0) goto L_0x0053
            r1 = 32
            if (r0 <= r1) goto L_0x0006
        L_0x0053:
            r1 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONTokener.nextClean():char");
    }

    public String nextString(char quote) throws JSONException {
        StringBuffer sb = new StringBuffer();
        while (true) {
            char c = next();
            switch (c) {
                case 0:
                case AsyncWeibo.DIRECT_MESSAGES /*10*/:
                case 13:
                    throw syntaxError("Unterminated string");
                case '\\':
                    char c2 = next();
                    switch (c2) {
                        case 'b':
                            sb.append(8);
                            continue;
                        case HttpStatus.SC_PROCESSING:
                            sb.append(12);
                            continue;
                        case 'n':
                            sb.append(10);
                            continue;
                        case 'r':
                            sb.append(13);
                            continue;
                        case 't':
                            sb.append(9);
                            continue;
                        case 'u':
                            sb.append((char) Integer.parseInt(next(4), 16));
                            continue;
                        case 'x':
                            sb.append((char) Integer.parseInt(next(2), 16));
                            continue;
                        default:
                            sb.append(c2);
                            continue;
                    }
                default:
                    if (c != quote) {
                        sb.append(c);
                        break;
                    } else {
                        return sb.toString();
                    }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String nextTo(char r4) {
        /*
            r3 = this;
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0005:
            char r0 = r3.next()
            if (r0 == r4) goto L_0x0015
            if (r0 == 0) goto L_0x0015
            r2 = 10
            if (r0 == r2) goto L_0x0015
            r2 = 13
            if (r0 != r2) goto L_0x0023
        L_0x0015:
            if (r0 == 0) goto L_0x001a
            r3.back()
        L_0x001a:
            java.lang.String r2 = r1.toString()
            java.lang.String r2 = r2.trim()
            return r2
        L_0x0023:
            r1.append(r0)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONTokener.nextTo(char):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String nextTo(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0005:
            char r0 = r3.next()
            int r2 = r4.indexOf(r0)
            if (r2 >= 0) goto L_0x0019
            if (r0 == 0) goto L_0x0019
            r2 = 10
            if (r0 == r2) goto L_0x0019
            r2 = 13
            if (r0 != r2) goto L_0x0027
        L_0x0019:
            if (r0 == 0) goto L_0x001e
            r3.back()
        L_0x001e:
            java.lang.String r2 = r1.toString()
            java.lang.String r2 = r2.trim()
            return r2
        L_0x0027:
            r1.append(r0)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONTokener.nextTo(java.lang.String):java.lang.String");
    }

    public Object nextValue() throws JSONException {
        char c = nextClean();
        switch (c) {
            case AsyncWeibo.EXISTS_FRIENDSHIP /*34*/:
            case AsyncWeibo.UPDATE_STATUS /*39*/:
                return nextString(c);
            case '[':
                back();
                return new JSONArray(this);
            case '{':
                back();
                return new JSONObject(this);
            default:
                StringBuffer sb = new StringBuffer();
                char b = c;
                while (c >= ' ' && ",:]}/\\\"[{;=#".indexOf(c) < 0) {
                    sb.append(c);
                    c = next();
                }
                back();
                String s = sb.toString().trim();
                if (s.equals("")) {
                    throw syntaxError("Missing value.");
                } else if (s.equalsIgnoreCase("true")) {
                    return Boolean.TRUE;
                } else {
                    if (s.equalsIgnoreCase(HttpState.PREEMPTIVE_DEFAULT)) {
                        return Boolean.FALSE;
                    }
                    if (s.equalsIgnoreCase("null")) {
                        return JSONObject.NULL;
                    }
                    if ((b < '0' || b > '9') && b != '.' && b != '-' && b != '+') {
                        return s;
                    }
                    if (b == '0') {
                        if (s.length() <= 2 || !(s.charAt(1) == 'x' || s.charAt(1) == 'X')) {
                            try {
                                return new Integer(Integer.parseInt(s, 8));
                            } catch (Exception e) {
                            }
                        } else {
                            try {
                                return new Integer(Integer.parseInt(s.substring(2), 16));
                            } catch (Exception e2) {
                            }
                        }
                    }
                    try {
                        return new Integer(s);
                    } catch (Exception e3) {
                        try {
                            return new Long(s);
                        } catch (Exception e4) {
                            try {
                                return new Double(s);
                            } catch (Exception e5) {
                                return s;
                            }
                        }
                    }
                }
        }
    }

    public char skipTo(char to) {
        char c;
        int index = this.myIndex;
        while (true) {
            c = next();
            if (c != 0) {
                if (c == to) {
                    back();
                    break;
                }
            } else {
                this.myIndex = index;
                break;
            }
        }
        return c;
    }

    public void skipPast(String to) {
        this.myIndex = this.mySource.indexOf(to, this.myIndex);
        if (this.myIndex < 0) {
            this.myIndex = this.mySource.length();
        } else {
            this.myIndex += to.length();
        }
    }

    public JSONException syntaxError(String message) {
        return new JSONException(new StringBuffer().append(message).append(toString()).toString());
    }

    public String toString() {
        return new StringBuffer().append(" at character ").append(this.myIndex).append(" of ").append(this.mySource).toString();
    }
}
