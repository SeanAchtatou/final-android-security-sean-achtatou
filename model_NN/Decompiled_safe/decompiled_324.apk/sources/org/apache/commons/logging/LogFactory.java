package org.apache.commons.logging;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public abstract class LogFactory {
    public static final String DIAGNOSTICS_DEST_PROPERTY = "org.apache.commons.logging.diagnostics.dest";
    public static final String FACTORY_DEFAULT = "org.apache.commons.logging.impl.LogFactoryImpl";
    public static final String FACTORY_PROPERTIES = "commons-logging.properties";
    public static final String FACTORY_PROPERTY = "org.apache.commons.logging.LogFactory";
    public static final String HASHTABLE_IMPLEMENTATION_PROPERTY = "org.apache.commons.logging.LogFactory.HashtableImpl";
    public static final String PRIORITY_KEY = "priority";
    protected static final String SERVICE_ID = "META-INF/services/org.apache.commons.logging.LogFactory";
    public static final String TCCL_KEY = "use_tccl";
    private static final String WEAK_HASHTABLE_CLASSNAME = "org.apache.commons.logging.impl.WeakHashtable";
    static Class class$java$lang$Thread;
    static Class class$org$apache$commons$logging$LogFactory;
    private static String diagnosticPrefix;
    private static PrintStream diagnosticsStream = null;
    protected static Hashtable factories;
    protected static LogFactory nullClassLoaderFactory = null;
    private static ClassLoader thisClassLoader;

    public abstract Object getAttribute(String str);

    public abstract String[] getAttributeNames();

    public abstract Log getInstance(Class cls) throws LogConfigurationException;

    public abstract Log getInstance(String str) throws LogConfigurationException;

    public abstract void release();

    public abstract void removeAttribute(String str);

    public abstract void setAttribute(String str, Object obj);

    static Class class$(String class$) {
        try {
            return Class.forName(class$);
        } catch (ClassNotFoundException forName) {
            throw new NoClassDefFoundError(forName.getMessage());
        }
    }

    static {
        Class class$;
        Class class$2;
        factories = null;
        if (class$org$apache$commons$logging$LogFactory != null) {
            class$ = class$org$apache$commons$logging$LogFactory;
        } else {
            class$ = class$(FACTORY_PROPERTY);
            class$org$apache$commons$logging$LogFactory = class$;
        }
        thisClassLoader = getClassLoader(class$);
        initDiagnostics();
        if (class$org$apache$commons$logging$LogFactory != null) {
            class$2 = class$org$apache$commons$logging$LogFactory;
        } else {
            class$2 = class$(FACTORY_PROPERTY);
            class$org$apache$commons$logging$LogFactory = class$2;
        }
        logClassLoaderEnvironment(class$2);
        factories = createFactoryStore();
        if (isDiagnosticsEnabled()) {
            logDiagnostic("BOOTSTRAP COMPLETED");
        }
    }

    protected LogFactory() {
    }

    /* JADX WARN: Type inference failed for: r4v7, types: [java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final java.util.Hashtable createFactoryStore() {
        /*
            r2 = 0
            java.lang.String r4 = "org.apache.commons.logging.LogFactory.HashtableImpl"
            java.lang.String r3 = java.lang.System.getProperty(r4)
            if (r3 != 0) goto L_0x000b
            java.lang.String r3 = "org.apache.commons.logging.impl.WeakHashtable"
        L_0x000b:
            java.lang.Class r1 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x001f }
            java.lang.Object r4 = r1.newInstance()     // Catch:{ Throwable -> 0x001f }
            r0 = r4
            java.util.Hashtable r0 = (java.util.Hashtable) r0     // Catch:{ Throwable -> 0x001f }
            r2 = r0
        L_0x0017:
            if (r2 != 0) goto L_0x001e
            java.util.Hashtable r2 = new java.util.Hashtable
            r2.<init>()
        L_0x001e:
            return r2
        L_0x001f:
            r4 = move-exception
            java.lang.String r4 = "org.apache.commons.logging.impl.WeakHashtable"
            boolean r4 = r4.equals(r3)
            if (r4 != 0) goto L_0x0017
            boolean r4 = isDiagnosticsEnabled()
            if (r4 == 0) goto L_0x0034
            java.lang.String r4 = "[ERROR] LogFactory: Load of custom hashtable failed"
            logDiagnostic(r4)
            goto L_0x0017
        L_0x0034:
            java.io.PrintStream r4 = java.lang.System.err
            java.lang.String r5 = "[ERROR] LogFactory: Load of custom hashtable failed"
            r4.println(r5)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.LogFactory.createFactoryStore():java.util.Hashtable");
    }

    /* JADX INFO: Multiple debug info for r2v34 java.lang.String: [D('factoryClassName' java.lang.String), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r2v40 org.apache.commons.logging.LogFactory: [D('factoryClass' java.lang.String), D('factory' org.apache.commons.logging.LogFactory)] */
    /* JADX INFO: Multiple debug info for r1v10 org.apache.commons.logging.LogFactory: [D('contextClassLoader' java.lang.ClassLoader), D('factory' org.apache.commons.logging.LogFactory)] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x014a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.commons.logging.LogFactory getFactory() throws org.apache.commons.logging.LogConfigurationException {
        /*
            java.lang.ClassLoader r1 = getContextClassLoader()
            if (r1 != 0) goto L_0x0011
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "Context classloader is null."
            logDiagnostic(r0)
        L_0x0011:
            org.apache.commons.logging.LogFactory r3 = getCachedFactory(r1)
            if (r3 == 0) goto L_0x001a
            r0 = r3
            r1 = r3
        L_0x0019:
            return r1
        L_0x001a:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x003b
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            java.lang.String r2 = "[LOOKUP] LogFactory implementation requested for the first time for context classloader "
            r0.<init>(r2)
            java.lang.String r2 = objectId(r1)
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            logDiagnostic(r0)
            java.lang.String r0 = "[LOOKUP] "
            logHierarchy(r0, r1)
        L_0x003b:
            java.lang.String r0 = "commons-logging.properties"
            java.util.Properties r4 = getConfigurationFile(r1, r0)
            r0 = r1
            if (r4 == 0) goto L_0x0058
            java.lang.String r2 = "use_tccl"
            java.lang.String r2 = r4.getProperty(r2)
            if (r2 == 0) goto L_0x0058
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)
            boolean r2 = r2.booleanValue()
            if (r2 != 0) goto L_0x0058
            java.lang.ClassLoader r0 = org.apache.commons.logging.LogFactory.thisClassLoader
        L_0x0058:
            boolean r2 = isDiagnosticsEnabled()
            if (r2 == 0) goto L_0x0063
            java.lang.String r2 = "[LOOKUP] Looking for system property [org.apache.commons.logging.LogFactory] to define the LogFactory subclass to use..."
            logDiagnostic(r2)
        L_0x0063:
            java.lang.String r2 = "org.apache.commons.logging.LogFactory"
            java.lang.String r2 = java.lang.System.getProperty(r2)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            if (r2 == 0) goto L_0x015c
            boolean r5 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            if (r5 == 0) goto L_0x008f
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            java.lang.String r6 = "[LOOKUP] Creating an instance of LogFactory class '"
            r5.<init>(r6)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            java.lang.StringBuffer r5 = r5.append(r2)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            java.lang.String r6 = "' as specified by system property "
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            java.lang.String r6 = "org.apache.commons.logging.LogFactory"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            java.lang.String r5 = r5.toString()     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            logDiagnostic(r5)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
        L_0x008f:
            org.apache.commons.logging.LogFactory r2 = newFactory(r2, r0, r1)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            r3 = r2
        L_0x0094:
            if (r3 != 0) goto L_0x022a
            boolean r2 = isDiagnosticsEnabled()
            if (r2 == 0) goto L_0x00a1
            java.lang.String r2 = "[LOOKUP] Looking for a resource file of name [META-INF/services/org.apache.commons.logging.LogFactory] to define the LogFactory subclass to use..."
            logDiagnostic(r2)
        L_0x00a1:
            java.lang.String r2 = "META-INF/services/org.apache.commons.logging.LogFactory"
            java.io.InputStream r2 = getResourceAsStream(r1, r2)     // Catch:{ Exception -> 0x01d5 }
            if (r2 == 0) goto L_0x01c7
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x01ba }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x01ba }
            java.lang.String r7 = "UTF-8"
            r6.<init>(r2, r7)     // Catch:{ UnsupportedEncodingException -> 0x01ba }
            r5.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x01ba }
        L_0x00b5:
            java.lang.String r2 = r5.readLine()     // Catch:{ Exception -> 0x01d5 }
            r5.close()     // Catch:{ Exception -> 0x01d5 }
            if (r2 == 0) goto L_0x022a
            java.lang.String r5 = ""
            boolean r5 = r5.equals(r2)     // Catch:{ Exception -> 0x01d5 }
            if (r5 != 0) goto L_0x022a
            boolean r5 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x01d5 }
            if (r5 == 0) goto L_0x00f6
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r6 = "[LOOKUP]  Creating an instance of LogFactory class "
            r5.<init>(r6)     // Catch:{ Exception -> 0x01d5 }
            java.lang.StringBuffer r5 = r5.append(r2)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r6 = " as specified by file '"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r6 = "META-INF/services/org.apache.commons.logging.LogFactory"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r6 = "' which was present in the path of the context"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r6 = " classloader."
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ Exception -> 0x01d5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01d5 }
            logDiagnostic(r5)     // Catch:{ Exception -> 0x01d5 }
        L_0x00f6:
            org.apache.commons.logging.LogFactory r2 = newFactory(r2, r0, r1)     // Catch:{ Exception -> 0x01d5 }
        L_0x00fa:
            if (r2 != 0) goto L_0x0218
            if (r4 == 0) goto L_0x020d
            boolean r3 = isDiagnosticsEnabled()
            if (r3 == 0) goto L_0x0109
            java.lang.String r3 = "[LOOKUP] Looking in properties file for entry with key 'org.apache.commons.logging.LogFactory' to define the LogFactory subclass to use..."
            logDiagnostic(r3)
        L_0x0109:
            java.lang.String r3 = "org.apache.commons.logging.LogFactory"
            java.lang.String r3 = r4.getProperty(r3)
            if (r3 == 0) goto L_0x01ff
            boolean r2 = isDiagnosticsEnabled()
            if (r2 == 0) goto L_0x012f
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r5 = "[LOOKUP] Properties file specifies LogFactory subclass '"
            r2.<init>(r5)
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r5 = "'"
            java.lang.StringBuffer r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            logDiagnostic(r2)
        L_0x012f:
            org.apache.commons.logging.LogFactory r0 = newFactory(r3, r0, r1)
        L_0x0133:
            if (r0 != 0) goto L_0x0148
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0140
            java.lang.String r0 = "[LOOKUP] Loading the default LogFactory implementation 'org.apache.commons.logging.impl.LogFactoryImpl' via the same classloader that loaded this LogFactory class (ie not looking in the context classloader)."
            logDiagnostic(r0)
        L_0x0140:
            java.lang.String r0 = "org.apache.commons.logging.impl.LogFactoryImpl"
            java.lang.ClassLoader r2 = org.apache.commons.logging.LogFactory.thisClassLoader
            org.apache.commons.logging.LogFactory r0 = newFactory(r0, r2, r1)
        L_0x0148:
            if (r0 == 0) goto L_0x0159
            cacheFactory(r1, r0)
            if (r4 == 0) goto L_0x0159
            java.util.Enumeration r2 = r4.propertyNames()
        L_0x0153:
            boolean r1 = r2.hasMoreElements()
            if (r1 != 0) goto L_0x021b
        L_0x0159:
            r1 = r0
            goto L_0x0019
        L_0x015c:
            boolean r2 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            if (r2 == 0) goto L_0x0094
            java.lang.String r2 = "[LOOKUP] No system property [org.apache.commons.logging.LogFactory] defined."
            logDiagnostic(r2)     // Catch:{ SecurityException -> 0x0169, RuntimeException -> 0x0192 }
            goto L_0x0094
        L_0x0169:
            r2 = move-exception
            boolean r5 = isDiagnosticsEnabled()
            if (r5 == 0) goto L_0x0094
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            java.lang.String r6 = "[LOOKUP] A security exception occurred while trying to create an instance of the custom factory class: ["
            r5.<init>(r6)
            java.lang.String r2 = r2.getMessage()
            java.lang.String r2 = r2.trim()
            java.lang.StringBuffer r2 = r5.append(r2)
            java.lang.String r5 = "]. Trying alternative implementations..."
            java.lang.StringBuffer r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            logDiagnostic(r2)
            goto L_0x0094
        L_0x0192:
            r0 = move-exception
            boolean r1 = isDiagnosticsEnabled()
            if (r1 == 0) goto L_0x01b9
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "[LOOKUP] An exception occurred while trying to create an instance of the custom factory class: ["
            r1.<init>(r2)
            java.lang.String r2 = r0.getMessage()
            java.lang.String r2 = r2.trim()
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "] as specified by a system property."
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            logDiagnostic(r1)
        L_0x01b9:
            throw r0
        L_0x01ba:
            r5 = move-exception
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01d5 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01d5 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x01d5 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x01d5 }
            goto L_0x00b5
        L_0x01c7:
            boolean r2 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x01d5 }
            if (r2 == 0) goto L_0x022a
            java.lang.String r2 = "[LOOKUP] No resource file with name 'META-INF/services/org.apache.commons.logging.LogFactory' found."
            logDiagnostic(r2)     // Catch:{ Exception -> 0x01d5 }
            r2 = r3
            goto L_0x00fa
        L_0x01d5:
            r2 = move-exception
            boolean r5 = isDiagnosticsEnabled()
            if (r5 == 0) goto L_0x022a
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            java.lang.String r6 = "[LOOKUP] A security exception occurred while trying to create an instance of the custom factory class: ["
            r5.<init>(r6)
            java.lang.String r2 = r2.getMessage()
            java.lang.String r2 = r2.trim()
            java.lang.StringBuffer r2 = r5.append(r2)
            java.lang.String r5 = "]. Trying alternative implementations..."
            java.lang.StringBuffer r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            logDiagnostic(r2)
            r2 = r3
            goto L_0x00fa
        L_0x01ff:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0218
            java.lang.String r0 = "[LOOKUP] Properties file has no entry specifying LogFactory subclass."
            logDiagnostic(r0)
            r0 = r2
            goto L_0x0133
        L_0x020d:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0218
            java.lang.String r0 = "[LOOKUP] No properties file available to determine LogFactory subclass from.."
            logDiagnostic(r0)
        L_0x0218:
            r0 = r2
            goto L_0x0133
        L_0x021b:
            java.lang.Object r1 = r2.nextElement()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r3 = r4.getProperty(r1)
            r0.setAttribute(r1, r3)
            goto L_0x0153
        L_0x022a:
            r2 = r3
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.LogFactory.getFactory():org.apache.commons.logging.LogFactory");
    }

    public static Log getLog(Class clazz) throws LogConfigurationException {
        return getFactory().getInstance(clazz);
    }

    public static Log getLog(String name) throws LogConfigurationException {
        return getFactory().getInstance(name);
    }

    public static void release(ClassLoader classLoader) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer("Releasing factory for classloader ").append(objectId(classLoader)).toString());
        }
        synchronized (factories) {
            if (classLoader != null) {
                LogFactory factory = (LogFactory) factories.get(classLoader);
                if (factory != null) {
                    factory.release();
                    factories.remove(classLoader);
                }
            } else if (nullClassLoaderFactory != null) {
                nullClassLoaderFactory.release();
                nullClassLoaderFactory = null;
            }
        }
    }

    public static void releaseAll() {
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Releasing factory for all classloaders.");
        }
        synchronized (factories) {
            Enumeration elements = factories.elements();
            while (elements.hasMoreElements()) {
                ((LogFactory) elements.nextElement()).release();
            }
            factories.clear();
            if (nullClassLoaderFactory != null) {
                nullClassLoaderFactory.release();
                nullClassLoaderFactory = null;
            }
        }
    }

    protected static ClassLoader getClassLoader(Class clazz) {
        try {
            return clazz.getClassLoader();
        } catch (SecurityException ex) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer("Unable to get classloader for class '").append(clazz).append("' due to security restrictions - ").append(ex.getMessage()).toString());
            }
            throw ex;
        }
    }

    protected static ClassLoader getContextClassLoader() throws LogConfigurationException {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                return LogFactory.directGetContextClassLoader();
            }
        });
    }

    protected static ClassLoader directGetContextClassLoader() throws LogConfigurationException {
        Class class$;
        Class class$2;
        try {
            if (class$java$lang$Thread != null) {
                class$2 = class$java$lang$Thread;
            } else {
                class$2 = class$("java.lang.Thread");
                class$java$lang$Thread = class$2;
            }
            return (ClassLoader) class$2.getMethod("getContextClassLoader", null).invoke(Thread.currentThread(), null);
        } catch (IllegalAccessException e) {
            throw new LogConfigurationException("Unexpected IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            InvocationTargetException e3 = e2;
            if (e3.getTargetException() instanceof SecurityException) {
                return null;
            }
            throw new LogConfigurationException("Unexpected InvocationTargetException", e3.getTargetException());
        } catch (NoSuchMethodException e4) {
            if (class$org$apache$commons$logging$LogFactory != null) {
                class$ = class$org$apache$commons$logging$LogFactory;
            } else {
                class$ = class$(FACTORY_PROPERTY);
                class$org$apache$commons$logging$LogFactory = class$;
            }
            return getClassLoader(class$);
        }
    }

    private static LogFactory getCachedFactory(ClassLoader contextClassLoader) {
        if (contextClassLoader == null) {
            return nullClassLoaderFactory;
        }
        return (LogFactory) factories.get(contextClassLoader);
    }

    private static void cacheFactory(ClassLoader classLoader, LogFactory factory) {
        if (factory == null) {
            return;
        }
        if (classLoader == null) {
            nullClassLoaderFactory = factory;
        } else {
            factories.put(classLoader, factory);
        }
    }

    protected static LogFactory newFactory(String factoryClass, ClassLoader classLoader, ClassLoader contextClassLoader) throws LogConfigurationException {
        Object result = AccessController.doPrivileged(new PrivilegedAction(classLoader, factoryClass) {
            private final ClassLoader val$classLoader;
            private final String val$factoryClass;

            {
                this.val$classLoader = val$classLoader;
                this.val$factoryClass = val$factoryClass;
            }

            public Object run() {
                return LogFactory.createFactory(this.val$factoryClass, this.val$classLoader);
            }
        });
        if (result instanceof LogConfigurationException) {
            LogConfigurationException ex = (LogConfigurationException) result;
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer("An error occurred while loading the factory class:").append(ex.getMessage()).toString());
            }
            throw ex;
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer("Created object ").append(objectId(result)).append(" to manage classloader ").append(objectId(contextClassLoader)).toString());
        }
        return (LogFactory) result;
    }

    protected static LogFactory newFactory(String factoryClass, ClassLoader classLoader) {
        return newFactory(factoryClass, classLoader, null);
    }

    protected static Object createFactory(String factoryClass, ClassLoader classLoader) {
        Class class$;
        String msg;
        Class class$2;
        Class class$3;
        Class class$4;
        Class logFactoryClass = null;
        if (classLoader != null) {
            try {
                logFactoryClass = classLoader.loadClass(factoryClass);
                if (class$org$apache$commons$logging$LogFactory != null) {
                    class$3 = class$org$apache$commons$logging$LogFactory;
                } else {
                    class$3 = class$(FACTORY_PROPERTY);
                    class$org$apache$commons$logging$LogFactory = class$3;
                }
                if (class$3.isAssignableFrom(logFactoryClass)) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer("Loaded class ").append(logFactoryClass.getName()).append(" from classloader ").append(objectId(classLoader)).toString());
                    }
                } else if (isDiagnosticsEnabled()) {
                    StringBuffer append = new StringBuffer("Factory class ").append(logFactoryClass.getName()).append(" loaded from classloader ").append(objectId(logFactoryClass.getClassLoader())).append(" does not extend '");
                    if (class$org$apache$commons$logging$LogFactory != null) {
                        class$4 = class$org$apache$commons$logging$LogFactory;
                    } else {
                        class$4 = class$(FACTORY_PROPERTY);
                        class$org$apache$commons$logging$LogFactory = class$4;
                    }
                    logDiagnostic(append.append(class$4.getName()).append("' as loaded by this classloader.").toString());
                    logHierarchy("[BAD CL TREE] ", classLoader);
                }
                return (LogFactory) logFactoryClass.newInstance();
            } catch (ClassNotFoundException e) {
                ClassNotFoundException ex = e;
                if (classLoader == thisClassLoader) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer("Unable to locate any class called '").append(factoryClass).append("' via classloader ").append(objectId(classLoader)).toString());
                    }
                    throw ex;
                }
            } catch (NoClassDefFoundError e2) {
                NoClassDefFoundError e3 = e2;
                if (classLoader == thisClassLoader) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer("Class '").append(factoryClass).append("' cannot be loaded").append(" via classloader ").append(objectId(classLoader)).append(" - it depends on some other class that cannot").append(" be found.").toString());
                    }
                    throw e3;
                }
            } catch (ClassCastException e4) {
                if (classLoader == thisClassLoader) {
                    boolean implementsLogFactory = implementsLogFactory(logFactoryClass);
                    StringBuffer append2 = new StringBuffer("The application has specified that a custom LogFactory implementation should be used but Class '").append(factoryClass).append("' cannot be converted to '");
                    if (class$org$apache$commons$logging$LogFactory != null) {
                        class$ = class$org$apache$commons$logging$LogFactory;
                    } else {
                        class$ = class$(FACTORY_PROPERTY);
                        class$org$apache$commons$logging$LogFactory = class$;
                    }
                    String msg2 = append2.append(class$.getName()).append("'. ").toString();
                    if (implementsLogFactory) {
                        msg = new StringBuffer(String.valueOf(msg2)).append("The conflict is caused by the presence of multiple LogFactory classes in incompatible classloaders. ").append("Background can be found in http://jakarta.apache.org/commons/logging/tech.html. ").append("If you have not explicitly specified a custom LogFactory then it is likely that ").append("the container has set one without your knowledge. ").append("In this case, consider using the commons-logging-adapters.jar file or ").append("specifying the standard LogFactory from the command line. ").toString();
                    } else {
                        msg = new StringBuffer(String.valueOf(msg2)).append("Please check the custom implementation. ").toString();
                    }
                    String msg3 = new StringBuffer(String.valueOf(msg)).append("Help can be found @http://jakarta.apache.org/commons/logging/troubleshooting.html.").toString();
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(msg3);
                    }
                    throw new ClassCastException(msg3);
                }
            } catch (Exception e5) {
                Exception e6 = e5;
                if (isDiagnosticsEnabled()) {
                    logDiagnostic("Unable to create LogFactory instance.");
                }
                if (logFactoryClass != null) {
                    if (class$org$apache$commons$logging$LogFactory != null) {
                        class$2 = class$org$apache$commons$logging$LogFactory;
                    } else {
                        class$2 = class$(FACTORY_PROPERTY);
                        class$org$apache$commons$logging$LogFactory = class$2;
                    }
                    if (!class$2.isAssignableFrom(logFactoryClass)) {
                        return new LogConfigurationException("The chosen LogFactory implementation does not extend LogFactory. Please check your configuration.", e6);
                    }
                }
                return new LogConfigurationException(e6);
            }
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer("Unable to load factory class via classloader ").append(objectId(classLoader)).append(" - trying the classloader associated with this LogFactory.").toString());
        }
        return (LogFactory) Class.forName(factoryClass).newInstance();
    }

    private static boolean implementsLogFactory(Class logFactoryClass) {
        boolean implementsLogFactory = false;
        if (logFactoryClass != null) {
            try {
                ClassLoader logFactoryClassLoader = logFactoryClass.getClassLoader();
                if (logFactoryClassLoader == null) {
                    logDiagnostic("[CUSTOM LOG FACTORY] was loaded by the boot classloader");
                } else {
                    logHierarchy("[CUSTOM LOG FACTORY] ", logFactoryClassLoader);
                    implementsLogFactory = Class.forName(FACTORY_PROPERTY, false, logFactoryClassLoader).isAssignableFrom(logFactoryClass);
                    if (implementsLogFactory) {
                        logDiagnostic(new StringBuffer("[CUSTOM LOG FACTORY] ").append(logFactoryClass.getName()).append(" implements LogFactory but was loaded by an incompatible classloader.").toString());
                    } else {
                        logDiagnostic(new StringBuffer("[CUSTOM LOG FACTORY] ").append(logFactoryClass.getName()).append(" does not implement LogFactory.").toString());
                    }
                }
            } catch (SecurityException e) {
                logDiagnostic(new StringBuffer("[CUSTOM LOG FACTORY] SecurityException thrown whilst trying to determine whether the compatibility was caused by a classloader conflict: ").append(e.getMessage()).toString());
            } catch (LinkageError e2) {
                logDiagnostic(new StringBuffer("[CUSTOM LOG FACTORY] LinkageError thrown whilst trying to determine whether the compatibility was caused by a classloader conflict: ").append(e2.getMessage()).toString());
            } catch (ClassNotFoundException e3) {
                logDiagnostic("[CUSTOM LOG FACTORY] LogFactory class cannot be loaded by classloader which loaded the custom LogFactory implementation. Is the custom factory in the right classloader?");
            }
        }
        return implementsLogFactory;
    }

    private static InputStream getResourceAsStream(ClassLoader loader, String name) {
        return (InputStream) AccessController.doPrivileged(new PrivilegedAction(loader, name) {
            private final ClassLoader val$loader;
            private final String val$name;

            {
                this.val$loader = val$loader;
                this.val$name = val$name;
            }

            public Object run() {
                if (this.val$loader != null) {
                    return this.val$loader.getResourceAsStream(this.val$name);
                }
                return ClassLoader.getSystemResourceAsStream(this.val$name);
            }
        });
    }

    private static Enumeration getResources(ClassLoader loader, String name) {
        return (Enumeration) AccessController.doPrivileged(new PrivilegedAction(loader, name) {
            private final ClassLoader val$loader;
            private final String val$name;

            {
                this.val$loader = val$loader;
                this.val$name = val$name;
            }

            public Object run() {
                try {
                    if (this.val$loader != null) {
                        return this.val$loader.getResources(this.val$name);
                    }
                    return ClassLoader.getSystemResources(this.val$name);
                } catch (IOException e) {
                    IOException e2 = e;
                    if (LogFactory.isDiagnosticsEnabled()) {
                        LogFactory.access$0(new StringBuffer("Exception while trying to find configuration file ").append(this.val$name).append(":").append(e2.getMessage()).toString());
                    }
                    return null;
                } catch (NoSuchMethodError e3) {
                    return null;
                }
            }
        });
    }

    private static Properties getProperties(URL url) {
        return (Properties) AccessController.doPrivileged(new PrivilegedAction(url) {
            private final URL val$url;

            {
                this.val$url = val$url;
            }

            public Object run() {
                try {
                    InputStream stream = this.val$url.openStream();
                    if (stream != null) {
                        Properties props = new Properties();
                        props.load(stream);
                        stream.close();
                        return props;
                    }
                } catch (IOException e) {
                    if (LogFactory.isDiagnosticsEnabled()) {
                        LogFactory.access$0(new StringBuffer("Unable to read URL ").append(this.val$url).toString());
                    }
                }
                return null;
            }
        });
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 144 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final java.util.Properties getConfigurationFile(java.lang.ClassLoader r13, java.lang.String r14) {
        /*
            r7 = 0
            r4 = 0
            r8 = 0
            java.util.Enumeration r10 = getResources(r13, r14)     // Catch:{ SecurityException -> 0x0116 }
            if (r10 != 0) goto L_0x0052
            r11 = 0
        L_0x000b:
            return r11
        L_0x000c:
            java.lang.Object r9 = r10.nextElement()     // Catch:{ SecurityException -> 0x0116 }
            java.net.URL r9 = (java.net.URL) r9     // Catch:{ SecurityException -> 0x0116 }
            java.util.Properties r3 = getProperties(r9)     // Catch:{ SecurityException -> 0x0116 }
            if (r3 == 0) goto L_0x0052
            if (r7 != 0) goto L_0x007a
            r8 = r9
            r7 = r3
            java.lang.String r11 = "priority"
            java.lang.String r6 = r7.getProperty(r11)     // Catch:{ SecurityException -> 0x0116 }
            r4 = 0
            if (r6 == 0) goto L_0x002a
            double r4 = java.lang.Double.parseDouble(r6)     // Catch:{ SecurityException -> 0x0116 }
        L_0x002a:
            boolean r11 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0116 }
            if (r11 == 0) goto L_0x0052
            java.lang.StringBuffer r11 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "[LOOKUP] Properties file found at '"
            r11.<init>(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r9)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r4)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r11 = r11.toString()     // Catch:{ SecurityException -> 0x0116 }
            logDiagnostic(r11)     // Catch:{ SecurityException -> 0x0116 }
        L_0x0052:
            boolean r11 = r10.hasMoreElements()     // Catch:{ SecurityException -> 0x0116 }
            if (r11 != 0) goto L_0x000c
        L_0x0058:
            boolean r11 = isDiagnosticsEnabled()
            if (r11 == 0) goto L_0x0078
            if (r7 != 0) goto L_0x0124
            java.lang.StringBuffer r11 = new java.lang.StringBuffer
            java.lang.String r12 = "[LOOKUP] No properties file of name '"
            r11.<init>(r12)
            java.lang.StringBuffer r11 = r11.append(r14)
            java.lang.String r12 = "' found."
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r11 = r11.toString()
            logDiagnostic(r11)
        L_0x0078:
            r11 = r7
            goto L_0x000b
        L_0x007a:
            java.lang.String r11 = "priority"
            java.lang.String r2 = r3.getProperty(r11)     // Catch:{ SecurityException -> 0x0116 }
            r0 = 0
            if (r2 == 0) goto L_0x0088
            double r0 = java.lang.Double.parseDouble(r2)     // Catch:{ SecurityException -> 0x0116 }
        L_0x0088:
            int r11 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r11 <= 0) goto L_0x00d2
            boolean r11 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0116 }
            if (r11 == 0) goto L_0x00ce
            java.lang.StringBuffer r11 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "[LOOKUP] Properties file at '"
            r11.<init>(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r9)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r0)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " overrides file at '"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r8)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r4)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r11 = r11.toString()     // Catch:{ SecurityException -> 0x0116 }
            logDiagnostic(r11)     // Catch:{ SecurityException -> 0x0116 }
        L_0x00ce:
            r8 = r9
            r7 = r3
            r4 = r0
            goto L_0x0052
        L_0x00d2:
            boolean r11 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0116 }
            if (r11 == 0) goto L_0x0052
            java.lang.StringBuffer r11 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "[LOOKUP] Properties file at '"
            r11.<init>(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r9)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r0)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " does not override file at '"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r8)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.StringBuffer r11 = r11.append(r4)     // Catch:{ SecurityException -> 0x0116 }
            java.lang.String r11 = r11.toString()     // Catch:{ SecurityException -> 0x0116 }
            logDiagnostic(r11)     // Catch:{ SecurityException -> 0x0116 }
            goto L_0x0052
        L_0x0116:
            r11 = move-exception
            boolean r11 = isDiagnosticsEnabled()
            if (r11 == 0) goto L_0x0058
            java.lang.String r11 = "SecurityException thrown while trying to find/read config files."
            logDiagnostic(r11)
            goto L_0x0058
        L_0x0124:
            java.lang.StringBuffer r11 = new java.lang.StringBuffer
            java.lang.String r12 = "[LOOKUP] Properties file of name '"
            r11.<init>(r12)
            java.lang.StringBuffer r11 = r11.append(r14)
            java.lang.String r12 = "' found at '"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.StringBuffer r11 = r11.append(r8)
            r12 = 34
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r11 = r11.toString()
            logDiagnostic(r11)
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.LogFactory.getConfigurationFile(java.lang.ClassLoader, java.lang.String):java.util.Properties");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    private static void initDiagnostics() {
        String classLoaderName;
        try {
            String dest = System.getProperty(DIAGNOSTICS_DEST_PROPERTY);
            if (dest != null) {
                if (dest.equals("STDOUT")) {
                    diagnosticsStream = System.out;
                } else if (dest.equals("STDERR")) {
                    diagnosticsStream = System.err;
                } else {
                    try {
                        diagnosticsStream = new PrintStream(new FileOutputStream(dest, true));
                    } catch (IOException e) {
                        return;
                    }
                }
                try {
                    ClassLoader classLoader = thisClassLoader;
                    if (thisClassLoader == null) {
                        classLoaderName = "BOOTLOADER";
                    } else {
                        classLoaderName = objectId(classLoader);
                    }
                } catch (SecurityException e2) {
                    classLoaderName = "UNKNOWN";
                }
                diagnosticPrefix = new StringBuffer("[LogFactory from ").append(classLoaderName).append("] ").toString();
            }
        } catch (SecurityException e3) {
        }
    }

    protected static boolean isDiagnosticsEnabled() {
        return diagnosticsStream != null;
    }

    static void access$0(String $0) {
        logDiagnostic($0);
    }

    private static final void logDiagnostic(String msg) {
        if (diagnosticsStream != null) {
            diagnosticsStream.print(diagnosticPrefix);
            diagnosticsStream.println(msg);
            diagnosticsStream.flush();
        }
    }

    protected static final void logRawDiagnostic(String msg) {
        if (diagnosticsStream != null) {
            diagnosticsStream.println(msg);
            diagnosticsStream.flush();
        }
    }

    private static void logClassLoaderEnvironment(Class clazz) {
        if (isDiagnosticsEnabled()) {
            try {
                logDiagnostic(new StringBuffer("[ENV] Extension directories (java.ext.dir): ").append(System.getProperty("java.ext.dir")).toString());
                logDiagnostic(new StringBuffer("[ENV] Application classpath (java.class.path): ").append(System.getProperty("java.class.path")).toString());
            } catch (SecurityException e) {
                logDiagnostic("[ENV] Security setting prevent interrogation of system classpaths.");
            }
            String className = clazz.getName();
            try {
                ClassLoader classLoader = getClassLoader(clazz);
                logDiagnostic(new StringBuffer("[ENV] Class ").append(className).append(" was loaded via classloader ").append(objectId(classLoader)).toString());
                logHierarchy(new StringBuffer("[ENV] Ancestry of classloader which loaded ").append(className).append(" is ").toString(), classLoader);
            } catch (SecurityException e2) {
                logDiagnostic(new StringBuffer("[ENV] Security forbids determining the classloader for ").append(className).toString());
            }
        }
    }

    private static void logHierarchy(String prefix, ClassLoader classLoader) {
        if (isDiagnosticsEnabled()) {
            if (classLoader != null) {
                logDiagnostic(new StringBuffer(String.valueOf(prefix)).append(objectId(classLoader)).append(" == '").append(classLoader.toString()).append("'").toString());
            }
            try {
                ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
                if (classLoader != null) {
                    StringBuffer buf = new StringBuffer(new StringBuffer(String.valueOf(prefix)).append("ClassLoader tree:").toString());
                    do {
                        buf.append(objectId(classLoader));
                        if (classLoader == systemClassLoader) {
                            buf.append(" (SYSTEM) ");
                        }
                        try {
                            classLoader = classLoader.getParent();
                            buf.append(" --> ");
                        } catch (SecurityException e) {
                            buf.append(" --> SECRET");
                        }
                    } while (classLoader != null);
                    buf.append("BOOT");
                    logDiagnostic(buf.toString());
                }
            } catch (SecurityException e2) {
                logDiagnostic(new StringBuffer(String.valueOf(prefix)).append("Security forbids determining the system classloader.").toString());
            }
        }
    }

    public static String objectId(Object o) {
        if (o == null) {
            return "null";
        }
        return new StringBuffer(String.valueOf(o.getClass().getName())).append("@").append(System.identityHashCode(o)).toString();
    }
}
