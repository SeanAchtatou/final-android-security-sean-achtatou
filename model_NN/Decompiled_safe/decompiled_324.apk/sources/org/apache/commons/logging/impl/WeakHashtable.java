package org.apache.commons.logging.impl;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public final class WeakHashtable extends Hashtable {
    private static final int MAX_CHANGES_BEFORE_PURGE = 100;
    private static final int PARTIAL_PURGE_COUNT = 10;
    private int changeCount = 0;
    private ReferenceQueue queue = new ReferenceQueue();

    public boolean containsKey(Object key) {
        return super.containsKey(new Referenced((AnonymousClass2) null, key));
    }

    public Enumeration elements() {
        purge();
        return super.elements();
    }

    public Set entrySet() {
        purge();
        Set<Map.Entry> referencedEntries = super.entrySet();
        Set unreferencedEntries = new HashSet();
        for (Map.Entry entry : referencedEntries) {
            Object key = Referenced.access$0((Referenced) entry.getKey());
            Object value = entry.getValue();
            if (key != null) {
                unreferencedEntries.add(new Entry(null, key, value));
            }
        }
        return unreferencedEntries;
    }

    public Object get(Object key) {
        return super.get(new Referenced((AnonymousClass2) null, key));
    }

    public Enumeration keys() {
        purge();
        return new Enumeration(super.keys()) {
            private final Enumeration val$enumer;

            {
                this.val$enumer = val$enumer;
            }

            public boolean hasMoreElements() {
                return this.val$enumer.hasMoreElements();
            }

            public Object nextElement() {
                return Referenced.access$0((Referenced) this.val$enumer.nextElement());
            }
        };
    }

    public Set keySet() {
        purge();
        Set<Referenced> referencedKeys = super.keySet();
        Set unreferencedKeys = new HashSet();
        for (Referenced referenceKey : referencedKeys) {
            Object keyValue = Referenced.access$0(referenceKey);
            if (keyValue != null) {
                unreferencedKeys.add(keyValue);
            }
        }
        return unreferencedKeys;
    }

    public Object put(Object key, Object value) {
        if (key == null) {
            throw new NullPointerException("Null keys are not allowed");
        } else if (value == null) {
            throw new NullPointerException("Null values are not allowed");
        } else {
            int i = this.changeCount;
            this.changeCount = i + 1;
            if (i > 100) {
                purge();
                this.changeCount = 0;
            } else if (this.changeCount % 10 == 0) {
                purgeOne();
            }
            return super.put(new Referenced(null, key, this.queue), value);
        }
    }

    public void putAll(Map t) {
        if (t != null) {
            for (Map.Entry entry : t.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public Collection values() {
        purge();
        return super.values();
    }

    public Object remove(Object key) {
        int i = this.changeCount;
        this.changeCount = i + 1;
        if (i > 100) {
            purge();
            this.changeCount = 0;
        } else if (this.changeCount % 10 == 0) {
            purgeOne();
        }
        return super.remove(new Referenced((AnonymousClass2) null, key));
    }

    public boolean isEmpty() {
        purge();
        return super.isEmpty();
    }

    public int size() {
        purge();
        return super.size();
    }

    public String toString() {
        purge();
        return super.toString();
    }

    /* access modifiers changed from: protected */
    public void rehash() {
        purge();
        super.rehash();
    }

    private void purge() {
        synchronized (this.queue) {
            while (true) {
                WeakKey key = (WeakKey) this.queue.poll();
                if (key != null) {
                    super.remove(WeakKey.access$0(key));
                }
            }
        }
    }

    private void purgeOne() {
        synchronized (this.queue) {
            WeakKey key = (WeakKey) this.queue.poll();
            if (key != null) {
                super.remove(WeakKey.access$0(key));
            }
        }
    }

    private static final class Entry implements Map.Entry {
        private final Object key;
        private final Object value;

        private Entry(Object key2, Object value2) {
            this.key = key2;
            this.value = value2;
        }

        Entry(AnonymousClass2 $0, Object $1, Object $2) {
            this($1, $2);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
            if (r3 == false) goto L_0x002a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
            if (r3 != false) goto L_0x001b;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 0
                r2 = 0
                if (r7 == 0) goto L_0x002b
                boolean r3 = r7 instanceof java.util.Map.Entry
                if (r3 == 0) goto L_0x002b
                r0 = r7
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                r1 = r0
                java.lang.Object r3 = r6.getKey()
                if (r3 != 0) goto L_0x002c
                java.lang.Object r3 = r1.getKey()
                if (r3 == 0) goto L_0x001b
                r3 = r5
            L_0x0019:
                if (r3 == 0) goto L_0x002a
            L_0x001b:
                java.lang.Object r3 = r6.getValue()
                if (r3 != 0) goto L_0x0039
                java.lang.Object r3 = r1.getValue()
                if (r3 == 0) goto L_0x0046
                r3 = r5
            L_0x0028:
                if (r3 != 0) goto L_0x0046
            L_0x002a:
                r2 = r5
            L_0x002b:
                return r2
            L_0x002c:
                java.lang.Object r3 = r6.getKey()
                java.lang.Object r4 = r1.getKey()
                boolean r3 = r3.equals(r4)
                goto L_0x0019
            L_0x0039:
                java.lang.Object r3 = r6.getValue()
                java.lang.Object r4 = r1.getValue()
                boolean r3 = r3.equals(r4)
                goto L_0x0028
            L_0x0046:
                r3 = 1
                r2 = r3
                goto L_0x002b
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.impl.WeakHashtable.Entry.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return (getKey() == null ? 0 : getKey().hashCode()) ^ (getValue() == null ? 0 : getValue().hashCode());
        }

        public Object setValue(Object value2) {
            throw new UnsupportedOperationException("Entry.setValue is not supported.");
        }

        public Object getValue() {
            return this.value;
        }

        public Object getKey() {
            return this.key;
        }
    }

    private static final class Referenced {
        private final int hashCode;
        private final WeakReference reference;

        private Referenced(Object referant) {
            this.reference = new WeakReference(referant);
            this.hashCode = referant.hashCode();
        }

        Referenced(AnonymousClass2 $0, Object $1) {
            this($1);
        }

        private Referenced(Object key, ReferenceQueue queue) {
            this.reference = new WeakKey(null, key, queue, this);
            this.hashCode = key.hashCode();
        }

        Referenced(AnonymousClass2 $0, Object $1, ReferenceQueue $2) {
            this($1, $2);
        }

        public int hashCode() {
            return this.hashCode;
        }

        static Object access$0(Referenced $0) {
            return $0.getValue();
        }

        private Object getValue() {
            return this.reference.get();
        }

        public boolean equals(Object o) {
            boolean result;
            if (!(o instanceof Referenced)) {
                return false;
            }
            Referenced otherKey = (Referenced) o;
            Object thisKeyValue = getValue();
            Object otherKeyValue = otherKey.getValue();
            if (thisKeyValue != null) {
                return thisKeyValue.equals(otherKeyValue);
            }
            if (otherKeyValue != null) {
                result = false;
            } else {
                result = true;
            }
            if (!result) {
                return result;
            }
            if (hashCode() != otherKey.hashCode()) {
                return false;
            }
            return true;
        }
    }

    private static final class WeakKey extends WeakReference {
        private final Referenced referenced;

        WeakKey(AnonymousClass2 $0, Object $1, ReferenceQueue $2, Referenced $3) {
            this($1, $2, $3);
        }

        private WeakKey(Object key, ReferenceQueue queue, Referenced referenced2) {
            super(key, queue);
            this.referenced = referenced2;
        }

        static Referenced access$0(WeakKey $0) {
            return $0.getReferenced();
        }

        private Referenced getReferenced() {
            return this.referenced;
        }
    }
}
