package org.apache.commons.logging.impl;

import java.io.Serializable;
import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

public class Log4JLogger implements Log, Serializable {
    private static final String FQCN;
    static Class class$org$apache$commons$logging$impl$Log4JLogger;
    static Class class$org$apache$log4j$Level;
    static Class class$org$apache$log4j$Priority;
    private static Priority traceLevel;
    private transient Logger logger = null;
    private String name = null;

    static Class class$(String class$) {
        try {
            return Class.forName(class$);
        } catch (ClassNotFoundException forName) {
            throw new NoClassDefFoundError(forName.getMessage());
        }
    }

    static {
        Class class$;
        Class class$2;
        Class class$3;
        Class class$4;
        if (class$org$apache$commons$logging$impl$Log4JLogger != null) {
            class$ = class$org$apache$commons$logging$impl$Log4JLogger;
        } else {
            class$ = class$("org.apache.commons.logging.impl.Log4JLogger");
            class$org$apache$commons$logging$impl$Log4JLogger = class$;
        }
        FQCN = class$.getName();
        if (class$org$apache$log4j$Priority != null) {
            class$2 = class$org$apache$log4j$Priority;
        } else {
            class$2 = class$("org.apache.log4j.Priority");
            class$org$apache$log4j$Priority = class$2;
        }
        if (class$org$apache$log4j$Level != null) {
            class$3 = class$org$apache$log4j$Level;
        } else {
            class$3 = class$("org.apache.log4j.Level");
            class$org$apache$log4j$Level = class$3;
        }
        if (!class$2.isAssignableFrom(class$3)) {
            throw new InstantiationError("Log4J 1.2 not available");
        }
        try {
            if (class$org$apache$log4j$Level != null) {
                class$4 = class$org$apache$log4j$Level;
            } else {
                class$4 = class$("org.apache.log4j.Level");
                class$org$apache$log4j$Level = class$4;
            }
            traceLevel = (Priority) class$4.getDeclaredField("TRACE").get(null);
        } catch (Exception e) {
            traceLevel = Priority.DEBUG;
        }
    }

    public Log4JLogger() {
    }

    public Log4JLogger(String name2) {
        this.name = name2;
        this.logger = getLogger();
    }

    public Log4JLogger(Logger logger2) {
        this.name = logger2.getName();
        this.logger = logger2;
    }

    public void trace(Object message) {
        getLogger().log(FQCN, traceLevel, message, (Throwable) null);
    }

    public void trace(Object message, Throwable t) {
        getLogger().log(FQCN, traceLevel, message, t);
    }

    public void debug(Object message) {
        getLogger().log(FQCN, Priority.DEBUG, message, (Throwable) null);
    }

    public void debug(Object message, Throwable t) {
        getLogger().log(FQCN, Priority.DEBUG, message, t);
    }

    public void info(Object message) {
        getLogger().log(FQCN, Priority.INFO, message, (Throwable) null);
    }

    public void info(Object message, Throwable t) {
        getLogger().log(FQCN, Priority.INFO, message, t);
    }

    public void warn(Object message) {
        getLogger().log(FQCN, Priority.WARN, message, (Throwable) null);
    }

    public void warn(Object message, Throwable t) {
        getLogger().log(FQCN, Priority.WARN, message, t);
    }

    public void error(Object message) {
        getLogger().log(FQCN, Priority.ERROR, message, (Throwable) null);
    }

    public void error(Object message, Throwable t) {
        getLogger().log(FQCN, Priority.ERROR, message, t);
    }

    public void fatal(Object message) {
        getLogger().log(FQCN, Priority.FATAL, message, (Throwable) null);
    }

    public void fatal(Object message, Throwable t) {
        getLogger().log(FQCN, Priority.FATAL, message, t);
    }

    public Logger getLogger() {
        if (this.logger == null) {
            this.logger = Logger.getLogger(this.name);
        }
        return this.logger;
    }

    public boolean isDebugEnabled() {
        return getLogger().isDebugEnabled();
    }

    public boolean isErrorEnabled() {
        return getLogger().isEnabledFor(Priority.ERROR);
    }

    public boolean isFatalEnabled() {
        return getLogger().isEnabledFor(Priority.FATAL);
    }

    public boolean isInfoEnabled() {
        return getLogger().isInfoEnabled();
    }

    public boolean isTraceEnabled() {
        return getLogger().isEnabledFor(traceLevel);
    }

    public boolean isWarnEnabled() {
        return getLogger().isEnabledFor(Priority.WARN);
    }
}
