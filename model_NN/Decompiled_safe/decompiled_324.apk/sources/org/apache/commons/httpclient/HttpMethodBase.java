package org.apache.commons.httpclient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.util.Collection;
import org.apache.commons.httpclient.auth.AuthState;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.cookie.CookieVersionSupport;
import org.apache.commons.httpclient.cookie.MalformedCookieException;
import org.apache.commons.httpclient.cookie.RFC2109Spec;
import org.apache.commons.httpclient.cookie.RFC2965Spec;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.util.ExceptionUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class HttpMethodBase implements HttpMethod {
    private static final int DEFAULT_INITIAL_BUFFER_SIZE = 4096;
    private static final Log LOG;
    private static final int RESPONSE_WAIT_TIME_MS = 3000;
    static Class class$org$apache$commons$httpclient$HttpMethodBase;
    private volatile boolean aborted = false;
    private boolean connectionCloseForced = false;
    private CookieSpec cookiespec = null;
    private boolean doAuthentication = true;
    protected HttpVersion effectiveVersion = null;
    private boolean followRedirects = false;
    private AuthState hostAuthState = new AuthState();
    private HttpHost httphost = null;
    private MethodRetryHandler methodRetryHandler;
    private HttpMethodParams params = new HttpMethodParams();
    private String path = null;
    private AuthState proxyAuthState = new AuthState();
    private String queryString = null;
    private int recoverableExceptionCount = 0;
    private HeaderGroup requestHeaders = new HeaderGroup();
    private boolean requestSent = false;
    private byte[] responseBody = null;
    private HttpConnection responseConnection = null;
    private HeaderGroup responseHeaders = new HeaderGroup();
    private InputStream responseStream = null;
    private HeaderGroup responseTrailerHeaders = new HeaderGroup();
    protected StatusLine statusLine = null;
    private boolean used = false;

    public abstract String getName();

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpMethodBase == null) {
            cls = class$("org.apache.commons.httpclient.HttpMethodBase");
            class$org$apache$commons$httpclient$HttpMethodBase = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpMethodBase;
        }
        LOG = LogFactory.getLog(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public HttpMethodBase() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0058, code lost:
        if (r7.equals("") != false) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HttpMethodBase(java.lang.String r7) throws java.lang.IllegalArgumentException, java.lang.IllegalStateException {
        /*
            r6 = this;
            r5 = 1
            r4 = 0
            r3 = 0
            r6.<init>()
            org.apache.commons.httpclient.HeaderGroup r2 = new org.apache.commons.httpclient.HeaderGroup
            r2.<init>()
            r6.requestHeaders = r2
            r6.statusLine = r3
            org.apache.commons.httpclient.HeaderGroup r2 = new org.apache.commons.httpclient.HeaderGroup
            r2.<init>()
            r6.responseHeaders = r2
            org.apache.commons.httpclient.HeaderGroup r2 = new org.apache.commons.httpclient.HeaderGroup
            r2.<init>()
            r6.responseTrailerHeaders = r2
            r6.path = r3
            r6.queryString = r3
            r6.responseStream = r3
            r6.responseConnection = r3
            r6.responseBody = r3
            r6.followRedirects = r4
            r6.doAuthentication = r5
            org.apache.commons.httpclient.params.HttpMethodParams r2 = new org.apache.commons.httpclient.params.HttpMethodParams
            r2.<init>()
            r6.params = r2
            org.apache.commons.httpclient.auth.AuthState r2 = new org.apache.commons.httpclient.auth.AuthState
            r2.<init>()
            r6.hostAuthState = r2
            org.apache.commons.httpclient.auth.AuthState r2 = new org.apache.commons.httpclient.auth.AuthState
            r2.<init>()
            r6.proxyAuthState = r2
            r6.used = r4
            r6.recoverableExceptionCount = r4
            r6.httphost = r3
            r6.connectionCloseForced = r4
            r6.effectiveVersion = r3
            r6.aborted = r4
            r6.requestSent = r4
            r6.cookiespec = r3
            if (r7 == 0) goto L_0x005a
            java.lang.String r2 = ""
            boolean r2 = r7.equals(r2)     // Catch:{ URIException -> 0x006e }
            if (r2 == 0) goto L_0x005c
        L_0x005a:
            java.lang.String r7 = "/"
        L_0x005c:
            org.apache.commons.httpclient.params.HttpMethodParams r2 = r6.getParams()     // Catch:{ URIException -> 0x006e }
            java.lang.String r0 = r2.getUriCharset()     // Catch:{ URIException -> 0x006e }
            org.apache.commons.httpclient.URI r2 = new org.apache.commons.httpclient.URI     // Catch:{ URIException -> 0x006e }
            r3 = 1
            r2.<init>(r7, r3, r0)     // Catch:{ URIException -> 0x006e }
            r6.setURI(r2)     // Catch:{ URIException -> 0x006e }
            return
        L_0x006e:
            r2 = move-exception
            r1 = r2
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "Invalid uri '"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.StringBuffer r3 = r3.append(r7)
            java.lang.String r4 = "': "
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r4 = r1.getMessage()
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.HttpMethodBase.<init>(java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    public URI getURI() throws URIException {
        StringBuffer buffer = new StringBuffer();
        if (this.httphost != null) {
            buffer.append(this.httphost.getProtocol().getScheme());
            buffer.append("://");
            buffer.append(this.httphost.getHostName());
            int port = this.httphost.getPort();
            if (!(port == -1 || port == this.httphost.getProtocol().getDefaultPort())) {
                buffer.append(":");
                buffer.append(port);
            }
        }
        buffer.append(this.path);
        if (this.queryString != null) {
            buffer.append('?');
            buffer.append(this.queryString);
        }
        return new URI(buffer.toString(), true, getParams().getUriCharset());
    }

    public void setURI(URI uri) throws URIException {
        if (uri.isAbsoluteURI()) {
            this.httphost = new HttpHost(uri);
        }
        setPath(uri.getPath() == null ? CookieSpec.PATH_DELIM : uri.getEscapedPath());
        setQueryString(uri.getEscapedQuery());
    }

    public void setFollowRedirects(boolean followRedirects2) {
        this.followRedirects = followRedirects2;
    }

    public boolean getFollowRedirects() {
        return this.followRedirects;
    }

    public void setHttp11(boolean http11) {
        if (http11) {
            this.params.setVersion(HttpVersion.HTTP_1_1);
        } else {
            this.params.setVersion(HttpVersion.HTTP_1_0);
        }
    }

    public boolean getDoAuthentication() {
        return this.doAuthentication;
    }

    public void setDoAuthentication(boolean doAuthentication2) {
        this.doAuthentication = doAuthentication2;
    }

    public boolean isHttp11() {
        return this.params.getVersion().equals(HttpVersion.HTTP_1_1);
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public void addRequestHeader(Header header) {
        LOG.trace("HttpMethodBase.addRequestHeader(Header)");
        if (header == null) {
            LOG.debug("null header value ignored");
        } else {
            getRequestHeaderGroup().addHeader(header);
        }
    }

    public void addResponseFooter(Header footer) {
        getResponseTrailerHeaderGroup().addHeader(footer);
    }

    public String getPath() {
        return (this.path == null || this.path.equals("")) ? CookieSpec.PATH_DELIM : this.path;
    }

    public void setQueryString(String queryString2) {
        this.queryString = queryString2;
    }

    public void setQueryString(NameValuePair[] params2) {
        LOG.trace("enter HttpMethodBase.setQueryString(NameValuePair[])");
        this.queryString = EncodingUtil.formUrlEncode(params2, StringEncodings.UTF8);
    }

    public String getQueryString() {
        return this.queryString;
    }

    public void setRequestHeader(String headerName, String headerValue) {
        setRequestHeader(new Header(headerName, headerValue));
    }

    public void setRequestHeader(Header header) {
        Header[] headers = getRequestHeaderGroup().getHeaders(header.getName());
        for (Header removeHeader : headers) {
            getRequestHeaderGroup().removeHeader(removeHeader);
        }
        getRequestHeaderGroup().addHeader(header);
    }

    public Header getRequestHeader(String headerName) {
        if (headerName == null) {
            return null;
        }
        return getRequestHeaderGroup().getCondensedHeader(headerName);
    }

    public Header[] getRequestHeaders() {
        return getRequestHeaderGroup().getAllHeaders();
    }

    public Header[] getRequestHeaders(String headerName) {
        return getRequestHeaderGroup().getHeaders(headerName);
    }

    /* access modifiers changed from: protected */
    public HeaderGroup getRequestHeaderGroup() {
        return this.requestHeaders;
    }

    /* access modifiers changed from: protected */
    public HeaderGroup getResponseTrailerHeaderGroup() {
        return this.responseTrailerHeaders;
    }

    /* access modifiers changed from: protected */
    public HeaderGroup getResponseHeaderGroup() {
        return this.responseHeaders;
    }

    public Header[] getResponseHeaders(String headerName) {
        return getResponseHeaderGroup().getHeaders(headerName);
    }

    public int getStatusCode() {
        return this.statusLine.getStatusCode();
    }

    public StatusLine getStatusLine() {
        return this.statusLine;
    }

    private boolean responseAvailable() {
        return (this.responseBody == null && this.responseStream == null) ? false : true;
    }

    public Header[] getResponseHeaders() {
        return getResponseHeaderGroup().getAllHeaders();
    }

    public Header getResponseHeader(String headerName) {
        if (headerName == null) {
            return null;
        }
        return getResponseHeaderGroup().getCondensedHeader(headerName);
    }

    public long getResponseContentLength() {
        Header[] headers = getResponseHeaderGroup().getHeaders("Content-Length");
        if (headers.length == 0) {
            return -1;
        }
        if (headers.length > 1) {
            LOG.warn("Multiple content-length headers detected");
        }
        int i = headers.length - 1;
        while (i >= 0) {
            try {
                return Long.parseLong(headers[i].getValue());
            } catch (NumberFormatException e) {
                NumberFormatException e2 = e;
                if (LOG.isWarnEnabled()) {
                    LOG.warn(new StringBuffer().append("Invalid content-length value: ").append(e2.getMessage()).toString());
                }
                i--;
            }
        }
        return -1;
    }

    public byte[] getResponseBody() throws IOException {
        InputStream instream;
        if (this.responseBody == null && (instream = getResponseBodyAsStream()) != null) {
            long contentLength = getResponseContentLength();
            if (contentLength > 2147483647L) {
                throw new IOException(new StringBuffer().append("Content too large to be buffered: ").append(contentLength).append(" bytes").toString());
            }
            int limit = getParams().getIntParameter(HttpMethodParams.BUFFER_WARN_TRIGGER_LIMIT, 1048576);
            if (contentLength == -1 || contentLength > ((long) limit)) {
                LOG.warn("Going to buffer response body of large or unknown size. Using getResponseBodyAsStream instead is recommended.");
            }
            LOG.debug("Buffering response body");
            ByteArrayOutputStream outstream = new ByteArrayOutputStream(contentLength > 0 ? (int) contentLength : DEFAULT_INITIAL_BUFFER_SIZE);
            byte[] buffer = new byte[DEFAULT_INITIAL_BUFFER_SIZE];
            while (true) {
                int len = instream.read(buffer);
                if (len <= 0) {
                    break;
                }
                outstream.write(buffer, 0, len);
            }
            outstream.close();
            setResponseStream(null);
            this.responseBody = outstream.toByteArray();
        }
        return this.responseBody;
    }

    public byte[] getResponseBody(int maxlen) throws IOException {
        InputStream instream;
        if (maxlen < 0) {
            throw new IllegalArgumentException("maxlen must be positive");
        }
        if (this.responseBody == null && (instream = getResponseBodyAsStream()) != null) {
            long contentLength = getResponseContentLength();
            if (contentLength == -1 || contentLength <= ((long) maxlen)) {
                LOG.debug("Buffering response body");
                ByteArrayOutputStream rawdata = new ByteArrayOutputStream(contentLength > 0 ? (int) contentLength : DEFAULT_INITIAL_BUFFER_SIZE);
                byte[] buffer = new byte[2048];
                int pos = 0;
                do {
                    int len = instream.read(buffer, 0, Math.min(buffer.length, maxlen - pos));
                    if (len == -1) {
                        break;
                    }
                    rawdata.write(buffer, 0, len);
                    pos += len;
                } while (pos < maxlen);
                setResponseStream(null);
                if (pos != maxlen || instream.read() == -1) {
                    this.responseBody = rawdata.toByteArray();
                } else {
                    throw new HttpContentTooLargeException(new StringBuffer().append("Content-Length not known but larger than ").append(maxlen).toString(), maxlen);
                }
            } else {
                throw new HttpContentTooLargeException(new StringBuffer().append("Content-Length is ").append(contentLength).toString(), maxlen);
            }
        }
        return this.responseBody;
    }

    public InputStream getResponseBodyAsStream() throws IOException {
        if (this.responseStream != null) {
            return this.responseStream;
        }
        if (this.responseBody == null) {
            return null;
        }
        InputStream byteResponseStream = new ByteArrayInputStream(this.responseBody);
        LOG.debug("re-creating response stream from byte array");
        return byteResponseStream;
    }

    public String getResponseBodyAsString() throws IOException {
        byte[] rawdata = null;
        if (responseAvailable()) {
            rawdata = getResponseBody();
        }
        if (rawdata != null) {
            return EncodingUtil.getString(rawdata, getResponseCharSet());
        }
        return null;
    }

    public String getResponseBodyAsString(int maxlen) throws IOException {
        if (maxlen < 0) {
            throw new IllegalArgumentException("maxlen must be positive");
        }
        byte[] rawdata = null;
        if (responseAvailable()) {
            rawdata = getResponseBody(maxlen);
        }
        if (rawdata != null) {
            return EncodingUtil.getString(rawdata, getResponseCharSet());
        }
        return null;
    }

    public Header[] getResponseFooters() {
        return getResponseTrailerHeaderGroup().getAllHeaders();
    }

    public Header getResponseFooter(String footerName) {
        if (footerName == null) {
            return null;
        }
        return getResponseTrailerHeaderGroup().getCondensedHeader(footerName);
    }

    /* access modifiers changed from: protected */
    public void setResponseStream(InputStream responseStream2) {
        this.responseStream = responseStream2;
    }

    /* access modifiers changed from: protected */
    public InputStream getResponseStream() {
        return this.responseStream;
    }

    public String getStatusText() {
        return this.statusLine.getReasonPhrase();
    }

    public void setStrictMode(boolean strictMode) {
        if (strictMode) {
            this.params.makeStrict();
        } else {
            this.params.makeLenient();
        }
    }

    public boolean isStrictMode() {
        return false;
    }

    public void addRequestHeader(String headerName, String headerValue) {
        addRequestHeader(new Header(headerName, headerValue));
    }

    /* access modifiers changed from: protected */
    public boolean isConnectionCloseForced() {
        return this.connectionCloseForced;
    }

    /* access modifiers changed from: protected */
    public void setConnectionCloseForced(boolean b) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer().append("Force-close connection: ").append(b).toString());
        }
        this.connectionCloseForced = b;
    }

    /* access modifiers changed from: protected */
    public boolean shouldCloseConnection(HttpConnection conn) {
        if (isConnectionCloseForced()) {
            LOG.debug("Should force-close connection.");
            return true;
        }
        Header connectionHeader = null;
        if (!conn.isTransparent()) {
            connectionHeader = this.responseHeaders.getFirstHeader("proxy-connection");
        }
        if (connectionHeader == null) {
            connectionHeader = this.responseHeaders.getFirstHeader("connection");
        }
        if (connectionHeader == null) {
            connectionHeader = this.requestHeaders.getFirstHeader("connection");
        }
        if (connectionHeader != null) {
            if (connectionHeader.getValue().equalsIgnoreCase("close")) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer().append("Should close connection in response to directive: ").append(connectionHeader.getValue()).toString());
                }
                return true;
            } else if (connectionHeader.getValue().equalsIgnoreCase("keep-alive")) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer().append("Should NOT close connection in response to directive: ").append(connectionHeader.getValue()).toString());
                }
                return false;
            } else if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer().append("Unknown directive: ").append(connectionHeader.toExternalForm()).toString());
            }
        }
        LOG.debug("Resorting to protocol version default close connection policy");
        if (this.effectiveVersion.greaterEquals(HttpVersion.HTTP_1_1)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer().append("Should NOT close connection, using ").append(this.effectiveVersion.toString()).toString());
            }
        } else if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer().append("Should close connection, using ").append(this.effectiveVersion.toString()).toString());
        }
        return this.effectiveVersion.lessEquals(HttpVersion.HTTP_1_0);
    }

    private void checkExecuteConditions(HttpState state, HttpConnection conn) throws HttpException {
        if (state == null) {
            throw new IllegalArgumentException("HttpState parameter may not be null");
        } else if (conn == null) {
            throw new IllegalArgumentException("HttpConnection parameter may not be null");
        } else if (this.aborted) {
            throw new IllegalStateException("Method has been aborted");
        } else if (!validate()) {
            throw new ProtocolException("HttpMethodBase object not valid");
        }
    }

    public int execute(HttpState state, HttpConnection conn) throws HttpException, IOException {
        LOG.trace("enter HttpMethodBase.execute(HttpState, HttpConnection)");
        this.responseConnection = conn;
        checkExecuteConditions(state, conn);
        this.statusLine = null;
        this.connectionCloseForced = false;
        conn.setLastResponseInputStream(null);
        if (this.effectiveVersion == null) {
            this.effectiveVersion = this.params.getVersion();
        }
        writeRequest(state, conn);
        this.requestSent = true;
        readResponse(state, conn);
        this.used = true;
        return this.statusLine.getStatusCode();
    }

    public void abort() {
        if (!this.aborted) {
            this.aborted = true;
            HttpConnection conn = this.responseConnection;
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean hasBeenUsed() {
        return this.used;
    }

    public void recycle() {
        LOG.trace("enter HttpMethodBase.recycle()");
        releaseConnection();
        this.path = null;
        this.followRedirects = false;
        this.doAuthentication = true;
        this.queryString = null;
        getRequestHeaderGroup().clear();
        getResponseHeaderGroup().clear();
        getResponseTrailerHeaderGroup().clear();
        this.statusLine = null;
        this.effectiveVersion = null;
        this.aborted = false;
        this.used = false;
        this.params = new HttpMethodParams();
        this.responseBody = null;
        this.recoverableExceptionCount = 0;
        this.connectionCloseForced = false;
        this.hostAuthState.invalidate();
        this.proxyAuthState.invalidate();
        this.cookiespec = null;
        this.requestSent = false;
    }

    public void releaseConnection() {
        try {
            if (this.responseStream != null) {
                try {
                    this.responseStream.close();
                } catch (IOException e) {
                }
            }
        } finally {
            ensureConnectionRelease();
        }
    }

    public void removeRequestHeader(String headerName) {
        Header[] headers = getRequestHeaderGroup().getHeaders(headerName);
        for (Header removeHeader : headers) {
            getRequestHeaderGroup().removeHeader(removeHeader);
        }
    }

    public void removeRequestHeader(Header header) {
        if (header != null) {
            getRequestHeaderGroup().removeHeader(header);
        }
    }

    public boolean validate() {
        return true;
    }

    private CookieSpec getCookieSpec(HttpState state) {
        if (this.cookiespec == null) {
            int i = state.getCookiePolicy();
            if (i == -1) {
                this.cookiespec = CookiePolicy.getCookieSpec(this.params.getCookiePolicy());
            } else {
                this.cookiespec = CookiePolicy.getSpecByPolicy(i);
            }
            this.cookiespec.setValidDateFormats((Collection) this.params.getParameter(HttpMethodParams.DATE_PATTERNS));
        }
        return this.cookiespec;
    }

    /* access modifiers changed from: protected */
    public void addCookieRequestHeader(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.addCookieRequestHeader(HttpState, HttpConnection)");
        Header[] cookieheaders = getRequestHeaderGroup().getHeaders("Cookie");
        for (Header cookieheader : cookieheaders) {
            if (cookieheader.isAutogenerated()) {
                getRequestHeaderGroup().removeHeader(cookieheader);
            }
        }
        CookieSpec matcher = getCookieSpec(state);
        String host = this.params.getVirtualHost();
        if (host == null) {
            host = conn.getHost();
        }
        Cookie[] cookies = matcher.match(host, conn.getPort(), getPath(), conn.isSecure(), state.getCookies());
        if (cookies != null && cookies.length > 0) {
            if (getParams().isParameterTrue(HttpMethodParams.SINGLE_COOKIE_HEADER)) {
                getRequestHeaderGroup().addHeader(new Header("Cookie", matcher.formatCookies(cookies), true));
            } else {
                for (Cookie formatCookie : cookies) {
                    getRequestHeaderGroup().addHeader(new Header("Cookie", matcher.formatCookie(formatCookie), true));
                }
            }
            if (matcher instanceof CookieVersionSupport) {
                CookieVersionSupport versupport = (CookieVersionSupport) matcher;
                int ver = versupport.getVersion();
                boolean needVersionHeader = false;
                for (Cookie version : cookies) {
                    if (ver != version.getVersion()) {
                        needVersionHeader = true;
                    }
                }
                if (needVersionHeader) {
                    getRequestHeaderGroup().addHeader(versupport.getVersionHeader());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addHostRequestHeader(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.addHostRequestHeader(HttpState, HttpConnection)");
        String host = this.params.getVirtualHost();
        if (host != null) {
            LOG.debug(new StringBuffer().append("Using virtual host name: ").append(host).toString());
        } else {
            host = conn.getHost();
        }
        int port = conn.getPort();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Adding Host request header");
        }
        if (conn.getProtocol().getDefaultPort() != port) {
            host = new StringBuffer().append(host).append(":").append(port).toString();
        }
        setRequestHeader("Host", host);
    }

    /* access modifiers changed from: protected */
    public void addProxyConnectionHeader(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.addProxyConnectionHeader(HttpState, HttpConnection)");
        if (!conn.isTransparent() && getRequestHeader("Proxy-Connection") == null) {
            addRequestHeader("Proxy-Connection", "Keep-Alive");
        }
    }

    /* access modifiers changed from: protected */
    public void addRequestHeaders(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.addRequestHeaders(HttpState, HttpConnection)");
        addUserAgentRequestHeader(state, conn);
        addHostRequestHeader(state, conn);
        addCookieRequestHeader(state, conn);
        addProxyConnectionHeader(state, conn);
    }

    /* access modifiers changed from: protected */
    public void addUserAgentRequestHeader(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.addUserAgentRequestHeaders(HttpState, HttpConnection)");
        if (getRequestHeader("User-Agent") == null) {
            String agent = (String) getParams().getParameter(HttpMethodParams.USER_AGENT);
            if (agent == null) {
                agent = "Jakarta Commons-HttpClient";
            }
            setRequestHeader("User-Agent", agent);
        }
    }

    /* access modifiers changed from: protected */
    public void checkNotUsed() throws IllegalStateException {
        if (this.used) {
            throw new IllegalStateException("Already used.");
        }
    }

    /* access modifiers changed from: protected */
    public void checkUsed() throws IllegalStateException {
        if (!this.used) {
            throw new IllegalStateException("Not Used.");
        }
    }

    protected static String generateRequestLine(HttpConnection connection, String name, String requestPath, String query, String version) {
        LOG.trace("enter HttpMethodBase.generateRequestLine(HttpConnection, String, String, String, String)");
        StringBuffer buf = new StringBuffer();
        buf.append(name);
        buf.append(" ");
        if (!connection.isTransparent()) {
            Protocol protocol = connection.getProtocol();
            buf.append(protocol.getScheme().toLowerCase());
            buf.append("://");
            buf.append(connection.getHost());
            if (!(connection.getPort() == -1 || connection.getPort() == protocol.getDefaultPort())) {
                buf.append(":");
                buf.append(connection.getPort());
            }
        }
        if (requestPath == null) {
            buf.append(CookieSpec.PATH_DELIM);
        } else {
            if (!connection.isTransparent() && !requestPath.startsWith(CookieSpec.PATH_DELIM)) {
                buf.append(CookieSpec.PATH_DELIM);
            }
            buf.append(requestPath);
        }
        if (query != null) {
            if (query.indexOf("?") != 0) {
                buf.append("?");
            }
            buf.append(query);
        }
        buf.append(" ");
        buf.append(version);
        buf.append("\r\n");
        return buf.toString();
    }

    /* access modifiers changed from: protected */
    public void processResponseBody(HttpState state, HttpConnection conn) {
    }

    /* access modifiers changed from: protected */
    public void processResponseHeaders(HttpState state, HttpConnection conn) {
        LOG.trace("enter HttpMethodBase.processResponseHeaders(HttpState, HttpConnection)");
        CookieSpec parser = getCookieSpec(state);
        processCookieHeaders(parser, getResponseHeaderGroup().getHeaders(RFC2109Spec.SET_COOKIE_KEY), state, conn);
        if ((parser instanceof CookieVersionSupport) && ((CookieVersionSupport) parser).getVersion() > 0) {
            processCookieHeaders(parser, getResponseHeaderGroup().getHeaders(RFC2965Spec.SET_COOKIE2_KEY), state, conn);
        }
    }

    /* access modifiers changed from: protected */
    public void processCookieHeaders(CookieSpec parser, Header[] headers, HttpState state, HttpConnection conn) {
        LOG.trace("enter HttpMethodBase.processCookieHeaders(Header[], HttpState, HttpConnection)");
        String host = this.params.getVirtualHost();
        if (host == null) {
            host = conn.getHost();
        }
        for (int i = 0; i < headers.length; i++) {
            Header header = headers[i];
            Cookie[] cookies = null;
            try {
                cookies = parser.parse(host, conn.getPort(), getPath(), conn.isSecure(), header);
            } catch (MalformedCookieException e) {
                MalformedCookieException e2 = e;
                if (LOG.isWarnEnabled()) {
                    LOG.warn(new StringBuffer().append("Invalid cookie header: \"").append(header.getValue()).append("\". ").append(e2.getMessage()).toString());
                }
            }
            if (cookies != null) {
                for (int j = 0; j < cookies.length; j++) {
                    Cookie cookie = cookies[j];
                    try {
                        parser.validate(host, conn.getPort(), getPath(), conn.isSecure(), cookie);
                        state.addCookie(cookie);
                        if (LOG.isDebugEnabled()) {
                            LOG.debug(new StringBuffer().append("Cookie accepted: \"").append(parser.formatCookie(cookie)).append("\"").toString());
                        }
                    } catch (MalformedCookieException e3) {
                        MalformedCookieException e4 = e3;
                        if (LOG.isWarnEnabled()) {
                            LOG.warn(new StringBuffer().append("Cookie rejected: \"").append(parser.formatCookie(cookie)).append("\". ").append(e4.getMessage()).toString());
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void processStatusLine(HttpState state, HttpConnection conn) {
    }

    /* access modifiers changed from: protected */
    public void readResponse(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.readResponse(HttpState, HttpConnection)");
        while (this.statusLine == null) {
            readStatusLine(state, conn);
            processStatusLine(state, conn);
            readResponseHeaders(state, conn);
            processResponseHeaders(state, conn);
            int status = this.statusLine.getStatusCode();
            if (status >= 100 && status < 200) {
                if (LOG.isInfoEnabled()) {
                    LOG.info(new StringBuffer().append("Discarding unexpected response: ").append(this.statusLine.toString()).toString());
                }
                this.statusLine = null;
            }
        }
        readResponseBody(state, conn);
        processResponseBody(state, conn);
    }

    /* access modifiers changed from: protected */
    public void readResponseBody(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.readResponseBody(HttpState, HttpConnection)");
        InputStream stream = readResponseBody(conn);
        if (stream == null) {
            responseBodyConsumed();
            return;
        }
        conn.setLastResponseInputStream(stream);
        setResponseStream(stream);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r12v0 */
    /* JADX WARN: Type inference failed for: r12v2 */
    /* JADX WARN: Type inference failed for: r12v3 */
    /* JADX WARN: Type inference failed for: r12v7 */
    /* JADX WARN: Type inference failed for: r12v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.InputStream readResponseBody(org.apache.commons.httpclient.HttpConnection r20) throws org.apache.commons.httpclient.HttpException, java.io.IOException {
        /*
            r19 = this;
            org.apache.commons.logging.Log r16 = org.apache.commons.httpclient.HttpMethodBase.LOG
            java.lang.String r17 = "enter HttpMethodBase.readResponseBody(HttpConnection)"
            r16.trace(r17)
            r16 = 0
            r0 = r16
            r1 = r19
            r1.responseBody = r0
            java.io.InputStream r9 = r20.getResponseInputStream()
            org.apache.commons.httpclient.Wire r16 = org.apache.commons.httpclient.Wire.CONTENT_WIRE
            boolean r16 = r16.enabled()
            if (r16 == 0) goto L_0x0027
            org.apache.commons.httpclient.WireLogInputStream r10 = new org.apache.commons.httpclient.WireLogInputStream
            org.apache.commons.httpclient.Wire r16 = org.apache.commons.httpclient.Wire.CONTENT_WIRE
            r0 = r10
            r1 = r9
            r2 = r16
            r0.<init>(r1, r2)
            r9 = r10
        L_0x0027:
            r0 = r19
            org.apache.commons.httpclient.StatusLine r0 = r0.statusLine
            r16 = r0
            int r16 = r16.getStatusCode()
            boolean r3 = canResponseHaveBody(r16)
            r12 = 0
            r0 = r19
            org.apache.commons.httpclient.HeaderGroup r0 = r0.responseHeaders
            r16 = r0
            java.lang.String r17 = "Transfer-Encoding"
            org.apache.commons.httpclient.Header r15 = r16.getFirstHeader(r17)
            if (r15 == 0) goto L_0x00fc
            java.lang.String r14 = r15.getValue()
            java.lang.String r16 = "chunked"
            r0 = r16
            r1 = r14
            boolean r16 = r0.equalsIgnoreCase(r1)
            if (r16 != 0) goto L_0x0081
            java.lang.String r16 = "identity"
            r0 = r16
            r1 = r14
            boolean r16 = r0.equalsIgnoreCase(r1)
            if (r16 != 0) goto L_0x0081
            org.apache.commons.logging.Log r16 = org.apache.commons.httpclient.HttpMethodBase.LOG
            boolean r16 = r16.isWarnEnabled()
            if (r16 == 0) goto L_0x0081
            org.apache.commons.logging.Log r16 = org.apache.commons.httpclient.HttpMethodBase.LOG
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "Unsupported transfer encoding: "
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r14
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r17 = r17.toString()
            r16.warn(r17)
        L_0x0081:
            org.apache.commons.httpclient.HeaderElement[] r6 = r15.getElements()
            int r11 = r6.length
            if (r11 <= 0) goto L_0x00ea
            java.lang.String r16 = "chunked"
            r17 = 1
            int r17 = r11 - r17
            r17 = r6[r17]
            java.lang.String r17 = r17.getName()
            boolean r16 = r16.equalsIgnoreCase(r17)
            if (r16 == 0) goto L_0x00ea
            org.apache.commons.httpclient.params.HttpConnectionParams r16 = r20.getParams()
            int r16 = r16.getSoTimeout()
            r0 = r20
            r1 = r16
            boolean r16 = r0.isResponseAvailable(r1)
            if (r16 == 0) goto L_0x00ce
            org.apache.commons.httpclient.ChunkedInputStream r12 = new org.apache.commons.httpclient.ChunkedInputStream
            r0 = r12
            r1 = r9
            r2 = r19
            r0.<init>(r1, r2)
        L_0x00b5:
            if (r3 != 0) goto L_0x0151
            r12 = 0
            r13 = r12
        L_0x00b9:
            if (r13 == 0) goto L_0x014e
            org.apache.commons.httpclient.AutoCloseInputStream r12 = new org.apache.commons.httpclient.AutoCloseInputStream
            org.apache.commons.httpclient.HttpMethodBase$1 r16 = new org.apache.commons.httpclient.HttpMethodBase$1
            r0 = r16
            r1 = r19
            r0.<init>(r1)
            r0 = r12
            r1 = r13
            r2 = r16
            r0.<init>(r1, r2)
        L_0x00cd:
            return r12
        L_0x00ce:
            org.apache.commons.httpclient.params.HttpMethodParams r16 = r19.getParams()
            java.lang.String r17 = "http.protocol.strict-transfer-encoding"
            boolean r16 = r16.isParameterTrue(r17)
            if (r16 == 0) goto L_0x00e2
            org.apache.commons.httpclient.ProtocolException r16 = new org.apache.commons.httpclient.ProtocolException
            java.lang.String r17 = "Chunk-encoded body declared but not sent"
            r16.<init>(r17)
            throw r16
        L_0x00e2:
            org.apache.commons.logging.Log r16 = org.apache.commons.httpclient.HttpMethodBase.LOG
            java.lang.String r17 = "Chunk-encoded body missing"
            r16.warn(r17)
            goto L_0x00b5
        L_0x00ea:
            org.apache.commons.logging.Log r16 = org.apache.commons.httpclient.HttpMethodBase.LOG
            java.lang.String r17 = "Response content is not chunk-encoded"
            r16.info(r17)
            r16 = 1
            r0 = r19
            r1 = r16
            r0.setConnectionCloseForced(r1)
            r12 = r9
            goto L_0x00b5
        L_0x00fc:
            long r7 = r19.getResponseContentLength()
            r16 = -1
            int r16 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r16 != 0) goto L_0x0147
            if (r3 == 0) goto L_0x0144
            r0 = r19
            org.apache.commons.httpclient.HttpVersion r0 = r0.effectiveVersion
            r16 = r0
            org.apache.commons.httpclient.HttpVersion r17 = org.apache.commons.httpclient.HttpVersion.HTTP_1_1
            boolean r16 = r16.greaterEquals(r17)
            if (r16 == 0) goto L_0x0144
            r0 = r19
            org.apache.commons.httpclient.HeaderGroup r0 = r0.responseHeaders
            r16 = r0
            java.lang.String r17 = "Connection"
            org.apache.commons.httpclient.Header r5 = r16.getFirstHeader(r17)
            r4 = 0
            if (r5 == 0) goto L_0x0129
            java.lang.String r4 = r5.getValue()
        L_0x0129:
            java.lang.String r16 = "close"
            r0 = r16
            r1 = r4
            boolean r16 = r0.equalsIgnoreCase(r1)
            if (r16 != 0) goto L_0x0144
            org.apache.commons.logging.Log r16 = org.apache.commons.httpclient.HttpMethodBase.LOG
            java.lang.String r17 = "Response content length is not known"
            r16.info(r17)
            r16 = 1
            r0 = r19
            r1 = r16
            r0.setConnectionCloseForced(r1)
        L_0x0144:
            r12 = r9
            goto L_0x00b5
        L_0x0147:
            org.apache.commons.httpclient.ContentLengthInputStream r12 = new org.apache.commons.httpclient.ContentLengthInputStream
            r12.<init>(r9, r7)
            goto L_0x00b5
        L_0x014e:
            r12 = r13
            goto L_0x00cd
        L_0x0151:
            r13 = r12
            goto L_0x00b9
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.HttpMethodBase.readResponseBody(org.apache.commons.httpclient.HttpConnection):java.io.InputStream");
    }

    /* access modifiers changed from: protected */
    public void readResponseHeaders(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.readResponseHeaders(HttpState,HttpConnection)");
        getResponseHeaderGroup().clear();
        getResponseHeaderGroup().setHeaders(HttpParser.parseHeaders(conn.getResponseInputStream(), getParams().getHttpElementCharset()));
    }

    /* access modifiers changed from: protected */
    public void readStatusLine(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.readStatusLine(HttpState, HttpConnection)");
        int maxGarbageLines = getParams().getIntParameter(HttpMethodParams.STATUS_LINE_GARBAGE_LIMIT, Integer.MAX_VALUE);
        int count = 0;
        while (true) {
            String s = conn.readLine(getParams().getHttpElementCharset());
            if (s == null && count == 0) {
                throw new NoHttpResponseException(new StringBuffer().append("The server ").append(conn.getHost()).append(" failed to respond").toString());
            }
            if (Wire.HEADER_WIRE.enabled()) {
                Wire.HEADER_WIRE.input(new StringBuffer().append(s).append("\r\n").toString());
            }
            if (s != null && StatusLine.startsWithHTTP(s)) {
                this.statusLine = new StatusLine(s);
                String versionStr = this.statusLine.getHttpVersion();
                if (!getParams().isParameterFalse(HttpMethodParams.UNAMBIGUOUS_STATUS_LINE) || !versionStr.equals("HTTP")) {
                    this.effectiveVersion = HttpVersion.parse(versionStr);
                    return;
                }
                getParams().setVersion(HttpVersion.HTTP_1_0);
                if (LOG.isWarnEnabled()) {
                    LOG.warn(new StringBuffer().append("Ambiguous status line (HTTP protocol version missing):").append(this.statusLine.toString()).toString());
                    return;
                }
                return;
            } else if (s != null && count < maxGarbageLines) {
                count++;
            }
        }
        throw new ProtocolException(new StringBuffer().append("The server ").append(conn.getHost()).append(" failed to respond with a valid HTTP response").toString());
    }

    /* access modifiers changed from: protected */
    public void writeRequest(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.writeRequest(HttpState, HttpConnection)");
        writeRequestLine(state, conn);
        writeRequestHeaders(state, conn);
        conn.writeLine();
        if (Wire.HEADER_WIRE.enabled()) {
            Wire.HEADER_WIRE.output("\r\n");
        }
        HttpVersion ver = getParams().getVersion();
        Header expectheader = getRequestHeader("Expect");
        String expectvalue = null;
        if (expectheader != null) {
            expectvalue = expectheader.getValue();
        }
        if (expectvalue != null && expectvalue.compareToIgnoreCase("100-continue") == 0) {
            if (ver.greaterEquals(HttpVersion.HTTP_1_1)) {
                conn.flushRequestOutputStream();
                int readTimeout = conn.getParams().getSoTimeout();
                try {
                    conn.setSocketTimeout(RESPONSE_WAIT_TIME_MS);
                    readStatusLine(state, conn);
                    processStatusLine(state, conn);
                    readResponseHeaders(state, conn);
                    processResponseHeaders(state, conn);
                    if (this.statusLine.getStatusCode() == 100) {
                        this.statusLine = null;
                        LOG.debug("OK to continue received");
                    } else {
                        conn.setSocketTimeout(readTimeout);
                        return;
                    }
                } catch (InterruptedIOException e) {
                    InterruptedIOException e2 = e;
                    if (!ExceptionUtil.isSocketTimeoutException(e2)) {
                        throw e2;
                    }
                    removeRequestHeader("Expect");
                    LOG.info("100 (continue) read timeout. Resume sending the request");
                } finally {
                    conn.setSocketTimeout(readTimeout);
                }
            } else {
                removeRequestHeader("Expect");
                LOG.info("'Expect: 100-continue' handshake is only supported by HTTP/1.1 or higher");
            }
        }
        writeRequestBody(state, conn);
        conn.flushRequestOutputStream();
    }

    /* access modifiers changed from: protected */
    public boolean writeRequestBody(HttpState state, HttpConnection conn) throws IOException, HttpException {
        return true;
    }

    /* access modifiers changed from: protected */
    public void writeRequestHeaders(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.writeRequestHeaders(HttpState,HttpConnection)");
        addRequestHeaders(state, conn);
        String charset = getParams().getHttpElementCharset();
        Header[] headers = getRequestHeaders();
        for (Header externalForm : headers) {
            String s = externalForm.toExternalForm();
            if (Wire.HEADER_WIRE.enabled()) {
                Wire.HEADER_WIRE.output(s);
            }
            conn.print(s, charset);
        }
    }

    /* access modifiers changed from: protected */
    public void writeRequestLine(HttpState state, HttpConnection conn) throws IOException, HttpException {
        LOG.trace("enter HttpMethodBase.writeRequestLine(HttpState, HttpConnection)");
        String requestLine = getRequestLine(conn);
        if (Wire.HEADER_WIRE.enabled()) {
            Wire.HEADER_WIRE.output(requestLine);
        }
        conn.print(requestLine, getParams().getHttpElementCharset());
    }

    private String getRequestLine(HttpConnection conn) {
        return generateRequestLine(conn, getName(), getPath(), getQueryString(), this.effectiveVersion.toString());
    }

    public HttpMethodParams getParams() {
        return this.params;
    }

    public void setParams(HttpMethodParams params2) {
        if (params2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = params2;
    }

    public HttpVersion getEffectiveVersion() {
        return this.effectiveVersion;
    }

    private static boolean canResponseHaveBody(int status) {
        LOG.trace("enter HttpMethodBase.canResponseHaveBody(int)");
        if ((status >= 100 && status <= 199) || status == 204 || status == 304) {
            return false;
        }
        return true;
    }

    public String getProxyAuthenticationRealm() {
        return this.proxyAuthState.getRealm();
    }

    public String getAuthenticationRealm() {
        return this.hostAuthState.getRealm();
    }

    /* access modifiers changed from: protected */
    public String getContentCharSet(Header contentheader) {
        NameValuePair param;
        LOG.trace("enter getContentCharSet( Header contentheader )");
        String charset = null;
        if (contentheader != null) {
            HeaderElement[] values = contentheader.getElements();
            if (values.length == 1 && (param = values[0].getParameterByName("charset")) != null) {
                charset = param.getValue();
            }
        }
        if (charset == null) {
            charset = getParams().getContentCharset();
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer().append("Default charset used: ").append(charset).toString());
            }
        }
        return charset;
    }

    public String getRequestCharSet() {
        return getContentCharSet(getRequestHeader("Content-Type"));
    }

    public String getResponseCharSet() {
        return getContentCharSet(getResponseHeader("Content-Type"));
    }

    public int getRecoverableExceptionCount() {
        return this.recoverableExceptionCount;
    }

    /* access modifiers changed from: protected */
    public void responseBodyConsumed() {
        this.responseStream = null;
        if (this.responseConnection != null) {
            this.responseConnection.setLastResponseInputStream(null);
            if (shouldCloseConnection(this.responseConnection)) {
                this.responseConnection.close();
            } else {
                try {
                    if (this.responseConnection.isResponseAvailable()) {
                        if (getParams().isParameterTrue(HttpMethodParams.WARN_EXTRA_INPUT)) {
                            LOG.warn("Extra response data detected - closing connection");
                        }
                        this.responseConnection.close();
                    }
                } catch (IOException e) {
                    LOG.warn(e.getMessage());
                    this.responseConnection.close();
                }
            }
        }
        this.connectionCloseForced = false;
        ensureConnectionRelease();
    }

    private void ensureConnectionRelease() {
        if (this.responseConnection != null) {
            this.responseConnection.releaseConnection();
            this.responseConnection = null;
        }
    }

    public HostConfiguration getHostConfiguration() {
        HostConfiguration hostconfig = new HostConfiguration();
        hostconfig.setHost(this.httphost);
        return hostconfig;
    }

    public void setHostConfiguration(HostConfiguration hostconfig) {
        if (hostconfig != null) {
            this.httphost = new HttpHost(hostconfig.getHost(), hostconfig.getPort(), hostconfig.getProtocol());
        } else {
            this.httphost = null;
        }
    }

    public MethodRetryHandler getMethodRetryHandler() {
        return this.methodRetryHandler;
    }

    public void setMethodRetryHandler(MethodRetryHandler handler) {
        this.methodRetryHandler = handler;
    }

    /* access modifiers changed from: package-private */
    public void fakeResponse(StatusLine statusline, HeaderGroup responseheaders, InputStream responseStream2) {
        this.used = true;
        this.statusLine = statusline;
        this.responseHeaders = responseheaders;
        this.responseBody = null;
        this.responseStream = responseStream2;
    }

    public AuthState getHostAuthState() {
        return this.hostAuthState;
    }

    public AuthState getProxyAuthState() {
        return this.proxyAuthState;
    }

    public boolean isAborted() {
        return this.aborted;
    }

    public boolean isRequestSent() {
        return this.requestSent;
    }
}
