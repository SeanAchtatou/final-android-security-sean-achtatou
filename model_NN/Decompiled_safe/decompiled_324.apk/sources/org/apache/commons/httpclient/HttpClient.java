package org.apache.commons.httpclient;

import java.io.IOException;
import java.security.Provider;
import java.security.Security;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HttpClient {
    private static final Log LOG;
    static Class class$org$apache$commons$httpclient$HttpClient;
    private HostConfiguration hostConfiguration;
    private HttpConnectionManager httpConnectionManager;
    private HttpClientParams params;
    private HttpState state;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpClient == null) {
            cls = class$("org.apache.commons.httpclient.HttpClient");
            class$org$apache$commons$httpclient$HttpClient = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpClient;
        }
        LOG = LogFactory.getLog(cls);
        if (LOG.isDebugEnabled()) {
            try {
                LOG.debug(new StringBuffer().append("Java version: ").append(System.getProperty("java.version")).toString());
                LOG.debug(new StringBuffer().append("Java vendor: ").append(System.getProperty("java.vendor")).toString());
                LOG.debug(new StringBuffer().append("Java class path: ").append(System.getProperty("java.class.path")).toString());
                LOG.debug(new StringBuffer().append("Operating system name: ").append(System.getProperty("os.name")).toString());
                LOG.debug(new StringBuffer().append("Operating system architecture: ").append(System.getProperty("os.arch")).toString());
                LOG.debug(new StringBuffer().append("Operating system version: ").append(System.getProperty("os.version")).toString());
                Provider[] providers = Security.getProviders();
                for (Provider provider : providers) {
                    LOG.debug(new StringBuffer().append(provider.getName()).append(" ").append(provider.getVersion()).append(": ").append(provider.getInfo()).toString());
                }
            } catch (SecurityException e) {
            }
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public HttpClient() {
        this(new HttpClientParams());
    }

    public HttpClient(HttpClientParams params2) {
        this.state = new HttpState();
        this.params = null;
        this.hostConfiguration = new HostConfiguration();
        if (params2 == null) {
            throw new IllegalArgumentException("Params may not be null");
        }
        this.params = params2;
        this.httpConnectionManager = null;
        Class clazz = params2.getConnectionManagerClass();
        if (clazz != null) {
            try {
                this.httpConnectionManager = (HttpConnectionManager) clazz.newInstance();
            } catch (Exception e) {
                LOG.warn("Error instantiating connection manager class, defaulting to SimpleHttpConnectionManager", e);
            }
        }
        if (this.httpConnectionManager == null) {
            this.httpConnectionManager = new SimpleHttpConnectionManager();
        }
        if (this.httpConnectionManager != null) {
            this.httpConnectionManager.getParams().setDefaults(this.params);
        }
    }

    public HttpClient(HttpClientParams params2, HttpConnectionManager httpConnectionManager2) {
        this.state = new HttpState();
        this.params = null;
        this.hostConfiguration = new HostConfiguration();
        if (httpConnectionManager2 == null) {
            throw new IllegalArgumentException("httpConnectionManager cannot be null");
        } else if (params2 == null) {
            throw new IllegalArgumentException("Params may not be null");
        } else {
            this.params = params2;
            this.httpConnectionManager = httpConnectionManager2;
            this.httpConnectionManager.getParams().setDefaults(this.params);
        }
    }

    public HttpClient(HttpConnectionManager httpConnectionManager2) {
        this(new HttpClientParams(), httpConnectionManager2);
    }

    public synchronized HttpState getState() {
        return this.state;
    }

    public synchronized void setState(HttpState state2) {
        this.state = state2;
    }

    public synchronized void setStrictMode(boolean strictMode) {
        if (strictMode) {
            this.params.makeStrict();
        } else {
            this.params.makeLenient();
        }
    }

    public synchronized boolean isStrictMode() {
        return false;
    }

    public synchronized void setTimeout(int newTimeoutInMilliseconds) {
        this.params.setSoTimeout(newTimeoutInMilliseconds);
    }

    public synchronized void setHttpConnectionFactoryTimeout(long timeout) {
        this.params.setConnectionManagerTimeout(timeout);
    }

    public synchronized void setConnectionTimeout(int newTimeoutInMilliseconds) {
        this.httpConnectionManager.getParams().setConnectionTimeout(newTimeoutInMilliseconds);
    }

    public int executeMethod(HttpMethod method) throws IOException, HttpException {
        LOG.trace("enter HttpClient.executeMethod(HttpMethod)");
        return executeMethod(null, method, null);
    }

    public int executeMethod(HostConfiguration hostConfiguration2, HttpMethod method) throws IOException, HttpException {
        LOG.trace("enter HttpClient.executeMethod(HostConfiguration,HttpMethod)");
        return executeMethod(hostConfiguration2, method, null);
    }

    public int executeMethod(HostConfiguration hostconfig, HttpMethod method, HttpState state2) throws IOException, HttpException {
        HttpState httpState;
        LOG.trace("enter HttpClient.executeMethod(HostConfiguration,HttpMethod,HttpState)");
        if (method == null) {
            throw new IllegalArgumentException("HttpMethod parameter may not be null");
        }
        HostConfiguration defaulthostconfig = getHostConfiguration();
        if (hostconfig == null) {
            hostconfig = defaulthostconfig;
        }
        URI uri = method.getURI();
        if (hostconfig == defaulthostconfig || uri.isAbsoluteURI()) {
            hostconfig = (HostConfiguration) hostconfig.clone();
            if (uri.isAbsoluteURI()) {
                hostconfig.setHost(uri);
            }
        }
        HttpConnectionManager httpConnectionManager2 = getHttpConnectionManager();
        HttpClientParams httpClientParams = this.params;
        if (state2 == null) {
            httpState = getState();
        } else {
            httpState = state2;
        }
        new HttpMethodDirector(httpConnectionManager2, hostconfig, httpClientParams, httpState).executeMethod(method);
        return method.getStatusCode();
    }

    public String getHost() {
        return this.hostConfiguration.getHost();
    }

    public int getPort() {
        return this.hostConfiguration.getPort();
    }

    public synchronized HostConfiguration getHostConfiguration() {
        return this.hostConfiguration;
    }

    public synchronized void setHostConfiguration(HostConfiguration hostConfiguration2) {
        this.hostConfiguration = hostConfiguration2;
    }

    public synchronized HttpConnectionManager getHttpConnectionManager() {
        return this.httpConnectionManager;
    }

    public synchronized void setHttpConnectionManager(HttpConnectionManager httpConnectionManager2) {
        this.httpConnectionManager = httpConnectionManager2;
        if (this.httpConnectionManager != null) {
            this.httpConnectionManager.getParams().setDefaults(this.params);
        }
    }

    public HttpClientParams getParams() {
        return this.params;
    }

    public void setParams(HttpClientParams params2) {
        if (params2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = params2;
    }
}
