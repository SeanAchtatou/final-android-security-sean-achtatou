package org.apache.commons.httpclient.auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClientError;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.util.ParameterFormatter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DigestScheme extends RFC2617Scheme {
    private static final char[] HEXADECIMAL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final Log LOG;
    private static final String NC = "00000001";
    private static final int QOP_AUTH = 2;
    private static final int QOP_AUTH_INT = 1;
    private static final int QOP_MISSING = 0;
    static Class class$org$apache$commons$httpclient$auth$DigestScheme;
    private String cnonce;
    private boolean complete;
    private final ParameterFormatter formatter;
    private int qopVariant;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$auth$DigestScheme == null) {
            cls = class$("org.apache.commons.httpclient.auth.DigestScheme");
            class$org$apache$commons$httpclient$auth$DigestScheme = cls;
        } else {
            cls = class$org$apache$commons$httpclient$auth$DigestScheme;
        }
        LOG = LogFactory.getLog(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public DigestScheme() {
        this.qopVariant = 0;
        this.complete = false;
        this.formatter = new ParameterFormatter();
    }

    public String getID() {
        String id = getRealm();
        String nonce = getParameter("nonce");
        if (nonce != null) {
            return new StringBuffer().append(id).append("-").append(nonce).toString();
        }
        return id;
    }

    public DigestScheme(String challenge) throws MalformedChallengeException {
        this();
        processChallenge(challenge);
    }

    public void processChallenge(String challenge) throws MalformedChallengeException {
        super.processChallenge(challenge);
        if (getParameter("realm") == null) {
            throw new MalformedChallengeException("missing realm in challange");
        } else if (getParameter("nonce") == null) {
            throw new MalformedChallengeException("missing nonce in challange");
        } else {
            boolean unsupportedQop = false;
            String qop = getParameter("qop");
            if (qop != null) {
                StringTokenizer tok = new StringTokenizer(qop, ",");
                while (true) {
                    if (!tok.hasMoreTokens()) {
                        break;
                    }
                    String variant = tok.nextToken().trim();
                    if (variant.equals("auth")) {
                        this.qopVariant = 2;
                        break;
                    } else if (variant.equals("auth-int")) {
                        this.qopVariant = 1;
                    } else {
                        unsupportedQop = true;
                        LOG.warn(new StringBuffer().append("Unsupported qop detected: ").append(variant).toString());
                    }
                }
            }
            if (!unsupportedQop || this.qopVariant != 0) {
                this.cnonce = createCnonce();
                this.complete = true;
                return;
            }
            throw new MalformedChallengeException("None of the qop methods is supported");
        }
    }

    public boolean isComplete() {
        if ("true".equalsIgnoreCase(getParameter("stale"))) {
            return false;
        }
        return this.complete;
    }

    public String getSchemeName() {
        return "digest";
    }

    public boolean isConnectionBased() {
        return false;
    }

    public String authenticate(Credentials credentials, String method, String uri) throws AuthenticationException {
        LOG.trace("enter DigestScheme.authenticate(Credentials, String, String)");
        try {
            UsernamePasswordCredentials usernamepassword = (UsernamePasswordCredentials) credentials;
            getParameters().put("methodname", method);
            getParameters().put("uri", uri);
            return new StringBuffer().append("Digest ").append(createDigestHeader(usernamepassword.getUserName(), createDigest(usernamepassword.getUserName(), usernamepassword.getPassword()))).toString();
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer().append("Credentials cannot be used for digest authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    public String authenticate(Credentials credentials, HttpMethod method) throws AuthenticationException {
        LOG.trace("enter DigestScheme.authenticate(Credentials, HttpMethod)");
        try {
            UsernamePasswordCredentials usernamepassword = (UsernamePasswordCredentials) credentials;
            getParameters().put("methodname", method.getName());
            StringBuffer buffer = new StringBuffer(method.getPath());
            String query = method.getQueryString();
            if (query != null) {
                if (query.indexOf("?") != 0) {
                    buffer.append("?");
                }
                buffer.append(method.getQueryString());
            }
            getParameters().put("uri", buffer.toString());
            if (getParameter("charset") == null) {
                getParameters().put("charset", method.getParams().getCredentialCharset());
            }
            return new StringBuffer().append("Digest ").append(createDigestHeader(usernamepassword.getUserName(), createDigest(usernamepassword.getUserName(), usernamepassword.getPassword()))).toString();
        } catch (ClassCastException e) {
            throw new InvalidCredentialsException(new StringBuffer().append("Credentials cannot be used for digest authentication: ").append(credentials.getClass().getName()).toString());
        }
    }

    private String createDigest(String uname, String pwd) throws AuthenticationException {
        String serverDigestValue;
        LOG.trace("enter DigestScheme.createDigest(String, String, Map)");
        String uri = getParameter("uri");
        String realm = getParameter("realm");
        String nonce = getParameter("nonce");
        String qop = getParameter("qop");
        String method = getParameter("methodname");
        String algorithm = getParameter("algorithm");
        if (algorithm == null) {
            algorithm = "MD5";
        }
        String charset = getParameter("charset");
        if (charset == null) {
            charset = "ISO-8859-1";
        }
        if (this.qopVariant == 1) {
            LOG.warn("qop=auth-int is not supported");
            throw new AuthenticationException("Unsupported qop in HTTP Digest authentication");
        }
        try {
            MessageDigest md5Helper = MessageDigest.getInstance("MD5");
            StringBuffer stringBuffer = new StringBuffer(uname.length() + realm.length() + pwd.length() + 2);
            stringBuffer.append(uname);
            stringBuffer.append(':');
            stringBuffer.append(realm);
            stringBuffer.append(':');
            stringBuffer.append(pwd);
            String a1 = stringBuffer.toString();
            if (algorithm.equals("MD5-sess")) {
                String tmp2 = encode(md5Helper.digest(EncodingUtil.getBytes(a1, charset)));
                StringBuffer stringBuffer2 = new StringBuffer(tmp2.length() + nonce.length() + this.cnonce.length() + 2);
                stringBuffer2.append(tmp2);
                stringBuffer2.append(':');
                stringBuffer2.append(nonce);
                stringBuffer2.append(':');
                stringBuffer2.append(this.cnonce);
                a1 = stringBuffer2.toString();
            } else if (!algorithm.equals("MD5")) {
                LOG.warn(new StringBuffer().append("Unhandled algorithm ").append(algorithm).append(" requested").toString());
            }
            String md5a1 = encode(md5Helper.digest(EncodingUtil.getBytes(a1, charset)));
            String a2 = null;
            if (this.qopVariant == 1) {
                LOG.error("Unhandled qop auth-int");
            } else {
                a2 = new StringBuffer().append(method).append(":").append(uri).toString();
            }
            String md5a2 = encode(md5Helper.digest(EncodingUtil.getAsciiBytes(a2)));
            if (this.qopVariant == 0) {
                LOG.debug("Using null qop method");
                StringBuffer stringBuffer3 = new StringBuffer(md5a1.length() + nonce.length() + md5a2.length());
                stringBuffer3.append(md5a1);
                stringBuffer3.append(':');
                stringBuffer3.append(nonce);
                stringBuffer3.append(':');
                stringBuffer3.append(md5a2);
                serverDigestValue = stringBuffer3.toString();
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer().append("Using qop method ").append(qop).toString());
                }
                String qopOption = getQopVariantString();
                StringBuffer stringBuffer4 = new StringBuffer(md5a1.length() + nonce.length() + NC.length() + this.cnonce.length() + qopOption.length() + md5a2.length() + 5);
                stringBuffer4.append(md5a1);
                stringBuffer4.append(':');
                stringBuffer4.append(nonce);
                stringBuffer4.append(':');
                stringBuffer4.append(NC);
                stringBuffer4.append(':');
                stringBuffer4.append(this.cnonce);
                stringBuffer4.append(':');
                stringBuffer4.append(qopOption);
                stringBuffer4.append(':');
                stringBuffer4.append(md5a2);
                serverDigestValue = stringBuffer4.toString();
            }
            return encode(md5Helper.digest(EncodingUtil.getAsciiBytes(serverDigestValue)));
        } catch (Exception e) {
            throw new AuthenticationException("Unsupported algorithm in HTTP Digest authentication: MD5");
        }
    }

    private String createDigestHeader(String uname, String digest) throws AuthenticationException {
        LOG.trace("enter DigestScheme.createDigestHeader(String, Map, String)");
        String uri = getParameter("uri");
        String realm = getParameter("realm");
        String nonce = getParameter("nonce");
        String opaque = getParameter("opaque");
        String algorithm = getParameter("algorithm");
        List params = new ArrayList(20);
        params.add(new NameValuePair("username", uname));
        params.add(new NameValuePair("realm", realm));
        params.add(new NameValuePair("nonce", nonce));
        params.add(new NameValuePair("uri", uri));
        params.add(new NameValuePair("response", digest));
        if (this.qopVariant != 0) {
            params.add(new NameValuePair("qop", getQopVariantString()));
            params.add(new NameValuePair("nc", NC));
            params.add(new NameValuePair("cnonce", this.cnonce));
        }
        if (algorithm != null) {
            params.add(new NameValuePair("algorithm", algorithm));
        }
        if (opaque != null) {
            params.add(new NameValuePair("opaque", opaque));
        }
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < params.size(); i++) {
            NameValuePair param = (NameValuePair) params.get(i);
            if (i > 0) {
                buffer.append(", ");
            }
            this.formatter.setAlwaysUseQuotes(!("nc".equals(param.getName()) || "qop".equals(param.getName())));
            this.formatter.format(buffer, param);
        }
        return buffer.toString();
    }

    private String getQopVariantString() {
        if (this.qopVariant == 1) {
            return "auth-int";
        }
        return "auth";
    }

    private static String encode(byte[] binaryData) {
        LOG.trace("enter DigestScheme.encode(byte[])");
        if (binaryData.length != 16) {
            return null;
        }
        char[] buffer = new char[32];
        for (int i = 0; i < 16; i++) {
            int low = binaryData[i] & 15;
            buffer[i * 2] = HEXADECIMAL[(binaryData[i] & 240) >> 4];
            buffer[(i * 2) + 1] = HEXADECIMAL[low];
        }
        return new String(buffer);
    }

    public static String createCnonce() {
        LOG.trace("enter DigestScheme.createCnonce()");
        try {
            return encode(MessageDigest.getInstance("MD5").digest(EncodingUtil.getAsciiBytes(Long.toString(System.currentTimeMillis()))));
        } catch (NoSuchAlgorithmException e) {
            throw new HttpClientError("Unsupported algorithm in HTTP Digest authentication: MD5");
        }
    }
}
