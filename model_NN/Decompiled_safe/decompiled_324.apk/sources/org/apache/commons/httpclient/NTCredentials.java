package org.apache.commons.httpclient;

import org.apache.commons.httpclient.util.LangUtils;

public class NTCredentials extends UsernamePasswordCredentials {
    private String domain;
    private String host;

    public NTCredentials() {
    }

    public NTCredentials(String userName, String password, String host2, String domain2) {
        super(userName, password);
        if (domain2 == null) {
            throw new IllegalArgumentException("Domain may not be null");
        }
        this.domain = domain2;
        if (host2 == null) {
            throw new IllegalArgumentException("Host may not be null");
        }
        this.host = host2;
    }

    public void setDomain(String domain2) {
        if (domain2 == null) {
            throw new IllegalArgumentException("Domain may not be null");
        }
        this.domain = domain2;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setHost(String host2) {
        if (host2 == null) {
            throw new IllegalArgumentException("Host may not be null");
        }
        this.host = host2;
    }

    public String getHost() {
        return this.host;
    }

    public String toString() {
        StringBuffer sbResult = new StringBuffer(super.toString());
        sbResult.append("@");
        sbResult.append(this.host);
        sbResult.append(".");
        sbResult.append(this.domain);
        return sbResult.toString();
    }

    public int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(super.hashCode(), this.host), this.domain);
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (!super.equals(o) || !(o instanceof NTCredentials)) {
            return false;
        }
        NTCredentials that = (NTCredentials) o;
        return LangUtils.equals(this.domain, that.domain) && LangUtils.equals(this.host, that.host);
    }
}
