package com.sun.activation.viewers;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.activation.CommandObject;
import javax.activation.DataHandler;

public class TextViewer extends Panel implements CommandObject {
    private boolean DEBUG = false;
    private DataHandler _dh = null;
    private TextArea text_area = null;
    private String text_buffer = null;
    private File text_file = null;

    public TextViewer() {
        setLayout(new GridLayout(1, 1));
        this.text_area = new TextArea("", 24, 80, 1);
        this.text_area.setEditable(false);
        add(this.text_area);
    }

    public void setCommandContext(String str, DataHandler dataHandler) throws IOException {
        this._dh = dataHandler;
        setInputStream(this._dh.getInputStream());
    }

    public void setInputStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                inputStream.close();
                this.text_buffer = byteArrayOutputStream.toString();
                this.text_area.setText(this.text_buffer);
                return;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public void addNotify() {
        TextViewer.super.addNotify();
        invalidate();
    }

    public Dimension getPreferredSize() {
        return this.text_area.getMinimumSize(24, 80);
    }
}
