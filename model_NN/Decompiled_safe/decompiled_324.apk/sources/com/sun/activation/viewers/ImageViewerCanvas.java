package com.sun.activation.viewers;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import org.apache.commons.httpclient.HttpStatus;

public class ImageViewerCanvas extends Canvas {
    private Image canvas_image = null;

    public void setImage(Image image) {
        this.canvas_image = image;
        invalidate();
        repaint();
    }

    public Dimension getPreferredSize() {
        if (this.canvas_image == null) {
            return new Dimension((int) HttpStatus.SC_OK, (int) HttpStatus.SC_OK);
        }
        return new Dimension(this.canvas_image.getWidth(this), this.canvas_image.getHeight(this));
    }

    public void paint(Graphics graphics) {
        if (this.canvas_image != null) {
            graphics.drawImage(this.canvas_image, 0, 0, this);
        }
    }
}
