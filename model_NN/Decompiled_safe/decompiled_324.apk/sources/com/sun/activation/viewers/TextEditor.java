package com.sun.activation.viewers;

import java.awt.Button;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.CommandObject;
import javax.activation.DataHandler;

public class TextEditor extends Panel implements CommandObject, ActionListener {
    private boolean DEBUG;
    private DataHandler _dh;
    private Panel button_panel;
    private InputStream data_ins;
    private FileInputStream fis;
    private GridBagLayout panel_gb;
    private Button save_button;
    private TextArea text_area;
    private String text_buffer;
    private File text_file;

    public TextEditor() {
        this.text_area = null;
        this.panel_gb = null;
        this.button_panel = null;
        this.save_button = null;
        this.text_file = null;
        this.text_buffer = null;
        this.data_ins = null;
        this.fis = null;
        this._dh = null;
        this.DEBUG = false;
        this.panel_gb = new GridBagLayout();
        setLayout(this.panel_gb);
        this.button_panel = new Panel();
        this.button_panel.setLayout(new FlowLayout());
        this.save_button = new Button("SAVE");
        this.button_panel.add(this.save_button);
        addGridComponent(this, this.button_panel, this.panel_gb, 0, 0, 1, 1, 1, 0);
        this.text_area = new TextArea("This is text", 24, 80, 1);
        this.text_area.setEditable(true);
        addGridComponent(this, this.text_area, this.panel_gb, 0, 1, 1, 2, 1, 1);
        this.save_button.addActionListener(this);
    }

    private void addGridComponent(Container container, Component component, GridBagLayout gridBagLayout, int i, int i2, int i3, int i4, int i5, int i6) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = i;
        gridBagConstraints.gridy = i2;
        gridBagConstraints.gridwidth = i3;
        gridBagConstraints.gridheight = i4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = (double) i6;
        gridBagConstraints.weightx = (double) i5;
        gridBagConstraints.anchor = 10;
        gridBagLayout.setConstraints(component, gridBagConstraints);
        container.add(component);
    }

    public void setCommandContext(String str, DataHandler dataHandler) throws IOException {
        this._dh = dataHandler;
        setInputStream(this._dh.getInputStream());
    }

    public void setInputStream(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                inputStream.close();
                this.text_buffer = byteArrayOutputStream.toString();
                this.text_area.setText(this.text_buffer);
                return;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    private void performSaveOperation() {
        OutputStream outputStream = null;
        try {
            outputStream = this._dh.getOutputStream();
        } catch (Exception e) {
        }
        String text = this.text_area.getText();
        if (outputStream == null) {
            System.out.println("Invalid outputstream in TextEditor!");
            System.out.println("not saving!");
        }
        try {
            outputStream.write(text.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e2) {
            System.out.println(new StringBuffer("TextEditor Save Operation failed with: ").append(e2).toString());
        }
    }

    public void addNotify() {
        TextEditor.super.addNotify();
        invalidate();
    }

    public Dimension getPreferredSize() {
        return this.text_area.getMinimumSize(24, 80);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == this.save_button) {
            performSaveOperation();
        }
    }
}
