package com.sun.activation.viewers;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.activation.CommandObject;
import javax.activation.DataHandler;

public class ImageViewer extends Panel implements CommandObject {
    private boolean DEBUG;
    private DataHandler _dh;
    private ImageViewerCanvas canvas;
    private Image image;

    public ImageViewer() {
        this.canvas = null;
        this.image = null;
        this._dh = null;
        this.DEBUG = false;
        this.canvas = new ImageViewerCanvas();
        add(this.canvas);
    }

    public void setCommandContext(String str, DataHandler dataHandler) throws IOException {
        this._dh = dataHandler;
        setInputStream(this._dh.getInputStream());
    }

    private void setInputStream(InputStream inputStream) throws IOException {
        MediaTracker mediaTracker = new MediaTracker(this);
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                break;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
        inputStream.close();
        this.image = getToolkit().createImage(byteArrayOutputStream.toByteArray());
        mediaTracker.addImage(this.image, 0);
        try {
            mediaTracker.waitForID(0);
            mediaTracker.waitForAll();
            if (mediaTracker.statusID(0, true) != 8) {
                System.out.println(new StringBuffer("Error occured in image loading = ").append(mediaTracker.getErrorsID(0)).toString());
            }
            this.canvas.setImage(this.image);
            if (this.DEBUG) {
                System.out.println("calling invalidate");
            }
        } catch (InterruptedException e) {
            throw new IOException("Error reading image data");
        }
    }

    public void addNotify() {
        ImageViewer.super.addNotify();
        invalidate();
        validate();
        doLayout();
    }

    public Dimension getPreferredSize() {
        return this.canvas.getPreferredSize();
    }
}
