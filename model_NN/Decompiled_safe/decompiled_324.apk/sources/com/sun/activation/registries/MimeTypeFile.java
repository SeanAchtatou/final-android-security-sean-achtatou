package com.sun.activation.registries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class MimeTypeFile {
    private static boolean DEBUG = false;
    private String fname = null;
    private Hashtable type_hash = new Hashtable();

    public MimeTypeFile(String str) throws IOException {
        this.fname = str;
        FileReader fileReader = new FileReader(new File(this.fname));
        try {
            parse(new BufferedReader(fileReader));
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
            }
        }
    }

    public MimeTypeFile(InputStream inputStream) throws IOException {
        parse(new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1")));
    }

    public MimeTypeFile() {
    }

    public MimeTypeEntry getMimeTypeEntry(String str) {
        return (MimeTypeEntry) this.type_hash.get(str);
    }

    public String getMIMETypeString(String str) {
        MimeTypeEntry mimeTypeEntry = getMimeTypeEntry(str);
        if (mimeTypeEntry != null) {
            return mimeTypeEntry.getMIMEType();
        }
        return null;
    }

    public void appendToRegistry(String str) {
        try {
            parse(new BufferedReader(new StringReader(str)));
        } catch (IOException e) {
        }
    }

    private void parse(BufferedReader bufferedReader) throws IOException {
        String stringBuffer;
        String str = null;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            if (str == null) {
                stringBuffer = readLine;
            } else {
                stringBuffer = new StringBuffer(String.valueOf(str)).append(readLine).toString();
            }
            int length = stringBuffer.length();
            if (stringBuffer.length() <= 0 || stringBuffer.charAt(length - 1) != '\\') {
                parseEntry(stringBuffer);
                str = null;
            } else {
                str = stringBuffer.substring(0, length - 1);
            }
        }
        if (str != null) {
            parseEntry(str);
        }
    }

    private void parseEntry(String str) {
        String str2;
        String trim = str.trim();
        if (trim.length() != 0 && trim.charAt(0) != '#') {
            if (trim.indexOf(61) > 0) {
                LineTokenizer lineTokenizer = new LineTokenizer(trim);
                String str3 = null;
                while (lineTokenizer.hasMoreTokens()) {
                    String nextToken = lineTokenizer.nextToken();
                    if (!lineTokenizer.hasMoreTokens() || !lineTokenizer.nextToken().equals("=") || !lineTokenizer.hasMoreTokens()) {
                        str2 = null;
                    } else {
                        str2 = lineTokenizer.nextToken();
                    }
                    if (str2 == null) {
                        System.err.println(new StringBuffer("Bad .mime.types entry: ").append(trim).toString());
                        return;
                    } else if (nextToken.equals("type")) {
                        str3 = str2;
                    } else if (nextToken.equals("exts")) {
                        StringTokenizer stringTokenizer = new StringTokenizer(str2, ",");
                        while (stringTokenizer.hasMoreTokens()) {
                            String nextToken2 = stringTokenizer.nextToken();
                            MimeTypeEntry mimeTypeEntry = new MimeTypeEntry(str3, nextToken2);
                            this.type_hash.put(nextToken2, mimeTypeEntry);
                            if (DEBUG) {
                                System.out.println(new StringBuffer("Added: ").append(mimeTypeEntry.toString()).toString());
                            }
                        }
                    }
                }
                return;
            }
            StringTokenizer stringTokenizer2 = new StringTokenizer(trim);
            if (stringTokenizer2.countTokens() != 0) {
                String nextToken3 = stringTokenizer2.nextToken();
                while (stringTokenizer2.hasMoreTokens()) {
                    String nextToken4 = stringTokenizer2.nextToken();
                    MimeTypeEntry mimeTypeEntry2 = new MimeTypeEntry(nextToken3, nextToken4);
                    this.type_hash.put(nextToken4, mimeTypeEntry2);
                    if (DEBUG) {
                        System.out.println(new StringBuffer("Added: ").append(mimeTypeEntry2.toString()).toString());
                    }
                }
            }
        }
    }

    public static void main(String[] strArr) throws Exception {
        DEBUG = true;
        System.out.println(new StringBuffer("ext ").append(strArr[1]).append(" type ").append(new MimeTypeFile(strArr[0]).getMIMETypeString(strArr[1])).toString());
        System.exit(0);
    }
}
