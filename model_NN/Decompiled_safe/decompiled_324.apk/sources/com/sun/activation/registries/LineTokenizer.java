package com.sun.activation.registries;

import java.util.NoSuchElementException;
import java.util.Vector;

/* compiled from: MimeTypeFile */
class LineTokenizer {
    private static final String singles = "=";
    private int currentPosition = 0;
    private int maxPosition;
    private Vector stack = new Vector();
    private String str;

    public LineTokenizer(String str2) {
        this.str = str2;
        this.maxPosition = str2.length();
    }

    private void skipWhiteSpace() {
        while (this.currentPosition < this.maxPosition && Character.isWhitespace(this.str.charAt(this.currentPosition))) {
            this.currentPosition++;
        }
    }

    public boolean hasMoreTokens() {
        if (this.stack.size() > 0) {
            return true;
        }
        skipWhiteSpace();
        return this.currentPosition < this.maxPosition;
    }

    public String nextToken() {
        int size = this.stack.size();
        if (size > 0) {
            String str2 = (String) this.stack.elementAt(size - 1);
            this.stack.removeElementAt(size - 1);
            return str2;
        }
        skipWhiteSpace();
        if (this.currentPosition >= this.maxPosition) {
            throw new NoSuchElementException();
        }
        int i = this.currentPosition;
        char charAt = this.str.charAt(i);
        if (charAt == '\"') {
            this.currentPosition++;
            boolean z = false;
            while (this.currentPosition < this.maxPosition) {
                String str3 = this.str;
                int i2 = this.currentPosition;
                this.currentPosition = i2 + 1;
                char charAt2 = str3.charAt(i2);
                if (charAt2 == '\\') {
                    this.currentPosition++;
                    z = true;
                } else if (charAt2 == '\"') {
                    if (!z) {
                        return this.str.substring(i + 1, this.currentPosition - 1);
                    }
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        i++;
                        if (i >= this.currentPosition - 1) {
                            return stringBuffer.toString();
                        }
                        char charAt3 = this.str.charAt(i);
                        if (charAt3 != '\\') {
                            stringBuffer.append(charAt3);
                        }
                    }
                }
            }
        } else if (singles.indexOf(charAt) >= 0) {
            this.currentPosition++;
        } else {
            while (this.currentPosition < this.maxPosition && singles.indexOf(this.str.charAt(this.currentPosition)) < 0 && !Character.isWhitespace(this.str.charAt(this.currentPosition))) {
                this.currentPosition++;
            }
        }
        return this.str.substring(i, this.currentPosition);
    }

    public void pushToken(String str2) {
        this.stack.addElement(str2);
    }
}
