package com.sun.activation.registries;

import weibo4j.AsyncWeibo;

public class MailcapTokenizer {
    public static final int EOI_TOKEN = 5;
    public static final int EQUALS_TOKEN = 61;
    public static final int SEMICOLON_TOKEN = 59;
    public static final int SLASH_TOKEN = 47;
    public static final int START_TOKEN = 1;
    public static final int STRING_TOKEN = 2;
    public static final int UNKNOWN_TOKEN = 0;
    private char autoquoteChar;
    private int currentToken;
    private String currentTokenValue;
    private String data;
    private int dataIndex = 0;
    private int dataLength;
    private boolean isAutoquoting;

    public MailcapTokenizer(String str) {
        this.data = str;
        this.dataLength = str.length();
        this.currentToken = 1;
        this.currentTokenValue = "";
        this.isAutoquoting = false;
        this.autoquoteChar = ';';
    }

    public void setIsAutoquoting(boolean z) {
        this.isAutoquoting = z;
    }

    public void setAutoquoteChar(char c) {
        this.autoquoteChar = c;
    }

    public int getCurrentToken() {
        return this.currentToken;
    }

    public static String nameForToken(int i) {
        switch (i) {
            case 0:
                return "unknown";
            case 1:
                return "start";
            case 2:
                return "string";
            case 5:
                return "EOI";
            case 47:
                return "'/'";
            case SEMICOLON_TOKEN /*59*/:
                return "';'";
            case EQUALS_TOKEN /*61*/:
                return "'='";
            default:
                return "really unknown";
        }
    }

    public String getCurrentTokenValue() {
        return this.currentTokenValue;
    }

    public int nextToken() {
        if (this.dataIndex < this.dataLength) {
            while (this.dataIndex < this.dataLength && isWhiteSpaceChar(this.data.charAt(this.dataIndex))) {
                this.dataIndex++;
            }
            if (this.dataIndex < this.dataLength) {
                char charAt = this.data.charAt(this.dataIndex);
                if (this.isAutoquoting) {
                    if (!isAutoquoteSpecialChar(charAt)) {
                        processAutoquoteToken();
                    } else if (charAt == ';' || charAt == '=') {
                        this.currentToken = charAt;
                        this.currentTokenValue = new Character(charAt).toString();
                        this.dataIndex++;
                    } else {
                        this.currentToken = 0;
                        this.currentTokenValue = new Character(charAt).toString();
                        this.dataIndex++;
                    }
                } else if (isStringTokenChar(charAt)) {
                    processStringToken();
                } else if (charAt == '/' || charAt == ';' || charAt == '=') {
                    this.currentToken = charAt;
                    this.currentTokenValue = new Character(charAt).toString();
                    this.dataIndex++;
                } else {
                    this.currentToken = 0;
                    this.currentTokenValue = new Character(charAt).toString();
                    this.dataIndex++;
                }
            } else {
                this.currentToken = 5;
                this.currentTokenValue = null;
            }
        } else {
            this.currentToken = 5;
            this.currentTokenValue = null;
        }
        return this.currentToken;
    }

    private void processStringToken() {
        int i = this.dataIndex;
        while (this.dataIndex < this.dataLength && isStringTokenChar(this.data.charAt(this.dataIndex))) {
            this.dataIndex++;
        }
        this.currentToken = 2;
        this.currentTokenValue = this.data.substring(i, this.dataIndex);
    }

    private void processAutoquoteToken() {
        int i = this.dataIndex;
        boolean z = false;
        while (this.dataIndex < this.dataLength && !z) {
            if (this.data.charAt(this.dataIndex) != this.autoquoteChar) {
                this.dataIndex++;
            } else {
                z = true;
            }
        }
        this.currentToken = 2;
        this.currentTokenValue = fixEscapeSequences(this.data.substring(i, this.dataIndex));
    }

    public static boolean isSpecialChar(char c) {
        switch (c) {
            case AsyncWeibo.EXISTS_FRIENDSHIP /*34*/:
            case AsyncWeibo.DESTROY_DIRECT_MESSAGES /*40*/:
            case AsyncWeibo.UPDATE_PROFILE /*41*/:
            case AsyncWeibo.TRENDS /*44*/:
            case '/':
            case ':':
            case SEMICOLON_TOKEN /*59*/:
            case '<':
            case EQUALS_TOKEN /*61*/:
            case '>':
            case '?':
            case '@':
            case '[':
            case '\\':
            case ']':
                return true;
            default:
                return false;
        }
    }

    public static boolean isAutoquoteSpecialChar(char c) {
        switch (c) {
            case SEMICOLON_TOKEN /*59*/:
            case EQUALS_TOKEN /*61*/:
                return true;
            case '<':
            default:
                return false;
        }
    }

    public static boolean isControlChar(char c) {
        return Character.isISOControl(c);
    }

    public static boolean isWhiteSpaceChar(char c) {
        return Character.isWhitespace(c);
    }

    public static boolean isStringTokenChar(char c) {
        return !isSpecialChar(c) && !isControlChar(c) && !isWhiteSpaceChar(c);
    }

    private static String fixEscapeSequences(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(length);
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt != '\\') {
                stringBuffer.append(charAt);
            } else if (i < length - 1) {
                stringBuffer.append(str.charAt(i + 1));
                i++;
            } else {
                stringBuffer.append(charAt);
            }
            i++;
        }
        return stringBuffer.toString();
    }
}
