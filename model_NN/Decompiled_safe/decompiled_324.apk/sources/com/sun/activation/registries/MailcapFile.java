package com.sun.activation.registries;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class MailcapFile {
    private static boolean debug;
    private Hashtable type_hash = null;

    static {
        debug = false;
        try {
            debug = Boolean.getBoolean("javax.activation.debug");
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0038 A[SYNTHETIC, Splitter:B:13:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MailcapFile(java.lang.String r6) throws java.io.IOException {
        /*
            r5 = this;
            r3 = 0
            r5.<init>()
            r5.type_hash = r3
            boolean r0 = com.sun.activation.registries.MailcapFile.debug
            if (r0 == 0) goto L_0x001e
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "new MailcapFile: file "
            r1.<init>(r2)
            java.lang.StringBuffer r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            r0.println(r1)
        L_0x001e:
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ all -> 0x0034 }
            r0.<init>(r6)     // Catch:{ all -> 0x0034 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x0040 }
            r1.<init>(r0)     // Catch:{ all -> 0x0040 }
            java.util.Hashtable r1 = r5.createMailcapHash(r1)     // Catch:{ all -> 0x0040 }
            r5.type_hash = r1     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x0033
            r0.close()     // Catch:{ IOException -> 0x003e }
        L_0x0033:
            return
        L_0x0034:
            r0 = move-exception
            r1 = r3
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x003b:
            throw r0
        L_0x003c:
            r1 = move-exception
            goto L_0x003b
        L_0x003e:
            r0 = move-exception
            goto L_0x0033
        L_0x0040:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.activation.registries.MailcapFile.<init>(java.lang.String):void");
    }

    public MailcapFile(InputStream inputStream) throws IOException {
        if (debug) {
            System.out.println("new MailcapFile: InputStream");
        }
        this.type_hash = createMailcapHash(new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1")));
    }

    public MailcapFile() {
        if (debug) {
            System.out.println("new MailcapFile: default");
        }
        this.type_hash = new Hashtable();
    }

    public Hashtable getMailcapList(String str) {
        Hashtable hashtable = (Hashtable) this.type_hash.get(str);
        Hashtable hashtable2 = (Hashtable) this.type_hash.get(new StringBuffer(String.valueOf(str.substring(0, str.indexOf(47) + 1))).append("*").toString());
        if (hashtable2 == null) {
            return hashtable;
        }
        if (hashtable != null) {
            return mergeResults(hashtable, hashtable2);
        }
        return hashtable2;
    }

    private Hashtable mergeResults(Hashtable hashtable, Hashtable hashtable2) {
        Enumeration keys = hashtable2.keys();
        Hashtable hashtable3 = (Hashtable) hashtable.clone();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            Vector vector = (Vector) hashtable3.get(str);
            if (vector == null) {
                hashtable3.put(str, hashtable2.get(str));
            } else {
                Enumeration elements = ((Vector) hashtable2.get(str)).elements();
                Vector vector2 = (Vector) vector.clone();
                hashtable3.put(str, vector2);
                while (elements.hasMoreElements()) {
                    vector2.addElement(elements.nextElement());
                }
            }
        }
        return hashtable3;
    }

    public void appendToMailcap(String str) {
        if (debug) {
            System.out.println(new StringBuffer("appendToMailcap: ").append(str).toString());
        }
        try {
            parse(new StringReader(str), this.type_hash);
        } catch (IOException e) {
        }
    }

    private Hashtable createMailcapHash(Reader reader) throws IOException {
        Hashtable hashtable = new Hashtable();
        parse(reader, hashtable);
        return hashtable;
    }

    private void parse(Reader reader, Hashtable hashtable) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        String str = null;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                String trim = readLine.trim();
                try {
                    if (trim.charAt(0) != '#') {
                        if (trim.charAt(trim.length() - 1) == '\\') {
                            if (str != null) {
                                str = new StringBuffer(String.valueOf(str)).append(trim.substring(0, trim.length() - 1)).toString();
                            } else {
                                str = trim.substring(0, trim.length() - 1);
                            }
                        } else if (str != null) {
                            try {
                                parseLine(new StringBuffer(String.valueOf(str)).append(trim).toString(), hashtable);
                            } catch (MailcapParseException e) {
                            }
                            str = null;
                        } else {
                            try {
                                parseLine(trim, hashtable);
                            } catch (MailcapParseException e2) {
                            }
                        }
                    }
                } catch (StringIndexOutOfBoundsException e3) {
                }
            } else {
                return;
            }
        }
    }

    protected static void parseLine(String str, Hashtable hashtable) throws MailcapParseException, IOException {
        String str2;
        int i;
        Hashtable hashtable2;
        int i2;
        Vector vector;
        MailcapTokenizer mailcapTokenizer = new MailcapTokenizer(str);
        mailcapTokenizer.setIsAutoquoting(false);
        if (debug) {
            System.out.println(new StringBuffer("parse: ").append(str).toString());
        }
        int nextToken = mailcapTokenizer.nextToken();
        String concat = "".concat(mailcapTokenizer.getCurrentTokenValue());
        if (nextToken != 2) {
            reportParseError(2, nextToken, mailcapTokenizer.getCurrentTokenValue());
        }
        String lowerCase = mailcapTokenizer.getCurrentTokenValue().toLowerCase();
        int nextToken2 = mailcapTokenizer.nextToken();
        String concat2 = concat.concat(mailcapTokenizer.getCurrentTokenValue());
        if (!(nextToken2 == 47 || nextToken2 == 59)) {
            reportParseError(47, 59, nextToken2, mailcapTokenizer.getCurrentTokenValue());
        }
        if (nextToken2 == 47) {
            int nextToken3 = mailcapTokenizer.nextToken();
            String concat3 = concat2.concat(mailcapTokenizer.getCurrentTokenValue());
            if (nextToken3 != 2) {
                reportParseError(2, nextToken3, mailcapTokenizer.getCurrentTokenValue());
            }
            String lowerCase2 = mailcapTokenizer.getCurrentTokenValue().toLowerCase();
            int nextToken4 = mailcapTokenizer.nextToken();
            concat2 = concat3.concat(mailcapTokenizer.getCurrentTokenValue());
            int i3 = nextToken4;
            str2 = lowerCase2;
            i = i3;
        } else {
            int i4 = nextToken2;
            str2 = "*";
            i = i4;
        }
        if (debug) {
            System.out.println(new StringBuffer("  Type: ").append(lowerCase).append(CookieSpec.PATH_DELIM).append(str2).toString());
        }
        Hashtable hashtable3 = (Hashtable) hashtable.get(new StringBuffer(String.valueOf(lowerCase)).append(CookieSpec.PATH_DELIM).append(str2).toString());
        if (hashtable3 == null) {
            Hashtable hashtable4 = new Hashtable();
            hashtable.put(new StringBuffer(String.valueOf(lowerCase)).append(CookieSpec.PATH_DELIM).append(str2).toString(), hashtable4);
            hashtable2 = hashtable4;
        } else {
            hashtable2 = hashtable3;
        }
        if (i != 59) {
            reportParseError(59, i, mailcapTokenizer.getCurrentTokenValue());
        }
        mailcapTokenizer.setIsAutoquoting(true);
        int nextToken5 = mailcapTokenizer.nextToken();
        mailcapTokenizer.setIsAutoquoting(false);
        concat2.concat(mailcapTokenizer.getCurrentTokenValue());
        if (!(nextToken5 == 2 || nextToken5 == 59)) {
            reportParseError(2, 59, nextToken5, mailcapTokenizer.getCurrentTokenValue());
        }
        if (nextToken5 != 59) {
            nextToken5 = mailcapTokenizer.nextToken();
        }
        if (nextToken5 == 59) {
            do {
                int nextToken6 = mailcapTokenizer.nextToken();
                if (nextToken6 != 2) {
                    reportParseError(2, nextToken6, mailcapTokenizer.getCurrentTokenValue());
                }
                String lowerCase3 = mailcapTokenizer.getCurrentTokenValue().toLowerCase();
                int nextToken7 = mailcapTokenizer.nextToken();
                if (!(nextToken7 == 61 || nextToken7 == 59 || nextToken7 == 5)) {
                    reportParseError(61, 59, 5, nextToken7, mailcapTokenizer.getCurrentTokenValue());
                }
                if (nextToken7 == 61) {
                    mailcapTokenizer.setIsAutoquoting(true);
                    int nextToken8 = mailcapTokenizer.nextToken();
                    mailcapTokenizer.setIsAutoquoting(false);
                    if (nextToken8 != 2) {
                        reportParseError(2, nextToken8, mailcapTokenizer.getCurrentTokenValue());
                    }
                    String currentTokenValue = mailcapTokenizer.getCurrentTokenValue();
                    if (lowerCase3.startsWith("x-java-")) {
                        String substring = lowerCase3.substring(7);
                        if (debug) {
                            System.out.println(new StringBuffer("    Command: ").append(substring).append(", Class: ").append(currentTokenValue).toString());
                        }
                        Vector vector2 = (Vector) hashtable2.get(substring);
                        if (vector2 == null) {
                            Vector vector3 = new Vector();
                            hashtable2.put(substring, vector3);
                            vector = vector3;
                        } else {
                            vector = vector2;
                        }
                        vector.insertElementAt(currentTokenValue, 0);
                    }
                    i2 = mailcapTokenizer.nextToken();
                    continue;
                } else {
                    i2 = nextToken7;
                    continue;
                }
            } while (i2 == 59);
        } else if (nextToken5 != 5) {
            reportParseError(5, 59, nextToken5, mailcapTokenizer.getCurrentTokenValue());
        }
    }

    protected static void reportParseError(int i, int i2, String str) throws MailcapParseException {
        throw new MailcapParseException(new StringBuffer("Encountered a ").append(MailcapTokenizer.nameForToken(i2)).append(" token (").append(str).append(") while expecting a ").append(MailcapTokenizer.nameForToken(i)).append(" token.").toString());
    }

    protected static void reportParseError(int i, int i2, int i3, String str) throws MailcapParseException {
        throw new MailcapParseException(new StringBuffer("Encountered a ").append(MailcapTokenizer.nameForToken(i3)).append(" token (").append(str).append(") while expecting a ").append(MailcapTokenizer.nameForToken(i)).append(" or a ").append(MailcapTokenizer.nameForToken(i2)).append(" token.").toString());
    }

    protected static void reportParseError(int i, int i2, int i3, int i4, String str) throws MailcapParseException {
        if (debug) {
            System.out.println(new StringBuffer("PARSE ERROR: Encountered a ").append(MailcapTokenizer.nameForToken(i4)).append(" token (").append(str).append(") while expecting a ").append(MailcapTokenizer.nameForToken(i)).append(", a ").append(MailcapTokenizer.nameForToken(i2)).append(", or a ").append(MailcapTokenizer.nameForToken(i3)).append(" token.").toString());
        }
        throw new MailcapParseException(new StringBuffer("Encountered a ").append(MailcapTokenizer.nameForToken(i4)).append(" token (").append(str).append(") while expecting a ").append(MailcapTokenizer.nameForToken(i)).append(", a ").append(MailcapTokenizer.nameForToken(i2)).append(", or a ").append(MailcapTokenizer.nameForToken(i3)).append(" token.").toString());
    }
}
