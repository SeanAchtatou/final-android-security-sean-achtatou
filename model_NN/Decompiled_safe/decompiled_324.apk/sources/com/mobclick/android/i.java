package com.mobclick.android;

import android.content.Context;
import android.util.Log;

final class i extends Thread {
    private static final Object a = new Object();
    private Context b;
    private int c;
    private String d;
    private String e;

    i(Context context, int i) {
        this.b = context;
        this.c = i;
    }

    i(Context context, String str, int i) {
        this.b = context;
        this.c = i;
        this.d = str;
    }

    i(Context context, String str, String str2, int i) {
        this.b = context;
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    public void run() {
        try {
            synchronized (a) {
                String name = this.b.getClass().getName();
                if (this.c == 0) {
                    Log.i("MobclickAgent", "【onPauseAsyc: 】" + name);
                    try {
                        if (this.b == null) {
                            Log.e("MobclickAgent", "unexpected null context");
                        } else {
                            Thread.sleep(2000);
                            MobclickAgent.a.e(this.b);
                        }
                    } catch (Exception e2) {
                        Log.e("MobclickAgent", "Exception occurred in Mobclick.onRause(). ");
                    }
                } else if (this.c == 1) {
                    Log.i("MobclickAgent", "【onResumeAsyc:】" + name);
                    MobclickAgent.a.a(this.b, this.d, this.e);
                } else if (this.c == 2) {
                    Log.i("MobclickAgent", "【onError:】" + name);
                    MobclickAgent.a.b(this.b, this.d);
                }
            }
        } catch (Exception e3) {
            Log.e("MobclickAgent", "Exception occurred when recording usage.");
        }
    }
}
