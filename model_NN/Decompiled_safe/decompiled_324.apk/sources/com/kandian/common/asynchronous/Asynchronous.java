package com.kandian.common.asynchronous;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.kandian.common.LoadDialog;
import java.util.HashMap;
import java.util.Map;

public class Asynchronous {
    /* access modifiers changed from: private */
    public AsyncCallback asyncCallback;
    /* access modifiers changed from: private */
    public AsyncExceptionHandle asyncExceptionHandle;
    /* access modifiers changed from: private */
    public AsyncProcess asyncProcess;
    Handler commoneHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (Asynchronous.this.loadingMsg != null) {
                Asynchronous.this.loadDialog.closeProgressDialog();
            }
            Map<String, Object> outputparameter = (Map) msg.obj;
            if (Asynchronous.this.asyncCallback != null && Asynchronous.this.asyncProcess != null) {
                Activity activity = null;
                try {
                    if (Asynchronous.this.context instanceof Activity) {
                        activity = (Activity) Asynchronous.this.context;
                    }
                    Asynchronous.this.asyncCallback.callback(activity, outputparameter, msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.handleMessage(msg);
            }
        }
    };
    /* access modifiers changed from: private */
    public Context context;
    Handler exceptionHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (Asynchronous.this.loadingMsg != null) {
                Asynchronous.this.loadDialog.closeProgressDialog();
            }
            Exception e = (Exception) msg.obj;
            if (Asynchronous.this.asyncExceptionHandle != null) {
                Activity activity = null;
                try {
                    if (Asynchronous.this.context instanceof Activity) {
                        activity = (Activity) Asynchronous.this.context;
                    }
                    Asynchronous.this.asyncExceptionHandle.handle(activity, e, msg);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                super.handleMessage(msg);
            }
        }
    };
    /* access modifiers changed from: private */
    public Map<String, Object> inputparameter = new HashMap();
    /* access modifiers changed from: private */
    public LoadDialog loadDialog;
    /* access modifiers changed from: private */
    public String loadingMsg;
    /* access modifiers changed from: private */
    public long validDataThreadId = -1;

    public Asynchronous(Context context2) {
        this.context = context2;
    }

    public void setLoadingMsg(String loadingMsg2) {
        this.loadingMsg = loadingMsg2;
    }

    public void setInputParameter(String key, Object obj) {
        this.inputparameter.put(key, obj);
    }

    public void asyncExceptionHandle(AsyncExceptionHandle asyncExceptionHandle2) {
        if (asyncExceptionHandle2 != null) {
            this.asyncExceptionHandle = asyncExceptionHandle2;
        }
    }

    public void asyncProcess(AsyncProcess asyncProcess2) {
        if (asyncProcess2 != null) {
            this.asyncProcess = asyncProcess2;
        }
    }

    public void asyncCallback(AsyncCallback asyncCallback2) {
        if (asyncCallback2 != null) {
            this.asyncCallback = asyncCallback2;
        }
    }

    public void start() {
        if (this.asyncCallback != null && this.asyncProcess != null) {
            if (this.loadingMsg != null && (this.context instanceof Activity)) {
                this.loadDialog = new LoadDialog(this.loadingMsg, (Activity) this.context);
                this.loadDialog.showProgressDialog();
            }
            Thread t = new Thread() {
                public void run() {
                    if (Asynchronous.this.validDataThreadId == getId()) {
                        Activity activity = null;
                        try {
                            if (Asynchronous.this.context instanceof Activity) {
                                activity = (Activity) Asynchronous.this.context;
                            }
                            int what = Asynchronous.this.asyncProcess.process(activity, Asynchronous.this.inputparameter);
                            Map<String, Object> outputparameter = Asynchronous.this.asyncProcess.getOutputparameter();
                            Message m = Message.obtain(Asynchronous.this.commoneHandler);
                            m.obj = outputparameter;
                            m.what = what;
                            m.sendToTarget();
                        } catch (Exception e) {
                            Exception e2 = e;
                            e2.printStackTrace();
                            Message m2 = Message.obtain(Asynchronous.this.exceptionHandler);
                            m2.obj = e2;
                            m2.what = 0;
                            m2.sendToTarget();
                        }
                    }
                }
            };
            this.validDataThreadId = t.getId();
            t.start();
        }
    }
}
