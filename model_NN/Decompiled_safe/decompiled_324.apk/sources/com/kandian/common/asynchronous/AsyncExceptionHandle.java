package com.kandian.common.asynchronous;

import android.content.Context;
import android.os.Message;

public abstract class AsyncExceptionHandle {
    public abstract void handle(Context context, Exception exc, Message message);
}
