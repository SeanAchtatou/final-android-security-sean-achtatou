package com.kandian.common.asynchronous;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;

public abstract class AsyncProcess {
    private Map<String, Object> outputparameter = new HashMap();

    public abstract int process(Context context, Map<String, Object> map) throws Exception;

    /* access modifiers changed from: protected */
    public void setCallbackParameter(String key, Object obj) {
        this.outputparameter.put(key, obj);
    }

    public Map<String, Object> getOutputparameter() {
        return this.outputparameter;
    }
}
