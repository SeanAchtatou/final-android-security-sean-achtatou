package com.kandian.common.asynchronous;

import android.content.Context;
import android.os.Message;
import java.util.Map;

public abstract class AsyncCallback {
    public abstract void callback(Context context, Map<String, Object> map, Message message);
}
