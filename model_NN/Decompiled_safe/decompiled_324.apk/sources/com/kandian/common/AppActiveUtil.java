package com.kandian.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;

public class AppActiveUtil {
    /* access modifiers changed from: private */
    public static String ACTIVEINFO_KEY = "ACTIVEINFO";
    public static final int ACTIVE_STATUS_ACTIVATED = 1;
    public static final int ACTIVE_STATUS_NOTACTIVATED = 0;
    /* access modifiers changed from: private */
    public static String LAST_OPEN_TIME_KEY = "LAST_OPEN_TIME";
    private static final int MSG_NETWORK_PROBLEM = 1;
    private static final int MSG_PARTNER_FAIL = 3;
    private static final int MSG_PARTNER_SUCC = 2;
    public static String TAG = "AppActiveUtil";
    /* access modifiers changed from: private */
    public static Context _context = null;
    static final Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Toast.makeText(AppActiveUtil._context, AppActiveUtil._context.getString(R.string.network_problem), 0).show();
                    ((LinearLayout) ((Activity) AppActiveUtil._context).findViewById(R.id.loadProgress)).setVisibility(0);
                    break;
                case 2:
                    ((ProgressBar) ((Activity) AppActiveUtil._context).findViewById(R.id.partnerCommitProgress)).setVisibility(8);
                    ((Button) ((Activity) AppActiveUtil._context).findViewById(R.id.ksapp_partner_btn)).setEnabled(true);
                    Toast.makeText(AppActiveUtil._context, AppActiveUtil._context.getString(R.string.ksapp_partner_succ), 0).show();
                    break;
                case 3:
                    ((ProgressBar) ((Activity) AppActiveUtil._context).findViewById(R.id.partnerCommitProgress)).setVisibility(8);
                    ((Button) ((Activity) AppActiveUtil._context).findViewById(R.id.ksapp_partner_btn)).setEnabled(true);
                    Toast.makeText(AppActiveUtil._context, AppActiveUtil._context.getString(R.string.ksapp_partner_fail), 0).show();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private final int MSG_LIST = 0;

    public static void appActive(final Context context, final String parm_partner) {
        _context = context;
        if (context instanceof Activity) {
            new Thread(new Runnable() {
                /* JADX WARNING: Code restructure failed: missing block: B:26:0x01b7, code lost:
                    if (r38.trim().length() == 0) goto L_0x01b9;
                 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r45 = this;
                        r30 = 0
                        r5 = 0
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x0094 }
                        r41 = r0
                        android.content.pm.PackageManager r41 = r41.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0094 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x0094 }
                        r42 = r0
                        java.lang.String r42 = r42.getPackageName()     // Catch:{ NameNotFoundException -> 0x0094 }
                        r43 = 0
                        android.content.pm.PackageInfo r41 = r41.getPackageInfo(r42, r43)     // Catch:{ NameNotFoundException -> 0x0094 }
                        r0 = r41
                        java.lang.String r0 = r0.packageName     // Catch:{ NameNotFoundException -> 0x0094 }
                        r30 = r0
                        java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0094 }
                        java.lang.String r42 = java.lang.String.valueOf(r30)     // Catch:{ NameNotFoundException -> 0x0094 }
                        r41.<init>(r42)     // Catch:{ NameNotFoundException -> 0x0094 }
                        java.lang.String r42 = "."
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ NameNotFoundException -> 0x0094 }
                        java.lang.String r42 = com.kandian.common.AppActiveUtil.LAST_OPEN_TIME_KEY     // Catch:{ NameNotFoundException -> 0x0094 }
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ NameNotFoundException -> 0x0094 }
                        java.lang.String r5 = r41.toString()     // Catch:{ NameNotFoundException -> 0x0094 }
                    L_0x003e:
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        r42 = 0
                        r0 = r41
                        r1 = r5
                        r2 = r42
                        android.content.SharedPreferences r34 = r0.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.LAST_OPEN_TIME_KEY     // Catch:{ Exception -> 0x0376 }
                        r42 = 0
                        r0 = r34
                        r1 = r41
                        r2 = r42
                        long r22 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r43 = "lastOpenTime = "
                        r42.<init>(r43)     // Catch:{ Exception -> 0x0376 }
                        r0 = r42
                        r1 = r22
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x0376 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0376 }
                        long r41 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0376 }
                        long r41 = r41 - r22
                        r43 = 86400000(0x5265c00, double:4.2687272E-316)
                        long r41 = r41 / r43
                        r0 = r41
                        double r0 = (double) r0     // Catch:{ Exception -> 0x0376 }
                        r13 = r0
                        r41 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                        int r41 = (r13 > r41 ? 1 : (r13 == r41 ? 0 : -1))
                        if (r41 >= 0) goto L_0x009a
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = "Open less than a day!!!"
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0376 }
                    L_0x0093:
                        return
                    L_0x0094:
                        r41 = move-exception
                        r9 = r41
                        java.lang.String r5 = ""
                        goto L_0x003e
                    L_0x009a:
                        long r22 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0376 }
                        android.content.SharedPreferences$Editor r12 = r34.edit()     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.LAST_OPEN_TIME_KEY     // Catch:{ Exception -> 0x0376 }
                        r0 = r12
                        r1 = r41
                        r2 = r22
                        r0.putLong(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        r12.commit()     // Catch:{ Exception -> 0x0376 }
                        r38 = 0
                        java.lang.String r28 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r27 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r39 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0376 }
                        r26 = 0
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r43 = "uuid1 = "
                        r42.<init>(r43)     // Catch:{ Exception -> 0x0376 }
                        r0 = r42
                        r1 = r38
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x0376 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0376 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0379 }
                        r41 = r0
                        java.lang.String r42 = "wifi"
                        java.lang.Object r40 = r41.getSystemService(r42)     // Catch:{ Exception -> 0x0379 }
                        android.net.wifi.WifiManager r40 = (android.net.wifi.WifiManager) r40     // Catch:{ Exception -> 0x0379 }
                        android.net.wifi.WifiInfo r15 = r40.getConnectionInfo()     // Catch:{ Exception -> 0x0379 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0379 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0379 }
                        java.lang.String r43 = "MacAddress = "
                        r42.<init>(r43)     // Catch:{ Exception -> 0x0379 }
                        java.lang.String r43 = r15.getMacAddress()     // Catch:{ Exception -> 0x0379 }
                        java.lang.StringBuilder r42 = r42.append(r43)     // Catch:{ Exception -> 0x0379 }
                        java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x0379 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0379 }
                        java.lang.String r26 = r15.getMacAddress()     // Catch:{ Exception -> 0x0379 }
                    L_0x0101:
                        java.lang.String r4 = ""
                        java.lang.String r29 = ""
                        java.lang.String r37 = ""
                        java.lang.String r8 = ""
                        java.lang.String r31 = ""
                        r24 = 0
                        r35 = 0
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r43 = "partner = "
                        r42.<init>(r43)     // Catch:{ Exception -> 0x0376 }
                        r0 = r42
                        r1 = r31
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x0376 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0376 }
                        r0 = r45
                        java.lang.String r0 = r4     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        if (r41 == 0) goto L_0x0135
                        r0 = r45
                        java.lang.String r0 = r4     // Catch:{ Exception -> 0x0376 }
                        r31 = r0
                    L_0x0135:
                        android.content.Context r41 = com.kandian.common.AppActiveUtil._context     // Catch:{ Exception -> 0x0376 }
                        org.json.JSONObject r19 = com.kandian.common.AppActiveUtil.getActiveInfo(r41)     // Catch:{ Exception -> 0x0376 }
                        if (r19 == 0) goto L_0x01ad
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ JSONException -> 0x0380 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r43 = "local jsonObject = "
                        r42.<init>(r43)     // Catch:{ JSONException -> 0x0380 }
                        r43 = 4
                        r0 = r19
                        r1 = r43
                        java.lang.String r43 = r0.toString(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.StringBuilder r42 = r42.append(r43)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r42 = r42.toString()     // Catch:{ JSONException -> 0x0380 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "packageName"
                        r0 = r19
                        r1 = r41
                        java.lang.String r29 = r0.getString(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "token"
                        r0 = r19
                        r1 = r41
                        java.lang.String r37 = r0.getString(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "deviceId"
                        r0 = r19
                        r1 = r41
                        java.lang.String r8 = r0.getString(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "partner"
                        r0 = r19
                        r1 = r41
                        java.lang.String r31 = r0.getString(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "status"
                        r0 = r19
                        r1 = r41
                        int r35 = r0.getInt(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "lastVisitTime"
                        r0 = r19
                        r1 = r41
                        long r24 = r0.getLong(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "uuid"
                        r0 = r19
                        r1 = r41
                        java.lang.String r38 = r0.getString(r1)     // Catch:{ JSONException -> 0x0380 }
                        java.lang.String r41 = "mac"
                        r0 = r19
                        r1 = r41
                        java.lang.String r26 = r0.getString(r1)     // Catch:{ JSONException -> 0x0380 }
                    L_0x01ad:
                        if (r38 == 0) goto L_0x01b9
                        java.lang.String r41 = r38.trim()     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.length()     // Catch:{ Exception -> 0x0376 }
                        if (r41 != 0) goto L_0x01da
                    L_0x01b9:
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        java.lang.String r38 = com.kandian.common.AppActiveUtil.readUUID(r41)     // Catch:{ Exception -> 0x0376 }
                        if (r38 == 0) goto L_0x01cf
                        java.lang.String r41 = r38.trim()     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.length()     // Catch:{ Exception -> 0x0376 }
                        if (r41 != 0) goto L_0x01da
                    L_0x01cf:
                        java.util.UUID r41 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r38 = r41.toString()     // Catch:{ Exception -> 0x0376 }
                        com.kandian.common.AppActiveUtil.saveUUID(r38)     // Catch:{ Exception -> 0x0376 }
                    L_0x01da:
                        if (r37 == 0) goto L_0x0394
                        java.lang.String r41 = r37.trim()     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.length()     // Catch:{ Exception -> 0x0376 }
                        if (r41 <= 0) goto L_0x0394
                        r41 = 1
                        r0 = r35
                        r1 = r41
                        if (r0 != r1) goto L_0x0394
                        long r41 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0376 }
                        long r41 = r41 - r24
                        r43 = 86400000(0x5265c00, double:4.2687272E-316)
                        long r41 = r41 / r43
                        r0 = r41
                        double r0 = (double) r0     // Catch:{ Exception -> 0x0376 }
                        r16 = r0
                        android.content.Context r41 = com.kandian.common.AppActiveUtil._context     // Catch:{ Exception -> 0x0376 }
                        r42 = 2131099665(0x7f060011, float:1.781169E38)
                        java.lang.String r41 = r41.getString(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.Integer r41 = java.lang.Integer.valueOf(r41)     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.intValue()     // Catch:{ Exception -> 0x0376 }
                        r0 = r41
                        double r0 = (double) r0     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        int r41 = (r16 > r41 ? 1 : (r16 == r41 ? 0 : -1))
                        if (r41 < 0) goto L_0x0093
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        r42 = 2131099658(0x7f06000a, float:1.7811675E38)
                        java.lang.String r41 = r41.getString(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = "{packageName}"
                        java.lang.String r43 = com.kandian.common.StringUtil.urlEncode(r29)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r41, r42, r43)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{partner}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r31)     // Catch:{ Exception -> 0x0376 }
                        r0 = r7
                        r1 = r41
                        r2 = r42
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{deviceId}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r8)     // Catch:{ Exception -> 0x0376 }
                        r0 = r7
                        r1 = r41
                        r2 = r42
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{model}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r28)     // Catch:{ Exception -> 0x0376 }
                        r0 = r7
                        r1 = r41
                        r2 = r42
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{manufacturer}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r27)     // Catch:{ Exception -> 0x0376 }
                        r0 = r7
                        r1 = r41
                        r2 = r42
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{version}"
                        r0 = r7
                        r1 = r41
                        r2 = r39
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{uuid}"
                        r0 = r7
                        r1 = r41
                        r2 = r38
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{mac}"
                        r0 = r7
                        r1 = r41
                        r2 = r26
                        java.lang.String r7 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0376 }
                        r41.<init>(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = "&t="
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x0376 }
                        long r42 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r7 = r41.toString()     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r43 = "appUseUrl = "
                        r42.<init>(r43)     // Catch:{ Exception -> 0x0376 }
                        r0 = r42
                        r1 = r7
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x0376 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0376 }
                        r32 = 0
                        java.lang.String r18 = ""
                        java.lang.String r32 = com.kandian.common.StringUtil.getStringFromURL(r7)     // Catch:{ IOException -> 0x0385 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ IOException -> 0x0385 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0385 }
                        java.lang.String r43 = "returnContent = "
                        r42.<init>(r43)     // Catch:{ IOException -> 0x0385 }
                        r0 = r42
                        r1 = r32
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ IOException -> 0x0385 }
                        java.lang.String r42 = r42.toString()     // Catch:{ IOException -> 0x0385 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ IOException -> 0x0385 }
                        if (r32 == 0) goto L_0x0093
                        java.lang.String r41 = r32.trim()     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.length()     // Catch:{ Exception -> 0x0376 }
                        if (r41 <= 0) goto L_0x0093
                        java.lang.String r41 = "lastVisitTime"
                        java.lang.Long r42 = java.lang.Long.valueOf(r32)     // Catch:{ JSONException -> 0x038a }
                        r0 = r19
                        r1 = r41
                        r2 = r42
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x038a }
                        r41 = 4
                        r0 = r19
                        r1 = r41
                        java.lang.String r18 = r0.toString(r1)     // Catch:{ JSONException -> 0x038a }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ JSONException -> 0x038a }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x038a }
                        java.lang.String r43 = "jsonContent = "
                        r42.<init>(r43)     // Catch:{ JSONException -> 0x038a }
                        r0 = r42
                        r1 = r18
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ JSONException -> 0x038a }
                        java.lang.String r42 = r42.toString()     // Catch:{ JSONException -> 0x038a }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ JSONException -> 0x038a }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x038f }
                        r41 = r0
                        android.content.pm.PackageManager r41 = r41.getPackageManager()     // Catch:{ NameNotFoundException -> 0x038f }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x038f }
                        r42 = r0
                        java.lang.String r42 = r42.getPackageName()     // Catch:{ NameNotFoundException -> 0x038f }
                        r43 = 0
                        android.content.pm.PackageInfo r41 = r41.getPackageInfo(r42, r43)     // Catch:{ NameNotFoundException -> 0x038f }
                        r0 = r41
                        java.lang.String r0 = r0.packageName     // Catch:{ NameNotFoundException -> 0x038f }
                        r29 = r0
                        java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x038f }
                        java.lang.String r42 = java.lang.String.valueOf(r29)     // Catch:{ NameNotFoundException -> 0x038f }
                        r41.<init>(r42)     // Catch:{ NameNotFoundException -> 0x038f }
                        java.lang.String r42 = ".activeinfo"
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ NameNotFoundException -> 0x038f }
                        java.lang.String r4 = r41.toString()     // Catch:{ NameNotFoundException -> 0x038f }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        r42 = 0
                        r0 = r41
                        r1 = r4
                        r2 = r42
                        android.content.SharedPreferences r33 = r0.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        android.content.SharedPreferences$Editor r12 = r33.edit()     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.ACTIVEINFO_KEY     // Catch:{ Exception -> 0x0376 }
                        r0 = r12
                        r1 = r41
                        r2 = r18
                        r0.putString(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        r12.commit()     // Catch:{ Exception -> 0x0376 }
                        goto L_0x0093
                    L_0x0376:
                        r41 = move-exception
                        goto L_0x0093
                    L_0x0379:
                        r41 = move-exception
                        r11 = r41
                        java.lang.String r26 = ""
                        goto L_0x0101
                    L_0x0380:
                        r41 = move-exception
                        r10 = r41
                        goto L_0x0093
                    L_0x0385:
                        r41 = move-exception
                        r9 = r41
                        goto L_0x0093
                    L_0x038a:
                        r41 = move-exception
                        r9 = r41
                        goto L_0x0093
                    L_0x038f:
                        r41 = move-exception
                        r9 = r41
                        goto L_0x0093
                    L_0x0394:
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x0564 }
                        r41 = r0
                        android.content.pm.PackageManager r41 = r41.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0564 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x0564 }
                        r42 = r0
                        java.lang.String r42 = r42.getPackageName()     // Catch:{ NameNotFoundException -> 0x0564 }
                        r43 = 0
                        android.content.pm.PackageInfo r41 = r41.getPackageInfo(r42, r43)     // Catch:{ NameNotFoundException -> 0x0564 }
                        r0 = r41
                        java.lang.String r0 = r0.packageName     // Catch:{ NameNotFoundException -> 0x0564 }
                        r29 = r0
                        java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0564 }
                        java.lang.String r42 = java.lang.String.valueOf(r29)     // Catch:{ NameNotFoundException -> 0x0564 }
                        r41.<init>(r42)     // Catch:{ NameNotFoundException -> 0x0564 }
                        java.lang.String r42 = ".activeinfo"
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ NameNotFoundException -> 0x0564 }
                        java.lang.String r4 = r41.toString()     // Catch:{ NameNotFoundException -> 0x0564 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        java.lang.String r42 = "phone"
                        java.lang.Object r36 = r41.getSystemService(r42)     // Catch:{ Exception -> 0x0376 }
                        android.telephony.TelephonyManager r36 = (android.telephony.TelephonyManager) r36     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r8 = r36.getDeviceId()     // Catch:{ Exception -> 0x0376 }
                        if (r8 == 0) goto L_0x03e5
                        java.lang.String r41 = r8.trim()     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.length()     // Catch:{ Exception -> 0x0376 }
                        if (r41 != 0) goto L_0x03ed
                    L_0x03e5:
                        long r41 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r8 = java.lang.String.valueOf(r41)     // Catch:{ Exception -> 0x0376 }
                    L_0x03ed:
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        r42 = 2131099657(0x7f060009, float:1.7811673E38)
                        java.lang.String r41 = r41.getString(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = "{packageName}"
                        java.lang.String r43 = com.kandian.common.StringUtil.urlEncode(r29)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r41, r42, r43)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{partner}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r31)     // Catch:{ Exception -> 0x0376 }
                        r0 = r6
                        r1 = r41
                        r2 = r42
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{deviceId}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r8)     // Catch:{ Exception -> 0x0376 }
                        r0 = r6
                        r1 = r41
                        r2 = r42
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{model}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r28)     // Catch:{ Exception -> 0x0376 }
                        r0 = r6
                        r1 = r41
                        r2 = r42
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{manufacturer}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r27)     // Catch:{ Exception -> 0x0376 }
                        r0 = r6
                        r1 = r41
                        r2 = r42
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{version}"
                        r0 = r6
                        r1 = r41
                        r2 = r39
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{token}"
                        java.lang.String r42 = com.kandian.common.StringUtil.urlEncode(r37)     // Catch:{ Exception -> 0x0376 }
                        r0 = r6
                        r1 = r41
                        r2 = r42
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{uuid}"
                        r0 = r6
                        r1 = r41
                        r2 = r38
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = "{mac}"
                        r0 = r6
                        r1 = r41
                        r2 = r26
                        java.lang.String r6 = com.kandian.common.StringUtil.replace(r0, r1, r2)     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0376 }
                        r41.<init>(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = "&t="
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x0376 }
                        long r42 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r6 = r41.toString()     // Catch:{ Exception -> 0x0376 }
                        r21 = 0
                        java.lang.String r21 = com.kandian.common.StringUtil.getStringFromURL(r6)     // Catch:{ IOException -> 0x0569 }
                        if (r21 == 0) goto L_0x0093
                        java.lang.String r41 = r21.trim()     // Catch:{ Exception -> 0x0376 }
                        int r41 = r41.length()     // Catch:{ Exception -> 0x0376 }
                        if (r41 <= 0) goto L_0x0093
                        org.json.JSONObject r20 = new org.json.JSONObject     // Catch:{ JSONException -> 0x056c }
                        r20.<init>(r21)     // Catch:{ JSONException -> 0x056c }
                        java.lang.String r41 = "deviceId"
                        r0 = r20
                        r1 = r41
                        r2 = r8
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x0576 }
                        java.lang.String r41 = "packageName"
                        r0 = r20
                        r1 = r41
                        r2 = r29
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x0576 }
                        java.lang.String r41 = "partner"
                        r0 = r20
                        r1 = r41
                        r2 = r31
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x0576 }
                        java.lang.String r41 = "lastVisitTime"
                        long r42 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x0576 }
                        r0 = r20
                        r1 = r41
                        r2 = r42
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x0576 }
                        java.lang.String r41 = "uuid"
                        r0 = r20
                        r1 = r41
                        r2 = r38
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x0576 }
                        java.lang.String r41 = "mac"
                        r0 = r20
                        r1 = r41
                        r2 = r26
                        r0.put(r1, r2)     // Catch:{ JSONException -> 0x0576 }
                        r41 = 4
                        r0 = r20
                        r1 = r41
                        java.lang.String r21 = r0.toString(r1)     // Catch:{ JSONException -> 0x0576 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.TAG     // Catch:{ Exception -> 0x0376 }
                        java.lang.StringBuilder r42 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r43 = "Client get the token = "
                        r42.<init>(r43)     // Catch:{ Exception -> 0x0376 }
                        r0 = r42
                        r1 = r37
                        java.lang.StringBuilder r42 = r0.append(r1)     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r42 = r42.toString()     // Catch:{ Exception -> 0x0376 }
                        com.kandian.common.Log.v(r41, r42)     // Catch:{ Exception -> 0x0376 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x0571 }
                        r41 = r0
                        android.content.pm.PackageManager r41 = r41.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0571 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ NameNotFoundException -> 0x0571 }
                        r42 = r0
                        java.lang.String r42 = r42.getPackageName()     // Catch:{ NameNotFoundException -> 0x0571 }
                        r43 = 0
                        android.content.pm.PackageInfo r41 = r41.getPackageInfo(r42, r43)     // Catch:{ NameNotFoundException -> 0x0571 }
                        r0 = r41
                        java.lang.String r0 = r0.packageName     // Catch:{ NameNotFoundException -> 0x0571 }
                        r29 = r0
                        java.lang.StringBuilder r41 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0571 }
                        java.lang.String r42 = java.lang.String.valueOf(r29)     // Catch:{ NameNotFoundException -> 0x0571 }
                        r41.<init>(r42)     // Catch:{ NameNotFoundException -> 0x0571 }
                        java.lang.String r42 = ".activeinfo"
                        java.lang.StringBuilder r41 = r41.append(r42)     // Catch:{ NameNotFoundException -> 0x0571 }
                        java.lang.String r4 = r41.toString()     // Catch:{ NameNotFoundException -> 0x0571 }
                        r0 = r45
                        android.content.Context r0 = r3     // Catch:{ Exception -> 0x0376 }
                        r41 = r0
                        r42 = 0
                        r0 = r41
                        r1 = r4
                        r2 = r42
                        android.content.SharedPreferences r33 = r0.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        android.content.SharedPreferences$Editor r12 = r33.edit()     // Catch:{ Exception -> 0x0376 }
                        java.lang.String r41 = com.kandian.common.AppActiveUtil.ACTIVEINFO_KEY     // Catch:{ Exception -> 0x0376 }
                        r0 = r12
                        r1 = r41
                        r2 = r21
                        r0.putString(r1, r2)     // Catch:{ Exception -> 0x0376 }
                        r12.commit()     // Catch:{ Exception -> 0x0376 }
                        r19 = r20
                        goto L_0x0093
                    L_0x0564:
                        r41 = move-exception
                        r9 = r41
                        goto L_0x0093
                    L_0x0569:
                        r9 = move-exception
                        goto L_0x0093
                    L_0x056c:
                        r41 = move-exception
                        r9 = r41
                    L_0x056f:
                        goto L_0x0093
                    L_0x0571:
                        r41 = move-exception
                        r9 = r41
                        goto L_0x0093
                    L_0x0576:
                        r41 = move-exception
                        r9 = r41
                        r19 = r20
                        goto L_0x056f
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.AppActiveUtil.AnonymousClass2.run():void");
                }
            }).start();
        } else {
            Log.v(TAG, "context is not available");
        }
    }

    public static void updatePartner(final Context context, final String partner) {
        _context = context;
        if (context instanceof Activity) {
            new Thread(new Runnable() {
                public void run() {
                    boolean result = true;
                    if (partner != null && partner.trim().length() > 0) {
                        String PREFS_NAME = null;
                        try {
                            PREFS_NAME = String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName) + ".activeinfo";
                        } catch (PackageManager.NameNotFoundException e) {
                            PackageManager.NameNotFoundException nameNotFoundException = e;
                            result = false;
                        }
                        try {
                            JSONObject localinfo = AppActiveUtil.getActiveInfo(context);
                            String localinfoString = "";
                            if (localinfo != null) {
                                try {
                                    String token = localinfo.getString("token");
                                    if (token == null || token.trim().length() == 0) {
                                        result = false;
                                    }
                                    localinfo.put("partner", partner);
                                    localinfoString = localinfo.toString(4);
                                } catch (JSONException e2) {
                                    JSONException jSONException = e2;
                                    result = false;
                                }
                            } else {
                                result = false;
                            }
                            SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, 0).edit();
                            editor.putString(AppActiveUtil.ACTIVEINFO_KEY, localinfoString);
                            editor.commit();
                            Log.v(AppActiveUtil.TAG, "commit localinfoString = " + localinfoString);
                            if (result) {
                                Message m = Message.obtain(AppActiveUtil.myViewUpdateHandler);
                                m.what = 2;
                                m.sendToTarget();
                                return;
                            }
                            Message m2 = Message.obtain(AppActiveUtil.myViewUpdateHandler);
                            m2.what = 3;
                            m2.sendToTarget();
                        } catch (Exception e3) {
                        }
                    }
                }
            }).start();
        } else {
            Log.v(TAG, "context is not available");
        }
    }

    public static JSONObject getActiveInfo(Context context) {
        JSONObject result = null;
        try {
            String jsonResult = context.getSharedPreferences(String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName) + ".activeinfo", 0).getString(ACTIVEINFO_KEY, "");
            if (jsonResult != null && jsonResult.trim().length() > 0) {
                try {
                    result = new JSONObject(jsonResult);
                } catch (JSONException e) {
                    return null;
                }
            }
            return result;
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void saveUUID(String uuid) {
        try {
            String uuidPath = String.valueOf(new File(PreferenceSetting.getDownloadDir()).getParent()) + "/.system";
            File uuidF = new File(uuidPath);
            uuidF.mkdirs();
            Log.v(TAG, "uuidPath = " + uuidF.getAbsolutePath());
            FileOutputStream out = new FileOutputStream(new File(uuidPath, "Runtime.dat"));
            generateProperties(uuid).store(out, "");
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static String readUUID(Context context) {
        Log.v(TAG, "PreferenceSetting.getDownloadDir() = " + PreferenceSetting.getDownloadDir());
        if (PreferenceSetting.getDownloadDir() == null) {
            PreferenceSetting.setDownloadDir(context);
        }
        File file = new File(String.valueOf(String.valueOf(new File(PreferenceSetting.getDownloadDir()).getParent()) + "/.system") + "/Runtime.dat");
        if (!file.exists()) {
            return null;
        }
        Properties pro = new Properties();
        try {
            FileInputStream in = new FileInputStream(file);
            pro.load(in);
            in.close();
            return (String) pro.get("uuid");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static Properties generateProperties(String uuid) {
        Properties pro = new Properties();
        pro.setProperty("uuid", uuid);
        return pro;
    }
}
