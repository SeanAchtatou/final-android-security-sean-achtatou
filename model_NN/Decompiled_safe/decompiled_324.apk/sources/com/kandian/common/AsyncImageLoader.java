package com.kandian.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;

public class AsyncImageLoader {
    private static final int IO_BUFFER_SIZE = 4096;
    private static AsyncImageLoader _instance = null;
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Drawable>> imageCache = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Bitmap>> imageCache1 = new HashMap<>();

    public interface ImageCallback {
        void imageLoaded(Drawable drawable, String str);
    }

    public interface ImageCallback1 {
        void imageLoaded(Bitmap bitmap, String str);
    }

    protected AsyncImageLoader() {
    }

    public static AsyncImageLoader instance() {
        if (_instance == null) {
            _instance = new AsyncImageLoader();
        }
        return _instance;
    }

    public Drawable loadDrawable(final String imageUrl, final ImageCallback imageCallback) {
        Drawable drawable;
        if (this.imageCache.containsKey(imageUrl) && (drawable = (Drawable) this.imageCache.get(imageUrl).get()) != null) {
            return drawable;
        }
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                imageCallback.imageLoaded((Drawable) message.obj, imageUrl);
            }
        };
        new Thread() {
            public void run() {
                Drawable drawable = AsyncImageLoader.loadImageFromUrl(imageUrl);
                AsyncImageLoader.this.imageCache.put(imageUrl, new SoftReference(drawable));
                handler.sendMessage(handler.obtainMessage(0, drawable));
            }
        }.start();
        return null;
    }

    public Bitmap loadBitmap(String imageUrl, ImageCallback1 imageCallback) {
        return loadBitmap(imageUrl, imageCallback, false);
    }

    public Bitmap loadBitmap(final String imageUrl, final ImageCallback1 imageCallback, final boolean flag) {
        Bitmap bitmap;
        if (imageUrl == null) {
            return null;
        }
        if (this.imageCache1.containsKey(imageUrl) && (bitmap = (Bitmap) this.imageCache1.get(imageUrl).get()) != null) {
            return bitmap;
        }
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                imageCallback.imageLoaded((Bitmap) message.obj, imageUrl);
            }
        };
        new Thread() {
            public void run() {
                Bitmap bitmap = AsyncImageLoader.GetNetBitmap(imageUrl, flag);
                AsyncImageLoader.this.imageCache1.put(imageUrl, new SoftReference(bitmap));
                handler.sendMessage(handler.obtainMessage(0, bitmap));
            }
        }.start();
        return null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.io.InputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable loadImageFromUrl(java.lang.String r9) {
        /*
            r8 = 0
            r5 = 0
            java.net.URL r6 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0019, IOException -> 0x001f, Exception -> 0x0025 }
            r6.<init>(r9)     // Catch:{ MalformedURLException -> 0x0019, IOException -> 0x001f, Exception -> 0x0025 }
            java.lang.Object r7 = r6.getContent()     // Catch:{ MalformedURLException -> 0x0019, IOException -> 0x001f, Exception -> 0x0025 }
            r0 = r7
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ MalformedURLException -> 0x0019, IOException -> 0x001f, Exception -> 0x0025 }
            r5 = r0
        L_0x000f:
            if (r5 == 0) goto L_0x002f
            java.lang.String r7 = "src"
            android.graphics.drawable.Drawable r1 = android.graphics.drawable.Drawable.createFromStream(r5, r7)     // Catch:{ OutOfMemoryError -> 0x002b }
            r7 = r1
        L_0x0018:
            return r7
        L_0x0019:
            r7 = move-exception
            r3 = r7
            r3.printStackTrace()
            goto L_0x000f
        L_0x001f:
            r7 = move-exception
            r2 = r7
            r2.printStackTrace()
            goto L_0x000f
        L_0x0025:
            r7 = move-exception
            r4 = r7
            r4.printStackTrace()
            goto L_0x000f
        L_0x002b:
            r7 = move-exception
            r2 = r7
            r7 = r8
            goto L_0x0018
        L_0x002f:
            r7 = r8
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.AsyncImageLoader.loadImageFromUrl(java.lang.String):android.graphics.drawable.Drawable");
    }

    public static Bitmap GetNetBitmap(String url, boolean flag) {
        IOException e;
        ByteArrayOutputStream dataStream;
        BufferedOutputStream out;
        Bitmap bitmap;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 2;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);
            try {
                dataStream = new ByteArrayOutputStream();
                out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            } catch (IOException e2) {
                e = e2;
                System.out.println(String.valueOf(url) + "===============");
                e.printStackTrace();
                return null;
            }
            try {
                copy(bufferedInputStream, out);
                out.flush();
                byte[] data = dataStream.toByteArray();
                if (flag) {
                    bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
                } else {
                    bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                }
                byte[] data2 = null;
                BufferedOutputStream bufferedOutputStream = out;
                BufferedInputStream bufferedInputStream2 = bufferedInputStream;
                return bitmap;
            } catch (IOException e3) {
                e = e3;
                System.out.println(String.valueOf(url) + "===============");
                e.printStackTrace();
                return null;
            }
        } catch (IOException e4) {
            e = e4;
            System.out.println(String.valueOf(url) + "===============");
            e.printStackTrace();
            return null;
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        while (true) {
            int read = in.read(b);
            if (read != -1) {
                out.write(b, 0, read);
            } else {
                return;
            }
        }
    }

    public void recycleBitMap() {
        if (this.imageCache1 != null && this.imageCache1.size() > 0) {
            try {
                for (SoftReference<Bitmap> sonReference : this.imageCache1.values()) {
                    Bitmap bitmap = (Bitmap) sonReference.get();
                    if (bitmap != null) {
                        bitmap.recycle();
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
