package com.kandian.common;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Pattern;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;

public class StringUtil {
    private static MultiThreadedHttpConnectionManager mgr = new MultiThreadedHttpConnectionManager();

    public static String TwitterCode(String strContent) {
        return Pattern.compile("[#](.[^\\[#]*)[#]").matcher(Pattern.compile("[@](.[^\\[@|:]*)[:]").matcher(strContent).replaceAll("<a href=\"http://t.sina.cn/dpool/ttt/domain.php?n=$1\">@$1</a>:")).replaceAll("<a href=\"http://t.sina.cn/dpool/ttt/hotword.php?keyword=$1\">#$1#</a>");
    }

    public static String HtmlCode(String strContent) {
        return Pattern.compile("([^(=)])((\\w)+[@]{1}((\\w)+[.]){1,3}(\\w)+)").matcher(Pattern.compile("([^(http://|http:\\\\)])((www|cn)[.](\\w)+[.]{1,}(net|com|cn|org|cc)(((\\/[\\~]*|\\[\\~]*)(\\w)+)|[.](\\w)+)*(((([?](\\w)+){1}[=]*))*((\\w)+){1}([\\&](\\w)+[\\=](\\w)+)*)*)").matcher(Pattern.compile("([^>=\"])((http|https|ftp|rtsp|mms):(\\/\\/|\\\\)[A-Za-z0-9\\./=\\?%\\-&_~`@':+!]+)").matcher(Pattern.compile("((http|https|ftp|rtsp|mms):(\\/\\/|\\\\)[A-Za-z0-9\\./=\\?%\\-&_~`@':+!]+)$").matcher(Pattern.compile("^((http|https|ftp|rtsp|mms):(\\/\\/|\\\\)[A-Za-z0-9\\./=\\?%\\-&_~`@':+!]+)").matcher(strContent).replaceAll("<a href=$1>$1</a>")).replaceAll("<a href=$1>$1</a>")).replaceAll("<a href=$2>$2</a>")).replaceAll("<a href=http://$2>$2</a>")).replaceAll("<a href=\"mailto:$2\">$2</a>");
    }

    public static String replace(String source, String regex, String replacement) {
        StringBuffer buffer = new StringBuffer();
        while (true) {
            int index = source.indexOf(regex);
            if (index < 0) {
                buffer.append(source);
                return buffer.toString();
            }
            buffer.append(source.substring(0, index));
            buffer.append(replacement);
            source = source.substring(regex.length() + index);
        }
    }

    /* JADX WARN: Type inference failed for: r11v9, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getStringFromURL(java.lang.String r13) throws java.io.IOException {
        /*
            r12 = 0
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            r6 = 0
            r1 = 0
            r9 = 0
            r4 = 0
            java.net.URL r10 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x0079, all -> 0x008c }
            r10.<init>(r13)     // Catch:{ MalformedURLException -> 0x00ad, IOException -> 0x0079, all -> 0x008c }
            java.net.URLConnection r11 = r10.openConnection()     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r4 = r0
            r11 = 1
            r4.setDoInput(r11)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r11 = 1
            r4.setDoOutput(r11)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r11 = 0
            r4.setUseCaches(r11)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r11 = 10000(0x2710, float:1.4013E-41)
            r4.setConnectTimeout(r11)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r11 = 30000(0x7530, float:4.2039E-41)
            r4.setReadTimeout(r11)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            int r3 = r4.getResponseCode()     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r11 = 200(0xc8, float:2.8E-43)
            if (r3 != r11) goto L_0x004b
            java.io.InputStream r6 = r4.getInputStream()     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r11.<init>(r6)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r2.<init>(r11)     // Catch:{ MalformedURLException -> 0x00b0, IOException -> 0x00a4, all -> 0x009d }
            r7 = 0
        L_0x0044:
            java.lang.String r7 = r2.readLine()     // Catch:{ MalformedURLException -> 0x0064, IOException -> 0x00a8, all -> 0x00a0 }
            if (r7 != 0) goto L_0x0060
            r1 = r2
        L_0x004b:
            if (r1 == 0) goto L_0x0050
            r1.close()
        L_0x0050:
            if (r6 == 0) goto L_0x0055
            r6.close()
        L_0x0055:
            if (r4 == 0) goto L_0x005a
            r4.disconnect()
        L_0x005a:
            java.lang.String r11 = r8.toString()
            r9 = r10
        L_0x005f:
            return r11
        L_0x0060:
            r8.append(r7)     // Catch:{ MalformedURLException -> 0x0064, IOException -> 0x00a8, all -> 0x00a0 }
            goto L_0x0044
        L_0x0064:
            r11 = move-exception
            r5 = r11
            r9 = r10
            r1 = r2
        L_0x0068:
            if (r1 == 0) goto L_0x006d
            r1.close()
        L_0x006d:
            if (r6 == 0) goto L_0x0072
            r6.close()
        L_0x0072:
            if (r4 == 0) goto L_0x0077
            r4.disconnect()
        L_0x0077:
            r11 = r12
            goto L_0x005f
        L_0x0079:
            r11 = move-exception
            r5 = r11
        L_0x007b:
            if (r1 == 0) goto L_0x0080
            r1.close()
        L_0x0080:
            if (r6 == 0) goto L_0x0085
            r6.close()
        L_0x0085:
            if (r4 == 0) goto L_0x008a
            r4.disconnect()
        L_0x008a:
            r11 = r12
            goto L_0x005f
        L_0x008c:
            r11 = move-exception
        L_0x008d:
            if (r1 == 0) goto L_0x0092
            r1.close()
        L_0x0092:
            if (r6 == 0) goto L_0x0097
            r6.close()
        L_0x0097:
            if (r4 == 0) goto L_0x009c
            r4.disconnect()
        L_0x009c:
            throw r11
        L_0x009d:
            r11 = move-exception
            r9 = r10
            goto L_0x008d
        L_0x00a0:
            r11 = move-exception
            r9 = r10
            r1 = r2
            goto L_0x008d
        L_0x00a4:
            r11 = move-exception
            r5 = r11
            r9 = r10
            goto L_0x007b
        L_0x00a8:
            r11 = move-exception
            r5 = r11
            r9 = r10
            r1 = r2
            goto L_0x007b
        L_0x00ad:
            r11 = move-exception
            r5 = r11
            goto L_0x0068
        L_0x00b0:
            r11 = move-exception
            r5 = r11
            r9 = r10
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.StringUtil.getStringFromURL(java.lang.String):java.lang.String");
    }

    private static Object fetch(String address) throws IOException {
        try {
            return new URL(address).getContent();
        } catch (Exception e) {
            return null;
        }
    }

    public static String urlEncode(String obj) {
        return urlEncode(obj, "GBK");
    }

    public static String urlEncode(String obj, String charset) {
        String result = null;
        if (obj != null) {
            try {
                result = URLEncoder.encode(obj, charset);
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        }
        return result;
    }

    /* JADX INFO: finally extract failed */
    public static String requestInvoker(String url, String requestBody) {
        if (url == null || url.trim().length() == 0) {
            return null;
        }
        HttpClient client = new HttpClient(mgr);
        PostMethod method = new PostMethod(url);
        method.setRequestBody(requestBody);
        client.getParams().setContentCharset("GBK");
        try {
            client.executeMethod(method);
            String responseBodyAsString = method.getResponseBodyAsString();
            method.releaseConnection();
            return responseBodyAsString;
        } catch (Exception e) {
            e.printStackTrace();
            method.releaseConnection();
            return null;
        } catch (Throwable th) {
            method.releaseConnection();
            throw th;
        }
    }

    public static String getSharedPreferences(Context context, String preferName, String key) {
        return context.getSharedPreferences(preferName, 0).getString(key, "");
    }

    public static void putSharedPreferences(Context context, String preferName, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(preferName, 0).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static int numberOfStr(String str, String con) {
        String str2 = " " + str;
        if (str2.endsWith(con)) {
            return str2.split(con).length;
        }
        return str2.split(con).length - 1;
    }

    public static String getDomain(String url) {
        try {
            URI uri = new URI(url);
            for (String d : ".com.cn|.com|.cn|.net|.org|.biz|.info|.cc|.tv".split("\\|")) {
                if (uri.getHost().endsWith(d)) {
                    String hostStr = uri.getHost().substring(0, uri.getHost().lastIndexOf(d));
                    return hostStr.substring(hostStr.lastIndexOf(".") + 1, hostStr.length());
                }
            }
            return null;
        } catch (URISyntaxException e) {
            return null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String md5(java.lang.String r7) {
        /*
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r6 = ""
            r1.<init>(r6)
            java.lang.String r6 = "MD5"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r6)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            byte[] r6 = r7.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r4.update(r6)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            byte[] r0 = r4.digest()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r5 = 0
        L_0x0019:
            int r6 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            if (r5 < r6) goto L_0x0021
            java.lang.String r6 = r1.toString()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
        L_0x0020:
            return r6
        L_0x0021:
            byte r3 = r0[r5]     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            if (r3 >= 0) goto L_0x0027
            int r3 = r3 + 256
        L_0x0027:
            r6 = 16
            if (r3 >= r6) goto L_0x0030
            java.lang.String r6 = "0"
            r1.append(r6)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
        L_0x0030:
            java.lang.String r6 = java.lang.Integer.toHexString(r3)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r1.append(r6)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            int r5 = r5 + 1
            goto L_0x0019
        L_0x003a:
            r6 = move-exception
            r2 = r6
            r2.printStackTrace()
            java.lang.String r6 = r1.toString()
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.StringUtil.md5(java.lang.String):java.lang.String");
    }
}
