package com.kandian.common;

import android.app.Application;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.kandian.cartoonapp.R;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;

public class ServiceConfig {
    private static String TAG = "ServiceConfig";
    private static ServiceConfig _self = null;
    public static String defaultServiceEntrance = "service.51tv.com";
    public static String defaultServiceEntranceName = "通用线路";
    private Application ownerApplication = null;
    private List<ServiceEntrance> serviceList = null;

    public class ServiceEntrance {
        private String entrance = null;
        private String name = null;
        int status = 1;

        public ServiceEntrance() {
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getName() {
            return this.name;
        }

        public void setEntrance(String entrance2) {
            this.entrance = entrance2;
        }

        public String getEntrance() {
            return this.entrance;
        }
    }

    protected ServiceConfig(Application app) {
        this.ownerApplication = app;
        initializeServiceConfig();
    }

    public static ServiceConfig instance(Application app) {
        if (_self == null) {
            _self = new ServiceConfig(app);
        }
        return _self;
    }

    public boolean initializeServiceConfig() {
        return initializeServiceConfigFromUrl();
    }

    public boolean initializeServiceConfigFromUrl() {
        try {
            String url = this.ownerApplication.getString(R.string.serviceConfigUrl);
            Log.v(TAG, "get configs from " + url);
            return initializeServiceConfigFromInputStream((InputStream) new URL(url).getContent());
        } catch (Exception e) {
            return false;
        }
    }

    public boolean initializeServiceConfigFromInputStream(InputStream input) {
        boolean z;
        RootElement root = new RootElement("DOCUMENT");
        final ArrayList<ServiceEntrance> serviceEntrances = new ArrayList<>();
        Element site = root.getChild("serviceconfig").getChild("service");
        site.setStartElementListener(new StartElementListener() {
            public void start(Attributes attributes) {
                serviceEntrances.add(new ServiceEntrance());
            }
        });
        site.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        site.getChild("name").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && serviceEntrances.size() > 0) {
                    ((ServiceEntrance) serviceEntrances.get(serviceEntrances.size() - 1)).setName(new String(body));
                }
            }
        });
        site.getChild("entrance").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && serviceEntrances.size() > 0) {
                    ((ServiceEntrance) serviceEntrances.get(serviceEntrances.size() - 1)).setEntrance(new String(body));
                }
            }
        });
        site.getChild("status").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && serviceEntrances.size() > 0) {
                    ((ServiceEntrance) serviceEntrances.get(serviceEntrances.size() - 1)).status = Integer.parseInt(body);
                }
            }
        });
        try {
            Xml.parse(input, Xml.Encoding.UTF_8, root.getContentHandler());
            if (serviceEntrances != null) {
                z = true;
            } else {
                z = false;
            }
            if (!z || !(serviceEntrances.size() > 0)) {
                return false;
            }
            this.serviceList = serviceEntrances;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<ServiceEntrance> getServiceList() {
        if (this.serviceList == null || this.serviceList.size() <= 1) {
            initializeServiceConfig();
        }
        if (this.serviceList == null || this.serviceList.size() == 0) {
            this.serviceList = new ArrayList();
            ServiceEntrance s = new ServiceEntrance();
            s.setName(defaultServiceEntranceName);
            s.setEntrance(defaultServiceEntrance);
            this.serviceList.add(s);
        }
        return this.serviceList;
    }

    public void setServiceList(List<ServiceEntrance> list) {
        this.serviceList = list;
    }
}
