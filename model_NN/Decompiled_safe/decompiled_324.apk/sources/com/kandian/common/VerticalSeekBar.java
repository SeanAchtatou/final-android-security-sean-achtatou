package com.kandian.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsSeekBar;
import weibo4j.AsyncWeibo;

public class VerticalSeekBar extends AbsSeekBar {
    public float curr;
    private boolean flag;
    private int height;
    private OnSeekBarChangeListener mOnSeekBarChangeListener;
    private Drawable mThumb;
    public float step;
    private int width;

    public interface OnSeekBarChangeListener {
        void onProgressChanged(VerticalSeekBar verticalSeekBar, int i, boolean z);

        void onStartTrackingTouch(VerticalSeekBar verticalSeekBar);

        void onStopTrackingTouch(VerticalSeekBar verticalSeekBar);
    }

    public VerticalSeekBar(Context context) {
        this(context, null);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 16842875);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.curr = (float) this.height;
        this.step = 0.0f;
        this.flag = false;
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener l) {
        this.mOnSeekBarChangeListener = l;
    }

    /* access modifiers changed from: package-private */
    public void onStartTrackingTouch() {
        if (this.mOnSeekBarChangeListener != null) {
            this.mOnSeekBarChangeListener.onStartTrackingTouch(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void onStopTrackingTouch() {
        if (this.mOnSeekBarChangeListener != null) {
            this.mOnSeekBarChangeListener.onStopTrackingTouch(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void onProgressRefresh(float scale, boolean fromUser) {
        Drawable thumb = this.mThumb;
        if (thumb != null) {
            setThumbPos(getHeight(), thumb, scale, Integer.MIN_VALUE);
            invalidate();
        }
        if (this.mOnSeekBarChangeListener != null) {
            if (getProgress() + 2 > getMax()) {
                this.curr = 0.0f;
            }
            this.mOnSeekBarChangeListener.onProgressChanged(this, getProgress(), fromUser);
        }
    }

    private void setThumbPos(int w, Drawable thumb, float scale, int gap) {
        int topBound;
        int bottomBound;
        int thumbWidth = thumb.getIntrinsicWidth();
        int thumbHeight = thumb.getIntrinsicHeight();
        int thumbPos = (int) (((float) ((((w - getPaddingLeft()) - getPaddingRight()) - thumbWidth) + (getThumbOffset() * 2))) * scale);
        if (gap == Integer.MIN_VALUE) {
            Rect oldBounds = thumb.getBounds();
            topBound = oldBounds.top;
            bottomBound = oldBounds.bottom;
        } else {
            topBound = gap;
            bottomBound = gap + thumbHeight;
        }
        thumb.setBounds(thumbPos, topBound, thumbPos + thumbWidth, bottomBound);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas c) {
        c.rotate(-90.0f);
        c.translate((float) (-this.height), 0.0f);
        super.onDraw(c);
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.height = View.MeasureSpec.getSize(heightMeasureSpec);
        this.width = View.MeasureSpec.getSize(widthMeasureSpec);
        this.step = ((float) this.height) / ((float) getMax());
        this.curr = ((float) this.height) - (((float) getProgress()) * this.step);
        setMeasuredDimension(this.width, this.height);
    }

    public void setThumb(Drawable thumb) {
        this.mThumb = thumb;
        super.setThumb(thumb);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(h, w, oldw, oldh);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        switch (event.getAction()) {
            case 0:
                setPressed(true);
                if (event.getY() > this.curr - 80.0f && event.getY() < this.curr + 80.0f) {
                    this.flag = true;
                }
                onStartTrackingTouch();
                trackTouchEvent(event);
                break;
            case 1:
                if (this.flag) {
                    this.curr = event.getY();
                    if (this.curr < 0.0f) {
                        this.curr = 0.0f;
                    }
                    if (this.curr > ((float) getHeight())) {
                        this.curr = (float) getHeight();
                    }
                    onStopTrackingTouch();
                }
                this.flag = false;
                trackTouchEvent(event);
                setPressed(false);
                break;
            case 2:
                trackTouchEvent(event);
                attemptClaimDrag();
                break;
            case 3:
                if (this.flag) {
                    onStopTrackingTouch();
                }
                setPressed(false);
                break;
        }
        return true;
    }

    private void trackTouchEvent(MotionEvent event) {
        float scale;
        if (this.flag) {
            int Height = getHeight();
            int available = (Height - getPaddingBottom()) - getPaddingTop();
            int Y = (int) event.getY();
            if (Y > Height - getPaddingBottom()) {
                scale = 0.0f;
            } else if (Y < getPaddingTop()) {
                scale = 1.0f;
            } else {
                scale = ((float) ((Height - getPaddingBottom()) - Y)) / ((float) available);
            }
            setProgress((int) (scale * ((float) getMax())));
        }
    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        KeyEvent newEvent;
        if (event.getAction() != 0) {
            return false;
        }
        switch (event.getKeyCode()) {
            case AsyncWeibo.DESTROY_FAVORITE /*19*/:
                newEvent = new KeyEvent(0, 22);
                break;
            case 20:
                newEvent = new KeyEvent(0, 21);
                break;
            case AsyncWeibo.UPDATE_DELIVERLY_DEVICE /*21*/:
                newEvent = new KeyEvent(0, 20);
                break;
            case AsyncWeibo.BLOCK /*22*/:
                newEvent = new KeyEvent(0, 19);
                break;
            default:
                newEvent = new KeyEvent(0, event.getKeyCode());
                break;
        }
        return newEvent.dispatch(this);
    }
}
