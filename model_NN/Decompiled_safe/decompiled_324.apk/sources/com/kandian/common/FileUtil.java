package com.kandian.common;

import java.io.File;
import java.io.FileFilter;

public class FileUtil {
    private static String TAG = "FileUtil";

    public static void storageDefrag(File downloadDir, File[] taskFiles) {
        try {
            File[] downloadFileDirs = downloadDir.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    if (pathname == null || !pathname.isDirectory()) {
                        return false;
                    }
                    return true;
                }
            });
            for (int i = 0; i < downloadFileDirs.length; i++) {
                if (!checkDownloadFileDir(taskFiles, downloadFileDirs[i].getName())) {
                    deleteFile(downloadFileDirs[i].getAbsoluteFile());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkDownloadFileDir(File[] taskFiles, String downloadFileDirName) {
        for (File name : taskFiles) {
            if (name.getName().startsWith(downloadFileDirName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean deleteFile(String filePath) {
        return deleteFile(new File(filePath));
    }

    public static boolean deleteFile(File file) {
        for (File deleteFile : file.listFiles()) {
            if (deleteFile.isDirectory()) {
                if (!deleteFile(deleteFile)) {
                    return false;
                }
            } else if (!deleteFile.delete()) {
                return false;
            }
        }
        return file.delete();
    }
}
