package com.kandian.common;

import android.app.Application;
import com.kandian.cartoonapp.R;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class VideoAssetMap {
    static String TAG = "VideoAssetMap";
    private static VideoAssetMap _instance = null;
    private HashMap<String, SoftReference<VideoAsset>> assetCache = new HashMap<>();

    protected VideoAssetMap() {
    }

    public static VideoAssetMap instance() {
        if (_instance == null) {
            _instance = new VideoAssetMap();
        }
        return _instance;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public VideoAsset getAsset(String assetKey, String assetType, Application app) {
        SoftReference<VideoAsset> result = this.assetCache.get(String.valueOf(assetKey) + "_" + assetType);
        if (result == null || result.get() == null) {
            return getAssetByKeyFromNetwork(assetKey, assetType, app);
        }
        return (VideoAsset) result.get();
    }

    public void putAsset(String assetKey, String assetType, VideoAsset asset) {
        this.assetCache.put(String.valueOf(assetKey) + "_" + assetType, new SoftReference(asset));
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public VideoAsset getAssetByKeyFromNetwork(String assetKey, String assetType, Application app) {
        AssetList results = parse(String.valueOf(app.getString(R.string.singleAssetServicePrefix)) + "&fq=key%3A" + assetKey + "&fq=assetType%3A" + assetType, app);
        if (results == null || results.getCurrentItemCount() <= 0) {
            return null;
        }
        return results.getList().get(0);
    }

    public VideoAsset getAsset(long assetId, String assetType, Application app) {
        return getAsset(String.valueOf(assetId) + "_0", assetType, app);
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        Log.v(TAG, "Fetching  " + address);
        return new URL(address).getContent();
    }

    public AssetList parse(String assetsListUrl, Application app) {
        AssetList resultList = new AssetList();
        AssetListSaxHandler saxHandler = new AssetListSaxHandler(app, resultList);
        try {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            InputStream in = (InputStream) fetch(assetsListUrl);
            if (in != null) {
                parser.parse(in, saxHandler);
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        return resultList;
    }
}
