package com.kandian.common;

import android.app.Activity;
import com.mobclick.android.MobclickAgent;

public class MobclickAgentWrapper {
    public static void onResume(Activity a) {
        MobclickAgent.onResume(a);
    }

    public static void onPause(Activity a) {
        MobclickAgent.onPause(a);
    }

    public static void onError(Activity a) {
    }
}
