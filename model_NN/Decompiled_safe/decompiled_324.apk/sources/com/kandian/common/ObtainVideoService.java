package com.kandian.common;

import android.app.Application;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.kandian.cartoonapp.R;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class ObtainVideoService {
    static String TAG = "ObtainVideoService";
    public static final int TYPE_FLV = 1;
    public static final int TYPE_MP4 = 2;
    public static final int TYPE_TS = 3;
    /* access modifiers changed from: private */
    public static PlayUrl playUrl = null;

    /* JADX INFO: Multiple debug info for r0v4 android.sax.Element: [D('resultcodeElement' android.sax.Element), D('playurls' android.sax.Element)] */
    /* JADX INFO: Multiple debug info for r0v5 java.lang.String: [D('resultcodeElement' android.sax.Element), D('serviceEntrance' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v29 java.lang.String: [D('token' java.lang.String), D('toParse' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v31 java.net.HttpURLConnection: [D('huc' java.net.HttpURLConnection), D('toParse' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v33 java.io.InputStream: [D('input' java.io.InputStream), D('huc' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r6v37 android.net.wifi.WifiInfo: [D('wifi' android.net.wifi.WifiManager), D('info' android.net.wifi.WifiInfo)] */
    public static ArrayList<String> getVideos(String playAddressServiceUrl, final Application app, int type) throws KSException {
        String toParse;
        String token;
        RootElement root = new RootElement("DOCUMENT");
        final ArrayList<String> urls = new ArrayList<>();
        Element url = root.getChild("flvinfolist").getChild("flvurl");
        Element playurls = root.getChild("resultcode");
        url.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        url.setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    urls.add(new String(body));
                }
            }
        });
        playurls.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        playurls.setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && !"0".endsWith(new String(body))) {
                    try {
                        throw new KSException(app.getString(R.string.flvcat_exception_parsefail));
                    } catch (KSException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        try {
            String serviceEntrance = StringUtil.replace(app.getString(R.string.obtainVideoServicePrefix), "service.51tv.com", PreferenceSetting.getServiceEntrance(app));
            Log.v(TAG, " serviceEntrance = " + serviceEntrance);
            String toParse2 = String.valueOf(serviceEntrance) + URLEncoder.encode(playAddressServiceUrl);
            switch (type) {
                case 1:
                    toParse = String.valueOf(toParse2) + "&hd=0";
                    break;
                case 2:
                    toParse = String.valueOf(toParse2) + "&hd=1";
                    break;
                case 3:
                    toParse = String.valueOf(toParse2) + "&hd=2";
                    break;
                default:
                    toParse = toParse2;
                    break;
            }
            try {
                WifiInfo info = ((WifiManager) app.getSystemService("wifi")).getConnectionInfo();
                Log.v(TAG, "MacAddress = " + info.getMacAddress());
                token = StringUtil.md5(String.valueOf(info.getMacAddress()) + DateUtil.getTimestamp());
            } catch (Exception e) {
                token = StringUtil.md5(DateUtil.getTimestamp());
            }
            String toParse3 = String.valueOf(toParse) + "&token=" + token;
            Log.v(TAG, "get flvs from " + toParse3);
            HttpURLConnection huc = (HttpURLConnection) new URL(toParse3).openConnection();
            Log.v(TAG, "getResponseCode = " + huc.getResponseCode());
            if (new StringBuilder(String.valueOf(huc.getResponseCode())).toString().startsWith("5") || new StringBuilder(String.valueOf(huc.getResponseCode())).toString().startsWith("4")) {
                throw new KSException(app.getString(R.string.flvcat_exception_notfound));
            } else if (!new StringBuilder(String.valueOf(huc.getResponseCode())).toString().startsWith(HtmlUtil.TYPE_DOWN)) {
                throw new KSException(app.getString(R.string.flvcat_exception_parsefail));
            } else {
                Xml.parse(huc.getInputStream(), Xml.Encoding.UTF_8, root.getContentHandler());
                return urls;
            }
        } catch (MalformedURLException e2) {
            throw new KSException(app.getString(R.string.flvcat_exception_notfound));
        } catch (IOException e3) {
            throw new KSException(app.getString(R.string.flvcat_exception_notfound));
        } catch (SAXException e4) {
            throw new KSException(app.getString(R.string.flvcat_exception_parsefail));
        }
    }

    public static ArrayList<String> getPlayUrls(VideoAsset asset, Application app) {
        if (asset == null) {
            return null;
        }
        String toParse = String.valueOf(app.getString(R.string.playUrlServicePrefix)) + "type=" + asset.getAssetType() + "&id=" + asset.getAssetId();
        if (!asset.isParentAsset()) {
            toParse = String.valueOf(toParse) + "&iid=" + asset.getItemId();
        }
        return getPlayUrls(toParse, app);
    }

    private static ArrayList<String> getPlayUrls(String playAddressServiceUrl, Application app) {
        Log.v(TAG, "Getting playUrls at " + playAddressServiceUrl);
        RootElement root = new RootElement("DOCUMENT");
        final ArrayList<String> urls = new ArrayList<>();
        Element url = root.getChild("asset").getChild("playurls").getChild("url");
        url.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        url.setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    if (body.contains("|")) {
                        Log.v(ObtainVideoService.TAG, "body contains |");
                        String[] segments = body.split("\\|");
                        for (int i = 0; i < segments.length; i++) {
                            if (segments[i] != null && !segments[i].trim().equals("")) {
                                Log.v(ObtainVideoService.TAG, "Segment " + i + ":" + segments[i]);
                                urls.add(new String(segments[i]));
                            }
                        }
                        return;
                    }
                    urls.add(new String(body));
                }
            }
        });
        try {
            InputStream input = (InputStream) new URL(playAddressServiceUrl).getContent();
            if (input != null) {
                Xml.parse(input, Xml.Encoding.UTF_8, root.getContentHandler());
            } else {
                Log.v(TAG, "inputStream is null: " + playAddressServiceUrl);
            }
            return urls;
        } catch (Exception e) {
            return null;
        }
    }

    public static ArrayList<PlayUrl> getNewPlayUrls(VideoAsset asset, Application app) {
        if (asset == null) {
            return null;
        }
        String toParse = String.valueOf(app.getString(R.string.playUrlServicePrefix)) + "type=" + asset.getAssetType() + "&id=" + asset.getAssetId();
        if (!asset.isParentAsset()) {
            toParse = String.valueOf(toParse) + "&iid=" + asset.getItemId();
        }
        return getNewPlayUrls(toParse, app);
    }

    private static ArrayList<PlayUrl> getNewPlayUrls(String playAddressServiceUrl, Application app) {
        Log.v(TAG, "Getting playUrls at " + playAddressServiceUrl);
        RootElement root = new RootElement("DOCUMENT");
        final ArrayList<PlayUrl> urls = new ArrayList<>();
        Element playurl = root.getChild("asset").getChild("playurls").getChild("playurl");
        playurl.setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                ObtainVideoService.playUrl = new PlayUrl();
            }
        });
        playurl.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        playurl.getChild("resourcecode").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null) {
                    ObtainVideoService.playUrl.setResourcecode(body.trim());
                }
            }
        });
        playurl.getChild("hd").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null) {
                    ObtainVideoService.playUrl.setHd(Integer.parseInt(body.trim()));
                }
            }
        });
        playurl.getChild(HtmlUtil.PLATFORM_IPAD).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null) {
                    ObtainVideoService.playUrl.setIpad(Integer.parseInt(body.trim()));
                }
            }
        });
        Element url = playurl.getChild("url");
        url.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        url.setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("")) {
                    if (body.contains("|")) {
                        Log.v(ObtainVideoService.TAG, "body contains |");
                        String[] segments = body.split("\\|");
                        for (int i = 0; i < segments.length; i++) {
                            if (segments[i] != null && !segments[i].trim().equals("")) {
                                Log.v(ObtainVideoService.TAG, "Segment " + i + ":" + segments[i]);
                                ObtainVideoService.playUrl.setUrl(new String(segments[i]));
                                urls.add(ObtainVideoService.playUrl);
                            }
                        }
                        return;
                    }
                    ObtainVideoService.playUrl.setUrl(new String(body));
                    urls.add(ObtainVideoService.playUrl);
                }
            }
        });
        try {
            InputStream input = (InputStream) new URL(playAddressServiceUrl).getContent();
            if (input != null) {
                Xml.parse(input, Xml.Encoding.UTF_8, root.getContentHandler());
            } else {
                Log.v(TAG, "inputStream is null: " + playAddressServiceUrl);
            }
            return urls;
        } catch (Exception e) {
            return null;
        }
    }
}
