package com.kandian.common;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.kandian.cartoonapp.R;
import com.kandian.common.SiteConfigs;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.net.URL;
import java.util.ArrayList;
import org.xml.sax.Attributes;

public class SystemConfigService extends Service {
    private static String TAG = "SystemConfig";
    private final IBinder mBinder = new LocalBinder();
    private NotificationManager mNM;
    /* access modifiers changed from: private */
    public SiteConfigs siteConfigs = null;

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        /* access modifiers changed from: package-private */
        public SystemConfigService getService() {
            return SystemConfigService.this;
        }
    }

    public void onCreate() {
        this.mNM = (NotificationManager) getSystemService("notification");
        if (!initializeSiteConfigs()) {
            Log.v(TAG, "failed to create site configs");
        }
    }

    public void onDestroy() {
    }

    public boolean initializeSiteConfigs() {
        return initializeSiteConfigsFromUrl();
    }

    public boolean initializeSiteConfigsFromUrl() {
        try {
            String url = getString(R.string.siteConfigUrl);
            Log.v(TAG, "get configs from " + url);
            return initializeSiteConfigsFromInputStream((InputStream) new URL(url).getContent());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean initializeSiteConfigsFromSharedPreferences() {
        String value = getSharedPreferences(getString(R.string.siteConfigPref), 0).getString("key", null);
        if (value != null) {
            return initializeSiteConfigsFromInputStream(new StringBufferInputStream(value));
        }
        return false;
    }

    public boolean initializeSiteConfigsFromResources() {
        return initializeSiteConfigsFromInputStream(new StringBufferInputStream(getString(R.string.siteConfig).trim()));
    }

    public boolean initializeSiteConfigsFromInputStream(InputStream input) {
        RootElement root = new RootElement("DOCUMENT");
        final ArrayList<SiteConfigs.Site> configs = new ArrayList<>();
        Element site = root.getChild("sites").getChild("site");
        site.setStartElementListener(new StartElementListener() {
            public void start(Attributes attributes) {
                ArrayList arrayList = configs;
                SiteConfigs access$0 = SystemConfigService.this.siteConfigs;
                access$0.getClass();
                arrayList.add(new SiteConfigs.Site());
            }
        });
        site.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        site.getChild("code").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && configs.size() > 0) {
                    ((SiteConfigs.Site) configs.get(configs.size() - 1)).code = new String(body);
                }
            }
        });
        site.getChild("name").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && configs.size() > 0) {
                    ((SiteConfigs.Site) configs.get(configs.size() - 1)).name = new String(body);
                }
            }
        });
        site.getChild("filetype").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && configs.size() > 0) {
                    ((SiteConfigs.Site) configs.get(configs.size() - 1)).fileType = new String(body);
                }
            }
        });
        site.getChild("urlrules").getChild("match").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && configs.size() > 0) {
                    ((SiteConfigs.Site) configs.get(configs.size() - 1)).urlRules.add(new String(body));
                }
            }
        });
        try {
            Xml.parse(input, Xml.Encoding.UTF_8, root.getContentHandler());
            if (!(configs != null) || !(configs.size() > 0)) {
                return false;
            }
            for (int i = 0; i < configs.size(); i++) {
                SiteConfigs.Site s = (SiteConfigs.Site) configs.get(i);
                if (s.code == null || s.fileType == null || s.urlRules.size() == 0) {
                    return false;
                }
            }
            setSiteConfigs(configs);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public SiteConfigs getSiteConfigs() {
        if (this.siteConfigs == null) {
            initializeSiteConfigs();
        }
        return this.siteConfigs;
    }

    public void setSiteConfigs(ArrayList<SiteConfigs.Site> siteList) {
        if (this.siteConfigs != null) {
            this.siteConfigs.setSiteConfigs(siteList);
            getSharedPreferences(getString(R.string.siteConfigPref), 0).edit().putString("key", this.siteConfigs.siteConfigsToXml());
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }
}
