package com.kandian.common;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import com.kandian.cartoonapp.R;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class PreferenceSetting {
    public static final String KS_MEDIAFILE_SUBDIR = "/video/";
    private static String KS_ROOT_DIR = "ks_root_dir";
    private static String TAG = "PreferenceSetting";
    private static String downloadDir = null;
    private static String mediaFileDir = null;

    public static String getDownloadDir() {
        Log.v(TAG, "System.getProperty(kuaishou.downloadDir) = " + System.getProperty("kuaishou.downloadDir"));
        return System.getProperty("kuaishou.downloadDir");
    }

    public static void setDownloadDir(Context context) {
        String mntPath = null;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        if (!settings.getString(KS_ROOT_DIR, "").equals("")) {
            mntPath = settings.getString(KS_ROOT_DIR, "");
            Log.v(TAG, "KS_ROOT_DIR = " + mntPath);
        } else {
            try {
                mntPath = System.getProperty("java.io.tmpdir");
                if (((long) new StatFs(mntPath).getBlockCount()) <= 0) {
                    if ("mounted".equals(Environment.getExternalStorageState())) {
                        Log.v(TAG, "Environment.getExternalStorageState() = " + Environment.getExternalStorageState());
                        mntPath = Environment.getExternalStorageDirectory().toString();
                    } else {
                        try {
                            try {
                                long blockCount = (long) new StatFs("/mnt/flash").getBlockCount();
                                Log.v(TAG, "/mnt/flash blockCount = " + blockCount);
                                if (blockCount > 0) {
                                    mntPath = "/mnt/flash";
                                }
                            } catch (Exception e) {
                            }
                        } catch (Exception e2) {
                        }
                    }
                }
            } catch (Exception e3) {
            }
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(KS_ROOT_DIR, mntPath);
            editor.commit();
        }
        String downloadDir2 = String.valueOf(mntPath) + context.getString(R.string.kuaishou_downloadDir);
        System.setProperty("kuaishou.downloadDir", downloadDir2);
        downloadDir = downloadDir2;
    }

    public static String getMediaFileDir() {
        Log.v(TAG, "get downloadRootDir = " + System.getProperty("kuaishou.downloadDir.mediaFileDir"));
        return System.getProperty("kuaishou.downloadDir.mediaFileDir");
    }

    public static void setMediaFileDir(Context context, String mediaFileDirString) {
        String dDir;
        String defaultStorage;
        if (mediaFileDirString == null) {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                defaultStorage = Environment.getExternalStorageDirectory().toString();
            } else {
                defaultStorage = System.getProperty("java.io.tmpdir");
            }
            dDir = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.setting_download_dir_key), defaultStorage);
        } else {
            dDir = mediaFileDirString;
        }
        if (!dDir.endsWith(CookieSpec.PATH_DELIM)) {
            dDir = String.valueOf(dDir) + CookieSpec.PATH_DELIM;
        }
        if (!dDir.endsWith(context.getString(R.string.kuaishou_downloadDir))) {
            if (dDir.endsWith(CookieSpec.PATH_DELIM)) {
                dDir = dDir.substring(0, dDir.lastIndexOf(CookieSpec.PATH_DELIM));
            }
            dDir = String.valueOf(dDir) + context.getString(R.string.kuaishou_downloadDir);
        }
        System.setProperty("kuaishou.downloadDir.mediaFileDir", dDir);
        mediaFileDir = dDir;
        Log.v(TAG, "set downloadRootDir = " + dDir);
    }

    public static boolean getDownloadAutoResume(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.setting_download_autoResume_key), false);
    }

    public static String getServiceEntrance(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.setting_service_entrance_key), ServiceConfig.defaultServiceEntrance);
    }

    public static boolean getDownloadByWifi(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        if (context.getString(R.string.download_by_wifi_only).equals("true")) {
            return settings.getBoolean(context.getString(R.string.setting_download_wifi_key), true);
        }
        return settings.getBoolean(context.getString(R.string.setting_download_wifi_key), false);
    }

    public static String getSystemConfigFilterSites(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.setting_systemconfig_filter_key), "");
    }

    public static boolean getVideoplayerFullscreen(Context context) {
        return context.getSharedPreferences(context.getString(R.string.video_progress_settings), 0).getBoolean(context.getString(R.string.video_fullscreen_attr), false);
    }

    public static boolean getContinuePlay(Application app) {
        return app.getSharedPreferences(app.getString(R.string.video_progress_settings), 0).getBoolean(app.getString(R.string.continuePlay_key), false);
    }

    public static void setContinuePlay(Application app, boolean isContinuePlay) {
        SharedPreferences.Editor editor = app.getSharedPreferences(app.getString(R.string.video_progress_settings), 0).edit();
        editor.putBoolean(app.getString(R.string.continuePlay_key), isContinuePlay);
        editor.commit();
    }
}
