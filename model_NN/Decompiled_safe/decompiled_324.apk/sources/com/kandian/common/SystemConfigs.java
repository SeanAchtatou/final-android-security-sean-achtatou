package com.kandian.common;

import android.app.Application;
import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.kandian.cartoonapp.R;
import com.kandian.common.SiteConfigs;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import org.xml.sax.Attributes;

public class SystemConfigs {
    private static String TAG = "SystemConfigs";
    private static SystemConfigs _self = null;
    private Application ownerApplication = null;
    private SiteConfigs siteConfigs = null;

    protected SystemConfigs(Application app) {
        this.ownerApplication = app;
        initializeSiteConfigs();
    }

    public static SystemConfigs instance(Application app) {
        if (_self == null) {
            _self = new SystemConfigs(app);
        }
        return _self;
    }

    public boolean initializeSiteConfigs() {
        return initializeSiteConfigsFromUrl();
    }

    public boolean initializeSiteConfigsFromUrl() {
        try {
            String url = this.ownerApplication.getString(R.string.siteConfigUrl);
            Log.v(TAG, "get configs from " + url);
            return initializeSiteConfigsFromInputStream((InputStream) new URL(url).getContent());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean initializeSiteConfigsFromInputStream(InputStream input) {
        SiteConfigs sConfigs = new SiteConfigs();
        RootElement rootElement = new RootElement("DOCUMENT");
        ArrayList<SiteConfigs.Site> configs = new ArrayList<>();
        Element site = rootElement.getChild("sites").getChild("site");
        final ArrayList<SiteConfigs.Site> arrayList = configs;
        final SiteConfigs siteConfigs2 = sConfigs;
        site.setStartElementListener(new StartElementListener() {
            public void start(Attributes attributes) {
                ArrayList arrayList = arrayList;
                SiteConfigs siteConfigs = siteConfigs2;
                siteConfigs.getClass();
                arrayList.add(new SiteConfigs.Site());
            }
        });
        site.setEndElementListener(new EndElementListener() {
            public void end() {
            }
        });
        final ArrayList<SiteConfigs.Site> arrayList2 = configs;
        site.getChild("code").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && arrayList2.size() > 0) {
                    ((SiteConfigs.Site) arrayList2.get(arrayList2.size() - 1)).code = new String(body);
                }
            }
        });
        final ArrayList<SiteConfigs.Site> arrayList3 = configs;
        site.getChild("name").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && arrayList3.size() > 0) {
                    ((SiteConfigs.Site) arrayList3.get(arrayList3.size() - 1)).name = new String(body);
                }
            }
        });
        final ArrayList<SiteConfigs.Site> arrayList4 = configs;
        site.getChild("referer").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && arrayList4.size() > 0) {
                    ((SiteConfigs.Site) arrayList4.get(arrayList4.size() - 1)).referer = Integer.parseInt(body);
                }
            }
        });
        final ArrayList<SiteConfigs.Site> arrayList5 = configs;
        site.getChild("filetype").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && arrayList5.size() > 0) {
                    ((SiteConfigs.Site) arrayList5.get(arrayList5.size() - 1)).fileType = new String(body);
                }
            }
        });
        final ArrayList<SiteConfigs.Site> arrayList6 = configs;
        site.getChild("urlrules").getChild("match").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                if (body != null && !body.trim().equals("") && arrayList6.size() > 0) {
                    ((SiteConfigs.Site) arrayList6.get(arrayList6.size() - 1)).urlRules.add(new String(body));
                }
            }
        });
        try {
            Xml.parse(input, Xml.Encoding.UTF_8, rootElement.getContentHandler());
            if (!(configs != null) || !(configs.size() > 0)) {
                return false;
            }
            for (int i = 0; i < configs.size(); i++) {
                SiteConfigs.Site s = (SiteConfigs.Site) configs.get(i);
                if (s.code == null || s.fileType == null || s.urlRules.size() == 0) {
                    return false;
                }
            }
            this.siteConfigs = sConfigs;
            this.siteConfigs.setSiteConfigs(configs);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public SiteConfigs getSiteConfigs() {
        if (this.siteConfigs == null) {
            initializeSiteConfigs();
        }
        return this.siteConfigs;
    }

    public void setSiteConfigs(ArrayList<SiteConfigs.Site> siteList) {
        if (this.siteConfigs != null) {
            this.siteConfigs.setSiteConfigs(siteList);
        }
    }
}
