package com.kandian.common;

import java.util.ArrayList;

public class AssetList {
    ArrayList<VideoAsset> list = new ArrayList<>();
    private int totalResultCount = 0;

    public void setTotalResultCount(int totalResultCount2) {
        this.totalResultCount = totalResultCount2;
    }

    public int getTotalResultCount() {
        return this.totalResultCount;
    }

    public int getCurrentItemCount() {
        return this.list.size();
    }

    public void clear() {
        this.list.clear();
        this.totalResultCount = 0;
    }

    public ArrayList<VideoAsset> getList() {
        return this.list;
    }

    public void appendList(AssetList fromList) {
        this.list.addAll((ArrayList) fromList.getList().clone());
        this.totalResultCount = fromList.getTotalResultCount();
    }

    public void remove(int i) {
        this.list.remove(i);
        this.totalResultCount--;
    }

    public VideoAsset get(int i) {
        return this.list.get(i);
    }
}
