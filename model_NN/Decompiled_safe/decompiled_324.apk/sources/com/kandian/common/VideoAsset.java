package com.kandian.common;

import android.app.Application;
import java.io.Serializable;

public class VideoAsset implements Cloneable, Serializable {
    private static final long serialVersionUID = -2072574083677171949L;
    private String assetActor = "";
    private String assetCode = "";
    private String assetDescription = "";
    private String assetDirector = "";
    private long assetId = 0;
    private long assetIdX = 0;
    private String assetKey = "";
    private String assetName = "";
    private String assetPinyin = "";
    private String assetSource = "";
    private String assetSourceCode = "";
    private String assetType = "";
    private int assetYear = 0;
    private String bigImageUrl = "";
    private String category = "";
    private int finished = 0;
    private String imageUrl = "";
    private long itemId = 0;
    private String origin = "";
    private int score = 0;
    private String showTime = "";
    private String smallPhotoImageUrl = "";
    private int total = 0;
    private String type = "0";
    private double vote = -1.0d;

    public String getSmallPhotoImageUrl() {
        return this.smallPhotoImageUrl;
    }

    public void setSmallPhotoImageUrl(String smallPhotoImageUrl2) {
        this.smallPhotoImageUrl = smallPhotoImageUrl2;
    }

    public void setAssetSource(String asetSource) {
        this.assetSource = asetSource;
    }

    public String getAssetSource() {
        return this.assetSource;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category2) {
        this.category = category2;
    }

    public int getAssetYear() {
        return this.assetYear;
    }

    public void setAssetYear(String assetYear2) {
        try {
            this.assetYear = Integer.parseInt(assetYear2);
        } catch (Exception e) {
        }
    }

    public void setAssetYear(int assetYear2) {
        this.assetYear = assetYear2;
    }

    public String getAssetDirector() {
        return this.assetDirector;
    }

    public void setAssetDirector(String assetDirector2) {
        this.assetDirector = assetDirector2;
    }

    public String getAssetActor() {
        return this.assetActor;
    }

    public void setAssetActor(String assetActor2) {
        this.assetActor = assetActor2;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setAssetName(String assetName2) {
        this.assetName = assetName2;
    }

    public String getAssetName() {
        return this.assetName;
    }

    public void setAssetCode(String assetCode2) {
        this.assetCode = assetCode2;
    }

    public String getAssetCode() {
        return this.assetCode;
    }

    public void setAssetDescription(String assetDescription2) {
        this.assetDescription = assetDescription2;
    }

    public String getAssetDescription() {
        return this.assetDescription;
    }

    public void setAssetId(long assetId2) {
        this.assetId = assetId2;
    }

    public void setAssetId(String assetId2) {
        try {
            this.assetId = Long.parseLong(assetId2);
        } catch (Exception e) {
        }
    }

    public long getAssetId() {
        return this.assetId;
    }

    public void setAssetType(String assetType2) {
        this.assetType = assetType2;
    }

    public String getAssetType() {
        return this.assetType;
    }

    public Object clone() {
        try {
            return (VideoAsset) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setAssetSourceCode(String assetSourceCode2) {
        this.assetSourceCode = assetSourceCode2;
    }

    public String getAssetSourceCode() {
        return this.assetSourceCode;
    }

    public void setItemId(long itemId2) {
        this.itemId = itemId2;
    }

    public void setItemId(String itemId2) {
        try {
            this.itemId = Long.parseLong(itemId2);
        } catch (Exception e) {
        }
    }

    public long getItemId() {
        return this.itemId;
    }

    public void setOrigin(String origin2) {
        this.origin = origin2;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setAssetIdX(long assetIdX2) {
        this.assetIdX = assetIdX2;
    }

    public void setAssetIdX(String assetIdX2) {
        try {
            this.assetIdX = Long.parseLong(assetIdX2);
        } catch (Exception e) {
        }
    }

    public long getAssetIdX() {
        return this.assetIdX;
    }

    public void setShowTime(String showTime2) {
        this.showTime = showTime2;
    }

    public String getShowTime() {
        return this.showTime;
    }

    public void setAssetKey(String assetKey2) {
        this.assetKey = assetKey2;
    }

    public String getAssetKey() {
        return this.assetKey;
    }

    public void setBigImageUrl(String bigImageUrl2) {
        this.bigImageUrl = bigImageUrl2;
    }

    public String getBigImageUrl() {
        return this.bigImageUrl;
    }

    public String getDisplayName(Application app) {
        String result = getAssetName().trim();
        VideoAsset parent = VideoAssetMap.instance().getAsset(getAssetId(), getAssetType(), app);
        String parentAssetName = "暂缺片名";
        if (parent != null) {
            parentAssetName = parent.getAssetName();
        }
        if (getAssetType().equals("11") || getAssetType().equals("13")) {
            if (getAssetIdX() > 0) {
                return String.valueOf(parentAssetName) + "第" + getAssetIdX() + "集";
            }
            return getAssetName();
        } else if (getAssetType().equals("12")) {
            if (parent == null || getAssetIdX() <= 0) {
                return result;
            }
            if (getAssetName() == null || "".equals(getAssetName())) {
                return String.valueOf(parentAssetName) + getShowTime();
            }
            return getAssetName();
        } else if (!getAssetType().equals("15") || parent == null || getAssetIdX() <= 0) {
            return result;
        } else {
            return String.valueOf(getAssetIdX()) + ":" + result;
        }
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getType() {
        return this.type;
    }

    public boolean isParentAsset() {
        if (this.type.equals("0")) {
            return true;
        }
        return false;
    }

    public int getFinished() {
        return this.finished;
    }

    public void setFinished(String finished2) {
        try {
            this.finished = Integer.parseInt(finished2);
        } catch (Exception e) {
        }
    }

    public int getTotal() {
        return this.total;
    }

    public void setTotal(String total2) {
        try {
            this.total = Integer.parseInt(total2);
        } catch (Exception e) {
        }
    }

    public void setAssetPinyin(String assetPinyin2) {
        this.assetPinyin = assetPinyin2;
    }

    public String getAssetPinyin() {
        return this.assetPinyin;
    }

    public void setVote(double vote2) {
        this.vote = vote2;
    }

    public double getVote() {
        return this.vote;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public void setScore(String score2) {
        try {
            this.score = Integer.parseInt(score2);
        } catch (Exception e) {
        }
    }

    public int getScore() {
        return this.score;
    }

    public int getTranslate() {
        return this.score;
    }
}
