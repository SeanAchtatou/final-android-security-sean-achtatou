package com.kandian.common;

import android.content.Context;
import com.kandian.cartoonapp.R;
import com.kandian.user.UserService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class HtmlUtil {
    public static final String PLATFORM_ANDROID = "android";
    public static final String PLATFORM_IPAD = "ipad";
    public static final String PLATFORM_PC = "pc";
    public static final String TYPE_DOWN = "2";
    public static final String TYPE_PLAY = "1";

    public static void statistics(String assettype, long assetid, long itemid, String platform, String appcode, String type, Context context) {
        String httpUrl = StringUtil.replace(StringUtil.replace(StringUtil.replace(StringUtil.replace(StringUtil.replace(StringUtil.replace(StringUtil.replace(context.getString(R.string.statisticsUrl), "{assettype}", assettype), "{assetid}", new StringBuilder(String.valueOf(assetid)).toString()), "{itemid}", new StringBuilder(String.valueOf(itemid)).toString()), "{platform}", platform), "{appcode}", appcode), "{type}", type), "{username}", UserService.getInstance().getUsername());
        Log.v("send request to ", "[" + httpUrl + "]");
        final String interFaceUrl = httpUrl;
        new Thread(new Runnable() {
            public void run() {
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(new URL(interFaceUrl).openStream()));
                    StringBuffer sb = new StringBuffer("");
                    while (true) {
                        String s = br.readLine();
                        if (s == null) {
                            br.close();
                            return;
                        }
                        sb.append(String.valueOf(s) + "\r\n");
                    }
                } catch (Exception e) {
                }
            }
        }).start();
    }

    public static void statisticsVideo(long assetid, String appcode, int videotype, String type, Context context) {
        String httpUrl = StringUtil.replace(StringUtil.replace(StringUtil.replace(StringUtil.replace(context.getString(R.string.statisticsVideoUrl), "{videotype}", new StringBuilder(String.valueOf(videotype)).toString()), "{assetid}", new StringBuilder(String.valueOf(assetid)).toString()), "{appcode}", appcode), "{type}", type);
        Log.v("send request to ", "[" + httpUrl + "]");
        final String interFaceUrl = httpUrl;
        new Thread(new Runnable() {
            public void run() {
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(new URL(interFaceUrl).openStream()));
                    StringBuffer sb = new StringBuffer("");
                    while (true) {
                        String s = br.readLine();
                        if (s == null) {
                            br.close();
                            return;
                        }
                        sb.append(String.valueOf(s) + "\r\n");
                    }
                } catch (Exception e) {
                }
            }
        }).start();
    }
}
