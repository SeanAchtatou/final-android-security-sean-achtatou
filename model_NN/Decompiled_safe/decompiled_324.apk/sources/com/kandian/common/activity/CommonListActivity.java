package com.kandian.common.activity;

import android.app.ListActivity;
import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Toast;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.StatisticsUtil;
import com.kandian.common.UpdateUtil;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.ArrayList;
import java.util.Map;

public class CommonListActivity extends ListActivity {
    public static final int MODE_ASYNC = 2;
    public static final int MODE_SYNC = 1;
    /* access modifiers changed from: private */
    public int common_numfound;
    /* access modifiers changed from: private */
    public int common_start;
    protected boolean isNeedPage = true;
    protected int mode;
    protected int rowId;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        UpdateUtil.checkUpdate(this);
        StatisticsUtil.updateCount(this);
        MobclickAgentWrapper.onResume(this);
        Log.v("CommonListActivity", "onResume");
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
        Log.v("CommonListActivity", "onPause");
    }

    /* access modifiers changed from: protected */
    public void initList(Context context, ArrayList<Object> items, int viewResourceId, boolean isNeedPage2, int mode2) {
        this.rowId = viewResourceId;
        this.isNeedPage = isNeedPage2;
        this.mode = mode2;
        setListAdapter(new RowAdapter(context, viewResourceId, items));
    }

    protected class RowAdapter extends ArrayAdapter<Object> {
        public RowAdapter(Context context, int textViewResourceId, ArrayList<Object> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ((LayoutInflater) CommonListActivity.this.getSystemService("layout_inflater")).inflate(CommonListActivity.this.rowId, (ViewGroup) null);
            }
            Object rowObj = getItem(position);
            if (rowObj != null && (view = CommonListActivity.this.buildRow(rowObj, position, view, parent)) == null) {
                view = convertView;
            }
            if (CommonListActivity.this.isNeedPage && position == getCount() - 1) {
                CommonListActivity.this.getData();
            }
            return view;
        }
    }

    /* access modifiers changed from: protected */
    public View buildRow(Object rowObj, int position, View v, ViewGroup parent) {
        return v;
    }

    /* access modifiers changed from: protected */
    public void beforeGetData() {
    }

    /* access modifiers changed from: protected */
    public void afterGetData(BaseAdapter ba, CommonResultSet ars) {
    }

    /* access modifiers changed from: protected */
    public void errorGetData() {
    }

    /* access modifiers changed from: protected */
    public CommonResultSet buildData() {
        return null;
    }

    /* access modifiers changed from: protected */
    public Object getItemObject(int position) {
        return ((RowAdapter) getListAdapter()).getItem(position);
    }

    /* access modifiers changed from: protected */
    public int getRowAdapterCount() {
        return ((RowAdapter) getListAdapter()).getCount();
    }

    /* access modifiers changed from: protected */
    public void clearRowAdapter() {
        ((RowAdapter) getListAdapter()).clear();
    }

    /* access modifiers changed from: protected */
    public RowAdapter getRowAdapter() {
        return (RowAdapter) getListAdapter();
    }

    /* access modifiers changed from: protected */
    public void getSyncData() {
        if (!this.isNeedPage || this.common_start <= 0 || this.common_numfound <= 0 || this.common_start < this.common_numfound) {
            CommonResultSet ars = buildData();
            RowAdapter myAdatper = (RowAdapter) getListAdapter();
            if (ars == null || ars.getSize() <= 0) {
                afterGetData((BaseAdapter) getListAdapter(), ars);
            } else if (this.common_start == 0 || this.common_numfound == 0 || this.common_start <= this.common_numfound) {
                for (int i = 0; i < ars.getSize(); i++) {
                    myAdatper.add(ars.get(i));
                }
                ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
                ars.setStart(getRowAdapterCount());
                this.common_start = ars.getStart();
                this.common_numfound = ars.getNumFound();
                afterGetData((BaseAdapter) getListAdapter(), ars);
            } else {
                afterGetData((BaseAdapter) getListAdapter(), ars);
            }
        } else {
            CommonResultSet ars2 = new CommonResultSet();
            ars2.setStart(this.common_start);
            ars2.setNumFound(this.common_numfound);
            afterGetData((BaseAdapter) getListAdapter(), ars2);
        }
    }

    /* access modifiers changed from: protected */
    public void getData() {
        beforeGetData();
        if (this.mode == 1) {
            CommonResultSet ars = buildData();
            if (ars != null) {
                RowAdapter myAdatper = (RowAdapter) getListAdapter();
                for (int i = 0; i < ars.getSize(); i++) {
                    myAdatper.add(ars.get(i));
                }
                ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
                afterGetData((BaseAdapter) getListAdapter(), ars);
            }
        } else if (this.mode != 2) {
        } else {
            if (!this.isNeedPage || this.common_start <= 0 || this.common_numfound <= 0 || this.common_start < this.common_numfound) {
                Asynchronous asynchronous = new Asynchronous(this);
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context context, Map<String, Object> map) throws Exception {
                        if (CommonListActivity.this.isNeedPage && CommonListActivity.this.common_start != 0 && CommonListActivity.this.common_numfound != 0 && CommonListActivity.this.common_start > CommonListActivity.this.common_numfound) {
                            return 0;
                        }
                        setCallbackParameter("result", CommonListActivity.this.buildData());
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    /* Debug info: failed to restart local var, previous not found, register: 5 */
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        RowAdapter myAdatper = (RowAdapter) CommonListActivity.this.getListAdapter();
                        CommonResultSet ars = (CommonResultSet) outputparameter.get("result");
                        if (ars == null || ars.getSize() <= 0) {
                            CommonListActivity.this.afterGetData((BaseAdapter) CommonListActivity.this.getListAdapter(), ars);
                        } else if (CommonListActivity.this.common_start == 0 || CommonListActivity.this.common_numfound == 0 || CommonListActivity.this.common_start <= CommonListActivity.this.common_numfound) {
                            for (int i = 0; i < ars.getSize(); i++) {
                                myAdatper.add(ars.get(i));
                            }
                            ((BaseAdapter) CommonListActivity.this.getListAdapter()).notifyDataSetChanged();
                            ars.setStart(CommonListActivity.this.getRowAdapterCount());
                            CommonListActivity.this.common_start = ars.getStart();
                            CommonListActivity.this.common_numfound = ars.getNumFound();
                            CommonListActivity.this.afterGetData((BaseAdapter) CommonListActivity.this.getListAdapter(), ars);
                        } else {
                            CommonListActivity.this.afterGetData((BaseAdapter) CommonListActivity.this.getListAdapter(), ars);
                        }
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                        Toast.makeText(c, "数据获取失败", 0).show();
                        CommonListActivity.this.errorGetData();
                    }
                });
                asynchronous.start();
                return;
            }
            CommonResultSet ars2 = new CommonResultSet();
            ars2.setStart(this.common_start);
            ars2.setNumFound(this.common_numfound);
            afterGetData((BaseAdapter) getListAdapter(), ars2);
        }
    }
}
