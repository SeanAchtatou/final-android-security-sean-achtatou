package com.kandian.common.activity;

import android.app.ListActivity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.SeekBar;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.HtmlUtil;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.StatisticsUtil;
import com.kandian.common.UpdateUtil;
import com.kandian.common.VerticalSeekBar;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.ArrayList;
import java.util.Map;
import org.apache.commons.httpclient.auth.AuthScope;

public class CommonSeekBarListActivity extends ListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "CommonSeekBarListActivity";
    private final int MSG_NETWORK_PROBLEM = 2;
    private final int MSG_NEXTLIST = 1;
    private final int MSG_PRELIST = -1;
    private final int MSG_REFRESH_LIST = 0;
    /* access modifiers changed from: private */
    public int NextPage = 0;
    public int PrePage = 1;
    public int currPage = 0;
    public boolean flag = true;
    /* access modifiers changed from: private */
    public boolean isLock = false;
    /* access modifiers changed from: private */
    public boolean isRefresh = false;
    /* access modifiers changed from: private */
    public boolean isUp = false;
    /* access modifiers changed from: private */
    public RefreshHandler mRedrawHandler = new RefreshHandler();
    /* access modifiers changed from: private */
    public int minTotalPages = 2;
    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        private boolean isfirstItem = false;
        private boolean islastItem = false;

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            Log.v(CommonSeekBarListActivity.TAG, "isRefresh=" + CommonSeekBarListActivity.this.isRefresh + ",getListView().getFirstVisiblePosition()=" + CommonSeekBarListActivity.this.getListView().getFirstVisiblePosition());
            if (CommonSeekBarListActivity.this.isRefresh && CommonSeekBarListActivity.this.getListView().getFirstVisiblePosition() != 0) {
                CommonSeekBarListActivity.this.getListView().setSelection(0);
            }
            if (totalItemCount <= 0) {
                return;
            }
            if (firstVisibleItem == 0 && !CommonSeekBarListActivity.this.isRefresh && CommonSeekBarListActivity.this.PrePage > 1) {
                this.isfirstItem = true;
            } else if (firstVisibleItem + visibleItemCount != totalItemCount || CommonSeekBarListActivity.this.currPage >= CommonSeekBarListActivity.this.totalpage) {
                this.islastItem = false;
                this.isfirstItem = false;
            } else {
                this.islastItem = true;
            }
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState != 0 && scrollState == 1) {
                CommonSeekBarListActivity.this.isRefresh = false;
                if (CommonSeekBarListActivity.this.verticalSeekBar != null && CommonSeekBarListActivity.this.verticalSeekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > CommonSeekBarListActivity.this.minTotalPages) {
                    CommonSeekBarListActivity.this.verticalSeekBar.setVisibility(0);
                }
                if (CommonSeekBarListActivity.this.seekBar != null && CommonSeekBarListActivity.this.seekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > CommonSeekBarListActivity.this.minTotalPages) {
                    CommonSeekBarListActivity.this.seekBar.setVisibility(0);
                }
                if (CommonSeekBarListActivity.this.isUp && view.getFirstVisiblePosition() == 0 && CommonSeekBarListActivity.this.PrePage > 1) {
                    this.isfirstItem = true;
                }
                if ((!this.islastItem || CommonSeekBarListActivity.this.isLock) && this.isfirstItem && !CommonSeekBarListActivity.this.isLock) {
                    CommonSeekBarListActivity.this.getData(-1);
                }
            }
        }
    };
    VerticalSeekBar.OnSeekBarChangeListener onSeekBarChange = new VerticalSeekBar.OnSeekBarChangeListener() {
        public void onProgressChanged(VerticalSeekBar verticalSeekBar, int progress, boolean fromUser) {
            if (progress == CommonSeekBarListActivity.this.totalpage) {
                progress = CommonSeekBarListActivity.this.totalpage - 1;
            }
            verticalSeekBar.setProgress(progress);
            if (!CommonSeekBarListActivity.this.isLock) {
                CommonSeekBarListActivity.this.currPage = CommonSeekBarListActivity.this.totalpage - progress;
                Message.obtain(CommonSeekBarListActivity.this.updateHandlerPage).sendToTarget();
            }
            if (verticalSeekBar != null && verticalSeekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > CommonSeekBarListActivity.this.minTotalPages) {
                verticalSeekBar.setVisibility(0);
            }
        }

        public void onStartTrackingTouch(VerticalSeekBar verticalSeekBar) {
            CommonSeekBarListActivity.this.flag = false;
        }

        public void onStopTrackingTouch(VerticalSeekBar verticalSeekBar) {
            if (!CommonSeekBarListActivity.this.isLock) {
                CommonSeekBarListActivity.this.getData(0);
            }
            CommonSeekBarListActivity.this.mRedrawHandler.sleep(3000);
        }
    };
    SeekBar.OnSeekBarChangeListener onSeekBarChange1 = new SeekBar.OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (seekBar != null && seekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > CommonSeekBarListActivity.this.minTotalPages) {
                seekBar.setVisibility(0);
            }
            Log.v(CommonSeekBarListActivity.TAG, String.valueOf(progress) + "@@@@@@@@@" + seekBar.getMax());
            if (progress == 0) {
                progress = 1;
            }
            seekBar.setProgress(progress);
            if (!CommonSeekBarListActivity.this.isLock) {
                CommonSeekBarListActivity.this.currPage = progress;
                Message.obtain(CommonSeekBarListActivity.this.updateHandlerPage).sendToTarget();
            }
            if (seekBar != null && seekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > CommonSeekBarListActivity.this.minTotalPages) {
                seekBar.setVisibility(0);
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            CommonSeekBarListActivity.this.flag = false;
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            if (!CommonSeekBarListActivity.this.isLock) {
                CommonSeekBarListActivity.this.getData(0);
            }
            CommonSeekBarListActivity.this.mRedrawHandler.visibility(3000);
        }
    };
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            boolean z;
            switch (event.getAction()) {
                case 0:
                    CommonSeekBarListActivity.this.py = event.getY();
                    break;
                case 1:
                    CommonSeekBarListActivity commonSeekBarListActivity = CommonSeekBarListActivity.this;
                    if (event.getY() - CommonSeekBarListActivity.this.py > 0.0f) {
                        z = true;
                    } else {
                        z = false;
                    }
                    commonSeekBarListActivity.isUp = z;
                    CommonSeekBarListActivity.this.mRedrawHandler.visibility(3000);
                    CommonSeekBarListActivity.this.mRedrawHandler.enabled(800);
                    break;
                case 2:
                    CommonSeekBarListActivity.this.isUp = event.getY() - CommonSeekBarListActivity.this.py > 0.0f;
                    break;
            }
            return false;
        }
    };
    /* access modifiers changed from: private */
    public int pageSize = 20;
    /* access modifiers changed from: private */
    public float py = 0.0f;
    protected int rowId;
    protected SeekBar seekBar = null;
    public int totalResultCount = 0;
    public int totalpage = 0;
    Handler updateHandlerPage = new Handler() {
        public void handleMessage(Message msg) {
            CommonSeekBarListActivity.this.onUpdatePage(CommonSeekBarListActivity.this.currPage);
        }
    };
    protected VerticalSeekBar verticalSeekBar = null;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        UpdateUtil.checkUpdate(this);
        StatisticsUtil.updateCount(this);
        MobclickAgentWrapper.onResume(this);
        Log.v("CommonListActivity", "onResume");
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
        Log.v("CommonListActivity", "onPause");
    }

    /* access modifiers changed from: protected */
    public void initList(Context context, ArrayList<Object> items, int viewResourceId, int verticalSeekBarId, int seekBarId) {
        Log.v("CommonListActivity", "*********************************************************initList");
        this.rowId = viewResourceId;
        setListAdapter(new RowAdapter(context, viewResourceId, items));
        if (verticalSeekBarId != -1) {
            this.verticalSeekBar = (VerticalSeekBar) findViewById(verticalSeekBarId);
        }
        if (seekBarId != -1) {
            this.seekBar = (SeekBar) findViewById(seekBarId);
        }
        if (this.seekBar != null) {
            this.seekBar.setOnSeekBarChangeListener(this.onSeekBarChange1);
        }
        if (this.verticalSeekBar != null) {
            this.verticalSeekBar.setOnSeekBarChangeListener(this.onSeekBarChange);
        }
        getListView().setOnScrollListener(this.onScrollListener);
        getListView().setOnTouchListener(this.onTouchListener);
        this.pageSize = Integer.parseInt(getString(R.string.pageSize));
    }

    protected class RowAdapter extends ArrayAdapter<Object> {
        public RowAdapter(Context context, int textViewResourceId, ArrayList<Object> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ((LayoutInflater) CommonSeekBarListActivity.this.getSystemService("layout_inflater")).inflate(CommonSeekBarListActivity.this.rowId, (ViewGroup) null);
            }
            Object rowObj = getItem(position);
            if (rowObj == null) {
                return view;
            }
            View view2 = CommonSeekBarListActivity.this.buildRow(rowObj, position, view, parent);
            if (view2 == null) {
                return convertView;
            }
            return view2;
        }
    }

    /* access modifiers changed from: protected */
    public View buildRow(Object rowObj, int position, View v, ViewGroup parent) {
        return v;
    }

    /* access modifiers changed from: protected */
    public View buildRow(Object rowObj, int position, View v, ViewGroup parent, boolean flag2) {
        return v;
    }

    /* access modifiers changed from: protected */
    public void beforeGetData() {
    }

    /* access modifiers changed from: protected */
    public void afterGetData(BaseAdapter ba, CommonResultSet ars, int currPage2, int start, int end) {
    }

    /* access modifiers changed from: protected */
    public void errorGetData() {
    }

    /* access modifiers changed from: protected */
    public CommonResultSet buildData(int start, int page_szie) {
        return null;
    }

    /* access modifiers changed from: protected */
    public Object getItemObject(int position) {
        return ((RowAdapter) getListAdapter()).getItem(position);
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        this.currPage = 1;
        getData(0);
    }

    /* access modifiers changed from: protected */
    public int getRowAdapterCount() {
        return ((RowAdapter) getListAdapter()).getCount();
    }

    /* access modifiers changed from: protected */
    public void clearRowAdapter() {
        ((RowAdapter) getListAdapter()).clear();
    }

    /* access modifiers changed from: protected */
    public RowAdapter getRowAdapter() {
        return (RowAdapter) getListAdapter();
    }

    /* access modifiers changed from: protected */
    public void getData(final int item) {
        try {
            beforeGetData();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Asynchronous asynchronous = new Asynchronous(this);
        asynchronous.asyncProcess(new AsyncProcess() {
            public int process(Context context, Map<String, Object> map) throws Exception {
                CommonSeekBarListActivity.this.isLock = true;
                CommonSeekBarListActivity.this.isRefresh = false;
                if (item == -1) {
                    Log.v(CommonSeekBarListActivity.TAG, "-1 currPage=" + CommonSeekBarListActivity.this.currPage + ",Prepage=" + CommonSeekBarListActivity.this.PrePage + ",NextPage=" + CommonSeekBarListActivity.this.NextPage);
                    CommonSeekBarListActivity.this.PrePage--;
                    CommonSeekBarListActivity.this.currPage = CommonSeekBarListActivity.this.PrePage;
                } else if (item == 1) {
                    Log.v(CommonSeekBarListActivity.TAG, "1 currPage=" + CommonSeekBarListActivity.this.currPage + ",Prepage=" + CommonSeekBarListActivity.this.PrePage + ",NextPage=" + CommonSeekBarListActivity.this.NextPage);
                    CommonSeekBarListActivity commonSeekBarListActivity = CommonSeekBarListActivity.this;
                    commonSeekBarListActivity.NextPage = commonSeekBarListActivity.NextPage + 1;
                    CommonSeekBarListActivity.this.currPage = CommonSeekBarListActivity.this.NextPage;
                } else if (item == 0) {
                    Log.v(CommonSeekBarListActivity.TAG, "0 currPage=" + CommonSeekBarListActivity.this.currPage + ",Prepage=" + CommonSeekBarListActivity.this.PrePage + ",NextPage=" + CommonSeekBarListActivity.this.NextPage);
                    if (CommonSeekBarListActivity.this.currPage >= CommonSeekBarListActivity.this.PrePage && CommonSeekBarListActivity.this.currPage <= CommonSeekBarListActivity.this.NextPage) {
                        setCallbackParameter("seekbar_status", new StringBuilder(String.valueOf(item)).toString());
                        return 0;
                    }
                }
                if (CommonSeekBarListActivity.this.pageSize == 0) {
                    CommonSeekBarListActivity.this.pageSize = Integer.parseInt(CommonSeekBarListActivity.this.getString(R.string.pageSize));
                }
                CommonResultSet ars = null;
                try {
                    ars = CommonSeekBarListActivity.this.buildData((CommonSeekBarListActivity.this.currPage - 1) * CommonSeekBarListActivity.this.pageSize, CommonSeekBarListActivity.this.pageSize);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (ars == null || ars.getSize() == 0) {
                    if (item == -1) {
                        CommonSeekBarListActivity.this.PrePage++;
                        CommonSeekBarListActivity.this.currPage = CommonSeekBarListActivity.this.PrePage;
                    } else if (item == 1) {
                        CommonSeekBarListActivity commonSeekBarListActivity2 = CommonSeekBarListActivity.this;
                        commonSeekBarListActivity2.NextPage = commonSeekBarListActivity2.NextPage - 1;
                        CommonSeekBarListActivity.this.currPage = CommonSeekBarListActivity.this.NextPage;
                    }
                    setCallbackParameter("seekbar_status", HtmlUtil.TYPE_DOWN);
                    return 0;
                }
                setCallbackParameter("result", ars);
                setCallbackParameter("seekbar_status", new StringBuilder(String.valueOf(item)).toString());
                return 0;
            }
        });
        asynchronous.asyncCallback(new AsyncCallback() {
            public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                int end;
                int i;
                RowAdapter myAdatper = (RowAdapter) CommonSeekBarListActivity.this.getListAdapter();
                CommonResultSet commonResultSet = (CommonResultSet) outputparameter.get("result");
                switch (Integer.parseInt((String) outputparameter.get("seekbar_status"))) {
                    case AuthScope.ANY_PORT /*-1*/:
                        if (commonResultSet != null && commonResultSet.getSize() > 0) {
                            CommonSeekBarListActivity.this.totalResultCount = commonResultSet.getNumFound();
                            for (int i2 = commonResultSet.getSize() - 1; i2 >= 0; i2--) {
                                myAdatper.insert(commonResultSet.get(i2), 0);
                            }
                            ((BaseAdapter) CommonSeekBarListActivity.this.getListAdapter()).notifyDataSetChanged();
                            CommonSeekBarListActivity.this.getListView().setSelection(CommonSeekBarListActivity.this.pageSize - 1);
                            commonResultSet.setStart(CommonSeekBarListActivity.this.getRowAdapterCount());
                            break;
                        }
                    case 0:
                        Log.v("#################", "commonResultSet = " + commonResultSet);
                        if (CommonSeekBarListActivity.this.currPage >= CommonSeekBarListActivity.this.PrePage && CommonSeekBarListActivity.this.currPage <= CommonSeekBarListActivity.this.NextPage) {
                            CommonSeekBarListActivity.this.getListView().setSelection((CommonSeekBarListActivity.this.currPage - CommonSeekBarListActivity.this.PrePage) * CommonSeekBarListActivity.this.pageSize);
                            CommonSeekBarListActivity.this.flag = true;
                            break;
                        } else {
                            CommonSeekBarListActivity.this.isRefresh = true;
                            CommonSeekBarListActivity.this.PrePage = CommonSeekBarListActivity.this.currPage;
                            CommonSeekBarListActivity.this.NextPage = CommonSeekBarListActivity.this.currPage;
                            CommonSeekBarListActivity.this.totalResultCount = 0;
                            myAdatper.clear();
                            if (commonResultSet != null && commonResultSet.getSize() > 0) {
                                CommonSeekBarListActivity.this.totalResultCount = commonResultSet.getNumFound();
                                for (int i3 = 0; i3 < commonResultSet.getSize(); i3++) {
                                    myAdatper.add(commonResultSet.get(i3));
                                }
                            }
                            ((BaseAdapter) CommonSeekBarListActivity.this.getListAdapter()).notifyDataSetChanged();
                            CommonSeekBarListActivity.this.getListView().requestFocus();
                            break;
                        }
                        break;
                    case 1:
                        if (commonResultSet != null && commonResultSet.getSize() > 0) {
                            CommonSeekBarListActivity.this.totalResultCount = commonResultSet.getNumFound();
                            for (int i4 = 0; i4 < commonResultSet.getSize(); i4++) {
                                myAdatper.add(commonResultSet.get(i4));
                            }
                            ((BaseAdapter) CommonSeekBarListActivity.this.getListAdapter()).notifyDataSetChanged();
                            commonResultSet.setStart(CommonSeekBarListActivity.this.getRowAdapterCount());
                            break;
                        }
                }
                CommonSeekBarListActivity.this.PrePage = CommonSeekBarListActivity.this.PrePage > CommonSeekBarListActivity.this.currPage ? CommonSeekBarListActivity.this.currPage : CommonSeekBarListActivity.this.PrePage;
                int start = ((CommonSeekBarListActivity.this.PrePage - 1) * CommonSeekBarListActivity.this.pageSize) + 1;
                if (CommonSeekBarListActivity.this.NextPage * CommonSeekBarListActivity.this.pageSize > CommonSeekBarListActivity.this.totalResultCount) {
                    end = CommonSeekBarListActivity.this.totalResultCount;
                } else {
                    end = CommonSeekBarListActivity.this.NextPage * CommonSeekBarListActivity.this.pageSize;
                }
                CommonSeekBarListActivity.this.totalpage = ((CommonSeekBarListActivity.this.totalResultCount + CommonSeekBarListActivity.this.pageSize) - 1) / CommonSeekBarListActivity.this.pageSize;
                if (CommonSeekBarListActivity.this.verticalSeekBar != null) {
                    CommonSeekBarListActivity.this.verticalSeekBar.setMax(CommonSeekBarListActivity.this.totalpage);
                    CommonSeekBarListActivity.this.verticalSeekBar.setProgress(CommonSeekBarListActivity.this.totalpage - CommonSeekBarListActivity.this.currPage);
                }
                if (CommonSeekBarListActivity.this.seekBar != null) {
                    CommonSeekBarListActivity.this.seekBar.setMax(CommonSeekBarListActivity.this.totalpage);
                    SeekBar seekBar = CommonSeekBarListActivity.this.seekBar;
                    if (CommonSeekBarListActivity.this.currPage == 0) {
                        i = 1;
                    } else {
                        i = CommonSeekBarListActivity.this.currPage;
                    }
                    seekBar.setProgress(i);
                }
                CommonSeekBarListActivity.this.isLock = false;
                Message.obtain(CommonSeekBarListActivity.this.updateHandlerPage).sendToTarget();
                if (CommonSeekBarListActivity.this.verticalSeekBar != null && CommonSeekBarListActivity.this.verticalSeekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > 1) {
                    CommonSeekBarListActivity.this.verticalSeekBar.setVisibility(0);
                    CommonSeekBarListActivity.this.mRedrawHandler.sleep(1000);
                }
                if (CommonSeekBarListActivity.this.seekBar != null && CommonSeekBarListActivity.this.seekBar.getVisibility() == 8 && CommonSeekBarListActivity.this.totalpage > 1) {
                    CommonSeekBarListActivity.this.seekBar.setVisibility(0);
                    CommonSeekBarListActivity.this.mRedrawHandler.sleep(1000);
                }
                try {
                    CommonSeekBarListActivity.this.minTotalPages = Integer.parseInt(CommonSeekBarListActivity.this.getString(R.string.min_total_seek_pages));
                } catch (Exception e) {
                    Log.v(CommonSeekBarListActivity.TAG, "min_total_seek_pageis wrong");
                }
                if (CommonSeekBarListActivity.this.verticalSeekBar != null && CommonSeekBarListActivity.this.verticalSeekBar.getVisibility() == 0 && CommonSeekBarListActivity.this.totalpage <= CommonSeekBarListActivity.this.minTotalPages) {
                    CommonSeekBarListActivity.this.verticalSeekBar.setVisibility(8);
                }
                if (CommonSeekBarListActivity.this.seekBar != null && CommonSeekBarListActivity.this.seekBar.getVisibility() == 0 && CommonSeekBarListActivity.this.totalpage <= CommonSeekBarListActivity.this.minTotalPages) {
                    CommonSeekBarListActivity.this.seekBar.setVisibility(8);
                }
                try {
                    CommonSeekBarListActivity.this.afterGetData((BaseAdapter) CommonSeekBarListActivity.this.getListAdapter(), commonResultSet, CommonSeekBarListActivity.this.currPage, start, end);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        });
        asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
            public void handle(Context c, Exception e, Message m) {
                Toast.makeText(c, "数据获取失败", 0).show();
                try {
                    CommonSeekBarListActivity.this.errorGetData();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        });
        asynchronous.start();
    }

    /* access modifiers changed from: protected */
    public void onUpdatePage(int currPage2) {
    }

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (CommonSeekBarListActivity.this.verticalSeekBar != null && CommonSeekBarListActivity.this.verticalSeekBar.getVisibility() == 0) {
                        CommonSeekBarListActivity.this.verticalSeekBar.layout(CommonSeekBarListActivity.this.verticalSeekBar.getLeft() + 60, CommonSeekBarListActivity.this.verticalSeekBar.getTop(), CommonSeekBarListActivity.this.verticalSeekBar.getLeft() + Math.abs(CommonSeekBarListActivity.this.verticalSeekBar.getWidth()), CommonSeekBarListActivity.this.verticalSeekBar.getTop() + Math.abs(CommonSeekBarListActivity.this.verticalSeekBar.getHeight()));
                    }
                    visibility(800);
                    return;
                case 1:
                    if (CommonSeekBarListActivity.this.verticalSeekBar != null && !CommonSeekBarListActivity.this.verticalSeekBar.isEnabled()) {
                        CommonSeekBarListActivity.this.verticalSeekBar.setEnabled(true);
                        return;
                    }
                    return;
                case 2:
                    if (CommonSeekBarListActivity.this.verticalSeekBar != null && CommonSeekBarListActivity.this.verticalSeekBar.getVisibility() == 0) {
                        CommonSeekBarListActivity.this.verticalSeekBar.setVisibility(8);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }

        public void enabled(long delayMillis) {
            removeMessages(1);
            sendMessageDelayed(obtainMessage(1), delayMillis);
        }

        public void visibility(long delayMillis) {
            removeMessages(2);
            sendMessageDelayed(obtainMessage(2), delayMillis);
        }
    }
}
