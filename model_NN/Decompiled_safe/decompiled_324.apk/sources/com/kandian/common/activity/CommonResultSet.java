package com.kandian.common.activity;

import java.util.ArrayList;
import java.util.List;

public class CommonResultSet {
    private int numFound;
    private ArrayList results;
    private int start;

    public int getNumFound() {
        return this.numFound;
    }

    public void setNumFound(int numFound2) {
        this.numFound = numFound2;
    }

    public CommonResultSet() {
        this.results = null;
        this.results = new ArrayList();
    }

    public ArrayList getResults() {
        return this.results;
    }

    public void setResults(List list) {
        if (this.results != null && list != null) {
            this.results.addAll(list);
        }
    }

    public int getStart() {
        return this.start;
    }

    public void setStart(int start2) {
        this.start = start2;
    }

    public void add(Object sv) {
        if (this.results != null) {
            this.results.add(sv);
        }
    }

    public Object get(int i) {
        return this.results.get(i);
    }

    public int getSize() {
        if (this.results != null) {
            return this.results.size();
        }
        return 0;
    }
}
