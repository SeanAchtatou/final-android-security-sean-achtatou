package com.kandian.common;

public class KSException extends Exception {
    private static final long serialVersionUID = 4763369855474598376L;

    public KSException(String message) {
        super(message);
    }

    public KSException(String message, Throwable cause) {
        super(message, cause);
    }

    public KSException(Throwable cause) {
        super(cause);
    }
}
