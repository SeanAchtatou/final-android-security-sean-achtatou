package com.kandian.common.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.TextView;

public class BorderTextView extends TextView {
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(-65536);
        canvas.drawLine(0.0f, 0.0f, (float) (getWidth() - 1), 0.0f, paint);
        canvas.drawArc(new RectF(0.0f, 0.0f, 4.0f, 4.0f), 180.0f, 90.0f, false, paint);
        canvas.drawLine(0.0f, 0.0f, 0.0f, (float) (getHeight() - 1), paint);
        canvas.drawLine((float) (getWidth() - 1), 0.0f, (float) (getWidth() - 1), (float) (getHeight() - 1), paint);
        canvas.drawLine(0.0f, (float) (getHeight() - 1), (float) (getWidth() - 1), (float) (getHeight() - 1), paint);
        super.onDraw(canvas);
    }

    public BorderTextView(Context context) {
        super(context);
    }

    public BorderTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BorderTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
