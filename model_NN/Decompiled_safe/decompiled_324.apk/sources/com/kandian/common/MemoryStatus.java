package com.kandian.common;

import android.os.Environment;
import android.os.StatFs;

public class MemoryStatus {
    static final int ERROR = -1;
    private static final String TAG = "MemoryStatus";

    public static String getMntPath(String obj) {
        if (obj == null || obj.trim().length() == 0) {
            return Environment.getExternalStorageDirectory().toString();
        }
        if (obj.endsWith(PreferenceSetting.KS_MEDIAFILE_SUBDIR)) {
            obj = obj.substring(0, obj.lastIndexOf(PreferenceSetting.KS_MEDIAFILE_SUBDIR));
        }
        return obj;
    }

    public static boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String getAvailableInternalMemorySizeText() {
        return formatSize(getAvailableInternalMemorySize());
    }

    public static long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public static String getTotalInternalMemorySizeText() {
        return formatSize(getTotalInternalMemorySize());
    }

    public static long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    public static String getAvailableExternalMemorySizeText() {
        return formatSize(getAvailableExternalMemorySize());
    }

    public static long getAvailableExternalMemorySize() {
        if (!externalMemoryAvailable()) {
            return -1;
        }
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public static String getAvailableExternalMemorySizeText(String mntPath) {
        return formatSize(getAvailableExternalMemorySize(mntPath));
    }

    public static long getAvailableExternalMemorySize(String mntPath) {
        try {
            StatFs stat = new StatFs(getMntPath(mntPath));
            return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
        } catch (Exception e) {
            return -1;
        }
    }

    public static String getAlreadyUsedExternalMemorySizeText() {
        return formatSize(getAlreadyUsedExternalMemorySize());
    }

    public static long getAlreadyUsedExternalMemorySize() {
        return getTotalExternalMemorySize() - getAvailableExternalMemorySize();
    }

    public static String getAlreadyUsedExternalMemorySizeText(String mtnPath) {
        return formatSize(getAlreadyUsedExternalMemorySize(mtnPath));
    }

    public static long getAlreadyUsedExternalMemorySize(String mtnPath) {
        return getTotalExternalMemorySize(mtnPath) - getAvailableExternalMemorySize(mtnPath);
    }

    public static String getTotalExternalMemorySizeText() {
        return formatSize(getTotalExternalMemorySize());
    }

    public static long getTotalExternalMemorySize() {
        if (!externalMemoryAvailable()) {
            return -1;
        }
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    public static String getTotalExternalMemorySizeText(String mntPath) {
        Log.v(TAG, "mntPath = " + mntPath);
        return formatSize(getTotalExternalMemorySize(mntPath));
    }

    public static long getTotalExternalMemorySize(String mntPath) {
        try {
            StatFs stat = new StatFs(getMntPath(mntPath));
            return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
        } catch (Exception e) {
            return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    public static String formatSize(long size) {
        String result;
        String suffix = "B";
        boolean decimal = false;
        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
                if (size >= 1024) {
                    suffix = "GB";
                    decimal = true;
                }
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));
        if (decimal) {
            int commaOffset = resultBuffer.length() - 3;
            resultBuffer.insert(commaOffset, '.');
            result = resultBuffer.substring(0, commaOffset + 2);
        } else {
            result = resultBuffer.toString();
        }
        return String.valueOf(result) + suffix;
    }
}
