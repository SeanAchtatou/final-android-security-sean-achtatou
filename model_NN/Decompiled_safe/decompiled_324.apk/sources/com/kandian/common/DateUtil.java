package com.kandian.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    public static final DateFormat FULL_DATE_FORMAT = new SimpleDateFormat("EEE, MMM d, yyyyy hh:mm:ss aa z");
    public static final DateFormat ISO8601_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS z");
    private static SimpleDateFormat formatter = null;
    private static final SimpleDateFormat mFormat8chars = new SimpleDateFormat("yyyyMMdd");
    private static final SimpleDateFormat mFormatDayInWeek = new SimpleDateFormat("EE");
    private static final SimpleDateFormat mFormatWeeks = new SimpleDateFormat("ww");
    private static final SimpleDateFormat mFormatYear = new SimpleDateFormat("yyyy");
    public static final long millisInDay = 86400000;

    public static int getWeek(Date date) {
        int week = Integer.parseInt(mFormatWeeks.format(date));
        String dayInWeek = mFormatDayInWeek.format(date);
        if (dayInWeek.toUpperCase().equals("������") || dayInWeek.toUpperCase().equals("SUNDAY")) {
            return week - 1;
        }
        return week;
    }

    public static int getYear(Date date) {
        return Integer.parseInt(mFormatYear.format(date));
    }

    public static String shortDate(Date aDate) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(aDate);
    }

    public static Date getEndOfDay(Date day) {
        return getEndOfDay(day, Calendar.getInstance());
    }

    public static Date getEndOfDay(Date day, Calendar cal) {
        if (day == null) {
            day = new Date();
        }
        cal.setTime(day);
        cal.set(11, cal.getMaximum(11));
        cal.set(12, cal.getMaximum(12));
        cal.set(13, cal.getMaximum(13));
        cal.set(14, cal.getMaximum(14));
        return cal.getTime();
    }

    public static Date getNoonOfDay(Date day, Calendar cal) {
        if (day == null) {
            day = new Date();
        }
        cal.setTime(day);
        cal.set(11, 12);
        cal.set(12, cal.getMinimum(12));
        cal.set(13, cal.getMinimum(13));
        cal.set(14, cal.getMinimum(14));
        return cal.getTime();
    }

    public static SimpleDateFormat get8charDateFormat() {
        return mFormat8chars;
    }

    public static String mailDate(Date aDate) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        return formatter.format(aDate);
    }

    public static String longDate(Date aDate) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(aDate);
    }

    public static String shortDateGB(Date aDate) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat("yyyy'��'MM'��'dd'��'");
        return formatter.format(aDate);
    }

    public static Date getStartOfDay(Date day) {
        return getStartOfDay(day, Calendar.getInstance());
    }

    public static Date getStartOfDay(Date day, Calendar cal) {
        if (day == null) {
            day = new Date();
        }
        cal.setTime(day);
        cal.set(11, cal.getMinimum(11));
        cal.set(12, cal.getMinimum(12));
        cal.set(13, cal.getMinimum(13));
        cal.set(14, cal.getMinimum(14));
        return cal.getTime();
    }

    public static String longDateGB(Date aDate) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(aDate);
    }

    public static String longDateLog(Date aDate) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatter.format(aDate);
    }

    public static String formatDate(Date aDate, String formatStr) {
        if (aDate == null) {
            return "";
        }
        formatter = new SimpleDateFormat(formatStr);
        return formatter.format(aDate);
    }

    public static String LDAPDate(Date aDate) {
        if (aDate == null) {
            return "";
        }
        return formatDate(aDate, "yyyyMMddHHmm'Z'");
    }

    public static Date getDate(String yyyymmdd) {
        if (yyyymmdd == null) {
            return null;
        }
        int year = Integer.parseInt(yyyymmdd.substring(0, yyyymmdd.indexOf("-")));
        int month = Integer.parseInt(yyyymmdd.substring(yyyymmdd.indexOf("-") + 1, yyyymmdd.lastIndexOf("-")));
        int day = Integer.parseInt(yyyymmdd.substring(yyyymmdd.lastIndexOf("-") + 1));
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);
        return cal.getTime();
    }

    public static Date getDateByMillisecond(String millisecond) {
        if (millisecond == null || millisecond.trim().length() == 0 || millisecond.equals("undefined")) {
            return null;
        }
        try {
            return new Date(Long.parseLong(millisecond));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date parser(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(strDate);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date parser24(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strDate);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date getShortDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean equals(Date date1, Date date2) {
        if (date1 == null && date2 == null) {
            return true;
        }
        if (date1 == null && date2 != null) {
            return false;
        }
        if (date1 == null || date2 != null) {
            return date1.equals(date2);
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Calendar.roll(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.Calendar.roll(int, int):void}
      ClspMth{java.util.Calendar.roll(int, boolean):void} */
    public static Date tomorrow() {
        Calendar calender = Calendar.getInstance();
        calender.roll(6, true);
        return calender.getTime();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Calendar.roll(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.Calendar.roll(int, int):void}
      ClspMth{java.util.Calendar.roll(int, boolean):void} */
    public static Date nextDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.roll(6, 1);
        if (isEndOfYear(date, cal.getTime())) {
            cal.roll(1, true);
            cal.roll(6, 1);
        }
        return cal.getTime();
    }

    private static boolean isEndOfYear(Date curDate, Date rollUpDate) {
        return curDate.compareTo(rollUpDate) >= 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Calendar.roll(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.Calendar.roll(int, int):void}
      ClspMth{java.util.Calendar.roll(int, boolean):void} */
    public static Date yesterday() {
        Calendar calender = Calendar.getInstance();
        calender.roll(6, false);
        return calender.getTime();
    }

    private static final String getDateFormat(Date aDate) {
        if (aDate == null) {
            return null;
        }
        return new SimpleDateFormat("M/d/yyyy").format(aDate);
    }

    public static String NVL(Date date) {
        if (date == null) {
            return "";
        }
        return getDateFormat(date);
    }

    public static Date addDate(Date baseDate, int type, int num) {
        try {
            Calendar cale = Calendar.getInstance();
            cale.setTime(baseDate);
            if (type == 1) {
                cale.add(1, num);
            } else if (type == 2) {
                cale.add(2, num);
            } else if (type == 3) {
                cale.add(5, num);
            }
            return cale.getTime();
        } catch (Exception e) {
            Exception exc = e;
            return null;
        }
    }

    public static Date getDate(String strDate, String formatter2) {
        try {
            return new SimpleDateFormat(formatter2).parse(strDate);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date getSysDate() {
        return new Date(System.currentTimeMillis());
    }

    public static Date getTheFirstDayOfCurMonth(Date date) {
        Calendar calender = Calendar.getInstance();
        calender.setTime(date);
        calender.set(5, 1);
        return calender.getTime();
    }

    public static Date getTheFirstDayOfCurMonth(String date) {
        return getTheFirstDayOfCurMonth(getShortDate(date));
    }

    public static String getTheFirstDayOfCurMonthStr(Date date) {
        return shortDate(getTheFirstDayOfCurMonth(date));
    }

    public static String getTheFirstDayOfCurMonthStr(String date) {
        return shortDate(getTheFirstDayOfCurMonth(date));
    }

    public static Date getTheEndDayOfCurMonth(Date date) {
        Date firstDay = getTheFirstDayOfCurMonth(date);
        Calendar calender = Calendar.getInstance();
        calender.setTime(firstDay);
        calender.roll(2, 1);
        calender.roll(6, -1);
        return calender.getTime();
    }

    public static Date getTheEndDayOfCurMonth(String date) {
        return getTheEndDayOfCurMonth(getShortDate(date));
    }

    public static String getTheEndDayOfCurMonthStr(Date date) {
        return shortDate(getTheEndDayOfCurMonth(date));
    }

    public static String getTheEndDayOfCurMonthStr(String date) {
        return shortDate(getTheEndDayOfCurMonth(date));
    }

    public static int getDateSpan(Date beginDate, Date endDate, int calType) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTime(beginDate);
        int[] p1 = {cal.get(1), cal.get(2) + 1, cal.get(5)};
        cal.clear();
        cal.setTime(endDate);
        int[] p2 = {cal.get(1), cal.get(2) + 1, cal.get(5)};
        int[] s = {p2[0] - p1[0], (((p2[0] * 12) + p2[1]) - (p1[0] * 12)) - p1[1], (int) ((endDate.getTime() - beginDate.getTime()) / millisInDay)};
        if (calType <= 3 || calType >= 1) {
            return s[calType - 1];
        }
        return 0;
    }

    public static String getTimestamp() {
        return String.valueOf(System.currentTimeMillis());
    }
}
