package com.kandian.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.ksfamily.KSApp;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.auth.AuthScope;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateUtil {
    private static String ACTIVEINFO_KEY = "ACTIVEINFO";
    private static final int MSG_NETWORK_PROBLEM = 2;
    private static final int MSG_NO_UPDATE = 1;
    private static final int MSG_UPDATE = 0;
    public static String TAG = "UpdateUtil";
    /* access modifiers changed from: private */
    public static Context _context = null;
    /* access modifiers changed from: private */
    public static View downLoadView;
    /* access modifiers changed from: private */
    public static Dialog downloadDialog;
    /* access modifiers changed from: private */
    public static long downloadThreadID = 0;
    /* access modifiers changed from: private */
    public static Map<Long, Boolean> downloadThreadMap = new HashMap();
    /* access modifiers changed from: private */
    public static int fileSize;
    private static Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ProgressBar download_PB = (ProgressBar) UpdateUtil.downLoadView.findViewById(R.id.download_pb);
            TextView dialogcontentTV = (TextView) UpdateUtil.downLoadView.findViewById(R.id.dialogcontent);
            if (!Thread.currentThread().isInterrupted()) {
                switch (msg.what) {
                    case AuthScope.ANY_PORT /*-1*/:
                        dialogcontentTV.setText("下载更新失败，请检查网络");
                        break;
                    case 0:
                        download_PB.setMax(UpdateUtil.fileSize);
                    case 1:
                        int downLoadFileSize = ((Integer) msg.obj).intValue();
                        download_PB.setProgress(downLoadFileSize);
                        dialogcontentTV.setText("下载进度：" + ((downLoadFileSize * 100) / UpdateUtil.fileSize) + "%");
                        break;
                    case 2:
                        dialogcontentTV.setText("更新下载完成");
                        if (UpdateUtil.downloadDialog != null) {
                            UpdateUtil.downloadDialog.dismiss();
                        }
                        UpdateUtil.installAPK((String) msg.obj);
                        break;
                }
            }
            super.handleMessage(msg);
        }
    };
    private static boolean isChecked = false;
    static final Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    final KSApp laestVersion = (KSApp) msg.obj;
                    View updaterView = LayoutInflater.from(UpdateUtil._context).inflate((int) R.layout.updater_view, (ViewGroup) null);
                    ((TextView) updaterView.findViewById(R.id.updatecontent)).setText(String.valueOf(laestVersion.getAppname()) + " " + laestVersion.getVersionname() + "\n" + laestVersion.getUpdateinfo());
                    new AlertDialog.Builder(UpdateUtil._context).setIcon((int) R.drawable.ksicon).setTitle((int) R.string.updater_dialog_title).setView(updaterView).setPositiveButton((int) R.string.updater_dialog_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            UpdateUtil.downloadAPK(laestVersion);
                        }
                    }).setNegativeButton((int) R.string.updater_dialog_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create().show();
                    break;
                case 2:
                    Toast.makeText(UpdateUtil._context, UpdateUtil._context.getString(R.string.network_problem), 0).show();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    public static boolean checkUpdate(final Context context) {
        if (PreferenceSetting.getDownloadDir() == null) {
            PreferenceSetting.setDownloadDir(context);
        }
        _context = context;
        if (!(context instanceof Activity)) {
            Log.v(TAG, "context is not available");
        } else if (isChecked) {
            return false;
        } else {
            isChecked = true;
            new Thread(new Runnable() {
                /* JADX WARNING: Code restructure failed: missing block: B:26:0x010b, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:27:0x010c, code lost:
                    r2 = r11;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:28:0x010e, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:29:0x010f, code lost:
                    r3 = r11;
                    r8.setVersioncode(0);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x0115, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:32:0x0118, code lost:
                    r11 = move-exception;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
                    return;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
                    return;
                 */
                /* JADX WARNING: Failed to process nested try/catch */
                /* JADX WARNING: Removed duplicated region for block: B:26:0x010b A[Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }, ExcHandler: IOException (r11v16 'e' java.io.IOException A[CUSTOM_DECLARE, Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }]), Splitter:B:12:0x008e] */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x0118 A[ExcHandler: NameNotFoundException (r11v1 'e' android.content.pm.PackageManager$NameNotFoundException A[CUSTOM_DECLARE]), Splitter:B:2:0x0005] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r14 = this;
                        com.kandian.ksfamily.KSApp r8 = new com.kandian.ksfamily.KSApp     // Catch:{ Exception -> 0x011b }
                        r8.<init>()     // Catch:{ Exception -> 0x011b }
                        android.content.Context r11 = r4     // Catch:{ NameNotFoundException -> 0x0118 }
                        android.content.pm.PackageManager r11 = r11.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0118 }
                        android.content.Context r12 = r4     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = r12.getPackageName()     // Catch:{ NameNotFoundException -> 0x0118 }
                        r13 = 0
                        android.content.pm.PackageInfo r11 = r11.getPackageInfo(r12, r13)     // Catch:{ NameNotFoundException -> 0x0118 }
                        int r10 = r11.versionCode     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r11 = com.kandian.common.UpdateUtil.TAG     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r13 = "versionCode = "
                        r12.<init>(r13)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = r12.append(r10)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = r12.toString()     // Catch:{ NameNotFoundException -> 0x0118 }
                        com.kandian.common.Log.v(r11, r12)     // Catch:{ NameNotFoundException -> 0x0118 }
                        android.content.Context r11 = r4     // Catch:{ NameNotFoundException -> 0x0118 }
                        android.content.pm.PackageManager r11 = r11.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0118 }
                        android.content.Context r12 = r4     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = r12.getPackageName()     // Catch:{ NameNotFoundException -> 0x0118 }
                        r13 = 0
                        android.content.pm.PackageInfo r11 = r11.getPackageInfo(r12, r13)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r5 = r11.packageName     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r11 = com.kandian.common.UpdateUtil.TAG     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r13 = "packageName = "
                        r12.<init>(r13)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = r12.toString()     // Catch:{ NameNotFoundException -> 0x0118 }
                        com.kandian.common.Log.v(r11, r12)     // Catch:{ NameNotFoundException -> 0x0118 }
                        android.content.Context r11 = r4     // Catch:{ NameNotFoundException -> 0x0118 }
                        r12 = 2131099655(0x7f060007, float:1.781167E38)
                        java.lang.String r11 = r11.getString(r12)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = "{packageName}"
                        java.lang.String r1 = com.kandian.common.StringUtil.replace(r11, r12, r5)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r11 = "{partner}"
                        android.content.Context r12 = r4     // Catch:{ NameNotFoundException -> 0x0118 }
                        r13 = 2131099659(0x7f06000b, float:1.7811677E38)
                        java.lang.String r12 = r12.getString(r13)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = com.kandian.common.StringUtil.urlEncode(r12)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r1 = com.kandian.common.StringUtil.replace(r1, r11, r12)     // Catch:{ NameNotFoundException -> 0x0118 }
                        r7 = 0
                        android.content.Context r11 = r4     // Catch:{ Exception -> 0x011d, NameNotFoundException -> 0x0118 }
                        org.json.JSONObject r0 = com.kandian.common.UpdateUtil.getActiveInfo(r11)     // Catch:{ Exception -> 0x011d, NameNotFoundException -> 0x0118 }
                        if (r0 == 0) goto L_0x0084
                        java.lang.String r11 = "status"
                        int r7 = r0.getInt(r11)     // Catch:{ Exception -> 0x011d, NameNotFoundException -> 0x0118 }
                    L_0x0084:
                        java.lang.String r11 = "{status}"
                        java.lang.String r12 = java.lang.String.valueOf(r7)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r1 = com.kandian.common.StringUtil.replace(r1, r11, r12)     // Catch:{ NameNotFoundException -> 0x0118 }
                        java.lang.String r11 = com.kandian.common.UpdateUtil.TAG     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.String r13 = "getStringFromURL "
                        r12.<init>(r13)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = r12.append(r1)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        com.kandian.common.Log.v(r11, r12)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.String r6 = com.kandian.common.StringUtil.getStringFromURL(r1)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.String r11 = com.kandian.common.UpdateUtil.TAG     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.String r13 = "result = "
                        r12.<init>(r13)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.StringBuilder r12 = r12.append(r6)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        com.kandian.common.Log.v(r11, r12)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        if (r6 == 0) goto L_0x00c6
                        java.lang.String r11 = r6.trim()     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        int r11 = r11.length()     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        if (r11 != 0) goto L_0x00c7
                    L_0x00c6:
                        return
                    L_0x00c7:
                        java.lang.String r11 = "\\|"
                        java.lang.String[] r9 = r6.split(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r11 = 1
                        r11 = r9[r11]     // Catch:{ Exception -> 0x010e, IOException -> 0x010b, NameNotFoundException -> 0x0118 }
                        java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x010e, IOException -> 0x010b, NameNotFoundException -> 0x0118 }
                        int r11 = r11.intValue()     // Catch:{ Exception -> 0x010e, IOException -> 0x010b, NameNotFoundException -> 0x0118 }
                        r8.setVersioncode(r11)     // Catch:{ Exception -> 0x010e, IOException -> 0x010b, NameNotFoundException -> 0x0118 }
                    L_0x00db:
                        r11 = 2
                        r11 = r9[r11]     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r8.setVersionname(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r11 = 3
                        r11 = r9[r11]     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r8.setAppname(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r11 = 4
                        r11 = r9[r11]     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r8.setUpdateinfo(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r11 = 5
                        r11 = r9[r11]     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r8.setDownloadurl(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r8.setPackagename(r5)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        int r11 = r8.getVersioncode()     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        if (r10 >= r11) goto L_0x00c6
                        android.os.Handler r11 = com.kandian.common.UpdateUtil.myViewUpdateHandler     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        android.os.Message r4 = android.os.Message.obtain(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r11 = 0
                        r4.what = r11     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r4.obj = r8     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        r4.sendToTarget()     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        goto L_0x00c6
                    L_0x010b:
                        r11 = move-exception
                        r2 = r11
                        goto L_0x00c6
                    L_0x010e:
                        r11 = move-exception
                        r3 = r11
                        r11 = 0
                        r8.setVersioncode(r11)     // Catch:{ IOException -> 0x010b, Exception -> 0x0115, NameNotFoundException -> 0x0118 }
                        goto L_0x00db
                    L_0x0115:
                        r11 = move-exception
                        r2 = r11
                        goto L_0x00c6
                    L_0x0118:
                        r11 = move-exception
                        r2 = r11
                        goto L_0x00c6
                    L_0x011b:
                        r11 = move-exception
                        goto L_0x00c6
                    L_0x011d:
                        r11 = move-exception
                        goto L_0x0084
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.UpdateUtil.AnonymousClass3.run():void");
                }
            }).start();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static void installAPK(String apkPath) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(apkPath)), "application/vnd.android.package-archive");
        _context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static synchronized void downloadAPK(final KSApp ksApp) {
        synchronized (UpdateUtil.class) {
            View dialogView = LayoutInflater.from(_context).inflate((int) R.layout.dialog_view, (ViewGroup) null);
            ((TextView) dialogView.findViewById(R.id.dialogcontent)).setText("准备下载更新...");
            Dialog dialog = new AlertDialog.Builder(_context).setIcon((int) R.drawable.ksicon).setTitle(ksApp.getAppname()).setView(dialogView).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    UpdateUtil.downloadThreadMap.put(Long.valueOf(UpdateUtil.downloadThreadID), false);
                    Log.v(UpdateUtil.TAG, "downloadThreadID = " + UpdateUtil.downloadThreadID);
                    dialog.cancel();
                }
            }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == 4) {
                        UpdateUtil.downloadThreadMap.put(Long.valueOf(UpdateUtil.downloadThreadID), false);
                        Log.v(UpdateUtil.TAG, "downloadThreadID = " + UpdateUtil.downloadThreadID);
                        dialog.cancel();
                    }
                    return false;
                }
            }).create();
            dialog.show();
            downloadDialog = dialog;
            downLoadView = dialogView;
            ((ProgressBar) dialogView.findViewById(R.id.download_pb)).setVisibility(0);
            Thread t = new Thread() {
                public void run() {
                    try {
                        String downloadDirName = String.valueOf(new File(PreferenceSetting.getDownloadDir()).getParent()) + UpdateUtil._context.getString(R.string.ksfamily_downloadDir);
                        File downloadDir = new File(downloadDirName);
                        if (!downloadDir.exists()) {
                            downloadDir.mkdirs();
                        }
                        Log.d(UpdateUtil.TAG, "downloadAPK start");
                        UpdateUtil.downloadThreadMap.put(Long.valueOf(getId()), true);
                        UpdateUtil.downloadFile(KSApp.this.getDownloadurl(), String.valueOf(downloadDirName) + KSApp.this.getPackagename() + ".apk", getId());
                    } catch (IOException e) {
                        UpdateUtil.sendMsg(-1, null);
                    }
                }
            };
            t.start();
            downloadThreadID = t.getId();
        }
    }

    public static void downloadFile(String url, String filepath, long threadId) throws IOException {
        URLConnection conn = new URL(url).openConnection();
        conn.connect();
        InputStream is = conn.getInputStream();
        fileSize = conn.getContentLength();
        if (fileSize <= 0) {
            throw new RuntimeException("无法获知文件大小 ");
        } else if (is == null) {
            throw new RuntimeException("stream is null");
        } else {
            FileOutputStream fos = new FileOutputStream(filepath);
            byte[] buf = new byte[1024];
            int downLoadFileSize = 0;
            sendMsg(0, 0);
            while (true) {
                int numread = is.read(buf);
                if (numread == -1) {
                    sendMsg(2, filepath);
                    try {
                        is.close();
                        return;
                    } catch (Exception ex) {
                        Log.e("tag", "error: " + ex.getMessage(), ex);
                        return;
                    }
                } else if (downloadThreadMap.get(Long.valueOf(threadId)).booleanValue()) {
                    fos.write(buf, 0, numread);
                    downLoadFileSize += numread;
                    sendMsg(1, Integer.valueOf(downLoadFileSize));
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void sendMsg(int flag, Object obj) {
        Message msg = new Message();
        msg.what = flag;
        msg.obj = obj;
        handler.sendMessage(msg);
    }

    public static JSONObject getActiveInfo(Context context) {
        JSONObject result = null;
        try {
            String jsonResult = context.getSharedPreferences(String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName) + ".activeinfo", 0).getString(ACTIVEINFO_KEY, "");
            if (jsonResult != null && jsonResult.trim().length() > 0) {
                try {
                    result = new JSONObject(jsonResult);
                } catch (JSONException e) {
                    return null;
                }
            }
            return result;
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }
}
