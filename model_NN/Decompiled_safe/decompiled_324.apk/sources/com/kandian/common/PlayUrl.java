package com.kandian.common;

public class PlayUrl {
    private int hd;
    private int ipad;
    private String resourcecode;
    private String url;

    public String getResourcecode() {
        return this.resourcecode;
    }

    public void setResourcecode(String resourcecode2) {
        this.resourcecode = resourcecode2;
    }

    public int getHd() {
        return this.hd;
    }

    public void setHd(int hd2) {
        this.hd = hd2;
    }

    public int getIpad() {
        return this.ipad;
    }

    public void setIpad(int ipad2) {
        this.ipad = ipad2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
