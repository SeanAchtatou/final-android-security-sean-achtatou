package com.kandian.common;

import android.app.Application;
import java.net.URI;
import java.util.ArrayList;

public class SiteUrls {
    static String TAG = "SiteUrls";
    public static final int URL_TYPE_FLV = 1;
    public static final int URL_TYPE_MP4 = 2;
    public static final int URL_TYPE_TS = 3;
    public static final int URL_TYPE_WEBPAGE = 0;
    ArrayList<SiteUrl> downloadUrls = new ArrayList<>();
    ArrayList<SiteUrl> playUrls = new ArrayList<>();
    ArrayList<SiteUrl> webpageUrls = new ArrayList<>();

    public class SiteUrl {
        public String url = null;
        public int urlType = 0;

        public SiteUrl(int type, String u) {
            this.urlType = type;
            this.url = u;
        }

        public String getDisplayName() {
            String result = SiteUrls.this.getSourceName(this.url);
            String type = null;
            switch (this.urlType) {
                case 1:
                    type = "flv";
                    break;
                case 2:
                    type = "mp4";
                    break;
                case 3:
                    type = "ts";
                    break;
            }
            if (type != null) {
                return String.valueOf(type) + "," + result;
            }
            return result;
        }
    }

    /* access modifiers changed from: private */
    public String getSourceName(String sourceUrl) {
        String result = sourceUrl;
        URI uri = URI.create(sourceUrl);
        if (uri != null) {
            String host = uri.getHost();
            int lastIndex = host.lastIndexOf(46);
            int firstIndex = host.indexOf(46);
            if (!(lastIndex == -1 || firstIndex == -1 || firstIndex >= lastIndex)) {
                result = host.substring(firstIndex + 1);
            }
        }
        Log.v(TAG, "sourceUrl " + sourceUrl + " is at " + result);
        return result;
    }

    public ArrayList<SiteUrl> getPlayUrls() {
        return this.playUrls;
    }

    public ArrayList<SiteUrl> getDownloadUrls() {
        return this.downloadUrls;
    }

    public ArrayList<SiteUrl> getWebpageUrls() {
        return this.webpageUrls;
    }

    public void initialize(ArrayList<String> urls, SiteConfigs siteConfigs, Application app) {
        this.webpageUrls.clear();
        this.downloadUrls.clear();
        this.playUrls.clear();
        if (siteConfigs != null && urls != null && urls.size() > 0) {
            String siteFilter = PreferenceSetting.getSystemConfigFilterSites(app);
            for (int i = 0; i < urls.size(); i++) {
                String urlToCheck = urls.get(i);
                if (siteConfigs.isWebpageSite(urlToCheck, siteFilter)) {
                    this.webpageUrls.add(new SiteUrl(0, urlToCheck));
                }
                if (siteConfigs.isMp4Site(urlToCheck, siteFilter)) {
                    this.playUrls.add(new SiteUrl(2, urlToCheck));
                    this.downloadUrls.add(new SiteUrl(2, urlToCheck));
                }
                if (siteConfigs.isFlvSite(urlToCheck, siteFilter)) {
                    this.downloadUrls.add(new SiteUrl(1, urlToCheck));
                }
                if (siteConfigs.isTsSite(urlToCheck, siteFilter)) {
                    this.downloadUrls.add(new SiteUrl(3, urlToCheck));
                }
            }
        }
    }

    public void newInitialize(ArrayList<PlayUrl> urls, SiteConfigs siteConfigs, Application app) {
        this.webpageUrls.clear();
        this.downloadUrls.clear();
        this.playUrls.clear();
        if (siteConfigs != null && urls != null && urls.size() > 0) {
            String siteFilter = PreferenceSetting.getSystemConfigFilterSites(app);
            for (int i = 0; i < urls.size(); i++) {
                PlayUrl playUrl = urls.get(i);
                String urlToCheck = playUrl.getUrl();
                if (siteConfigs.isWebpageSite(urlToCheck, siteFilter)) {
                    this.webpageUrls.add(new SiteUrl(0, urlToCheck));
                }
                if (siteConfigs.isMp4Site(playUrl, siteFilter)) {
                    this.playUrls.add(new SiteUrl(2, urlToCheck));
                    this.downloadUrls.add(new SiteUrl(2, urlToCheck));
                }
                if (siteConfigs.isFlvSite(urlToCheck, siteFilter)) {
                    this.downloadUrls.add(new SiteUrl(1, urlToCheck));
                }
                if (siteConfigs.isTsSite(urlToCheck, siteFilter)) {
                    this.downloadUrls.add(new SiteUrl(3, urlToCheck));
                }
            }
        }
    }
}
