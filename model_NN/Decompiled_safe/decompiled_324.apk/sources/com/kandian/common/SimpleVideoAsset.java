package com.kandian.common;

import android.app.Application;

public class SimpleVideoAsset {
    private long assetId = 0;
    private long assetIdX = 0;
    private String assetKey = "";
    private String assetType = "";
    private String bigImageUrl = "";
    private String category = "";
    private String displayName = "";
    private int finished = 0;
    private String imageUrl = "";
    private String origin = "";
    private double vote = -1.0d;

    public SimpleVideoAsset(VideoAsset videoAsset, Application app) {
        setImageUrl(videoAsset.getImageUrl());
        setBigImageUrl(videoAsset.getBigImageUrl());
        setAssetType(videoAsset.getAssetType());
        setAssetId(videoAsset.getAssetId());
        setAssetIdX(videoAsset.getAssetIdX());
        setAssetKey(videoAsset.getAssetKey());
        setCategory(videoAsset.getCategory());
        setOrigin(videoAsset.getOrigin());
        setFinished(videoAsset.getFinished());
        setVote(videoAsset.getVote());
        setDisplayName(videoAsset.getDisplayName(app));
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category2) {
        this.category = category2;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setDisplayName(String displayName2) {
        this.displayName = displayName2;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setAssetId(long assetId2) {
        this.assetId = assetId2;
    }

    public void setAssetId(String assetId2) {
        try {
            this.assetId = Long.parseLong(assetId2);
        } catch (Exception e) {
        }
    }

    public long getAssetId() {
        return this.assetId;
    }

    public void setOrigin(String origin2) {
        this.origin = origin2;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setAssetIdX(long assetIdX2) {
        this.assetIdX = assetIdX2;
    }

    public void setAssetIdX(String assetIdX2) {
        try {
            this.assetIdX = Long.parseLong(assetIdX2);
        } catch (Exception e) {
        }
    }

    public long getAssetIdX() {
        return this.assetIdX;
    }

    public void setAssetKey(String assetKey2) {
        this.assetKey = assetKey2;
    }

    public String getAssetKey() {
        return this.assetKey;
    }

    public int getFinished() {
        return this.finished;
    }

    public void setFinished(int finished2) {
        this.finished = finished2;
    }

    public void setFinished(String finished2) {
        try {
            this.finished = Integer.parseInt(finished2);
        } catch (Exception e) {
        }
    }

    public void setVote(double vote2) {
        this.vote = vote2;
    }

    public double getVote() {
        return this.vote;
    }

    public void setAssetType(String assetType2) {
        this.assetType = assetType2;
    }

    public String getAssetType() {
        return this.assetType;
    }

    public void setBigImageUrl(String bigImageUrl2) {
        this.bigImageUrl = bigImageUrl2;
    }

    public String getBigImageUrl() {
        return this.bigImageUrl;
    }
}
