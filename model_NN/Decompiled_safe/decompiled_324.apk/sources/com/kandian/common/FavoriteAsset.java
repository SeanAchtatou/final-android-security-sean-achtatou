package com.kandian.common;

import org.json.JSONObject;

public class FavoriteAsset {
    public static final int OPT_ACTION_ADD = 1;
    public static final int OPT_ACTION_DEL = 2;
    public static final int OPT_ACTION_NORMAL = 0;
    public static final int STATUS_NORMAL = 0;
    public static final int STATUS_OFFLINE = 1;
    public static final int UPDATE_NEED = 1;
    public static final int UPDATE_NOT_NEED = 0;
    private String asseType = "";
    private long assetId = 0;
    private String assetKey = "";
    private String assetName = "";
    private String bigImageUrl = "";
    private String imageUrl = "";
    private int isUpdate;
    private int optaction = 0;
    private String origin = "";
    private int status = 0;
    private double vote = 0.0d;

    public int getIsUpdate() {
        return this.isUpdate;
    }

    public void setIsUpdate(int isUpdate2) {
        this.isUpdate = isUpdate2;
    }

    public FavoriteAsset(VideoAsset videoAsset) {
        setAssetId(videoAsset.getAssetId());
        setAssetName(videoAsset.getAssetName());
        setAsseType(videoAsset.getAssetType());
        setAssetKey(videoAsset.getAssetKey());
        setVote(videoAsset.getVote());
        setImageUrl(videoAsset.getImageUrl());
        setBigImageUrl(videoAsset.getBigImageUrl());
        setOrigin(videoAsset.getOrigin());
        setStatus(0);
        setOptaction(0);
        setIsUpdate(0);
    }

    public FavoriteAsset() {
    }

    public long getAssetId() {
        return this.assetId;
    }

    public void setAssetId(long assetId2) {
        this.assetId = assetId2;
    }

    public String getAssetName() {
        return this.assetName;
    }

    public void setAssetName(String assetName2) {
        this.assetName = assetName2;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setOrigin(String origin2) {
        this.origin = origin2;
    }

    public double getVote() {
        return this.vote;
    }

    public void setVote(double vote2) {
        this.vote = vote2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public void setBigImageUrl(String bigImageUrl2) {
        this.bigImageUrl = bigImageUrl2;
    }

    public String getBigImageUrl() {
        return this.bigImageUrl;
    }

    public String getAssetKey() {
        return this.assetKey;
    }

    public void setAssetKey(String assetKey2) {
        this.assetKey = assetKey2;
    }

    public String getAsseType() {
        return this.asseType;
    }

    public void setAsseType(String asseType2) {
        this.asseType = asseType2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getOptaction() {
        return this.optaction;
    }

    public void setOptaction(int optaction2) {
        this.optaction = optaction2;
    }

    public static String getJsonString4JavaPOJO(FavoriteAsset pojoCalss) {
        StringBuffer sb = new StringBuffer("{");
        sb.append("\"").append("assetId").append("\":\"").append(pojoCalss.getAssetId()).append("\",");
        sb.append("\"").append("assetName").append("\":\"").append(pojoCalss.getAssetName()).append("\",");
        sb.append("\"").append("origin").append("\":\"").append(pojoCalss.getOrigin()).append("\",");
        sb.append("\"").append("vote").append("\":\"").append(pojoCalss.getVote()).append("\",");
        sb.append("\"").append("imageUrl").append("\":\"").append(pojoCalss.getImageUrl()).append("\",");
        sb.append("\"").append("bigImageUrl").append("\":\"").append(pojoCalss.getBigImageUrl()).append("\",");
        sb.append("\"").append("assetKey").append("\":\"").append(pojoCalss.getAssetKey()).append("\",");
        sb.append("\"").append("asseType").append("\":\"").append(pojoCalss.getAsseType()).append("\",");
        sb.append("\"").append("status").append("\":\"").append(pojoCalss.getStatus()).append("\",");
        sb.append("\"").append("optaction").append("\":\"").append(pojoCalss.getOptaction()).append("\",");
        sb.append("\"").append("isUpdate").append("\":\"").append(pojoCalss.getIsUpdate()).append("\"");
        sb.append("}");
        return sb.toString();
    }

    public static FavoriteAsset getObject4JsonString(String jsonString) {
        FavoriteAsset favorite = new FavoriteAsset();
        try {
            JSONObject json = new JSONObject(jsonString);
            favorite.setAssetId(json.getLong("assetId"));
            favorite.setAssetName(json.getString("assetName"));
            favorite.setAsseType(json.getString("asseType"));
            favorite.setAssetKey(json.getString("assetKey"));
            favorite.setImageUrl(json.getString("imageUrl"));
            favorite.setBigImageUrl(json.getString("bigImageUrl"));
            favorite.setVote(json.getDouble("vote"));
            favorite.setOrigin(json.getString("origin"));
            try {
                favorite.setStatus(json.getInt("status"));
                favorite.setOptaction(json.getInt("optaction"));
                favorite.setIsUpdate(json.getInt("isUpdate"));
            } catch (Exception e) {
                favorite.setStatus(0);
                favorite.setOptaction(0);
                favorite.setIsUpdate(0);
            }
            return favorite;
        } catch (Exception e2) {
            return null;
        }
    }
}
