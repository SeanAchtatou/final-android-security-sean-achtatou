package com.kandian.common;

import android.content.Context;
import com.kandian.cartoonapp.R;

public class SiteViewUrl {
    public static String getAssetUrl4W(String assettype, String assetkey, Context context) {
        return context.getString(R.string.viewurl).replace("assettype", assettype).replace("assetkey", assetkey);
    }
}
