package com.kandian.common;

import android.content.Context;
import com.kandian.cartoonapp.R;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import weibo4j.AsyncWeibo;

public class AssetListSaxHandler extends DefaultHandler {
    final int ASSETNAME = 1;
    final int ASSETTYPE = 2;
    final int CATEGORY = 3;
    final int CLICKTOTAL = 4;
    final int CREATETIME = 5;
    final int HD = 6;
    final int ID = 7;
    final int IDX = 32;
    final int INITIALS = 8;
    final int INTRO = 9;
    final int ISFINISHED = 10;
    final int ITEMID = 11;
    final int KEY = 12;
    final int LASTMODIFIEDTIME = 13;
    final int LENGTH = 14;
    final int OPLUSPHOTO = 15;
    final int OPLUSTHUMB = 16;
    final int ORIGIN = 17;
    final int ORIGINTYPE = 18;
    final int PERSONNEL1 = 19;
    final int PERSONNEL2 = 20;
    final int PINYIN = 21;
    final int PLUGINS = 22;
    final int RESOURCECODE = 23;
    final int RESOURCENAME = 24;
    final int SCORE = 25;
    final int SHOWTIME = 26;
    final int SMALLPHOTO = 27;
    final String TAG = "AssetListSaxHandler";
    final int TOTAL = 28;
    final int TYPE = 29;
    final int VOTE = 30;
    final int YEAR = 31;
    Context context = null;
    VideoAsset currentAsset = null;
    int currentElementType = 0;
    final StringBuffer currentText = new StringBuffer();
    AssetList itemList = null;

    public AssetListSaxHandler(Context context2, AssetList items) {
        this.itemList = items;
        this.context = context2;
    }

    public void startDocument() {
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        String name = attributes.getValue("name");
        if (localName.equals("result")) {
            try {
                int num = Integer.parseInt(attributes.getValue("numFound"));
                Log.v("AssetListSaxHandler", "Found " + num + " results.");
                this.itemList.setTotalResultCount(num);
            } catch (Exception e) {
            }
        } else if (localName.equals("doc")) {
            this.currentAsset = new VideoAsset();
        } else if (name == null) {
        } else {
            if (name.equals("assetName")) {
                this.currentElementType = 1;
            } else if (name.equals("assetType")) {
                this.currentElementType = 2;
            } else if (name.equals("category")) {
                this.currentElementType = 3;
            } else if (name.equals("showTime")) {
                this.currentElementType = 26;
            } else if (name.equals("id")) {
                this.currentElementType = 7;
            } else if (name.equals("idx")) {
                this.currentElementType = 32;
            } else if (name.equals("introduction")) {
                this.currentElementType = 9;
            } else if (name.equals("itemId")) {
                this.currentElementType = 11;
            } else if (name.equals("key")) {
                this.currentElementType = 12;
            } else if (name.equals("oplusPhoto")) {
                this.currentElementType = 15;
            } else if (name.equals("oplusThumb")) {
                this.currentElementType = 16;
            } else if (name.equals("origin")) {
                this.currentElementType = 17;
            } else if (name.equals("origintype")) {
                this.currentElementType = 18;
            } else if (name.equals("personnel1")) {
                this.currentElementType = 19;
            } else if (name.equals("personnel2")) {
                this.currentElementType = 20;
            } else if (name.equals("pinyin")) {
                this.currentElementType = 21;
            } else if (name.equals("resourcesCode")) {
                this.currentElementType = 23;
            } else if (name.equals("resourcesName")) {
                this.currentElementType = 24;
            } else if (name.equals("score")) {
                this.currentElementType = 25;
            } else if (name.equals("type")) {
                this.currentElementType = 29;
            } else if (name.equals("year")) {
                this.currentElementType = 31;
            } else if (name.equals("isFinished")) {
                this.currentElementType = 10;
            } else if (name.equals("total")) {
                this.currentElementType = 28;
            } else if (name.equals("vote")) {
                this.currentElementType = 30;
            } else if (name.equals("smallPhoto")) {
                this.currentElementType = 27;
            }
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (localName.equals("doc")) {
            if (this.currentAsset != null) {
                VideoAsset item = (VideoAsset) this.currentAsset.clone();
                if (item.getAssetId() > 0) {
                    VideoAssetMap.instance().putAsset(item.getAssetKey(), item.getAssetType(), item);
                    this.itemList.getList().add(item);
                }
                this.currentAsset = null;
            }
        } else if (this.currentAsset != null) {
            switch (this.currentElementType) {
                case 1:
                    this.currentAsset.setAssetName(this.currentText.toString().trim());
                    break;
                case 2:
                    this.currentAsset.setAssetType(this.currentText.toString().trim());
                    break;
                case 3:
                    this.currentAsset.setCategory(this.currentText.toString().trim());
                    break;
                case 7:
                    this.currentAsset.setAssetId(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.USER_DETAIL /*9*/:
                    this.currentAsset.setAssetDescription(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.DIRECT_MESSAGES /*10*/:
                    this.currentAsset.setFinished(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.SEND_DIRECT_MESSAGE /*11*/:
                    this.currentAsset.setItemId(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.CREATE /*12*/:
                    this.currentAsset.setAssetKey(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.LEAVE /*15*/:
                    this.currentAsset.setBigImageUrl(String.valueOf(this.context.getString(R.string.imageServerPrefix)) + this.currentText.toString());
                    break;
                case 16:
                    this.currentAsset.setImageUrl(String.valueOf(this.context.getString(R.string.imageServerPrefix)) + this.currentText.toString());
                    break;
                case 17:
                    this.currentAsset.setOrigin(this.currentText.toString());
                    break;
                case AsyncWeibo.DESTROY_FAVORITE /*19*/:
                    this.currentAsset.setAssetActor(this.currentText.toString().trim());
                    break;
                case 20:
                    this.currentAsset.setAssetDirector(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.UPDATE_DELIVERLY_DEVICE /*21*/:
                    this.currentAsset.setAssetPinyin(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.UNBLOCK /*23*/:
                    this.currentAsset.setAssetSourceCode(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.TEST /*24*/:
                    String sources = this.currentText.toString().trim();
                    if (sources.length() > 0) {
                        this.currentAsset.setAssetSource(sources.substring(0, sources.length() - 1));
                        break;
                    }
                    break;
                case AsyncWeibo.GET_DOWNTIME_SCHEDULE /*25*/:
                    this.currentAsset.setScore(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.DESTROY_STATUS /*26*/:
                    this.currentAsset.setShowTime(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.SEARCH /*27*/:
                    this.currentAsset.setSmallPhotoImageUrl(String.valueOf(this.context.getString(R.string.imageServerPrefix)) + this.currentText.toString());
                    break;
                case 28:
                    this.currentAsset.setTotal(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.FRIENDS_IDS /*29*/:
                    this.currentAsset.setType(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.FOLLOWERS_IDS /*30*/:
                    double voteValue = -1.0d;
                    try {
                        voteValue = Double.parseDouble(this.currentText.toString().trim());
                    } catch (Exception e) {
                        Log.v("AssetListSaxHandler", e.toString());
                    }
                    this.currentAsset.setVote(voteValue);
                    break;
                case AsyncWeibo.UPDATE_PROFILE_COLORS /*31*/:
                    this.currentAsset.setAssetYear(this.currentText.toString().trim());
                    break;
                case AsyncWeibo.CREATE_FRIENDSHIP /*32*/:
                    this.currentAsset.setAssetIdX(this.currentText.toString().trim());
                    break;
            }
            this.currentText.delete(0, this.currentText.length());
            this.currentElementType = 0;
        }
    }

    public void endDocument() {
    }

    public void characters(char[] ch, int start, int length) {
        if (this.currentElementType > 0) {
            this.currentText.append(ch, start, length);
        }
    }
}
