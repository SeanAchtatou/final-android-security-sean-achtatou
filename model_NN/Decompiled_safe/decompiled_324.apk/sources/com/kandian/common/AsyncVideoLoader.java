package com.kandian.common;

import android.app.Application;
import android.os.Message;
import android.webkit.URLUtil;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class AsyncVideoLoader {
    private static AsyncVideoLoader _instance = null;
    public final int FAILED = 2;
    public final int INITIALIZED = 0;
    public final int LOADED = 1;
    final int MAX_DOWNLOAD_TIMES = 5;
    public final int PROGRESS_CHANGED = 3;
    /* access modifiers changed from: private */
    public String TAG = "AsyncVideoLoader";
    public final int WAITING = 4;
    public final int WIFI_CLOSED = 5;
    private HashMap<String, MyThread> activeThreadMap = new HashMap<>();
    private HashMap<String, VideoCallback> callbackMap = new HashMap<>();
    /* access modifiers changed from: private */
    public File tmpDir = null;
    private HashMap<String, CacheFile> videoCache = new HashMap<>();

    public interface VideoCallback {
        void networkStatus(String str);

        void videoBufferProgressChanged(Long l, String str);

        void videoInitialized(CacheFile cacheFile, String str);

        void videoLoaded(CacheFile cacheFile, String str);

        void videoLoadingFailed(Exception exc, String str, String str2);
    }

    public class CacheFile {
        public String cachePath = null;
        public long fileSize = -1;

        public CacheFile() {
        }
    }

    public class MyThread extends Thread {
        private volatile boolean stopThreadFlag;

        public MyThread(String threadName) {
            super(threadName);
            this.stopThreadFlag = false;
            this.stopThreadFlag = false;
        }

        public void stopThreadFlag() {
            this.stopThreadFlag = true;
        }

        public boolean getStopThreadFlag() {
            return this.stopThreadFlag;
        }
    }

    protected AsyncVideoLoader() {
        Log.v(this.TAG, "tmpDir is " + PreferenceSetting.getDownloadDir());
        this.tmpDir = new File(PreferenceSetting.getDownloadDir());
        if (this.tmpDir.exists() || this.tmpDir.mkdirs()) {
            Log.v(this.TAG, "tmp directory exists");
        } else {
            Log.v(this.TAG, "failed in creating tmp directory fails");
        }
    }

    public static AsyncVideoLoader instance() {
        if (_instance == null) {
            _instance = new AsyncVideoLoader();
        }
        return _instance;
    }

    public CacheFile obtainFile(String filePath) {
        CacheFile cache = new CacheFile();
        cache.cachePath = filePath;
        File file = new File(filePath);
        if (file.exists()) {
            cache.fileSize = file.length();
        }
        return cache;
    }

    public CacheFile loadVideo(String videoUrl, String refererUrl, String filePath, VideoCallback videoCallback, Application app) {
        if (!URLUtil.isNetworkUrl(videoUrl)) {
            return obtainFile(videoUrl);
        }
        registerVideoCallback(filePath, videoCallback);
        if (this.activeThreadMap.containsKey(filePath)) {
            return null;
        }
        final String str = filePath;
        final String str2 = videoUrl;
        final String str3 = refererUrl;
        final Application application = app;
        MyThread t = new MyThread(videoUrl) {
            public void run() {
                File temp;
                try {
                    if (str == null) {
                        temp = File.createTempFile("mediaplayertmp", "dat", AsyncVideoLoader.this.tmpDir);
                        temp.deleteOnExit();
                    } else {
                        temp = new File(str);
                    }
                    CacheFile cacheFile = null;
                    int i = 0;
                    while (i < 5) {
                        cacheFile = AsyncVideoLoader.this.saveVideoFromUrl(str2, str3, temp, this, application);
                        if (!temp.exists() || temp.length() <= 0 || temp.length() != cacheFile.fileSize) {
                            try {
                                Log.v(AsyncVideoLoader.this.TAG, "waiting for 10 seconds");
                                Message message = Message.obtain();
                                message.what = 4;
                                AsyncVideoLoader.this.handleMessage(message, str, str2);
                                Thread.sleep(10000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            i++;
                        } else {
                            Message message2 = Message.obtain();
                            message2.what = 1;
                            message2.obj = cacheFile;
                            AsyncVideoLoader.this.handleMessage(message2, str, str2);
                            return;
                        }
                    }
                    Exception eWeird = new Exception("something werid happens");
                    Log.v(AsyncVideoLoader.this.TAG, "temp.length:cacheFile.fileSize = " + temp.length() + ":" + cacheFile.fileSize);
                    Message message3 = Message.obtain();
                    message3.what = 2;
                    message3.obj = eWeird;
                    AsyncVideoLoader.this.handleMessage(message3, str, str2);
                } catch (Exception e2) {
                    Message message4 = Message.obtain();
                    message4.what = 2;
                    message4.obj = e2;
                    AsyncVideoLoader.this.handleMessage(message4, str, str2);
                }
            }
        };
        this.activeThreadMap.put(filePath, t);
        t.start();
        return null;
    }

    /* access modifiers changed from: private */
    public CacheFile saveVideoFromUrl(String urlToSave, String refererUrl, File targetFile, MyThread currentThread, Application app) throws IOException {
        return saveVideoFromUrl(urlToSave, refererUrl, targetFile, true, currentThread, app);
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0378  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.kandian.common.AsyncVideoLoader.CacheFile saveVideoFromUrl(java.lang.String r37, java.lang.String r38, java.io.File r39, boolean r40, com.kandian.common.AsyncVideoLoader.MyThread r41, android.app.Application r42) throws java.io.IOException {
        /*
            r36 = this;
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "saving "
            r5.<init>(r6)
            r0 = r5
            r1 = r37
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
            boolean r4 = android.webkit.URLUtil.isNetworkUrl(r37)
            if (r4 != 0) goto L_0x003d
            com.kandian.common.AsyncVideoLoader$CacheFile r13 = new com.kandian.common.AsyncVideoLoader$CacheFile
            r0 = r13
            r1 = r36
            r0.<init>()
            r0 = r37
            r1 = r13
            r1.cachePath = r0
            java.io.File r4 = new java.io.File
            r0 = r4
            r1 = r37
            r0.<init>(r1)
            long r4 = r4.length()
            r13.fileSize = r4
            r4 = r13
        L_0x003c:
            return r4
        L_0x003d:
            boolean r4 = com.kandian.common.PreferenceSetting.getDownloadByWifi(r42)
            if (r4 == 0) goto L_0x008a
            java.lang.String r4 = "connectivity"
            r0 = r42
            r1 = r4
            java.lang.Object r15 = r0.getSystemService(r1)
            android.net.ConnectivityManager r15 = (android.net.ConnectivityManager) r15
            android.net.NetworkInfo r11 = r15.getActiveNetworkInfo()
            if (r11 == 0) goto L_0x005b
            int r4 = r11.getType()
            r5 = 1
            if (r4 == r5) goto L_0x008a
        L_0x005b:
            android.os.Message r27 = android.os.Message.obtain()
            r4 = 5
            r0 = r4
            r1 = r27
            r1.what = r0
            r4 = 2131099828(0x7f0600b4, float:1.781202E38)
            r0 = r42
            r1 = r4
            java.lang.String r4 = r0.getString(r1)
            r0 = r4
            r1 = r27
            r1.obj = r0
            java.lang.String r4 = r39.getAbsolutePath()
            r0 = r36
            r1 = r27
            r2 = r4
            r3 = r37
            r0.handleMessage(r1, r2, r3)
            java.io.IOException r4 = new java.io.IOException
            java.lang.String r5 = "wifi connection is not available."
            r4.<init>(r5)
            throw r4
        L_0x008a:
            java.net.URL r35 = new java.net.URL
            r0 = r35
            r1 = r37
            r0.<init>(r1)
            java.net.URLConnection r14 = r35.openConnection()
            java.lang.String r4 = "Accept"
            java.lang.String r5 = "*/*"
            r14.setRequestProperty(r4, r5)
            java.lang.String r4 = "Accept-Encoding"
            java.lang.String r5 = "gzip, deflate"
            r14.setRequestProperty(r4, r5)
            java.lang.String r4 = "Accept-Language"
            java.lang.String r5 = "zh-cn"
            r14.setRequestProperty(r4, r5)
            if (r38 == 0) goto L_0x00b7
            java.lang.String r4 = "Referer"
            r0 = r14
            r1 = r4
            r2 = r38
            r0.setRequestProperty(r1, r2)
        L_0x00b7:
            java.lang.String r4 = "ku6"
            java.lang.String r5 = com.kandian.common.StringUtil.getDomain(r38)
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x00ca
            java.lang.String r4 = "User-Agent"
            java.lang.String r5 = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; zh-cn) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8"
            r14.setRequestProperty(r4, r5)
        L_0x00ca:
            r30 = -1
            boolean r4 = r39.exists()
            if (r4 == 0) goto L_0x0127
            long r4 = r39.length()
            r6 = 0
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x0127
            if (r40 == 0) goto L_0x0127
            long r4 = r39.length()
            r6 = 1
            long r30 = r4 - r6
            java.lang.String r4 = "Range"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "bytes="
            r5.<init>(r6)
            r0 = r5
            r1 = r30
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r6 = "-"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r14.setRequestProperty(r4, r5)
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Range: "
            r5.<init>(r6)
            long r6 = r39.length()
            r8 = 1
            long r6 = r6 - r8
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "-"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
        L_0x0127:
            r14.connect()
            int r4 = r14.getContentLength()
            r0 = r4
            long r0 = (long) r0
            r16 = r0
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r37)
            r5.<init>(r6)
            java.lang.String r6 = " Content type is "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r14.getContentType()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Content length is "
            r5.<init>(r6)
            r0 = r5
            r1 = r16
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
            r32 = 0
            r4 = 0
            int r4 = (r16 > r4 ? 1 : (r16 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x01b8
            r4 = 0
            int r4 = (r30 > r4 ? 1 : (r30 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x01b0
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "contentLength is -1 during break-point retransmission of"
            r5.<init>(r6)
            r0 = r5
            r1 = r37
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r6 = ", trying to re-download."
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
            r8 = 0
            r4 = r36
            r5 = r37
            r6 = r38
            r7 = r39
            r9 = r41
            r10 = r42
            com.kandian.common.AsyncVideoLoader$CacheFile r4 = r4.saveVideoFromUrl(r5, r6, r7, r8, r9, r10)
            goto L_0x003c
        L_0x01b0:
            java.io.IOException r4 = new java.io.IOException
            java.lang.String r5 = "contentLength is -1"
            r4.<init>(r5)
            throw r4
        L_0x01b8:
            java.io.InputStream r32 = r14.getInputStream()     // Catch:{ FileNotFoundException -> 0x01c6 }
            if (r32 != 0) goto L_0x0201
            java.io.IOException r4 = new java.io.IOException
            java.lang.String r5 = "stream is null"
            r4.<init>(r5)
            throw r4
        L_0x01c6:
            r20 = move-exception
            r4 = 0
            int r4 = (r30 > r4 ? 1 : (r30 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x0200
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "break-point retransmission fails"
            r5.<init>(r6)
            r0 = r5
            r1 = r37
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r6 = ", trying to re-download."
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
            r8 = 0
            r4 = r36
            r5 = r37
            r6 = r38
            r7 = r39
            r9 = r41
            r10 = r42
            com.kandian.common.AsyncVideoLoader$CacheFile r4 = r4.saveVideoFromUrl(r5, r6, r7, r8, r9, r10)
            goto L_0x003c
        L_0x0200:
            throw r20
        L_0x0201:
            com.kandian.common.AsyncVideoLoader$CacheFile r13 = new com.kandian.common.AsyncVideoLoader$CacheFile
            r0 = r13
            r1 = r36
            r0.<init>()
            java.lang.String r4 = r39.getAbsolutePath()
            r13.cachePath = r4
            r4 = 0
            int r4 = (r30 > r4 ? 1 : (r30 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0275
            long r4 = r16 + r30
            r13.fileSize = r4
        L_0x0219:
            java.io.RandomAccessFile r29 = new java.io.RandomAccessFile
            java.lang.String r4 = "rw"
            r0 = r29
            r1 = r39
            r2 = r4
            r0.<init>(r1, r2)
            r4 = 0
            int r4 = (r30 > r4 ? 1 : (r30 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x022e
            r29.seek(r30)
        L_0x022e:
            r4 = 2048(0x800, float:2.87E-42)
            byte[] r12 = new byte[r4]
            r33 = 0
            r25 = 0
            long r23 = java.lang.System.currentTimeMillis()
            r18 = 0
            r22 = 0
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Started reading bytes for "
            r5.<init>(r6)
            r0 = r5
            r1 = r37
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
        L_0x0258:
            r0 = r32
            r1 = r12
            int r28 = r0.read(r1)
            if (r28 >= 0) goto L_0x027b
        L_0x0261:
            r29.close()     // Catch:{ IOException -> 0x0354 }
            r32.close()     // Catch:{ IOException -> 0x0354 }
        L_0x0267:
            boolean r4 = r41.getStopThreadFlag()
            if (r4 == 0) goto L_0x0378
            java.io.IOException r4 = new java.io.IOException
            java.lang.String r5 = "stop by outside command."
            r4.<init>(r5)
            throw r4
        L_0x0275:
            r0 = r16
            r2 = r13
            r2.fileSize = r0
            goto L_0x0219
        L_0x027b:
            r4 = 0
            r0 = r29
            r1 = r12
            r2 = r4
            r3 = r28
            r0.write(r1, r2, r3)
            r0 = r28
            long r0 = (long) r0
            r4 = r0
            long r33 = r33 + r4
            if (r22 != 0) goto L_0x02b3
            android.os.Message r27 = android.os.Message.obtain()
            r4 = 0
            r0 = r4
            r1 = r27
            r1.what = r0
            r0 = r13
            r1 = r27
            r1.obj = r0
            java.lang.String r4 = r39.getAbsolutePath()
            r0 = r36
            r1 = r27
            r2 = r4
            r3 = r37
            r0.handleMessage(r1, r2, r3)
            r22 = 1
        L_0x02ac:
            boolean r4 = r41.getStopThreadFlag()
            if (r4 == 0) goto L_0x0258
            goto L_0x0261
        L_0x02b3:
            if (r22 == 0) goto L_0x02ac
            long r18 = java.lang.System.currentTimeMillis()
            r4 = 5000(0x1388, double:2.4703E-320)
            long r4 = r4 + r23
            int r4 = (r18 > r4 ? 1 : (r18 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x02ac
            r23 = r18
            r25 = r33
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r37)
            r5.<init>(r6)
            java.lang.String r6 = " saved "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r5
            r1 = r33
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r6 = "bytes"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.kandian.common.Log.v(r4, r5)
            android.os.Message r27 = android.os.Message.obtain()
            r4 = 3
            r0 = r4
            r1 = r27
            r1.what = r0
            java.lang.Long r4 = new java.lang.Long
            long r5 = r30 + r33
            r4.<init>(r5)
            r0 = r4
            r1 = r27
            r1.obj = r0
            java.lang.String r4 = r39.getAbsolutePath()
            r0 = r36
            r1 = r27
            r2 = r4
            r3 = r37
            r0.handleMessage(r1, r2, r3)
            boolean r4 = com.kandian.common.PreferenceSetting.getDownloadByWifi(r42)
            if (r4 == 0) goto L_0x02ac
            java.lang.String r4 = "connectivity"
            r0 = r42
            r1 = r4
            java.lang.Object r15 = r0.getSystemService(r1)
            android.net.ConnectivityManager r15 = (android.net.ConnectivityManager) r15
            android.net.NetworkInfo r11 = r15.getActiveNetworkInfo()
            if (r11 == 0) goto L_0x032f
            int r4 = r11.getType()
            r5 = 1
            if (r4 == r5) goto L_0x02ac
        L_0x032f:
            r4 = 5
            r0 = r4
            r1 = r27
            r1.what = r0
            r4 = 2131099828(0x7f0600b4, float:1.781202E38)
            r0 = r42
            r1 = r4
            java.lang.String r4 = r0.getString(r1)
            r0 = r4
            r1 = r27
            r1.obj = r0
            java.lang.String r4 = r39.getAbsolutePath()
            r0 = r36
            r1 = r27
            r2 = r4
            r3 = r37
            r0.handleMessage(r1, r2, r3)
            goto L_0x0261
        L_0x0354:
            r4 = move-exception
            r21 = r4
            r0 = r36
            java.lang.String r0 = r0.TAG
            r4 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "error: "
            r5.<init>(r6)
            java.lang.String r6 = r21.getMessage()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r0 = r4
            r1 = r5
            r2 = r21
            com.kandian.common.Log.e(r0, r1, r2)
            goto L_0x0267
        L_0x0378:
            r4 = r13
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.AsyncVideoLoader.saveVideoFromUrl(java.lang.String, java.lang.String, java.io.File, boolean, com.kandian.common.AsyncVideoLoader$MyThread, android.app.Application):com.kandian.common.AsyncVideoLoader$CacheFile");
    }

    public void stopLoading(String targetFilePath) {
        Log.v(this.TAG, "stopping thread for " + targetFilePath);
        MyThread t = this.activeThreadMap.get(targetFilePath);
        if (t != null) {
            t.stopThreadFlag();
            t.interrupt();
            return;
        }
        Log.v(this.TAG, "trying to stop un-existing thread for " + targetFilePath);
    }

    public void registerVideoCallback(String targetFilePath, VideoCallback callback) {
        this.callbackMap.put(targetFilePath, callback);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public CacheFile updateVideoCallback(String targetFilePath, VideoCallback callback) {
        MyThread t = this.activeThreadMap.get(targetFilePath);
        if (t == null || !t.isAlive()) {
            Log.v(this.TAG, "no downloading thread for " + targetFilePath);
            return null;
        }
        Log.v(this.TAG, "found downloading thread for " + targetFilePath);
        this.callbackMap.put(targetFilePath, callback);
        return this.videoCache.get(targetFilePath);
    }

    public void removeVideoCallback(String filePath, VideoCallback callback) {
        this.callbackMap.remove(filePath);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public void handleMessage(Message message, String filePath, String videoUrl) {
        VideoCallback callback = this.callbackMap.get(filePath);
        if (callback != null) {
            switch (message.what) {
                case 0:
                    CacheFile c = (CacheFile) message.obj;
                    this.videoCache.put(c.cachePath, c);
                    callback.videoInitialized(c, videoUrl);
                    return;
                case 1:
                    this.activeThreadMap.remove(filePath);
                    callback.videoLoaded((CacheFile) message.obj, videoUrl);
                    return;
                case 2:
                    this.activeThreadMap.remove(filePath);
                    callback.videoLoadingFailed((Exception) message.obj, videoUrl, filePath);
                    return;
                case 3:
                    callback.videoBufferProgressChanged((Long) message.obj, videoUrl);
                    return;
                case 4:
                default:
                    return;
                case 5:
                    callback.networkStatus((String) message.obj);
                    return;
            }
        } else {
            Log.v(this.TAG, String.valueOf(videoUrl) + " callback is null");
        }
    }
}
