package com.kandian.common;

import android.app.Application;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class SiteConfigs {
    static String TAG = "SiteConfigs";
    ArrayList<Site> flvSiteConfigs = new ArrayList<>();
    ArrayList<Site> playableSiteConfigs = new ArrayList<>();
    ArrayList<Site> siteConfigs = new ArrayList<>();
    ArrayList<Site> tsSiteConfigs = new ArrayList<>();
    ArrayList<Site> webpageSiteConfigs = new ArrayList<>();

    public class Site {
        String code = null;
        String fileType = null;
        String name = null;
        int referer = 0;
        ArrayList<String> urlRules = new ArrayList<>();

        public Site() {
        }

        public String getCode() {
            return this.code;
        }

        public void setCode(String code2) {
            this.code = code2;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getFileType() {
            return this.fileType;
        }

        public void setFileType(String fileType2) {
            this.fileType = fileType2;
        }
    }

    public void setSiteConfigs(ArrayList<Site> configs) {
        this.playableSiteConfigs.clear();
        this.siteConfigs = configs;
        this.flvSiteConfigs.clear();
        this.webpageSiteConfigs.clear();
        for (int i = 0; i < this.siteConfigs.size(); i++) {
            Site site = this.siteConfigs.get(i);
            if (site.fileType.contains("mp4")) {
                this.playableSiteConfigs.add(site);
            }
            if (site.fileType.contains("flv")) {
                this.flvSiteConfigs.add(site);
            }
            if (site.fileType.contains("ts")) {
                this.tsSiteConfigs.add(site);
            }
            if (site.fileType.contains("webpage")) {
                this.webpageSiteConfigs.add(site);
            }
        }
    }

    public String cdataText(String t) {
        return "<![CDATA[" + t + "]]>";
    }

    public String siteConfigsToXml() {
        String result = String.valueOf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n") + "<DOCUMENT><sites>";
        for (int i = 0; i < this.siteConfigs.size(); i++) {
            Site site = this.siteConfigs.get(i);
            String result2 = String.valueOf(String.valueOf(String.valueOf(String.valueOf(result) + "<site>") + "<code>" + cdataText(site.code) + "</code>") + "<filetype>" + cdataText(site.fileType) + "</filetype>") + "<urlrules>";
            for (int j = 0; j < site.urlRules.size(); j++) {
                result2 = String.valueOf(result2) + "<match>" + cdataText(site.urlRules.get(j)) + "</match>";
            }
            result = String.valueOf(String.valueOf(result2) + "</urlrules>") + "</site>";
        }
        return String.valueOf(result) + "</sites></DOCUMENT>";
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0011 A[EDGE_INSN: B:24:0x0011->B:3:0x0011 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getPlaySourcesQueryString(android.app.Application r10) {
        /*
            r9 = this;
            r8 = -1
            java.lang.String r3 = "fq="
            r1 = 0
            java.lang.String r5 = com.kandian.common.PreferenceSetting.getSystemConfigFilterSites(r10)
            r2 = 0
        L_0x0009:
            java.util.ArrayList<com.kandian.common.SiteConfigs$Site> r6 = r9.playableSiteConfigs
            int r6 = r6.size()
            if (r2 < r6) goto L_0x0026
        L_0x0011:
            java.lang.String r6 = com.kandian.common.SiteConfigs.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "playable sources string is "
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r3)
            java.lang.String r7 = r7.toString()
            com.kandian.common.Log.v(r6, r7)
            return r3
        L_0x0026:
            java.util.ArrayList<com.kandian.common.SiteConfigs$Site> r6 = r9.playableSiteConfigs
            java.lang.Object r4 = r6.get(r2)
            com.kandian.common.SiteConfigs$Site r4 = (com.kandian.common.SiteConfigs.Site) r4
            java.lang.String r6 = r4.getFileType()
            if (r6 == 0) goto L_0x005d
            java.lang.String r6 = r4.getFileType()
            java.lang.String r7 = ","
            int r6 = r6.indexOf(r7)
            if (r6 <= r8) goto L_0x005d
            java.lang.String r6 = r4.getFileType()
            java.lang.String r7 = ","
            java.lang.String[] r0 = r6.split(r7)
            if (r0 == 0) goto L_0x0084
            int r6 = r0.length
            if (r6 <= 0) goto L_0x0084
            java.lang.String r6 = r4.getCode()
            int r6 = com.kandian.common.StringUtil.numberOfStr(r5, r6)
            int r7 = r0.length
            if (r6 < r7) goto L_0x0084
        L_0x005a:
            int r2 = r2 + 1
            goto L_0x0009
        L_0x005d:
            if (r5 == 0) goto L_0x0084
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = r4.getCode()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r6.<init>(r7)
            java.lang.String r7 = "_"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.getFileType()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            int r6 = r5.indexOf(r6)
            if (r6 > r8) goto L_0x005a
        L_0x0084:
            java.lang.String r6 = r4.code
            java.lang.String r7 = "openv"
            boolean r6 = r6.contains(r7)
            if (r6 != 0) goto L_0x0011
            if (r1 == 0) goto L_0x00b0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r6.<init>(r7)
            java.lang.String r7 = "+OR+resourcesCode%3A%28"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.code
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "%29"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r6.toString()
            goto L_0x005a
        L_0x00b0:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r6.<init>(r7)
            java.lang.String r7 = "resourcesCode%3A%28"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.code
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "%29"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r6.toString()
            r1 = 1
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.SiteConfigs.getPlaySourcesQueryString(android.app.Application):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0011 A[EDGE_INSN: B:24:0x0011->B:3:0x0011 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getPlaySourcesQueryStringForShortVideo(android.app.Application r10) {
        /*
            r9 = this;
            r8 = -1
            java.lang.String r3 = "fq="
            r1 = 0
            java.lang.String r5 = com.kandian.common.PreferenceSetting.getSystemConfigFilterSites(r10)
            r2 = 0
        L_0x0009:
            java.util.ArrayList<com.kandian.common.SiteConfigs$Site> r6 = r9.playableSiteConfigs
            int r6 = r6.size()
            if (r2 < r6) goto L_0x0026
        L_0x0011:
            java.lang.String r6 = com.kandian.common.SiteConfigs.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "playable sources string is "
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r3)
            java.lang.String r7 = r7.toString()
            com.kandian.common.Log.v(r6, r7)
            return r3
        L_0x0026:
            java.util.ArrayList<com.kandian.common.SiteConfigs$Site> r6 = r9.playableSiteConfigs
            java.lang.Object r4 = r6.get(r2)
            com.kandian.common.SiteConfigs$Site r4 = (com.kandian.common.SiteConfigs.Site) r4
            java.lang.String r6 = r4.getFileType()
            if (r6 == 0) goto L_0x005d
            java.lang.String r6 = r4.getFileType()
            java.lang.String r7 = ","
            int r6 = r6.indexOf(r7)
            if (r6 <= r8) goto L_0x005d
            java.lang.String r6 = r4.getFileType()
            java.lang.String r7 = ","
            java.lang.String[] r0 = r6.split(r7)
            if (r0 == 0) goto L_0x0084
            int r6 = r0.length
            if (r6 <= 0) goto L_0x0084
            java.lang.String r6 = r4.getCode()
            int r6 = com.kandian.common.StringUtil.numberOfStr(r5, r6)
            int r7 = r0.length
            if (r6 < r7) goto L_0x0084
        L_0x005a:
            int r2 = r2 + 1
            goto L_0x0009
        L_0x005d:
            if (r5 == 0) goto L_0x0084
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = r4.getCode()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r6.<init>(r7)
            java.lang.String r7 = "_"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.getFileType()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            int r6 = r5.indexOf(r6)
            if (r6 > r8) goto L_0x005a
        L_0x0084:
            java.lang.String r6 = r4.code
            java.lang.String r7 = "openv"
            boolean r6 = r6.contains(r7)
            if (r6 != 0) goto L_0x0011
            if (r1 == 0) goto L_0x00b0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r6.<init>(r7)
            java.lang.String r7 = "+OR+reservedstr2%3A%28"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.code
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "%29"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r6.toString()
            goto L_0x005a
        L_0x00b0:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r6.<init>(r7)
            java.lang.String r7 = "reservedstr2%3A%28"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.code
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "%29"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r6.toString()
            r1 = 1
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.SiteConfigs.getPlaySourcesQueryStringForShortVideo(android.app.Application):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getDownloadSourcesQueryString(android.app.Application r10) {
        /*
            r9 = this;
            r8 = -1
            java.lang.String r3 = "fq="
            r1 = 0
            java.lang.String r5 = com.kandian.common.PreferenceSetting.getSystemConfigFilterSites(r10)
            r2 = 0
        L_0x0009:
            java.util.ArrayList<com.kandian.common.SiteConfigs$Site> r6 = r9.siteConfigs
            int r6 = r6.size()
            if (r2 < r6) goto L_0x0012
            return r3
        L_0x0012:
            java.util.ArrayList<com.kandian.common.SiteConfigs$Site> r6 = r9.siteConfigs
            java.lang.Object r4 = r6.get(r2)
            com.kandian.common.SiteConfigs$Site r4 = (com.kandian.common.SiteConfigs.Site) r4
            java.lang.String r6 = r4.getFileType()
            if (r6 == 0) goto L_0x0049
            java.lang.String r6 = r4.getFileType()
            java.lang.String r7 = ","
            int r6 = r6.indexOf(r7)
            if (r6 <= r8) goto L_0x0049
            java.lang.String r6 = r4.getFileType()
            java.lang.String r7 = ","
            java.lang.String[] r0 = r6.split(r7)
            if (r0 == 0) goto L_0x0070
            int r6 = r0.length
            if (r6 <= 0) goto L_0x0070
            java.lang.String r6 = r4.getCode()
            int r6 = com.kandian.common.StringUtil.numberOfStr(r5, r6)
            int r7 = r0.length
            if (r6 < r7) goto L_0x0070
        L_0x0046:
            int r2 = r2 + 1
            goto L_0x0009
        L_0x0049:
            if (r5 == 0) goto L_0x0070
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = r4.getCode()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r6.<init>(r7)
            java.lang.String r7 = "_"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.getFileType()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            int r6 = r5.indexOf(r6)
            if (r6 > r8) goto L_0x0046
        L_0x0070:
            if (r1 == 0) goto L_0x0092
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r6.<init>(r7)
            java.lang.String r7 = "+OR+resourcesCode%3A%28"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.code
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "%29"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r6.toString()
            goto L_0x0046
        L_0x0092:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r3)
            r6.<init>(r7)
            java.lang.String r7 = "resourcesCode%3A%28"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = r4.code
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "%29"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r6.toString()
            r1 = 1
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.common.SiteConfigs.getDownloadSourcesQueryString(android.app.Application):java.lang.String");
    }

    public String getWebpageSourcesQueryString(Application app) {
        String result = "fq=";
        boolean foundOne = false;
        for (int i = 0; i < this.webpageSiteConfigs.size(); i++) {
            Site site = this.webpageSiteConfigs.get(i);
            if (foundOne) {
                result = String.valueOf(result) + "+OR+resourcesCode%3A%28" + site.code + "%29";
            } else {
                result = String.valueOf(result) + "resourcesCode%3A%28" + site.code + "%29";
                foundOne = true;
            }
        }
        return result;
    }

    public String getAssetKeys(String url) {
        String result = "fq=";
        try {
            Log.v(TAG, url);
            String jsonStr = StringUtil.getStringFromURL(url);
            if (jsonStr == null || jsonStr.trim().length() <= 0) {
                return result;
            }
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            Log.v(TAG, new StringBuilder(String.valueOf(jsonArray.length())).toString());
            boolean foundOne = false;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = (JSONObject) jsonArray.get(i);
                if (foundOne) {
                    result = String.valueOf(result) + "+OR+key%3A" + obj.getString("assetkey");
                } else {
                    result = String.valueOf(result) + "(key%3A" + obj.getString("assetkey");
                    foundOne = true;
                }
                if (i == jsonArray.length() - 1) {
                    result = String.valueOf(result) + ")";
                }
            }
            return String.valueOf(result) + "&totalCount=" + jsonObject.getInt("totalCount");
        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
            return result;
        }
    }

    public boolean isPlayableSite(String url, String siteFilter) {
        return isMp4Site(url, siteFilter);
    }

    public boolean isMp4Site(String url, String siteFilter) {
        for (int i = 0; i < this.playableSiteConfigs.size(); i++) {
            Site site = this.playableSiteConfigs.get(i);
            if (siteFilter == null || siteFilter.indexOf(String.valueOf(site.getCode()) + "_mp4") <= -1) {
                for (int j = 0; j < site.urlRules.size(); j++) {
                    if (url.startsWith(site.urlRules.get(j))) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    public boolean isMp4Site(PlayUrl playUrl, String siteFilter) {
        for (int i = 0; i < this.playableSiteConfigs.size(); i++) {
            Site site = this.playableSiteConfigs.get(i);
            if ((siteFilter == null || siteFilter.indexOf(String.valueOf(site.getCode()) + "_mp4") <= -1) && playUrl.getResourcecode().equals(site.getCode())) {
                String url = playUrl.getUrl();
                for (int j = 0; j < site.urlRules.size(); j++) {
                    if (url.startsWith(site.urlRules.get(j))) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    public boolean isFlvSite(String url, String siteFilter) {
        for (int i = 0; i < this.flvSiteConfigs.size(); i++) {
            Site site = this.flvSiteConfigs.get(i);
            if (siteFilter == null || siteFilter.indexOf(String.valueOf(site.getCode()) + "_flv") <= -1) {
                for (int j = 0; j < site.urlRules.size(); j++) {
                    if (url.startsWith(site.urlRules.get(j))) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    public boolean isTsSite(String url, String siteFilter) {
        for (int i = 0; i < this.tsSiteConfigs.size(); i++) {
            Site site = this.tsSiteConfigs.get(i);
            for (int j = 0; j < site.urlRules.size(); j++) {
                if (url.startsWith(site.urlRules.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isWebpageSite(String url, String siteFilter) {
        for (int i = 0; i < this.webpageSiteConfigs.size(); i++) {
            Site site = this.webpageSiteConfigs.get(i);
            for (int j = 0; j < site.urlRules.size(); j++) {
                if (url.startsWith(site.urlRules.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isDownloadableSite(String url, String siteFilter) {
        for (int i = 0; i < this.siteConfigs.size(); i++) {
            Site site = this.siteConfigs.get(i);
            for (int j = 0; j < site.urlRules.size(); j++) {
                if (url.startsWith(site.urlRules.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public int isReferer(String url) {
        for (int i = 0; i < this.siteConfigs.size(); i++) {
            Site site = this.siteConfigs.get(i);
            for (int j = 0; j < site.urlRules.size(); j++) {
                if (url.startsWith(site.urlRules.get(j))) {
                    return site.referer;
                }
            }
        }
        return 0;
    }

    public ArrayList<Site> getSiteConfigs() {
        return this.siteConfigs;
    }
}
