package com.kandian.common;

import android.app.Activity;
import android.app.ProgressDialog;

public class LoadDialog {
    private Activity activity;
    private ProgressDialog loadingdialog;
    private String title;

    public LoadDialog(String title2, Activity activity2) {
        this.title = title2;
        this.activity = activity2;
    }

    public void showProgressDialog() {
        this.loadingdialog = new ProgressDialog(this.activity);
        this.loadingdialog.setMessage(this.title);
        this.loadingdialog.setIndeterminate(true);
        this.loadingdialog.setCancelable(true);
        this.loadingdialog.show();
    }

    public void closeProgressDialog() {
        if (this.loadingdialog != null) {
            this.loadingdialog.dismiss();
        }
    }
}
