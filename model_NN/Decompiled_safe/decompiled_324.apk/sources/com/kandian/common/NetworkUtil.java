package com.kandian.common;

import android.app.Application;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
    public static String TAG = "NetworkUtil";

    public static boolean checkNetwork(Application app) {
        NetworkInfo networkInfo = ((ConnectivityManager) app.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isConnected();
        }
        return false;
    }
}
