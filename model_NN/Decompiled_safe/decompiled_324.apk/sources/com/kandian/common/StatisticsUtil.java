package com.kandian.common;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.kandian.cartoonapp.R;
import org.apache.commons.httpclient.cookie.CookiePolicy;

public class StatisticsUtil {
    public static String TAG = "StatisticsUtil";

    public static void updateCount(final Context context) {
        if (context instanceof Activity) {
            new Thread(new Runnable() {
                public void run() {
                    String countName = CookiePolicy.DEFAULT;
                    try {
                        countName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
                    } catch (Exception e) {
                    }
                    final String url = StringUtil.replace(context.getString(R.string.countUrl), "{packageName}", countName);
                    final Context context = context;
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            WebView wv = (WebView) LayoutInflater.from(context).inflate((int) R.layout.statistics_count, (ViewGroup) null).findViewById(R.id.wvcount);
                            wv.setWebViewClient(new WebViewClient() {
                                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                    Log.v(StatisticsUtil.TAG, "loading " + url);
                                    return false;
                                }
                            });
                            WebSettings webSettings = wv.getSettings();
                            webSettings.setJavaScriptEnabled(true);
                            webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
                            wv.loadUrl(url);
                            Log.v(StatisticsUtil.TAG, "send " + url);
                        }
                    });
                }
            }).start();
        }
    }
}
