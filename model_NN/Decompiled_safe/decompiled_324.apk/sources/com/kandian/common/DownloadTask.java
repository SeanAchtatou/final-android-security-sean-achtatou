package com.kandian.common;

import android.app.Application;
import com.kandian.common.AsyncVideoLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class DownloadTask implements AsyncVideoLoader.VideoCallback {
    private static final int MAX_RETRANSMIT_TIMES = 30;
    private static final int RETRANSMIT_INTERVAL = 15000;
    public static final int STATUS_DOWNLOADED = 2;
    public static final int STATUS_DOWNLOADING = 1;
    public static final int STATUS_FAILED = 3;
    public static final int STATUS_TO_DOWNLOAD = 0;
    /* access modifiers changed from: private */
    public static String TAG = "DownloadTask";
    public static final int TASK_STATUS_FINISHED = 4;
    public static final int TASK_STATUS_PAUSED = 3;
    public static final int TASK_STATUS_PREPARING = 0;
    public static final int TASK_STATUS_READY = 1;
    public static final int TASK_STATUS_RUNNING = 2;
    private ArrayList<String> activeTmpFiles = new ArrayList<>();
    Application app = null;
    private AsyncVideoLoader asyncVideoLoader = AsyncVideoLoader.instance();
    AsyncVideoLoader.CacheFile currentCacheFile = null;
    long currentCacheFileBufferSize = 0;
    long currentDownloadSpeed = -1;
    int currentPosition = -1;
    long currentTime = 0;
    /* access modifiers changed from: private */
    public int currentTransmitCount = 0;
    private ArrayList<String> downloadedFiles = new ArrayList<>();
    private ArrayList<String> extractedVideoUrls = null;
    private String mediaFileDir = null;
    private int numOfChapters = 0;
    private int numOfDownloaded = 0;
    long oldCacheFileBufferSize = 0;
    long previousDownloadSpeed = 1;
    long previousTime = 0;
    private String refererPage = null;
    private HashMap<String, Integer> statusMap = new HashMap<>();
    private String suffix = "";
    TaskCallback taskCallback;
    private long taskId = 0;
    private int taskStatus = 3;
    private String videoName = null;
    private int videoType = 0;

    public interface TaskCallback {
        void networkStauts();

        void taskFailed();

        void taskFinished();

        void taskProgressUpdated();

        void taskReady();
    }

    public int getCurrentTransmitCount() {
        return this.currentTransmitCount;
    }

    public long getCurrentDownloadSpeed() {
        return this.currentDownloadSpeed;
    }

    public AsyncVideoLoader.CacheFile getCurrentCacheFile() {
        return this.currentCacheFile;
    }

    public void setCurrentCacheFile(AsyncVideoLoader.CacheFile currentCacheFile2) {
        this.currentCacheFile = currentCacheFile2;
        this.currentCacheFileBufferSize = 0;
        this.oldCacheFileBufferSize = 0;
    }

    public String getKey() {
        return String.valueOf(this.refererPage) + this.videoType;
    }

    public DownloadTask(String vName, int vType, String rPage, Application app2) {
        initialize(vName, vType, rPage, 0, 0, "", app2);
    }

    public DownloadTask(String vName, int vType, String rPage, int nChapters, long taskId2, String mediaFileDir2, Application app2) {
        initialize(vName, vType, rPage, nChapters, taskId2, mediaFileDir2, app2);
    }

    /* access modifiers changed from: protected */
    public void initialize(String vName, int vType, String rPage, int nChapters, long tId, String vFileDir, Application app2) {
        this.videoName = vName;
        this.videoType = vType;
        this.refererPage = rPage;
        this.app = app2;
        Log.v(TAG, "tId = " + tId);
        Log.v(TAG, "vFileDir = " + vFileDir);
        if (tId > 0) {
            this.taskId = tId;
            if (vFileDir == null || vFileDir.trim().length() == 0) {
                setMediaFileDir(getDefaultDownloadDir());
            } else {
                setMediaFileDir(vFileDir);
            }
        } else {
            this.taskId = System.currentTimeMillis();
            setMediaFileDir(getDownloadDir());
        }
        if (nChapters > 0) {
            this.numOfChapters = nChapters;
            initializeDownloadedFiles();
        }
        Log.v(TAG, "mediaFileDir = " + this.mediaFileDir);
    }

    public void initializeDownloadedFiles() {
        new File(getMediaFileDir()).mkdirs();
        switch (this.videoType) {
            case 1:
                this.suffix = ".flv";
                break;
            case 2:
                this.suffix = ".mp4";
                break;
            case 3:
                this.suffix = ".ts";
                break;
        }
        boolean finished = true;
        Log.v(TAG, "total " + this.numOfChapters + " chapters.");
        this.numOfDownloaded = 0;
        this.downloadedFiles.clear();
        for (int i = 0; i < this.numOfChapters; i++) {
            String filePath = String.valueOf(getMediaFileDir()) + "chapter" + (i + 1) + this.suffix;
            this.downloadedFiles.add(filePath);
            Log.v(TAG, "-------filePath = " + filePath);
            if (new File(filePath).exists()) {
                this.statusMap.put(getTmpDownloadFile(filePath), new Integer(2));
            } else {
                this.statusMap.put(getTmpDownloadFile(filePath), new Integer(0));
            }
            if (new File(filePath).exists()) {
                this.numOfDownloaded++;
            } else {
                finished = false;
            }
        }
        if (finished && this.numOfChapters > 0) {
            this.taskStatus = 4;
        }
    }

    public String getDownloadDir() {
        if (this.mediaFileDir != null) {
            return this.mediaFileDir;
        }
        return String.valueOf(PreferenceSetting.getMediaFileDir()) + this.taskId + CookieSpec.PATH_DELIM;
    }

    public String getDefaultDownloadDir() {
        return String.valueOf(PreferenceSetting.getDownloadDir()) + this.taskId + CookieSpec.PATH_DELIM;
    }

    public String getTmpDownloadFile(String targetFile) {
        return String.valueOf(targetFile) + ".tmp";
    }

    public Properties generateProperties() {
        Properties pro = new Properties();
        pro.setProperty("videoName", getVideoName());
        pro.setProperty("videoType", new StringBuilder(String.valueOf(getVideoType())).toString());
        pro.setProperty("refererPage", getRefererPage());
        pro.setProperty("taskId", new StringBuilder(String.valueOf(getTaskId())).toString());
        pro.setProperty("numOfChapters", new StringBuilder(String.valueOf(getNumOfChapters())).toString());
        pro.setProperty("mediaFileDir", new StringBuilder(String.valueOf(getDownloadDir())).toString());
        return pro;
    }

    public static DownloadTask initializeFromFile(File file, Application app2) {
        DownloadTask result;
        if (!file.exists()) {
            return null;
        }
        Properties pro = new Properties();
        try {
            FileInputStream in = new FileInputStream(file);
            pro.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String mFileDir = (String) pro.get("mediaFileDir");
            if (mFileDir == null || mFileDir.trim().length() == 0) {
                mFileDir = String.valueOf(file.getParent()) + CookieSpec.PATH_DELIM + ((String) pro.get("taskId")) + CookieSpec.PATH_DELIM;
            }
            result = new DownloadTask((String) pro.get("videoName"), Integer.parseInt((String) pro.get("videoType")), (String) pro.get("refererPage"), Integer.parseInt((String) pro.get("numOfChapters")), Long.parseLong((String) pro.get("taskId")), mFileDir, app2);
        } catch (Exception e2) {
            result = null;
        }
        return result;
    }

    private String normalizeFileName(String toNormalize) {
        if (toNormalize != null) {
            return toNormalize.replace(CookieSpec.PATH_DELIM, "");
        }
        return null;
    }

    public String getVideoName() {
        return this.videoName;
    }

    public void setVideoName(String videoName2) {
        this.videoName = videoName2;
    }

    public int getVideoType() {
        return this.videoType;
    }

    public void setVideoType(int videoType2) {
        this.videoType = videoType2;
    }

    public String getRefererPage() {
        return this.refererPage;
    }

    public void setRefererPage(String refererPage2) {
        this.refererPage = refererPage2;
    }

    public int getNumOfChapters() {
        return this.numOfChapters;
    }

    public void setNumOfChapters(int numOfChapters2) {
        this.numOfChapters = numOfChapters2;
    }

    public ArrayList<String> getExtractedVideoUrls() {
        return this.extractedVideoUrls;
    }

    public void setExtractedVideoUrls(ArrayList<String> extractedVideoUrls2) {
        this.extractedVideoUrls = extractedVideoUrls2;
    }

    public ArrayList<String> getDownloadedFiles() {
        return this.downloadedFiles;
    }

    public void setDownloadedFiles(ArrayList<String> downloadedFiles2) {
        this.downloadedFiles = downloadedFiles2;
    }

    public String getMediaFileDir() {
        if (this.mediaFileDir == null) {
            return getDownloadDir();
        }
        return this.mediaFileDir;
    }

    public void setMediaFileDir(String mediaFileDir2) {
        this.mediaFileDir = mediaFileDir2;
    }

    public void startDownload() {
        startDownload(false);
    }

    public void startDownload(boolean restart) {
        if (this.taskStatus != 2 && this.taskStatus != 4) {
            this.currentTransmitCount = 0;
            _startDownload(restart);
        }
    }

    /* access modifiers changed from: private */
    public void _startDownload(boolean restart) {
        if (restart || this.taskStatus != 1) {
            this.taskStatus = 0;
            try {
                this.extractedVideoUrls = ObtainVideoService.getVideos(this.refererPage, this.app, this.videoType);
                if (this.extractedVideoUrls == null) {
                    taskFailed();
                    return;
                } else if (this.extractedVideoUrls.size() == this.numOfChapters && this.numOfChapters > 0) {
                    taskReady();
                } else if (this.extractedVideoUrls.size() <= 0 || this.numOfChapters != 0) {
                    taskFailed();
                    return;
                } else {
                    this.numOfChapters = this.extractedVideoUrls.size();
                    taskReady();
                }
            } catch (KSException e) {
                e.printStackTrace();
                taskFailed();
                return;
            }
        }
        if (this.taskStatus != 1 || this.numOfChapters <= 0) {
            Log.v(TAG, "we cannot start Task " + this.taskId);
            return;
        }
        int i = 0;
        while (i < this.numOfChapters) {
            if (new File(this.downloadedFiles.get(i)).exists()) {
                if (i == this.numOfChapters - 1) {
                    taskFinished();
                }
                i++;
            } else {
                this.taskStatus = 2;
                startDownload(i);
                return;
            }
        }
    }

    private void startDownload(int position) {
        if (this.extractedVideoUrls.size() == 0 || position >= this.numOfChapters) {
            taskFailed();
            return;
        }
        String urlToSave = this.extractedVideoUrls.get(position);
        String tmpFile = getTmpDownloadFile(this.downloadedFiles.get(position));
        if (!new File(this.downloadedFiles.get(position)).exists()) {
            this.activeTmpFiles.add(tmpFile);
            this.currentPosition = position;
            Log.v(TAG, "startDownload the " + position + "th at Round " + this.currentTransmitCount);
            this.asyncVideoLoader.loadVideo(urlToSave, this.refererPage, tmpFile, this, this.app);
        } else if (position < this.numOfChapters - 1) {
            startDownload(position + 1);
        } else {
            taskFinished();
        }
    }

    public void setNumOfDownloaded(int numOfDownloaded2) {
        this.numOfDownloaded = numOfDownloaded2;
    }

    public int getNumOfDownloaded() {
        return this.numOfDownloaded;
    }

    public void taskFinished() {
        this.taskStatus = 4;
        if (this.taskCallback != null) {
            this.taskCallback.taskFinished();
        }
    }

    public void taskFailed() {
        if (this.currentTransmitCount < 30) {
            this.currentTransmitCount++;
            new Thread() {
                public void run() {
                    Log.v(DownloadTask.TAG, "waiting to retransmit: " + DownloadTask.this.currentTransmitCount);
                    try {
                        Thread.sleep(15000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    DownloadTask.this._startDownload(true);
                }
            }.start();
            return;
        }
        this.taskStatus = 3;
        if (this.taskCallback != null) {
            this.taskCallback.taskFailed();
        }
    }

    public void taskReady() {
        initializeDownloadedFiles();
        this.taskStatus = 1;
        if (this.taskCallback != null) {
            this.taskCallback.taskReady();
        }
    }

    public void checkRunning() {
        AsyncVideoLoader.CacheFile cache;
        int i = 0;
        while (i < this.downloadedFiles.size()) {
            String filePath = this.downloadedFiles.get(i);
            if (new File(filePath).exists() || !new File(getTmpDownloadFile(filePath)).exists() || (cache = this.asyncVideoLoader.updateVideoCallback(getTmpDownloadFile(filePath), this)) == null) {
                i++;
            } else {
                Log.v(TAG, "updated videoCallback for " + filePath);
                this.currentPosition = i;
                this.currentCacheFile = cache;
                this.taskStatus = 2;
                taskProgressUpdated();
                return;
            }
        }
    }

    public void taskProgressUpdated() {
        Log.v(TAG, String.valueOf(this.videoName) + " progress updated, status:" + this.taskStatus);
        if (this.taskCallback != null) {
            this.taskCallback.taskProgressUpdated();
        }
    }

    public TaskCallback getTaskCallback() {
        return this.taskCallback;
    }

    public void setTaskCallback(TaskCallback taskCallback2) {
        this.taskCallback = taskCallback2;
    }

    public void setTaskStatus(int taskStatus2) {
        this.taskStatus = taskStatus2;
    }

    public int getTaskStatus() {
        return this.taskStatus;
    }

    public boolean isFinished() {
        if (this.taskStatus == 4) {
            return true;
        }
        return false;
    }

    public boolean isPaused() {
        if (this.taskStatus == 3) {
            return true;
        }
        return false;
    }

    public boolean clean() {
        boolean result = true;
        try {
            File dir = new File(getMediaFileDir());
            if (dir != null && dir.exists()) {
                for (File deleteFile : dir.listFiles()) {
                    if (deleteFile.exists() && !deleteFile.delete()) {
                        result = false;
                    }
                }
                dir.delete();
            }
        } catch (Exception e) {
        }
        return result;
    }

    public void stopDownload() {
        this.currentTransmitCount = 30;
        _stopDownload();
    }

    private void _stopDownload() {
        for (int i = 0; i < this.activeTmpFiles.size(); i++) {
            this.asyncVideoLoader.stopLoading(this.activeTmpFiles.get(i));
        }
        this.taskStatus = 3;
    }

    public void setTaskId(long taskId2) {
        this.taskId = taskId2;
    }

    public long getTaskId() {
        return this.taskId;
    }

    public int getCurrentFileProgress() {
        int progress;
        if (isFinished()) {
            return 100;
        }
        if (this.currentCacheFile == null) {
            return 0;
        }
        if (this.currentCacheFile.fileSize == 0) {
            return 0;
        }
        File f = new File(this.currentCacheFile.cachePath);
        if (f == null || !f.exists()) {
            progress = 0;
        } else {
            progress = new Long((100 * this.currentCacheFileBufferSize) / this.currentCacheFile.fileSize).intValue();
            Log.v(TAG, "current progress is " + this.currentCacheFileBufferSize + CookieSpec.PATH_DELIM + this.currentCacheFile.fileSize);
        }
        if (progress > 100) {
            Log.v(TAG, "progress is greater than 100 for " + f.getAbsolutePath());
            progress = 0;
        }
        return progress;
    }

    public int getOverallProgress() {
        int p;
        if (getNumOfChapters() < 1) {
            p = getCurrentFileProgress();
        } else {
            p = (int) (((double) ((int) ((((double) getNumOfDownloaded()) / ((double) getNumOfChapters())) * 100.0d))) + ((((double) getCurrentFileProgress()) * 1.0d) / ((double) getNumOfChapters())));
        }
        if (p > 100) {
            return 100;
        }
        if (p < 0) {
            return 0;
        }
        return p;
    }

    public void videoInitialized(AsyncVideoLoader.CacheFile cache, String videoUrl) {
        this.statusMap.put(cache.cachePath, new Integer(1));
        setCurrentCacheFile(cache);
        this.previousTime = System.currentTimeMillis();
        this.previousDownloadSpeed = 1;
    }

    public void videoLoaded(AsyncVideoLoader.CacheFile cache, String videoUrl) {
        File f = new File(cache.cachePath);
        if (!f.renameTo(new File(this.downloadedFiles.get(this.currentPosition)))) {
            Log.v("VideoCallback", "rename " + this.downloadedFiles.get(this.currentPosition) + " failed.");
            taskFailed();
        } else {
            this.statusMap.put(cache.cachePath, new Integer(2));
            this.numOfDownloaded++;
            if (this.currentPosition < this.numOfChapters - 1) {
                startDownload(this.currentPosition + 1);
                taskProgressUpdated();
            } else {
                taskFinished();
            }
        }
        this.activeTmpFiles.remove(f);
    }

    public void videoLoadingFailed(Exception e, String videoUrl, String filePath) {
        this.statusMap.put(filePath, new Integer(3));
        e.printStackTrace();
        this.activeTmpFiles.remove(filePath);
        taskFailed();
    }

    public void videoBufferProgressChanged(Long newBufferSize, String videoUrl) {
        this.taskStatus = 2;
        this.currentCacheFileBufferSize = newBufferSize.longValue();
        this.currentTime = System.currentTimeMillis();
        this.currentDownloadSpeed = (this.currentCacheFileBufferSize - this.oldCacheFileBufferSize) / (this.currentTime - this.previousTime);
        this.oldCacheFileBufferSize = this.currentCacheFileBufferSize;
        this.previousTime = this.currentTime;
        if (this.currentDownloadSpeed >= 0) {
            Log.v(TAG, "currentDownload speed is !" + this.currentDownloadSpeed);
            taskProgressUpdated();
        } else {
            Log.v(TAG, "speed is negative, weird!");
            taskFailed();
        }
        this.previousDownloadSpeed = this.currentDownloadSpeed;
    }

    public void networkStatus(String message) {
        if (this.taskCallback != null) {
            this.taskCallback.networkStauts();
        }
    }
}
