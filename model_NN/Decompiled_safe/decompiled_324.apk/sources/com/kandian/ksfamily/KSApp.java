package com.kandian.ksfamily;

import java.io.Serializable;

public class KSApp implements Serializable {
    public static final int KSAPP_STATUS_INSTALLED = 1;
    public static final int KSAPP_STATUS_NOT_INSTALLED = 0;
    public static final int KSAPP_STATUS_UPDATE = 2;
    private static final long serialVersionUID = 2407204643956679491L;
    private int appicon;
    private String applogo;
    private String appname;
    private String description;
    private String downloadpage;
    private String downloadurl;
    private String firmware;
    private long id;
    private String launcher;
    private String localAPKPath;
    private String packagename;
    private String partner;
    private String sinaweiboid;
    private String sinaweibokeyword;
    private int status;
    private String updateinfo;
    private int versioncode;
    private String versionname;

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getPackagename() {
        return this.packagename;
    }

    public void setPackagename(String packagename2) {
        this.packagename = packagename2;
    }

    public String getAppname() {
        return this.appname;
    }

    public void setAppname(String appname2) {
        this.appname = appname2;
    }

    public int getVersioncode() {
        return this.versioncode;
    }

    public void setVersioncode(int versioncode2) {
        this.versioncode = versioncode2;
    }

    public String getVersionname() {
        return this.versionname;
    }

    public void setVersionname(String versionname2) {
        this.versionname = versionname2;
    }

    public String getApplogo() {
        return this.applogo;
    }

    public void setApplogo(String applogo2) {
        this.applogo = applogo2;
    }

    public int getAppicon() {
        return this.appicon;
    }

    public void setAppicon(int appicon2) {
        this.appicon = appicon2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getUpdateinfo() {
        return this.updateinfo;
    }

    public void setUpdateinfo(String updateinfo2) {
        this.updateinfo = updateinfo2;
    }

    public String getDownloadurl() {
        return this.downloadurl;
    }

    public void setDownloadurl(String downloadurl2) {
        this.downloadurl = downloadurl2;
    }

    public String getSinaweiboid() {
        return this.sinaweiboid;
    }

    public void setSinaweiboid(String sinaweiboid2) {
        this.sinaweiboid = sinaweiboid2;
    }

    public String getSinaweibokeyword() {
        return this.sinaweibokeyword;
    }

    public void setSinaweibokeyword(String sinaweibokeyword2) {
        this.sinaweibokeyword = sinaweibokeyword2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public String getLocalAPKPath() {
        return this.localAPKPath;
    }

    public void setLocalAPKPath(String localAPKPath2) {
        this.localAPKPath = localAPKPath2;
    }

    public String getLauncher() {
        return this.launcher;
    }

    public void setLauncher(String launcher2) {
        this.launcher = launcher2;
    }

    public String getFirmware() {
        return this.firmware;
    }

    public void setFirmware(String firmware2) {
        this.firmware = firmware2;
    }

    public void setPartner(String partner2) {
        this.partner = partner2;
    }

    public String getPartner() {
        return this.partner;
    }

    public String getDownloadpage() {
        return this.downloadpage;
    }

    public void setDownloadpage(String downloadpage2) {
        this.downloadpage = downloadpage2;
    }
}
