package com.kandian.ksfamily;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.PreferenceSetting;
import com.kandian.common.StringUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class KSFamilyListActivity extends ListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "KSFamilyListActivity";
    private final int MSG_LIST = 0;
    private final int MSG_NETWORK_PROBLEM = 2;
    private final int MSG_REFRESH_LIST = 1;
    /* access modifiers changed from: private */
    public KSFamilyListActivity _context;
    /* access modifiers changed from: private */
    public AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
            KSFamilyListActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public View downLoadView;
    /* access modifiers changed from: private */
    public Dialog downloadDialog;
    /* access modifiers changed from: private */
    public long downloadThreadID = 0;
    /* access modifiers changed from: private */
    public Map<Long, Boolean> downloadThreadMap = new HashMap();
    /* access modifiers changed from: private */
    public int fileSize;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ProgressBar download_PB = (ProgressBar) KSFamilyListActivity.this.downLoadView.findViewById(R.id.download_pb);
            TextView dialogcontentTV = (TextView) KSFamilyListActivity.this.downLoadView.findViewById(R.id.dialogcontent);
            if (!Thread.currentThread().isInterrupted()) {
                switch (msg.what) {
                    case AuthScope.ANY_PORT /*-1*/:
                        dialogcontentTV.setText("下载失败，请检查您的存储状态和网络链接");
                        break;
                    case 0:
                        download_PB.setMax(KSFamilyListActivity.this.fileSize);
                    case 1:
                        int downLoadFileSize = ((Integer) msg.obj).intValue();
                        download_PB.setProgress(downLoadFileSize);
                        dialogcontentTV.setText("下载进度：" + ((downLoadFileSize * 100) / KSFamilyListActivity.this.fileSize) + "%");
                        break;
                    case 2:
                        dialogcontentTV.setText("文件下载完成");
                        if (KSFamilyListActivity.this.downloadDialog != null) {
                            KSFamilyListActivity.this.downloadDialog.dismiss();
                        }
                        KSFamilyListActivity.this.installAPK((String) msg.obj);
                        break;
                }
            }
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public Map<String, KSApp> installedKSApps = null;
    protected ArrayAdapter<CharSequence> mAdapter;
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            KSAppAdapter myAdatper = (KSAppAdapter) KSFamilyListActivity.this.getListAdapter();
            ProgressBar statusProgress = (ProgressBar) KSFamilyListActivity.this.findViewById(R.id.ksfamily_statusProgress);
            TextView ksfamilyModeCount_TT = (TextView) KSFamilyListActivity.this.findViewById(R.id.ksfamily_mode_count);
            switch (msg.what) {
                case 1:
                    KSFamilyListActivity.this.totalResultCount = 0;
                    myAdatper.clear();
                case 0:
                    List<KSApp> tmpAssetList = (List) msg.obj;
                    if (tmpAssetList != null) {
                        KSFamilyListActivity.this.totalResultCount = tmpAssetList.size();
                        for (int i = 0; i < KSFamilyListActivity.this.totalResultCount; i++) {
                            myAdatper.add((KSApp) tmpAssetList.get(i));
                        }
                    }
                    statusProgress.setVisibility(8);
                    ksfamilyModeCount_TT.setText(StringUtil.replace(KSFamilyListActivity.this.getString(R.string.ksfamily_mode_count), "{total}", new StringBuilder(String.valueOf(myAdatper.getCount())).toString()));
                    ((BaseAdapter) KSFamilyListActivity.this.getListAdapter()).notifyDataSetChanged();
                    break;
                case 2:
                    Toast.makeText(KSFamilyListActivity.this, KSFamilyListActivity.this.getString(R.string.network_problem), 0).show();
                    break;
            }
            if (myAdatper.getCount() == 0) {
                ksfamilyModeCount_TT.setText((int) R.string.nodata);
                statusProgress.setVisibility(8);
                ksfamilyModeCount_TT.setText(KSFamilyListActivity.this.getString(R.string.nodata));
            }
            super.handleMessage(msg);
        }
    };
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            KSFamilyListActivity.this.getData(true);
        }
    };
    /* access modifiers changed from: private */
    public int totalResultCount = 0;
    private ProgressDialog v_ProgressDialog = null;
    /* access modifiers changed from: private */
    public long validDataThreadId = -1;

    public void onCreate(Bundle savedInstanceState) {
        this.v_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ksfamily_activity);
        this._context = this;
        this.asyncImageLoader = AsyncImageLoader.instance();
        setListAdapter(new KSAppAdapter(this, R.layout.ksapp_row, new ArrayList<>()));
        getListView().setTextFilterEnabled(true);
        this.v_ProgressDialog.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        ((KSAppAdapter) getListAdapter()).clear();
        getData(false);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (getListAdapter() == null || getListAdapter().getCount() == 0) {
            getData(false);
        }
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    private class KSAppAdapter extends ArrayAdapter<KSApp> {
        private List<KSApp> items;

        public KSAppAdapter(Context context, int textViewResourceId, List<KSApp> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) KSFamilyListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.ksapp_row, (ViewGroup) null);
            }
            KSApp ksApp = this.items.get(position);
            if (ksApp != null) {
                ImageView imageView = (ImageView) v.findViewById(R.id.applogo);
                if (imageView != null) {
                    imageView.setTag(ksApp.getApplogo());
                    Drawable cachedImage = KSFamilyListActivity.this.asyncImageLoader.loadDrawable(ksApp.getApplogo(), new AsyncImageLoader.ImageCallback() {
                        public void imageLoaded(Drawable imageDrawable, String imageUrl) {
                            ImageView imageViewByTag = (ImageView) KSFamilyListActivity.this.getListView().findViewWithTag(imageUrl);
                            if (imageViewByTag != null) {
                                imageViewByTag.setImageDrawable(imageDrawable);
                            }
                        }
                    });
                    if (cachedImage != null) {
                        imageView.setImageDrawable(cachedImage);
                    }
                }
                TextView appnameTT = (TextView) v.findViewById(R.id.appname);
                if (appnameTT != null) {
                    appnameTT.setText(ksApp.getAppname());
                }
                TextView firmwareinfoTT = (TextView) v.findViewById(R.id.firmwareinfo);
                if (firmwareinfoTT != null) {
                    firmwareinfoTT.setText(StringUtil.replace(KSFamilyListActivity.this.getString(R.string.firmware_text), "{firmware}", ksApp.getFirmware()));
                }
                TextView ksappstatusTT = (TextView) v.findViewById(R.id.ksappstatus);
                if (!(ksappstatusTT == null || KSFamilyListActivity.this.installedKSApps == null)) {
                    KSApp installedKSApp = (KSApp) KSFamilyListActivity.this.installedKSApps.get(ksApp.getPackagename());
                    if (installedKSApp == null) {
                        ksappstatusTT.setTextColor(KSFamilyListActivity.this.getResources().getColor(R.color.good_vote_color));
                        ksappstatusTT.setText((int) R.string.ksappstatus_install);
                        ksApp.setStatus(0);
                    } else if (installedKSApp.getVersioncode() < ksApp.getVersioncode()) {
                        ksappstatusTT.setTextColor(KSFamilyListActivity.this.getResources().getColor(R.color.good_vote_color));
                        ksappstatusTT.setText((int) R.string.ksappstatus_update);
                        ksApp.setStatus(2);
                    } else {
                        ksappstatusTT.setTextColor(KSFamilyListActivity.this.getResources().getColor(R.color.bad_vote_color));
                        ksappstatusTT.setText((int) R.string.ksappstatus_installed);
                        ksApp.setStatus(1);
                    }
                }
            }
            return v;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        final KSApp ksApp = (KSApp) ((KSAppAdapter) getListAdapter()).getItem(position);
        Log.v(TAG, "current ksapp status = " + ksApp.getStatus());
        if (ksApp.getStatus() == 0 || ksApp.getStatus() == 2) {
            if (this._context.getString(R.string.partner).equals("lephone")) {
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse(ksApp.getDownloadpage()));
                this._context.startActivity(i);
                return;
            }
            View dialogView = LayoutInflater.from(this._context).inflate((int) R.layout.dialog_view, (ViewGroup) null);
            ((TextView) dialogView.findViewById(R.id.dialogcontent)).setText(String.valueOf(ksApp.getAppname()) + " " + ksApp.getVersionname() + "\n" + ksApp.getDescription());
            new AlertDialog.Builder(this._context).setIcon((int) R.drawable.ksicon).setTitle(ksApp.getAppname()).setView(dialogView).setPositiveButton((int) R.string.install_mode_local, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    KSFamilyListActivity.this.downloadAPK(ksApp);
                }
            }).setNegativeButton((int) R.string.install_mode_market, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent i = new Intent("android.intent.action.VIEW");
                    String marketUrl = "market://details?id=" + ksApp.getPackagename();
                    Log.v(KSFamilyListActivity.TAG, "Re-directing to " + marketUrl);
                    i.setData(Uri.parse(marketUrl));
                    KSFamilyListActivity.this._context.startActivity(i);
                }
            }).create().show();
        } else if (ksApp.getStatus() == 1) {
            runKSApp(ksApp.getPackagename());
        }
    }

    public void onStart() {
        Log.v(TAG, "onStart()");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public synchronized void getData(final boolean refresh) {
        ((ProgressBar) findViewById(R.id.ksfamily_statusProgress)).setVisibility(0);
        ((TextView) findViewById(R.id.ksfamily_mode_count)).setText((int) R.string.retrieving);
        Log.v(TAG, "refresh/initialDataThreadId" + refresh + CookieSpec.PATH_DELIM + this.validDataThreadId);
        Thread t = new Thread() {
            public void run() {
                List<KSApp> tmpKSAppList;
                try {
                    KSFamilyListActivity.this.installedKSApps = KSAppDataService.getInstalledKSApps(KSFamilyListActivity.this._context);
                    if (refresh) {
                        tmpKSAppList = new KSAppDataService().getLatestKSAppList(KSFamilyListActivity.this.getString(R.string.ksapplistIF));
                    } else if (KSAppDataService.ksAppList == null) {
                        tmpKSAppList = new KSAppDataService().getLatestKSAppList(KSFamilyListActivity.this.getString(R.string.ksapplistIF));
                    } else {
                        tmpKSAppList = KSAppDataService.ksAppList;
                    }
                    if (tmpKSAppList == null) {
                        Message m = Message.obtain(KSFamilyListActivity.this.myViewUpdateHandler);
                        m.what = 2;
                        m.sendToTarget();
                    } else if (KSFamilyListActivity.this.validDataThreadId == getId()) {
                        Message m2 = Message.obtain(KSFamilyListActivity.this.myViewUpdateHandler);
                        if (refresh) {
                            m2.what = 1;
                        } else {
                            m2.what = 0;
                        }
                        m2.obj = tmpKSAppList;
                        m2.sendToTarget();
                    }
                } catch (Exception e) {
                    Message m3 = Message.obtain(KSFamilyListActivity.this.myViewUpdateHandler);
                    m3.what = 2;
                    m3.sendToTarget();
                }
            }
        };
        this.validDataThreadId = t.getId();
        Log.v(TAG, "Change DataThreadId to " + this.validDataThreadId);
        t.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i(TAG, "Stop looping the child thread's message queue");
        super.onDestroy();
    }

    private void runKSApp(String packagename) {
        String mainActivity = KSAppDataService.getLauncherName(this._context, packagename);
        Intent mIntent = new Intent();
        mIntent.setComponent(new ComponentName(packagename, mainActivity));
        mIntent.setAction("android.intent.action.MAIN");
        mIntent.addCategory("android.intent.category.LAUNCHER");
        startActivity(mIntent);
    }

    /* access modifiers changed from: private */
    public synchronized void downloadAPK(final KSApp ksApp) {
        View dialogView = LayoutInflater.from(this._context).inflate((int) R.layout.dialog_view, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.dialogcontent)).setText("准备下载...");
        Dialog dialog = new AlertDialog.Builder(this._context).setIcon((int) R.drawable.ksicon).setTitle(ksApp.getAppname()).setView(dialogView).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                KSFamilyListActivity.this.downloadThreadMap.put(Long.valueOf(KSFamilyListActivity.this.downloadThreadID), false);
                Log.v(KSFamilyListActivity.TAG, "downloadThreadID = " + KSFamilyListActivity.this.downloadThreadID);
                dialog.cancel();
            }
        }).setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == 4) {
                    KSFamilyListActivity.this.downloadThreadMap.put(Long.valueOf(KSFamilyListActivity.this.downloadThreadID), false);
                    Log.v(KSFamilyListActivity.TAG, "downloadThreadID = " + KSFamilyListActivity.this.downloadThreadID);
                    dialog.cancel();
                }
                return false;
            }
        }).create();
        dialog.show();
        this.downloadDialog = dialog;
        this.downLoadView = dialogView;
        ((ProgressBar) dialogView.findViewById(R.id.download_pb)).setVisibility(0);
        Thread t = new Thread() {
            public void run() {
                try {
                    String downloadDirName = String.valueOf(new File(PreferenceSetting.getDownloadDir()).getParent()) + KSFamilyListActivity.this._context.getString(R.string.ksfamily_downloadDir);
                    File downloadDir = new File(downloadDirName);
                    if (!downloadDir.exists()) {
                        downloadDir.mkdirs();
                    }
                    Log.d(KSFamilyListActivity.TAG, "downloadAPK start");
                    KSFamilyListActivity.this.downloadThreadMap.put(Long.valueOf(getId()), true);
                    KSFamilyListActivity.this.downloadFile(ksApp.getDownloadurl(), String.valueOf(downloadDirName) + ksApp.getPackagename() + ".apk", getId());
                } catch (IOException e) {
                    KSFamilyListActivity.this.sendMsg(-1, null);
                }
            }
        };
        t.start();
        this.downloadThreadID = t.getId();
    }

    public void downloadFile(String url, String filepath, long threadId) throws IOException {
        URLConnection conn = new URL(url).openConnection();
        conn.connect();
        InputStream is = conn.getInputStream();
        this.fileSize = conn.getContentLength();
        if (this.fileSize <= 0) {
            throw new RuntimeException("无法获知文件大小 ");
        } else if (is == null) {
            throw new RuntimeException("stream is null");
        } else {
            FileOutputStream fos = new FileOutputStream(filepath);
            byte[] buf = new byte[1024];
            int downLoadFileSize = 0;
            sendMsg(0, 0);
            while (true) {
                int numread = is.read(buf);
                if (numread == -1) {
                    sendMsg(2, filepath);
                    try {
                        is.close();
                        return;
                    } catch (Exception ex) {
                        Log.e("tag", "error: " + ex.getMessage(), ex);
                        return;
                    }
                } else if (this.downloadThreadMap.get(Long.valueOf(threadId)).booleanValue()) {
                    fos.write(buf, 0, numread);
                    downLoadFileSize += numread;
                    sendMsg(1, Integer.valueOf(downLoadFileSize));
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendMsg(int flag, Object obj) {
        Message msg = new Message();
        msg.what = flag;
        msg.obj = obj;
        this.handler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    public void installAPK(String apkPath) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(apkPath)), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.ksfamilymenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                this.refreshListener.onClick(getListView());
                return true;
            case R.id.menu_back:
                this.backListener.onClick(getListView());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
