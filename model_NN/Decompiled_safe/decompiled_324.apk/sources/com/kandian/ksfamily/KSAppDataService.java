package com.kandian.ksfamily;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.kandian.common.Log;
import com.kandian.common.StringUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class KSAppDataService {
    private static String TAG = "KSAppDataService";
    public static List<KSApp> ksAppList = null;
    public static Map<String, String> launcherInfoMap = null;
    private int totalcount = 0;

    public List<KSApp> getLatestKSAppList(String dataIF) {
        JSONException e;
        IOException e2;
        List<KSApp> results = null;
        Log.v(TAG, "GetData begin!! ");
        try {
            JSONArray jsonArray = new JSONArray(StringUtil.getStringFromURL(StringUtil.replace(dataIF, "{packageName}", "")).trim());
            List<KSApp> results2 = new ArrayList<>();
            int i = 0;
            while (i < jsonArray.length()) {
                try {
                    JSONObject jsonKSApp = (JSONObject) jsonArray.get(i);
                    KSApp ksApp = new KSApp();
                    ksApp.setId(jsonKSApp.getLong("id"));
                    ksApp.setPackagename(jsonKSApp.getString("packagename"));
                    ksApp.setAppname(jsonKSApp.getString("appname"));
                    ksApp.setVersioncode(jsonKSApp.getInt("versioncode"));
                    ksApp.setVersionname(jsonKSApp.getString("versionname"));
                    ksApp.setApplogo(jsonKSApp.getString("applogo"));
                    ksApp.setDescription(jsonKSApp.getString("description"));
                    ksApp.setUpdateinfo(jsonKSApp.getString("updateinfo"));
                    ksApp.setDownloadurl(jsonKSApp.getString("downloadurl"));
                    ksApp.setSinaweiboid(jsonKSApp.getString("sinaweiboid"));
                    ksApp.setSinaweibokeyword(jsonKSApp.getString("sinaweibokeyword"));
                    ksApp.setFirmware(jsonKSApp.getString("firmware"));
                    ksApp.setDownloadpage(jsonKSApp.getString("downloadpage"));
                    results2.add(ksApp);
                    i++;
                } catch (IOException e3) {
                    e2 = e3;
                    results = results2;
                    e2.printStackTrace();
                    return results;
                } catch (JSONException e4) {
                    e = e4;
                    results = results2;
                    e.printStackTrace();
                    return results;
                }
            }
            ksAppList = results2;
            Log.v(TAG, "GetData end!! ");
            return results2;
        } catch (IOException e5) {
            e2 = e5;
            e2.printStackTrace();
            return results;
        } catch (JSONException e6) {
            e = e6;
            e.printStackTrace();
            return results;
        }
    }

    public static Map<String, KSApp> getInstalledKSApps(Context context) {
        Map<String, KSApp> results = new HashMap<>();
        PackageManager pckMan = context.getPackageManager();
        List<PackageInfo> packs = pckMan.getInstalledPackages(0);
        Log.v(TAG, "installed");
        for (int i = 0; i < packs.size(); i++) {
            PackageInfo p = packs.get(i);
            if (p.packageName != null && p.packageName.startsWith("com.kandian")) {
                KSApp ksApp = new KSApp();
                ksApp.setPackagename(p.packageName);
                ksApp.setVersioncode(p.versionCode);
                ksApp.setVersionname(p.versionName);
                ksApp.setAppname(p.applicationInfo.loadLabel(pckMan).toString());
                ksApp.setLauncher("");
                results.put(ksApp.getPackagename(), ksApp);
            }
        }
        return results;
    }

    public static String getLauncherName(Context context, String packageName) {
        PackageManager pkgMgt = context.getPackageManager();
        Intent it = new Intent("android.intent.action.MAIN");
        it.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> ra = pkgMgt.queryIntentActivities(it, 0);
        for (int i = 0; i < ra.size(); i++) {
            ActivityInfo ai = ra.get(i).activityInfo;
            if (ai.name != null && ai.name.startsWith(packageName)) {
                return ai.name;
            }
        }
        return null;
    }

    public int getTotalcount() {
        return this.totalcount;
    }

    public void setTotalcount(int totalcount2) {
        this.totalcount = totalcount2;
    }
}
