package com.kandian.user.history;

import android.content.Context;
import android.content.SharedPreferences;
import com.kandian.common.HtmlUtil;
import com.kandian.common.VideoAsset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UserHistoryService {
    private static String TAG = "UserHistoryService";
    public static String UserHistoryServiceSharedPreferencesName = "com.kandian.user.history";
    private static UserHistoryService ourInstance = new UserHistoryService();
    public int limit = 100;

    private UserHistoryService() {
    }

    public static UserHistoryService getInstance() {
        return ourInstance;
    }

    public void recordHistory(VideoAsset asset, String platform, String appcode, String playtype, Context context, long assetIdx, String showtime) {
        try {
            HtmlUtil.statistics(asset.getAssetType(), asset.getAssetId(), asset.getItemId(), platform, appcode, playtype, context);
            addHistory(asset, context, assetIdx, showtime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addHistory(VideoAsset asset, Context context, long assetIdx, String showtime) {
        UserHistoryAsset deleteOne;
        UserHistoryAsset userHistoryAsset = new UserHistoryAsset(asset, assetIdx, showtime);
        SharedPreferences settings = context.getSharedPreferences(UserHistoryServiceSharedPreferencesName, 0);
        SharedPreferences.Editor editor = settings.edit();
        Collection<String> collection = settings.getAll().values();
        ArrayList<UserHistoryAsset> histories = new ArrayList<>();
        for (String element : collection) {
            UserHistoryAsset uha = UserHistoryAsset.getObject4JsonString(element);
            if (uha != null) {
                histories.add(uha);
            }
        }
        ArrayList<UserHistoryAsset> histories2 = sort(histories);
        if (histories2.size() == this.limit && (deleteOne = histories2.get(histories2.size() - 1)) != null) {
            editor.remove(new StringBuilder(String.valueOf(deleteOne.getAssetId())).toString());
            editor.commit();
        }
        editor.putString(new StringBuilder(String.valueOf(userHistoryAsset.getAssetId())).toString(), UserHistoryAsset.getJsonString4JavaPOJO(userHistoryAsset));
        editor.commit();
    }

    public void clearHistory(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(UserHistoryServiceSharedPreferencesName, 0).edit();
        editor.clear();
        editor.commit();
    }

    public List<UserHistoryAsset> getHistory(Context context) {
        SharedPreferences settings = context.getSharedPreferences(UserHistoryServiceSharedPreferencesName, 0);
        SharedPreferences.Editor edit = settings.edit();
        Collection<String> collection = settings.getAll().values();
        ArrayList<UserHistoryAsset> histories = new ArrayList<>();
        for (String element : collection) {
            UserHistoryAsset uha = UserHistoryAsset.getObject4JsonString(element);
            if (uha != null) {
                histories.add(uha);
            }
        }
        return sort(histories);
    }

    private ArrayList<UserHistoryAsset> sort(ArrayList<UserHistoryAsset> list) {
        if (!(list == null || list.size() == 0)) {
            Collections.sort(list, new Comparator() {
                public int compare(Object object1, Object object2) {
                    if (((UserHistoryAsset) object1).getTimestamp() < ((UserHistoryAsset) object2).getTimestamp()) {
                        return 1;
                    }
                    return -1;
                }
            });
        }
        return list;
    }
}
