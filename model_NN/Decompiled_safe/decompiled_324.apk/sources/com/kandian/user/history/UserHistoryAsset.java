package com.kandian.user.history;

import com.kandian.common.VideoAsset;
import org.json.JSONObject;

public class UserHistoryAsset {
    private String asseType = "";
    private long assetId = 0;
    private String assetKey = "";
    private String assetName = "";
    private String bigImageUrl = "";
    private String imageUrl = "";
    private long index;
    private String origin = "";
    private String showtime;
    private long timestamp = 0;
    private double vote = 0.0d;

    public long getIndex() {
        return this.index;
    }

    public void setIndex(long index2) {
        this.index = index2;
    }

    public String getShowtime() {
        return this.showtime;
    }

    public void setShowtime(String showtime2) {
        this.showtime = showtime2;
    }

    public UserHistoryAsset() {
    }

    public UserHistoryAsset(VideoAsset videoAsset, long assetIdx, String showtime2) {
        setAssetId(videoAsset.getAssetId());
        setAssetName(videoAsset.getAssetName());
        setAsseType(videoAsset.getAssetType());
        setAssetKey(videoAsset.getAssetKey());
        setVote(videoAsset.getVote());
        setImageUrl(videoAsset.getImageUrl());
        setBigImageUrl(videoAsset.getBigImageUrl());
        setOrigin(videoAsset.getOrigin());
        setTimestamp(System.currentTimeMillis());
        setIndex(assetIdx);
        setShowtime(showtime2);
    }

    public long getAssetId() {
        return this.assetId;
    }

    public void setAssetId(long assetId2) {
        this.assetId = assetId2;
    }

    public String getAssetName() {
        return this.assetName;
    }

    public void setAssetName(String assetName2) {
        this.assetName = assetName2;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setOrigin(String origin2) {
        this.origin = origin2;
    }

    public double getVote() {
        return this.vote;
    }

    public void setVote(double vote2) {
        this.vote = vote2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getBigImageUrl() {
        return this.bigImageUrl;
    }

    public void setBigImageUrl(String bigImageUrl2) {
        this.bigImageUrl = bigImageUrl2;
    }

    public String getAssetKey() {
        return this.assetKey;
    }

    public void setAssetKey(String assetKey2) {
        this.assetKey = assetKey2;
    }

    public String getAsseType() {
        return this.asseType;
    }

    public void setAsseType(String asseType2) {
        this.asseType = asseType2;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp2) {
        this.timestamp = timestamp2;
    }

    public static String getJsonString4JavaPOJO(UserHistoryAsset pojoCalss) {
        StringBuffer sb = new StringBuffer("{");
        sb.append("\"").append("assetId").append("\":\"").append(pojoCalss.getAssetId()).append("\",");
        sb.append("\"").append("assetName").append("\":\"").append(pojoCalss.getAssetName()).append("\",");
        sb.append("\"").append("origin").append("\":\"").append(pojoCalss.getOrigin()).append("\",");
        sb.append("\"").append("vote").append("\":\"").append(pojoCalss.getVote()).append("\",");
        sb.append("\"").append("imageUrl").append("\":\"").append(pojoCalss.getImageUrl()).append("\",");
        sb.append("\"").append("bigImageUrl").append("\":\"").append(pojoCalss.getBigImageUrl()).append("\",");
        sb.append("\"").append("assetKey").append("\":\"").append(pojoCalss.getAssetKey()).append("\",");
        sb.append("\"").append("asseType").append("\":\"").append(pojoCalss.getAsseType()).append("\",");
        sb.append("\"").append("index").append("\":\"").append(pojoCalss.getIndex()).append("\",");
        sb.append("\"").append("showtime").append("\":\"").append(pojoCalss.getShowtime()).append("\",");
        sb.append("\"").append("timestamp").append("\":\"").append(pojoCalss.getTimestamp()).append("\"");
        sb.append("}");
        return sb.toString();
    }

    public static UserHistoryAsset getObject4JsonString(String jsonString) {
        UserHistoryAsset userHistoryAsset = new UserHistoryAsset();
        try {
            JSONObject json = new JSONObject(jsonString);
            userHistoryAsset.setAssetId(json.getLong("assetId"));
            userHistoryAsset.setAssetName(json.getString("assetName"));
            userHistoryAsset.setAsseType(json.getString("asseType"));
            userHistoryAsset.setAssetKey(json.getString("assetKey"));
            userHistoryAsset.setImageUrl(json.getString("imageUrl"));
            userHistoryAsset.setBigImageUrl(json.getString("bigImageUrl"));
            userHistoryAsset.setVote(json.getDouble("vote"));
            userHistoryAsset.setOrigin(json.getString("origin"));
            userHistoryAsset.setIndex(json.getLong("index"));
            userHistoryAsset.setShowtime(json.getString("showtime"));
            userHistoryAsset.setTimestamp(json.getLong("timestamp"));
            return userHistoryAsset;
        } catch (Exception e) {
            return null;
        }
    }
}
