package com.kandian.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.user.share.QQWeiboService;
import java.util.Map;

public class WeiboWebViewActivity extends Activity {
    private static String TAG = "WeiboWebViewActivity";
    private final Activity context = this;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        String action = getIntent().getStringExtra("action");
        WeiboWebViewHelper weiboWebViewHelper = new WeiboWebViewHelper();
        if (action != null && action.equals("UserBindShare")) {
            userBindShare(weiboWebViewHelper);
        } else if (action != null && action.equals("AutoCreateBind")) {
            weiboWebViewHelper.showWeiboWebView(R.layout.weibologin, this.context, new WeiboWebViewAction() {
                public void doWhenSuccess(Activity context, Map<String, String> param) {
                    QQWeiboService qqWeiboService = (QQWeiboService) QQWeiboService.getInstance();
                    final String access_token = param.get("access_token");
                    final String access_token_secret = param.get("access_token_secret");
                    final String userId = param.get("userId");
                    qqWeiboService.setAccess_token(access_token);
                    qqWeiboService.setAccess_token_secret(access_token_secret);
                    Asynchronous asynchronous = new Asynchronous(context);
                    asynchronous.setLoadingMsg("登录中,请稍等...");
                    asynchronous.asyncProcess(new AsyncProcess() {
                        public int process(Context c, Map<String, Object> map) {
                            setCallbackParameter("UserResult", UserService.getInstance().autocreatebind(userId, UserUtils.randomNumeric(6), access_token, access_token_secret, 2, c));
                            return 0;
                        }
                    });
                    asynchronous.asyncCallback(new AsyncCallback() {
                        public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                            UserService instance = UserService.getInstance();
                            if (((UserResult) outputparameter.get("UserResult")).getResultcode() == 1) {
                                Toast.makeText(c, "登录成功!", 0).show();
                                WeiboWebViewActivity.this.bindSyncShare();
                                Intent intent = new Intent();
                                intent.setClass(c, UserActivity.class);
                                WeiboWebViewActivity.this.startActivity(intent);
                                return;
                            }
                            Toast.makeText(c, "登录失败", 0).show();
                            Intent intent2 = new Intent();
                            intent2.setClass(c, UserLoginActivity.class);
                            WeiboWebViewActivity.this.startActivity(intent2);
                        }
                    });
                    asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                        public void handle(Context c, Exception e, Message m) {
                            Toast.makeText(c, "网络问题,登录失败", 0).show();
                        }
                    });
                    asynchronous.start();
                }

                public void doWhenFailed(Activity context) {
                }
            });
        }
    }

    private void userBindShare(WeiboWebViewHelper weiboWebViewHelper) {
        weiboWebViewHelper.showWeiboWebView(R.layout.weibologin, this.context, new WeiboWebViewAction() {
            public void doWhenSuccess(Activity context, Map<String, String> param) {
                QQWeiboService qqWeiboService = (QQWeiboService) QQWeiboService.getInstance();
                final String access_token = param.get("access_token");
                final String access_token_secret = param.get("access_token_secret");
                String str = param.get("v");
                final String userId = param.get("userId");
                qqWeiboService.setAccess_token(access_token);
                qqWeiboService.setAccess_token_secret(access_token_secret);
                Asynchronous asynchronous = new Asynchronous(context);
                asynchronous.setLoadingMsg("绑定中,请稍等...");
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> map) {
                        setCallbackParameter("UserResult", UserService.getInstance().binding(2, "腾讯微博", userId, null, c, access_token, access_token_secret));
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        UserService instance = UserService.getInstance();
                        UserResult userResult = (UserResult) outputparameter.get("UserResult");
                        if (userResult.getResultcode() == 1) {
                            Toast.makeText(c, "绑定成功!", 0).show();
                            WeiboWebViewActivity.this.bindSyncShare();
                            Intent intent = new Intent();
                            intent.setClass(c, UserActivity.class);
                            WeiboWebViewActivity.this.startActivity(intent);
                        } else if (userResult.getResultcode() == 4) {
                            Toast.makeText(c, "您绑定的腾讯用户已经被绑定过,绑定失败 !", 0).show();
                        } else if (userResult.getResultcode() == 2) {
                            Toast.makeText(c, "您已经绑定过该分享,绑定失败", 0).show();
                        } else {
                            Toast.makeText(c, "绑定失败", 0).show();
                        }
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                        Toast.makeText(c, "网络问题,绑定失败", 0).show();
                    }
                });
                asynchronous.start();
                WeiboWebViewActivity.this.bindSyncShare();
                Intent intent = new Intent();
                intent.setClass(context, UserActivity.class);
                WeiboWebViewActivity.this.startActivity(intent);
            }

            public void doWhenFailed(Activity context) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void bindSyncShare() {
        try {
            Log.v(TAG, "bindSyncShare!!");
            UserService userService = UserService.getInstance();
            UserResult syncAction = userService.syncAction(userService.getUsername(), "1,2", this.context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
