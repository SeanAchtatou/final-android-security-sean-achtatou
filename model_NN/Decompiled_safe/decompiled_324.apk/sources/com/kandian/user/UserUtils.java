package com.kandian.user;

import java.util.Random;
import java.util.regex.Pattern;

public class UserUtils {
    public static boolean emailFormat(String email) {
        if (!Pattern.compile("^([a-z0-9A-Z]+[-|\\.|_]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$").matcher(email).find()) {
            return false;
        }
        return true;
    }

    public static String randomNumeric(int length) {
        StringBuffer buffer = new StringBuffer("1234567890");
        StringBuffer sb = new StringBuffer();
        Random r = new Random();
        int range = buffer.length();
        for (int i = 0; i < length; i++) {
            sb.append(buffer.charAt(r.nextInt(range)));
        }
        return sb.toString();
    }
}
