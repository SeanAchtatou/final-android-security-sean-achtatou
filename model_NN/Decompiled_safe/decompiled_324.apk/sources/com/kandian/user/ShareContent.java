package com.kandian.user;

public class ShareContent {
    private String content;
    private String imageurl;

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getImageurl() {
        return this.imageurl;
    }

    public void setImageurl(String imageurl2) {
        this.imageurl = imageurl2;
    }

    public ShareContent(String content2, String imageurl2) {
        this.content = content2;
        this.imageurl = imageurl2;
    }

    public ShareContent() {
    }

    public ShareContent(String content2) {
        this.content = content2;
    }
}
