package com.kandian.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.ArrayList;
import java.util.Map;

public class ShareListActivity extends ListActivity {
    private static String TAG = "ShareListActivity";
    /* access modifiers changed from: private */
    public Activity context = this;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sharelist);
        UserService instance = UserService.getInstance();
        ArrayList<ShareObject> shareList = UserService.shareListAll;
        Log.v(TAG, "ShareListActivity=" + shareList.size());
        setListAdapter(new ShareAdapter(this, R.layout.sharerow, shareList));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        ShareObject so = (ShareObject) ((ShareAdapter) getListAdapter()).getItem(position);
        UserService userService = UserService.getInstance();
        if (so.getSharetype() == 0) {
            Intent it = new Intent("android.intent.action.VIEW");
            it.putExtra("sms_body", userService.getShareContent());
            it.setType("vnd.android-dir/mms-sms");
            startActivity(it);
        } else if (userService.isHasThisShare(so.getSharetype())) {
            Asynchronous asynchronous = new Asynchronous(this.context);
            asynchronous.setLoadingMsg("分享提交中,请稍等...");
            asynchronous.setInputParameter("shareObject", so);
            asynchronous.asyncProcess(new AsyncProcess() {
                public int process(Context context, Map<String, Object> inputparameter) throws Exception {
                    UserService userService = UserService.getInstance();
                    ShareObject myso = userService.getShareObject(((ShareObject) inputparameter.get("shareObject")).getSharetype());
                    setCallbackParameter("res", ShareFactory.getInstance().getShareService(myso).update(myso.getShareusername(), myso.getSharepassword(), new ShareContent(userService.getShareContent())));
                    return 0;
                }
            });
            asynchronous.asyncCallback(new AsyncCallback() {
                public void callback(Context context, Map<String, Object> outputparameter, Message m) {
                    if (((String) outputparameter.get("res")) != null) {
                        Toast.makeText(context, "分享成功", 0).show();
                    } else {
                        Toast.makeText(context, "分享失败", 0).show();
                    }
                    UserService.getInstance().back(context);
                }
            });
            asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                public void handle(Context context, Exception e, Message m) {
                    Toast.makeText(context, "网络问题,分享失败", 0).show();
                    UserService.getInstance().back(context);
                }
            });
            asynchronous.start();
        } else {
            showDialog(so.getSharetype());
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(final int sharetype) {
        final View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.sharedialog, (ViewGroup) null);
        return new AlertDialog.Builder(this).setTitle(getString(R.string.str_please_input)).setView(textEntryView).setPositiveButton((int) R.string.str_share, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final String username = ((EditText) textEntryView.findViewById(R.id.username_edit)).getText().toString();
                final String password = ((EditText) textEntryView.findViewById(R.id.password_edit)).getText().toString();
                if (username == null || username.trim().length() == 0 || password == null || password.trim().length() == 0) {
                    Toast.makeText(ShareListActivity.this.context, ShareListActivity.this.getString(R.string.str_reg_alert), 0).show();
                    return;
                }
                Asynchronous asynchronous = new Asynchronous(ShareListActivity.this.context);
                asynchronous.setLoadingMsg("分享提交中,请稍等...");
                final int i = sharetype;
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> map) throws Exception {
                        UserService userService = UserService.getInstance();
                        ShareService ss = ShareFactory.getInstance().getShareService(i);
                        String res = ss.login(username, password);
                        if (!(res == null || (res = ss.update(username, password, new ShareContent(userService.getShareContent()))) == null)) {
                            final String str = username;
                            final String str2 = password;
                            final int i = i;
                            new Thread() {
                                public void run() {
                                    try {
                                        Log.v("UserService", "begin auto login");
                                        UserService.getInstance().register(str, str2, i, ShareListActivity.this.context, null, null);
                                    } catch (Exception e) {
                                        Log.v("UserService", e.getMessage());
                                    }
                                }
                            }.start();
                        }
                        setCallbackParameter("res", res);
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        if (((String) outputparameter.get("res")) != null) {
                            Toast.makeText(c, "分享成功", 0).show();
                        } else {
                            Toast.makeText(c, "分享失败", 0).show();
                        }
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                        Toast.makeText(c, "网络问题,分享失败", 0).show();
                    }
                });
                asynchronous.start();
            }
        }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                UserService.getInstance().back(ShareListActivity.this.context);
            }
        }).create();
    }

    private class ShareAdapter extends ArrayAdapter<ShareObject> {
        public ShareAdapter(Context context, int textViewResourceId, ArrayList<ShareObject> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextView tt;
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) ShareListActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.sharerow, (ViewGroup) null);
            }
            ShareObject so = (ShareObject) getItem(position);
            if (!(so == null || (tt = (TextView) v.findViewById(R.id.sharename)) == null)) {
                tt.setText(UserService.getInstance().getShareName(so.getSharetype()));
            }
            return v;
        }
    }
}
