package com.kandian.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.user.favorite.UserFavoriteService;
import java.util.Map;
import org.apache.commons.httpclient.HttpState;

public class UserLoginActivity extends Activity {
    /* access modifiers changed from: private */
    public String TAG = "UserLoginActivity";
    /* access modifiers changed from: private */
    public String callBackActivity = "";
    /* access modifiers changed from: private */
    public UserLoginActivity context = this;
    protected ArrayAdapter<CharSequence> mAdapter;
    /* access modifiers changed from: private */
    public String mypwd;
    /* access modifiers changed from: private */
    public String myusername;
    private int stype = 0;
    /* access modifiers changed from: private */
    public int usersharetype;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.userlogin);
        this.callBackActivity = getIntent().getStringExtra("callBackActivity");
        AutoCompleteBuilder.build(R.id.username_edit_txt, this.context);
        AutoCompleteTextView un = (AutoCompleteTextView) findViewById(R.id.username_edit_txt);
        EditText pw = (EditText) findViewById(R.id.password_edit_txt);
        SharedPreferences settings = this.context.getSharedPreferences(UserService.PROKEY_HISTORY, 0);
        String his_username = settings.getString("his_username", "");
        String his_password = settings.getString("his_password", "");
        Log.v(this.TAG, "his_username:" + his_username + " his_password:" + his_password);
        un.setText(his_username);
        pw.setText(his_password);
        un.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((InputMethodManager) UserLoginActivity.this.getSystemService("input_method")).showSoftInput(v, 0);
            }
        });
        pw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((InputMethodManager) UserLoginActivity.this.getSystemService("input_method")).showSoftInput(v, 0);
            }
        });
        ((Button) findViewById(R.id.login_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String username = ((AutoCompleteTextView) UserLoginActivity.this.findViewById(R.id.username_edit_txt)).getText().toString();
                final String password = ((EditText) UserLoginActivity.this.findViewById(R.id.password_edit_txt)).getText().toString();
                UserLoginActivity.this.myusername = username;
                UserLoginActivity.this.mypwd = password;
                if (username == null || username.trim().length() == 0 || password == null || password.trim().length() == 0) {
                    Toast.makeText(UserLoginActivity.this.context, UserLoginActivity.this.getString(R.string.str_login_failed_msg), 0).show();
                    return;
                }
                Asynchronous asynchronous = new Asynchronous(UserLoginActivity.this.context);
                asynchronous.setLoadingMsg("登录中,请稍等...");
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> map) {
                        UserResult succ = UserService.getInstance().verification(username, password, UserLoginActivity.this.usersharetype, c);
                        Log.v(UserLoginActivity.this.TAG, "login username:" + username + " password:" + password + " usersharetype:" + UserLoginActivity.this.usersharetype);
                        setCallbackParameter("UserResult", succ);
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        UserService instance = UserService.getInstance();
                        if (1 == ((UserResult) outputparameter.get("UserResult")).getResultcode()) {
                            Log.v(UserLoginActivity.this.TAG, "success");
                            UserFavoriteService.isNeedSync = true;
                            Toast.makeText(c, UserLoginActivity.this.getString(R.string.str_login_success), 0).show();
                            if (UserLoginActivity.this.callBackActivity == null || UserLoginActivity.this.callBackActivity.trim().length() == 0) {
                                Intent intent = new Intent();
                                intent.setClass(UserLoginActivity.this.context, UserActivity.class);
                                UserLoginActivity.this.startActivity(intent);
                                return;
                            }
                            Intent intent2 = new Intent();
                            intent2.setClassName(UserLoginActivity.this.context, UserLoginActivity.this.callBackActivity);
                            UserLoginActivity.this.context.startActivity(intent2);
                            return;
                        }
                        Log.v(UserLoginActivity.this.TAG, "failed");
                        Toast.makeText(c, "用户名密码错误,登录失败", 0).show();
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                        Toast.makeText(c, "网络问题,登录失败", 0).show();
                    }
                });
                asynchronous.start();
            }
        });
        ((Button) findViewById(R.id.reg_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(UserLoginActivity.this.context, UserRegActivity.class);
                UserLoginActivity.this.startActivity(intent);
            }
        });
        ((Button) findViewById(R.id.sina_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserLoginActivity.this.usersharetype = 1;
                UserLoginActivity.this.showDialog(UserLoginActivity.this.usersharetype);
            }
        });
        Button qq_button = (Button) findViewById(R.id.qq_button);
        qq_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(UserLoginActivity.this.context, WeiboWebViewActivity.class);
                intent.putExtra("action", "AutoCreateBind");
                UserLoginActivity.this.startActivity(intent);
            }
        });
        if (HttpState.PREEMPTIVE_DEFAULT.equals(getString(R.string.is_needs_qq))) {
            qq_button.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void shareLogin(final int sharetype, final String shareusername, final String sharepassword) {
        Asynchronous asynchronous = new Asynchronous(this.context);
        asynchronous.setLoadingMsg("登录中,请稍等...");
        asynchronous.asyncProcess(new AsyncProcess() {
            public int process(Context c, Map<String, Object> map) {
                UserService userService = UserService.getInstance();
                if (UserLoginActivity.this.usersharetype == 0) {
                    return 0;
                }
                ShareService ss = ShareFactory.getInstance().getShareService(UserLoginActivity.this.usersharetype);
                if (ss == null) {
                    setCallbackParameter("res", "loginfailed");
                    return 0;
                } else if (ss.login(shareusername, sharepassword) != null) {
                    try {
                        Log.v("UserService", "begin login");
                        if (userService.autocreatebind(shareusername, sharepassword, null, null, sharetype, UserLoginActivity.this.context).getResultcode() == 1) {
                            setCallbackParameter("res", "success");
                            return 0;
                        }
                        setCallbackParameter("res", "loginfailed");
                        return 0;
                    } catch (Exception e) {
                        e.printStackTrace();
                        setCallbackParameter("res", "loginfailed");
                        return 0;
                    }
                } else {
                    setCallbackParameter("res", "loginfailed");
                    return 0;
                }
            }
        });
        asynchronous.asyncCallback(new AsyncCallback() {
            public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                UserService userService = UserService.getInstance();
                if (UserLoginActivity.this.usersharetype != 0) {
                    String res = (String) outputparameter.get("res");
                    if (res.trim().equals("success")) {
                        userService.setUserInfo(shareusername, sharepassword, UserLoginActivity.this.usersharetype);
                        UserLoginActivity.this.saveUserNameAndPwd(shareusername, sharepassword, UserLoginActivity.this.usersharetype);
                        Toast.makeText(UserLoginActivity.this.context, UserLoginActivity.this.getString(R.string.str_login_success), 0).show();
                        Intent intent = new Intent();
                        if (UserLoginActivity.this.callBackActivity == null || UserLoginActivity.this.callBackActivity.indexOf("com.kandian.ssmapp") == -1) {
                            intent.setClass(UserLoginActivity.this.context, UserActivity.class);
                        } else {
                            intent.setClassName(UserLoginActivity.this.context, UserLoginActivity.this.callBackActivity);
                        }
                        UserLoginActivity.this.context.startActivity(intent);
                    } else if (res.trim().equals("loginfailed")) {
                        Toast.makeText(UserLoginActivity.this.context, "登录失败", 0).show();
                    }
                }
            }
        });
        asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
            public void handle(Context c, Exception e, Message m) {
                Toast.makeText(c, "网络问题,登录失败", 0).show();
            }
        });
        asynchronous.start();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                final View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.sharelogindialog, (ViewGroup) null);
                return new AlertDialog.Builder(this).setTitle((int) R.string.sinaWeiboTitle).setView(textEntryView).setPositiveButton((int) R.string.str_login, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String shareusername = ((EditText) textEntryView.findViewById(R.id.share_username_edit)).getText().toString();
                        String sharepassword = ((EditText) textEntryView.findViewById(R.id.share_password_edit)).getText().toString();
                        if (shareusername == null || shareusername.trim().length() == 0 || sharepassword == null || sharepassword.trim().length() == 0) {
                            Toast.makeText(UserLoginActivity.this.context, UserLoginActivity.this.getString(R.string.str_login_failed_msg), 0).show();
                            return;
                        }
                        UserLoginActivity.this.myusername = shareusername;
                        UserLoginActivity.this.mypwd = sharepassword;
                        UserLoginActivity.this.shareLogin(UserLoginActivity.this.usersharetype, shareusername, sharepassword);
                    }
                }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        UserLoginActivity.this.usersharetype = 0;
                    }
                }).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: private */
    public void saveUserNameAndPwd(String username, String password, int usersharetype2) {
        SharedPreferences.Editor editor = this.context.getSharedPreferences("KuaiShouUserService", 0).edit();
        editor.remove("username");
        editor.remove("password");
        editor.remove("usersharetype");
        editor.commit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.putString("usersharetype", new StringBuilder(String.valueOf(usersharetype2)).toString());
        editor.commit();
    }
}
