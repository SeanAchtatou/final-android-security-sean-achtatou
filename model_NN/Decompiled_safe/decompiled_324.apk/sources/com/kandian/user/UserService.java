package com.kandian.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.PreferenceSetting;
import com.kandian.common.StringUtil;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.user.account.UserAccountHelper;
import com.kandian.user.account.UserInfo;
import com.kandian.user.favorite.UserFavoriteService;
import com.kandian.user.share.QQWeiboService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class UserService {
    public static final int ACTION_BINDING = 1;
    public static final int ACTION_UNBINDING = 2;
    private static String ACTIVEINFO_KEY = "ACTIVEINFO";
    public static final int AUTO_BIND_CREATE = 1;
    public static final int AUTO_BIND_EXIST = 0;
    public static final String PROKEY = "KuaiShouUserService";
    public static final String PROKEY_HISTORY = "KuaiShouUserServiceHistory";
    public static final String PROKEY_SHARE = "KuaiShouUserService_share";
    public static final int SYNC_ACTION_FAVORITES = 2;
    public static final int SYNC_ACTION_VOTE = 1;
    public static ArrayList<ShareObject> allshares = new ArrayList<>();
    private static boolean isneedlogined = true;
    private static UserService ourInstance = new UserService();
    public static ArrayList<ShareObject> shareListAll = new ArrayList<>();
    /* access modifiers changed from: private */
    public static String syncaction;
    private String TAG = "UserService";
    String access_token;
    String access_token_secret;
    private String baseurl = "http://w.51tv.com/ksAppServlet";
    /* access modifiers changed from: private */
    public String callbackActivityClassName;
    private String lastlogin_time;
    private String password;
    private String shareContent;
    private ArrayList<ShareObject> shareList = new ArrayList<>();
    private String username;
    private int usersharetype;

    static {
        shareListAll.add(new ShareObject("短消息发送分享", 0));
        shareListAll.add(new ShareObject("新浪微博", 1));
        shareListAll.add(new ShareObject("腾讯微博", 2));
        allshares.add(new ShareObject("新浪微博", 1));
        allshares.add(new ShareObject("腾讯微博", 2));
    }

    public static UserService getInstance() {
        return ourInstance;
    }

    private UserService() {
    }

    public String getLastlogin_time() {
        return this.lastlogin_time;
    }

    public void setLastlogin_time(String lastlogin_time2) {
        this.lastlogin_time = lastlogin_time2;
    }

    public void setUserInfo(String username2, String password2, int usersharetype2) {
        this.username = username2;
        this.password = password2;
        this.usersharetype = usersharetype2;
    }

    public String getSyncaction() {
        return syncaction;
    }

    public void setSyncaction(String syncaction2) {
        syncaction = syncaction2;
    }

    public int getUsersharetype() {
        return this.usersharetype;
    }

    public void setUsersharetype(int usersharetype2) {
        this.usersharetype = usersharetype2;
    }

    public String getUsername() {
        return this.username;
    }

    public ShareObject getNextShareObject(int sharetype) {
        int sharetype2 = sharetype + 1;
        Iterator<ShareObject> it = allshares.iterator();
        while (it.hasNext()) {
            ShareObject so = it.next();
            if (so.getSharetype() == sharetype2) {
                return so;
            }
        }
        return null;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public ArrayList<ShareObject> getShareList() {
        return this.shareList;
    }

    private void addShareObject(ShareObject so, Context context) {
        if (so != null && this.shareList != null && !isHasThisShare(so.getSharetype())) {
            this.shareList.add(so);
            if (so.getSharetype() == 2) {
                QQWeiboService qqWeiboService = (QQWeiboService) QQWeiboService.getInstance();
                qqWeiboService.init(context.getString(R.string.qqCustomKey), context.getString(R.string.qqCustomSecrect));
                qqWeiboService.setAccess_token(so.getAccess_token());
                qqWeiboService.setAccess_token_secret(so.getAccess_token_secret());
                Log.v(this.TAG, "qq getAccess_token =" + so.getAccess_token());
                Log.v(this.TAG, "qq getAccess_token_secret =" + so.getAccess_token_secret());
            }
            SharedPreferences.Editor editor = context.getSharedPreferences(PROKEY_SHARE, 0).edit();
            editor.putString("share_" + so.getSharetype(), so.serializationJSON());
            editor.commit();
        }
    }

    private void removeShareType(int sharetype, Context context) {
        ArrayList<ShareObject> newList = new ArrayList<>();
        if (this.shareList != null && this.shareList.size() != 0) {
            Iterator<ShareObject> it = this.shareList.iterator();
            while (it.hasNext()) {
                ShareObject so = it.next();
                if (so.getSharetype() != sharetype) {
                    newList.add(so);
                } else {
                    SharedPreferences.Editor editor = context.getSharedPreferences(PROKEY_SHARE, 0).edit();
                    editor.remove("share_" + so.getSharetype());
                    editor.commit();
                }
            }
            this.shareList = newList;
        }
    }

    public boolean isHasThisShare(int sharetype) {
        if (this.shareList == null || this.shareList.size() == 0) {
            return false;
        }
        Iterator<ShareObject> it = this.shareList.iterator();
        while (it.hasNext()) {
            if (it.next().getSharetype() == sharetype) {
                return true;
            }
        }
        return false;
    }

    public boolean isHasThisShare(int sharetype, Context context) {
        String so = context.getSharedPreferences(PROKEY_SHARE, 0).getString("share_" + sharetype, "");
        if (so == null || so.trim().length() == 0) {
            return false;
        }
        return true;
    }

    public ShareObject getShareObject(int sharetype) {
        if (this.shareList == null || this.shareList.size() == 0) {
            return null;
        }
        Iterator<ShareObject> it = this.shareList.iterator();
        while (it.hasNext()) {
            ShareObject so = it.next();
            if (so.getSharetype() == sharetype) {
                return so;
            }
        }
        return null;
    }

    public String getShareName(int sharetype) {
        if (shareListAll == null || shareListAll.size() == 0) {
            return null;
        }
        Iterator<ShareObject> it = shareListAll.iterator();
        while (it.hasNext()) {
            ShareObject so = it.next();
            if (so.getSharetype() == sharetype) {
                return so.getSharename();
            }
        }
        return null;
    }

    public void login(Context context) {
        UserAccountHelper userAccountHelper = UserAccountHelper.getInstance();
        userAccountHelper.init(context);
        UserInfo userInfo = userAccountHelper.getAccount((Activity) context);
        String username2 = null;
        String password2 = null;
        int usersharetype2 = 0;
        if (userInfo != null) {
            username2 = userInfo.getUsername();
            password2 = userInfo.getPassword();
            usersharetype2 = Integer.parseInt(userInfo.getShareType());
        }
        Log.v(">>>>>>login", "username:[" + username2 + "]");
        Log.v(">>>>>>login", "password:[" + password2 + "]");
        Log.v(">>>>>>login", "usersharetype:[" + usersharetype2 + "]");
        if (username2 == null || username2.trim().length() <= 0 || password2 == null || password2.trim().length() <= 0) {
            Intent intent = new Intent();
            intent.setClass(context, UserLoginActivity.class);
            context.startActivity(intent);
            return;
        }
        this.username = username2;
        this.password = password2;
        if (this.shareList == null || this.shareList.size() == 0) {
            Asynchronous asynchronous = new Asynchronous(context);
            asynchronous.setLoadingMsg("跳转中,请稍等...");
            asynchronous.setInputParameter("username", username2);
            asynchronous.setInputParameter("password", password2);
            asynchronous.setInputParameter("usersharetype", new StringBuilder(String.valueOf(usersharetype2)).toString());
            asynchronous.asyncProcess(new AsyncProcess() {
                public int process(Context c, Map<String, Object> inputparameter) {
                    String username = (String) inputparameter.get("username");
                    String password = (String) inputparameter.get("password");
                    String usersharetype = (String) inputparameter.get("usersharetype");
                    setCallbackParameter("UserResult", UserService.getInstance().verification(username, password, Integer.parseInt(usersharetype), c));
                    setCallbackParameter("username", username);
                    setCallbackParameter("password", password);
                    setCallbackParameter("usersharetype", usersharetype);
                    return 0;
                }
            });
            asynchronous.asyncCallback(new AsyncCallback() {
                public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                    UserResult userResult = (UserResult) outputparameter.get("UserResult");
                    String str = (String) outputparameter.get("username");
                    String str2 = (String) outputparameter.get("password");
                    String usersharetype = (String) outputparameter.get("usersharetype");
                    UserService userService = UserService.getInstance();
                    if (userResult.getResultcode() == 1) {
                        userService.setUsersharetype(Integer.parseInt(usersharetype));
                        userService.setSyncaction(UserService.syncaction);
                        Log.v("UserService", "login success " + userResult.getResultcode());
                        if (UserService.this.callbackActivityClassName == null || UserService.this.callbackActivityClassName.indexOf("com.kandian.ssmapp") == -1) {
                            Intent intent = new Intent();
                            intent.setClass(c, UserActivity.class);
                            c.startActivity(intent);
                            return;
                        }
                        Intent intent2 = new Intent();
                        intent2.setClassName(c, UserService.this.callbackActivityClassName);
                        c.startActivity(intent2);
                        return;
                    }
                    userService.setUsername(null);
                    userService.setPassword(null);
                    userService.setUsersharetype(0);
                    userService.setSyncaction(null);
                    Log.v("UserService", "login failed " + userResult.getResultcode());
                    Intent intent3 = new Intent();
                    intent3.setClass(c, UserLoginActivity.class);
                    c.startActivity(intent3);
                }
            });
            asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                public void handle(Context c, Exception e, Message m) {
                    Intent intent = new Intent();
                    intent.setClass(c, UserLoginActivity.class);
                    c.startActivity(intent);
                }
            });
            asynchronous.start();
        } else if (this.callbackActivityClassName == null || this.callbackActivityClassName.indexOf("com.kandian.ssmapp") == -1) {
            Intent intent2 = new Intent();
            intent2.setClass(context, UserActivity.class);
            context.startActivity(intent2);
        } else {
            Intent intent3 = new Intent();
            intent3.setClassName(context, this.callbackActivityClassName);
            context.startActivity(intent3);
        }
    }

    public void login(Context context, String callbackActivityClassName2) {
        this.callbackActivityClassName = callbackActivityClassName2;
        login(context);
    }

    public void logout(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("KuaiShouUserService", 0).edit();
        editor.remove("username");
        editor.remove("password");
        editor.remove("usersharetype");
        editor.remove("syncaction");
        editor.commit();
        this.shareList = new ArrayList<>();
        SharedPreferences.Editor editor2 = context.getSharedPreferences(PROKEY_SHARE, 0).edit();
        editor2.remove("share_0");
        editor2.remove("share_1");
        editor2.remove("share_2");
        editor2.commit();
        this.username = null;
        this.password = null;
        UserFavoriteService.getInstance().clearLocal(context);
        UserAccountHelper userAccountHelper = UserAccountHelper.getInstance();
        userAccountHelper.init(context);
        userAccountHelper.removeAccount((Activity) context);
        Log.v(">>>>>>logout", "logout!");
        back(context);
    }

    public boolean islogin(Context context) {
        SharedPreferences settings = context.getSharedPreferences("KuaiShouUserService", 0);
        String username2 = settings.getString("username", "");
        String password2 = settings.getString("password", "");
        if (username2 == null || username2.trim().length() <= 0 || password2 == null || password2.trim().length() <= 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x02d1  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0365  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.kandian.user.UserResult verification(java.lang.String r43, java.lang.String r44, int r45, android.content.Context r46) {
        /*
            r42 = this;
            r30 = 0
            if (r43 == 0) goto L_0x001a
            java.lang.String r38 = r43.trim()
            int r38 = r38.length()
            if (r38 == 0) goto L_0x001a
            if (r44 == 0) goto L_0x001a
            java.lang.String r38 = r44.trim()
            int r38 = r38.length()
            if (r38 != 0) goto L_0x0024
        L_0x001a:
            com.kandian.user.UserResult r38 = new com.kandian.user.UserResult
            r39 = 2
            java.lang.String r40 = "登录失败,用户名密码没有填全"
            r38.<init>(r39, r40)
        L_0x0023:
            return r38
        L_0x0024:
            java.lang.String r19 = android.os.Build.MANUFACTURER
            if (r19 != 0) goto L_0x002a
            java.lang.String r19 = ""
        L_0x002a:
            r0 = r42
            r1 = r46
            java.lang.String r9 = r0.getdeviceid(r1)
            if (r9 != 0) goto L_0x0036
            java.lang.String r9 = ""
        L_0x0036:
            java.lang.String r20 = android.os.Build.MODEL
            if (r20 != 0) goto L_0x003c
            java.lang.String r20 = ""
        L_0x003c:
            r0 = r42
            r1 = r46
            java.lang.String r37 = r0.getUUID(r1)
            r0 = r42
            r1 = r46
            java.lang.String r18 = r0.getmac(r1)
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            java.lang.String r39 = "{\"username\":\""
            r38.<init>(r39)
            r0 = r38
            r1 = r43
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"password\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r44
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"sharetype\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r45
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"manufacturer\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r19
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"deviceid\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r9
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"model\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r20
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"mac\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r18
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\",\"uuid\":\""
            java.lang.StringBuilder r38 = r38.append(r39)
            r0 = r38
            r1 = r37
            java.lang.StringBuilder r38 = r0.append(r1)
            java.lang.String r39 = "\"}"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r16 = r38.toString()
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "json:"
            r39.<init>(r40)
            r0 = r39
            r1 = r16
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            java.lang.String r16 = com.kandian.user.UserInfoCrypto.encrypt(r16)
            if (r16 != 0) goto L_0x00fe
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.String r39 = "encrypt failed response is null!"
            com.kandian.common.Log.v(r38, r39)
            com.kandian.user.UserResult r38 = new com.kandian.user.UserResult
            r39 = 2
            java.lang.String r40 = "登录失败"
            r38.<init>(r39, r40)
            goto L_0x0023
        L_0x00fe:
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "json encrypt:"
            r39.<init>(r40)
            r0 = r39
            r1 = r16
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            java.lang.StringBuilder r38 = new java.lang.StringBuilder
            r0 = r42
            java.lang.String r0 = r0.baseurl
            r39 = r0
            java.lang.String r39 = java.lang.String.valueOf(r39)
            r38.<init>(r39)
            java.lang.String r39 = "?method=login"
            java.lang.StringBuilder r38 = r38.append(r39)
            java.lang.String r34 = r38.toString()
            long r28 = java.lang.System.currentTimeMillis()
            r0 = r34
            r1 = r16
            java.lang.String r21 = com.kandian.common.StringUtil.requestInvoker(r0, r1)
            long r12 = java.lang.System.currentTimeMillis()
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "request address:"
            r39.<init>(r40)
            r0 = r39
            r1 = r34
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r40 = " used "
            java.lang.StringBuilder r39 = r39.append(r40)
            long r40 = r12 - r28
            java.lang.StringBuilder r39 = r39.append(r40)
            java.lang.String r40 = "ms"
            java.lang.StringBuilder r39 = r39.append(r40)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            if (r21 == 0) goto L_0x017d
            java.lang.String r38 = r21.trim()
            int r38 = r38.length()
            if (r38 != 0) goto L_0x01a4
        L_0x017d:
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "failed response is null! res:"
            r39.<init>(r40)
            r0 = r39
            r1 = r21
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            com.kandian.user.UserResult r38 = new com.kandian.user.UserResult
            r39 = 2
            java.lang.String r40 = "登录失败"
            r38.<init>(r39, r40)
            goto L_0x0023
        L_0x01a4:
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "res:"
            r39.<init>(r40)
            r0 = r39
            r1 = r21
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            java.lang.String r21 = com.kandian.user.UserInfoCrypto.decrypt(r21)
            r0 = r42
            java.lang.String r0 = r0.TAG
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder
            java.lang.String r40 = "res decrypt:"
            r39.<init>(r40)
            r0 = r39
            r1 = r21
            java.lang.StringBuilder r39 = r0.append(r1)
            java.lang.String r39 = r39.toString()
            com.kandian.common.Log.v(r38, r39)
            r0 = r42
            r1 = r21
            org.json.JSONObject r15 = r0.getJSONObject(r1)
            r22 = 0
            r33 = 0
            r32 = 0
            r31 = 0
            java.lang.String r38 = "resultCode"
            r0 = r15
            r1 = r38
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            if (r22 == 0) goto L_0x0357
            java.lang.String r38 = r22.trim()     // Catch:{ Exception -> 0x035b }
            int r38 = r38.length()     // Catch:{ Exception -> 0x035b }
            if (r38 <= 0) goto L_0x0357
            java.lang.String r38 = r22.trim()     // Catch:{ Exception -> 0x035b }
            java.lang.String r39 = "1"
            boolean r38 = r38.equals(r39)     // Catch:{ Exception -> 0x035b }
            if (r38 == 0) goto L_0x0357
            com.kandian.user.account.UserAccountHelper r35 = com.kandian.user.account.UserAccountHelper.getInstance()     // Catch:{ Exception -> 0x035b }
            r0 = r35
            r1 = r46
            r0.init(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r46
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Exception -> 0x035b }
            r5 = r0
            r0 = r35
            r1 = r5
            com.kandian.user.account.UserInfo r36 = r0.getAccount(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r42
            java.lang.String r0 = r0.TAG     // Catch:{ Exception -> 0x035b }
            r38 = r0
            java.lang.String r39 = "addAccount"
            com.kandian.common.Log.v(r38, r39)     // Catch:{ Exception -> 0x035b }
            r0 = r46
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Exception -> 0x035b }
            r5 = r0
            r0 = r35
            r1 = r43
            r2 = r44
            r3 = r45
            r4 = r5
            r0.addAccount(r1, r2, r3, r4)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "autoShareAction"
            r0 = r15
            r1 = r38
            java.lang.String r38 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            com.kandian.user.UserService.syncaction = r38     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "login_name"
            r0 = r15
            r1 = r38
            java.lang.String r33 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "password"
            r0 = r15
            r1 = r38
            java.lang.String r32 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "lastlogin_time"
            r0 = r15
            r1 = r38
            java.lang.String r31 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r42
            java.lang.String r0 = r0.TAG     // Catch:{ Exception -> 0x035b }
            r38 = r0
            java.lang.StringBuilder r39 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x035b }
            java.lang.String r40 = "get syncaction:"
            r39.<init>(r40)     // Catch:{ Exception -> 0x035b }
            java.lang.String r40 = com.kandian.user.UserService.syncaction     // Catch:{ Exception -> 0x035b }
            java.lang.StringBuilder r39 = r39.append(r40)     // Catch:{ Exception -> 0x035b }
            java.lang.String r39 = r39.toString()     // Catch:{ Exception -> 0x035b }
            com.kandian.common.Log.v(r38, r39)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "loginBindList"
            r0 = r15
            r1 = r38
            java.lang.String r17 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            if (r17 == 0) goto L_0x02b5
            java.lang.String r38 = r17.trim()     // Catch:{ Exception -> 0x035b }
            java.lang.String r39 = "[]"
            boolean r38 = r38.equals(r39)     // Catch:{ Exception -> 0x035b }
            if (r38 != 0) goto L_0x02b5
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ Exception -> 0x035b }
            r0 = r7
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x035b }
            if (r7 == 0) goto L_0x02b5
            int r38 = r7.length()     // Catch:{ Exception -> 0x035b }
            if (r38 <= 0) goto L_0x02b5
            r14 = 0
        L_0x02ac:
            int r38 = r7.length()     // Catch:{ Exception -> 0x035b }
            r0 = r14
            r1 = r38
            if (r0 < r1) goto L_0x02dc
        L_0x02b5:
            r30 = 1
        L_0x02b7:
            if (r22 == 0) goto L_0x02c3
            java.lang.String r38 = r22.trim()
            int r38 = r38.length()
            if (r38 != 0) goto L_0x02c5
        L_0x02c3:
            java.lang.String r22 = "0"
        L_0x02c5:
            java.lang.String r38 = r22.trim()
            java.lang.String r39 = "3"
            boolean r38 = r38.equals(r39)
            if (r38 == 0) goto L_0x0365
            com.kandian.user.UserResult r38 = new com.kandian.user.UserResult
            r39 = 3
            java.lang.String r40 = "用户不存在"
            r38.<init>(r39, r40)
            goto L_0x0023
        L_0x02dc:
            org.json.JSONObject r8 = r7.getJSONObject(r14)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "shareName"
            r0 = r8
            r1 = r38
            java.lang.String r26 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "sharePassword"
            r0 = r8
            r1 = r38
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "shareType"
            r0 = r8
            r1 = r38
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "access_token"
            r0 = r8
            r1 = r38
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            java.lang.String r38 = "access_token_secret"
            r0 = r8
            r1 = r38
            java.lang.String r6 = r0.getString(r1)     // Catch:{ Exception -> 0x035b }
            com.kandian.user.ShareObject r27 = new com.kandian.user.ShareObject     // Catch:{ Exception -> 0x035b }
            r27.<init>()     // Catch:{ Exception -> 0x035b }
            r0 = r27
            r1 = r24
            r0.setSharepassword(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r27
            r1 = r26
            r0.setShareusername(r1)     // Catch:{ Exception -> 0x035b }
            int r38 = java.lang.Integer.parseInt(r25)     // Catch:{ Exception -> 0x035b }
            r0 = r27
            r1 = r38
            r0.setSharetype(r1)     // Catch:{ Exception -> 0x035b }
            int r38 = java.lang.Integer.parseInt(r25)     // Catch:{ Exception -> 0x035b }
            r0 = r42
            r1 = r38
            java.lang.String r38 = r0.getShareName(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r27
            r1 = r38
            r0.setSharename(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r27
            r1 = r5
            r0.setAccess_token(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r27
            r1 = r6
            r0.setAccess_token_secret(r1)     // Catch:{ Exception -> 0x035b }
            r0 = r42
            r1 = r27
            r2 = r46
            r0.addShareObject(r1, r2)     // Catch:{ Exception -> 0x035b }
            int r14 = r14 + 1
            goto L_0x02ac
        L_0x0357:
            r30 = 0
            goto L_0x02b7
        L_0x035b:
            r38 = move-exception
            r10 = r38
            r30 = 0
            r10.printStackTrace()
            goto L_0x02b7
        L_0x0365:
            if (r30 == 0) goto L_0x03f9
            r43 = r33
            r44 = r32
            r0 = r43
            r1 = r42
            r1.username = r0
            r0 = r44
            r1 = r42
            r1.password = r0
            r0 = r31
            r1 = r42
            r1.lastlogin_time = r0
            if (r46 == 0) goto L_0x03ee
            java.lang.String r38 = "KuaiShouUserService"
            r39 = 0
            r0 = r46
            r1 = r38
            r2 = r39
            android.content.SharedPreferences r23 = r0.getSharedPreferences(r1, r2)
            android.content.SharedPreferences$Editor r11 = r23.edit()
            java.lang.String r38 = "username"
            r0 = r11
            r1 = r38
            r0.remove(r1)
            java.lang.String r38 = "password"
            r0 = r11
            r1 = r38
            r0.remove(r1)
            java.lang.String r38 = "syncaction"
            r0 = r11
            r1 = r38
            r0.remove(r1)
            java.lang.String r38 = "usersharetype"
            r0 = r11
            r1 = r38
            r0.remove(r1)
            r11.commit()
            java.lang.String r38 = "username"
            r0 = r11
            r1 = r38
            r2 = r43
            r0.putString(r1, r2)
            java.lang.String r38 = "password"
            r0 = r11
            r1 = r38
            r2 = r44
            r0.putString(r1, r2)
            java.lang.String r38 = "syncaction"
            java.lang.String r39 = com.kandian.user.UserService.syncaction
            r0 = r11
            r1 = r38
            r2 = r39
            r0.putString(r1, r2)
            java.lang.String r38 = "usersharetype"
            java.lang.String r39 = "0"
            r0 = r11
            r1 = r38
            r2 = r39
            r0.putString(r1, r2)
            r11.commit()
            r0 = r42
            r1 = r46
            r2 = r43
            r3 = r44
            r0.saveHistoryUserInfo(r1, r2, r3)
        L_0x03ee:
            com.kandian.user.UserResult r38 = new com.kandian.user.UserResult
            r39 = 1
            java.lang.String r40 = "登录成功"
            r38.<init>(r39, r40)
            goto L_0x0023
        L_0x03f9:
            com.kandian.user.UserResult r38 = new com.kandian.user.UserResult
            r39 = 2
            java.lang.String r40 = "登录失败,请检查用户名密码是否填写正确!"
            r38.<init>(r39, r40)
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.user.UserService.verification(java.lang.String, java.lang.String, int, android.content.Context):com.kandian.user.UserResult");
    }

    private String getUUID(Context context) {
        JSONObject jsonObject = getActiveInfo(context);
        String uuid = null;
        if (jsonObject != null) {
            try {
                Log.v(this.TAG, "local jsonObject = " + jsonObject.toString(4));
                uuid = jsonObject.getString("uuid");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (uuid != null && uuid.trim().length() != 0) {
            return uuid;
        }
        String uuid2 = readUUID(context);
        if (uuid2 != null && uuid2.trim().length() != 0) {
            return uuid2;
        }
        String uuid3 = UUID.randomUUID().toString();
        saveUUID(uuid3);
        return uuid3;
    }

    private String readUUID(Context context) {
        Log.v(this.TAG, "PreferenceSetting.getDownloadDir() = " + PreferenceSetting.getDownloadDir());
        if (PreferenceSetting.getDownloadDir() == null) {
            PreferenceSetting.setDownloadDir(context);
        }
        File file = new File(String.valueOf(String.valueOf(new File(PreferenceSetting.getDownloadDir()).getParent()) + "/.system") + "/Runtime.dat");
        if (!file.exists()) {
            return null;
        }
        Properties pro = new Properties();
        try {
            FileInputStream in = new FileInputStream(file);
            pro.load(in);
            in.close();
            return (String) pro.get("uuid");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void saveUUID(String uuid) {
        try {
            String uuidPath = String.valueOf(new File(PreferenceSetting.getDownloadDir()).getParent()) + "/.system";
            File uuidF = new File(uuidPath);
            uuidF.mkdirs();
            Log.v(this.TAG, "uuidPath = " + uuidF.getAbsolutePath());
            FileOutputStream out = new FileOutputStream(new File(uuidPath, "Runtime.dat"));
            generateProperties(uuid).store(out, "");
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    protected static Properties generateProperties(String uuid) {
        Properties pro = new Properties();
        pro.setProperty("uuid", uuid);
        return pro;
    }

    private JSONObject getActiveInfo(Context context) {
        JSONObject result = null;
        try {
            String jsonResult = context.getSharedPreferences(String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName) + ".activeinfo", 0).getString(ACTIVEINFO_KEY, "");
            if (jsonResult != null && jsonResult.trim().length() > 0) {
                try {
                    result = new JSONObject(jsonResult);
                } catch (JSONException e) {
                    return null;
                }
            }
            return result;
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    public void back(Context context) {
        if (this.callbackActivityClassName != null) {
            Intent intent = new Intent();
            intent.setClassName(context, this.callbackActivityClassName);
            intent.setFlags(67108864);
            context.startActivity(intent);
        } else if (context instanceof Activity) {
            ((Activity) context).finish();
        }
    }

    public UserResult binding(int shareType, String sharename, String shareusername, String sharepassword, Context context, String access_token2, String access_token_secret2) {
        String message;
        Log.v(this.TAG, "share binding shareType:" + shareType + " sharename:" + sharename + " shareusername:" + shareusername + " sharepassword:" + sharepassword + " username:" + this.username + " password:" + this.password + " access_token:" + access_token2 + " access_token_secret:" + access_token_secret2);
        if (!check()) {
            return new UserResult(2, "绑定失败");
        }
        if (shareType == 0) {
            return new UserResult(2, "无法绑定");
        }
        ShareObject so = new ShareObject();
        so.setSharetype(shareType);
        so.setShareusername(shareusername);
        so.setSharepassword(sharepassword);
        so.setSharename(sharename);
        String res = ShareFactory.getInstance().getShareService(so).login(shareusername, sharepassword);
        if (res == null) {
            return new UserResult(2, "绑定失败 无法连接分享");
        }
        if (access_token2 == null) {
            access_token2 = "";
        }
        if (access_token_secret2 == null) {
            access_token_secret2 = "";
        }
        if (shareusername == null || shareusername.trim().length() == 0) {
            shareusername = "null";
        }
        if (sharepassword == null || sharepassword.trim().length() == 0) {
            sharepassword = "null";
        }
        Log.v(this.TAG, "share response:" + res);
        String json = "{\"action\":\"1\",\"usercode\":\"" + this.username + "\",\"shareusername\":\"" + shareusername + "\",\"sharepassword\":\"" + sharepassword + "\",\"sharetype\":\"" + shareType + "\",\"access_token\":\"" + access_token2 + "\",\"access_token_secret\":\"" + access_token_secret2 + "\"}";
        Log.v(this.TAG, "json:" + json);
        String json2 = UserInfoCrypto.encrypt(json);
        if (json2 == null) {
            Log.v(this.TAG, "encrypt failed response is null!");
            return new UserResult(2, "绑定失败");
        }
        Log.v(this.TAG, "json encrypt:" + json2);
        String url = String.valueOf(this.baseurl) + "?method=bind";
        long start = System.currentTimeMillis();
        String response = StringUtil.requestInvoker(url, json2);
        Log.v(this.TAG, "request address:" + url + " used " + (System.currentTimeMillis() - start) + "ms");
        if (response == null || response.trim().length() == 0) {
            Log.v(this.TAG, "failed response is null! res:" + response);
            return new UserResult(2, "绑定失败");
        }
        Log.v(this.TAG, "response:" + response);
        String response2 = UserInfoCrypto.decrypt(response);
        Log.v(this.TAG, "response decrypt:" + response2);
        JSONObject jo = getJSONObject(response2);
        try {
            int result = jo.getInt("resultCode");
            if (result != 1) {
                message = jo.getString("message");
            } else {
                message = "绑定成功";
                addShareObject(so, context);
                createFriendship(shareusername, sharepassword, shareType, context);
            }
            return new UserResult(result, message);
        } catch (JSONException e) {
            e.printStackTrace();
            return new UserResult(2, "绑定失败 ");
        }
    }

    public void createFriendship(String userId, String password2, int sharetype, Context context) {
        try {
            ShareService ss = ShareFactory.getInstance().getShareService(sharetype);
            String tartgetUserId = null;
            if (sharetype == 1) {
                tartgetUserId = context.getString(R.string.sinaWeibo);
            } else if (sharetype == 2) {
                tartgetUserId = context.getString(R.string.qqWeibo);
                ((QQWeiboService) QQWeiboService.getInstance()).init(context.getString(R.string.qqCustomKey), context.getString(R.string.qqCustomSecrect));
            }
            Log.v(this.TAG, "response createFriendship begin! tartgetUserId:" + tartgetUserId + " userId:" + userId + " password:" + password2);
            String createFriendship = ss.createFriendship(tartgetUserId, userId, password2);
            Log.v(this.TAG, "response createFriendship end!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public UserResult register(String username2, String password2, int shareType, Context context, String access_token2, String access_token_secret2) {
        String message;
        if (username2 == null || username2.trim().length() == 0 || password2 == null || password2.trim().length() == 0) {
            return new UserResult(2, "注册失败,用户名密码没有填全");
        }
        String json = "{\"username\":\"" + username2 + "\",\"password\":\"" + password2 + "\",\"resourcecode\":\"1\",\"partnercode\":\"" + context.getString(R.string.partner) + "\"}";
        Log.v(this.TAG, "json:" + json);
        String json2 = UserInfoCrypto.encrypt(json);
        if (json2 == null) {
            Log.v(this.TAG, "encrypt failed response is null!");
            return new UserResult(2, "注册失败");
        }
        Log.v(this.TAG, "json encrypt:" + json2);
        String url = String.valueOf(this.baseurl) + "?method=register";
        long start = System.currentTimeMillis();
        String res = StringUtil.requestInvoker(url, json2);
        Log.v(this.TAG, "request address:" + url + " used " + (System.currentTimeMillis() - start) + "ms");
        if (res == null || res.trim().length() == 0) {
            Log.v(this.TAG, "failed response is null! res:" + res);
            return new UserResult(2, "注册失败");
        }
        Log.v(this.TAG, "res:" + res);
        String res2 = UserInfoCrypto.decrypt(res);
        Log.v(this.TAG, "res decrypt:" + res2);
        JSONObject jo = getJSONObject(res2);
        try {
            int result = jo.getInt("resultCode");
            if (result != 1) {
                message = jo.getString("message");
                Log.v(this.TAG, "register failed result=" + result);
            } else {
                Log.v(this.TAG, "register success result=" + result);
                message = "注册成功";
                this.username = username2;
                this.password = password2;
                SharedPreferences.Editor editor = context.getSharedPreferences("KuaiShouUserService", 0).edit();
                editor.putString("username", username2);
                editor.putString("password", password2);
                editor.putString("usersharetype", String.valueOf(this.usersharetype));
                editor.commit();
                UserAccountHelper userAccountHelper = UserAccountHelper.getInstance();
                userAccountHelper.init(context);
                if (userAccountHelper.addAccount(username2, password2, shareType, (Activity) context)) {
                    Log.v(this.TAG, "account result=" + result);
                }
                saveHistoryUserInfo(context, username2, password2);
                if (shareType != 0) {
                    this.usersharetype = shareType;
                    if (binding(shareType, null, username2, password2, context, access_token2, access_token_secret2).getResultcode() == 1) {
                        ShareObject so = new ShareObject();
                        so.setSharetype(shareType);
                        so.setShareusername(username2);
                        so.setSharepassword(password2);
                        addShareObject(so, context);
                    }
                }
            }
            UserResult userResult = new UserResult(result, message);
            UserResult userResult2 = userResult;
            return userResult;
        } catch (JSONException e) {
            e.printStackTrace();
            return new UserResult(2, "注册失败");
        }
    }

    public UserResult unbinding(String shareusername, String sharepassword, int shareType, Context context) {
        String message;
        if (!check()) {
            return new UserResult(2, "解绑定失败");
        }
        if (shareType == 0) {
            return new UserResult(2, "解绑定失败");
        }
        if (!check()) {
            return new UserResult(2, "解绑定失败");
        }
        String json = "{\"action\":\"2\",\"usercode\":\"" + this.username + "\",\"shareusername\":\"" + shareusername + "\",\"sharepassword\":\"" + sharepassword + "\",\"sharetype\":\"" + shareType + "\"}";
        Log.v(this.TAG, "json:" + json);
        String json2 = UserInfoCrypto.encrypt(json);
        if (json2 == null) {
            Log.v(this.TAG, "encrypt failed response is null!");
            return new UserResult(2, "解绑定失败");
        }
        Log.v(this.TAG, "json encrypt:" + json2);
        String url = String.valueOf(this.baseurl) + "?method=bind";
        long start = System.currentTimeMillis();
        String response = StringUtil.requestInvoker(url, json2);
        Log.v(this.TAG, "request address:" + url + " used " + (System.currentTimeMillis() - start) + "ms");
        if (response == null || response.trim().length() == 0) {
            Log.v(this.TAG, "failed response is null! res:" + response);
            return new UserResult(2, "解绑定失败");
        }
        Log.v(this.TAG, "response:" + response);
        String response2 = UserInfoCrypto.decrypt(response);
        Log.v(this.TAG, "response decrypt:" + response2);
        JSONObject jo = getJSONObject(response2);
        try {
            int result = jo.getInt("resultCode");
            if (result != 1) {
                message = jo.getString("message");
            } else {
                message = "解绑定成功";
                removeShareType(shareType, context);
            }
            return new UserResult(result, message);
        } catch (JSONException e) {
            e.printStackTrace();
            return new UserResult(2, "解绑定失败");
        }
    }

    private boolean check() {
        if (this.username == null || this.username.trim().length() <= 0 || this.password == null || this.password.trim().length() <= 0) {
            return false;
        }
        return true;
    }

    public void sendSMS(String content, Context context) {
        Intent it = new Intent("android.intent.action.VIEW");
        it.putExtra("sms_body", content);
        it.setType("vnd.android-dir/mms-sms");
        context.startActivity(it);
    }

    public String getShareContent() {
        return this.shareContent;
    }

    public void share(Context context, String shareContent2) {
        this.shareContent = shareContent2;
        Intent intent = new Intent();
        intent.setClass(context, ShareListActivity.class);
        context.startActivity(intent);
    }

    private JSONObject getJSONObject(String res) {
        try {
            return new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0227  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x02bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.kandian.user.UserResult autobind(java.lang.String r33, java.lang.String r34, java.lang.String r35, java.lang.String r36, int r37, android.content.Context r38, int r39) {
        /*
            r32 = this;
            r24 = 0
            java.lang.StringBuilder r28 = new java.lang.StringBuilder
            java.lang.String r29 = "{\"usercode\":\""
            r28.<init>(r29)
            r0 = r28
            r1 = r33
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "\",\"password\":\""
            java.lang.StringBuilder r28 = r28.append(r29)
            r0 = r28
            r1 = r34
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "\",\"shareusername\":\""
            java.lang.StringBuilder r28 = r28.append(r29)
            r0 = r28
            r1 = r35
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "\",\"sharepassword\":\""
            java.lang.StringBuilder r28 = r28.append(r29)
            r0 = r28
            r1 = r36
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "\",\"sharetype\":\""
            java.lang.StringBuilder r28 = r28.append(r29)
            r0 = r28
            r1 = r37
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "\",\"action\":\""
            java.lang.StringBuilder r28 = r28.append(r29)
            r0 = r28
            r1 = r39
            java.lang.StringBuilder r28 = r0.append(r1)
            java.lang.String r29 = "\"}"
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r13 = r28.toString()
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "json:"
            r29.<init>(r30)
            r0 = r29
            r1 = r13
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            java.lang.String r13 = com.kandian.user.UserInfoCrypto.encrypt(r13)
            if (r13 != 0) goto L_0x0097
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.String r29 = "encrypt failed response is null!"
            com.kandian.common.Log.v(r28, r29)
            com.kandian.user.UserResult r28 = new com.kandian.user.UserResult
            r29 = 2
            java.lang.String r30 = "同步设置失败"
            r28.<init>(r29, r30)
        L_0x0096:
            return r28
        L_0x0097:
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "json encrypt:"
            r29.<init>(r30)
            r0 = r29
            r1 = r13
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            java.lang.StringBuilder r28 = new java.lang.StringBuilder
            r0 = r32
            java.lang.String r0 = r0.baseurl
            r29 = r0
            java.lang.String r29 = java.lang.String.valueOf(r29)
            r28.<init>(r29)
            java.lang.String r29 = "?method=autobind"
            java.lang.StringBuilder r28 = r28.append(r29)
            java.lang.String r27 = r28.toString()
            long r22 = java.lang.System.currentTimeMillis()
            r0 = r27
            r1 = r13
            java.lang.String r18 = com.kandian.common.StringUtil.requestInvoker(r0, r1)
            long r9 = java.lang.System.currentTimeMillis()
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "request address:"
            r29.<init>(r30)
            r0 = r29
            r1 = r27
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r30 = " used "
            java.lang.StringBuilder r29 = r29.append(r30)
            long r30 = r9 - r22
            java.lang.StringBuilder r29 = r29.append(r30)
            java.lang.String r30 = "ms"
            java.lang.StringBuilder r29 = r29.append(r30)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            if (r18 == 0) goto L_0x0114
            java.lang.String r28 = r18.trim()
            int r28 = r28.length()
            if (r28 != 0) goto L_0x013b
        L_0x0114:
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "failed response is null! res:"
            r29.<init>(r30)
            r0 = r29
            r1 = r18
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            com.kandian.user.UserResult r28 = new com.kandian.user.UserResult
            r29 = 2
            java.lang.String r30 = "autobind失败"
            r28.<init>(r29, r30)
            goto L_0x0096
        L_0x013b:
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "res:"
            r29.<init>(r30)
            r0 = r29
            r1 = r18
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            java.lang.String r18 = com.kandian.user.UserInfoCrypto.decrypt(r18)
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "res decrypt:"
            r29.<init>(r30)
            r0 = r29
            r1 = r18
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            r0 = r32
            r1 = r18
            org.json.JSONObject r12 = r0.getJSONObject(r1)
            r19 = 0
            r26 = 0
            r25 = 0
            java.lang.String r28 = "resultCode"
            r0 = r12
            r1 = r28
            java.lang.String r19 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            if (r19 == 0) goto L_0x02ad
            java.lang.String r28 = r19.trim()     // Catch:{ Exception -> 0x02b1 }
            int r28 = r28.length()     // Catch:{ Exception -> 0x02b1 }
            if (r28 <= 0) goto L_0x02ad
            java.lang.String r28 = r19.trim()     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r29 = "1"
            boolean r28 = r28.equals(r29)     // Catch:{ Exception -> 0x02b1 }
            if (r28 == 0) goto L_0x02ad
            java.lang.String r28 = "autoShareAction"
            r0 = r12
            r1 = r28
            java.lang.String r28 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            com.kandian.user.UserService.syncaction = r28     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "login_name"
            r0 = r12
            r1 = r28
            java.lang.String r26 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "password"
            r0 = r12
            r1 = r28
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            r0 = r32
            java.lang.String r0 = r0.TAG     // Catch:{ Exception -> 0x02b1 }
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r30 = "get syncaction:"
            r29.<init>(r30)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r30 = com.kandian.user.UserService.syncaction     // Catch:{ Exception -> 0x02b1 }
            java.lang.StringBuilder r29 = r29.append(r30)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r29 = r29.toString()     // Catch:{ Exception -> 0x02b1 }
            com.kandian.common.Log.v(r28, r29)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "loginBindList"
            r0 = r12
            r1 = r28
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            if (r14 == 0) goto L_0x020b
            java.lang.String r28 = r14.trim()     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r29 = "[]"
            boolean r28 = r28.equals(r29)     // Catch:{ Exception -> 0x02b1 }
            if (r28 != 0) goto L_0x020b
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ Exception -> 0x02b1 }
            r5.<init>(r14)     // Catch:{ Exception -> 0x02b1 }
            if (r5 == 0) goto L_0x020b
            int r28 = r5.length()     // Catch:{ Exception -> 0x02b1 }
            if (r28 <= 0) goto L_0x020b
            r11 = 0
        L_0x0202:
            int r28 = r5.length()     // Catch:{ Exception -> 0x02b1 }
            r0 = r11
            r1 = r28
            if (r0 < r1) goto L_0x0232
        L_0x020b:
            r24 = 1
        L_0x020d:
            if (r19 == 0) goto L_0x0219
            java.lang.String r28 = r19.trim()
            int r28 = r28.length()
            if (r28 != 0) goto L_0x021b
        L_0x0219:
            java.lang.String r19 = "0"
        L_0x021b:
            java.lang.String r28 = r19.trim()
            java.lang.String r29 = "3"
            boolean r28 = r28.equals(r29)
            if (r28 == 0) goto L_0x02bb
            com.kandian.user.UserResult r28 = new com.kandian.user.UserResult
            r29 = 3
            java.lang.String r30 = "用户不存在"
            r28.<init>(r29, r30)
            goto L_0x0096
        L_0x0232:
            org.json.JSONObject r6 = r5.getJSONObject(r11)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "shareName"
            r0 = r6
            r1 = r28
            java.lang.String r17 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "sharePassword"
            r0 = r6
            r1 = r28
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "shareType"
            r0 = r6
            r1 = r28
            java.lang.String r16 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "access_token"
            r0 = r6
            r1 = r28
            java.lang.String r3 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            java.lang.String r28 = "access_token_secret"
            r0 = r6
            r1 = r28
            java.lang.String r4 = r0.getString(r1)     // Catch:{ Exception -> 0x02b1 }
            com.kandian.user.ShareObject r21 = new com.kandian.user.ShareObject     // Catch:{ Exception -> 0x02b1 }
            r21.<init>()     // Catch:{ Exception -> 0x02b1 }
            r0 = r21
            r1 = r36
            r0.setSharepassword(r1)     // Catch:{ Exception -> 0x02b1 }
            r0 = r21
            r1 = r35
            r0.setShareusername(r1)     // Catch:{ Exception -> 0x02b1 }
            int r28 = java.lang.Integer.parseInt(r16)     // Catch:{ Exception -> 0x02b1 }
            r0 = r21
            r1 = r28
            r0.setSharetype(r1)     // Catch:{ Exception -> 0x02b1 }
            int r28 = java.lang.Integer.parseInt(r16)     // Catch:{ Exception -> 0x02b1 }
            r0 = r32
            r1 = r28
            java.lang.String r28 = r0.getShareName(r1)     // Catch:{ Exception -> 0x02b1 }
            r0 = r21
            r1 = r28
            r0.setSharename(r1)     // Catch:{ Exception -> 0x02b1 }
            r0 = r21
            r1 = r3
            r0.setAccess_token(r1)     // Catch:{ Exception -> 0x02b1 }
            r0 = r21
            r1 = r4
            r0.setAccess_token_secret(r1)     // Catch:{ Exception -> 0x02b1 }
            r0 = r32
            r1 = r21
            r2 = r38
            r0.addShareObject(r1, r2)     // Catch:{ Exception -> 0x02b1 }
            int r11 = r11 + 1
            goto L_0x0202
        L_0x02ad:
            r24 = 0
            goto L_0x020d
        L_0x02b1:
            r28 = move-exception
            r7 = r28
            r24 = 0
            r7.printStackTrace()
            goto L_0x020d
        L_0x02bb:
            r0 = r32
            java.lang.String r0 = r0.TAG
            r28 = r0
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = "succuss:"
            r29.<init>(r30)
            r0 = r29
            r1 = r24
            java.lang.StringBuilder r29 = r0.append(r1)
            java.lang.String r29 = r29.toString()
            com.kandian.common.Log.v(r28, r29)
            if (r24 == 0) goto L_0x0385
            r0 = r26
            r1 = r32
            r1.username = r0
            r0 = r25
            r1 = r32
            r1.password = r0
            r0 = r32
            java.lang.String r0 = r0.username
            r28 = r0
            r0 = r28
            r1 = r32
            r1.username = r0
            r0 = r32
            java.lang.String r0 = r0.password
            r28 = r0
            r0 = r28
            r1 = r32
            r1.password = r0
            if (r38 == 0) goto L_0x037a
            java.lang.String r28 = "KuaiShouUserService"
            r29 = 0
            r0 = r38
            r1 = r28
            r2 = r29
            android.content.SharedPreferences r20 = r0.getSharedPreferences(r1, r2)
            android.content.SharedPreferences$Editor r8 = r20.edit()
            java.lang.String r28 = "username"
            r0 = r8
            r1 = r28
            r0.remove(r1)
            java.lang.String r28 = "password"
            r0 = r8
            r1 = r28
            r0.remove(r1)
            java.lang.String r28 = "syncaction"
            r0 = r8
            r1 = r28
            r0.remove(r1)
            java.lang.String r28 = "usersharetype"
            r0 = r8
            r1 = r28
            r0.remove(r1)
            r8.commit()
            java.lang.String r28 = "username"
            r0 = r32
            java.lang.String r0 = r0.username
            r29 = r0
            r0 = r8
            r1 = r28
            r2 = r29
            r0.putString(r1, r2)
            java.lang.String r28 = "password"
            r0 = r32
            java.lang.String r0 = r0.password
            r29 = r0
            r0 = r8
            r1 = r28
            r2 = r29
            r0.putString(r1, r2)
            java.lang.String r28 = "syncaction"
            java.lang.String r29 = com.kandian.user.UserService.syncaction
            r0 = r8
            r1 = r28
            r2 = r29
            r0.putString(r1, r2)
            java.lang.String r28 = "usersharetype"
            java.lang.StringBuilder r29 = new java.lang.StringBuilder
            java.lang.String r30 = java.lang.String.valueOf(r37)
            r29.<init>(r30)
            java.lang.String r29 = r29.toString()
            r0 = r8
            r1 = r28
            r2 = r29
            r0.putString(r1, r2)
            r8.commit()
        L_0x037a:
            com.kandian.user.UserResult r28 = new com.kandian.user.UserResult
            r29 = 1
            java.lang.String r30 = "登录成功"
            r28.<init>(r29, r30)
            goto L_0x0096
        L_0x0385:
            com.kandian.user.UserResult r28 = new com.kandian.user.UserResult
            r29 = 2
            java.lang.String r30 = "登录失败,请检查用户名密码是否填写正确!"
            r28.<init>(r29, r30)
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.user.UserService.autobind(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context, int):com.kandian.user.UserResult");
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x02c6  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0362  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.kandian.user.UserResult autocreatebind(java.lang.String r41, java.lang.String r42, java.lang.String r43, java.lang.String r44, int r45, android.content.Context r46) {
        /*
            r40 = this;
            r28 = 0
            java.lang.String r17 = android.os.Build.MANUFACTURER
            if (r17 != 0) goto L_0x0008
            java.lang.String r17 = ""
        L_0x0008:
            r0 = r40
            r1 = r46
            java.lang.String r7 = r0.getdeviceid(r1)
            if (r7 != 0) goto L_0x0014
            java.lang.String r7 = ""
        L_0x0014:
            java.lang.String r18 = android.os.Build.MODEL
            if (r18 != 0) goto L_0x001a
            java.lang.String r18 = ""
        L_0x001a:
            r0 = r40
            r1 = r46
            java.lang.String r35 = r0.getUUID(r1)
            r0 = r40
            r1 = r46
            java.lang.String r16 = r0.getmac(r1)
            java.lang.StringBuilder r36 = new java.lang.StringBuilder
            java.lang.String r37 = "{\"shareusername\":\""
            r36.<init>(r37)
            r0 = r36
            r1 = r41
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"sharepassword\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r42
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"access_token\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r43
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"access_token_secret\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r44
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"sharetype\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r45
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"manufacturer\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r17
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"deviceid\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r7
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"model\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r18
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"mac\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r16
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\",\"uuid\":\""
            java.lang.StringBuilder r36 = r36.append(r37)
            r0 = r36
            r1 = r35
            java.lang.StringBuilder r36 = r0.append(r1)
            java.lang.String r37 = "\"}"
            java.lang.StringBuilder r36 = r36.append(r37)
            java.lang.String r14 = r36.toString()
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = "json:"
            r37.<init>(r38)
            r0 = r37
            r1 = r14
            java.lang.StringBuilder r37 = r0.append(r1)
            java.lang.String r37 = r37.toString()
            com.kandian.common.Log.v(r36, r37)
            java.lang.String r14 = com.kandian.user.UserInfoCrypto.encrypt(r14)
            if (r14 != 0) goto L_0x00f6
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.String r37 = "encrypt failed response is null!"
            com.kandian.common.Log.v(r36, r37)
            com.kandian.user.UserResult r36 = new com.kandian.user.UserResult
            r37 = 2
            java.lang.String r38 = "autocreatebind失败"
            r36.<init>(r37, r38)
        L_0x00f5:
            return r36
        L_0x00f6:
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = "json encrypt:"
            r37.<init>(r38)
            r0 = r37
            r1 = r14
            java.lang.StringBuilder r37 = r0.append(r1)
            java.lang.String r37 = r37.toString()
            com.kandian.common.Log.v(r36, r37)
            java.lang.StringBuilder r36 = new java.lang.StringBuilder
            r0 = r40
            java.lang.String r0 = r0.baseurl
            r37 = r0
            java.lang.String r37 = java.lang.String.valueOf(r37)
            r36.<init>(r37)
            java.lang.String r37 = "?method=autocreatebind"
            java.lang.StringBuilder r36 = r36.append(r37)
            java.lang.String r32 = r36.toString()
            long r26 = java.lang.System.currentTimeMillis()
            r0 = r32
            r1 = r14
            java.lang.String r22 = com.kandian.common.StringUtil.requestInvoker(r0, r1)
            long r10 = java.lang.System.currentTimeMillis()
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = "request address:"
            r37.<init>(r38)
            r0 = r37
            r1 = r32
            java.lang.StringBuilder r37 = r0.append(r1)
            java.lang.String r38 = " used "
            java.lang.StringBuilder r37 = r37.append(r38)
            long r38 = r10 - r26
            java.lang.StringBuilder r37 = r37.append(r38)
            java.lang.String r38 = "ms"
            java.lang.StringBuilder r37 = r37.append(r38)
            java.lang.String r37 = r37.toString()
            com.kandian.common.Log.v(r36, r37)
            if (r22 == 0) goto L_0x0173
            java.lang.String r36 = r22.trim()
            int r36 = r36.length()
            if (r36 != 0) goto L_0x019a
        L_0x0173:
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = "failed response is null! res:"
            r37.<init>(r38)
            r0 = r37
            r1 = r22
            java.lang.StringBuilder r37 = r0.append(r1)
            java.lang.String r37 = r37.toString()
            com.kandian.common.Log.v(r36, r37)
            com.kandian.user.UserResult r36 = new com.kandian.user.UserResult
            r37 = 2
            java.lang.String r38 = "autocreatebind失败"
            r36.<init>(r37, r38)
            goto L_0x00f5
        L_0x019a:
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = "res:"
            r37.<init>(r38)
            r0 = r37
            r1 = r22
            java.lang.StringBuilder r37 = r0.append(r1)
            java.lang.String r37 = r37.toString()
            com.kandian.common.Log.v(r36, r37)
            java.lang.String r22 = com.kandian.user.UserInfoCrypto.decrypt(r22)
            r0 = r40
            java.lang.String r0 = r0.TAG
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder
            java.lang.String r38 = "res decrypt:"
            r37.<init>(r38)
            r0 = r37
            r1 = r22
            java.lang.StringBuilder r37 = r0.append(r1)
            java.lang.String r37 = r37.toString()
            com.kandian.common.Log.v(r36, r37)
            r0 = r40
            r1 = r22
            org.json.JSONObject r13 = r0.getJSONObject(r1)
            r23 = 0
            r31 = 0
            r30 = 0
            r29 = 0
            java.lang.String r36 = "resultCode"
            r0 = r13
            r1 = r36
            java.lang.String r23 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            if (r23 == 0) goto L_0x035e
            java.lang.String r36 = r23.trim()     // Catch:{ Exception -> 0x02d8 }
            int r36 = r36.length()     // Catch:{ Exception -> 0x02d8 }
            if (r36 <= 0) goto L_0x035e
            java.lang.String r36 = r23.trim()     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r37 = "1"
            boolean r36 = r36.equals(r37)     // Catch:{ Exception -> 0x02d8 }
            if (r36 == 0) goto L_0x035e
            java.lang.String r36 = "autoShareAction"
            r0 = r13
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            com.kandian.user.UserService.syncaction = r36     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "login_name"
            r0 = r13
            r1 = r36
            java.lang.String r31 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "password"
            r0 = r13
            r1 = r36
            java.lang.String r30 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "lastlogin_time"
            r0 = r13
            r1 = r36
            java.lang.String r29 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            com.kandian.user.account.UserAccountHelper r33 = com.kandian.user.account.UserAccountHelper.getInstance()     // Catch:{ Exception -> 0x02d1 }
            r0 = r33
            r1 = r46
            r0.init(r1)     // Catch:{ Exception -> 0x02d1 }
            r0 = r46
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Exception -> 0x02d1 }
            r5 = r0
            r0 = r33
            r1 = r5
            com.kandian.user.account.UserInfo r34 = r0.getAccount(r1)     // Catch:{ Exception -> 0x02d1 }
            r0 = r40
            java.lang.String r0 = r0.TAG     // Catch:{ Exception -> 0x02d1 }
            r36 = r0
            java.lang.String r37 = "addAccount"
            com.kandian.common.Log.v(r36, r37)     // Catch:{ Exception -> 0x02d1 }
            r36 = 0
            r0 = r46
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Exception -> 0x02d1 }
            r5 = r0
            r0 = r33
            r1 = r31
            r2 = r30
            r3 = r36
            r4 = r5
            r0.addAccount(r1, r2, r3, r4)     // Catch:{ Exception -> 0x02d1 }
        L_0x0262:
            r0 = r40
            java.lang.String r0 = r0.TAG     // Catch:{ Exception -> 0x02d8 }
            r36 = r0
            java.lang.StringBuilder r37 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r38 = "get syncaction:"
            r37.<init>(r38)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r38 = com.kandian.user.UserService.syncaction     // Catch:{ Exception -> 0x02d8 }
            java.lang.StringBuilder r37 = r37.append(r38)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r37 = r37.toString()     // Catch:{ Exception -> 0x02d8 }
            com.kandian.common.Log.v(r36, r37)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "loginBindList"
            r0 = r13
            r1 = r36
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            if (r15 == 0) goto L_0x02aa
            java.lang.String r36 = r15.trim()     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r37 = "[]"
            boolean r36 = r36.equals(r37)     // Catch:{ Exception -> 0x02d8 }
            if (r36 != 0) goto L_0x02aa
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ Exception -> 0x02d8 }
            r5.<init>(r15)     // Catch:{ Exception -> 0x02d8 }
            if (r5 == 0) goto L_0x02aa
            int r36 = r5.length()     // Catch:{ Exception -> 0x02d8 }
            if (r36 <= 0) goto L_0x02aa
            r12 = 0
        L_0x02a1:
            int r36 = r5.length()     // Catch:{ Exception -> 0x02d8 }
            r0 = r12
            r1 = r36
            if (r0 < r1) goto L_0x02e1
        L_0x02aa:
            r28 = 1
        L_0x02ac:
            if (r23 == 0) goto L_0x02b8
            java.lang.String r36 = r23.trim()
            int r36 = r36.length()
            if (r36 != 0) goto L_0x02ba
        L_0x02b8:
            java.lang.String r23 = "0"
        L_0x02ba:
            java.lang.String r36 = r23.trim()
            java.lang.String r37 = "3"
            boolean r36 = r36.equals(r37)
            if (r36 == 0) goto L_0x0362
            com.kandian.user.UserResult r36 = new com.kandian.user.UserResult
            r37 = 3
            java.lang.String r38 = "用户不存在"
            r36.<init>(r37, r38)
            goto L_0x00f5
        L_0x02d1:
            r36 = move-exception
            r8 = r36
            r8.printStackTrace()     // Catch:{ Exception -> 0x02d8 }
            goto L_0x0262
        L_0x02d8:
            r36 = move-exception
            r8 = r36
            r28 = 0
            r8.printStackTrace()
            goto L_0x02ac
        L_0x02e1:
            org.json.JSONObject r6 = r5.getJSONObject(r12)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "shareName"
            r0 = r6
            r1 = r36
            java.lang.String r21 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "sharePassword"
            r0 = r6
            r1 = r36
            java.lang.String r19 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "shareType"
            r0 = r6
            r1 = r36
            java.lang.String r20 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            com.kandian.user.ShareObject r25 = new com.kandian.user.ShareObject     // Catch:{ Exception -> 0x02d8 }
            r25.<init>()     // Catch:{ Exception -> 0x02d8 }
            r0 = r25
            r1 = r19
            r0.setSharepassword(r1)     // Catch:{ Exception -> 0x02d8 }
            r0 = r25
            r1 = r21
            r0.setShareusername(r1)     // Catch:{ Exception -> 0x02d8 }
            int r36 = java.lang.Integer.parseInt(r20)     // Catch:{ Exception -> 0x02d8 }
            r0 = r25
            r1 = r36
            r0.setSharetype(r1)     // Catch:{ Exception -> 0x02d8 }
            int r36 = java.lang.Integer.parseInt(r20)     // Catch:{ Exception -> 0x02d8 }
            r0 = r40
            r1 = r36
            java.lang.String r36 = r0.getShareName(r1)     // Catch:{ Exception -> 0x02d8 }
            r0 = r25
            r1 = r36
            r0.setSharename(r1)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "access_token"
            r0 = r6
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            r0 = r25
            r1 = r36
            r0.setAccess_token(r1)     // Catch:{ Exception -> 0x02d8 }
            java.lang.String r36 = "access_token_secret"
            r0 = r6
            r1 = r36
            java.lang.String r36 = r0.getString(r1)     // Catch:{ Exception -> 0x02d8 }
            r0 = r25
            r1 = r36
            r0.setAccess_token_secret(r1)     // Catch:{ Exception -> 0x02d8 }
            r0 = r40
            r1 = r25
            r2 = r46
            r0.addShareObject(r1, r2)     // Catch:{ Exception -> 0x02d8 }
            int r12 = r12 + 1
            goto L_0x02a1
        L_0x035e:
            r28 = 0
            goto L_0x02ac
        L_0x0362:
            if (r28 == 0) goto L_0x03f3
            r0 = r31
            r1 = r40
            r1.username = r0
            r0 = r30
            r1 = r40
            r1.password = r0
            r0 = r29
            r1 = r40
            r1.lastlogin_time = r0
            if (r46 == 0) goto L_0x03e8
            java.lang.String r36 = "KuaiShouUserService"
            r37 = 0
            r0 = r46
            r1 = r36
            r2 = r37
            android.content.SharedPreferences r24 = r0.getSharedPreferences(r1, r2)
            android.content.SharedPreferences$Editor r9 = r24.edit()
            java.lang.String r36 = "username"
            r0 = r9
            r1 = r36
            r0.remove(r1)
            java.lang.String r36 = "password"
            r0 = r9
            r1 = r36
            r0.remove(r1)
            java.lang.String r36 = "syncaction"
            r0 = r9
            r1 = r36
            r0.remove(r1)
            java.lang.String r36 = "usersharetype"
            r0 = r9
            r1 = r36
            r0.remove(r1)
            r9.commit()
            java.lang.String r36 = "username"
            r0 = r40
            java.lang.String r0 = r0.username
            r37 = r0
            r0 = r9
            r1 = r36
            r2 = r37
            r0.putString(r1, r2)
            java.lang.String r36 = "password"
            r0 = r40
            java.lang.String r0 = r0.password
            r37 = r0
            r0 = r9
            r1 = r36
            r2 = r37
            r0.putString(r1, r2)
            java.lang.String r36 = "syncaction"
            java.lang.String r37 = com.kandian.user.UserService.syncaction
            r0 = r9
            r1 = r36
            r2 = r37
            r0.putString(r1, r2)
            java.lang.String r36 = "usersharetype"
            java.lang.String r37 = "0"
            r0 = r9
            r1 = r36
            r2 = r37
            r0.putString(r1, r2)
            r9.commit()
        L_0x03e8:
            com.kandian.user.UserResult r36 = new com.kandian.user.UserResult
            r37 = 1
            java.lang.String r38 = "登录成功"
            r36.<init>(r37, r38)
            goto L_0x00f5
        L_0x03f3:
            com.kandian.user.UserResult r36 = new com.kandian.user.UserResult
            r37 = 2
            java.lang.String r38 = "登录失败,请检查用户名密码是否填写正确!"
            r36.<init>(r37, r38)
            goto L_0x00f5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.user.UserService.autocreatebind(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):com.kandian.user.UserResult");
    }

    public UserResult syncAction(String username2, String syncaction2, Context context) {
        if (!check()) {
            return new UserResult(2, "同步设置失败");
        }
        String json = "{\"username\":\"" + username2 + "\",\"autoshareaction\":\"" + syncaction2 + "\"}";
        Log.v(this.TAG, "json:" + json);
        String json2 = UserInfoCrypto.encrypt(json);
        if (json2 == null) {
            Log.v(this.TAG, "encrypt failed response is null!");
            return new UserResult(2, "同步设置失败");
        }
        Log.v(this.TAG, "json encrypt:" + json2);
        String url = String.valueOf(this.baseurl) + "?method=autoShare";
        long start = System.currentTimeMillis();
        String response = StringUtil.requestInvoker(url, json2);
        Log.v(this.TAG, "request address:" + url + " used " + (System.currentTimeMillis() - start) + "ms");
        if (response == null || response.trim().length() == 0) {
            Log.v(this.TAG, "failed response is null! res:" + response);
            return new UserResult(2, "同步设置失败");
        }
        Log.v(this.TAG, "response:" + response);
        String response2 = UserInfoCrypto.decrypt(response);
        Log.v(this.TAG, "response decrypt:" + response2);
        JSONObject jo = getJSONObject(response2);
        try {
            int result = jo.getInt("resultCode");
            if (result != 1) {
                String message = jo.getString("message");
                Log.v(this.TAG, "syancaction failed " + result);
                return new UserResult(2, "同步设置失败");
            }
            Log.v(this.TAG, "syancaction success " + result);
            syncaction = syncaction2;
            UserResult userResult = new UserResult(result, "同步设置成功");
            SharedPreferences.Editor editor = context.getSharedPreferences("KuaiShouUserService", 0).edit();
            editor.putString("syncaction", syncaction2);
            editor.commit();
            return userResult;
        } catch (JSONException e) {
            e.printStackTrace();
            return new UserResult(2, "同步设置失败");
        }
    }

    public boolean userlogin(Context context) {
        String username2;
        String password2;
        int usersharetype2;
        try {
            Log.v("UserService", "begin login");
            UserService userService = getInstance();
            SharedPreferences settings = context.getSharedPreferences("KuaiShouUserService", 0);
            UserAccountHelper userAccountHelper = UserAccountHelper.getInstance();
            userAccountHelper.init(context);
            UserInfo userInfo = userAccountHelper.getAccount((Activity) context);
            if (userInfo == null) {
                username2 = settings.getString("username", "");
                password2 = settings.getString("password", "");
                usersharetype2 = Integer.parseInt(settings.getString("usersharetype", "0"));
            } else {
                username2 = userInfo.getUsername();
                password2 = userInfo.getPassword();
                usersharetype2 = Integer.parseInt(userInfo.getShareType());
            }
            if (username2 == null || username2.trim().length() == 0 || password2 == null || password2.trim().length() == 0) {
                Log.v("UserService", "username and password is null");
                return false;
            }
            UserResult ur = userService.verification(username2, password2, usersharetype2, context);
            if (ur.getResultcode() == 1) {
                userService.setUsersharetype(usersharetype2);
                userService.setSyncaction(syncaction);
                Log.v("UserService", "login success " + ur.getResultcode());
                return true;
            }
            userService.setUsername(null);
            userService.setPassword(null);
            userService.setUsersharetype(0);
            userService.setSyncaction(null);
            Log.v("UserService", "login failed " + ur.getResultcode());
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void autologin(final Context context) {
        if (!isneedlogined) {
            Log.v("UserService", "islogined");
            return;
        }
        isneedlogined = false;
        if (context instanceof Activity) {
            new Thread() {
                public void run() {
                    try {
                        Log.v("UserService", "begin auto login");
                        UserService.getInstance().userlogin(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    private void saveHistoryUserInfo(Context context, String username2, String password2) {
        SharedPreferences.Editor his_editor = context.getSharedPreferences(PROKEY_HISTORY, 0).edit();
        his_editor.putString("his_username", username2);
        his_editor.putString("his_password", password2);
        his_editor.commit();
    }

    public String getdeviceid(Context context) {
        String deviceid = null;
        try {
            deviceid = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (deviceid == null) {
            return "";
        }
        return deviceid;
    }

    public String getmac(Context context) {
        try {
            WifiInfo info = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            Log.v(this.TAG, "getMacAddress = " + info.getMacAddress());
            return info.getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
