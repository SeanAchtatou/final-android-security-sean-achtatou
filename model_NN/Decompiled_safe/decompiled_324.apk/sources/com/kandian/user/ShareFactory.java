package com.kandian.user;

import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.user.share.QQWeiboService;
import com.kandian.user.share.SinaWeiboBasicService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class ShareFactory {
    /* access modifiers changed from: private */
    public static String TAG = "ShareFactory";
    private static ShareFactory ourInstance = new ShareFactory();

    private ShareFactory() {
    }

    public static ShareFactory getInstance() {
        return ourInstance;
    }

    public ShareService getShareService(ShareObject so) {
        if (so.getSharetype() == 0) {
            return null;
        }
        ShareService shareService = null;
        if (so.getSharetype() == 1) {
            shareService = SinaWeiboBasicService.getInstance();
        } else if (so.getSharetype() == 2) {
            shareService = QQWeiboService.getInstance();
        }
        return shareService;
    }

    public ShareService getShareService(int sharetype) {
        if (sharetype == 0) {
            return null;
        }
        ShareService shareService = null;
        if (sharetype == 1) {
            shareService = SinaWeiboBasicService.getInstance();
        } else if (sharetype == 2) {
            shareService = QQWeiboService.getInstance();
        }
        return shareService;
    }

    public void sync(final String content, int syncaction, final Activity activity, final String imageUrl) {
        String sa;
        Log.v(TAG, "imageUrl:" + imageUrl);
        UserService us = UserService.getInstance();
        if (us != null && (sa = us.getSyncaction()) != null && sa.trim().length() != 0) {
            ((QQWeiboService) QQWeiboService.getInstance()).init(activity.getString(R.string.qqCustomKey), activity.getString(R.string.qqCustomSecrect));
            if (sa.indexOf(String.valueOf(syncaction)) > -1) {
                Asynchronous asynchronous = new Asynchronous(activity);
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> map) throws Exception {
                        UserService us = UserService.getInstance();
                        ArrayList<ShareObject> result = new ArrayList<>();
                        ArrayList<ShareObject> list = us.getShareList();
                        ShareContent sc = new ShareContent(content, imageUrl);
                        Log.v(ShareFactory.TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@ list.size():" + list.size());
                        if (list != null && list.size() > 0) {
                            Iterator<ShareObject> it = list.iterator();
                            while (it.hasNext()) {
                                ShareObject so = it.next();
                                Log.v(ShareFactory.TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@ send Sharename" + so.getSharename() + " Sharetype=" + so.getSharetype());
                                String res = ShareFactory.this.send(sc, activity, so);
                                Log.v(ShareFactory.TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@ res" + res);
                                if (res != null) {
                                    result.add(so);
                                }
                            }
                        }
                        setCallbackParameter("result", result);
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        ArrayList<ShareObject> result = (ArrayList) outputparameter.get("result");
                        if (result != null && result.size() != 0) {
                            UserService us = UserService.getInstance();
                            String msg = "成功同步到";
                            for (int i = 0; i < result.size(); i++) {
                                ShareObject so = (ShareObject) result.get(i);
                                if (i == 0) {
                                    msg = String.valueOf(msg) + us.getShareName(so.getSharetype());
                                } else {
                                    msg = String.valueOf(msg) + "," + us.getShareName(so.getSharetype());
                                }
                            }
                            Toast.makeText(activity, msg, 0).show();
                        }
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                    }
                });
                asynchronous.start();
                return;
            }
            Log.v(TAG, "Syncaction is " + syncaction + " sa=" + sa);
        }
    }

    /* access modifiers changed from: private */
    public String send(ShareContent content, Activity activity, ShareObject so) {
        try {
            UserService instance = UserService.getInstance();
            ShareService ss = getShareService(so.getSharetype());
            Log.v(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ShareService " + ss);
            return ss.update(so.getShareusername(), so.getSharepassword(), content);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
