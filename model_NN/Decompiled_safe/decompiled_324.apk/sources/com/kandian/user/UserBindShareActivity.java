package com.kandian.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.Map;

public class UserBindShareActivity extends Activity {
    /* access modifiers changed from: private */
    public String TAG = "UserBindShareActivity";
    /* access modifiers changed from: private */
    public UserBindShareActivity context = this;
    boolean needcreatefriend = true;
    /* access modifiers changed from: private */
    public String needonebyone = "0";
    /* access modifiers changed from: private */
    public String shareName = "";
    /* access modifiers changed from: private */
    public int shareType = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.usersharebinding);
        AutoCompleteBuilder.build(R.id.username_edit_txt, this.context);
        ((Button) findViewById(R.id.back_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Log.v(UserBindShareActivity.this.TAG, "needonebyone is " + UserBindShareActivity.this.needonebyone);
                Intent intent = new Intent();
                intent.setClass(UserBindShareActivity.this.context, WeiboPromptActivity.class);
                UserBindShareActivity.this.startActivity(intent);
            }
        });
        UserService userService = UserService.getInstance();
        Intent intent = getIntent();
        this.shareType = Integer.parseInt(intent.getStringExtra("sharetype"));
        this.needonebyone = intent.getStringExtra("needonebyone");
        Log.v(this.TAG, "needonebyone is " + this.needonebyone);
        Log.v(this.TAG, "shareType is " + this.shareType);
        ((TextView) findViewById(R.id.title_share)).setText("请输入" + userService.getShareName(this.shareType) + "的帐号密码进行绑定");
        ((Button) findViewById(R.id.binding_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String username = ((AutoCompleteTextView) UserBindShareActivity.this.findViewById(R.id.username_edit_txt)).getText().toString();
                final String password = ((EditText) UserBindShareActivity.this.findViewById(R.id.password_edit_txt)).getText().toString();
                String againpwd = ((EditText) UserBindShareActivity.this.findViewById(R.id.password_edit_again_txt)).getText().toString();
                if (username == null || username.trim().length() == 0 || password == null || password.trim().length() == 0 || againpwd == null || againpwd.trim().length() == 0) {
                    Toast.makeText(UserBindShareActivity.this.context, UserBindShareActivity.this.getString(R.string.str_reg_alert), 0).show();
                } else if (!password.trim().equals(againpwd.trim())) {
                    Toast.makeText(UserBindShareActivity.this.context, UserBindShareActivity.this.getString(R.string.str_pwd_repeat_error), 0).show();
                } else {
                    Asynchronous asynchronous = new Asynchronous(UserBindShareActivity.this.context);
                    asynchronous.setLoadingMsg("绑定中,请稍等...");
                    asynchronous.asyncProcess(new AsyncProcess() {
                        public int process(Context c, Map<String, Object> map) {
                            setCallbackParameter("UserResult", UserService.getInstance().binding(UserBindShareActivity.this.shareType, UserBindShareActivity.this.shareName, username, password, c, null, null));
                            setCallbackParameter("username", username);
                            setCallbackParameter("password", password);
                            return 0;
                        }
                    });
                    asynchronous.asyncCallback(new AsyncCallback() {
                        public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                            UserService userService = UserService.getInstance();
                            UserResult userResult = (UserResult) outputparameter.get("UserResult");
                            if (userResult.getResultcode() == 1) {
                                Toast.makeText(UserBindShareActivity.this.context, "绑定成功!", 0).show();
                                if (UserBindShareActivity.this.needcreatefriend) {
                                    userService.createFriendship((String) outputparameter.get("username"), (String) outputparameter.get("password"), UserBindShareActivity.this.shareType, UserBindShareActivity.this.context);
                                }
                                Log.v(UserBindShareActivity.this.TAG, "needonebyone is " + UserBindShareActivity.this.needonebyone);
                                if (UserBindShareActivity.this.needonebyone != null && UserBindShareActivity.this.needonebyone.trim().equals("0")) {
                                    UserBindShareActivity.this.bindSyncShare();
                                    Intent intent = new Intent();
                                    intent.setClass(c, UserActivity.class);
                                    UserBindShareActivity.this.startActivity(intent);
                                } else if (userService.getNextShareObject(UserBindShareActivity.this.shareType) != null) {
                                    Intent intent2 = new Intent();
                                    intent2.setClass(c, WeiboPromptActivity.class);
                                    intent2.putExtra("text", UserBindShareActivity.this.getString(R.string.str_qq_prompt));
                                    intent2.putExtra("action", "qq");
                                    UserBindShareActivity.this.startActivity(intent2);
                                } else {
                                    UserBindShareActivity.this.bindSyncShare();
                                    Intent intent3 = new Intent();
                                    intent3.setClass(UserBindShareActivity.this.context, UserActivity.class);
                                    UserBindShareActivity.this.startActivity(intent3);
                                }
                            } else if (userResult.getResultcode() == 4) {
                                Toast.makeText(UserBindShareActivity.this.context, "您绑定的" + userService.getShareName(UserBindShareActivity.this.shareType) + "用户已经被绑定过,绑定失败 !", 0).show();
                            } else if (userResult.getResultcode() == 2) {
                                Toast.makeText(UserBindShareActivity.this.context, "您已经绑定过该分享,绑定失败", 0).show();
                            } else {
                                Toast.makeText(UserBindShareActivity.this.context, "绑定失败", 0).show();
                            }
                        }
                    });
                    asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                        public void handle(Context c, Exception e, Message m) {
                            Toast.makeText(c, "网络问题,绑定失败", 0).show();
                        }
                    });
                    asynchronous.start();
                }
            }
        });
        CheckBox needcreatefriend_cb = (CheckBox) findViewById(R.id.needcreatefriend);
        needcreatefriend_cb.setChecked(true);
        needcreatefriend_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UserBindShareActivity.this.needcreatefriend = isChecked;
            }
        });
    }

    /* access modifiers changed from: private */
    public void bindSyncShare() {
        try {
            Log.v(this.TAG, "bindSyncShare!!");
            UserService userService = UserService.getInstance();
            UserResult syncAction = userService.syncAction(userService.getUsername(), "1,2", this.context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
