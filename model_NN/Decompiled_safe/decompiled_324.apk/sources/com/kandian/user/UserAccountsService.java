package com.kandian.user;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.kandian.user.account.UserAccountHelper;

public class UserAccountsService extends Service {
    public IBinder onBind(Intent intent) {
        UserAccountHelper userAccountHelper = UserAccountHelper.getInstance();
        userAccountHelper.init(this);
        return userAccountHelper.getIBinder(this, intent);
    }
}
