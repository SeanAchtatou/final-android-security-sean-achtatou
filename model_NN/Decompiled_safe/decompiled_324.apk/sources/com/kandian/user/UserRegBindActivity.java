package com.kandian.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.Map;

public class UserRegBindActivity extends Activity {
    private static final int PLEASE_READ_SERVICE = 2;
    private static final int TERMS_OF_SERVICE = 1;
    /* access modifiers changed from: private */
    public String TAG = "UserRegActivity";
    /* access modifiers changed from: private */
    public UserRegBindActivity context = this;
    /* access modifiers changed from: private */
    public boolean isreadservice = false;
    /* access modifiers changed from: private */
    public String sharepassword;
    /* access modifiers changed from: private */
    public int sharetype;
    /* access modifiers changed from: private */
    public String shareusername;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.userreg);
        Intent intent = getIntent();
        this.shareusername = intent.getStringExtra("shareusername");
        this.sharepassword = intent.getStringExtra("sharepassword");
        this.sharetype = Integer.parseInt(intent.getStringExtra("sharetype"));
        ((AutoCompleteTextView) findViewById(R.id.username_edit_txt)).setText(this.shareusername);
        ((EditText) findViewById(R.id.password_edit_txt)).setText(this.sharepassword);
        ((EditText) findViewById(R.id.password_edit_again_txt)).setText(this.sharepassword);
        ((CheckBox) findViewById(R.id.terms_of_service_cb)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UserRegBindActivity.this.isreadservice = isChecked;
                Log.v(UserRegBindActivity.this.TAG, "CheckBox " + UserRegBindActivity.this.isreadservice);
                UserRegBindActivity.this.showDialog(1);
            }
        });
        AutoCompleteBuilder.build(R.id.username_edit_txt, this.context);
        ((Button) findViewById(R.id.back_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserRegBindActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.reg_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String ksusername = ((AutoCompleteTextView) UserRegBindActivity.this.findViewById(R.id.username_edit_txt)).getText().toString();
                final String kspassword = ((EditText) UserRegBindActivity.this.findViewById(R.id.password_edit_txt)).getText().toString();
                String againpwd = ((EditText) UserRegBindActivity.this.findViewById(R.id.password_edit_again_txt)).getText().toString();
                if (ksusername == null || ksusername.trim().length() == 0 || kspassword == null || kspassword.trim().length() == 0 || againpwd == null || againpwd.trim().length() == 0) {
                    Toast.makeText(UserRegBindActivity.this.context, UserRegBindActivity.this.getString(R.string.str_reg_alert), 0).show();
                } else if (!kspassword.trim().equals(againpwd.trim())) {
                    Toast.makeText(UserRegBindActivity.this.context, UserRegBindActivity.this.getString(R.string.str_pwd_repeat_error), 0).show();
                } else if (!UserRegBindActivity.this.isreadservice) {
                    UserRegBindActivity.this.showDialog(2);
                } else {
                    Asynchronous asynchronous = new Asynchronous(UserRegBindActivity.this.context);
                    asynchronous.setLoadingMsg("注册中,请稍等...");
                    asynchronous.asyncProcess(new AsyncProcess() {
                        public int process(Context c, Map<String, Object> map) {
                            setCallbackParameter("UserResult", UserService.getInstance().autobind(ksusername, kspassword, UserRegBindActivity.this.shareusername, UserRegBindActivity.this.sharepassword, UserRegBindActivity.this.sharetype, c, 1));
                            return 0;
                        }
                    });
                    asynchronous.asyncCallback(new AsyncCallback() {
                        public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                            UserService userService = UserService.getInstance();
                            UserResult userResult = (UserResult) outputparameter.get("UserResult");
                            if (1 == userResult.getResultcode()) {
                                userService.setUserInfo(ksusername, kspassword, UserRegBindActivity.this.sharetype);
                                UserRegBindActivity.this.saveUserNameAndPwd(ksusername, kspassword, UserRegBindActivity.this.sharetype);
                                Toast.makeText(c, "注册视频快手用户成功", 0).show();
                                Intent intent = new Intent();
                                intent.setClass(UserRegBindActivity.this.context, UserSyncShareActivity.class);
                                UserRegBindActivity.this.startActivity(intent);
                            } else if (userResult.getResultcode() == 2) {
                                Toast.makeText(c, "用户名已经存在", 0).show();
                            } else if (userResult.getResultcode() == 0) {
                                Toast.makeText(c, "注册失败", 0).show();
                            }
                        }
                    });
                    asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                        public void handle(Context c, Exception e, Message m) {
                            Toast.makeText(c, "网络问题,注册失败", 0).show();
                        }
                    });
                    asynchronous.start();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void saveUserNameAndPwd(String username, String password, int usersharetype) {
        SharedPreferences.Editor editor = this.context.getSharedPreferences("KuaiShouUserService", 0).edit();
        editor.remove("username");
        editor.remove("password");
        editor.remove("usersharetype");
        editor.commit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.putString("usersharetype", new StringBuilder(String.valueOf(usersharetype)).toString());
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.str_terms_of_service_title).setMessage((int) R.string.str_terms_of_service).setNeutralButton((int) R.string.str_agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 2:
                return new AlertDialog.Builder(this).setTitle((int) R.string.str_terms_of_service_title).setMessage((int) R.string.str_terms_of_service_error).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            default:
                return null;
        }
    }
}
