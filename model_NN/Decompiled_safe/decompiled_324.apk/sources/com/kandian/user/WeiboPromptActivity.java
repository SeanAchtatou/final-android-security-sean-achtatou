package com.kandian.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.kandian.cartoonapp.R;
import com.kandian.common.HtmlUtil;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;

public class WeiboPromptActivity extends Activity {
    private static String TAG = "WeiboPromptActivity";
    /* access modifiers changed from: private */
    public final Activity context = this;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.weiboprompt);
        Intent intent = getIntent();
        String textStr = intent.getStringExtra("text");
        final String action = intent.getStringExtra("action");
        ((TextView) findViewById(R.id.qq_prompt)).setText(textStr);
        ((Button) findViewById(R.id.goto_qq_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (action.equals("sina")) {
                    Intent intent = new Intent();
                    intent.setClass(WeiboPromptActivity.this.context, UserBindShareActivity.class);
                    intent.putExtra("sharetype", HtmlUtil.TYPE_PLAY);
                    intent.putExtra("needonebyone", HtmlUtil.TYPE_PLAY);
                    WeiboPromptActivity.this.startActivity(intent);
                } else if (action.equals("qq")) {
                    Intent intent2 = new Intent();
                    intent2.setClass(WeiboPromptActivity.this.context, WeiboWebViewActivity.class);
                    intent2.putExtra("action", "UserBindShare");
                    WeiboPromptActivity.this.startActivity(intent2);
                }
            }
        });
        ((Button) findViewById(R.id.back_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WeiboPromptActivity.this.bindSyncShare();
                Intent intent = new Intent();
                intent.setClass(WeiboPromptActivity.this.context, UserActivity.class);
                WeiboPromptActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void bindSyncShare() {
        try {
            Log.v(TAG, "bindSyncShare!!");
            UserService userService = UserService.getInstance();
            UserResult syncAction = userService.syncAction(userService.getUsername(), "1,2", this.context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
