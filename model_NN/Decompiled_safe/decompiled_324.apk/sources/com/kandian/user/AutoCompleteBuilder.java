package com.kandian.user;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import com.kandian.cartoonapp.R;

public class AutoCompleteBuilder {
    static final String[] mails = {"@gmail.com", "@qq.com", "@sina.com", "@163.com", "@126.com", "@hotmail.com"};

    public static void build(final int actvId, final Activity context) {
        ((AutoCompleteTextView) context.findViewById(actvId)).addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String word = s.toString();
                if (word.length() >= 2 && word.indexOf("@") <= 0) {
                    String[] mymails = new String[AutoCompleteBuilder.mails.length];
                    for (int i = 0; i < mymails.length; i++) {
                        mymails[i] = String.valueOf(word) + AutoCompleteBuilder.mails[i];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context, (int) R.layout.simple_dropdown_item_1line, mymails);
                    AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) context.findViewById(actvId);
                    autoCompleteTextView.setThreshold(1);
                    autoCompleteTextView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }
}
