package com.kandian.user;

public class UserResult {
    public static final int RESULT_FAILED = 2;
    public static final int RESULT_SHARE_BIND_EXIST = 4;
    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_UNKNOW_FAILED = 0;
    public static final int RESULT_USER_NOT_EXIST = 3;
    private String msg;
    private int resultcode;

    public UserResult() {
    }

    public UserResult(int resultcode2, String msg2) {
        this.resultcode = resultcode2;
        this.msg = msg2;
    }

    public int getResultcode() {
        return this.resultcode;
    }

    public void setResultcode(int resultcode2) {
        this.resultcode = resultcode2;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg2) {
        this.msg = msg2;
    }
}
