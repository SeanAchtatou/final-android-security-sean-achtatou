package com.kandian.user.share;

import android.os.Environment;
import android.util.Log;
import com.kandian.user.ShareContent;
import com.kandian.user.ShareService;
import java.util.List;
import qq.QQWeibo;
import weibo4j.Comment;
import weibo4j.Status;

public class QQWeiboService extends ShareService {
    private static String TAG = "QQWeiboService";
    private static QQWeiboService ourInstance = new QQWeiboService();
    private String access_token;
    private String access_token_secret;

    /* renamed from: qq  reason: collision with root package name */
    private QQWeibo f0qq;
    private String userId;
    private String verify;

    public String getUserId() {
        this.userId = this.f0qq.getUserCode();
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public void setVerify(String verify2) {
        this.verify = verify2;
    }

    private QQWeiboService() {
    }

    public String getVerify() {
        return this.verify;
    }

    public void init(String customKey, String customSecrect) {
        this.f0qq = new QQWeibo(customKey, customSecrect);
    }

    public String getRequestToken() {
        try {
            return this.f0qq.getRequestToken();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getAccessToken(String verify2) {
        this.verify = verify2;
        this.f0qq.getAccessToken(verify2);
    }

    public static ShareService getInstance() {
        return ourInstance;
    }

    public String getAccess_token() {
        this.access_token = this.f0qq.getAccess_token();
        return this.f0qq.getAccess_token();
    }

    public String getAccess_token_secret() {
        this.access_token_secret = this.f0qq.getAccess_token_secret();
        return this.f0qq.getAccess_token_secret();
    }

    public void setAccess_token(String access_token2) {
        if (this.f0qq != null) {
            this.f0qq.setAccess_token(access_token2);
        }
        this.access_token = access_token2;
    }

    public void setAccess_token_secret(String access_token_secret2) {
        if (this.f0qq != null) {
            this.f0qq.setAccess_token_secret(access_token_secret2);
        }
        this.access_token_secret = access_token_secret2;
    }

    public String login(String userId2, String password) {
        if (this.f0qq.getAccess_token() == null || this.f0qq.getAccess_token_secret() == null) {
            return null;
        }
        return "";
    }

    public String update(String userId2, String password, ShareContent content) {
        if (this.f0qq == null) {
            return null;
        }
        Log.v(TAG, String.valueOf(content.getContent()) + " " + content.getImageurl());
        String imageUrl = getImageFile(content.getImageurl());
        if (this.f0qq != null) {
            this.f0qq.setAccess_token(this.access_token);
        }
        if (this.f0qq != null) {
            this.f0qq.setAccess_token_secret(this.access_token_secret);
        }
        Log.v(TAG, "getAccess_token=" + this.f0qq.getAccess_token());
        Log.v(TAG, "getAccess_token_secret=" + this.f0qq.getAccess_token_secret());
        Log.v(TAG, "getCustomKey=" + this.f0qq.getCustomKey());
        Log.v(TAG, "getCustomSecrect=" + this.f0qq.getCustomSecrect());
        if (this.f0qq.publishMsg(content.getContent(), imageUrl)) {
            return "ok";
        }
        return "failed";
    }

    private String getImageFile(String imageurl) {
        if (imageurl == null) {
            return null;
        }
        String downloadFile = Environment.getExternalStorageDirectory() + "/kuaishou/kuaishoupostertemp.jpg";
        Log.v(TAG, "downloadFile path=" + downloadFile);
        if (downloadFile(imageurl, downloadFile) == null) {
            return null;
        }
        return downloadFile;
    }

    public String createFriend(String userId2, String password, String destUserId) {
        if (this.f0qq == null) {
            return null;
        }
        Log.v(TAG, "createFriend destUserId=" + destUserId);
        if (this.f0qq.createfriends(destUserId)) {
            return "ok";
        }
        return null;
    }

    public String createFriendship(String targetUserid, String userId2, String password) {
        if (this.f0qq == null) {
            return null;
        }
        this.f0qq.setAccess_token(this.access_token);
        this.f0qq.setAccess_token_secret(this.access_token_secret);
        Log.v(TAG, "getAccess_token =" + this.f0qq.getAccess_token());
        Log.v(TAG, "getAccess_token_secret =" + this.f0qq.getAccess_token_secret());
        Log.v(TAG, "getCustomKey =" + this.f0qq.getCustomKey());
        Log.v(TAG, "getCustomSecrect =" + this.f0qq.getCustomSecrect());
        Log.v(TAG, "createFriend targetUserid=" + targetUserid);
        if (this.f0qq.createfriends(targetUserid)) {
            return "ok";
        }
        return null;
    }

    public Comment updateComment(String comment, String id, String password, String cid, int param) {
        return null;
    }

    public int transmitBlog(String comment, String id, String password, String cid, int param) {
        return 0;
    }

    public List<Comment> getComments(String uid, String password) {
        return null;
    }

    public List<Status> getRetweet(String userid, String password) {
        return null;
    }

    public String getUserId(String username, String password) {
        return null;
    }
}
