package com.kandian.user.share;

import android.os.Environment;
import com.kandian.common.Log;
import com.kandian.user.ShareContent;
import com.kandian.user.ShareService;
import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import weibo4j.Comment;
import weibo4j.Paging;
import weibo4j.Status;
import weibo4j.User;
import weibo4j.Weibo;
import weibo4j.WeiboException;

public class SinaWeiboBasicService extends ShareService {
    private static String TAG = "SinaWeiboBasicService";
    private static SinaWeiboBasicService ourInstance = new SinaWeiboBasicService();

    private SinaWeiboBasicService() {
    }

    public static ShareService getInstance() {
        return ourInstance;
    }

    private Weibo getWeibo(String userId, String password) {
        try {
            Weibo weibo = Weibo.instantiate(userId, password);
            Log.v(TAG, "getWeibo new !!");
            return weibo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String createFriendship(String targetUserid, String userId, String password) {
        try {
            Log.v(TAG, "login");
            User user = Weibo.instantiate(userId, password).createFriendship(targetUserid);
            if (user != null) {
                Log.v(TAG, user.toString());
                return user.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String login(String userId, String password) {
        try {
            Log.v(TAG, "login");
            User user = Weibo.instantiate(userId, password).showUser(Weibo.userId);
            if (user != null) {
                Log.v(TAG, user.toString());
                return user.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String update(String userId, String password, ShareContent content) {
        if (content == null) {
            return null;
        }
        Log.v(TAG, "begin update userId:" + userId + " password:" + password + " content:" + content);
        Weibo weibo = getWeibo(userId, password);
        if (weibo == null) {
            return null;
        }
        Status status = null;
        File imageFile = null;
        try {
            if (content.getImageurl() == null || content.getImageurl().trim().length() == 0) {
                status = weibo.updateStatus(content.getContent());
            } else {
                Log.v(TAG, "smallphoto url :" + content.getImageurl());
                imageFile = getImageFile(content.getImageurl());
                if (imageFile != null) {
                    Log.v(TAG, "update with image ! [" + content.getImageurl() + "]");
                    status = weibo.uploadStatus(URLEncoder.encode(content.getContent(), StringEncodings.UTF8), imageFile);
                } else {
                    Log.v(TAG, "update without image!");
                    status = weibo.updateStatus(content.getContent());
                }
            }
            if (status != null) {
                Log.v(TAG, status.getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.v(TAG, "end update userId:" + userId + " password:" + password + " content:" + content);
        if (imageFile != null) {
            imageFile.deleteOnExit();
            Log.v(TAG, "delete file!");
        }
        if (status != null) {
            return status.getSource();
        }
        return null;
    }

    public String createFriend(String userId, String password, String destUserId) {
        User user = null;
        try {
            Log.v(TAG, "login");
            user = getWeibo(userId, password).createFriendship(destUserId);
            if (user != null) {
                Log.v(TAG, user.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user != null) {
            return user.toString();
        }
        return null;
    }

    private File getImageFile(String imageurl) {
        String downloadFile = Environment.getExternalStorageDirectory() + "/kuaishou/kuaishoupostertemp.jpg";
        Log.v(TAG, "downloadFile path=" + downloadFile);
        return downloadFile(imageurl, downloadFile);
    }

    public Comment updateComment(String comment, String id, String password, String cid, int param) {
        Weibo weibo = getWeibo(id, password);
        if (weibo == null) {
            return null;
        }
        try {
            Comment cmt = weibo.updateComment(comment, cid, null, param);
            Log.v(TAG, "updateComment!");
            return cmt;
        } catch (WeiboException e) {
            Log.v(TAG, "WeiboException:" + e);
            return null;
        }
    }

    public int transmitBlog(String comment, String id, String password, String cid, int param) {
        Weibo weibo = getWeibo(id, password);
        if (weibo == null) {
            return 0;
        }
        try {
            if (comment.length() > 140) {
                return 0;
            }
            if (comment.length() == 0) {
                comment = "转发微博。";
            }
            weibo.repost(cid, URLEncoder.encode(comment), param);
            Log.v(TAG, "transmit!");
            return 1;
        } catch (WeiboException e) {
            Log.v(TAG, "WeiboException:" + e);
            return 2;
        }
    }

    public List<Comment> getComments(String userId, String password) {
        Weibo weibo = getWeibo(userId, password);
        List<Comment> list = new ArrayList<>();
        if (weibo == null) {
            return list;
        }
        try {
            return weibo.getCommentsByMe();
        } catch (WeiboException e) {
            e.printStackTrace();
            return list;
        }
    }

    public List<Status> getRetweet(String userid, String password) {
        Weibo weibo = getWeibo(userid, password);
        List<Status> list = new ArrayList<>();
        if (weibo == null) {
            return list;
        }
        try {
            return weibo.getRepostByMe(new Paging(1, 20, Weibo.userId));
        } catch (WeiboException e) {
            e.printStackTrace();
            return list;
        }
    }

    public String getUserId(String username, String password) {
        Weibo instantiate = Weibo.instantiate(username, password);
        return Weibo.userId;
    }
}
