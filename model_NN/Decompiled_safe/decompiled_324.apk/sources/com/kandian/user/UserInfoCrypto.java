package com.kandian.user;

public class UserInfoCrypto {
    private static final byte[] decodingTable = new byte[128];
    private static final byte[] encodingTable = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};

    static {
        for (int i = 0; i < 128; i++) {
            decodingTable[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            decodingTable[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            decodingTable[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            decodingTable[i4] = (byte) ((i4 - 48) + 52);
        }
        decodingTable[43] = 62;
        decodingTable[47] = 63;
    }

    /* JADX INFO: Multiple debug info for r0v14 int: [D('d1' int), D('b2' int)] */
    /* JADX INFO: Multiple debug info for r1v6 int: [D('d2' int), D('b3' int)] */
    /* JADX INFO: Multiple debug info for r0v25 int: [D('d1' int), D('b2' int)] */
    public static byte[] encode(byte[] data) {
        byte[] bytes;
        int modulus = data.length % 3;
        if (modulus == 0) {
            bytes = new byte[((data.length * 4) / 3)];
        } else {
            bytes = new byte[(((data.length / 3) + 1) * 4)];
        }
        int dataLength = data.length - modulus;
        int j = 0;
        for (int i = 0; i < dataLength; i += 3) {
            int a1 = data[i] & 255;
            int a2 = data[i + 1] & 255;
            int a3 = data[i + 2] & 255;
            bytes[j] = encodingTable[(a1 >>> 2) & 63];
            bytes[j + 1] = encodingTable[((a1 << 4) | (a2 >>> 4)) & 63];
            bytes[j + 2] = encodingTable[((a2 << 2) | (a3 >>> 6)) & 63];
            bytes[j + 3] = encodingTable[a3 & 63];
            j += 4;
        }
        switch (modulus) {
            case 1:
                int d1 = data[data.length - 1] & 255;
                bytes[bytes.length - 4] = encodingTable[(d1 >>> 2) & 63];
                bytes[bytes.length - 3] = encodingTable[(d1 << 4) & 63];
                bytes[bytes.length - 2] = 61;
                bytes[bytes.length - 1] = 61;
                break;
            case 2:
                int d12 = data[data.length - 2] & 255;
                int d2 = data[data.length - 1] & 255;
                bytes[bytes.length - 4] = encodingTable[(d12 >>> 2) & 63];
                bytes[bytes.length - 3] = encodingTable[((d12 << 4) | (d2 >>> 4)) & 63];
                bytes[bytes.length - 2] = encodingTable[(d2 << 2) & 63];
                bytes[bytes.length - 1] = 61;
                break;
        }
        return bytes;
    }

    public static byte[] decode(byte[] data) {
        byte[] bytes;
        byte[] data2 = discardNonBase64Bytes(data);
        if (data2[data2.length - 2] == 61) {
            bytes = new byte[((((data2.length / 4) - 1) * 3) + 1)];
        } else if (data2[data2.length - 1] == 61) {
            bytes = new byte[((((data2.length / 4) - 1) * 3) + 2)];
        } else {
            bytes = new byte[((data2.length / 4) * 3)];
        }
        int i = 0;
        int j = 0;
        while (i < data2.length - 4) {
            byte b1 = decodingTable[data2[i]];
            byte b2 = decodingTable[data2[i + 1]];
            byte b3 = decodingTable[data2[i + 2]];
            byte b4 = decodingTable[data2[i + 3]];
            bytes[j] = (byte) ((b1 << 2) | (b2 >> 4));
            bytes[j + 1] = (byte) ((b2 << 4) | (b3 >> 2));
            bytes[j + 2] = (byte) ((b3 << 6) | b4);
            i += 4;
            j += 3;
        }
        if (data2[data2.length - 2] == 61) {
            bytes[bytes.length - 1] = (byte) ((decodingTable[data2[data2.length - 4]] << 2) | (decodingTable[data2[data2.length - 3]] >> 4));
        } else if (data2[data2.length - 1] == 61) {
            byte b12 = decodingTable[data2[data2.length - 4]];
            byte b22 = decodingTable[data2[data2.length - 3]];
            byte b32 = decodingTable[data2[data2.length - 2]];
            bytes[bytes.length - 2] = (byte) ((b12 << 2) | (b22 >> 4));
            bytes[bytes.length - 1] = (byte) ((b22 << 4) | (b32 >> 2));
        } else {
            byte b13 = decodingTable[data2[data2.length - 4]];
            byte b23 = decodingTable[data2[data2.length - 3]];
            byte b33 = decodingTable[data2[data2.length - 2]];
            byte b42 = decodingTable[data2[data2.length - 1]];
            bytes[bytes.length - 3] = (byte) ((b13 << 2) | (b23 >> 4));
            bytes[bytes.length - 2] = (byte) ((b23 << 4) | (b33 >> 2));
            bytes[bytes.length - 1] = (byte) ((b33 << 6) | b42);
        }
        return bytes;
    }

    public static byte[] decode(String data) {
        byte[] bytes;
        String data2 = discardNonBase64Chars(data);
        if (data2.charAt(data2.length() - 2) == '=') {
            bytes = new byte[((((data2.length() / 4) - 1) * 3) + 1)];
        } else if (data2.charAt(data2.length() - 1) == '=') {
            bytes = new byte[((((data2.length() / 4) - 1) * 3) + 2)];
        } else {
            bytes = new byte[((data2.length() / 4) * 3)];
        }
        int i = 0;
        int j = 0;
        while (i < data2.length() - 4) {
            byte b1 = decodingTable[data2.charAt(i)];
            byte b2 = decodingTable[data2.charAt(i + 1)];
            byte b3 = decodingTable[data2.charAt(i + 2)];
            byte b4 = decodingTable[data2.charAt(i + 3)];
            bytes[j] = (byte) ((b1 << 2) | (b2 >> 4));
            bytes[j + 1] = (byte) ((b2 << 4) | (b3 >> 2));
            bytes[j + 2] = (byte) ((b3 << 6) | b4);
            i += 4;
            j += 3;
        }
        if (data2.charAt(data2.length() - 2) == '=') {
            bytes[bytes.length - 1] = (byte) ((decodingTable[data2.charAt(data2.length() - 4)] << 2) | (decodingTable[data2.charAt(data2.length() - 3)] >> 4));
        } else if (data2.charAt(data2.length() - 1) == '=') {
            byte b12 = decodingTable[data2.charAt(data2.length() - 4)];
            byte b22 = decodingTable[data2.charAt(data2.length() - 3)];
            byte b32 = decodingTable[data2.charAt(data2.length() - 2)];
            bytes[bytes.length - 2] = (byte) ((b12 << 2) | (b22 >> 4));
            bytes[bytes.length - 1] = (byte) ((b22 << 4) | (b32 >> 2));
        } else {
            byte b13 = decodingTable[data2.charAt(data2.length() - 4)];
            byte b23 = decodingTable[data2.charAt(data2.length() - 3)];
            byte b33 = decodingTable[data2.charAt(data2.length() - 2)];
            byte b42 = decodingTable[data2.charAt(data2.length() - 1)];
            bytes[bytes.length - 3] = (byte) ((b13 << 2) | (b23 >> 4));
            bytes[bytes.length - 2] = (byte) ((b23 << 4) | (b33 >> 2));
            bytes[bytes.length - 1] = (byte) ((b33 << 6) | b42);
        }
        return bytes;
    }

    private static byte[] discardNonBase64Bytes(byte[] data) {
        byte[] temp = new byte[data.length];
        int bytesCopied = 0;
        for (int i = 0; i < data.length; i++) {
            if (isValidBase64Byte(data[i])) {
                temp[bytesCopied] = data[i];
                bytesCopied++;
            }
        }
        byte[] newData = new byte[bytesCopied];
        System.arraycopy(temp, 0, newData, 0, bytesCopied);
        return newData;
    }

    private static String discardNonBase64Chars(String data) {
        StringBuffer sb = new StringBuffer();
        int length = data.length();
        for (int i = 0; i < length; i++) {
            if (isValidBase64Byte((byte) data.charAt(i))) {
                sb.append(data.charAt(i));
            }
        }
        return sb.toString();
    }

    private static boolean isValidBase64Byte(byte b) {
        if (b == 61) {
            return true;
        }
        if (b < 0 || b >= 128) {
            return false;
        }
        if (decodingTable[b] == -1) {
            return false;
        }
        return true;
    }

    public static String encrypt(String data) {
        return new String(encode(data.getBytes()));
    }

    public static String decrypt(String data) {
        return new String(decode(new String(data)));
    }

    public static void main(String[] args) {
        byte[] result = encode("mason".getBytes());
        System.out.println("mason");
        System.out.println(new String(result));
        System.out.println(new String(decode(new String(result))));
    }
}
