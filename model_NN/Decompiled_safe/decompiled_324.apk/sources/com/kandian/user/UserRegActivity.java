package com.kandian.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.Map;

public class UserRegActivity extends Activity {
    private static final int PLEASE_READ_SERVICE = 2;
    private static final int TERMS_OF_SERVICE = 1;
    /* access modifiers changed from: private */
    public String TAG = "UserRegActivity";
    /* access modifiers changed from: private */
    public UserRegActivity context = this;
    /* access modifiers changed from: private */
    public boolean isreadservice = false;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.str_terms_of_service_title).setMessage((int) R.string.str_terms_of_service).setNeutralButton((int) R.string.str_agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 2:
                return new AlertDialog.Builder(this).setTitle((int) R.string.str_terms_of_service_title).setMessage((int) R.string.str_terms_of_service_error).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            default:
                return null;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.userreg);
        AutoCompleteBuilder.build(R.id.username_edit_txt, this.context);
        ((CheckBox) findViewById(R.id.terms_of_service_cb)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UserRegActivity.this.isreadservice = isChecked;
                Log.v(UserRegActivity.this.TAG, "CheckBox " + UserRegActivity.this.isreadservice);
                UserRegActivity.this.showDialog(1);
            }
        });
        ((Button) findViewById(R.id.back_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserRegActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.reg_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String username = ((AutoCompleteTextView) UserRegActivity.this.findViewById(R.id.username_edit_txt)).getText().toString();
                final String password = ((EditText) UserRegActivity.this.findViewById(R.id.password_edit_txt)).getText().toString();
                String againpwd = ((EditText) UserRegActivity.this.findViewById(R.id.password_edit_again_txt)).getText().toString();
                Log.v(UserRegActivity.this.TAG, new StringBuilder(String.valueOf(UserRegActivity.this.isreadservice)).toString());
                if (!UserRegActivity.this.isreadservice) {
                    UserRegActivity.this.showDialog(2);
                } else if (username == null || username.trim().length() == 0 || password == null || password.trim().length() == 0 || againpwd == null || againpwd.trim().length() == 0) {
                    Toast.makeText(UserRegActivity.this.context, UserRegActivity.this.getString(R.string.str_reg_alert), 0).show();
                } else if (!password.trim().equals(againpwd.trim())) {
                    Toast.makeText(UserRegActivity.this.context, UserRegActivity.this.getString(R.string.str_pwd_repeat_error), 0).show();
                } else {
                    Asynchronous asynchronous = new Asynchronous(UserRegActivity.this.context);
                    asynchronous.setLoadingMsg("注册中,请稍等...");
                    asynchronous.asyncProcess(new AsyncProcess() {
                        public int process(Context c, Map<String, Object> map) {
                            setCallbackParameter("UserResult", UserService.getInstance().register(username, password, 0, c, null, null));
                            return 0;
                        }
                    });
                    asynchronous.asyncCallback(new AsyncCallback() {
                        public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                            UserService userService = UserService.getInstance();
                            UserResult userResult = (UserResult) outputparameter.get("UserResult");
                            if (userResult.getResultcode() == 1) {
                                userService.setPassword(password);
                                userService.setUsername(username);
                                Toast.makeText(c, "注册成功", 0).show();
                                Intent intent = new Intent();
                                intent.setClass(c, WeiboPromptActivity.class);
                                intent.putExtra("text", UserRegActivity.this.getString(R.string.str_sina_prompt));
                                intent.putExtra("action", "sina");
                                UserRegActivity.this.startActivity(intent);
                            } else if (userResult.getResultcode() == 2) {
                                Toast.makeText(c, "用户名已经存在", 0).show();
                            } else if (userResult.getResultcode() == 0) {
                                Toast.makeText(c, "注册失败", 0).show();
                            }
                        }
                    });
                    asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                        public void handle(Context c, Exception e, Message m) {
                            Toast.makeText(c, "网络问题,注册失败", 0).show();
                        }
                    });
                    asynchronous.start();
                }
            }
        });
    }
}
