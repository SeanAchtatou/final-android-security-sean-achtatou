package com.kandian.user;

import org.json.JSONObject;

public class ShareObject {
    public static final int SHARE_QQ_WEIBO = 2;
    public static final int SHARE_SINA_BASIC_WEIBO = 1;
    public static final int SHARE_SMS = 0;
    private String access_token;
    private String access_token_secret;
    private String sharename;
    private String sharepassword;
    private int sharetype;
    private String shareusername;

    public ShareObject() {
    }

    public void setAccess_token(String access_token2) {
        this.access_token = access_token2;
    }

    public void setAccess_token_secret(String access_token_secret2) {
        this.access_token_secret = access_token_secret2;
    }

    public String getAccess_token() {
        return this.access_token;
    }

    public String getAccess_token_secret() {
        return this.access_token_secret;
    }

    public ShareObject(String sharename2, int sharetype2) {
        this.sharename = sharename2;
        this.sharetype = sharetype2;
    }

    public String getShareusername() {
        if (this.shareusername == null) {
            return "";
        }
        return this.shareusername;
    }

    public void setShareusername(String shareusername2) {
        this.shareusername = shareusername2;
    }

    public String getSharepassword() {
        if (this.sharepassword == null) {
            return "";
        }
        return this.sharepassword;
    }

    public void setSharepassword(String sharepassword2) {
        this.sharepassword = sharepassword2;
    }

    public int getSharetype() {
        return this.sharetype;
    }

    public void setSharetype(int sharetype2) {
        this.sharetype = sharetype2;
    }

    public String getSharename() {
        if (this.sharename == null) {
            return "";
        }
        return this.sharename;
    }

    public void setSharename(String sharename2) {
        this.sharename = sharename2;
    }

    public String serializationJSON() {
        StringBuffer sb = new StringBuffer("{");
        sb.append("\"").append("sharetype").append("\":\"").append(this.sharetype).append("\",");
        sb.append("\"").append("sharename").append("\":\"").append(getSharename()).append("\",");
        sb.append("\"").append("shareusername").append("\":\"").append(getShareusername()).append("\",");
        sb.append("\"").append("sharepassword").append("\":\"").append(getSharepassword()).append("\",");
        sb.append("\"").append("access_token").append("\":\"").append(getAccess_token()).append("\",");
        sb.append("\"").append("access_token_secret").append("\":\"").append(getAccess_token_secret()).append("\"");
        sb.append("}");
        return sb.toString();
    }

    public static ShareObject deserializeJSON(String jsonString) {
        ShareObject so = new ShareObject();
        try {
            JSONObject json = new JSONObject(jsonString);
            so.setSharename(json.getString("sharename"));
            so.setSharetype(json.getInt("sharetype"));
            so.setShareusername(json.getString("shareusername"));
            so.setSharepassword(json.getString("sharepassword"));
            so.setAccess_token(json.getString("access_token"));
            so.setAccess_token_secret(json.getString("access_token_secret"));
            return so;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
