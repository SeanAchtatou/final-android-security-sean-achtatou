package com.kandian.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.HtmlUtil;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserActivity extends ListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "UserActivity";
    private final int DIALOG_BINDING_SHARE = 2;
    private final int DIALOG_SHARE_REG = 3;
    private final int DIALOG_SHARE_SETTING = 1;
    /* access modifiers changed from: private */
    public Activity context = this;
    /* access modifiers changed from: private */
    public ShareObject shareObject;
    /* access modifiers changed from: private */
    public Map<String, String> syncactionmap = new HashMap();
    View.OnClickListener unbindingListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (UserService.getInstance().islogin(UserActivity.this.context)) {
                Intent intent = new Intent();
                intent.setClass(UserActivity.this.context, ShareUnbindActivity.class);
                UserActivity.this.startActivity(intent);
                return;
            }
            Toast.makeText(UserActivity.this.context, UserActivity.this.getString(R.string.str_please_login), 0).show();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.user);
        ((TextView) findViewById(R.id.usernamedesc)).setText("您好!" + UserService.getInstance().getUsername());
        ((Button) findViewById(R.id.logout_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserService.getInstance().logout(UserActivity.this.context);
                Toast.makeText(UserActivity.this.context, UserActivity.this.getString(R.string.str_logout_ok), 0).show();
            }
        });
        ((Button) findViewById(R.id.logout_back_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserService.getInstance().back(UserActivity.this.context);
            }
        });
        ((Button) findViewById(R.id.sync_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserActivity.this.showDialog(1);
            }
        });
        setListAdapter(new ShareAdapter(this, R.layout.userrow, UserService.allshares));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        UserService.getInstance().back(this.context);
        return true;
    }

    private boolean[] getSyncAction() {
        String str = UserService.getInstance().getSyncaction();
        if (str == null || str.trim().length() == 0) {
            return new boolean[2];
        }
        boolean[] res = new boolean[2];
        if (str.indexOf(HtmlUtil.TYPE_PLAY) > -1) {
            this.syncactionmap.put(HtmlUtil.TYPE_PLAY, "");
            res[0] = true;
        } else {
            res[0] = false;
        }
        if (str.indexOf(HtmlUtil.TYPE_DOWN) > -1) {
            this.syncactionmap.put(HtmlUtil.TYPE_DOWN, "");
            res[1] = true;
        } else {
            res[1] = false;
        }
        return res;
    }

    /* access modifiers changed from: private */
    public void bindSyncShare() {
        try {
            Log.v(TAG, "bindSyncShare!!");
            UserService userService = UserService.getInstance();
            UserResult syncAction = userService.syncAction(userService.getUsername(), "1,2", this.context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        boolean[] syncArr = getSyncAction();
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.str_share_setting).setMultiChoiceItems((int) R.array.str_sync_action_array, syncArr, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton, boolean isChecked) {
                        if (isChecked) {
                            UserActivity.this.syncactionmap.put(new StringBuilder(String.valueOf(whichButton + 1)).toString(), "");
                        } else {
                            UserActivity.this.syncactionmap.remove(new StringBuilder(String.valueOf(whichButton + 1)).toString());
                        }
                    }
                }).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String syncaction = "";
                        for (String key : UserActivity.this.syncactionmap.keySet()) {
                            syncaction = String.valueOf(syncaction) + key + ",";
                        }
                        Asynchronous asynchronous = new Asynchronous(UserActivity.this.context);
                        asynchronous.setLoadingMsg("同步设置中,请稍等...");
                        asynchronous.setInputParameter("syncaction", syncaction);
                        asynchronous.asyncProcess(new AsyncProcess() {
                            public int process(Context c, Map<String, Object> inputparameter) {
                                UserService userService = UserService.getInstance();
                                setCallbackParameter("UserResult", userService.syncAction(userService.getUsername(), (String) inputparameter.get("syncaction"), c));
                                return 0;
                            }
                        });
                        asynchronous.asyncCallback(new AsyncCallback() {
                            public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                                UserService instance = UserService.getInstance();
                                if (((UserResult) outputparameter.get("UserResult")).getResultcode() == 1) {
                                    Toast.makeText(c, "同步设置成功", 0).show();
                                    Intent intent = new Intent();
                                    intent.setClass(UserActivity.this.context, UserActivity.class);
                                    UserActivity.this.startActivity(intent);
                                    return;
                                }
                                Toast.makeText(c, "同步设置失败", 0).show();
                            }
                        });
                        asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                            public void handle(Context c, Exception e, Message m) {
                                Toast.makeText(c, "网络问题,同步设置失败", 0).show();
                            }
                        });
                        asynchronous.start();
                    }
                }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 2:
            default:
                return null;
            case 3:
                View textEntryView2 = LayoutInflater.from(this).inflate((int) R.layout.sharereg, (ViewGroup) null);
                String url = getString(R.string.sina_reg_url);
                Log.v(TAG, url);
                WebView wv = (WebView) textEntryView2.findViewById(R.id.sinaWeibo);
                wv.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
                wv.requestFocusFromTouch();
                wv.getSettings().setJavaScriptEnabled(true);
                wv.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Log.v(UserActivity.TAG, "url " + url);
                        if (!url.startsWith("sms:")) {
                            return true;
                        }
                        Intent sendIntent = new Intent("android.intent.action.SENDTO", Uri.parse("sms://" + url.replaceAll("sms:", "")));
                        sendIntent.putExtra("sms_body", "");
                        UserActivity.this.startActivity(sendIntent);
                        return true;
                    }
                });
                wv.loadUrl(url);
                return new AlertDialog.Builder(this).setView(textEntryView2).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v(TAG, "<><><><><>onResume()");
        UserService instance = UserService.getInstance();
        setListAdapter(new ShareAdapter(this, R.layout.userrow, UserService.allshares));
        ((ShareAdapter) getListAdapter()).notifyDataSetChanged();
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        Log.v(TAG, "<><><><><>onPause()");
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    private class ShareAdapter extends ArrayAdapter<ShareObject> {
        public ShareAdapter(Context context, int textViewResourceId, ArrayList<ShareObject> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) UserActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.userrow, (ViewGroup) null);
            }
            UserService userService = UserService.getInstance();
            final ShareObject so = (ShareObject) getItem(position);
            if (so != null) {
                final int sharetype = so.getSharetype();
                final Button sharename_btn = (Button) v.findViewById(R.id.user_bind_button);
                final Button user_createfriends_button = (Button) v.findViewById(R.id.user_createfriends_button);
                sharename_btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        UserService userService = UserService.getInstance();
                        UserActivity.this.shareObject = userService.getShareObject(so.getSharetype());
                        if (UserActivity.this.shareObject == null) {
                            UserActivity.this.shareObject = so;
                        }
                        if (userService.isHasThisShare(so.getSharetype(), UserActivity.this.context)) {
                            Asynchronous asynchronous = new Asynchronous(UserActivity.this.context);
                            asynchronous.setLoadingMsg("取消绑定中,请稍等...");
                            final ShareObject shareObject = so;
                            asynchronous.asyncProcess(new AsyncProcess() {
                                public int process(Context c, Map<String, Object> map) {
                                    UserService userService = UserService.getInstance();
                                    ShareObject aShareObject = userService.getShareObject(shareObject.getSharetype());
                                    setCallbackParameter("UserResult", userService.unbinding(aShareObject.getShareusername(), aShareObject.getSharepassword(), aShareObject.getSharetype(), UserActivity.this.context));
                                    return 0;
                                }
                            });
                            final int i = sharetype;
                            final Button button = sharename_btn;
                            final Button button2 = user_createfriends_button;
                            asynchronous.asyncCallback(new AsyncCallback() {
                                public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                                    if (((UserResult) outputparameter.get("UserResult")).getResultcode() == 1) {
                                        String shareName = UserService.getInstance().getShareName(i);
                                        button.setText("我要绑定" + shareName);
                                        button2.setText("注册" + shareName + "用户");
                                        Toast.makeText(c, "取消绑定成功", 0).show();
                                        return;
                                    }
                                    Toast.makeText(c, "取消绑定失败", 0).show();
                                }
                            });
                            asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                                public void handle(Context c, Exception e, Message m) {
                                    Toast.makeText(c, "网络问题,取消绑定失败", 0).show();
                                }
                            });
                            asynchronous.start();
                        } else if (so.getSharetype() == 1) {
                            final View textEntryView = LayoutInflater.from(UserActivity.this.context).inflate((int) R.layout.sharelogindialog, (ViewGroup) null);
                            new AlertDialog.Builder(UserActivity.this).setTitle("请输入新浪微博用户名密码").setView(textEntryView).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    final String shareusername = ((EditText) textEntryView.findViewById(R.id.share_username_edit)).getText().toString();
                                    final String sharepassword = ((EditText) textEntryView.findViewById(R.id.share_password_edit)).getText().toString();
                                    if (shareusername == null || shareusername.trim().length() == 0 || sharepassword == null || sharepassword.trim().length() == 0) {
                                        Toast.makeText(UserActivity.this.context, UserActivity.this.getString(R.string.str_login_failed_msg), 0).show();
                                        return;
                                    }
                                    Asynchronous asynchronous = new Asynchronous(UserActivity.this.context);
                                    asynchronous.setLoadingMsg("绑定中,请稍等...");
                                    asynchronous.asyncProcess(new AsyncProcess() {
                                        public int process(Context c, Map<String, Object> map) {
                                            setCallbackParameter("UserResult", UserService.getInstance().binding(UserActivity.this.shareObject.getSharetype(), UserActivity.this.shareObject.getSharename(), shareusername, sharepassword, UserActivity.this.context, null, null));
                                            return 0;
                                        }
                                    });
                                    asynchronous.asyncCallback(new AsyncCallback() {
                                        public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                                            UserService userService = UserService.getInstance();
                                            UserResult userResult = (UserResult) outputparameter.get("UserResult");
                                            if (userResult.getResultcode() == 1) {
                                                Toast.makeText(UserActivity.this.context, "绑定成功!", 0).show();
                                                UserActivity.this.bindSyncShare();
                                                Intent intent = new Intent();
                                                intent.setClass(UserActivity.this.context, UserActivity.class);
                                                UserActivity.this.startActivity(intent);
                                            } else if (userResult.getResultcode() == 4) {
                                                Toast.makeText(UserActivity.this.context, "您绑定的" + userService.getShareName(UserActivity.this.shareObject.getSharetype()) + "用户已经被绑定过,绑定失败 !", 0).show();
                                            } else if (userResult.getResultcode() == 2) {
                                                Toast.makeText(UserActivity.this.context, "您已经绑定过该分享,绑定失败", 0).show();
                                            } else {
                                                Toast.makeText(UserActivity.this.context, "绑定失败", 0).show();
                                            }
                                        }
                                    });
                                    asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                                        public void handle(Context c, Exception e, Message m) {
                                            Toast.makeText(c, "网络问题,绑定失败", 0).show();
                                        }
                                    });
                                    asynchronous.start();
                                }
                            }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            }).create().show();
                        } else if (so.getSharetype() == 2) {
                            Intent intent = new Intent();
                            intent.setClass(UserActivity.this.context, WeiboWebViewActivity.class);
                            intent.putExtra("action", "UserBindShare");
                            UserActivity.this.startActivity(intent);
                        }
                    }
                });
                user_createfriends_button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        UserService userService = UserService.getInstance();
                        ShareObject aShareObject = userService.getShareObject(so.getSharetype());
                        if (aShareObject != null) {
                            userService.createFriendship(aShareObject.getShareusername(), aShareObject.getSharepassword(), aShareObject.getSharetype(), UserActivity.this.context);
                            Toast.makeText(UserActivity.this.context, "您关注了快手" + aShareObject.getSharename(), 0).show();
                        } else if (so.getSharetype() == 1) {
                            UserActivity.this.showDialog(3);
                        } else {
                            Toast.makeText(UserActivity.this.context, "请先绑定腾讯微博", 0).show();
                        }
                    }
                });
                String shareName = userService.getShareName(sharetype);
                if (userService.isHasThisShare(sharetype, UserActivity.this.context)) {
                    sharename_btn.setText("取消" + shareName + "绑定");
                    user_createfriends_button.setText("关注快手" + shareName);
                } else {
                    sharename_btn.setText("我要绑定" + shareName);
                    user_createfriends_button.setText("注册" + shareName + "用户");
                }
            }
            return v;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.usermenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_unbinding:
                this.unbindingListener.onClick(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
