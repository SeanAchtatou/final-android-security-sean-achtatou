package com.kandian.user.account;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

public interface UserAccountInterface {
    boolean addAccount(String str, String str2, int i, Context context);

    UserInfo getAccount(Context context);

    IBinder getIBinder(Service service, Intent intent);

    boolean removeAccount(Context context);
}
