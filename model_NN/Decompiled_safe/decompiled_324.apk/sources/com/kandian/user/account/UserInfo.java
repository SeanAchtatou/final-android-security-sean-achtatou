package com.kandian.user.account;

public class UserInfo {
    private String password;
    private String shareType;
    private String username;

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public String getShareType() {
        return this.shareType;
    }

    public void setShareType(String shareType2) {
        this.shareType = shareType2;
    }

    public UserInfo() {
    }

    public UserInfo(String username2, String password2, String shareType2) {
        this.username = username2;
        this.password = password2;
        this.shareType = shareType2;
    }
}
