package com.kandian.user.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class UserAuthenticator extends AbstractAccountAuthenticator {
    private Context context;

    public UserAuthenticator(Context context2) {
        super(context2);
        this.context = context2;
    }

    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        Bundle ret = new Bundle();
        Intent intent = new Intent();
        intent.setClassName(this.context, "com.kandian.user.UserLoginActivity");
        intent.putExtra("accountAuthenticatorResponse", response);
        ret.putParcelable("intent", intent);
        return ret;
    }

    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        return null;
    }

    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        return null;
    }

    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) {
        return null;
    }

    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) {
        return null;
    }
}
