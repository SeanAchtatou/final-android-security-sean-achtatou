package com.kandian.user.account;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import com.kandian.cartoonapp.R;

public class UserAccountHelper {
    public static final String PROKEY = "KuaiShouUserService";
    private static UserAccountHelper ourInstance = new UserAccountHelper();
    private Context context;
    private boolean isneedUserAccount = true;
    int sdk = 5;
    private UserAccountInterface userAccountInterface;

    public static UserAccountHelper getInstance() {
        return ourInstance;
    }

    private UserAccountHelper() {
    }

    public void init(Context context2) {
        if (this.context == null) {
            this.context = context2;
        }
        this.isneedUserAccount = Boolean.parseBoolean(this.context.getString(R.string.isNeedUserAccount));
        try {
            if (Class.forName("android.accounts.AccountManager") != null) {
                this.sdk = 5;
            } else {
                this.sdk = 4;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            this.sdk = 4;
        }
        if (this.sdk > 4) {
            load();
        }
    }

    private void load() {
        if (this.userAccountInterface == null) {
            this.userAccountInterface = new UserAccountImpl();
        }
    }

    public UserInfo getAccount(Context context2) {
        if (this.sdk > 4 && this.userAccountInterface != null && this.isneedUserAccount) {
            return this.userAccountInterface.getAccount(context2);
        }
        SharedPreferences settings = context2.getSharedPreferences("KuaiShouUserService", 0);
        String username = settings.getString("username", "");
        String password = settings.getString("password", "");
        String usersharetype = settings.getString("usersharetype", "0");
        if (username == null || username.trim().length() <= 0 || password == null || password.trim().length() <= 0) {
            return null;
        }
        UserInfo ui = new UserInfo();
        ui.setUsername(username);
        ui.setShareType(usersharetype);
        ui.setPassword(password);
        return ui;
    }

    public boolean addAccount(String username, String password, int shareType, Context context2) {
        if (this.sdk <= 4 || this.userAccountInterface == null || !this.isneedUserAccount) {
            return false;
        }
        return this.userAccountInterface.addAccount(username, password, shareType, context2);
    }

    public boolean removeAccount(Context context2) {
        if (this.sdk <= 4 || this.userAccountInterface == null || !this.isneedUserAccount) {
            return false;
        }
        return this.userAccountInterface.removeAccount(context2);
    }

    public IBinder getIBinder(Service service, Intent intent) {
        if (this.sdk <= 4 || this.userAccountInterface == null || !this.isneedUserAccount) {
            return null;
        }
        return this.userAccountInterface.getIBinder(service, intent);
    }
}
