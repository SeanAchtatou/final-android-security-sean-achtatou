package com.kandian.user.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

public class UserAccountImpl implements UserAccountInterface {
    private String TAG = "UserAccountImpl";
    private final String accountType = "www.kuaishou.com";
    private UserAuthenticator userAuthenticator = null;

    public IBinder getIBinder(Service service, Intent intent) {
        if (!intent.getAction().equals("android.accounts.AccountAuthenticator")) {
            return null;
        }
        if (this.userAuthenticator == null) {
            this.userAuthenticator = new UserAuthenticator(service);
        }
        return this.userAuthenticator.getIBinder();
    }

    public UserInfo getAccount(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType("www.kuaishou.com");
        if (accounts == null || accounts.length == 0) {
            return null;
        }
        Account a = accounts[0];
        String password = accountManager.getPassword(a);
        String username = accountManager.getUserData(a, "username");
        String password2 = accountManager.getUserData(a, "password");
        String shareType = accountManager.getUserData(a, "shareType");
        UserInfo ui = new UserInfo();
        ui.setPassword(password2);
        ui.setShareType(shareType);
        ui.setUsername(username);
        return ui;
    }

    public boolean removeAccount(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account[] accounts = accountManager.getAccountsByType("www.kuaishou.com");
        if (accounts == null || accounts.length == 0) {
            return true;
        }
        try {
            accountManager.removeAccount(accounts[0], null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean addAccount(String username, String password, int shareType, Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = new Account(username, "www.kuaishou.com");
        Bundle userdata = new Bundle();
        userdata.putString("username", username);
        userdata.putString("password", password);
        userdata.putString("shareType", new StringBuilder(String.valueOf(shareType)).toString());
        if (!accountManager.addAccountExplicitly(account, password, userdata)) {
            return false;
        }
        Bundle result = new Bundle();
        result.putString("authAccount", username);
        result.putString("accountType", "www.kuaishou.com");
        return true;
    }
}
