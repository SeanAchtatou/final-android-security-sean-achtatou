package com.kandian.user;

import android.app.Activity;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.user.share.QQWeiboService;
import java.util.HashMap;
import java.util.Map;

public class WeiboWebViewHelper {
    /* access modifiers changed from: private */
    public static String TAG = "WeiboWebViewHelper";

    public void showWeiboWebView(int resid, final Activity context, final WeiboWebViewAction weiboWebViewAction) {
        context.setContentView((int) R.layout.weibologin);
        String customKey = context.getString(R.string.qqCustomKey);
        String customSecrect = context.getString(R.string.qqCustomSecrect);
        Log.v(TAG, "**************** customKey:" + customKey + " customSecrect:" + customSecrect);
        QQWeiboService qqWeiboService = (QQWeiboService) QQWeiboService.getInstance();
        qqWeiboService.init(customKey, customSecrect);
        String authorizeUrl = qqWeiboService.getRequestToken();
        if (authorizeUrl == null) {
            Toast.makeText(context, "网络问题请稍候再试!", 0).show();
            Intent intent = new Intent();
            intent.setClass(context, UserActivity.class);
            context.startActivity(intent);
            return;
        }
        Log.v(TAG, "*************** " + authorizeUrl);
        WebView wv = (WebView) context.findViewById(R.id.weibowebview);
        wv.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);
        webSettings.setSupportZoom(true);
        wv.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.indexOf("http://open.t.qq.com/oauth_html/mobile.php") <= -1 || url.indexOf("type=3&code=0") <= 0 || url.indexOf("v=") <= 0) {
                    Log.v(WeiboWebViewHelper.TAG, "shouldOverrideUrlLoading " + url);
                    return false;
                }
                QQWeiboService qqWeiboService = (QQWeiboService) QQWeiboService.getInstance();
                String[] arr = url.split("v=");
                Log.v(WeiboWebViewHelper.TAG, "@@@@@ v= " + arr[0]);
                Log.v(WeiboWebViewHelper.TAG, "@@@@@ v= " + arr[1]);
                qqWeiboService.getAccessToken(arr[1]);
                if (weiboWebViewAction != null) {
                    Map<String, String> param = new HashMap<>();
                    param.put("v", arr[1]);
                    String access_token = qqWeiboService.getAccess_token();
                    String access_token_secret = qqWeiboService.getAccess_token_secret();
                    String userId = qqWeiboService.getUserId();
                    param.put("access_token", access_token);
                    param.put("access_token_secret", access_token_secret);
                    param.put("userId", userId);
                    Log.v(WeiboWebViewHelper.TAG, "@@@@@ access_token " + access_token);
                    Log.v(WeiboWebViewHelper.TAG, "@@@@@ access_token_secret " + access_token_secret);
                    Log.v(WeiboWebViewHelper.TAG, "@@@@@ userId " + userId);
                    weiboWebViewAction.doWhenSuccess(context, param);
                } else {
                    UserService.getInstance().back(context);
                }
                return true;
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(context, "网络问题,请稍候重试!", 0).show();
                UserService.getInstance().back(context);
            }
        });
        wv.loadUrl(authorizeUrl);
        wv.setOnTouchListener(new View.OnTouchListener() {
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Log.v(WeiboWebViewHelper.TAG, "MotionEvent.ACTION_DOWN");
                        break;
                    case 1:
                        break;
                    default:
                        return false;
                }
                Log.v(WeiboWebViewHelper.TAG, "MotionEvent.ACTION_UP");
                if (v.hasFocus()) {
                    return false;
                }
                Log.v(WeiboWebViewHelper.TAG, "MotionEvent.ACTION_UP requestFocus()");
                v.requestFocus();
                return false;
            }
        });
        wv.requestFocus();
    }
}
