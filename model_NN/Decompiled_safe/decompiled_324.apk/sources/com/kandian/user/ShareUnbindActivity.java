package com.kandian.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.ArrayList;
import java.util.Map;

public class ShareUnbindActivity extends ListActivity {
    private static String TAG = "ShareUnbindActivity";
    /* access modifiers changed from: private */
    public Activity context = this;
    /* access modifiers changed from: private */
    public ShareObject shareObject;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sharelist);
        UserService userService = UserService.getInstance();
        ArrayList<ShareObject> shareList = new ArrayList<>();
        for (String element : getSharedPreferences(UserService.PROKEY_SHARE, 0).getAll().values()) {
            ShareObject shareObject2 = ShareObject.deserializeJSON(element);
            if (shareObject2 != null) {
                shareObject2.setSharename(userService.getShareName(shareObject2.getSharetype()));
                shareList.add(shareObject2);
            }
        }
        Log.v(TAG, "ShareUnbindActivity=" + shareList.size());
        setListAdapter(new ShareAdapter(this, R.layout.sharerow, shareList));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        ShareObject so = (ShareObject) ((ShareAdapter) getListAdapter()).getItem(position);
        UserService userService = UserService.getInstance();
        this.shareObject = so;
        Log.v(TAG, "ShareUnbindActivity " + userService.getUsername() + " " + userService.getPassword() + " " + so.getSharetype());
        showDialog(so.getSharetype());
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(final int sharetype) {
        return new AlertDialog.Builder(this).setTitle("您确定要解绑吗?").setPositiveButton((int) R.string.str_binding_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Asynchronous asynchronous = new Asynchronous(ShareUnbindActivity.this.context);
                asynchronous.setLoadingMsg("解绑提交中,请稍等...");
                final int i = sharetype;
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> map) throws Exception {
                        setCallbackParameter("UserResult", UserService.getInstance().unbinding(ShareUnbindActivity.this.shareObject.getShareusername(), ShareUnbindActivity.this.shareObject.getSharepassword(), i, ShareUnbindActivity.this.context));
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        UserService instance = UserService.getInstance();
                        if (((UserResult) outputparameter.get("UserResult")).getResultcode() == 1) {
                            Toast.makeText(c, ShareUnbindActivity.this.getString(R.string.str_unbind_succ), 0).show();
                        } else {
                            Toast.makeText(c, ShareUnbindActivity.this.getString(R.string.str_unbind_failed), 0).show();
                        }
                        Intent intent = new Intent();
                        intent.setClass(ShareUnbindActivity.this.context, UserActivity.class);
                        ShareUnbindActivity.this.context.startActivity(intent);
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                        Toast.makeText(c, "网络问题,解绑失败", 0).show();
                    }
                });
                asynchronous.start();
            }
        }).setNegativeButton((int) R.string.str_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                UserService.getInstance().back(ShareUnbindActivity.this.context);
            }
        }).create();
    }

    private class ShareAdapter extends ArrayAdapter<ShareObject> {
        public ShareAdapter(Context context, int textViewResourceId, ArrayList<ShareObject> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextView tt;
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) ShareUnbindActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.sharerow, (ViewGroup) null);
            }
            ShareObject so = (ShareObject) getItem(position);
            if (!(so == null || (tt = (TextView) v.findViewById(R.id.sharename)) == null)) {
                tt.setText(UserService.getInstance().getShareName(so.getSharetype()));
            }
            return v;
        }
    }
}
