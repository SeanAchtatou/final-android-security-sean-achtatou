package com.kandian.user;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.HtmlUtil;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserSyncShareActivity extends ListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "ShareListActivity";
    /* access modifiers changed from: private */
    public Activity context = this;
    /* access modifiers changed from: private */
    public Map<String, SyncAction> syncactionmap = new HashMap();

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.usersync);
        ((Button) findViewById(R.id.ok_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (UserSyncShareActivity.this.syncactionmap.size() == 0) {
                    Toast.makeText(UserSyncShareActivity.this.context, "请至少选择一项", 0).show();
                    return;
                }
                String syncaction = "";
                for (String key : UserSyncShareActivity.this.syncactionmap.keySet()) {
                    syncaction = String.valueOf(syncaction) + key + ",";
                }
                Asynchronous asynchronous = new Asynchronous(UserSyncShareActivity.this.context);
                asynchronous.setLoadingMsg("同步设置中,请稍等...");
                asynchronous.setInputParameter("syncaction", syncaction);
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> inputparameter) {
                        UserService userService = UserService.getInstance();
                        setCallbackParameter("UserResult", userService.syncAction(userService.getUsername(), (String) inputparameter.get("syncaction"), c));
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        UserService instance = UserService.getInstance();
                        if (((UserResult) outputparameter.get("UserResult")).getResultcode() == 1) {
                            Toast.makeText(c, "同步设置成功", 0).show();
                            Intent intent = new Intent();
                            intent.setClass(UserSyncShareActivity.this.context, UserActivity.class);
                            UserSyncShareActivity.this.startActivity(intent);
                            return;
                        }
                        Toast.makeText(c, "同步设置失败", 0).show();
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                        Toast.makeText(c, "网络问题,同步设置失败", 0).show();
                    }
                });
                asynchronous.start();
            }
        });
        ((Button) findViewById(R.id.skip_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserService.getInstance().back(UserSyncShareActivity.this.context);
            }
        });
        ArrayList<SyncAction> syncActions = new ArrayList<>();
        syncActions.add(new SyncAction("自动分享评分", HtmlUtil.TYPE_PLAY));
        syncActions.add(new SyncAction("自动分享收藏", HtmlUtil.TYPE_DOWN));
        Log.v(TAG, "UserSyncShareActivity=" + syncActions.size());
        setListAdapter(new SyncActionAdapter(this, R.layout.usersyncrow, syncActions));
    }

    private class SyncActionAdapter extends ArrayAdapter<SyncAction> {
        public SyncActionAdapter(Context context, int textViewResourceId, ArrayList<SyncAction> items) {
            super(context, textViewResourceId, items);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) UserSyncShareActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.usersyncrow, (ViewGroup) null);
            }
            final SyncAction sa = (SyncAction) getItem(position);
            if (sa != null) {
                CheckBox selectsync_cb = (CheckBox) v.findViewById(R.id.selectsync);
                ((TextView) v.findViewById(R.id.syncname)).setText(sa.getName());
                String syncaction = UserSyncShareActivity.this.context.getSharedPreferences("KuaiShouUserService", 0).getString("syncaction", "");
                Log.v(UserSyncShareActivity.TAG, "syncaction=" + syncaction + " id=" + sa.id);
                if (syncaction != null && syncaction.trim().length() > 0) {
                    if (syncaction.indexOf(sa.id) >= 0) {
                        selectsync_cb.setChecked(true);
                        UserSyncShareActivity.this.syncactionmap.put(sa.getId(), sa);
                    } else {
                        selectsync_cb.setChecked(false);
                    }
                }
                selectsync_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            UserSyncShareActivity.this.syncactionmap.put(sa.getId(), sa);
                        } else {
                            UserSyncShareActivity.this.syncactionmap.remove(sa.getId());
                        }
                    }
                });
            }
            return v;
        }
    }

    private class SyncAction {
        /* access modifiers changed from: private */
        public String id;
        private String name;

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String id2) {
            this.id = id2;
        }

        public SyncAction(String name2, String id2) {
            this.name = name2;
            this.id = id2;
        }
    }
}
