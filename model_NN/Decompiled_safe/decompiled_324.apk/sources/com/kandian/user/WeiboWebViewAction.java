package com.kandian.user;

import android.app.Activity;
import java.util.Map;

public interface WeiboWebViewAction {
    void doWhenFailed(Activity activity);

    void doWhenSuccess(Activity activity, Map<String, String> map);
}
