package com.kandian.user.favorite;

import android.content.Context;
import android.content.SharedPreferences;
import com.kandian.common.FavoriteAsset;
import com.kandian.common.Log;
import com.kandian.user.UserInfoCrypto;
import com.kandian.user.UserService;
import java.io.IOException;
import java.net.URLDecoder;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserFavoriteService {
    private static String TAG = "UserFavoriteService";
    public static String UserFavoriteSharedPreferencesName = "com.kandian.user.favorite";
    private static final String favoriteUrl = "http://w.51tv.com/ksAppServlet?method=favorite";
    public static boolean isNeedSync = true;
    private static UserFavoriteService ourInstance = new UserFavoriteService();

    private UserFavoriteService() {
    }

    public static UserFavoriteService getInstance() {
        return ourInstance;
    }

    public void clearLocal(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(UserFavoriteSharedPreferencesName, 0).edit();
        editor.clear();
        editor.commit();
    }

    public boolean addFavorite(String appcode, Context context, FavoriteAsset favoriteAsset) {
        SharedPreferences.Editor editor = context.getSharedPreferences(UserFavoriteSharedPreferencesName, 0).edit();
        String userName = UserService.getInstance().getUsername();
        if (userName == null || userName.trim().length() == 0) {
            favoriteAsset.setOptaction(1);
            editor.remove(new StringBuilder(String.valueOf(favoriteAsset.getAssetId())).toString());
            editor.commit();
            editor.putString(new StringBuilder(String.valueOf(favoriteAsset.getAssetId())).toString(), FavoriteAsset.getJsonString4JavaPOJO(favoriteAsset));
            editor.commit();
            isNeedSync = true;
            return true;
        }
        String response = UserInfoCrypto.decrypt(getRemote(String.valueOf(favoriteUrl) + "&action=add&packagename=" + appcode + "&username=" + userName + "&assetid=" + favoriteAsset.getAssetId() + "&assettype=" + favoriteAsset.getAsseType()));
        Log.v(TAG, response);
        isNeedSync = true;
        if (response != null && response.trim().length() > 0) {
            try {
                if (getJSONObject(response).getInt("resultCode") == 1) {
                    favoriteAsset.setOptaction(0);
                    editor.putString(new StringBuilder(String.valueOf(favoriteAsset.getAssetId())).toString(), FavoriteAsset.getJsonString4JavaPOJO(favoriteAsset));
                    editor.commit();
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        favoriteAsset.setOptaction(1);
        editor.putString(new StringBuilder(String.valueOf(favoriteAsset.getAssetId())).toString(), FavoriteAsset.getJsonString4JavaPOJO(favoriteAsset));
        editor.commit();
        return false;
    }

    public boolean delFavorite(String appcode, Context context, FavoriteAsset[] favoriteAssets) {
        if (favoriteAssets == null || favoriteAssets.length == 0) {
            return true;
        }
        SharedPreferences.Editor editor = context.getSharedPreferences(UserFavoriteSharedPreferencesName, 0).edit();
        String userName = UserService.getInstance().getUsername();
        if (userName == null || userName.trim().length() == 0) {
            setDelete(editor, favoriteAssets);
            return true;
        }
        String assetstrs = "";
        for (int i = 0; i < favoriteAssets.length; i++) {
            favoriteAssets[i].setOptaction(2);
            assetstrs = String.valueOf(assetstrs) + favoriteAssets[i].getAssetId() + "$" + favoriteAssets[i].getAsseType() + ",";
        }
        String response = getRemote(String.valueOf(favoriteUrl) + "&action=del&packagename=" + appcode + "&username=" + userName + "&assetstrs=" + assetstrs);
        Log.v(TAG, response);
        isNeedSync = true;
        if (response == null || response.trim().length() <= 0) {
            setDelete(editor, favoriteAssets);
            return false;
        }
        Log.v(TAG, response);
        String response2 = UserInfoCrypto.decrypt(response);
        Log.v(TAG, response2);
        if (response2.equals("[]")) {
            editor.clear();
            editor.commit();
            return true;
        }
        JSONArray json = getJSONArray(response2);
        int length = json.length();
        if (length > 0) {
            editor.clear();
            editor.commit();
            for (int i2 = 0; i2 < length; i2++) {
                FavoriteAsset favoriteAsset = buildFavoriteAsset(json, i2);
                if (favoriteAsset != null) {
                    editor.putString(new StringBuilder(String.valueOf(favoriteAsset.getAssetId())).toString(), FavoriteAsset.getJsonString4JavaPOJO(favoriteAsset));
                }
            }
            editor.commit();
        }
        return true;
    }

    private void setDelete(SharedPreferences.Editor editor, FavoriteAsset[] favoriteAssets) {
        if (favoriteAssets != null && favoriteAssets.length != 0) {
            for (int i = 0; i < favoriteAssets.length; i++) {
                favoriteAssets[i].setOptaction(2);
                editor.remove(new StringBuilder(String.valueOf(favoriteAssets[i].getAssetId())).toString());
                editor.commit();
                editor.putString(new StringBuilder(String.valueOf(favoriteAssets[i].getAssetId())).toString(), FavoriteAsset.getJsonString4JavaPOJO(favoriteAssets[i]));
                editor.commit();
            }
            isNeedSync = true;
        }
    }

    public void syncFavorite(String appcode, Context context) {
        String userName = UserService.getInstance().getUsername();
        if (userName == null || userName.trim().length() == 0) {
            isNeedSync = true;
            return;
        }
        SharedPreferences settings = context.getSharedPreferences(UserFavoriteSharedPreferencesName, 0);
        SharedPreferences.Editor editor = settings.edit();
        String assetstrs = "";
        for (String element : settings.getAll().values()) {
            FavoriteAsset favoriteAsset = FavoriteAsset.getObject4JsonString(element);
            if (!(favoriteAsset == null || favoriteAsset.getOptaction() == 0)) {
                assetstrs = String.valueOf(assetstrs) + favoriteAsset.getAssetId() + "$" + favoriteAsset.getAsseType() + "$" + favoriteAsset.getOptaction() + ",";
            }
        }
        String response = getRemote(String.valueOf(favoriteUrl) + "&action=synfavorite&packagename=" + appcode + "&username=" + userName + "&assetstrs=" + assetstrs);
        if (response != null && response.trim().length() > 0) {
            Log.v(TAG, response);
            String response2 = UserInfoCrypto.decrypt(response);
            Log.v(TAG, response2);
            JSONArray json = getJSONArray(response2);
            int length = json.length();
            if (length > 0) {
                editor.clear();
                editor.commit();
                for (int i = 0; i < length; i++) {
                    FavoriteAsset favoriteAsset2 = buildFavoriteAsset(json, i);
                    if (favoriteAsset2 != null) {
                        editor.putString(new StringBuilder(String.valueOf(favoriteAsset2.getAssetId())).toString(), FavoriteAsset.getJsonString4JavaPOJO(favoriteAsset2));
                    }
                }
                editor.commit();
            }
        }
    }

    private FavoriteAsset buildFavoriteAsset(JSONArray json, int index) {
        Exception e;
        FavoriteAsset favorite = null;
        try {
            FavoriteAsset favorite2 = new FavoriteAsset();
            try {
                JSONObject jobj = (JSONObject) json.get(index);
                favorite2.setAssetId(jobj.getLong("assetId"));
                favorite2.setAssetName(URLDecoder.decode(jobj.getString("assetName"), "GBK"));
                favorite2.setAsseType(jobj.getString("asseType"));
                favorite2.setAssetKey(jobj.getString("assetKey"));
                favorite2.setImageUrl(jobj.getString("imageUrl"));
                favorite2.setBigImageUrl(jobj.getString("bigImageUrl"));
                favorite2.setVote(jobj.getDouble("vote"));
                favorite2.setOrigin(URLDecoder.decode(jobj.getString("origin"), "GBK"));
                favorite2.setStatus(jobj.getInt("status"));
                favorite2.setOptaction(jobj.getInt("optAction"));
                favorite2.setIsUpdate(jobj.getInt("isUpdate"));
                return favorite2;
            } catch (Exception e2) {
                e = e2;
                favorite = favorite2;
                e.printStackTrace();
                return favorite;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return favorite;
        }
    }

    public void updateFavorite(String appcode, Context context, FavoriteAsset favoriteAsset) {
        try {
            String userName = UserService.getInstance().getUsername();
            if (userName == null || userName.trim().length() == 0) {
                userName = "";
            }
            Log.v(TAG, UserInfoCrypto.decrypt(getRemote(String.valueOf(favoriteUrl) + "&action=update&packagename=" + appcode + "&username=" + userName + "&assetid=" + favoriteAsset.getAssetId() + "&assettype=" + favoriteAsset.getAsseType())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONObject getJSONObject(String res) {
        try {
            return new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONArray getJSONArray(String res) {
        try {
            return new JSONArray(res);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getRemote(String url) {
        try {
            Log.v(TAG, url);
            HttpClient client = new HttpClient(new MultiThreadedHttpConnectionManager());
            GetMethod method = new GetMethod(url);
            client.setTimeout(600000);
            method.setHttp11(true);
            method.addRequestHeader("Content-Type", "text/html; charset=UTF-8");
            method.addRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
            client.getParams().setContentCharset(StringEncodings.UTF8);
            client.executeMethod(method);
            return method.getResponseBodyAsString();
        } catch (IOException e) {
            return null;
        }
    }
}
