package com.kandian.user;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;

public class SinaRegActivity extends Activity {
    /* access modifiers changed from: private */
    public static String TAG = "SinaRegActivity";
    private final Activity context = this;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.weiboreg);
        Log.v(TAG, "http://3g.sina.com.cn/prog/wapsite/sso/register.php?wm=9270_0004");
        WebView wv = (WebView) findViewById(R.id.sinaWeibo);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.requestFocusFromTouch();
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.v(SinaRegActivity.TAG, "url " + url);
                if (!url.startsWith("sms:")) {
                    return false;
                }
                String url2 = url.replaceAll("sms:", "");
                Intent sendIntent = new Intent("android.intent.action.SENDTO", Uri.parse("sms://"));
                sendIntent.putExtra("address", url2);
                SinaRegActivity.this.startActivity(sendIntent);
                return true;
            }
        });
        wv.loadUrl("http://3g.sina.com.cn/prog/wapsite/sso/register.php?wm=9270_0004");
    }
}
