package com.kandian.user;

import java.util.List;
import weibo4j.Comment;
import weibo4j.Status;

public abstract class ShareService {
    public abstract String createFriend(String str, String str2, String str3);

    public abstract String createFriendship(String str, String str2, String str3);

    public abstract List<Comment> getComments(String str, String str2);

    public abstract List<Status> getRetweet(String str, String str2);

    public abstract String getUserId(String str, String str2);

    public abstract String login(String str, String str2);

    public abstract int transmitBlog(String str, String str2, String str3, String str4, int i);

    public abstract String update(String str, String str2, ShareContent shareContent);

    public abstract Comment updateComment(String str, String str2, String str3, String str4, int i);

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.io.InputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0071 A[SYNTHETIC, Splitter:B:29:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0076 A[Catch:{ IOException -> 0x007b }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0088 A[SYNTHETIC, Splitter:B:41:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008d A[Catch:{ IOException -> 0x0091 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x009e A[SYNTHETIC, Splitter:B:52:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00a3 A[Catch:{ IOException -> 0x00a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00b0 A[SYNTHETIC, Splitter:B:60:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00b5 A[Catch:{ IOException -> 0x00b9 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:38:0x0083=Splitter:B:38:0x0083, B:49:0x0099=Splitter:B:49:0x0099, B:26:0x006c=Splitter:B:26:0x006c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File downloadFile(java.lang.String r17, java.lang.String r18) {
        /*
            r16 = this;
            java.lang.String r14 = "ShareService"
            java.lang.String r15 = "begin download smallphoto image!"
            com.kandian.common.Log.v(r14, r15)
            r8 = 0
            r6 = 0
            r10 = 0
            java.io.File r7 = new java.io.File     // Catch:{ MalformedURLException -> 0x006a, IOException -> 0x0081, Exception -> 0x0097 }
            r0 = r7
            r1 = r18
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x006a, IOException -> 0x0081, Exception -> 0x0097 }
            boolean r14 = r7.exists()     // Catch:{ MalformedURLException -> 0x00d8, IOException -> 0x00cf, Exception -> 0x00c6, all -> 0x00bf }
            if (r14 != 0) goto L_0x001b
            r7.createNewFile()     // Catch:{ MalformedURLException -> 0x00d8, IOException -> 0x00cf, Exception -> 0x00c6, all -> 0x00bf }
        L_0x001b:
            java.io.FileOutputStream r11 = new java.io.FileOutputStream     // Catch:{ MalformedURLException -> 0x00d8, IOException -> 0x00cf, Exception -> 0x00c6, all -> 0x00bf }
            r11.<init>(r7)     // Catch:{ MalformedURLException -> 0x00d8, IOException -> 0x00cf, Exception -> 0x00c6, all -> 0x00bf }
            java.net.URL r9 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            r0 = r9
            r1 = r17
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            java.net.URLConnection r13 = r9.openConnection()     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            java.lang.String r14 = "referer"
            java.lang.String r15 = "www.51tv.com"
            r13.addRequestProperty(r14, r15)     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            java.lang.Object r9 = r9.getContent()     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            r0 = r9
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            r8 = r0
            r14 = 1024(0x400, float:1.435E-42)
            byte[] r12 = new byte[r14]     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            int r2 = r8.read(r12)     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
        L_0x0043:
            r14 = -1
            if (r2 != r14) goto L_0x005b
            java.lang.String r14 = "ShareService"
            java.lang.String r15 = "download smallphoto image end !"
            com.kandian.common.Log.v(r14, r15)     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            if (r8 == 0) goto L_0x0052
            r8.close()     // Catch:{ IOException -> 0x0064 }
        L_0x0052:
            if (r11 == 0) goto L_0x0057
            r11.close()     // Catch:{ IOException -> 0x0064 }
        L_0x0057:
            r10 = r11
            r6 = r7
            r14 = r7
        L_0x005a:
            return r14
        L_0x005b:
            r14 = 0
            r11.write(r12, r14, r2)     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            int r2 = r8.read(r12)     // Catch:{ MalformedURLException -> 0x00dc, IOException -> 0x00d3, Exception -> 0x00ca, all -> 0x00c2 }
            goto L_0x0043
        L_0x0064:
            r14 = move-exception
            r3 = r14
            r3.printStackTrace()
            goto L_0x0057
        L_0x006a:
            r14 = move-exception
            r4 = r14
        L_0x006c:
            r4.printStackTrace()     // Catch:{ all -> 0x00ad }
            if (r8 == 0) goto L_0x0074
            r8.close()     // Catch:{ IOException -> 0x007b }
        L_0x0074:
            if (r10 == 0) goto L_0x0079
            r10.close()     // Catch:{ IOException -> 0x007b }
        L_0x0079:
            r14 = 0
            goto L_0x005a
        L_0x007b:
            r14 = move-exception
            r3 = r14
            r3.printStackTrace()
            goto L_0x0079
        L_0x0081:
            r14 = move-exception
            r3 = r14
        L_0x0083:
            r3.printStackTrace()     // Catch:{ all -> 0x00ad }
            if (r8 == 0) goto L_0x008b
            r8.close()     // Catch:{ IOException -> 0x0091 }
        L_0x008b:
            if (r10 == 0) goto L_0x0079
            r10.close()     // Catch:{ IOException -> 0x0091 }
            goto L_0x0079
        L_0x0091:
            r14 = move-exception
            r3 = r14
            r3.printStackTrace()
            goto L_0x0079
        L_0x0097:
            r14 = move-exception
            r5 = r14
        L_0x0099:
            r5.printStackTrace()     // Catch:{ all -> 0x00ad }
            if (r8 == 0) goto L_0x00a1
            r8.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x00a1:
            if (r10 == 0) goto L_0x0079
            r10.close()     // Catch:{ IOException -> 0x00a7 }
            goto L_0x0079
        L_0x00a7:
            r14 = move-exception
            r3 = r14
            r3.printStackTrace()
            goto L_0x0079
        L_0x00ad:
            r14 = move-exception
        L_0x00ae:
            if (r8 == 0) goto L_0x00b3
            r8.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00b3:
            if (r10 == 0) goto L_0x00b8
            r10.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00b8:
            throw r14
        L_0x00b9:
            r15 = move-exception
            r3 = r15
            r3.printStackTrace()
            goto L_0x00b8
        L_0x00bf:
            r14 = move-exception
            r6 = r7
            goto L_0x00ae
        L_0x00c2:
            r14 = move-exception
            r10 = r11
            r6 = r7
            goto L_0x00ae
        L_0x00c6:
            r14 = move-exception
            r5 = r14
            r6 = r7
            goto L_0x0099
        L_0x00ca:
            r14 = move-exception
            r5 = r14
            r10 = r11
            r6 = r7
            goto L_0x0099
        L_0x00cf:
            r14 = move-exception
            r3 = r14
            r6 = r7
            goto L_0x0083
        L_0x00d3:
            r14 = move-exception
            r3 = r14
            r10 = r11
            r6 = r7
            goto L_0x0083
        L_0x00d8:
            r14 = move-exception
            r4 = r14
            r6 = r7
            goto L_0x006c
        L_0x00dc:
            r14 = move-exception
            r4 = r14
            r10 = r11
            r6 = r7
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kandian.user.ShareService.downloadFile(java.lang.String, java.lang.String):java.io.File");
    }
}
