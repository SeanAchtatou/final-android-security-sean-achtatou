package com.kandian.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.kandian.cartoonapp.R;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import java.util.Map;

public class BindExistUserActivity extends Activity {
    private String TAG = "BindExistUserActivity";
    /* access modifiers changed from: private */
    public BindExistUserActivity context = this;
    /* access modifiers changed from: private */
    public String sharepassword;
    /* access modifiers changed from: private */
    public int sharetype;
    /* access modifiers changed from: private */
    public String shareusername;

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.binduser);
        Intent intent = getIntent();
        this.shareusername = intent.getStringExtra("shareusername");
        this.sharepassword = intent.getStringExtra("sharepassword");
        this.sharetype = Integer.parseInt(intent.getStringExtra("sharetype"));
        AutoCompleteBuilder.build(R.id.ksusername, this.context);
        ((Button) findViewById(R.id.user_bind_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String ksusername = ((AutoCompleteTextView) BindExistUserActivity.this.findViewById(R.id.ksusername)).getText().toString();
                final String kspassword = ((EditText) BindExistUserActivity.this.findViewById(R.id.kspassword)).getText().toString();
                if (ksusername == null || ksusername.trim().length() == 0 || kspassword == null || kspassword.trim().length() == 0) {
                    Toast.makeText(BindExistUserActivity.this.context, BindExistUserActivity.this.getString(R.string.str_login_failed_msg), 0).show();
                    return;
                }
                Log.v("USERANDPWD_DIALOG", "ksusername:" + ksusername + " kspassword:" + kspassword);
                Asynchronous asynchronous = new Asynchronous(BindExistUserActivity.this.context);
                asynchronous.setLoadingMsg("绑定中,请稍等...");
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context c, Map<String, Object> map) {
                        setCallbackParameter("UserResult", UserService.getInstance().autobind(ksusername, kspassword, BindExistUserActivity.this.shareusername, BindExistUserActivity.this.sharepassword, BindExistUserActivity.this.sharetype, c, 0));
                        return 0;
                    }
                });
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context c, Map<String, Object> outputparameter, Message m) {
                        UserService userService = UserService.getInstance();
                        if (1 == ((UserResult) outputparameter.get("UserResult")).getResultcode()) {
                            userService.setUserInfo(ksusername, kspassword, BindExistUserActivity.this.sharetype);
                            BindExistUserActivity.this.saveUserNameAndPwd(ksusername, kspassword, BindExistUserActivity.this.sharetype);
                            Toast.makeText(c, "绑定视频快手用户成功", 0).show();
                            Intent intent = new Intent();
                            intent.setClass(BindExistUserActivity.this.context, UserSyncShareActivity.class);
                            BindExistUserActivity.this.startActivity(intent);
                            return;
                        }
                        Toast.makeText(c, "视频快手用户名密码错误或者用户不存在,绑定失败", 0).show();
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context c, Exception e, Message m) {
                    }
                });
                asynchronous.start();
            }
        });
        ((Button) findViewById(R.id.user_create_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(BindExistUserActivity.this.context, UserRegBindActivity.class);
                intent.putExtra("shareusername", BindExistUserActivity.this.shareusername);
                intent.putExtra("sharepassword", BindExistUserActivity.this.sharepassword);
                intent.putExtra("sharetype", new StringBuilder(String.valueOf(BindExistUserActivity.this.sharetype)).toString());
                BindExistUserActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void saveUserNameAndPwd(String username, String password, int usersharetype) {
        SharedPreferences.Editor editor = this.context.getSharedPreferences("KuaiShouUserService", 0).edit();
        editor.remove("username");
        editor.remove("password");
        editor.remove("usersharetype");
        editor.commit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.putString("usersharetype", new StringBuilder(String.valueOf(usersharetype)).toString());
        editor.commit();
    }
}
