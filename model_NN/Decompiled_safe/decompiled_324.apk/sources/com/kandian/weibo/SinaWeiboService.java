package com.kandian.weibo;

import com.kandian.common.DateUtil;
import com.kandian.common.Log;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import weibo4j.Weibo;

public class SinaWeiboService {
    private static String TAG = "SinaWeiboService";
    private static SinaWeiboService ourInstance = new SinaWeiboService();

    private SinaWeiboService() {
    }

    public static SinaWeiboService getInstance() {
        return ourInstance;
    }

    public List search(String searchKey) {
        JSONException e;
        JSONArray contentJsonArray;
        List contentList = null;
        try {
            String jsonString = getSearchResult(searchKey);
            Log.v(TAG, "data:" + jsonString);
            if (!(jsonString == null || jsonString.trim().length() <= 0 || (contentJsonArray = new JSONObject(jsonString).getJSONArray("results")) == null)) {
                List contentList2 = new ArrayList();
                int i = 0;
                while (i < contentJsonArray.length()) {
                    try {
                        JSONObject contentJsonObject = (JSONObject) contentJsonArray.get(i);
                        SinaWeiboContent content = new SinaWeiboContent();
                        content.setContent_id(contentJsonObject.getLong("id"));
                        content.setContent_text(getStringFromJson(contentJsonObject, "text"));
                        content.setThumbnail_pic(getStringFromJson(contentJsonObject, "thumbnail_pic"));
                        content.setContent_source(getStringFromJson(contentJsonObject, "source"));
                        JSONObject userJsonObject = contentJsonObject.getJSONObject("user");
                        SinaWeiboUser user = new SinaWeiboUser();
                        user.setId(userJsonObject.getLong("id"));
                        user.setScreen_name(getStringFromJson(userJsonObject, "screen_name"));
                        user.setName(getStringFromJson(userJsonObject, "name"));
                        user.setLocation(getStringFromJson(userJsonObject, "location"));
                        user.setDescription(getStringFromJson(userJsonObject, "description"));
                        user.setUrl(getStringFromJson(userJsonObject, "url"));
                        user.setProfile_image_url(getStringFromJson(userJsonObject, "profile_image_url"));
                        user.setDomain(getStringFromJson(userJsonObject, Cookie2.DOMAIN));
                        user.setGender(getStringFromJson(userJsonObject, "gender"));
                        content.addMicroBlogUser(user);
                        contentList2.add(content);
                        i++;
                    } catch (JSONException e2) {
                        e = e2;
                        contentList = contentList2;
                    }
                }
                contentList = contentList2;
            }
            Log.v(TAG, "#######contentList.size():" + contentList.size());
        } catch (JSONException e3) {
            e = e3;
            e.printStackTrace();
            return contentList;
        }
        return contentList;
    }

    private String getStringFromJson(JSONObject jsonObject, String attr) {
        if (!(jsonObject == null || attr == null)) {
            try {
                if (attr.trim().length() > 0) {
                    return jsonObject.getString(attr);
                }
            } catch (JSONException e) {
                return "";
            }
        }
        return "";
    }

    private String getSearchResult(String searchKey) {
        long currentTimeMillis = System.currentTimeMillis() - DateUtil.millisInDay;
        long currentTimeMillis2 = System.currentTimeMillis();
        String queryString = "";
        if (searchKey != null) {
            try {
                if (searchKey.trim().length() > 0) {
                    String[] keywords = searchKey.split(",");
                    for (int i = 0; i < keywords.length; i++) {
                        if (keywords[i] != null && keywords[i].trim().length() > 0 && !"null".equals(keywords[i].trim().toLowerCase())) {
                            queryString = String.valueOf(queryString) + "~" + URLEncoder.encode(keywords[i].trim(), StringEncodings.UTF8);
                        }
                    }
                }
            } catch (IOException e) {
                return null;
            }
        }
        String dataSource = "http://api.t.sina.com.cn/statuses/search.json?q=" + queryString + "&source=" + Weibo.CONSUMER_KEY + "&needcount=true&count=200&page=" + 1;
        Log.v(TAG, dataSource);
        HttpClient client = new HttpClient(new MultiThreadedHttpConnectionManager());
        GetMethod method = new GetMethod(dataSource);
        client.setTimeout(600000);
        method.setHttp11(true);
        method.addRequestHeader("Content-Type", "text/html; charset=UTF-8");
        method.addRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
        client.getParams().setContentCharset(StringEncodings.UTF8);
        client.executeMethod(method);
        return method.getResponseBodyAsString();
    }
}
