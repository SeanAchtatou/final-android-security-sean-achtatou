package com.kandian.weibo;

import java.util.ArrayList;
import java.util.Date;

public class SinaWeiboContent {
    private String content_createtime;
    private long content_id;
    private String content_source;
    private String content_text;
    private Date created_at;
    private ArrayList<SinaWeiboUser> microBlogUsers = new ArrayList<>();
    private String thumbnail_pic;

    public Date getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(Date created_at2) {
        this.created_at = created_at2;
    }

    public void addMicroBlogUser(SinaWeiboUser user) {
        if (user != null) {
            if (this.microBlogUsers != null) {
                this.microBlogUsers.add(user);
                return;
            }
            this.microBlogUsers = new ArrayList<>();
            this.microBlogUsers.add(user);
        }
    }

    public String toString() {
        StringBuffer str = new StringBuffer("");
        str.append("content_id[").append(this.content_id).append("]");
        str.append("content_createtime[").append(this.content_createtime).append("]");
        str.append("thumbnail_pic[").append(this.thumbnail_pic).append("]");
        return str.toString();
    }

    public long getContent_id() {
        return this.content_id;
    }

    public void setContent_id(long content_id2) {
        this.content_id = content_id2;
    }

    public String getContent_createtime() {
        return this.content_createtime;
    }

    public void setContent_createtime(String content_createtime2) {
        this.content_createtime = content_createtime2;
    }

    public String getContent_text() {
        return this.content_text;
    }

    public void setContent_text(String content_text2) {
        this.content_text = content_text2;
    }

    public String getThumbnail_pic() {
        return this.thumbnail_pic;
    }

    public void setThumbnail_pic(String thumbnail_pic2) {
        this.thumbnail_pic = thumbnail_pic2;
    }

    public String getContent_source() {
        return this.content_source;
    }

    public void setContent_source(String content_source2) {
        this.content_source = content_source2;
    }

    public ArrayList<SinaWeiboUser> getMicroBlogUsers() {
        return this.microBlogUsers;
    }

    public void setMicroBlogUsers(ArrayList<SinaWeiboUser> microBlogUsers2) {
        this.microBlogUsers = microBlogUsers2;
    }
}
