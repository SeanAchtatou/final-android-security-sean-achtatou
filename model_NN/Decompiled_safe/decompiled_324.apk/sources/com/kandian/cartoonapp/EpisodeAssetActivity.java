package com.kandian.cartoonapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.common.AssetList;
import com.kandian.common.AssetListSaxHandler;
import com.kandian.common.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class EpisodeAssetActivity extends AssetActivity {
    /* access modifiers changed from: private */
    public EpisodeAssetActivity _context;
    /* access modifiers changed from: private */
    public String[] assetKeys = null;
    /* access modifiers changed from: private */
    public String assetType = "";
    /* access modifiers changed from: private */
    public int currPage;
    /* access modifiers changed from: private */
    public final List<String> list = new ArrayList();
    /* access modifiers changed from: private */
    public String listUrl = "";
    Handler myViewUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            AssetList tmpAssetList = (AssetList) msg.obj;
            switch (msg.what) {
                case AuthScope.ANY_PORT /*-1*/:
                    if (tmpAssetList != null) {
                        List alist = new ArrayList();
                        if (EpisodeAssetActivity.this.assetKeys != null) {
                            for (String add : EpisodeAssetActivity.this.assetKeys) {
                                alist.add(add);
                            }
                        }
                        for (int i = 0; i < tmpAssetList.getCurrentItemCount(); i++) {
                            alist.add(tmpAssetList.get(i).getAssetKey());
                        }
                        EpisodeAssetActivity.this.assetKeys = new String[alist.size()];
                        for (int i2 = 0; i2 < alist.size(); i2++) {
                            EpisodeAssetActivity.this.assetKeys[i2] = alist.get(i2).toString();
                        }
                        break;
                    }
                    break;
                case 0:
                    Toast.makeText(EpisodeAssetActivity.this, EpisodeAssetActivity.this.getString(R.string.network_problem), 0).show();
                    break;
                case 1:
                    if (tmpAssetList != null) {
                        List alist2 = new ArrayList();
                        for (int i3 = 0; i3 < tmpAssetList.getCurrentItemCount(); i3++) {
                            alist2.add(tmpAssetList.get(i3).getAssetKey());
                        }
                        if (EpisodeAssetActivity.this.assetKeys != null) {
                            for (String add2 : EpisodeAssetActivity.this.assetKeys) {
                                alist2.add(add2);
                            }
                            EpisodeAssetActivity episodeAssetActivity = EpisodeAssetActivity.this;
                            episodeAssetActivity.position = episodeAssetActivity.position + tmpAssetList.getCurrentItemCount();
                        } else if (EpisodeAssetActivity.this.currPage == 1) {
                            EpisodeAssetActivity.this.position = 0;
                        } else {
                            EpisodeAssetActivity.this.position = tmpAssetList.getList().size() - 1;
                        }
                        EpisodeAssetActivity.this.assetKeys = new String[alist2.size()];
                        for (int i4 = 0; i4 < alist2.size(); i4++) {
                            EpisodeAssetActivity.this.assetKeys[i4] = alist2.get(i4).toString();
                        }
                        break;
                    }
                    break;
            }
            if (msg.what != 0) {
                Intent intent = new Intent();
                intent.setClass(EpisodeAssetActivity.this._context, EpisodeAssetActivity.class);
                intent.putExtra("position", EpisodeAssetActivity.this.position);
                intent.putExtra("assetKeys", EpisodeAssetActivity.this.assetKeys);
                intent.putExtra("assetKey", EpisodeAssetActivity.this.assetKeys[EpisodeAssetActivity.this.position]);
                intent.putExtra("assetType", EpisodeAssetActivity.this.assetType);
                intent.putExtra("currPage", EpisodeAssetActivity.this.currPage);
                intent.putExtra("totalPage", EpisodeAssetActivity.this.totalPage);
                intent.putExtra("listUrl", EpisodeAssetActivity.this.listUrl);
                EpisodeAssetActivity.this.startActivity(intent);
                EpisodeAssetActivity.this.finish();
            }
            super.handleMessage(msg);
        }
    };
    View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.btnPre) {
                EpisodeAssetActivity episodeAssetActivity = EpisodeAssetActivity.this;
                episodeAssetActivity.position = episodeAssetActivity.position + 1;
                if (EpisodeAssetActivity.this.position == EpisodeAssetActivity.this.assetKeys.length) {
                    EpisodeAssetActivity episodeAssetActivity2 = EpisodeAssetActivity.this;
                    episodeAssetActivity2.currPage = episodeAssetActivity2.currPage + 1;
                    EpisodeAssetActivity.this.getData(-1);
                    return;
                }
            } else if (v.getId() == R.id.btnNext) {
                EpisodeAssetActivity episodeAssetActivity3 = EpisodeAssetActivity.this;
                episodeAssetActivity3.position = episodeAssetActivity3.position - 1;
                if (EpisodeAssetActivity.this.position < 0) {
                    EpisodeAssetActivity episodeAssetActivity4 = EpisodeAssetActivity.this;
                    episodeAssetActivity4.currPage = episodeAssetActivity4.currPage - 1;
                    EpisodeAssetActivity.this.getData(1);
                    return;
                }
            } else if (v.getId() == R.id.btnFirst) {
                if (EpisodeAssetActivity.this.totalPage != 1) {
                    EpisodeAssetActivity.this.assetKeys = null;
                    EpisodeAssetActivity.this.currPage = EpisodeAssetActivity.this.totalPage;
                    EpisodeAssetActivity.this.getData(1);
                    return;
                }
                EpisodeAssetActivity.this.position = EpisodeAssetActivity.this.assetKeys.length - 1;
            } else if (v.getId() == R.id.btnLast) {
                if (EpisodeAssetActivity.this.totalPage != 1) {
                    EpisodeAssetActivity.this.assetKeys = null;
                    EpisodeAssetActivity.this.currPage = 1;
                    EpisodeAssetActivity.this.getData(1);
                    return;
                }
                EpisodeAssetActivity.this.position = 0;
            }
            Intent intent = new Intent();
            intent.setClass(EpisodeAssetActivity.this._context, EpisodeAssetActivity.class);
            intent.putExtra("position", EpisodeAssetActivity.this.position);
            intent.putExtra("assetKeys", EpisodeAssetActivity.this.assetKeys);
            intent.putExtra("assetKey", EpisodeAssetActivity.this.assetKeys[EpisodeAssetActivity.this.position]);
            intent.putExtra("assetType", EpisodeAssetActivity.this.assetType);
            intent.putExtra("currPage", EpisodeAssetActivity.this.currPage);
            intent.putExtra("totalPage", EpisodeAssetActivity.this.totalPage);
            intent.putExtra("listUrl", EpisodeAssetActivity.this.listUrl);
            EpisodeAssetActivity.this.startActivity(intent);
            EpisodeAssetActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public int position = 0;
    /* access modifiers changed from: private */
    public int totalPage;
    /* access modifiers changed from: private */
    public long validDataThreadId = -1;

    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.move_asset_activity_head);
        super.onCreate(savedInstanceState);
        Log.v("EpisodeAssetActivity", "Working");
        this._context = this;
        this.position = getIntent().getIntExtra("position", 0);
        this.assetKeys = getIntent().getStringArrayExtra("assetKeys");
        this.assetType = getIntent().getStringExtra("assetType");
        this.listUrl = getIntent().getStringExtra("listUrl");
        this.currPage = getIntent().getIntExtra("currPage", 0);
        this.totalPage = getIntent().getIntExtra("totalPage", 0);
        Log.v("====================", "currPage=" + this.currPage + ",totalPage=" + this.totalPage);
        if (getAsset() != null) {
            ((TextView) findViewById(R.id.assetName)).setText(getAsset().getDisplayName(getApplication()));
            View tabRoot = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.episodeasset_activity, (LinearLayout) findViewById(R.id.root));
            if (getAsset().getAssetType().equals("10")) {
                findViewById(R.id.llSet).setVisibility(8);
                findViewById(R.id.assetDirector).setVisibility(0);
                findViewById(R.id.assetActor).setVisibility(0);
                findViewById(R.id.assetYear).setVisibility(0);
                findViewById(R.id.assetCategory).setVisibility(0);
            } else {
                Button btnPre = (Button) tabRoot.findViewById(R.id.btnPre);
                Button btnNext = (Button) tabRoot.findViewById(R.id.btnNext);
                btnPre.setCompoundDrawablesWithIntrinsicBounds(R.drawable.left, 0, 0, 0);
                btnNext.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right, 0);
                Button btnFirst = (Button) tabRoot.findViewById(R.id.btnFirst);
                Button btnLast = (Button) tabRoot.findViewById(R.id.btnLast);
                btnFirst.setCompoundDrawablesWithIntrinsicBounds(R.drawable.first, 0, 0, 0);
                btnLast.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.last, 0);
                btnFirst.setOnClickListener(this.onClickListener);
                btnNext.setOnClickListener(this.onClickListener);
                btnPre.setOnClickListener(this.onClickListener);
                btnLast.setOnClickListener(this.onClickListener);
                if (this.currPage == this.totalPage && this.assetKeys.length == 1) {
                    btnNext.setEnabled(false);
                    btnLast.setEnabled(false);
                    btnPre.setEnabled(false);
                    btnFirst.setEnabled(false);
                } else if (this.position == 0 && this.currPage == 1) {
                    btnNext.setEnabled(false);
                    btnLast.setEnabled(false);
                    btnPre.setText(getString(R.string.btnPre));
                } else {
                    if (this.position == this.assetKeys.length - 1 && this.currPage == this.totalPage) {
                        btnPre.setEnabled(false);
                        btnFirst.setEnabled(false);
                        btnNext.setText(getString(R.string.btnNext));
                    } else if (this.position != 0 || this.currPage == 1 || this.assetKeys.length <= 20) {
                        btnNext.setEnabled(true);
                        btnPre.setEnabled(true);
                        btnFirst.setEnabled(true);
                        btnLast.setEnabled(true);
                        btnPre.setText(getString(R.string.btnPre));
                        btnNext.setText(getString(R.string.btnNext));
                    } else {
                        btnNext.setEnabled(false);
                        btnLast.setEnabled(false);
                        btnPre.setText(getString(R.string.btnPre));
                    }
                }
            }
            TextView textView = new TextView(this);
            setTabStyle(textView);
            textView.setText(getString(R.string.assetDetailsLabel));
            if (getAsset().getAssetType().equals("10")) {
                TextView assetDirector = (TextView) findViewById(R.id.assetDirector);
                String text = getAsset().getAssetDirector() != null ? getAsset().getAssetDirector() : "";
                assetDirector.setText(String.valueOf(getString(R.string.assetDirectorLabel)) + text);
                String director = text;
                if (!"".equals(director) && director.indexOf(CookieSpec.PATH_DELIM) != -1) {
                    String[] dir = director.split(CookieSpec.PATH_DELIM);
                    for (int i = 0; i < dir.length; i++) {
                        this.list.add(dir[i]);
                    }
                } else if (!"".equals(director)) {
                    this.list.add(director);
                }
                TextView assetActor = (TextView) findViewById(R.id.assetActor);
                String text2 = getAsset().getAssetActor() != null ? getAsset().getAssetActor() : "";
                assetActor.setText(String.valueOf(getString(R.string.assetActorLabel)) + text2);
                String actor = text2;
                if (!"".equals(actor) && actor.indexOf(CookieSpec.PATH_DELIM) != -1) {
                    String[] act = actor.split(CookieSpec.PATH_DELIM);
                    for (int i2 = 0; i2 < act.length; i2++) {
                        this.list.add(act[i2]);
                    }
                } else if (!"".equals(actor)) {
                    this.list.add(actor);
                }
                ImageButton search_btn = (ImageButton) findViewById(R.id.search_dir_act_btn);
                if (this.list == null || this.list.size() <= 0) {
                    search_btn.setVisibility(8);
                } else {
                    CharSequence[] persons = new CharSequence[this.list.size()];
                    for (int i3 = 0; i3 < this.list.size(); i3++) {
                        persons[i3] = this.list.get(i3);
                    }
                    final CharSequence[] charSequenceArr = persons;
                    search_btn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            new AlertDialog.Builder(EpisodeAssetActivity.this._context).setTitle(EpisodeAssetActivity.this.getString(R.string.search_btn)).setItems(charSequenceArr, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String diract = (String) EpisodeAssetActivity.this.list.get(i);
                                    Intent intent = new Intent();
                                    if (diract != null) {
                                        if (diract.trim().indexOf(" ") != -1) {
                                            diract = diract.trim().replace(" ", "+");
                                        }
                                        intent.setAction("android.intent.action.SEARCH");
                                        intent.putExtra("query", diract);
                                        intent.setClass(EpisodeAssetActivity.this._context, SearchActivity.class);
                                        EpisodeAssetActivity.this.startActivity(intent);
                                    }
                                }
                            }).create().show();
                        }
                    });
                }
                ((TextView) findViewById(R.id.assetYear)).setText(String.valueOf(getString(R.string.assetYearLabel)) + (getAsset().getAssetYear() > 0 ? new StringBuilder(String.valueOf(getAsset().getAssetYear())).toString() : ""));
                ((TextView) findViewById(R.id.assetCategory)).setText(String.valueOf(getString(R.string.assetCategoryLabel)) + (getAsset().getCategory() != null ? getAsset().getCategory() : ""));
            }
            ((TextView) findViewById(R.id.episodeIntroduction)).setText(getAsset().getAssetDescription() != null ? getAsset().getAssetDescription() : "");
            ((TextView) findViewById(R.id.episodeSource)).setText(String.valueOf(getString(R.string.assetSourceLabel)) + (getAsset().getAssetSource() != null ? getAsset().getAssetSource() : ""));
            return;
        }
        Log.v("EpisodeAssetActivity", "asset is null:" + getIntent().getStringExtra("assetKey") + "," + getIntent().getStringExtra("assetType"));
    }

    /* access modifiers changed from: protected */
    public synchronized void getData(final int item) {
        Thread t = new Thread() {
            public void run() {
                int currentCount = (EpisodeAssetActivity.this.currPage - 1) * 20;
                EpisodeAssetActivity.this.listUrl = EpisodeAssetActivity.this.listUrl.replace(EpisodeAssetActivity.this.listUrl.substring(EpisodeAssetActivity.this.listUrl.indexOf("start="), EpisodeAssetActivity.this.listUrl.indexOf("&rows")), "start=" + currentCount);
                Log.v("===============", "listUrl=" + EpisodeAssetActivity.this.listUrl);
                AssetList tmpAssetList = EpisodeAssetActivity.this.aparse(EpisodeAssetActivity.this.listUrl);
                if (tmpAssetList == null) {
                    Message m = Message.obtain(EpisodeAssetActivity.this.myViewUpdateHandler);
                    m.what = 0;
                    if (EpisodeAssetActivity.this.assetKeys == null) {
                        EpisodeAssetActivity.this.currPage = EpisodeAssetActivity.this.getIntent().getIntExtra("currPage", 0);
                    } else if (item == -1) {
                        EpisodeAssetActivity episodeAssetActivity = EpisodeAssetActivity.this;
                        episodeAssetActivity.currPage = episodeAssetActivity.currPage - 1;
                    } else if (item == 1) {
                        EpisodeAssetActivity episodeAssetActivity2 = EpisodeAssetActivity.this;
                        episodeAssetActivity2.currPage = episodeAssetActivity2.currPage + 1;
                    }
                    m.sendToTarget();
                } else if (EpisodeAssetActivity.this.validDataThreadId == getId()) {
                    Message m2 = Message.obtain(EpisodeAssetActivity.this.myViewUpdateHandler);
                    m2.what = item;
                    m2.obj = tmpAssetList;
                    m2.sendToTarget();
                }
            }
        };
        this.validDataThreadId = t.getId();
        Log.v("EpisodeAssetActivity", "Change DataThreadId to " + this.validDataThreadId);
        t.start();
    }

    public AssetList aparse(String assetsListUrl) {
        AssetList results = new AssetList();
        AssetListSaxHandler saxHandler = new AssetListSaxHandler(this, results);
        try {
            InputStream input = (InputStream) fetch(assetsListUrl);
            if (input != null) {
                SAXParserFactory.newInstance().newSAXParser().parse(input, saxHandler);
                return results;
            }
            throw new IOException("inputStream is null:" + assetsListUrl);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        Log.v("EpisodeAssetActivity", "Fetching  " + address);
        return new URL(address).getContent();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }
}
