package com.kandian.cartoonapp;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class MicroBlogSaxHandler extends DefaultHandler {
    final String TAG = "MicroBlogSaxHandler";
    private int isUserFlg = 0;
    private MicroBlog microBlog;
    private MicroBlogResults microBlogResults;
    private MicroBlogUser microBlogUser;
    private String textFlg = "";

    public void startDocument() {
        this.microBlogResults = new MicroBlogResults();
    }

    public MicroBlogResults getMicroBlogResults() {
        return this.microBlogResults;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (localName != null && localName.trim().length() != 0) {
            if (localName.equals("totalcount")) {
                this.textFlg = "totalcount";
            } else if (localName.equals("content")) {
                this.microBlog = new MicroBlog();
            } else if (localName.equals("id")) {
                if (this.isUserFlg == 0) {
                    this.textFlg = "content_id";
                } else if (this.isUserFlg == 1) {
                    this.textFlg = "user_id";
                }
            } else if (localName.equals("created_at")) {
                this.textFlg = "created_at";
            } else if (localName.equals("text")) {
                this.textFlg = "text";
            } else if (localName.equals("thumbnail_pic")) {
                this.textFlg = "thumbnail_pic";
            } else if (localName.equals("source")) {
                this.textFlg = "source";
            } else if (localName.equals("user")) {
                this.microBlogUser = new MicroBlogUser();
                this.isUserFlg = 1;
            } else if (localName.equals("screen_name")) {
                this.textFlg = "screen_name";
            } else if (localName.equals("name")) {
                this.textFlg = "name";
            } else if (localName.equals("location")) {
                this.textFlg = "location";
            } else if (localName.equals("description")) {
                this.textFlg = "description";
            } else if (localName.equals("url")) {
                this.textFlg = "url";
            } else if (localName.equals("profile_image_url")) {
                this.textFlg = "profile_image_url";
            } else if (localName.equals("gender")) {
                this.textFlg = "gender";
            }
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (localName != null && localName.trim().length() != 0) {
            if (localName.equals("content")) {
                if (this.microBlog != null) {
                    this.microBlogResults.addMicroBlog(this.microBlog);
                    this.microBlog = null;
                }
            } else if (localName.equals("user") && this.microBlog != null && this.microBlogUser != null) {
                this.microBlog.addMicroBlogUser(this.microBlogUser);
                this.microBlogUser = null;
                this.isUserFlg = 0;
            }
        }
    }

    public void endDocument() {
    }

    public void characters(char[] ch, int start, int length) {
        String data = new String(ch, start, length).trim();
        if (data != null && data.trim().length() != 0) {
            if (this.textFlg.equals("totalcount")) {
                if (data != null && data.trim().length() > 0) {
                    this.microBlogResults.setTotalcount(Integer.parseInt(data));
                }
            } else if (this.textFlg == "content_id") {
                if (this.microBlog != null && data != null && data.trim().length() > 0) {
                    this.microBlog.setContent_id(data);
                }
            } else if (this.textFlg == "created_at") {
                if (this.microBlog != null && data != null && data.trim().length() > 0) {
                    this.microBlog.setContent_createtime(data);
                }
            } else if (this.textFlg == "text") {
                if (this.microBlog != null && data != null && data.trim().length() > 0) {
                    this.microBlog.setContent_text(data);
                }
            } else if (this.textFlg == "thumbnail_pic") {
                if (this.microBlog != null && data != null && data.trim().length() > 0) {
                    this.microBlog.setThumbnail_pic(data);
                }
            } else if (this.textFlg == "source") {
                if (this.microBlog != null && data != null && data.trim().length() > 0) {
                    this.microBlog.setContent_source(data);
                }
            } else if (this.textFlg == "user_id") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setId(data);
                }
            } else if (this.textFlg == "screen_name") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setScreen_name(data);
                }
            } else if (this.textFlg == "screen_name") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setScreen_name(data);
                }
            } else if (this.textFlg == "name") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setName(data);
                }
            } else if (this.textFlg == "location") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setLocation(data);
                }
            } else if (this.textFlg == "description") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setDescription(data);
                }
            } else if (this.textFlg == "url") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setUrl(data);
                }
            } else if (this.textFlg == "profile_image_url") {
                if (this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                    this.microBlogUser.setProfile_image_url(data);
                }
            } else if (this.textFlg == "gender" && this.microBlogUser != null && data != null && data.trim().length() > 0 && this.isUserFlg == 1) {
                this.microBlogUser.setGender(data);
            }
        }
    }
}
