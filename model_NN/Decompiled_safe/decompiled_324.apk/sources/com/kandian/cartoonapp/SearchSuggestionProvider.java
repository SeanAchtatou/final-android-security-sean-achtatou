package com.kandian.cartoonapp;

import android.app.Application;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import com.kandian.common.AssetList;
import com.kandian.common.AssetListSaxHandler;
import com.kandian.common.Log;
import com.kandian.common.SiteConfigs;
import com.kandian.common.SystemConfigs;
import com.kandian.common.VideoAsset;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;

public class SearchSuggestionProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "com.kandian.cartoonapp.SearchSuggestionProvider";
    public static final int MODE = 3;
    private static final String TAG = "SearchSuggestionProvider";
    SystemConfigs systemConfigs = null;

    public SearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, 3);
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Exception e;
        Log.v(TAG, "query begin");
        try {
            String query = selectionArgs[0];
            SearchCursor cursor = new SearchCursor(new String[]{"_ID", "suggest_text_1", "suggest_text_2", "suggest_format", "suggest_intent_query"});
            try {
                List list = query(query);
                if (list == null) {
                    SearchCursor searchCursor = cursor;
                    return super.query(uri, projection, selection, selectionArgs, sortOrder);
                }
                for (int i = 0; i < list.size(); i++) {
                    VideoAsset va = (VideoAsset) list.get(i);
                    cursor.addRow(new String[]{Integer.toString(i), va.getAssetName(), va.getAssetActor(), "0", va.getAssetName()});
                }
                Log.v(TAG, "query end");
                SearchCursor searchCursor2 = cursor;
                return cursor;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return super.query(uri, projection, selection, selectionArgs, sortOrder);
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return super.query(uri, projection, selection, selectionArgs, sortOrder);
        }
    }

    public List query(String query) {
        if (query == null || "".equals(query)) {
            return null;
        }
        String url = getContext().getString(R.string.searchServicePrefix).replace("&rows=20", "&rows=5").replace("{query}", URLEncoder.encode(query));
        this.systemConfigs = SystemConfigs.instance((Application) getContext().getApplicationContext());
        SiteConfigs siteConfigs = this.systemConfigs.getSiteConfigs();
        if (siteConfigs != null) {
            url = String.valueOf(url) + "&" + siteConfigs.getDownloadSourcesQueryString((Application) getContext().getApplicationContext());
        } else {
            Log.v(TAG, "SiteConfigs is null");
        }
        Log.v(TAG, url);
        AssetList assetList = parse(url);
        if (assetList == null) {
            return null;
        }
        return assetList.getList();
    }

    private AssetList parse(String assetsListUrl) {
        AssetList results = new AssetList();
        try {
            SAXParserFactory.newInstance().newSAXParser().parse((InputStream) fetch(assetsListUrl), new AssetListSaxHandler(getContext(), results));
            return results;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object fetch(String address) throws MalformedURLException, IOException {
        Log.v(TAG, "Fetching  " + address);
        return new URL(address).getContent();
    }

    private class SearchCursor extends MatrixCursor {
        public SearchCursor(String[] columnNames) {
            super(columnNames);
        }
    }
}
