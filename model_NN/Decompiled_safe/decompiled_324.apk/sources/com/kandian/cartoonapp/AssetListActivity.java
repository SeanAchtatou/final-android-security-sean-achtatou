package com.kandian.cartoonapp;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.cartoonapp.DownloadService;
import com.kandian.common.AppActiveUtil;
import com.kandian.common.AssetList;
import com.kandian.common.AssetListSaxHandler;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.Log;
import com.kandian.common.SimpleVideoAsset;
import com.kandian.common.SiteConfigs;
import com.kandian.common.SystemConfigs;
import com.kandian.common.UpdateUtil;
import com.kandian.common.activity.CommonResultSet;
import com.kandian.common.activity.CommonSeekBarListActivity;
import com.kandian.ksfamily.KSApp;
import com.kandian.ksfamily.KSFamilyListActivity;
import com.kandian.other.KSAboutActivity;
import com.kandian.other.KSHelpActivity;
import com.kandian.other.PreferenceSettingActivity;
import com.kandian.user.UserService;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class AssetListActivity extends CommonSeekBarListActivity {
    /* access modifiers changed from: private */
    public static String TAG = "AssetListActivity";
    private static int eventRepeatCount = 0;
    /* access modifiers changed from: private */
    public AssetListActivity _context;
    private String alistUrl = "";
    private AsyncImageLoader asyncImageLoader = null;
    /* access modifiers changed from: private */
    public DownloadService downloadService = null;
    View.OnClickListener ksaboutListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(AssetListActivity.this._context, KSAboutActivity.class);
            KSApp ksApp = new KSApp();
            ksApp.setAppname(AssetListActivity.this._context.getString(R.string.app_name));
            ksApp.setPartner(AssetListActivity.this._context.getString(R.string.partner));
            ksApp.setAppicon(R.drawable.kscartoon);
            ksApp.setDescription(AssetListActivity.this._context.getString(R.string.app_description));
            try {
                ksApp.setPackagename(AssetListActivity.this._context.getPackageManager().getPackageInfo(AssetListActivity.this._context.getPackageName(), 0).packageName);
                ksApp.setVersionname(AssetListActivity.this._context.getPackageManager().getPackageInfo(AssetListActivity.this._context.getPackageName(), 0).versionName);
                ksApp.setVersioncode(AssetListActivity.this._context.getPackageManager().getPackageInfo(AssetListActivity.this._context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException e) {
                ksApp.setPackagename("");
                ksApp.setVersionname("");
            }
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("ksAppInfo", ksApp);
            intent.putExtras(mBundle);
            AssetListActivity.this.startActivity(intent);
        }
    };
    View.OnClickListener ksfamilyListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(AssetListActivity.this._context, KSFamilyListActivity.class);
            AssetListActivity.this.startActivity(intent);
        }
    };
    View.OnClickListener kshelpListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(AssetListActivity.this._context, KSHelpActivity.class);
            KSApp ksApp = new KSApp();
            ksApp.setAppname(AssetListActivity.this._context.getString(R.string.app_name));
            ksApp.setAppicon(R.drawable.kscartoon);
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("ksAppInfo", ksApp);
            intent.putExtras(mBundle);
            AssetListActivity.this.startActivity(intent);
        }
    };
    private String listUrl = null;
    View.OnClickListener loginListener = new View.OnClickListener() {
        public void onClick(View v) {
            UserService.getInstance().login(AssetListActivity.this._context, AssetListActivity.this.getString(R.string.loginHomeActivity));
        }
    };
    protected ArrayAdapter<CharSequence> mAdapter;
    private ServiceConnection mConnection = null;
    private boolean mIsBound = false;
    private boolean onlyEpisodes = false;
    View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View v) {
            AssetListActivity.this.refresh();
        }
    };
    View.OnClickListener settingListener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(AssetListActivity.this._context, PreferenceSettingActivity.class);
            intent.putExtra("SystemConfigFilter", true);
            AssetListActivity.this.startActivity(intent);
        }
    };
    private SystemConfigs systemConfigs = null;
    private ProgressDialog v_ProgressDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        this.v_ProgressDialog = ProgressDialog.show(this, "Please wait...", "Retrieving data ...", true);
        super.onCreate(savedInstanceState);
        this._context = this;
        this.asyncImageLoader = AsyncImageLoader.instance();
        setContentView((int) R.layout.assetlist_activity);
        NavigationBar.setup(this);
        this._context = this;
        this.onlyEpisodes = getIntent().getBooleanExtra("onlyepisodes", false);
        if (this.onlyEpisodes) {
            ((LinearLayout) findViewById(R.id.navigation)).setVisibility(8);
        }
        Log.v("AssetListActivity", "onCreate");
        this.listUrl = getIntent().getStringExtra("listurl");
        if (this.listUrl == null || this.listUrl.trim().length() == 0) {
            this.listUrl = getString(R.string.latestcartoonlist);
        }
        ((Button) findViewById(R.id.moreData)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AssetListActivity.this.getData(1);
            }
        });
        TextView statusView = (TextView) findViewById(R.id.Status);
        if (statusView != null) {
            statusView.setText(getString(R.string.retrieving));
        }
        initList(this._context, new ArrayList(), R.layout.assetrow, R.id.verticalSeekBar, -1);
        getListView().setTextFilterEnabled(true);
        this.v_ProgressDialog.dismiss();
        this.mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                AssetListActivity.this.downloadService = ((DownloadService.DownloadBinder) service).getService();
                Log.v(AssetListActivity.TAG, "downloadService is connected");
            }

            public void onServiceDisconnected(ComponentName className) {
                AssetListActivity.this.downloadService = null;
                Log.v(AssetListActivity.TAG, "downloadService is disconnected");
            }
        };
        doBindService();
    }

    /* access modifiers changed from: protected */
    public View buildRow(Object rowObj, int position, View convertView, ViewGroup parent) {
        View v;
        String categoryText;
        LayoutInflater vi = (LayoutInflater) getSystemService("layout_inflater");
        if (this.onlyEpisodes) {
            v = vi.inflate((int) R.layout.episodeassetrow, (ViewGroup) null);
        } else {
            v = vi.inflate((int) R.layout.assetrow, (ViewGroup) null);
        }
        SimpleVideoAsset asset = (SimpleVideoAsset) rowObj;
        if (asset != null) {
            ImageView imageView = (ImageView) v.findViewById(R.id.assetImage);
            if (imageView != null) {
                imageView.setTag(asset.getImageUrl());
                Bitmap cachedImage = this.asyncImageLoader.loadBitmap(asset.getImageUrl(), new AsyncImageLoader.ImageCallback1() {
                    public void imageLoaded(Bitmap imageDrawable, String imageUrl) {
                        ImageView imageViewByTag = (ImageView) AssetListActivity.this.getListView().findViewWithTag(imageUrl);
                        if (imageViewByTag != null) {
                            imageViewByTag.setImageBitmap(imageDrawable);
                        }
                    }
                });
                if (cachedImage != null) {
                    imageView.setImageBitmap(cachedImage);
                }
            }
            TextView tt = (TextView) v.findViewById(R.id.toptext);
            if (tt != null) {
                tt.setText(asset.getDisplayName());
            }
            if (!this.onlyEpisodes) {
                TextView categoryt = (TextView) v.findViewById(R.id.categorytext);
                if (categoryt != null) {
                    String categoryText2 = asset.getOrigin();
                    if (categoryText2 == null || categoryText2.equals("")) {
                        categoryText = asset.getCategory();
                    } else {
                        categoryText = String.valueOf(categoryText2) + "  " + asset.getCategory();
                    }
                    categoryt.setText(categoryText);
                }
                TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                if (bt != null) {
                    if (asset.getAssetIdX() == 0) {
                        String voteString = "-";
                        if (asset.getVote() >= 0.6d) {
                            voteString = String.valueOf(new Double(asset.getVote() * 100.0d).intValue()) + "%";
                            bt.setTextColor(getResources().getColor(R.color.good_vote_color));
                        } else if (asset.getVote() >= 0.0d) {
                            voteString = String.valueOf(new Double(asset.getVote() * 100.0d).intValue()) + "%";
                            bt.setTextColor(getResources().getColor(R.color.bad_vote_color));
                        } else {
                            bt.setTextColor(getResources().getColor(R.drawable.white));
                        }
                        bt.setText(voteString);
                    } else {
                        bt.setTextColor(getResources().getColor(R.drawable.white));
                    }
                }
            }
        }
        Log.v("==================", String.valueOf(this.flag) + "=======" + position + "%%%%%%%%%%%%" + getRowAdapterCount() + "," + getRowAdapterCount());
        if (this.flag) {
            if (position == getRowAdapterCount() - 1) {
                if (getRowAdapterCount() < this.totalResultCount && this.currPage < this.totalpage) {
                    getData(1);
                }
            } else if (position == 0 && this.PrePage > 1) {
                getData(-1);
            }
        } else if (!this.flag && position == 0) {
            this.flag = true;
        }
        return v;
    }

    /* access modifiers changed from: protected */
    public void beforeGetData() {
        ((ProgressBar) findViewById(R.id.statusProgress)).setVisibility(0);
        ((TextView) findViewById(R.id.Loading)).setText(getString(R.string.getdata));
        ((Button) findViewById(R.id.moreData)).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void afterGetData(BaseAdapter ba, CommonResultSet ars, int currPage, int start, int end) {
        if (this.onlyEpisodes && this.verticalSeekBar.getVisibility() == 0 && ars.getNumFound() < 80) {
            this.verticalSeekBar.setVisibility(8);
        }
        TextView statusView = (TextView) findViewById(R.id.Status);
        ((ProgressBar) findViewById(R.id.statusProgress)).setVisibility(8);
        ((TextView) findViewById(R.id.Loading)).setText("第" + currPage + "页 ");
        if (ars != null) {
            statusView.setText("已加载" + start + "-" + end + CookieSpec.PATH_DELIM + ars.getNumFound() + "条资源。");
        }
    }

    /* access modifiers changed from: protected */
    public void errorGetData() {
        ((Button) findViewById(R.id.moreData)).setVisibility(0);
        Toast.makeText(this._context, getString(R.string.network_problem), 0).show();
    }

    /* access modifiers changed from: protected */
    public void onUpdatePage(int currPage) {
        ((TextView) findViewById(R.id.Loading)).setText("第" + currPage + "页 ");
    }

    /* access modifiers changed from: protected */
    public CommonResultSet buildData(int start, int page_szie) {
        CommonResultSet commonResultSet = new CommonResultSet();
        String parseUrl = this.listUrl.replace("start=0", "start=" + start);
        if (this.systemConfigs == null) {
            this.systemConfigs = SystemConfigs.instance(getApplication());
            if (this.systemConfigs == null) {
                return null;
            }
        }
        SiteConfigs siteConfigs = this.systemConfigs.getSiteConfigs();
        if (siteConfigs != null) {
            parseUrl = String.valueOf(parseUrl) + "&" + siteConfigs.getDownloadSourcesQueryString(getApplication());
        } else {
            Log.v(TAG, "SiteConfigs is null");
        }
        Log.v(TAG, "======================" + parseUrl);
        this.alistUrl = parseUrl;
        AssetList tmpAssetList = parse(parseUrl);
        List<SimpleVideoAsset> list = new ArrayList<>();
        if (tmpAssetList == null) {
            return null;
        }
        if (tmpAssetList.getCurrentItemCount() > 0) {
            for (int i = 0; i < tmpAssetList.getCurrentItemCount(); i++) {
                list.add(new SimpleVideoAsset(tmpAssetList.get(i), getApplication()));
            }
        }
        int numfound = tmpAssetList.getTotalResultCount();
        commonResultSet.setResults(list);
        commonResultSet.setNumFound(numfound);
        commonResultSet.setStart(start);
        return commonResultSet;
    }

    public void onStart() {
        Log.v(TAG, "onStart()");
        Log.v(TAG, "onStart onlyEpisodes = " + this.onlyEpisodes);
        if (this.onlyEpisodes) {
            getData(1);
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v(TAG, "onResume()");
        eventRepeatCount = 0;
        if (getListAdapter() == null || getListAdapter().getCount() == 0) {
            getData(1);
        }
        UpdateUtil.checkUpdate(this);
        AppActiveUtil.appActive(this, getString(R.string.partner));
        UserService.autologin(this._context);
        super.onResume();
    }

    /* access modifiers changed from: package-private */
    public void doBindService() {
        startService(new Intent(this, DownloadService.class));
        if (bindService(new Intent(this, DownloadService.class), this.mConnection, 1)) {
            Log.v(TAG, "succeeding in binding service");
        } else {
            Log.v(TAG, "failed in binding service");
        }
        this.mIsBound = true;
    }

    /* access modifiers changed from: package-private */
    public void doUnbindService() {
        if (this.mIsBound) {
            unbindService(this.mConnection);
            this.mIsBound = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i(TAG, "Stop looping the child thread's message queue");
        doUnbindService();
        super.onDestroy();
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        Log.v("AssetListActivity", "Fetching  " + address);
        return new URL(address).getContent();
    }

    public AssetList parse(String assetsListUrl) {
        AssetList results = new AssetList();
        AssetListSaxHandler saxHandler = new AssetListSaxHandler(this, results);
        try {
            InputStream input = (InputStream) fetch(assetsListUrl);
            if (input != null) {
                SAXParserFactory.newInstance().newSAXParser().parse(input, saxHandler);
                return results;
            }
            throw new IOException("inputStream is null:" + assetsListUrl);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v("AssetListActivity", "Starting AssetActivity at position" + position);
        SimpleVideoAsset asset = (SimpleVideoAsset) getItemObject(position);
        Intent intent = new Intent();
        if (this.onlyEpisodes) {
            String[] assetKeys = new String[getRowAdapterCount()];
            for (int i = 0; i < getRowAdapterCount(); i++) {
                assetKeys[i] = ((SimpleVideoAsset) getItemObject(i)).getAssetKey();
            }
            intent.setClass(this, EpisodeAssetActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("assetKeys", assetKeys);
            intent.putExtra("currPage", this.currPage);
            intent.putExtra("totalPage", this.totalpage);
            intent.putExtra("listUrl", this.alistUrl);
        } else if (asset.getAssetType().equals("10")) {
            intent.setClass(this, EpisodeAssetActivity.class);
        } else {
            intent.setClass(this, MovieAssetActivity.class);
        }
        intent.putExtra("assetKey", asset.getAssetKey());
        intent.putExtra("assetType", asset.getAssetType());
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.onlyEpisodes) {
            return true;
        }
        getMenuInflater().inflate(R.menu.assetlistmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh /*2131361965*/:
                this.refreshListener.onClick(getListView());
                return true;
            case R.id.menu_setting /*2131361966*/:
                this.settingListener.onClick(findViewById(R.id.menu_refresh));
                return true;
            case R.id.menu_ksfamily /*2131361967*/:
                this.ksfamilyListener.onClick(getListView());
                return true;
            case R.id.menu_kshelp /*2131361968*/:
                this.kshelpListener.onClick(getListView());
                return true;
            case R.id.menu_ksabout /*2131361969*/:
                this.ksaboutListener.onClick(getListView());
                try {
                    Debug.dumpHprofData("/sdcard/dump.hprof");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.menu_login /*2131361970*/:
                this.loginListener.onClick(getListView());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.onlyEpisodes) {
            if (keyCode == 4 && eventRepeatCount == 0) {
                Toast.makeText(this, "再按一次返回程序退出。", 0).show();
                eventRepeatCount = 1;
                return true;
            } else if (keyCode == 4) {
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
