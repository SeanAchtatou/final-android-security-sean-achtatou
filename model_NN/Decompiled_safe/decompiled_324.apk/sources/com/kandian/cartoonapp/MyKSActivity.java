package com.kandian.cartoonapp;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TextView;
import com.kandian.common.AppActiveUtil;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.UpdateUtil;

public class MyKSActivity extends TabActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.my_activity);
        NavigationBar.setup(this);
        TabHost tabHost = getTabHost();
        TextView favoriteTab = new TextView(this);
        TabUtil.setTabStyle(this, favoriteTab);
        favoriteTab.setText(getString(R.string.str_ks_favorite));
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(favoriteTab).setContent(new Intent(this, MyViewsActivity.class)));
        TextView historyTab = new TextView(this);
        TabUtil.setTabStyle(this, historyTab);
        historyTab.setText(getString(R.string.str_ks_history));
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(historyTab).setContent(new Intent(this, MyHistoryActivity.class)));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        UpdateUtil.checkUpdate(this);
        AppActiveUtil.appActive(this, getString(R.string.partner));
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public boolean onSearchRequested() {
        boolean result = super.onSearchRequested();
        Log.v("GuideActivity", "onSearchRequested " + result);
        return result;
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
