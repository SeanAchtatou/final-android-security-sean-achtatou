package com.kandian.cartoonapp;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.kandian.common.AsyncImageLoader;
import com.kandian.common.FavoriteAsset;
import com.kandian.common.Log;
import com.kandian.common.MobclickAgentWrapper;
import com.kandian.common.StatisticsUtil;
import com.kandian.common.asynchronous.AsyncCallback;
import com.kandian.common.asynchronous.AsyncExceptionHandle;
import com.kandian.common.asynchronous.AsyncProcess;
import com.kandian.common.asynchronous.Asynchronous;
import com.kandian.user.UserLoginActivity;
import com.kandian.user.UserService;
import com.kandian.user.favorite.UserFavoriteService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyViewsActivity extends ListActivity {
    private static String TAG = "MyViewsActivity";
    /* access modifiers changed from: private */
    public ArrayList<CheckBox> allCheck = new ArrayList<>();
    /* access modifiers changed from: private */
    public Map<String, FavoriteAsset> allCheckData = new HashMap();
    /* access modifiers changed from: private */
    public AsyncImageLoader asyncImageLoader = null;
    View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View v) {
        }
    };
    /* access modifiers changed from: private */
    public Activity context = null;
    View.OnClickListener deleteListener = new View.OnClickListener() {
        public void onClick(View v) {
            MyViewsActivity.this.modelType = "del";
            MyViewsActivity.this.changModel(0);
        }
    };
    private ArrayList<FavoriteAsset> favorites = null;
    /* access modifiers changed from: private */
    public String modelType = "list";
    private Map myFavorites = null;

    /* access modifiers changed from: private */
    public void loadFavorites() {
        Log.v(TAG, "isNeedSync=" + UserFavoriteService.isNeedSync);
        if (UserFavoriteService.isNeedSync) {
            Asynchronous asynchronous = new Asynchronous(this.context);
            asynchronous.setLoadingMsg("收藏加载中,请稍等...");
            asynchronous.asyncProcess(new AsyncProcess() {
                public int process(Context context, Map<String, Object> map) throws Exception {
                    if (UserFavoriteService.isNeedSync) {
                        UserFavoriteService.getInstance().syncFavorite(MyViewsActivity.this.getString(R.string.appcode), context);
                        UserFavoriteService.isNeedSync = false;
                    }
                    return 0;
                }
            });
            asynchronous.asyncCallback(new AsyncCallback() {
                public void callback(Context context, Map<String, Object> map, Message m) {
                    MyViewsActivity.this.buildFavorites();
                    Toast.makeText(context, "收藏加载成功!", 0).show();
                }
            });
            asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                public void handle(Context context, Exception e, Message m) {
                    MyViewsActivity.this.buildFavorites();
                    UserFavoriteService.isNeedSync = true;
                    Toast.makeText(context, "收藏加载失败,请重试!", 0).show();
                }
            });
            asynchronous.start();
            return;
        }
        buildFavorites();
    }

    /* access modifiers changed from: private */
    public void buildFavorites() {
        TextView emptyStatusView = (TextView) findViewById(16908292);
        if (emptyStatusView != null) {
            emptyStatusView.setText(this.context.getString(R.string.retrieving));
        }
        this.myFavorites = getSharedPreferences(UserFavoriteService.UserFavoriteSharedPreferencesName, 0).getAll();
        this.favorites = new ArrayList<>();
        for (String element : this.myFavorites.values()) {
            FavoriteAsset favoriteAsset = FavoriteAsset.getObject4JsonString(element);
            if (!(favoriteAsset == null || favoriteAsset.getOptaction() == 2)) {
                this.favorites.add(favoriteAsset);
            }
        }
        setListAdapter(new MyviewsAdaptor(this.context, R.layout.asset_favoriterow, this.favorites));
        Log.v(TAG, "favorites size=" + this.favorites.size());
        if (this.favorites.size() == 0 && emptyStatusView != null) {
            emptyStatusView.setText(this.context.getString(R.string.no_favorites));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        setContentView((int) R.layout.myviews_activity);
        Log.v(TAG, "onCreate");
        this.asyncImageLoader = AsyncImageLoader.instance();
        Button login_button = (Button) findViewById(R.id.need_login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(MyViewsActivity.this.context, UserLoginActivity.class);
                MyViewsActivity.this.startActivity(intent);
            }
        });
        String userName = UserService.getInstance().getUsername();
        if (userName == null || userName.trim().length() == 0) {
            login_button.setVisibility(0);
            TextView emptyStatusView = (TextView) findViewById(R.id.no_favorites);
            if (emptyStatusView != null) {
                emptyStatusView.setText(getString(R.string.not_login_long));
            }
            emptyStatusView.setVisibility(0);
        } else {
            loadFavorites();
        }
        ((CheckBox) findViewById(R.id.cbxall)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < MyViewsActivity.this.allCheck.size(); i++) {
                        ((CheckBox) MyViewsActivity.this.allCheck.get(i)).setChecked(isChecked);
                    }
                    return;
                }
                for (int i2 = 0; i2 < MyViewsActivity.this.allCheck.size(); i2++) {
                    ((CheckBox) MyViewsActivity.this.allCheck.get(i2)).setChecked(isChecked);
                }
            }
        });
        final Button btndelete = (Button) findViewById(R.id.btnDelete);
        btndelete.setEnabled(false);
        btndelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FavoriteAsset[] favoriteAssets = new FavoriteAsset[MyViewsActivity.this.allCheckData.size()];
                int i = 0;
                for (String key : MyViewsActivity.this.allCheckData.keySet()) {
                    if (MyViewsActivity.this.allCheckData.containsKey(key)) {
                        favoriteAssets[i] = (FavoriteAsset) MyViewsActivity.this.allCheckData.get(key);
                    }
                    i++;
                }
                final FavoriteAsset[] fas = favoriteAssets;
                Asynchronous asynchronous = new Asynchronous(MyViewsActivity.this.context);
                asynchronous.setLoadingMsg("收藏删除中,请稍等...");
                asynchronous.asyncProcess(new AsyncProcess() {
                    public int process(Context context, Map<String, Object> map) throws Exception {
                        UserFavoriteService.getInstance().delFavorite(MyViewsActivity.this.getString(R.string.appcode), context, fas);
                        return 0;
                    }
                });
                final Button button = btndelete;
                asynchronous.asyncCallback(new AsyncCallback() {
                    public void callback(Context context, Map<String, Object> map, Message m) {
                        Toast.makeText(context, "收藏删除成功!", 0).show();
                        MyViewsActivity.this.loadFavorites();
                        button.setEnabled(false);
                        MyViewsActivity.this.changModel(0);
                    }
                });
                asynchronous.asyncExceptionHandle(new AsyncExceptionHandle() {
                    public void handle(Context context, Exception e, Message m) {
                        Intent intent = new Intent();
                        intent.setClass(context, MyKSActivity.class);
                        context.startActivity(intent);
                        Toast.makeText(context, "收藏删除失败,请重试!", 0).show();
                    }
                });
                asynchronous.start();
            }
        });
        ((Button) findViewById(R.id.btnCancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyViewsActivity.this.modelType = "list";
                MyViewsActivity.this.changModel(8);
            }
        });
    }

    private class MyviewsAdaptor extends ArrayAdapter<FavoriteAsset> {
        private ArrayList items = null;

        public MyviewsAdaptor(Context context, int textViewResourceId, ArrayList<FavoriteAsset> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) MyViewsActivity.this.getSystemService("layout_inflater")).inflate(R.layout.asset_favoriterow, (ViewGroup) null);
            }
            FavoriteAsset favoriteAsset = (FavoriteAsset) this.items.get(position);
            if (favoriteAsset != null) {
                CheckBox ckb = (CheckBox) v.findViewById(R.id.cbxasset);
                ckb.setTag(Long.valueOf(((FavoriteAsset) this.items.get(position)).getAssetId()));
                MyViewsActivity.this.allCheck.add(ckb);
                final FavoriteAsset favoriteAsset2 = favoriteAsset;
                ckb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            Log.v("flag", "allCheckData ADD" + favoriteAsset2.getAssetId());
                            MyViewsActivity.this.allCheckData.put(new StringBuilder(String.valueOf(favoriteAsset2.getAssetId())).toString(), favoriteAsset2);
                        } else {
                            Log.v("flag", "allCheckData REMOVE" + favoriteAsset2.getAssetId());
                            MyViewsActivity.this.allCheckData.remove(new StringBuilder(String.valueOf(favoriteAsset2.getAssetId())).toString());
                        }
                        Button btndelete = (Button) MyViewsActivity.this.findViewById(R.id.btnDelete);
                        if (MyViewsActivity.this.allCheckData.size() == 0) {
                            btndelete.setEnabled(false);
                        } else {
                            btndelete.setEnabled(true);
                        }
                    }
                });
                ImageView imageView = (ImageView) v.findViewById(R.id.assetImage);
                TextView tt = (TextView) v.findViewById(R.id.toptext);
                TextView categoryt = (TextView) v.findViewById(R.id.categorytext);
                if (imageView != null) {
                    if (favoriteAsset.getImageUrl() == null || "".equals(favoriteAsset.getImageUrl())) {
                        imageView.setImageResource(R.drawable.oplusphoto);
                    } else {
                        imageView.setTag(favoriteAsset.getImageUrl());
                        Bitmap cachedImage = MyViewsActivity.this.asyncImageLoader.loadBitmap(favoriteAsset.getImageUrl(), new AsyncImageLoader.ImageCallback1() {
                            public void imageLoaded(Bitmap imageDrawable, String imageUrl) {
                                ImageView imageViewByTag = (ImageView) MyViewsActivity.this.getListView().findViewWithTag(imageUrl);
                                if (imageViewByTag != null) {
                                    imageViewByTag.setImageBitmap(imageDrawable);
                                }
                            }
                        });
                        if (cachedImage != null) {
                            imageView.setImageBitmap(cachedImage);
                        }
                    }
                    imageView.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                        }
                    });
                    if (tt != null) {
                        String type = "";
                        if (favoriteAsset.getAsseType().equals("10")) {
                            type = MyViewsActivity.this.getString(R.string.type_movie);
                        } else if (favoriteAsset.getAsseType().equals("11")) {
                            type = MyViewsActivity.this.getString(R.string.type_series);
                        } else if (favoriteAsset.getAsseType().equals("12")) {
                            type = MyViewsActivity.this.getString(R.string.type_variety);
                        }
                        tt.setText(String.valueOf(type) + favoriteAsset.getAssetName());
                    }
                    if (categoryt != null) {
                        categoryt.setText(favoriteAsset.getOrigin());
                    }
                    TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                    if (bt != null) {
                        double vote = favoriteAsset.getVote();
                        String voteString = "-";
                        if (vote >= 0.6d) {
                            voteString = String.valueOf(new Double(100.0d * vote).intValue()) + "%";
                            bt.setTextColor(MyViewsActivity.this.getResources().getColor(R.color.good_vote_color));
                        } else if (vote >= 0.0d) {
                            voteString = String.valueOf(new Double(100.0d * vote).intValue()) + "%";
                            bt.setTextColor(MyViewsActivity.this.getResources().getColor(R.color.bad_vote_color));
                        } else {
                            bt.setTextColor(MyViewsActivity.this.getResources().getColor(R.drawable.white));
                        }
                        if (favoriteAsset.getIsUpdate() == 1) {
                        }
                        bt.setText(String.valueOf(voteString) + " ");
                    }
                    TextView assetStatus = (TextView) v.findViewById(R.id.assetStatus);
                    if (assetStatus != null) {
                        String statusMsg = "";
                        if (favoriteAsset.getStatus() == 1) {
                            statusMsg = "(资源已失效)";
                        }
                        assetStatus.setText(statusMsg);
                    }
                }
            }
            MyViewsActivity.this.changModel(0);
            return v;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.v(TAG, "Starting AssetActivity at position" + position);
        FavoriteAsset favoriteAsset = (FavoriteAsset) ((MyviewsAdaptor) getListAdapter()).getItem(position);
        if (this.modelType.equals("del")) {
            if (this.allCheck != null && this.allCheck.size() > 0) {
                for (int i = 0; i < this.allCheck.size(); i++) {
                    CheckBox ckb = this.allCheck.get(position);
                    if (favoriteAsset.getAssetId() == Long.parseLong(ckb.getTag().toString())) {
                        if (ckb.isChecked()) {
                            ckb.setChecked(false);
                        } else {
                            ckb.setChecked(true);
                        }
                    }
                }
            }
        } else if (this.modelType.equals("list")) {
            Intent intent = new Intent();
            if (favoriteAsset.getAsseType().equals("10")) {
                intent.setClass(this, EpisodeAssetActivity.class);
            } else {
                intent.setClass(this, MovieAssetActivity.class);
            }
            intent.putExtra("assetKey", favoriteAsset.getAssetKey());
            intent.putExtra("assetType", favoriteAsset.getAsseType());
            startActivity(intent);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.myviewsmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_back /*2131361974*/:
                this.backListener.onClick(findViewById(R.id.menu_back));
                return true;
            case R.id.menu_delete /*2131361975*/:
                this.deleteListener.onClick(findViewById(R.id.menu_delete));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void changModel(int model) {
        if ((this.modelType.equals("del") && model == 0) || (this.modelType.equals("list") && 8 == model)) {
            if (findViewById(R.id.LinearLayout02).getVisibility() == 0 && 8 == model) {
                findViewById(R.id.LinearLayout02).setVisibility(model);
            } else if (findViewById(R.id.LinearLayout02).getVisibility() == 8 && model == 0) {
                findViewById(R.id.LinearLayout02).setVisibility(model);
            }
            if (findViewById(R.id.LinearLayout01).getVisibility() == 0 && 8 == model) {
                findViewById(R.id.LinearLayout01).setVisibility(model);
            } else if (findViewById(R.id.LinearLayout01).getVisibility() == 8 && model == 0) {
                findViewById(R.id.LinearLayout01).setVisibility(model);
            }
            for (int i = 0; i < this.allCheck.size(); i++) {
                CheckBox ckb = this.allCheck.get(i);
                if (ckb.getVisibility() == 0 && 8 == model) {
                    ckb.setVisibility(model);
                } else if (ckb.getVisibility() == 8 && model == 0) {
                    ckb.setVisibility(model);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        StatisticsUtil.updateCount(this);
        super.onResume();
        MobclickAgentWrapper.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgentWrapper.onPause(this);
    }
}
